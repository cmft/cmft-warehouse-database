﻿

create procedure ETL.BuildAPCBaseVTEAssessmentReference 
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseVTEAssessmentReference
			where
				BaseVTEAssessmentReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseVTEAssessmentReference
where
	not exists
	(
	select
		1
	from
		APC.BaseVTEAssessment
	where
		BaseVTEAssessment.MergeVTEAssessmentRecno = BaseVTEAssessmentReference.MergeVTEAssessmentRecno
	)
and	BaseVTEAssessmentReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseVTEAssessmentReference target
using
	(
	select
		BaseVTEAssessment.MergeVTEAssessmentRecno
		,ContextID = Context.ContextID
		,AssessmentDateID =
			coalesce(
				 AssessmentDate.DateID

				,case
				when BaseVTEAssessment.AssessmentDate is null
				then NullDate.DateID

				when BaseVTEAssessment.AssessmentDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,PreventativeMedicationStartDateID =
			coalesce(
				 PreventativeMedicationStartDate.DateID

				,case
				when BaseVTEAssessment.PreventativeMedicationStartDate is null
				then NullDate.DateID

				when BaseVTEAssessment.PreventativeMedicationStartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,PreventativeNursingActionStartDateID =
			coalesce(
				 PreventativeNursingActionStartDate.DateID

				,case
				when BaseVTEAssessment.PreventativeNursingActionStartDate is null
				then NullDate.DateID

				when BaseVTEAssessment.PreventativeNursingActionStartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,NoAssessmentReasonID
		,WardID = Ward.SourceWardID	
		,BaseVTEAssessment.Created
		,BaseVTEAssessment.Updated
		,BaseVTEAssessment.ByWhom
	from
		APC.BaseVTEAssessment

	inner join WH.Context
	on	Context.ContextCode = BaseVTEAssessment.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(BaseVTEAssessment.WardCode, '-1')
	and	Ward.SourceContextCode = BaseVTEAssessment.ContextCode

	inner join APC.VTEAssessmentNotRequiredReason
	on	VTEAssessmentNotRequiredReason.SourceVTEAssessmentNotRequiredReasonCode = coalesce(BaseVTEAssessment.NoAssessmentReasonID, '-1')
	and	VTEAssessmentNotRequiredReason.SourceContextCode = BaseVTEAssessment.ContextCode

	left join WH.Calendar AssessmentDate
	on	AssessmentDate.TheDate = BaseVTEAssessment.AssessmentDate

	left join WH.Calendar PreventativeMedicationStartDate
	on	PreventativeMedicationStartDate.TheDate = BaseVTEAssessment.PreventativeMedicationStartDate

	left join WH.Calendar PreventativeNursingActionStartDate
	on	PreventativeNursingActionStartDate.TheDate = BaseVTEAssessment.PreventativeNursingActionStartDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseVTEAssessment.Updated > @LastUpdated
	and	BaseVTEAssessment.ContextCode = @ContextCode

	) source
	on	source.MergeVTEAssessmentRecno = target.MergeVTEAssessmentRecno

	when not matched
	then
		insert
			(
			MergeVTEAssessmentRecno
			,ContextID
			,AssessmentDateID
			,PreventativeMedicationStartDateID
			,PreventativeNursingActionStartDateID
			,NoAssessmentReasonID
			,WardID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeVTEAssessmentRecno
			,source.ContextID
			,source.AssessmentDateID
			,source.PreventativeMedicationStartDateID
			,source.PreventativeNursingActionStartDateID
			,source.NoAssessmentReasonID
			,source.WardID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeVTEAssessmentRecno = source.MergeVTEAssessmentRecno
			,target.ContextID = source.ContextID
			,target.AssessmentDateID = source.AssessmentDateID
			,target.PreventativeMedicationStartDateID = source.PreventativeMedicationStartDateID
			,target.PreventativeNursingActionStartDateID = source.PreventativeNursingActionStartDateID
			,target.NoAssessmentReasonID = source.NoAssessmentReasonID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






﻿






CREATE proc [ETL].[BuildHRG4OP]

as

/* Process OP Encounter */

-- Delete records with no associated OP Encounter record

delete from OP.HRG4Encounter
where
	not exists
	(
	select
		1
	from
		OP.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
	)


-- Delete records that are about to be reinserted

delete from OP.HRG4Encounter
where
	exists
	(
	select
		1
	from
		ETL.HRG4OPEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
	)


insert into OP.HRG4Encounter
	(
	 [MergeEncounterRecno]
	,[HRGCode]
	,[GroupingMethodFlag]
	,[DominantOperationCode]
	,Created
	,ByWhom
	)

select
	 Encounter.MergeEncounterRecno
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,case when Encounter.GroupingMethodFlag = '' then null else Encounter.GroupingMethodFlag end
	,case when Encounter.DominantOperationCode = '' then null else Encounter.DominantOperationCode end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4OPEncounter Encounter


/* Process Unbundled */

-- Delete records with no associated OP Encounter record

delete from OP.HRG4Unbundled
where
	not exists
	(
	select
		1
	from
		OP.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
	)

-- Delete records that are about to be reinserted

--delete from OP.HRG4Unbundled
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.HRG4OPUnbundled Unbundled

--	inner join ETL.HRG4OPEncounter Encounter
--	on	Encounter.RowNo = Unbundled.RowNo
--	and	Encounter.FinancialYear = Unbundled.FinancialYear

--	where
--		Encounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
--	)



-- delete records that are about to be reinserted

-- this method will delete all MergeEncounterRecnos that are in the ETL.Encounter irrespective of whether in destination table or not.
-- ensures that only correct recnos are in table after proc completes

delete from OP.HRG4Unbundled
where
	exists
	(
	select
		1
	from
		ETL.HRG4OPEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
	)


insert into OP.HRG4Unbundled
	(
	 MergeEncounterRecno
	,SequenceNo
	,HRGCode
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,Unbundled.SequenceNo
	,case when Unbundled.HRGCode = '' then null else Unbundled.HRGCode end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4OPUnbundled Unbundled

inner join ETL.HRG4OPEncounter Encounter
on	Unbundled.RowNo = Encounter.RowNo
and	Unbundled.FinancialYear = Encounter.FinancialYear


/* Process Quality */

-- Delete records with no associated OP Encounter record

delete from OP.HRG4Quality
where
	not exists
	(
	select
		1
	from
		OP.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
	)

-- Delete records that might be reinserted (if we don't use this method then rows in quality table will remain even if sucessfully processed subsequently)

delete from OP.HRG4Quality
where
	exists
	(
	select
		1
	from
		ETL.HRG4OPEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
	)

-- Delete records that are about to be reinserted

--delete from OP.HRG4Quality
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.HRG4OPQuality Quality

--	inner join ETL.HRG4OPEncounter Encounter
--	on	Encounter.RowNo = Quality.RowNo
--	and	Encounter.FinancialYear = Quality.FinancialYear

--	where
--		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
--	)


insert into OP.HRG4Quality
	(
	 MergeEncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4OPQuality Quality

inner join ETL.HRG4OPEncounter Encounter
on	Quality.RowNo = Encounter.RowNo
and	Quality.FinancialYear = Encounter.FinancialYear


--assign HRG to BaseEncounter
update
	OP.BaseEncounter
set
	HRGCode = HRG4Encounter.HRGCode
from
	OP.BaseEncounter

inner join OP.HRG4Encounter
on	HRG4Encounter.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno








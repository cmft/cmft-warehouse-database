﻿
create procedure [ETL].[BuildAPCProcessListArchive] as

insert
into
	APC.ProcessListArchive
(
	 MergeRecno
	,ArchiveTime
)
select
	 MergeRecno
	,ArchiveTime = getdate()
from
	APC.ProcessList;

truncate table APC.ProcessList;


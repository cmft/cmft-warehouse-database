﻿CREATE procedure [ETL].[BuildComplaintBaseComplaint]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Complaint.BaseComplaint target
using 
	(
	select
		ComplaintRecno
		,SourceComplaintCode
		,ComplaintCode
		,ClaimantCode
		,ClaimTypeCode
		,BehalfOfCode
		,BehalfOfTypeCode
		,CaseNumber
		,ReceivedFromCode
		,ReceiptDate
		,AcknowledgementDate
		,AcknowledgementMethodCode
		,ComplaintDetail
		,ResolutionDate
		,OutcomeDetail
		,Satisfied
		,ResponseDate
		,DelayReasonCode
		,SeverityCode
		,ResponseDueDate
		,LetterDate
		,GradeCode
		,LikelihoodCode
		,RiskRatingCode
		,RiskScore
		,HandlerCode
		,StatusTypeCode
		,CaseTypeCode
		,ConsentRequired
		,ConsentDate
		,EventDate
		,InitialContactDate
		,InitialDueDate
		,InitialSeverityCode
		,InitalScore
		,SourceTargetDate
		,SequenceNo
		,CategoryCode
		,CategoryTypeCode
		,PrimaryOrSecondary
		,OrganisationCode
		,SiteCode
		,SiteTypeCode
		,SourceDivisionCode
		,SourceDirectorateCode
		,DepartmentCode
		,WardCode
		,ServiceCode
		,ProfessionCode
		,Reopened
		,ReopenedDetail
		,ConsentTargetDate
		,TargetDate7Day
		,TargetDate14Day
		,TargetDate25Day
		,TargetDate40Day
		,Duration
		,InterfaceCode
		,Created
		,Updated
		,ByWhom
		,ContextCode
	from
		ETL.TLoadComplaintBaseComplaint

		) source
	on	source.ComplaintRecno = target.ComplaintRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CMFT||ULYSS'
	then delete

	when not matched 
	then
		insert
			(
			ComplaintRecno
			,SourceComplaintCode
			,ComplaintCode
			,ClaimantCode
			,ClaimTypeCode
			,BehalfOfCode
			,BehalfOfTypeCode
			,CaseNumber
			,ReceivedFromCode
			,ReceiptDate
			,AcknowledgementDate
			,AcknowledgementMethodCode
			,ComplaintDetail
			,ResolutionDate
			,OutcomeDetail
			,Satisfied
			,ResponseDate
			,DelayReasonCode
			,SeverityCode
			,ResponseDueDate
			,LetterDate
			,GradeCode
			,LikelihoodCode
			,RiskRatingCode
			,RiskScore
			,HandlerCode
			,StatusTypeCode
			,CaseTypeCode
			,ConsentRequired
			,ConsentDate
			,EventDate
			,InitialContactDate
			,InitialDueDate
			,InitialSeverityCode
			,InitalScore
			,SourceTargetDate
			,SequenceNo
			,CategoryCode
			,CategoryTypeCode
			,PrimaryOrSecondary
			,OrganisationCode
			,SiteCode
			,SiteTypeCode
			,SourceDivisionCode
			,SourceDirectorateCode
			,DepartmentCode
			,WardCode
			,ServiceCode
			,ProfessionCode
			,Reopened
			,ReopenedDetail	
			,ConsentTargetDate
			,TargetDate7Day
			,TargetDate14Day
			,TargetDate25Day
			,TargetDate40Day
			,Duration		
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)

		values
			(
			 source.ComplaintRecno
			,source.SourceComplaintCode
			,source.ComplaintCode
			,source.ClaimantCode
			,source.ClaimTypeCode
			,source.BehalfOfCode
			,source.BehalfOfTypeCode
			,source.CaseNumber
			,source.ReceivedFromCode
			,source.ReceiptDate
			,source.AcknowledgementDate
			,source.AcknowledgementMethodCode
			,source.ComplaintDetail
			,source.ResolutionDate
			,source.OutcomeDetail
			,source.Satisfied
			,source.ResponseDate
			,source.DelayReasonCode
			,source.SeverityCode
			,source.ResponseDueDate
			,source.LetterDate
			,source.GradeCode
			,source.LikelihoodCode
			,source.RiskRatingCode
			,source.RiskScore
			,source.HandlerCode
			,source.StatusTypeCode
			,source.CaseTypeCode
			,source.ConsentRequired
			,source.ConsentDate
			,source.EventDate
			,source.InitialContactDate
			,source.InitialDueDate
			,source.InitialSeverityCode
			,source.InitalScore
			,source.SourceTargetDate
			,source.SequenceNo
			,source.CategoryCode
			,source.CategoryTypeCode
			,source.PrimaryOrSecondary
			,source.OrganisationCode
			,source.SiteCode
			,source.SiteTypeCode
			,source.SourceDivisionCode
			,source.SourceDirectorateCode
			,source.DepartmentCode
			,source.WardCode
			,source.ServiceCode
			,source.ProfessionCode
			,source.Reopened
			,source.ReopenedDetail	
			,source.ConsentTargetDate
			,source.TargetDate7Day
			,source.TargetDate14Day
			,source.TargetDate25Day
			,source.TargetDate40Day
			,source.Duration						
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.ComplaintRecno, 0) = isnull(source.ComplaintRecno, 0)
		and isnull(target.SourceComplaintCode, '') = isnull(source.SourceComplaintCode, '')
		and isnull(target.ComplaintCode, cast('' as varbinary(50))) = isnull(source.ComplaintCode, cast('' as varbinary(50)))
		and isnull(target.ClaimantCode, '') = isnull(source.ClaimantCode, '')
		and isnull(target.ClaimTypeCode, '') = isnull(source.ClaimTypeCode, '')
		and isnull(target.BehalfOfCode, '') = isnull(source.BehalfOfCode, '')
		and isnull(target.BehalfOfTypeCode, '') = isnull(source.BehalfOfTypeCode, '')
		and isnull(target.CaseNumber, '') = isnull(source.CaseNumber, '')
		and isnull(target.ReceivedFromCode, '') = isnull(source.ReceivedFromCode, '')
		and isnull(target.ReceiptDate, getdate()) = isnull(source.ReceiptDate, getdate())
		and isnull(target.AcknowledgementDate, getdate()) = isnull(source.AcknowledgementDate, getdate())
		and isnull(target.AcknowledgementMethodCode, '') = isnull(source.AcknowledgementMethodCode, '')
		and isnull(target.ComplaintDetail, '') = isnull(source.ComplaintDetail, '')
		and isnull(target.ResolutionDate, getdate()) = isnull(source.ResolutionDate, getdate())
		and isnull(target.OutcomeDetail, '') = isnull(source.OutcomeDetail, '')
		and isnull(target.Satisfied, '') = isnull(source.Satisfied, '')
		and isnull(target.ResponseDate, getdate()) = isnull(source.ResponseDate, getdate())
		and isnull(target.DelayReasonCode, '') = isnull(source.DelayReasonCode, '')
		and isnull(target.SeverityCode, '') = isnull(source.SeverityCode, '')
		and isnull(target.ResponseDueDate, getdate()) = isnull(source.ResponseDueDate, getdate())
		and isnull(target.LetterDate, getdate()) = isnull(source.LetterDate, getdate())
		and isnull(target.GradeCode, '') = isnull(source.GradeCode, '')
		and isnull(target.LikelihoodCode, '') = isnull(source.LikelihoodCode, '')
		and isnull(target.RiskRatingCode, '') = isnull(source.RiskRatingCode, '')
		and isnull(target.RiskScore, 0) = isnull(source.RiskScore,0)
		and isnull(target.HandlerCode, '') = isnull(source.HandlerCode, '')
		and isnull(target.StatusTypeCode, 0) = isnull(source.StatusTypeCode, 0)
		and isnull(target.CaseTypeCode, 0) = isnull(source.CaseTypeCode, 0)
		and isnull(target.ConsentRequired, 0) = isnull(source.ConsentRequired, 0)
		and isnull(target.ConsentDate, getdate()) = isnull(source.ConsentDate, getdate())
		and isnull(target.EventDate, getdate()) = isnull(source.EventDate, getdate())
		and isnull(target.InitialContactDate, getdate()) = isnull(source.InitialContactDate, getdate())
		and isnull(target.InitialDueDate, getdate()) = isnull(source.InitialDueDate, getdate())
		and isnull(target.InitialSeverityCode, '') = isnull(source.InitialSeverityCode, '')
		and isnull(target.InitalScore, 0) = isnull(source.InitalScore, 0)
		and isnull(target.SourceTargetDate, getdate()) = isnull(source.SourceTargetDate, getdate())
		and isnull(target.SequenceNo, 0) = isnull(source.SequenceNo, 0)
		and isnull(target.CategoryCode, cast('' as varbinary(50))) = isnull(source.CategoryCode, cast('' as varbinary(50)))
		and isnull(target.CategoryTypeCode, cast('' as varbinary(50))) = isnull(source.CategoryTypeCode, cast('' as varbinary(50)))
		and isnull(target.PrimaryOrSecondary, '') = isnull(source.PrimaryOrSecondary, '')
		and isnull(target.OrganisationCode, '') = isnull(source.OrganisationCode, '')
		and isnull(target.SiteCode, cast('' as varbinary(50))) = isnull(source.SiteCode, cast('' as varbinary(50)))
		and isnull(target.SiteTypeCode, '') = isnull(source.SiteTypeCode, '')
		and isnull(target.SourceDivisionCode, cast('' as varbinary(50))) = isnull(source.SourceDivisionCode, cast('' as varbinary(50)))
		and isnull(target.SourceDirectorateCode, cast('' as varbinary(50))) = isnull(source.SourceDirectorateCode, cast('' as varbinary(50)))
		and isnull(target.DepartmentCode, cast('' as varbinary(50))) = isnull(source.DepartmentCode, cast('' as varbinary(50)))
		and isnull(target.WardCode, cast('' as varbinary(50))) = isnull(source.WardCode, cast('' as varbinary(50)))
		and isnull(target.ServiceCode, '') = isnull(source.ServiceCode, '')
		and isnull(target.ProfessionCode, '') = isnull(source.ProfessionCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.Reopened, '') = isnull(source.Reopened, '')
		and isnull(target.ReopenedDetail, '') = isnull(source.ReopenedDetail, '')
		and isnull(target.ConsentTargetDate, getdate()) = isnull(source.ConsentTargetDate, getdate())
		and isnull(target.TargetDate7Day, getdate()) = isnull(source.TargetDate7Day, getdate())
		and isnull(target.TargetDate14Day, getdate()) = isnull(source.TargetDate14Day	, getdate())		
		and isnull(target.TargetDate25Day, getdate()) = isnull(source.TargetDate25Day, getdate())
		and isnull(target.TargetDate40Day, getdate()) = isnull(source.TargetDate40Day	, getdate())	
		and isnull(target.Duration, 0) = isnull(source.Duration, 0)	
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	
	then 
		update 
		set
			target.ComplaintRecno = source.ComplaintRecno
			,target.SourceComplaintCode = source.SourceComplaintCode
			,target.ComplaintCode = source.ComplaintCode
			,target.ClaimantCode = source.ClaimantCode
			,target.ClaimTypeCode = source.ClaimTypeCode
			,target.BehalfOfCode = source.BehalfOfCode
			,target.BehalfOfTypeCode = source.BehalfOfTypeCode
			,target.CaseNumber = source.CaseNumber
			,target.ReceivedFromCode = source.ReceivedFromCode
			,target.ReceiptDate = source.ReceiptDate
			,target.AcknowledgementDate = source.AcknowledgementDate
			,target.AcknowledgementMethodCode = source.AcknowledgementMethodCode
			,target.ComplaintDetail = source.ComplaintDetail
			,target.ResolutionDate = source.ResolutionDate
			,target.OutcomeDetail = source.OutcomeDetail
			,target.Satisfied = source.Satisfied
			,target.ResponseDate = source.ResponseDate
			,target.DelayReasonCode = source.DelayReasonCode
			,target.SeverityCode = source.SeverityCode
			,target.ResponseDueDate = source.ResponseDueDate
			,target.LetterDate = source.LetterDate
			,target.GradeCode = source.GradeCode
			,target.LikelihoodCode = source.LikelihoodCode
			,target.RiskRatingCode = source.RiskRatingCode
			,target.RiskScore = source.RiskScore
			,target.HandlerCode = source.HandlerCode
			,target.StatusTypeCode = source.StatusTypeCode
			,target.CaseTypeCode = source.CaseTypeCode
			,target.ConsentRequired = source.ConsentRequired
			,target.ConsentDate = source.ConsentDate
			,target.EventDate = source.EventDate
			,target.InitialContactDate = source.InitialContactDate
			,target.InitialDueDate = source.InitialDueDate
			,target.InitialSeverityCode = source.InitialSeverityCode
			,target.InitalScore = source.InitalScore
			,target.SourceTargetDate = source.SourceTargetDate
			,target.SequenceNo = source.SequenceNo
			,target.CategoryCode = source.CategoryCode
			,target.CategoryTypeCode = source.CategoryTypeCode
			,target.PrimaryOrSecondary = source.PrimaryOrSecondary
			,target.OrganisationCode = source.OrganisationCode
			,target.SiteCode = source.SiteCode
			,target.SiteTypeCode = source.SiteTypeCode
			,target.SourceDivisionCode = source.SourceDivisionCode
			,target.SourceDirectorateCode = source.SourceDirectorateCode
			,target.DepartmentCode = source.DepartmentCode
			,target.WardCode = source.WardCode
			,target.ServiceCode = source.ServiceCode
			,target.ProfessionCode = source.ProfessionCode
			,target.InterfaceCode = source.InterfaceCode
			,target.Reopened = source.Reopened
			,target.ReopenedDetail = source.ReopenedDetail
			,target.ConsentTargetDate = source.ConsentTargetDate
			,target.TargetDate7Day = source.TargetDate7Day	
			,target.TargetDate14Day = source.TargetDate14Day
			,target.TargetDate25Day = source.TargetDate25Day	
			,target.TargetDate40Day = source.TargetDate40Day
			,target.Duration = source.Duration
			,target.ContextCode = source.ContextCode
			,target.Created = source.Created
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime





GO


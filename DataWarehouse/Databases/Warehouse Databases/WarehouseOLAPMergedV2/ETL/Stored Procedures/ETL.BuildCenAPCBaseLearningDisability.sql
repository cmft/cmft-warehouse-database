﻿

CREATE PROCEDURE [ETL].[BuildCenAPCBaseLearningDisability]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.BaseLearningDisability target
using
	(
	select
		LearningDisabilityRecno
		,SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,AssessmentDate	
		,AssessmentTime	
		,KnownLearningDisability	
		,ReasonableAdjustmentsCarePlan	
		,LDPassport
		,WardCode
		,InterfaceCode
		,ContextCode
	from
		ETL.TLoadCenAPCBaseLearningDisability
	) source
	on	source.LearningDisabilityRecno = target.LearningDisabilityRecno
	and	source.ContextCode = target.ContextCode

	when not matched by source
	and target.ContextCode = 'CEN||BEDMAN'
	then delete

	when not matched
	then
		insert
			(
			LearningDisabilityRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,AssessmentDate	
			,AssessmentTime	
			,KnownLearningDisability	
			,ReasonableAdjustmentsCarePlan	
			,LDPassport
			,WardCode
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.LearningDisabilityRecno
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.AssessmentDate	
			,source.AssessmentTime	
			,source.KnownLearningDisability	
			,source.ReasonableAdjustmentsCarePlan	
			,source.LDPassport
			,source.WardCode
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.LearningDisabilityRecno, 0) = isnull(source.LearningDisabilityRecno, 0)
		and	isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and	isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.AssessmentDate, getdate()) = isnull(source.AssessmentDate, getdate())
		and isnull(target.AssessmentTime, getdate()) = isnull(source.AssessmentTime, getdate())
		and isnull(target.KnownLearningDisability, 0) = isnull(source.KnownLearningDisability, 0)
		and isnull(target.ReasonableAdjustmentsCarePlan, 0) = isnull(source.ReasonableAdjustmentsCarePlan, 0)
		and isnull(target.LDPassport, 0) = isnull(source.LDPassport, 0)
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			target.LearningDisabilityRecno = source.LearningDisabilityRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.AssessmentDate = source.AssessmentDate
			,target.AssessmentTime = source.AssessmentTime
			,target.KnownLearningDisability = source.KnownLearningDisability
			,target.ReasonableAdjustmentsCarePlan = source.ReasonableAdjustmentsCarePlan
			,target.LDPassport = source.LDPassport
			,target.WardCode = source.WardCode
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

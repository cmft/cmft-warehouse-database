﻿CREATE procedure ETL.BuildCenTheatreBaseSession

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Theatre.BaseSession target
using
	(
	select
		 SourceUniqueID
		,ActualSessionMinutes
		,PlannedSessionMinutes
		,StartTime
		,EndTime
		,SessionNumber
		,TheatreCode
		,ConsultantCode
		,SpecialtyCode
		,SurgeonCode1
		,SurgeonCode2
		,AnaesthetistCode1
		,AnaesthetistCode2
		,AnaesthetistCode3
		,ScoutNurseCode
		,InstrumentNurseCode
		,AnaestheticNurseCode
		,LastUpdated
		,TimetableDetailCode
		,Comment
		,SessionOrder
		,CancelledFlag
		,CancelledMinutes
		,OverrunReason
		,OverrunReasonDate
		,OverrunReasonStaffCode
	from
		ETL.TLoadCenTheatreBaseSession
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	and	target.ContextCode = 'CEN||ORMIS'

	when not matched by source
	and	target.ContextCode = 'CEN||ORMIS'
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,ActualSessionMinutes
			,PlannedSessionMinutes
			,StartTime
			,EndTime
			,SessionNumber
			,TheatreCode
			,ConsultantCode
			,SpecialtyCode
			,SurgeonCode1
			,SurgeonCode2
			,AnaesthetistCode1
			,AnaesthetistCode2
			,AnaesthetistCode3
			,ScoutNurseCode
			,InstrumentNurseCode
			,AnaestheticNurseCode
			,LastUpdated
			,TimetableDetailCode
			,Comment
			,SessionOrder
			,CancelledFlag
			,CancelledMinutes
			,OverrunReason
			,OverrunReasonDate
			,OverrunReasonStaffCode
			,ContextCode
			)
		values
			(
			 source.SourceUniqueID
			,source.ActualSessionMinutes
			,source.PlannedSessionMinutes
			,source.StartTime
			,source.EndTime
			,source.SessionNumber
			,source.TheatreCode
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.SurgeonCode1
			,source.SurgeonCode2
			,source.AnaesthetistCode1
			,source.AnaesthetistCode2
			,source.AnaesthetistCode3
			,source.ScoutNurseCode
			,source.InstrumentNurseCode
			,source.AnaestheticNurseCode
			,source.LastUpdated
			,source.TimetableDetailCode
			,source.Comment
			,source.SessionOrder
			,source.CancelledFlag
			,source.CancelledMinutes
			,source.OverrunReason
			,source.OverrunReasonDate
			,source.OverrunReasonStaffCode
			,'CEN||ORMIS'
			)

	when matched
	and not
		(
			target.ActualSessionMinutes = source.ActualSessionMinutes
		and target.PlannedSessionMinutes = source.PlannedSessionMinutes
		and target.StartTime = source.StartTime
		and target.EndTime = source.EndTime
		and target.SessionNumber = source.SessionNumber
		and target.TheatreCode = source.TheatreCode
		and target.ConsultantCode = source.ConsultantCode
		and target.SpecialtyCode = source.SpecialtyCode
		and target.SurgeonCode1 = source.SurgeonCode1
		and target.SurgeonCode2 = source.SurgeonCode2
		and target.AnaesthetistCode1 = source.AnaesthetistCode1
		and target.AnaesthetistCode2 = source.AnaesthetistCode2
		and target.AnaesthetistCode3 = source.AnaesthetistCode3
		and target.ScoutNurseCode = source.ScoutNurseCode
		and target.InstrumentNurseCode = source.InstrumentNurseCode
		and target.AnaestheticNurseCode = source.AnaestheticNurseCode
		and target.LastUpdated = source.LastUpdated
		and target.TimetableDetailCode = source.TimetableDetailCode
		and target.Comment = source.Comment
		and target.SessionOrder = source.SessionOrder
		and target.CancelledFlag = source.CancelledFlag
		and target.CancelledMinutes = source.CancelledMinutes
		and target.OverrunReason = source.OverrunReason
		and target.OverrunReasonDate = source.OverrunReasonDate
		and target.OverrunReasonStaffCode = source.OverrunReasonStaffCode
		)
	then
		update
		set
			 target.ActualSessionMinutes = source.ActualSessionMinutes
			,target.PlannedSessionMinutes = source.PlannedSessionMinutes
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.SessionNumber = source.SessionNumber
			,target.TheatreCode = source.TheatreCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.SurgeonCode1 = source.SurgeonCode1
			,target.SurgeonCode2 = source.SurgeonCode2
			,target.AnaesthetistCode1 = source.AnaesthetistCode1
			,target.AnaesthetistCode2 = source.AnaesthetistCode2
			,target.AnaesthetistCode3 = source.AnaesthetistCode3
			,target.ScoutNurseCode = source.ScoutNurseCode
			,target.InstrumentNurseCode = source.InstrumentNurseCode
			,target.AnaestheticNurseCode = source.AnaestheticNurseCode
			,target.LastUpdated = source.LastUpdated
			,target.TimetableDetailCode = source.TimetableDetailCode
			,target.Comment = source.Comment
			,target.SessionOrder = source.SessionOrder
			,target.CancelledFlag = source.CancelledFlag
			,target.CancelledMinutes = source.CancelledMinutes
			,target.OverrunReason = source.OverrunReason
			,target.OverrunReasonDate = source.OverrunReasonDate
			,target.OverrunReasonStaffCode = source.OverrunReasonStaffCode

output
	$action into @MergeSummary
;


if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿create procedure [ETL].[ImportDiagnosticBase] as

--import the data
exec ETL.BuildDiagnosticBase


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Diagnostic].[Base]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildDiagnosticBaseReference

﻿create procedure ETL.BuildBedOccupancySpecialtySpecialtySectorMap
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	BedOccupancy.SpecialtySpecialtySectorMap target
using
	(
	select
		 SpecialtyCode
		,SpecialtySectorID = cast(SpecialtySectorID as int)
	from
		ETL.TImportSpecialtySpecialtySectorMap
	where
		SpecialtyCode is not null
	) source
	on	source.SpecialtyCode = target.SpecialtyCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SpecialtyCode	  
			,SpecialtySectorID	
			,Created
			,ByWhom
			)
		values
			(
			 source.SpecialtyCode	  
			,source.SpecialtySectorID	
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')	
		and isnull(target.SpecialtySectorID, 0) = isnull(source.SpecialtySectorID, 0)
		)
	then
		update
		set
			 target.SpecialtyCode = source.SpecialtyCode
			,target.SpecialtySectorID = source.SpecialtySectorID
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

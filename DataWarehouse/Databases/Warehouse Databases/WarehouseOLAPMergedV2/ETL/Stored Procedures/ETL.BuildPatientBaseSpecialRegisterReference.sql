﻿


CREATE procedure [ETL].[BuildPatientBaseSpecialRegisterReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Patient.BaseSpecialRegisterReference
			where
				BaseSpecialRegisterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Patient.BaseSpecialRegisterReference
where
	not exists
	(
	select
		1
	from
		Patient.BaseSpecialRegister
	where
		BaseSpecialRegister.MergeSpecialRegisterRecno = BaseSpecialRegisterReference.MergeSpecialRegisterRecno
	)
and	BaseSpecialRegisterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Patient.BaseSpecialRegisterReference target
using
	(
	select
		 BaseLD.MergeSpecialRegisterRecno
		,ContextID = Context.ContextID
		,EnteredDateID =
			coalesce(
				 EnteredDate.DateID

				,case
				when BaseLD.EnteredDate is null
				then NullDate.DateID

				when BaseLD.EnteredDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		
		,SpecialRegisterID = SpecialRegister.SourceSpecialRegisterID	
		,Created = BaseLD.CreatedTime
		,ByWhom = coalesce(BaseLD.UpdatedByWhom,BaseLD.CreatedByWhom)
		,Updated = BaseLD.UpdatedTime
	
	from
		Patient.BaseSpecialRegister BaseLD

	inner join WH.Context
	on	Context.ContextCode = BaseLD.ContextCode

	inner join Patient.SpecialRegister
	on BaseLD.SpecialRegisterCode = SpecialRegister.SourceSpecialRegisterCode
	and BaseLD.ContextCode = SpecialRegister.SourceContextCode

	left join WH.Calendar EnteredDate
	on	EnteredDate.TheDate = BaseLD.EnteredDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseLD.UpdatedTime > @LastUpdated
	and	BaseLD.ContextCode = @ContextCode

	) source
	on	source.MergeSpecialRegisterRecno = target.MergeSpecialRegisterRecno

	when not matched
	then
		insert
			(
			MergeSpecialRegisterRecno
			,ContextID
			,EnteredDateID
			,SpecialRegisterID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeSpecialRegisterRecno
			,source.ContextID
			,source.EnteredDateID
			,source.SpecialRegisterID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeSpecialRegisterRecno = source.MergeSpecialRegisterRecno
			,target.ContextID = source.ContextID
			,target.EnteredDateID = source.EnteredDateID
			,target.SpecialRegisterID = source.SpecialRegisterID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



﻿


CREATE procedure [ETL].[BuildPEXBaseResponseReference] 
	@ContextCode varchar(20)

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)
		

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				PEX.BaseResponseReference
			where
				BaseResponseReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)

delete
from
	PEX.BaseResponseReference
where
	not exists
	(
	select
		1
	from
		PEX.BaseResponseReference
	where
		BaseResponseReference.MergeResponseRecno = BaseResponseReference.MergeResponseRecno
	)
and	BaseResponseReference.ContextID = @ContextID


select
	@deleted = @@ROWCOUNT


merge
	PEX.BaseResponseReference target
using
	(
	select 
		 Encounter.MergeResponseRecno
		,ContextID = Context.ContextID
		,SiteID = '-1' --Site.SourceSiteID
		,WardID = Ward.SourceWardID
		,SurveyID = Survey.SourceSurveyID
		,SurveyDateID = coalesce(Calendar.DateID
							,case
								when Encounter.SurveyDate is null then NullDate.DateID
								when Encounter.SurveyDate < CalendarStartDate.DateValue then PreviousDate.DateID
								else FutureDate.DateID
							end
							)
		,LocationID = Location.SourceLocationID
		,QuestionID = Question.SourceQuestionID
		,AnswerID = Answer.SourceAnswerID
		,ChannelID = Channel.SourceChannelID								
		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		PEX.BaseResponse Encounter

	inner join WH.Context 
	on	Context.ContextCode = Encounter.ContextCode

	--inner join WH.Site 
	--on	Site.SourceSiteCode = coalesce(Encounter.HospitalSiteCode, '-1') 
	--and	Site.SourceContextCode = Encounter.ContextCode
	
	inner join WH.Ward 
	on	Ward.SourceWardCode = coalesce(Encounter.WardID, '-1') 
	and	Ward.SourceContextCode = Encounter.ContextCode
	
	inner join WH.Survey 
	on	Survey .SourceSurveyCode = coalesce(Encounter.SurveyID, '-1') 
	and	Survey .SourceContextCode = Encounter.ContextCode

	inner join WH.Location
	on	Location.SourceLocationCode = coalesce(Encounter.LocationID, '-1') 
	and	Location.SourceContextCode = Encounter.ContextCode
	
	inner join WH.Question
	on	Question.SourceQuestionCode = coalesce(Encounter.QuestionID, '-1') 
	and	Question.SourceContextCode = Encounter.ContextCode

	inner join WH.Answer
	on	Answer.SourceAnswerCode = coalesce(Encounter.AnswerID, '-1') 
	and	Answer.SourceContextCode = Encounter.ContextCode

	inner join WH.Channel
	on	Channel.SourceChannelCode = coalesce(Encounter.ChannelID, '-1') 
	and	Channel.SourceContextCode = Encounter.ContextCode
	
	left join WH.Calendar 
	on	Calendar.TheDate = Encounter.SurveyDate

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'
	
	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'
	
	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'
	
	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'



	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode
	) source
on	source.MergeResponseRecno = target.MergeResponseRecno


when not matched
then
	insert
		(
		 MergeResponseRecno
		,ContextID
		,SiteID
		,WardID
		,SurveyID
		,SurveyDateID
		,LocationID
		,QuestionID
		,AnswerID
		,ChannelID
		,Created
		,Updated
		,ByWhom
		)
	values
		(
		 source.MergeResponseRecno
		,source.ContextID
		,source.SiteID
		,source.WardID
		,source.SurveyID
		,source.SurveyDateID
		,source.LocationID
		,source.QuestionID
		,source.AnswerID
		,source.ChannelID
		,source.Created
		,source.Updated
		,source.ByWhom
		)

when matched and not 
	(
	target.Updated = source.Updated
	)
then
	update
	set
		target.MergeResponseRecno = source.MergeResponseRecno
		,target.ContextID = source.ContextID
		,target.SiteID  = source.SiteID
		,target.WardID  = source.WardID
		,target.SurveyDateID = source.SurveyDateID
		,target.SurveyID = source.SurveyID 
		,target.LocationID = source.LocationID
		,target.QuestionID = source.QuestionID
		,target.AnswerID = source.AnswerID
		,target.ChannelID = source.ChannelID
		,target.Created = source.Created
		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats








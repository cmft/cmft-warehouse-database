﻿




CREATE procedure [ETL].[BuildCenAPCWLBaseEncounter]
as

/******************************************************************************************************
Stored procedure : [ETL].[BuildCenAPCWLBaseEncounter]
Description		 : As proc name

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
17/10/2014	Paul Egan       Added TCITime for day case appointment reminders.
*******************************************************************************************************/

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

--clear out old snapshots
delete
from
	APCWL.BaseEncounter
where 
	not exists
	(
	select
		1
	from
		(
	--Month end snapshots
		select
			Calendar.TheDate
		from
			WH.Calendar
		where
			Calendar.TheDate in
			(
			select
				max(TheDate)
			from
				WH.Calendar
			group by
				 year(TheDate)
				,month(TheDate)

			union
	--snapshots in the last fourteen days
			select
				TheDate
			from
				WH.Calendar
			where
				TheDate between dateadd(day , -14 , getdate()) and getdate()

			union
	--Sunday snapshots
			select
				TheDate
			from
				WH.Calendar
			where
				datename(dw, TheDate) = 'Sunday'
			)
		) Calendar
		where
			BaseEncounter.CensusDate = TheDate
	)
and	BaseEncounter.ContextCode =  'CEN||PAS'

select @deleted = @@ROWCOUNT


merge
	APCWL.BaseEncounter target
using
	(
	select
		 EncounterRecno = cast(EncounterRecno as int)
		,InterfaceCode = cast(InterfaceCode as varchar(5))
		,SourcePatientNo = cast(SourcePatientNo as varchar(20))
		,SourceEncounterNo = cast(SourceEncounterNo as varchar(50))
		,CensusDate = cast(CensusDate as smalldatetime)
		,PatientSurname = cast(PatientSurname as varchar(30))
		,PatientForename = cast(PatientForename as varchar(20))
		,PatientTitle = cast(PatientTitle as varchar(10))
		,SexCode = cast(SexCode as varchar(1))
		,DateOfBirth = cast(DateOfBirth as datetime)
		,DateOfDeath = cast(DateOfDeath as smalldatetime)
		,NHSNumber = cast(NHSNumber as varchar(17))
		,DistrictNo = cast(DistrictNo as varchar(14))
		,MaritalStatusCode = cast(MaritalStatusCode as varchar(1))
		,ReligionCode = cast(ReligionCode as varchar(4))
		,Postcode = cast(Postcode as varchar(10))
		,PatientsAddress1 = cast(PatientsAddress1 as varchar(25))
		,PatientsAddress2 = cast(PatientsAddress2 as varchar(25))
		,PatientsAddress3 = cast(PatientsAddress3 as varchar(25))
		,PatientsAddress4 = cast(PatientsAddress4 as varchar(25))
		,DHACode = cast(DHACode as varchar(3))
		,HomePhone = cast(HomePhone as varchar(23))
		,WorkPhone = cast(WorkPhone as varchar(23))
		,EthnicOriginCode = cast(EthnicOriginCode as varchar(4))
		,ConsultantCode = cast(ConsultantCode as varchar(20))
		,SpecialtyCode = cast(SpecialtyCode as varchar(4))
		,PASSpecialtyCode = cast(PASSpecialtyCode as varchar(4))
		,EpisodeSpecialtyCode = cast(EpisodeSpecialtyCode as varchar(4))
		,ManagementIntentionCode = cast(ManagementIntentionCode as varchar(1))
		,AdmissionMethodCode = cast(AdmissionMethodCode as varchar(2))
		,PriorityCode = cast(PriorityCode as varchar(1))
		,WaitingListCode = cast(WaitingListCode as varchar(6))
		,CommentClinical = cast(CommentClinical as varchar(60))
		,CommentNonClinical = cast(CommentNonClinical as varchar(60))
		,IntendedPrimaryOperationCode = cast(IntendedPrimaryOperationCode as varchar(7))
		,Operation = cast(Operation as varchar(60))
		,SiteCode = cast(SiteCode as varchar(4))
		,WardCode = cast(WardCode as varchar(4))
		,WLStatus = cast(WLStatus as varchar(20))
		,ProviderCode = cast(ProviderCode as varchar(4))
		,PurchaserCode = cast(PurchaserCode as varchar(5))
		,ContractSerialNumber = cast(ContractSerialNumber as varchar(6))
		,CancelledBy = cast(CancelledBy as varchar(3))
		,BookingTypeCode = cast(BookingTypeCode as varchar(4))
		,CasenoteNumber = cast(CasenoteNumber as varchar(14))
		,OriginalDateOnWaitingList = cast(OriginalDateOnWaitingList as smalldatetime)
		,DateOnWaitingList = cast(DateOnWaitingList as smalldatetime)
		,TCIDate = cast(TCIDate as date)
		,KornerWait = cast(KornerWait as int)
		,CountOfDaysSuspended = cast(CountOfDaysSuspended as varchar(4))
		,SuspensionStartDate = cast(SuspensionStartDate as smalldatetime)
		,SuspensionEndDate = cast(SuspensionEndDate as smalldatetime)
		,SuspensionReasonCode = cast(SuspensionReasonCode as varchar(4))
		,SuspensionReason = cast(SuspensionReason as varchar(30))
		,EpisodicGpCode = cast(EpisodicGpCode as varchar(8))
		,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(8))
		,EpisodicGpPracticeCode = cast(EpisodicGpPracticeCode as varchar(8))
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(8))
		,RegisteredPracticeCode = cast(RegisteredPracticeCode as varchar(8))
		,SourceTreatmentFunctionCode = cast(SourceTreatmentFunctionCode as int)
		,TreatmentFunctionCode = cast(TreatmentFunctionCode as int)
		,NationalSpecialtyCode = cast(NationalSpecialtyCode as varchar(10))
		,PCTCode = cast(PCTCode as varchar(8))
		,BreachDate = cast(BreachDate as smalldatetime)
		,ExpectedAdmissionDate = cast(ExpectedAdmissionDate as smalldatetime)
		,ReferralDate = cast(ReferralDate as smalldatetime)
		,ExpectedLOS = cast(ExpectedLOS as varchar(10))
		,ProcedureDate = cast(ProcedureDate as smalldatetime)
		,FutureCancellationDate = cast(FutureCancellationDate as smalldatetime)
		,TheatreKey = cast(TheatreKey as int)
		,TheatreInterfaceCode = cast(TheatreInterfaceCode as char(5))
		,EpisodeNo = cast(EpisodeNo as varchar(10))
		,BreachDays = cast(BreachDays as int)
		,MRSA = cast(MRSA as varchar(10))
		,RTTPathwayID = cast(RTTPathwayID as varchar(25))
		,RTTPathwayCondition = cast(RTTPathwayCondition as varchar(20))
		,RTTStartDate = cast(RTTStartDate as smalldatetime)
		,RTTEndDate = cast(RTTEndDate as smalldatetime)
		,RTTSpecialtyCode = cast(RTTSpecialtyCode as varchar(10))
		,RTTCurrentProviderCode = cast(RTTCurrentProviderCode as varchar(10))
		,RTTCurrentStatusCode = cast(RTTCurrentStatusCode as varchar(10))
		,RTTCurrentStatusDate = cast(RTTCurrentStatusDate as smalldatetime)
		,RTTCurrentPrivatePatientFlag = cast(RTTCurrentPrivatePatientFlag as bit)
		,RTTOverseasStatusFlag = cast(RTTOverseasStatusFlag as bit)
		,NationalBreachDate = cast(NationalBreachDate as smalldatetime)
		,NationalBreachDays = cast(NationalBreachDays as int)
		,DerivedBreachDays = cast(DerivedBreachDays as int)
		,DerivedClockStartDate = cast(DerivedClockStartDate as smalldatetime)
		,DerivedBreachDate = cast(DerivedBreachDate as smalldatetime)
		,RTTBreachDate = cast(RTTBreachDate as smalldatetime)
		,RTTDiagnosticBreachDate = cast(RTTDiagnosticBreachDate as smalldatetime)
		,NationalDiagnosticBreachDate = cast(NationalDiagnosticBreachDate as smalldatetime)
		,SocialSuspensionDays = cast(SocialSuspensionDays as int)
		,AgeCode = cast(AgeCode as varchar(27))
		,Cases = cast(Cases as int)
		,LengthOfWait = cast(LengthOfWait as int)
		,DurationCode = cast(DurationCode as varchar(3))
		,WaitTypeCode = cast(WaitTypeCode as varchar(2))
		,StatusCode = cast(StatusCode as varchar(2))
		,CategoryCode = cast(CategoryCode as varchar(2))
		,AddedToWaitingListTime = cast(AddedToWaitingListTime as smalldatetime)
		,AddedLastWeek = cast(AddedLastWeek as int)
		,WaitingListAddMinutes = cast(WaitingListAddMinutes as decimal)
		,OPCSCoded = cast(OPCSCoded as int)
		,WithRTTStartDate = cast(WithRTTStartDate as int)
		,WithRTTStatusCode = cast(WithRTTStatusCode as int)
		,WithExpectedAdmissionDate = cast(WithExpectedAdmissionDate as int)
		,SourceUniqueID = cast(SourceUniqueID as varchar(255))
		,AdminCategoryCode = cast(AdminCategoryCode as varchar(10))
		,RTTActivity = cast(RTTActivity as bit)
		,RTTBreachStatusCode = cast(RTTBreachStatusCode as varchar(1))
		,DiagnosticProcedure = cast(DiagnosticProcedure as bit)
		,DirectorateCode = cast(DirectorateCode as varchar(5))
		,GuaranteedAdmissionDate = cast(GuaranteedAdmissionDate as smalldatetime)
		,DurationAtTCIDateCode = cast(DurationAtTCIDateCode as varchar(3))
		,WithTCIDate = cast(WithTCIDate as int)
		,WithGuaranteedAdmissionDate = cast(WithGuaranteedAdmissionDate as int)
		,WithRTTOpenPathway = cast(WithRTTOpenPathway as int)
		,RTTPathwayStartDateCurrent = cast(RTTPathwayStartDateCurrent as datetime)
		,RTTWeekBandReturnCode = cast(RTTWeekBandReturnCode as varchar(5))
		,RTTDaysWaiting = cast(RTTDaysWaiting as int)
		,AdmissionReason = cast(AdmissionReason as varchar(255))
		,ContextCode = cast(ContextCode as varchar(8))
		,IsShortNotice = cast(IsShortNotice as bit)
		,RTTComment1 = cast(RTTComment1 as varchar(max))
		,RTTComment2 = cast(RTTComment2 as varchar(max))
		,CCGCode = cast(CCGCode as varchar(10))
		,ResidenceCCGCode = cast(ResidenceCCGCode as varchar(10))
		,TCITime = cast(TCITime as datetime2(0))									-- Added 17/10/2014 Paul Egan
		,FutureCancellationTime = cast(FutureCancellationTime as datetime2(0))		-- Added 17/03/2015 Paul Egan
	from
		ETL.TLoadCenAPCWLBaseEncounter Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno


	when not matched
	then
		insert
			(
			 EncounterRecno
			,InterfaceCode
			,SourcePatientNo
			,SourceEncounterNo
			,CensusDate
			,PatientSurname
			,PatientForename
			,PatientTitle
			,SexCode
			,DateOfBirth
			,DateOfDeath
			,NHSNumber
			,DistrictNo
			,MaritalStatusCode
			,ReligionCode
			,Postcode
			,PatientsAddress1
			,PatientsAddress2
			,PatientsAddress3
			,PatientsAddress4
			,DHACode
			,HomePhone
			,WorkPhone
			,EthnicOriginCode
			,ConsultantCode
			,SpecialtyCode
			,PASSpecialtyCode
			,EpisodeSpecialtyCode
			,ManagementIntentionCode
			,AdmissionMethodCode
			,PriorityCode
			,WaitingListCode
			,CommentClinical
			,CommentNonClinical
			,IntendedPrimaryOperationCode
			,Operation
			,SiteCode
			,WardCode
			,WLStatus
			,ProviderCode
			,PurchaserCode
			,ContractSerialNumber
			,CancelledBy
			,BookingTypeCode
			,CasenoteNumber
			,OriginalDateOnWaitingList
			,DateOnWaitingList
			,TCIDate
			,KornerWait
			,CountOfDaysSuspended
			,SuspensionStartDate
			,SuspensionEndDate
			,SuspensionReasonCode
			,SuspensionReason
			,EpisodicGpCode
			,RegisteredGpPracticeCode
			,EpisodicGpPracticeCode
			,RegisteredGpCode
			,RegisteredPracticeCode
			,SourceTreatmentFunctionCode
			,TreatmentFunctionCode
			,NationalSpecialtyCode
			,PCTCode
			,BreachDate
			,ExpectedAdmissionDate
			,ReferralDate
			,ExpectedLOS
			,ProcedureDate
			,FutureCancellationDate
			,TheatreKey
			,TheatreInterfaceCode
			,EpisodeNo
			,BreachDays
			,MRSA
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,NationalBreachDate
			,NationalBreachDays
			,DerivedBreachDays
			,DerivedClockStartDate
			,DerivedBreachDate
			,RTTBreachDate
			,RTTDiagnosticBreachDate
			,NationalDiagnosticBreachDate
			,SocialSuspensionDays
			,AgeCode
			,Cases
			,LengthOfWait
			,DurationCode
			,WaitTypeCode
			,StatusCode
			,CategoryCode
			,AddedToWaitingListTime
			,AddedLastWeek
			,WaitingListAddMinutes
			,OPCSCoded
			,WithRTTStartDate
			,WithRTTStatusCode
			,WithExpectedAdmissionDate
			,SourceUniqueID
			,AdminCategoryCode
			,RTTActivity
			,RTTBreachStatusCode
			,DiagnosticProcedure
			,DirectorateCode
			,GuaranteedAdmissionDate
			,DurationAtTCIDateCode
			,WithTCIDate
			,WithGuaranteedAdmissionDate
			,WithRTTOpenPathway
			,RTTPathwayStartDateCurrent
			,RTTWeekBandReturnCode
			,RTTDaysWaiting
			,AdmissionReason
			,ContextCode
			,IsShortNotice
			,RTTComment1
			,RTTComment2
			,CCGCode
			,ResidenceCCGCode
			,Created
			,Updated
			,ByWhom
			,TCITime
			,FutureCancellationTime
			)
		values
			(
			 EncounterRecno
			,InterfaceCode
			,SourcePatientNo
			,SourceEncounterNo
			,CensusDate
			,PatientSurname
			,PatientForename
			,PatientTitle
			,SexCode
			,DateOfBirth
			,DateOfDeath
			,NHSNumber
			,DistrictNo
			,MaritalStatusCode
			,ReligionCode
			,Postcode
			,PatientsAddress1
			,PatientsAddress2
			,PatientsAddress3
			,PatientsAddress4
			,DHACode
			,HomePhone
			,WorkPhone
			,EthnicOriginCode
			,ConsultantCode
			,SpecialtyCode
			,PASSpecialtyCode
			,EpisodeSpecialtyCode
			,ManagementIntentionCode
			,AdmissionMethodCode
			,PriorityCode
			,WaitingListCode
			,CommentClinical
			,CommentNonClinical
			,IntendedPrimaryOperationCode
			,Operation
			,SiteCode
			,WardCode
			,WLStatus
			,ProviderCode
			,PurchaserCode
			,ContractSerialNumber
			,CancelledBy
			,BookingTypeCode
			,CasenoteNumber
			,OriginalDateOnWaitingList
			,DateOnWaitingList
			,TCIDate
			,KornerWait
			,CountOfDaysSuspended
			,SuspensionStartDate
			,SuspensionEndDate
			,SuspensionReasonCode
			,SuspensionReason
			,EpisodicGpCode
			,RegisteredGpPracticeCode
			,EpisodicGpPracticeCode
			,RegisteredGpCode
			,RegisteredPracticeCode
			,SourceTreatmentFunctionCode
			,TreatmentFunctionCode
			,NationalSpecialtyCode
			,PCTCode
			,BreachDate
			,ExpectedAdmissionDate
			,ReferralDate
			,ExpectedLOS
			,ProcedureDate
			,FutureCancellationDate
			,TheatreKey
			,TheatreInterfaceCode
			,EpisodeNo
			,BreachDays
			,MRSA
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,NationalBreachDate
			,NationalBreachDays
			,DerivedBreachDays
			,DerivedClockStartDate
			,DerivedBreachDate
			,RTTBreachDate
			,RTTDiagnosticBreachDate
			,NationalDiagnosticBreachDate
			,SocialSuspensionDays
			,AgeCode
			,Cases
			,LengthOfWait
			,DurationCode
			,WaitTypeCode
			,StatusCode
			,CategoryCode
			,AddedToWaitingListTime
			,AddedLastWeek
			,WaitingListAddMinutes
			,OPCSCoded
			,WithRTTStartDate
			,WithRTTStatusCode
			,WithExpectedAdmissionDate
			,SourceUniqueID
			,AdminCategoryCode
			,RTTActivity
			,RTTBreachStatusCode
			,DiagnosticProcedure
			,DirectorateCode
			,GuaranteedAdmissionDate
			,DurationAtTCIDateCode
			,WithTCIDate
			,WithGuaranteedAdmissionDate
			,WithRTTOpenPathway
			,RTTPathwayStartDateCurrent
			,RTTWeekBandReturnCode
			,RTTDaysWaiting
			,AdmissionReason
			,ContextCode
			,IsShortNotice
			,RTTComment1
			,RTTComment2
			,CCGCode
			,ResidenceCCGCode
			,getdate()
			,getdate()
			,suser_name()
			,TCITime
			,FutureCancellationTime
			)

	when matched
	and not
		(
			isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceEncounterNo, '') = isnull(source.SourceEncounterNo, '')
		and isnull(target.CensusDate, getdate()) = isnull(source.CensusDate, getdate())
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.PatientTitle, '') = isnull(source.PatientTitle, '')
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.MaritalStatusCode, '') = isnull(source.MaritalStatusCode, '')
		and isnull(target.ReligionCode, '') = isnull(source.ReligionCode, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.PatientsAddress1, '') = isnull(source.PatientsAddress1, '')
		and isnull(target.PatientsAddress2, '') = isnull(source.PatientsAddress2, '')
		and isnull(target.PatientsAddress3, '') = isnull(source.PatientsAddress3, '')
		and isnull(target.PatientsAddress4, '') = isnull(source.PatientsAddress4, '')
		and isnull(target.DHACode, '') = isnull(source.DHACode, '')
		and isnull(target.HomePhone, '') = isnull(source.HomePhone, '')
		and isnull(target.WorkPhone, '') = isnull(source.WorkPhone, '')
		and isnull(target.EthnicOriginCode, '') = isnull(source.EthnicOriginCode, '')
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		and isnull(target.PASSpecialtyCode, '') = isnull(source.PASSpecialtyCode, '')
		and isnull(target.EpisodeSpecialtyCode, '') = isnull(source.EpisodeSpecialtyCode, '')
		and isnull(target.ManagementIntentionCode, '') = isnull(source.ManagementIntentionCode, '')
		and isnull(target.AdmissionMethodCode, '') = isnull(source.AdmissionMethodCode, '')
		and isnull(target.PriorityCode, '') = isnull(source.PriorityCode, '')
		and isnull(target.WaitingListCode, '') = isnull(source.WaitingListCode, '')
		and isnull(target.CommentClinical, '') = isnull(source.CommentClinical, '')
		and isnull(target.CommentNonClinical, '') = isnull(source.CommentNonClinical, '')
		and isnull(target.IntendedPrimaryOperationCode, '') = isnull(source.IntendedPrimaryOperationCode, '')
		and isnull(target.Operation, '') = isnull(source.Operation, '')
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.WLStatus, '') = isnull(source.WLStatus, '')
		and isnull(target.ProviderCode, '') = isnull(source.ProviderCode, '')
		and isnull(target.PurchaserCode, '') = isnull(source.PurchaserCode, '')
		and isnull(target.ContractSerialNumber, '') = isnull(source.ContractSerialNumber, '')
		and isnull(target.CancelledBy, '') = isnull(source.CancelledBy, '')
		and isnull(target.BookingTypeCode, '') = isnull(source.BookingTypeCode, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.OriginalDateOnWaitingList, getdate()) = isnull(source.OriginalDateOnWaitingList, getdate())
		and isnull(target.DateOnWaitingList, getdate()) = isnull(source.DateOnWaitingList, getdate())
		and isnull(target.TCIDate, getdate()) = isnull(source.TCIDate, getdate())
		and isnull(target.KornerWait, 0) = isnull(source.KornerWait, 0)
		and isnull(target.CountOfDaysSuspended, '') = isnull(source.CountOfDaysSuspended, '')
		and isnull(target.SuspensionStartDate, getdate()) = isnull(source.SuspensionStartDate, getdate())
		and isnull(target.SuspensionEndDate, getdate()) = isnull(source.SuspensionEndDate, getdate())
		and isnull(target.SuspensionReasonCode, '') = isnull(source.SuspensionReasonCode, '')
		and isnull(target.SuspensionReason, '') = isnull(source.SuspensionReason, '')
		and isnull(target.EpisodicGpCode, '') = isnull(source.EpisodicGpCode, '')
		and isnull(target.RegisteredGpPracticeCode, '') = isnull(source.RegisteredGpPracticeCode, '')
		and isnull(target.EpisodicGpPracticeCode, '') = isnull(source.EpisodicGpPracticeCode, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredPracticeCode, '') = isnull(source.RegisteredPracticeCode, '')
		and isnull(target.SourceTreatmentFunctionCode, 0) = isnull(source.SourceTreatmentFunctionCode, 0)
		and isnull(target.TreatmentFunctionCode, 0) = isnull(source.TreatmentFunctionCode, 0)
		and isnull(target.NationalSpecialtyCode, '') = isnull(source.NationalSpecialtyCode, '')
		and isnull(target.PCTCode, '') = isnull(source.PCTCode, '')
		and isnull(target.BreachDate, getdate()) = isnull(source.BreachDate, getdate())
		and isnull(target.ExpectedAdmissionDate, getdate()) = isnull(source.ExpectedAdmissionDate, getdate())
		and isnull(target.ReferralDate, getdate()) = isnull(source.ReferralDate, getdate())
		and isnull(target.ExpectedLOS, '') = isnull(source.ExpectedLOS, '')
		and isnull(target.ProcedureDate, getdate()) = isnull(source.ProcedureDate, getdate())
		and isnull(target.FutureCancellationDate, getdate()) = isnull(source.FutureCancellationDate, getdate())
		and isnull(target.TheatreKey, 0) = isnull(source.TheatreKey, 0)
		and isnull(target.TheatreInterfaceCode, '') = isnull(source.TheatreInterfaceCode, '')
		and isnull(target.EpisodeNo, '') = isnull(source.EpisodeNo, '')
		and isnull(target.BreachDays, 0) = isnull(source.BreachDays, 0)
		and isnull(target.MRSA, '') = isnull(source.MRSA, '')
		and isnull(target.RTTPathwayID, '') = isnull(source.RTTPathwayID, '')
		and isnull(target.RTTPathwayCondition, '') = isnull(source.RTTPathwayCondition, '')
		and isnull(target.RTTStartDate, getdate()) = isnull(source.RTTStartDate, getdate())
		and isnull(target.RTTEndDate, getdate()) = isnull(source.RTTEndDate, getdate())
		and isnull(target.RTTSpecialtyCode, '') = isnull(source.RTTSpecialtyCode, '')
		and isnull(target.RTTCurrentProviderCode, '') = isnull(source.RTTCurrentProviderCode, '')
		and isnull(target.RTTCurrentStatusCode, '') = isnull(source.RTTCurrentStatusCode, '')
		and isnull(target.RTTCurrentStatusDate, getdate()) = isnull(source.RTTCurrentStatusDate, getdate())
		and isnull(target.RTTCurrentPrivatePatientFlag, 0) = isnull(source.RTTCurrentPrivatePatientFlag, 0)
		and isnull(target.RTTOverseasStatusFlag, 0) = isnull(source.RTTOverseasStatusFlag, 0)
		and isnull(target.NationalBreachDate, getdate()) = isnull(source.NationalBreachDate, getdate())
		and isnull(target.NationalBreachDays, 0) = isnull(source.NationalBreachDays, 0)
		and isnull(target.DerivedBreachDays, 0) = isnull(source.DerivedBreachDays, 0)
		and isnull(target.DerivedClockStartDate, getdate()) = isnull(source.DerivedClockStartDate, getdate())
		and isnull(target.DerivedBreachDate, getdate()) = isnull(source.DerivedBreachDate, getdate())
		and isnull(target.RTTBreachDate, getdate()) = isnull(source.RTTBreachDate, getdate())
		and isnull(target.RTTDiagnosticBreachDate, getdate()) = isnull(source.RTTDiagnosticBreachDate, getdate())
		and isnull(target.NationalDiagnosticBreachDate, getdate()) = isnull(source.NationalDiagnosticBreachDate, getdate())
		and isnull(target.SocialSuspensionDays, 0) = isnull(source.SocialSuspensionDays, 0)
		and isnull(target.AgeCode, '') = isnull(source.AgeCode, '')
		and isnull(target.Cases, 0) = isnull(source.Cases, 0)
		and isnull(target.LengthOfWait, 0) = isnull(source.LengthOfWait, 0)
		and isnull(target.DurationCode, '') = isnull(source.DurationCode, '')
		and isnull(target.WaitTypeCode, '') = isnull(source.WaitTypeCode, '')
		and isnull(target.StatusCode, '') = isnull(source.StatusCode, '')
		and isnull(target.CategoryCode, '') = isnull(source.CategoryCode, '')
		and isnull(target.AddedToWaitingListTime, getdate()) = isnull(source.AddedToWaitingListTime, getdate())
		and isnull(target.AddedLastWeek, 0) = isnull(source.AddedLastWeek, 0)
		and isnull(target.WaitingListAddMinutes, 0) = isnull(source.WaitingListAddMinutes, 0)
		and isnull(target.OPCSCoded, 0) = isnull(source.OPCSCoded, 0)
		and isnull(target.WithRTTStartDate, 0) = isnull(source.WithRTTStartDate, 0)
		and isnull(target.WithRTTStatusCode, 0) = isnull(source.WithRTTStatusCode, 0)
		and isnull(target.WithExpectedAdmissionDate, 0) = isnull(source.WithExpectedAdmissionDate, 0)
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.AdminCategoryCode, '') = isnull(source.AdminCategoryCode, '')
		and isnull(target.RTTActivity, 0) = isnull(source.RTTActivity, 0)
		and isnull(target.RTTBreachStatusCode, '') = isnull(source.RTTBreachStatusCode, '')
		and isnull(target.DiagnosticProcedure, 0) = isnull(source.DiagnosticProcedure, 0)
		and isnull(target.DirectorateCode, '') = isnull(source.DirectorateCode, '')
		and isnull(target.GuaranteedAdmissionDate, getdate()) = isnull(source.GuaranteedAdmissionDate, getdate())
		and isnull(target.DurationAtTCIDateCode, '') = isnull(source.DurationAtTCIDateCode, '')
		and isnull(target.WithTCIDate, 0) = isnull(source.WithTCIDate, 0)
		and isnull(target.WithGuaranteedAdmissionDate, 0) = isnull(source.WithGuaranteedAdmissionDate, 0)
		and isnull(target.WithRTTOpenPathway, 0) = isnull(source.WithRTTOpenPathway, 0)
		and isnull(target.RTTPathwayStartDateCurrent, getdate()) = isnull(source.RTTPathwayStartDateCurrent, getdate())
		and isnull(target.RTTWeekBandReturnCode, '') = isnull(source.RTTWeekBandReturnCode, '')
		and isnull(target.RTTDaysWaiting, 0) = isnull(source.RTTDaysWaiting, 0)
		and isnull(target.AdmissionReason, '') = isnull(source.AdmissionReason, '')
		and isnull(target.IsShortNotice, 0) = isnull(source.IsShortNotice, 0)
		and isnull(target.RTTComment1, '') = isnull(source.RTTComment1, '')
		and isnull(target.RTTComment2, '') = isnull(source.RTTComment2, '')
		and isnull(target.CCGCode, '') = isnull(source.CCGCode, '')
		and isnull(target.ResidenceCCGCode, '') = isnull(source.ResidenceCCGCode, '')
		and isnull(target.TCITime, getdate()) = isnull(source.TCITime, getdate())
		and isnull(target.FutureCancellationTime, getdate()) = isnull(source.FutureCancellationTime, getdate())	
		)
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.InterfaceCode = source.InterfaceCode
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.CensusDate = source.CensusDate
			,target.PatientSurname = source.PatientSurname
			,target.PatientForename = source.PatientForename
			,target.PatientTitle = source.PatientTitle
			,target.SexCode = source.SexCode
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.NHSNumber = source.NHSNumber
			,target.DistrictNo = source.DistrictNo
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.ReligionCode = source.ReligionCode
			,target.Postcode = source.Postcode
			,target.PatientsAddress1 = source.PatientsAddress1
			,target.PatientsAddress2 = source.PatientsAddress2
			,target.PatientsAddress3 = source.PatientsAddress3
			,target.PatientsAddress4 = source.PatientsAddress4
			,target.DHACode = source.DHACode
			,target.HomePhone = source.HomePhone
			,target.WorkPhone = source.WorkPhone
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.PASSpecialtyCode = source.PASSpecialtyCode
			,target.EpisodeSpecialtyCode = source.EpisodeSpecialtyCode
			,target.ManagementIntentionCode = source.ManagementIntentionCode
			,target.AdmissionMethodCode = source.AdmissionMethodCode
			,target.PriorityCode = source.PriorityCode
			,target.WaitingListCode = source.WaitingListCode
			,target.CommentClinical = source.CommentClinical
			,target.CommentNonClinical = source.CommentNonClinical
			,target.IntendedPrimaryOperationCode = source.IntendedPrimaryOperationCode
			,target.Operation = source.Operation
			,target.SiteCode = source.SiteCode
			,target.WardCode = source.WardCode
			,target.WLStatus = source.WLStatus
			,target.ProviderCode = source.ProviderCode
			,target.PurchaserCode = source.PurchaserCode
			,target.ContractSerialNumber = source.ContractSerialNumber
			,target.CancelledBy = source.CancelledBy
			,target.BookingTypeCode = source.BookingTypeCode
			,target.CasenoteNumber = source.CasenoteNumber
			,target.OriginalDateOnWaitingList = source.OriginalDateOnWaitingList
			,target.DateOnWaitingList = source.DateOnWaitingList
			,target.TCIDate = source.TCIDate
			,target.KornerWait = source.KornerWait
			,target.CountOfDaysSuspended = source.CountOfDaysSuspended
			,target.SuspensionStartDate = source.SuspensionStartDate
			,target.SuspensionEndDate = source.SuspensionEndDate
			,target.SuspensionReasonCode = source.SuspensionReasonCode
			,target.SuspensionReason = source.SuspensionReason
			,target.EpisodicGpCode = source.EpisodicGpCode
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredPracticeCode = source.RegisteredPracticeCode
			,target.SourceTreatmentFunctionCode = source.SourceTreatmentFunctionCode
			,target.TreatmentFunctionCode = source.TreatmentFunctionCode
			,target.NationalSpecialtyCode = source.NationalSpecialtyCode
			,target.PCTCode = source.PCTCode
			,target.BreachDate = source.BreachDate
			,target.ExpectedAdmissionDate = source.ExpectedAdmissionDate
			,target.ReferralDate = source.ReferralDate
			,target.ExpectedLOS = source.ExpectedLOS
			,target.ProcedureDate = source.ProcedureDate
			,target.FutureCancellationDate = source.FutureCancellationDate
			,target.TheatreKey = source.TheatreKey
			,target.TheatreInterfaceCode = source.TheatreInterfaceCode
			,target.EpisodeNo = source.EpisodeNo
			,target.BreachDays = source.BreachDays
			,target.MRSA = source.MRSA
			,target.RTTPathwayID = source.RTTPathwayID
			,target.RTTPathwayCondition = source.RTTPathwayCondition
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.RTTSpecialtyCode = source.RTTSpecialtyCode
			,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
			,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
			,target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
			,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
			,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
			,target.NationalBreachDate = source.NationalBreachDate
			,target.NationalBreachDays = source.NationalBreachDays
			,target.DerivedBreachDays = source.DerivedBreachDays
			,target.DerivedClockStartDate = source.DerivedClockStartDate
			,target.DerivedBreachDate = source.DerivedBreachDate
			,target.RTTBreachDate = source.RTTBreachDate
			,target.RTTDiagnosticBreachDate = source.RTTDiagnosticBreachDate
			,target.NationalDiagnosticBreachDate = source.NationalDiagnosticBreachDate
			,target.SocialSuspensionDays = source.SocialSuspensionDays
			,target.AgeCode = source.AgeCode
			,target.Cases = source.Cases
			,target.LengthOfWait = source.LengthOfWait
			,target.DurationCode = source.DurationCode
			,target.WaitTypeCode = source.WaitTypeCode
			,target.StatusCode = source.StatusCode
			,target.CategoryCode = source.CategoryCode
			,target.AddedToWaitingListTime = source.AddedToWaitingListTime
			,target.AddedLastWeek = source.AddedLastWeek
			,target.WaitingListAddMinutes = source.WaitingListAddMinutes
			,target.OPCSCoded = source.OPCSCoded
			,target.WithRTTStartDate = source.WithRTTStartDate
			,target.WithRTTStatusCode = source.WithRTTStatusCode
			,target.WithExpectedAdmissionDate = source.WithExpectedAdmissionDate
			,target.SourceUniqueID = source.SourceUniqueID
			,target.AdminCategoryCode = source.AdminCategoryCode
			,target.RTTActivity = source.RTTActivity
			,target.RTTBreachStatusCode = source.RTTBreachStatusCode
			,target.DiagnosticProcedure = source.DiagnosticProcedure
			,target.DirectorateCode = source.DirectorateCode
			,target.GuaranteedAdmissionDate = source.GuaranteedAdmissionDate
			,target.DurationAtTCIDateCode = source.DurationAtTCIDateCode
			,target.WithTCIDate = source.WithTCIDate
			,target.WithGuaranteedAdmissionDate = source.WithGuaranteedAdmissionDate
			,target.WithRTTOpenPathway = source.WithRTTOpenPathway
			,target.RTTPathwayStartDateCurrent = source.RTTPathwayStartDateCurrent
			,target.RTTWeekBandReturnCode = source.RTTWeekBandReturnCode
			,target.RTTDaysWaiting = source.RTTDaysWaiting
			,target.AdmissionReason = source.AdmissionReason
			,target.ContextCode = source.ContextCode
			,target.IsShortNotice = source.IsShortNotice
			,target.RTTComment1 = source.RTTComment1
			,target.RTTComment2 = source.RTTComment2
			,target.CCGCode = source.CCGCode
			,target.ResidenceCCGCode = source.ResidenceCCGCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.TCITime = source.TCITime
			,target.FutureCancellationTime = source.FutureCancellationTime

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	--,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		--,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






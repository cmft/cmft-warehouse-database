﻿CREATE procedure [ETL].[BuildTraTheatreBaseTimetableTemplate]

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Theatre.BaseTimetableTemplate target
using
	(
	select
		 TimetableTemplateCode
		,SessionNumber
		,DayNumber
		,TheatreCode
		,SessionStartTime
		,SessionEndTime
		,ConsultantCode
		,AnaesthetistCode
		,SpecialtyCode
		,LogLastUpdated
		,RecordLogTemplates
	from
		ETL.TLoadTraTheatreBaseTimetableTemplate
	) source
	on	source.TimetableTemplateCode = target.TimetableTemplateCode
	and	source.SessionNumber = target.SessionNumber
	and	source.DayNumber = target.DayNumber
	and	target.ContextCode = 'TRA||ORMIS'

	when not matched by source
	and	target.ContextCode = 'TRA||ORMIS'
	then delete

	when not matched
	then
		insert
			(
			 TimetableTemplateCode
			,SessionNumber
			,DayNumber
			,TheatreCode
			,SessionStartTime
			,SessionEndTime
			,ConsultantCode
			,AnaesthetistCode
			,SpecialtyCode
			,LogLastUpdated
			,RecordLogTemplates
			,ContextCode
			)
		values
			(
			 source.TimetableTemplateCode
			,source.SessionNumber
			,source.DayNumber
			,source.TheatreCode
			,source.SessionStartTime
			,source.SessionEndTime
			,source.ConsultantCode
			,source.AnaesthetistCode
			,source.SpecialtyCode
			,source.LogLastUpdated
			,source.RecordLogTemplates
			,'TRA||ORMIS'
			)

	when matched
	and not
		(
			target.TheatreCode = source.TheatreCode
		and target.SessionStartTime = source.SessionStartTime
		and target.SessionEndTime = source.SessionEndTime
		and target.ConsultantCode = source.ConsultantCode
		and target.AnaesthetistCode = source.AnaesthetistCode
		and target.SpecialtyCode = source.SpecialtyCode
		and target.LogLastUpdated = source.LogLastUpdated
		and target.RecordLogTemplates = source.RecordLogTemplates
		)
	then
		update
		set
			 target.TheatreCode = source.TheatreCode
			,target.SessionStartTime = source.SessionStartTime
			,target.SessionEndTime = source.SessionEndTime
			,target.ConsultantCode = source.ConsultantCode
			,target.AnaesthetistCode = source.AnaesthetistCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.LogLastUpdated = source.LogLastUpdated
			,target.RecordLogTemplates = source.RecordLogTemplates

output
	$action into @MergeSummary
;


if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

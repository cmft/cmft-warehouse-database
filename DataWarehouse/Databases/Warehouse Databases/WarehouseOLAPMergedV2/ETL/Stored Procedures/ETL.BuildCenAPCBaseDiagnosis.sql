﻿
CREATE procedure [ETL].[BuildCenAPCBaseDiagnosis]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

delete
from
	APC.BaseDiagnosis
where
	not exists
	(
	select
		1
	from
		ETL.TLoadCenAPCBaseDiagnosis
	where
		TLoadCenAPCBaseDiagnosis.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	)

and	exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	and	BaseEncounter.ContextCode = 'CEN||PAS'
	)

select
	@deleted = @@ROWCOUNT

--remove orphaned records
delete
from
	APC.BaseDiagnosis
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	)

and	not exists
	(
	select
		1
	from
		APC.BaseEncounterHistory
	where
		BaseEncounterHistory.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	)

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	APC.BaseDiagnosis target
using
	(
	select distinct --"distinct" to be removed when duplicates in [$(Warehouse)].APC.Diagnosis has been fixed...
		 MergeEncounterRecno
		,SequenceNo
		,DiagnosisCode
	from
		ETL.TLoadCenAPCBaseDiagnosis
--to be removed when duplicates in [$(Warehouse)].APC.Diagnosis has been fixed... 5 Apr 2013 PDO
	where
		not exists
		(
		select
			1
		from
			ETL.TLoadCenAPCBaseDiagnosis Previous
		where
			Previous.MergeEncounterRecno = TLoadCenAPCBaseDiagnosis.MergeEncounterRecno
		and	Previous.SequenceNo = TLoadCenAPCBaseDiagnosis.SequenceNo
		and	Previous.DiagnosisCode > TLoadCenAPCBaseDiagnosis.DiagnosisCode
		)

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno
	and	source.SequenceNo = target.SequenceNo

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,SequenceNo
			,DiagnosisCode
			,Created
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.SequenceNo
			,source.DiagnosisCode
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			target.DiagnosisCode = source.DiagnosisCode
		)
	then
		update
		set
			 target.DiagnosisCode = source.DiagnosisCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



﻿CREATE procedure [ETL].[ImportAE] as

-------------------------------------------------
--When		Who	What
--20141215	PDO	Created procedure
-------------------------------------------------

truncate table AE.ProcessList

exec ETL.ImportAEBaseEncounter
exec ETL.ImportAEBaseDiagnosis
exec ETL.ImportAEBaseTreatment
--exec ETL.ImportAEBaseInvestigation
exec ETL.ImportCenAEBaseAssault
exec ETL.ImportTraAEBaseAssault

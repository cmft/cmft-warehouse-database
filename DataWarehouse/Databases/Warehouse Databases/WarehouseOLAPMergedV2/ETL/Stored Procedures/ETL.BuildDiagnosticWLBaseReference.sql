﻿CREATE procedure ETL.BuildDiagnosticWLBaseReference as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table DiagnosticWL.BaseReference


insert
into
	DiagnosticWL.BaseReference
(
	 MergeRecno
	,ContextID
	,ExamID
	,CensusDateID
	,SpecialtyID
	,ProfessionalCarerID
	,SiteID
	,SexID
)
select
	 Base.MergeRecno
	,Diagnostic.ContextID

	,ExamID =
		coalesce(
			 Diagnostic.ExamID
			,NullExam.SourceExamID
		)

	,Diagnostic.CensusDateID

	,Diagnostic.SpecialtyID

	,ProfessionalCarerID =
		coalesce(
			 ProfessionalCarer.SourceProfessionalCarerID
			,-1
		)

	,Diagnostic.SiteID
	,Diagnostic.SexID

from
	(
	select
		 SourceMergeRecno = BaseReference.MergeRecno
		,BaseReference.ContextID
		,BaseReference.ExamID
		,BaseReference.CensusDateID
		,DataSourceID =
			(
			select
				DataSource.DataSourceID
			from
				Diagnostic.DataSource
			where
				DataSource.DataSourceCode = 'RAD'
			)

		,BaseReference.SpecialtyID
		,BaseReference.SiteID
		,BaseReference.SexID

	from
		DiagnosticWL.BaseRadiologyReference BaseReference

	--union all

	--select
	--	 SourceMergeRecno = BaseReference.MergeRecno
	--	,BaseReference.ContextID
	--	,BaseReference.ExamID
	--	,BaseReference.CensusDateID
	--	,DataSourceID =
	--		(
	--		select
	--			DataSource.DataSourceID
	--		from
	--			Diagnostic.DataSource
	--		where
	--			DataSource.DataSourceCode = 'SLEEP'
	--		)

	--	,BaseReference.SpecialtyID
	--	,BaseReference.SiteID
	--	,BaseReference.SexID

	--from
	--	DiagnosticWL.BaseSleepStudiesReference BaseReference

	union all

	select
		 SourceMergeRecno = BaseReference.MergeEncounterRecno
		,BaseReference.ContextID
		,ExamID = null
		,BaseReference.CensusDateID
		,DataSourceID =
			(
			select
				DataSource.DataSourceID
			from
				Diagnostic.DataSource
			where
				DataSource.DataSourceCode = 'APCWL'
			)

		,BaseReference.SpecialtyID
		,BaseReference.SiteID
		,BaseReference.SexID
	from
		APCWL.BaseEncounterReference BaseReference

	union all

	select
		 SourceMergeRecno = BaseReference.MergeEncounterRecno
		,BaseReference.ContextID
		,ExamID = null
		,BaseReference.CensusDateID
		,DataSourceID =
			(
			select
				DataSource.DataSourceID
			from
				Diagnostic.DataSource
			where
				DataSource.DataSourceCode = 'OPWL'
			)

		,BaseReference.SpecialtyID
		,BaseReference.SiteID
		,BaseReference.SexID
	from
		OPWL.BaseEncounterReference BaseReference

	) Diagnostic

inner join WH.Context
on	Context.ContextID = Diagnostic.ContextID

inner join DiagnosticWL.Base
on	Base.SourceMergeRecno = Diagnostic.SourceMergeRecno
and	Base.DataSourceID = Diagnostic.DataSourceID
and	Base.ContextCode = Context.ContextCode

left join Diagnostic.Exam NullExam
on	NullExam.SourceContextCode = 'N/A'
and	NullExam.SourceExamCode = '-1'

left join WH.ProfessionalCarer ProfessionalCarer
on	ProfessionalCarer.SourceProfessionalCarerCode = coalesce(Base.ProfessionalCarerCode, '-1')
and	ProfessionalCarer.SourceContextCode = Base.ContextCode

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




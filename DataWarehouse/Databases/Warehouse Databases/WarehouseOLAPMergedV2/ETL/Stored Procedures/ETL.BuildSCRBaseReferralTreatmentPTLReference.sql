﻿CREATE procedure [ETL].[BuildSCRBaseReferralTreatmentPTLReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)



--more efficient to delete here than in the merge
delete
from
	SCR.BaseReferralTreatmentPTLReference
where
	not exists
	(
	select
		1
	from
		SCR.BaseReferralTreatmentPTL
	where
		BaseReferralTreatmentPTL.MergeRecno = BaseReferralTreatmentPTLReference.MergeRecno
	)
and	BaseReferralTreatmentPTLReference.ContextID = @ContextID


select
	@deleted = @@ROWCOUNT


merge
	SCR.BaseReferralTreatmentPTLReference target
using
	(
	select
		 Encounter.MergeRecno
		,ContextID = Context.ContextID

		,CensusDateID  = CensusDate.DateID
		,SexID = Sex.SourceSexID
		,PracticeID = Practice.SourceValueID
		,PriorityTypeID = PriorityType.SourceValueID
		,CancerTypeID = CancerType.SourceValueID
		,CancerStatusID = CancerStatus.SourceValueID
		,SourceForOutpatientID = SourceForOutpatient.SourceSourceOfReferralID
		,InitialTreatmentID = Treatment.SourceValueID
		,TreatmentSettingID = TreatmentSetting.SourceValueID
		,ConsultantID = Consultant.SourceConsultantID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,PrimaryDiagnosisID = Diagnosis.SourceValueID
		,FirstAppointmentConsultantID = FirstAppointmentConsultant.SourceConsultantID
		,FirstAppointmentOrganisationID = FirstAppointmentOrganisation.SourceValueID
		,ReferringOrganisationID = ReferringOrganisation.SourceValueID
		,TreatmentOrganisationID = TreatmentOrganisation.SourceValueID
		,AtDiagnosisAgeID = Age.AgeID

		,Encounter.Created
		,Encounter.ByWhom
	from
		SCR.BaseReferralTreatmentPTL Encounter

	left join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	left join WH.Calendar CensusDate
	on	CensusDate.TheDate = Encounter.CensusDate

	left join SCR.Practice
	on	Practice.PracticeCode = coalesce(Encounter.PracticeCode, '-1')
	and	Practice.ContextCode = Encounter.ContextCode

	left join SCR.PriorityType
	on	PriorityType.PriorityTypeCode = coalesce(Encounter.PriorityTypeCode, '-1')
	and	PriorityType.ContextCode = Encounter.ContextCode

	left join SCR.CancerType
	on	CancerType.CancerTypeCode = coalesce(Encounter.CancerTypeCode, '-1')
	and	CancerType.ContextCode = Encounter.ContextCode

	left join SCR.CancerStatus
	on	CancerStatus.CancerStatusCode = coalesce(Encounter.CancerStatusCode, '-1')
	and	CancerStatus.ContextCode = Encounter.ContextCode

	left join OP.SourceOfReferral SourceForOutpatient
	on	SourceForOutpatient.SourceSourceOfReferralCode = coalesce(Encounter.SourceForOutpatientCode, '-1')
	and	SourceForOutpatient.SourceContextCode = Encounter.ContextCode

	left join SCR.Treatment
	on	Treatment.TreatmentCode = coalesce(Encounter.InitialTreatmentCode, '-1')
	and	Treatment.ContextCode = Encounter.ContextCode

	left join SCR.TreatmentSetting
	on	TreatmentSetting.TreatmentSettingCode = coalesce(Encounter.TreatmentSettingCode, '-1')
	and	TreatmentSetting.ContextCode = Encounter.ContextCode

	left join SCR.Organisation ReferringOrganisation
	on	ReferringOrganisation.OrganisationCode = coalesce(Encounter.ReferringOrganisationCode, '-1')
	and	ReferringOrganisation.ContextCode = Encounter.ContextCode

	left join SCR.Organisation TreatmentOrganisation
	on	TreatmentOrganisation.OrganisationCode = coalesce(Encounter.TreatmentOrganisationCode, '-1')
	and	TreatmentOrganisation.ContextCode = Encounter.ContextCode

	left join SCR.Organisation FirstAppointmentOrganisation
	on	FirstAppointmentOrganisation.OrganisationCode = coalesce(Encounter.FirstAppointmentOrganisationCode, '-1')
	and	FirstAppointmentOrganisation.ContextCode = Encounter.ContextCode

	left join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = cast(coalesce(Encounter.ConsultantCode, '-1') as varchar)
	and	Consultant.SourceContextCode = Encounter.ContextCode

	left join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = cast(coalesce(Encounter.SpecialtyCode, '-1') as varchar)
	and	Specialty.SourceContextCode = Encounter.ContextCode

	left join SCR.Diagnosis
	on	Diagnosis.DiagnosisCode = coalesce(Encounter.PrimaryDiagnosisCode, '-1')
	and	Diagnosis.ContextCode = Encounter.ContextCode

	left join WH.Consultant FirstAppointmentConsultant
	on	FirstAppointmentConsultant.SourceConsultantCode = cast(coalesce(Encounter.FirstAppointmentConsultantCode, '-1') as varchar)
	and	FirstAppointmentConsultant.SourceContextCode = Encounter.ContextCode

	left join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.GenderCode, '-1') as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	left join WH.Age
	on	Age.AgeCode =
		case
		when Encounter.AgeAtDiagnosis is null
		then 'Age Unknown'
		when Encounter.AgeAtDiagnosis > 99
		then '99+'
		else
			right(
					'00' +
					cast(
						Encounter.AgeAtDiagnosis
						as varchar
					)
					,2
			) + ' Year' +
			case
			when Encounter.AgeAtDiagnosis = 1
			then ''
			else 's'
			end

		end

	where
		Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			 MergeRecno
			,ContextID
			,CensusDateID
			,AtDiagnosisAgeID
			,SexID
			,PracticeID
			,PriorityTypeID
			,CancerTypeID
			,CancerStatusID
			,SourceForOutpatientID
			,InitialTreatmentID
			,TreatmentSettingID
			,ConsultantID
			,SpecialtyID
			,PrimaryDiagnosisID
			,FirstAppointmentConsultantID
			,FirstAppointmentOrganisationID
			,ReferringOrganisationID
			,TreatmentOrganisationID
			,Created
			,ByWhom
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.CensusDateID
			,source.AtDiagnosisAgeID
			,source.SexID
			,source.PracticeID
			,source.PriorityTypeID
			,source.CancerTypeID
			,source.CancerStatusID
			,source.SourceForOutpatientID
			,source.InitialTreatmentID
			,source.TreatmentSettingID
			,source.ConsultantID
			,source.SpecialtyID
			,source.PrimaryDiagnosisID
			,source.FirstAppointmentConsultantID
			,source.FirstAppointmentOrganisationID
			,source.ReferringOrganisationID
			,source.TreatmentOrganisationID
			,source.Created
			,source.ByWhom
			)

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





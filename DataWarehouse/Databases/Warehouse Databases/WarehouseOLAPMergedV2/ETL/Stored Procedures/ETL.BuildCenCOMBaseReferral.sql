﻿CREATE procedure [ETL].[BuildCenCOMBaseReferral]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	COM.BaseReferral target
using
	(
	select
		 SourceUniqueID = cast(SourceUniqueID as numeric)
		,SourcePatientID = cast(SourcePatientID as numeric)
		,ReferredBySpecialtyID = cast(ReferredBySpecialtyID as numeric)
		,ReferredToSpecialtyID = cast(ReferredToSpecialtyID as numeric)
		,SourceofReferralID = cast(SourceofReferralID as numeric)
		,ReferralReceivedTime = cast(ReferralReceivedTime as datetime)
		,ReferralReceivedDate = cast(ReferralReceivedDate as date)
		,ReasonforReferralID = cast(ReasonforReferralID as numeric)
		,CancelledTime = cast(CancelledTime as datetime)
		,CancelledDate = cast(CancelledDate as date)
		,CancelledReasonID = cast(CancelledReasonID as numeric)
		,ReferralSentTime = cast(ReferralSentTime as datetime)
		,ReferralSentDate = cast(ReferralSentDate as date)
		,Urgency = cast(Urgency as numeric)
		,Priority = cast(Priority as numeric)
		,AuthorisedTime = cast(AuthorisedTime as datetime)
		,AuthorisedDate = cast(AuthorisedDate as date)
		,ReferralClosedTime = cast(ReferralClosedTime as datetime)
		,ReferralClosedDate = cast(ReferralClosedDate as date)
		,ClosureReasonID = cast(ClosureReasonID as numeric)
		,ReferredByProfessionalCarerID = cast(ReferredByProfessionalCarerID as numeric)
		,ReferredByStaffTeamID = cast(ReferredByStaffTeamID as numeric)
		,ReferredByHealthOrgID = cast(ReferredByHealthOrgID as numeric)
		,ReferredToProfessionalCarerID = cast(ReferredToProfessionalCarerID as numeric)
		,ReferredToHealthOrgID = cast(ReferredToHealthOrgID as numeric)
		,ReferredToStaffTeamID = cast(ReferredToStaffTeamID as numeric)
		,OutcomeOfReferral = cast(OutcomeOfReferral as numeric)
		,ReferralStatus = cast(ReferralStatus as numeric)
		,RejectionID = cast(RejectionID as numeric)
		,TypeofReferralID = cast(TypeofReferralID as numeric)
		,PatientSourceID = cast(PatientSourceID as numeric)
		,PatientSourceSystemUniqueID = cast(PatientSourceSystemUniqueID as varchar)
		,PatientNHSNumber = cast(PatientNHSNumber as varchar)
		,PatientNHSNumberStatusIndicator = cast(PatientNHSNumberStatusIndicator as char)
		,PatientTitleID = cast(PatientTitleID as numeric)
		,PatientForename = cast(PatientForename as varchar)
		,PatientSurname = cast(PatientSurname as varchar)
		,PatientAddress1 = cast(PatientAddress1 as varchar)
		,PatientAddress2 = cast(PatientAddress2 as varchar)
		,PatientAddress3 = cast(PatientAddress3 as varchar)
		,PatientAddress4 = cast(PatientAddress4 as varchar)
		,PatientPostcode = cast(PatientPostcode as varchar)
		,PatientDateOfBirth = cast(PatientDateOfBirth as datetime)
		,PatientDateOfDeath = cast(PatientDateOfDeath as datetime)
		,PatientSexID = cast(PatientSexID as numeric)
		,PatientEthnicGroupID = cast(PatientEthnicGroupID as numeric)
		,RTTStatusDate = cast(RTTStatusDate as datetime)
		,RTTPatientPathwayID = cast(RTTPatientPathwayID as varchar)
		,RTTStartTime = cast(RTTStartTime as datetime)
		,RTTStartFlag = cast(RTTStartFlag as varchar)
		,RTTStatusID = cast(RTTStatusID as numeric)
		,CreatedTime = cast(CreatedTime as datetime)
		,ModifiedTime = cast(ModifiedTime as datetime)
		,CreatedByID = cast(CreatedByID as varchar)
		,ModifiedByID = cast(ModifiedByID as varchar)
		,ArchiveFlag = cast(ArchiveFlag as varchar)
		,ParentID = cast(ParentID as numeric)
		,HealthOrgOwner = cast(HealthOrgOwner as numeric)
		,RecordStatus = cast(RecordStatus as char)
		,RequestedServiceID = cast(RequestedServiceID as int)
		,AgeCode = cast(AgeCode as varchar)
		,ContextCode = cast(ContextCode as varchar)
		,Created = cast(Created as datetime)
		,Updated = cast(Updated as datetime)
		,ByWhom = cast(ByWhom as varchar)
	from
		ETL.TLoadCenCOMBaseReferral Referral
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.ContextCode = 'CEN||IPM'
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,SourcePatientID
			,ReferredBySpecialtyID
			,ReferredToSpecialtyID
			,SourceofReferralID
			,ReferralReceivedTime
			,ReferralReceivedDate
			,ReasonForReferralID
			,CancelledTime
			,CancelledDate
			,CancelledReasonID
			,ReferralSentTime
			,ReferralSentDate
			,Urgency
			,Priority
			,AuthorisedTime
			,AuthorisedDate
			,ReferralClosedTime
			,ReferralClosedDate
			,ClosureReasonID
			,ReferredByProfessionalCarerID
			,ReferredByStaffTeamID
			,ReferredByHealthOrgID
			,ReferredToProfessionalCarerID
			,ReferredToHealthOrgID
			,ReferredToStaffTeamID
			,OutcomeOfReferral
			,ReferralStatus
			,RejectionID
			,TypeofReferralID
			,PatientSourceID
			,PatientSourceSystemUniqueID
			,PatientNHSNumber
			,PatientNHSNumberStatusIndicator
			,PatientTitleID
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,PatientPostcode
			,PatientDateOfBirth
			,PatientDateOfDeath
			,PatientSexID
			,PatientEthnicGroupID
			,RTTStatusDate
			,RTTPatientPathwayID
			,RTTStartTime
			,RTTStartFlag
			,RTTStatusID
			,CreatedTime
			,ModifiedTime
			,CreatedByID
			,ModifiedByID
			,ArchiveFlag
			,ParentID
			,HealthOrgOwner
			,RecordStatus
			,RequestedServiceID
			,ContextCode
			,AgeCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 SourceUniqueID
			,SourcePatientID
			,ReferredBySpecialtyID
			,ReferredToSpecialtyID
			,SourceofReferralID
			,ReferralReceivedTime
			,ReferralReceivedDate
			,ReasonforReferralID
			,CancelledTime
			,CancelledDate
			,CancelledReasonID
			,ReferralSentTime
			,ReferralSentDate
			,Urgency
			,Priority
			,AuthorisedTime
			,AuthorisedDate
			,ReferralClosedTime
			,ReferralClosedDate
			,ClosureReasonID
			,ReferredByProfessionalCarerID
			,ReferredByStaffTeamID
			,ReferredByHealthOrgID
			,ReferredToProfessionalCarerID
			,ReferredToHealthOrgID
			,ReferredToStaffTeamID
			,OutcomeOfReferral
			,ReferralStatus
			,RejectionID
			,TypeofReferralID
			,PatientSourceID
			,PatientSourceSystemUniqueID
			,PatientNHSNumber
			,PatientNHSNumberStatusIndicator
			,PatientTitleID
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,PatientPostcode
			,PatientDateOfBirth
			,PatientDateOfDeath
			,PatientSexID
			,PatientEthnicGroupID
			,RTTStatusDate
			,RTTPatientPathwayID
			,RTTStartTime
			,RTTStartFlag
			,RTTStatusID
			,CreatedTime
			,ModifiedTime
			,CreatedByID
			,ModifiedByID
			,ArchiveFlag
			,ParentID
			,HealthOrgOwner
			,RecordStatus
			,RequestedServiceID
			,ContextCode
			,AgeCode
			,Created
			,Updated
			,ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.SourcePatientID = source.SourcePatientID
			,target.ReferredBySpecialtyID = source.ReferredBySpecialtyID
			,target.ReferredToSpecialtyID = source.ReferredToSpecialtyID
			,target.SourceofReferralID = source.SourceofReferralID
			,target.ReferralReceivedTime = source.ReferralReceivedTime
			,target.ReferralReceivedDate = source.ReferralReceivedDate
			,target.ReasonForReferralID = source.ReasonforReferralID
			,target.CancelledTime = source.CancelledTime
			,target.CancelledDate = source.CancelledDate
			,target.CancelledReasonID = source.CancelledReasonID
			,target.ReferralSentTime = source.ReferralSentTime
			,target.ReferralSentDate = source.ReferralSentDate
			,target.Urgency = source.Urgency
			,target.Priority = source.Priority
			,target.AuthorisedTime = source.AuthorisedTime
			,target.AuthorisedDate = source.AuthorisedDate
			,target.ReferralClosedTime = source.ReferralClosedTime
			,target.ReferralClosedDate = source.ReferralClosedDate
			,target.ClosureReasonID = source.ClosureReasonID
			,target.ReferredByProfessionalCarerID = source.ReferredByProfessionalCarerID
			,target.ReferredByStaffTeamID = source.ReferredByStaffTeamID
			,target.ReferredByHealthOrgID = source.ReferredByHealthOrgID
			,target.ReferredToProfessionalCarerID = source.ReferredToProfessionalCarerID
			,target.ReferredToHealthOrgID = source.ReferredToHealthOrgID
			,target.ReferredToStaffTeamID = source.ReferredToStaffTeamID
			,target.OutcomeOfReferral = source.OutcomeOfReferral
			,target.ReferralStatus = source.ReferralStatus
			,target.RejectionID = source.RejectionID
			,target.TypeofReferralID = source.TypeofReferralID
			,target.PatientSourceID = source.PatientSourceID
			,target.PatientSourceSystemUniqueID = source.PatientSourceSystemUniqueID
			,target.PatientNHSNumber = source.PatientNHSNumber
			,target.PatientNHSNumberStatusIndicator = source.PatientNHSNumberStatusIndicator
			,target.PatientTitleID = source.PatientTitleID
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.PatientPostcode = source.PatientPostcode
			,target.PatientDateOfBirth = source.PatientDateOfBirth
			,target.PatientDateOfDeath = source.PatientDateOfDeath
			,target.PatientSexID = source.PatientSexID
			,target.PatientEthnicGroupID = source.PatientEthnicGroupID
			,target.RTTStatusDate = source.RTTStatusDate
			,target.RTTPatientPathwayID = source.RTTPatientPathwayID
			,target.RTTStartTime = source.RTTStartTime
			,target.RTTStartFlag = source.RTTStartFlag
			,target.RTTStatusID = source.RTTStatusID
			,target.CreatedTime = source.CreatedTime
			,target.ModifiedTime = source.ModifiedTime
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.ArchiveFlag = source.ArchiveFlag
			,target.ParentID = source.ParentID
			,target.HealthOrgOwner = source.HealthOrgOwner
			,target.RecordStatus = source.RecordStatus
			,target.RequestedServiceID = source.RequestedServiceID
			,target.ContextCode = source.ContextCode
			,target.AgeCode = source.AgeCode
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

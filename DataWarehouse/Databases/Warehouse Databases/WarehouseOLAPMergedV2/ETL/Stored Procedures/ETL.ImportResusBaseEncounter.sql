﻿CREATE procedure [ETL].[ImportResusBaseEncounter] as


--import the data
exec ETL.BuildResusBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Resus].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildResusBaseEncounterReference 'CMFT||CARD'


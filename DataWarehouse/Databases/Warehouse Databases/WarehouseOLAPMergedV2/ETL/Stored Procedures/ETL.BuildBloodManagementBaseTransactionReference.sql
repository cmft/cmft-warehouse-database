﻿

--Update BloodManagement.BaseTransaction
--set Updated = getdate()
      
      

CREATE procedure [ETL].[BuildBloodManagementBaseTransactionReference] --'CMFT||BLDTRK'
      @ContextCode varchar(15)
as

declare
      @StartTime datetime = getdate()
      ,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
      ,@Elapsed int
      ,@Stats varchar(max)

declare
      @deleted int
      ,@inserted int
      ,@updated int

declare
      @MergeSummary TABLE(Action nvarchar(10));

declare
      @ContextID int =
            (
            select
                  Context.ContextID
            from
                  WH.Context
            where
                  Context.ContextCode = @ContextCode
            )


declare
      @LastUpdated datetime =
            coalesce(
                  (
                  select
                        MAX(Updated)
                  from
                        BloodManagement.BaseTransactionReference
                  where
                        BaseTransactionReference.ContextID = @ContextID
                  )
                  ,'1 jan 1900'
            )


--more efficient to delete here than in the merge
delete
from
      BloodManagement.BaseTransactionReference
where
     not exists
      (
      select
            1
      from
            BloodManagement.BaseTransaction
      where
            BaseTransaction.MergeRecno = BaseTransactionReference.MergeRecno
      )
and   BaseTransactionReference.ContextID = @ContextID 

select
      @deleted = @@ROWCOUNT

merge
      BloodManagement.BaseTransactionReference target
using
      (
      select 
            BaseTransaction.MergeRecno
            ,ContextID = Context.ContextID
            ,SiteID = [Site].SourceSiteID
            ,LocationID = Location.SourceLocationID
            ,SystemUserID = [SystemUser].SystemUserID 
            ,TransactionTypeID = TransactionType.SourceTransactionTypeID
            ,ConditionID = SourceTransactionConditionID
            ,FateID = SourceBloodUnitFateID
            ,BloodGroupID = SourceBloodGroupID
            ,BloodProductID = SourceBloodProductID

            ,BaseTransaction.Created
            ,BaseTransaction.Updated
            ,BaseTransaction.ByWhom
      from
            BloodManagement.BaseTransaction

      inner join WH.Context
      on    Context.ContextCode = BaseTransaction.ContextCode

      left join WH.Location
      on    Location.SourceLocationCode = cast(coalesce(BaseTransaction.LocationID, '-1') as varchar)
      and   Location.SourceContextCode = BaseTransaction.ContextCode
      
      left join WH.[Site]
      on    [Site].SourceSiteCode = cast(coalesce(BaseTransaction.SiteID, '-1') as varchar)
      and   [Site].SourceContextCode = BaseTransaction.ContextCode

      left join WH.[SystemUser]
      on    [SystemUser].UserID = cast(coalesce(BaseTransaction.SystemUserID, '-1') as varchar)
      and   [SystemUser].ContextCode = BaseTransaction.ContextCode
      
      left join BloodManagement.TransactionType
      on    TransactionType.SourceTransactionTypeCode = coalesce(BaseTransaction.TransactionTypeID, '-1')
      and   TransactionType.SourceContextCode = BaseTransaction.ContextCode
      
      left join BloodManagement.TransactionCondition
      on    TransactionCondition.SourceTransactionConditionCode = coalesce(BaseTransaction.ConditionID, '-1')
      and   TransactionCondition.SourceContextCode = BaseTransaction.ContextCode
      
      left join BloodManagement.UnitGroup
      on    UnitGroup.SourceBloodGroupCode = coalesce(BaseTransaction.BloodGroupID, '-1')
      and   UnitGroup.SourceContextCode = BaseTransaction.ContextCode
      
      left join BloodManagement.UnitFate
      on    UnitFate.SourceBloodUnitFateCode = coalesce(BaseTransaction.FateID, '-1')
      and   UnitFate.SourceContextCode = BaseTransaction.ContextCode

      left join BloodManagement.Product
      on    Product.SourceBloodProductCode = coalesce(BaseTransaction.BloodProductID, '-1')
      and   Product.SourceContextCode = BaseTransaction.ContextCode

      where
            BaseTransaction.Updated > @LastUpdated
      and   BaseTransaction.ContextCode = @ContextCode

      ) source
      on    source.MergeRecno = target.MergeRecno

      when not matched
      then
            insert
                  (
                  MergeRecno
                  ,ContextID 
                  ,SiteID 
                  ,LocationID 
                  ,SystemUserID 
                  ,TransactionTypeID
                  ,ConditionID
                  ,FateID
                  ,BloodGroupID
                  ,BloodProductID
                  
                  ,Created
                  ,Updated
                  ,ByWhom
                  )
            values
                  (
                  source.MergeRecno
                  ,source.ContextID 
                  ,source.SiteID 
                  ,source.LocationID 
                  ,source.SystemUserID 
                  ,source.TransactionTypeID
                  ,source.ConditionID
                  ,source.FateID
                  ,source.BloodGroupID
                  ,source.BloodProductID
                  
                  ,source.Created
                  ,source.Updated
                  ,source.ByWhom
                  )

      when matched
      and not
            (
                  target.Updated = source.Updated
            )
      then
            update
            set
                  target.MergeRecno = source.MergeRecno
                  ,target.ContextID = source.ContextID
                  ,target.SiteID = source.SiteID
                  ,target.LocationID = source.LocationID
                  ,target.SystemUserID = source.SystemUserID
                  ,target.TransactionTypeID = source.TransactionTypeID
                  ,target.ConditionID = source.ConditionID
                  ,target.FateID = source.FateID
                  ,target.BloodGroupID = source.BloodGroupID
                  ,target.BloodProductID = source.BloodProductID
                        
                  ,target.Updated = source.Updated
                  ,target.ByWhom = source.ByWhom
                  
            
output
      $action into @MergeSummary
;


select
      @inserted = sum(Inserted)
      ,@updated = sum(Updated)
from
      (
      select
            Inserted = case when Action = 'INSERT' then 1 else 0 end
            ,Updated = case when Action = 'UPDATE' then 1 else 0 end
      from
            @MergeSummary
      ) MergeSummary

select
      @Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
      @Stats =
            'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
            ', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
            ', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
            ', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
      @ProcedureName
      ,@Stats









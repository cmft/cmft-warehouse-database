﻿CREATE procedure [ETL].[BuildDiagnosticBaseRadiology]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 ExamEncounterRecno
	,SourceUniqueID = cast(SourceUniqueID as varchar(50))
	,AccessionNo = cast(AccessionNo as varchar(50))
	,ExamCode = cast(ExamCode as varchar(50))
	,InterventionalFlag = cast(InterventionalFlag as varchar(50))
	,AllocatedTimeMins = cast(AllocatedTimeMins as int)
	,ExamDate = cast(ExamDate as datetime)
	,ExamStartTime = cast(ExamStartTime as datetime)
	,ExamEndTime = cast(ExamEndTime as datetime)
	,RoomCode = cast(RoomCode as varchar(50))
	,Radiographer1Code = cast(Radiographer1Code as varchar(50))
	,Radiographer1CompetenceLevel = cast(Radiographer1CompetenceLevel as varchar(50))
	,Radiographer1Difficulty = cast(Radiographer1Difficulty as varchar(50))
	,Radiographer2Code = cast(Radiographer2Code as varchar(50))
	,Radiographer2CompetenceLevel = cast(Radiographer2CompetenceLevel as varchar(50))
	,Radiographer2Difficulty = cast(Radiographer2Difficulty as varchar(50))
	,Radiographer3Code = cast(Radiographer3Code as varchar(50))
	,Radiographer3CompetenceLevel = cast(Radiographer3CompetenceLevel as varchar(50))
	,Radiographer3Difficulty = cast(Radiographer3Difficulty as varchar(50))
	,RadiologistCode = cast(RadiologistCode as varchar(50))
	,ContrastBatchNo = cast(ContrastBatchNo as varchar(50))
	,BoneDensitometryValue = cast(BoneDensitometryValue as varchar(50))
	,Concentration = cast(Concentration as varchar(50))
	,ContractCode = cast(ContractCode as varchar(50))
	,ContrastCode = cast(ContrastCode as varchar(50))
	,ImageStoreDisk = cast(ImageStoreDisk as varchar(50))
	,ExamQuality = cast(ExamQuality as varchar(50))
	,FlexibleForm = cast(FlexibleForm as varchar(50))
	,IgnoreAppt = cast(IgnoreAppt as varchar(50))
	,InjectedBy = cast(InjectedBy as varchar(50))
	,MuseumCode = cast(MuseumCode as varchar(50))
	,Reaction = cast(Reaction as varchar(50))
	,HasRedDot = cast(HasRedDot as varchar(50))
	,RestrainDose = cast(RestrainDose as varchar(50))
	,RestrainName = cast(RestrainName as varchar(50))
	,RestrainType = cast(RestrainType as varchar(50))
	,ScanSlices = cast(ScanSlices as varchar(50))
	,ExamScreeningTime = cast(ExamScreeningTime as varchar(50))
	,ExamStatus = cast(ExamStatus as varchar(50))
	,ExamBookedDate = cast(ExamBookedDate as datetime)
	,ExamBookedTime = cast(ExamBookedTime as datetime)
	,ApptBookMode = cast(ApptBookMode as varchar(50))
	,ApptCommitFlag = cast(ApptCommitFlag as varchar(50))
	,ApptPreviousDate = cast(ApptPreviousDate as datetime)
	,ApptPreviousTime = cast(ApptPreviousTime as datetime)
	,ApptLetterPrintedDate = cast(ApptLetterPrintedDate as varchar(50))
	,SiteCodeAtTimeOfBooking = cast(SiteCodeAtTimeOfBooking as varchar(50))
	,OrderCommsUniqueID = cast(OrderCommsUniqueID as varchar(50))
	,DaysWaiting = cast(DaysWaiting as int)
	,ExamLogicalDeleteFlag = cast(ExamLogicalDeleteFlag as varchar(50))
	,ExamCreatedDate = cast(ExamCreatedDate as datetime)
	,ExamCreatedTime = cast(ExamCreatedTime as datetime)
	,ExamModifiedDate = cast(ExamModifiedDate as datetime)
	,ExamModifiedTime = cast(ExamModifiedTime as datetime)
	,ReportCheckedBy = cast(ReportCheckedBy as varchar(50))
	,DateReported = cast(DateReported as datetime)
	,DateTyped = cast(DateTyped as datetime)
	,DefaultLength = cast(DefaultLength as int)
	,IDChecked = cast(IDChecked as varchar(50))
	,IDCheckedBy = cast(IDCheckedBy as varchar(50))
	,IsTyped = cast(IsTyped as varchar(50))
	,IsVerified = cast(IsVerified as varchar(50))
	,MPPSID = cast(MPPSID as varchar(50))
	,MPPSStatus = cast(MPPSStatus as varchar(50))
	,OrderID = cast(OrderID as varchar(50))
	,PregnancyChecked = cast(PregnancyChecked as varchar(50))
	,PregnancyCheckedBy = cast(PregnancyCheckedBy as varchar(50))
	,QuantityUsed = cast(QuantityUsed as float)
	,StudyID = cast(StudyID as varchar(50))
	,TimeFirstVerified = cast(TimeFirstVerified as datetime)
	,TimeLastVerified = cast(TimeLastVerified as datetime)
	,WaitBreachDate = cast(WaitBreachDate as datetime)
	,WaitNPland = cast(WaitNPland as varchar(50))
	,WasPlanned = cast(WasPlanned as varchar(50))
	,WasScheduled = cast(WasScheduled as varchar(50))
	,WasWaiting = cast(WasWaiting as varchar(50))
	,WeeksWaiting = cast(WeeksWaiting as int)
	,RADExamCreated = cast(RADExamCreated as smalldatetime)
	,RADExamUpdated = cast(RADExamUpdated as smalldatetime)
	,EventEncounterRecno = cast(EventEncounterRecno as int)
	,EventSourceUniqueID = cast(EventSourceUniqueID as varchar(50))
	,RTTPathwayID = cast(RTTPathwayID as varchar(50))
	,SourcePatientNo = cast(SourcePatientNo as varchar(50))
	,OriginalSourcePatientNo = cast(OriginalSourcePatientNo as varchar(50))
	,Abnormal = cast(Abnormal as varchar(50))
	,AbnormalActioned = cast(AbnormalActioned as varchar(50))
	,AENumber = cast(AENumber as varchar(50))
	,AntiD = cast(AntiD as varchar(50))
	,AttendanceNo = cast(AttendanceNo as varchar(50))
	,AttendedFlag = cast(AttendedFlag as varchar(50))
	,AwaitingAcknowledgement = cast(AwaitingAcknowledgement as varchar(50))
	,BillReferrerFlag = cast(BillReferrerFlag as varchar(50))
	,BookingModeCode = cast(BookingModeCode as varchar(50))
	,BookedByCode = cast(BookedByCode as varchar(50))
	,RequestDate = cast(RequestDate as datetime)
	,BookedDate = cast(BookedDate as datetime)
	,EventDate = cast(EventDate as datetime)
	,EventTime = cast(EventTime as datetime)
	,DateOnWL = cast(DateOnWL as datetime)
	,DayOfWeek = cast(DayOfWeek as varchar(50))
	,EventLogicalDeleteFlag = cast(EventLogicalDeleteFlag as varchar(50))
	,HasPathway = cast(HasPathway as varchar(50))
	,IntendedRadiologistCode = cast(IntendedRadiologistCode as varchar(50))
	,IsDictated = cast(IsDictated as varchar(50))
	,IsProcessed = cast(IsProcessed as varchar(50))
	,JustifiedBy = cast(JustifiedBy as varchar(50))
	,NumberOfProcs = cast(NumberOfProcs as int)
	,NumberOfScannedDocs = cast(NumberOfScannedDocs as int)
	,PatientTypeCode = cast(PatientTypeCode as varchar(50))
	,KeyDate = cast(KeyDate as datetime)
	,NumberOfCancellations = cast(NumberOfCancellations as int)
	,PacsVolumeKey = cast(PacsVolumeKey as varchar(50))
	,ImageNo = cast(ImageNo as varchar(50))
	,LeadClinician = cast(LeadClinician as varchar(50))
	,LmpDate = cast(LmpDate as datetime)
	,MobilityCode = cast(MobilityCode as varchar(50))
	,DuringOnCallHoursFlag = cast(DuringOnCallHoursFlag as varchar(50))
	,PatientHeight = cast(PatientHeight as varchar(50))
	,PatientWeight = cast(PatientWeight as varchar(50))
	,Practitioner = cast(Practitioner as varchar(50))
	,PregnancyEventKey = cast(PregnancyEventKey as varchar(50))
	,PregnancyFlag = cast(PregnancyFlag as varchar(50))
	,Questions = cast(Questions as varchar(4000))
	,Reason = cast(Reason as varchar(4000))
	,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(50))
	,ReferrerCode = cast(ReferrerCode as varchar(50))
	,ReferralLocationCode = cast(ReferralLocationCode as varchar(50))
	,ReferringPCTCode = cast(ReferringPCTCode as varchar(50))
	,RequestCategoryCode = cast(RequestCategoryCode as varchar(50))
	,RequiredClinician = cast(RequiredClinician as varchar(50))
	,VettingStatus = cast(VettingStatus as varchar(50))
	,XDSFolderID = cast(XDSFolderID as varchar(50))
	,XDSFolderUID = cast(XDSFolderUID as varchar(50))
	,AgeAtEvent = cast(AgeAtEvent as int)
	,ClinicalHistory = cast(ClinicalHistory as varchar(4000))
	,Comment = cast(Comment as varchar(4000))
	,ConsentComment = cast(ConsentComment as varchar(4000))
	,PatientCreationDate = cast(PatientCreationDate as datetime)
	,PatientCreationTime = cast(PatientCreationTime as datetime)
	,Deleted = cast(Deleted as varchar(50))
	,EmailConsent = cast(EmailConsent as varchar(50))
	,EpisodicPCTCode = cast(EpisodicPCTCode as varchar(50))
	,EpisodicPCT = cast(EpisodicPCT as varchar(200))
	,SiteCode = cast(SiteCode as varchar(50))
	,AttendancesAtSite = cast(AttendancesAtSite as varchar(50))
	,SpecialtyCode = cast(SpecialtyCode as varchar(50))
	,UnreportedFlag = cast(UnreportedFlag as varchar(50))
	,PriorityCode = cast(PriorityCode as varchar(50))
	,DictatedDate = cast(DictatedDate as datetime)
	,DictatedTime = cast(DictatedTime as datetime)
	,DictatedByCode = cast(DictatedByCode as varchar(50))
	,DictatedBy = cast(DictatedBy as varchar(50))
	,DictatedBy2Code = cast(DictatedBy2Code as varchar(50))
	,DictatedBy2 = cast(DictatedBy2 as varchar(50))
	,ConsentFlag = cast(ConsentFlag as varchar(50))
	,ContrastReactionFlag = cast(ContrastReactionFlag as varchar(50))
	,EpisodicReferrerCode = cast(EpisodicReferrerCode as varchar(50))
	,EpisodicLocationCode = cast(EpisodicLocationCode as varchar(50))
	,Surname = cast(Surname as varchar(50))
	,Forenames = cast(Forenames as varchar(50))
	,Title = cast(Title as varchar(50))
	,DateOfBirth = cast(DateOfBirth as datetime)
	,DateOfDeath = cast(DateOfDeath as datetime)
	,DistrictNo = cast(DistrictNo as varchar(50))
	,Language = cast(Language as varchar(50))
	,PatientModifiedDate = cast(PatientModifiedDate as datetime)
	,PatientModifiedTime = cast(PatientModifiedTime as datetime)
	,NHSNumber = cast(NHSNumber as varchar(50))
	,NHSNumberStatus = cast(NHSNumberStatus as varchar(50))
	,NHSTraceStatus = cast(NHSTraceStatus as varchar(50))
	,RegisteredGpCode = cast(RegisteredGpCode as varchar(50))
	,RegisteredPracticeCode = cast(RegisteredPracticeCode as varchar(50))
	,TelMobileConsent = cast(TelMobileConsent as varchar(50))
	,EthnicOriginCode = cast(EthnicOriginCode as varchar(50))
	,MRSAIndicator = cast(MRSAIndicator as varchar(50))
	,SexCode = cast(SexCode as varchar(50))
	,Address1 = cast(Address1 as varchar(50))
	,Address2 = cast(Address2 as varchar(50))
	,Address3 = cast(Address3 as varchar(50))
	,Address4 = cast(Address4 as varchar(50))
	,Postcode = cast(Postcode as varchar(50))
	,PostcodePart1 = cast(PostcodePart1 as varchar(50))
	,PostcodePart2 = cast(PostcodePart2 as varchar(50))
	,ResidentialStatusCode = cast(ResidentialStatusCode as varchar(50))
	,PhoneMobile = cast(PhoneMobile as varchar(50))
	,Phone1 = cast(Phone1 as varchar(50))
	,Phone2 = cast(Phone2 as varchar(50))
	,OriginatingPasInternalPatientNo1 = cast(OriginatingPasInternalPatientNo1 as varchar(50))
	,OriginatingPasInternalPatientNo2 = cast(OriginatingPasInternalPatientNo2 as varchar(50))
	,RestrictedDetailsFlag = cast(RestrictedDetailsFlag as varchar(50))
	,DiabetesDrugFlag = cast(DiabetesDrugFlag as varchar(50))
	,ExamsInRange = cast(ExamsInRange as varchar(1000))
	,UsesInhalerFlag = cast(UsesInhalerFlag as varchar(50))
	,PreviousEventDate = cast(PreviousEventDate as datetime)
	,NumberOfAttendances = cast(NumberOfAttendances as int)
	,NumberOfEvents = cast(NumberOfEvents as int)
	,PdsScn = cast(PdsScn as varchar(50))
	,PatientDetailsStatus = cast(PatientDetailsStatus as varchar(50))
	,HeartTransplantFlag = cast(HeartTransplantFlag as varchar(50))
	,CurrentLocationArrivalDate = cast(CurrentLocationArrivalDate as datetime)
	,EventCreatedDate = cast(EventCreatedDate as datetime)
	,EventCreatedTime = cast(EventCreatedTime as datetime)
	,EventModifiedDate = cast(EventModifiedDate as datetime)
	,EventModifiedTime = cast(EventModifiedTime as datetime)
	,RADEventCreated = cast(RADEventCreated as smalldatetime)
	,RADEventUpdated = cast(RADEventUpdated as smalldatetime)
	,ReportedDate = cast(ReportedDate as datetime)
	,ReportedTime = cast(ReportedTime as datetime)
	,ReportedBy = cast(ReportedBy as varchar(50))
	,TypedDate = cast(TypedDate as datetime)
	,TypedTime = cast(TypedTime as datetime)
	,TypedBy = cast(TypedBy as varchar(50))
	,VerifiedDate = cast(VerifiedDate as datetime)
	,VerifiedTime = cast(VerifiedTime as datetime)
	,VerifiedBy = cast(VerifiedBy as varchar(50))
	,LastVerifiedDate = cast(LastVerifiedDate as datetime)
	,LastVerifiedTime = cast(LastVerifiedTime as datetime)
	,LastVerifiedBy = cast(LastVerifiedBy as varchar(50))
	,AgeCode = cast(AgeCode as varchar(50))
	,OutcomeCode = cast(OutcomeCode as int)
	,ExamDurationMins = cast(ExamDurationMins as int)
	,ExamToReportDays = cast(ExamToReportDays as int)
	,ReportToTypeDays = cast(ReportToTypeDays as int)
	,TypeToVerifyDays = cast(TypeToVerifyDays as int)
	,ReportToVerifyDays = cast(ReportToVerifyDays as int)
	,ExamToVerifyDays = cast(ExamToVerifyDays as int)
	,DirectorateCode = cast(DirectorateCode as varchar(5))
	,CCGCode = cast(CCGCode as varchar(10))
	,ResidenceCCGCode = cast(ResidenceCCGCode as varchar(10))
	,NationalExamCode = cast(NationalExamCode as varchar(10))
	,ContextCode = cast(ContextCode as varchar(20))
into
	#TLoadDiagnosticBaseRadiology
from
	ETL.TLoadDiagnosticBaseRadiology Encounter


--CREATE CLUSTERED INDEX [IX_TLoadDiagnosticBase_01]
--ON #TLoadDiagnosticBase (ContextCode, SourceUniqueID)
 

merge
	Diagnostic.BaseRadiology target
using
	(
	select
		 ExamEncounterRecno
		,SourceUniqueID
		,AccessionNo
		,ExamCode
		,InterventionalFlag
		,AllocatedTimeMins
		,ExamDate
		,ExamStartTime
		,ExamEndTime
		,RoomCode
		,Radiographer1Code
		,Radiographer1CompetenceLevel
		,Radiographer1Difficulty
		,Radiographer2Code
		,Radiographer2CompetenceLevel
		,Radiographer2Difficulty
		,Radiographer3Code
		,Radiographer3CompetenceLevel
		,Radiographer3Difficulty
		,RadiologistCode
		,ContrastBatchNo
		,BoneDensitometryValue
		,Concentration
		,ContractCode
		,ContrastCode
		,ImageStoreDisk
		,ExamQuality
		,FlexibleForm
		,IgnoreAppt
		,InjectedBy
		,MuseumCode
		,Reaction
		,HasRedDot
		,RestrainDose
		,RestrainName
		,RestrainType
		,ScanSlices
		,ExamScreeningTime
		,ExamStatus
		,ExamBookedDate
		,ExamBookedTime
		,ApptBookMode
		,ApptCommitFlag
		,ApptPreviousDate
		,ApptPreviousTime
		,ApptLetterPrintedDate
		,SiteCodeAtTimeOfBooking
		,OrderCommsUniqueID
		,DaysWaiting
		,ExamLogicalDeleteFlag
		,ExamCreatedDate
		,ExamCreatedTime
		,ExamModifiedDate
		,ExamModifiedTime
		,ReportCheckedBy
		,DateReported
		,DateTyped
		,DefaultLength
		,IDChecked
		,IDCheckedBy
		,IsTyped
		,IsVerified
		,MPPSID
		,MPPSStatus
		,OrderID
		,PregnancyChecked
		,PregnancyCheckedBy
		,QuantityUsed
		,StudyID
		,TimeFirstVerified
		,TimeLastVerified
		,WaitBreachDate
		,WaitNPland
		,WasPlanned
		,WasScheduled
		,WasWaiting
		,WeeksWaiting
		,RADExamCreated
		,RADExamUpdated
		,EventEncounterRecno
		,EventSourceUniqueID
		,RTTPathwayID
		,SourcePatientNo
		,OriginalSourcePatientNo
		,Abnormal
		,AbnormalActioned
		,AENumber
		,AntiD
		,AttendanceNo
		,AttendedFlag
		,AwaitingAcknowledgement
		,BillReferrerFlag
		,BookingModeCode
		,BookedByCode
		,RequestDate
		,BookedDate
		,EventDate
		,EventTime
		,DateOnWL
		,DayOfWeek
		,EventLogicalDeleteFlag
		,HasPathway
		,IntendedRadiologistCode
		,IsDictated
		,IsProcessed
		,JustifiedBy
		,NumberOfProcs
		,NumberOfScannedDocs
		,PatientTypeCode
		,KeyDate
		,NumberOfCancellations
		,PacsVolumeKey
		,ImageNo
		,LeadClinician
		,LmpDate
		,MobilityCode
		,DuringOnCallHoursFlag
		,PatientHeight
		,PatientWeight
		,Practitioner
		,PregnancyEventKey
		,PregnancyFlag
		,Questions
		,Reason
		,SourceOfReferralCode
		,ReferrerCode
		,ReferralLocationCode
		,ReferringPCTCode
		,RequestCategoryCode
		,RequiredClinician
		,VettingStatus
		,XDSFolderID
		,XDSFolderUID
		,AgeAtEvent
		,ClinicalHistory
		,Comment
		,ConsentComment
		,PatientCreationDate
		,PatientCreationTime
		,Deleted
		,EmailConsent
		,EpisodicPCTCode
		,EpisodicPCT
		,SiteCode
		,AttendancesAtSite
		,SpecialtyCode
		,UnreportedFlag
		,PriorityCode
		,DictatedDate
		,DictatedTime
		,DictatedByCode
		,DictatedBy
		,DictatedBy2Code
		,DictatedBy2
		,ConsentFlag
		,ContrastReactionFlag
		,EpisodicReferrerCode
		,EpisodicLocationCode
		,Surname
		,Forenames
		,Title
		,DateOfBirth
		,DateOfDeath
		,DistrictNo
		,Language
		,PatientModifiedDate
		,PatientModifiedTime
		,NHSNumber
		,NHSNumberStatus
		,NHSTraceStatus
		,RegisteredGpCode
		,RegisteredPracticeCode
		,TelMobileConsent
		,EthnicOriginCode
		,MRSAIndicator
		,SexCode
		,Address1
		,Address2
		,Address3
		,Address4
		,Postcode
		,PostcodePart1
		,PostcodePart2
		,ResidentialStatusCode
		,PhoneMobile
		,Phone1
		,Phone2
		,OriginatingPasInternalPatientNo1
		,OriginatingPasInternalPatientNo2
		,RestrictedDetailsFlag
		,DiabetesDrugFlag
		,ExamsInRange
		,UsesInhalerFlag
		,PreviousEventDate
		,NumberOfAttendances
		,NumberOfEvents
		,PdsScn
		,PatientDetailsStatus
		,HeartTransplantFlag
		,CurrentLocationArrivalDate
		,EventCreatedDate
		,EventCreatedTime
		,EventModifiedDate
		,EventModifiedTime
		,RADEventCreated
		,RADEventUpdated
		,ReportedDate
		,ReportedTime
		,ReportedBy
		,TypedDate
		,TypedTime
		,TypedBy
		,VerifiedDate
		,VerifiedTime
		,VerifiedBy
		,LastVerifiedDate
		,LastVerifiedTime
		,LastVerifiedBy
		,AgeCode
		,OutcomeCode
		,ExamDurationMins
		,ExamToReportDays
		,ReportToTypeDays
		,TypeToVerifyDays
		,ReportToVerifyDays
		,ExamToVerifyDays
		,DirectorateCode
		,CCGCode
		,ResidenceCCGCode
		,NationalExamCode
		,ContextCode
	from
		#TLoadDiagnosticBaseRadiology
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete


	when not matched
	then
		insert
			(
			 ExamEncounterRecno
			,SourceUniqueID
			,AccessionNo
			,ExamCode
			,InterventionalFlag
			,AllocatedTimeMins
			,ExamDate
			,ExamStartTime
			,ExamEndTime
			,RoomCode
			,Radiographer1Code
			,Radiographer1CompetenceLevel
			,Radiographer1Difficulty
			,Radiographer2Code
			,Radiographer2CompetenceLevel
			,Radiographer2Difficulty
			,Radiographer3Code
			,Radiographer3CompetenceLevel
			,Radiographer3Difficulty
			,RadiologistCode
			,ContrastBatchNo
			,BoneDensitometryValue
			,Concentration
			,ContractCode
			,ContrastCode
			,ImageStoreDisk
			,ExamQuality
			,FlexibleForm
			,IgnoreAppt
			,InjectedBy
			,MuseumCode
			,Reaction
			,HasRedDot
			,RestrainDose
			,RestrainName
			,RestrainType
			,ScanSlices
			,ExamScreeningTime
			,ExamStatus
			,ExamBookedDate
			,ExamBookedTime
			,ApptBookMode
			,ApptCommitFlag
			,ApptPreviousDate
			,ApptPreviousTime
			,ApptLetterPrintedDate
			,SiteCodeAtTimeOfBooking
			,OrderCommsUniqueID
			,DaysWaiting
			,ExamLogicalDeleteFlag
			,ExamCreatedDate
			,ExamCreatedTime
			,ExamModifiedDate
			,ExamModifiedTime
			,ReportCheckedBy
			,DateReported
			,DateTyped
			,DefaultLength
			,IDChecked
			,IDCheckedBy
			,IsTyped
			,IsVerified
			,MPPSID
			,MPPSStatus
			,OrderID
			,PregnancyChecked
			,PregnancyCheckedBy
			,QuantityUsed
			,StudyID
			,TimeFirstVerified
			,TimeLastVerified
			,WaitBreachDate
			,WaitNPland
			,WasPlanned
			,WasScheduled
			,WasWaiting
			,WeeksWaiting
			,RADExamCreated
			,RADExamUpdated
			,EventEncounterRecno
			,EventSourceUniqueID
			,RTTPathwayID
			,SourcePatientNo
			,OriginalSourcePatientNo
			,Abnormal
			,AbnormalActioned
			,AENumber
			,AntiD
			,AttendanceNo
			,AttendedFlag
			,AwaitingAcknowledgement
			,BillReferrerFlag
			,BookingModeCode
			,BookedByCode
			,RequestDate
			,BookedDate
			,EventDate
			,EventTime
			,DateOnWL
			,DayOfWeek
			,EventLogicalDeleteFlag
			,HasPathway
			,IntendedRadiologistCode
			,IsDictated
			,IsProcessed
			,JustifiedBy
			,NumberOfProcs
			,NumberOfScannedDocs
			,PatientTypeCode
			,KeyDate
			,NumberOfCancellations
			,PacsVolumeKey
			,ImageNo
			,LeadClinician
			,LmpDate
			,MobilityCode
			,DuringOnCallHoursFlag
			,PatientHeight
			,PatientWeight
			,Practitioner
			,PregnancyEventKey
			,PregnancyFlag
			,Questions
			,Reason
			,SourceOfReferralCode
			,ReferrerCode
			,ReferralLocationCode
			,ReferringPCTCode
			,RequestCategoryCode
			,RequiredClinician
			,VettingStatus
			,XDSFolderID
			,XDSFolderUID
			,AgeAtEvent
			,ClinicalHistory
			,Comment
			,ConsentComment
			,PatientCreationDate
			,PatientCreationTime
			,Deleted
			,EmailConsent
			,EpisodicPCTCode
			,EpisodicPCT
			,SiteCode
			,AttendancesAtSite
			,SpecialtyCode
			,UnreportedFlag
			,PriorityCode
			,DictatedDate
			,DictatedTime
			,DictatedByCode
			,DictatedBy
			,DictatedBy2Code
			,DictatedBy2
			,ConsentFlag
			,ContrastReactionFlag
			,EpisodicReferrerCode
			,EpisodicLocationCode
			,Surname
			,Forenames
			,Title
			,DateOfBirth
			,DateOfDeath
			,DistrictNo
			,Language
			,PatientModifiedDate
			,PatientModifiedTime
			,NHSNumber
			,NHSNumberStatus
			,NHSTraceStatus
			,RegisteredGpCode
			,RegisteredPracticeCode
			,TelMobileConsent
			,EthnicOriginCode
			,MRSAIndicator
			,SexCode
			,Address1
			,Address2
			,Address3
			,Address4
			,Postcode
			,PostcodePart1
			,PostcodePart2
			,ResidentialStatusCode
			,PhoneMobile
			,Phone1
			,Phone2
			,OriginatingPasInternalPatientNo1
			,OriginatingPasInternalPatientNo2
			,RestrictedDetailsFlag
			,DiabetesDrugFlag
			,ExamsInRange
			,UsesInhalerFlag
			,PreviousEventDate
			,NumberOfAttendances
			,NumberOfEvents
			,PdsScn
			,PatientDetailsStatus
			,HeartTransplantFlag
			,CurrentLocationArrivalDate
			,EventCreatedDate
			,EventCreatedTime
			,EventModifiedDate
			,EventModifiedTime
			,RADEventCreated
			,RADEventUpdated
			,ReportedDate
			,ReportedTime
			,ReportedBy
			,TypedDate
			,TypedTime
			,TypedBy
			,VerifiedDate
			,VerifiedTime
			,VerifiedBy
			,LastVerifiedDate
			,LastVerifiedTime
			,LastVerifiedBy
			,AgeCode
			,OutcomeCode
			,ExamDurationMins
			,ExamToReportDays
			,ReportToTypeDays
			,TypeToVerifyDays
			,ReportToVerifyDays
			,ExamToVerifyDays
			,DirectorateCode
			,CCGCode
			,ResidenceCCGCode
			,NationalExamCode
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 source.ExamEncounterRecno
			,source.SourceUniqueID
			,source.AccessionNo
			,source.ExamCode
			,source.InterventionalFlag
			,source.AllocatedTimeMins
			,source.ExamDate
			,source.ExamStartTime
			,source.ExamEndTime
			,source.RoomCode
			,source.Radiographer1Code
			,source.Radiographer1CompetenceLevel
			,source.Radiographer1Difficulty
			,source.Radiographer2Code
			,source.Radiographer2CompetenceLevel
			,source.Radiographer2Difficulty
			,source.Radiographer3Code
			,source.Radiographer3CompetenceLevel
			,source.Radiographer3Difficulty
			,source.RadiologistCode
			,source.ContrastBatchNo
			,source.BoneDensitometryValue
			,source.Concentration
			,source.ContractCode
			,source.ContrastCode
			,source.ImageStoreDisk
			,source.ExamQuality
			,source.FlexibleForm
			,source.IgnoreAppt
			,source.InjectedBy
			,source.MuseumCode
			,source.Reaction
			,source.HasRedDot
			,source.RestrainDose
			,source.RestrainName
			,source.RestrainType
			,source.ScanSlices
			,source.ExamScreeningTime
			,source.ExamStatus
			,source.ExamBookedDate
			,source.ExamBookedTime
			,source.ApptBookMode
			,source.ApptCommitFlag
			,source.ApptPreviousDate
			,source.ApptPreviousTime
			,source.ApptLetterPrintedDate
			,source.SiteCodeAtTimeOfBooking
			,source.OrderCommsUniqueID
			,source.DaysWaiting
			,source.ExamLogicalDeleteFlag
			,source.ExamCreatedDate
			,source.ExamCreatedTime
			,source.ExamModifiedDate
			,source.ExamModifiedTime
			,source.ReportCheckedBy
			,source.DateReported
			,source.DateTyped
			,source.DefaultLength
			,source.IDChecked
			,source.IDCheckedBy
			,source.IsTyped
			,source.IsVerified
			,source.MPPSID
			,source.MPPSStatus
			,source.OrderID
			,source.PregnancyChecked
			,source.PregnancyCheckedBy
			,source.QuantityUsed
			,source.StudyID
			,source.TimeFirstVerified
			,source.TimeLastVerified
			,source.WaitBreachDate
			,source.WaitNPland
			,source.WasPlanned
			,source.WasScheduled
			,source.WasWaiting
			,source.WeeksWaiting
			,source.RADExamCreated
			,source.RADExamUpdated
			,source.EventEncounterRecno
			,source.EventSourceUniqueID
			,source.RTTPathwayID
			,source.SourcePatientNo
			,source.OriginalSourcePatientNo
			,source.Abnormal
			,source.AbnormalActioned
			,source.AENumber
			,source.AntiD
			,source.AttendanceNo
			,source.AttendedFlag
			,source.AwaitingAcknowledgement
			,source.BillReferrerFlag
			,source.BookingModeCode
			,source.BookedByCode
			,source.RequestDate
			,source.BookedDate
			,source.EventDate
			,source.EventTime
			,source.DateOnWL
			,source.DayOfWeek
			,source.EventLogicalDeleteFlag
			,source.HasPathway
			,source.IntendedRadiologistCode
			,source.IsDictated
			,source.IsProcessed
			,source.JustifiedBy
			,source.NumberOfProcs
			,source.NumberOfScannedDocs
			,source.PatientTypeCode
			,source.KeyDate
			,source.NumberOfCancellations
			,source.PacsVolumeKey
			,source.ImageNo
			,source.LeadClinician
			,source.LmpDate
			,source.MobilityCode
			,source.DuringOnCallHoursFlag
			,source.PatientHeight
			,source.PatientWeight
			,source.Practitioner
			,source.PregnancyEventKey
			,source.PregnancyFlag
			,source.Questions
			,source.Reason
			,source.SourceOfReferralCode
			,source.ReferrerCode
			,source.ReferralLocationCode
			,source.ReferringPCTCode
			,source.RequestCategoryCode
			,source.RequiredClinician
			,source.VettingStatus
			,source.XDSFolderID
			,source.XDSFolderUID
			,source.AgeAtEvent
			,source.ClinicalHistory
			,source.Comment
			,source.ConsentComment
			,source.PatientCreationDate
			,source.PatientCreationTime
			,source.Deleted
			,source.EmailConsent
			,source.EpisodicPCTCode
			,source.EpisodicPCT
			,source.SiteCode
			,source.AttendancesAtSite
			,source.SpecialtyCode
			,source.UnreportedFlag
			,source.PriorityCode
			,source.DictatedDate
			,source.DictatedTime
			,source.DictatedByCode
			,source.DictatedBy
			,source.DictatedBy2Code
			,source.DictatedBy2
			,source.ConsentFlag
			,source.ContrastReactionFlag
			,source.EpisodicReferrerCode
			,source.EpisodicLocationCode
			,source.Surname
			,source.Forenames
			,source.Title
			,source.DateOfBirth
			,source.DateOfDeath
			,source.DistrictNo
			,source.Language
			,source.PatientModifiedDate
			,source.PatientModifiedTime
			,source.NHSNumber
			,source.NHSNumberStatus
			,source.NHSTraceStatus
			,source.RegisteredGpCode
			,source.RegisteredPracticeCode
			,source.TelMobileConsent
			,source.EthnicOriginCode
			,source.MRSAIndicator
			,source.SexCode
			,source.Address1
			,source.Address2
			,source.Address3
			,source.Address4
			,source.Postcode
			,source.PostcodePart1
			,source.PostcodePart2
			,source.ResidentialStatusCode
			,source.PhoneMobile
			,source.Phone1
			,source.Phone2
			,source.OriginatingPasInternalPatientNo1
			,source.OriginatingPasInternalPatientNo2
			,source.RestrictedDetailsFlag
			,source.DiabetesDrugFlag
			,source.ExamsInRange
			,source.UsesInhalerFlag
			,source.PreviousEventDate
			,source.NumberOfAttendances
			,source.NumberOfEvents
			,source.PdsScn
			,source.PatientDetailsStatus
			,source.HeartTransplantFlag
			,source.CurrentLocationArrivalDate
			,source.EventCreatedDate
			,source.EventCreatedTime
			,source.EventModifiedDate
			,source.EventModifiedTime
			,source.RADEventCreated
			,source.RADEventUpdated
			,source.ReportedDate
			,source.ReportedTime
			,source.ReportedBy
			,source.TypedDate
			,source.TypedTime
			,source.TypedBy
			,source.VerifiedDate
			,source.VerifiedTime
			,source.VerifiedBy
			,source.LastVerifiedDate
			,source.LastVerifiedTime
			,source.LastVerifiedBy
			,source.AgeCode
			,source.OutcomeCode
			,source.ExamDurationMins
			,source.ExamToReportDays
			,source.ReportToTypeDays
			,source.TypeToVerifyDays
			,source.ReportToVerifyDays
			,source.ExamToVerifyDays
			,source.DirectorateCode
			,source.CCGCode
			,source.ResidenceCCGCode
			,source.NationalExamCode
			,source.ContextCode
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.ExamEncounterRecno, 0) = isnull(source.ExamEncounterRecno, 0)
		and isnull(target.AccessionNo, '') = isnull(source.AccessionNo, '')
		and isnull(target.ExamCode, '') = isnull(source.ExamCode, '')
		and isnull(target.InterventionalFlag, '') = isnull(source.InterventionalFlag, '')
		and isnull(target.AllocatedTimeMins, 0) = isnull(source.AllocatedTimeMins, 0)
		and isnull(target.ExamDate, getdate()) = isnull(source.ExamDate, getdate())
		and isnull(target.ExamStartTime, getdate()) = isnull(source.ExamStartTime, getdate())
		and isnull(target.ExamEndTime, getdate()) = isnull(source.ExamEndTime, getdate())
		and isnull(target.RoomCode, '') = isnull(source.RoomCode, '')
		and isnull(target.Radiographer1Code, '') = isnull(source.Radiographer1Code, '')
		and isnull(target.Radiographer1CompetenceLevel, '') = isnull(source.Radiographer1CompetenceLevel, '')
		and isnull(target.Radiographer1Difficulty, '') = isnull(source.Radiographer1Difficulty, '')
		and isnull(target.Radiographer2Code, '') = isnull(source.Radiographer2Code, '')
		and isnull(target.Radiographer2CompetenceLevel, '') = isnull(source.Radiographer2CompetenceLevel, '')
		and isnull(target.Radiographer2Difficulty, '') = isnull(source.Radiographer2Difficulty, '')
		and isnull(target.Radiographer3Code, '') = isnull(source.Radiographer3Code, '')
		and isnull(target.Radiographer3CompetenceLevel, '') = isnull(source.Radiographer3CompetenceLevel, '')
		and isnull(target.Radiographer3Difficulty, '') = isnull(source.Radiographer3Difficulty, '')
		and isnull(target.RadiologistCode, '') = isnull(source.RadiologistCode, '')
		and isnull(target.ContrastBatchNo, '') = isnull(source.ContrastBatchNo, '')
		and isnull(target.BoneDensitometryValue, '') = isnull(source.BoneDensitometryValue, '')
		and isnull(target.Concentration, '') = isnull(source.Concentration, '')
		and isnull(target.ContractCode, '') = isnull(source.ContractCode, '')
		and isnull(target.ContrastCode, '') = isnull(source.ContrastCode, '')
		and isnull(target.ImageStoreDisk, '') = isnull(source.ImageStoreDisk, '')
		and isnull(target.ExamQuality, '') = isnull(source.ExamQuality, '')
		and isnull(target.FlexibleForm, '') = isnull(source.FlexibleForm, '')
		and isnull(target.IgnoreAppt, '') = isnull(source.IgnoreAppt, '')
		and isnull(target.InjectedBy, '') = isnull(source.InjectedBy, '')
		and isnull(target.MuseumCode, '') = isnull(source.MuseumCode, '')
		and isnull(target.Reaction, '') = isnull(source.Reaction, '')
		and isnull(target.HasRedDot, '') = isnull(source.HasRedDot, '')
		and isnull(target.RestrainDose, '') = isnull(source.RestrainDose, '')
		and isnull(target.RestrainName, '') = isnull(source.RestrainName, '')
		and isnull(target.RestrainType, '') = isnull(source.RestrainType, '')
		and isnull(target.ScanSlices, '') = isnull(source.ScanSlices, '')
		and isnull(target.ExamScreeningTime, '') = isnull(source.ExamScreeningTime, '')
		and isnull(target.ExamStatus, '') = isnull(source.ExamStatus, '')
		and isnull(target.ExamBookedDate, getdate()) = isnull(source.ExamBookedDate, getdate())
		and isnull(target.ExamBookedTime, getdate()) = isnull(source.ExamBookedTime, getdate())
		and isnull(target.ApptBookMode, '') = isnull(source.ApptBookMode, '')
		and isnull(target.ApptCommitFlag, '') = isnull(source.ApptCommitFlag, '')
		and isnull(target.ApptPreviousDate, getdate()) = isnull(source.ApptPreviousDate, getdate())
		and isnull(target.ApptPreviousTime, getdate()) = isnull(source.ApptPreviousTime, getdate())
		and isnull(target.ApptLetterPrintedDate, '') = isnull(source.ApptLetterPrintedDate, '')
		and isnull(target.SiteCodeAtTimeOfBooking, '') = isnull(source.SiteCodeAtTimeOfBooking, '')
		and isnull(target.OrderCommsUniqueID, '') = isnull(source.OrderCommsUniqueID, '')
		and isnull(target.DaysWaiting, 0) = isnull(source.DaysWaiting, 0)
		and isnull(target.ExamLogicalDeleteFlag, '') = isnull(source.ExamLogicalDeleteFlag, '')
		and isnull(target.ExamCreatedDate, getdate()) = isnull(source.ExamCreatedDate, getdate())
		and isnull(target.ExamCreatedTime, getdate()) = isnull(source.ExamCreatedTime, getdate())
		and isnull(target.ExamModifiedDate, getdate()) = isnull(source.ExamModifiedDate, getdate())
		and isnull(target.ExamModifiedTime, getdate()) = isnull(source.ExamModifiedTime, getdate())
		and isnull(target.ReportCheckedBy, '') = isnull(source.ReportCheckedBy, '')
		and isnull(target.DateReported, getdate()) = isnull(source.DateReported, getdate())
		and isnull(target.DateTyped, getdate()) = isnull(source.DateTyped, getdate())
		and isnull(target.DefaultLength, 0) = isnull(source.DefaultLength, 0)
		and isnull(target.IDChecked, '') = isnull(source.IDChecked, '')
		and isnull(target.IDCheckedBy, '') = isnull(source.IDCheckedBy, '')
		and isnull(target.IsTyped, '') = isnull(source.IsTyped, '')
		and isnull(target.IsVerified, '') = isnull(source.IsVerified, '')
		and isnull(target.MPPSID, '') = isnull(source.MPPSID, '')
		and isnull(target.MPPSStatus, '') = isnull(source.MPPSStatus, '')
		and isnull(target.OrderID, '') = isnull(source.OrderID, '')
		and isnull(target.PregnancyChecked, '') = isnull(source.PregnancyChecked, '')
		and isnull(target.PregnancyCheckedBy, '') = isnull(source.PregnancyCheckedBy, '')
		and isnull(target.QuantityUsed, 0) = isnull(source.QuantityUsed, 0)
		and isnull(target.StudyID, '') = isnull(source.StudyID, '')
		and isnull(target.TimeFirstVerified, getdate()) = isnull(source.TimeFirstVerified, getdate())
		and isnull(target.TimeLastVerified, getdate()) = isnull(source.TimeLastVerified, getdate())
		and isnull(target.WaitBreachDate, getdate()) = isnull(source.WaitBreachDate, getdate())
		and isnull(target.WaitNPland, '') = isnull(source.WaitNPland, '')
		and isnull(target.WasPlanned, '') = isnull(source.WasPlanned, '')
		and isnull(target.WasScheduled, '') = isnull(source.WasScheduled, '')
		and isnull(target.WasWaiting, '') = isnull(source.WasWaiting, '')
		and isnull(target.WeeksWaiting, 0) = isnull(source.WeeksWaiting, 0)
		and isnull(target.RADExamCreated, getdate()) = isnull(source.RADExamCreated, getdate())
		and isnull(target.RADExamUpdated, getdate()) = isnull(source.RADExamUpdated, getdate())
		and isnull(target.EventEncounterRecno, 0) = isnull(source.EventEncounterRecno, 0)
		and isnull(target.EventSourceUniqueID, '') = isnull(source.EventSourceUniqueID, '')
		and isnull(target.RTTPathwayID, '') = isnull(source.RTTPathwayID, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.OriginalSourcePatientNo, '') = isnull(source.OriginalSourcePatientNo, '')
		and isnull(target.Abnormal, '') = isnull(source.Abnormal, '')
		and isnull(target.AbnormalActioned, '') = isnull(source.AbnormalActioned, '')
		and isnull(target.AENumber, '') = isnull(source.AENumber, '')
		and isnull(target.AntiD, '') = isnull(source.AntiD, '')
		and isnull(target.AttendanceNo, '') = isnull(source.AttendanceNo, '')
		and isnull(target.AttendedFlag, '') = isnull(source.AttendedFlag, '')
		and isnull(target.AwaitingAcknowledgement, '') = isnull(source.AwaitingAcknowledgement, '')
		and isnull(target.BillReferrerFlag, '') = isnull(source.BillReferrerFlag, '')
		and isnull(target.BookingModeCode, '') = isnull(source.BookingModeCode, '')
		and isnull(target.BookedByCode, '') = isnull(source.BookedByCode, '')
		and isnull(target.RequestDate, getdate()) = isnull(source.RequestDate, getdate())
		and isnull(target.BookedDate, getdate()) = isnull(source.BookedDate, getdate())
		and isnull(target.EventDate, getdate()) = isnull(source.EventDate, getdate())
		and isnull(target.EventTime, getdate()) = isnull(source.EventTime, getdate())
		and isnull(target.DateOnWL, getdate()) = isnull(source.DateOnWL, getdate())
		and isnull(target.DayOfWeek, '') = isnull(source.DayOfWeek, '')
		and isnull(target.EventLogicalDeleteFlag, '') = isnull(source.EventLogicalDeleteFlag, '')
		and isnull(target.HasPathway, '') = isnull(source.HasPathway, '')
		and isnull(target.IntendedRadiologistCode, '') = isnull(source.IntendedRadiologistCode, '')
		and isnull(target.IsDictated, '') = isnull(source.IsDictated, '')
		and isnull(target.IsProcessed, '') = isnull(source.IsProcessed, '')
		and isnull(target.JustifiedBy, '') = isnull(source.JustifiedBy, '')
		and isnull(target.NumberOfProcs, 0) = isnull(source.NumberOfProcs, 0)
		and isnull(target.NumberOfScannedDocs, 0) = isnull(source.NumberOfScannedDocs, 0)
		and isnull(target.PatientTypeCode, '') = isnull(source.PatientTypeCode, '')
		and isnull(target.KeyDate, getdate()) = isnull(source.KeyDate, getdate())
		and isnull(target.NumberOfCancellations, 0) = isnull(source.NumberOfCancellations, 0)
		and isnull(target.PacsVolumeKey, '') = isnull(source.PacsVolumeKey, '')
		and isnull(target.ImageNo, '') = isnull(source.ImageNo, '')
		and isnull(target.LeadClinician, '') = isnull(source.LeadClinician, '')
		and isnull(target.LmpDate, getdate()) = isnull(source.LmpDate, getdate())
		and isnull(target.MobilityCode, '') = isnull(source.MobilityCode, '')
		and isnull(target.DuringOnCallHoursFlag, '') = isnull(source.DuringOnCallHoursFlag, '')
		and isnull(target.PatientHeight, '') = isnull(source.PatientHeight, '')
		and isnull(target.PatientWeight, '') = isnull(source.PatientWeight, '')
		and isnull(target.Practitioner, '') = isnull(source.Practitioner, '')
		and isnull(target.PregnancyEventKey, '') = isnull(source.PregnancyEventKey, '')
		and isnull(target.PregnancyFlag, '') = isnull(source.PregnancyFlag, '')
		and isnull(target.Questions, '') = isnull(source.Questions, '')
		and isnull(target.Reason, '') = isnull(source.Reason, '')
		and isnull(target.SourceOfReferralCode, '') = isnull(source.SourceOfReferralCode, '')
		and isnull(target.ReferrerCode, '') = isnull(source.ReferrerCode, '')
		and isnull(target.ReferralLocationCode, '') = isnull(source.ReferralLocationCode, '')
		and isnull(target.ReferringPCTCode, '') = isnull(source.ReferringPCTCode, '')
		and isnull(target.RequestCategoryCode, '') = isnull(source.RequestCategoryCode, '')
		and isnull(target.RequiredClinician, '') = isnull(source.RequiredClinician, '')
		and isnull(target.VettingStatus, '') = isnull(source.VettingStatus, '')
		and isnull(target.XDSFolderID, '') = isnull(source.XDSFolderID, '')
		and isnull(target.XDSFolderUID, '') = isnull(source.XDSFolderUID, '')
		and isnull(target.AgeAtEvent, 0) = isnull(source.AgeAtEvent, 0)
		and isnull(target.ClinicalHistory, '') = isnull(source.ClinicalHistory, '')
		and isnull(target.Comment, '') = isnull(source.Comment, '')
		and isnull(target.ConsentComment, '') = isnull(source.ConsentComment, '')
		and isnull(target.PatientCreationDate, getdate()) = isnull(source.PatientCreationDate, getdate())
		and isnull(target.PatientCreationTime, getdate()) = isnull(source.PatientCreationTime, getdate())
		and isnull(target.Deleted, '') = isnull(source.Deleted, '')
		and isnull(target.EmailConsent, '') = isnull(source.EmailConsent, '')
		and isnull(target.EpisodicPCTCode, '') = isnull(source.EpisodicPCTCode, '')
		and isnull(target.EpisodicPCT, '') = isnull(source.EpisodicPCT, '')
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.AttendancesAtSite, '') = isnull(source.AttendancesAtSite, '')
		and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		and isnull(target.UnreportedFlag, '') = isnull(source.UnreportedFlag, '')
		and isnull(target.PriorityCode, '') = isnull(source.PriorityCode, '')
		and isnull(target.DictatedDate, getdate()) = isnull(source.DictatedDate, getdate())
		and isnull(target.DictatedTime, getdate()) = isnull(source.DictatedTime, getdate())
		and isnull(target.DictatedByCode, '') = isnull(source.DictatedByCode, '')
		and isnull(target.DictatedBy, '') = isnull(source.DictatedBy, '')
		and isnull(target.DictatedBy2Code, '') = isnull(source.DictatedBy2Code, '')
		and isnull(target.DictatedBy2, '') = isnull(source.DictatedBy2, '')
		and isnull(target.ConsentFlag, '') = isnull(source.ConsentFlag, '')
		and isnull(target.ContrastReactionFlag, '') = isnull(source.ContrastReactionFlag, '')
		and isnull(target.EpisodicReferrerCode, '') = isnull(source.EpisodicReferrerCode, '')
		and isnull(target.EpisodicLocationCode, '') = isnull(source.EpisodicLocationCode, '')
		and isnull(target.Surname, '') = isnull(source.Surname, '')
		and isnull(target.Forenames, '') = isnull(source.Forenames, '')
		and isnull(target.Title, '') = isnull(source.Title, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.Language, '') = isnull(source.Language, '')
		and isnull(target.PatientModifiedDate, getdate()) = isnull(source.PatientModifiedDate, getdate())
		and isnull(target.PatientModifiedTime, getdate()) = isnull(source.PatientModifiedTime, getdate())
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.NHSNumberStatus, '') = isnull(source.NHSNumberStatus, '')
		and isnull(target.NHSTraceStatus, '') = isnull(source.NHSTraceStatus, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredPracticeCode, '') = isnull(source.RegisteredPracticeCode, '')
		and isnull(target.TelMobileConsent, '') = isnull(source.TelMobileConsent, '')
		and isnull(target.EthnicOriginCode, '') = isnull(source.EthnicOriginCode, '')
		and isnull(target.MRSAIndicator, '') = isnull(source.MRSAIndicator, '')
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.Address1, '') = isnull(source.Address1, '')
		and isnull(target.Address2, '') = isnull(source.Address2, '')
		and isnull(target.Address3, '') = isnull(source.Address3, '')
		and isnull(target.Address4, '') = isnull(source.Address4, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.PostcodePart1, '') = isnull(source.PostcodePart1, '')
		and isnull(target.PostcodePart2, '') = isnull(source.PostcodePart2, '')
		and isnull(target.ResidentialStatusCode, '') = isnull(source.ResidentialStatusCode, '')
		and isnull(target.PhoneMobile, '') = isnull(source.PhoneMobile, '')
		and isnull(target.Phone1, '') = isnull(source.Phone1, '')
		and isnull(target.Phone2, '') = isnull(source.Phone2, '')
		and isnull(target.OriginatingPasInternalPatientNo1, '') = isnull(source.OriginatingPasInternalPatientNo1, '')
		and isnull(target.OriginatingPasInternalPatientNo2, '') = isnull(source.OriginatingPasInternalPatientNo2, '')
		and isnull(target.RestrictedDetailsFlag, '') = isnull(source.RestrictedDetailsFlag, '')
		and isnull(target.DiabetesDrugFlag, '') = isnull(source.DiabetesDrugFlag, '')
		and isnull(target.ExamsInRange, '') = isnull(source.ExamsInRange, '')
		and isnull(target.UsesInhalerFlag, '') = isnull(source.UsesInhalerFlag, '')
		and isnull(target.PreviousEventDate, getdate()) = isnull(source.PreviousEventDate, getdate())
		and isnull(target.NumberOfAttendances, 0) = isnull(source.NumberOfAttendances, 0)
		and isnull(target.NumberOfEvents, 0) = isnull(source.NumberOfEvents, 0)
		and isnull(target.PdsScn, '') = isnull(source.PdsScn, '')
		and isnull(target.PatientDetailsStatus, '') = isnull(source.PatientDetailsStatus, '')
		and isnull(target.HeartTransplantFlag, '') = isnull(source.HeartTransplantFlag, '')
		and isnull(target.CurrentLocationArrivalDate, getdate()) = isnull(source.CurrentLocationArrivalDate, getdate())
		and isnull(target.EventCreatedDate, getdate()) = isnull(source.EventCreatedDate, getdate())
		and isnull(target.EventCreatedTime, getdate()) = isnull(source.EventCreatedTime, getdate())
		and isnull(target.EventModifiedDate, getdate()) = isnull(source.EventModifiedDate, getdate())
		and isnull(target.EventModifiedTime, getdate()) = isnull(source.EventModifiedTime, getdate())
		and isnull(target.RADEventCreated, getdate()) = isnull(source.RADEventCreated, getdate())
		and isnull(target.RADEventUpdated, getdate()) = isnull(source.RADEventUpdated, getdate())
		and isnull(target.ReportedDate, getdate()) = isnull(source.ReportedDate, getdate())
		and isnull(target.ReportedTime, getdate()) = isnull(source.ReportedTime, getdate())
		and isnull(target.ReportedBy, '') = isnull(source.ReportedBy, '')
		and isnull(target.TypedDate, getdate()) = isnull(source.TypedDate, getdate())
		and isnull(target.TypedTime, getdate()) = isnull(source.TypedTime, getdate())
		and isnull(target.TypedBy, '') = isnull(source.TypedBy, '')
		and isnull(target.VerifiedDate, getdate()) = isnull(source.VerifiedDate, getdate())
		and isnull(target.VerifiedTime, getdate()) = isnull(source.VerifiedTime, getdate())
		and isnull(target.VerifiedBy, '') = isnull(source.VerifiedBy, '')
		and isnull(target.LastVerifiedDate, getdate()) = isnull(source.LastVerifiedDate, getdate())
		and isnull(target.LastVerifiedTime, getdate()) = isnull(source.LastVerifiedTime, getdate())
		and isnull(target.LastVerifiedBy, '') = isnull(source.LastVerifiedBy, '')
		and isnull(target.AgeCode, '') = isnull(source.AgeCode, '')
		and isnull(target.OutcomeCode, 0) = isnull(source.OutcomeCode, 0)
		and isnull(target.ExamDurationMins, 0) = isnull(source.ExamDurationMins, 0)
		and isnull(target.ExamToReportDays, 0) = isnull(source.ExamToReportDays, 0)
		and isnull(target.ReportToTypeDays, 0) = isnull(source.ReportToTypeDays, 0)
		and isnull(target.TypeToVerifyDays, 0) = isnull(source.TypeToVerifyDays, 0)
		and isnull(target.ReportToVerifyDays, 0) = isnull(source.ReportToVerifyDays, 0)
		and isnull(target.ExamToVerifyDays, 0) = isnull(source.ExamToVerifyDays, 0)
		and isnull(target.DirectorateCode, '') = isnull(source.DirectorateCode, '')
		and isnull(target.CCGCode, '') = isnull(source.CCGCode, '')
		and isnull(target.ResidenceCCGCode, '') = isnull(source.ResidenceCCGCode, '')
		and isnull(target.NationalExamCode, '') = isnull(source.NationalExamCode, '')
		)
	then
		update
		set
			 target.ExamEncounterRecno = source.ExamEncounterRecno
			,target.AccessionNo = source.AccessionNo
			,target.ExamCode = source.ExamCode
			,target.InterventionalFlag = source.InterventionalFlag
			,target.AllocatedTimeMins = source.AllocatedTimeMins
			,target.ExamDate = source.ExamDate
			,target.ExamStartTime = source.ExamStartTime
			,target.ExamEndTime = source.ExamEndTime
			,target.RoomCode = source.RoomCode
			,target.Radiographer1Code = source.Radiographer1Code
			,target.Radiographer1CompetenceLevel = source.Radiographer1CompetenceLevel
			,target.Radiographer1Difficulty = source.Radiographer1Difficulty
			,target.Radiographer2Code = source.Radiographer2Code
			,target.Radiographer2CompetenceLevel = source.Radiographer2CompetenceLevel
			,target.Radiographer2Difficulty = source.Radiographer2Difficulty
			,target.Radiographer3Code = source.Radiographer3Code
			,target.Radiographer3CompetenceLevel = source.Radiographer3CompetenceLevel
			,target.Radiographer3Difficulty = source.Radiographer3Difficulty
			,target.RadiologistCode = source.RadiologistCode
			,target.ContrastBatchNo = source.ContrastBatchNo
			,target.BoneDensitometryValue = source.BoneDensitometryValue
			,target.Concentration = source.Concentration
			,target.ContractCode = source.ContractCode
			,target.ContrastCode = source.ContrastCode
			,target.ImageStoreDisk = source.ImageStoreDisk
			,target.ExamQuality = source.ExamQuality
			,target.FlexibleForm = source.FlexibleForm
			,target.IgnoreAppt = source.IgnoreAppt
			,target.InjectedBy = source.InjectedBy
			,target.MuseumCode = source.MuseumCode
			,target.Reaction = source.Reaction
			,target.HasRedDot = source.HasRedDot
			,target.RestrainDose = source.RestrainDose
			,target.RestrainName = source.RestrainName
			,target.RestrainType = source.RestrainType
			,target.ScanSlices = source.ScanSlices
			,target.ExamScreeningTime = source.ExamScreeningTime
			,target.ExamStatus = source.ExamStatus
			,target.ExamBookedDate = source.ExamBookedDate
			,target.ExamBookedTime = source.ExamBookedTime
			,target.ApptBookMode = source.ApptBookMode
			,target.ApptCommitFlag = source.ApptCommitFlag
			,target.ApptPreviousDate = source.ApptPreviousDate
			,target.ApptPreviousTime = source.ApptPreviousTime
			,target.ApptLetterPrintedDate = source.ApptLetterPrintedDate
			,target.SiteCodeAtTimeOfBooking = source.SiteCodeAtTimeOfBooking
			,target.OrderCommsUniqueID = source.OrderCommsUniqueID
			,target.DaysWaiting = source.DaysWaiting
			,target.ExamLogicalDeleteFlag = source.ExamLogicalDeleteFlag
			,target.ExamCreatedDate = source.ExamCreatedDate
			,target.ExamCreatedTime = source.ExamCreatedTime
			,target.ExamModifiedDate = source.ExamModifiedDate
			,target.ExamModifiedTime = source.ExamModifiedTime
			,target.ReportCheckedBy = source.ReportCheckedBy
			,target.DateReported = source.DateReported
			,target.DateTyped = source.DateTyped
			,target.DefaultLength = source.DefaultLength
			,target.IDChecked = source.IDChecked
			,target.IDCheckedBy = source.IDCheckedBy
			,target.IsTyped = source.IsTyped
			,target.IsVerified = source.IsVerified
			,target.MPPSID = source.MPPSID
			,target.MPPSStatus = source.MPPSStatus
			,target.OrderID = source.OrderID
			,target.PregnancyChecked = source.PregnancyChecked
			,target.PregnancyCheckedBy = source.PregnancyCheckedBy
			,target.QuantityUsed = source.QuantityUsed
			,target.StudyID = source.StudyID
			,target.TimeFirstVerified = source.TimeFirstVerified
			,target.TimeLastVerified = source.TimeLastVerified
			,target.WaitBreachDate = source.WaitBreachDate
			,target.WaitNPland = source.WaitNPland
			,target.WasPlanned = source.WasPlanned
			,target.WasScheduled = source.WasScheduled
			,target.WasWaiting = source.WasWaiting
			,target.WeeksWaiting = source.WeeksWaiting
			,target.RADExamCreated = source.RADExamCreated
			,target.RADExamUpdated = source.RADExamUpdated
			,target.EventEncounterRecno = source.EventEncounterRecno
			,target.EventSourceUniqueID = source.EventSourceUniqueID
			,target.RTTPathwayID = source.RTTPathwayID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.OriginalSourcePatientNo = source.OriginalSourcePatientNo
			,target.Abnormal = source.Abnormal
			,target.AbnormalActioned = source.AbnormalActioned
			,target.AENumber = source.AENumber
			,target.AntiD = source.AntiD
			,target.AttendanceNo = source.AttendanceNo
			,target.AttendedFlag = source.AttendedFlag
			,target.AwaitingAcknowledgement = source.AwaitingAcknowledgement
			,target.BillReferrerFlag = source.BillReferrerFlag
			,target.BookingModeCode = source.BookingModeCode
			,target.BookedByCode = source.BookedByCode
			,target.RequestDate = source.RequestDate
			,target.BookedDate = source.BookedDate
			,target.EventDate = source.EventDate
			,target.EventTime = source.EventTime
			,target.DateOnWL = source.DateOnWL
			,target.DayOfWeek = source.DayOfWeek
			,target.EventLogicalDeleteFlag = source.EventLogicalDeleteFlag
			,target.HasPathway = source.HasPathway
			,target.IntendedRadiologistCode = source.IntendedRadiologistCode
			,target.IsDictated = source.IsDictated
			,target.IsProcessed = source.IsProcessed
			,target.JustifiedBy = source.JustifiedBy
			,target.NumberOfProcs = source.NumberOfProcs
			,target.NumberOfScannedDocs = source.NumberOfScannedDocs
			,target.PatientTypeCode = source.PatientTypeCode
			,target.KeyDate = source.KeyDate
			,target.NumberOfCancellations = source.NumberOfCancellations
			,target.PacsVolumeKey = source.PacsVolumeKey
			,target.ImageNo = source.ImageNo
			,target.LeadClinician = source.LeadClinician
			,target.LmpDate = source.LmpDate
			,target.MobilityCode = source.MobilityCode
			,target.DuringOnCallHoursFlag = source.DuringOnCallHoursFlag
			,target.PatientHeight = source.PatientHeight
			,target.PatientWeight = source.PatientWeight
			,target.Practitioner = source.Practitioner
			,target.PregnancyEventKey = source.PregnancyEventKey
			,target.PregnancyFlag = source.PregnancyFlag
			,target.Questions = source.Questions
			,target.Reason = source.Reason
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ReferrerCode = source.ReferrerCode
			,target.ReferralLocationCode = source.ReferralLocationCode
			,target.ReferringPCTCode = source.ReferringPCTCode
			,target.RequestCategoryCode = source.RequestCategoryCode
			,target.RequiredClinician = source.RequiredClinician
			,target.VettingStatus = source.VettingStatus
			,target.XDSFolderID = source.XDSFolderID
			,target.XDSFolderUID = source.XDSFolderUID
			,target.AgeAtEvent = source.AgeAtEvent
			,target.ClinicalHistory = source.ClinicalHistory
			,target.Comment = source.Comment
			,target.ConsentComment = source.ConsentComment
			,target.PatientCreationDate = source.PatientCreationDate
			,target.PatientCreationTime = source.PatientCreationTime
			,target.Deleted = source.Deleted
			,target.EmailConsent = source.EmailConsent
			,target.EpisodicPCTCode = source.EpisodicPCTCode
			,target.EpisodicPCT = source.EpisodicPCT
			,target.SiteCode = source.SiteCode
			,target.AttendancesAtSite = source.AttendancesAtSite
			,target.SpecialtyCode = source.SpecialtyCode
			,target.UnreportedFlag = source.UnreportedFlag
			,target.PriorityCode = source.PriorityCode
			,target.DictatedDate = source.DictatedDate
			,target.DictatedTime = source.DictatedTime
			,target.DictatedByCode = source.DictatedByCode
			,target.DictatedBy = source.DictatedBy
			,target.DictatedBy2Code = source.DictatedBy2Code
			,target.DictatedBy2 = source.DictatedBy2
			,target.ConsentFlag = source.ConsentFlag
			,target.ContrastReactionFlag = source.ContrastReactionFlag
			,target.EpisodicReferrerCode = source.EpisodicReferrerCode
			,target.EpisodicLocationCode = source.EpisodicLocationCode
			,target.Surname = source.Surname
			,target.Forenames = source.Forenames
			,target.Title = source.Title
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.DistrictNo = source.DistrictNo
			,target.Language = source.Language
			,target.PatientModifiedDate = source.PatientModifiedDate
			,target.PatientModifiedTime = source.PatientModifiedTime
			,target.NHSNumber = source.NHSNumber
			,target.NHSNumberStatus = source.NHSNumberStatus
			,target.NHSTraceStatus = source.NHSTraceStatus
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredPracticeCode = source.RegisteredPracticeCode
			,target.TelMobileConsent = source.TelMobileConsent
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.MRSAIndicator = source.MRSAIndicator
			,target.SexCode = source.SexCode
			,target.Address1 = source.Address1
			,target.Address2 = source.Address2
			,target.Address3 = source.Address3
			,target.Address4 = source.Address4
			,target.Postcode = source.Postcode
			,target.PostcodePart1 = source.PostcodePart1
			,target.PostcodePart2 = source.PostcodePart2
			,target.ResidentialStatusCode = source.ResidentialStatusCode
			,target.PhoneMobile = source.PhoneMobile
			,target.Phone1 = source.Phone1
			,target.Phone2 = source.Phone2
			,target.OriginatingPasInternalPatientNo1 = source.OriginatingPasInternalPatientNo1
			,target.OriginatingPasInternalPatientNo2 = source.OriginatingPasInternalPatientNo2
			,target.RestrictedDetailsFlag = source.RestrictedDetailsFlag
			,target.DiabetesDrugFlag = source.DiabetesDrugFlag
			,target.ExamsInRange = source.ExamsInRange
			,target.UsesInhalerFlag = source.UsesInhalerFlag
			,target.PreviousEventDate = source.PreviousEventDate
			,target.NumberOfAttendances = source.NumberOfAttendances
			,target.NumberOfEvents = source.NumberOfEvents
			,target.PdsScn = source.PdsScn
			,target.PatientDetailsStatus = source.PatientDetailsStatus
			,target.HeartTransplantFlag = source.HeartTransplantFlag
			,target.CurrentLocationArrivalDate = source.CurrentLocationArrivalDate
			,target.EventCreatedDate = source.EventCreatedDate
			,target.EventCreatedTime = source.EventCreatedTime
			,target.EventModifiedDate = source.EventModifiedDate
			,target.EventModifiedTime = source.EventModifiedTime
			,target.RADEventCreated = source.RADEventCreated
			,target.RADEventUpdated = source.RADEventUpdated
			,target.ReportedDate = source.ReportedDate
			,target.ReportedTime = source.ReportedTime
			,target.ReportedBy = source.ReportedBy
			,target.TypedDate = source.TypedDate
			,target.TypedTime = source.TypedTime
			,target.TypedBy = source.TypedBy
			,target.VerifiedDate = source.VerifiedDate
			,target.VerifiedTime = source.VerifiedTime
			,target.VerifiedBy = source.VerifiedBy
			,target.LastVerifiedDate = source.LastVerifiedDate
			,target.LastVerifiedTime = source.LastVerifiedTime
			,target.LastVerifiedBy = source.LastVerifiedBy
			,target.AgeCode = source.AgeCode
			,target.OutcomeCode = source.OutcomeCode
			,target.ExamDurationMins = source.ExamDurationMins
			,target.ExamToReportDays = source.ExamToReportDays
			,target.ReportToTypeDays = source.ReportToTypeDays
			,target.TypeToVerifyDays = source.TypeToVerifyDays
			,target.ReportToVerifyDays = source.ReportToVerifyDays
			,target.ExamToVerifyDays = source.ExamToVerifyDays
			,target.DirectorateCode = source.DirectorateCode
			,target.CCGCode = source.CCGCode
			,target.ResidenceCCGCode = source.ResidenceCCGCode
			,target.NationalExamCode = source.NationalExamCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


﻿
CREATE procedure [ETL].[BuildTLoadAEBaseEncounter] @LastLoadTime datetime as

declare @ReturnValue int = 0


BEGIN TRY

	truncate table ETL.TLoadAEBaseEncounter

	insert
	into
		ETL.TLoadAEBaseEncounter
	(
		 EncounterRecno
		,SourceUniqueID
		,UniqueBookingReferenceNo
		,PathwayId
		,PathwayIdIssuerCode
		,RTTStatusCode
		,RTTStartDate
		,RTTEndDate
		,DistrictNo
		,TrustNo
		,CasenoteNo
		,DistrictNoOrganisationCode
		,NHSNumber
		,NHSNumberStatusCode
		,PatientTitle
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,Postcode
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,CarerSupportIndicator
		,RegisteredGpCode
		,RegisteredPracticeCode
		,AttendanceNumber
		,ArrivalModeCode
		,AttendanceCategoryCode
		,AttendanceDisposalCode
		,IncidentLocationTypeCode
		,PatientGroupCode
		,SourceOfReferralCode
		,ArrivalDate
		,ArrivalTime
		,AgeOnArrival
		,InitialAssessmentTime
		,SeenForTreatmentTime
		,AttendanceConclusionTime
		,DepartureTime
		,CommissioningSerialNo
		,NHSServiceAgreementLineNo
		,ProviderReferenceNo
		,CommissionerReferenceNo
		,ProviderCode
		,CommissionerCode
		,StaffMemberCode
		,InvestigationCodeFirst
		,InvestigationCodeSecond
		,DiagnosisCodeFirst
		,DiagnosisCodeSecond
		,TreatmentCodeFirst
		,TreatmentCodeSecond
		,PASHRGCode
		,HRGVersionCode
		,PASDGVPCode
		,SiteCode
		,Created
		,Updated
		,ByWhom
		,InterfaceCode
		,PCTCode
		,ResidencePCTCode
		,InvestigationCodeList
		,TriageCategoryCode
		,PresentingProblem
		,PresentingProblemCode
		,ToXrayTime
		,FromXrayTime
		,ToSpecialtyTime
		,SeenBySpecialtyTime
		,EthnicCategoryCode
		,ReligionCode
		,ReferredToSpecialtyCode
		,DecisionToAdmitTime
		,DischargeDestinationCode
		,RegisteredTime
		,TransportRequestTime
		,TransportAvailableTime
		,AscribeLeftDeptTime
		,CDULeftDepartmentTime
		,PCDULeftDepartmentTime
		,TreatmentDateFirst
		,TreatmentDateSecond
		,SourceAttendanceDisposalCode
		,AmbulanceArrivalTime
		,AmbulanceCrewPRF
		,UnplannedReattend7Day
		,ArrivalTimeAdjusted
		,ClinicalAssessmentTime
		,LevelOfCareCode
		,EncounterBreachStatusCode
		,EncounterStartTimeOfDay
		,EncounterEndTimeOfDay
		,EncounterStartDate
		,EncounterEndDate
		,EncounterDurationMinutes
		,AgeCode
		,LeftWithoutBeingSeenCases
		,HRGCode
		,Reportable
		,CareGroup
		,EPMINo
		,DepartmentTypeCode
		,DepartureTimeAdjusted
		,CarePathwayAttendanceConclusionTime
		,CarePathwayDepartureTime
		,ContextCode
		,GpCodeAtAttendance
		,GpPracticeCodeAtAttendance
		,PostcodeAtAttendance
		,CCGCode
		,AlcoholLocation
		,AlcoholInPast12Hours
		,AttendanceResidenceCCGCode
		,ServiceID
		,Action
		,EncounterChecksum
	)
	select
		 *
		,EncounterChecksum =
			checksum
			(
			 EncounterRecno
			,SourceUniqueID
			,UniqueBookingReferenceNo
			,PathwayId
			,PathwayIdIssuerCode
			,RTTStatusCode
			,RTTStartDate
			,RTTEndDate
			,DistrictNo
			,TrustNo
			,CasenoteNo
			,DistrictNoOrganisationCode
			,NHSNumber
			,NHSNumberStatusCode
			,PatientTitle
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,Postcode
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,CarerSupportIndicator
			,RegisteredGpCode
			,RegisteredPracticeCode
			,AttendanceNumber
			,ArrivalModeCode
			,AttendanceCategoryCode
			,AttendanceDisposalCode
			,IncidentLocationTypeCode
			,PatientGroupCode
			,SourceOfReferralCode
			,ArrivalDate
			,ArrivalTime
			,AgeOnArrival
			,InitialAssessmentTime
			,SeenForTreatmentTime
			,AttendanceConclusionTime
			,DepartureTime
			,CommissioningSerialNo
			,NHSServiceAgreementLineNo
			,ProviderReferenceNo
			,CommissionerReferenceNo
			,ProviderCode
			,CommissionerCode
			,StaffMemberCode
			,InvestigationCodeFirst
			,InvestigationCodeSecond
			,DiagnosisCodeFirst
			,DiagnosisCodeSecond
			,TreatmentCodeFirst
			,TreatmentCodeSecond
			,PASHRGCode
			,HRGVersionCode
			,PASDGVPCode
			,SiteCode
			--,Created
			--,Updated
			--,ByWhom
			,InterfaceCode
			,PCTCode
			,ResidencePCTCode
			,InvestigationCodeList
			,TriageCategoryCode
			,PresentingProblem
			,PresentingProblemCode
			,ToXrayTime
			,FromXrayTime
			,ToSpecialtyTime
			,SeenBySpecialtyTime
			,EthnicCategoryCode
			,ReligionCode
			,ReferredToSpecialtyCode
			,DecisionToAdmitTime
			,DischargeDestinationCode
			,RegisteredTime
			,TransportRequestTime
			,TransportAvailableTime
			,AscribeLeftDeptTime
			,CDULeftDepartmentTime
			,PCDULeftDepartmentTime
			,TreatmentDateFirst
			,TreatmentDateSecond
			,SourceAttendanceDisposalCode
			,AmbulanceArrivalTime
			,AmbulanceCrewPRF
			,UnplannedReattend7Day
			,ArrivalTimeAdjusted
			,ClinicalAssessmentTime
			,LevelOfCareCode
			,EncounterBreachStatusCode
			,EncounterStartTimeOfDay
			,EncounterEndTimeOfDay
			,EncounterStartDate
			,EncounterEndDate
			,EncounterDurationMinutes
			,AgeCode
			,LeftWithoutBeingSeenCases
			,HRGCode
			,Reportable
			,CareGroup
			,EPMINo
			,DepartmentTypeCode
			,DepartureTimeAdjusted
			,CarePathwayAttendanceConclusionTime
			,CarePathwayDepartureTime
			,ContextCode
			,GpCodeAtAttendance
			,GpPracticeCodeAtAttendance
			,PostcodeAtAttendance
			,CCGCode
			,AlcoholLocation
			,AlcoholInPast12Hours
			,AttendanceResidenceCCGCode
			,ServiceID
			)
	from
		(
		select
			 EncounterRecno = cast(EncounterRecno as int)
			,SourceUniqueID = cast(SourceUniqueID as varchar(50))
			,UniqueBookingReferenceNo = cast(UniqueBookingReferenceNo as varchar(50))
			,PathwayId = cast(PathwayId as varchar(50))
			,PathwayIdIssuerCode = cast(PathwayIdIssuerCode as varchar(10))
			,RTTStatusCode = cast(RTTStatusCode as varchar(10))
			,RTTStartDate = cast(RTTStartDate as smalldatetime)
			,RTTEndDate = cast(RTTEndDate as smalldatetime)
			,DistrictNo = cast(DistrictNo as varchar(50))
			,TrustNo = cast(TrustNo as varchar(50))
			,CasenoteNo = cast(CasenoteNo as varchar(50))
			,DistrictNoOrganisationCode = cast(DistrictNoOrganisationCode as varchar(10))
			,NHSNumber = cast(NHSNumber as varchar(17))
			,NHSNumberStatusCode = cast(NHSNumberStatusCode as varchar(10))
			,PatientTitle = cast(PatientTitle as varchar(50))
			,PatientForename = cast(PatientForename as varchar(100))
			,PatientSurname = cast(PatientSurname as varchar(100))
			,PatientAddress1 = cast(PatientAddress1 as varchar(50))
			,PatientAddress2 = cast(PatientAddress2 as varchar(50))
			,PatientAddress3 = cast(PatientAddress3 as varchar(50))
			,PatientAddress4 = cast(PatientAddress4 as varchar(50))
			,Postcode = cast(Postcode as varchar(10))
			,DateOfBirth = cast(DateOfBirth as datetime)
			,DateOfDeath = cast(DateOfDeath as smalldatetime)
			,SexCode = cast(SexCode as varchar(10))
			,CarerSupportIndicator = cast(CarerSupportIndicator as bit)
			,RegisteredGpCode = cast(RegisteredGpCode as varchar(10))
			,RegisteredPracticeCode = cast(RegisteredPracticeCode as varchar(10))
			,AttendanceNumber = cast(AttendanceNumber as varchar(50))
			,ArrivalModeCode = cast(ArrivalModeCode as varchar(10))
			,AttendanceCategoryCode = cast(AttendanceCategoryCode as varchar(10))
			,AttendanceDisposalCode = cast(AttendanceDisposalCode as varchar(10))
			,IncidentLocationTypeCode = cast(IncidentLocationTypeCode as varchar(10))
			,PatientGroupCode = cast(PatientGroupCode as varchar(10))
			,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(10))
			,ArrivalDate = cast(ArrivalDate as smalldatetime)
			,ArrivalTime = cast(ArrivalTime as smalldatetime)
			,AgeOnArrival = cast(AgeOnArrival as tinyint)
			,InitialAssessmentTime = cast(InitialAssessmentTime as smalldatetime)
			,SeenForTreatmentTime = cast(SeenForTreatmentTime as smalldatetime)
			,AttendanceConclusionTime = cast(AttendanceConclusionTime as smalldatetime)
			,DepartureTime = cast(DepartureTime as smalldatetime)
			,CommissioningSerialNo = cast(CommissioningSerialNo as varchar(50))
			,NHSServiceAgreementLineNo = cast(NHSServiceAgreementLineNo as varchar(50))
			,ProviderReferenceNo = cast(ProviderReferenceNo as varchar(50))
			,CommissionerReferenceNo = cast(CommissionerReferenceNo as varchar(50))
			,ProviderCode = cast(ProviderCode as varchar(10))
			,CommissionerCode = cast(CommissionerCode as varchar(10))
			,StaffMemberCode = cast(StaffMemberCode as varchar(10))
			,InvestigationCodeFirst = cast(InvestigationCodeFirst as varchar(10))
			,InvestigationCodeSecond = cast(InvestigationCodeSecond as varchar(10))
			,DiagnosisCodeFirst = cast(DiagnosisCodeFirst as varchar(10))
			,DiagnosisCodeSecond = cast(DiagnosisCodeSecond as varchar(10))
			,TreatmentCodeFirst = cast(TreatmentCodeFirst as varchar(10))
			,TreatmentCodeSecond = cast(TreatmentCodeSecond as varchar(10))
			,PASHRGCode = cast(PASHRGCode as varchar(10))
			,HRGVersionCode = cast(HRGVersionCode as varchar(10))
			,PASDGVPCode = cast(PASDGVPCode as varchar(10))
			,SiteCode = cast(SiteCode as varchar(10))
			,Created = cast(Created as datetime)
			,Updated = cast(Updated as datetime)
			,ByWhom = cast(ByWhom as varchar(50))
			,InterfaceCode = cast(InterfaceCode as varchar(5))
			,PCTCode = cast(PCTCode as varchar(8))
			,ResidencePCTCode = cast(ResidencePCTCode as varchar(10))
			,InvestigationCodeList = cast(InvestigationCodeList as varchar(16))
			,TriageCategoryCode = cast(TriageCategoryCode as varchar(10))
			,PresentingProblem = cast(PresentingProblem as varchar(4000))
			,PresentingProblemCode = cast(PresentingProblemCode as varchar(10))
			,ToXrayTime = cast(ToXrayTime as smalldatetime)
			,FromXrayTime = cast(FromXrayTime as smalldatetime)
			,ToSpecialtyTime = cast(ToSpecialtyTime as smalldatetime)
			,SeenBySpecialtyTime = cast(SeenBySpecialtyTime as smalldatetime)
			,EthnicCategoryCode = cast(EthnicCategoryCode as varchar(10))
			,ReligionCode = cast(ReligionCode as varchar(10))
			,ReferredToSpecialtyCode = cast(ReferredToSpecialtyCode as varchar(10))
			,DecisionToAdmitTime = cast(DecisionToAdmitTime as smalldatetime)
			,DischargeDestinationCode = cast(DischargeDestinationCode as varchar(10))
			,RegisteredTime = cast(RegisteredTime as smalldatetime)
			,TransportRequestTime = cast(TransportRequestTime as smalldatetime)
			,TransportAvailableTime = cast(TransportAvailableTime as smalldatetime)
			,AscribeLeftDeptTime = cast(AscribeLeftDeptTime as smalldatetime)
			,CDULeftDepartmentTime = cast(CDULeftDepartmentTime as smalldatetime)
			,PCDULeftDepartmentTime = cast(PCDULeftDepartmentTime as smalldatetime)
			,TreatmentDateFirst = cast(TreatmentDateFirst as smalldatetime)
			,TreatmentDateSecond = cast(TreatmentDateSecond as smalldatetime)
			,SourceAttendanceDisposalCode = cast(SourceAttendanceDisposalCode as int)
			,AmbulanceArrivalTime = cast(AmbulanceArrivalTime as smalldatetime)
			,AmbulanceCrewPRF = cast(AmbulanceCrewPRF as varchar(30))
			,UnplannedReattend7Day = cast(UnplannedReattend7Day as int)
			,ArrivalTimeAdjusted = cast(ArrivalTimeAdjusted as smalldatetime)
			,ClinicalAssessmentTime = cast(ClinicalAssessmentTime as smalldatetime)
			,LevelOfCareCode = cast(LevelOfCareCode as varchar(10))
			,EncounterBreachStatusCode = cast(EncounterBreachStatusCode as varchar(1))
			,EncounterStartTimeOfDay = cast(EncounterStartTimeOfDay as int)
			,EncounterEndTimeOfDay = cast(EncounterEndTimeOfDay as int)
			,EncounterStartDate = cast(EncounterStartDate as date)
			,EncounterEndDate = cast(EncounterEndDate as date)
			,EncounterDurationMinutes = cast(EncounterDurationMinutes as int)
			,AgeCode = cast(AgeCode as varchar(27))
			,LeftWithoutBeingSeenCases = cast(LeftWithoutBeingSeenCases as int)
			,HRGCode = cast(HRGCode as varchar(10))
			,Reportable = cast(Reportable as bit)
			,CareGroup = cast(CareGroup as varchar(20))
			,EPMINo = cast(EPMINo as varchar(50))
			,DepartmentTypeCode = cast(DepartmentTypeCode as varchar(2))
			,DepartureTimeAdjusted = cast(DepartureTimeAdjusted as smalldatetime)
			,CarePathwayAttendanceConclusionTime = cast(CarePathwayAttendanceConclusionTime as smalldatetime)
			,CarePathwayDepartureTime = cast(CarePathwayDepartureTime as smalldatetime)
			,ContextCode = cast(ContextCode as varchar(10))
			,GpCodeAtAttendance = cast(GpCodeAtAttendance as varchar(8))
			,GpPracticeCodeAtAttendance = cast(GpPracticeCodeAtAttendance as varchar(8))
			,PostcodeAtAttendance = cast(PostcodeAtAttendance as varchar(8))
			,CCGCode = cast(CCGCode as varchar(8))

			,AlcoholLocation = cast(AlcoholLocation as varchar(80))
			,AlcoholInPast12Hours = cast(AlcoholInPast12Hours as varchar(10))

			,AttendanceResidenceCCGCode = cast(AttendanceResidenceCCGCode as varchar(10))
			,ServiceID = cast(ServiceID as int)
			,Action
		from
			(
	--Central
			select
				 AEEncounter.EncounterRecno
				,AEEncounter.SourceUniqueID
				,AEEncounter.UniqueBookingReferenceNo
				,AEEncounter.PathwayId
				,AEEncounter.PathwayIdIssuerCode
				,AEEncounter.RTTStatusCode
				,AEEncounter.RTTStartDate
				,AEEncounter.RTTEndDate
				,AEEncounter.DistrictNo
				,AEEncounter.TrustNo
				,AEEncounter.CasenoteNo
				,AEEncounter.DistrictNoOrganisationCode
				,AEEncounter.NHSNumber
				,NHSNumberStatusCode = AEEncounter.NHSNumberStatusId
				,AEEncounter.PatientTitle
				,AEEncounter.PatientForename
				,AEEncounter.PatientSurname
				,AEEncounter.PatientAddress1
				,AEEncounter.PatientAddress2
				,AEEncounter.PatientAddress3
				,AEEncounter.PatientAddress4
				,AEEncounter.Postcode
				,AEEncounter.DateOfBirth
				,AEEncounter.DateOfDeath
				,AEEncounter.SexCode
				,AEEncounter.CarerSupportIndicator
				,RegisteredGpCode = 
					EpisodicGpCode

				,RegisteredPracticeCode =
					EpisodicGpPracticeCode

				,AEEncounter.AttendanceNumber
				,AEEncounter.ArrivalModeCode
				,AEEncounter.AttendanceCategoryCode

				,AttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
					--coalesce(
					--	AEEncounter.AttendanceDisposalCode
					--	,'-1'
					--)

				,AEEncounter.IncidentLocationTypeCode
				,AEEncounter.PatientGroupCode
				,AEEncounter.SourceOfReferralCode
				,AEEncounter.ArrivalDate
				,AEEncounter.ArrivalTime
				,AEEncounter.AgeOnArrival
				,AEEncounter.InitialAssessmentTime
				,AEEncounter.SeenForTreatmentTime
				,AEEncounter.AttendanceConclusionTime
				,AEEncounter.DepartureTime
				,AEEncounter.CommissioningSerialNo
				,AEEncounter.NHSServiceAgreementLineNo
				,AEEncounter.ProviderReferenceNo
				,AEEncounter.CommissionerReferenceNo
				,AEEncounter.ProviderCode
				,AEEncounter.CommissionerCode
				,AEEncounter.StaffMemberCode
				,AEEncounter.InvestigationCodeFirst
				,AEEncounter.InvestigationCodeSecond
				,AEEncounter.DiagnosisCodeFirst
				,AEEncounter.DiagnosisCodeSecond
				,AEEncounter.TreatmentCodeFirst
				,AEEncounter.TreatmentCodeSecond
				,AEEncounter.PASHRGCode
				,AEEncounter.HRGVersionCode
				,AEEncounter.PASDGVPCode
				,AEEncounter.SiteCode
				,AEEncounter.Created
				,Updated = coalesce(AEEncounter.Updated, getdate())
				,AEEncounter.ByWhom
				,AEEncounter.InterfaceCode
				,AEEncounter.PCTCode
				,AEEncounter.ResidencePCTCode
				,AEEncounter.InvestigationCodeList
				,AEEncounter.TriageCategoryCode
				,AEEncounter.PresentingProblem
				,AEEncounter.PresentingProblemCode
				,AEEncounter.ToXrayTime
				,AEEncounter.FromXrayTime
				,AEEncounter.ToSpecialtyTime
				,AEEncounter.SeenBySpecialtyTime
				,AEEncounter.EthnicCategoryCode
				,AEEncounter.ReligionCode
				,AEEncounter.ReferredToSpecialtyCode
				,AEEncounter.DecisionToAdmitTime
				,AEEncounter.DischargeDestinationCode
				,AEEncounter.RegisteredTime
				,AEEncounter.TransportRequestTime
				,AEEncounter.TransportAvailableTime
				,AEEncounter.AscribeLeftDeptTime
				,AEEncounter.CDULeftDepartmentTime
				,AEEncounter.PCDULeftDepartmentTime
				,AEEncounter.TreatmentDateFirst
				,AEEncounter.TreatmentDateSecond
				,AEEncounter.SourceAttendanceDisposalCode
				,AEEncounter.AmbulanceArrivalTime
				,AEEncounter.AmbulanceCrewPRF

			-- determine which attendances follow a previous unplanned attendance within seven days
				,UnplannedReattend7Day =
					case
					when AEEncounter.AttendanceCategoryCode = 2
					then 0

					when exists
						(
						select
							1
						from
							[$(Warehouse)].AE.Encounter PreviousAEEncounter
						where
							PreviousAEEncounter.DistrictNo = AEEncounter.DistrictNo
						and	PreviousAEEncounter.SiteCode = AEEncounter.SiteCode
						and	PreviousAEEncounter.AttendanceCategoryCode != 2

						-- exclude children's division from the measurement of reattendance rate
						and	PreviousAEEncounter.SiteCode != 'RW3RC'

						and	cast(
								coalesce(
									 PreviousAEEncounter.PCDULeftDepartmentTime
									,PreviousAEEncounter.CDULeftDepartmentTime
									,PreviousAEEncounter.DepartureTime
									,PreviousAEEncounter.ArrivalTime
								)
								as date
							) between dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime) - 7 , 0) and AEEncounter.ArrivalTime

						and	coalesce(
								 PreviousAEEncounter.PCDULeftDepartmentTime
								,PreviousAEEncounter.CDULeftDepartmentTime
								,PreviousAEEncounter.DepartureTime
								,PreviousAEEncounter.ArrivalTime
							) < AEEncounter.ArrivalTime
						)
					then 1
					else 0
					end
					--UnplannedReattend.UnplannedReattend7Day

				,AEEncounter.ArrivalTimeAdjusted
				,AEEncounter.ClinicalAssessmentTime


				,LevelOfCareCode =
					case
					when
						AEEncounter.AttendanceDisposalCode = '01'
					then 2 --high
					when
						AEInvestigation.InvestigationDate is null
					and	AEEncounter.ToSpecialtyTime is null
					then 0 --low
					else 1 --medium
					end

				,EncounterBreachStatusCode = 
					case
					when AEEncounter.DepartureTimeAdjusted is null then 'X'
					when
						exists
						(
						select
							1
						from
							AE.Stage EncounterBreach
						where
							EncounterBreach.StageCode = 'INDEPARTMENTADJUSTEDNATIONAL'
						and
							datediff(
								 minute
								,AEEncounter.ArrivalTimeAdjusted
									,coalesce(
										 AEEncounter.PCDULeftDepartmentTime
										,AEEncounter.CDULeftDepartmentTime
										,AEEncounter.DepartureTimeAdjusted
								)
							)
							 > EncounterBreach.BreachValue
						)
					then 'B'
					else 'N'
					end

				--,EncounterStartTimeOfDay =
				--	coalesce(
				--		datediff(
				--			 minute
				--			,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
				--			,AEEncounter.ArrivalTime
				--		)
				--		,-1
				--	)

				,EncounterStartTimeOfDay =
					coalesce(
						datediff(
							 minute
							,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTimeAdjusted), 0)
							,AEEncounter.ArrivalTimeAdjusted
						)
						,-1
					)

				--,EncounterEndTimeOfDay =
				--	coalesce(
				--		datediff(
				--			 minute
				--			,dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
				--			,AEEncounter.DepartureTime
				--		)
				--		,-1
				--	)

				,EncounterEndTimeOfDay =
					coalesce(
						datediff(
							 minute
							,dateadd(
								 day
								,datediff(
									day
									,0
									,coalesce(
										 AEEncounter.PCDULeftDepartmentTime
										,AEEncounter.CDULeftDepartmentTime
										,AEEncounter.DepartureTimeAdjusted
									)
								)
								,0
							)
							,coalesce(
								 AEEncounter.PCDULeftDepartmentTime
								,AEEncounter.CDULeftDepartmentTime
								,AEEncounter.DepartureTimeAdjusted
							)
						)
						,-1
					)

				--,EncounterStartDate =
				--	dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)

				,EncounterStartDate =
					dateadd(day, datediff(day, 0, AEEncounter.ArrivalTimeAdjusted), 0)

				--,EncounterEndDate = 
				--	coalesce(
				--		 dateadd(day, datediff(day, 0, AEEncounter.PCDULeftDepartmentTime), 0)
				--		,dateadd(day, datediff(day, 0, AEEncounter.CDULeftDepartmentTime), 0)
				--		,dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
				--		,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
				--	)

				,EncounterEndDate = 
					coalesce(
						 dateadd(day, datediff(day, 0, AEEncounter.PCDULeftDepartmentTime), 0)
						,dateadd(day, datediff(day, 0, AEEncounter.CDULeftDepartmentTime), 0)
						,dateadd(day, datediff(day, 0, AEEncounter.DepartureTimeAdjusted), 0)
						,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTimeAdjusted), 0)
					)

				--,EncounterDurationMinutes =
				--	datediff(
				--		 minute
				--		,AEEncounter.ArrivalTime
				--		,AEEncounter.DepartureTime
				--	)

				,EncounterDurationMinutes =
					datediff(
						 minute
						,AEEncounter.ArrivalTimeAdjusted
						,coalesce(
							 AEEncounter.PCDULeftDepartmentTime
							,AEEncounter.CDULeftDepartmentTime
							,AEEncounter.DepartureTimeAdjusted
						)
					)

				,AgeCode =
					case
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) is null then 'Age Unknown'
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 0 then '00 Days'
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) = 1 then '01 Day'
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 1 and
					datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 28 then
						right('0' + Convert(Varchar,datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate)), 2) + ' Days'
					
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 28 and
					datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end
						 < 1 then 'Between 28 Days and 1 Month'
					when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end = 1
						then '01 Month'
					when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end > 1 and
					 datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end <= 23
					then
					right('0' + Convert(varchar,datediff(month, AEEncounter.DateOfBirth,AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end), 2) + ' Months'
					when datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
					(
					case 
					when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
					or
						(
							datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
						And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
						) then 1 else 0 end
					) > 99 then '99+'
					else right('0' + convert(varchar, datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
					(
					case 
					when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
					or
						(
							datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
						And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
						) then 1 else 0 end
					)), 2) + ' Years'

				end

				,LeftWithoutBeingSeenCases =
					case
					when
							(
								SourceAttendanceDisposalCode in ('10804' , '6193', '17369', '17368', '17759', '17901') -- GS 2011-06-02 change additional codes 17369 and 17368 added
							or																			   -- GS 2011-10-12 change additional codes '17759','17901'(both Left dept before being treated) added
								LookupBase.Lkp_Name in ( 'Did Not Wait' , 'Self Discharge') 
							)
						and	SeenForTreatmentTime is null
						and ClinicalAssessmentTime is null
					then 1
					else 0
					end
					
				--,HRG4AEEncounter.HRGCode
				,HRGCode = null

				,Reportable = coalesce(AEEncounter.Reportable , 1)
				,CareGroup = CareGroup.Reference
				,EPMINo = null

				,DepartmentTypeCode =
					case
					when
						(
							AEEncounter.ContextCode = 'CEN||ADAS'
						or	AEEncounter.ContextCode = 'TRA||ADAS'
						or	Site.NationalValueCode = 'RW3T1'
						or	
							(
								Site.NationalValueCode = 'RW3TR' -- conversion of TGH to type 3 following Trafford Service redesign
							and
								ArrivalTime >= '28 nov 2013 08:00:00'
							)			
						
						) then '03'
					when Site.NationalValueCode in ('RW3SM' , 'RW3RE' , 'RW3DH') then '02'
					else '01'
					end

				,AEEncounter.DepartureTimeAdjusted
				,AEEncounter.CarePathwayAttendanceConclusionTime
				,AEEncounter.CarePathwayDepartureTime
				,AEEncounter.ContextCode
				,AEEncounter.GpCodeAtAttendance
				,AEEncounter.GpPracticeCodeAtAttendance
				,AEEncounter.PostcodeAtAttendance
				,AEEncounter.CCGCode
				,AEEncounter.AlcoholLocation
				,AEEncounter.AlcoholInPast12Hours

				,AttendanceResidenceCCGCode = PostcodeAtAttendance.CCGCode

				,ServiceID =
					case
					when
						(
							AEEncounter.ContextCode = 'CEN||ADAS'
						or	AEEncounter.ContextCode = 'TRA||ADAS'
						or	Site.NationalValueCode = 'RW3T1'
						) then 3
					when	-- Assign Trafford UCC to 2 instead of 3. 04/12/2014 Paul Egan
						(
							Site.NationalValueCode = 'RW3TR' -- conversion of TGH to type 3 following Trafford Service redesign
						and
							ArrivalTime >= '28 nov 2013 08:00:00'
						) then 2
					when Site.NationalValueCode in ('RW3SM' , 'RW3RE' , 'RW3DH') then 2
					else 1
					end
					
				,Action

			from
				(
				select
					 EncounterRecno
					,Encounter.SourceUniqueID
					,Encounter.UniqueBookingReferenceNo
					,Encounter.PathwayId
					,Encounter.PathwayIdIssuerCode
					,Encounter.RTTStatusCode
					,Encounter.RTTStartDate
					,Encounter.RTTEndDate
					,Encounter.DistrictNo
					,Encounter.TrustNo
					,Encounter.CasenoteNo
					,Encounter.DistrictNoOrganisationCode

					,NHSNumber =
						replace(
							coalesce(
								Patient.NHSNumber
								,case
								when
									(
										Encounter.NHSNumber = '1234567899'  
									or	rtrim(Encounter.NHSNumber) = ''
									or Encounter.NHSNumber = 'REMOVE'
									or left(Encounter.NHSNumber,4) = 'SLF?' 
									or left(Encounter.NHSNumber,4) = 'MAN?'
									)
								then null
								else Encounter.NHSNumber
								end
							)
							,' '
							,''
						)

					,NHSNumberStatusId =
						coalesce(
							 Patient.NHSNumberStatusId
							,case
							when 
								(
									Encounter.NHSNumber = '1234567899'  
								or	Encounter.NHSNumber is null
								or	rtrim(Encounter.NHSNumber) = ''
								or Encounter.NHSNumber = 'REMOVE'
								or left(Encounter.NHSNumber,4) = 'SLF?'
								or left(Encounter.NHSNumber,4) = 'MAN?' 
								)			
							then 'RT'

							when 
								Encounter.NHSNumber is not null
							then 'NP'

							else coalesce(
									 Encounter.NHSNumberStatusId
									,'RT'
								)
							end
							,'RT'
						)
					,Encounter.PatientTitle
					,Encounter.PatientForename
					,Encounter.PatientSurname
					,Encounter.PatientAddress1
					,Encounter.PatientAddress2
					,Encounter.PatientAddress3
					,Encounter.PatientAddress4
					,Encounter.Postcode
					,Encounter.DateOfBirth
					,Encounter.DateOfDeath
					,Encounter.SexCode
					,Encounter.CarerSupportIndicator
					,Encounter.RegisteredGpCode
					,Encounter.RegisteredGpPracticeCode
					,Encounter.AttendanceNumber
					,Encounter.ArrivalModeCode
					,Encounter.AttendanceCategoryCode
					,Encounter.AttendanceDisposalCode
					,Encounter.IncidentLocationTypeCode
					,Encounter.PatientGroupCode
					,Encounter.SourceOfReferralCode
					,Encounter.ArrivalDate
					,Encounter.ArrivalTime
					,Encounter.AgeOnArrival
					,Encounter.InitialAssessmentTime
					,Encounter.SeenForTreatmentTime
					,Encounter.AttendanceConclusionTime
					,Encounter.DepartureTime
					,Encounter.CommissioningSerialNo
					,Encounter.NHSServiceAgreementLineNo
					,Encounter.ProviderReferenceNo
					,Encounter.CommissionerReferenceNo
					,Encounter.ProviderCode
					,Encounter.CommissionerCode
					,Encounter.StaffMemberCode
					,Encounter.InvestigationCodeFirst
					,Encounter.InvestigationCodeSecond
					,Encounter.DiagnosisCodeFirst
					,Encounter.DiagnosisCodeSecond
					,Encounter.TreatmentCodeFirst
					,Encounter.TreatmentCodeSecond
					,Encounter.PASHRGCode
					,Encounter.HRGVersionCode
					,Encounter.PASDGVPCode
					,Encounter.SiteCode
					,Encounter.Created
					,Encounter.Updated
					,Encounter.ByWhom
					,Encounter.InterfaceCode
					,Encounter.PCTCode
					,Encounter.ResidencePCTCode
					,Encounter.InvestigationCodeList
					,Encounter.TriageCategoryCode
					,Encounter.PresentingProblem
					,PresentingProblemCode =
						
							case 
								when Encounter.PresentingProblemCode is null then '-1'
								else
								Encounter.PresentingProblemCode
							end

					,Encounter.ToXrayTime
					,Encounter.FromXrayTime
					,Encounter.ToSpecialtyTime
					,Encounter.SeenBySpecialtyTime
					,Encounter.EthnicCategoryCode
					,Encounter.ReligionCode
					,Encounter.ReferredToSpecialtyCode
					,Encounter.DecisionToAdmitTime
					,Encounter.DischargeDestinationCode
					,Encounter.RegisteredTime
					,Encounter.TransportRequestTime
					,Encounter.TransportAvailableTime
					,Encounter.AscribeLeftDeptTime
					,Encounter.CDULeftDepartmentTime
					,Encounter.PCDULeftDepartmentTime
					,Encounter.TreatmentDateFirst
					,Encounter.TreatmentDateSecond
					,Encounter.SourceAttendanceDisposalCode
					,Encounter.AmbulanceArrivalTime
					,Encounter.AmbulanceCrewPRF
					,Encounter.UnplannedReattend7Day
					,Encounter.ArrivalTimeAdjusted
					,Encounter.ClinicalAssessmentTime
					,Encounter.Reportable
					,Encounter.SourceCareGroupCode
					,EpisodicGpCode = 
						coalesce(
							 CASE WHEN EpisodicGpAtArrival.GpCode = 'G9999981' THEN NULL ELSE EpisodicGpAtArrival.GpCode END
							,Encounter.RegisteredGpCode
							,'G9999998'
						)
					,EpisodicGpPracticeCode = 
						coalesce(
							 CASE WHEN EpisodicGpAtArrival.GpPracticeCode = 'V81998' THEN NULL ELSE EpisodicGpAtArrival.GpPracticeCode END
							,Encounter.RegisteredGpPracticeCode
							,'V81999'
						)
					,Encounter.CarePathwayAttendanceConclusionTime
					,Encounter.CarePathwayDepartureTime
					,ContextCode =
						case
						when Encounter.SiteCode = 'RW3TR' and Encounter.InterfaceCode = 'ADAS' then 'TRA||ADAS'
						else
							'CEN||' + 
							case
							when Encounter.InterfaceCode in ('INFO', 'INQ') then 'PAS'
							else Encounter.InterfaceCode
							end
						end

					,DepartureTimeAdjusted = 
						case
						when
								Encounter.AttendanceDisposalCode in ('02' , '03') 
							and Encounter.AttendanceCategoryCode = 1  
							and Encounter.AttendanceConclusionTime < Encounter.DepartureTime
								then
									coalesce(
										 Encounter.CarePathwayAttendanceConclusionTime
										,Encounter.AttendanceConclusionTime
										,Encounter.CarePathwayDepartureTime
										,Encounter.DepartureTime
									)
						else
							coalesce(
								 Encounter.CarePathwayDepartureTime
								,Encounter.DepartureTime
							)
						end

					,GpCodeAtAttendance
					,GpPracticeCodeAtAttendance
					,PostcodeAtAttendance
					,CCGCode
					,AlcoholLocation
					,AlcoholInPast12Hours

					,Action = case when Encounter.Updated is null then 'INSERT' else 'UPDATE' end
					
				from
					[$(Warehouse)].AE.Encounter

				left outer join [$(Warehouse)].PAS.Patient
					on	Patient.DistrictNo = Encounter.DistrictNo

				left outer join  [$(Warehouse)].PAS.PatientGp EpisodicGpAtArrival
					on EpisodicGpAtArrival.SourcePatientNo = Patient.SourcePatientNo
				and Encounter.ArrivalDate between EpisodicGpAtArrival.EffectiveFromDate and isnull(EpisodicGpAtArrival.EffectiveToDate,getdate())

				where
					isnull(Encounter.Updated, Encounter.Created) > @LastLoadTime

				) AEEncounter

			left join [$(Warehouse)].AE.Investigation AEInvestigation
			on	AEInvestigation.AESourceUniqueID = AEEncounter.SourceUniqueID
			and	AEInvestigation.SequenceNo = 1


			--left join [$(Warehouse)].AE.HRG4Encounter HRG4AEEncounter
			--on	HRG4AEEncounter.EncounterRecno = AEEncounter.EncounterRecno

			left join [$(Warehouse)].AE.LookupBase
			on	LookupBase.Lkp_ID = AEEncounter.SourceAttendanceDisposalCode

			LEFT JOIN [$(Warehouse)].AE.Reference CareGroup
			ON  CareGroup.ReferenceCode = AEEncounter.SourceCareGroupCode
			AND CareGroup.ReferenceParentCode = '5713'

			--SITE	Site

			left join WH.Member Site
			on	Site.SourceValueCode = coalesce(cast(AEEncounter.SiteCode as varchar), '-1')
			and	Site.AttributeCode = 'SITE'
			and	Site.SourceContextCode = AEEncounter.ContextCode

			left join [$(Organisation)].dbo.CCGPostcode PostcodeAtAttendance
			on	PostcodeAtAttendance.Postcode =
					case
					when len(AEEncounter.PostcodeAtAttendance) = 8 then AEEncounter.PostcodeAtAttendance
					else left(AEEncounter.PostcodeAtAttendance, 3) + ' ' + right(AEEncounter.PostcodeAtAttendance, 4) 
					end

			union all

	--Trafford
			select
				 EncounterRecno = AEEncounter.EncounterRecno
				,AEEncounter.SourceUniqueID
				,AEEncounter.UniqueBookingReferenceNo
				,AEEncounter.PathwayId
				,AEEncounter.PathwayIdIssuerCode
				,AEEncounter.RTTStatusCode
				,AEEncounter.RTTStartDate
				,AEEncounter.RTTEndDate
				,AEEncounter.DistrictNo
				,AEEncounter.TrustNo
				,AEEncounter.CasenoteNo
				,AEEncounter.DistrictNoOrganisationCode
				,AEEncounter.NHSNumber
				,NHSNumberStatusCode = AEEncounter.NHSNumberStatusId
				,AEEncounter.PatientTitle
				,AEEncounter.PatientForename
				,AEEncounter.PatientSurname
				,AEEncounter.PatientAddress1
				,AEEncounter.PatientAddress2
				,AEEncounter.PatientAddress3
				,AEEncounter.PatientAddress4
				,AEEncounter.Postcode
				,AEEncounter.DateOfBirth
				,AEEncounter.DateOfDeath
				,AEEncounter.SexCode
				,AEEncounter.CarerSupportIndicator
				,RegisteredGpCode =
					coalesce(AEEncounter.RegisteredGpCode,	'G9999998')

				,RegisteredPracticeCode =
					coalesce(AEEncounter.RegisteredGpPracticeCode, 'V81999')

				,AEEncounter.AttendanceNumber
				,AEEncounter.ArrivalModeCode
				,AEEncounter.AttendanceCategoryCode

				,AttendanceDisposalCode =
					coalesce(
						AEEncounter.AttendanceDisposalCode
						,'-1'
					)

				,AEEncounter.IncidentLocationTypeCode
				,AEEncounter.PatientGroupCode
				,AEEncounter.SourceOfReferralCode
				,AEEncounter.ArrivalDate
				,AEEncounter.ArrivalTime
				,AEEncounter.AgeOnArrival
				,AEEncounter.InitialAssessmentTime
				,AEEncounter.SeenForTreatmentTime
				,AEEncounter.AttendanceConclusionTime
				,AEEncounter.DepartureTime
				,AEEncounter.CommissioningSerialNo
				,AEEncounter.NHSServiceAgreementLineNo
				,AEEncounter.ProviderReferenceNo
				,AEEncounter.CommissionerReferenceNo
				,AEEncounter.ProviderCode
				,AEEncounter.CommissionerCode
				,AEEncounter.StaffMemberCode
				,AEEncounter.InvestigationCodeFirst
				,AEEncounter.InvestigationCodeSecond
				,AEEncounter.DiagnosisCodeFirst
				,AEEncounter.DiagnosisCodeSecond
				,AEEncounter.TreatmentCodeFirst
				,AEEncounter.TreatmentCodeSecond
				,AEEncounter.PASHRGCode
				,AEEncounter.HRGVersionCode
				,AEEncounter.PASDGVPCode
				,AEEncounter.SiteCode
				,AEEncounter.Created
				,AEEncounter.Updated
				,AEEncounter.ByWhom
				,AEEncounter.InterfaceCode
				,AEEncounter.PCTCode
				,AEEncounter.ResidencePCTCode
				,AEEncounter.InvestigationCodeList
				,AEEncounter.TriageCategoryCode
				,AEEncounter.PresentingProblem
				,PresentingProblemCode = '-1'

				,AEEncounter.ToXrayTime
				,AEEncounter.FromXrayTime
				,AEEncounter.ToSpecialtyTime
				,AEEncounter.SeenBySpecialtyTime
				,AEEncounter.EthnicCategoryCode
				,AEEncounter.ReligionCode
				,ReferredToSpecialtyCode =
					coalesce(
						 AEEncounter.ReferredToSpecialtyCode
						,0
					)
				,AEEncounter.DecisionToAdmitTime
				,DischargeDestinationCode = null
				,RegisteredTime = null
				,TransportRequestTime = null
				,TransportAvailableTime = null
				,AscribeLeftDeptTime = null
				,CDULeftDepartmentTime = null
				,PCDULeftDepartmentTime = null
				,TreatmentDateFirst = null
				,TreatmentDateSecond = null
				,SourceAttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
				,AmbulanceArrivalTime = null
				,AmbulanceCrewPRF = null

				,UnplannedReattend7Day =
					UnplannedReattend.UnplannedReattend7Day

				,ArrivalTimeAdjusted =
					AEEncounter.ArrivalTime

				,ClinicalAssessmentTime = null


				,LevelOfCareCode =
					case
					when
						AEEncounter.AttendanceDisposalCode = '01'
					then 2 --high
					when
						AEInvestigation.InvestigationDate is null
					and	AEEncounter.ToSpecialtyTime is null
					then 0 --low
					else 1 --medium
					end

				,EncounterBreachStatusCode = 
					case
					when AEEncounter.DepartureTime is null then 'X'
					when EncounterBreach.BreachValue is null
					then 'N'
					else 'B'
					end

				,EncounterStartTimeOfDay =
					coalesce(
						datediff(
							 minute
							,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
							,AEEncounter.ArrivalTime
						)
						,-1
					)

				--,EncounterEndTimeOfDay =
				--	coalesce(
				--		datediff(
				--			 minute
				--			,dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
				--			,AEEncounter.DepartureTime
				--		)
				--		,-1
				--	)

				,EncounterEndTimeOfDay =
					coalesce(
						datediff(
							 minute
							,dateadd(day, datediff(day, 0, AEEncounter.DepartureTimeAdjusted), 0)
							,AEEncounter.DepartureTimeAdjusted
						)
						,-1
					)

				,EncounterStartDate =
					dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)

				--,EncounterEndDate = 
				--	coalesce(
				--		 dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
				--		,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
				--	)

				,EncounterEndDate = 
					coalesce(
						 dateadd(day, datediff(day, 0, AEEncounter.DepartureTimeAdjusted), 0)
						,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
					)

				--,EncounterDurationMinutes =
				--	datediff(
				--		 minute
				--		,AEEncounter.ArrivalTime
				--		,AEEncounter.DepartureTime
				--	)

				,EncounterDurationMinutes =
					datediff(
						 minute
						,AEEncounter.ArrivalTime
						,AEEncounter.DepartureTimeAdjusted
					)

				,AgeCode =
					case
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) is null then 'Age Unknown'
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 0 then '00 Days'
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) = 1 then '01 Day'
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 1 and
					datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 28 then
						right('0' + Convert(Varchar,datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate)), 2) + ' Days'
					
					when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 28 and
					datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end
						 < 1 then 'Between 28 Days and 1 Month'
					when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end = 1
						then '01 Month'
					when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end > 1 and
					 datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end <= 23
					then
					right('0' + Convert(varchar,datediff(month, AEEncounter.DateOfBirth,AEEncounter.ArrivalDate) -
					case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end), 2) + ' Months'
					when datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
					(
					case 
					when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
					or
						(
							datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
						And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
						) then 1 else 0 end
					) > 99 then '99+'
					else right('0' + convert(varchar, datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
					(
					case 
					when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
					or
						(
							datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
						And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
						) then 1 else 0 end
					)), 2) + ' Years'

				end

				,LeftWithoutBeingSeenCases =
					case
					when
						AttendanceDisposalCode in ('138' , '4917', '5082') 
					and	SeenForTreatmentTime is null
					then 1
					else 0
					end

				,HRG4AEEncounter.HRGCode

				,Reportable = 1 --coalesce(AEEncounter.Reportable , 1)
				--,CareGroup = CareGroup.Reference
				,CareGroup = null
				,AEEncounter.EPMINo


				,DepartmentTypeCode =
					case
					when
						(
							AEEncounter.ContextCode = 'CEN||ADAS'
						or	AEEncounter.ContextCode = 'TRA||ADAS'
						or	Site.NationalValueCode = 'RW3T1'
						or	
							(
								Site.NationalValueCode = 'RW3TR' -- conversion of TGH to type 3 following Trafford Service redesign
							and
								ArrivalTime >= '28 nov 2013 08:00:00'
							)			
						
						) then '03'
					when Site.NationalValueCode in ('RW3SM' , 'RW3RE' , 'RW3DH') then '02'
					else '01'
					end

				,AEEncounter.DepartureTimeAdjusted
				,CarePathwayAttendanceConclusionTime = null
				,CarePathwayDepartureTime = null
				,AEEncounter.ContextCode
				,GpCodeAtAttendance = AEEncounter.RegisteredGpCode
				,GpPracticeCodeAtAttendance = AEEncounter.RegisteredGpPracticeCode
				,PostcodeAtAttendance = AEEncounter.Postcode

				,CCGCode = 
					coalesce(
						 CCGPractice.ParentOrganisationCode
						,CCGPostcode.CCGCode
						,'02A'
						)

				,AlcoholLocation = null
				,AlcoholInPast12Hours = null

				,AttendanceResidenceCCGCode = CCGPostcode.CCGCode

				,ServiceID =
					case
					when
						(
							AEEncounter.ContextCode = 'CEN||ADAS'
						or	AEEncounter.ContextCode = 'TRA||ADAS'
						or	Site.NationalValueCode = 'RW3T1'
						) then 3
					when	-- Assign Trafford UCC to 2 instead of 3. 04/12/2014 Paul Egan
						(
							Site.NationalValueCode = 'RW3TR' -- conversion of TGH to type 3 following Trafford Service redesign
						and
							ArrivalTime >= '28 nov 2013 08:00:00'
						) then 2
					when Site.NationalValueCode in ('RW3SM' , 'RW3RE' , 'RW3DH') then 2
					else 1
					end
					
				,Action

			from
				(
				select
					 * 
					,DepartureTimeAdjusted = 
						coalesce(
							 Encounter.AttendanceConclusionTime
							,Encounter.DepartureTime
						)
					,ContextCode = 'TRA||' + Encounter.InterfaceCode

					,Action = case when Encounter.Updated is null then 'INSERT' else 'UPDATE' end
					
				from
					[$(TraffordWarehouse)].dbo.AEEncounter Encounter
				where
					isnull(Encounter.Updated, Encounter.Created) > @LastLoadTime
				) AEEncounter

			left join AE.Stage EncounterBreach
			on	EncounterBreach.StageCode = 'INDEPARTMENTADJUSTEDNATIONAL'
			--and
			--	datediff(
			--		 minute
			--		,AEEncounter.ArrivalTime
			--		,AEEncounter.DepartureTime
			--	)
			--	 > EncounterBreach.BreachValue
			and
				datediff(
					 minute
					,AEEncounter.ArrivalTime
					,AEEncounter.DepartureTimeAdjusted
				)
				 > EncounterBreach.BreachValue


			left join [$(TraffordWarehouse)].dbo.AEInvestigation AEInvestigation
			on	AEInvestigation.AESourceUniqueID = AEEncounter.SourceUniqueID
			and	AEInvestigation.SequenceNo = 1

			left join [$(TraffordWarehouse)].dbo.HRG4AEEncounter
			on	HRG4AEEncounter.EncounterRecno = AEEncounter.EncounterRecno

			-- determine which unplanned attendances follow a previous unplanned attendance within seven days
			left join (
				select
					 SourceUniqueID
					,UnplannedReattend7Day = 1
				from
					[$(TraffordWarehouse)].dbo.AEEncounter
				where
					isnull(AEEncounter.AttendanceDetails,'New Episode') != 'Planned return'
				and	AEEncounter.AttendanceCategoryCode != 2
				and	exists
						(
						select
							1
						from
							[$(TraffordWarehouse)].dbo.AEEncounter PreviousAEEncounter
						where
							PreviousAEEncounter.DistrictNo = AEEncounter.DistrictNo
						and	PreviousAEEncounter.SiteCode = AEEncounter.SiteCode
						and	coalesce(
								 dateadd(day, datediff(day, 0, PreviousAEEncounter.DepartureTime), 0)
								,dateadd(day, datediff(day, 0, PreviousAEEncounter.ArrivalTime), 0)
							) between dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime) - 7 , 0) and AEEncounter.ArrivalTime
						and	coalesce(
								 PreviousAEEncounter.DepartureTime
								,PreviousAEEncounter.ArrivalTime
							) < AEEncounter.ArrivalTime
						and	isnull(AEEncounter.AttendanceDetails,'New Episode') != 'Planned return'
						and	PreviousAEEncounter.AttendanceCategoryCode != 2
						)
				) UnplannedReattend

			on	UnplannedReattend.SourceUniqueID = AEEncounter.SourceUniqueID


			left join [$(Warehouse)].AE.LookupBase
			on	LookupBase.Lkp_ID = AEEncounter.AttendanceDisposalCode


			--SITE	Site

			left join WH.Member Site
			on	Site.SourceValueCode = coalesce(cast(AEEncounter.SiteCode as varchar), '-1')
			and	Site.AttributeCode = 'SITE'
			and	Site.SourceContextCode = AEEncounter.ContextCode

			left join [$(Organisation)].dbo.CCGPractice CCGPractice
			on CCGPractice.OrganisationCode = AEEncounter.RegisteredGpPracticeCode


			left join [$(Organisation)].dbo.CCGPostcode CCGPostcode
			on	CCGPostcode.Postcode = 
				case
				when datalength(rtrim(ltrim(AEEncounter.Postcode))) = 6 then left(AEEncounter.Postcode, 2) + '   ' + right(AEEncounter.Postcode, 3)
				when datalength(rtrim(ltrim(AEEncounter.Postcode))) = 7 then left(AEEncounter.Postcode, 3) + '  ' + right(AEEncounter.Postcode, 3)
				else AEEncounter.Postcode
				end	

			) Encounter
		) Encounter


--now pick up deletes
	insert
	into
		ETL.TLoadAEBaseEncounter
	(
		EncounterRecno
		,ContextCode
		,Action
	)
	select
		Activity.EncounterRecno
		,Activity.ContextCode
		,Action = 'DELETE'
	from
		AE.BaseEncounter Activity
	where
		Activity.ContextCode in
			(
			'CEN||ADAS'
			,'CEN||PAS'
			,'CEN||SYM'
			,'TRA||ADAS'

			)

	and	not exists
		(
		select
			1
		from
			[$(Warehouse)].AE.Encounter Encounter
		where
			Encounter.EncounterRecno = Activity.EncounterRecno
		)
		
	union all
	
	select
		Activity.EncounterRecno
		,Activity.ContextCode
		,Action = 'DELETE'
	from
		AE.BaseEncounter Activity
	where
		Activity.ContextCode in
			(
			'TRA||SYM'
			)
	and	not exists
		(
		select
			1
		from
			[$(TraffordWarehouse)].dbo.AEEncounter Encounter
		where
			Encounter.EncounterRecno = Activity.EncounterRecno
		)

END TRY

BEGIN
    CATCH
	
	set @ReturnValue = @@ERROR

    DECLARE @ErrorSeverity INT,
            @ErrorNumber   INT,
            @ErrorMessage nvarchar(4000),
            @ErrorState INT,
            @ErrorLine  INT,
            @ErrorProc nvarchar(200)
            -- Grab error information from SQL functions
    SET @ErrorSeverity = ERROR_SEVERITY()
    SET @ErrorNumber   = ERROR_NUMBER()
    SET @ErrorMessage  = ERROR_MESSAGE()
    SET @ErrorState    = ERROR_STATE()
    SET @ErrorLine     = ERROR_LINE()
    SET @ErrorProc     = ERROR_PROCEDURE()
    SET @ErrorMessage  = 'An error occurred.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
    -- Not all errors generate an error state, to set to 1 if it's zero
    IF @ErrorState  = 0
    SET @ErrorState = 1
    -- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
    IF @@TRANCOUNT > 0
    BEGIN
            --print 'Rollback transaction'
            ROLLBACK TRANSACTION
    END
    RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
END CATCH


RETURN @ReturnValue



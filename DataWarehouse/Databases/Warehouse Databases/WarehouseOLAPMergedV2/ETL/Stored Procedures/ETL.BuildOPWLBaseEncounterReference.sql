﻿CREATE procedure [ETL].[BuildOPWLBaseEncounterReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				OPWL.BaseEncounterReference
			where
				BaseEncounterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	OPWL.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		OPWL.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
	)
and	BaseEncounterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	OPWL.BaseEncounterReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,AgeID = Age.AgeID
		,SexID = Sex.SourceSexID
		,EthnicOriginID = EthnicCategory.SourceEthnicCategoryID
		,MaritalStatusID = MaritalStatus.SourceMaritalStatusID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,PASSpecialtyID = PASSpecialty.SourceSpecialtyID
		,RTTSpecialtyID = RTTSpecialty.SourceSpecialtyID
		,RTTCurrentStatusID = RTTCurrentStatus.SourceRTTStatusID
		,ConsultantID = Consultant.SourceConsultantID
		,SiteID = Site.SourceSiteID
		,WaitingListID = WaitingList.SourceWaitingListID
		,SourceOfReferralID = SourceOfReferral.SourceSourceOfReferralID
		,FirstAttendanceID = FirstAttendance.SourceFirstAttendanceID

		,CensusDateID =
			coalesce(
				 CensusDate.DateID
				,NullDate.DateID
			)

		,AppointmentDateID =
			coalesce(
				 AppointmentDate.DateID

				,case
				when Encounter.AppointmentDate is null
				then NullDate.DateID

				when Encounter.AppointmentDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,WaitTypeID = WaitType.WaitTypeID

		,DurationID = Duration.DurationID
		,AppointmentDateDurationID = AppointmentDateDuration.DurationID

		,WaitStatusID = WaitStatus.WaitStatusID
		,ReligionID = Religion.SourceReligionID

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		OPWL.BaseEncounter Encounter

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	inner join WH.Age
	on	Age.AgeCode = Encounter.AgeCode


	inner join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.SexCode, '-1') as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	inner join WH.EthnicCategory EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryCode = cast(coalesce(Encounter.EthnicOriginCode, '-1') as varchar)
	and	EthnicCategory.SourceContextCode = Encounter.ContextCode

	inner join WH.MaritalStatus MaritalStatus
	on	MaritalStatus.SourceMaritalStatusCode = cast(coalesce(Encounter.MaritalStatusCode, '-1') as varchar)
	and	MaritalStatus.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(Encounter.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty PASSpecialty
	on	PASSpecialty.SourceSpecialtyCode = coalesce(Encounter.PASSpecialtyCode, '-1')
	and	PASSpecialty.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty RTTSpecialty
	on	RTTSpecialty.SourceSpecialtyCode = coalesce(Encounter.RTTSpecialtyCode, '-1')
	and	RTTSpecialty.SourceContextCode = Encounter.ContextCode

	inner join WH.RTTStatus RTTCurrentStatus
	on	RTTCurrentStatus.SourceRTTStatusCode = coalesce(Encounter.RTTCurrentStatusCode, '-1')
	and	RTTCurrentStatus.SourceContextCode = Encounter.ContextCode

	inner join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = cast(coalesce(Encounter.ConsultantCode, '-1') as varchar)
	and	Consultant.SourceContextCode = Encounter.ContextCode

	inner join WH.Site
	on	Site.SourceSiteCode = coalesce(Encounter.SiteCode, '-1')
	and	Site.SourceContextCode = Encounter.ContextCode

	--losing records here
	inner join OPWL.WaitingList WaitingList
	on	WaitingList.SourceWaitingListCode = coalesce(Encounter.WaitingListCode, '-1')
	and	WaitingList.SourceContextCode = Encounter.ContextCode

	inner join OP.SourceOfReferral SourceOfReferral
	on	SourceOfReferral.SourceSourceOfReferralCode = coalesce(Encounter.SourceOfReferralCode, '-1')
	and	SourceOfReferral.SourceContextCode = Encounter.ContextCode

	inner join OP.FirstAttendance FirstAttendance
	on	FirstAttendance.SourceFirstAttendanceCode = coalesce(Encounter.FirstAttendanceCode, '-1')
	and	FirstAttendance.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar CensusDate
	on	CensusDate.TheDate = Encounter.CensusDate

	left join WH.Calendar AppointmentDate
	on	AppointmentDate.TheDate = Encounter.AppointmentDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	inner join OPWL.WaitType WaitType
	on	WaitType.WaitTypeID = coalesce(Encounter.WaitTypeCode, 10)

	inner join WH.Duration Duration
	on	Duration.DurationCode = coalesce(Encounter.DurationCode, 'NA')

	inner join WH.Duration AppointmentDateDuration
	on	AppointmentDateDuration.DurationCode = coalesce(Encounter.DurationAtAppointmentDateCode, 'NA')

	inner join WH.WaitStatus
	on	WaitStatus.WaitStatusCode = Encounter.StatusCode

	inner join WH.Religion Religion
	on	Religion.SourceReligionCode = coalesce(Encounter.ReligionCode, '-1')
	and	Religion.SourceContextCode = Encounter.ContextCode

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,AgeID
			,SexID
			,EthnicOriginID
			,MaritalStatusID
			,SpecialtyID
			,PASSpecialtyID
			,RTTSpecialtyID
			,RTTCurrentStatusID
			,ConsultantID
			,SiteID
			,WaitingListID
			,SourceOfReferralID
			,FirstAttendanceID
			,CensusDateID
			,AppointmentDateID
			,WaitTypeID
			,DurationID
			,AppointmentDateDurationID
			,WaitStatusID
			,ReligionID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.AgeID
			,source.SexID
			,source.EthnicOriginID
			,source.MaritalStatusID
			,source.SpecialtyID
			,source.PASSpecialtyID
			,source.RTTSpecialtyID
			,source.RTTCurrentStatusID
			,source.ConsultantID
			,source.SiteID
			,source.WaitingListID
			,source.SourceOfReferralID
			,source.FirstAttendanceID
			,source.CensusDateID
			,source.AppointmentDateID
			,source.WaitTypeID
			,source.DurationID
			,source.AppointmentDateDurationID
			,source.WaitStatusID
			,source.ReligionID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.AgeID = source.AgeID
			,target.SexID = source.SexID
			,target.EthnicOriginID = source.EthnicOriginID
			,target.MaritalStatusID = source.MaritalStatusID
			,target.SpecialtyID = source.SpecialtyID
			,target.PASSpecialtyID = source.PASSpecialtyID
			,target.RTTSpecialtyID = source.RTTSpecialtyID
			,target.RTTCurrentStatusID = source.RTTCurrentStatusID
			,target.ConsultantID = source.ConsultantID
			,target.SiteID = source.SiteID
			,target.WaitingListID = source.WaitingListID
			,target.SourceOfReferralID = source.SourceOfReferralID
			,target.FirstAttendanceID = source.FirstAttendanceID
			,target.CensusDateID = source.CensusDateID
			,target.AppointmentDateID = source.AppointmentDateID
			,target.WaitTypeID = source.WaitTypeID
			,target.DurationID = source.DurationID
			,target.AppointmentDateDurationID = source.AppointmentDateDurationID
			,target.WaitStatusID = source.WaitStatusID
			,target.ReligionID = source.ReligionID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

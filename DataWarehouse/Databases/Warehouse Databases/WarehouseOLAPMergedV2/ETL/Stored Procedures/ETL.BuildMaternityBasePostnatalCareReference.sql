﻿

create procedure [ETL].[BuildMaternityBasePostnatalCareReference] 
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Maternity.BasePostnatalCareReference
			where
				BasePostnatalCareReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Maternity.BasePostnatalCareReference
where
	not exists
	(
	select
		1
	from
		Maternity.BasePostnatalCare
	where
		BasePostnatalCare.MergePostnatalCareRecno = BasePostnatalCareReference.MergePostnatalCareRecno
	)
and	BasePostnatalCareReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Maternity.BasePostnatalCareReference target
using
	(
	select
		BasePostnatalCare.MergePostnatalCareRecno
		,ContextID = Context.ContextID
		,DischargedToHealthVisitorDateID =
			coalesce(
				 DischargedToHealthVisitorDate.DateID

				,case
				when BasePostnatalCare.DischargedToHealthVisitorDate is null
				then NullDate.DateID

				when BasePostnatalCare.DischargedToHealthVisitorDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,BasePostnatalCare.Created
		,BasePostnatalCare.Updated
		,BasePostnatalCare.ByWhom
	from
		Maternity.BasePostnatalCare

	inner join WH.Context
	on	Context.ContextCode = BasePostnatalCare.ContextCode

	left join WH.Calendar DischargedToHealthVisitorDate
	on	DischargedToHealthVisitorDate.TheDate = BasePostnatalCare.DischargedToHealthVisitorDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BasePostnatalCare.Updated > @LastUpdated
	and	BasePostnatalCare.ContextCode = @ContextCode

	) source
	on	source.MergePostnatalCareRecno = target.MergePostnatalCareRecno

	when not matched
	then
		insert
			(
			MergePostnatalCareRecno
			,ContextID
			,DischargedToHealthVisitorDateID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergePostnatalCareRecno
			,source.ContextID
			,source.DischargedToHealthVisitorDateID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergePostnatalCareRecno = source.MergePostnatalCareRecno
			,target.ContextID = source.ContextID
			,target.DischargedToHealthVisitorDateID = source.DischargedToHealthVisitorDateID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






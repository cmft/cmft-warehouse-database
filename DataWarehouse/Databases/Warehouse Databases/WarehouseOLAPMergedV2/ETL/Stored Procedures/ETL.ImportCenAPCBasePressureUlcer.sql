﻿CREATE procedure [ETL].[ImportCenAPCBasePressureUlcer] as

--import the data
exec ETL.BuildCenAPCBasePressureUlcer


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BasePressureUlcer]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCBasePressureUlcerReference 'CEN||BEDMAN'


﻿
create procedure [ETL].[BuildRenalBasePatientLabPanelKidneyFunction]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||CV'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Renal.BasePatientLabPanelKidneyFunction target
using [$(Warehouse)].Renal.PatientLabPanelKidneyFunction source
on	source.PatientLabPanelKidneyFunctionRecno = target.PatientLabPanelKidneyFunctionRecno

when matched and not
(
	isnull(target.[SourceUniqueID], '') = isnull(source.[SourceUniqueID], '')
	and isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[LabTestObjectID], 0) = isnull(source.[LabTestObjectID], 0)
	and isnull(target.[LabTestDate], getdate()) = isnull(source.[LabTestDate], getdate())
	and isnull(target.[LabTestQualifier], '') = isnull(source.[LabTestQualifier], '')
	and isnull(target.[LabTestStatus], '') = isnull(source.[LabTestStatus], '')
	and isnull(target.[24hrCreatinineNum], 0) = isnull(source.[24hrCreatinineNum], 0)
	and isnull(target.[24hrCreatinineStr], '') = isnull(source.[24hrCreatinineStr], '')
	and isnull(target.[24hrCreatinineFlg], '') = isnull(source.[24hrCreatinineFlg], '')
	and isnull(target.[24hProteinNum], 0) = isnull(source.[24hProteinNum], 0)
	and isnull(target.[24hProteinStr], '') = isnull(source.[24hProteinStr], '')
	and isnull(target.[24hProteinFlg], '') = isnull(source.[24hProteinFlg], '')
	and isnull(target.[24hUreaNum], 0) = isnull(source.[24hUreaNum], 0)
	and isnull(target.[24hUreaStr], '') = isnull(source.[24hUreaStr], '')
	and isnull(target.[24hUreaFlg], '') = isnull(source.[24hUreaFlg], '')
	and isnull(target.[CreatinineNum], 0) = isnull(source.[CreatinineNum], 0)
	and isnull(target.[CreatinineStr], '') = isnull(source.[CreatinineStr], '')
	and isnull(target.[CreatinineFlg], '') = isnull(source.[CreatinineFlg], '')
	and isnull(target.[CreatinineUrineNum], 0) = isnull(source.[CreatinineUrineNum], 0)
	and isnull(target.[CreatinineUrineStr], '') = isnull(source.[CreatinineUrineStr], '')
	and isnull(target.[CreatinineUrineFlg], '') = isnull(source.[CreatinineUrineFlg], '')
	and isnull(target.[CreatinineClearanceNum], 0) = isnull(source.[CreatinineClearanceNum], 0)
	and isnull(target.[CreatinineClearanceStr], '') = isnull(source.[CreatinineClearanceStr], '')
	and isnull(target.[CreatinineClearanceFlg], '') = isnull(source.[CreatinineClearanceFlg], '')
	and isnull(target.[KtVNum], 0) = isnull(source.[KtVNum], 0)
	and isnull(target.[KtVStr], '') = isnull(source.[KtVStr], '')
	and isnull(target.[KtVFlg], '') = isnull(source.[KtVFlg], '')
	and isnull(target.[ProteinNum], 0) = isnull(source.[ProteinNum], 0)
	and isnull(target.[ProteinStr], '') = isnull(source.[ProteinStr], '')
	and isnull(target.[ProteinFlg], '') = isnull(source.[ProteinFlg], '')
	and isnull(target.[ProteinUrineNum], 0) = isnull(source.[ProteinUrineNum], 0)
	and isnull(target.[ProteinUrineStr], '') = isnull(source.[ProteinUrineStr], '')
	and isnull(target.[ProteinUrineFlg], '') = isnull(source.[ProteinUrineFlg], '')
	and isnull(target.[RecirculationRatioNum], 0) = isnull(source.[RecirculationRatioNum], 0)
	and isnull(target.[RecirculationRatioStr], '') = isnull(source.[RecirculationRatioStr], '')
	and isnull(target.[RecirculationRatioFlg], '') = isnull(source.[RecirculationRatioFlg], '')
	and isnull(target.[BUNNum], 0) = isnull(source.[BUNNum], 0)
	and isnull(target.[BUNStr], '') = isnull(source.[BUNStr], '')
	and isnull(target.[BUNFlg], '') = isnull(source.[BUNFlg], '')
	and isnull(target.[UreaUrineNum], 0) = isnull(source.[UreaUrineNum], 0)
	and isnull(target.[UreaUrineStr], '') = isnull(source.[UreaUrineStr], '')
	and isnull(target.[UreaUrineFlg], '') = isnull(source.[UreaUrineFlg], '')
	and isnull(target.[BloodUreaNitrogenArterialNum], 0) = isnull(source.[BloodUreaNitrogenArterialNum], 0)
	and isnull(target.[BloodUreaNitrogenArterialStr], '') = isnull(source.[BloodUreaNitrogenArterialStr], '')
	and isnull(target.[BloodUreaNitrogenArterialFlg], '') = isnull(source.[BloodUreaNitrogenArterialFlg], '')
	and isnull(target.[UreaClearanceNum], 0) = isnull(source.[UreaClearanceNum], 0)
	and isnull(target.[UreaClearanceStr], '') = isnull(source.[UreaClearanceStr], '')
	and isnull(target.[UreaClearanceFlg], '') = isnull(source.[UreaClearanceFlg], '')
	and isnull(target.[UreaCreaRatioNum], 0) = isnull(source.[UreaCreaRatioNum], 0)
	and isnull(target.[UreaCreaRatioStr], '') = isnull(source.[UreaCreaRatioStr], '')
	and isnull(target.[UreaCreaRatioFlg], '') = isnull(source.[UreaCreaRatioFlg], '')
	and isnull(target.[BloodUreaNitrogenPeripheralNum], 0) = isnull(source.[BloodUreaNitrogenPeripheralNum], 0)
	and isnull(target.[BloodUreaNitrogenPeripheralStr], '') = isnull(source.[BloodUreaNitrogenPeripheralStr], '')
	and isnull(target.[BloodUreaNitrogenPeripheralFlg], '') = isnull(source.[BloodUreaNitrogenPeripheralFlg], '')
	and isnull(target.[URRNum], 0) = isnull(source.[URRNum], 0)
	and isnull(target.[URRStr], '') = isnull(source.[URRStr], '')
	and isnull(target.[URRFlg], '') = isnull(source.[URRFlg], '')
	and isnull(target.[BloodUreaNitrogenVenousNum], 0) = isnull(source.[BloodUreaNitrogenVenousNum], 0)
	and isnull(target.[BloodUreaNitrogenVenousStr], '') = isnull(source.[BloodUreaNitrogenVenousStr], '')
	and isnull(target.[BloodUreaNitrogenVenousFlg], '') = isnull(source.[BloodUreaNitrogenVenousFlg], '')
	and isnull(target.[VolumeNum], 0) = isnull(source.[VolumeNum], 0)
	and isnull(target.[VolumeStr], '') = isnull(source.[VolumeStr], '')
	and isnull(target.[VolumeFlg], '') = isnull(source.[VolumeFlg], '')
	and isnull(target.[CollTimeNum], 0) = isnull(source.[CollTimeNum], 0)
	and isnull(target.[CollTimeStr], '') = isnull(source.[CollTimeStr], '')
	and isnull(target.[CollTimeFlg], '') = isnull(source.[CollTimeFlg], '')
)
then update set
	target.[SourceUniqueID] = source.[SourceUniqueID]
	,target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[LabTestObjectID] = source.[LabTestObjectID]
	,target.[LabTestDate] = source.[LabTestDate]
	,target.[LabTestQualifier] = source.[LabTestQualifier]
	,target.[LabTestStatus] = source.[LabTestStatus]
	,target.[24hrCreatinineNum] = source.[24hrCreatinineNum]
	,target.[24hrCreatinineStr] = source.[24hrCreatinineStr]
	,target.[24hrCreatinineFlg] = source.[24hrCreatinineFlg]
	,target.[24hProteinNum] = source.[24hProteinNum]
	,target.[24hProteinStr] = source.[24hProteinStr]
	,target.[24hProteinFlg] = source.[24hProteinFlg]
	,target.[24hUreaNum] = source.[24hUreaNum]
	,target.[24hUreaStr] = source.[24hUreaStr]
	,target.[24hUreaFlg] = source.[24hUreaFlg]
	,target.[CreatinineNum] = source.[CreatinineNum]
	,target.[CreatinineStr] = source.[CreatinineStr]
	,target.[CreatinineFlg] = source.[CreatinineFlg]
	,target.[CreatinineUrineNum] = source.[CreatinineUrineNum]
	,target.[CreatinineUrineStr] = source.[CreatinineUrineStr]
	,target.[CreatinineUrineFlg] = source.[CreatinineUrineFlg]
	,target.[CreatinineClearanceNum] = source.[CreatinineClearanceNum]
	,target.[CreatinineClearanceStr] = source.[CreatinineClearanceStr]
	,target.[CreatinineClearanceFlg] = source.[CreatinineClearanceFlg]
	,target.[KtVNum] = source.[KtVNum]
	,target.[KtVStr] = source.[KtVStr]
	,target.[KtVFlg] = source.[KtVFlg]
	,target.[ProteinNum] = source.[ProteinNum]
	,target.[ProteinStr] = source.[ProteinStr]
	,target.[ProteinFlg] = source.[ProteinFlg]
	,target.[ProteinUrineNum] = source.[ProteinUrineNum]
	,target.[ProteinUrineStr] = source.[ProteinUrineStr]
	,target.[ProteinUrineFlg] = source.[ProteinUrineFlg]
	,target.[RecirculationRatioNum] = source.[RecirculationRatioNum]
	,target.[RecirculationRatioStr] = source.[RecirculationRatioStr]
	,target.[RecirculationRatioFlg] = source.[RecirculationRatioFlg]
	,target.[BUNNum] = source.[BUNNum]
	,target.[BUNStr] = source.[BUNStr]
	,target.[BUNFlg] = source.[BUNFlg]
	,target.[UreaUrineNum] = source.[UreaUrineNum]
	,target.[UreaUrineStr] = source.[UreaUrineStr]
	,target.[UreaUrineFlg] = source.[UreaUrineFlg]
	,target.[BloodUreaNitrogenArterialNum] = source.[BloodUreaNitrogenArterialNum]
	,target.[BloodUreaNitrogenArterialStr] = source.[BloodUreaNitrogenArterialStr]
	,target.[BloodUreaNitrogenArterialFlg] = source.[BloodUreaNitrogenArterialFlg]
	,target.[UreaClearanceNum] = source.[UreaClearanceNum]
	,target.[UreaClearanceStr] = source.[UreaClearanceStr]
	,target.[UreaClearanceFlg] = source.[UreaClearanceFlg]
	,target.[UreaCreaRatioNum] = source.[UreaCreaRatioNum]
	,target.[UreaCreaRatioStr] = source.[UreaCreaRatioStr]
	,target.[UreaCreaRatioFlg] = source.[UreaCreaRatioFlg]
	,target.[BloodUreaNitrogenPeripheralNum] = source.[BloodUreaNitrogenPeripheralNum]
	,target.[BloodUreaNitrogenPeripheralStr] = source.[BloodUreaNitrogenPeripheralStr]
	,target.[BloodUreaNitrogenPeripheralFlg] = source.[BloodUreaNitrogenPeripheralFlg]
	,target.[URRNum] = source.[URRNum]
	,target.[URRStr] = source.[URRStr]
	,target.[URRFlg] = source.[URRFlg]
	,target.[BloodUreaNitrogenVenousNum] = source.[BloodUreaNitrogenVenousNum]
	,target.[BloodUreaNitrogenVenousStr] = source.[BloodUreaNitrogenVenousStr]
	,target.[BloodUreaNitrogenVenousFlg] = source.[BloodUreaNitrogenVenousFlg]
	,target.[VolumeNum] = source.[VolumeNum]
	,target.[VolumeStr] = source.[VolumeStr]
	,target.[VolumeFlg] = source.[VolumeFlg]
	,target.[CollTimeNum] = source.[CollTimeNum]
	,target.[CollTimeStr] = source.[CollTimeStr]
	,target.[CollTimeFlg] = source.[CollTimeFlg]
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	[SourceUniqueID]
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[LabTestObjectID]
	,[LabTestDate]
	,[LabTestQualifier]
	,[LabTestStatus]
	,[24hrCreatinineNum]
	,[24hrCreatinineStr]
	,[24hrCreatinineFlg]
	,[24hProteinNum]
	,[24hProteinStr]
	,[24hProteinFlg]
	,[24hUreaNum]
	,[24hUreaStr]
	,[24hUreaFlg]
	,[CreatinineNum]
	,[CreatinineStr]
	,[CreatinineFlg]
	,[CreatinineUrineNum]
	,[CreatinineUrineStr]
	,[CreatinineUrineFlg]
	,[CreatinineClearanceNum]
	,[CreatinineClearanceStr]
	,[CreatinineClearanceFlg]
	,[KtVNum]
	,[KtVStr]
	,[KtVFlg]
	,[ProteinNum]
	,[ProteinStr]
	,[ProteinFlg]
	,[ProteinUrineNum]
	,[ProteinUrineStr]
	,[ProteinUrineFlg]
	,[RecirculationRatioNum]
	,[RecirculationRatioStr]
	,[RecirculationRatioFlg]
	,[BUNNum]
	,[BUNStr]
	,[BUNFlg]
	,[UreaUrineNum]
	,[UreaUrineStr]
	,[UreaUrineFlg]
	,[BloodUreaNitrogenArterialNum]
	,[BloodUreaNitrogenArterialStr]
	,[BloodUreaNitrogenArterialFlg]
	,[UreaClearanceNum]
	,[UreaClearanceStr]
	,[UreaClearanceFlg]
	,[UreaCreaRatioNum]
	,[UreaCreaRatioStr]
	,[UreaCreaRatioFlg]
	,[BloodUreaNitrogenPeripheralNum]
	,[BloodUreaNitrogenPeripheralStr]
	,[BloodUreaNitrogenPeripheralFlg]
	,[URRNum]
	,[URRStr]
	,[URRFlg]
	,[BloodUreaNitrogenVenousNum]
	,[BloodUreaNitrogenVenousStr]
	,[BloodUreaNitrogenVenousFlg]
	,[VolumeNum]
	,[VolumeStr]
	,[VolumeFlg]
	,[CollTimeNum]
	,[CollTimeStr]
	,[CollTimeFlg]
	,[PatientLabPanelKidneyFunctionRecno]
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[SourceUniqueID]
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[LabTestObjectID]
	,source.[LabTestDate]
	,source.[LabTestQualifier]
	,source.[LabTestStatus]
	,source.[24hrCreatinineNum]
	,source.[24hrCreatinineStr]
	,source.[24hrCreatinineFlg]
	,source.[24hProteinNum]
	,source.[24hProteinStr]
	,source.[24hProteinFlg]
	,source.[24hUreaNum]
	,source.[24hUreaStr]
	,source.[24hUreaFlg]
	,source.[CreatinineNum]
	,source.[CreatinineStr]
	,source.[CreatinineFlg]
	,source.[CreatinineUrineNum]
	,source.[CreatinineUrineStr]
	,source.[CreatinineUrineFlg]
	,source.[CreatinineClearanceNum]
	,source.[CreatinineClearanceStr]
	,source.[CreatinineClearanceFlg]
	,source.[KtVNum]
	,source.[KtVStr]
	,source.[KtVFlg]
	,source.[ProteinNum]
	,source.[ProteinStr]
	,source.[ProteinFlg]
	,source.[ProteinUrineNum]
	,source.[ProteinUrineStr]
	,source.[ProteinUrineFlg]
	,source.[RecirculationRatioNum]
	,source.[RecirculationRatioStr]
	,source.[RecirculationRatioFlg]
	,source.[BUNNum]
	,source.[BUNStr]
	,source.[BUNFlg]
	,source.[UreaUrineNum]
	,source.[UreaUrineStr]
	,source.[UreaUrineFlg]
	,source.[BloodUreaNitrogenArterialNum]
	,source.[BloodUreaNitrogenArterialStr]
	,source.[BloodUreaNitrogenArterialFlg]
	,source.[UreaClearanceNum]
	,source.[UreaClearanceStr]
	,source.[UreaClearanceFlg]
	,source.[UreaCreaRatioNum]
	,source.[UreaCreaRatioStr]
	,source.[UreaCreaRatioFlg]
	,source.[BloodUreaNitrogenPeripheralNum]
	,source.[BloodUreaNitrogenPeripheralStr]
	,source.[BloodUreaNitrogenPeripheralFlg]
	,source.[URRNum]
	,source.[URRStr]
	,source.[URRFlg]
	,source.[BloodUreaNitrogenVenousNum]
	,source.[BloodUreaNitrogenVenousStr]
	,source.[BloodUreaNitrogenVenousFlg]
	,source.[VolumeNum]
	,source.[VolumeStr]
	,source.[VolumeFlg]
	,source.[CollTimeNum]
	,source.[CollTimeStr]
	,source.[CollTimeFlg]
	,source.[PatientLabPanelKidneyFunctionRecno]
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

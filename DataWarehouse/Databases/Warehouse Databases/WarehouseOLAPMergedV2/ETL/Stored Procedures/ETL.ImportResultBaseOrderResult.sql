﻿create procedure [ETL].[ImportResultBaseOrderResult] as

-------------------------------------------------
--When		Who	What
--20150508	DG	Created procedure
-------------------------------------------------

--load CWS and ICEs into TLoad table
exec ETL.BuildTLoadResultBaseOrderResult

--load encounters and generate process list
exec ETL.LoadResultBaseOrderResult




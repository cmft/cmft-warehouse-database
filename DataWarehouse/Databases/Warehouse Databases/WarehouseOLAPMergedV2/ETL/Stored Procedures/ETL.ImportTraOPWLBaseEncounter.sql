﻿create procedure [ETL].[ImportTraOPWLBaseEncounter] as

--import the data
exec ETL.BuildTraOPWLBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[OPWL].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildOPWLBaseEncounterReference 'TRA||UG'

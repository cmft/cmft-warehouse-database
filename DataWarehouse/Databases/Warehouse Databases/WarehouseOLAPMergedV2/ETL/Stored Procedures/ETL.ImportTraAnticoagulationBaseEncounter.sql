﻿CREATE procedure ETL.ImportTraAnticoagulationBaseEncounter as

--import the data
exec ETL.BuildTraAnticoagulationBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Anticoagulation].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAnticoagulationBaseEncounterReference 'TRA||DAWN'

﻿CREATE procedure [ETL].[BuildCasenoteBaseReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	Casenote.BaseReference
where
	not exists
	(
	select
		1
	from
		Casenote.Base
	where
		Base.MergeRecno = BaseReference.MergeRecno
	)
and	BaseReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


select
	 Base.MergeRecno
	,ContextID = Context.ContextID
	,CasenoteLocationID = CasenoteLocation.SourceLocationID
into
	#Base
from
	Casenote.Base

inner join WH.Context
on	Context.ContextCode = Base.ContextCode

inner join Casenote.Location CasenoteLocation
on	CasenoteLocation.SourceLocationCode = coalesce(Base.CasenoteLocationCode, '-1')
and	CasenoteLocation.SourceContextCode = Base.ContextCode

where
	Base.ContextCode = @ContextCode

CREATE UNIQUE CLUSTERED INDEX #IX_Base ON #Base
	(
	 MergeRecno
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


merge
	Casenote.BaseReference target
using
	(
	select
		 MergeRecno
		,ContextID
		,CasenoteLocationID
	from
		#Base
	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			 MergeRecno
			,ContextID
			,CasenoteLocationID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.CasenoteLocationID
			)

	when matched
	and not
		(
			target.CasenoteLocationID = source.CasenoteLocationID
		)
	then
		update
		set
			 target.CasenoteLocationID = source.CasenoteLocationID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





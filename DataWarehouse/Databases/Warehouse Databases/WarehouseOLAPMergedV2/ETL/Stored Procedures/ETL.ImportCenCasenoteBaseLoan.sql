﻿CREATE procedure [ETL].ImportCenCasenoteBaseLoan as

--import the data
exec ETL.BuildCenCasenoteBaseLoan


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Casenote].[BaseLoan]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember

--Build the BaseReference table
exec ETL.BuildCasenoteBaseLoanReference 'CEN||PAS'


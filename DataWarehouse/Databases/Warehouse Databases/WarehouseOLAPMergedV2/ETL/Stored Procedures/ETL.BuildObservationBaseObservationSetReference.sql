﻿CREATE procedure [ETL].[BuildObservationBaseObservationSetReference]

	@ContextCode varchar(50)

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Observation.BaseObservationSetReference
			where
				BaseObservationSetReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Observation.BaseObservationSetReference
where
	not exists
	(
	select
		1
	from
		Observation.BaseObservationSet
	where
		BaseObservationSet.MergeObservationSetRecno = BaseObservationSetReference.MergeObservationSetRecno
	)
and	BaseObservationSetReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Observation.BaseObservationSetReference target
using
	(
	select
		 ObservationSet.MergeObservationSetRecno
		,ContextID = Context.ContextID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,LocationID = Location.SourceLocationID
		,ObservationProfileApplicationID = null
		,ObservationNotTakenReasonID = ObservationNotTakenReason.SourceObservationNotTakenReasonID
		,ClinicianPresentSeniorityID = null
		,StartDateID = 
				coalesce(
						 StartDate.DateID

						,case
						when ObservationSet.StartDate is null
						then NullDate.DateID

						when ObservationSet.StartDate < CalendarStartDate.DateValue
						then PreviousDate.DateID

						else FutureDate.DateID
						end
					)
		,TakenDateID = 
				coalesce(
						 TakenDate.DateID

						,case
						when ObservationSet.TakenDate is null
						then NullDate.DateID

						when ObservationSet.TakenDate < CalendarStartDate.DateValue
						then PreviousDate.DateID

						else FutureDate.DateID
						end
					)
		,OverallRiskIndexID = OverallRiskIndex.SourceOverallRiskIndexID
		,OverallAssessedStatusID = OverallAssessedStatus.SourceOverallAssessedStatusID
		,AlertSeverityID = AlertSeverity.SourceAlertSeverityID
		,DueTimeStatusID = ObservationStatus.SourceObservationStatusID
		,ObservationSet.Created
		,ObservationSet.Updated
		,ObservationSet.ByWhom
	from
		Observation.BaseObservationSet ObservationSet

	inner join WH.Context
	on	Context.ContextCode = ObservationSet.ContextCode

	inner join WH.Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(ObservationSet.SpecialtyID, '-1')
	and	Specialty.SourceContextCode = ObservationSet.ContextCode

	inner join WH.Location
	on	Location.SourceLocationCode = coalesce(ObservationSet.LocationID, '-1')
	and	Location.SourceContextCode = ObservationSet.ContextCode

	inner join Observation.ObservationNotTakenReason
	on	ObservationNotTakenReason.SourceObservationNotTakenReasonCode = coalesce(ObservationSet.ObservationNotTakenReasonID, '-1')
	and	ObservationNotTakenReason.SourceContextCode = ObservationSet.ContextCode

	inner join Observation.OverallRiskIndex
	on	OverallRiskIndex.SourceOverallRiskIndexCode = coalesce(ObservationSet.OverallRiskIndexCode, '-1')
	and	OverallRiskIndex.SourceContextCode = ObservationSet.ContextCode

	inner join Observation.OverallAssessedStatus
	on	OverallAssessedStatus.SourceOverallAssessedStatusCode = coalesce(ObservationSet.OverallAssessedStatusID, '-1')
	and	OverallAssessedStatus.SourceContextCode = ObservationSet.ContextCode

	inner join Observation.ObservationStatus
	on	ObservationStatus.SourceObservationStatusCode = coalesce(ObservationSet.DueTimeStatusID, '-1')
	and	ObservationStatus.SourceContextCode = ObservationSet.ContextCode

	inner join Observation.AlertSeverity
	on	AlertSeverity.SourceAlertSeverityCode = coalesce(ObservationSet.AlertSeverityID, '-1')
	and	AlertSeverity.SourceContextCode = ObservationSet.ContextCode

	left join WH.Calendar StartDate
	on	StartDate.TheDate = ObservationSet.StartDate

	left join WH.Calendar TakenDate
	on	TakenDate.TheDate = ObservationSet.TakenDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		ObservationSet.Updated > @LastUpdated
	and	ObservationSet.ContextCode = @ContextCode

	) source
	on	source.MergeObservationSetRecno = target.MergeObservationSetRecno

	when not matched
	then
		insert
			(
			MergeObservationSetRecno
			,ContextID
			,SpecialtyID
			,LocationID
			,ObservationProfileApplicationID
			,ObservationNotTakenReasonID
			,ClinicianPresentSeniorityID
			,StartDateID
			,TakenDateID
			,OverallRiskIndexID
			,OverallAssessedStatusID
			,AlertSeverityID
			,DueTimeStatusID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeObservationSetRecno
			,source.ContextID
			,source.SpecialtyID
			,source.LocationID
			,source.ObservationProfileApplicationID
			,source.ObservationNotTakenReasonID
			,source.ClinicianPresentSeniorityID
			,source.StartDateID
			,source.TakenDateID
			,source.OverallRiskIndexID
			,source.OverallAssessedStatusID
			,source.AlertSeverityID
			,source.DueTimeStatusID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and target.Updated != source.Updated

	then
		update
		set
			target.ContextID = source.ContextID
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.ObservationProfileApplicationID = source.ObservationProfileApplicationID
			,target.ObservationNotTakenReasonID = source.ObservationNotTakenReasonID
			,target.ClinicianPresentSeniorityID = source.ClinicianPresentSeniorityID
			,target.StartDateID = source.StartDateID
			,target.TakenDateID = source.TakenDateID	
			,target.OverallRiskIndexID = source.OverallRiskIndexID
			,target.OverallAssessedStatusID = source.OverallAssessedStatusID	
			,target.AlertSeverityID = source.AlertSeverityID
			,target.DueTimeStatusID = source.DueTimeStatusID	
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


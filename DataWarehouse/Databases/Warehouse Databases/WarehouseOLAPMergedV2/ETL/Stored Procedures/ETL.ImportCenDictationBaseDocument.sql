﻿



CREATE Procedure [ETL].[ImportCenDictationBaseDocument]

as

--import the data
exec ETL.BuildCenDictationBaseDocument


declare
      @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Dictation].[BaseDocument]'
      ,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
      exec WH.BuildMember

--Build the BaseReference table
exec ETL.BuildDictationBaseDocumentReference 'CEN||MEDI'




﻿
create procedure [ETL].[BuildRenalBasePatientVascularAccessEvents]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||CV'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Renal.BasePatientVascularAccessEvents target
using [$(Warehouse)].Renal.PatientVascularAccessEvents source
on	source.PatientVascularAccessEventsRecno = target.PatientVascularAccessEventsRecno

when matched and not
(
	isnull(target.[SourceUniqueID], '') = isnull(source.[SourceUniqueID], '')
	and isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[StartDate], getdate()) = isnull(source.[StartDate], getdate())
	and isnull(target.[StopDate], getdate()) = isnull(source.[StopDate], getdate())
	and isnull(target.[DaysElapsed], 0) = isnull(source.[DaysElapsed], 0)
	and isnull(target.[AgeAtEvent], 0) = isnull(source.[AgeAtEvent], 0)
	and isnull(target.[TimelineEvent], '') = isnull(source.[TimelineEvent], '')
	and isnull(target.[TimelineEventDetail], '') = isnull(source.[TimelineEventDetail], '')
	and isnull(target.[InformationSource], '') = isnull(source.[InformationSource], '')
	and isnull(target.[Notes], '') = isnull(source.[Notes], '')
	and isnull(target.[Etiology], '') = isnull(source.[Etiology], '')
	and isnull(target.[AccessType], '') = isnull(source.[AccessType], '')
	and isnull(target.[AccessLocation], '') = isnull(source.[AccessLocation], '')
	and isnull(target.[PermanentStatus], '') = isnull(source.[PermanentStatus], '')
	and isnull(target.[Brand], '') = isnull(source.[Brand], '')
	and isnull(target.[CatheterType], '') = isnull(source.[CatheterType], '')
	and isnull(target.[Configuration], '') = isnull(source.[Configuration], '')
	and isnull(target.[Elasticity], '') = isnull(source.[Elasticity], '')
	and isnull(target.[ExternalStatus], '') = isnull(source.[ExternalStatus], '')
	and isnull(target.[NeedleGuage], '') = isnull(source.[NeedleGuage], '')
	and isnull(target.[NeedleLength], '') = isnull(source.[NeedleLength], '')
	and isnull(target.[NeedleType], '') = isnull(source.[NeedleType], '')
	and isnull(target.[NeedleGuage2], '') = isnull(source.[NeedleGuage2], '')
	and isnull(target.[NeedleLength2], '') = isnull(source.[NeedleLength2], '')
	and isnull(target.[NeedleType2], '') = isnull(source.[NeedleType2], '')
	and isnull(target.[VenousLumen], '') = isnull(source.[VenousLumen], '')
	and isnull(target.[ArterialLumen], '') = isnull(source.[ArterialLumen], '')
	and isnull(target.[Preparation], '') = isnull(source.[Preparation], '')
	and isnull(target.[PrimingProcedures], '') = isnull(source.[PrimingProcedures], '')
	and isnull(target.[Surgeon], '') = isnull(source.[Surgeon], '')
	and isnull(target.[VascularAccessObjectID], 0) = isnull(source.[VascularAccessObjectID], 0)
)
then update set
	target.[SourceUniqueID] = source.[SourceUniqueID]
	,target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[StartDate] = source.[StartDate]
	,target.[StopDate] = source.[StopDate]
	,target.[DaysElapsed] = source.[DaysElapsed]
	,target.[AgeAtEvent] = source.[AgeAtEvent]
	,target.[TimelineEvent] = source.[TimelineEvent]
	,target.[TimelineEventDetail] = source.[TimelineEventDetail]
	,target.[InformationSource] = source.[InformationSource]
	,target.[Notes] = source.[Notes]
	,target.[Etiology] = source.[Etiology]
	,target.[AccessType] = source.[AccessType]
	,target.[AccessLocation] = source.[AccessLocation]
	,target.[PermanentStatus] = source.[PermanentStatus]
	,target.[Brand] = source.[Brand]
	,target.[CatheterType] = source.[CatheterType]
	,target.[Configuration] = source.[Configuration]
	,target.[Elasticity] = source.[Elasticity]
	,target.[ExternalStatus] = source.[ExternalStatus]
	,target.[NeedleGuage] = source.[NeedleGuage]
	,target.[NeedleLength] = source.[NeedleLength]
	,target.[NeedleType] = source.[NeedleType]
	,target.[NeedleGuage2] = source.[NeedleGuage2]
	,target.[NeedleLength2] = source.[NeedleLength2]
	,target.[NeedleType2] = source.[NeedleType2]
	,target.[VenousLumen] = source.[VenousLumen]
	,target.[ArterialLumen] = source.[ArterialLumen]
	,target.[Preparation] = source.[Preparation]
	,target.[PrimingProcedures] = source.[PrimingProcedures]
	,target.[Surgeon] = source.[Surgeon]
	,target.[VascularAccessObjectID] = source.[VascularAccessObjectID]
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	[SourceUniqueID]
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[StartDate]
	,[StopDate]
	,[DaysElapsed]
	,[AgeAtEvent]
	,[TimelineEvent]
	,[TimelineEventDetail]
	,[InformationSource]
	,[Notes]
	,[Etiology]
	,[AccessType]
	,[AccessLocation]
	,[PermanentStatus]
	,[Brand]
	,[CatheterType]
	,[Configuration]
	,[Elasticity]
	,[ExternalStatus]
	,[NeedleGuage]
	,[NeedleLength]
	,[NeedleType]
	,[NeedleGuage2]
	,[NeedleLength2]
	,[NeedleType2]
	,[VenousLumen]
	,[ArterialLumen]
	,[Preparation]
	,[PrimingProcedures]
	,[Surgeon]
	,[VascularAccessObjectID]
	,[PatientVascularAccessEventsRecno]
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[SourceUniqueID]
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[StartDate]
	,source.[StopDate]
	,source.[DaysElapsed]
	,source.[AgeAtEvent]
	,source.[TimelineEvent]
	,source.[TimelineEventDetail]
	,source.[InformationSource]
	,source.[Notes]
	,source.[Etiology]
	,source.[AccessType]
	,source.[AccessLocation]
	,source.[PermanentStatus]
	,source.[Brand]
	,source.[CatheterType]
	,source.[Configuration]
	,source.[Elasticity]
	,source.[ExternalStatus]
	,source.[NeedleGuage]
	,source.[NeedleLength]
	,source.[NeedleType]
	,source.[NeedleGuage2]
	,source.[NeedleLength2]
	,source.[NeedleType2]
	,source.[VenousLumen]
	,source.[ArterialLumen]
	,source.[Preparation]
	,source.[PrimingProcedures]
	,source.[Surgeon]
	,source.[VascularAccessObjectID]
	,source.[PatientVascularAccessEventsRecno]
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

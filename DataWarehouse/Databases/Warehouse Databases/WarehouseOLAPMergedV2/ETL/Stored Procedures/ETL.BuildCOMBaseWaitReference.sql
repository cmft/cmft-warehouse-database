﻿CREATE procedure [ETL].[BuildCOMBaseWaitReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				COM.BaseWaitReference
			)
			,'1 jan 1900'
		)

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	COM.BaseWaitReference
where
	not exists
	(
	select
		1
	from
		COM.BaseWait
	where
		BaseWait.MergeEncounterRecno = BaseWaitReference.MergeEncounterRecno
	)
and	ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	COM.BaseWaitReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,SpecialtyID = Specialty.SourceValueID
		,ProfessionalCarerID = ProfessionalCarer.SourceValueID
		,StaffTeamID = StaffTeam.SourceValueID
		,CreatedByID = CreatedBy.SourceValueID
		,ModifiedByID = ModifiedBy.SourceValueID

		,ReferralReceivedDateID =
			coalesce(
				 ReferralReceivedDate.DateID

				,case
				when Encounter.ReferralReceivedDate is null
				then NullDate.DateID

				when Encounter.ReferralReceivedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ReferralReasonID = ReferralReason.SourceValueID
		,ReferralTypeID = ReferralType.SourceValueID
		,ReferralSourceID = ReferralSource.SourceValueID

		,CensusDateID =
			coalesce(
				 CensusDate.DateID
				,NullDate.DateID
			)

		,DurationID = Duration.DurationID
		,SexID = Sex.SourceSexID
		,EthnicCategoryID = EthnicCategory.SourceEthnicCategoryID
		,MaritalStatusID = MaritalStatus.SourceMaritalStatusID
		,ReligionID = Religion.SourceReligionID
		,AgeID = Age.AgeID

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		COM.BaseWait Encounter

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	inner join COM.Specialty Specialty
	on	Specialty.SpecialtyID = coalesce(Encounter.ReferredToSpecialtyID, '-1')
	and	Specialty.ContextCode = Encounter.ContextCode

	inner join COM.ProfessionalCarer ProfessionalCarer
	on	ProfessionalCarer.ProfessionalCarerID = coalesce(Encounter.ReferredToProfessionalCarerID, '-1')
	and	ProfessionalCarer.ContextCode = Encounter.ContextCode

	inner join COM.StaffTeam StaffTeam
	on	StaffTeam.StaffTeamID = coalesce(Encounter.ReferredToStaffTeamID, '-1')
	and	StaffTeam.ContextCode = Encounter.ContextCode

	inner join COM.SystemUser CreatedBy
	on	CreatedBy.SystemUserID = Encounter.CreatedByID
	and	CreatedBy.ContextCode = Encounter.ContextCode

	inner join COM.SystemUser ModifiedBy
	on	ModifiedBy.SystemUserID = Encounter.ModifiedByID
	and	ModifiedBy.ContextCode = Encounter.ContextCode

	left join WH.Calendar CensusDate
	on	CensusDate.TheDate = Encounter.CensusDate

	left join WH.Calendar ReferralReceivedDate
	on	ReferralReceivedDate.TheDate = Encounter.ReferralReceivedDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	inner join COM.ReferralReason ReferralReason
	on	ReferralReason.ReferralReasonID = coalesce(Encounter.ReasonforReferralID, '-1')
	and	ReferralReason.ContextCode = Encounter.ContextCode

	inner join COM.ReferralType ReferralType
	on	ReferralType.ReferralTypeID = coalesce(Encounter.TypeofReferralID, '-1')
	and	ReferralType.ContextCode = Encounter.ContextCode

	inner join COM.ReferralSource ReferralSource
	on	ReferralSource.ReferralSourceID = coalesce(Encounter.SourceofReferralID, '-1')
	and	ReferralSource.ContextCode = Encounter.ContextCode

	inner join WH.Duration
	on	Duration.DurationCode = Encounter.DurationCode

	inner join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.PatientSexID, -1) as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	inner join WH.EthnicCategory EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryCode = cast(coalesce(Encounter.PatientEthnicGroupID, -1) as varchar)
	and	EthnicCategory.SourceContextCode = Encounter.ContextCode

	inner join WH.MaritalStatus MaritalStatus
	on	MaritalStatus.SourceMaritalStatusCode = cast(coalesce(Encounter.PatientMaritalStatusID, -1) as varchar)
	and	MaritalStatus.SourceContextCode = Encounter.ContextCode

	inner join WH.Religion Religion
	on	Religion.SourceReligionCode = cast(coalesce(Encounter.PatientReligionID, -1) as varchar)
	and	Religion.SourceContextCode = Encounter.ContextCode

	inner join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,CensusDateID
			,ReferralReceivedDateID
			,AgeID
			,EthnicCategoryID
			,MaritalStatusID
			,SexID
			,ProfessionalCarerID
			,ReferralReasonID
			,ReferralSourceID
			,ReferralTypeID
			,ReligionID
			,SpecialtyID
			,StaffTeamID
			,CreatedByID
			,ModifiedByID
			,DurationID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.CensusDateID
			,source.ReferralReceivedDateID
			,source.AgeID
			,source.EthnicCategoryID
			,source.MaritalStatusID
			,source.SexID
			,source.ProfessionalCarerID
			,source.ReferralReasonID
			,source.ReferralSourceID
			,source.ReferralTypeID
			,source.ReligionID
			,source.SpecialtyID
			,source.StaffTeamID
			,source.CreatedByID
			,source.ModifiedByID
			,source.DurationID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.CensusDateID = source.CensusDateID
			,target.ReferralReceivedDateID = source.ReferralReceivedDateID
			,target.AgeID = source.AgeID
			,target.EthnicCategoryID = source.EthnicCategoryID
			,target.MaritalStatusID = source.MaritalStatusID
			,target.SexID = source.SexID
			,target.ProfessionalCarerID = source.ProfessionalCarerID
			,target.ReferralReasonID = source.ReferralReasonID
			,target.ReferralSourceID = source.ReferralSourceID
			,target.ReferralTypeID = source.ReferralTypeID
			,target.ReligionID = source.ReligionID
			,target.SpecialtyID = source.SpecialtyID
			,target.StaffTeamID = source.StaffTeamID
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.DurationID = source.DurationID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

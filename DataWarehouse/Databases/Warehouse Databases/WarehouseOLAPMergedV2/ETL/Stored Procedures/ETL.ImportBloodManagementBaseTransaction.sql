﻿
CREATE procedure [ETL].[ImportBloodManagementBaseTransaction] as

--import the data
exec ETL.BuildBloodManagementBaseTransaction


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[BloodManagement].[BaseTransaction]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
 exec ETL.BuildBloodManagementBaseTransactionReference 'CMFT||BLDTRK' 




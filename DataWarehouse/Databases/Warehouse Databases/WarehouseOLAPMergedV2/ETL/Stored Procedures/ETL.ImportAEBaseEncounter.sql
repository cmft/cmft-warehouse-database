﻿CREATE procedure [ETL].[ImportAEBaseEncounter] as

-------------------------------------------------
--When		Who	What
--20141215	PDO	Created procedure
-------------------------------------------------

declare @ReturnValue int


declare @Process varchar(255) =  OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @LastLoadTime datetime =
	Utility.GetLastLoadTime(@Process);

if @LastLoadTime is null
	set @LastLoadTime = '1 jan 1900' --default to start of time


--load Central and Trafford into TLoad table
exec @ReturnValue = ETL.BuildTLoadAEBaseEncounter @LastLoadTime

if @ReturnValue <> 0
	return @ReturnValue

--load encounters and generate process list
exec @ReturnValue = ETL.LoadAEBaseEncounter

if @ReturnValue <> 0
	return @ReturnValue


BEGIN TRY

	--rebuild the process list with the latest changes
	delete
	from
		AE.ProcessList
	where
		Dataset = 'Encounter'


	insert
	into
		AE.ProcessList
		(
		MergeRecno
		,Dataset
		,Action
		)
	select
		Activity.MergeEncounterRecno
		,Dataset = 'Encounter'
		,Action = case when Activity.Updated is null then 'INSERT' else 'UPDATE' end
	from
		AE.BaseEncounter Activity
	where
		isnull(Activity.Updated, Activity.Created) > @LastLoadTime

	union all

	select
		Activity.MergeEncounterRecno
		,Dataset = 'Encounter'
		,Action = 'DELETE'
	from
		AE.BaseEncounterReference Activity
	where
		not exists
		(
		select
			1
		from
			AE.BaseEncounter Encounter
		where
			Encounter.MergeEncounterRecno = Activity.MergeEncounterRecno
		)

	--generate missing reference data
	declare
		 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[AE].[BaseEncounter]'
		,@ProcessListTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[AE].[ProcessList]'
		,@NewRows int

	declare	@ActivityTableOverride varchar(max) = 
		'(
		select
			Encounter.*
		from
			' + @ActivityTable + ' Encounter

		inner join ' + @ProcessListTable + ' ProcessList
		on	ProcessList.MergeRecno = Encounter.MergeEncounterRecno
		and	Dataset = ''Encounter''
		)'

	exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output,@Debug = 0 ,@ActivityTableOverride = @ActivityTableOverride

	if @NewRows > 0
		exec WH.BuildMember

	--load reference values
	exec ETL.LoadAEBaseEncounterReference

	--Update the last load time
	set @LastLoadTime = getdate()
	exec Utility.SetLastLoadTime @Process, @LastLoadTime


END TRY

BEGIN
	CATCH
	
	set @ReturnValue = @@ERROR

	DECLARE @ErrorSeverity INT,
			@ErrorNumber   INT,
			@ErrorMessage nvarchar(4000),
			@ErrorState INT,
			@ErrorLine  INT,
			@ErrorProc nvarchar(200)
			-- Grab error information from SQL functions
	SET @ErrorSeverity = ERROR_SEVERITY()
	SET @ErrorNumber   = ERROR_NUMBER()
	SET @ErrorMessage  = ERROR_MESSAGE()
	SET @ErrorState    = ERROR_STATE()
	SET @ErrorLine     = ERROR_LINE()
	SET @ErrorProc     = ERROR_PROCEDURE()
	SET @ErrorMessage  = 'An error occurred.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
	-- Not all errors generate an error state, to set to 1 if it's zero
	IF @ErrorState  = 0
	SET @ErrorState = 1
	-- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
	IF @@TRANCOUNT > 0
	BEGIN
			--print 'Rollback transaction'
			ROLLBACK TRANSACTION
	END
	RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
END CATCH

RETURN @ReturnValue

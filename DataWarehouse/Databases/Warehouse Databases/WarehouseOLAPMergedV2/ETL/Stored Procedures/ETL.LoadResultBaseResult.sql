﻿CREATE procedure [ETL].[LoadResultBaseResult]
as

-------------------------------------------------
--When		Who	What
--20150617	DG	Created procedure
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE
	(
	 MergeRecno int
	,Action nvarchar(10)
	);


merge
	Result.BaseResult target
using
	(
	select
		 ResultRecno
		,Action
		,ContextCode
		,SourceUniqueID
		,SourcePatientNo
		,DistrictNo
		,CasenoteNumber
		,NHSNumber
		,DateOfBirth
		,SexCode
		,ConsultantCode
		,MainSpecialtyCode
		,DisciplineCode
		,SpecialtyCode
		,LocationCode
		,ResultSetSourceUniqueID
		,ResultSetCode
		,UniqueResultSetCode
		,ResultSetStatusCode
		,ResultSetComment
		,SampleReferenceCode
		,SampleTypeCode
		,SampleType
		,EffectiveTime
		,ResultCode
		,UniqueResultCode
		,Result
		,UnitOfMeasurement
		,Abnormal
		,LowerReferenceRangeValue
		,UpperReferenceRangeValue
		,ResultComment
		,ResultTime
		,InterfaceCode
	from
		ETL.TLoadResultBaseResult Result
	) source
	on	source.ContextCode = target.ContextCode
	and	source.ResultRecno = target.ResultRecno

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	and	source.Action = 'INSERT'
	then
		insert
			(
			 ResultRecno
			,ContextCode
			,SourceUniqueID
			,SourcePatientNo
			,DistrictNo
			,CasenoteNumber
			,NHSNumber
			,DateOfBirth
			,SexCode
			,ConsultantCode
			,MainSpecialtyCode
			,DisciplineCode
			,SpecialtyCode
			,LocationCode
			,ResultSetSourceUniqueID
			,ResultSetCode
			,UniqueResultSetCode
			,ResultSetStatusCode
			,ResultSetComment
			,SampleReferenceCode
			,SampleTypeCode
			,SampleType
			,EffectiveTime
			,ResultCode
			,UniqueResultCode
			,Result
			,UnitOfMeasurement
			,Abnormal
			,LowerReferenceRangeValue
			,UpperReferenceRangeValue
			,ResultComment
			,ResultTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.ResultRecno
			,source.ContextCode
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.DistrictNo
			,source.CasenoteNumber
			,source.NHSNumber
			,source.DateOfBirth
			,source.SexCode
			,source.ConsultantCode
			,source.MainSpecialtyCode
			,source.DisciplineCode
			,source.SpecialtyCode
			,source.LocationCode
			,source.ResultSetSourceUniqueID
			,source.ResultSetCode
			,source.UniqueResultSetCode
			,source.ResultSetStatusCode
			,source.ResultSetComment
			,source.SampleReferenceCode
			,source.SampleTypeCode
			,source.SampleType
			,source.EffectiveTime
			,source.ResultCode
			,source.UniqueResultCode
			,source.Result
			,source.UnitOfMeasurement
			,source.Abnormal
			,source.LowerReferenceRangeValue
			,source.UpperReferenceRangeValue
			,source.ResultComment
			,source.ResultTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and	source.Action = 'UPDATE'
	then
		update
		set
			 target.ResultRecno = source.ResultRecno
			,target.ContextCode = source.ContextCode
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.DistrictNo = source.DistrictNo
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SexCode = source.SexCode
			,target.ConsultantCode = source.ConsultantCode
			,target.MainSpecialtyCode = source.MainSpecialtyCode
			,target.DisciplineCode = source.DisciplineCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.LocationCode = source.LocationCode
			,target.ResultSetSourceUniqueID = source.ResultSetSourceUniqueID
			,target.ResultSetCode = source.ResultSetCode
			,target.UniqueResultSetCode = source.UniqueResultSetCode
			,target.ResultSetStatusCode = source.ResultSetStatusCode
			,target.ResultSetComment = source.ResultSetComment
			,target.SampleReferenceCode = source.SampleReferenceCode
			,target.SampleTypeCode = source.SampleTypeCode
			,target.SampleType = source.SampleType
			,target.EffectiveTime = source.EffectiveTime
			,target.ResultCode = source.ResultCode
			,target.UniqueResultCode = source.UniqueResultCode
			,target.Result = source.Result
			,target.UnitOfMeasurement = source.UnitOfMeasurement
			,target.Abnormal = source.Abnormal
			,target.LowerReferenceRangeValue = source.LowerReferenceRangeValue
			,target.UpperReferenceRangeValue = source.UpperReferenceRangeValue
			,target.ResultComment = source.ResultComment
			,target.ResultTime = source.ResultTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	 coalesce(inserted.MergeResultRecno, deleted.MergeResultRecno)
	,$action
	into
		@MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


--merge ProcessList in case it already has rows from a previous (failed) run
merge
	Result.ProcessList target
using
	@MergeSummary source
on	source.MergeRecno = target.MergeRecno

when not matched
then
	insert
		(
		 MergeRecno
		,Action
		)
	values
		(
		 source.MergeRecno
		,source.Action
		)

when matched
then
	update
	set
		target.Action = source.Action
;



select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


--clear down Process lists in classic

exec [$(Warehouse)].ETL.BuildCENICEProcessListArchive
exec [$(Warehouse)].ETL.BuildCWSProcessListArchive
exec [$(Warehouse)].ETL.BuildGPICEProcessListArchive




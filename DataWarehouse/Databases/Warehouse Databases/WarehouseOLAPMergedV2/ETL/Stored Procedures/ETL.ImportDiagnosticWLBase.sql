﻿CREATE procedure [ETL].[ImportDiagnosticWLBase] as

--import the data
exec ETL.BuildDiagnosticWLBase


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[DiagnosticWL].[Base]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildDiagnosticWLBaseReference

﻿



CREATE procedure [ETL].[BuildAPCBaseEncounterReference]
	@ContextCode varchar(10)
as

-- 20150616	RR updated the process list proc to account for all episodes in spells where at least one episode has been updated.
--				Tested on data from 1/1/15 to 31/5/15 and all current merge recno's are coming through, plus an additional 3000ish episodes
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseEncounterReference
			where
				BaseEncounterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)

--Generate the process list
--delete
--from
--	APC.ProcessList
--where
--	exists
--	(
--	select
--		1
--	from
--		APC.BaseEncounterReference
--	where
--		BaseEncounterReference.MergeEncounterRecno = ProcessList.MergeRecno
--	and	BaseEncounterReference.ContextID = @ContextID
--	)

insert into	APC.ProcessList
	(
	MergeRecno
	)
select
	MergeRecno = BaseEncounter.MergeEncounterRecno
from
	APC.BaseEncounter
where
	BaseEncounter.Updated > @LastUpdated
and	BaseEncounter.ContextCode = @ContextCode
and not exists
		(
		select
			1
		from
			APC.ProcessList
		where
			ProcessList.MergeRecno = BaseEncounter.MergeEncounterRecno
		)

Insert into APC.ProcessList
	(
	MergeRecno
	)
Select
	MergeRecno = BaseEncounter.MergeEncounterRecno
from
	(
	Select distinct
		GlobalProviderSpellNo
	from	
		APC.ProcessList
	inner join APC.BaseEncounter
	on ProcessList.MergeRecno = BaseEncounter.MergeEncounterRecno
	) Spell

inner join APC.BaseEncounter
on Spell.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo

where
	not exists
		(
		Select
			1
		from
			APC.ProcessList Present
		where
			Present.MergeRecno = BaseEncounter.MergeEncounterRecno
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
	)
and	BaseEncounterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT

select
	 Encounter.MergeEncounterRecno
	,ContextID = Context.ContextID

	,AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID
	,AdmissionSourceID = AdmissionSource.SourceAdmissionSourceID
	,DischargeMethodID = DischargeMethod.SourceDischargeMethodID
	,DischargeDestinationID = DischargeDestination.SourceDischargeDestinationID
	,IntendedManagementID = IntendedManagement.SourceIntendedManagementID
	,ISTAdmissionSpecialtyID = ISTAdmissionSpecialty.SourceSpecialtyID
	,NeonatalLevelOfCareID = NeonatalLevelOfCare.SourceNeonatalLevelOfCareID
	,PatientClassificationID = PatientClassification.SourcePatientClassificationID
	,VTECategoryID = VTECategory.VTECategoryID
	,StartWardID = StartWard.SourceWardID
	,EndWardID = EndWard.SourceWardID
	,StartLocationID = StartLocation.SourceLocationID
	,EndLocationID = EndLocation.SourceLocationID
	,AgeID = Age.AgeID

	,AdmissionDateID =
		coalesce(
			 AdmissionDate.DateID

			,case
			when Encounter.AdmissionDate is null
			then NullDate.DateID

			when Encounter.AdmissionDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,DischargeDateID =
		coalesce(
			 DischargeDate.DateID

			,case
			when Encounter.DischargeDate is null
			then NullDate.DateID

			when Encounter.DischargeDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)


	,EpisodeStartDateID =
		coalesce(
			 EpisodeStartDate.DateID

			,case
			when Encounter.EpisodeStartDate is null
			then NullDate.DateID

			when Encounter.EpisodeStartDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)


	,EpisodeEndDateID =
		coalesce(
			 EpisodeEndDate.DateID

			,case
			when Encounter.EpisodeEndDate is null
			then NullDate.DateID

			when Encounter.EpisodeEndDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,ConsultantID = Consultant.SourceConsultantID
	,SexID = Sex.SourceSexID
	,StartSiteID = StartSite.SourceSiteID
	,EndSiteID = EndSite.SourceSiteID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,NHSNumberStatusID = NHSNumberStatus.SourceNHSNumberStatusID
	,EthnicOriginID = EthnicCategory.SourceEthnicCategoryID
	,AdminCategoryID = AdministrativeCategory.SourceAdministrativeCategoryID
	,LastEpisodeInSpellID = LastEpisodeInSpell.SourceLastEpisodeInSpellID
	,CCGID = coalesce(CCG.CCGID,0) -- just while we sort CCG reference data out - DG
	,ResidenceCCGID = ResidenceCCG.CCGID
	,ReligionID = Religion.SourceReligionID
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
into
	#Encounter
from
	APC.BaseEncounter Encounter

inner join WH.Context
on	Context.ContextCode = Encounter.ContextCode

inner join APC.AdmissionMethod AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodCode = coalesce(Encounter.AdmissionMethodCode, '-1')
and	AdmissionMethod.SourceContextCode = Encounter.ContextCode

inner join APC.AdmissionSource AdmissionSource
on	AdmissionSource.SourceAdmissionSourceCode = coalesce(Encounter.AdmissionSourceCode, '-1')
and	AdmissionSource.SourceContextCode = Encounter.ContextCode

inner join APC.DischargeMethod DischargeMethod
on	DischargeMethod.SourceDischargeMethodCode = coalesce(Encounter.DischargeMethodCode, '-1')
and	DischargeMethod.SourceContextCode = Encounter.ContextCode

inner join APC.DischargeDestination DischargeDestination
on	DischargeDestination.SourceDischargeDestinationCode = coalesce(Encounter.DischargeDestinationCode, '-1')
and	DischargeDestination.SourceContextCode = Encounter.ContextCode

inner join APC.IntendedManagement IntendedManagement
on	IntendedManagement.SourceIntendedManagementCode = coalesce(Encounter.ManagementIntentionCode, '-1')
and	IntendedManagement.SourceContextCode = Encounter.ContextCode

inner join WH.Specialty ISTAdmissionSpecialty
on	ISTAdmissionSpecialty.SourceSpecialtyCode = coalesce(Encounter.ISTAdmissionSpecialtyCode, '-1')
and	ISTAdmissionSpecialty.SourceContextCode = Encounter.ContextCode

inner join APC.NeonatalLevelOfCare NeonatalLevelOfCare
on	NeonatalLevelOfCare.SourceNeonatalLevelOfCareCode = coalesce(Encounter.NeonatalLevelOfCare, '-1')
and	NeonatalLevelOfCare.SourceContextCode = Encounter.ContextCode

inner join APC.PatientClassification PatientClassification
on	PatientClassification.SourcePatientClassificationCode = coalesce(Encounter.PatientClassificationCode, '-1')
and	PatientClassification.SourceContextCode = Encounter.ContextCode

inner join APC.VTECategory VTECategory
on	VTECategory.VTECategoryCode = coalesce(Encounter.VTECategoryCode, '-1')

inner join APC.Ward StartWard
on	StartWard.SourceWardCode = coalesce(Encounter.StartWardTypeCode, '-1')
and	StartWard.SourceContextCode = Encounter.ContextCode

inner join APC.Ward EndWard
on	EndWard.SourceWardCode = coalesce(Encounter.EndWardTypeCode, '-1')
and	EndWard.SourceContextCode = Encounter.ContextCode

inner join WH.Location StartLocation
on	StartLocation.SourceLocationCode = coalesce(Encounter.StartWardTypeCode, '-1')
and	StartLocation.SourceContextCode = Encounter.ContextCode

inner join WH.Location EndLocation
on	EndLocation.SourceLocationCode = coalesce(Encounter.EndWardTypeCode, '-1')
and	EndLocation.SourceContextCode = Encounter.ContextCode

inner join WH.Age
on	Age.AgeCode = Encounter.AgeCode

left join WH.Calendar AdmissionDate
on	AdmissionDate.TheDate = Encounter.AdmissionDate

left join WH.Calendar DischargeDate
on	DischargeDate.TheDate = Encounter.DischargeDate

left join WH.Calendar EpisodeStartDate
on	EpisodeStartDate.TheDate = Encounter.EpisodeStartDate

left join WH.Calendar EpisodeEndDate
on	EpisodeEndDate.TheDate = Encounter.EpisodeEndDate

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'

inner join WH.Consultant Consultant
on	Consultant.SourceConsultantCode = cast(coalesce(Encounter.ConsultantCode, '-1') as varchar)
and	Consultant.SourceContextCode = Encounter.ContextCode

inner join WH.Sex Sex
on	Sex.SourceSexCode = cast(coalesce(Encounter.SexCode, '-1') as varchar)
and	Sex.SourceContextCode = Encounter.ContextCode

inner join WH.Site StartSite
on	StartSite.SourceSiteCode = coalesce(Encounter.StartSiteCode, '-1')
and	StartSite.SourceContextCode = Encounter.ContextCode

inner join WH.Site EndSite
on	EndSite.SourceSiteCode = coalesce(Encounter.EndSiteCode, '-1')
and	EndSite.SourceContextCode = Encounter.ContextCode

inner join WH.Specialty Specialty
on	Specialty.SourceSpecialtyCode = coalesce(Encounter.SpecialtyCode, '-1')
and	Specialty.SourceContextCode = Encounter.ContextCode

inner join WH.AdministrativeCategory
on	AdministrativeCategory.SourceAdministrativeCategoryCode = coalesce(Encounter.AdminCategoryCode, '-1')
and	AdministrativeCategory.SourceContextCode = Encounter.ContextCode

inner join APC.LastEpisodeInSpell
on	LastEpisodeInSpell.SourceLastEpisodeInSpellCode = coalesce(Encounter.LastEpisodeInSpellIndicator, '-1')
and	LastEpisodeInSpell.SourceContextCode = Encounter.ContextCode

inner join WH.NHSNumberStatus
on	NHSNumberStatus.SourceNHSNumberStatusCode = coalesce(Encounter.NHSNumberStatusCode , '-1')
and	NHSNumberStatus.SourceContextCode = Encounter.ContextCode

inner join WH.EthnicCategory
on	EthnicCategory.SourceEthnicCategoryCode = coalesce(Encounter.EthnicOriginCode , '-1')
and	EthnicCategory.SourceContextCode = Encounter.ContextCode

left join WH.CCG
on	CCG.CCGCode = coalesce(Encounter.CCGCode , 'N/A')

left join WH.CCG ResidenceCCG
on	ResidenceCCG.CCGCode = coalesce(Encounter.ResidenceCCGCode , 'N/A')

inner join WH.Religion
on	Religion.SourceReligionCode = coalesce(Encounter.ReligionCode , '-1')
and	Religion.SourceContextCode = Encounter.ContextCode

where
	Encounter.Updated > @LastUpdated
and	Encounter.ContextCode = @ContextCode

CREATE UNIQUE CLUSTERED INDEX #IX_Encounter ON #Encounter
	(
	 MergeEncounterRecno
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


merge
	APC.BaseEncounterReference target
using
	(
	select
		 MergeEncounterRecno
		,ContextID
		,AdmissionMethodID
		,AdmissionSourceID
		,DischargeDestinationID
		,DischargeMethodID
		,IntendedManagementID
		,ISTAdmissionSpecialtyID
		,NeonatalLevelOfCareID
		,PatientClassificationID
		,VTECategoryID
		,StartWardID
		,EndWardID
		,StartLocationID 
		,EndLocationID 
		,AgeID
		,AdmissionDateID
		,DischargeDateID
		,EpisodeStartDateID
		,EpisodeEndDateID
		,ConsultantID
		,SexID
		,StartSiteID
		,EndSiteID
		,SpecialtyID
		,NHSNumberStatusID
		,EthnicOriginID
		,AdminCategoryID
		,LastEpisodeInSpellID
		,CCGID
		,ResidenceCCGID
		,ReligionID
		,Created
		,Updated
		,ByWhom
	from
		#Encounter
	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,AdmissionMethodID
			,AdmissionSourceID
			,DischargeDestinationID
			,DischargeMethodID
			,IntendedManagementID
			,ISTAdmissionSpecialtyID
			,NeonatalLevelOfCareID
			,PatientClassificationID
			,VTECategoryID
			,StartWardID
			,EndWardID
			,StartLocationID 
			,EndLocationID 
			,AgeID
			,AdmissionDateID
			,DischargeDateID
			,EpisodeStartDateID
			,EpisodeEndDateID
			,ConsultantID
			,SexID
			,StartSiteID
			,EndSiteID
			,SpecialtyID
			,NHSNumberStatusID
			,EthnicOriginID
			,AdminCategoryID
			,LastEpisodeInSpellID
			,CCGID
			,ResidenceCCGID
			,ReligionID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.AdmissionMethodID
			,source.AdmissionSourceID
			,source.DischargeDestinationID
			,source.DischargeMethodID
			,source.IntendedManagementID
			,source.ISTAdmissionSpecialtyID
			,source.NeonatalLevelOfCareID
			,source.PatientClassificationID
			,source.VTECategoryID
			,source.StartWardID
			,source.EndWardID
			,source.StartLocationID 
			,source.EndLocationID 
			,source.AgeID
			,source.AdmissionDateID
			,source.DischargeDateID
			,source.EpisodeStartDateID
			,source.EpisodeEndDateID
			,source.ConsultantID
			,source.SexID
			,source.StartSiteID
			,source.EndSiteID
			,source.SpecialtyID
			,source.NHSNumberStatusID
			,source.EthnicOriginID
			,source.AdminCategoryID
			,source.LastEpisodeInSpellID
			,source.CCGID
			,source.ResidenceCCGID
			,source.ReligionID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.AdmissionMethodID = source.AdmissionMethodID
			,target.AdmissionSourceID = source.AdmissionSourceID
			,target.DischargeDestinationID = source.DischargeDestinationID
			,target.DischargeMethodID = source.DischargeMethodID
			,target.IntendedManagementID = source.IntendedManagementID
			,target.ISTAdmissionSpecialtyID = source.ISTAdmissionSpecialtyID
			,target.NeonatalLevelOfCareID = source.NeonatalLevelOfCareID
			,target.PatientClassificationID = source.PatientClassificationID
			,target.VTECategoryID = source.VTECategoryID
			,target.StartWardID = source.StartWardID
			,target.EndWardID = source.EndWardID
			,target.StartLocationID = source.StartLocationID
			,target.EndLocationID = source.EndLocationID
			,target.AgeID = source.AgeID
			,target.AdmissionDateID = source.AdmissionDateID
			,target.DischargeDateID = source.DischargeDateID
			,target.EpisodeStartDateID = source.EpisodeStartDateID
			,target.EpisodeEndDateID = source.EpisodeEndDateID
			,target.ConsultantID = source.ConsultantID
			,target.SexID = source.SexID
			,target.StartSiteID = source.StartSiteID
			,target.EndSiteID = source.EndSiteID
			,target.SpecialtyID = source.SpecialtyID
			,target.NHSNumberStatusID = source.NHSNumberStatusID
			,target.EthnicOriginID = source.EthnicOriginID
			,target.AdminCategoryID = source.AdminCategoryID
			,target.LastEpisodeInSpellID = source.LastEpisodeInSpellID
			,target.CCGID = source.CCGID
			,target.ResidenceCCGID = source.ResidenceCCGID
			,target.ReligionID = source.ReligionID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





﻿CREATE procedure [ETL].[BuildCasenoteBaseLoanReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	Casenote.BaseLoanReference
where
	not exists
	(
	select
		1
	from
		Casenote.BaseLoan
	where
		BaseLoan.MergeRecno = BaseLoanReference.MergeRecno
	)
and	BaseLoanReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


select
	 BaseLoan.MergeRecno
	,ContextID = Context.ContextID
	,BorrowerID = Borrower.SourceBorrowerID
	,SystemUserID = SystemUser.SystemUserID
into
	#BaseLoan
from
	Casenote.BaseLoan

inner join WH.Context
on	Context.ContextCode = BaseLoan.ContextCode

inner join Casenote.Borrower
on	Borrower.SourceBorrowerCode = coalesce(BaseLoan.BorrowerCode, '-1')
and	Borrower.SourceContextCode = BaseLoan.ContextCode

inner join WH.SystemUser
on	SystemUser.UserID = coalesce(BaseLoan.UserId, '-1')
and	SystemUser.ContextCode = BaseLoan.ContextCode

where
	BaseLoan.ContextCode = @ContextCode

CREATE UNIQUE CLUSTERED INDEX #IX_BaseLoan ON #BaseLoan
	(
	 MergeRecno
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


merge
	Casenote.BaseLoanReference target
using
	(
	select
		 MergeRecno
		,ContextID
		,BorrowerID
		,SystemUserID
	from
		#BaseLoan
	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			 MergeRecno
			,ContextID
			,BorrowerID
			,SystemUserID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.BorrowerID
			,source.SystemUserID
			)

	when matched
	and not
		(
			target.BorrowerID = source.BorrowerID
		and	target.SystemUserID = source.SystemUserID
		)
	then
		update
		set
			 target.BorrowerID = source.BorrowerID
			,target.SystemUserID = source.SystemUserID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





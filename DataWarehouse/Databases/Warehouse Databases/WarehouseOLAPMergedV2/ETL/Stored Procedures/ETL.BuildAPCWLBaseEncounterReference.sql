﻿CREATE procedure [ETL].[BuildAPCWLBaseEncounterReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APCWL.BaseEncounterReference
			where
				BaseEncounterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APCWL.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		APCWL.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
	)
and	BaseEncounterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APCWL.BaseEncounterReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,AgeID = Age.AgeID
		,SexID = Sex.SourceSexID
		,EthnicOriginID = EthnicCategory.SourceEthnicCategoryID
		,MaritalStatusID = MaritalStatus.SourceMaritalStatusID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,PASSpecialtyID = PASSpecialty.SourceSpecialtyID
		,EpisodicSpecialtyID = EpisodicSpecialty.SourceSpecialtyID
		,RTTSpecialtyID = RTTSpecialty.SourceSpecialtyID
		,IntendedManagementID = IntendedManagement.SourceIntendedManagementID
		,AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID
		,PriorityID = Priority.SourcePriorityID
		,RTTCurrentStatusID = RTTCurrentStatus.SourceRTTStatusID
		,ConsultantID = Consultant.SourceConsultantID
		,SiteID = Site.SourceSiteID
		,WardID = Ward.SourceWardID
		,WaitingListID = WaitingList.SourceWaitingListID
		,AdminCategoryID = AdministrativeCategory.SourceAdministrativeCategoryID

		,CensusDateID =
			coalesce(
				 CensusDate.DateID
				,NullDate.DateID
			)

		,TCIDateID =
			coalesce(
				 TCIDate.DateID

				,case
				when Encounter.TCIDate is null
				then NullDate.DateID

				when Encounter.TCIDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,WaitTypeID = WaitType.WaitTypeID

		,DurationID = Duration.DurationID
		,TCIDateDurationID = TCIDateDuration.DurationID

		,WaitStatusID = WaitStatus.WaitStatusID
		,ReligionID = Religion.SourceReligionID

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		APCWL.BaseEncounter Encounter

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	inner join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	inner join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.SexCode, '-1') as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	inner join WH.EthnicCategory EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryCode = cast(coalesce(Encounter.EthnicOriginCode, '-1') as varchar)
	and	EthnicCategory.SourceContextCode = Encounter.ContextCode

	inner join WH.MaritalStatus MaritalStatus
	on	MaritalStatus.SourceMaritalStatusCode = cast(coalesce(Encounter.MaritalStatusCode, '-1') as varchar)
	and	MaritalStatus.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(Encounter.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty PASSpecialty
	on	PASSpecialty.SourceSpecialtyCode = coalesce(Encounter.PASSpecialtyCode, '-1')
	and	PASSpecialty.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty EpisodicSpecialty
	on	EpisodicSpecialty.SourceSpecialtyCode = coalesce(Encounter.EpisodeSpecialtyCode, '-1')
	and	EpisodicSpecialty.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty RTTSpecialty
	on	RTTSpecialty.SourceSpecialtyCode = coalesce(Encounter.RTTSpecialtyCode, '-1')
	and	RTTSpecialty.SourceContextCode = Encounter.ContextCode

	inner join APC.IntendedManagement IntendedManagement
	on	IntendedManagement.SourceIntendedManagementCode = coalesce(Encounter.ManagementIntentionCode, '-1')
	and	IntendedManagement.SourceContextCode = Encounter.ContextCode

	inner join APC.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.SourceAdmissionMethodCode = coalesce(Encounter.AdmissionMethodCode, '-1')
	and	AdmissionMethod.SourceContextCode = Encounter.ContextCode

	inner join APCWL.Priority Priority
	on	Priority.SourcePriorityCode = coalesce(Encounter.PriorityCode, '-1')
	and	Priority.SourceContextCode = Encounter.ContextCode

	inner join WH.RTTStatus RTTCurrentStatus
	on	RTTCurrentStatus.SourceRTTStatusCode = coalesce(Encounter.RTTCurrentStatusCode, '-1')
	and	RTTCurrentStatus.SourceContextCode = Encounter.ContextCode

	inner join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = cast(coalesce(Encounter.ConsultantCode, '-1') as varchar)
	and	Consultant.SourceContextCode = Encounter.ContextCode

	inner join WH.Site
	on	Site.SourceSiteCode = coalesce(Encounter.SiteCode, '-1')
	and	Site.SourceContextCode = Encounter.ContextCode

	inner join APC.Ward Ward
	on	Ward.SourceWardCode = coalesce(Encounter.WardCode, '-1')
	and	Ward.SourceContextCode = Encounter.ContextCode

	inner join APCWL.WaitingList WaitingList
	on	WaitingList.SourceWaitingListCode = coalesce(Encounter.WaitingListCode, '-1')
	and	WaitingList.SourceContextCode = Encounter.ContextCode

	inner join WH.AdministrativeCategory
	on	AdministrativeCategory.SourceAdministrativeCategoryCode = coalesce(Encounter.AdminCategoryCode, '-1')
	and	AdministrativeCategory.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar CensusDate
	on	CensusDate.TheDate = Encounter.CensusDate

	left join WH.Calendar TCIDate
	on	TCIDate.TheDate = Encounter.TCIDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	inner join APCWL.WaitType WaitType
	on	WaitType.WaitTypeID = coalesce(Encounter.WaitTypeCode, 10)

	inner join WH.Duration Duration
	on	Duration.DurationCode = coalesce(Encounter.DurationCode, 'NA')

	inner join WH.Duration TCIDateDuration
	on	TCIDateDuration.DurationCode = coalesce(Encounter.DurationAtTCIDateCode, 'NA')

	inner join WH.WaitStatus
	on	WaitStatus.WaitStatusCode = Encounter.StatusCode

	inner join WH.Religion Religion
	on	Religion.SourceReligionCode = coalesce(Encounter.ReligionCode, '-1')
	and	Religion.SourceContextCode = Encounter.ContextCode

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,AgeID
			,SexID
			,EthnicOriginID
			,MaritalStatusID
			,SpecialtyID
			,PASSpecialtyID
			,EpisodicSpecialtyID
			,RTTSpecialtyID
			,IntendedManagementID
			,AdmissionMethodID
			,PriorityID
			,RTTCurrentStatusID
			,ConsultantID
			,SiteID
			,WardID
			,WaitingListID
			,AdminCategoryID
			,CensusDateID
			,TCIDateID
			,WaitTypeID
			,DurationID
			,TCIDateDurationID
			,WaitStatusID
			,ReligionID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.AgeID
			,source.SexID
			,source.EthnicOriginID
			,source.MaritalStatusID
			,source.SpecialtyID
			,source.PASSpecialtyID
			,source.EpisodicSpecialtyID
			,source.RTTSpecialtyID
			,source.IntendedManagementID
			,source.AdmissionMethodID
			,source.PriorityID
			,source.RTTCurrentStatusID
			,source.ConsultantID
			,source.SiteID
			,source.WardID
			,source.WaitingListID
			,source.AdminCategoryID
			,source.CensusDateID
			,source.TCIDateID
			,source.WaitTypeID
			,source.DurationID
			,source.TCIDateDurationID
			,source.WaitStatusID
			,source.ReligionID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.AgeID = source.AgeID
			,target.SexID = source.SexID
			,target.EthnicOriginID = source.EthnicOriginID
			,target.MaritalStatusID = source.MaritalStatusID
			,target.SpecialtyID = source.SpecialtyID
			,target.PASSpecialtyID = source.PASSpecialtyID
			,target.EpisodicSpecialtyID = source.EpisodicSpecialtyID
			,target.RTTSpecialtyID = source.RTTSpecialtyID
			,target.IntendedManagementID = source.IntendedManagementID
			,target.AdmissionMethodID = source.AdmissionMethodID
			,target.PriorityID = source.PriorityID
			,target.RTTCurrentStatusID = source.RTTCurrentStatusID
			,target.ConsultantID = source.ConsultantID
			,target.SiteID = source.SiteID
			,target.WardID = source.WardID
			,target.WaitingListID = source.WaitingListID
			,target.AdminCategoryID = source.AdminCategoryID
			,target.CensusDateID = source.CensusDateID
			,target.TCIDateID = source.TCIDateID
			,target.WaitTypeID = source.WaitTypeID
			,target.DurationID = source.DurationID
			,target.TCIDateDurationID = source.TCIDateDurationID
			,target.WaitStatusID = source.WaitStatusID
			,target.ReligionID = source.ReligionID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

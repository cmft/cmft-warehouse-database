﻿
CREATE procedure [ETL].[BuildCenOPWLBaseEncounter]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

--prepare list of valid snapshot dates
select distinct
	CensusDate = Calendar.TheDate
into
	#ValidSnapshotDate
from
	WH.Calendar
where
	Calendar.TheDate in
		(
		select
			max(TheDate)
		from
			WH.Calendar
		where
			TheDate between DATEADD(year, -2, getdate()) and getdate()
		group by
			 year(TheDate)
			,month(TheDate)

		union
--snapshots in the last fourteen days
		select
			TheDate
		from
			WH.Calendar
		where
			TheDate between dateadd(day , -14 , getdate()) and getdate()

		union
--Sunday snapshots
		select
			TheDate
		from
			WH.Calendar
		where
			datename(dw, TheDate) = 'Sunday'
		and	TheDate between DATEADD(year, -2, getdate()) and getdate()
		)


--clear out old snapshots
delete
from
	OPWL.BaseEncounter
where 
	not exists
	(
	select
		1
	from
		#ValidSnapshotDate Calendar
	where
		Calendar.CensusDate = BaseEncounter.CensusDate
	)
and	BaseEncounter.ContextCode =  'CEN||PAS'

select @deleted = @@ROWCOUNT


--this prevents updating of snapshots
delete
from
	#ValidSnapshotDate
where
	exists
	(
	select
		1
	from
		OPWL.BaseEncounter
	where
		BaseEncounter.CensusDate = #ValidSnapshotDate.CensusDate
	and BaseEncounter.ContextCode =  'CEN||PAS'
	)

select
	Encounter.*
	,EncounterChecksum =
		checksum
			(
			 Encounter.AdditionFlag
			,Encounter.AdminCategoryCode
			,Encounter.AdmissionMethodCode
			,Encounter.AppointmentCategoryCode
			,Encounter.AppointmentDate
			,Encounter.AppointmentDoctor
			,Encounter.AppointmentID
			,Encounter.AppointmentStatusCode
			,Encounter.AppointmentTime
			,Encounter.AppointmentTypeCode
			,Encounter.BookedDate
			,Encounter.BookingTypeCode
			,Encounter.BreachDate
			,Encounter.BreachDays
			,Encounter.BreachTypeCode
			,Encounter.CancelledBy
			,Encounter.CasenoteNumber
			,Encounter.CensusDate
			,Encounter.ClinicCode
			,Encounter.ClockStartDate
			,Encounter.CommentClinical
			,Encounter.CommentNonClinical
			,Encounter.ContextCode
			,Encounter.ContractSerialNumber
			,Encounter.CountOfCNDs
			,Encounter.CountOfDaysSuspended
			,Encounter.CountOfDNAs
			,Encounter.CountOfHospitalCancels
			,Encounter.CountOfPatientCancels
			,Encounter.DateOfBirth
			,Encounter.DateOfDeath
			,Encounter.DateOnWaitingList
			,Encounter.DerivedBreachDate
			,Encounter.DerivedBreachDays
			,Encounter.DerivedClockStartDate
			,Encounter.DHACode
			,Encounter.DistrictNo
			,Encounter.EpisodeNo
			,Encounter.EpisodicGpCode
			,Encounter.EncounterRecno
			,Encounter.EthnicOriginCode
			,Encounter.FirstAttendanceCode
			,Encounter.ExpectedAdmissionDate
			,Encounter.FutureCancellationDate
			,Encounter.FuturePatientCancelDate
			,Encounter.HomePhone
			,Encounter.IntendedPrimaryOperationCode
			,Encounter.InterfaceCode
			,Encounter.LastAppointmentFlag
			,Encounter.LocalEpisodicGpCode
			,Encounter.LocalRegisteredGpCode
			,Encounter.ManagementIntentionCode
			,Encounter.MaritalStatusCode
			,Encounter.MRSA
			,Encounter.NationalBreachDate
			,Encounter.NationalBreachDays
			,Encounter.NationalDiagnosticBreachDate
			,Encounter.NationalSpecialtyCode
			,Encounter.NextOfKinHomePhone
			,Encounter.NextOfKinName
			,Encounter.NextOfKinRelationship
			,Encounter.NextOfKinWorkPhone
			,Encounter.NHSNumber
			,Encounter.Operation
			,Encounter.OriginalDateOnWaitingList
			,Encounter.PASSpecialtyCode
			,Encounter.PatientChoice
			,Encounter.PatientDeathIndicator
			,Encounter.PatientForename
			,Encounter.PatientAddress1
			,Encounter.PatientAddress2
			,Encounter.PatientAddress3
			,Encounter.PatientAddress4
			,Encounter.PatientSurname
			,Encounter.PatientTitle
			,Encounter.PCTCode
			,Encounter.Postcode
			,Encounter.PriorityCode
			,Encounter.ProviderCode
			,Encounter.PurchaserCode
			,Encounter.QM08EndWaitDate
			,Encounter.QM08StartWaitDate
			,Encounter.ReferralDate
			,Encounter.RegisteredGpCode
			,Encounter.ReligionCode
			,Encounter.ReportCategoryCode
			,Encounter.RTTBreachDate
			,Encounter.RTTCurrentPrivatePatientFlag
			,Encounter.RTTCurrentProviderCode
			,Encounter.RTTCurrentStatusCode
			,Encounter.RTTCurrentStatusDate
			,Encounter.RTTDiagnosticBreachDate
			,Encounter.RTTEndDate
			,Encounter.RTTOverseasStatusFlag
			,Encounter.RTTPathwayCondition
			,Encounter.RTTPathwayID
			,Encounter.RTTSpecialtyCode
			,Encounter.RTTStartDate
			,Encounter.SexCode
			,Encounter.SiteCode
			,Encounter.SourceEncounterNo
			,Encounter.SourceOfReferralCode
			,Encounter.SourcePatientNo
			,Encounter.SourceTreatmentFunctionCode
			,Encounter.SourceUniqueID
			,Encounter.SpecialtyTypeCode
			,Encounter.SuspensionEndDate
			,Encounter.SuspensionReason
			,Encounter.SuspensionReasonCode
			,Encounter.SuspensionStartDate
			,Encounter.TreatmentFunctionCode
			,Encounter.WaitingListCode
			,Encounter.WardCode
			,Encounter.WeeksWaiting
			,Encounter.WLStatus
			,Encounter.WorkPhone
			,Encounter.Cases
			,Encounter.ConsultantCode
			,Encounter.SpecialtyCode
			,Encounter.AgeCode
			,Encounter.TCIDate
			,Encounter.LengthOfWait
			,Encounter.DurationCode
			,Encounter.RegisteredGpPracticeCode
			,Encounter.EpisodicGpPracticeCode
			,Encounter.WaitTypeCode
			,Encounter.StatusCode
			,Encounter.CategoryCode
			,Encounter.OPCSCoded
			,Encounter.WithRTTStartDate
			,Encounter.WithRTTStatusCode
			,Encounter.WithExpectedAdmissionDate
			,Encounter.RTTActivity
			,Encounter.RTTBreachStatusCode
			,Encounter.DiagnosticProcedure
			,Encounter.DirectorateCode
			,Encounter.WithAppointmentDate
			,Encounter.WithRTTOpenPathway
			,Encounter.RTTPathwayStartDateCurrent
			,Encounter.RTTWeekBandReturnCode
			,Encounter.RTTDaysWaiting
			,Encounter.DurationAtAppointmentDateCode
			,Encounter.CCGCode
			,Encounter.ResidenceCCGCode
			,Encounter.DataSourceID
		)
into
	#Encounter
from
	(
	select
		 AdditionFlag
		,AdminCategoryCode = cast(AdminCategoryCode as varchar(10))
		,AdmissionMethodCode = cast(AdmissionMethodCode as varchar(2))
		,AppointmentCategoryCode = cast(AppointmentCategoryCode as varchar(4))
		,AppointmentDate = cast(AppointmentDate as smalldatetime)
		,AppointmentDoctor = cast(AppointmentDoctor as varchar(8))
		,AppointmentID = cast(AppointmentID as varchar(50))
		,AppointmentStatusCode = cast(AppointmentStatusCode as varchar(4))
		,AppointmentTime = cast(AppointmentTime as smalldatetime)
		,AppointmentTypeCode = cast(AppointmentTypeCode as varchar(5))
		,BookedDate = cast(BookedDate as smalldatetime)
		,BookingTypeCode = cast(BookingTypeCode as varchar(4))
		,BreachDate = cast(BreachDate as smalldatetime)
		,BreachDays = cast(BreachDays as int)
		,BreachTypeCode = cast(BreachTypeCode as varchar(10))
		,CancelledBy = cast(CancelledBy as varchar(3))
		,CasenoteNumber = cast(CasenoteNumber as varchar(14))
		,CensusDate = cast(Encounter.CensusDate as smalldatetime)
		,ClinicCode = cast(ClinicCode as varchar(15))
		,ClockStartDate = cast(ClockStartDate as smalldatetime)
		,CommentClinical = cast(CommentClinical as varchar(60))
		,CommentNonClinical = cast(CommentNonClinical as varchar(60))
		,ContextCode = cast(ContextCode as varchar(8))
		,ContractSerialNumber = cast(ContractSerialNumber as varchar(6))
		,CountOfCNDs = cast(CountOfCNDs as int)
		,CountOfDaysSuspended = cast(CountOfDaysSuspended as varchar(4))
		,CountOfDNAs = cast(CountOfDNAs as int)
		,CountOfHospitalCancels = cast(CountOfHospitalCancels as int)
		,CountOfPatientCancels = cast(CountOfPatientCancels as int)
		,DateOfBirth = cast(DateOfBirth as datetime)
		,DateOfDeath = cast(DateOfDeath as smalldatetime)
		,DateOnWaitingList = cast(DateOnWaitingList as smalldatetime)
		,DerivedBreachDate = cast(DerivedBreachDate as smalldatetime)
		,DerivedBreachDays = cast(DerivedBreachDays as int)
		,DerivedClockStartDate = cast(DerivedClockStartDate as smalldatetime)
		,DHACode = cast(DHACode as varchar(3))
		,DistrictNo = cast(DistrictNo as varchar(14))
		,EpisodeNo = cast(EpisodeNo as smallint)
		,EpisodicGpCode = cast(EpisodicGpCode as varchar(8))
		,EncounterRecno = cast(EncounterRecno as int)
		,EthnicOriginCode = cast(EthnicOriginCode as varchar(4))
		,FirstAttendanceCode = cast(FirstAttendanceCode as int)
		,ExpectedAdmissionDate = cast(ExpectedAdmissionDate as smalldatetime)
		,FutureCancellationDate = cast(FutureCancellationDate as smalldatetime)
		,FuturePatientCancelDate = cast(FuturePatientCancelDate as smalldatetime)
		,HomePhone = cast(HomePhone as varchar(23))
		,IntendedPrimaryOperationCode = cast(IntendedPrimaryOperationCode as varchar(7))
		,InterfaceCode = cast(InterfaceCode as varchar(5))
		,LastAppointmentFlag = cast(LastAppointmentFlag as bit)
		,LocalEpisodicGpCode = cast(LocalEpisodicGpCode as varchar(8))
		,LocalRegisteredGpCode = cast(LocalRegisteredGpCode as varchar(8))
		,ManagementIntentionCode = cast(ManagementIntentionCode as varchar(1))
		,MaritalStatusCode = cast(MaritalStatusCode as varchar(1))
		,MRSA = cast(MRSA as varchar(10))
		,NationalBreachDate = cast(NationalBreachDate as smalldatetime)
		,NationalBreachDays = cast(NationalBreachDays as int)
		,NationalDiagnosticBreachDate = cast(NationalDiagnosticBreachDate as smalldatetime)
		,NationalSpecialtyCode = cast(NationalSpecialtyCode as varchar(10))
		,NextOfKinHomePhone = cast(NextOfKinHomePhone as varchar(23))
		,NextOfKinName = cast(NextOfKinName as varchar(40))
		,NextOfKinRelationship = cast(NextOfKinRelationship as varchar(9))
		,NextOfKinWorkPhone = cast(NextOfKinWorkPhone as varchar(23))
		,NHSNumber = cast(NHSNumber as varchar(17))
		,Operation = cast(Operation as varchar(60))
		,OriginalDateOnWaitingList = cast(OriginalDateOnWaitingList as smalldatetime)
		,PASSpecialtyCode = cast(PASSpecialtyCode as varchar(8))
		,PatientChoice = cast(PatientChoice as varchar(3))
		,PatientDeathIndicator = cast(PatientDeathIndicator as int)
		,PatientForename = cast(PatientForename as varchar(20))
		,PatientAddress1 = cast(PatientAddress1 as varchar(25))
		,PatientAddress2 = cast(PatientAddress2 as varchar(25))
		,PatientAddress3 = cast(PatientAddress3 as varchar(25))
		,PatientAddress4 = cast(PatientAddress4 as varchar(25))
		,PatientSurname = cast(PatientSurname as varchar(30))
		,PatientTitle = cast(PatientTitle as varchar(10))
		,PCTCode = cast(PCTCode as varchar(8))
		,Postcode = cast(Postcode as varchar(10))
		,PriorityCode = cast(PriorityCode as varchar(2))
		,ProviderCode = cast(ProviderCode as varchar(8))
		,PurchaserCode = cast(PurchaserCode as varchar(8))
		,QM08EndWaitDate = cast(QM08EndWaitDate as smalldatetime)
		,QM08StartWaitDate = cast(QM08StartWaitDate as smalldatetime)
		,ReferralDate = cast(ReferralDate as smalldatetime)
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(8))
		,ReligionCode = cast(ReligionCode as varchar(4))
		,ReportCategoryCode = cast(ReportCategoryCode as varchar(8))
		,RTTBreachDate = cast(RTTBreachDate as smalldatetime)
		,RTTCurrentPrivatePatientFlag = cast(RTTCurrentPrivatePatientFlag as bit)
		,RTTCurrentProviderCode = cast(RTTCurrentProviderCode as varchar(10))
		,RTTCurrentStatusCode = cast(RTTCurrentStatusCode as varchar(10))
		,RTTCurrentStatusDate = cast(RTTCurrentStatusDate as smalldatetime)
		,RTTDiagnosticBreachDate = cast(RTTDiagnosticBreachDate as smalldatetime)
		,RTTEndDate = cast(RTTEndDate as smalldatetime)
		,RTTOverseasStatusFlag = cast(RTTOverseasStatusFlag as bit)
		,RTTPathwayCondition = cast(RTTPathwayCondition as varchar(20))
		,RTTPathwayID = cast(RTTPathwayID as varchar(25))
		,RTTSpecialtyCode = cast(RTTSpecialtyCode as varchar(10))
		,RTTStartDate = cast(RTTStartDate as smalldatetime)
		,SexCode = cast(SexCode as varchar(1))
		,SiteCode = cast(SiteCode as varchar(4))
		,SourceEncounterNo = cast(SourceEncounterNo as varchar(50))
		,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(4))
		,SourcePatientNo = cast(SourcePatientNo as varchar(20))
		,SourceTreatmentFunctionCode = cast(SourceTreatmentFunctionCode as int)
		,SourceUniqueID = cast(SourceUniqueID as varchar(255))
		,SpecialtyTypeCode = cast(SpecialtyTypeCode as varchar(1))
		,SuspensionEndDate = cast(SuspensionEndDate as smalldatetime)
		,SuspensionReason = cast(SuspensionReason as varchar(30))
		,SuspensionReasonCode = cast(SuspensionReasonCode as varchar(4))
		,SuspensionStartDate = cast(SuspensionStartDate as smalldatetime)
		,TreatmentFunctionCode = cast(TreatmentFunctionCode as int)
		,WaitingListCode = cast(WaitingListCode as varchar(8))
		,WardCode = cast(WardCode as varchar(4))
		,WeeksWaiting = cast(WeeksWaiting as int)
		,WLStatus = cast(WLStatus as varchar(20))
		,WorkPhone = cast(WorkPhone as varchar(23))
		,Cases = cast(Cases as int)
		,ConsultantCode = cast(ConsultantCode as varchar(20))
		,SpecialtyCode = cast(SpecialtyCode as varchar(4))
		,AgeCode = cast(AgeCode as varchar(27))
		,TCIDate = cast(TCIDate as smalldatetime)
		,LengthOfWait = cast(LengthOfWait as int)
		,DurationCode = cast(DurationCode as varchar(3))
		,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(8))
		,EpisodicGpPracticeCode = cast(EpisodicGpPracticeCode as varchar(8))
		,WaitTypeCode = cast(WaitTypeCode as varchar(2))
		,StatusCode = cast(StatusCode as varchar(2))
		,CategoryCode = cast(CategoryCode as varchar(2))
		,OPCSCoded = cast(OPCSCoded as int)
		,WithRTTStartDate = cast(WithRTTStartDate as int)
		,WithRTTStatusCode = cast(WithRTTStatusCode as int)
		,WithExpectedAdmissionDate = cast(WithExpectedAdmissionDate as int)
		,RTTActivity = cast(RTTActivity as bit)
		,RTTBreachStatusCode = cast(RTTBreachStatusCode as varchar(1))
		,DiagnosticProcedure = cast(DiagnosticProcedure as bit)
		,DirectorateCode = cast(DirectorateCode as varchar(5))
		,WithAppointmentDate = cast(WithAppointmentDate as int)
		,WithRTTOpenPathway = cast(WithRTTOpenPathway as int)
		,RTTPathwayStartDateCurrent = cast(RTTPathwayStartDateCurrent as datetime)
		,RTTWeekBandReturnCode = cast(RTTWeekBandReturnCode as varchar(5))
		,RTTDaysWaiting = cast(RTTDaysWaiting as int)
		,DurationAtAppointmentDateCode = cast(DurationAtAppointmentDateCode as varchar(3))
		,CCGCode = cast(CCGCode as varchar(10))
		,ResidenceCCGCode = cast(ResidenceCCGCode as varchar(10))
		,DataSourceID = cast(DataSourceID as int)
	from
		ETL.TLoadCenOPWLBaseEncounter Encounter
		
	inner join #ValidSnapshotDate
	on	#ValidSnapshotDate.CensusDate = Encounter.CensusDate
	
	) Encounter
	;


--add index
create unique clustered index #IX_Encounter on #Encounter (ContextCode, DataSourceID, EncounterRecno);


merge
	OPWL.BaseEncounter target
using
	#Encounter source
on	source.ContextCode = target.ContextCode
and	source.DataSourceID = target.DataSourceID
and	source.EncounterRecno = target.EncounterRecno


when not matched
then
	insert
		(
		 AdditionFlag
		,AdminCategoryCode
		,AdmissionMethodCode
		,AppointmentCategoryCode
		,AppointmentDate
		,AppointmentDoctor
		,AppointmentID
		,AppointmentStatusCode
		,AppointmentTime
		,AppointmentTypeCode
		,BookedDate
		,BookingTypeCode
		,BreachDate
		,BreachDays
		,BreachTypeCode
		,CancelledBy
		,CasenoteNumber
		,CensusDate
		,ClinicCode
		,ClockStartDate
		,CommentClinical
		,CommentNonClinical
		,ContextCode
		,ContractSerialNumber
		,CountOfCNDs
		,CountOfDaysSuspended
		,CountOfDNAs
		,CountOfHospitalCancels
		,CountOfPatientCancels
		,DateOfBirth
		,DateOfDeath
		,DateOnWaitingList
		,DerivedBreachDate
		,DerivedBreachDays
		,DerivedClockStartDate
		,DHACode
		,DistrictNo
		,EpisodeNo
		,EpisodicGpCode
		,EncounterRecno
		,EthnicOriginCode
		,FirstAttendanceCode
		,ExpectedAdmissionDate
		,FutureCancellationDate
		,FuturePatientCancelDate
		,HomePhone
		,IntendedPrimaryOperationCode
		,InterfaceCode
		,LastAppointmentFlag
		,LocalEpisodicGpCode
		,LocalRegisteredGpCode
		,ManagementIntentionCode
		,MaritalStatusCode
		,MRSA
		,NationalBreachDate
		,NationalBreachDays
		,NationalDiagnosticBreachDate
		,NationalSpecialtyCode
		,NextOfKinHomePhone
		,NextOfKinName
		,NextOfKinRelationship
		,NextOfKinWorkPhone
		,NHSNumber
		,Operation
		,OriginalDateOnWaitingList
		,PASSpecialtyCode
		,PatientChoice
		,PatientDeathIndicator
		,PatientForename
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,PatientSurname
		,PatientTitle
		,PCTCode
		,Postcode
		,PriorityCode
		,ProviderCode
		,PurchaserCode
		,QM08EndWaitDate
		,QM08StartWaitDate
		,ReferralDate
		,RegisteredGpCode
		,ReligionCode
		,ReportCategoryCode
		,RTTBreachDate
		,RTTCurrentPrivatePatientFlag
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTDiagnosticBreachDate
		,RTTEndDate
		,RTTOverseasStatusFlag
		,RTTPathwayCondition
		,RTTPathwayID
		,RTTSpecialtyCode
		,RTTStartDate
		,SexCode
		,SiteCode
		,SourceEncounterNo
		,SourceOfReferralCode
		,SourcePatientNo
		,SourceTreatmentFunctionCode
		,SourceUniqueID
		,SpecialtyTypeCode
		,SuspensionEndDate
		,SuspensionReason
		,SuspensionReasonCode
		,SuspensionStartDate
		,TreatmentFunctionCode
		,WaitingListCode
		,WardCode
		,WeeksWaiting
		,WLStatus
		,WorkPhone
		,Cases
		,ConsultantCode
		,SpecialtyCode
		,AgeCode
		,TCIDate
		,LengthOfWait
		,DurationCode
		,RegisteredGpPracticeCode
		,EpisodicGpPracticeCode
		,WaitTypeCode
		,StatusCode
		,CategoryCode
		,OPCSCoded
		,WithRTTStartDate
		,WithRTTStatusCode
		,WithExpectedAdmissionDate
		,RTTActivity
		,RTTBreachStatusCode
		,DiagnosticProcedure
		,DirectorateCode
		,WithAppointmentDate
		,WithRTTOpenPathway
		,RTTPathwayStartDateCurrent
		,RTTWeekBandReturnCode
		,RTTDaysWaiting
		,DurationAtAppointmentDateCode
		,CCGCode
		,ResidenceCCGCode
		,DataSourceID
		,Created
		,Updated
		,ByWhom
		)
	values
		(
		 source.AdditionFlag
		,source.AdminCategoryCode
		,source.AdmissionMethodCode
		,source.AppointmentCategoryCode
		,source.AppointmentDate
		,source.AppointmentDoctor
		,source.AppointmentID
		,source.AppointmentStatusCode
		,source.AppointmentTime
		,source.AppointmentTypeCode
		,source.BookedDate
		,source.BookingTypeCode
		,source.BreachDate
		,source.BreachDays
		,source.BreachTypeCode
		,source.CancelledBy
		,source.CasenoteNumber
		,source.CensusDate
		,source.ClinicCode
		,source.ClockStartDate
		,source.CommentClinical
		,source.CommentNonClinical
		,source.ContextCode
		,source.ContractSerialNumber
		,source.CountOfCNDs
		,source.CountOfDaysSuspended
		,source.CountOfDNAs
		,source.CountOfHospitalCancels
		,source.CountOfPatientCancels
		,source.DateOfBirth
		,source.DateOfDeath
		,source.DateOnWaitingList
		,source.DerivedBreachDate
		,source.DerivedBreachDays
		,source.DerivedClockStartDate
		,source.DHACode
		,source.DistrictNo
		,source.EpisodeNo
		,source.EpisodicGpCode
		,source.EncounterRecno
		,source.EthnicOriginCode
		,source.FirstAttendanceCode
		,source.ExpectedAdmissionDate
		,source.FutureCancellationDate
		,source.FuturePatientCancelDate
		,source.HomePhone
		,source.IntendedPrimaryOperationCode
		,source.InterfaceCode
		,source.LastAppointmentFlag
		,source.LocalEpisodicGpCode
		,source.LocalRegisteredGpCode
		,source.ManagementIntentionCode
		,source.MaritalStatusCode
		,source.MRSA
		,source.NationalBreachDate
		,source.NationalBreachDays
		,source.NationalDiagnosticBreachDate
		,source.NationalSpecialtyCode
		,source.NextOfKinHomePhone
		,source.NextOfKinName
		,source.NextOfKinRelationship
		,source.NextOfKinWorkPhone
		,source.NHSNumber
		,source.Operation
		,source.OriginalDateOnWaitingList
		,source.PASSpecialtyCode
		,source.PatientChoice
		,source.PatientDeathIndicator
		,source.PatientForename
		,source.PatientAddress1
		,source.PatientAddress2
		,source.PatientAddress3
		,source.PatientAddress4
		,source.PatientSurname
		,source.PatientTitle
		,source.PCTCode
		,source.Postcode
		,source.PriorityCode
		,source.ProviderCode
		,source.PurchaserCode
		,source.QM08EndWaitDate
		,source.QM08StartWaitDate
		,source.ReferralDate
		,source.RegisteredGpCode
		,source.ReligionCode
		,source.ReportCategoryCode
		,source.RTTBreachDate
		,source.RTTCurrentPrivatePatientFlag
		,source.RTTCurrentProviderCode
		,source.RTTCurrentStatusCode
		,source.RTTCurrentStatusDate
		,source.RTTDiagnosticBreachDate
		,source.RTTEndDate
		,source.RTTOverseasStatusFlag
		,source.RTTPathwayCondition
		,source.RTTPathwayID
		,source.RTTSpecialtyCode
		,source.RTTStartDate
		,source.SexCode
		,source.SiteCode
		,source.SourceEncounterNo
		,source.SourceOfReferralCode
		,source.SourcePatientNo
		,source.SourceTreatmentFunctionCode
		,source.SourceUniqueID
		,source.SpecialtyTypeCode
		,source.SuspensionEndDate
		,source.SuspensionReason
		,source.SuspensionReasonCode
		,source.SuspensionStartDate
		,source.TreatmentFunctionCode
		,source.WaitingListCode
		,source.WardCode
		,source.WeeksWaiting
		,source.WLStatus
		,source.WorkPhone
		,source.Cases
		,source.ConsultantCode
		,source.SpecialtyCode
		,source.AgeCode
		,source.TCIDate
		,source.LengthOfWait
		,source.DurationCode
		,source.RegisteredGpPracticeCode
		,source.EpisodicGpPracticeCode
		,source.WaitTypeCode
		,source.StatusCode
		,source.CategoryCode
		,source.OPCSCoded
		,source.WithRTTStartDate
		,source.WithRTTStatusCode
		,source.WithExpectedAdmissionDate
		,source.RTTActivity
		,source.RTTBreachStatusCode
		,source.DiagnosticProcedure
		,source.DirectorateCode
		,source.WithAppointmentDate
		,source.WithRTTOpenPathway
		,source.RTTPathwayStartDateCurrent
		,source.RTTWeekBandReturnCode
		,source.RTTDaysWaiting
		,source.DurationAtAppointmentDateCode
		,source.CCGCode
		,source.ResidenceCCGCode
		,source.DataSourceID
		,getdate()
		,getdate()
		,suser_name()
		)

when matched
and	source.EncounterChecksum
	<>
	checksum
		(
		 target.AdditionFlag
		,target.AdminCategoryCode
		,target.AdmissionMethodCode
		,target.AppointmentCategoryCode
		,target.AppointmentDate
		,target.AppointmentDoctor
		,target.AppointmentID
		,target.AppointmentStatusCode
		,target.AppointmentTime
		,target.AppointmentTypeCode
		,target.BookedDate
		,target.BookingTypeCode
		,target.BreachDate
		,target.BreachDays
		,target.BreachTypeCode
		,target.CancelledBy
		,target.CasenoteNumber
		,target.CensusDate
		,target.ClinicCode
		,target.ClockStartDate
		,target.CommentClinical
		,target.CommentNonClinical
		,target.ContextCode
		,target.ContractSerialNumber
		,target.CountOfCNDs
		,target.CountOfDaysSuspended
		,target.CountOfDNAs
		,target.CountOfHospitalCancels
		,target.CountOfPatientCancels
		,target.DateOfBirth
		,target.DateOfDeath
		,target.DateOnWaitingList
		,target.DerivedBreachDate
		,target.DerivedBreachDays
		,target.DerivedClockStartDate
		,target.DHACode
		,target.DistrictNo
		,target.EpisodeNo
		,target.EpisodicGpCode
		,target.EncounterRecno
		,target.EthnicOriginCode
		,target.FirstAttendanceCode
		,target.ExpectedAdmissionDate
		,target.FutureCancellationDate
		,target.FuturePatientCancelDate
		,target.HomePhone
		,target.IntendedPrimaryOperationCode
		,target.InterfaceCode
		,target.LastAppointmentFlag
		,target.LocalEpisodicGpCode
		,target.LocalRegisteredGpCode
		,target.ManagementIntentionCode
		,target.MaritalStatusCode
		,target.MRSA
		,target.NationalBreachDate
		,target.NationalBreachDays
		,target.NationalDiagnosticBreachDate
		,target.NationalSpecialtyCode
		,target.NextOfKinHomePhone
		,target.NextOfKinName
		,target.NextOfKinRelationship
		,target.NextOfKinWorkPhone
		,target.NHSNumber
		,target.Operation
		,target.OriginalDateOnWaitingList
		,target.PASSpecialtyCode
		,target.PatientChoice
		,target.PatientDeathIndicator
		,target.PatientForename
		,target.PatientAddress1
		,target.PatientAddress2
		,target.PatientAddress3
		,target.PatientAddress4
		,target.PatientSurname
		,target.PatientTitle
		,target.PCTCode
		,target.Postcode
		,target.PriorityCode
		,target.ProviderCode
		,target.PurchaserCode
		,target.QM08EndWaitDate
		,target.QM08StartWaitDate
		,target.ReferralDate
		,target.RegisteredGpCode
		,target.ReligionCode
		,target.ReportCategoryCode
		,target.RTTBreachDate
		,target.RTTCurrentPrivatePatientFlag
		,target.RTTCurrentProviderCode
		,target.RTTCurrentStatusCode
		,target.RTTCurrentStatusDate
		,target.RTTDiagnosticBreachDate
		,target.RTTEndDate
		,target.RTTOverseasStatusFlag
		,target.RTTPathwayCondition
		,target.RTTPathwayID
		,target.RTTSpecialtyCode
		,target.RTTStartDate
		,target.SexCode
		,target.SiteCode
		,target.SourceEncounterNo
		,target.SourceOfReferralCode
		,target.SourcePatientNo
		,target.SourceTreatmentFunctionCode
		,target.SourceUniqueID
		,target.SpecialtyTypeCode
		,target.SuspensionEndDate
		,target.SuspensionReason
		,target.SuspensionReasonCode
		,target.SuspensionStartDate
		,target.TreatmentFunctionCode
		,target.WaitingListCode
		,target.WardCode
		,target.WeeksWaiting
		,target.WLStatus
		,target.WorkPhone
		,target.Cases
		,target.ConsultantCode
		,target.SpecialtyCode
		,target.AgeCode
		,target.TCIDate
		,target.LengthOfWait
		,target.DurationCode
		,target.RegisteredGpPracticeCode
		,target.EpisodicGpPracticeCode
		,target.WaitTypeCode
		,target.StatusCode
		,target.CategoryCode
		,target.OPCSCoded
		,target.WithRTTStartDate
		,target.WithRTTStatusCode
		,target.WithExpectedAdmissionDate
		,target.RTTActivity
		,target.RTTBreachStatusCode
		,target.DiagnosticProcedure
		,target.DirectorateCode
		,target.WithAppointmentDate
		,target.WithRTTOpenPathway
		,target.RTTPathwayStartDateCurrent
		,target.RTTWeekBandReturnCode
		,target.RTTDaysWaiting
		,target.DurationAtAppointmentDateCode
		,target.CCGCode
		,target.ResidenceCCGCode
		,target.DataSourceID
	)

then
	update
	set
		 target.AdditionFlag = source.AdditionFlag
		,target.AdminCategoryCode = source.AdminCategoryCode
		,target.AdmissionMethodCode = source.AdmissionMethodCode
		,target.AppointmentCategoryCode = source.AppointmentCategoryCode
		,target.AppointmentDate = source.AppointmentDate
		,target.AppointmentDoctor = source.AppointmentDoctor
		,target.AppointmentID = source.AppointmentID
		,target.AppointmentStatusCode = source.AppointmentStatusCode
		,target.AppointmentTime = source.AppointmentTime
		,target.AppointmentTypeCode = source.AppointmentTypeCode
		,target.BookedDate = source.BookedDate
		,target.BookingTypeCode = source.BookingTypeCode
		,target.BreachDate = source.BreachDate
		,target.BreachDays = source.BreachDays
		,target.BreachTypeCode = source.BreachTypeCode
		,target.CancelledBy = source.CancelledBy
		,target.CasenoteNumber = source.CasenoteNumber
		,target.CensusDate = source.CensusDate
		,target.ClinicCode = source.ClinicCode
		,target.ClockStartDate = source.ClockStartDate
		,target.CommentClinical = source.CommentClinical
		,target.CommentNonClinical = source.CommentNonClinical
		,target.ContextCode = source.ContextCode
		,target.ContractSerialNumber = source.ContractSerialNumber
		,target.CountOfCNDs = source.CountOfCNDs
		,target.CountOfDaysSuspended = source.CountOfDaysSuspended
		,target.CountOfDNAs = source.CountOfDNAs
		,target.CountOfHospitalCancels = source.CountOfHospitalCancels
		,target.CountOfPatientCancels = source.CountOfPatientCancels
		,target.DateOfBirth = source.DateOfBirth
		,target.DateOfDeath = source.DateOfDeath
		,target.DateOnWaitingList = source.DateOnWaitingList
		,target.DerivedBreachDate = source.DerivedBreachDate
		,target.DerivedBreachDays = source.DerivedBreachDays
		,target.DerivedClockStartDate = source.DerivedClockStartDate
		,target.DHACode = source.DHACode
		,target.DistrictNo = source.DistrictNo
		,target.EpisodeNo = source.EpisodeNo
		,target.EpisodicGpCode = source.EpisodicGpCode
		,target.EncounterRecno = source.EncounterRecno
		,target.EthnicOriginCode = source.EthnicOriginCode
		,target.FirstAttendanceCode = source.FirstAttendanceCode
		,target.ExpectedAdmissionDate = source.ExpectedAdmissionDate
		,target.FutureCancellationDate = source.FutureCancellationDate
		,target.FuturePatientCancelDate = source.FuturePatientCancelDate
		,target.HomePhone = source.HomePhone
		,target.IntendedPrimaryOperationCode = source.IntendedPrimaryOperationCode
		,target.InterfaceCode = source.InterfaceCode
		,target.LastAppointmentFlag = source.LastAppointmentFlag
		,target.LocalEpisodicGpCode = source.LocalEpisodicGpCode
		,target.LocalRegisteredGpCode = source.LocalRegisteredGpCode
		,target.ManagementIntentionCode = source.ManagementIntentionCode
		,target.MaritalStatusCode = source.MaritalStatusCode
		,target.MRSA = source.MRSA
		,target.NationalBreachDate = source.NationalBreachDate
		,target.NationalBreachDays = source.NationalBreachDays
		,target.NationalDiagnosticBreachDate = source.NationalDiagnosticBreachDate
		,target.NationalSpecialtyCode = source.NationalSpecialtyCode
		,target.NextOfKinHomePhone = source.NextOfKinHomePhone
		,target.NextOfKinName = source.NextOfKinName
		,target.NextOfKinRelationship = source.NextOfKinRelationship
		,target.NextOfKinWorkPhone = source.NextOfKinWorkPhone
		,target.NHSNumber = source.NHSNumber
		,target.Operation = source.Operation
		,target.OriginalDateOnWaitingList = source.OriginalDateOnWaitingList
		,target.PASSpecialtyCode = source.PASSpecialtyCode
		,target.PatientChoice = source.PatientChoice
		,target.PatientDeathIndicator = source.PatientDeathIndicator
		,target.PatientForename = source.PatientForename
		,target.PatientAddress1 = source.PatientAddress1
		,target.PatientAddress2 = source.PatientAddress2
		,target.PatientAddress3 = source.PatientAddress3
		,target.PatientAddress4 = source.PatientAddress4
		,target.PatientSurname = source.PatientSurname
		,target.PatientTitle = source.PatientTitle
		,target.PCTCode = source.PCTCode
		,target.Postcode = source.Postcode
		,target.PriorityCode = source.PriorityCode
		,target.ProviderCode = source.ProviderCode
		,target.PurchaserCode = source.PurchaserCode
		,target.QM08EndWaitDate = source.QM08EndWaitDate
		,target.QM08StartWaitDate = source.QM08StartWaitDate
		,target.ReferralDate = source.ReferralDate
		,target.RegisteredGpCode = source.RegisteredGpCode
		,target.ReligionCode = source.ReligionCode
		,target.ReportCategoryCode = source.ReportCategoryCode
		,target.RTTBreachDate = source.RTTBreachDate
		,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
		,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
		,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
		,target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
		,target.RTTDiagnosticBreachDate = source.RTTDiagnosticBreachDate
		,target.RTTEndDate = source.RTTEndDate
		,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
		,target.RTTPathwayCondition = source.RTTPathwayCondition
		,target.RTTPathwayID = source.RTTPathwayID
		,target.RTTSpecialtyCode = source.RTTSpecialtyCode
		,target.RTTStartDate = source.RTTStartDate
		,target.SexCode = source.SexCode
		,target.SiteCode = source.SiteCode
		,target.SourceEncounterNo = source.SourceEncounterNo
		,target.SourceOfReferralCode = source.SourceOfReferralCode
		,target.SourcePatientNo = source.SourcePatientNo
		,target.SourceTreatmentFunctionCode = source.SourceTreatmentFunctionCode
		,target.SourceUniqueID = source.SourceUniqueID
		,target.SpecialtyTypeCode = source.SpecialtyTypeCode
		,target.SuspensionEndDate = source.SuspensionEndDate
		,target.SuspensionReason = source.SuspensionReason
		,target.SuspensionReasonCode = source.SuspensionReasonCode
		,target.SuspensionStartDate = source.SuspensionStartDate
		,target.TreatmentFunctionCode = source.TreatmentFunctionCode
		,target.WaitingListCode = source.WaitingListCode
		,target.WardCode = source.WardCode
		,target.WeeksWaiting = source.WeeksWaiting
		,target.WLStatus = source.WLStatus
		,target.WorkPhone = source.WorkPhone
		,target.Cases = source.Cases
		,target.ConsultantCode = source.ConsultantCode
		,target.SpecialtyCode = source.SpecialtyCode
		,target.AgeCode = source.AgeCode
		,target.TCIDate = source.TCIDate
		,target.LengthOfWait = source.LengthOfWait
		,target.DurationCode = source.DurationCode
		,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
		,target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
		,target.WaitTypeCode = source.WaitTypeCode
		,target.StatusCode = source.StatusCode
		,target.CategoryCode = source.CategoryCode
		,target.OPCSCoded = source.OPCSCoded
		,target.WithRTTStartDate = source.WithRTTStartDate
		,target.WithRTTStatusCode = source.WithRTTStatusCode
		,target.WithExpectedAdmissionDate = source.WithExpectedAdmissionDate
		,target.RTTActivity = source.RTTActivity
		,target.RTTBreachStatusCode = source.RTTBreachStatusCode
		,target.DiagnosticProcedure = source.DiagnosticProcedure
		,target.DirectorateCode = source.DirectorateCode
		,target.WithAppointmentDate = source.WithAppointmentDate
		,target.WithRTTOpenPathway = source.WithRTTOpenPathway
		,target.RTTPathwayStartDateCurrent = source.RTTPathwayStartDateCurrent
		,target.RTTWeekBandReturnCode = source.RTTWeekBandReturnCode
		,target.RTTDaysWaiting = source.RTTDaysWaiting
		,target.DurationAtAppointmentDateCode = source.DurationAtAppointmentDateCode
		,target.CCGCode = source.CCGCode
		,target.ResidenceCCGCode = source.ResidenceCCGCode
		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


﻿CREATE procedure [ETL].[ImportHRBaseSummary] as

--import the data
exec ETL.BuildHRBaseSummary


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[HR].[BaseSummary]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildHRBaseSummaryReference 'CMFT||ESREXT'


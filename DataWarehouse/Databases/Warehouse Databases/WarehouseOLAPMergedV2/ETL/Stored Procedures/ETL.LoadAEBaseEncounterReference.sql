﻿


CREATE procedure [ETL].[LoadAEBaseEncounterReference]
as

--	20151110	RR	Updated join to AE.AttendanceDisposal to use SourceAttendanceDisposalCode rather than AttendanceDisposalCode (this is what is being brought through in the Reference table)

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	AE.BaseEncounterReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID
		,AgeID = Age.AgeID

		,ArrivalModeID = ArrivalMode.SourceArrivalModeID
		,AttendanceCategoryID = AttendanceCategory.SourceAttendanceCategoryID
		,AttendanceDisposalID = AttendanceDisposal.SourceAttendanceDisposalID
		,DiagnosisFirstID = DiagnosisFirst.SourceDiagnosisID
		,DiagnosisSecondID = DiagnosisSecond.SourceDiagnosisID

		,EncounterStartDateID =
			coalesce(
				 EncounterStartDate.DateID

				,case
				when Encounter.EncounterStartDate is null
				then NullDate.DateID

				when Encounter.EncounterStartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,EncounterEndDateID =
			coalesce(
				 EncounterEndDate.DateID

				,case
				when Encounter.EncounterEndDate is null
				then NullDate.DateID

				when Encounter.EncounterEndDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,EthnicOriginID = EthnicOrigin.SourceEthnicCategoryID
		,IncidentLocationTypeID = IncidentLocationType.SourceIncidentLocationTypeID
		,InvestigationFirstID = InvestigationFirst.SourceInvestigationID
		,InvestigationSecondID = InvestigationSecond.SourceInvestigationID
		,NHSNumberStatusID = NHSNumberStatus.SourceNHSNumberStatusID
		,PatientGroupID = PatientGroup.SourcePatientGroupID
		,ReferredToSpecialtyID = ReferredToSpecialty.SourceSpecialtyID
		,SexID = Sex.SourceSexID
		,SiteID = Site.SourceSiteID
		,LocationID = Location.SourceLocationID
		,SourceOfReferralID = SourceOfReferral.SourceSourceOfReferralID
		,StaffMemberID = StaffMember.SourceStaffMemberID
		,TreatmentFirstID = TreatmentFirst.SourceTreatmentID
		,TreatmentSecondID = TreatmentSecond.SourceTreatmentID
		,TriageCategoryID = TriageCategory.SourceTriageCategoryID
		,AttendanceResidenceCCGID = AttendanceResidenceCCG.CCGID
		,CareGroupID = CareGroup.SourceCareGroupID
		,ReligionID = Religion.SourceReligionID

		,ProcessList.Action
	from
		AE.BaseEncounter Encounter
		
	inner join AE.ProcessList
	on	ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	and	ProcessList.Dataset = 'Encounter'

	left join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	left join AE.ArrivalMode
	on	ArrivalMode.SourceArrivalModeCode = cast(coalesce(Encounter.ArrivalModeCode, '-1') as varchar)
	and	ArrivalMode.SourceContextCode = Encounter.ContextCode

	left join AE.AttendanceCategory
	on	AttendanceCategory.SourceAttendanceCategoryCode = cast(coalesce(Encounter.AttendanceCategoryCode, '-1') as varchar)
	and	AttendanceCategory.SourceContextCode = Encounter.ContextCode

	left join AE.AttendanceDisposal
	on	AttendanceDisposal.SourceAttendanceDisposalCode = cast(coalesce(Encounter.SourceAttendanceDisposalCode, '-1') as varchar)
	and	AttendanceDisposal.SourceContextCode = Encounter.ContextCode

	left join AE.Diagnosis DiagnosisFirst
	on	DiagnosisFirst.SourceDiagnosisCode = cast(coalesce(Encounter.DiagnosisCodeFirst, '-1') as varchar)
	and	DiagnosisFirst.SourceContextCode = Encounter.ContextCode

	left join AE.Diagnosis DiagnosisSecond
	on	DiagnosisSecond.SourceDiagnosisCode = cast(coalesce(Encounter.DiagnosisCodeSecond, '-1') as varchar)
	and	DiagnosisSecond.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar EncounterStartDate
	on	EncounterStartDate.TheDate = Encounter.EncounterStartDate

	left join WH.Calendar EncounterEndDate
	on	EncounterEndDate.TheDate = Encounter.EncounterEndDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join WH.EthnicCategory EthnicOrigin
	on	EthnicOrigin.SourceEthnicCategoryCode = cast(coalesce(Encounter.EthnicCategoryCode, '-1') as varchar)
	and	EthnicOrigin.SourceContextCode = Encounter.ContextCode

	left join AE.IncidentLocationType
	on	IncidentLocationType.SourceIncidentLocationTypeCode = cast(coalesce(Encounter.IncidentLocationTypeCode, '-1') as varchar)
	and	IncidentLocationType.SourceContextCode = Encounter.ContextCode

	left join AE.Investigation InvestigationFirst
	on	InvestigationFirst.SourceInvestigationCode = cast(coalesce(Encounter.InvestigationCodeFirst, '-1') as varchar)
	and	InvestigationFirst.SourceContextCode = Encounter.ContextCode

	left join AE.Investigation InvestigationSecond
	on	InvestigationSecond.SourceInvestigationCode = cast(coalesce(Encounter.InvestigationCodeSecond, '-1') as varchar)
	and	InvestigationSecond.SourceContextCode = Encounter.ContextCode

	left join WH.NHSNumberStatus NHSNumberStatus
	on	NHSNumberStatus.SourceNHSNumberStatusCode = cast(coalesce(Encounter.NHSNumberStatusCode, '-1') as varchar)
	and	NHSNumberStatus.SourceContextCode = Encounter.ContextCode

	left join AE.PatientGroup
	on	PatientGroup.SourcePatientGroupCode = cast(coalesce(Encounter.PatientGroupCode, '-1') as varchar)
	and	PatientGroup.SourceContextCode = Encounter.ContextCode

	left join WH.Specialty ReferredToSpecialty
	on	ReferredToSpecialty.SourceSpecialtyCode = coalesce(Encounter.ReferredToSpecialtyCode, '-1')
	and	ReferredToSpecialty.SourceContextCode = Encounter.ContextCode

	left join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.SexCode, '-1') as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	left join WH.Site Site
	on	Site.SourceSiteCode = cast(coalesce(Encounter.SiteCode, '-1') as varchar)
	and	Site.SourceContextCode = Encounter.ContextCode

	left join WH.Location Location
	on	Location.SourceLocationCode = coalesce(Encounter.SiteCode, '-1')
	and	Location.SourceContextCode = Encounter.ContextCode

	left join AE.SourceOfReferral
	on	SourceOfReferral.SourceSourceOfReferralCode = cast(coalesce(Encounter.SourceOfReferralCode, '-1') as varchar)
	and	SourceOfReferral.SourceContextCode = Encounter.ContextCode

	left join AE.StaffMember
	on	StaffMember.SourceStaffMemberCode = cast(coalesce(Encounter.StaffMemberCode, '-1') as varchar)
	and	StaffMember.SourceContextCode = Encounter.ContextCode

	left join AE.Treatment TreatmentFirst
	on	TreatmentFirst.SourceTreatmentCode = cast(coalesce(Encounter.TreatmentCodeFirst, '-1') as varchar)
	and	TreatmentFirst.SourceContextCode = Encounter.ContextCode

	left join AE.Treatment TreatmentSecond
	on	TreatmentSecond.SourceTreatmentCode = cast(coalesce(Encounter.TreatmentCodeSecond, '-1') as varchar)
	and	TreatmentSecond.SourceContextCode = Encounter.ContextCode

	left join AE.TriageCategory
	on	TriageCategory.SourceTriageCategoryCode = cast(coalesce(Encounter.TriageCategoryCode, '-1') as varchar)
	and	TriageCategory.SourceContextCode = Encounter.ContextCode

	left join WH.CCG AttendanceResidenceCCG
	on	AttendanceResidenceCCG.CCGCode = coalesce(Encounter.AttendanceResidenceCCGCode, 'N/A')

	left join AE.CareGroup
	on	CareGroup.SourceCareGroupCode = cast(coalesce(Encounter.CareGroup, '-1') as varchar)
	and	CareGroup.SourceContextCode = Encounter.ContextCode

	left join WH.Religion
	on	Religion.SourceReligionCode = cast(coalesce(Encounter.ReligionCode, '-1') as varchar)
	and	Religion.SourceContextCode = Encounter.ContextCode

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno


	when matched
	and source.Action = 'DELETE'
	then
		delete

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,AgeID
			,ArrivalModeID
			,AttendanceCategoryID
			,AttendanceDisposalID
			,DiagnosisFirstID
			,DiagnosisSecondID
			,EncounterStartDateID
			,EncounterEndDateID
			,EthnicOriginID
			,IncidentLocationTypeID
			,InvestigationFirstID
			,InvestigationSecondID
			,NHSNumberStatusID
			,PatientGroupID
			,ReferredToSpecialtyID
			,SexID
			,SiteID
			,LocationID
			,SourceOfReferralID
			,StaffMemberID
			,TreatmentFirstID
			,TreatmentSecondID
			,TriageCategoryID
			,AttendanceResidenceCCGID
			,CareGroupID
			,ReligionID
			,Created
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.AgeID
			,source.ArrivalModeID
			,source.AttendanceCategoryID
			,source.AttendanceDisposalID
			,source.DiagnosisFirstID
			,source.DiagnosisSecondID
			,source.EncounterStartDateID
			,source.EncounterEndDateID
			,source.EthnicOriginID
			,source.IncidentLocationTypeID
			,source.InvestigationFirstID
			,source.InvestigationSecondID
			,source.NHSNumberStatusID
			,source.PatientGroupID
			,source.ReferredToSpecialtyID
			,source.SexID
			,source.SiteID
			,source.LocationID
			,source.SourceOfReferralID
			,source.StaffMemberID
			,source.TreatmentFirstID
			,source.TreatmentSecondID
			,source.TriageCategoryID
			,source.AttendanceResidenceCCGID
			,source.CareGroupID
			,source.ReligionID
			,getdate()
			,suser_name()
			)

	when matched
	and source.Action <> 'DELETE'
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.AgeID = source.AgeID
			,target.ArrivalModeID = source.ArrivalModeID
			,target.AttendanceCategoryID = source.AttendanceCategoryID
			,target.AttendanceDisposalID = source.AttendanceDisposalID
			,target.DiagnosisFirstID = source.DiagnosisFirstID
			,target.DiagnosisSecondID = source.DiagnosisSecondID
			,target.EncounterStartDateID = source.EncounterStartDateID
			,target.EncounterEndDateID = source.EncounterEndDateID
			,target.EthnicOriginID = source.EthnicOriginID
			,target.IncidentLocationTypeID = source.IncidentLocationTypeID
			,target.InvestigationFirstID = source.InvestigationFirstID
			,target.InvestigationSecondID = source.InvestigationSecondID
			,target.NHSNumberStatusID = source.NHSNumberStatusID
			,target.PatientGroupID = source.PatientGroupID
			,target.ReferredToSpecialtyID = source.ReferredToSpecialtyID
			,target.SexID = source.SexID
			,target.SiteID = source.SiteID
			,target.LocationID = source.LocationID
			,target.SourceOfReferralID = source.SourceOfReferralID
			,target.StaffMemberID = source.StaffMemberID
			,target.TreatmentFirstID = source.TreatmentFirstID
			,target.TreatmentSecondID = source.TreatmentSecondID
			,target.TriageCategoryID = source.TriageCategoryID
			,target.AttendanceResidenceCCGID = source.AttendanceResidenceCCGID
			,target.CareGroupID = source.CareGroupID
			,target.ReligionID = source.ReligionID
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



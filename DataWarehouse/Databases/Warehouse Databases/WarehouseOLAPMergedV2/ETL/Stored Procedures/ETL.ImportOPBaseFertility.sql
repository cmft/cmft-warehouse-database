﻿
CREATE procedure [ETL].[ImportOPBaseFertility] as

--import the data
exec ETL.BuildOPBaseFertility


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[OP].[BaseFertility]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildOPBaseFertilityReference 


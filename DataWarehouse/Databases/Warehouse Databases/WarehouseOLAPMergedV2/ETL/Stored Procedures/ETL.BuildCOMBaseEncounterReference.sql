﻿CREATE procedure [ETL].[BuildCOMBaseEncounterReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				COM.BaseEncounterReference
			)
			,'1 jan 1900'
		)

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	COM.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		COM.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
	)
and	ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	COM.BaseEncounterReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,SpecialtyID = Specialty.SourceValueID
		,ProfessionalCarerID = ProfessionalCarer.SourceValueID
		,StaffTeamID = StaffTeam.SourceValueID
		,LocationID = Location.SourceValueID
		,ModifiedByID = ModifiedBy.SourceValueID
		,ReferredToSpecialtyID = ReferredToSpecialty.SourceValueID

		,StartDateID =
			coalesce(
				 StartDate.DateID

				,case
				when Encounter.StartDate is null
				then NullDate.DateID

				when Encounter.StartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ReferralReasonID = ReferralReason.SourceValueID
		,ReferralTypeID = ReferralType.SourceValueID
		,ReferralSourceID = ReferralSource.SourceValueID
		,SexID = Sex.SourceSexID
		,EthnicCategoryID = EthnicCategory.SourceEthnicCategoryID
		,ConsultationMediumID = ConsultationMedium.SourceValueID
		,MaritalStatusID = MaritalStatus.SourceMaritalStatusID
		,ReligionID = Religion.SourceReligionID
		,SpokenLanguageID = SpokenLanguage.SourceValueID
		,GPPracticeID = GPPractice.SourceValueID
		,AgeID = Age.AgeID
		,CreatedByID = CreatedBy.SourceValueID

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		COM.BaseEncounter Encounter

	left join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	left join COM.Specialty Specialty
	on	Specialty.SpecialtyID = coalesce(Encounter.SpecialtyID, '-1')
	and	Specialty.ContextCode = Encounter.ContextCode

	left join COM.ProfessionalCarer ProfessionalCarer
	on	ProfessionalCarer.ProfessionalCarerID = coalesce(Encounter.SeenByProfessionalCarerID,Encounter.ProfessionalCarerID, '-1') --CH 9/1/15 Encounter.SeenByProfessionalCarerID added
	and	ProfessionalCarer.ContextCode = Encounter.ContextCode

	left join COM.StaffTeam StaffTeam
	on	StaffTeam.StaffTeamID = coalesce(Encounter.StaffTeamID, '-1')
	and	StaffTeam.ContextCode = Encounter.ContextCode

	left join COM.Location Location
	on	Location.LocationID = coalesce(Encounter.EncounterLocationID, '-1')
	and	Location.ContextCode = Encounter.ContextCode

	left join COM.SystemUser ModifiedBy
	on	ModifiedBy.SystemUserID = Encounter.ModifiedBy
	and	ModifiedBy.ContextCode = Encounter.ContextCode

	left join COM.Specialty ReferredToSpecialty
	on	ReferredToSpecialty.SpecialtyID = coalesce(Encounter.ReferredToSpecialtyID, '-1')
	and	ReferredToSpecialty.ContextCode = Encounter.ContextCode

	left join WH.Calendar StartDate
	on	StartDate.TheDate = Encounter.StartDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join COM.ReferralReason ReferralReason
	on	ReferralReason.ReferralReasonID = coalesce(Encounter.ReferralReasonID, '-1')
	and	ReferralReason.ContextCode = Encounter.ContextCode

	left join COM.ReferralType ReferralType
	on	ReferralType.ReferralTypeID = coalesce(Encounter.ReferralTypeID, '-1')
	and	ReferralType.ContextCode = Encounter.ContextCode

	left join COM.ReferralSource ReferralSource
	on	ReferralSource.ReferralSourceID = coalesce(Encounter.ReferralSourceID, '-1')
	and	ReferralSource.ContextCode = Encounter.ContextCode

	left join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.PatientSexID, -1) as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	left join WH.EthnicCategory EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryCode = cast(coalesce(Encounter.PatientEthnicGroupID, -1) as varchar)
	and	EthnicCategory.SourceContextCode = Encounter.ContextCode

	left join WH.MaritalStatus MaritalStatus
	on	MaritalStatus.SourceMaritalStatusCode = cast(coalesce(Encounter.PatientMaritalStatusID, -1) as varchar)
	and	MaritalStatus.SourceContextCode = Encounter.ContextCode

	left join WH.Religion Religion
	on	Religion.SourceReligionCode = cast(coalesce(Encounter.PatientReligionID, -1) as varchar)
	and	Religion.SourceContextCode = Encounter.ContextCode

	left join COM.ConsultationMedium ConsultationMedium
	on	ConsultationMedium.ConsultationMediumID = coalesce(Encounter.EncounterTypeID, '-1')
	and	ConsultationMedium.ContextCode = Encounter.ContextCode

	left join COM.SpokenLanguage SpokenLanguage
	on	SpokenLanguage.SpokenLanguageID = coalesce(Encounter.PatientSpokenLanguageID, '-1')
	and	SpokenLanguage.ContextCode = Encounter.ContextCode

	left join COM.GPPractice GPPractice
	on	GPPractice.GPPracticeID = coalesce(Encounter.PatientEncounterRegisteredPracticeID, '-1')
	and	GPPractice.ContextCode = Encounter.ContextCode

	left join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	left join COM.SystemUser CreatedBy
	on	CreatedBy.SystemUserID = Encounter.CreatedBy
	and	CreatedBy.ContextCode = Encounter.ContextCode

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,SpecialtyID
			,ProfessionalCarerID
			,StaffTeamID
			,LocationID
			,ModifiedByID
			,ReferredToSpecialtyID
			,StartDateID
			,ReferralReasonID
			,ReferralTypeID
			,ReferralSourceID
			,SexID
			,EthnicCategoryID
			,ConsultationMediumID
			,MaritalStatusID
			,ReligionID
			,SpokenLanguageID
			,GPPracticeID
			,AgeID
			,CreatedByID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.SpecialtyID
			,source.ProfessionalCarerID
			,source.StaffTeamID
			,source.LocationID
			,source.ModifiedByID
			,source.ReferredToSpecialtyID
			,source.StartDateID
			,source.ReferralReasonID
			,source.ReferralTypeID
			,source.ReferralSourceID
			,source.SexID
			,source.EthnicCategoryID
			,source.ConsultationMediumID
			,source.MaritalStatusID
			,source.ReligionID
			,source.SpokenLanguageID
			,source.GPPracticeID
			,source.AgeID
			,source.CreatedByID
			,source.Created
			,source.Updated
			,source.ByWhom
			)
	
	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.SpecialtyID = source.SpecialtyID
			,target.ProfessionalCarerID = source.ProfessionalCarerID
			,target.StaffTeamID = source.StaffTeamID
			,target.LocationID = source.LocationID
			,target.ModifiedByID = source.ModifiedByID
			,target.ReferredToSpecialtyID = source.ReferredToSpecialtyID
			,target.StartDateID = source.StartDateID
			,target.ReferralReasonID = source.ReferralReasonID
			,target.ReferralTypeID = source.ReferralTypeID
			,target.ReferralSourceID = source.ReferralSourceID
			,target.SexID = source.SexID
			,target.EthnicCategoryID = source.EthnicCategoryID
			,target.ConsultationMediumID = source.ConsultationMediumID
			,target.MaritalStatusID = source.MaritalStatusID
			,target.ReligionID = source.ReligionID
			,target.SpokenLanguageID = source.SpokenLanguageID
			,target.GPPracticeID = source.GPPracticeID
			,target.AgeID = source.AgeID
			,target.CreatedByID = source.CreatedByID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

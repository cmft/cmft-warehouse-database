﻿

CREATE procedure [ETL].[BuildTLoadOPBaseOperation]

as



truncate table ETL.TLoadOPBaseOperation

--Central
insert into	ETL.TLoadOPBaseOperation

select 
	MergeEncounterRecno
	,EncounterRecno
	,ContextCode 
	,SequenceNo
	,OperationCode
	,OperationDate
	
from
	ETL.TImportCenOPBaseOperation CenImport


-- Trafford
insert into	ETL.TLoadOPBaseOperation

select 
	MergeEncounterRecno
	,EncounterRecno
	,ContextCode
	,SequenceNo
	,OperationCode
	,OperationDate
	 
from
	ETL.TImportTraOPBaseOperation TraImport


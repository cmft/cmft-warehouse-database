﻿CREATE procedure [ETL].[ImportOP] as

-------------------------------------------------
--When		Who	What
--20141029	PDO	Created procedure
--20150618	RR	Added OP Operation process
-------------------------------------------------

--load Central and Trafford into TLoad table
exec ETL.BuildTLoadOPBaseEncounter

--load encounters and generate process list
exec ETL.LoadOPBaseEncounter

--generate missing reference data
declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[OP].[BaseEncounter]'
	,@ProcessListTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[OP].[ProcessList]'
	,@NewRows int

declare	@ActivityTableOverride varchar(max) = 
	'(
	select
		Encounter.*
	from
		' + @ActivityTable + ' Encounter

	inner join ' + @ProcessListTable + ' ProcessList
	on	ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	)'

exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output,@Debug = 0 ,@ActivityTableOverride = @ActivityTableOverride

if @NewRows > 0
	exec WH.BuildMember
	
if @NewRows > 0
	exec  ETL.BuildWHConsultantBase

--load reference values
exec ETL.LoadOPBaseEncounterReference


-- 20150618 RR added 
--load Central and Trafford into TLoad table
exec ETL.BuildTLoadOPBaseOperation

--load Operations
exec ETL.LoadOPBaseOperation

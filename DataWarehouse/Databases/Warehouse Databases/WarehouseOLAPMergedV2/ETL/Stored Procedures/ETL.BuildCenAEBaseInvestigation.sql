﻿create procedure [ETL].[BuildCenAEBaseInvestigation]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


delete
from
	AE.BaseInvestigation
where
	not exists
	(
	select
		1
	from
		ETL.TLoadCenAEBaseInvestigation
	where
		TLoadCenAEBaseInvestigation.MergeEncounterRecno = BaseInvestigation.MergeEncounterRecno
	)

and	exists
	(
	select
		1
	from
		AE.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseInvestigation.MergeEncounterRecno
	and	BaseEncounter.ContextCode in
		(
		 'CEN||ADAS'
		,'CEN||PAS'
		,'CEN||SYM'
		,'TRA||ADAS'
		)
	)

select
	@deleted = @@ROWCOUNT


merge
	AE.BaseInvestigation target
using
	(
	select
		 MergeEncounterRecno
		,SequenceNo = cast(SequenceNo as smallint)
		,InvestigationSchemeCode = cast(InvestigationSchemeCode as varchar(10))
		,InvestigationCode = cast(InvestigationCode as varchar(50))
		,InvestigationTime = cast(InvestigationTime as smalldatetime)
		,ResultTime = cast(ResultTime as smalldatetime)
		,ContextCode
		,Created = getdate()
		,Updated = getdate()
		,ByWhom = suser_name()
	from
		ETL.TLoadCenAEBaseInvestigation Encounter
	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno
	and	source.SequenceNo = target.SequenceNo

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,SequenceNo
			,InvestigationSchemeCode
			,InvestigationCode
			,InvestigationTime
			,ResultTime
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.SequenceNo
			,source.InvestigationSchemeCode
			,source.InvestigationCode
			,source.InvestigationTime
			,source.ResultTime
			,source.ContextCode
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			isnull(target.InvestigationSchemeCode, '') = isnull(source.InvestigationSchemeCode, '')
		and isnull(target.InvestigationCode, '') = isnull(source.InvestigationCode, '')
		and isnull(target.InvestigationTime, '') = isnull(source.InvestigationTime, '')
		and isnull(target.ResultTime, '') = isnull(source.ResultTime, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			 target.InvestigationSchemeCode = source.InvestigationSchemeCode
			,target.InvestigationCode = source.InvestigationCode
			,target.InvestigationTime = source.InvestigationTime
			,target.ResultTime = source.ResultTime
			,target.ContextCode = source.ContextCode

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



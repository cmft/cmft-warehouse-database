﻿CREATE procedure [ETL].[BuildMortalityQuestionBase] as

Insert into Mortality.QuestionBase
	(
	Question
	,QuestionNo
	,YesNoResponse
	,FormTypeID
	)
Select distinct
	Question
	,QuestionNo
	,YesNoResponse = 0
	,SourceFormTypeID
from 
	Mortality.BaseReview
	--471 excluding QuestionNo / 1485 with QuestionNo
inner join Mortality.FormType
on BaseReview.FormTypeCode = FormType.SourceFormTypeCode
and BaseReview.ContextCode = FormType.SourceContextCode
where
	not exists
		(
		Select
			1
		from
			Mortality.QuestionBase Present
		where
			Present.FormTypeID = FormType.SourceFormTypeID
		and Present.Question = BaseReview.Question
		and Present.QuestionNo = BaseReview.QuestionNo
		)

Update QuestionBase
set YesNoResponse = 1
from 
	Mortality.QuestionBase
inner join Mortality.QuestionBase Present
on QuestionBase.Question = Present.Question
--and QuestionBase.FormTypeID = Present.FormTypeID
and Present.YesNoResponse = 1
and QuestionBase.YesNoResponse = 0
and QuestionBase.Question not in (
'CauseOfDeath1a'
,'CauseOfDeath1b'
,'CauseOfDeath1c'
,'CauseOfDeath2'
,'CodedDiagnosis'
)


/*

Update Question
set YesNoResponse = 1
--select BaseReview.Question,Response,COUNT(*)
from Mortality.BaseReview
inner join Mortality.FormType
on BaseReview.FormTypeCode = FormType.SourceFormTypeCode
inner join Mortality.QuestionBase Question
on BaseReview.Question = Question.Question
and BaseReview.QuestionNo = Question.QuestionNo
and FormType.SourceFormTypeID = Question.FormTypeID

inner join [Warehouse2\Dev].WarehouseOLAPMergedV2.dbo.[Import]
-- Select Question,count(*) from (Select distinct Question,YesNoResponse from [Warehouse2\Dev].WarehouseOLAPMergedV2.dbo.[Import]) A group by Question having count(*)>1
on Question.Question = Import.Question
and Import.YesNoResponse = 1
and Question.Question not in (
'CauseOfDeath1a'
,'CauseOfDeath1b'
,'CauseOfDeath1c'
,'CauseOfDeath2'
,'CodedDiagnosis'
)

Update Question
set YesNoResponse = 1
-- select BaseReview.Question,Response,COUNT(*)
from Mortality.BaseReview
inner join Mortality.FormType
on BaseReview.FormTypeCode = FormType.SourceFormTypeCode
inner join Mortality.QuestionBase Question
on BaseReview.Question = Question.Question
and BaseReview.QuestionNo = Question.QuestionNo
and FormType.SourceFormTypeID = Question.FormTypeID

inner join [Warehouse2\Dev].WarehouseOLAPMergedV2.dbo.[Import]
-- Select Question,count(*) from (Select distinct Question,YesNoResponse from [Warehouse2\Dev].WarehouseOLAPMergedV2.dbo.[Import]) A group by Question having count(*)>1
on Question.Question = Import.Question
and Import.YesNoResponse = 1
and Import.FormTypeCode = FormType.SourceFormTypeCode
and Question.Question in (
'CauseOfDeath1a'
,'CauseOfDeath1b'
,'CauseOfDeath1c'
,'CauseOfDeath2'
,'CodedDiagnosis'
)
--group by BaseReview.Question,Response




Update Mortality.QuestionBase
set YesNoResponse = 1
where
	FormTypeID = 3393587
and Question in (
'CaseReferredToCoroner'
,'CaseDiscussedWithCoroner'
,'CaseDiscussedWithCoronerDocumentedInMedicalRecords'
,'CaseDiscussedWithCoronerPostMortemPerformed'  
,'CaseDiscussedWithCoronerOutcomeAppropriate'
,'DeathCertificateIssued'
,'RequestForHospitalPostMortem'
,'PermissionForHospitalPostMortemGiven'
,'FullPermissionForHospitalPostMortemGiven'
,'LimitedPermissionForHospitalPostMortemGiven'
,'CauseOfDeathAgreed'
,'AnyFurtherPointsOfDiscussion'
,'OrganDonationDiscussed'
,'TissueDonationDiscussed'
,'OrgansDonated' 
,'TissuesDonated'
,'SecondaryReviewFuturePresentation'
)



Update Mortality.QuestionBase
set YesNoResponse = 1
where
	FormTypeID = 3394280
and Question in (
'MRSA'
	,'MedicationErrorsReported'
	,'CriticalIncident'
	,'IntensiveCareWithdrawn'
	,'LOTADNR'
	,'ConsultantDiscussion'
	,'PalliativeCare'
	,'AppointmentAttended'
	,'FollowUp'
	,'DoneDifferently'
)
select * from Mortality.FormType
*/


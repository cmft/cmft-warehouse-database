﻿CREATE procedure [ETL].[BuildTheatreBaseProcedureDetailReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	Theatre.BaseProcedureDetailReference
where
	not exists
	(
	select
		1
	from
		Theatre.BaseProcedureDetail
	where
		BaseProcedureDetail.MergeRecno = BaseProcedureDetailReference.MergeRecno
	)
and	BaseProcedureDetailReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Theatre.BaseProcedureDetailReference target
using
	(
	select
		 Encounter.MergeRecno
		,ContextID = @ContextID
		,OperationID = coalesce(Operation.OperationID, -1)
	from
		Theatre.BaseProcedureDetail Encounter

	left join Theatre.BaseOperation
	on	BaseOperation.OperationID = Encounter.ProcedureCode
	and	BaseOperation.ContextCode = Encounter.ContextCode

	left join WH.Operation
	on	Operation.OperationCode = BaseOperation.OperationCode

	where
		Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			 MergeRecno
			,ContextID
			,OperationID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.OperationID
			)

	when matched
	and not
		(
			isnull(target.OperationID, 0) = isnull(source.OperationID, 0)
		)
	then
		update
		set
			 target.OperationID = source.OperationID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Context: ' + @ContextCode +
		', Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


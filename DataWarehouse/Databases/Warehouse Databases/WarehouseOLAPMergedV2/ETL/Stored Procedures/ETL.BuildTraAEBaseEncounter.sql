﻿

CREATE procedure [ETL].[BuildTraAEBaseEncounter]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	AE.BaseEncounter target
using
	(
	select
		 EncounterRecno = cast(EncounterRecno as int)
		,SourceUniqueID = cast(SourceUniqueID as varchar(50))
		,UniqueBookingReferenceNo = cast(UniqueBookingReferenceNo as varchar(50))
		,PathwayId = cast(PathwayId as varchar(50))
		,PathwayIdIssuerCode = cast(PathwayIdIssuerCode as varchar(10))
		,RTTStatusCode = cast(RTTStatusCode as varchar(10))
		,RTTStartDate = cast(RTTStartDate as smalldatetime)
		,RTTEndDate = cast(RTTEndDate as smalldatetime)
		,DistrictNo = cast(DistrictNo as varchar(50))
		,TrustNo = cast(TrustNo as varchar(50))
		,CasenoteNo = cast(CasenoteNo as varchar(50))
		,DistrictNoOrganisationCode = cast(DistrictNoOrganisationCode as varchar(10))
		,NHSNumber = cast(NHSNumber as varchar(17))
		,NHSNumberStatusCode = cast(NHSNumberStatusCode as varchar(10))
		,PatientTitle = cast(PatientTitle as varchar(50))
		,PatientForename = cast(PatientForename as varchar(100))
		,PatientSurname = cast(PatientSurname as varchar(100))
		,PatientAddress1 = cast(PatientAddress1 as varchar(50))
		,PatientAddress2 = cast(PatientAddress2 as varchar(50))
		,PatientAddress3 = cast(PatientAddress3 as varchar(50))
		,PatientAddress4 = cast(PatientAddress4 as varchar(50))
		,Postcode = cast(Postcode as varchar(10))
		,DateOfBirth = cast(DateOfBirth as datetime)
		,DateOfDeath = cast(DateOfDeath as smalldatetime)
		,SexCode = cast(SexCode as varchar(10))
		,CarerSupportIndicator = cast(CarerSupportIndicator as bit)
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(10))
		,RegisteredPracticeCode = cast(RegisteredPracticeCode as varchar(10))
		,AttendanceNumber = cast(AttendanceNumber as varchar(50))
		,ArrivalModeCode = cast(ArrivalModeCode as varchar(10))
		,AttendanceCategoryCode = cast(AttendanceCategoryCode as varchar(10))
		,AttendanceDisposalCode = cast(AttendanceDisposalCode as varchar(10))
		,IncidentLocationTypeCode = cast(IncidentLocationTypeCode as varchar(10))
		,PatientGroupCode = cast(PatientGroupCode as varchar(10))
		,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(10))
		,ArrivalDate = cast(ArrivalDate as smalldatetime)
		,ArrivalTime = cast(ArrivalTime as smalldatetime)
		,AgeOnArrival = cast(AgeOnArrival as tinyint)
		,InitialAssessmentTime = cast(InitialAssessmentTime as smalldatetime)
		,SeenForTreatmentTime = cast(SeenForTreatmentTime as smalldatetime)
		,AttendanceConclusionTime = cast(AttendanceConclusionTime as smalldatetime)
		,DepartureTime = cast(DepartureTime as smalldatetime)
		,CommissioningSerialNo = cast(CommissioningSerialNo as varchar(50))
		,NHSServiceAgreementLineNo = cast(NHSServiceAgreementLineNo as varchar(50))
		,ProviderReferenceNo = cast(ProviderReferenceNo as varchar(50))
		,CommissionerReferenceNo = cast(CommissionerReferenceNo as varchar(50))
		,ProviderCode = cast(ProviderCode as varchar(10))
		,CommissionerCode = cast(CommissionerCode as varchar(10))
		,StaffMemberCode = cast(StaffMemberCode as varchar(10))
		,InvestigationCodeFirst = cast(InvestigationCodeFirst as varchar(10))
		,InvestigationCodeSecond = cast(InvestigationCodeSecond as varchar(10))
		,DiagnosisCodeFirst = cast(DiagnosisCodeFirst as varchar(10))
		,DiagnosisCodeSecond = cast(DiagnosisCodeSecond as varchar(10))
		,TreatmentCodeFirst = cast(TreatmentCodeFirst as varchar(10))
		,TreatmentCodeSecond = cast(TreatmentCodeSecond as varchar(10))
		,PASHRGCode = cast(PASHRGCode as varchar(10))
		,HRGVersionCode = cast(HRGVersionCode as varchar(10))
		,PASDGVPCode = cast(PASDGVPCode as varchar(10))
		,SiteCode = cast(SiteCode as varchar(10))
		,Created = cast(Created as datetime)
		,Updated = cast(Updated as datetime)
		,ByWhom = cast(ByWhom as varchar(50))
		,InterfaceCode = cast(InterfaceCode as varchar(5))
		,PCTCode = cast(PCTCode as varchar(8))
		,ResidencePCTCode = cast(ResidencePCTCode as varchar(10))
		,InvestigationCodeList = cast(InvestigationCodeList as varchar(16))
		,TriageCategoryCode = cast(TriageCategoryCode as varchar(10))
		,PresentingProblem = cast(PresentingProblem as varchar(4000))
		,PresentingProblemCode = cast(PresentingProblemCode as varchar(10))
		,ToXrayTime = cast(ToXrayTime as smalldatetime)
		,FromXrayTime = cast(FromXrayTime as smalldatetime)
		,ToSpecialtyTime = cast(ToSpecialtyTime as smalldatetime)
		,SeenBySpecialtyTime = cast(SeenBySpecialtyTime as smalldatetime)
		,EthnicCategoryCode = cast(EthnicCategoryCode as varchar(10))
		,ReligionCode = cast(ReligionCode as varchar(10))
		,ReferredToSpecialtyCode = cast(ReferredToSpecialtyCode as varchar(10))
		,DecisionToAdmitTime = cast(DecisionToAdmitTime as smalldatetime)
		,DischargeDestinationCode = cast(DischargeDestinationCode as varchar(10))
		,RegisteredTime = cast(RegisteredTime as smalldatetime)
		,TransportRequestTime = cast(TransportRequestTime as smalldatetime)
		,TransportAvailableTime = cast(TransportAvailableTime as smalldatetime)
		,AscribeLeftDeptTime = cast(AscribeLeftDeptTime as smalldatetime)
		,CDULeftDepartmentTime = cast(CDULeftDepartmentTime as smalldatetime)
		,PCDULeftDepartmentTime = cast(PCDULeftDepartmentTime as smalldatetime)
		,TreatmentDateFirst = cast(TreatmentDateFirst as smalldatetime)
		,TreatmentDateSecond = cast(TreatmentDateSecond as smalldatetime)
		,SourceAttendanceDisposalCode = cast(SourceAttendanceDisposalCode as int)
		,AmbulanceArrivalTime = cast(AmbulanceArrivalTime as smalldatetime)
		,AmbulanceCrewPRF = cast(AmbulanceCrewPRF as varchar(30))
		,UnplannedReattend7Day = cast(UnplannedReattend7Day as int)
		,ArrivalTimeAdjusted = cast(ArrivalTimeAdjusted as smalldatetime)
		,ClinicalAssessmentTime = cast(ClinicalAssessmentTime as smalldatetime)
		,LevelOfCareCode = cast(LevelOfCareCode as varchar(10))
		,EncounterBreachStatusCode = cast(EncounterBreachStatusCode as varchar(1))
		,EncounterStartTimeOfDay = cast(EncounterStartTimeOfDay as int)
		,EncounterEndTimeOfDay = cast(EncounterEndTimeOfDay as int)
		,EncounterStartDate = cast(EncounterStartDate as date)
		,EncounterEndDate = cast(EncounterEndDate as date)
		,EncounterDurationMinutes = cast(EncounterDurationMinutes as int)
		,AgeCode = cast(AgeCode as varchar(27))
		,LeftWithoutBeingSeenCases = cast(LeftWithoutBeingSeenCases as int)
		,HRGCode = cast(HRGCode as varchar(10))
		,Reportable = cast(Reportable as bit)
		,CareGroup = cast(CareGroup as varchar(20))
		,EPMINo = cast(EPMINo as varchar(50))
		,DepartmentTypeCode = cast(DepartmentTypeCode as varchar(2))
		,DepartureTimeAdjusted = cast(DepartureTimeAdjusted as smalldatetime)
		,CarePathwayAttendanceConclusionTime = cast(CarePathwayAttendanceConclusionTime as smalldatetime)
		,CarePathwayDepartureTime = cast(CarePathwayDepartureTime as smalldatetime)
		,ContextCode = cast(ContextCode as varchar(10))
		,GpCodeAtAttendance = cast(GpCodeAtAttendance as varchar(8))
		,GpPracticeCodeAtAttendance = cast(GpPracticeCodeAtAttendance as varchar(8))
		,PostcodeAtAttendance = cast(PostcodeAtAttendance as varchar(8))
		,CCGCode = cast(CCGCode as varchar(8))
		,BaseUpdated = getdate()
		,AttendanceResidenceCCGCode = cast(AttendanceResidenceCCGCode as varchar(10))
		,ServiceID = cast(ServiceID as int)
	from
		ETL.TLoadTraAEBaseEncounter Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno

	when not matched by source
	and	target.ContextCode = 'TRA||SYM'
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,UniqueBookingReferenceNo
			,PathwayId
			,PathwayIdIssuerCode
			,RTTStatusCode
			,RTTStartDate
			,RTTEndDate
			,DistrictNo
			,TrustNo
			,CasenoteNo
			,DistrictNoOrganisationCode
			,NHSNumber
			,NHSNumberStatusCode
			,PatientTitle
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,Postcode
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,CarerSupportIndicator
			,RegisteredGpCode
			,RegisteredPracticeCode
			,AttendanceNumber
			,ArrivalModeCode
			,AttendanceCategoryCode
			,AttendanceDisposalCode
			,IncidentLocationTypeCode
			,PatientGroupCode
			,SourceOfReferralCode
			,ArrivalDate
			,ArrivalTime
			,AgeOnArrival
			,InitialAssessmentTime
			,SeenForTreatmentTime
			,AttendanceConclusionTime
			,DepartureTime
			,CommissioningSerialNo
			,NHSServiceAgreementLineNo
			,ProviderReferenceNo
			,CommissionerReferenceNo
			,ProviderCode
			,CommissionerCode
			,StaffMemberCode
			,InvestigationCodeFirst
			,InvestigationCodeSecond
			,DiagnosisCodeFirst
			,DiagnosisCodeSecond
			,TreatmentCodeFirst
			,TreatmentCodeSecond
			,PASHRGCode
			,HRGVersionCode
			,PASDGVPCode
			,SiteCode
			,Created
			,Updated
			,ByWhom
			,InterfaceCode
			,PCTCode
			,ResidencePCTCode
			,InvestigationCodeList
			,TriageCategoryCode
			,PresentingProblem
			,PresentingProblemCode
			,ToXrayTime
			,FromXrayTime
			,ToSpecialtyTime
			,SeenBySpecialtyTime
			,EthnicCategoryCode
			,ReligionCode
			,ReferredToSpecialtyCode
			,DecisionToAdmitTime
			,DischargeDestinationCode
			,RegisteredTime
			,TransportRequestTime
			,TransportAvailableTime
			,AscribeLeftDeptTime
			,CDULeftDepartmentTime
			,PCDULeftDepartmentTime
			,TreatmentDateFirst
			,TreatmentDateSecond
			,SourceAttendanceDisposalCode
			,AmbulanceArrivalTime
			,AmbulanceCrewPRF
			,UnplannedReattend7Day
			,ArrivalTimeAdjusted
			,ClinicalAssessmentTime
			,LevelOfCareCode
			,EncounterBreachStatusCode
			,EncounterStartTimeOfDay
			,EncounterEndTimeOfDay
			,EncounterStartDate
			,EncounterEndDate
			,EncounterDurationMinutes
			,AgeCode
			,LeftWithoutBeingSeenCases
			,HRGCode
			,Reportable
			,CareGroup
			,EPMINo
			,DepartmentTypeCode
			,DepartureTimeAdjusted
			,CarePathwayAttendanceConclusionTime
			,CarePathwayDepartureTime
			,ContextCode
			,GpCodeAtAttendance
			,GpPracticeCodeAtAttendance
			,PostcodeAtAttendance
			,CCGCode
			,BaseUpdated
			,AttendanceResidenceCCGCode
			,ServiceID
			)
		values
			(
			 EncounterRecno
			,SourceUniqueID
			,UniqueBookingReferenceNo
			,PathwayId
			,PathwayIdIssuerCode
			,RTTStatusCode
			,RTTStartDate
			,RTTEndDate
			,DistrictNo
			,TrustNo
			,CasenoteNo
			,DistrictNoOrganisationCode
			,NHSNumber
			,NHSNumberStatusCode
			,PatientTitle
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,Postcode
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,CarerSupportIndicator
			,RegisteredGpCode
			,RegisteredPracticeCode
			,AttendanceNumber
			,ArrivalModeCode
			,AttendanceCategoryCode
			,AttendanceDisposalCode
			,IncidentLocationTypeCode
			,PatientGroupCode
			,SourceOfReferralCode
			,ArrivalDate
			,ArrivalTime
			,AgeOnArrival
			,InitialAssessmentTime
			,SeenForTreatmentTime
			,AttendanceConclusionTime
			,DepartureTime
			,CommissioningSerialNo
			,NHSServiceAgreementLineNo
			,ProviderReferenceNo
			,CommissionerReferenceNo
			,ProviderCode
			,CommissionerCode
			,StaffMemberCode
			,InvestigationCodeFirst
			,InvestigationCodeSecond
			,DiagnosisCodeFirst
			,DiagnosisCodeSecond
			,TreatmentCodeFirst
			,TreatmentCodeSecond
			,PASHRGCode
			,HRGVersionCode
			,PASDGVPCode
			,SiteCode
			,Created
			,Updated
			,ByWhom
			,InterfaceCode
			,PCTCode
			,ResidencePCTCode
			,InvestigationCodeList
			,TriageCategoryCode
			,PresentingProblem
			,PresentingProblemCode
			,ToXrayTime
			,FromXrayTime
			,ToSpecialtyTime
			,SeenBySpecialtyTime
			,EthnicCategoryCode
			,ReligionCode
			,ReferredToSpecialtyCode
			,DecisionToAdmitTime
			,DischargeDestinationCode
			,RegisteredTime
			,TransportRequestTime
			,TransportAvailableTime
			,AscribeLeftDeptTime
			,CDULeftDepartmentTime
			,PCDULeftDepartmentTime
			,TreatmentDateFirst
			,TreatmentDateSecond
			,SourceAttendanceDisposalCode
			,AmbulanceArrivalTime
			,AmbulanceCrewPRF
			,UnplannedReattend7Day
			,ArrivalTimeAdjusted
			,ClinicalAssessmentTime
			,LevelOfCareCode
			,EncounterBreachStatusCode
			,EncounterStartTimeOfDay
			,EncounterEndTimeOfDay
			,EncounterStartDate
			,EncounterEndDate
			,EncounterDurationMinutes
			,AgeCode
			,LeftWithoutBeingSeenCases
			,HRGCode
			,Reportable
			,CareGroup
			,EPMINo
			,DepartmentTypeCode
			,DepartureTimeAdjusted
			,CarePathwayAttendanceConclusionTime
			,CarePathwayDepartureTime
			,ContextCode
			,GpCodeAtAttendance
			,GpPracticeCodeAtAttendance
			,PostcodeAtAttendance
			,CCGCode
			,BaseUpdated
			,AttendanceResidenceCCGCode
			,ServiceID
			)

	when matched
	and not
		(
			isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.UniqueBookingReferenceNo, '') = isnull(source.UniqueBookingReferenceNo, '')
		and isnull(target.PathwayId, '') = isnull(source.PathwayId, '')
		and isnull(target.PathwayIdIssuerCode, '') = isnull(source.PathwayIdIssuerCode, '')
		and isnull(target.RTTStatusCode, '') = isnull(source.RTTStatusCode, '')
		and isnull(target.RTTStartDate, getdate()) = isnull(source.RTTStartDate, getdate())
		and isnull(target.RTTEndDate, getdate()) = isnull(source.RTTEndDate, getdate())
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.TrustNo, '') = isnull(source.TrustNo, '')
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.DistrictNoOrganisationCode, '') = isnull(source.DistrictNoOrganisationCode, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.NHSNumberStatusCode, 0) = isnull(source.NHSNumberStatusCode, 0)
		and isnull(target.PatientTitle, '') = isnull(source.PatientTitle, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.PatientAddress1, '') = isnull(source.PatientAddress1, '')
		and isnull(target.PatientAddress2, '') = isnull(source.PatientAddress2, '')
		and isnull(target.PatientAddress3, '') = isnull(source.PatientAddress3, '')
		and isnull(target.PatientAddress4, '') = isnull(source.PatientAddress4, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.CarerSupportIndicator, 0) = isnull(source.CarerSupportIndicator, 0)
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredPracticeCode, '') = isnull(source.RegisteredPracticeCode, '')
		and isnull(target.AttendanceNumber, '') = isnull(source.AttendanceNumber, '')
		and isnull(target.ArrivalModeCode, '') = isnull(source.ArrivalModeCode, '')
		and isnull(target.AttendanceCategoryCode, '') = isnull(source.AttendanceCategoryCode, '')
		and isnull(target.AttendanceDisposalCode, '') = isnull(source.AttendanceDisposalCode, '')
		and isnull(target.IncidentLocationTypeCode, '') = isnull(source.IncidentLocationTypeCode, '')
		and isnull(target.PatientGroupCode, '') = isnull(source.PatientGroupCode, '')
		and isnull(target.SourceOfReferralCode, '') = isnull(source.SourceOfReferralCode, '')
		and isnull(target.ArrivalDate, getdate()) = isnull(source.ArrivalDate, getdate())
		and isnull(target.ArrivalTime, getdate()) = isnull(source.ArrivalTime, getdate())
		and isnull(target.AgeOnArrival, 0) = isnull(source.AgeOnArrival, 0)
		and isnull(target.InitialAssessmentTime, getdate()) = isnull(source.InitialAssessmentTime, getdate())
		and isnull(target.SeenForTreatmentTime, getdate()) = isnull(source.SeenForTreatmentTime, getdate())
		and isnull(target.AttendanceConclusionTime, getdate()) = isnull(source.AttendanceConclusionTime, getdate())
		and isnull(target.DepartureTime, getdate()) = isnull(source.DepartureTime, getdate())
		and isnull(target.CommissioningSerialNo, '') = isnull(source.CommissioningSerialNo, '')
		and isnull(target.NHSServiceAgreementLineNo, '') = isnull(source.NHSServiceAgreementLineNo, '')
		and isnull(target.ProviderReferenceNo, '') = isnull(source.ProviderReferenceNo, '')
		and isnull(target.CommissionerReferenceNo, '') = isnull(source.CommissionerReferenceNo, '')
		and isnull(target.ProviderCode, '') = isnull(source.ProviderCode, '')
		and isnull(target.CommissionerCode, '') = isnull(source.CommissionerCode, '')
		and isnull(target.StaffMemberCode, '') = isnull(source.StaffMemberCode, '')
		and isnull(target.InvestigationCodeFirst, '') = isnull(source.InvestigationCodeFirst, '')
		and isnull(target.InvestigationCodeSecond, '') = isnull(source.InvestigationCodeSecond, '')
		and isnull(target.DiagnosisCodeFirst, '') = isnull(source.DiagnosisCodeFirst, '')
		and isnull(target.DiagnosisCodeSecond, '') = isnull(source.DiagnosisCodeSecond, '')
		and isnull(target.TreatmentCodeFirst, '') = isnull(source.TreatmentCodeFirst, '')
		and isnull(target.TreatmentCodeSecond, '') = isnull(source.TreatmentCodeSecond, '')
		and isnull(target.PASHRGCode, '') = isnull(source.PASHRGCode, '')
		and isnull(target.HRGVersionCode, '') = isnull(source.HRGVersionCode, '')
		and isnull(target.PASDGVPCode, '') = isnull(source.PASDGVPCode, '')
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.Created, getdate()) = isnull(source.Created, getdate())
		and isnull(target.Updated, getdate()) = isnull(source.Updated, getdate())
		and isnull(target.ByWhom, '') = isnull(source.ByWhom, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.PCTCode, '') = isnull(source.PCTCode, '')
		and isnull(target.ResidencePCTCode, '') = isnull(source.ResidencePCTCode, '')
		and isnull(target.InvestigationCodeList, '') = isnull(source.InvestigationCodeList, '')
		and isnull(target.TriageCategoryCode, '') = isnull(source.TriageCategoryCode, '')
		and isnull(target.PresentingProblem, '') = isnull(source.PresentingProblem, '')
		and isnull(target.PresentingProblemCode, '') = isnull(source.PresentingProblemCode, '')
		and isnull(target.ToXrayTime, getdate()) = isnull(source.ToXrayTime, getdate())
		and isnull(target.FromXrayTime, getdate()) = isnull(source.FromXrayTime, getdate())
		and isnull(target.ToSpecialtyTime, getdate()) = isnull(source.ToSpecialtyTime, getdate())
		and isnull(target.SeenBySpecialtyTime, getdate()) = isnull(source.SeenBySpecialtyTime, getdate())
		and isnull(target.EthnicCategoryCode, '') = isnull(source.EthnicCategoryCode, '')
		and isnull(target.ReligionCode, '') = isnull(source.ReligionCode, '')
		and isnull(target.ReferredToSpecialtyCode, '') = isnull(source.ReferredToSpecialtyCode, '')
		and isnull(target.DecisionToAdmitTime, getdate()) = isnull(source.DecisionToAdmitTime, getdate())
		and isnull(target.DischargeDestinationCode, '') = isnull(source.DischargeDestinationCode, '')
		and isnull(target.RegisteredTime, getdate()) = isnull(source.RegisteredTime, getdate())
		and isnull(target.TransportRequestTime, getdate()) = isnull(source.TransportRequestTime, getdate())
		and isnull(target.TransportAvailableTime, getdate()) = isnull(source.TransportAvailableTime, getdate())
		and isnull(target.AscribeLeftDeptTime, getdate()) = isnull(source.AscribeLeftDeptTime, getdate())
		and isnull(target.CDULeftDepartmentTime, getdate()) = isnull(source.CDULeftDepartmentTime, getdate())
		and isnull(target.PCDULeftDepartmentTime, getdate()) = isnull(source.PCDULeftDepartmentTime, getdate())
		and isnull(target.TreatmentDateFirst, getdate()) = isnull(source.TreatmentDateFirst, getdate())
		and isnull(target.TreatmentDateSecond, getdate()) = isnull(source.TreatmentDateSecond, getdate())
		and isnull(target.SourceAttendanceDisposalCode, 0) = isnull(source.SourceAttendanceDisposalCode, 0)
		and isnull(target.AmbulanceArrivalTime, getdate()) = isnull(source.AmbulanceArrivalTime, getdate())
		and isnull(target.AmbulanceCrewPRF, '') = isnull(source.AmbulanceCrewPRF, '')
		and isnull(target.UnplannedReattend7Day, 0) = isnull(source.UnplannedReattend7Day, 0)
		and isnull(target.ArrivalTimeAdjusted, getdate()) = isnull(source.ArrivalTimeAdjusted, getdate())
		and isnull(target.ClinicalAssessmentTime, getdate()) = isnull(source.ClinicalAssessmentTime, getdate())
		and isnull(target.LevelOfCareCode, '') = isnull(source.LevelOfCareCode, '')
		and isnull(target.EncounterBreachStatusCode, '') = isnull(source.EncounterBreachStatusCode, '')
		and isnull(target.EncounterStartTimeOfDay, 0) = isnull(source.EncounterStartTimeOfDay, 0)
		and isnull(target.EncounterEndTimeOfDay, 0) = isnull(source.EncounterEndTimeOfDay, 0)
		and isnull(target.EncounterStartDate, getdate()) = isnull(source.EncounterStartDate, getdate())
		and isnull(target.EncounterEndDate, getdate()) = isnull(source.EncounterEndDate, getdate())
		and isnull(target.EncounterDurationMinutes, 0) = isnull(source.EncounterDurationMinutes, 0)
		and isnull(target.AgeCode, '') = isnull(source.AgeCode, '')
		and isnull(target.LeftWithoutBeingSeenCases, 0) = isnull(source.LeftWithoutBeingSeenCases, 0)
		and isnull(target.HRGCode, '') = isnull(source.HRGCode, '')
		and isnull(target.Reportable, 0) = isnull(source.Reportable, 0)
		and isnull(target.CareGroup, '') = isnull(source.CareGroup, '')
		and isnull(target.EPMINo, '') = isnull(source.EPMINo, '')
		and isnull(target.DepartmentTypeCode, '') = isnull(source.DepartmentTypeCode, '')
		and isnull(target.DepartureTimeAdjusted, getdate()) = isnull(source.DepartureTimeAdjusted, getdate())
		and isnull(target.CarePathwayAttendanceConclusionTime, getdate()) = isnull(source.CarePathwayAttendanceConclusionTime, getdate())
		and isnull(target.CarePathwayDepartureTime, getdate()) = isnull(source.CarePathwayDepartureTime, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.GpCodeAtAttendance, '') = isnull(source.GpCodeAtAttendance, '')
		and isnull(target.GpPracticeCodeAtAttendance, '') = isnull(source.GpPracticeCodeAtAttendance, '')
		and isnull(target.PostcodeAtAttendance, '') = isnull(source.PostcodeAtAttendance, '')
		and isnull(target.CCGCode, '') = isnull(source.CCGCode, '')
		and isnull(target.AttendanceResidenceCCGCode, '') = isnull(source.AttendanceResidenceCCGCode, '')
		and isnull(target.ServiceID, 0) = isnull(source.ServiceID, 0)
		)
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.UniqueBookingReferenceNo = source.UniqueBookingReferenceNo
			,target.PathwayId = source.PathwayId
			,target.PathwayIdIssuerCode = source.PathwayIdIssuerCode
			,target.RTTStatusCode = source.RTTStatusCode
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.DistrictNo = source.DistrictNo
			,target.TrustNo = source.TrustNo
			,target.CasenoteNo = source.CasenoteNo
			,target.DistrictNoOrganisationCode = source.DistrictNoOrganisationCode
			,target.NHSNumber = source.NHSNumber
			,target.NHSNumberStatusCode = source.NHSNumberStatusCode
			,target.PatientTitle = source.PatientTitle
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.Postcode = source.Postcode
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.SexCode = source.SexCode
			,target.CarerSupportIndicator = source.CarerSupportIndicator
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredPracticeCode = source.RegisteredPracticeCode
			,target.AttendanceNumber = source.AttendanceNumber
			,target.ArrivalModeCode = source.ArrivalModeCode
			,target.AttendanceCategoryCode = source.AttendanceCategoryCode
			,target.AttendanceDisposalCode = source.AttendanceDisposalCode
			,target.IncidentLocationTypeCode = source.IncidentLocationTypeCode
			,target.PatientGroupCode = source.PatientGroupCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ArrivalDate = source.ArrivalDate
			,target.ArrivalTime = source.ArrivalTime
			,target.AgeOnArrival = source.AgeOnArrival
			,target.InitialAssessmentTime = source.InitialAssessmentTime
			,target.SeenForTreatmentTime = source.SeenForTreatmentTime
			,target.AttendanceConclusionTime = source.AttendanceConclusionTime
			,target.DepartureTime = source.DepartureTime
			,target.CommissioningSerialNo = source.CommissioningSerialNo
			,target.NHSServiceAgreementLineNo = source.NHSServiceAgreementLineNo
			,target.ProviderReferenceNo = source.ProviderReferenceNo
			,target.CommissionerReferenceNo = source.CommissionerReferenceNo
			,target.ProviderCode = source.ProviderCode
			,target.CommissionerCode = source.CommissionerCode
			,target.StaffMemberCode = source.StaffMemberCode
			,target.InvestigationCodeFirst = source.InvestigationCodeFirst
			,target.InvestigationCodeSecond = source.InvestigationCodeSecond
			,target.DiagnosisCodeFirst = source.DiagnosisCodeFirst
			,target.DiagnosisCodeSecond = source.DiagnosisCodeSecond
			,target.TreatmentCodeFirst = source.TreatmentCodeFirst
			,target.TreatmentCodeSecond = source.TreatmentCodeSecond
			,target.PASHRGCode = source.PASHRGCode
			,target.HRGVersionCode = source.HRGVersionCode
			,target.PASDGVPCode = source.PASDGVPCode
			,target.SiteCode = source.SiteCode
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
			,target.InterfaceCode = source.InterfaceCode
			,target.PCTCode = source.PCTCode
			,target.ResidencePCTCode = source.ResidencePCTCode
			,target.InvestigationCodeList = source.InvestigationCodeList
			,target.TriageCategoryCode = source.TriageCategoryCode
			,target.PresentingProblem = source.PresentingProblem
			,target.PresentingProblemCode = source.PresentingProblemCode
			,target.ToXrayTime = source.ToXrayTime
			,target.FromXrayTime = source.FromXrayTime
			,target.ToSpecialtyTime = source.ToSpecialtyTime
			,target.SeenBySpecialtyTime = source.SeenBySpecialtyTime
			,target.EthnicCategoryCode = source.EthnicCategoryCode
			,target.ReligionCode = source.ReligionCode
			,target.ReferredToSpecialtyCode = source.ReferredToSpecialtyCode
			,target.DecisionToAdmitTime = source.DecisionToAdmitTime
			,target.DischargeDestinationCode = source.DischargeDestinationCode
			,target.RegisteredTime = source.RegisteredTime
			,target.TransportRequestTime = source.TransportRequestTime
			,target.TransportAvailableTime = source.TransportAvailableTime
			,target.AscribeLeftDeptTime = source.AscribeLeftDeptTime
			,target.CDULeftDepartmentTime = source.CDULeftDepartmentTime
			,target.PCDULeftDepartmentTime = source.PCDULeftDepartmentTime
			,target.TreatmentDateFirst = source.TreatmentDateFirst
			,target.TreatmentDateSecond = source.TreatmentDateSecond
			,target.SourceAttendanceDisposalCode = source.SourceAttendanceDisposalCode
			,target.AmbulanceArrivalTime = source.AmbulanceArrivalTime
			,target.AmbulanceCrewPRF = source.AmbulanceCrewPRF
			,target.UnplannedReattend7Day = source.UnplannedReattend7Day
			,target.ArrivalTimeAdjusted = source.ArrivalTimeAdjusted
			,target.ClinicalAssessmentTime = source.ClinicalAssessmentTime
			,target.LevelOfCareCode = source.LevelOfCareCode
			,target.EncounterBreachStatusCode = source.EncounterBreachStatusCode
			,target.EncounterStartTimeOfDay = source.EncounterStartTimeOfDay
			,target.EncounterEndTimeOfDay = source.EncounterEndTimeOfDay
			,target.EncounterStartDate = source.EncounterStartDate
			,target.EncounterEndDate = source.EncounterEndDate
			,target.EncounterDurationMinutes = source.EncounterDurationMinutes
			,target.AgeCode = source.AgeCode
			,target.LeftWithoutBeingSeenCases = source.LeftWithoutBeingSeenCases
			,target.HRGCode = source.HRGCode
			,target.Reportable = source.Reportable
			,target.CareGroup = source.CareGroup
			,target.EPMINo = source.EPMINo
			,target.DepartmentTypeCode = source.DepartmentTypeCode
			,target.DepartureTimeAdjusted = source.DepartureTimeAdjusted
			,target.CarePathwayAttendanceConclusionTime = source.CarePathwayAttendanceConclusionTime
			,target.CarePathwayDepartureTime = source.CarePathwayDepartureTime
			,target.ContextCode = source.ContextCode
			,target.GpCodeAtAttendance = source.GpCodeAtAttendance
			,target.GpPracticeCodeAtAttendance = source.GpPracticeCodeAtAttendance
			,target.PostcodeAtAttendance = source.PostcodeAtAttendance
			,target.CCGCode = source.CCGCode
			,target.BaseUpdated = source.BaseUpdated
			,target.AttendanceResidenceCCGCode = source.AttendanceResidenceCCGCode
			,target.ServiceID = source.ServiceID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



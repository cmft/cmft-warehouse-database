﻿CREATE procedure [ETL].[BuildCOMBaseReferralReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				COM.BaseReferralReference
			)
			,'1 jan 1900'
		)

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	COM.BaseReferralReference
where
	not exists
	(
	select
		1
	from
		COM.BaseReferral
	where
		BaseReferral.MergeEncounterRecno = BaseReferralReference.MergeEncounterRecno
	)
and	ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	COM.BaseReferralReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,ReferredBySpecialtyID = ReferredBySpecialty.SourceValueID
		,ReferredToSpecialtyID = ReferredToSpecialty.SourceValueID
		,ReferredByProfessionalCarerID = ReferredByProfessionalCarer.SourceValueID
		,ReferredToProfessionalCarerID = ReferredToProfessionalCarer.SourceValueID
		,ReferredByStaffTeamID = ReferredByStaffTeam.SourceValueID
		,ReferredToStaffTeamID = ReferredToStaffTeam.SourceValueID
		,AgeID = Age.AgeID
		,EthnicCategoryID = EthnicCategory.SourceEthnicCategoryID
		,SexID = Sex.SourceSexID
		,ReferralReasonID = ReferralReason.SourceValueID
		,ReferralSourceID = ReferralSource.SourceValueID
		,ReferralTypeID = ReferralType.SourceValueID
		,ReferralCancelledReasonID = ReferralCancelledReason.SourceValueID
		,ReferralClosureReasonID = ReferralClosureReason.SourceValueID
		,RequestedServiceID = RequestedService.SourceValueID
		,ReferralStatusID = ReferralStatus.SourceValueID
		,ReferralOutcomeID = ReferralOutcome.SourceValueID
		,ReferredByGPPracticeID = ReferredByGPPractice.SourceValueID
		,ReferredToGPPracticeID = ReferredToGPPractice.SourceValueID
		,CreatedByID = CreatedBy.SourceValueID
		,ModifiedByID = ModifiedBy.SourceValueID

		,ReferralReceivedDateID =
			coalesce(
				 ReferralReceivedDate.DateID

				,case
				when Encounter.ReferralReceivedDate is null
				then NullDate.DateID

				when Encounter.ReferralReceivedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ReferralAuthorisedDateID =
			coalesce(
				 ReferralAuthorisedDate.DateID

				,case
				when Encounter.AuthorisedDate is null
				then NullDate.DateID

				when Encounter.AuthorisedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ReferralSentDateID =
			coalesce(
				 ReferralSentDate.DateID

				,case
				when Encounter.ReferralSentDate is null
				then NullDate.DateID

				when Encounter.ReferralSentDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ReferralCancelledDateID =
			coalesce(
				 ReferralCancelledDate.DateID

				,case
				when Encounter.CancelledDate is null
				then NullDate.DateID

				when Encounter.CancelledDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ReferralClosedDateID =
			coalesce(
				 ReferralClosedDate.DateID

				,case
				when Encounter.ReferralClosedDate is null
				then NullDate.DateID

				when Encounter.ReferralClosedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ReferredToHealthOrganisationID = null
		,ReferredByHealthOrganisationID = null

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		COM.BaseReferral Encounter

	left join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	left join COM.Specialty ReferredBySpecialty
	on	ReferredBySpecialty.SpecialtyID = coalesce(Encounter.ReferredBySpecialtyID, -1)

	left join COM.Specialty ReferredToSpecialty
	on	ReferredToSpecialty.SpecialtyID = coalesce(Encounter.ReferredToSpecialtyID, -1)

	left join COM.ProfessionalCarer ReferredByProfessionalCarer
	on	ReferredByProfessionalCarer.ProfessionalCarerID = coalesce(Encounter.ReferredByProfessionalCarerID, -1)

	left join COM.ProfessionalCarer ReferredToProfessionalCarer
	on	ReferredToProfessionalCarer.ProfessionalCarerID = coalesce(Encounter.ReferredToProfessionalCarerID, -1)

	left join COM.StaffTeam ReferredByStaffTeam
	on	ReferredByStaffTeam.StaffTeamID = coalesce(Encounter.ReferredByStaffTeamID, -1)

	left join COM.StaffTeam ReferredToStaffTeam
	on	ReferredToStaffTeam.StaffTeamID = coalesce(Encounter.ReferredToStaffTeamID, -1)

	left join COM.ReferralReason ReferralReason
	on	ReferralReason.ReferralReasonID = coalesce(Encounter.ReasonForReferralID, '-1')
	and	ReferralReason.ContextCode = Encounter.ContextCode

	left join COM.ReferralType ReferralType
	on	ReferralType.ReferralTypeID = coalesce(Encounter.TypeofReferralID, '-1')
	and	ReferralType.ContextCode = Encounter.ContextCode

	left join COM.ReferralSource ReferralSource
	on	ReferralSource.ReferralSourceID = coalesce(Encounter.SourceofReferralID, '-1')
	and	ReferralSource.ContextCode = Encounter.ContextCode

	left join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.PatientSexID, -1) as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	left join WH.EthnicCategory EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryCode = cast(coalesce(Encounter.PatientEthnicGroupID, -1) as varchar)
	and	EthnicCategory.SourceContextCode = Encounter.ContextCode

	left join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	left join COM.ReferralCancelledReason ReferralCancelledReason
	on	ReferralCancelledReason.ReferralCancelledReasonID = coalesce(Encounter.CancelledReasonID, '-1')
	and	ReferralCancelledReason.ContextCode = Encounter.ContextCode

	left join COM.ReferralClosureReason ReferralClosureReason
	on	ReferralClosureReason.ReferralClosureReasonID = coalesce(Encounter.ClosureReasonID, '-1')
	and	ReferralClosureReason.ContextCode = Encounter.ContextCode

	left join COM.RequestedService RequestedService
	on	RequestedService.RequestedServiceID = coalesce(Encounter.RequestedServiceID, '-1')
	and	RequestedService.ContextCode = Encounter.ContextCode

	left join COM.ReferralStatus ReferralStatus
	on	ReferralStatus.ReferralStatusID = coalesce(Encounter.ReferralStatus, '-1')
	and	ReferralStatus.ContextCode = Encounter.ContextCode

	left join COM.ReferralOutcome ReferralOutcome
	on	ReferralOutcome.ReferralOutcomeID = coalesce(Encounter.OutcomeOfReferral, '-1')
	and	ReferralOutcome.ContextCode = Encounter.ContextCode

	left join COM.GPPractice ReferredByGPPractice
	on	ReferredByGPPractice.GPPracticeID = coalesce(Encounter.ReferredByHealthOrgID, '-1')
	and	ReferredByGPPractice.ContextCode = Encounter.ContextCode

	left join COM.GPPractice ReferredToGPPractice
	on	ReferredToGPPractice.GPPracticeID = coalesce(Encounter.ReferredToHealthOrgID, '-1')
	and	ReferredToGPPractice.ContextCode = Encounter.ContextCode

	left join COM.SystemUser CreatedBy
	on	CreatedBy.SystemUserID = coalesce(Encounter.CreatedByID, '-1')

	left join COM.SystemUser ModifiedBy
	on	ModifiedBy.SystemUserID = coalesce(Encounter.ModifiedByID, '-1')

	left join WH.Calendar ReferralReceivedDate
	on	ReferralReceivedDate.TheDate = Encounter.ReferralReceivedDate

	left join WH.Calendar ReferralAuthorisedDate
	on	ReferralAuthorisedDate.TheDate = Encounter.AuthorisedDate

	left join WH.Calendar ReferralSentDate
	on	ReferralSentDate.TheDate = Encounter.ReferralSentDate

	left join WH.Calendar ReferralCancelledDate
	on	ReferralCancelledDate.TheDate = Encounter.CancelledDate

	left join WH.Calendar ReferralClosedDate
	on	ReferralClosedDate.TheDate = Encounter.ReferralClosedDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,ReferredBySpecialtyID
			,ReferredToSpecialtyID
			,ReferredByProfessionalCarerID
			,ReferredToProfessionalCarerID
			,ReferredByStaffTeamID
			,ReferredToStaffTeamID
			,CreatedByID
			,ModifiedByID
			,ReferralClosureReasonID
			,ReferralCancelledReasonID
			,ReferralReasonID
			,ReferralStatusID
			,ReferralTypeID
			,ReferralOutcomeID
			,ReferralSourceID
			,ReferredToHealthOrganisationID
			,ReferredByHealthOrganisationID
			,SexID
			,EthnicCategoryID
			,RequestedServiceID
			,ReferralReceivedDateID
			,AgeID
			,ReferralAuthorisedDateID
			,ReferralSentDateID
			,ReferralCancelledDateID
			,ReferralClosedDateID
			,ReferredByGPPracticeID
			,ReferredToGPPracticeID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.ReferredBySpecialtyID
			,source.ReferredToSpecialtyID
			,source.ReferredByProfessionalCarerID
			,source.ReferredToProfessionalCarerID
			,source.ReferredByStaffTeamID
			,source.ReferredToStaffTeamID
			,source.CreatedByID
			,source.ModifiedByID
			,source.ReferralClosureReasonID
			,source.ReferralCancelledReasonID
			,source.ReferralReasonID
			,source.ReferralStatusID
			,source.ReferralTypeID
			,source.ReferralOutcomeID
			,source.ReferralSourceID
			,source.ReferredToHealthOrganisationID
			,source.ReferredByHealthOrganisationID
			,source.SexID
			,source.EthnicCategoryID
			,source.RequestedServiceID
			,source.ReferralReceivedDateID
			,source.AgeID
			,source.ReferralAuthorisedDateID
			,source.ReferralSentDateID
			,source.ReferralCancelledDateID
			,source.ReferralClosedDateID
			,source.ReferredByGPPracticeID
			,source.ReferredToGPPracticeID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.ReferredBySpecialtyID = source.ReferredBySpecialtyID
			,target.ReferredToSpecialtyID = source.ReferredToSpecialtyID
			,target.ReferredByProfessionalCarerID = source.ReferredByProfessionalCarerID
			,target.ReferredToProfessionalCarerID = source.ReferredToProfessionalCarerID
			,target.ReferredByStaffTeamID = source.ReferredByStaffTeamID
			,target.ReferredToStaffTeamID = source.ReferredToStaffTeamID
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.ReferralClosureReasonID = source.ReferralClosureReasonID
			,target.ReferralCancelledReasonID = source.ReferralCancelledReasonID
			,target.ReferralReasonID = source.ReferralReasonID
			,target.ReferralStatusID = source.ReferralStatusID
			,target.ReferralTypeID = source.ReferralTypeID
			,target.ReferralOutcomeID = source.ReferralOutcomeID
			,target.ReferralSourceID = source.ReferralSourceID
			,target.ReferredToHealthOrganisationID = source.ReferredToHealthOrganisationID
			,target.ReferredByHealthOrganisationID = source.ReferredByHealthOrganisationID
			,target.SexID = source.SexID
			,target.EthnicCategoryID = source.EthnicCategoryID
			,target.RequestedServiceID = source.RequestedServiceID
			,target.ReferralReceivedDateID = source.ReferralReceivedDateID
			,target.AgeID = source.AgeID
			,target.ReferralAuthorisedDateID = source.ReferralAuthorisedDateID
			,target.ReferralSentDateID = source.ReferralSentDateID
			,target.ReferralCancelledDateID = source.ReferralCancelledDateID
			,target.ReferralClosedDateID = source.ReferralClosedDateID
			,target.ReferredByGPPracticeID = source.ReferredByGPPracticeID
			,target.ReferredToGPPracticeID = source.ReferredToGPPracticeID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

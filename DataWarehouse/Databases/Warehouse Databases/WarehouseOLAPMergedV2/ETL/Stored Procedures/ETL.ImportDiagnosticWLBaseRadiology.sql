﻿CREATE procedure [ETL].[ImportDiagnosticWLBaseRadiology] as

--import the data
exec ETL.BuildDiagnosticWLBaseRadiology


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[DiagnosticWL].[BaseRadiology]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildDiagnosticWLBaseRadiologyReference 'CMFT||CRIS'

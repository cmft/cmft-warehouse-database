﻿


CREATE procedure [ETL].[ImportBloodManagementBaseDrugAdministered] as

--import the data
exec ETL.BuildBloodManagementBaseDrugAdministered


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[BloodManagement].[BaseDrugAdministered]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
 exec ETL.BuildBloodManagementBaseDrugAdministeredReference 'CMFT||RECALL'




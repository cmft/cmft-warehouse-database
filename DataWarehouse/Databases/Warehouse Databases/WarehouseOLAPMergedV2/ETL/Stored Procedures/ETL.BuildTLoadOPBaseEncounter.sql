﻿CREATE procedure [ETL].[BuildTLoadOPBaseEncounter] as

truncate table ETL.TLoadOPBaseEncounter

--Central
insert
into
	ETL.TLoadOPBaseEncounter
(
	 EncounterRecno
	,Action
	,ContextCode
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,NHSNumberStatusCode
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,DNACode
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AttendanceOutcomeCode
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferralConsultantCode
	,ReferralSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryProcedureDate
	,SecondaryProcedureCode1
	,SecondaryProcedureDate1
	,SecondaryProcedureCode2
	,SecondaryProcedureDate2
	,SecondaryProcedureCode3
	,SecondaryProcedureDate3
	,SecondaryProcedureCode4
	,SecondaryProcedureDate4
	,SecondaryProcedureCode5
	,SecondaryProcedureDate5
	,SecondaryProcedureCode6
	,SecondaryProcedureDate6
	,SecondaryProcedureCode7
	,SecondaryProcedureDate7
	,SecondaryProcedureCode8
	,SecondaryProcedureDate8
	,SecondaryProcedureCode9
	,SecondaryProcedureDate9
	,SecondaryProcedureCode10
	,SecondaryProcedureDate10
	,SecondaryProcedureCode11
	,SecondaryProcedureDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
	,LocalAdminCategoryCode
	,PCTCode
	,LocalityCode
	,AgeCode
	,HRGCode
	,Cases
	,LengthOfWait
	,IsWardAttender
	,RTTActivity
	,RTTBreachStatusCode
	,ClockStartDate
	,RTTBreachDate
	,RTTTreated
	,DirectorateCode
	,ReferralSpecialtyTypeCode
	,LastDNAorPatientCancelledDate
	,ReferredByCode
	,ReferrerCode
	,AppointmentOutcomeCode
	,MedicalStaffTypeCode
	,TreatmentFunctionCode
	,CommissioningSerialNo
	,DerivedFirstAttendanceFlag
	,AttendanceIdentifier
	,RegisteredGpAtAppointmentCode
	,RegisteredGpPracticeAtAppointmentCode
	,ReferredByConsultantCode
	,ReferredByGpCode
	,ReferredByGdpCode
	,PseudoPostcode
	,CCGCode
	,EpisodicPostcode
	,GpCodeAtAppointment
	,GpPracticeCodeAtAppointment
	,PostcodeAtAppointment
	,EpisodicSiteCode
	,AppointmentResidenceCCGCode
)
select
	 ProcessList.EncounterRecno
	,ProcessList.Action
	,ContextCode = 'CEN||PAS'

	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,NHSNumberStatusCode
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,DNACode
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AttendanceOutcomeCode
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferralConsultantCode
	,ReferralSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryProcedureDate
	,SecondaryProcedureCode1
	,SecondaryProcedureDate1
	,SecondaryProcedureCode2
	,SecondaryProcedureDate2
	,SecondaryProcedureCode3
	,SecondaryProcedureDate3
	,SecondaryProcedureCode4
	,SecondaryProcedureDate4
	,SecondaryProcedureCode5
	,SecondaryProcedureDate5
	,SecondaryProcedureCode6
	,SecondaryProcedureDate6
	,SecondaryProcedureCode7
	,SecondaryProcedureDate7
	,SecondaryProcedureCode8
	,SecondaryProcedureDate8
	,SecondaryProcedureCode9
	,SecondaryProcedureDate9
	,SecondaryProcedureCode10
	,SecondaryProcedureDate10
	,SecondaryProcedureCode11
	,SecondaryProcedureDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
	,LocalAdminCategoryCode
	,PCTCode
	,LocalityCode
	,AgeCode
	,HRGCode
	,Cases
	,LengthOfWait
	,IsWardAttender
	,RTTActivity
	,RTTBreachStatusCode
	,ClockStartDate
	,RTTBreachDate
	,RTTTreated
	,DirectorateCode
	,ReferralSpecialtyTypeCode
	,LastDNAorPatientCancelledDate
	,ReferredByCode
	,ReferrerCode
	,AppointmentOutcomeCode
	,MedicalStaffTypeCode
	,TreatmentFunctionCode
	,CommissioningSerialNo
	,DerivedFirstAttendanceFlag
	,AttendanceIdentifier
	,RegisteredGpAtAppointmentCode
	,RegisteredGpPracticeAtAppointmentCode
	,ReferredByConsultantCode
	,ReferredByGpCode
	,ReferredByGdpCode
	,PseudoPostcode
	,CCGCode
	,EpisodicPostcode
	,GpCodeAtAppointment
	,GpPracticeCodeAtAppointment
	,PostcodeAtAppointment
	,EpisodicSiteCode
	,AppointmentResidenceCCGCode
from
	[$(Warehouse)].OP.ProcessList

left join ETL.TImportCenOPBaseEncounter Import
on	Import.EncounterRecno = ProcessList.EncounterRecno


--Trafford
insert
into
	ETL.TLoadOPBaseEncounter
(
	 EncounterRecno
	,Action
	,ContextCode

	,AdminCategoryCode
	,AgeCode
	,AppointmentCancelDate
	,AppointmentCategoryCode
	,AppointmentCreateDate
	,AppointmentCreatedBy
	,AppointmentDate
	,AppointmentOutcomeCode
	,AppointmentResidenceCCGCode
	,AppointmentStatusCode
	,AppointmentTime
	,AppointmentTypeCode
	,AttendanceIdentifier
	,AttendanceOutcomeCode
	,BookingTypeCode
	,ByWhom
	,CancelledByCode
	,CasenoteNo
	,Cases
	,CCGCode
	,ClinicCode
	,ClockStartDate
	,CommissioningSerialNo
	,ConsultantCode
	,ContractSerialNo
	,Created
	,DateOfBirth
	,DateOfDeath
	,DerivedFirstAttendanceFlag
	,DestinationSiteCode
	,DHACode
	,DirectorateCode
	,DischargeDate
	,DisposalCode
	,DistrictNo
	,DNACode
	,DoctorCode
	,EBookingReferenceNo
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicPostcode
	,EthnicOriginCode
	,FirstAttendanceFlag
	,GpCodeAtAppointment
	,GpPracticeCodeAtAppointment
	,HRGCode
	,InterfaceCode
	,IsWardAttender
	,LastDNAorPatientCancelledDate
	,LastRevisedBy
	,LastRevisedDate
	,LengthOfWait
	,LocalAdminCategoryCode
	,LocalityCode
	,MaritalStatusCode
	,MedicalStaffTypeCode
	,NHSNumber
	,NHSNumberStatusCode
	,OverseasStatusFlag
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientCancelReason
	,PatientChoiceCode
	,PatientForename
	,PatientSurname
	,PatientTitle
	,PCTCode
	,Postcode
	,PostcodeAtAppointment
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryProcedureDate
	,PriorityCode
	,ProviderCode
	,PurchaserCode
	,QM08EndWaitDate
	,QM08StartWaitDate
	,ReasonForReferralCode
	,ReferralConsultantCode
	,ReferralDate
	,ReferralSpecialtyCode
	,ReferralSpecialtyTypeCode
	,ReferredByCode
	,ReferredByConsultantCode
	,ReferredByGdpCode
	,ReferredByGpCode
	,ReferrerCode
	,RegisteredGpAtAppointmentCode
	,RegisteredGpCode
	,RegisteredGpPracticeAtAppointmentCode
	,RegisteredGpPracticeCode
	,ReligionCode
	,RTTActivity
	,RTTBreachDate
	,RTTBreachStatusCode
	,RTTCurrentPrivatePatientFlag
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTEndDate
	,RTTOverseasStatusFlag
	,RTTPathwayCondition
	,RTTPathwayID
	,RTTPeriodStatusCode
	,RTTSpecialtyCode
	,RTTStartDate
	,RTTTreated
	,ScheduledCancelReasonCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryProcedureCode1
	,SecondaryProcedureCode10
	,SecondaryProcedureCode11
	,SecondaryProcedureCode2
	,SecondaryProcedureCode3
	,SecondaryProcedureCode4
	,SecondaryProcedureCode5
	,SecondaryProcedureCode6
	,SecondaryProcedureCode7
	,SecondaryProcedureCode8
	,SecondaryProcedureCode9
	,SecondaryProcedureDate1
	,SecondaryProcedureDate10
	,SecondaryProcedureDate11
	,SecondaryProcedureDate2
	,SecondaryProcedureDate3
	,SecondaryProcedureDate4
	,SecondaryProcedureDate5
	,SecondaryProcedureDate6
	,SecondaryProcedureDate7
	,SecondaryProcedureDate8
	,SecondaryProcedureDate9
	,SexCode
	,SiteCode
	,SourceEncounterNo
	,SourceOfReferralCode
	,SourcePatientNo
	,SourceUniqueID
	,SpecialtyCode
	,SubsidiaryDiagnosisCode
	,TransportRequiredFlag
	,TreatmentFunctionCode
	,Updated
)
select
	 ProcessList.EncounterRecno
	,ProcessList.Action
	,ContextCode = 'TRA||UG'

	,AdminCategoryCode = cast(AdminCategoryCode as varchar(10))
	,AgeCode = cast(AgeCode as varchar(27))
	,AppointmentCancelDate = cast(AppointmentCancelDate as int)
	,AppointmentCategoryCode = cast(AppointmentCategoryCode as varchar(10))
	,AppointmentCreateDate = cast(AppointmentCreateDate as smalldatetime)
	,AppointmentCreatedBy = cast(AppointmentCreatedBy as varchar(10))
	,AppointmentDate = cast(AppointmentDate as smalldatetime)
	,AppointmentOutcomeCode = cast(AppointmentOutcomeCode as varchar(3))
	,AppointmentResidenceCCGCode = cast(AppointmentResidenceCCGCode as varchar(10))
	,AppointmentStatusCode = cast(AppointmentStatusCode as varchar(10))
	,AppointmentTime = cast(AppointmentTime as smalldatetime)
	,AppointmentTypeCode = cast(AppointmentTypeCode as varchar(10))
	,AttendanceIdentifier = cast(AttendanceIdentifier as varchar(40))
	,AttendanceOutcomeCode = cast(AttendanceOutcomeCode as varchar(10))
	,BookingTypeCode = cast(BookingTypeCode as varchar(10))
	,ByWhom = cast(ByWhom as varchar(50))
	,CancelledByCode = cast(CancelledByCode as varchar(1))
	,CasenoteNo = cast(CasenoteNo as varchar(16))
	,Cases = cast(Cases as int)
	,CCGCode = cast(CCGCode as varchar(10))
	,ClinicCode = cast(ClinicCode as varchar(10))
	,ClockStartDate = cast(ClockStartDate as smalldatetime)
	,CommissioningSerialNo = cast(CommissioningSerialNo as varchar(10))
	,ConsultantCode = cast(ConsultantCode as varchar(20))
	,ContractSerialNo = cast(ContractSerialNo as varchar(6))
	,Created = cast(Created as datetime)
	,DateOfBirth = cast(DateOfBirth as datetime)
	,DateOfDeath = cast(DateOfDeath as datetime)
	,DerivedFirstAttendanceFlag = cast(DerivedFirstAttendanceFlag as varchar(1))
	,DestinationSiteCode = cast(DestinationSiteCode as varchar(10))
	,DHACode = cast(DHACode as varchar(3))
	,DirectorateCode = cast(DirectorateCode as varchar(2))
	,DischargeDate = cast(DischargeDate as smalldatetime)
	,DisposalCode = cast(DisposalCode as varchar(10))
	,DistrictNo = cast(DistrictNo as varchar(12))
	,DNACode = cast(DNACode as varchar(10))
	,DoctorCode = cast(DoctorCode as varchar(20))
	,EBookingReferenceNo = cast(EBookingReferenceNo as varchar(50))
	,EpisodicGpCode = cast(EpisodicGpCode as varchar(8))
	,EpisodicGpPracticeCode = cast(EpisodicGpPracticeCode as varchar(6))
	,EpisodicPostCode = cast(EpisodicPostCode as varchar(8))
	,EthnicOriginCode = cast(EthnicOriginCode as varchar(10))
	,FirstAttendanceFlag = cast(FirstAttendanceFlag as varchar(10))
	,GpCodeAtAppointment = cast(GpCodeAtAppointment as varchar(8))
	,GpPracticeCodeAtAppointment = cast(GpPracticeCodeAtAppointment as varchar(8))
	,HRGCode = cast(HRGCode as varchar(10))
	,InterfaceCode = cast(InterfaceCode as varchar(5))
	,IsWardAttender = cast(IsWardAttender as bit)
	,LastDNAorPatientCancelledDate = cast(LastDNAorPatientCancelledDate as smalldatetime)
	,LastRevisedBy = cast(LastRevisedBy as varchar(10))
	,LastRevisedDate = cast(LastRevisedDate as smalldatetime)
	,LengthOfWait = cast(LengthOfWait as int)
	,LocalAdminCategoryCode = cast(LocalAdminCategoryCode as varchar(10))
	,LocalityCode = cast(LocalityCode as varchar(10))
	,MaritalStatusCode = cast(MaritalStatusCode as varchar(10))
	,MedicalStaffTypeCode = cast(MedicalStaffTypeCode as varchar(10))
	,NHSNumber = cast(NHSNumber as varchar(17))
	,NHSNumberStatusCode = cast(NHSNumberStatusCode as varchar(10))
	,OverseasStatusFlag = cast(OverseasStatusFlag as varchar(10))
	,PatientAddress1 = cast(PatientAddress1 as varchar(100))
	,PatientAddress2 = cast(PatientAddress2 as varchar(100))
	,PatientAddress3 = cast(PatientAddress3 as varchar(100))
	,PatientAddress4 = cast(PatientAddress4 as varchar(100))
	,PatientCancelReason = cast(PatientCancelReason as varchar(50))
	,PatientChoiceCode = cast(PatientChoiceCode as varchar(10))
	,PatientForename = cast(PatientForename as varchar(80))
	,PatientSurname = cast(PatientSurname as varchar(80))
	,PatientTitle = cast(PatientTitle as varchar(10))
	,PCTCode = cast(PCTCode as varchar(10))
	,Postcode = cast(Postcode as varchar(8))
	,PostcodeAtAppointment = cast(PostcodeAtAppointment as varchar(8))
	,PrimaryDiagnosisCode = cast(PrimaryDiagnosisCode as varchar(10))
	,PrimaryOperationCode = cast(PrimaryOperationCode as varchar(10))
	,PrimaryProcedureDate = cast(PrimaryProcedureDate as smalldatetime)
	,PriorityCode = cast(PriorityCode as varchar(10))
	,ProviderCode = cast(ProviderCode as varchar(10))
	,PurchaserCode = cast(PurchaserCode as varchar(10))
	,QM08EndWaitDate = cast(QM08EndWaitDate as smalldatetime)
	,QM08StartWaitDate = cast(QM08StartWaitDate as smalldatetime)
	,ReasonForReferralCode = cast(ReasonForReferralCode as varchar(10))
	,ReferralConsultantCode = cast(ReferralConsultantCode as varchar(20))
	,ReferralDate = cast(ReferralDate as smalldatetime)
	,ReferralSpecialtyCode = cast(ReferralSpecialtyCode as varchar(10))
	,ReferralSpecialtyTypeCode = cast(ReferralSpecialtyTypeCode as varchar(10))
	,ReferredByCode = cast(ReferredByCode as varchar(10))
	,ReferredByConsultantCode = cast(ReferredByConsultantCode as varchar(20))
	,ReferredByGdpCode = cast(ReferredByGdpCode as varchar(10))
	,ReferredByGpCode = cast(ReferredByGpCode as varchar(10))
	,ReferrerCode = cast(ReferrerCode as varchar(10))
	,RegisteredGpAtAppointmentCode = cast(RegisteredGpAtAppointmentCode as varchar(8))
	,RegisteredGpCode = cast(RegisteredGpCode as varchar(8))
	,RegisteredGpPracticeAtAppointmentCode = cast(RegisteredGpPracticeAtAppointmentCode as varchar(6))
	,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(6))
	,ReligionCode = cast(ReligionCode as varchar(10))
	,RTTActivity = cast(RTTActivity as int)
	,RTTBreachDate = cast(RTTBreachDate as smalldatetime)
	,RTTBreachStatusCode = cast(RTTBreachStatusCode as varchar(10))
	,RTTCurrentPrivatePatientFlag = cast(RTTCurrentPrivatePatientFlag as bit)
	,RTTCurrentProviderCode = cast(RTTCurrentProviderCode as varchar(10))
	,RTTCurrentStatusCode = cast(RTTCurrentStatusCode as varchar(10))
	,RTTCurrentStatusDate = cast(RTTCurrentStatusDate as smalldatetime)
	,RTTEndDate = cast(RTTEndDate as smalldatetime)
	,RTTOverseasStatusFlag = cast(RTTOverseasStatusFlag as varchar(10))
	,RTTPathwayCondition = cast(RTTPathwayCondition as varchar(20))
	,RTTPathwayID = cast(RTTPathwayID as varchar(25))
	,RTTPeriodStatusCode = cast(RTTPeriodStatusCode as varchar(10))
	,RTTSpecialtyCode = cast(RTTSpecialtyCode as varchar(10))
	,RTTStartDate = cast(RTTStartDate as smalldatetime)
	,RTTTreated = cast(RTTTreated as int)
	,ScheduledCancelReasonCode = cast(ScheduledCancelReasonCode as varchar(10))
	,SecondaryDiagnosisCode1 = cast(SecondaryDiagnosisCode1 as varchar(10))
	,SecondaryDiagnosisCode10 = cast(SecondaryDiagnosisCode10 as varchar(10))
	,SecondaryDiagnosisCode11 = cast(SecondaryDiagnosisCode11 as varchar(10))
	,SecondaryDiagnosisCode12 = cast(SecondaryDiagnosisCode12 as varchar(10))
	,SecondaryDiagnosisCode2 = cast(SecondaryDiagnosisCode2 as varchar(10))
	,SecondaryDiagnosisCode3 = cast(SecondaryDiagnosisCode3 as varchar(10))
	,SecondaryDiagnosisCode4 = cast(SecondaryDiagnosisCode4 as varchar(10))
	,SecondaryDiagnosisCode5 = cast(SecondaryDiagnosisCode5 as varchar(10))
	,SecondaryDiagnosisCode6 = cast(SecondaryDiagnosisCode6 as varchar(10))
	,SecondaryDiagnosisCode7 = cast(SecondaryDiagnosisCode7 as varchar(10))
	,SecondaryDiagnosisCode8 = cast(SecondaryDiagnosisCode8 as varchar(10))
	,SecondaryDiagnosisCode9 = cast(SecondaryDiagnosisCode9 as varchar(10))
	,SecondaryProcedureCode1 = cast(SecondaryProcedureCode1 as varchar(10))
	,SecondaryProcedureCode10 = cast(SecondaryProcedureCode10 as varchar(10))
	,SecondaryProcedureCode11 = cast(SecondaryProcedureCode11 as varchar(10))
	,SecondaryProcedureCode2 = cast(SecondaryProcedureCode2 as varchar(10))
	,SecondaryProcedureCode3 = cast(SecondaryProcedureCode3 as varchar(10))
	,SecondaryProcedureCode4 = cast(SecondaryProcedureCode4 as varchar(10))
	,SecondaryProcedureCode5 = cast(SecondaryProcedureCode5 as varchar(10))
	,SecondaryProcedureCode6 = cast(SecondaryProcedureCode6 as varchar(10))
	,SecondaryProcedureCode7 = cast(SecondaryProcedureCode7 as varchar(10))
	,SecondaryProcedureCode8 = cast(SecondaryProcedureCode8 as varchar(10))
	,SecondaryProcedureCode9 = cast(SecondaryProcedureCode9 as varchar(10))
	,SecondaryProcedureDate1 = cast(SecondaryProcedureDate1 as smalldatetime)
	,SecondaryProcedureDate10 = cast(SecondaryProcedureDate10 as smalldatetime)
	,SecondaryProcedureDate11 = cast(SecondaryProcedureDate11 as smalldatetime)
	,SecondaryProcedureDate2 = cast(SecondaryProcedureDate2 as smalldatetime)
	,SecondaryProcedureDate3 = cast(SecondaryProcedureDate3 as smalldatetime)
	,SecondaryProcedureDate4 = cast(SecondaryProcedureDate4 as smalldatetime)
	,SecondaryProcedureDate5 = cast(SecondaryProcedureDate5 as smalldatetime)
	,SecondaryProcedureDate6 = cast(SecondaryProcedureDate6 as smalldatetime)
	,SecondaryProcedureDate7 = cast(SecondaryProcedureDate7 as smalldatetime)
	,SecondaryProcedureDate8 = cast(SecondaryProcedureDate8 as smalldatetime)
	,SecondaryProcedureDate9 = cast(SecondaryProcedureDate9 as smalldatetime)
	,SexCode = cast(SexCode as varchar(10))
	,SiteCode = cast(SiteCode as varchar(10))
	,SourceEncounterNo = cast(SourceEncounterNo as varchar(20))
	,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(10))
	,SourcePatientNo = cast(SourcePatientNo as varchar(20))
	,SourceUniqueID = cast(SourceUniqueID as varchar(50))
	,SpecialtyCode = cast(SpecialtyCode as varchar(10))
	,SubsidiaryDiagnosisCode = cast(SubsidiaryDiagnosisCode as varchar(10))
	,TransportRequiredFlag = cast(TransportRequiredFlag as varchar(10))
	,TreatmentFunctionCode = cast(TreatmentFunctionCode as varchar(10))
	,Updated = cast(Updated as datetime)
from
	[$(TraffordWarehouse)].OP.ProcessList

inner join ETL.TImportTraOPBaseEncounter Import
on	Import.EncounterRecno = ProcessList.EncounterRecno


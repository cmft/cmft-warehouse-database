﻿CREATE procedure [ETL].[BuildCenTheatreBasePatientBooking]

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Theatre.BasePatientBooking target
using
	(
	select
		 SequenceNo
		,SourceUniqueID
		,DistrictNo
		,IntendedProcedureDate
		,IntendedTheatreCode
		,CustodianDetail
		,IntendedSessionTypeCode
		,Surname
		,Forename
		,DateOfBirth
		,PatientSourceUniqueID
		,TheatreCode
		,ConsultantCode
		,ConsultantCode2
		,BiohazardFlag
		,BloodProductsRequiredFlag
		,SpecialEquipmentFlag
		,PatientAgeInYears
		,PatientAgeInMonths
		,AdmissionWardCode
		,SessionStartTime
		,PatientEpisodeNumber
		,Operation
		,PreferredOrder
		,PatientBookingStatusCode
		,PatientStatusCode
		,PatientClassificationCode2
		,AnaestheticCode
		,AnaestheticTypeCode
		,SurgeonComment
		,SurgeonSpecialtyCode
		,BedNumber
		,WaitingListSourceUniqueID
		,SessionNo
		,OperationDuration
		,ChangeOverDuration
		,SexCode
		,SurgeonCode
		,BookingEventAdmittedTime
		,SurgeryRequestedTime
		,ReadyForSurgeryTime
		,VerifiedFlag
		,PlannedReturnFlag
		,EmergencyFlag
		,TheatreSessionSort
		,EstimatedStartTime
		,IntendedSurgeonCode
		,PatientClassificationCode
		,WardCode
		,Surgeon1SpecialtyCode
		,LogLastUpdated
		,RecordLogDetails
		,WaitingDetailsSourceUniqueID
		,AnaesthetistCode
		,Address1
		,Address3
		,Postcode
		,HomeTelephone
		,BusinessTelephone
		,AgeInDays
		,AdmissionTypeCode
		,ChangedSinceLastDataTransferFlag
		,ASAScoreCode
		,Unused4
		,Unused5
		,Unused6
		,Unused7
		,Unused8
		,PathologyRequiredFlag
		,XrayRequiredFlag
		,PathologyComment
		,XrayComment
		,BloodProductsRequiredComment
		,UnitsOfBloodProductRequired
		,WardAdvisedTime
		,WardAdvisedTime2
		,ModeOfTransportCode
		,NurseRequestingTransferCode
		,NurseReceivingNotificationCode
		,OxygenRequiredFlag
		,IVFlag
		,TransferredToWardCode
		,Surgeon2Code
		,Surgeon3Code
		,Anaesthetist2Code
		,Anaesthetist3Code
		,ScoutNurseCode
		,AnaesthetisNurseCode
		,InstrumentNurseCode
		,Technician1Code
		,Technician2Code
		,Unused1
		,InTheatreTime
		,CampusCode
		,ClinicalPriorityCode
		,StaffCode
		,AdditionalCaseFlag
		,UnplannedReturnCode
		,UnplannedReturnComment
		,AdmittingStaffCode
		,ReviewFlag
		,ExternalReferenceNo
		,UserFlag
		,AdmittedTime
		,ACCFlag
		,ACCNumber
		,ACCCode
		,MajorMinorCaseFlag
		,EstimatedStartTimeFixedFlag
		,PatientLanguageCode
		,LMOCode
		,ReferrerCode
		,MaritalStatusCode
		,PatientAddressStatus
		,LastUpdated
		,OldMRN
		,EstimatedArrivalTime
		,Address2
		,Address4
		,SentForTime
		,PorterLeftTime
		,PorterCode
		,CombinedCaseOrderNumberF
		,CombinedCaseConsultantCode
		,CombinedCaseTheatreCode
		,CombinedCaseSessionNo
		,CombinedCaseEstimateStartTime
		,CombinedCaseSpecialtyCode
		,CombinedCaseSurgeonCode
		,CombinedCaseAnaesthetistCode
		,CombinedCaseFlag
		,CombinedCaseFixedTimeFlag
		,PicklistPrintedFlag
		,PicklistMergedFlag
		,BookingInformationReceivedTime
		,InitiatedByStaffName
		,RequestingSurgerySpecialtyCode
		,HSCComment
		,SessionSourceUniqueID
		,CombinedCaseSessionSourceUniqueID
		,ReasonForOrderCode
		,PrintedFlag
		,PorterDelayCode
		,NHSNumber
		,CasenoteNumber
	from
		ETL.TLoadCenTheatreBasePatientBooking
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	and	target.ContextCode = 'CEN||ORMIS'

	when not matched by source
	and	target.ContextCode = 'CEN||ORMIS'
	then delete

	when not matched
	then
		insert
			(
			 SequenceNo
			,SourceUniqueID
			,DistrictNo
			,IntendedProcedureDate
			,IntendedTheatreCode
			,CustodianDetail
			,IntendedSessionTypeCode
			,Surname
			,Forename
			,DateOfBirth
			,PatientSourceUniqueID
			,TheatreCode
			,ConsultantCode
			,ConsultantCode2
			,BiohazardFlag
			,BloodProductsRequiredFlag
			,SpecialEquipmentFlag
			,PatientAgeInYears
			,PatientAgeInMonths
			,AdmissionWardCode
			,SessionStartTime
			,PatientEpisodeNumber
			,Operation
			,PreferredOrder
			,PatientBookingStatusCode
			,PatientStatusCode
			,PatientClassificationCode2
			,AnaestheticCode
			,AnaestheticTypeCode
			,SurgeonComment
			,SurgeonSpecialtyCode
			,BedNumber
			,WaitingListSourceUniqueID
			,SessionNo
			,OperationDuration
			,ChangeOverDuration
			,SexCode
			,SurgeonCode
			,BookingEventAdmittedTime
			,SurgeryRequestedTime
			,ReadyForSurgeryTime
			,VerifiedFlag
			,PlannedReturnFlag
			,EmergencyFlag
			,TheatreSessionSort
			,EstimatedStartTime
			,IntendedSurgeonCode
			,PatientClassificationCode
			,WardCode
			,Surgeon1SpecialtyCode
			,LogLastUpdated
			,RecordLogDetails
			,WaitingDetailsSourceUniqueID
			,AnaesthetistCode
			,Address1
			,Address3
			,Postcode
			,HomeTelephone
			,BusinessTelephone
			,AgeInDays
			,AdmissionTypeCode
			,ChangedSinceLastDataTransferFlag
			,ASAScoreCode
			,Unused4
			,Unused5
			,Unused6
			,Unused7
			,Unused8
			,PathologyRequiredFlag
			,XrayRequiredFlag
			,PathologyComment
			,XrayComment
			,BloodProductsRequiredComment
			,UnitsOfBloodProductRequired
			,WardAdvisedTime
			,WardAdvisedTime2
			,ModeOfTransportCode
			,NurseRequestingTransferCode
			,NurseReceivingNotificationCode
			,OxygenRequiredFlag
			,IVFlag
			,TransferredToWardCode
			,Surgeon2Code
			,Surgeon3Code
			,Anaesthetist2Code
			,Anaesthetist3Code
			,ScoutNurseCode
			,AnaesthetisNurseCode
			,InstrumentNurseCode
			,Technician1Code
			,Technician2Code
			,Unused1
			,InTheatreTime
			,CampusCode
			,ClinicalPriorityCode
			,StaffCode
			,AdditionalCaseFlag
			,UnplannedReturnCode
			,UnplannedReturnComment
			,AdmittingStaffCode
			,ReviewFlag
			,ExternalReferenceNo
			,UserFlag
			,AdmittedTime
			,ACCFlag
			,ACCNumber
			,ACCCode
			,MajorMinorCaseFlag
			,EstimatedStartTimeFixedFlag
			,PatientLanguageCode
			,LMOCode
			,ReferrerCode
			,MaritalStatusCode
			,PatientAddressStatus
			,LastUpdated
			,OldMRN
			,EstimatedArrivalTime
			,Address2
			,Address4
			,SentForTime
			,PorterLeftTime
			,PorterCode
			,CombinedCaseOrderNumberF
			,CombinedCaseConsultantCode
			,CombinedCaseTheatreCode
			,CombinedCaseSessionNo
			,CombinedCaseEstimateStartTime
			,CombinedCaseSpecialtyCode
			,CombinedCaseSurgeonCode
			,CombinedCaseAnaesthetistCode
			,CombinedCaseFlag
			,CombinedCaseFixedTimeFlag
			,PicklistPrintedFlag
			,PicklistMergedFlag
			,BookingInformationReceivedTime
			,InitiatedByStaffName
			,RequestingSurgerySpecialtyCode
			,HSCComment
			,SessionSourceUniqueID
			,CombinedCaseSessionSourceUniqueID
			,ReasonForOrderCode
			,PrintedFlag
			,PorterDelayCode
			,NHSNumber
			,CasenoteNumber
			,ContextCode
			)
		values
			(
			 source.SequenceNo
			,source.SourceUniqueID
			,source.DistrictNo
			,source.IntendedProcedureDate
			,source.IntendedTheatreCode
			,source.CustodianDetail
			,source.IntendedSessionTypeCode
			,source.Surname
			,source.Forename
			,source.DateOfBirth
			,source.PatientSourceUniqueID
			,source.TheatreCode
			,source.ConsultantCode
			,source.ConsultantCode2
			,source.BiohazardFlag
			,source.BloodProductsRequiredFlag
			,source.SpecialEquipmentFlag
			,source.PatientAgeInYears
			,source.PatientAgeInMonths
			,source.AdmissionWardCode
			,source.SessionStartTime
			,source.PatientEpisodeNumber
			,source.Operation
			,source.PreferredOrder
			,source.PatientBookingStatusCode
			,source.PatientStatusCode
			,source.PatientClassificationCode2
			,source.AnaestheticCode
			,source.AnaestheticTypeCode
			,source.SurgeonComment
			,source.SurgeonSpecialtyCode
			,source.BedNumber
			,source.WaitingListSourceUniqueID
			,source.SessionNo
			,source.OperationDuration
			,source.ChangeOverDuration
			,source.SexCode
			,source.SurgeonCode
			,source.BookingEventAdmittedTime
			,source.SurgeryRequestedTime
			,source.ReadyForSurgeryTime
			,source.VerifiedFlag
			,source.PlannedReturnFlag
			,source.EmergencyFlag
			,source.TheatreSessionSort
			,source.EstimatedStartTime
			,source.IntendedSurgeonCode
			,source.PatientClassificationCode
			,source.WardCode
			,source.Surgeon1SpecialtyCode
			,source.LogLastUpdated
			,source.RecordLogDetails
			,source.WaitingDetailsSourceUniqueID
			,source.AnaesthetistCode
			,source.Address1
			,source.Address3
			,source.Postcode
			,source.HomeTelephone
			,source.BusinessTelephone
			,source.AgeInDays
			,source.AdmissionTypeCode
			,source.ChangedSinceLastDataTransferFlag
			,source.ASAScoreCode
			,source.Unused4
			,source.Unused5
			,source.Unused6
			,source.Unused7
			,source.Unused8
			,source.PathologyRequiredFlag
			,source.XrayRequiredFlag
			,source.PathologyComment
			,source.XrayComment
			,source.BloodProductsRequiredComment
			,source.UnitsOfBloodProductRequired
			,source.WardAdvisedTime
			,source.WardAdvisedTime2
			,source.ModeOfTransportCode
			,source.NurseRequestingTransferCode
			,source.NurseReceivingNotificationCode
			,source.OxygenRequiredFlag
			,source.IVFlag
			,source.TransferredToWardCode
			,source.Surgeon2Code
			,source.Surgeon3Code
			,source.Anaesthetist2Code
			,source.Anaesthetist3Code
			,source.ScoutNurseCode
			,source.AnaesthetisNurseCode
			,source.InstrumentNurseCode
			,source.Technician1Code
			,source.Technician2Code
			,source.Unused1
			,source.InTheatreTime
			,source.CampusCode
			,source.ClinicalPriorityCode
			,source.StaffCode
			,source.AdditionalCaseFlag
			,source.UnplannedReturnCode
			,source.UnplannedReturnComment
			,source.AdmittingStaffCode
			,source.ReviewFlag
			,source.ExternalReferenceNo
			,source.UserFlag
			,source.AdmittedTime
			,source.ACCFlag
			,source.ACCNumber
			,source.ACCCode
			,source.MajorMinorCaseFlag
			,source.EstimatedStartTimeFixedFlag
			,source.PatientLanguageCode
			,source.LMOCode
			,source.ReferrerCode
			,source.MaritalStatusCode
			,source.PatientAddressStatus
			,source.LastUpdated
			,source.OldMRN
			,source.EstimatedArrivalTime
			,source.Address2
			,source.Address4
			,source.SentForTime
			,source.PorterLeftTime
			,source.PorterCode
			,source.CombinedCaseOrderNumberF
			,source.CombinedCaseConsultantCode
			,source.CombinedCaseTheatreCode
			,source.CombinedCaseSessionNo
			,source.CombinedCaseEstimateStartTime
			,source.CombinedCaseSpecialtyCode
			,source.CombinedCaseSurgeonCode
			,source.CombinedCaseAnaesthetistCode
			,source.CombinedCaseFlag
			,source.CombinedCaseFixedTimeFlag
			,source.PicklistPrintedFlag
			,source.PicklistMergedFlag
			,source.BookingInformationReceivedTime
			,source.InitiatedByStaffName
			,source.RequestingSurgerySpecialtyCode
			,source.HSCComment
			,source.SessionSourceUniqueID
			,source.CombinedCaseSessionSourceUniqueID
			,source.ReasonForOrderCode
			,source.PrintedFlag
			,source.PorterDelayCode
			,source.NHSNumber
			,source.CasenoteNumber
			,'CEN||ORMIS'
			)

	when matched
	and not
		(
			target.SequenceNo = source.SequenceNo
		and target.DistrictNo = source.DistrictNo
		and target.IntendedProcedureDate = source.IntendedProcedureDate
		and target.IntendedTheatreCode = source.IntendedTheatreCode
		and target.CustodianDetail = source.CustodianDetail
		and target.IntendedSessionTypeCode = source.IntendedSessionTypeCode
		and target.Surname = source.Surname
		and target.Forename = source.Forename
		and target.DateOfBirth = source.DateOfBirth
		and target.PatientSourceUniqueID = source.PatientSourceUniqueID
		and target.TheatreCode = source.TheatreCode
		and target.ConsultantCode = source.ConsultantCode
		and target.ConsultantCode2 = source.ConsultantCode2
		and target.BiohazardFlag = source.BiohazardFlag
		and target.BloodProductsRequiredFlag = source.BloodProductsRequiredFlag
		and target.SpecialEquipmentFlag = source.SpecialEquipmentFlag
		and target.PatientAgeInYears = source.PatientAgeInYears
		and target.PatientAgeInMonths = source.PatientAgeInMonths
		and target.AdmissionWardCode = source.AdmissionWardCode
		and target.SessionStartTime = source.SessionStartTime
		and target.PatientEpisodeNumber = source.PatientEpisodeNumber
		and target.Operation = source.Operation
		and target.PreferredOrder = source.PreferredOrder
		and target.PatientBookingStatusCode = source.PatientBookingStatusCode
		and target.PatientStatusCode = source.PatientStatusCode
		and target.PatientClassificationCode2 = source.PatientClassificationCode2
		and target.AnaestheticCode = source.AnaestheticCode
		and target.AnaestheticTypeCode = source.AnaestheticTypeCode
		and target.SurgeonComment = source.SurgeonComment
		and target.SurgeonSpecialtyCode = source.SurgeonSpecialtyCode
		and target.BedNumber = source.BedNumber
		and target.WaitingListSourceUniqueID = source.WaitingListSourceUniqueID
		and target.SessionNo = source.SessionNo
		and target.OperationDuration = source.OperationDuration
		and target.ChangeOverDuration = source.ChangeOverDuration
		and target.SexCode = source.SexCode
		and target.SurgeonCode = source.SurgeonCode
		and target.BookingEventAdmittedTime = source.BookingEventAdmittedTime
		and target.SurgeryRequestedTime = source.SurgeryRequestedTime
		and target.ReadyForSurgeryTime = source.ReadyForSurgeryTime
		and target.VerifiedFlag = source.VerifiedFlag
		and target.PlannedReturnFlag = source.PlannedReturnFlag
		and target.EmergencyFlag = source.EmergencyFlag
		and target.TheatreSessionSort = source.TheatreSessionSort
		and target.EstimatedStartTime = source.EstimatedStartTime
		and target.IntendedSurgeonCode = source.IntendedSurgeonCode
		and target.PatientClassificationCode = source.PatientClassificationCode
		and target.WardCode = source.WardCode
		and target.Surgeon1SpecialtyCode = source.Surgeon1SpecialtyCode
		and target.LogLastUpdated = source.LogLastUpdated
		and target.RecordLogDetails = source.RecordLogDetails
		and target.WaitingDetailsSourceUniqueID = source.WaitingDetailsSourceUniqueID
		and target.AnaesthetistCode = source.AnaesthetistCode
		and target.Address1 = source.Address1
		and target.Address3 = source.Address3
		and target.Postcode = source.Postcode
		and target.HomeTelephone = source.HomeTelephone
		and target.BusinessTelephone = source.BusinessTelephone
		and target.AgeInDays = source.AgeInDays
		and target.AdmissionTypeCode = source.AdmissionTypeCode
		and target.ChangedSinceLastDataTransferFlag = source.ChangedSinceLastDataTransferFlag
		and target.ASAScoreCode = source.ASAScoreCode
		and target.Unused4 = source.Unused4
		and target.Unused5 = source.Unused5
		and target.Unused6 = source.Unused6
		and target.Unused7 = source.Unused7
		and target.Unused8 = source.Unused8
		and target.PathologyRequiredFlag = source.PathologyRequiredFlag
		and target.XrayRequiredFlag = source.XrayRequiredFlag
		and target.PathologyComment = source.PathologyComment
		and target.XrayComment = source.XrayComment
		and target.BloodProductsRequiredComment = source.BloodProductsRequiredComment
		and target.UnitsOfBloodProductRequired = source.UnitsOfBloodProductRequired
		and target.WardAdvisedTime = source.WardAdvisedTime
		and target.WardAdvisedTime2 = source.WardAdvisedTime2
		and target.ModeOfTransportCode = source.ModeOfTransportCode
		and target.NurseRequestingTransferCode = source.NurseRequestingTransferCode
		and target.NurseReceivingNotificationCode = source.NurseReceivingNotificationCode
		and target.OxygenRequiredFlag = source.OxygenRequiredFlag
		and target.IVFlag = source.IVFlag
		and target.TransferredToWardCode = source.TransferredToWardCode
		and target.Surgeon2Code = source.Surgeon2Code
		and target.Surgeon3Code = source.Surgeon3Code
		and target.Anaesthetist2Code = source.Anaesthetist2Code
		and target.Anaesthetist3Code = source.Anaesthetist3Code
		and target.ScoutNurseCode = source.ScoutNurseCode
		and target.AnaesthetisNurseCode = source.AnaesthetisNurseCode
		and target.InstrumentNurseCode = source.InstrumentNurseCode
		and target.Technician1Code = source.Technician1Code
		and target.Technician2Code = source.Technician2Code
		and target.Unused1 = source.Unused1
		and target.InTheatreTime = source.InTheatreTime
		and target.CampusCode = source.CampusCode
		and target.ClinicalPriorityCode = source.ClinicalPriorityCode
		and target.StaffCode = source.StaffCode
		and target.AdditionalCaseFlag = source.AdditionalCaseFlag
		and target.UnplannedReturnCode = source.UnplannedReturnCode
		and target.UnplannedReturnComment = source.UnplannedReturnComment
		and target.AdmittingStaffCode = source.AdmittingStaffCode
		and target.ReviewFlag = source.ReviewFlag
		and target.ExternalReferenceNo = source.ExternalReferenceNo
		and target.UserFlag = source.UserFlag
		and target.AdmittedTime = source.AdmittedTime
		and target.ACCFlag = source.ACCFlag
		and target.ACCNumber = source.ACCNumber
		and target.ACCCode = source.ACCCode
		and target.MajorMinorCaseFlag = source.MajorMinorCaseFlag
		and target.EstimatedStartTimeFixedFlag = source.EstimatedStartTimeFixedFlag
		and target.PatientLanguageCode = source.PatientLanguageCode
		and target.LMOCode = source.LMOCode
		and target.ReferrerCode = source.ReferrerCode
		and target.MaritalStatusCode = source.MaritalStatusCode
		and target.PatientAddressStatus = source.PatientAddressStatus
		and target.LastUpdated = source.LastUpdated
		and target.OldMRN = source.OldMRN
		and target.EstimatedArrivalTime = source.EstimatedArrivalTime
		and target.Address2 = source.Address2
		and target.Address4 = source.Address4
		and target.SentForTime = source.SentForTime
		and target.PorterLeftTime = source.PorterLeftTime
		and target.PorterCode = source.PorterCode
		and target.CombinedCaseOrderNumberF = source.CombinedCaseOrderNumberF
		and target.CombinedCaseConsultantCode = source.CombinedCaseConsultantCode
		and target.CombinedCaseTheatreCode = source.CombinedCaseTheatreCode
		and target.CombinedCaseSessionNo = source.CombinedCaseSessionNo
		and target.CombinedCaseEstimateStartTime = source.CombinedCaseEstimateStartTime
		and target.CombinedCaseSpecialtyCode = source.CombinedCaseSpecialtyCode
		and target.CombinedCaseSurgeonCode = source.CombinedCaseSurgeonCode
		and target.CombinedCaseAnaesthetistCode = source.CombinedCaseAnaesthetistCode
		and target.CombinedCaseFlag = source.CombinedCaseFlag
		and target.CombinedCaseFixedTimeFlag = source.CombinedCaseFixedTimeFlag
		and target.PicklistPrintedFlag = source.PicklistPrintedFlag
		and target.PicklistMergedFlag = source.PicklistMergedFlag
		and target.BookingInformationReceivedTime = source.BookingInformationReceivedTime
		and target.InitiatedByStaffName = source.InitiatedByStaffName
		and target.RequestingSurgerySpecialtyCode = source.RequestingSurgerySpecialtyCode
		and target.HSCComment = source.HSCComment
		and target.SessionSourceUniqueID = source.SessionSourceUniqueID
		and target.CombinedCaseSessionSourceUniqueID = source.CombinedCaseSessionSourceUniqueID
		and target.ReasonForOrderCode = source.ReasonForOrderCode
		and target.PrintedFlag = source.PrintedFlag
		and target.PorterDelayCode = source.PorterDelayCode
		and target.NHSNumber = source.NHSNumber
		and target.CasenoteNumber = source.CasenoteNumber
		)
	then
		update
		set
			 target.SequenceNo = source.SequenceNo
			,target.DistrictNo = source.DistrictNo
			,target.IntendedProcedureDate = source.IntendedProcedureDate
			,target.IntendedTheatreCode = source.IntendedTheatreCode
			,target.CustodianDetail = source.CustodianDetail
			,target.IntendedSessionTypeCode = source.IntendedSessionTypeCode
			,target.Surname = source.Surname
			,target.Forename = source.Forename
			,target.DateOfBirth = source.DateOfBirth
			,target.PatientSourceUniqueID = source.PatientSourceUniqueID
			,target.TheatreCode = source.TheatreCode
			,target.ConsultantCode = source.ConsultantCode
			,target.ConsultantCode2 = source.ConsultantCode2
			,target.BiohazardFlag = source.BiohazardFlag
			,target.BloodProductsRequiredFlag = source.BloodProductsRequiredFlag
			,target.SpecialEquipmentFlag = source.SpecialEquipmentFlag
			,target.PatientAgeInYears = source.PatientAgeInYears
			,target.PatientAgeInMonths = source.PatientAgeInMonths
			,target.AdmissionWardCode = source.AdmissionWardCode
			,target.SessionStartTime = source.SessionStartTime
			,target.PatientEpisodeNumber = source.PatientEpisodeNumber
			,target.Operation = source.Operation
			,target.PreferredOrder = source.PreferredOrder
			,target.PatientBookingStatusCode = source.PatientBookingStatusCode
			,target.PatientStatusCode = source.PatientStatusCode
			,target.PatientClassificationCode2 = source.PatientClassificationCode2
			,target.AnaestheticCode = source.AnaestheticCode
			,target.AnaestheticTypeCode = source.AnaestheticTypeCode
			,target.SurgeonComment = source.SurgeonComment
			,target.SurgeonSpecialtyCode = source.SurgeonSpecialtyCode
			,target.BedNumber = source.BedNumber
			,target.WaitingListSourceUniqueID = source.WaitingListSourceUniqueID
			,target.SessionNo = source.SessionNo
			,target.OperationDuration = source.OperationDuration
			,target.ChangeOverDuration = source.ChangeOverDuration
			,target.SexCode = source.SexCode
			,target.SurgeonCode = source.SurgeonCode
			,target.BookingEventAdmittedTime = source.BookingEventAdmittedTime
			,target.SurgeryRequestedTime = source.SurgeryRequestedTime
			,target.ReadyForSurgeryTime = source.ReadyForSurgeryTime
			,target.VerifiedFlag = source.VerifiedFlag
			,target.PlannedReturnFlag = source.PlannedReturnFlag
			,target.EmergencyFlag = source.EmergencyFlag
			,target.TheatreSessionSort = source.TheatreSessionSort
			,target.EstimatedStartTime = source.EstimatedStartTime
			,target.IntendedSurgeonCode = source.IntendedSurgeonCode
			,target.PatientClassificationCode = source.PatientClassificationCode
			,target.WardCode = source.WardCode
			,target.Surgeon1SpecialtyCode = source.Surgeon1SpecialtyCode
			,target.LogLastUpdated = source.LogLastUpdated
			,target.RecordLogDetails = source.RecordLogDetails
			,target.WaitingDetailsSourceUniqueID = source.WaitingDetailsSourceUniqueID
			,target.AnaesthetistCode = source.AnaesthetistCode
			,target.Address1 = source.Address1
			,target.Address3 = source.Address3
			,target.Postcode = source.Postcode
			,target.HomeTelephone = source.HomeTelephone
			,target.BusinessTelephone = source.BusinessTelephone
			,target.AgeInDays = source.AgeInDays
			,target.AdmissionTypeCode = source.AdmissionTypeCode
			,target.ChangedSinceLastDataTransferFlag = source.ChangedSinceLastDataTransferFlag
			,target.ASAScoreCode = source.ASAScoreCode
			,target.Unused4 = source.Unused4
			,target.Unused5 = source.Unused5
			,target.Unused6 = source.Unused6
			,target.Unused7 = source.Unused7
			,target.Unused8 = source.Unused8
			,target.PathologyRequiredFlag = source.PathologyRequiredFlag
			,target.XrayRequiredFlag = source.XrayRequiredFlag
			,target.PathologyComment = source.PathologyComment
			,target.XrayComment = source.XrayComment
			,target.BloodProductsRequiredComment = source.BloodProductsRequiredComment
			,target.UnitsOfBloodProductRequired = source.UnitsOfBloodProductRequired
			,target.WardAdvisedTime = source.WardAdvisedTime
			,target.WardAdvisedTime2 = source.WardAdvisedTime2
			,target.ModeOfTransportCode = source.ModeOfTransportCode
			,target.NurseRequestingTransferCode = source.NurseRequestingTransferCode
			,target.NurseReceivingNotificationCode = source.NurseReceivingNotificationCode
			,target.OxygenRequiredFlag = source.OxygenRequiredFlag
			,target.IVFlag = source.IVFlag
			,target.TransferredToWardCode = source.TransferredToWardCode
			,target.Surgeon2Code = source.Surgeon2Code
			,target.Surgeon3Code = source.Surgeon3Code
			,target.Anaesthetist2Code = source.Anaesthetist2Code
			,target.Anaesthetist3Code = source.Anaesthetist3Code
			,target.ScoutNurseCode = source.ScoutNurseCode
			,target.AnaesthetisNurseCode = source.AnaesthetisNurseCode
			,target.InstrumentNurseCode = source.InstrumentNurseCode
			,target.Technician1Code = source.Technician1Code
			,target.Technician2Code = source.Technician2Code
			,target.Unused1 = source.Unused1
			,target.InTheatreTime = source.InTheatreTime
			,target.CampusCode = source.CampusCode
			,target.ClinicalPriorityCode = source.ClinicalPriorityCode
			,target.StaffCode = source.StaffCode
			,target.AdditionalCaseFlag = source.AdditionalCaseFlag
			,target.UnplannedReturnCode = source.UnplannedReturnCode
			,target.UnplannedReturnComment = source.UnplannedReturnComment
			,target.AdmittingStaffCode = source.AdmittingStaffCode
			,target.ReviewFlag = source.ReviewFlag
			,target.ExternalReferenceNo = source.ExternalReferenceNo
			,target.UserFlag = source.UserFlag
			,target.AdmittedTime = source.AdmittedTime
			,target.ACCFlag = source.ACCFlag
			,target.ACCNumber = source.ACCNumber
			,target.ACCCode = source.ACCCode
			,target.MajorMinorCaseFlag = source.MajorMinorCaseFlag
			,target.EstimatedStartTimeFixedFlag = source.EstimatedStartTimeFixedFlag
			,target.PatientLanguageCode = source.PatientLanguageCode
			,target.LMOCode = source.LMOCode
			,target.ReferrerCode = source.ReferrerCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.PatientAddressStatus = source.PatientAddressStatus
			,target.LastUpdated = source.LastUpdated
			,target.OldMRN = source.OldMRN
			,target.EstimatedArrivalTime = source.EstimatedArrivalTime
			,target.Address2 = source.Address2
			,target.Address4 = source.Address4
			,target.SentForTime = source.SentForTime
			,target.PorterLeftTime = source.PorterLeftTime
			,target.PorterCode = source.PorterCode
			,target.CombinedCaseOrderNumberF = source.CombinedCaseOrderNumberF
			,target.CombinedCaseConsultantCode = source.CombinedCaseConsultantCode
			,target.CombinedCaseTheatreCode = source.CombinedCaseTheatreCode
			,target.CombinedCaseSessionNo = source.CombinedCaseSessionNo
			,target.CombinedCaseEstimateStartTime = source.CombinedCaseEstimateStartTime
			,target.CombinedCaseSpecialtyCode = source.CombinedCaseSpecialtyCode
			,target.CombinedCaseSurgeonCode = source.CombinedCaseSurgeonCode
			,target.CombinedCaseAnaesthetistCode = source.CombinedCaseAnaesthetistCode
			,target.CombinedCaseFlag = source.CombinedCaseFlag
			,target.CombinedCaseFixedTimeFlag = source.CombinedCaseFixedTimeFlag
			,target.PicklistPrintedFlag = source.PicklistPrintedFlag
			,target.PicklistMergedFlag = source.PicklistMergedFlag
			,target.BookingInformationReceivedTime = source.BookingInformationReceivedTime
			,target.InitiatedByStaffName = source.InitiatedByStaffName
			,target.RequestingSurgerySpecialtyCode = source.RequestingSurgerySpecialtyCode
			,target.HSCComment = source.HSCComment
			,target.SessionSourceUniqueID = source.SessionSourceUniqueID
			,target.CombinedCaseSessionSourceUniqueID = source.CombinedCaseSessionSourceUniqueID
			,target.ReasonForOrderCode = source.ReasonForOrderCode
			,target.PrintedFlag = source.PrintedFlag
			,target.PorterDelayCode = source.PorterDelayCode
			,target.NHSNumber = source.NHSNumber
			,target.CasenoteNumber = source.CasenoteNumber

output
	$action into @MergeSummary
;


if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

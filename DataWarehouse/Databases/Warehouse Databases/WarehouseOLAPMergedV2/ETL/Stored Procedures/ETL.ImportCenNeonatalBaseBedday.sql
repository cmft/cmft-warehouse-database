﻿
CREATE procedure [ETL].[ImportCenNeonatalBaseBedday] as

--import the data
exec ETL.BuildCenNeonatalBaseBedday


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Neonatal].[BaseBedday]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildNeonatalBaseBeddayReference 'CEN||BDGR'


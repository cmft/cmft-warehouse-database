﻿CREATE procedure [ETL].[BuildBedOccupancyWardBedConfiguration]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	BedOccupancy.WardBedConfiguration target
using
	(
	select
		 WardCode = cast(WardCode as varchar)
		,ContextCode = cast(ContextCode as varchar)
		,StartDate = cast(StartDate as date)
		,EndDate = cast(case when EndDate = '' then null else EndDate end as date)
		,InpatientBeds = cast(InpatientBeds as int)
		,DaycaseBeds = cast(DaycaseBeds as int)
		,SingleRoomBeds = cast(SingleRoomBeds as int)
		,SpecialtySectorID = cast(SpecialtySectorID as int)
	from
		ETL.TImportWardBedConfiguration
	where
		WardCode is not null
	) source
	on	source.ContextCode = target.ContextCode
	and	source.WardCode = target.WardCode
	and	source.StartDate = target.StartDate

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 WardCode
			,ContextCode
			,StartDate
			,EndDate
			,InpatientBeds
			,DaycaseBeds
			,SingleRoomBeds
			,SpecialtySectorID
			,Created
			,ByWhom
			)
		values
			(
			 WardCode
			,ContextCode
			,StartDate
			,EndDate
			,InpatientBeds
			,DaycaseBeds
			,SingleRoomBeds
			,SpecialtySectorID
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.StartDate, getdate()) = isnull(source.StartDate, getdate())
		and isnull(target.EndDate, getdate()) = isnull(source.EndDate, getdate())
		and isnull(target.InpatientBeds, 0) = isnull(source.InpatientBeds, 0)
		and isnull(target.DaycaseBeds, 0) = isnull(source.DaycaseBeds, 0)
		and isnull(target.SingleRoomBeds, 0) = isnull(source.SingleRoomBeds, 0)
		and isnull(target.SpecialtySectorID, 0) = isnull(source.SpecialtySectorID, 0)
		)
	then
		update
		set
			 target.WardCode = source.WardCode
			,target.ContextCode = source.ContextCode
			,target.StartDate = source.StartDate
			,target.EndDate = source.EndDate
			,target.InpatientBeds = source.InpatientBeds
			,target.DaycaseBeds = source.DaycaseBeds
			,target.SingleRoomBeds = source.SingleRoomBeds
			,target.SpecialtySectorID = source.SpecialtySectorID
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

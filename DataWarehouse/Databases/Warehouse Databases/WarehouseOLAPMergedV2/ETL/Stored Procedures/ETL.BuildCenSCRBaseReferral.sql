﻿CREATE procedure [ETL].[BuildCenSCRBaseReferral]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)



declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	SCR.BaseReferral target
using
	(
	select
		 UniqueRecordId
		,DemographicUniqueRecordId = cast(DemographicUniqueRecordId as int)
		,SystemId = cast(SystemId as varchar(255))
		,CancerSite = cast(CancerSite as varchar(50))
		,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(2))
		,ReferringOrganisationCode = cast(ReferringOrganisationCode as varchar(6))
		,ReferrerCode = cast(ReferrerCode as varchar(10))
		,PriorityTypeCode = cast(PriorityTypeCode as varchar(2))
		,DecisionToReferDate = cast(DecisionToReferDate as datetime)
		,ReceiptOfReferralDate = cast(ReceiptOfReferralDate as datetime)
		,ConsultantCode = cast(ConsultantCode as varchar(8))
		,SpecialtyCode = cast(SpecialtyCode as int)
		,FirstAppointmentDate = cast(FirstAppointmentDate as datetime)
		,FirstAppointmentOrganisationCode = cast(FirstAppointmentOrganisationCode as varchar(5))
		,ReferralToFirstAppointmentDelayReasonCode = cast(ReferralToFirstAppointmentDelayReasonCode as varchar(2))
		,CancerTypeCode = cast(CancerTypeCode as varchar(2))
		,CancerStatusCode = cast(CancerStatusCode as varchar(2))
		,FirstAppointmentOfferedCode = cast(FirstAppointmentOfferedCode as int)
		,CancelledAppointmentDate = cast(CancelledAppointmentDate as datetime)
		,FirstAppointmentWaitingTimeAdjusted = cast(FirstAppointmentWaitingTimeAdjusted as int)
		,FirstAppointmentAdjustmentReasonCode = cast(FirstAppointmentAdjustmentReasonCode as int)
		,MethodOfReferralCode = cast(MethodOfReferralCode as int)
		,SourceForOutpatientCode = cast(SourceForOutpatientCode as varchar(2))
		,ReferredToSpecialistDate = cast(ReferredToSpecialistDate as datetime)
		,ReferredFromOrganisationCode = cast(ReferredFromOrganisationCode as varchar(5))
		,FirstAppointmentWithSpecialistDate = cast(FirstAppointmentWithSpecialistDate as datetime)
		,FirstAppointmentSpecialistOrganisationCode = cast(FirstAppointmentSpecialistOrganisationCode as varchar(5))
		,ConsultantUpgradeDate = cast(ConsultantUpgradeDate as datetime)
		,OrganisationUpgradeCode = cast(OrganisationUpgradeCode as varchar(5))
		,WhenPatientUpgradedCode = cast(WhenPatientUpgradedCode as int)
		,PatientUpgradedByCode = cast(PatientUpgradedByCode as int)
		,DiagnosisDate = cast(DiagnosisDate as datetime)
		,PrimaryDiagnosisCode = cast(PrimaryDiagnosisCode as varchar(5))
		,PrimaryDiagnosisCode1 = cast(PrimaryDiagnosisCode1 as varchar(5))
		,DiagnosisOrganisationCode = cast(DiagnosisOrganisationCode as varchar(5))
		,PatientInformedDate = cast(PatientInformedDate as datetime)
		,OtherDiagnosisDate = cast(OtherDiagnosisDate as datetime)
		,LateralityCode = cast(LateralityCode as varchar(1))
		,BasisOfDiagnosisCode = cast(BasisOfDiagnosisCode as int)
		,HistologyGroupCode = cast(HistologyGroupCode as int)
		,HistologyCode = cast(HistologyCode as varchar(10))
		,GradeOfDifferentiationCode = cast(GradeOfDifferentiationCode as varchar(2))
		,PreTreatmentTStageCode = cast(PreTreatmentTStageCode as varchar(5))
		,PreTreatmentTStageCertaintyCode = cast(PreTreatmentTStageCertaintyCode as varchar(2))
		,PreTreatmentNStageCode = cast(PreTreatmentNStageCode as varchar(5))
		,PreTreatmentNStageCertaintyCode = cast(PreTreatmentNStageCertaintyCode as varchar(2))
		,PreTreatmentMStageCode = cast(PreTreatmentMStageCode as varchar(5))
		,PreTreatmentMStageCertaintyCode = cast(PreTreatmentMStageCertaintyCode as varchar(2))
		,PreTreatmentOverallCertaintyCode = cast(PreTreatmentOverallCertaintyCode as varchar(2))
		,SiteSpecificClassificationCode = cast(SiteSpecificClassificationCode as varchar(2))
		,FinalOverallCertaintyCode = cast(FinalOverallCertaintyCode as varchar(2))
		,FinalTStageCertaintyCode = cast(FinalTStageCertaintyCode as varchar(2))
		,FinalTStageCode = cast(FinalTStageCode as varchar(5))
		,FinalNStageCertaintyCode = cast(FinalNStageCertaintyCode as varchar(2))
		,FinalNStageCode = cast(FinalNStageCode as varchar(5))
		,FinalMStageCertaintyCode = cast(FinalMStageCertaintyCode as varchar(2))
		,FinalMStageCode = cast(FinalMStageCode as varchar(5))
		,GpInformedOfDiagnosis = cast(GpInformedOfDiagnosis as int)
		,GpInformedOfDiagnosisDate = cast(GpInformedOfDiagnosisDate as datetime)
		,ReasonNotInformed = cast(ReasonNotInformed as varchar(255))
		,RelativeCarerInformedOfDiagnosis = cast(RelativeCarerInformedOfDiagnosis as int)
		,ReferredToSpecialistNurseDate = cast(ReferredToSpecialistNurseDate as datetime)
		,FirstSeenBySpecialistNurseDate = cast(FirstSeenBySpecialistNurseDate as datetime)
		,DecisionToReferAdjustment = cast(DecisionToReferAdjustment as int)
		,DecisionToTreatAdjustment = cast(DecisionToTreatAdjustment as int)
		,DecisionToReferAdjustmentReasonCode = cast(DecisionToReferAdjustmentReasonCode as int)
		,DecisionToTreatAdjustmentReasonCode = cast(DecisionToTreatAdjustmentReasonCode as int)
		,DecisionToReferAdjustmentDelayReasonCode = cast(DecisionToReferAdjustmentDelayReasonCode as int)
		,DecisionToTreatAdjustmentDelayReasonCode = cast(DecisionToTreatAdjustmentDelayReasonCode as int)
		,ReferringSymptoms1 = cast(ReferringSymptoms1 as int)
		,ReferringSymptoms2 = cast(ReferringSymptoms2 as int)
		,ReferringSymptoms3 = cast(ReferringSymptoms3 as int)
		,SpecialistNursePresent = cast(SpecialistNursePresent as varchar(3))
		,FinalTNMDate = cast(FinalTNMDate as datetime)
		,PreTreatmentTNMDate = cast(PreTreatmentTNMDate as datetime)
		,FirstAppointmentConsultantCode = cast(FirstAppointmentConsultantCode as varchar(8))
		,ReferralToAppropriateSpecialistCode = cast(ReferralToAppropriateSpecialistCode as int)
		,TertiaryCentreReferredToDate = cast(TertiaryCentreReferredToDate as datetime)
		,TertiaryCentreOrganisationCode = cast(TertiaryCentreOrganisationCode as varchar(5))
		,ReasonForReferralTertiaryCentreCode = cast(ReasonForReferralTertiaryCentreCode as int)
		,MakeNewReferralCode = cast(MakeNewReferralCode as int)
		,NewTumourSite = cast(NewTumourSite as varchar(20))
		,AutoReferralFlag = cast(AutoReferralFlag as int)
		,OtherTumourSiteDiagnosisCode = cast(OtherTumourSiteDiagnosisCode as varchar(5))
		,OtherTumourSiteDiagnosis = cast(OtherTumourSiteDiagnosis as varchar(5))
		,InappropriateReferralCode = cast(InappropriateReferralCode as int)
		,InappropriateReason = cast(InappropriateReason as varchar(255))
		,TumourStatusCode = cast(TumourStatusCode as int)
		,NonCancerDetails = cast(NonCancerDetails as varchar(255))
		,FirstAppointmentTypeCode = cast(FirstAppointmentTypeCode as int)
		,ReasonNoAppointmentCode = cast(ReasonNoAppointmentCode as int)
		,AssessedByCode = cast(AssessedByCode as int)
		,OtherSymptoms = cast(OtherSymptoms as varchar(max))
		,AdditionalCommentsReferral = cast(AdditionalCommentsReferral as varchar(max))
		,ReferralToFirstAppointmentDelayReasonComments = cast(ReferralToFirstAppointmentDelayReasonComments as varchar(max))
		,DecisionToReferDelayReasonComments = cast(DecisionToReferDelayReasonComments as varchar(max))
		,DecisionToTreatDelayReasonComments = cast(DecisionToTreatDelayReasonComments as varchar(max))
		,DiagnosisComments = cast(DiagnosisComments as varchar(max))
		,SiteCode
		,IntegratedTNM = cast(IntegratedTNM as varchar(4))
		,ContextCode
	from
		ETL.TLoadCenSCRBaseReferral
	) source
	on	source.ContextCode = target.ContextCode
	and	source.UniqueRecordId = target.UniqueRecordId

	when not matched by source
	and	target.ContextCode = 'CEN||SCR'
	then delete

	when not matched
	then
		insert
			(
			 UniqueRecordId
			,DemographicUniqueRecordId
			,SystemId
			,CancerSite
			,SourceOfReferralCode
			,ReferringOrganisationCode
			,ReferrerCode
			,PriorityTypeCode
			,DecisionToReferDate
			,ReceiptOfReferralDate
			,ConsultantCode
			,SpecialtyCode
			,FirstAppointmentDate
			,FirstAppointmentOrganisationCode
			,ReferralToFirstAppointmentDelayReasonCode
			,CancerTypeCode
			,CancerStatusCode
			,FirstAppointmentOfferedCode
			,CancelledAppointmentDate
			,FirstAppointmentWaitingTimeAdjusted
			,FirstAppointmentAdjustmentReasonCode
			,MethodOfReferralCode
			,SourceForOutpatientCode
			,ReferredToSpecialistDate
			,ReferredFromOrganisationCode
			,FirstAppointmentWithSpecialistDate
			,FirstAppointmentSpecialistOrganisationCode
			,ConsultantUpgradeDate
			,OrganisationUpgradeCode
			,WhenPatientUpgradedCode
			,PatientUpgradedByCode
			,DiagnosisDate
			,PrimaryDiagnosisCode
			,PrimaryDiagnosisCode1
			,DiagnosisOrganisationCode
			,PatientInformedDate
			,OtherDiagnosisDate
			,LateralityCode
			,BasisOfDiagnosisCode
			,HistologyGroupCode
			,HistologyCode
			,GradeOfDifferentiationCode
			,PreTreatmentTStageCode
			,PreTreatmentTStageCertaintyCode
			,PreTreatmentNStageCode
			,PreTreatmentNStageCertaintyCode
			,PreTreatmentMStageCode
			,PreTreatmentMStageCertaintyCode
			,PreTreatmentOverallCertaintyCode
			,SiteSpecificClassificationCode
			,FinalOverallCertaintyCode
			,FinalTStageCertaintyCode
			,FinalTStageCode
			,FinalNStageCertaintyCode
			,FinalNStageCode
			,FinalMStageCertaintyCode
			,FinalMStageCode
			,GpInformedOfDiagnosis
			,GpInformedOfDiagnosisDate
			,ReasonNotInformed
			,RelativeCarerInformedOfDiagnosis
			,ReferredToSpecialistNurseDate
			,FirstSeenBySpecialistNurseDate
			,DecisionToReferAdjustment
			,DecisionToTreatAdjustment
			,DecisionToReferAdjustmentReasonCode
			,DecisionToTreatAdjustmentReasonCode
			,DecisionToReferAdjustmentDelayReasonCode
			,DecisionToTreatAdjustmentDelayReasonCode
			,ReferringSymptoms1
			,ReferringSymptoms2
			,ReferringSymptoms3
			,SpecialistNursePresent
			,FinalTNMDate
			,PreTreatmentTNMDate
			,FirstAppointmentConsultantCode
			,ReferralToAppropriateSpecialistCode
			,TertiaryCentreReferredToDate
			,TertiaryCentreOrganisationCode
			,ReasonForReferralTertiaryCentreCode
			,MakeNewReferralCode
			,NewTumourSite
			,AutoReferralFlag
			,OtherTumourSiteDiagnosisCode
			,OtherTumourSiteDiagnosis
			,InappropriateReferralCode
			,InappropriateReason
			,TumourStatusCode
			,NonCancerDetails
			,FirstAppointmentTypeCode
			,ReasonNoAppointmentCode
			,AssessedByCode
			,OtherSymptoms
			,AdditionalCommentsReferral
			,ReferralToFirstAppointmentDelayReasonComments
			,DecisionToReferDelayReasonComments
			,DecisionToTreatDelayReasonComments
			,DiagnosisComments
			,IntegratedTNM
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.UniqueRecordId
			,source.DemographicUniqueRecordId
			,source.SystemId
			,source.CancerSite
			,source.SourceOfReferralCode
			,source.ReferringOrganisationCode
			,source.ReferrerCode
			,source.PriorityTypeCode
			,source.DecisionToReferDate
			,source.ReceiptOfReferralDate
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.FirstAppointmentDate
			,source.FirstAppointmentOrganisationCode
			,source.ReferralToFirstAppointmentDelayReasonCode
			,source.CancerTypeCode
			,source.CancerStatusCode
			,source.FirstAppointmentOfferedCode
			,source.CancelledAppointmentDate
			,source.FirstAppointmentWaitingTimeAdjusted
			,source.FirstAppointmentAdjustmentReasonCode
			,source.MethodOfReferralCode
			,source.SourceForOutpatientCode
			,source.ReferredToSpecialistDate
			,source.ReferredFromOrganisationCode
			,source.FirstAppointmentWithSpecialistDate
			,source.FirstAppointmentSpecialistOrganisationCode
			,source.ConsultantUpgradeDate
			,source.OrganisationUpgradeCode
			,source.WhenPatientUpgradedCode
			,source.PatientUpgradedByCode
			,source.DiagnosisDate
			,source.PrimaryDiagnosisCode
			,source.PrimaryDiagnosisCode1
			,source.DiagnosisOrganisationCode
			,source.PatientInformedDate
			,source.OtherDiagnosisDate
			,source.LateralityCode
			,source.BasisOfDiagnosisCode
			,source.HistologyGroupCode
			,source.HistologyCode
			,source.GradeOfDifferentiationCode
			,source.PreTreatmentTStageCode
			,source.PreTreatmentTStageCertaintyCode
			,source.PreTreatmentNStageCode
			,source.PreTreatmentNStageCertaintyCode
			,source.PreTreatmentMStageCode
			,source.PreTreatmentMStageCertaintyCode
			,source.PreTreatmentOverallCertaintyCode
			,source.SiteSpecificClassificationCode
			,source.FinalOverallCertaintyCode
			,source.FinalTStageCertaintyCode
			,source.FinalTStageCode
			,source.FinalNStageCertaintyCode
			,source.FinalNStageCode
			,source.FinalMStageCertaintyCode
			,source.FinalMStageCode
			,source.GpInformedOfDiagnosis
			,source.GpInformedOfDiagnosisDate
			,source.ReasonNotInformed
			,source.RelativeCarerInformedOfDiagnosis
			,source.ReferredToSpecialistNurseDate
			,source.FirstSeenBySpecialistNurseDate
			,source.DecisionToReferAdjustment
			,source.DecisionToTreatAdjustment
			,source.DecisionToReferAdjustmentReasonCode
			,source.DecisionToTreatAdjustmentReasonCode
			,source.DecisionToReferAdjustmentDelayReasonCode
			,source.DecisionToTreatAdjustmentDelayReasonCode
			,source.ReferringSymptoms1
			,source.ReferringSymptoms2
			,source.ReferringSymptoms3
			,source.SpecialistNursePresent
			,source.FinalTNMDate
			,source.PreTreatmentTNMDate
			,source.FirstAppointmentConsultantCode
			,source.ReferralToAppropriateSpecialistCode
			,source.TertiaryCentreReferredToDate
			,source.TertiaryCentreOrganisationCode
			,source.ReasonForReferralTertiaryCentreCode
			,source.MakeNewReferralCode
			,source.NewTumourSite
			,source.AutoReferralFlag
			,source.OtherTumourSiteDiagnosisCode
			,source.OtherTumourSiteDiagnosis
			,source.InappropriateReferralCode
			,source.InappropriateReason
			,source.TumourStatusCode
			,source.NonCancerDetails
			,source.FirstAppointmentTypeCode
			,source.ReasonNoAppointmentCode
			,source.AssessedByCode
			,source.OtherSymptoms
			,source.AdditionalCommentsReferral
			,source.ReferralToFirstAppointmentDelayReasonComments
			,source.DecisionToReferDelayReasonComments
			,source.DecisionToTreatDelayReasonComments
			,source.DiagnosisComments
			,source.IntegratedTNM
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.UniqueRecordId, 0) = isnull(source.UniqueRecordId, 0)
		and isnull(target.DemographicUniqueRecordId, 0) = isnull(source.DemographicUniqueRecordId, 0)
		and isnull(target.SystemId, '') = isnull(source.SystemId, '')
		and isnull(target.CancerSite, '') = isnull(source.CancerSite, '')
		and isnull(target.SourceOfReferralCode, '') = isnull(source.SourceOfReferralCode, '')
		and isnull(target.ReferringOrganisationCode, '') = isnull(source.ReferringOrganisationCode, '')
		and isnull(target.ReferrerCode, '') = isnull(source.ReferrerCode, '')
		and isnull(target.PriorityTypeCode, '') = isnull(source.PriorityTypeCode, '')
		and isnull(target.DecisionToReferDate, getdate()) = isnull(source.DecisionToReferDate, getdate())
		and isnull(target.ReceiptOfReferralDate, getdate()) = isnull(source.ReceiptOfReferralDate, getdate())
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.SpecialtyCode, 0) = isnull(source.SpecialtyCode, 0)
		and isnull(target.FirstAppointmentDate, getdate()) = isnull(source.FirstAppointmentDate, getdate())
		and isnull(target.FirstAppointmentOrganisationCode, '') = isnull(source.FirstAppointmentOrganisationCode, '')
		and isnull(target.ReferralToFirstAppointmentDelayReasonCode, '') = isnull(source.ReferralToFirstAppointmentDelayReasonCode, '')
		and isnull(target.CancerTypeCode, '') = isnull(source.CancerTypeCode, '')
		and isnull(target.CancerStatusCode, '') = isnull(source.CancerStatusCode, '')
		and isnull(target.FirstAppointmentOfferedCode, 0) = isnull(source.FirstAppointmentOfferedCode, 0)
		and isnull(target.CancelledAppointmentDate, getdate()) = isnull(source.CancelledAppointmentDate, getdate())
		and isnull(target.FirstAppointmentWaitingTimeAdjusted, 0) = isnull(source.FirstAppointmentWaitingTimeAdjusted, 0)
		and isnull(target.FirstAppointmentAdjustmentReasonCode, 0) = isnull(source.FirstAppointmentAdjustmentReasonCode, 0)
		and isnull(target.MethodOfReferralCode, 0) = isnull(source.MethodOfReferralCode, 0)
		and isnull(target.SourceForOutpatientCode, '') = isnull(source.SourceForOutpatientCode, '')
		and isnull(target.ReferredToSpecialistDate, getdate()) = isnull(source.ReferredToSpecialistDate, getdate())
		and isnull(target.ReferredFromOrganisationCode, '') = isnull(source.ReferredFromOrganisationCode, '')
		and isnull(target.FirstAppointmentWithSpecialistDate, getdate()) = isnull(source.FirstAppointmentWithSpecialistDate, getdate())
		and isnull(target.FirstAppointmentSpecialistOrganisationCode, '') = isnull(source.FirstAppointmentSpecialistOrganisationCode, '')
		and isnull(target.ConsultantUpgradeDate, getdate()) = isnull(source.ConsultantUpgradeDate, getdate())
		and isnull(target.OrganisationUpgradeCode, '') = isnull(source.OrganisationUpgradeCode, '')
		and isnull(target.WhenPatientUpgradedCode, 0) = isnull(source.WhenPatientUpgradedCode, 0)
		and isnull(target.PatientUpgradedByCode, 0) = isnull(source.PatientUpgradedByCode, 0)
		and isnull(target.DiagnosisDate, getdate()) = isnull(source.DiagnosisDate, getdate())
		and isnull(target.PrimaryDiagnosisCode, '') = isnull(source.PrimaryDiagnosisCode, '')
		and isnull(target.PrimaryDiagnosisCode1, '') = isnull(source.PrimaryDiagnosisCode1, '')
		and isnull(target.DiagnosisOrganisationCode, '') = isnull(source.DiagnosisOrganisationCode, '')
		and isnull(target.PatientInformedDate, getdate()) = isnull(source.PatientInformedDate, getdate())
		and isnull(target.OtherDiagnosisDate, getdate()) = isnull(source.OtherDiagnosisDate, getdate())
		and isnull(target.LateralityCode, '') = isnull(source.LateralityCode, '')
		and isnull(target.BasisOfDiagnosisCode, 0) = isnull(source.BasisOfDiagnosisCode, 0)
		and isnull(target.HistologyGroupCode, 0) = isnull(source.HistologyGroupCode, 0)
		and isnull(target.HistologyCode, '') = isnull(source.HistologyCode, '')
		and isnull(target.GradeOfDifferentiationCode, '') = isnull(source.GradeOfDifferentiationCode, '')
		and isnull(target.PreTreatmentTStageCode, '') = isnull(source.PreTreatmentTStageCode, '')
		and isnull(target.PreTreatmentTStageCertaintyCode, '') = isnull(source.PreTreatmentTStageCertaintyCode, '')
		and isnull(target.PreTreatmentNStageCode, '') = isnull(source.PreTreatmentNStageCode, '')
		and isnull(target.PreTreatmentNStageCertaintyCode, '') = isnull(source.PreTreatmentNStageCertaintyCode, '')
		and isnull(target.PreTreatmentMStageCode, '') = isnull(source.PreTreatmentMStageCode, '')
		and isnull(target.PreTreatmentMStageCertaintyCode, '') = isnull(source.PreTreatmentMStageCertaintyCode, '')
		and isnull(target.PreTreatmentOverallCertaintyCode, '') = isnull(source.PreTreatmentOverallCertaintyCode, '')
		and isnull(target.SiteSpecificClassificationCode, '') = isnull(source.SiteSpecificClassificationCode, '')
		and isnull(target.FinalOverallCertaintyCode, '') = isnull(source.FinalOverallCertaintyCode, '')
		and isnull(target.FinalTStageCertaintyCode, '') = isnull(source.FinalTStageCertaintyCode, '')
		and isnull(target.FinalTStageCode, '') = isnull(source.FinalTStageCode, '')
		and isnull(target.FinalNStageCertaintyCode, '') = isnull(source.FinalNStageCertaintyCode, '')
		and isnull(target.FinalNStageCode, '') = isnull(source.FinalNStageCode, '')
		and isnull(target.FinalMStageCertaintyCode, '') = isnull(source.FinalMStageCertaintyCode, '')
		and isnull(target.FinalMStageCode, '') = isnull(source.FinalMStageCode, '')
		and isnull(target.GpInformedOfDiagnosis, 0) = isnull(source.GpInformedOfDiagnosis, 0)
		and isnull(target.GpInformedOfDiagnosisDate, getdate()) = isnull(source.GpInformedOfDiagnosisDate, getdate())
		and isnull(target.ReasonNotInformed, '') = isnull(source.ReasonNotInformed, '')
		and isnull(target.RelativeCarerInformedOfDiagnosis, 0) = isnull(source.RelativeCarerInformedOfDiagnosis, 0)
		and isnull(target.ReferredToSpecialistNurseDate, getdate()) = isnull(source.ReferredToSpecialistNurseDate, getdate())
		and isnull(target.FirstSeenBySpecialistNurseDate, getdate()) = isnull(source.FirstSeenBySpecialistNurseDate, getdate())
		and isnull(target.DecisionToReferAdjustment, 0) = isnull(source.DecisionToReferAdjustment, 0)
		and isnull(target.DecisionToTreatAdjustment, 0) = isnull(source.DecisionToTreatAdjustment, 0)
		and isnull(target.DecisionToReferAdjustmentReasonCode, 0) = isnull(source.DecisionToReferAdjustmentReasonCode, 0)
		and isnull(target.DecisionToTreatAdjustmentReasonCode, 0) = isnull(source.DecisionToTreatAdjustmentReasonCode, 0)
		and isnull(target.DecisionToReferAdjustmentDelayReasonCode, 0) = isnull(source.DecisionToReferAdjustmentDelayReasonCode, 0)
		and isnull(target.DecisionToTreatAdjustmentDelayReasonCode, 0) = isnull(source.DecisionToTreatAdjustmentDelayReasonCode, 0)
		and isnull(target.ReferringSymptoms1, 0) = isnull(source.ReferringSymptoms1, 0)
		and isnull(target.ReferringSymptoms2, 0) = isnull(source.ReferringSymptoms2, 0)
		and isnull(target.ReferringSymptoms3, 0) = isnull(source.ReferringSymptoms3, 0)
		and isnull(target.SpecialistNursePresent, '') = isnull(source.SpecialistNursePresent, '')
		and isnull(target.FinalTNMDate, getdate()) = isnull(source.FinalTNMDate, getdate())
		and isnull(target.PreTreatmentTNMDate, getdate()) = isnull(source.PreTreatmentTNMDate, getdate())
		and isnull(target.FirstAppointmentConsultantCode, '') = isnull(source.FirstAppointmentConsultantCode, '')
		and isnull(target.ReferralToAppropriateSpecialistCode, 0) = isnull(source.ReferralToAppropriateSpecialistCode, 0)
		and isnull(target.TertiaryCentreReferredToDate, getdate()) = isnull(source.TertiaryCentreReferredToDate, getdate())
		and isnull(target.TertiaryCentreOrganisationCode, '') = isnull(source.TertiaryCentreOrganisationCode, '')
		and isnull(target.ReasonForReferralTertiaryCentreCode, 0) = isnull(source.ReasonForReferralTertiaryCentreCode, 0)
		and isnull(target.MakeNewReferralCode, 0) = isnull(source.MakeNewReferralCode, 0)
		and isnull(target.NewTumourSite, '') = isnull(source.NewTumourSite, '')
		and isnull(target.AutoReferralFlag, 0) = isnull(source.AutoReferralFlag, 0)
		and isnull(target.OtherTumourSiteDiagnosisCode, '') = isnull(source.OtherTumourSiteDiagnosisCode, '')
		and isnull(target.OtherTumourSiteDiagnosis, '') = isnull(source.OtherTumourSiteDiagnosis, '')
		and isnull(target.InappropriateReferralCode, 0) = isnull(source.InappropriateReferralCode, 0)
		and isnull(target.InappropriateReason, '') = isnull(source.InappropriateReason, '')
		and isnull(target.TumourStatusCode, 0) = isnull(source.TumourStatusCode, 0)
		and isnull(target.NonCancerDetails, '') = isnull(source.NonCancerDetails, '')
		and isnull(target.FirstAppointmentTypeCode, 0) = isnull(source.FirstAppointmentTypeCode, 0)
		and isnull(target.ReasonNoAppointmentCode, 0) = isnull(source.ReasonNoAppointmentCode, 0)
		and isnull(target.AssessedByCode, 0) = isnull(source.AssessedByCode, 0)
		and isnull(target.OtherSymptoms, '') = isnull(source.OtherSymptoms, '')
		and isnull(target.AdditionalCommentsReferral, '') = isnull(source.AdditionalCommentsReferral, '')
		and isnull(target.ReferralToFirstAppointmentDelayReasonComments, '') = isnull(source.ReferralToFirstAppointmentDelayReasonComments, '')
		and isnull(target.DecisionToReferDelayReasonComments, '') = isnull(source.DecisionToReferDelayReasonComments, '')
		and isnull(target.DecisionToTreatDelayReasonComments, '') = isnull(source.DecisionToTreatDelayReasonComments, '')
		and isnull(target.DiagnosisComments, '') = isnull(source.DiagnosisComments, '')
		and isnull(target.IntegratedTNM, '') = isnull(source.IntegratedTNM, '')

		)
	then
		update
		set
			 target.UniqueRecordId = source.UniqueRecordId
			,target.DemographicUniqueRecordId = source.DemographicUniqueRecordId
			,target.SystemId = source.SystemId
			,target.CancerSite = source.CancerSite
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ReferringOrganisationCode = source.ReferringOrganisationCode
			,target.ReferrerCode = source.ReferrerCode
			,target.PriorityTypeCode = source.PriorityTypeCode
			,target.DecisionToReferDate = source.DecisionToReferDate
			,target.ReceiptOfReferralDate = source.ReceiptOfReferralDate
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.FirstAppointmentDate = source.FirstAppointmentDate
			,target.FirstAppointmentOrganisationCode = source.FirstAppointmentOrganisationCode
			,target.ReferralToFirstAppointmentDelayReasonCode = source.ReferralToFirstAppointmentDelayReasonCode
			,target.CancerTypeCode = source.CancerTypeCode
			,target.CancerStatusCode = source.CancerStatusCode
			,target.FirstAppointmentOfferedCode = source.FirstAppointmentOfferedCode
			,target.CancelledAppointmentDate = source.CancelledAppointmentDate
			,target.FirstAppointmentWaitingTimeAdjusted = source.FirstAppointmentWaitingTimeAdjusted
			,target.FirstAppointmentAdjustmentReasonCode = source.FirstAppointmentAdjustmentReasonCode
			,target.MethodOfReferralCode = source.MethodOfReferralCode
			,target.SourceForOutpatientCode = source.SourceForOutpatientCode
			,target.ReferredToSpecialistDate = source.ReferredToSpecialistDate
			,target.ReferredFromOrganisationCode = source.ReferredFromOrganisationCode
			,target.FirstAppointmentWithSpecialistDate = source.FirstAppointmentWithSpecialistDate
			,target.FirstAppointmentSpecialistOrganisationCode = source.FirstAppointmentSpecialistOrganisationCode
			,target.ConsultantUpgradeDate = source.ConsultantUpgradeDate
			,target.OrganisationUpgradeCode = source.OrganisationUpgradeCode
			,target.WhenPatientUpgradedCode = source.WhenPatientUpgradedCode
			,target.PatientUpgradedByCode = source.PatientUpgradedByCode
			,target.DiagnosisDate = source.DiagnosisDate
			,target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
			,target.PrimaryDiagnosisCode1 = source.PrimaryDiagnosisCode1
			,target.DiagnosisOrganisationCode = source.DiagnosisOrganisationCode
			,target.PatientInformedDate = source.PatientInformedDate
			,target.OtherDiagnosisDate = source.OtherDiagnosisDate
			,target.LateralityCode = source.LateralityCode
			,target.BasisOfDiagnosisCode = source.BasisOfDiagnosisCode
			,target.HistologyGroupCode = source.HistologyGroupCode
			,target.HistologyCode = source.HistologyCode
			,target.GradeOfDifferentiationCode = source.GradeOfDifferentiationCode
			,target.PreTreatmentTStageCode = source.PreTreatmentTStageCode
			,target.PreTreatmentTStageCertaintyCode = source.PreTreatmentTStageCertaintyCode
			,target.PreTreatmentNStageCode = source.PreTreatmentNStageCode
			,target.PreTreatmentNStageCertaintyCode = source.PreTreatmentNStageCertaintyCode
			,target.PreTreatmentMStageCode = source.PreTreatmentMStageCode
			,target.PreTreatmentMStageCertaintyCode = source.PreTreatmentMStageCertaintyCode
			,target.PreTreatmentOverallCertaintyCode = source.PreTreatmentOverallCertaintyCode
			,target.SiteSpecificClassificationCode = source.SiteSpecificClassificationCode
			,target.FinalOverallCertaintyCode = source.FinalOverallCertaintyCode
			,target.FinalTStageCertaintyCode = source.FinalTStageCertaintyCode
			,target.FinalTStageCode = source.FinalTStageCode
			,target.FinalNStageCertaintyCode = source.FinalNStageCertaintyCode
			,target.FinalNStageCode = source.FinalNStageCode
			,target.FinalMStageCertaintyCode = source.FinalMStageCertaintyCode
			,target.FinalMStageCode = source.FinalMStageCode
			,target.GpInformedOfDiagnosis = source.GpInformedOfDiagnosis
			,target.GpInformedOfDiagnosisDate = source.GpInformedOfDiagnosisDate
			,target.ReasonNotInformed = source.ReasonNotInformed
			,target.RelativeCarerInformedOfDiagnosis = source.RelativeCarerInformedOfDiagnosis
			,target.ReferredToSpecialistNurseDate = source.ReferredToSpecialistNurseDate
			,target.FirstSeenBySpecialistNurseDate = source.FirstSeenBySpecialistNurseDate
			,target.DecisionToReferAdjustment = source.DecisionToReferAdjustment
			,target.DecisionToTreatAdjustment = source.DecisionToTreatAdjustment
			,target.DecisionToReferAdjustmentReasonCode = source.DecisionToReferAdjustmentReasonCode
			,target.DecisionToTreatAdjustmentReasonCode = source.DecisionToTreatAdjustmentReasonCode
			,target.DecisionToReferAdjustmentDelayReasonCode = source.DecisionToReferAdjustmentDelayReasonCode
			,target.DecisionToTreatAdjustmentDelayReasonCode = source.DecisionToTreatAdjustmentDelayReasonCode
			,target.ReferringSymptoms1 = source.ReferringSymptoms1
			,target.ReferringSymptoms2 = source.ReferringSymptoms2
			,target.ReferringSymptoms3 = source.ReferringSymptoms3
			,target.SpecialistNursePresent = source.SpecialistNursePresent
			,target.FinalTNMDate = source.FinalTNMDate
			,target.PreTreatmentTNMDate = source.PreTreatmentTNMDate
			,target.FirstAppointmentConsultantCode = source.FirstAppointmentConsultantCode
			,target.ReferralToAppropriateSpecialistCode = source.ReferralToAppropriateSpecialistCode
			,target.TertiaryCentreReferredToDate = source.TertiaryCentreReferredToDate
			,target.TertiaryCentreOrganisationCode = source.TertiaryCentreOrganisationCode
			,target.ReasonForReferralTertiaryCentreCode = source.ReasonForReferralTertiaryCentreCode
			,target.MakeNewReferralCode = source.MakeNewReferralCode
			,target.NewTumourSite = source.NewTumourSite
			,target.AutoReferralFlag = source.AutoReferralFlag
			,target.OtherTumourSiteDiagnosisCode = source.OtherTumourSiteDiagnosisCode
			,target.OtherTumourSiteDiagnosis = source.OtherTumourSiteDiagnosis
			,target.InappropriateReferralCode = source.InappropriateReferralCode
			,target.InappropriateReason = source.InappropriateReason
			,target.TumourStatusCode = source.TumourStatusCode
			,target.NonCancerDetails = source.NonCancerDetails
			,target.FirstAppointmentTypeCode = source.FirstAppointmentTypeCode
			,target.ReasonNoAppointmentCode = source.ReasonNoAppointmentCode
			,target.AssessedByCode = source.AssessedByCode
			,target.OtherSymptoms = source.OtherSymptoms
			,target.AdditionalCommentsReferral = source.AdditionalCommentsReferral
			,target.ReferralToFirstAppointmentDelayReasonComments = source.ReferralToFirstAppointmentDelayReasonComments
			,target.DecisionToReferDelayReasonComments = source.DecisionToReferDelayReasonComments
			,target.DecisionToTreatDelayReasonComments = source.DecisionToTreatDelayReasonComments
			,target.DiagnosisComments = source.DiagnosisComments
			,target.IntegratedTNM = source.IntegratedTNM
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



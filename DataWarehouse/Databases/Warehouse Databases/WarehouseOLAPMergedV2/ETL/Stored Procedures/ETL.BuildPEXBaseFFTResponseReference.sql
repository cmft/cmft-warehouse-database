﻿


CREATE procedure [ETL].[BuildPEXBaseFFTResponseReference] 
	@ContextCode varchar(20)

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)
		

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				PEX.BaseFFTResponseReference
			where
				PEX.BaseFFTResponseReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)

delete
from
	PEX.BaseFFTResponseReference
where
	not exists
	(
	select
		1
	from
		PEX.BaseFFTResponseReference
	where
		BaseFFTResponseReference.MergeResponseRecno = BaseFFTResponseReference.MergeResponseRecno
	)
and	BaseFFTResponseReference.ContextID = @ContextID


select
	@deleted = @@ROWCOUNT


merge
	PEX.BaseFFTResponseReference target
using
	(
	select  
		 Encounter.MergeResponseRecno
		,AnswerID = Answer.SourceAnswerID
		,IsDischargeID = IsDischarge.SourceAnswerID
		,SurveyID = Survey.SourceSurveyID
		,LocationID = Location.SourceLocationID	
		,SurveyDateID = coalesce(Calendar.DateID
							,case
								when Encounter.SurveyTime is null then NullDate.DateID
								when Encounter.SurveyTime < CalendarStartDate.DateValue then PreviousDate.DateID
								else FutureDate.DateID
							end
							)						
		,ResponseDateID = coalesce(Calendar.DateID
							,case
								when Encounter.ResponseTime is null then NullDate.DateID
								when Encounter.ResponseTime < CalendarStartDate.DateValue then PreviousDate.DateID
								else FutureDate.DateID
							end
							)	
		,ContextID = Context.ContextID							
		,Created = Encounter.Created
		,Updated = Encounter.Updated
		,ByWhom = Encounter.ByWhom
	from
		PEX.BaseFFTResponse Encounter

	inner join WH.Context 
	on	Context.ContextCode = Encounter.ContextCode
	
	inner join WH.Survey 
	on	Survey .SourceSurveyCode = coalesce(Encounter.SurveyID, '-1') 
	and	Survey .SourceContextCode = Encounter.ContextCode

	inner join WH.Location
	on	Location.SourceLocationCode = coalesce(Encounter.LocationID, '-1') 
	and	Location.SourceContextCode = Encounter.ContextCode

	inner join WH.Answer
	on	Answer.SourceAnswerCode = coalesce(Encounter.AnswerID, '-1') 
	and	Answer.SourceContextCode = Encounter.ContextCode

	left join WH.Answer IsDischarge
	on	IsDischarge.SourceAnswerCode = coalesce(Encounter.IsDischargeID, '-1') 
	and	IsDischarge.SourceContextCode = Encounter.ContextCode
	
	left join WH.Calendar 
	on	Calendar.TheDate = cast(Encounter.SurveyTime as date)

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'
	
	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'
	
	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'
	
	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'


	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode
	
	
	
	) source
on	source.MergeResponseRecno = target.MergeResponseRecno


when not matched
then
	insert
		(
		 MergeResponseRecno
		,AnswerID
		,IsDischargeID
		,SurveyID
		,LocationID
		,SurveyDateID
		,ResponseDateID		
		,ContextID
		,Created
		,Updated
		,ByWhom
		)
	values
		(
		 source.MergeResponseRecno
		,source.AnswerID
		,source.IsDischargeID
		,source.SurveyID
		,source.LocationID
		,source.SurveyDateID
		,source.ResponseDateID
		,source.ContextID
		,source.Created
		,source.Updated
		,source.ByWhom
		)

when matched and not 
	(
	target.Updated = source.Updated
	)
then
	update
	set
		target.MergeResponseRecno = source.MergeResponseRecno
		,target.AnswerID = source.AnswerID
		,target.IsDischargeID = source.IsDischargeID
		,target.SurveyID = source.SurveyID
		,target.LocationID = source.LocationID
		,target.SurveyDateID = source.SurveyDateID
		,target.ResponseDateID = source.ResponseDateID	
		,target.ContextID = source.ContextID
		,target.Created = source.Created
		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats








﻿


CREATE procedure [ETL].[BuildHRG4APC] as

/* Process APC Encounter */

-- Delete records with no associated APC Encounter record

delete from APC.HRG4Encounter
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
	and Encounter.Reportable = 1 
	)


-- Delete records that are about to be reinserted

delete from APC.HRG4Encounter
where
	exists
	(
	select
		1
	from
		ETL.HRG4APCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
	)


insert into APC.HRG4Encounter
	(
	 MergeEncounterRecno
	,HRGCode
	,GroupingMethodFlag
	,DominantOperationCode
	,PBCCode
	,LOE
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,case when Encounter.GroupingMethodFlag = '' then null else Encounter.GroupingMethodFlag end
	,case when Encounter.DominantOperationCode = '' then null else Encounter.DominantOperationCode end
	,case when Encounter.PBCCode = '' then null else Encounter.PBCCode end
	,Encounter.LOE
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4APCEncounter Encounter



/* Process APC Spell */

-- Delete records with no associated APC Encounter record

delete from APC.HRG4Spell
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Spell.MergeEncounterRecno
	and Encounter.Reportable = 1 
	)

-- Delete records that are about to be reinserted

--delete from APC.HRG4Spell
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.HRG4APCSpell Spell

--	inner join ETL.HRG4APCEncounter Encounter
--	on	Encounter.RowNo = Spell.RowNo
--	and	Encounter.FinancialYear = Spell.FinancialYear

--	where
--		Encounter.MergeEncounterRecno = HRG4Spell.MergeEncounterRecno
--	)


-- delete records that are about to be reinserted

-- this method will delete all MergeEncounterRecnos that are in the ETL.Encounter irrespective of whether in destination table or not.
-- ensures that only correct recnos are in table after proc completes

delete from APC.HRG4Spell
where
	exists
	(
	select
		1
	from
		ETL.HRG4APCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Spell.MergeEncounterRecno
	)



insert into APC.HRG4Spell
	(
	 MergeEncounterRecno
	,HRGCode
	,GroupingMethodFlag
	,DominantOperationCode
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode
	,EpisodeCount
	,LOS
	,PBCCode
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,case when Spell.HRGCode = '' then null else Spell.HRGCode end
	,case when Spell.GroupingMethodFlag = '' then null else Spell.GroupingMethodFlag end
	,case when Spell.DominantOperationCode = '' then null else Spell.DominantOperationCode end
	,case when Spell.PrimaryDiagnosisCode = '' then null else Spell.PrimaryDiagnosisCode end
	,case when Spell.SecondaryDiagnosisCode = '' then null else Spell.SecondaryDiagnosisCode end
	,Spell.EpisodeCount
	,Spell.LOS
	,case when Spell.[PBCCode] = '' then null else Spell.[PBCCode] end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4APCSpell Spell

	inner join ETL.HRG4APCEncounter Encounter
	on	Encounter.RowNo = Spell.RowNo
	and	Encounter.FinancialYear = Spell.FinancialYear


/* Process APC Unbundled */

-- Delete records with no associated APC Encounter record

delete from APC.HRG4Unbundled
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
	and Encounter.Reportable = 1 
	)

-- Delete records that are about to be reinserted

--delete from APC.HRG4Unbundled
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.HRG4APCUnbundled Unbundled

--	inner join ETL.HRG4APCEncounter Encounter
--	on	Encounter.RowNo = Unbundled.RowNo
--	and	Encounter.FinancialYear = Unbundled.FinancialYear

--	where
--		Encounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
--	)


-- delete records that are about to be reinserted

-- this method will delete all MergeEncounterRecnos that are in the ETL.Encounter irrespective of whether in destination table or not.
-- ensures that only correct recnos are in table after proc completes

delete from APC.HRG4Unbundled
where
	exists
	(
	select
		1
	from
		ETL.HRG4APCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
	)


insert into APC.HRG4Unbundled
	(
	 MergeEncounterRecno
	,SequenceNo
	,HRGCode
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,Unbundled.SequenceNo
	,case when Unbundled.HRGCode = '' then null else Unbundled.HRGCode end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4APCUnbundled Unbundled

	inner join ETL.HRG4APCEncounter Encounter
	on	Encounter.RowNo = Unbundled.RowNo
	and	Encounter.FinancialYear = Unbundled.FinancialYear



/* Process APC Flag */

-- Delete records with no associated APC Encounter record

delete from APC.HRG4Flag
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Flag.MergeEncounterRecno
	and Encounter.Reportable = 1 
	)

-- Delete records that are about to be reinserted

--delete from APC.HRG4Flag
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.HRG4APCFlag Flag

--	inner join ETL.HRG4APCEncounter Encounter
--	on	Encounter.RowNo = Flag.RowNo
--	and Encounter.FinancialYear = Flag.FinancialYear

--	where
--		Encounter.MergeEncounterRecno = HRG4Flag.MergeEncounterRecno
--	)


-- delete records that are about to be reinserted

-- this method will delete all MergeEncounterRecnos that are in the ETL.Encounter irrespective of whether in destination table or not.
-- ensures that only correct recnos are in table after proc completes

delete from APC.HRG4Flag
where
	exists
	(
	select
		1
	from
		ETL.HRG4APCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Flag.MergeEncounterRecno
	)


insert into APC.HRG4Flag
	(
	 MergeEncounterRecno
	,SequenceNo
	,FlagCode
	,Created
	,ByWhom
	)

select
	 Encounter.MergeEncounterRecno
	,Flag.SequenceNo
	,case when Flag.FlagCode = '' then null else Flag.FlagCode end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4APCFlag Flag

	inner join ETL.HRG4APCEncounter Encounter
	on	Encounter.RowNo = Flag.RowNo
	and Encounter.FinancialYear = Flag.FinancialYear

/* Process APC Quality */

-- Delete records with no associated APC Encounter record

delete from APC.HRG4Quality
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
	and Encounter.Reportable = 1 
	)

-- Delete records that might be reinserted (if we don't use this method then rows in quality table will remain even if sucessfully processed subsequently)

delete from APC.HRG4Quality
where
	exists
	(
	select
		1
	from
		ETL.HRG4APCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
	)


-- Delete records that are about to be reinserted

--delete from APC.HRG4Quality
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.HRG4APCQuality Quality

--	inner join ETL.HRG4APCEncounter Encounter
--	on	Encounter.RowNo = Quality.RowNo
--	and Encounter.FinancialYear = Quality.FinancialYear

--	where
--		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
--	)

insert into APC.HRG4Quality
	(
	 MergeEncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4APCQuality Quality

	inner join ETL.HRG4APCEncounter Encounter
	on	Encounter.RowNo = Quality.RowNo
	and Encounter.FinancialYear = Quality.FinancialYear



--assign HRG to BaseEncounter
update
	APC.BaseEncounter
set
	HRGCode = HRG4Encounter.HRGCode
from
	APC.BaseEncounter

inner join APC.HRG4Encounter
on	HRG4Encounter.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

--clear previously assigned spell values
update
	APC.BaseEncounter
set
	SpellHRGCode = null
from
	APC.BaseEncounter
where
	exists
	(
	select
		1
	from
		APC.HRG4Spell

	inner join APC.BaseEncounter Spell
	on	Spell.MergeEncounterRecno = HRG4Spell.MergeEncounterRecno

	where
		Spell.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
	)


--assign dominant episode with spell HRG
update
	APC.BaseEncounter
set
	SpellHRGCode = HRG4Spell.HRGCode
from
	APC.BaseEncounter

inner join APC.HRG4Spell
on	HRG4Spell.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno




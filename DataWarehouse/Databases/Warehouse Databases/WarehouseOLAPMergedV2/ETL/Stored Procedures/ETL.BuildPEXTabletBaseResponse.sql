﻿


CREATE procedure [ETL].[BuildPEXTabletBaseResponse]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	PEX.BaseResponse target
using
	(
	select
		[ResponseRecno]
      ,[SurveyTakenID]
      ,[SurveyDate]
      ,[SurveyTime]
      ,[SurveyID]
      ,[LocationID]
      ,[WardID]
      ,[QuestionID]
      ,[AnswerID]
      ,Answer
      ,[ChannelID]
      ,[QuestionTypeID]
      ,[LocationTypeID]
      ,[ContextCode]
      ,[Created]
      ,[Updated]
      ,[ByWhom]
	from
		ETL.TLoadPEXTabletBaseResponse
	) source
	on	source.ResponseRecno = target.ResponseRecno
	and	source.ContextCode = target.ContextCode
	and	source.ChannelID = target.ChannelID

	when not matched by source
	and	target.ContextCode = 'CMFT||CRT'
	and target.ChannelID = 6 --Tablet
	
	then delete

	when not matched
	then
		insert
			(
			[ResponseRecno]
			,[SurveyTakenID]
			,[SurveyDate]
			,[SurveyTime]
			,[SurveyID]
			,[LocationID]
			,[WardID]
			,[QuestionID]
			,[AnswerID]
			,Answer
			,[ChannelID]
			,[QuestionTypeID]
			,[LocationTypeID]
			,[ContextCode]
			,[Created]
			,[Updated]
			,[ByWhom]
			)
		values
			(
			 source.[ResponseRecno]
			,source.[SurveyTakenID]
			,source.[SurveyDate]
			,source.[SurveyTime]
			,source.[SurveyID]
			,source.[LocationID]
			,source.[WardID]
			,source.[QuestionID]
			,source.[AnswerID]
			,source.Answer
			,source.[ChannelID]
			,source.[QuestionTypeID]
			,source.[LocationTypeID]
			,source.[ContextCode]
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.ResponseRecno, 0) = isnull(source.ResponseRecno, 0)
		and	isnull(target.SurveyTakenID, 0) = isnull(source.SurveyTakenID, 0)
		and isnull(target.SurveyDate, getdate()) = isnull(source.SurveyDate, getdate())
		and isnull(target.SurveyTime, getdate()) = isnull(source.SurveyTime, getdate())		
		and	isnull(target.SurveyID, 0) = isnull(source.SurveyID, 0)
		and	isnull(target.LocationID, 0) = isnull(source.LocationID, 0)
		and	isnull(target.WardID, 0) = isnull(source.WardID, 0)		
		and	isnull(target.QuestionID, 0) = isnull(source.QuestionID, 0)
		and	isnull(target.AnswerID, 0) = isnull(source.AnswerID, 0)
		and	isnull(target.Answer, 0) = isnull(source.Answer, 0)		
		and	isnull(target.ChannelID, 0) = isnull(source.ChannelID, 0)		
		and	isnull(target.QuestionTypeID, 0) = isnull(source.QuestionTypeID, 0)
		and	isnull(target.LocationTypeID, 0) = isnull(source.LocationTypeID, 0)		
		and	isnull(target.ContextCode, 0) = isnull(source.ContextCode, 0)

		)
	then
		update
		set
			target.ResponseRecno = source.ResponseRecno
			,target.SurveyTakenID = source.SurveyTakenID
			,target.SurveyDate = source.SurveyDate
			,target.SurveyTime = source.SurveyTime			
			,target.SurveyID = source.SurveyID
			,target.LocationID = source.LocationID
			,target.WardID = source.WardID			
			,target.QuestionID = source.QuestionID
			,target.AnswerID = source.AnswerID
			,target.Answer = source.Answer			
			,target.ChannelID = source.ChannelID
			,target.QuestionTypeID = source.QuestionTypeID
			,target.LocationTypeID = source.LocationTypeID
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime



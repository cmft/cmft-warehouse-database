﻿

CREATE procedure [ETL].[BuildQCRBaseAuditAnswerReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				QCR.BaseAuditAnswerReference
			where
				BaseAuditAnswerReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete from QCR.BaseAuditAnswerReference
where not exists
(
	select 1
	from QCR.BaseAuditAnswer
	where BaseAuditAnswer.AuditAnswerRecno = BaseAuditAnswerReference.AuditAnswerRecno
)
and	BaseAuditAnswerReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge QCR.BaseAuditAnswerReference target
using
	(
	select
	
		 AuditAnswer.AuditAnswerRecno
		,ContextID = Context.ContextID
		,AuditDateID = Calendar.DateID
		,AuditTypeID = AuditType.SourceAuditTypeID
		,LocationID = Location.SourceLocationID
		,WardID = Ward.SourceWardID
		,QuestionID = Question.SourceQuestionID
		,Answer
		,AuditAnswer.Created
		,AuditAnswer.Updated
		,AuditAnswer.ByWhom

	from QCR.BaseAuditAnswer AuditAnswer

	inner join WH.Context on Context.ContextCode = AuditAnswer.ContextCode

	inner join WH.AuditType on	AuditType.SourceContextCode = AuditAnswer.ContextCode
	and AuditType.SourceAuditTypeCode = AuditAnswer.AuditTypeCode
	
	inner join WH.Location on Location.SourceContextCode = AuditAnswer.ContextCode
	and Location.SourceLocationCode = AuditAnswer.LocationCode
		
	inner join WH.Question on Question.SourceContextCode = AuditAnswer.ContextCode
	and Question.SourceQuestionCode = AuditAnswer.QuestionCode
	
	inner join WH.Calendar on AuditAnswer.AuditDate = Calendar.TheDate

	inner join WH.Ward 
	on	coalesce(AuditAnswer.WardCode, '-1') = Ward.SourceWardCode 
	and	AuditAnswer.ContextCode = Ward.SourceContextCode

	where
		AuditAnswer.Updated > @LastUpdated
	and	AuditAnswer.ContextCode = @ContextCode

	) source
	on	source.AuditAnswerRecno = target.AuditAnswerRecno

	when not matched then insert
	(
		 AuditAnswerRecno
		,ContextID
		,AuditDateID
		,AuditTypeID
		,LocationID
		,WardID
		,QuestionID
		,Answer			
		,Created
		,Updated
		,ByWhom
	)
	values
	(
		 source.AuditAnswerRecno
		,source.ContextID
		,source.AuditDateID
		,source.AuditTypeID
		,source.LocationID
		,source.WardID
		,source.QuestionID
		,source.Answer			
		,source.Created
		,source.Updated
		,source.ByWhom
	)	

	when matched and not
	(
		isnull(target.ContextID, 0) = isnull(source.ContextID, 0)
		and isnull(target.AuditDateID, 0) = isnull(source.AuditDateID, 0)
		and isnull(target.AuditTypeID, 0) = isnull(source.AuditTypeID, 0)
		and isnull(target.LocationID, 0) = isnull(source.LocationID, 0)
		and isnull(target.WardID, 0) = isnull(source.WardID, 0)
		and isnull(target.QuestionID, 0) = isnull(source.QuestionID, 0)
		and isnull(target.Answer, 0) = isnull(source.Answer, 0)
		and isnull(target.Created, getdate()) = isnull(source.Created, getdate())
		and isnull(target.Updated, getdate()) = isnull(source.Updated, getdate())
		and isnull(target.ByWhom, '') = isnull(source.ByWhom, '')		
	)
	then update set
		target.ContextID = source.ContextID
		,target.AuditDateID = source.AuditDateID
		,target.AuditTypeID = source.AuditTypeID
		,target.LocationID = source.LocationID
		,target.WardID = source.WardID
		,target.QuestionID = source.QuestionID
		,target.Answer = source.Answer
		,target.Created = source.Created
		,target.Updated = source.Updated
		,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime



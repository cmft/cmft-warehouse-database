﻿CREATE procedure [ETL].[BuildCenCasenoteBase]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


select
	 SourcePatientNo = cast(SourcePatientNo as varchar(20))
	,CasenoteNumber = cast(CasenoteNumber as varchar(20))
	,AllocatedDate = cast(AllocatedDate as date)
	,CasenoteLocationCode = cast(CasenoteLocationCode as varchar(4))
	,CasenoteLocationDate = cast(CasenoteLocationDate as date)
	,CasenoteStatus = cast(CasenoteStatus as varchar(10))
	,WithdrawnDate = cast(WithdrawnDate as date)
	,ContextCode = cast(ContextCode as varchar(8))
into
	#CasenoteBase
from
	ETL.TLoadCenCasenoteBase


CREATE UNIQUE CLUSTERED INDEX IX_TLoadCenCasenoteBase ON #CasenoteBase
	(
	 SourcePatientNo
	,CasenoteNumber
	,ContextCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


merge
	Casenote.Base target
using
	(
	select
		 SourcePatientNo
		,CasenoteNumber
		,AllocatedDate
		,CasenoteLocationCode
		,CasenoteLocationDate
		,CasenoteStatus
		,WithdrawnDate
		,ContextCode
	from
		#CasenoteBase
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourcePatientNo = target.SourcePatientNo
	and	source.CasenoteNumber = target.CasenoteNumber

	when not matched by source
	and	target.ContextCode = 'CEN||PAS'
	then delete

	when not matched
	then
		insert
			(
			 SourcePatientNo
			,CasenoteNumber
			,AllocatedDate
			,CasenoteLocationCode
			,CasenoteLocationDate
			,CasenoteStatus
			,WithdrawnDate
			,ContextCode
			)
		values
			(
			 source.SourcePatientNo
			,source.CasenoteNumber
			,source.AllocatedDate
			,source.CasenoteLocationCode
			,source.CasenoteLocationDate
			,source.CasenoteStatus
			,source.WithdrawnDate
			,source.ContextCode
			)

	when matched
	and not
		(
			isnull(target.AllocatedDate, getdate()) = isnull(source.AllocatedDate, getdate())
		and isnull(target.CasenoteLocationCode, '') = isnull(source.CasenoteLocationCode, '')
		and isnull(target.CasenoteLocationDate, getdate()) = isnull(source.CasenoteLocationDate, getdate())
		and isnull(target.CasenoteStatus, '') = isnull(source.CasenoteStatus, '')
		and isnull(target.WithdrawnDate, getdate()) = isnull(source.WithdrawnDate, getdate())
		)
	then
		update
		set
			 target.AllocatedDate = source.AllocatedDate
			,target.CasenoteLocationCode = source.CasenoteLocationCode
			,target.CasenoteLocationDate = source.CasenoteLocationDate
			,target.CasenoteStatus = source.CasenoteStatus
			,target.WithdrawnDate = source.WithdrawnDate


output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



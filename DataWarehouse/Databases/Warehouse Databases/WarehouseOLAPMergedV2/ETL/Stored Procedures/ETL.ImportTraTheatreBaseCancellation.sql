﻿CREATE procedure [ETL].[ImportTraTheatreBaseCancellation] as

--import the data
exec ETL.BuildTraTheatreBaseCancellation


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Theatre].[BaseCancellation]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildTheatreBaseCancellationReference 'TRA||ORMIS'

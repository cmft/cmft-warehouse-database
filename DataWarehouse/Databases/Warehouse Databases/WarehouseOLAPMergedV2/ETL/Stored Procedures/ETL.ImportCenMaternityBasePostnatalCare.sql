﻿
CREATE procedure [ETL].[ImportCenMaternityBasePostnatalCare] as

--import the data
exec ETL.BuildCenMaternityBasePostnatalCare


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Maternity].[BasePostnatalCare]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildMaternityBasePostnatalCareReference 'CEN||SMMIS'



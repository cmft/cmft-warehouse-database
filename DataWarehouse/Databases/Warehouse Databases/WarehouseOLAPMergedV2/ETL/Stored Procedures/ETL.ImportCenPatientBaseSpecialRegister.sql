﻿
CREATE Procedure [ETL].[ImportCenPatientBaseSpecialRegister]

as

--import the data
exec ETL.BuildCenPatientBaseSpecialRegister

declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Patient].[BaseSpecialRegister]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember
	
	
exec ETL.BuildPatientBaseSpecialRegisterReference 'CEN||PAS'



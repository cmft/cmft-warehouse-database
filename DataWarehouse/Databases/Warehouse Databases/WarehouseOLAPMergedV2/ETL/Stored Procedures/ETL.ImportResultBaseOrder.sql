﻿CREATE procedure [ETL].[ImportResultBaseOrder] as

-------------------------------------------------
--When		Who	What
--20150508	DG	Created procedure
-------------------------------------------------

--load CWS and ICEs into TLoad table
exec ETL.BuildTLoadResultBaseOrder

--load encounters and generate process list
exec ETL.LoadResultBaseOrder

--generate missing reference data
declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Result].[BaseOrder]'
	,@NewRows int

exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember

--load reference values
exec ETL.LoadResultBaseOrderReference 'CEN||ICE'
exec ETL.LoadResultBaseOrderReference 'CEN||GPICE'
exec ETL.LoadResultBaseOrderReference 'CEN||PAS'



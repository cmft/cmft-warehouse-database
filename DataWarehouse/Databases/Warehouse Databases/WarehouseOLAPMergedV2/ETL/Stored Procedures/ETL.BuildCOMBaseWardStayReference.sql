﻿
CREATE procedure [ETL].[BuildCOMBaseWardStayReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				COM.BaseWardStayReference
			)
			,'1 jan 1900'
		)

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	COM.BaseWardStayReference
where
	not exists
	(
	select
		1
	from
		COM.BaseWardStay
	where
		BaseWardStay.MergeEncounterRecno = BaseWardStayReference.MergeEncounterRecno
	)
and	ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	COM.BaseWardStayReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,SpecialtyID = Specialty.SourceValueID
		,ProfessionalCarerID = ProfessionalCarer.SourceValueID
		,CreatedByID = CreatedBy.SourceValueID
		,ModifiedByID = ModifiedBy.SourceValueID
		,SexID = Sex.SourceSexID
		,EthnicCategoryID = EthnicCategory.SourceEthnicCategoryID
		,AgeID = Age.AgeID
		,GPPracticeID = GPPractice.SourceValueID
		,LocationID = Location.SourceValueID
		,AdmissionMethodID = AdmissionMethod.SourceValueID
		,AdmissionSourceID = AdmissionSource.SourceValueID
		,PatientClassificationID = PatientClassification.SourcePatientClassificationID
		,AdminCategoryID = AdminCategory.SourceValueID
		,ManagementIntentionID = ManagementIntention.SourceValueID
		,AdmissionReasonID = AdmissionReason.SourceValueID
		,DischargeDestinationID = DischargeDestination.SourceValueID
		,DischargeMethodID = DischargeMethod.SourceValueID

		,AdmissionDateID =
			coalesce(
				 AdmissionDate.DateID

				,case
				when Encounter.AdmissionDate is null
				then NullDate.DateID

				when Encounter.AdmissionDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,DischargeDateID =
			coalesce(
				 DischargeDate.DateID

				,case
				when Encounter.DischargeDate is null
				then NullDate.DateID

				when Encounter.DischargeDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,EndDateID =
			coalesce(
				 EndDate.DateID

				,case
				when Encounter.EndDate is null
				then NullDate.DateID

				when Encounter.EndDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		COM.BaseWardStay Encounter

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	inner join COM.Specialty Specialty
	on	Specialty.SpecialtyID = coalesce(Encounter.SpellSpecialty, '-1')
	and	Specialty.ContextCode = Encounter.ContextCode

	inner join COM.ProfessionalCarer ProfessionalCarer
	on	ProfessionalCarer.ProfessionalCarerID = coalesce(Encounter.SpellProfessionalCarer, '-1')
	and	ProfessionalCarer.ContextCode = Encounter.ContextCode

	inner join COM.SystemUser CreatedBy
	on	CreatedBy.SystemUserID = Encounter.CreatedByID
	and	CreatedBy.ContextCode = Encounter.ContextCode

	inner join COM.SystemUser ModifiedBy
	on	ModifiedBy.SystemUserID = Encounter.ModifiedByID
	and	ModifiedBy.ContextCode = Encounter.ContextCode

	inner join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.PatientSexID, -1) as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	inner join WH.EthnicCategory EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryCode = cast(coalesce(Encounter.PatientEthnicGroupID, -1) as varchar)
	and	EthnicCategory.SourceContextCode = Encounter.ContextCode

	inner join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	inner join COM.GPPractice
	on	GPPractice.GPPracticeID = coalesce(Encounter.RegisteredPracticeID, '-1')
	and	GPPractice.ContextCode = Encounter.ContextCode

	inner join COM.Location
	on	Location.LocationID = cast(coalesce(Encounter.SourceServicePointID, -1) as varchar)
	and	Location.ContextCode = Encounter.ContextCode

	inner join WH.PatientClassification
	on	PatientClassification.SourcePatientClassificationCode = coalesce(Encounter.SourcePatientClassificationID, '-1')
	and	PatientClassification.SourceContextCode = Encounter.ContextCode

	inner join WH.AdminCategory AdminCategory
	on	AdminCategory.AdminCategoryID = cast(coalesce(Encounter.SourceAdminCategoryID, -1) as varchar)
	and	AdminCategory.ContextCode = Encounter.ContextCode

	inner join WH.ManagementIntention
	on	ManagementIntention.ManagementIntentionID = cast(coalesce(Encounter.SourceIntendedManagementID, -1) as varchar)
	and	ManagementIntention.ContextCode = Encounter.ContextCode

	inner join WH.AdmissionReason
	on	AdmissionReason.AdmissionReasonID = coalesce(Encounter.SourceReasonForAdmissionID, '-1')
	and	AdmissionReason.ContextCode = Encounter.ContextCode

	inner join WH.DischargeDestination
	on	DischargeDestination.DischargeDestinationID = cast(coalesce(Encounter.SourceDischargeDestinationID, -1) as varchar)
	and	DischargeDestination.ContextCode = Encounter.ContextCode

	inner join WH.DischargeMethod
	on	DischargeMethod.DischargeMethodID = cast(coalesce(Encounter.SourceDischargeMethodID, -1) as varchar)
	and	DischargeMethod.ContextCode = Encounter.ContextCode

	inner join WH.AdmissionMethod
	on	AdmissionMethod.AdmissionMethodID = cast(coalesce(Encounter.SourceAdmissionMethodID, -1) as varchar)
	and	AdmissionMethod.ContextCode = Encounter.ContextCode

	inner join WH.AdmissionSource
	on	AdmissionSource.AdmissionSourceID = cast(coalesce(Encounter.SourceAdmissionSourceID, -1) as varchar)
	and	AdmissionSource.ContextCode = Encounter.ContextCode

	left join WH.Calendar AdmissionDate
	on	AdmissionDate.TheDate = Encounter.AdmissionDate

	left join WH.Calendar DischargeDate
	on	DischargeDate.TheDate = Encounter.DischargeDate

	left join WH.Calendar EndDate
	on	EndDate.TheDate = Encounter.EndDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,AdmissionDateID
			,DischargeDateID
			,EndDateID
			,ProfessionalCarerID
			,SpecialtyID
			,GPPracticeID
			,SexID
			,EthnicCategoryID
			,AgeID
			,LocationID
			,AdmissionMethodID
			,AdmissionSourceID
			,PatientClassificationID
			,AdminCategoryID
			,ManagementIntentionID
			,AdmissionReasonID
			,DischargeDestinationID
			,DischargeMethodID
			,CreatedByID
			,ModifiedByID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.AdmissionDateID
			,source.DischargeDateID
			,source.EndDateID
			,source.ProfessionalCarerID
			,source.SpecialtyID
			,source.GPPracticeID
			,source.SexID
			,source.EthnicCategoryID
			,source.AgeID
			,source.LocationID
			,source.AdmissionMethodID
			,source.AdmissionSourceID
			,source.PatientClassificationID
			,source.AdminCategoryID
			,source.ManagementIntentionID
			,source.AdmissionReasonID
			,source.DischargeDestinationID
			,source.DischargeMethodID
			,source.CreatedByID
			,source.ModifiedByID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.AdmissionDateID = source.AdmissionDateID
			,target.DischargeDateID = source.DischargeDateID
			,target.EndDateID = source.EndDateID
			,target.ProfessionalCarerID = source.ProfessionalCarerID
			,target.SpecialtyID = source.SpecialtyID
			,target.GPPracticeID = source.GPPracticeID
			,target.SexID = source.SexID
			,target.EthnicCategoryID = source.EthnicCategoryID
			,target.AgeID = source.AgeID
			,target.LocationID = source.LocationID
			,target.AdmissionMethodID = source.AdmissionMethodID
			,target.AdmissionSourceID = source.AdmissionSourceID
			,target.PatientClassificationID = source.PatientClassificationID
			,target.AdminCategoryID = source.AdminCategoryID
			,target.ManagementIntentionID = source.ManagementIntentionID
			,target.AdmissionReasonID = source.AdmissionReasonID
			,target.DischargeDestinationID = source.DischargeDestinationID
			,target.DischargeMethodID = source.DischargeMethodID
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


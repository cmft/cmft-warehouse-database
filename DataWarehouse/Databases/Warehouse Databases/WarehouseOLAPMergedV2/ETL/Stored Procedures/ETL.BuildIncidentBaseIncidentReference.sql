﻿

CREATE procedure [ETL].[BuildIncidentBaseIncidentReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Incident.BaseIncidentReference
			where
				BaseIncidentReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Incident.BaseIncidentReference
where
	not exists
	(
	select
		1
	from
		Incident.BaseIncident
	where
		BaseIncident.MergeIncidentRecno = BaseIncidentReference.MergeIncidentRecno
	)
and	BaseIncidentReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Incident.BaseIncidentReference target
using
	(
	select
		Incident.MergeIncidentRecno
		,ContextID = Context.ContextID
		,IncidentDateID =
			coalesce(
				 IncidentDate.DateID

				,case
				when Incident.IncidentDate is null
				then NullDate.DateID

				when Incident.IncidentDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,Cause1ID = Cause1.SourceCauseID
		,Cause2ID = Cause2.SourceCauseID
		,IncidentTypeID = IncidentType.SourceIncidentTypeID
		,IncidentGradeID = Grade.SourceGradeID
		,SeverityID = Severity.SourceSeverityID
		,CauseGroupID = CauseGroup.SourceCauseGroupID
		--,SiteID
		,WardID = Ward.SourceWardID
		,Incident.Created
		,Incident.Updated
		,Incident.ByWhom
	from
		Incident.BaseIncident Incident

	inner join WH.Context
	on	Context.ContextCode = Incident.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(Incident.WardCode, '-1')
	and	Ward.SourceContextCode = Incident.ContextCode

	inner join Incident.Severity Severity
	on	Severity.SourceSeverityCode = coalesce(Incident.SeverityCode, '-1')
	and	Severity.SourceContextCode = Incident.ContextCode

	inner join Incident.Grade Grade
	on	Grade.SourceGradeCode = coalesce(Incident.IncidentGradeCode, '-1')
	and	Grade.SourceContextCode = Incident.ContextCode

	inner join Incident.IncidentType IncidentType
	on	IncidentType.SourceIncidentTypeCode = coalesce(Incident.IncidentTypeCode, '-1')
	and	IncidentType.SourceContextCode = Incident.ContextCode

	inner join Incident.CauseGroup CauseGroup
	on	CauseGroup.SourceCauseGroupCode = coalesce(Incident.CauseGroupCode, '-1')
	and	CauseGroup.SourceContextCode = Incident.ContextCode

	inner join Incident.Cause Cause1
	on	Cause1.SourceCauseCode = coalesce(Incident.CauseCode1, '-1')
	and	Cause1.SourceContextCode = Incident.ContextCode

	inner join Incident.Cause Cause2
	on	Cause2.SourceCauseCode = coalesce(Incident.CauseCode2, '-1')
	and	Cause2.SourceContextCode = Incident.ContextCode

	left join WH.Calendar IncidentDate
	on	IncidentDate.TheDate = Incident.IncidentDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		Incident.Updated > @LastUpdated
	and	Incident.ContextCode = @ContextCode

	) source
	on	source.MergeIncidentRecno = target.MergeIncidentRecno

	when not matched
	then
		insert
			(
			MergeIncidentRecno
			,ContextID
			,IncidentDateID
			,Cause1ID
			,Cause2ID
			,IncidentTypeID
			,IncidentGradeID
			,SeverityID
			,CauseGroupID
			--,SiteID
			,WardID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeIncidentRecno
			,source.ContextID
			,source.IncidentDateID
			,source.Cause1ID
			,source.Cause2ID
			,source.IncidentTypeID
			,source.IncidentGradeID
			,source.SeverityID
			,source.CauseGroupID
			--,source.SiteID
			,source.WardID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeIncidentRecno = source.MergeIncidentRecno
			,target.ContextID = source.ContextID
			,target.IncidentDateID = source.IncidentDateID
			,target.Cause2ID = source.Cause2ID
			,target.IncidentTypeID = source.IncidentTypeID
			,target.IncidentGradeID = source.IncidentGradeID
			,target.SeverityID = source.SeverityID
			,target.CauseGroupID = source.CauseGroupID
			--,target.SiteID = source.SiteID
			,target.WardID = source.WardID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






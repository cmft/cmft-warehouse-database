﻿


CREATE procedure [ETL].[BuildAPCBaseLearningDisabilityReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseLearningDisabilityReference
			where
				BaseLearningDisabilityReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseLearningDisabilityReference
where
	not exists
	(
	select
		1
	from
		APC.BaseLearningDisability
	where
		BaseLearningDisability.MergeLearningDisabilityRecno = BaseLearningDisabilityReference.MergeLearningDisabilityRecno
	)
and	BaseLearningDisabilityReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseLearningDisabilityReference target
using
	(
	select
		BaseLD.MergeLearningDisabilityRecno
		,ContextID = Context.ContextID
		,AssessmentDateID =
			coalesce(
				 AssessmentDate.DateID

				,case
				when BaseLD.AssessmentDate is null
				then NullDate.DateID

				when BaseLD.AssessmentDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		
		,WardID = Ward.SourceWardID	
		,BaseLD.Created
		,BaseLD.Updated
		,BaseLD.ByWhom
	from
		APC.BaseLearningDisability BaseLD

	inner join WH.Context
	on	Context.ContextCode = BaseLD.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(BaseLD.WardCode, '-1')
	and	Ward.SourceContextCode = BaseLD.ContextCode

	left join WH.Calendar AssessmentDate
	on	AssessmentDate.TheDate = BaseLD.AssessmentDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseLD.Updated > @LastUpdated
	and	BaseLD.ContextCode = @ContextCode

	) source
	on	source.MergeLearningDisabilityRecno = target.MergeLearningDisabilityRecno

	when not matched
	then
		insert
			(
			MergeLearningDisabilityRecno
			,ContextID
			,AssessmentDateID
			,WardID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeLearningDisabilityRecno
			,source.ContextID
			,source.AssessmentDateID
			,source.WardID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeLearningDisabilityRecno = source.MergeLearningDisabilityRecno
			,target.ContextID = source.ContextID
			,target.AssessmentDateID = source.AssessmentDateID
			,target.WardID = source.WardID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats







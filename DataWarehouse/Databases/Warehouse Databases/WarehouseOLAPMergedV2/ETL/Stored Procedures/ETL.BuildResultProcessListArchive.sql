﻿create procedure [ETL].[BuildResultProcessListArchive] as

insert
into
	Result.ProcessListArchive
(
	 MergeRecno
	,Action
	,ArchiveTime
)
select
	 MergeRecno
	,Action
	,ArchiveTime = getdate()
from
	Result.ProcessList;

truncate table Result.ProcessList;

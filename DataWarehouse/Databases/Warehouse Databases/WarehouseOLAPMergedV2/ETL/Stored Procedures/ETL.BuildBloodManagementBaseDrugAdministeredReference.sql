﻿







CREATE procedure [ETL].[BuildBloodManagementBaseDrugAdministeredReference] --'CMFT||RECALL'
	@ContextCode varchar(15)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				BloodManagement.BaseDrugAdministeredReference
			where
				BaseDrugAdministeredReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	BloodManagement.BaseDrugAdministeredReference
where
	not exists
	(
	select
		1
	from
		BloodManagement.BaseDrugAdministered
	where
		BaseDrugAdministered.MergeRecno = BaseDrugAdministeredReference.MergeRecno
	)
and	BaseDrugAdministeredReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


--select top 100 * from BloodManagement.BaseTranexamicAcid

merge
	BloodManagement.BaseDrugAdministeredReference target
using
	(
	select
		BaseDrugAdministered.MergeRecno
		,ContextID = Context.ContextID
		,LocationID = SourceLocationID 
		,SpecialtyID = SourceSpecialtyID 
		,ConsultantID = SourceConsultantID 
		,DrugID = SourceDrugID
		,UnitID = SourceUnitID
		
		,BaseDrugAdministered.Created
		,BaseDrugAdministered.Updated
		,BaseDrugAdministered.ByWhom
	from
		BloodManagement.BaseDrugAdministered

	inner join WH.Context
	on	Context.ContextCode = BaseDrugAdministered.ContextCode

	left join WH.Location
	on	Location.SourceLocationCode = coalesce(BaseDrugAdministered.SessionLocationCode, '-1')
	and	Location.SourceContextCode = BaseDrugAdministered.ContextCode
	
	left join WH.Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(BaseDrugAdministered.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = BaseDrugAdministered.ContextCode

	left join WH.Consultant
	on	Consultant.SourceConsultantCode = coalesce(BaseDrugAdministered.ConsultantCode, '-1')
	and	Consultant.SourceContextCode = BaseDrugAdministered.ContextCode

	left join BloodManagement.Drug
	on	Drug.SourceDrugCode = coalesce(BaseDrugAdministered.DrugCode, '-1')
	and	Drug.SourceContextCode = BaseDrugAdministered.ContextCode
	
	left join BloodManagement.Unit
	on	Unit.SourceUnitCode = coalesce(BaseDrugAdministered.UnitsCode, '-1')
	and	Unit.SourceContextCode = BaseDrugAdministered.ContextCode
	
	where
		BaseDrugAdministered.Updated > @LastUpdated
	and	BaseDrugAdministered.ContextCode = @ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			MergeRecno
			,ContextID 
			,LocationID 
			,SpecialtyID
			,ConsultantID
			,DrugID
			,UnitID
	
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeRecno
			,source.ContextID 
			,source.LocationID 
			,source.SpecialtyID
			,source.ConsultantID
			,source.DrugID
			,source.UnitID
				
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeRecno = source.MergeRecno
			,target.ContextID = source.ContextID
			,target.LocationID = source.LocationID
			,target.SpecialtyID = source.SpecialtyID
			,target.ConsultantID = source.ConsultantID
			,target.DrugID = source.DrugID
			,target.UnitID = source.UnitID
			
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
			
		
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats











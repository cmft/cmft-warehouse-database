﻿CREATE procedure [ETL].[BuildRFBaseEncounterReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				RF.BaseEncounterReference
			where
				BaseEncounterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	RF.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		RF.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
	)
and	BaseEncounterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	RF.BaseEncounterReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = @ContextID

		,AgeID = Age.AgeID
		,SexID = Sex.SourceSexID
		,EthnicOriginID = EthnicOrigin.SourceEthnicCategoryID
		,MaritalStatusID = MaritalStatus.SourceMaritalStatusID
		,SiteID = Site.SourceSiteID

		,ReferralDateID =
			coalesce(
				 ReferralDate.DateID

				,case
				when Encounter.ReferralDate is null
				then NullDate.DateID

				when Encounter.ReferralDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,AdminCategoryID = AdminCategory.SourceValueID
		,SourceOfReferralID = SourceOfReferral.SourceSourceOfReferralID
		,ReasonForReferralID = ReasonForReferral.SourceReasonForReferralID
		,ConsultantID = Consultant.SourceConsultantID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,RTTCurrentStatusID = RTTCurrentStatus.SourceRTTStatusID
		,ResidenceCCGID = ResidenceCCG.CCGID
		,CCGID = CCG.CCGID
		,ReligionID = Religion.SourceReligionID

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		RF.BaseEncounter Encounter

	left join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	left join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.SexCode, '-1') as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	left join WH.EthnicCategory EthnicOrigin
	on	EthnicOrigin.SourceEthnicCategoryCode = cast(coalesce(Encounter.EthnicOriginCode, '-1') as varchar)
	and	EthnicOrigin.SourceContextCode = Encounter.ContextCode

	left join WH.MaritalStatus MaritalStatus
	on	MaritalStatus.SourceMaritalStatusCode = cast(coalesce(Encounter.MaritalStatusCode, '-1') as varchar)
	and	MaritalStatus.SourceContextCode = Encounter.ContextCode

	left join WH.Site Site
	on	Site.SourceSiteCode = cast(coalesce(Encounter.SiteCode, '-1') as varchar)
	and	Site.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar ReferralDate
	on	ReferralDate.TheDate = Encounter.ReferralDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join WH.AdminCategory AdminCategory
	on	AdminCategory.AdminCategoryID = coalesce(Encounter.AdminCategoryCode, '-1')
	and	AdminCategory.ContextCode = Encounter.ContextCode

	left join OP.SourceOfReferral SourceOfReferral
	on	SourceOfReferral.SourceSourceOfReferralCode = coalesce(Encounter.SourceOfReferralCode, '-1')
	and	SourceOfReferral.SourceContextCode = Encounter.ContextCode

	left join OP.ReasonForReferral ReasonForReferral
	on	ReasonForReferral.SourceReasonForReferralCode = coalesce(Encounter.ReasonForReferralCode, '-1')
	and	ReasonForReferral.SourceContextCode = Encounter.ContextCode

	left join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = coalesce(Encounter.ConsultantCode, '-1')
	and	Consultant.SourceContextCode = Encounter.ContextCode

	left join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(Encounter.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = Encounter.ContextCode

	left join WH.RTTStatus RTTCurrentStatus
	on	RTTCurrentStatus.SourceRTTStatusCode = coalesce(Encounter.RTTCurrentStatusCode, '-1')
	and	RTTCurrentStatus.SourceContextCode = Encounter.ContextCode

	left join WH.CCG ResidenceCCG
	on	ResidenceCCG.CCGCode = coalesce(Encounter.ResidenceCCGCode , 'N/A')

	left join WH.CCG CCG
	on	CCG.CCGCode = coalesce(Encounter.CCGCode , 'N/A')

	left join WH.Religion Religion
	on	Religion.SourceReligionCode = coalesce(Encounter.ReligionCode, '-1')
	and	Religion.SourceContextCode = Encounter.ContextCode

	where
		coalesce(Encounter.Updated, getdate()) > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,AgeID
			,SexID
			,EthnicOriginID
			,MaritalStatusID
			,SiteID
			,ReferralDateID
			,AdminCategoryID
			,SourceOfReferralID
			,ReasonForReferralID
			,ConsultantID
			,SpecialtyID
			,RTTCurrentStatusID
			,ResidenceCCGID
			,CCGID
			,ReligionID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.AgeID
			,source.SexID
			,source.EthnicOriginID
			,source.MaritalStatusID
			,source.SiteID
			,source.ReferralDateID
			,source.AdminCategoryID
			,source.SourceOfReferralID
			,source.ReasonForReferralID
			,source.ConsultantID
			,source.SpecialtyID
			,source.RTTCurrentStatusID
			,source.ResidenceCCGID
			,source.CCGID
			,source.ReligionID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			isnull(target.Updated, getdate()) = isnull(source.Updated, getdate())
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.AgeID = source.AgeID
			,target.SexID = source.SexID
			,target.EthnicOriginID = source.EthnicOriginID
			,target.MaritalStatusID = source.MaritalStatusID
			,target.SiteID = source.SiteID
			,target.ReferralDateID = source.ReferralDateID
			,target.AdminCategoryID = source.AdminCategoryID
			,target.SourceOfReferralID = source.SourceOfReferralID
			,target.ReasonForReferralID = source.ReasonForReferralID
			,target.ConsultantID = source.ConsultantID
			,target.SpecialtyID = source.SpecialtyID
			,target.RTTCurrentStatusID = source.RTTCurrentStatusID
			,target.ResidenceCCGID = source.ResidenceCCGID
			,target.CCGID = source.CCGID
			,target.ReligionID = source.ReligionID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

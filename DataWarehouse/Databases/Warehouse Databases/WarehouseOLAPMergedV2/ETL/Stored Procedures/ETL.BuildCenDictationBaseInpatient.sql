﻿

CREATE procedure [ETL].[BuildCenDictationBaseInpatient]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Dictation.BaseInpatient target
using
	(
	select
		InpatientRecno
		,SourcePatientNo
		,AdmissionTime
		,SpellSequenceNo
		,RequestTypeCode 
		,InterfaceCode 
		,ContextCode
	from
		ETL.TLoadCenDictationInpatient Inpatient
	) source
	on	source.InpatientRecno = target.InpatientRecno
	and	source.ContextCode = target.ContextCode
	

	when not matched by source
	and	target.ContextCode = 'CEN||MEDI'

	then delete

	when not matched
	then
		insert
			(
			InpatientRecno
			,SourcePatientNo
			,AdmissionTime
			,SpellSequenceNo
			,RequestTypeCode
			,InterfaceCode 
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.InpatientRecno
			,source.SourcePatientNo
			,source.AdmissionTime
			,source.SpellSequenceNo
			,source.RequestTypeCode
			,source.InterfaceCode 
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	
			isnull(target.InpatientRecno, 0) = isnull(source.InpatientRecno, 0)
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.AdmissionTime, '') = isnull(source.AdmissionTime, '')
		and isnull(target.SpellSequenceNo, 0) = isnull(source.SpellSequenceNo, 0)
		and isnull(target.RequestTypeCode, '') = isnull(source.RequestTypeCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			target.InpatientRecno = source.InpatientRecno
			,target.SourcePatientNo = source.SourcePatientNo
			,target.AdmissionTime = source.AdmissionTime
			,target.SpellSequenceNo = source.SpellSequenceNo
			,target.RequestTypeCode = source.RequestTypeCode
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime




﻿









CREATE procedure [ETL].[BuildHRG4AE] as

/* Process AE Encounter */

-- Delete records with no associated AE Encounter record

delete from AE.HRG4Encounter
where
	not exists
	(
	select
		1
	from
		AE.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
	)

-- Delete records that are about to be reinserted

delete from AE.HRG4Encounter
where
	exists
	(
	select
		1
	from
		ETL.HRG4AEEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
	)


insert into AE.HRG4Encounter
	(
	 MergeEncounterRecno
	,HRGCode
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4AEEncounter Encounter


/* Process AE Quality */

-- Delete records with no associated AE Encounter record

delete from AE.HRG4Quality
where
	not exists
	(
	select
		1
	from
		AE.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
	)


-- Delete records that might be reinserted (if we don't use this method then rows in quality table will remain even if sucessfully processed subsequently)

delete from AE.HRG4Quality
where
	exists
	(
	select
		1
	from
		ETL.HRG4AEEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
	)


-- Delete records that are about to be reinserted

--delete from AE.HRG4Quality
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.HRG4AEQuality Quality

--	inner join ETL.HRG4AEEncounter Encounter
--	on	Encounter.RowNo = Quality.RowNo
--	and Encounter.FinancialYear = Quality.FinancialYear

--	where
--		Encounter.MergeEncounterRecno = HRG4Quality.MergeEncounterRecno
--	)


insert into AE.HRG4Quality
	(
	 MergeEncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4AEQuality Quality

	inner join ETL.HRG4AEEncounter Encounter
	on	Encounter.RowNo = Quality.RowNo
	and Encounter.FinancialYear = Quality.FinancialYear











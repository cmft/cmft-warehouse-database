﻿create procedure [ETL].[ImportTraTheatreBaseSession] as

--import the data
exec ETL.BuildTraTheatreBaseSession


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Theatre].[BaseSession]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildTheatreBaseSessionReference 'TRA||ORMIS'

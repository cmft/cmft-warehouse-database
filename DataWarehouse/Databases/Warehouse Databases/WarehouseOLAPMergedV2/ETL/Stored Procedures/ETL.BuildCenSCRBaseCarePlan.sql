﻿
CREATE procedure [ETL].[BuildCenSCRBaseCarePlan]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||SCR'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge SCR.BaseCarePlan target
using ETL.TLoadCenSCRBaseCarePlan source
on	source.UniqueRecordId = target.UniqueRecordId

when matched and not
(
	isnull(target.[ReferralUniqueRecordId], 0) = isnull(source.[ReferralUniqueRecordId], 0)
	and isnull(target.[SystemId], '') = isnull(source.[SystemId], '')
	and isnull(target.[WasPatientDiscussedAtMDT], '') = isnull(source.[WasPatientDiscussedAtMDT], '')
	and isnull(target.[MDTDate], getdate()) = isnull(source.[MDTDate], getdate())
	and isnull(target.[CarePlanAgreedDate], getdate()) = isnull(source.[CarePlanAgreedDate], getdate())
	and isnull(target.[DecisionOrganisationCode], '') = isnull(source.[DecisionOrganisationCode], '')
	and isnull(target.[Recurrence], '') = isnull(source.[Recurrence], '')
	and isnull(target.[CancerCarePlanIntentCode], '') = isnull(source.[CancerCarePlanIntentCode], '')
	and isnull(target.[FirstPlannedTreatmentTypeCode], '') = isnull(source.[FirstPlannedTreatmentTypeCode], '')
	and isnull(target.[SecondPlannedTreatmentTypeCode], '') = isnull(source.[SecondPlannedTreatmentTypeCode], '')
	and isnull(target.[ThirdPlannedTreatmentTypeCode], '') = isnull(source.[ThirdPlannedTreatmentTypeCode], '')
	and isnull(target.[FourthPlannedTreatmentTypeCode], '') = isnull(source.[FourthPlannedTreatmentTypeCode], '')
	and isnull(target.[NoTreatmentReasonCode], '') = isnull(source.[NoTreatmentReasonCode], '')
	and isnull(target.[CoMorbidityIndexCode], 0) = isnull(source.[CoMorbidityIndexCode], 0)
	and isnull(target.[PerformanceStatusCode], 0) = isnull(source.[PerformanceStatusCode], 0)
	and isnull(target.[PrimaryCareCommunicationDate], getdate()) = isnull(source.[PrimaryCareCommunicationDate], getdate())
	and isnull(target.[CarePlanAgreed], '') = isnull(source.[CarePlanAgreed], '')
	and isnull(target.[PlanTypeCode], '') = isnull(source.[PlanTypeCode], '')
	and isnull(target.[NotFirstChoiceOfTreatmentReasonCode], '') = isnull(source.[NotFirstChoiceOfTreatmentReasonCode], '')
	and isnull(target.[LocationCode], '') = isnull(source.[LocationCode], '')
	and isnull(target.[PrePostTreatment], '') = isnull(source.[PrePostTreatment], '')
	and isnull(target.[OncologistPresent], '') = isnull(source.[OncologistPresent], '')
	and isnull(target.[MDTActionedBy], '') = isnull(source.[MDTActionedBy], '')
	and isnull(target.[ReviewedByPathologistPriorToMDT], '') = isnull(source.[ReviewedByPathologistPriorToMDT], '')
	and isnull(target.[ReviewedBy], '') = isnull(source.[ReviewedBy], '')
	and isnull(target.[ReferredTo], '') = isnull(source.[ReferredTo], '')
	and isnull(target.[WhoReferredToCode], '') = isnull(source.[WhoReferredToCode], '')
	and isnull(target.[CopyLetterTo], '') = isnull(source.[CopyLetterTo], '')
	and isnull(target.[TrustReferredToCode], '') = isnull(source.[TrustReferredToCode], '')
	and isnull(target.[ReasonForReferalCode], 0) = isnull(source.[ReasonForReferalCode], 0)
	and isnull(target.[WasPatientDiscussedAtNetworkMDT], '') = isnull(source.[WasPatientDiscussedAtNetworkMDT], '')
	and isnull(target.[NetworkMeetingDate], getdate()) = isnull(source.[NetworkMeetingDate], getdate())
	and isnull(target.[NetworkActionedBy], '') = isnull(source.[NetworkActionedBy], '')
	and isnull(target.[PlannedTransplantTypeCode], 0) = isnull(source.[PlannedTransplantTypeCode], 0)
	and isnull(target.[RecurrenceTypeCode], '') = isnull(source.[RecurrenceTypeCode], '')
	and isnull(target.[RecurrenceDiagnosedBy], '') = isnull(source.[RecurrenceDiagnosedBy], '')
	and isnull(target.[ReasonNotCurative], '') = isnull(source.[ReasonNotCurative], '')
	and isnull(target.[Seizures], 0) = isnull(source.[Seizures], 0)
	and isnull(target.[Surgery], 0) = isnull(source.[Surgery], 0)
	and isnull(target.[CarePlanAgreedAtMDT], '') = isnull(source.[CarePlanAgreedAtMDT], '')
	and isnull(target.[MDTComments], '') = isnull(source.[MDTComments], '')
	and isnull(target.[NetworkDecisionCode], '') = isnull(source.[NetworkDecisionCode], '')
	and isnull(target.[NetworkComments], '') = isnull(source.[NetworkComments], '')
	and isnull(target.[ReasonNotDiscussedCode], '') = isnull(source.[ReasonNotDiscussedCode], '')
)
	
then update set
	target.[ReferralUniqueRecordId] = source.[ReferralUniqueRecordId]
	,target.[SystemId] = source.[SystemId]
	,target.[WasPatientDiscussedAtMDT] = source.[WasPatientDiscussedAtMDT]
	,target.[MDTDate] = source.[MDTDate]
	,target.[CarePlanAgreedDate] = source.[CarePlanAgreedDate]
	,target.[DecisionOrganisationCode] = source.[DecisionOrganisationCode]
	,target.[Recurrence] = source.[Recurrence]
	,target.[CancerCarePlanIntentCode] = source.[CancerCarePlanIntentCode]
	,target.[FirstPlannedTreatmentTypeCode] = source.[FirstPlannedTreatmentTypeCode]
	,target.[SecondPlannedTreatmentTypeCode] = source.[SecondPlannedTreatmentTypeCode]
	,target.[ThirdPlannedTreatmentTypeCode] = source.[ThirdPlannedTreatmentTypeCode]
	,target.[FourthPlannedTreatmentTypeCode] = source.[FourthPlannedTreatmentTypeCode]
	,target.[NoTreatmentReasonCode] = source.[NoTreatmentReasonCode]
	,target.[CoMorbidityIndexCode] = source.[CoMorbidityIndexCode]
	,target.[PerformanceStatusCode] = source.[PerformanceStatusCode]
	,target.[PrimaryCareCommunicationDate] = source.[PrimaryCareCommunicationDate]
	,target.[CarePlanAgreed] = source.[CarePlanAgreed]
	,target.[PlanTypeCode] = source.[PlanTypeCode]
	,target.[NotFirstChoiceOfTreatmentReasonCode] = source.[NotFirstChoiceOfTreatmentReasonCode]
	,target.[LocationCode] = source.[LocationCode]
	,target.[PrePostTreatment] = source.[PrePostTreatment]
	,target.[OncologistPresent] = source.[OncologistPresent]
	,target.[MDTActionedBy] = source.[MDTActionedBy]
	,target.[ReviewedByPathologistPriorToMDT] = source.[ReviewedByPathologistPriorToMDT]
	,target.[ReviewedBy] = source.[ReviewedBy]
	,target.[ReferredTo] = source.[ReferredTo]
	,target.[WhoReferredToCode] = source.[WhoReferredToCode]
	,target.[CopyLetterTo] = source.[CopyLetterTo]
	,target.[TrustReferredToCode] = source.[TrustReferredToCode]
	,target.[ReasonForReferalCode] = source.[ReasonForReferalCode]
	,target.[WasPatientDiscussedAtNetworkMDT] = source.[WasPatientDiscussedAtNetworkMDT]
	,target.[NetworkMeetingDate] = source.[NetworkMeetingDate]
	,target.[NetworkActionedBy] = source.[NetworkActionedBy]
	,target.[PlannedTransplantTypeCode] = source.[PlannedTransplantTypeCode]
	,target.[RecurrenceTypeCode] = source.[RecurrenceTypeCode]
	,target.[RecurrenceDiagnosedBy] = source.[RecurrenceDiagnosedBy]
	,target.[ReasonNotCurative] = source.[ReasonNotCurative]
	,target.[Seizures] = source.[Seizures]
	,target.[Surgery] = source.[Surgery]
	,target.[CarePlanAgreedAtMDT] = source.[CarePlanAgreedAtMDT]
	,target.[MDTComments] = source.[MDTComments]
	,target.[NetworkDecisionCode] = source.[NetworkDecisionCode]
	,target.[NetworkComments] = source.[NetworkComments]
	,target.[ReasonNotDiscussedCode] = source.[ReasonNotDiscussedCode]
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	UniqueRecordId
	,[ReferralUniqueRecordId]
	,[SystemId]
	,[WasPatientDiscussedAtMDT]
	,[MDTDate]
	,[CarePlanAgreedDate]
	,[DecisionOrganisationCode]
	,[Recurrence]
	,[CancerCarePlanIntentCode]
	,[FirstPlannedTreatmentTypeCode]
	,[SecondPlannedTreatmentTypeCode]
	,[ThirdPlannedTreatmentTypeCode]
	,[FourthPlannedTreatmentTypeCode]
	,[NoTreatmentReasonCode]
	,[CoMorbidityIndexCode]
	,[PerformanceStatusCode]
	,[PrimaryCareCommunicationDate]
	,[CarePlanAgreed]
	,[PlanTypeCode]
	,[NotFirstChoiceOfTreatmentReasonCode]
	,[LocationCode]
	,[PrePostTreatment]
	,[OncologistPresent]
	,[MDTActionedBy]
	,[ReviewedByPathologistPriorToMDT]
	,[ReviewedBy]
	,[ReferredTo]
	,[WhoReferredToCode]
	,[CopyLetterTo]
	,[TrustReferredToCode]
	,[ReasonForReferalCode]
	,[WasPatientDiscussedAtNetworkMDT]
	,[NetworkMeetingDate]
	,[NetworkActionedBy]
	,[PlannedTransplantTypeCode]
	,[RecurrenceTypeCode]
	,[RecurrenceDiagnosedBy]
	,[ReasonNotCurative]
	,[Seizures]
	,[Surgery]
	,[CarePlanAgreedAtMDT]
	,[MDTComments]
	,[NetworkDecisionCode]
	,[NetworkComments]
	,[ReasonNotDiscussedCode]
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.UniqueRecordId
	,source.[ReferralUniqueRecordId]
	,source.[SystemId]
	,source.[WasPatientDiscussedAtMDT]
	,source.[MDTDate]
	,source.[CarePlanAgreedDate]
	,source.[DecisionOrganisationCode]
	,source.[Recurrence]
	,source.[CancerCarePlanIntentCode]
	,source.[FirstPlannedTreatmentTypeCode]
	,source.[SecondPlannedTreatmentTypeCode]
	,source.[ThirdPlannedTreatmentTypeCode]
	,source.[FourthPlannedTreatmentTypeCode]
	,source.[NoTreatmentReasonCode]
	,source.[CoMorbidityIndexCode]
	,source.[PerformanceStatusCode]
	,source.[PrimaryCareCommunicationDate]
	,source.[CarePlanAgreed]
	,source.[PlanTypeCode]
	,source.[NotFirstChoiceOfTreatmentReasonCode]
	,source.[LocationCode]
	,source.[PrePostTreatment]
	,source.[OncologistPresent]
	,source.[MDTActionedBy]
	,source.[ReviewedByPathologistPriorToMDT]
	,source.[ReviewedBy]
	,source.[ReferredTo]
	,source.[WhoReferredToCode]
	,source.[CopyLetterTo]
	,source.[TrustReferredToCode]
	,source.[ReasonForReferalCode]
	,source.[WasPatientDiscussedAtNetworkMDT]
	,source.[NetworkMeetingDate]
	,source.[NetworkActionedBy]
	,source.[PlannedTransplantTypeCode]
	,source.[RecurrenceTypeCode]
	,source.[RecurrenceDiagnosedBy]
	,source.[ReasonNotCurative]
	,source.[Seizures]
	,source.[Surgery]
	,source.[CarePlanAgreedAtMDT]
	,source.[MDTComments]
	,source.[NetworkDecisionCode]
	,source.[NetworkComments]
	,source.[ReasonNotDiscussedCode]
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿create procedure [ETL].[ImportCenAPCWLBaseEncounter] as

--import the data
exec ETL.BuildCenAPCWLBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APCWL].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCWLBaseEncounterReference 'CEN||PAS'

﻿CREATE procedure [ETL].[ImportCenSCR] as

exec ETL.ImportCenSCRBaseReferral

exec ETL.ImportCenSCRBaseDefinitiveTreatment

exec ETL.ImportCenSCRBaseReferralTreatmentPTL

exec ETL.ImportCenSCRBaseCarePlan

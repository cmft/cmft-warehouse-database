﻿CREATE procedure [ETL].[BuildCenCOMBaseEncounter]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	COM.BaseEncounter target
using
	(
	select
		 EncounterRecno
		,SourceEncounterID
		,SourceUniqueID
		,SpecialtyID
		,StaffTeamID
		,ProfessionalCarerID
		,SeenByProfessionalCarerID
		,EncounterProfessionalCarerID
		,StartDate
		,StartTime
		,EndTime
		,ArrivedTime
		,SeenTime
		,DepartedTime
		,AttendedID
		,OutcomeID
		,EncounterOutcomeCode
		,EncounterDuration
		,ReferralID
		,ProfessionalCarerEpisodeID
		,PatientSourceID
		,PatientSourceSystemUniqueID
		,PatientNHSNumber
		,PatientNHSNumberStatusIndicator
		,PatientIDLocalIdentifier
		,PatientIDPASNumber
		,PatientIDPatientIdentifier
		,PatientIDPatientFacilityID
		,PatientTitleID
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,PatientPostcode
		,PatientDateOfBirth
		,PatientDateOfDeath
		,PatientSexID
		,PatientEthnicGroupID
		,PatientSpokenLanguageID
		,PatientReligionID
		,PatientMaritalStatusID
		,PatientCurrentRegisteredPracticeCode
		,PatientEncounterRegisteredPracticeCode
		,PatientCurrentRegisteredGPCode
		,PatientEncounterRegisteredGPCode
		,VisitID
		,ContactTypeID
		,EncounterTypeID
		,ScheduleTypeID
		,ScheduleReasonID
		,CanceledReasonID
		,CanceledTime
		,CancelledByID
		,MoveReasonID
		,MoveTime
		,ServicePointID
		,ServicePointSessionID
		,MoveCount
		,LocationTypeID
		,LocationDescription
		,LocationTypeHealthOrgID
		,EncounterLocationID
		,ServicePointStaysID
		,WaitingListID
		,PlannedAttendees
		,ActualAttendees
		,ProviderSpellID
		,RTTStatusID
		,EarliestReasinableOfferTime
		,EncounterInterventionsCount
		,CreatedTime
		,ModifiedTime
		,CreatedBy
		,ModifiedBy
		,Archived
		,ParentID
		,HealthOrgOwnerID
		,RecordStatus
		,JointActivityFlag
		,Comment
		,DerivedFirstAttendanceFlag
		,ReferralReceivedTime
		,ReferralReceivedDate
		,ReferralClosedTime
		,ReferralClosedDate
		,ReferralReasonID
		,ReferredToSpecialtyID
		,ReferredToStaffTeamID
		,ReferredToProfCarerID
		,ReferralSourceID
		,ReferralTypeID
		,CommissionerCode
		,PctOfResidence
		,LocalAuthorityCode
		,DistrictOfResidence
		,ContextCode
		,PatientEncounterRegisteredPracticeID
		,DNAReasonID
		,AgeCode
		,WaitDays
		,Created
		,Updated
		,ByWhom
		,CalledTime
	from
		ETL.TLoadCenCOMBaseEncounter Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.SourceEncounterRecno

	when not matched by source
	and	target.ContextCode = 'CEN||IPM'
	then delete

	when not matched
	then
		insert
			(
			 SourceEncounterRecno
			,SourceEncounterID
			,SourceUniqueID
			,SpecialtyID
			,StaffTeamID
			,ProfessionalCarerID
			,SeenByProfessionalCarerID
			,EncounterProfessionalCarerID
			,StartDate
			,StartTime
			,EndTime
			,ArrivedTime
			,SeenTime
			,DepartedTime
			,AttendedID
			,OutcomeID
			,EncounterOutcomeCode
			,EncounterDuration
			,ReferralID
			,ProfessionalCarerEpisodeID
			,PatientSourceID
			,PatientSourceSystemUniqueID
			,PatientNHSNumber
			,PatientNHSNumberStatusIndicator
			,PatientIDLocalIdentifier
			,PatientIDPASNumber
			,PatientIDPatientIdentifier
			,PatientIDPatientFacilityID
			,PatientTitleID
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,PatientPostcode
			,PatientDateOfBirth
			,PatientDateOfDeath
			,PatientSexID
			,PatientEthnicGroupID
			,PatientSpokenLanguageID
			,PatientReligionID
			,PatientMaritalStatusID
			,PatientCurrentRegisteredPracticeCode
			,PatientEncounterRegisteredPracticeCode
			,PatientCurrentRegisteredGPCode
			,PatientEncounterRegisteredGPCode
			,VisitID
			,ContactTypeID
			,EncounterTypeID
			,ScheduleTypeID
			,ScheduleReasonID
			,CanceledReasonID
			,CanceledTime
			,CancelledByID
			,MoveReasonID
			,MoveTime
			,ServicePointID
			,ServicePointSessionID
			,MoveCount
			,LocationTypeID
			,LocationDescription
			,LocationTypeHealthOrgID
			,EncounterLocationID
			,ServicePointStaysID
			,WaitingListID
			,PlannedAttendees
			,ActualAttendees
			,ProviderSpellID
			,RTTStatusID
			,EarliestReasinableOfferTime
			,EncounterInterventionsCount
			,CreatedTime
			,ModifiedTime
			,CreatedBy
			,ModifiedBy
			,Archived
			,ParentID
			,HealthOrgOwnerID
			,RecordStatus
			,JointActivityFlag
			,Comment
			,DerivedFirstAttendanceFlag
			,ReferralReceivedTime
			,ReferralReceivedDate
			,ReferralClosedTime
			,ReferralClosedDate
			,ReferralReasonID
			,ReferredToSpecialtyID
			,ReferredToStaffTeamID
			,ReferredToProfCarerID
			,ReferralSourceID
			,ReferralTypeID
			,CommissionerCode
			,PctOfResidence
			,LocalAuthorityCode
			,DistrictOfResidence
			,ContextCode
			,PatientEncounterRegisteredPracticeID
			,DNAReasonID
			,AgeCode
			,WaitDays
			,Created
			,Updated
			,ByWhom
			,CalledTime
			)
		values
			(
			 source.EncounterRecno
			,source.SourceEncounterID
			,source.SourceUniqueID
			,source.SpecialtyID
			,source.StaffTeamID
			,source.ProfessionalCarerID
			,source.SeenByProfessionalCarerID
			,source.EncounterProfessionalCarerID
			,source.StartDate
			,source.StartTime
			,source.EndTime
			,source.ArrivedTime
			,source.SeenTime
			,source.DepartedTime
			,source.AttendedID
			,source.OutcomeID
			,source.EncounterOutcomeCode
			,source.EncounterDuration
			,source.ReferralID
			,source.ProfessionalCarerEpisodeID
			,source.PatientSourceID
			,source.PatientSourceSystemUniqueID
			,source.PatientNHSNumber
			,source.PatientNHSNumberStatusIndicator
			,source.PatientIDLocalIdentifier
			,source.PatientIDPASNumber
			,source.PatientIDPatientIdentifier
			,source.PatientIDPatientFacilityID
			,source.PatientTitleID
			,source.PatientForename
			,source.PatientSurname
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.PatientPostcode
			,source.PatientDateOfBirth
			,source.PatientDateOfDeath
			,source.PatientSexID
			,source.PatientEthnicGroupID
			,source.PatientSpokenLanguageID
			,source.PatientReligionID
			,source.PatientMaritalStatusID
			,source.PatientCurrentRegisteredPracticeCode
			,source.PatientEncounterRegisteredPracticeCode
			,source.PatientCurrentRegisteredGPCode
			,source.PatientEncounterRegisteredGPCode
			,source.VisitID
			,source.ContactTypeID
			,source.EncounterTypeID
			,source.ScheduleTypeID
			,source.ScheduleReasonID
			,source.CanceledReasonID
			,source.CanceledTime
			,source.CancelledByID
			,source.MoveReasonID
			,source.MoveTime
			,source.ServicePointID
			,source.ServicePointSessionID
			,source.MoveCount
			,source.LocationTypeID
			,source.LocationDescription
			,source.LocationTypeHealthOrgID
			,source.EncounterLocationID
			,source.ServicePointStaysID
			,source.WaitingListID
			,source.PlannedAttendees
			,source.ActualAttendees
			,source.ProviderSpellID
			,source.RTTStatusID
			,source.EarliestReasinableOfferTime
			,source.EncounterInterventionsCount
			,source.CreatedTime
			,source.ModifiedTime
			,source.CreatedBy
			,source.ModifiedBy
			,source.Archived
			,source.ParentID
			,source.HealthOrgOwnerID
			,source.RecordStatus
			,source.JointActivityFlag
			,source.Comment
			,source.DerivedFirstAttendanceFlag
			,source.ReferralReceivedTime
			,source.ReferralReceivedDate
			,source.ReferralClosedTime
			,source.ReferralClosedDate
			,source.ReferralReasonID
			,source.ReferredToSpecialtyID
			,source.ReferredToStaffTeamID
			,source.ReferredToProfCarerID
			,source.ReferralSourceID
			,source.ReferralTypeID
			,source.CommissionerCode
			,source.PctOfResidence
			,source.LocalAuthorityCode
			,source.DistrictOfResidence
			,source.ContextCode
			,source.PatientEncounterRegisteredPracticeID
			,source.DNAReasonID
			,source.AgeCode
			,source.WaitDays
			,source.Created
			,source.Updated
			,source.ByWhom
			,source.CalledTime
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.SourceEncounterID = source.SourceEncounterID
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SpecialtyID = source.SpecialtyID
			,target.StaffTeamID = source.StaffTeamID
			,target.ProfessionalCarerID = source.ProfessionalCarerID
			,target.SeenByProfessionalCarerID = source.SeenByProfessionalCarerID
			,target.EncounterProfessionalCarerID = source.EncounterProfessionalCarerID
			,target.StartDate = source.StartDate
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.ArrivedTime = source.ArrivedTime
			,target.SeenTime = source.SeenTime
			,target.DepartedTime = source.DepartedTime
			,target.AttendedID = source.AttendedID
			,target.OutcomeID = source.OutcomeID
			,target.EncounterOutcomeCode = source.EncounterOutcomeCode
			,target.EncounterDuration = source.EncounterDuration
			,target.ReferralID = source.ReferralID
			,target.ProfessionalCarerEpisodeID = source.ProfessionalCarerEpisodeID
			,target.PatientSourceID = source.PatientSourceID
			,target.PatientSourceSystemUniqueID = source.PatientSourceSystemUniqueID
			,target.PatientNHSNumber = source.PatientNHSNumber
			,target.PatientNHSNumberStatusIndicator = source.PatientNHSNumberStatusIndicator
			,target.PatientIDLocalIdentifier = source.PatientIDLocalIdentifier
			,target.PatientIDPASNumber = source.PatientIDPASNumber
			,target.PatientIDPatientIdentifier = source.PatientIDPatientIdentifier
			,target.PatientIDPatientFacilityID = source.PatientIDPatientFacilityID
			,target.PatientTitleID = source.PatientTitleID
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.PatientPostcode = source.PatientPostcode
			,target.PatientDateOfBirth = source.PatientDateOfBirth
			,target.PatientDateOfDeath = source.PatientDateOfDeath
			,target.PatientSexID = source.PatientSexID
			,target.PatientEthnicGroupID = source.PatientEthnicGroupID
			,target.PatientSpokenLanguageID = source.PatientSpokenLanguageID
			,target.PatientReligionID = source.PatientReligionID
			,target.PatientMaritalStatusID = source.PatientMaritalStatusID
			,target.PatientCurrentRegisteredPracticeCode = source.PatientCurrentRegisteredPracticeCode
			,target.PatientEncounterRegisteredPracticeCode = source.PatientEncounterRegisteredPracticeCode
			,target.PatientCurrentRegisteredGPCode = source.PatientCurrentRegisteredGPCode
			,target.PatientEncounterRegisteredGPCode = source.PatientEncounterRegisteredGPCode
			,target.VisitID = source.VisitID
			,target.ContactTypeID = source.ContactTypeID
			,target.EncounterTypeID = source.EncounterTypeID
			,target.ScheduleTypeID = source.ScheduleTypeID
			,target.ScheduleReasonID = source.ScheduleReasonID
			,target.CanceledReasonID = source.CanceledReasonID
			,target.CanceledTime = source.CanceledTime
			,target.CancelledByID = source.CancelledByID
			,target.MoveReasonID = source.MoveReasonID
			,target.MoveTime = source.MoveTime
			,target.ServicePointID = source.ServicePointID
			,target.ServicePointSessionID = source.ServicePointSessionID
			,target.MoveCount = source.MoveCount
			,target.LocationTypeID = source.LocationTypeID
			,target.LocationDescription = source.LocationDescription
			,target.LocationTypeHealthOrgID = source.LocationTypeHealthOrgID
			,target.EncounterLocationID = source.EncounterLocationID
			,target.ServicePointStaysID = source.ServicePointStaysID
			,target.WaitingListID = source.WaitingListID
			,target.PlannedAttendees = source.PlannedAttendees
			,target.ActualAttendees = source.ActualAttendees
			,target.ProviderSpellID = source.ProviderSpellID
			,target.RTTStatusID = source.RTTStatusID
			,target.EarliestReasinableOfferTime = source.EarliestReasinableOfferTime
			,target.EncounterInterventionsCount = source.EncounterInterventionsCount
			,target.CreatedTime = source.CreatedTime
			,target.ModifiedTime = source.ModifiedTime
			,target.CreatedBy = source.CreatedBy
			,target.ModifiedBy = source.ModifiedBy
			,target.Archived = source.Archived
			,target.ParentID = source.ParentID
			,target.HealthOrgOwnerID = source.HealthOrgOwnerID
			,target.RecordStatus = source.RecordStatus
			,target.JointActivityFlag = source.JointActivityFlag
			,target.Comment = source.Comment
			,target.DerivedFirstAttendanceFlag = source.DerivedFirstAttendanceFlag
			,target.ReferralReceivedTime = source.ReferralReceivedTime
			,target.ReferralReceivedDate = source.ReferralReceivedDate
			,target.ReferralClosedTime = source.ReferralClosedTime
			,target.ReferralClosedDate = source.ReferralClosedDate
			,target.ReferralReasonID = source.ReferralReasonID
			,target.ReferredToSpecialtyID = source.ReferredToSpecialtyID
			,target.ReferredToStaffTeamID = source.ReferredToStaffTeamID
			,target.ReferredToProfCarerID = source.ReferredToProfCarerID
			,target.ReferralSourceID = source.ReferralSourceID
			,target.ReferralTypeID = source.ReferralTypeID
			,target.CommissionerCode = source.CommissionerCode
			,target.PctOfResidence = source.PctOfResidence
			,target.LocalAuthorityCode = source.LocalAuthorityCode
			,target.DistrictOfResidence = source.DistrictOfResidence
			,target.PatientEncounterRegisteredPracticeID = source.PatientEncounterRegisteredPracticeID
			,target.DNAReasonID = source.DNAReasonID
			,target.AgeCode = source.AgeCode
			,target.WaitDays = source.WaitDays
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
			,target.CalledTime = source.CalledTime

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿create procedure [ETL].[BuildTLoadAEBaseInvestigation] @LastLoadTime datetime as

declare @ReturnValue int = 0

-- 20151015	RR/CH	Phil Huitson raised an issue regarding investigations.  We identified ICE investigations which were previously in BaseInvestigation, where no longer in (think relates to the deployment of AE 28/09/2015)
--					Removed @LastLoadTime as not needed for investigations, plus it impacts on sequence numbering for an attendance.  
--					It assigns @LastLoadTime at end of Import proc so potentially can miss some investigations if ICE runs at the same time as A&E

BEGIN TRY

	truncate table ETL.TLoadAEBaseInvestigation

	--Central
	insert
	into
		ETL.TLoadAEBaseInvestigation
	(
		InvestigationRecno
		,MergeEncounterRecno
		,InvestigationSchemeCode
		,InvestigationCode
		,InvestigationTime
		,ResultTime
		,ContextCode
		,Action
	)

		select
			Investigation.InvestigationRecno
			,MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
			,InvestigationSchemeCode = cast(Investigation.InvestigationSchemeInUse as varchar(10))
			,InvestigationCode = cast(Investigation.InvestigationCode as varchar(50))
			,InvestigationTime = cast(Investigation.InvestigationDate as smalldatetime)
			,ResultTime = cast(Investigation.ResultDate as smalldatetime)
			,BaseEncounter.ContextCode	
			,Action = case when Investigation.Updated is null then 'INSERT' else 'UPDATE' end
		from
			[$(Warehouse)].AE.Investigation Investigation

	--this assumes that SourceUniqueID is unique across contexts! as it happens, Investigations only exist against 'CEN||SYM' so this is okay
		inner join AE.BaseEncounter
		on	BaseEncounter.SourceUniqueID = Investigation.AESourceUniqueID
		and	BaseEncounter.ContextCode in
										(
										 'CEN||SYM'
										--,'CEN||ADAS'
										--,'CEN||PAS'
										--,'TRA||ADAS'
										)
					
		--where
		--	isnull(Investigation.Updated, Investigation.Created) > @LastLoadTime


	--ICE Tests
	--CH 04/08/15 doesnt insert records that have happened in symphony already based on the national investigation code (to avoid duplicates)
	insert
	into
		ETL.TLoadAEBaseInvestigation
	(
		InvestigationRecno
		,MergeEncounterRecno
		,InvestigationCode
		,InvestigationTime
		,ContextCode
		,Action
	)
	select
		InvestigationRecno 
		,Encounter.MergeEncounterRecno
		,InvestigationCode
		,Encounter.InvestigationTime
		,Encounter.ContextCode
		,Encounter.Action
	from
		(

		select
			InvestigationRecno = CENICETest.TestRecno
			,BaseEncounter.MergeEncounterRecno
			,InvestigationTime = CENICETest.OrderRequestTime
			,InvestigationCode = CENICETest.TestCode
			,CENICETest.ContextCode
			,Action = case when CENICETest.Updated is null then 'INSERT' else 'UPDATE' end
		from
			ETL.TImportCenResultBaseCENICETest CENICETest
			
		inner join AE.BaseEncounter
		on	CENICETest.DistrictNo = BaseEncounter.DistrictNo
		and	CENICETest.OrderRequestTime between BaseEncounter.ArrivalTime and BaseEncounter.DepartureTime
		and BaseEncounter.ContextCode = 'CEN||SYM'
		and	not exists
			(
			select
				1
			from
				AE.BaseEncounter Previous
			where
				Previous.DistrictNo = BaseEncounter.DistrictNo
			and	CENICETest.OrderRequestTime between Previous.ArrivalTime and Previous.DepartureTime
			and Previous.ContextCode = BaseEncounter.ContextCode
			and	(
					Previous.ArrivalTime > BaseEncounter.ArrivalTime
				or	(
						Previous.ArrivalTime = BaseEncounter.ArrivalTime
					and	Previous.MergeEncounterRecno > BaseEncounter.MergeEncounterRecno
					)
				)
			)

		inner join Result.Test
		on	Test.SourceTestCode = CENICETest.TestCode
		and Test.SourceContextCode = 'CEN||ICE'

		where
			not exists
				(
				select
					1
				from
					ETL.TLoadAEBaseInvestigation BaseInvestigation
					
				inner join AE.Investigation
				on BaseInvestigation.InvestigationCode = Investigation.SourceInvestigationCode
				and BaseInvestigation.ContextCode = Investigation.SourceContextCode
				
				where
					BaseInvestigation.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
				and	Investigation.NationalInvestigationCode = Test.NationalTestCode
				)
					
		--and	isnull(CENICETest.Updated, CENICETest.Created) > @LastLoadTime
		) Encounter



	--Trafford
	insert
	into
		ETL.TLoadAEBaseInvestigation
	(
		InvestigationRecno
		,MergeEncounterRecno
		,InvestigationSchemeCode
		,InvestigationCode
		,InvestigationTime
		,ResultTime
		,ContextCode
		,Action
	)
	select
		Investigation.InvestigationRecno
		,BaseEncounter.MergeEncounterRecno
		,InvestigationSchemeCode = cast(Investigation.InvestigationSchemeInUse as varchar(10))
		,InvestigationCode = cast(Investigation.InvestigationCode as varchar(50))
		,InvestigationTime = cast(Investigation.InvestigationDate as smalldatetime)
		,ResultTime = cast(Investigation.ResultDate as smalldatetime)
		,BaseEncounter.ContextCode
		,Action = case when Investigation.Updated is null then 'INSERT' else 'UPDATE' end
	from
		[$(TraffordWarehouse)].dbo.AEInvestigation Investigation

	inner join AE.BaseEncounter
	on	BaseEncounter.SourceUniqueID = Investigation.AESourceUniqueID
	and	BaseEncounter.ContextCode in
									(
									 'TRA||SYM'
									)
	--where
	--	isnull(Investigation.Updated, Investigation.Created) > @LastLoadTime


--now pick up deletes
	insert
	into
		ETL.TLoadAEBaseInvestigation
	(
		InvestigationRecno
		,ContextCode
		,Action
		,MergeEncounterRecno
	)
	select
		Activity.InvestigationRecno
		,Activity.ContextCode
		,Action = 'DELETE'
		,Activity.MergeEncounterRecno
	from
		AE.BaseInvestigation Activity
	where
		Activity.ContextCode in
			(
			'CEN||ADAS'
			,'CEN||PAS'
			,'CEN||SYM'
			)

	and	not exists
		(
		select
			1
		from
			[$(Warehouse)].AE.Investigation Encounter
		where
			Encounter.InvestigationRecno = Activity.InvestigationRecno
		)
		
	union all
	
	select
		Activity.InvestigationRecno
		,Activity.ContextCode
		,Action = 'DELETE'
		,Activity.MergeEncounterRecno
	from
		AE.BaseInvestigation Activity
	where
		Activity.ContextCode in
			(
			'TRA||ADAS'
			,'TRA||SYM'
			)
	and	not exists
		(
		select
			1
		from
			[$(TraffordWarehouse)].dbo.AEInvestigation Encounter
		where
			Encounter.InvestigationRecno = Activity.InvestigationRecno
		)


END TRY

BEGIN
    CATCH
	
	set @ReturnValue = @@ERROR

    DECLARE @ErrorSeverity INT,
            @ErrorNumber   INT,
            @ErrorMessage nvarchar(4000),
            @ErrorState INT,
            @ErrorLine  INT,
            @ErrorProc nvarchar(200)
            -- Grab error information from SQL functions
    SET @ErrorSeverity = ERROR_SEVERITY()
    SET @ErrorNumber   = ERROR_NUMBER()
    SET @ErrorMessage  = ERROR_MESSAGE()
    SET @ErrorState    = ERROR_STATE()
    SET @ErrorLine     = ERROR_LINE()
    SET @ErrorProc     = ERROR_PROCEDURE()
    SET @ErrorMessage  = 'An error occurred.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
    -- Not all errors generate an error state, to set to 1 if it's zero
    IF @ErrorState  = 0
    SET @ErrorState = 1
    -- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
    IF @@TRANCOUNT > 0
    BEGIN
            --print 'Rollback transaction'
            ROLLBACK TRANSACTION
    END
    RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
END CATCH


RETURN @ReturnValue

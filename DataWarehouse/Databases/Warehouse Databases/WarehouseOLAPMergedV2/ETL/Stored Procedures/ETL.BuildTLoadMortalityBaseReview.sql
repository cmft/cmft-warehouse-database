﻿


CREATE proc [ETL].[BuildTLoadMortalityBaseReview]

as


Truncate table ETL.TLoadMortalityBaseReview

-- CMFT||MORTARCHV
-- Insert the old data first

Insert into ETL.TLoadMortalityBaseReview
(
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo 
	,Question
	,Response
	,ContextCode 
	,ReviewCheckSum
)

Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo 
	,Question
	,Response
	,ContextCode 
	,ReviewCheckSum = checksum
							(SourceUniqueID
							,SourcePatientNo	
							,EpisodeStartTime	
							,APCContextCode
							,FormTypeCode
							,ReviewStatusCode
							,PrimaryReviewAssignedTime
							,PrimaryReviewCompletedTime
							,PrimaryReviewerCode
							,SecondaryReviewAssignedTime 
							,SecondaryReviewCompletedTime
							,SecondaryReviewerCode 
							,ClassificationGrade
							,RequiresFurtherInvestigation
							,QuestionNo 
							,Question
							,Response
							,ContextCode
							)
from
	(
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime 
		,PrimaryReviewCompletedTime 
		,PrimaryReviewerCode 
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime 
		,SecondaryReviewerCode 
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
	from
		ETL.TImportMortalityBaseReviewCriticalCare
		
	union
	
	Select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime 
		,PrimaryReviewCompletedTime 
		,PrimaryReviewerCode 
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime 
		,SecondaryReviewerCode
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
	from
		ETL.TImportMortalityBaseReviewGynaecology
		
	union
	
	Select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime 
		,PrimaryReviewCompletedTime 
		,PrimaryReviewerCode 
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime 
		,SecondaryReviewerCode
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
	from
		ETL.TImportMortalityBaseReviewNICU
		
	union
	
	Select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime 
		,PrimaryReviewCompletedTime 
		,PrimaryReviewerCode 
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime 
		,SecondaryReviewerCode
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
	from
		ETL.TImportMortalityBaseReviewSpecialistMedicine
		
	union
	
	Select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime 
		,PrimaryReviewCompletedTime 
		,PrimaryReviewerCode 
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime 
		,SecondaryReviewerCode 
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
	from
		ETL.TImportMortalityBaseReviewSurgery
		
	union
	
	Select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime 
		,PrimaryReviewCompletedTime 
		,PrimaryReviewerCode 
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime 
		,SecondaryReviewerCode 
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
	from
		ETL.TImportMortalityBaseReviewPaediatrics
	)
	Mortality


-- CMFT||MORT
-- Insert the new data 

Insert into ETL.TLoadMortalityBaseReview
	(
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,MortalityReviewAdded 
		,ReviewStatusCode
		,PrimaryReviewAssignedTime
		,PrimaryReviewCompletedTime
		,PrimaryReviewerCode
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime
		,SecondaryReviewerCode 
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
		,ReviewCheckSum
	)
Select 
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,MortalityReviewAdded 
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode 
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo 
	,Question
	,Response
	,ContextCode 
	,ReviewCheckSum
from
	(
	Select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,MortalityReviewAdded 
		,ReviewStatusCode
		,PrimaryReviewAssignedTime
		,PrimaryReviewCompletedTime
		,PrimaryReviewerCode 
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime
		,SecondaryReviewerCode 
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
		,ReviewCheckSum = checksum
								(SourceUniqueID
								,SourcePatientNo	
								,EpisodeStartTime	
								,APCContextCode
								,FormTypeCode
								,MortalityReviewAdded 
								,ReviewStatusCode
								,PrimaryReviewAssignedTime
								,PrimaryReviewCompletedTime
								,PrimaryReviewerCode
								,SecondaryReviewAssignedTime 
								,SecondaryReviewCompletedTime
								,SecondaryReviewerCode 
								,ClassificationGrade
								,RequiresFurtherInvestigation
								,QuestionNo 
								,Question
								,Response
								,ContextCode
								)
	from
		ETL.TImportMortalityBaseReviewAdult
	
	union
	
	Select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,MortalityReviewAdded 
		,ReviewStatusCode
		,PrimaryReviewAssignedTime
		,PrimaryReviewCompletedTime
		,PrimaryReviewerCode 
		,SecondaryReviewAssignedTime 
		,SecondaryReviewCompletedTime
		,SecondaryReviewerCode 
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo 
		,Question
		,Response
		,ContextCode 
		,ReviewCheckSum = checksum
								(SourceUniqueID
								,SourcePatientNo	
								,EpisodeStartTime	
								,APCContextCode
								,FormTypeCode
								,MortalityReviewAdded 
								,ReviewStatusCode
								,PrimaryReviewAssignedTime
								,PrimaryReviewCompletedTime
								,PrimaryReviewerCode
								,SecondaryReviewAssignedTime 
								,SecondaryReviewCompletedTime
								,SecondaryReviewerCode 
								,ClassificationGrade
								,RequiresFurtherInvestigation
								,QuestionNo 
								,Question
								,Response
								,ContextCode
								)
	from
		ETL.TImportMortalityBaseReviewNeonatology
	) 
	CMFTMort














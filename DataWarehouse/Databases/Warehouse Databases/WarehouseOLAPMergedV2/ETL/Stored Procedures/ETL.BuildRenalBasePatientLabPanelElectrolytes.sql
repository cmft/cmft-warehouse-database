﻿
create procedure [ETL].[BuildRenalBasePatientLabPanelElectrolytes]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||CV'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Renal.BasePatientLabPanelElectrolytes target
using [$(Warehouse)].Renal.PatientLabPanelElectrolytes source
on	source.PatientLabPanelElectrolytesRecno = target.PatientLabPanelElectrolytesRecno

when matched and not
(
	isnull(target.[SourceUniqueID], '') = isnull(source.[SourceUniqueID], '')
	and isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[LabTestObjectID], 0) = isnull(source.[LabTestObjectID], 0)
	and isnull(target.[LabTestDate], getdate()) = isnull(source.[LabTestDate], getdate())
	and isnull(target.[LabTestQualifier], '') = isnull(source.[LabTestQualifier], '')
	and isnull(target.[LabTestStatus], '') = isnull(source.[LabTestStatus], '')
	and isnull(target.[AnionGapNum], 0) = isnull(source.[AnionGapNum], 0)
	and isnull(target.[AnionGapStr], '') = isnull(source.[AnionGapStr], '')
	and isnull(target.[AnionGapFlg], '') = isnull(source.[AnionGapFlg], '')
	and isnull(target.[ChlorideNum], 0) = isnull(source.[ChlorideNum], 0)
	and isnull(target.[ChlorideStr], '') = isnull(source.[ChlorideStr], '')
	and isnull(target.[ChlorideFlg], '') = isnull(source.[ChlorideFlg], '')
	and isnull(target.[CO2Num], 0) = isnull(source.[CO2Num], 0)
	and isnull(target.[CO2Str], '') = isnull(source.[CO2Str], '')
	and isnull(target.[CO2Flg], '') = isnull(source.[CO2Flg], '')
	and isnull(target.[CreatinineNum], 0) = isnull(source.[CreatinineNum], 0)
	and isnull(target.[CreatinineStr], '') = isnull(source.[CreatinineStr], '')
	and isnull(target.[CreatinineFlg], '') = isnull(source.[CreatinineFlg], '')
	and isnull(target.[GlucoseNum], 0) = isnull(source.[GlucoseNum], 0)
	and isnull(target.[GlucoseStr], '') = isnull(source.[GlucoseStr], '')
	and isnull(target.[GlucoseFlg], '') = isnull(source.[GlucoseFlg], '')
	and isnull(target.[HCO3Num], 0) = isnull(source.[HCO3Num], 0)
	and isnull(target.[HCO3Str], '') = isnull(source.[HCO3Str], '')
	and isnull(target.[HCO3Flg], '') = isnull(source.[HCO3Flg], '')
	and isnull(target.[PotassiumNum], 0) = isnull(source.[PotassiumNum], 0)
	and isnull(target.[PotassiumStr], '') = isnull(source.[PotassiumStr], '')
	and isnull(target.[PotassiumFlg], '') = isnull(source.[PotassiumFlg], '')
	and isnull(target.[SodiumNum], 0) = isnull(source.[SodiumNum], 0)
	and isnull(target.[SodiumStr], '') = isnull(source.[SodiumStr], '')
	and isnull(target.[SodiumFlg], '') = isnull(source.[SodiumFlg], '')
	and isnull(target.[UrateNum], 0) = isnull(source.[UrateNum], 0)
	and isnull(target.[UrateStr], '') = isnull(source.[UrateStr], '')
	and isnull(target.[UrateFlg], '') = isnull(source.[UrateFlg], '')
	and isnull(target.[BloodUreaNitrogenNum], 0) = isnull(source.[BloodUreaNitrogenNum], 0)
	and isnull(target.[BloodUreaNitrogenStr], '') = isnull(source.[BloodUreaNitrogenStr], '')
	and isnull(target.[BloodUreaNitrogenFlg], '') = isnull(source.[BloodUreaNitrogenFlg], '')
	and isnull(target.[UricAcidNum], 0) = isnull(source.[UricAcidNum], 0)
	and isnull(target.[UricAcidStr], '') = isnull(source.[UricAcidStr], '')
	and isnull(target.[UricAcidFlg], '') = isnull(source.[UricAcidFlg], '')
)
then update set
	target.[SourceUniqueID] = source.[SourceUniqueID]
	,target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[LabTestObjectID] = source.[LabTestObjectID]
	,target.[LabTestDate] = source.[LabTestDate]
	,target.[LabTestQualifier] = source.[LabTestQualifier]
	,target.[LabTestStatus] = source.[LabTestStatus]
	,target.[AnionGapNum] = source.[AnionGapNum]
	,target.[AnionGapStr] = source.[AnionGapStr]
	,target.[AnionGapFlg] = source.[AnionGapFlg]
	,target.[ChlorideNum] = source.[ChlorideNum]
	,target.[ChlorideStr] = source.[ChlorideStr]
	,target.[ChlorideFlg] = source.[ChlorideFlg]
	,target.[CO2Num] = source.[CO2Num]
	,target.[CO2Str] = source.[CO2Str]
	,target.[CO2Flg] = source.[CO2Flg]
	,target.[CreatinineNum] = source.[CreatinineNum]
	,target.[CreatinineStr] = source.[CreatinineStr]
	,target.[CreatinineFlg] = source.[CreatinineFlg]
	,target.[GlucoseNum] = source.[GlucoseNum]
	,target.[GlucoseStr] = source.[GlucoseStr]
	,target.[GlucoseFlg] = source.[GlucoseFlg]
	,target.[HCO3Num] = source.[HCO3Num]
	,target.[HCO3Str] = source.[HCO3Str]
	,target.[HCO3Flg] = source.[HCO3Flg]
	,target.[PotassiumNum] = source.[PotassiumNum]
	,target.[PotassiumStr] = source.[PotassiumStr]
	,target.[PotassiumFlg] = source.[PotassiumFlg]
	,target.[SodiumNum] = source.[SodiumNum]
	,target.[SodiumStr] = source.[SodiumStr]
	,target.[SodiumFlg] = source.[SodiumFlg]
	,target.[UrateNum] = source.[UrateNum]
	,target.[UrateStr] = source.[UrateStr]
	,target.[UrateFlg] = source.[UrateFlg]
	,target.[BloodUreaNitrogenNum] = source.[BloodUreaNitrogenNum]
	,target.[BloodUreaNitrogenStr] = source.[BloodUreaNitrogenStr]
	,target.[BloodUreaNitrogenFlg] = source.[BloodUreaNitrogenFlg]
	,target.[UricAcidNum] = source.[UricAcidNum]
	,target.[UricAcidStr] = source.[UricAcidStr]
	,target.[UricAcidFlg] = source.[UricAcidFlg]
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	 [SourceUniqueID]
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[LabTestObjectID]
	,[LabTestDate]
	,[LabTestQualifier]
	,[LabTestStatus]
	,[AnionGapNum]
	,[AnionGapStr]
	,[AnionGapFlg]
	,[ChlorideNum]
	,[ChlorideStr]
	,[ChlorideFlg]
	,[CO2Num]
	,[CO2Str]
	,[CO2Flg]
	,[CreatinineNum]
	,[CreatinineStr]
	,[CreatinineFlg]
	,[GlucoseNum]
	,[GlucoseStr]
	,[GlucoseFlg]
	,[HCO3Num]
	,[HCO3Str]
	,[HCO3Flg]
	,[PotassiumNum]
	,[PotassiumStr]
	,[PotassiumFlg]
	,[SodiumNum]
	,[SodiumStr]
	,[SodiumFlg]
	,[UrateNum]
	,[UrateStr]
	,[UrateFlg]
	,[BloodUreaNitrogenNum]
	,[BloodUreaNitrogenStr]
	,[BloodUreaNitrogenFlg]
	,[UricAcidNum]
	,[UricAcidStr]
	,[UricAcidFlg]
	,[PatientLabPanelElectrolytesRecno]
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[SourceUniqueID]
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[LabTestObjectID]
	,source.[LabTestDate]
	,source.[LabTestQualifier]
	,source.[LabTestStatus]
	,source.[AnionGapNum]
	,source.[AnionGapStr]
	,source.[AnionGapFlg]
	,source.[ChlorideNum]
	,source.[ChlorideStr]
	,source.[ChlorideFlg]
	,source.[CO2Num]
	,source.[CO2Str]
	,source.[CO2Flg]
	,source.[CreatinineNum]
	,source.[CreatinineStr]
	,source.[CreatinineFlg]
	,source.[GlucoseNum]
	,source.[GlucoseStr]
	,source.[GlucoseFlg]
	,source.[HCO3Num]
	,source.[HCO3Str]
	,source.[HCO3Flg]
	,source.[PotassiumNum]
	,source.[PotassiumStr]
	,source.[PotassiumFlg]
	,source.[SodiumNum]
	,source.[SodiumStr]
	,source.[SodiumFlg]
	,source.[UrateNum]
	,source.[UrateStr]
	,source.[UrateFlg]
	,source.[BloodUreaNitrogenNum]
	,source.[BloodUreaNitrogenStr]
	,source.[BloodUreaNitrogenFlg]
	,source.[UricAcidNum]
	,source.[UricAcidStr]
	,source.[UricAcidFlg]
	,source.[PatientLabPanelElectrolytesRecno]
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

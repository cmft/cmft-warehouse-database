﻿










CREATE proc [ETL].[BuildPSSAPC]

as

/* Process APC Encounter */

-- delete records with no associated APC Encounter record

delete from APC.PSSEncounter
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSEncounter.MergeEncounterRecno
	)

-- delete records that are about to be reinserted

delete from APC.PSSEncounter
where
	exists
	(
	select
		1
	from
		ETL.PSSAPCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSEncounter.MergeEncounterRecno
	)

insert into	APC.PSSEncounter
	(
	MergeEncounterRecno
	,NationalProgrammeOfCareServiceCode
	,ServiceLineCode
	,ServiceLineCodeList
	,Created
	,ByWhom
	)

select
	MergeEncounterRecno
	,NationalProgrammeOfCareServiceCode
	,ServiceLineCode
	,ServiceLineCodeList
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.PSSAPCEncounter Encounter
where
	coalesce(NationalProgrammeOfCareServiceCode, '') <> ''
and coalesce(ServiceLineCode, '') <> ''
and coalesce(ServiceLineCodeList, '') <> ''


/* Process APC Spell */

-- delete records with no associated APC Encounter record

delete from APC.PSSSpell
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSSpell.MergeEncounterRecno
	)

-- delete records that are about to be reinserted

-- this method will delete all MergeEncounterRecnos that are in the ETL.Encounter irrespective of whether in destination table or not.
-- ensures that only correct recnos are in table after proc completes


delete from APC.PSSSpell

where
	exists
	(
	select
		1
	from
		ETL.PSSAPCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSSpell.MergeEncounterRecno
	)


--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.PSSAPCSpell Spell

--	inner join ETL.PSSAPCEncounter Encounter
--	on	Encounter.RowNo = Spell.RowNo

--	where
--		Encounter.MergeEncounterRecno = PSSSpell.MergeEncounterRecno
--	)


insert into APC.PSSSpell
	(
	MergeEncounterRecno
	,NationalProgrammeOfCareServiceCode
	,ServiceLineCode
	,Created
	,ByWhom
	)

select
	MergeEncounterRecno
	,Spell.NationalProgrammeOfCareServiceCode
	,Spell.ServiceLineCode
	,Created = getdate()
	,ByWhom = suser_name()
from
	 ETL.PSSAPCSpell Spell

inner join ETL.PSSAPCEncounter Encounter
on	Spell.RowNo = Encounter.RowNo 
and	Spell.FinancialYear = Encounter.FinancialYear

where
	coalesce(Spell.NationalProgrammeOfCareServiceCode, '') <> ''
and coalesce(Spell.ServiceLineCode, '') <> ''


/* Process Quality */

-- Delete records with no associated APC Encounter record

delete from APC.PSSQuality
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSQuality.MergeEncounterRecno
	)


-- Delete records that are about to be reinserted

--delete from APC.PSSQuality
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.PSSAPCQuality Quality

--	inner join ETL.PSSAPCEncounter Encounter
--	on	Encounter.RowNo = Quality.RowNo

--	where
--		Encounter.MergeEncounterRecno = PSSQuality.MergeEncounterRecno
--	)

-- Delete records that might be reinserted (if we don't use this method then rows in quality table will remain even if sucessfully processed subsequently)

delete from APC.PSSQuality
where
	exists
	(
	select
		1
	from
		ETL.PSSAPCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSQuality.MergeEncounterRecno
	)

insert into APC.PSSQuality
	(
	 MergeEncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	,Created
	,ByWhom
	)
select
	 MergeEncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.PSSAPCQuality Quality

inner join ETL.PSSAPCEncounter Encounter
on	Quality.RowNo = Encounter.RowNo
and	Quality.FinancialYear = Encounter.FinancialYear















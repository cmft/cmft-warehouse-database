﻿


CREATE procedure [ETL].[BuildDictationBaseDocumentReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	
declare
	 @deleted int
	,@inserted int
	,@updated int
	

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Dictation.BaseDocumentReference
			where
				BaseDocumentReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)

--more efficient to delete here than in the merge
delete
from
	Dictation.BaseDocumentReference
where
	not exists
	(
	select
		1
	from
		Dictation.BaseDocument
	where
		BaseDocument.MergeDocumentRecno = BaseDocumentReference.MergeDocumentRecno
	)
and	BaseDocumentReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT

merge
	Dictation.BaseDocumentReference target
using
	(
	select
		BaseDocument.MergeDocumentRecno
		,ContextID = Context.ContextID
		,DocumentTypeID = SourceDocumentTypeID
		,SignedByID = SystemUserID
		,SignedStatusID	= SourceDocumentStatusID
		,DepartmentID = SourceDepartmentID
		,BaseDocument.Created
		,BaseDocument.Updated
		,BaseDocument.ByWhom
	from
		Dictation.BaseDocument

	inner join WH.Context
	on	Context.ContextCode = BaseDocument.ContextCode

	left join WH.DocumentStatus
	on DocumentStatus.SourceDocumentStatusCode = coalesce(BaseDocument.SignedStatusCode,'-1')
	and DocumentStatus.SourceContextCode = BaseDocument.ContextCode
	
	left join WH.DocumentType
	on DocumentType.SourceDocumentTypeCode = coalesce(BaseDocument.DocumentTypeCode,'-1')
	and DocumentType.SourceContextCode = BaseDocument.ContextCode
	
	left join WH.SystemUser 
	on SystemUser.UserID = coalesce(BaseDocument.SignedByCode,'-1')
	and SystemUser.ContextCode = BaseDocument.ContextCode

	left join WH.Department 
	on Department.SourceDepartmentCode = coalesce(BaseDocument.DepartmentCode,'-1')
	and Department.SourceContextCode = BaseDocument.ContextCode
	
	where
		BaseDocument.Updated > @LastUpdated
	and	BaseDocument.ContextCode = @ContextCode

	) source
	on	source.MergeDocumentRecno = target.MergeDocumentRecno

	when not matched
	then
		insert
			(
			MergeDocumentRecno
			,ContextID
			,DocumentTypeID
			,SignedByID
			,SignedStatusID
			,DepartmentID
			,Created
			,Updated
			,ByWhom
			)
			
		values
			(
			source.MergeDocumentRecno
			,source.ContextID
			,source.DocumentTypeID
			,source.SignedByID
			,source.SignedStatusID
			,source.DepartmentID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeDocumentRecno = source.MergeDocumentRecno
			,target.ContextID = source.ContextID
			,target.DocumentTypeID = source.DocumentTypeID
			,target.SignedByID = source.SignedByID
			,target.SignedStatusID = source.SignedStatusID
			,target.DepartmentID = source.DepartmentID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats







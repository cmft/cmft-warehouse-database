﻿CREATE procedure [ETL].[LoadResultBaseOrderResult]
as

-------------------------------------------------
--When		Who	What
--20150709	DG	Created procedure
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE
	(
	 MergeRecno int
	,Action nvarchar(10)
	);


merge
	Result.BaseOrderResult target
using
	(
	select
		 OrderResultRecno
		,ContextCode = cast(ContextCode as varchar(8))
		,OrderSourceUniqueID = cast(OrderSourceUniqueID as int)
		,OrderRequestTime = cast(OrderRequestTime as datetime)
		,ResultSourceUniqueID = cast(ResultSourceUniqueID as int)
		,InterfaceCode = cast(InterfaceCode as varchar(10))
		,OrderResultChecksum = 
			checksum(
				 OrderResultRecno
				,ContextCode
				,OrderSourceUniqueID
				,OrderRequestTime
				,ResultSourceUniqueID
				,InterfaceCode
			)				
	from
		ETL.TLoadResultBaseOrderResult
	) source
	on	source.ContextCode = target.ContextCode
	and	source.OrderResultRecno = target.OrderResultRecno

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			OrderResultRecno
			,ContextCode
			,OrderSourceUniqueID
			,OrderRequestTime
			,ResultSourceUniqueID
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,OrderResultChecksum
			)
		values
			(
			 source.OrderResultRecno
			,source.ContextCode
			,source.OrderSourceUniqueID
			,source.OrderRequestTime
			,source.ResultSourceUniqueID
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,source.OrderResultChecksum
			)

	when matched
	and	target.OrderResultChecksum <> source.OrderResultChecksum
	then
		update
		set
			 target.OrderResultRecno = source.OrderResultRecno
			,target.ContextCode = source.ContextCode
			,target.OrderSourceUniqueID = source.OrderSourceUniqueID
			,target.OrderRequestTime = source.OrderRequestTime
			,target.ResultSourceUniqueID = source.ResultSourceUniqueID
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.OrderResultChecksum = source.OrderResultChecksum

output
	 coalesce(inserted.MergeOrderResultRecno, deleted.MergeOrderResultRecno)
	,$action
	into
		@MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


--merge ProcessList in case it already has rows from a previous (failed) run
--merge
--	Result.ProcessList target
--using
--	@MergeSummary source
--on	source.MergeRecno = target.MergeRecno

--when not matched
--then
--	insert
--		(
--		 MergeRecno
--		,Action
--		)
--	values
--		(
--		 MergeRecno
--		,Action
--		)

--when matched
--then
--	update
--	set
--		target.Action = source.Action
;



select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




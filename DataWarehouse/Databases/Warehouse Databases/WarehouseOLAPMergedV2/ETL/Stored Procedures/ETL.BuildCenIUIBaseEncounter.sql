﻿
CREATE procedure [ETL].[BuildCenIUIBaseEncounter]


as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge IUI.BaseEncounter target
using 
	(
	select
		 EncounterRecno
		,DateOfBirth
		,NHSNumber
		,CasenoteNumber
		,PatientName
		,AttemptNumber
		,CommissionerCode
		,TreatmentDate
		,OutcomeID
		,TreatmentID
		,InterfaceCode
		,ContextCode
		,Created
		,Updated
		,ByWhom
	from
		ETL.TLoadCenIUIBaseEncounter

		) source
	on	source.EncounterRecno = target.EncounterRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CEN||IUI'
	then delete

	when not matched 
	then
		insert
			(
			 EncounterRecno
			,DateOfBirth
			,NHSNumber
			,CasenoteNumber
			,PatientName
			,AttemptNumber
			,CommissionerCode
			,TreatmentDate
			,OutcomeID
			,TreatmentID
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)

		values
			(
			 source.EncounterRecno
			,source.DateOfBirth
			,source.NHSNumber
			,source.CasenoteNumber
			,source.PatientName
			,source.AttemptNumber
			,source.CommissionerCode
			,source.TreatmentDate
			,source.OutcomeID
			,source.TreatmentID
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
		  isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.PatientName, '') = isnull(source.PatientName, '')
		and isnull(target.AttemptNumber, 0) = isnull(source.AttemptNumber, 0)
		and isnull(target.CommissionerCode, '') = isnull(source.CommissionerCode, '')
		and isnull(target.TreatmentDate, getdate()) = isnull(source.TreatmentDate, getdate())
		and isnull(target.OutcomeID, 0) = isnull(source.OutcomeID, 0)
		and isnull(target.TreatmentID, 0) = isnull(source.TreatmentID, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	
	then 
		update 
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.DateOfBirth = source.DateOfBirth
			,target.NHSNumber = source.NHSNumber
			,target.CasenoteNumber = source.CasenoteNumber
			,target.PatientName = source.PatientName
			,target.AttemptNumber = source.AttemptNumber
			,target.CommissionerCode = source.CommissionerCode
			,target.TreatmentDate = source.TreatmentDate
			,target.OutcomeID = source.OutcomeID
			,target.TreatmentID = source.TreatmentID
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Created = source.Created
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


﻿CREATE procedure [ETL].[BuildCenCOMBaseWardStay]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	COM.BaseWardStay target
using
	(
	select
		 EncounterRecno
		,SourceUniqueID
		,SourceSpellNo
		,SourceEncounterNo
		,ProviderSpellNo
		,AdmissionTime
		,DischargeTime
		,AdmissionDate
		,DischargeDate
		,StartTime
		,EndTime
		,StartDate
		,EndDate
		,PatientSourceUniqueID
		,PatientSourceSystemUniqueID
		,PatientNHSNumber
		,PatientNHSNumberStatusIndicator
		,PatientTitleID
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,PatientPostcode
		,PatientDateOfBirth
		,PatientDateOfDeath
		,PatientSexID
		,PatientEthnicGroupID
		,SourceServicePointID
		,SourceAdmissionMethodID
		,SourceAdmissionSourceID
		,SourcePatientClassificationID
		,SourceAdminCategoryID
		,SourceIntendedManagementID
		,SourceReasonForAdmissionID
		,ExpectedDischargeDate
		,SourceDischargeDestinationID
		,SourceDischargeMethodID
		,SpellProfessionalCarer
		,SpellSpecialty
		,WardStayPracticeCode
		,WardStayGPCode
		,WardStayPCTCode
		,CreatedTime
		,ModifiedTime
		,CreatedByID
		,ModifiedByID
		,ArchiveFlag
		,HealthOrgOwner
		,InterfaceCode
		,RegisteredPracticeID
		,LOS
		,AgeCode
		,ContextCode
		,Created
		,Updated
		,ByWhom
	from
		ETL.TLoadCenCOMBaseWardStay WardStay
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno

	when not matched by source
	and	target.ContextCode = 'CEN||IPM'
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,SourceSpellNo
			,SourceEncounterNo
			,ProviderSpellNo
			,AdmissionTime
			,DischargeTime
			,AdmissionDate
			,DischargeDate
			,StartTime
			,EndTime
			,StartDate
			,EndDate
			,PatientSourceUniqueID
			,PatientSourceSystemUniqueID
			,PatientNHSNumber
			,PatientNHSNumberStatusIndicator
			,PatientTitleID
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,PatientPostcode
			,PatientDateOfBirth
			,PatientDateOfDeath
			,PatientSexID
			,PatientEthnicGroupID
			,SourceServicePointID
			,SourceAdmissionMethodID
			,SourceAdmissionSourceID
			,SourcePatientClassificationID
			,SourceAdminCategoryID
			,SourceIntendedManagementID
			,SourceReasonForAdmissionID
			,ExpectedDischargeDate
			,SourceDischargeDestinationID
			,SourceDischargeMethodID
			,SpellProfessionalCarer
			,SpellSpecialty
			,WardStayPracticeCode
			,WardStayGPCode
			,WardStayPCTCode
			,CreatedTime
			,ModifiedTime
			,CreatedByID
			,ModifiedByID
			,ArchiveFlag
			,HealthOrgOwner
			,InterfaceCode
			,RegisteredPracticeID
			,LOS
			,AgeCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.EncounterRecno
			,source.SourceUniqueID
			,source.SourceSpellNo
			,source.SourceEncounterNo
			,source.ProviderSpellNo
			,source.AdmissionTime
			,source.DischargeTime
			,source.AdmissionDate
			,source.DischargeDate
			,source.StartTime
			,source.EndTime
			,source.StartDate
			,source.EndDate
			,source.PatientSourceUniqueID
			,source.PatientSourceSystemUniqueID
			,source.PatientNHSNumber
			,source.PatientNHSNumberStatusIndicator
			,source.PatientTitleID
			,source.PatientForename
			,source.PatientSurname
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.PatientPostcode
			,source.PatientDateOfBirth
			,source.PatientDateOfDeath
			,source.PatientSexID
			,source.PatientEthnicGroupID
			,source.SourceServicePointID
			,source.SourceAdmissionMethodID
			,source.SourceAdmissionSourceID
			,source.SourcePatientClassificationID
			,source.SourceAdminCategoryID
			,source.SourceIntendedManagementID
			,source.SourceReasonForAdmissionID
			,source.ExpectedDischargeDate
			,source.SourceDischargeDestinationID
			,source.SourceDischargeMethodID
			,source.SpellProfessionalCarer
			,source.SpellSpecialty
			,source.WardStayPracticeCode
			,source.WardStayGPCode
			,source.WardStayPCTCode
			,source.CreatedTime
			,source.ModifiedTime
			,source.CreatedByID
			,source.ModifiedByID
			,source.ArchiveFlag
			,source.HealthOrgOwner
			,source.InterfaceCode
			,source.RegisteredPracticeID
			,source.LOS
			,source.AgeCode
			,source.ContextCode
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.SourceSpellNo = source.SourceSpellNo
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.ProviderSpellNo = source.ProviderSpellNo
			,target.AdmissionTime = source.AdmissionTime
			,target.DischargeTime = source.DischargeTime
			,target.AdmissionDate = source.AdmissionDate
			,target.DischargeDate = source.DischargeDate
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.StartDate = source.StartDate
			,target.EndDate = source.EndDate
			,target.PatientSourceUniqueID = source.PatientSourceUniqueID
			,target.PatientSourceSystemUniqueID = source.PatientSourceSystemUniqueID
			,target.PatientNHSNumber = source.PatientNHSNumber
			,target.PatientNHSNumberStatusIndicator = source.PatientNHSNumberStatusIndicator
			,target.PatientTitleID = source.PatientTitleID
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.PatientPostcode = source.PatientPostcode
			,target.PatientDateOfBirth = source.PatientDateOfBirth
			,target.PatientDateOfDeath = source.PatientDateOfDeath
			,target.PatientSexID = source.PatientSexID
			,target.PatientEthnicGroupID = source.PatientEthnicGroupID
			,target.SourceServicePointID = source.SourceServicePointID
			,target.SourceAdmissionMethodID = source.SourceAdmissionMethodID
			,target.SourceAdmissionSourceID = source.SourceAdmissionSourceID
			,target.SourcePatientClassificationID = source.SourcePatientClassificationID
			,target.SourceAdminCategoryID = source.SourceAdminCategoryID
			,target.SourceIntendedManagementID = source.SourceIntendedManagementID
			,target.SourceReasonForAdmissionID = source.SourceReasonForAdmissionID
			,target.ExpectedDischargeDate = source.ExpectedDischargeDate
			,target.SourceDischargeDestinationID = source.SourceDischargeDestinationID
			,target.SourceDischargeMethodID = source.SourceDischargeMethodID
			,target.SpellProfessionalCarer = source.SpellProfessionalCarer
			,target.SpellSpecialty = source.SpellSpecialty
			,target.WardStayPracticeCode = source.WardStayPracticeCode
			,target.WardStayGPCode = source.WardStayGPCode
			,target.WardStayPCTCode = source.WardStayPCTCode
			,target.CreatedTime = source.CreatedTime
			,target.ModifiedTime = source.ModifiedTime
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.ArchiveFlag = source.ArchiveFlag
			,target.HealthOrgOwner = source.HealthOrgOwner
			,target.InterfaceCode = source.InterfaceCode
			,target.RegisteredPracticeID = source.RegisteredPracticeID
			,target.LOS = source.LOS
			,target.AgeCode = source.AgeCode
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

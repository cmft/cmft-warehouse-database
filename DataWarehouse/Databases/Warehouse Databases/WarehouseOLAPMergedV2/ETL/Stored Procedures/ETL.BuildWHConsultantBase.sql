﻿


CREATE procedure [ETL].[BuildWHConsultantBase] as

--CH 20102015 result specialty brought through

truncate table WH.ConsultantBase

insert into WH.ConsultantBase

(
	 ConsultantID
	,ConsultantCode
	,Consultant
	,ProviderCode
	,MainSourceSpecialtyCode
	,MainSourceSpecialty
	,MainSpecialtyCode
	,MainSpecialty
	,Title
	,Initials
	,Surname
	,ContextCode
)

select
	 ConsultantID
	,ConsultantCode
	,Consultant
	,ProviderCode
	,MainSourceSpecialtyCode = SourceSpecialtyCode
	,MainSourceSpecialty = SourceSpecialty
	,MainSpecialtyCode = NationalSpecialtyCode
	,MainSpecialty = NationalSpecialty
	,Title
	,Initials
	,Surname
	,ContextCode = Consultant.SourceContextCode
from
	(
	select 
		 ConsultantID = Consultant.SourceValueID
		,ConsultantCode = Consultant.NationalValueCode
		,Consultant= Consultant.SourceValue

		,ProviderCode = 
			coalesce(
				 CentralConsultant.ProviderCode
				,case when Consultant.SourceContextCode = 'TRA||UG' then 'RW3TR' else 'N/A' end
			)
		,MainSpecialtyCode =
			coalesce(
				 cast(CentralConsultant.MainSpecialtyCode as varchar)
				,cast(TraffordConsultant.MainSpecialtyCode as varchar)
				,cast(ResultConsultant.NationalSpecialtyCode as varchar)
				,cast(SCRConsultant.SpecialtyCode as varchar)
				,'-1'
			)

		,Title =
			LEFT(
				coalesce(
					 CentralConsultant.Title
					,SCRConsultant.Title
				)
				,6
			)

		,Initials = CentralConsultant.Initials

		,Surname =
			LEFT(
				coalesce(
					 CentralConsultant.Surname
					,TraffordConsultant.surname
					,Consultant.SourceValue
					,'Not Specified'
				)
				,50
			)

		,SourceContextCode = Consultant.SourceContextCode

	from
		WH.Member Consultant

	left join [$(Warehouse)].PAS.Consultant CentralConsultant
	on	CentralConsultant.ConsultantCode = Consultant.SourceValueCode
	and	Consultant.SourceContextCode = 'CEN||PAS'

	left join [$(Warehouse)].PAS.Provider CentralProvider
	on	CentralProvider.ProviderCode = CentralConsultant.ProviderCode

	left join [$(Warehouse)].SCR.Consultant SCRConsultant
	on	SCRConsultant.ConsultantCode = Consultant.SourceValueCode
	and	Consultant.SourceContextCode = 'CEN||SCR'

	left join [$(TraffordReferenceData)].dbo.INFODEPT_cab_consultants TraffordConsultant
	on	TraffordConsultant.sds_code = Consultant.SourceValueCode
	and	Consultant.SourceContextCode = 'TRA||UG'

	left join [$(Warehouse)].Result.CENICEClinician ResultConsultant
	on	cast(ResultConsultant.ClinicianID as varchar) = Consultant.SourceValueCode
	and	Consultant.SourceContextCode = 'CEN||ICE'

	where
		Consultant.AttributeCode = 'CONSUL'
	) Consultant

left join WH.Specialty
on	Specialty.SourceSpecialtyCode = Consultant.MainSpecialtyCode
and	Specialty.SourceContextCode = Consultant.SourceContextCode
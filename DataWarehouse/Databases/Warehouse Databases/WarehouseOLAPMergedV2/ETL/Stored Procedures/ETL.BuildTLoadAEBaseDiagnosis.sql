﻿
CREATE procedure [ETL].[BuildTLoadAEBaseDiagnosis] @LastLoadTime datetime as

declare @ReturnValue int = 0


BEGIN TRY

	truncate table ETL.TLoadAEBaseDiagnosis

	insert
	into
		ETL.TLoadAEBaseDiagnosis
	(
		 MergeEncounterRecno
		,SequenceNo
		,DiagnosticSchemeCode
		,DiagnosisCode
		,DiagnosisSiteCode
		,DiagnosisSideCode
		,ContextCode
		,Action
		,DiagnosisRecno
	)
	select
		 *
	from
		(
		select
			 MergeEncounterRecno
			,SequenceNo = cast(SequenceNo as smallint)
			,DiagnosticSchemeCode = cast(DiagnosticSchemeCode as varchar(10))
			,DiagnosisCode = cast(DiagnosisCode as varchar(50))
			,DiagnosisSiteCode = cast(DiagnosisSiteCode as varchar(10))
			,DiagnosisSideCode = cast(DiagnosisSideCode as varchar(10))
			,ContextCode
			,Action
			,DiagnosisRecno
		from
			(
	--Central
			select
				 BaseEncounter.MergeEncounterRecno
				,Diagnosis.SequenceNo
				,DiagnosticSchemeCode = Diagnosis.DiagnosticSchemeInUse
				,Diagnosis.DiagnosisCode
				,Diagnosis.DiagnosisSiteCode
				,Diagnosis.DiagnosisSideCode
				,BaseEncounter.ContextCode
				,Action = case when Diagnosis.Updated is null then 'INSERT' else 'UPDATE' end
				,DiagnosisRecno
			from
				[$(Warehouse)].AE.Diagnosis

	--this assumes that SourceUniqueID is unique across contexts! as it happens, diagnoses only exist against 'CEN||SYM' so this is okay
			inner join AE.BaseEncounter
			on	BaseEncounter.SourceUniqueID = Diagnosis.AESourceUniqueID
			and	BaseEncounter.ContextCode in
				(
				 'CEN||SYM'
				--,'CEN||ADAS'
				--,'CEN||PAS'
				--,'TRA||ADAS'
				)

			where
				isnull(Diagnosis.Updated, Diagnosis.Created) > @LastLoadTime

			union all

	--Trafford
			select
				 BaseEncounter.MergeEncounterRecno
				,Diagnosis.SequenceNo
				,DiagnosticSchemeCode = Diagnosis.DiagnosticSchemeInUse
				,Diagnosis.DiagnosisCode
				,DiagnosisSiteCode = null -- not CDS mapped in Trafford Symphony
				,DiagnosisSideCode = null -- not CDS mapped in Trafford Symphony
				,BaseEncounter.ContextCode
				,Action = case when Diagnosis.Updated is null then 'INSERT' else 'UPDATE' end
				,Diagnosis.DiagnosisRecno
			from
				[$(TraffordWarehouse)].dbo.AEDiagnosis Diagnosis

			inner join AE.BaseEncounter
			on	BaseEncounter.SourceUniqueID = Diagnosis.AESourceUniqueID
			and	BaseEncounter.ContextCode in
				(
				 'TRA||SYM'
				)

			where
				isnull(Diagnosis.Updated, Diagnosis.Created) > @LastLoadTime


			) Encounter
		) Encounter


--now pick up deletes
	insert
	into
		ETL.TLoadAEBaseDiagnosis
	(
		DiagnosisRecno
		,ContextCode
		,Action
	)
	select
		Activity.DiagnosisRecno
		,Activity.ContextCode
		,Action = 'DELETE'
	from
		AE.BaseDiagnosis Activity
	where
		Activity.ContextCode in
			(
			'CEN||ADAS'
			,'CEN||PAS'
			,'CEN||SYM'
			)

	and	not exists
		(
		select
			1
		from
			[$(Warehouse)].AE.Diagnosis Encounter
		where
			Encounter.DiagnosisRecno = Activity.DiagnosisRecno
		)
		
	union all
	
	select
		Activity.DiagnosisRecno
		,Activity.ContextCode
		,Action = 'DELETE'
	from
		AE.BaseDiagnosis Activity
	where
		Activity.ContextCode in
			(
			'TRA||ADAS'
			,'TRA||SYM'
			)
	and	not exists
		(
		select
			1
		from
			[$(TraffordWarehouse)].dbo.AEDiagnosis Encounter
		where
			Encounter.DiagnosisRecno = Activity.DiagnosisRecno
		)


END TRY

BEGIN
    CATCH
	
	set @ReturnValue = @@ERROR

    DECLARE @ErrorSeverity INT,
            @ErrorNumber   INT,
            @ErrorMessage nvarchar(4000),
            @ErrorState INT,
            @ErrorLine  INT,
            @ErrorProc nvarchar(200)
            -- Grab error information from SQL functions
    SET @ErrorSeverity = ERROR_SEVERITY()
    SET @ErrorNumber   = ERROR_NUMBER()
    SET @ErrorMessage  = ERROR_MESSAGE()
    SET @ErrorState    = ERROR_STATE()
    SET @ErrorLine     = ERROR_LINE()
    SET @ErrorProc     = ERROR_PROCEDURE()
    SET @ErrorMessage  = 'An error occurred.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
    -- Not all errors generate an error state, to set to 1 if it's zero
    IF @ErrorState  = 0
    SET @ErrorState = 1
    -- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
    IF @@TRANCOUNT > 0
    BEGIN
            --print 'Rollback transaction'
            ROLLBACK TRANSACTION
    END
    RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
END CATCH


RETURN @ReturnValue




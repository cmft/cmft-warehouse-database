﻿create procedure [ETL].[ImportCenTheatreBaseTimetableTemplate] as

--import the data
exec ETL.BuildCenTheatreBaseTimetableTemplate


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Theatre].[BaseTimetableTemplate]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildTheatreBaseTimetableTemplateReference 'CEN||ORMIS'



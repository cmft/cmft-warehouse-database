﻿CREATE procedure [ETL].[ImportCenCOMBaseCaseload] as


--import the data
exec ETL.BuildCenCOMBaseCaseload


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[COM].[BaseCaseload]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildCOMBaseCaseloadReference 'CEN||IPM'

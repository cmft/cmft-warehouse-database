﻿CREATE procedure [ETL].[ImportCenMaternityBaseAntenatalCare] as

--import the data
exec ETL.BuildCenMaternityBaseAntenatalCare


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Maternity].[BaseAntnatalCare]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildMaternityBaseAntenatalCareReference 'CEN||SMMIS'


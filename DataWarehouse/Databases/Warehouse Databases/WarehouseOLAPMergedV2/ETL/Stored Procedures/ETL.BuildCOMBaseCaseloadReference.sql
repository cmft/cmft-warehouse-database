﻿CREATE procedure [ETL].[BuildCOMBaseCaseloadReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				COM.BaseCaseloadReference
			)
			,'1 jan 1900'
		)
declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	COM.BaseCaseloadReference
where
	not exists
	(
	select
		1
	from
		COM.BaseCaseload
	where
		BaseCaseload.MergeEncounterRecno = BaseCaseloadReference.MergeEncounterRecno
	)
and	ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	COM.BaseCaseloadReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,SpecialtyID = Specialty.SourceValueID
		,ProfessionalCarerID = ProfessionalCarer.SourceValueID
		,StaffTeamID = StaffTeam.SourceValueID
		,CreatedByID = CreatedBy.SourceValueID
		,ModifiedByID = ModifiedBy.SourceValueID

		,AllocationDateID =
			coalesce(
				 AllocationDate.DateID

				,case
				when Encounter.AllocationDate is null
				then NullDate.DateID

				when Encounter.AllocationDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,DischargeDateID =
			coalesce(
				 DischargeDate.DateID

				,case
				when Encounter.DischargeDate is null
				then NullDate.DateID

				when Encounter.DischargeDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,SexID = Sex.SourceSexID
		,EthnicCategoryID = EthnicCategory.SourceEthnicCategoryID
		,AgeID = Age.AgeID
		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		COM.BaseCaseload Encounter

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	inner join COM.Specialty Specialty
	on	Specialty.SpecialtyID = coalesce(Encounter.AllocationSpecialtyID, '-1')
	and	Specialty.ContextCode = Encounter.ContextCode

	inner join COM.ProfessionalCarer ProfessionalCarer
	on	ProfessionalCarer.ProfessionalCarerID = coalesce(Encounter.AllocationProfessionalCarerID, '-1')
	and	ProfessionalCarer.ContextCode = Encounter.ContextCode

	inner join COM.StaffTeam StaffTeam
	on	StaffTeam.StaffTeamID = coalesce(Encounter.AllocationStaffTeamID, '-1')
	and	StaffTeam.ContextCode = Encounter.ContextCode

	inner join COM.SystemUser CreatedBy
	on	CreatedBy.SystemUserID = Encounter.CreatedByID
	and	CreatedBy.ContextCode = Encounter.ContextCode

	inner join COM.SystemUser ModifiedBy
	on	ModifiedBy.SystemUserID = Encounter.ModifiedByID
	and	ModifiedBy.ContextCode = Encounter.ContextCode

	left join WH.Calendar AllocationDate
	on	AllocationDate.TheDate = Encounter.AllocationDate

	left join WH.Calendar DischargeDate
	on	DischargeDate.TheDate = Encounter.DischargeDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	inner join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.PatientSexID, -1) as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	inner join WH.EthnicCategory EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryCode = cast(coalesce(Encounter.PatientEthnicGroupID, -1) as varchar)
	and	EthnicCategory.SourceContextCode = Encounter.ContextCode

	inner join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,SpecialtyID
			,ProfessionalCarerID
			,StaffTeamID
			,CreatedByID
			,ModifiedByID
			,AllocationDateID
			,DischargeDateID
			,SexID
			,EthnicCategoryID
			,AgeID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.SpecialtyID
			,source.ProfessionalCarerID
			,source.StaffTeamID
			,source.CreatedByID
			,source.ModifiedByID
			,source.AllocationDateID
			,source.DischargeDateID
			,source.SexID
			,source.EthnicCategoryID
			,source.AgeID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.SpecialtyID = source.SpecialtyID
			,target.ProfessionalCarerID = source.ProfessionalCarerID
			,target.StaffTeamID = source.StaffTeamID
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.AllocationDateID = source.AllocationDateID
			,target.DischargeDateID = source.DischargeDateID
			,target.SexID = source.SexID
			,target.EthnicCategoryID = source.EthnicCategoryID
			,target.AgeID = source.AgeID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿
CREATE procedure [ETL].[BuildTLoadResultBaseOrderResult] as

truncate table ETL.TLoadResultBaseOrderResult


insert
into
	ETL.TLoadResultBaseOrderResult
(
	OrderResultRecno
	,ContextCode
	,OrderSourceUniqueID
	,OrderRequestTime
	,ResultSourceUniqueID
	,InterfaceCode
)

select
	OrderResultRecno
	,ContextCode
	,OrderSourceUniqueID
	,OrderRequestTime
	,ResultSourceUniqueID
	,InterfaceCode
from
	ETL.TImportCenResultBaseCENICEOrderResult


﻿CREATE procedure [ETL].[LoadOPBaseDiaryReference]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	OP.BaseDiaryReference target
using
	(
	select
		 Encounter.MergeRecno
		,ContextID = Context.ContextID
		,CensusDateID = CensusDate.DateID
		,ClinicID = Clinic.SourceClinicID
		,DoctorID = Doctor.SourceDoctorID

		,SessionStartDateID =
			coalesce(
				 SessionStartDate.DateID

				,case
				when Encounter.SessionStartTime is null
				then NullDate.DateID

				when Encounter.SessionStartTime < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
			
		,SessionStartTimeID = 
			isnull(
				cast(
					datepart(hour, Encounter.SessionStartTime)
					as int
				) * 60
				+
				cast(
					datepart(minute, Encounter.SessionStartTime)
					as int
				)
				,-1
			)

		,SessionEndDateID =
			coalesce(
				 SessionEndDate.DateID

				,case
				when Encounter.SessionEndTime is null
				then NullDate.DateID

				when Encounter.SessionEndTime < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
			
		,SessionEndTimeID = 
			isnull(
				cast(
					datepart(hour, Encounter.SessionEndTime)
					as int
				) * 60
				+
				cast(
					datepart(minute, Encounter.SessionEndTime)
					as int
				)
				,-1
			)
	from
		OP.BaseDiary Encounter

	left join WH.Calendar CensusDate
	on	CensusDate.TheDate = Encounter.CensusDate

	left join OP.Clinic Clinic
	on	Clinic.SourceClinicCode = cast(isnull(Encounter.ClinicCode, '-1') as varchar)
	and	Clinic.SourceContextCode = Encounter.ContextCode

	left join OP.Doctor Doctor
	on	Doctor.SourceDoctorCode = cast(isnull(Encounter.DoctorCode, '-1') as varchar)
	and	Doctor.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar SessionStartDate
	on	SessionStartDate.TheDate = cast(Encounter.SessionStartTime as date)

	left join WH.Calendar SessionEndDate
	on	SessionEndDate.TheDate = cast(Encounter.SessionEndTime as date)

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'
	
	left join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			 MergeRecno
			,ContextID
			,CensusDateID
			,ClinicID
			,DoctorID
			,SessionStartDateID
			,SessionStartTimeID
			,SessionEndDateID
			,SessionEndTimeID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.CensusDateID
			,source.ClinicID
			,source.DoctorID
			,source.SessionStartDateID
			,source.SessionStartTimeID
			,source.SessionEndDateID
			,source.SessionEndTimeID
			)

	when matched
	and	checksum(
			 source.ContextID
			,source.CensusDateID
			,source.ClinicID
			,source.DoctorID
			,source.SessionStartDateID
			,source.SessionStartTimeID
			,source.SessionEndDateID
			,source.SessionEndTimeID
		)
		 <>
		checksum(
			 target.ContextID
			,target.CensusDateID
			,target.ClinicID
			,target.DoctorID
			,target.SessionStartDateID
			,target.SessionStartTimeID
			,target.SessionEndDateID
			,target.SessionEndTimeID
		)

		
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.CensusDateID = source.CensusDateID
			,target.ClinicID = source.ClinicID
			,target.DoctorID = source.DoctorID
			,target.SessionStartDateID = source.SessionStartDateID
			,target.SessionStartTimeID = source.SessionStartTimeID
			,target.SessionEndDateID = source.SessionEndDateID
			,target.SessionEndTimeID = source.SessionEndTimeID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿
CREATE procedure [ETL].[BuildTLoadAEBaseTreatment] @LastLoadTime datetime as

declare @ReturnValue int = 0


BEGIN TRY

	truncate table ETL.TLoadAEBaseTreatment

	insert
	into
		ETL.TLoadAEBaseTreatment
	(
		 MergeEncounterRecno
		,SequenceNo
		,TreatmentSchemeCode
		,TreatmentCode
		,TreatmentTime
		,ContextCode
		,Action
		,TreatmentRecno
	)
	select
		 MergeEncounterRecno
		,SequenceNo = cast(SequenceNo as smallint)
		,TreatmentSchemeCode = cast(TreatmentSchemeCode as varchar(10))
		,TreatmentCode = cast(TreatmentCode as varchar(50))
		,TreatmentTime = cast(TreatmentTime as smalldatetime)
		,ContextCode
		,Action
		,TreatmentRecno
	from
		(
	--Central
		select
			 BaseEncounter.MergeEncounterRecno
			,Treatment.SequenceNo
			,TreatmentSchemeCode = Treatment.ProcedureSchemeInUse
			,TreatmentCode = Treatment.ProcedureCode
			,TreatmentTime = Treatment.ProcedureDate
			,BaseEncounter.ContextCode
			,Action = case when Treatment.Updated is null then 'INSERT' else 'UPDATE' end
			,TreatmentRecno = Treatment.ProcedureRecno
		from
			[$(Warehouse)].AE.[Procedure] Treatment

	--this assumes that SourceUniqueID is unique across contexts! as it happens, treatments only exist against 'CEN||SYM' so this is okay
		inner join AE.BaseEncounter
		on	BaseEncounter.SourceUniqueID = Treatment.AESourceUniqueID
		and	BaseEncounter.ContextCode in
			(
			 'CEN||SYM'
			--,'CEN||ADAS'
			--,'CEN||PAS'
			--,'TRA||ADAS'
			)

		where
			isnull(Treatment.Updated, Treatment.Created) > @LastLoadTime

		union all

	--Trafford
		select
			 BaseEncounter.MergeEncounterRecno
			,Treatment.SequenceNo
			,TreatmentSchemeCode = Treatment.ProcedureSchemeInUse
			,TreatmentCode = Treatment.ProcedureCode
			,TreatmentTime = Treatment.ProcedureDate
			,BaseEncounter.ContextCode
			,Action = case when Treatment.Updated is null then 'INSERT' else 'UPDATE' end
			,TreatmentRecno = Treatment.ProcedureRecno
		from
			[$(TraffordWarehouse)].dbo.AEProcedure Treatment

		inner join AE.BaseEncounter
		on	BaseEncounter.SourceUniqueID = Treatment.AESourceUniqueID
		and	BaseEncounter.ContextCode in
			(
			 'TRA||SYM'
			)

		where
			isnull(Treatment.Updated, Treatment.Created) > @LastLoadTime


		) Encounter


--now pick up deletes
	insert
	into
		ETL.TLoadAEBaseTreatment
	(
		TreatmentRecno
		,ContextCode
		,Action
	)
	select
		Activity.TreatmentRecno
		,Activity.ContextCode
		,Action = 'DELETE'
	from
		AE.BaseTreatment Activity
	where
		Activity.ContextCode in
			(
			'CEN||ADAS'
			,'CEN||PAS'
			,'CEN||SYM'
			)

	and	not exists
		(
		select
			1
		from
			[$(Warehouse)].AE.[Procedure] Encounter
		where
			Encounter.ProcedureRecno = Activity.TreatmentRecno
		)
		
	union all
	
	select
		Activity.TreatmentRecno
		,Activity.ContextCode
		,Action = 'DELETE'
	from
		AE.BaseTreatment Activity
	where
		Activity.ContextCode in
			(
			'TRA||ADAS'
			,'TRA||SYM'
			)
	and	not exists
		(
		select
			1
		from
			[$(TraffordWarehouse)].dbo.AEProcedure Encounter
		where
			Encounter.ProcedureRecno = Activity.TreatmentRecno
		)


END TRY

BEGIN
    CATCH
	
	set @ReturnValue = @@ERROR

    DECLARE @ErrorSeverity INT,
            @ErrorNumber   INT,
            @ErrorMessage nvarchar(4000),
            @ErrorState INT,
            @ErrorLine  INT,
            @ErrorProc nvarchar(200)
            -- Grab error information from SQL functions
    SET @ErrorSeverity = ERROR_SEVERITY()
    SET @ErrorNumber   = ERROR_NUMBER()
    SET @ErrorMessage  = ERROR_MESSAGE()
    SET @ErrorState    = ERROR_STATE()
    SET @ErrorLine     = ERROR_LINE()
    SET @ErrorProc     = ERROR_PROCEDURE()
    SET @ErrorMessage  = 'An error occurred.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
    -- Not all errors generate an error state, to set to 1 if it's zero
    IF @ErrorState  = 0
    SET @ErrorState = 1
    -- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
    IF @@TRANCOUNT > 0
    BEGIN
            --print 'Rollback transaction'
            ROLLBACK TRANSACTION
    END
    RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
END CATCH


RETURN @ReturnValue




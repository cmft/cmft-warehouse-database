﻿


CREATE procedure [ETL].[BuildIUIBaseEncounterReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				IUI.BaseEncounterReference
			where
				BaseEncounterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	IUI.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		IUI.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
	)
and	BaseEncounterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	IUI.BaseEncounterReference target
using
	(
	select
		BaseEncounter.MergeEncounterRecno
		,ContextID = Context.ContextID
		,TreatmentDateID =
			coalesce(
				 TreatmentDate.DateID

				,case
				when BaseEncounter.TreatmentDate is null
				then NullDate.DateID

				when BaseEncounter.TreatmentDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		
		,OutcomeID = Outcome.SourceOutcomeID	
		,TreatmentID = Treatment.SourceValueID		
		,BaseEncounter.Created
		,BaseEncounter.Updated
		,BaseEncounter.ByWhom
	from
		IUI.BaseEncounter

	inner join WH.Context
	on	Context.ContextCode = BaseEncounter.ContextCode

	inner join WH.Outcome
	on	Outcome.SourceOutcomeCode = coalesce(BaseEncounter.OutcomeID, '-1')
	and	Outcome.SourceContextCode = BaseEncounter.ContextCode

	inner join WH.Treatment
	on	Treatment.TreatmentCode = coalesce(BaseEncounter.TreatmentID, '-1')
	and	Treatment.ContextCode = BaseEncounter.ContextCode

	left join WH.Calendar TreatmentDate
	on	TreatmentDate.TheDate = BaseEncounter.TreatmentDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseEncounter.Updated > @LastUpdated
	and	BaseEncounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			MergeEncounterRecno
			,ContextID
			,TreatmentDateID
			,OutcomeID
			,TreatmentID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.TreatmentDateID
			,source.OutcomeID
			,source.TreatmentID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.TreatmentDateID = source.TreatmentDateID
			,target.OutcomeID = source.OutcomeID
			,target.TreatmentID = source.TreatmentID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats







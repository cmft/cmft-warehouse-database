﻿
create procedure [ETL].[ImportPEXBaseResponse]
 as

--import the data
--Hospedia
exec ETL.BuildPEXBedUnitBaseResponse
--CRT
exec ETL.BuildPEXKioskBaseResponse
exec ETL.BuildPEXOnlineBaseResponse
exec ETL.BuildPEXPostcardBaseResponse
exec ETL.BuildPEXTabletBaseResponse
--Envoy
exec ETL.BuildPEXSMSBaseResponse



--Allocate Directorate
--exec Allocation.AllocatePEX
--Not required in this Import should be on its own

declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[PEX].[BaseResponse]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildPEXBaseResponseReference 'CMFT||CRT'
exec ETL.BuildPEXBaseResponseReference 'CMFT||HOSP'
exec ETL.BuildPEXBaseResponseReference 'CMFT||ENVOY'




﻿CREATE procedure [ETL].BuildSCRBaseReferralReference
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				SCR.BaseReferralReference
			where
				BaseReferralReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	SCR.BaseReferralReference
where
	not exists
	(
	select
		1
	from
		SCR.BaseReferral
	where
		BaseReferral.MergeRecno = BaseReferralReference.MergeRecno
	)
and	BaseReferralReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	SCR.BaseReferralReference target
using
	(
	select
		 Referral.MergeRecno
		,ContextID = Context.ContextID

		,SpecialtyID = Specialty.SourceSpecialtyID
		,ReceiptOfReferralDateID = ReceiptOfReferralDate.DateID
		,FirstAppointmentDateID = FirstAppointmentDate.DateID
		,DiagnosisDateID = DiagnosisDate.DateID

		,Referral.Created
		,Referral.Updated
		,Referral.ByWhom
	from
		SCR.BaseReferral Referral

	inner join WH.Context
	on	Context.ContextCode = Referral.ContextCode

	left join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(Referral.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = Referral.ContextCode

	left join WH.Calendar ReceiptOfReferralDate
	on	ReceiptOfReferralDate.TheDate = Referral.ReceiptOfReferralDate

	left join WH.Calendar FirstAppointmentDate
	on	FirstAppointmentDate.TheDate = Referral.FirstAppointmentDate

	left join WH.Calendar DiagnosisDate
	on	DiagnosisDate.TheDate = Referral.DiagnosisDate


	where
		Referral.Updated > @LastUpdated
	and	Referral.ContextCode = @ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			 [MergeRecno]
			,[ContextID]
			,[SpecialtyID]
			,[ReceiptOfReferralDateID]
			,[FirstAppointmentDateID]
			,[DiagnosisDateID]
			,[Created]
			,[Updated]
			,[ByWhom]			
			)
		values
			(
			 source.[MergeRecno]
			,source.[ContextID]
			,source.[SpecialtyID]
			,source.[ReceiptOfReferralDateID]
			,source.[FirstAppointmentDateID]
			,source.[DiagnosisDateID]
			,source.[Created]
			,source.[Updated]
			,source.[ByWhom]			
			)

	when matched
	then
		update
		set
		 target.[MergeRecno] = source.[MergeRecno]
		,target.[ContextID] = source.[ContextID]
		,target.[SpecialtyID] = source.[SpecialtyID]
		,target.[ReceiptOfReferralDateID] = source.[ReceiptOfReferralDateID]
		,target.[FirstAppointmentDateID] = source.[FirstAppointmentDateID]
		,target.[DiagnosisDateID] = source.[DiagnosisDateID]
		,target.[Created] = source.[Created]
		,target.[Updated] = source.[Updated]
		,target.[ByWhom] = source.[ByWhom]
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



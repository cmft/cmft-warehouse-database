﻿
CREATE procedure [ETL].[BuildCenObservationBaseAdmission]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Observation.BaseAdmission target
using
	(
	select
		AdmissionRecno
		,SourceUniqueID
		,CasenoteNumber
		,DateOfBirth
		,SexID
		,Surname
		,Forename
		,AdmissionTime
		,StartSpecialtyID
		,StartLocationID
		,DischargeTime
		,EndSpecialtyID
		,EndLocationID
		,TreatmentOutcomeID
		,DischargeDestinationID
		,Comment
		,CreatedByUserID
		,CreatedTime
		,LastModifiedByUserID
		,LastModifiedTime
		,ContextCode
	from
		ETL.TLoadCenObservationBaseAdmission

	) source
	on	source.AdmissionRecno = target.AdmissionRecno
	and	source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CEN||PTRACK'
	then delete

	when not matched 
	then
		insert
			(
			AdmissionRecno
			,SourceUniqueID
			,CasenoteNumber
			,DateOfBirth
			,SexID
			,Surname
			,Forename
			,AdmissionTime
			,StartSpecialtyID
			,StartLocationID
			,DischargeTime
			,EndSpecialtyID
			,EndLocationID
			,TreatmentOutcomeID
			,DischargeDestinationID
			,Comment
			,CreatedByUserID
			,CreatedTime
			,LastModifiedByUserID
			,LastModifiedTime
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.AdmissionRecno
			,source.SourceUniqueID
			,source.CasenoteNumber
			,source.DateOfBirth
			,source.SexID
			,source.Surname
			,source.Forename
			,source.AdmissionTime
			,source.StartSpecialtyID
			,source.StartLocationID
			,source.DischargeTime
			,source.EndSpecialtyID
			,source.EndLocationID
			,source.TreatmentOutcomeID
			,source.DischargeDestinationID
			,source.Comment
			,source.CreatedByUserID
			,source.CreatedTime
			,source.LastModifiedByUserID
			,source.LastModifiedTime
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched 
	and not
			isnull(target.LastModifiedTime, getdate()) = isnull(source.LastModifiedTime, getdate())
	then
		update 
		set
			 target.AdmissionRecno = source.AdmissionRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SexID = source.SexID
			,target.Surname = source.Surname
			,target.Forename = source.Forename
			,target.AdmissionTime = source.AdmissionTime
			,target.StartSpecialtyID = source.StartSpecialtyID
			,target.StartLocationID = source.StartLocationID
			,target.DischargeTime = source.DischargeTime
			,target.EndSpecialtyID = source.EndSpecialtyID
			,target.EndLocationID = source.EndLocationID
			,target.TreatmentOutcomeID = source.TreatmentOutcomeID
			,target.DischargeDestinationID = source.DischargeDestinationID
			,target.Comment = source.Comment
			,target.CreatedByUserID = source.CreatedByUserID
			,target.CreatedTime = source.CreatedTime
			,target.LastModifiedByUserID = source.LastModifiedByUserID
			,target.LastModifiedTime = source.LastModifiedTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

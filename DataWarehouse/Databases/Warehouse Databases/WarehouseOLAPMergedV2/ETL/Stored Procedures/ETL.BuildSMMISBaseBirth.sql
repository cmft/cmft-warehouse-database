﻿CREATE procedure [ETL].[BuildSMMISBaseBirth]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Maternity.BaseBirth target
using
	(
	select
		  BirthRecno = cast(BirthRecno as int)
		,ContextCode = cast(ContextCode as varchar)
		,MotherNo = cast(MotherNo as nchar)
		,MotherNhsNumber = cast(MotherNhsNumber as nchar)
		,MotherNhsNoValid = cast(MotherNhsNoValid as nchar)
		,CurrentOccurrence = cast(CurrentOccurrence as numeric)
		,MotherMaidenName = cast(MotherMaidenName as nvarchar)
		,MotherCurrentSurname = cast(MotherCurrentSurname as nvarchar)
		,MotherFForenames = cast(MotherFForenames as nvarchar)
		,MotherTitle = cast(MotherTitle as nvarchar)
		,MotherDateOfBirth = cast(MotherDateOfBirth as datetime2)
		,MotherPlaceOfBirth = cast(MotherPlaceOfBirth as nchar)
		,UkEntryDate = cast(UkEntryDate as datetime2)
		,MotherDateApprox = cast(MotherDateApprox as nchar)
		,MotherEthnicGroupPrefix = cast(MotherEthnicGroupPrefix as nchar)
		,MotherEthnicGroupBody = cast(MotherEthnicGroupBody as nchar)
		,MotherEthnicGroupOpcs = cast(MotherEthnicGroupOpcs as nchar)
		,MotherEthnicGroupPas = cast(MotherEthnicGroupPas as nchar)
		,MotherGpSurname = cast(MotherGpSurname as nvarchar)
		,MotherGpSequence = cast(MotherGpSequence as numeric)
		,MotherTraceStatus = cast(MotherTraceStatus as nchar)
		,MotherLocation = cast(MotherLocation as nvarchar)
		,MotherDateOfDeath = cast(MotherDateOfDeath as datetime2)
		,MotherTimeOfDeath = cast(MotherTimeOfDeath as nchar)
		,MotherDeathIndicator = cast(MotherDeathIndicator as nchar)
		,PregnancyPortable = cast(PregnancyPortable as nchar)
		,PregnancyDiskCreateDate = cast(PregnancyDiskCreateDate as datetime2)
		,PregnancyStatus = cast(PregnancyStatus as nchar)
		,PregnancyType = cast(PregnancyType as nchar)
		,PregnancyUnbooked = cast(PregnancyUnbooked as nchar)
		,PregnancyCardNumber = cast(PregnancyCardNumber as numeric)
		,PregnancyCreateDate = cast(PregnancyCreateDate as datetime2)
		,PregnancyCreateTime = cast(PregnancyCreateTime as nchar)
		,MotherReligion = cast(MotherReligion as nchar)
		,OtherReligion = cast(OtherReligion as nvarchar)
		,Marital = cast(Marital as nchar)
		,MSurname = cast(MSurname as nvarchar)
		,PrefName = cast(PrefName as nvarchar)
		,MotherOccupation = cast(MotherOccupation as nvarchar)
		,MainIncome = cast(MainIncome as nvarchar)
		,PrevEmployment = cast(PrevEmployment as nvarchar)
		,MotherTelephoneWork = cast(MotherTelephoneWork as nvarchar)
		,OneParentFam = cast(OneParentFam as nchar)
		,SocialClass = cast(SocialClass as nchar)
		,BkScreen1Ind = cast(BkScreen1Ind as nchar)
		,BkScreen1aInd = cast(BkScreen1aInd as nchar)
		,BkScreen2Ind = cast(BkScreen2Ind as nchar)
		,InvScreen1Ind = cast(InvScreen1Ind as nchar)
		,InvScreen2Ind = cast(InvScreen2Ind as nchar)
		,AnScreen1Ind = cast(AnScreen1Ind as nchar)
		,AnScreen2Ind = cast(AnScreen2Ind as nchar)
		,AnReason = cast(AnReason as nchar)
		,PregnancyAnDate = cast(PregnancyAnDate as datetime2)
		,AnOperation = cast(AnOperation as nchar)
		,PregnancyGpSurname = cast(PregnancyGpSurname as nvarchar)
		,PregnancyGpSequence = cast(PregnancyGpSequence as numeric)
		,TypeOfPatient = cast(TypeOfPatient as nchar)
		,OverseasVisitor = cast(OverseasVisitor as nchar)
		,PregnancyConCode = cast(PregnancyConCode as nchar)
		,PregnancyBookingMidwife = cast(PregnancyBookingMidwife as nvarchar)
		,PregnancyMidwifeTeam = cast(PregnancyMidwifeTeam as nvarchar)
		,ReasonCompletion = cast(ReasonCompletion as nchar)
		,SpecOtherPlace = cast(SpecOtherPlace as nchar)
		,ConfirmedDob = cast(ConfirmedDob as nchar)
		,ReferredBy = cast(ReferredBy as nchar)
		,ArchiveDate = cast(ArchiveDate as datetime2)
		,AnSex = cast(AnSex as nchar)
		,PregnancyBookingData = cast(PregnancyBookingData as nchar)
		,Erdip = cast(Erdip as nchar)
		,HospReferredTo = cast(HospReferredTo as nchar)
		,ReferralDate = cast(ReferralDate as datetime2)
		,ReferralNumber = cast(ReferralNumber as nvarchar)
		,PregnancyLocation = cast(PregnancyLocation as nvarchar)
		,ScunEcno = cast(ScunEcno as nvarchar)
		,AnWardType = cast(AnWardType as nchar)
		,ReferredByOther = cast(ReferredByOther as nvarchar)
		,ReferredByHosp = cast(ReferredByHosp as nchar)
		,PregnancyCommunityType = cast(PregnancyCommunityType as nchar)
		,ReferralGestWks = cast(ReferralGestWks as numeric)
		,ReferralGestDays = cast(ReferralGestDays as numeric)
		,LmpAtReferral = cast(LmpAtReferral as datetime2)
		,EddAtReferral = cast(EddAtReferral as datetime2)
		,BnScreen1Ind = cast(BnScreen1Ind as nchar)
		,NumberInfants = cast(NumberInfants as nchar)
		,OneParent = cast(OneParent as nchar)
		,LabourOnset = cast(LabourOnset as nchar)
		,Augmentation = cast(Augmentation as nchar)
		,OnsetFirstDate = cast(OnsetFirstDate as datetime2)
		,OnsetFirstTime = cast(OnsetFirstTime as nchar)
		,OnsetSecondDate = cast(OnsetSecondDate as datetime2)
		,OnsetSecondTime = cast(OnsetSecondTime as nchar)
		,MethodIndAug = cast(MethodIndAug as nchar)
		,BnScreen2Ind = cast(BnScreen2Ind as nchar)
		,Pyrexia = cast(Pyrexia as nchar)
		,BloodLoss = cast(BloodLoss as nvarchar)
		,Perineum = cast(Perineum as nchar)
		,NonPerinealTears = cast(NonPerinealTears as nchar)
		,Suture = cast(Suture as nchar)
		,ManualRemoval = cast(ManualRemoval as nchar)
		,PlacentaMembranes = cast(PlacentaMembranes as nchar)
		,Membranes = cast(Membranes as nchar)
		,LabCompl = cast(LabCompl as nchar)
		,LabComplText = cast(LabComplText as nvarchar)
		,IntendedFeeding = cast(IntendedFeeding as nchar)
		,LabResearch = cast(LabResearch as nchar)
		,Support = cast(Support as nchar)
		,DScreen1Ind = cast(DScreen1Ind as nchar)
		,Eclampsia = cast(Eclampsia as nchar)
		,Thromboembolism = cast(Thromboembolism as nchar)
		,Erpc = cast(Erpc as nchar)
		,PuerperalPsychosis = cast(PuerperalPsychosis as nchar)
		,Infection = cast(Infection as nchar)
		,DischHbTest = cast(DischHbTest as nchar)
		,DischHaemoglobin = cast(DischHaemoglobin as nchar)
		,AntiD = cast(AntiD as nchar)
		,RubellaVac = cast(RubellaVac as nchar)
		,BloodTrans = cast(BloodTrans as nvarchar)
		,Sterilization = cast(Sterilization as nchar)
		,Contraception = cast(Contraception as nvarchar)
		,DMatCompl = cast(DMatCompl as nvarchar)
		,DMatComplText = cast(DMatComplText as nvarchar)
		,DScreen2Ind = cast(DScreen2Ind as nchar)
		,Appointment = cast(Appointment as nchar)
		,ResearchDisch = cast(ResearchDisch as nchar)
		,AgeAtDel = cast(AgeAtDel as nchar)
		,LabourDuration1stStage = cast(LabourDuration1stStage as nchar)
		,LabourDuration2ndStage = cast(LabourDuration2ndStage as nchar)
		,LabourDurationLabour = cast(LabourDurationLabour as nchar)
		,PnLenStay = cast(PnLenStay as nchar)
		,AnComplicationsIcd = cast(AnComplicationsIcd as nchar)
		,DelComplicationsIcd = cast(DelComplicationsIcd as nchar)
		,PuerpComplicationsIcd = cast(PuerpComplicationsIcd as nchar)
		,MatOperationsIcd = cast(MatOperationsIcd as nchar)
		,TenSmokingStage = cast(TenSmokingStage as nchar)
		,TenCigsPerDay = cast(TenCigsPerDay as numeric)
		,TenPartnerSmoke = cast(TenPartnerSmoke as nchar)
		,DischSmokingStage = cast(DischSmokingStage as nchar)
		,DischCigsPerDay = cast(DischCigsPerDay as numeric)
		,DischPartnerSmoke = cast(DischPartnerSmoke as nchar)
		,RiskDelivery = cast(RiskDelivery as nchar)
		,DischMedication = cast(DischMedication as nchar)
		,DischMedicationText = cast(DischMedicationText as nvarchar)
		,DischPartnerCigsPerDay = cast(DischPartnerCigsPerDay as numeric)
		,TenPartnerCigsPerDay = cast(TenPartnerCigsPerDay as numeric)
		,SkinToSkin = cast(SkinToSkin as nchar)
		,SkinToSkinText = cast(SkinToSkinText as nvarchar)
		,AnalMucosa = cast(AnalMucosa as nchar)
		,TempMaxLabour = cast(TempMaxLabour as numeric)
		,EpisiotomySutured = cast(EpisiotomySutured as nchar)
		,LabourLocation = cast(LabourLocation as nvarchar)
		,BreastFeedInitiated = cast(BreastFeedInitiated as nchar)
		,Date1stBreastFeed = cast(Date1stBreastFeed as datetime2)
		,Time1stBreastFeed = cast(Time1stBreastFeed as nchar)
		,PnLenStayHours = cast(PnLenStayHours as numeric)
		,PnLenStayMinutes = cast(PnLenStayMinutes as numeric)
		,TransBpSystolic = cast(TransBpSystolic as numeric)
		,TransBpDiastolic = cast(TransBpDiastolic as numeric)
		,TransToCommunityDate = cast(TransToCommunityDate as datetime2)
		,TransferringHospNumber = cast(TransferringHospNumber as nchar)
		,CmwTeam = cast(CmwTeam as nvarchar)
		,CmwDefaultMidwife = cast(CmwDefaultMidwife as nvarchar)
		,CmwBackupMidwife = cast(CmwBackupMidwife as nvarchar)
		,CmwSource = cast(CmwSource as nchar)
		,CmwHospitalName = cast(CmwHospitalName as nvarchar)
		,CmwHospitalChar = cast(CmwHospitalChar as nchar)
		,CmwHospitalOrgCode = cast(CmwHospitalOrgCode as nvarchar)
		,TransComments = cast(TransComments as nvarchar)
		,CmwMDischDate = cast(CmwMDischDate as datetime2)
		,CmwMDischCondition = cast(CmwMDischCondition as nvarchar)
		,CpNormalLabour = cast(CpNormalLabour as nchar)
		,CpReasonNotUsed = cast(CpReasonNotUsed as nvarchar)
		,CpOtherReasonNotUsed = cast(CpOtherReasonNotUsed as nvarchar)
		,PnCare = cast(PnCare as nchar)
		,PnCareHosp = cast(PnCareHosp as nchar)
		,PnCareOther = cast(PnCareOther as nvarchar)
		,NonPerinealSuture = cast(NonPerinealSuture as nchar)
		,TimeSutureStarted = cast(TimeSutureStarted as nchar)
		,ReasonSutureDelayed = cast(ReasonSutureDelayed as nvarchar)
		,SterilisationText = cast(SterilisationText as nvarchar)
		,OneToOneCare = cast(OneToOneCare as nchar)
		,FgmReversal = cast(FgmReversal as nchar)
		,FgmReversalStage = cast(FgmReversalStage as nchar)
		,FgmReferralToCpMw = cast(FgmReferralToCpMw as nchar)
		,DelGbsCarrier = cast(DelGbsCarrier as nchar)
		,DischargedToHvDate = cast(DischargedToHvDate as datetime2)
		,DischargedToHvTime = cast(DischargedToHvTime as nchar)
		,PlannedDischargeDate = cast(PlannedDischargeDate as datetime2)
		,PlannedDischargeTime = cast(PlannedDischargeTime as nchar)
		,PnComplications = cast(PnComplications as nvarchar)
		,PnComplicationsText = cast(PnComplicationsText as nvarchar)
		,Occurrence = cast(Occurrence as numeric)
		,BirthOrder = cast(BirthOrder as nchar)
		,InfantNo = cast(InfantNo as nchar)
		,BnScreen3Ind = cast(BnScreen3Ind as nchar)
		,RuptureMethod = cast(RuptureMethod as nchar)
		,RuptureDate = cast(RuptureDate as datetime2)
		,RuptureTime = cast(RuptureTime as nchar)
		,CordProlapse = cast(CordProlapse as nchar)
		,PresentationLabour = cast(PresentationLabour as nchar)
		,ElectronicFh = cast(ElectronicFh as nchar)
		,FetalPhTest = cast(FetalPhTest as nchar)
		,FetalPh = cast(FetalPh as nchar)
		,Meconium = cast(Meconium as nchar)
		,CordBloodGas = cast(CordBloodGas as nchar)
		,TimeOfBirth = cast(TimeOfBirth as nchar)
		,BirthWeight = cast(BirthWeight as nchar)
		,BirthWeightCentile = cast(BirthWeightCentile as nchar)
		,Gestation = cast(Gestation as nchar)
		,GestationByDate = cast(GestationByDate as nchar)
		,GestationByUs = cast(GestationByUs as nchar)
		,GestUsCertain = cast(GestUsCertain as nchar)
		,CongenitalAbnorm = cast(CongenitalAbnorm as nchar)
		,CongenitalDesc = cast(CongenitalDesc as nvarchar)
		,BnScreen4Ind = cast(BnScreen4Ind as nchar)
		,PresentationDeliv = cast(PresentationDeliv as nchar)
		,BreechDiagnosis = cast(BreechDiagnosis as nchar)
		,BirthingPool = cast(BirthingPool as nchar)
		,MethodDelivery = cast(MethodDelivery as nchar)
		,IndicCaesarian = cast(IndicCaesarian as nvarchar)
		,Outcome = cast(Outcome as nchar)
		,PlaceDelivery = cast(PlaceDelivery as nchar)
		,ReasonForChange = cast(ReasonForChange as nchar)
		,ConductDel = cast(ConductDel as nchar)
		,DeliveryTeam = cast(DeliveryTeam as nvarchar)
		,DeliveryMidwife = cast(DeliveryMidwife as nvarchar)
		,SpecPlaceDelivery = cast(SpecPlaceDelivery as nchar)
		,BnScreen5Ind = cast(BnScreen5Ind as nchar)
		,ApgarOne = cast(ApgarOne as nchar)
		,ApgarFive = cast(ApgarFive as nchar)
		,ApgarTen = cast(ApgarTen as nchar)
		,OnsetRespiration = cast(OnsetRespiration as nchar)
		,ResusPressure = cast(ResusPressure as nchar)
		,ResusDrugs = cast(ResusDrugs as nvarchar)
		,ChildDischAddress = cast(ChildDischAddress as nchar)
		,ConsPaed = cast(ConsPaed as nchar)
		,InfCompl = cast(InfCompl as nchar)
		,InfComplText = cast(InfComplText as nvarchar)
		,InfResearch = cast(InfResearch as nchar)
		,MidwifeDelName = cast(MidwifeDelName as nvarchar)
		,MidwifeDelInitials = cast(MidwifeDelInitials as nchar)
		,MidwifeDelStatus = cast(MidwifeDelStatus as nvarchar)
		,ChildNumber = cast(ChildNumber as nchar)
		,IndexPage = cast(IndexPage as nchar)
		,HealthDistrict = cast(HealthDistrict as nchar)
		,TransferType = cast(TransferType as nchar)
		,HealthVisitor = cast(HealthVisitor as nchar)
		,Registration = cast(Registration as nchar)
		,BirthPlace = cast(BirthPlace as nchar)
		,BirthHA = cast(BirthHA as nchar)
		,OPCSBirthArea = cast(OPCSBirthArea as nchar)
		,OPCSResidArea = cast(OPCSResidArea as nchar)
		,BornAt = cast(BornAt as nchar)
		,LAWard = cast(LAWard as nchar)
		,AssessGest = cast(AssessGest as nchar)
		,AgreedGestFrom = cast(AgreedGestFrom as nchar)
		,AgreedGestTo = cast(AgreedGestTo as nchar)
		,HeadCircum = cast(HeadCircum as nchar)
		,HeadCircumCentile = cast(HeadCircumCentile as nchar)
		,Length = cast(Length as nchar)
		,LengthCentile = cast(LengthCentile as nchar)
		,DScreenInd = cast(DScreenInd as nchar)
		,HIP = cast(HIP as nchar)
		,Convulsions = cast(Convulsions as nchar)
		,OtherAbnorm = cast(OtherAbnorm as nchar)
		,Jaundice = cast(Jaundice as nchar)
		,Bilirubin = cast(Bilirubin as nchar)
		,DInfCompl = cast(DInfCompl as nvarchar)
		,DInfComplText = cast(DInfComplText as nvarchar)
		,CongAbsDisch = cast(CongAbsDisch as nchar)
		,DScreen3AInd = cast(DScreen3AInd as nchar)
		,SCBU = cast(SCBU as nchar)
		,TransCare = cast(TransCare as nchar)
		,Cord = cast(Cord as nchar)
		,Guthrie = cast(Guthrie as nchar)
		,GuthrieDate = cast(GuthrieDate as datetime2)
		,Haemoglobinopathy = cast(Haemoglobinopathy as nchar)
		,HaemoglobinopathyDate = cast(HaemoglobinopathyDate as datetime2)
		,Vitamink = cast(Vitamink as nchar)
		,VitaminkDate = cast(VitaminkDate as datetime2)
		,VitaminkText = cast(VitaminkText as nvarchar)
		,InfHbTest = cast(InfHbTest as nchar)
		,InfHaemaglobin = cast(InfHaemaglobin as nchar)
		,InfBloodGroup = cast(InfBloodGroup as nchar)
		,InfCoombs = cast(InfCoombs as nchar)
		,DischargeExamination = cast(DischargeExamination as nchar)
		,Condition = cast(Condition as nchar)
		,DischExamComments = cast(DischExamComments as nvarchar)
		,HighestLoc = cast(HighestLoc as nchar)
		,Condition28Day = cast(Condition28Day as nchar)
		,Resarch28Day = cast(Resarch28Day as nchar)
		,Comments = cast(Comments as nvarchar)
		,Feeding28Day = cast(Feeding28Day as nchar)
		,NnCompl = cast(NnCompl as nchar)
		,NnComplText = cast(NnComplText as nvarchar)
		,DatePlacenta = cast(DatePlacenta as datetime2)
		,TimePlacenta = cast(TimePlacenta as nchar)
		,BirthDurationLabour = cast(BirthDurationLabour as nchar)
		,BirthDuration2ndStage = cast(BirthDuration2ndStage as nchar)
		,BirthDuration3rdStage = cast(BirthDuration3rdStage as nchar)
		,HearingTest = cast(HearingTest as nchar)
		,dateHearing = cast(dateHearing as datetime2)
		,HearingResult = cast(HearingResult as nchar)
		,ActualHospital = cast(ActualHospital as nchar)
		,BirthDuration1stStage = cast(BirthDuration1stStage as nchar)
		,Tab0 = cast(Tab0 as nchar)
		,Tab1 = cast(Tab1 as nchar)
		,Tab2 = cast(Tab2 as nchar)
		,Tab3 = cast(Tab3 as nchar)
		,tab4 = cast(tab4 as nchar)
		,DelHospCode = cast(DelHospCode as nchar)
		,DelHospName = cast(DelHospName as nvarchar)
		,ChildHealthCode = cast(ChildHealthCode as nchar)
		,CordVenousPH = cast(CordVenousPH as numeric)
		,CordVenBaseDeficit = cast(CordVenBaseDeficit as numeric)
		,CordArterialPH = cast(CordArterialPH as numeric)
		,CordArtBaseDeficit = cast(CordArtBaseDeficit as numeric)
		,ThirdStageManagement = cast(ThirdStageManagement as nvarchar)
		,Inf1Complications = cast(Inf1Complications as nchar)
		,Inf2Complications = cast(Inf2Complications as nchar)
		,CreateBaby = cast(CreateBaby as nchar)
		,BnBoxCode = cast(BnBoxCode as nchar)
		,EcvLabour = cast(EcvLabour as nchar)
		,HighestLoc2002 = cast(HighestLoc2002 as nchar)
		,BirthLocation = cast(BirthLocation as nvarchar)
		,DateCsDecided = cast(DateCsDecided as datetime2)
		,TimeCsDecided = cast(TimeCsDecided as nchar)
		,TimeToCaesarian = cast(TimeToCaesarian as nchar)
		,MidwifeKnown = cast(MidwifeKnown as nchar)
		,GestationDays = cast(GestationDays as numeric)
		,GestByDateDays = cast(GestByDateDays as numeric)
		,GestByUsDays = cast(GestByUsDays as numeric)
		,AssessGestDays = cast(AssessGestDays as numeric)
		,AgreedGestFromDays = cast(AgreedGestFromDays as numeric)
		,FollowUpDay = cast(FollowUpDay as numeric)
		,DilationAtCs = cast(DilationAtCs as numeric)
		,TransWeight = cast(TransWeight as numeric)
		,TransFeeding = cast(TransFeeding as nchar)
		,CmwIDischWeight = cast(CmwIDischWeight as numeric)
		,CmwIDischCondition = cast(CmwIDischCondition as nvarchar)
		,PnSex = cast(PnSex as nchar)
		,PnDateOfBirth = cast(PnDateOfBirth as datetime2)
		,PnAgeAtDischarge = cast(PnAgeAtDischarge as numeric)
		,BirthAnDate = cast(BirthAnDate as datetime2)
		,IndicInstrumental = cast(IndicInstrumental as nvarchar)
		,ShoulderDystocia = cast(ShoulderDystocia as nchar)
		,ShoulderInjury = cast(ShoulderInjury as nchar)
		,ShoulderInjuryText = cast(ShoulderInjuryText as nvarchar)
		,CsReasonForDelay = cast(CsReasonForDelay as nchar)
		,Palpable5ths = cast(Palpable5ths as nchar)
		,FetalPosition = cast(FetalPosition as nchar)
		,Station = cast(Station as nchar)
		,Moulding = cast(Moulding as nchar)
		,Caput = cast(Caput as nchar)
		,LiquorColour = cast(LiquorColour as nvarchar)
		,DeliveryOfBaby = cast(DeliveryOfBaby as nchar)
		,CordEntanglement = cast(CordEntanglement as nchar)
		,Position = cast(Position as nchar)
		,NewbornExamination = cast(NewbornExamination as nchar)
		,NewbornExaminationDate = cast(NewbornExaminationDate as datetime2)
		,NewbornExaminationTime = cast(NewbornExaminationTime as nchar)
		,ExaminationCompletedBy = cast(ExaminationCompletedBy as nchar)
		,ExaminationMidwifePin = cast(ExaminationMidwifePin as nvarchar)
		,ExaminationText = cast(ExaminationText as nvarchar)
		,Heart = cast(Heart as nchar)
		,Eyes = cast(Eyes as nchar)
		,Testes = cast(Testes as nchar)
		,BirthMarksBruises = cast(BirthMarksBruises as nchar)
		,BirthMarksBruisesText = cast(BirthMarksBruisesText as nvarchar)
		,NewbornReferral = cast(NewbornReferral as nchar)
		,CongenitalList = cast(CongenitalList as nvarchar)
		,RedBookCompleted = cast(RedBookCompleted as nchar)
		,InfantNhsNumber = cast(InfantNhsNumber as nchar)
		,InfantNhsNoValid = cast(InfantNhsNoValid as nchar)
		,InfantSex = cast(InfantSex as nchar)
		,InfantMaidenName = cast(InfantMaidenName as nvarchar)
		,InfantCurrentSurname = cast(InfantCurrentSurname as nvarchar)
		,InfantForenames = cast(InfantForenames as nvarchar)
		,InfantDateOfBirth = cast(InfantDateOfBirth as datetime2)
		,InfantPlaceOfBirth = cast(InfantPlaceOfBirth as nchar)
		,InfantEthnicGroupPrefix = cast(InfantEthnicGroupPrefix as nchar)
		,InfantEthnicGroupBody = cast(InfantEthnicGroupBody as nchar)
		,InfantEthnicGroupOpcs = cast(InfantEthnicGroupOpcs as nchar)
		,InfantEthnicGroupPas = cast(InfantEthnicGroupPas as nchar)
		,InfantGpSurname = cast(InfantGpSurname as nvarchar)
		,InfantGpSequence = cast(InfantGpSequence as numeric)
		,InfantTraceStatus = cast(InfantTraceStatus as nchar)
		,InfantLocation = cast(InfantLocation as nvarchar)
		,Created = cast(Created as datetime)
		,Updated = cast(Updated as datetime)
		,ByWhom = cast(ByWhom as varchar)
	from ETL.TLoadSMMISBaseBirth
	) source
	on	source.BirthRecno = target.BirthRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 BirthRecno
			,ContextCode
			,MotherNo
			,MotherNhsNumber
			,MotherNhsNoValid
			,CurrentOccurrence
			,MotherMaidenName
			,MotherCurrentSurname
			,MotherFForenames
			,MotherTitle
			,MotherDateOfBirth
			,MotherPlaceOfBirth
			,UkEntryDate
			,MotherDateApprox
			,MotherEthnicGroupPrefix
			,MotherEthnicGroupBody
			,MotherEthnicGroupOpcs
			,MotherEthnicGroupPas
			,MotherGpSurname
			,MotherGpSequence
			,MotherTraceStatus
			,MotherLocation
			,MotherDateOfDeath
			,MotherTimeOfDeath
			,MotherDeathIndicator
			,PregnancyPortable
			,PregnancyDiskCreateDate
			,PregnancyStatus
			,PregnancyType
			,PregnancyUnbooked
			,PregnancyCardNumber
			,PregnancyCreateDate
			,PregnancyCreateTime
			,MotherReligion
			,OtherReligion
			,Marital
			,MSurname
			,PrefName
			,MotherOccupation
			,MainIncome
			,PrevEmployment
			,MotherTelephoneWork
			,OneParentFam
			,SocialClass
			,BkScreen1Ind
			,BkScreen1aInd
			,BkScreen2Ind
			,InvScreen1Ind
			,InvScreen2Ind
			,AnScreen1Ind
			,AnScreen2Ind
			,AnReason
			,PregnancyAnDate
			,AnOperation
			,PregnancyGpSurname
			,PregnancyGpSequence
			,TypeOfPatient
			,OverseasVisitor
			,PregnancyConCode
			,PregnancyBookingMidwife
			,PregnancyMidwifeTeam
			,ReasonCompletion
			,SpecOtherPlace
			,ConfirmedDob
			,ReferredBy
			,ArchiveDate
			,AnSex
			,PregnancyBookingData
			,Erdip
			,HospReferredTo
			,ReferralDate
			,ReferralNumber
			,PregnancyLocation
			,ScunEcno
			,AnWardType
			,ReferredByOther
			,ReferredByHosp
			,PregnancyCommunityType
			,ReferralGestWks
			,ReferralGestDays
			,LmpAtReferral
			,EddAtReferral
			,BnScreen1Ind
			,NumberInfants
			,OneParent
			,LabourOnset
			,Augmentation
			,OnsetFirstDate
			,OnsetFirstTime
			,OnsetSecondDate
			,OnsetSecondTime
			,MethodIndAug
			,BnScreen2Ind
			,Pyrexia
			,BloodLoss
			,Perineum
			,NonPerinealTears
			,Suture
			,ManualRemoval
			,PlacentaMembranes
			,Membranes
			,LabCompl
			,LabComplText
			,IntendedFeeding
			,LabResearch
			,Support
			,DScreen1Ind
			,Eclampsia
			,Thromboembolism
			,Erpc
			,PuerperalPsychosis
			,Infection
			,DischHbTest
			,DischHaemoglobin
			,AntiD
			,RubellaVac
			,BloodTrans
			,Sterilization
			,Contraception
			,DMatCompl
			,DMatComplText
			,DScreen2Ind
			,Appointment
			,ResearchDisch
			,AgeAtDel
			,LabourDuration1stStage
			,LabourDuration2ndStage
			,LabourDurationLabour
			,PnLenStay
			,AnComplicationsIcd
			,DelComplicationsIcd
			,PuerpComplicationsIcd
			,MatOperationsIcd
			,TenSmokingStage
			,TenCigsPerDay
			,TenPartnerSmoke
			,DischSmokingStage
			,DischCigsPerDay
			,DischPartnerSmoke
			,RiskDelivery
			,DischMedication
			,DischMedicationText
			,DischPartnerCigsPerDay
			,TenPartnerCigsPerDay
			,SkinToSkin
			,SkinToSkinText
			,AnalMucosa
			,TempMaxLabour
			,EpisiotomySutured
			,LabourLocation
			,BreastFeedInitiated
			,Date1stBreastFeed
			,Time1stBreastFeed
			,PnLenStayHours
			,PnLenStayMinutes
			,TransBpSystolic
			,TransBpDiastolic
			,TransToCommunityDate
			,TransferringHospNumber
			,CmwTeam
			,CmwDefaultMidwife
			,CmwBackupMidwife
			,CmwSource
			,CmwHospitalName
			,CmwHospitalChar
			,CmwHospitalOrgCode
			,TransComments
			,CmwMDischDate
			,CmwMDischCondition
			,CpNormalLabour
			,CpReasonNotUsed
			,CpOtherReasonNotUsed
			,PnCare
			,PnCareHosp
			,PnCareOther
			,NonPerinealSuture
			,TimeSutureStarted
			,ReasonSutureDelayed
			,SterilisationText
			,OneToOneCare
			,FgmReversal
			,FgmReversalStage
			,FgmReferralToCpMw
			,DelGbsCarrier
			,DischargedToHvDate
			,DischargedToHvTime
			,PlannedDischargeDate
			,PlannedDischargeTime
			,PnComplications
			,PnComplicationsText
			,Occurrence
			,BirthOrder
			,InfantNo
			,BnScreen3Ind
			,RuptureMethod
			,RuptureDate
			,RuptureTime
			,CordProlapse
			,PresentationLabour
			,ElectronicFh
			,FetalPhTest
			,FetalPh
			,Meconium
			,CordBloodGas
			,TimeOfBirth
			,BirthWeight
			,BirthWeightCentile
			,Gestation
			,GestationByDate
			,GestationByUs
			,GestUsCertain
			,CongenitalAbnorm
			,CongenitalDesc
			,BnScreen4Ind
			,PresentationDeliv
			,BreechDiagnosis
			,BirthingPool
			,MethodDelivery
			,IndicCaesarian
			,Outcome
			,PlaceDelivery
			,ReasonForChange
			,ConductDel
			,DeliveryTeam
			,DeliveryMidwife
			,SpecPlaceDelivery
			,BnScreen5Ind
			,ApgarOne
			,ApgarFive
			,ApgarTen
			,OnsetRespiration
			,ResusPressure
			,ResusDrugs
			,ChildDischAddress
			,ConsPaed
			,InfCompl
			,InfComplText
			,InfResearch
			,MidwifeDelName
			,MidwifeDelInitials
			,MidwifeDelStatus
			,ChildNumber
			,IndexPage
			,HealthDistrict
			,TransferType
			,HealthVisitor
			,Registration
			,BirthPlace
			,BirthHA
			,OPCSBirthArea
			,OPCSResidArea
			,BornAt
			,LAWard
			,AssessGest
			,AgreedGestFrom
			,AgreedGestTo
			,HeadCircum
			,HeadCircumCentile
			,Length
			,LengthCentile
			,DScreenInd
			,HIP
			,Convulsions
			,OtherAbnorm
			,Jaundice
			,Bilirubin
			,DInfCompl
			,DInfComplText
			,CongAbsDisch
			,DScreen3AInd
			,SCBU
			,TransCare
			,Cord
			,Guthrie
			,GuthrieDate
			,Haemoglobinopathy
			,HaemoglobinopathyDate
			,Vitamink
			,VitaminkDate
			,VitaminkText
			,InfHbTest
			,InfHaemaglobin
			,InfBloodGroup
			,InfCoombs
			,DischargeExamination
			,Condition
			,DischExamComments
			,HighestLoc
			,Condition28Day
			,Resarch28Day
			,Comments
			,Feeding28Day
			,NnCompl
			,NnComplText
			,DatePlacenta
			,TimePlacenta
			,BirthDurationLabour
			,BirthDuration2ndStage
			,BirthDuration3rdStage
			,HearingTest
			,dateHearing
			,HearingResult
			,ActualHospital
			,BirthDuration1stStage
			,Tab0
			,Tab1
			,Tab2
			,Tab3
			,tab4
			,DelHospCode
			,DelHospName
			,ChildHealthCode
			,CordVenousPH
			,CordVenBaseDeficit
			,CordArterialPH
			,CordArtBaseDeficit
			,ThirdStageManagement
			,Inf1Complications
			,Inf2Complications
			,CreateBaby
			,BnBoxCode
			,EcvLabour
			,HighestLoc2002
			,BirthLocation
			,DateCsDecided
			,TimeCsDecided
			,TimeToCaesarian
			,MidwifeKnown
			,GestationDays
			,GestByDateDays
			,GestByUsDays
			,AssessGestDays
			,AgreedGestFromDays
			,FollowUpDay
			,DilationAtCs
			,TransWeight
			,TransFeeding
			,CmwIDischWeight
			,CmwIDischCondition
			,PnSex
			,PnDateOfBirth
			,PnAgeAtDischarge
			,BirthAnDate
			,IndicInstrumental
			,ShoulderDystocia
			,ShoulderInjury
			,ShoulderInjuryText
			,CsReasonForDelay
			,Palpable5ths
			,FetalPosition
			,Station
			,Moulding
			,Caput
			,LiquorColour
			,DeliveryOfBaby
			,CordEntanglement
			,Position
			,NewbornExamination
			,NewbornExaminationDate
			,NewbornExaminationTime
			,ExaminationCompletedBy
			,ExaminationMidwifePin
			,ExaminationText
			,Heart
			,Eyes
			,Testes
			,BirthMarksBruises
			,BirthMarksBruisesText
			,NewbornReferral
			,CongenitalList
			,RedBookCompleted
			,InfantNhsNumber
			,InfantNhsNoValid
			,InfantSex
			,InfantMaidenName
			,InfantCurrentSurname
			,InfantForenames
			,InfantDateOfBirth
			,InfantPlaceOfBirth
			,InfantEthnicGroupPrefix
			,InfantEthnicGroupBody
			,InfantEthnicGroupOpcs
			,InfantEthnicGroupPas
			,InfantGpSurname
			,InfantGpSequence
			,InfantTraceStatus
			,InfantLocation
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 BirthRecno
			,ContextCode
			,MotherNo
			,MotherNhsNumber
			,MotherNhsNoValid
			,CurrentOccurrence
			,MotherMaidenName
			,MotherCurrentSurname
			,MotherFForenames
			,MotherTitle
			,MotherDateOfBirth
			,MotherPlaceOfBirth
			,UkEntryDate
			,MotherDateApprox
			,MotherEthnicGroupPrefix
			,MotherEthnicGroupBody
			,MotherEthnicGroupOpcs
			,MotherEthnicGroupPas
			,MotherGpSurname
			,MotherGpSequence
			,MotherTraceStatus
			,MotherLocation
			,MotherDateOfDeath
			,MotherTimeOfDeath
			,MotherDeathIndicator
			,PregnancyPortable
			,PregnancyDiskCreateDate
			,PregnancyStatus
			,PregnancyType
			,PregnancyUnbooked
			,PregnancyCardNumber
			,PregnancyCreateDate
			,PregnancyCreateTime
			,MotherReligion
			,OtherReligion
			,Marital
			,MSurname
			,PrefName
			,MotherOccupation
			,MainIncome
			,PrevEmployment
			,MotherTelephoneWork
			,OneParentFam
			,SocialClass
			,BkScreen1Ind
			,BkScreen1aInd
			,BkScreen2Ind
			,InvScreen1Ind
			,InvScreen2Ind
			,AnScreen1Ind
			,AnScreen2Ind
			,AnReason
			,PregnancyAnDate
			,AnOperation
			,PregnancyGpSurname
			,PregnancyGpSequence
			,TypeOfPatient
			,OverseasVisitor
			,PregnancyConCode
			,PregnancyBookingMidwife
			,PregnancyMidwifeTeam
			,ReasonCompletion
			,SpecOtherPlace
			,ConfirmedDob
			,ReferredBy
			,ArchiveDate
			,AnSex
			,PregnancyBookingData
			,Erdip
			,HospReferredTo
			,ReferralDate
			,ReferralNumber
			,PregnancyLocation
			,ScunEcno
			,AnWardType
			,ReferredByOther
			,ReferredByHosp
			,PregnancyCommunityType
			,ReferralGestWks
			,ReferralGestDays
			,LmpAtReferral
			,EddAtReferral
			,BnScreen1Ind
			,NumberInfants
			,OneParent
			,LabourOnset
			,Augmentation
			,OnsetFirstDate
			,OnsetFirstTime
			,OnsetSecondDate
			,OnsetSecondTime
			,MethodIndAug
			,BnScreen2Ind
			,Pyrexia
			,BloodLoss
			,Perineum
			,NonPerinealTears
			,Suture
			,ManualRemoval
			,PlacentaMembranes
			,Membranes
			,LabCompl
			,LabComplText
			,IntendedFeeding
			,LabResearch
			,Support
			,DScreen1Ind
			,Eclampsia
			,Thromboembolism
			,Erpc
			,PuerperalPsychosis
			,Infection
			,DischHbTest
			,DischHaemoglobin
			,AntiD
			,RubellaVac
			,BloodTrans
			,Sterilization
			,Contraception
			,DMatCompl
			,DMatComplText
			,DScreen2Ind
			,Appointment
			,ResearchDisch
			,AgeAtDel
			,LabourDuration1stStage
			,LabourDuration2ndStage
			,LabourDurationLabour
			,PnLenStay
			,AnComplicationsIcd
			,DelComplicationsIcd
			,PuerpComplicationsIcd
			,MatOperationsIcd
			,TenSmokingStage
			,TenCigsPerDay
			,TenPartnerSmoke
			,DischSmokingStage
			,DischCigsPerDay
			,DischPartnerSmoke
			,RiskDelivery
			,DischMedication
			,DischMedicationText
			,DischPartnerCigsPerDay
			,TenPartnerCigsPerDay
			,SkinToSkin
			,SkinToSkinText
			,AnalMucosa
			,TempMaxLabour
			,EpisiotomySutured
			,LabourLocation
			,BreastFeedInitiated
			,Date1stBreastFeed
			,Time1stBreastFeed
			,PnLenStayHours
			,PnLenStayMinutes
			,TransBpSystolic
			,TransBpDiastolic
			,TransToCommunityDate
			,TransferringHospNumber
			,CmwTeam
			,CmwDefaultMidwife
			,CmwBackupMidwife
			,CmwSource
			,CmwHospitalName
			,CmwHospitalChar
			,CmwHospitalOrgCode
			,TransComments
			,CmwMDischDate
			,CmwMDischCondition
			,CpNormalLabour
			,CpReasonNotUsed
			,CpOtherReasonNotUsed
			,PnCare
			,PnCareHosp
			,PnCareOther
			,NonPerinealSuture
			,TimeSutureStarted
			,ReasonSutureDelayed
			,SterilisationText
			,OneToOneCare
			,FgmReversal
			,FgmReversalStage
			,FgmReferralToCpMw
			,DelGbsCarrier
			,DischargedToHvDate
			,DischargedToHvTime
			,PlannedDischargeDate
			,PlannedDischargeTime
			,PnComplications
			,PnComplicationsText
			,Occurrence
			,BirthOrder
			,InfantNo
			,BnScreen3Ind
			,RuptureMethod
			,RuptureDate
			,RuptureTime
			,CordProlapse
			,PresentationLabour
			,ElectronicFh
			,FetalPhTest
			,FetalPh
			,Meconium
			,CordBloodGas
			,TimeOfBirth
			,BirthWeight
			,BirthWeightCentile
			,Gestation
			,GestationByDate
			,GestationByUs
			,GestUsCertain
			,CongenitalAbnorm
			,CongenitalDesc
			,BnScreen4Ind
			,PresentationDeliv
			,BreechDiagnosis
			,BirthingPool
			,MethodDelivery
			,IndicCaesarian
			,Outcome
			,PlaceDelivery
			,ReasonForChange
			,ConductDel
			,DeliveryTeam
			,DeliveryMidwife
			,SpecPlaceDelivery
			,BnScreen5Ind
			,ApgarOne
			,ApgarFive
			,ApgarTen
			,OnsetRespiration
			,ResusPressure
			,ResusDrugs
			,ChildDischAddress
			,ConsPaed
			,InfCompl
			,InfComplText
			,InfResearch
			,MidwifeDelName
			,MidwifeDelInitials
			,MidwifeDelStatus
			,ChildNumber
			,IndexPage
			,HealthDistrict
			,TransferType
			,HealthVisitor
			,Registration
			,BirthPlace
			,BirthHA
			,OPCSBirthArea
			,OPCSResidArea
			,BornAt
			,LAWard
			,AssessGest
			,AgreedGestFrom
			,AgreedGestTo
			,HeadCircum
			,HeadCircumCentile
			,Length
			,LengthCentile
			,DScreenInd
			,HIP
			,Convulsions
			,OtherAbnorm
			,Jaundice
			,Bilirubin
			,DInfCompl
			,DInfComplText
			,CongAbsDisch
			,DScreen3AInd
			,SCBU
			,TransCare
			,Cord
			,Guthrie
			,GuthrieDate
			,Haemoglobinopathy
			,HaemoglobinopathyDate
			,Vitamink
			,VitaminkDate
			,VitaminkText
			,InfHbTest
			,InfHaemaglobin
			,InfBloodGroup
			,InfCoombs
			,DischargeExamination
			,Condition
			,DischExamComments
			,HighestLoc
			,Condition28Day
			,Resarch28Day
			,Comments
			,Feeding28Day
			,NnCompl
			,NnComplText
			,DatePlacenta
			,TimePlacenta
			,BirthDurationLabour
			,BirthDuration2ndStage
			,BirthDuration3rdStage
			,HearingTest
			,dateHearing
			,HearingResult
			,ActualHospital
			,BirthDuration1stStage
			,Tab0
			,Tab1
			,Tab2
			,Tab3
			,tab4
			,DelHospCode
			,DelHospName
			,ChildHealthCode
			,CordVenousPH
			,CordVenBaseDeficit
			,CordArterialPH
			,CordArtBaseDeficit
			,ThirdStageManagement
			,Inf1Complications
			,Inf2Complications
			,CreateBaby
			,BnBoxCode
			,EcvLabour
			,HighestLoc2002
			,BirthLocation
			,DateCsDecided
			,TimeCsDecided
			,TimeToCaesarian
			,MidwifeKnown
			,GestationDays
			,GestByDateDays
			,GestByUsDays
			,AssessGestDays
			,AgreedGestFromDays
			,FollowUpDay
			,DilationAtCs
			,TransWeight
			,TransFeeding
			,CmwIDischWeight
			,CmwIDischCondition
			,PnSex
			,PnDateOfBirth
			,PnAgeAtDischarge
			,BirthAnDate
			,IndicInstrumental
			,ShoulderDystocia
			,ShoulderInjury
			,ShoulderInjuryText
			,CsReasonForDelay
			,Palpable5ths
			,FetalPosition
			,Station
			,Moulding
			,Caput
			,LiquorColour
			,DeliveryOfBaby
			,CordEntanglement
			,Position
			,NewbornExamination
			,NewbornExaminationDate
			,NewbornExaminationTime
			,ExaminationCompletedBy
			,ExaminationMidwifePin
			,ExaminationText
			,Heart
			,Eyes
			,Testes
			,BirthMarksBruises
			,BirthMarksBruisesText
			,NewbornReferral
			,CongenitalList
			,RedBookCompleted
			,InfantNhsNumber
			,InfantNhsNoValid
			,InfantSex
			,InfantMaidenName
			,InfantCurrentSurname
			,InfantForenames
			,InfantDateOfBirth
			,InfantPlaceOfBirth
			,InfantEthnicGroupPrefix
			,InfantEthnicGroupBody
			,InfantEthnicGroupOpcs
			,InfantEthnicGroupPas
			,InfantGpSurname
			,InfantGpSequence
			,InfantTraceStatus
			,InfantLocation
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
		isnull(target.MotherNo, '') = isnull(source.MotherNo, '')
		and isnull(target.MotherNhsNumber, '') = isnull(source.MotherNhsNumber, '')
		and isnull(target.MotherNhsNoValid, '') = isnull(source.MotherNhsNoValid, '')
		and isnull(target.CurrentOccurrence, 0) = isnull(source.CurrentOccurrence, 0)
		and isnull(target.MotherMaidenName, '') = isnull(source.MotherMaidenName, '')
		and isnull(target.MotherCurrentSurname, '') = isnull(source.MotherCurrentSurname, '')
		and isnull(target.MotherFForenames, '') = isnull(source.MotherFForenames, '')
		and isnull(target.MotherTitle, '') = isnull(source.MotherTitle, '')
		and isnull(target.MotherDateOfBirth, getdate()) = isnull(source.MotherDateOfBirth, getdate())
		and isnull(target.MotherPlaceOfBirth, '') = isnull(source.MotherPlaceOfBirth, '')
		and isnull(target.UkEntryDate, getdate()) = isnull(source.UkEntryDate, getdate())
		and isnull(target.MotherDateApprox, '') = isnull(source.MotherDateApprox, '')
		and isnull(target.MotherEthnicGroupPrefix, '') = isnull(source.MotherEthnicGroupPrefix, '')
		and isnull(target.MotherEthnicGroupBody, '') = isnull(source.MotherEthnicGroupBody, '')
		and isnull(target.MotherEthnicGroupOpcs, '') = isnull(source.MotherEthnicGroupOpcs, '')
		and isnull(target.MotherEthnicGroupPas, '') = isnull(source.MotherEthnicGroupPas, '')
		and isnull(target.MotherGpSurname, '') = isnull(source.MotherGpSurname, '')
		and isnull(target.MotherGpSequence, 0) = isnull(source.MotherGpSequence, 0)
		and isnull(target.MotherTraceStatus, '') = isnull(source.MotherTraceStatus, '')
		and isnull(target.MotherLocation, '') = isnull(source.MotherLocation, '')
		and isnull(target.MotherDateOfDeath, getdate()) = isnull(source.MotherDateOfDeath, getdate())
		and isnull(target.MotherTimeOfDeath, '') = isnull(source.MotherTimeOfDeath, '')
		and isnull(target.MotherDeathIndicator, '') = isnull(source.MotherDeathIndicator, '')
		and isnull(target.PregnancyPortable, '') = isnull(source.PregnancyPortable, '')
		and isnull(target.PregnancyDiskCreateDate, getdate()) = isnull(source.PregnancyDiskCreateDate, getdate())
		and isnull(target.PregnancyStatus, '') = isnull(source.PregnancyStatus, '')
		and isnull(target.PregnancyType, '') = isnull(source.PregnancyType, '')
		and isnull(target.PregnancyUnbooked, '') = isnull(source.PregnancyUnbooked, '')
		and isnull(target.PregnancyCardNumber, 0) = isnull(source.PregnancyCardNumber, 0)
		and isnull(target.PregnancyCreateDate, getdate()) = isnull(source.PregnancyCreateDate, getdate())
		and isnull(target.PregnancyCreateTime, '') = isnull(source.PregnancyCreateTime, '')
		and isnull(target.MotherReligion, '') = isnull(source.MotherReligion, '')
		and isnull(target.OtherReligion, '') = isnull(source.OtherReligion, '')
		and isnull(target.Marital, '') = isnull(source.Marital, '')
		and isnull(target.MSurname, '') = isnull(source.MSurname, '')
		and isnull(target.PrefName, '') = isnull(source.PrefName, '')
		and isnull(target.MotherOccupation, '') = isnull(source.MotherOccupation, '')
		and isnull(target.MainIncome, '') = isnull(source.MainIncome, '')
		and isnull(target.PrevEmployment, '') = isnull(source.PrevEmployment, '')
		and isnull(target.MotherTelephoneWork, '') = isnull(source.MotherTelephoneWork, '')
		and isnull(target.OneParentFam, '') = isnull(source.OneParentFam, '')
		and isnull(target.SocialClass, '') = isnull(source.SocialClass, '')
		and isnull(target.BkScreen1Ind, '') = isnull(source.BkScreen1Ind, '')
		and isnull(target.BkScreen1aInd, '') = isnull(source.BkScreen1aInd, '')
		and isnull(target.BkScreen2Ind, '') = isnull(source.BkScreen2Ind, '')
		and isnull(target.InvScreen1Ind, '') = isnull(source.InvScreen1Ind, '')
		and isnull(target.InvScreen2Ind, '') = isnull(source.InvScreen2Ind, '')
		and isnull(target.AnScreen1Ind, '') = isnull(source.AnScreen1Ind, '')
		and isnull(target.AnScreen2Ind, '') = isnull(source.AnScreen2Ind, '')
		and isnull(target.AnReason, '') = isnull(source.AnReason, '')
		and isnull(target.PregnancyAnDate, getdate()) = isnull(source.PregnancyAnDate, getdate())
		and isnull(target.AnOperation, '') = isnull(source.AnOperation, '')
		and isnull(target.PregnancyGpSurname, '') = isnull(source.PregnancyGpSurname, '')
		and isnull(target.PregnancyGpSequence, 0) = isnull(source.PregnancyGpSequence, 0)
		and isnull(target.TypeOfPatient, '') = isnull(source.TypeOfPatient, '')
		and isnull(target.OverseasVisitor, '') = isnull(source.OverseasVisitor, '')
		and isnull(target.PregnancyConCode, '') = isnull(source.PregnancyConCode, '')
		and isnull(target.PregnancyBookingMidwife, '') = isnull(source.PregnancyBookingMidwife, '')
		and isnull(target.PregnancyMidwifeTeam, '') = isnull(source.PregnancyMidwifeTeam, '')
		and isnull(target.ReasonCompletion, '') = isnull(source.ReasonCompletion, '')
		and isnull(target.SpecOtherPlace, '') = isnull(source.SpecOtherPlace, '')
		and isnull(target.ConfirmedDob, '') = isnull(source.ConfirmedDob, '')
		and isnull(target.ReferredBy, '') = isnull(source.ReferredBy, '')
		and isnull(target.ArchiveDate, getdate()) = isnull(source.ArchiveDate, getdate())
		and isnull(target.AnSex, '') = isnull(source.AnSex, '')
		and isnull(target.PregnancyBookingData, '') = isnull(source.PregnancyBookingData, '')
		and isnull(target.Erdip, '') = isnull(source.Erdip, '')
		and isnull(target.HospReferredTo, '') = isnull(source.HospReferredTo, '')
		and isnull(target.ReferralDate, getdate()) = isnull(source.ReferralDate, getdate())
		and isnull(target.ReferralNumber, '') = isnull(source.ReferralNumber, '')
		and isnull(target.PregnancyLocation, '') = isnull(source.PregnancyLocation, '')
		and isnull(target.ScunEcno, '') = isnull(source.ScunEcno, '')
		and isnull(target.AnWardType, '') = isnull(source.AnWardType, '')
		and isnull(target.ReferredByOther, '') = isnull(source.ReferredByOther, '')
		and isnull(target.ReferredByHosp, '') = isnull(source.ReferredByHosp, '')
		and isnull(target.PregnancyCommunityType, '') = isnull(source.PregnancyCommunityType, '')
		and isnull(target.ReferralGestWks, 0) = isnull(source.ReferralGestWks, 0)
		and isnull(target.ReferralGestDays, 0) = isnull(source.ReferralGestDays, 0)
		and isnull(target.LmpAtReferral, getdate()) = isnull(source.LmpAtReferral, getdate())
		and isnull(target.EddAtReferral, getdate()) = isnull(source.EddAtReferral, getdate())
		and isnull(target.BnScreen1Ind, '') = isnull(source.BnScreen1Ind, '')
		and isnull(target.NumberInfants, '') = isnull(source.NumberInfants, '')
		and isnull(target.OneParent, '') = isnull(source.OneParent, '')
		and isnull(target.LabourOnset, '') = isnull(source.LabourOnset, '')
		and isnull(target.Augmentation, '') = isnull(source.Augmentation, '')
		and isnull(target.OnsetFirstDate, getdate()) = isnull(source.OnsetFirstDate, getdate())
		and isnull(target.OnsetFirstTime, '') = isnull(source.OnsetFirstTime, '')
		and isnull(target.OnsetSecondDate, getdate()) = isnull(source.OnsetSecondDate, getdate())
		and isnull(target.OnsetSecondTime, '') = isnull(source.OnsetSecondTime, '')
		and isnull(target.MethodIndAug, '') = isnull(source.MethodIndAug, '')
		and isnull(target.BnScreen2Ind, '') = isnull(source.BnScreen2Ind, '')
		and isnull(target.Pyrexia, '') = isnull(source.Pyrexia, '')
		and isnull(target.BloodLoss, '') = isnull(source.BloodLoss, '')
		and isnull(target.Perineum, '') = isnull(source.Perineum, '')
		and isnull(target.NonPerinealTears, '') = isnull(source.NonPerinealTears, '')
		and isnull(target.Suture, '') = isnull(source.Suture, '')
		and isnull(target.ManualRemoval, '') = isnull(source.ManualRemoval, '')
		and isnull(target.PlacentaMembranes, '') = isnull(source.PlacentaMembranes, '')
		and isnull(target.Membranes, '') = isnull(source.Membranes, '')
		and isnull(target.LabCompl, '') = isnull(source.LabCompl, '')
		and isnull(target.LabComplText, '') = isnull(source.LabComplText, '')
		and isnull(target.IntendedFeeding, '') = isnull(source.IntendedFeeding, '')
		and isnull(target.LabResearch, '') = isnull(source.LabResearch, '')
		and isnull(target.Support, '') = isnull(source.Support, '')
		and isnull(target.DScreen1Ind, '') = isnull(source.DScreen1Ind, '')
		and isnull(target.Eclampsia, '') = isnull(source.Eclampsia, '')
		and isnull(target.Thromboembolism, '') = isnull(source.Thromboembolism, '')
		and isnull(target.Erpc, '') = isnull(source.Erpc, '')
		and isnull(target.PuerperalPsychosis, '') = isnull(source.PuerperalPsychosis, '')
		and isnull(target.Infection, '') = isnull(source.Infection, '')
		and isnull(target.DischHbTest, '') = isnull(source.DischHbTest, '')
		and isnull(target.DischHaemoglobin, '') = isnull(source.DischHaemoglobin, '')
		and isnull(target.AntiD, '') = isnull(source.AntiD, '')
		and isnull(target.RubellaVac, '') = isnull(source.RubellaVac, '')
		and isnull(target.BloodTrans, '') = isnull(source.BloodTrans, '')
		and isnull(target.Sterilization, '') = isnull(source.Sterilization, '')
		and isnull(target.Contraception, '') = isnull(source.Contraception, '')
		and isnull(target.DMatCompl, '') = isnull(source.DMatCompl, '')
		and isnull(target.DMatComplText, '') = isnull(source.DMatComplText, '')
		and isnull(target.DScreen2Ind, '') = isnull(source.DScreen2Ind, '')
		and isnull(target.Appointment, '') = isnull(source.Appointment, '')
		and isnull(target.ResearchDisch, '') = isnull(source.ResearchDisch, '')
		and isnull(target.AgeAtDel, '') = isnull(source.AgeAtDel, '')
		and isnull(target.LabourDuration1stStage, '') = isnull(source.LabourDuration1stStage, '')
		and isnull(target.LabourDuration2ndStage, '') = isnull(source.LabourDuration2ndStage, '')
		and isnull(target.LabourDurationLabour, '') = isnull(source.LabourDurationLabour, '')
		and isnull(target.PnLenStay, '') = isnull(source.PnLenStay, '')
		and isnull(target.AnComplicationsIcd, '') = isnull(source.AnComplicationsIcd, '')
		and isnull(target.DelComplicationsIcd, '') = isnull(source.DelComplicationsIcd, '')
		and isnull(target.PuerpComplicationsIcd, '') = isnull(source.PuerpComplicationsIcd, '')
		and isnull(target.MatOperationsIcd, '') = isnull(source.MatOperationsIcd, '')
		and isnull(target.TenSmokingStage, '') = isnull(source.TenSmokingStage, '')
		and isnull(target.TenCigsPerDay, 0) = isnull(source.TenCigsPerDay, 0)
		and isnull(target.TenPartnerSmoke, '') = isnull(source.TenPartnerSmoke, '')
		and isnull(target.DischSmokingStage, '') = isnull(source.DischSmokingStage, '')
		and isnull(target.DischCigsPerDay, 0) = isnull(source.DischCigsPerDay, 0)
		and isnull(target.DischPartnerSmoke, '') = isnull(source.DischPartnerSmoke, '')
		and isnull(target.RiskDelivery, '') = isnull(source.RiskDelivery, '')
		and isnull(target.DischMedication, '') = isnull(source.DischMedication, '')
		and isnull(target.DischMedicationText, '') = isnull(source.DischMedicationText, '')
		and isnull(target.DischPartnerCigsPerDay, 0) = isnull(source.DischPartnerCigsPerDay, 0)
		and isnull(target.TenPartnerCigsPerDay, 0) = isnull(source.TenPartnerCigsPerDay, 0)
		and isnull(target.SkinToSkin, '') = isnull(source.SkinToSkin, '')
		and isnull(target.SkinToSkinText, '') = isnull(source.SkinToSkinText, '')
		and isnull(target.AnalMucosa, '') = isnull(source.AnalMucosa, '')
		and isnull(target.TempMaxLabour, 0) = isnull(source.TempMaxLabour, 0)
		and isnull(target.EpisiotomySutured, '') = isnull(source.EpisiotomySutured, '')
		and isnull(target.LabourLocation, '') = isnull(source.LabourLocation, '')
		and isnull(target.BreastFeedInitiated, '') = isnull(source.BreastFeedInitiated, '')
		and isnull(target.Date1stBreastFeed, getdate()) = isnull(source.Date1stBreastFeed, getdate())
		and isnull(target.Time1stBreastFeed, '') = isnull(source.Time1stBreastFeed, '')
		and isnull(target.PnLenStayHours, 0) = isnull(source.PnLenStayHours, 0)
		and isnull(target.PnLenStayMinutes, 0) = isnull(source.PnLenStayMinutes, 0)
		and isnull(target.TransBpSystolic, 0) = isnull(source.TransBpSystolic, 0)
		and isnull(target.TransBpDiastolic, 0) = isnull(source.TransBpDiastolic, 0)
		and isnull(target.TransToCommunityDate, getdate()) = isnull(source.TransToCommunityDate, getdate())
		and isnull(target.TransferringHospNumber, '') = isnull(source.TransferringHospNumber, '')
		and isnull(target.CmwTeam, '') = isnull(source.CmwTeam, '')
		and isnull(target.CmwDefaultMidwife, '') = isnull(source.CmwDefaultMidwife, '')
		and isnull(target.CmwBackupMidwife, '') = isnull(source.CmwBackupMidwife, '')
		and isnull(target.CmwSource, '') = isnull(source.CmwSource, '')
		and isnull(target.CmwHospitalName, '') = isnull(source.CmwHospitalName, '')
		and isnull(target.CmwHospitalChar, '') = isnull(source.CmwHospitalChar, '')
		and isnull(target.CmwHospitalOrgCode, '') = isnull(source.CmwHospitalOrgCode, '')
		and isnull(target.TransComments, '') = isnull(source.TransComments, '')
		and isnull(target.CmwMDischDate, getdate()) = isnull(source.CmwMDischDate, getdate())
		and isnull(target.CmwMDischCondition, '') = isnull(source.CmwMDischCondition, '')
		and isnull(target.CpNormalLabour, '') = isnull(source.CpNormalLabour, '')
		and isnull(target.CpReasonNotUsed, '') = isnull(source.CpReasonNotUsed, '')
		and isnull(target.CpOtherReasonNotUsed, '') = isnull(source.CpOtherReasonNotUsed, '')
		and isnull(target.PnCare, '') = isnull(source.PnCare, '')
		and isnull(target.PnCareHosp, '') = isnull(source.PnCareHosp, '')
		and isnull(target.PnCareOther, '') = isnull(source.PnCareOther, '')
		and isnull(target.NonPerinealSuture, '') = isnull(source.NonPerinealSuture, '')
		and isnull(target.TimeSutureStarted, '') = isnull(source.TimeSutureStarted, '')
		and isnull(target.ReasonSutureDelayed, '') = isnull(source.ReasonSutureDelayed, '')
		and isnull(target.SterilisationText, '') = isnull(source.SterilisationText, '')
		and isnull(target.OneToOneCare, '') = isnull(source.OneToOneCare, '')
		and isnull(target.FgmReversal, '') = isnull(source.FgmReversal, '')
		and isnull(target.FgmReversalStage, '') = isnull(source.FgmReversalStage, '')
		and isnull(target.FgmReferralToCpMw, '') = isnull(source.FgmReferralToCpMw, '')
		and isnull(target.DelGbsCarrier, '') = isnull(source.DelGbsCarrier, '')
		and isnull(target.DischargedToHvDate, getdate()) = isnull(source.DischargedToHvDate, getdate())
		and isnull(target.DischargedToHvTime, '') = isnull(source.DischargedToHvTime, '')
		and isnull(target.PlannedDischargeDate, getdate()) = isnull(source.PlannedDischargeDate, getdate())
		and isnull(target.PlannedDischargeTime, '') = isnull(source.PlannedDischargeTime, '')
		and isnull(target.PnComplications, '') = isnull(source.PnComplications, '')
		and isnull(target.PnComplicationsText, '') = isnull(source.PnComplicationsText, '')
		and isnull(target.Occurrence, 0) = isnull(source.Occurrence, 0)
		and isnull(target.BirthOrder, '') = isnull(source.BirthOrder, '')
		and isnull(target.InfantNo, '') = isnull(source.InfantNo, '')
		and isnull(target.BnScreen3Ind, '') = isnull(source.BnScreen3Ind, '')
		and isnull(target.RuptureMethod, '') = isnull(source.RuptureMethod, '')
		and isnull(target.RuptureDate, getdate()) = isnull(source.RuptureDate, getdate())
		and isnull(target.RuptureTime, '') = isnull(source.RuptureTime, '')
		and isnull(target.CordProlapse, '') = isnull(source.CordProlapse, '')
		and isnull(target.PresentationLabour, '') = isnull(source.PresentationLabour, '')
		and isnull(target.ElectronicFh, '') = isnull(source.ElectronicFh, '')
		and isnull(target.FetalPhTest, '') = isnull(source.FetalPhTest, '')
		and isnull(target.FetalPh, '') = isnull(source.FetalPh, '')
		and isnull(target.Meconium, '') = isnull(source.Meconium, '')
		and isnull(target.CordBloodGas, '') = isnull(source.CordBloodGas, '')
		and isnull(target.TimeOfBirth, '') = isnull(source.TimeOfBirth, '')
		and isnull(target.BirthWeight, '') = isnull(source.BirthWeight, '')
		and isnull(target.BirthWeightCentile, '') = isnull(source.BirthWeightCentile, '')
		and isnull(target.Gestation, '') = isnull(source.Gestation, '')
		and isnull(target.GestationByDate, '') = isnull(source.GestationByDate, '')
		and isnull(target.GestationByUs, '') = isnull(source.GestationByUs, '')
		and isnull(target.GestUsCertain, '') = isnull(source.GestUsCertain, '')
		and isnull(target.CongenitalAbnorm, '') = isnull(source.CongenitalAbnorm, '')
		and isnull(target.CongenitalDesc, '') = isnull(source.CongenitalDesc, '')
		and isnull(target.BnScreen4Ind, '') = isnull(source.BnScreen4Ind, '')
		and isnull(target.PresentationDeliv, '') = isnull(source.PresentationDeliv, '')
		and isnull(target.BreechDiagnosis, '') = isnull(source.BreechDiagnosis, '')
		and isnull(target.BirthingPool, '') = isnull(source.BirthingPool, '')
		and isnull(target.MethodDelivery, '') = isnull(source.MethodDelivery, '')
		and isnull(target.IndicCaesarian, '') = isnull(source.IndicCaesarian, '')
		and isnull(target.Outcome, '') = isnull(source.Outcome, '')
		and isnull(target.PlaceDelivery, '') = isnull(source.PlaceDelivery, '')
		and isnull(target.ReasonForChange, '') = isnull(source.ReasonForChange, '')
		and isnull(target.ConductDel, '') = isnull(source.ConductDel, '')
		and isnull(target.DeliveryTeam, '') = isnull(source.DeliveryTeam, '')
		and isnull(target.DeliveryMidwife, '') = isnull(source.DeliveryMidwife, '')
		and isnull(target.SpecPlaceDelivery, '') = isnull(source.SpecPlaceDelivery, '')
		and isnull(target.BnScreen5Ind, '') = isnull(source.BnScreen5Ind, '')
		and isnull(target.ApgarOne, '') = isnull(source.ApgarOne, '')
		and isnull(target.ApgarFive, '') = isnull(source.ApgarFive, '')
		and isnull(target.ApgarTen, '') = isnull(source.ApgarTen, '')
		and isnull(target.OnsetRespiration, '') = isnull(source.OnsetRespiration, '')
		and isnull(target.ResusPressure, '') = isnull(source.ResusPressure, '')
		and isnull(target.ResusDrugs, '') = isnull(source.ResusDrugs, '')
		and isnull(target.ChildDischAddress, '') = isnull(source.ChildDischAddress, '')
		and isnull(target.ConsPaed, '') = isnull(source.ConsPaed, '')
		and isnull(target.InfCompl, '') = isnull(source.InfCompl, '')
		and isnull(target.InfComplText, '') = isnull(source.InfComplText, '')
		and isnull(target.InfResearch, '') = isnull(source.InfResearch, '')
		and isnull(target.MidwifeDelName, '') = isnull(source.MidwifeDelName, '')
		and isnull(target.MidwifeDelInitials, '') = isnull(source.MidwifeDelInitials, '')
		and isnull(target.MidwifeDelStatus, '') = isnull(source.MidwifeDelStatus, '')
		and isnull(target.ChildNumber, '') = isnull(source.ChildNumber, '')
		and isnull(target.IndexPage, '') = isnull(source.IndexPage, '')
		and isnull(target.HealthDistrict, '') = isnull(source.HealthDistrict, '')
		and isnull(target.TransferType, '') = isnull(source.TransferType, '')
		and isnull(target.HealthVisitor, '') = isnull(source.HealthVisitor, '')
		and isnull(target.Registration, '') = isnull(source.Registration, '')
		and isnull(target.BirthPlace, '') = isnull(source.BirthPlace, '')
		and isnull(target.BirthHA, '') = isnull(source.BirthHA, '')
		and isnull(target.OPCSBirthArea, '') = isnull(source.OPCSBirthArea, '')
		and isnull(target.OPCSResidArea, '') = isnull(source.OPCSResidArea, '')
		and isnull(target.BornAt, '') = isnull(source.BornAt, '')
		and isnull(target.LAWard, '') = isnull(source.LAWard, '')
		and isnull(target.AssessGest, '') = isnull(source.AssessGest, '')
		and isnull(target.AgreedGestFrom, '') = isnull(source.AgreedGestFrom, '')
		and isnull(target.AgreedGestTo, '') = isnull(source.AgreedGestTo, '')
		and isnull(target.HeadCircum, '') = isnull(source.HeadCircum, '')
		and isnull(target.HeadCircumCentile, '') = isnull(source.HeadCircumCentile, '')
		and isnull(target.Length, '') = isnull(source.Length, '')
		and isnull(target.LengthCentile, '') = isnull(source.LengthCentile, '')
		and isnull(target.DScreenInd, '') = isnull(source.DScreenInd, '')
		and isnull(target.HIP, '') = isnull(source.HIP, '')
		and isnull(target.Convulsions, '') = isnull(source.Convulsions, '')
		and isnull(target.OtherAbnorm, '') = isnull(source.OtherAbnorm, '')
		and isnull(target.Jaundice, '') = isnull(source.Jaundice, '')
		and isnull(target.Bilirubin, '') = isnull(source.Bilirubin, '')
		and isnull(target.DInfCompl, '') = isnull(source.DInfCompl, '')
		and isnull(target.DInfComplText, '') = isnull(source.DInfComplText, '')
		and isnull(target.CongAbsDisch, '') = isnull(source.CongAbsDisch, '')
		and isnull(target.DScreen3AInd, '') = isnull(source.DScreen3AInd, '')
		and isnull(target.SCBU, '') = isnull(source.SCBU, '')
		and isnull(target.TransCare, '') = isnull(source.TransCare, '')
		and isnull(target.Cord, '') = isnull(source.Cord, '')
		and isnull(target.Guthrie, '') = isnull(source.Guthrie, '')
		and isnull(target.GuthrieDate, getdate()) = isnull(source.GuthrieDate, getdate())
		and isnull(target.Haemoglobinopathy, '') = isnull(source.Haemoglobinopathy, '')
		and isnull(target.HaemoglobinopathyDate, getdate()) = isnull(source.HaemoglobinopathyDate, getdate())
		and isnull(target.Vitamink, '') = isnull(source.Vitamink, '')
		and isnull(target.VitaminkDate, getdate()) = isnull(source.VitaminkDate, getdate())
		and isnull(target.VitaminkText, '') = isnull(source.VitaminkText, '')
		and isnull(target.InfHbTest, '') = isnull(source.InfHbTest, '')
		and isnull(target.InfHaemaglobin, '') = isnull(source.InfHaemaglobin, '')
		and isnull(target.InfBloodGroup, '') = isnull(source.InfBloodGroup, '')
		and isnull(target.InfCoombs, '') = isnull(source.InfCoombs, '')
		and isnull(target.DischargeExamination, '') = isnull(source.DischargeExamination, '')
		and isnull(target.Condition, '') = isnull(source.Condition, '')
		and isnull(target.DischExamComments, '') = isnull(source.DischExamComments, '')
		and isnull(target.HighestLoc, '') = isnull(source.HighestLoc, '')
		and isnull(target.Condition28Day, '') = isnull(source.Condition28Day, '')
		and isnull(target.Resarch28Day, '') = isnull(source.Resarch28Day, '')
		and isnull(target.Comments, '') = isnull(source.Comments, '')
		and isnull(target.Feeding28Day, '') = isnull(source.Feeding28Day, '')
		and isnull(target.NnCompl, '') = isnull(source.NnCompl, '')
		and isnull(target.NnComplText, '') = isnull(source.NnComplText, '')
		and isnull(target.DatePlacenta, getdate()) = isnull(source.DatePlacenta, getdate())
		and isnull(target.TimePlacenta, '') = isnull(source.TimePlacenta, '')
		and isnull(target.BirthDurationLabour, '') = isnull(source.BirthDurationLabour, '')
		and isnull(target.BirthDuration2ndStage, '') = isnull(source.BirthDuration2ndStage, '')
		and isnull(target.BirthDuration3rdStage, '') = isnull(source.BirthDuration3rdStage, '')
		and isnull(target.HearingTest, '') = isnull(source.HearingTest, '')
		and isnull(target.dateHearing, getdate()) = isnull(source.dateHearing, getdate())
		and isnull(target.HearingResult, '') = isnull(source.HearingResult, '')
		and isnull(target.ActualHospital, '') = isnull(source.ActualHospital, '')
		and isnull(target.BirthDuration1stStage, '') = isnull(source.BirthDuration1stStage, '')
		and isnull(target.Tab0, '') = isnull(source.Tab0, '')
		and isnull(target.Tab1, '') = isnull(source.Tab1, '')
		and isnull(target.Tab2, '') = isnull(source.Tab2, '')
		and isnull(target.Tab3, '') = isnull(source.Tab3, '')
		and isnull(target.tab4, '') = isnull(source.tab4, '')
		and isnull(target.DelHospCode, '') = isnull(source.DelHospCode, '')
		and isnull(target.DelHospName, '') = isnull(source.DelHospName, '')
		and isnull(target.ChildHealthCode, '') = isnull(source.ChildHealthCode, '')
		and isnull(target.CordVenousPH, 0) = isnull(source.CordVenousPH, 0)
		and isnull(target.CordVenBaseDeficit, 0) = isnull(source.CordVenBaseDeficit, 0)
		and isnull(target.CordArterialPH, 0) = isnull(source.CordArterialPH, 0)
		and isnull(target.CordArtBaseDeficit, 0) = isnull(source.CordArtBaseDeficit, 0)
		and isnull(target.ThirdStageManagement, '') = isnull(source.ThirdStageManagement, '')
		and isnull(target.Inf1Complications, '') = isnull(source.Inf1Complications, '')
		and isnull(target.Inf2Complications, '') = isnull(source.Inf2Complications, '')
		and isnull(target.CreateBaby, '') = isnull(source.CreateBaby, '')
		and isnull(target.BnBoxCode, '') = isnull(source.BnBoxCode, '')
		and isnull(target.EcvLabour, '') = isnull(source.EcvLabour, '')
		and isnull(target.HighestLoc2002, '') = isnull(source.HighestLoc2002, '')
		and isnull(target.BirthLocation, '') = isnull(source.BirthLocation, '')
		and isnull(target.DateCsDecided, getdate()) = isnull(source.DateCsDecided, getdate())
		and isnull(target.TimeCsDecided, '') = isnull(source.TimeCsDecided, '')
		and isnull(target.TimeToCaesarian, '') = isnull(source.TimeToCaesarian, '')
		and isnull(target.MidwifeKnown, '') = isnull(source.MidwifeKnown, '')
		and isnull(target.GestationDays, 0) = isnull(source.GestationDays, 0)
		and isnull(target.GestByDateDays, 0) = isnull(source.GestByDateDays, 0)
		and isnull(target.GestByUsDays, 0) = isnull(source.GestByUsDays, 0)
		and isnull(target.AssessGestDays, 0) = isnull(source.AssessGestDays, 0)
		and isnull(target.AgreedGestFromDays, 0) = isnull(source.AgreedGestFromDays, 0)
		and isnull(target.FollowUpDay, 0) = isnull(source.FollowUpDay, 0)
		and isnull(target.DilationAtCs, 0) = isnull(source.DilationAtCs, 0)
		and isnull(target.TransWeight, 0) = isnull(source.TransWeight, 0)
		and isnull(target.TransFeeding, '') = isnull(source.TransFeeding, '')
		and isnull(target.CmwIDischWeight, 0) = isnull(source.CmwIDischWeight, 0)
		and isnull(target.CmwIDischCondition, '') = isnull(source.CmwIDischCondition, '')
		and isnull(target.PnSex, '') = isnull(source.PnSex, '')
		and isnull(target.PnDateOfBirth, getdate()) = isnull(source.PnDateOfBirth, getdate())
		and isnull(target.PnAgeAtDischarge, 0) = isnull(source.PnAgeAtDischarge, 0)
		and isnull(target.BirthAnDate, getdate()) = isnull(source.BirthAnDate, getdate())
		and isnull(target.IndicInstrumental, '') = isnull(source.IndicInstrumental, '')
		and isnull(target.ShoulderDystocia, '') = isnull(source.ShoulderDystocia, '')
		and isnull(target.ShoulderInjury, '') = isnull(source.ShoulderInjury, '')
		and isnull(target.ShoulderInjuryText, '') = isnull(source.ShoulderInjuryText, '')
		and isnull(target.CsReasonForDelay, '') = isnull(source.CsReasonForDelay, '')
		and isnull(target.Palpable5ths, '') = isnull(source.Palpable5ths, '')
		and isnull(target.FetalPosition, '') = isnull(source.FetalPosition, '')
		and isnull(target.Station, '') = isnull(source.Station, '')
		and isnull(target.Moulding, '') = isnull(source.Moulding, '')
		and isnull(target.Caput, '') = isnull(source.Caput, '')
		and isnull(target.LiquorColour, '') = isnull(source.LiquorColour, '')
		and isnull(target.DeliveryOfBaby, '') = isnull(source.DeliveryOfBaby, '')
		and isnull(target.CordEntanglement, '') = isnull(source.CordEntanglement, '')
		and isnull(target.Position, '') = isnull(source.Position, '')
		and isnull(target.NewbornExamination, '') = isnull(source.NewbornExamination, '')
		and isnull(target.NewbornExaminationDate, getdate()) = isnull(source.NewbornExaminationDate, getdate())
		and isnull(target.NewbornExaminationTime, '') = isnull(source.NewbornExaminationTime, '')
		and isnull(target.ExaminationCompletedBy, '') = isnull(source.ExaminationCompletedBy, '')
		and isnull(target.ExaminationMidwifePin, '') = isnull(source.ExaminationMidwifePin, '')
		and isnull(target.ExaminationText, '') = isnull(source.ExaminationText, '')
		and isnull(target.Heart, '') = isnull(source.Heart, '')
		and isnull(target.Eyes, '') = isnull(source.Eyes, '')
		and isnull(target.Testes, '') = isnull(source.Testes, '')
		and isnull(target.BirthMarksBruises, '') = isnull(source.BirthMarksBruises, '')
		and isnull(target.BirthMarksBruisesText, '') = isnull(source.BirthMarksBruisesText, '')
		and isnull(target.NewbornReferral, '') = isnull(source.NewbornReferral, '')
		and isnull(target.CongenitalList, '') = isnull(source.CongenitalList, '')
		and isnull(target.RedBookCompleted, '') = isnull(source.RedBookCompleted, '')
		and isnull(target.InfantNhsNumber, '') = isnull(source.InfantNhsNumber, '')
		and isnull(target.InfantNhsNoValid, '') = isnull(source.InfantNhsNoValid, '')
		and isnull(target.InfantSex, '') = isnull(source.InfantSex, '')
		and isnull(target.InfantMaidenName, '') = isnull(source.InfantMaidenName, '')
		and isnull(target.InfantCurrentSurname, '') = isnull(source.InfantCurrentSurname, '')
		and isnull(target.InfantForenames, '') = isnull(source.InfantForenames, '')
		and isnull(target.InfantDateOfBirth, getdate()) = isnull(source.InfantDateOfBirth, getdate())
		and isnull(target.InfantPlaceOfBirth, '') = isnull(source.InfantPlaceOfBirth, '')
		and isnull(target.InfantEthnicGroupPrefix, '') = isnull(source.InfantEthnicGroupPrefix, '')
		and isnull(target.InfantEthnicGroupBody, '') = isnull(source.InfantEthnicGroupBody, '')
		and isnull(target.InfantEthnicGroupOpcs, '') = isnull(source.InfantEthnicGroupOpcs, '')
		and isnull(target.InfantEthnicGroupPas, '') = isnull(source.InfantEthnicGroupPas, '')
		and isnull(target.InfantGpSurname, '') = isnull(source.InfantGpSurname, '')
		and isnull(target.InfantGpSequence, 0) = isnull(source.InfantGpSequence, 0)
		and isnull(target.InfantTraceStatus, '') = isnull(source.InfantTraceStatus, '')
		and isnull(target.InfantLocation, '') = isnull(source.InfantLocation, '')
	)
	then
		update
		set
			target.MotherNo = source.MotherNo
			,target.MotherNhsNumber = source.MotherNhsNumber
			,target.MotherNhsNoValid = source.MotherNhsNoValid
			,target.CurrentOccurrence = source.CurrentOccurrence
			,target.MotherMaidenName = source.MotherMaidenName
			,target.MotherCurrentSurname = source.MotherCurrentSurname
			,target.MotherFForenames = source.MotherFForenames
			,target.MotherTitle = source.MotherTitle
			,target.MotherDateOfBirth = source.MotherDateOfBirth
			,target.MotherPlaceOfBirth = source.MotherPlaceOfBirth
			,target.UkEntryDate = source.UkEntryDate
			,target.MotherDateApprox = source.MotherDateApprox
			,target.MotherEthnicGroupPrefix = source.MotherEthnicGroupPrefix
			,target.MotherEthnicGroupBody = source.MotherEthnicGroupBody
			,target.MotherEthnicGroupOpcs = source.MotherEthnicGroupOpcs
			,target.MotherEthnicGroupPas = source.MotherEthnicGroupPas
			,target.MotherGpSurname = source.MotherGpSurname
			,target.MotherGpSequence = source.MotherGpSequence
			,target.MotherTraceStatus = source.MotherTraceStatus
			,target.MotherLocation = source.MotherLocation
			,target.MotherDateOfDeath = source.MotherDateOfDeath
			,target.MotherTimeOfDeath = source.MotherTimeOfDeath
			,target.MotherDeathIndicator = source.MotherDeathIndicator
			,target.PregnancyPortable = source.PregnancyPortable
			,target.PregnancyDiskCreateDate = source.PregnancyDiskCreateDate
			,target.PregnancyStatus = source.PregnancyStatus
			,target.PregnancyType = source.PregnancyType
			,target.PregnancyUnbooked = source.PregnancyUnbooked
			,target.PregnancyCardNumber = source.PregnancyCardNumber
			,target.PregnancyCreateDate = source.PregnancyCreateDate
			,target.PregnancyCreateTime = source.PregnancyCreateTime
			,target.MotherReligion = source.MotherReligion
			,target.OtherReligion = source.OtherReligion
			,target.Marital = source.Marital
			,target.MSurname = source.MSurname
			,target.PrefName = source.PrefName
			,target.MotherOccupation = source.MotherOccupation
			,target.MainIncome = source.MainIncome
			,target.PrevEmployment = source.PrevEmployment
			,target.MotherTelephoneWork = source.MotherTelephoneWork
			,target.OneParentFam = source.OneParentFam
			,target.SocialClass = source.SocialClass
			,target.BkScreen1Ind = source.BkScreen1Ind
			,target.BkScreen1aInd = source.BkScreen1aInd
			,target.BkScreen2Ind = source.BkScreen2Ind
			,target.InvScreen1Ind = source.InvScreen1Ind
			,target.InvScreen2Ind = source.InvScreen2Ind
			,target.AnScreen1Ind = source.AnScreen1Ind
			,target.AnScreen2Ind = source.AnScreen2Ind
			,target.AnReason = source.AnReason
			,target.PregnancyAnDate = source.PregnancyAnDate
			,target.AnOperation = source.AnOperation
			,target.PregnancyGpSurname = source.PregnancyGpSurname
			,target.PregnancyGpSequence = source.PregnancyGpSequence
			,target.TypeOfPatient = source.TypeOfPatient
			,target.OverseasVisitor = source.OverseasVisitor
			,target.PregnancyConCode = source.PregnancyConCode
			,target.PregnancyBookingMidwife = source.PregnancyBookingMidwife
			,target.PregnancyMidwifeTeam = source.PregnancyMidwifeTeam
			,target.ReasonCompletion = source.ReasonCompletion
			,target.SpecOtherPlace = source.SpecOtherPlace
			,target.ConfirmedDob = source.ConfirmedDob
			,target.ReferredBy = source.ReferredBy
			,target.ArchiveDate = source.ArchiveDate
			,target.AnSex = source.AnSex
			,target.PregnancyBookingData = source.PregnancyBookingData
			,target.Erdip = source.Erdip
			,target.HospReferredTo = source.HospReferredTo
			,target.ReferralDate = source.ReferralDate
			,target.ReferralNumber = source.ReferralNumber
			,target.PregnancyLocation = source.PregnancyLocation
			,target.ScunEcno = source.ScunEcno
			,target.AnWardType = source.AnWardType
			,target.ReferredByOther = source.ReferredByOther
			,target.ReferredByHosp = source.ReferredByHosp
			,target.PregnancyCommunityType = source.PregnancyCommunityType
			,target.ReferralGestWks = source.ReferralGestWks
			,target.ReferralGestDays = source.ReferralGestDays
			,target.LmpAtReferral = source.LmpAtReferral
			,target.EddAtReferral = source.EddAtReferral
			,target.BnScreen1Ind = source.BnScreen1Ind
			,target.NumberInfants = source.NumberInfants
			,target.OneParent = source.OneParent
			,target.LabourOnset = source.LabourOnset
			,target.Augmentation = source.Augmentation
			,target.OnsetFirstDate = source.OnsetFirstDate
			,target.OnsetFirstTime = source.OnsetFirstTime
			,target.OnsetSecondDate = source.OnsetSecondDate
			,target.OnsetSecondTime = source.OnsetSecondTime
			,target.MethodIndAug = source.MethodIndAug
			,target.BnScreen2Ind = source.BnScreen2Ind
			,target.Pyrexia = source.Pyrexia
			,target.BloodLoss = source.BloodLoss
			,target.Perineum = source.Perineum
			,target.NonPerinealTears = source.NonPerinealTears
			,target.Suture = source.Suture
			,target.ManualRemoval = source.ManualRemoval
			,target.PlacentaMembranes = source.PlacentaMembranes
			,target.Membranes = source.Membranes
			,target.LabCompl = source.LabCompl
			,target.LabComplText = source.LabComplText
			,target.IntendedFeeding = source.IntendedFeeding
			,target.LabResearch = source.LabResearch
			,target.Support = source.Support
			,target.DScreen1Ind = source.DScreen1Ind
			,target.Eclampsia = source.Eclampsia
			,target.Thromboembolism = source.Thromboembolism
			,target.Erpc = source.Erpc
			,target.PuerperalPsychosis = source.PuerperalPsychosis
			,target.Infection = source.Infection
			,target.DischHbTest = source.DischHbTest
			,target.DischHaemoglobin = source.DischHaemoglobin
			,target.AntiD = source.AntiD
			,target.RubellaVac = source.RubellaVac
			,target.BloodTrans = source.BloodTrans
			,target.Sterilization = source.Sterilization
			,target.Contraception = source.Contraception
			,target.DMatCompl = source.DMatCompl
			,target.DMatComplText = source.DMatComplText
			,target.DScreen2Ind = source.DScreen2Ind
			,target.Appointment = source.Appointment
			,target.ResearchDisch = source.ResearchDisch
			,target.AgeAtDel = source.AgeAtDel
			,target.LabourDuration1stStage = source.LabourDuration1stStage
			,target.LabourDuration2ndStage = source.LabourDuration2ndStage
			,target.LabourDurationLabour = source.LabourDurationLabour
			,target.PnLenStay = source.PnLenStay
			,target.AnComplicationsIcd = source.AnComplicationsIcd
			,target.DelComplicationsIcd = source.DelComplicationsIcd
			,target.PuerpComplicationsIcd = source.PuerpComplicationsIcd
			,target.MatOperationsIcd = source.MatOperationsIcd
			,target.TenSmokingStage = source.TenSmokingStage
			,target.TenCigsPerDay = source.TenCigsPerDay
			,target.TenPartnerSmoke = source.TenPartnerSmoke
			,target.DischSmokingStage = source.DischSmokingStage
			,target.DischCigsPerDay = source.DischCigsPerDay
			,target.DischPartnerSmoke = source.DischPartnerSmoke
			,target.RiskDelivery = source.RiskDelivery
			,target.DischMedication = source.DischMedication
			,target.DischMedicationText = source.DischMedicationText
			,target.DischPartnerCigsPerDay = source.DischPartnerCigsPerDay
			,target.TenPartnerCigsPerDay = source.TenPartnerCigsPerDay
			,target.SkinToSkin = source.SkinToSkin
			,target.SkinToSkinText = source.SkinToSkinText
			,target.AnalMucosa = source.AnalMucosa
			,target.TempMaxLabour = source.TempMaxLabour
			,target.EpisiotomySutured = source.EpisiotomySutured
			,target.LabourLocation = source.LabourLocation
			,target.BreastFeedInitiated = source.BreastFeedInitiated
			,target.Date1stBreastFeed = source.Date1stBreastFeed
			,target.Time1stBreastFeed = source.Time1stBreastFeed
			,target.PnLenStayHours = source.PnLenStayHours
			,target.PnLenStayMinutes = source.PnLenStayMinutes
			,target.TransBpSystolic = source.TransBpSystolic
			,target.TransBpDiastolic = source.TransBpDiastolic
			,target.TransToCommunityDate = source.TransToCommunityDate
			,target.TransferringHospNumber = source.TransferringHospNumber
			,target.CmwTeam = source.CmwTeam
			,target.CmwDefaultMidwife = source.CmwDefaultMidwife
			,target.CmwBackupMidwife = source.CmwBackupMidwife
			,target.CmwSource = source.CmwSource
			,target.CmwHospitalName = source.CmwHospitalName
			,target.CmwHospitalChar = source.CmwHospitalChar
			,target.CmwHospitalOrgCode = source.CmwHospitalOrgCode
			,target.TransComments = source.TransComments
			,target.CmwMDischDate = source.CmwMDischDate
			,target.CmwMDischCondition = source.CmwMDischCondition
			,target.CpNormalLabour = source.CpNormalLabour
			,target.CpReasonNotUsed = source.CpReasonNotUsed
			,target.CpOtherReasonNotUsed = source.CpOtherReasonNotUsed
			,target.PnCare = source.PnCare
			,target.PnCareHosp = source.PnCareHosp
			,target.PnCareOther = source.PnCareOther
			,target.NonPerinealSuture = source.NonPerinealSuture
			,target.TimeSutureStarted = source.TimeSutureStarted
			,target.ReasonSutureDelayed = source.ReasonSutureDelayed
			,target.SterilisationText = source.SterilisationText
			,target.OneToOneCare = source.OneToOneCare
			,target.FgmReversal = source.FgmReversal
			,target.FgmReversalStage = source.FgmReversalStage
			,target.FgmReferralToCpMw = source.FgmReferralToCpMw
			,target.DelGbsCarrier = source.DelGbsCarrier
			,target.DischargedToHvDate = source.DischargedToHvDate
			,target.DischargedToHvTime = source.DischargedToHvTime
			,target.PlannedDischargeDate = source.PlannedDischargeDate
			,target.PlannedDischargeTime = source.PlannedDischargeTime
			,target.PnComplications = source.PnComplications
			,target.PnComplicationsText = source.PnComplicationsText
			,target.Occurrence = source.Occurrence
			,target.BirthOrder = source.BirthOrder
			,target.InfantNo = source.InfantNo
			,target.BnScreen3Ind = source.BnScreen3Ind
			,target.RuptureMethod = source.RuptureMethod
			,target.RuptureDate = source.RuptureDate
			,target.RuptureTime = source.RuptureTime
			,target.CordProlapse = source.CordProlapse
			,target.PresentationLabour = source.PresentationLabour
			,target.ElectronicFh = source.ElectronicFh
			,target.FetalPhTest = source.FetalPhTest
			,target.FetalPh = source.FetalPh
			,target.Meconium = source.Meconium
			,target.CordBloodGas = source.CordBloodGas
			,target.TimeOfBirth = source.TimeOfBirth
			,target.BirthWeight = source.BirthWeight
			,target.BirthWeightCentile = source.BirthWeightCentile
			,target.Gestation = source.Gestation
			,target.GestationByDate = source.GestationByDate
			,target.GestationByUs = source.GestationByUs
			,target.GestUsCertain = source.GestUsCertain
			,target.CongenitalAbnorm = source.CongenitalAbnorm
			,target.CongenitalDesc = source.CongenitalDesc
			,target.BnScreen4Ind = source.BnScreen4Ind
			,target.PresentationDeliv = source.PresentationDeliv
			,target.BreechDiagnosis = source.BreechDiagnosis
			,target.BirthingPool = source.BirthingPool
			,target.MethodDelivery = source.MethodDelivery
			,target.IndicCaesarian = source.IndicCaesarian
			,target.Outcome = source.Outcome
			,target.PlaceDelivery = source.PlaceDelivery
			,target.ReasonForChange = source.ReasonForChange
			,target.ConductDel = source.ConductDel
			,target.DeliveryTeam = source.DeliveryTeam
			,target.DeliveryMidwife = source.DeliveryMidwife
			,target.SpecPlaceDelivery = source.SpecPlaceDelivery
			,target.BnScreen5Ind = source.BnScreen5Ind
			,target.ApgarOne = source.ApgarOne
			,target.ApgarFive = source.ApgarFive
			,target.ApgarTen = source.ApgarTen
			,target.OnsetRespiration = source.OnsetRespiration
			,target.ResusPressure = source.ResusPressure
			,target.ResusDrugs = source.ResusDrugs
			,target.ChildDischAddress = source.ChildDischAddress
			,target.ConsPaed = source.ConsPaed
			,target.InfCompl = source.InfCompl
			,target.InfComplText = source.InfComplText
			,target.InfResearch = source.InfResearch
			,target.MidwifeDelName = source.MidwifeDelName
			,target.MidwifeDelInitials = source.MidwifeDelInitials
			,target.MidwifeDelStatus = source.MidwifeDelStatus
			,target.ChildNumber = source.ChildNumber
			,target.IndexPage = source.IndexPage
			,target.HealthDistrict = source.HealthDistrict
			,target.TransferType = source.TransferType
			,target.HealthVisitor = source.HealthVisitor
			,target.Registration = source.Registration
			,target.BirthPlace = source.BirthPlace
			,target.BirthHA = source.BirthHA
			,target.OPCSBirthArea = source.OPCSBirthArea
			,target.OPCSResidArea = source.OPCSResidArea
			,target.BornAt = source.BornAt
			,target.LAWard = source.LAWard
			,target.AssessGest = source.AssessGest
			,target.AgreedGestFrom = source.AgreedGestFrom
			,target.AgreedGestTo = source.AgreedGestTo
			,target.HeadCircum = source.HeadCircum
			,target.HeadCircumCentile = source.HeadCircumCentile
			,target.Length = source.Length
			,target.LengthCentile = source.LengthCentile
			,target.DScreenInd = source.DScreenInd
			,target.HIP = source.HIP
			,target.Convulsions = source.Convulsions
			,target.OtherAbnorm = source.OtherAbnorm
			,target.Jaundice = source.Jaundice
			,target.Bilirubin = source.Bilirubin
			,target.DInfCompl = source.DInfCompl
			,target.DInfComplText = source.DInfComplText
			,target.CongAbsDisch = source.CongAbsDisch
			,target.DScreen3AInd = source.DScreen3AInd
			,target.SCBU = source.SCBU
			,target.TransCare = source.TransCare
			,target.Cord = source.Cord
			,target.Guthrie = source.Guthrie
			,target.GuthrieDate = source.GuthrieDate
			,target.Haemoglobinopathy = source.Haemoglobinopathy
			,target.HaemoglobinopathyDate = source.HaemoglobinopathyDate
			,target.Vitamink = source.Vitamink
			,target.VitaminkDate = source.VitaminkDate
			,target.VitaminkText = source.VitaminkText
			,target.InfHbTest = source.InfHbTest
			,target.InfHaemaglobin = source.InfHaemaglobin
			,target.InfBloodGroup = source.InfBloodGroup
			,target.InfCoombs = source.InfCoombs
			,target.DischargeExamination = source.DischargeExamination
			,target.Condition = source.Condition
			,target.DischExamComments = source.DischExamComments
			,target.HighestLoc = source.HighestLoc
			,target.Condition28Day = source.Condition28Day
			,target.Resarch28Day = source.Resarch28Day
			,target.Comments = source.Comments
			,target.Feeding28Day = source.Feeding28Day
			,target.NnCompl = source.NnCompl
			,target.NnComplText = source.NnComplText
			,target.DatePlacenta = source.DatePlacenta
			,target.TimePlacenta = source.TimePlacenta
			,target.BirthDurationLabour = source.BirthDurationLabour
			,target.BirthDuration2ndStage = source.BirthDuration2ndStage
			,target.BirthDuration3rdStage = source.BirthDuration3rdStage
			,target.HearingTest = source.HearingTest
			,target.dateHearing = source.dateHearing
			,target.HearingResult = source.HearingResult
			,target.ActualHospital = source.ActualHospital
			,target.BirthDuration1stStage = source.BirthDuration1stStage
			,target.Tab0 = source.Tab0
			,target.Tab1 = source.Tab1
			,target.Tab2 = source.Tab2
			,target.Tab3 = source.Tab3
			,target.tab4 = source.tab4
			,target.DelHospCode = source.DelHospCode
			,target.DelHospName = source.DelHospName
			,target.ChildHealthCode = source.ChildHealthCode
			,target.CordVenousPH = source.CordVenousPH
			,target.CordVenBaseDeficit = source.CordVenBaseDeficit
			,target.CordArterialPH = source.CordArterialPH
			,target.CordArtBaseDeficit = source.CordArtBaseDeficit
			,target.ThirdStageManagement = source.ThirdStageManagement
			,target.Inf1Complications = source.Inf1Complications
			,target.Inf2Complications = source.Inf2Complications
			,target.CreateBaby = source.CreateBaby
			,target.BnBoxCode = source.BnBoxCode
			,target.EcvLabour = source.EcvLabour
			,target.HighestLoc2002 = source.HighestLoc2002
			,target.BirthLocation = source.BirthLocation
			,target.DateCsDecided = source.DateCsDecided
			,target.TimeCsDecided = source.TimeCsDecided
			,target.TimeToCaesarian = source.TimeToCaesarian
			,target.MidwifeKnown = source.MidwifeKnown
			,target.GestationDays = source.GestationDays
			,target.GestByDateDays = source.GestByDateDays
			,target.GestByUsDays = source.GestByUsDays
			,target.AssessGestDays = source.AssessGestDays
			,target.AgreedGestFromDays = source.AgreedGestFromDays
			,target.FollowUpDay = source.FollowUpDay
			,target.DilationAtCs = source.DilationAtCs
			,target.TransWeight = source.TransWeight
			,target.TransFeeding = source.TransFeeding
			,target.CmwIDischWeight = source.CmwIDischWeight
			,target.CmwIDischCondition = source.CmwIDischCondition
			,target.PnSex = source.PnSex
			,target.PnDateOfBirth = source.PnDateOfBirth
			,target.PnAgeAtDischarge = source.PnAgeAtDischarge
			,target.BirthAnDate = source.BirthAnDate
			,target.IndicInstrumental = source.IndicInstrumental
			,target.ShoulderDystocia = source.ShoulderDystocia
			,target.ShoulderInjury = source.ShoulderInjury
			,target.ShoulderInjuryText = source.ShoulderInjuryText
			,target.CsReasonForDelay = source.CsReasonForDelay
			,target.Palpable5ths = source.Palpable5ths
			,target.FetalPosition = source.FetalPosition
			,target.Station = source.Station
			,target.Moulding = source.Moulding
			,target.Caput = source.Caput
			,target.LiquorColour = source.LiquorColour
			,target.DeliveryOfBaby = source.DeliveryOfBaby
			,target.CordEntanglement = source.CordEntanglement
			,target.Position = source.Position
			,target.NewbornExamination = source.NewbornExamination
			,target.NewbornExaminationDate = source.NewbornExaminationDate
			,target.NewbornExaminationTime = source.NewbornExaminationTime
			,target.ExaminationCompletedBy = source.ExaminationCompletedBy
			,target.ExaminationMidwifePin = source.ExaminationMidwifePin
			,target.ExaminationText = source.ExaminationText
			,target.Heart = source.Heart
			,target.Eyes = source.Eyes
			,target.Testes = source.Testes
			,target.BirthMarksBruises = source.BirthMarksBruises
			,target.BirthMarksBruisesText = source.BirthMarksBruisesText
			,target.NewbornReferral = source.NewbornReferral
			,target.CongenitalList = source.CongenitalList
			,target.RedBookCompleted = source.RedBookCompleted
			,target.InfantNhsNumber = source.InfantNhsNumber
			,target.InfantNhsNoValid = source.InfantNhsNoValid
			,target.InfantSex = source.InfantSex
			,target.InfantMaidenName = source.InfantMaidenName
			,target.InfantCurrentSurname = source.InfantCurrentSurname
			,target.InfantForenames = source.InfantForenames
			,target.InfantDateOfBirth = source.InfantDateOfBirth
			,target.InfantPlaceOfBirth = source.InfantPlaceOfBirth
			,target.InfantEthnicGroupPrefix = source.InfantEthnicGroupPrefix
			,target.InfantEthnicGroupBody = source.InfantEthnicGroupBody
			,target.InfantEthnicGroupOpcs = source.InfantEthnicGroupOpcs
			,target.InfantEthnicGroupPas = source.InfantEthnicGroupPas
			,target.InfantGpSurname = source.InfantGpSurname
			,target.InfantGpSequence = source.InfantGpSequence
			,target.InfantTraceStatus = source.InfantTraceStatus
			,target.InfantLocation = source.InfantLocation
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



﻿


Create procedure [ETL].[BuildOPBaseFertilityReference]
	
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


--more efficient to delete here than in the merge
delete
from
	OP.BaseFertilityReference
where
	not exists
	(
	select
		1
	from
		OP.BaseFertility
	where
		BaseFertility.MergeRecno = BaseFertilityReference.MergeRecno
	)


select
	@deleted = @@ROWCOUNT


merge
	OP.BaseFertilityReference target
using
	(
	select
		BaseFertility.MergeRecno
		,ContextID = Context.ContextID

		,EncounterDateID =
			coalesce(
				 EncounterDate.DateID

				,case
				when BaseFertility.EncounterDate is null
				then NullDate.DateID

				when BaseFertility.EncounterDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,TreatmentID = SourceValueID
		
		,BaseFertility.Created
		,BaseFertility.Updated
		,BaseFertility.ByWhom
	from
		OP.BaseFertility

	inner join WH.Context
	on	Context.ContextCode = BaseFertility.ContextCode

	left join WH.Calendar EncounterDate
	on	EncounterDate.TheDate = BaseFertility.EncounterDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join WH.Treatment
	on BaseFertility.TreatmentType = Treatment.TreatmentCode
	and BaseFertility.ContextCode = Treatment.ContextCode
	
	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			MergeRecno
			,ContextID 
			,EncounterDateID 
			,TreatmentID 
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeRecno
			,source.ContextID 
			,source.EncounterDateID 
			,source.TreatmentID 
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeRecno = source.MergeRecno
			,target.ContextID = source.ContextID
			,target.EncounterDateID = source.EncounterDateID
			,target.TreatmentID = source.TreatmentID
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats







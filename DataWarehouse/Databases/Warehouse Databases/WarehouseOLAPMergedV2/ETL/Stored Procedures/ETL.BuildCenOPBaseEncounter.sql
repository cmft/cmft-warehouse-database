﻿CREATE procedure [ETL].[BuildCenOPBaseEncounter]
as

-------------------------------------------------
--When		Who	What
--20140916	PDO	DirectorateCode and PurchaserCode are assigned as part of the allocation process therefore removed from UPDATE check
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


truncate table OP.ProcessList


select
	*
	,EncounterChecksum =
	checksum(
		 SourceUniqueID
		,SourcePatientNo
		,SourceEncounterNo
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,NHSNumber
		,NHSNumberStatusCode
		,DistrictNo
		,Postcode
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,DHACode
		,EthnicOriginCode
		,MaritalStatusCode
		,ReligionCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,SiteCode
		,AppointmentDate
		,AppointmentTime
		,ClinicCode
		,AdminCategoryCode
		,SourceOfReferralCode
		,ReasonForReferralCode
		,PriorityCode
		,FirstAttendanceFlag
		,DNACode
		,AppointmentStatusCode
		,CancelledByCode
		,TransportRequiredFlag
		,AttendanceOutcomeCode
		,AppointmentTypeCode
		,DisposalCode
		,ConsultantCode
		,SpecialtyCode
		,ReferralConsultantCode
		,ReferralSpecialtyCode
		,BookingTypeCode
		,CasenoteNo
		,AppointmentCreateDate
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,DoctorCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,PrimaryOperationCode
		,PrimaryProcedureDate
		,SecondaryProcedureCode1
		,SecondaryProcedureDate1
		,SecondaryProcedureCode2
		,SecondaryProcedureDate2
		,SecondaryProcedureCode3
		,SecondaryProcedureDate3
		,SecondaryProcedureCode4
		,SecondaryProcedureDate4
		,SecondaryProcedureCode5
		,SecondaryProcedureDate5
		,SecondaryProcedureCode6
		,SecondaryProcedureDate6
		,SecondaryProcedureCode7
		,SecondaryProcedureDate7
		,SecondaryProcedureCode8
		,SecondaryProcedureDate8
		,SecondaryProcedureCode9
		,SecondaryProcedureDate9
		,SecondaryProcedureCode10
		,SecondaryProcedureDate10
		,SecondaryProcedureCode11
		,SecondaryProcedureDate11
		--,PurchaserCode
		,ProviderCode
		,ContractSerialNo
		,ReferralDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,AppointmentCategoryCode
		,AppointmentCreatedBy
		,AppointmentCancelDate
		,LastRevisedDate
		,LastRevisedBy
		,OverseasStatusFlag
		,PatientChoiceCode
		,ScheduledCancelReasonCode
		,PatientCancelReason
		,DischargeDate
		,QM08StartWaitDate
		,QM08EndWaitDate
		,DestinationSiteCode
		,EBookingReferenceNo
		,InterfaceCode
		,Created
		,Updated
		,ByWhom
		,LocalAdminCategoryCode
		,PCTCode
		,LocalityCode
		,AgeCode
		,HRGCode
		,Cases
		,LengthOfWait
		,IsWardAttender
		,RTTActivity
		,RTTBreachStatusCode
		,ClockStartDate
		,RTTBreachDate
		,RTTTreated
		--,DirectorateCode
		,ReferralSpecialtyTypeCode
		,LastDNAorPatientCancelledDate
		,ReferredByCode
		,ReferrerCode
		,AppointmentOutcomeCode
		,MedicalStaffTypeCode
		,ContextCode
		,TreatmentFunctionCode
		,CommissioningSerialNo
		,DerivedFirstAttendanceFlag
		,AttendanceIdentifier
		,RegisteredGpAtAppointmentCode
		,RegisteredGpPracticeAtAppointmentCode
		,ReferredByConsultantCode
		,ReferredByGpCode
		,ReferredByGdpCode
		,PseudoPostcode
		,CCGCode 
		,EpisodicPostcode
		,GpCodeAtAppointment 
		,GpPracticeCodeAtAppointment
		,PostcodeAtAppointment
		,EpisodicSiteCode
		,AppointmentResidenceCCGCode
	)
into
	#Encounter
from
	(
	select
		 EncounterRecno = cast(EncounterRecno as int)
		,SourceUniqueID = cast(SourceUniqueID as varchar(50))
		,SourcePatientNo = cast(SourcePatientNo as varchar(20))
		,SourceEncounterNo = cast(SourceEncounterNo as varchar(20))
		,PatientTitle = cast(PatientTitle as varchar(10))
		,PatientForename = cast(PatientForename as varchar(100))
		,PatientSurname = cast(PatientSurname as varchar(100))
		,DateOfBirth = cast(DateOfBirth as datetime)
		,DateOfDeath = cast(DateOfDeath as smalldatetime)
		,SexCode = cast(SexCode as varchar(10))
		,NHSNumber = cast(NHSNumber as varchar(17))
		,NHSNumberStatusCode = cast(NHSNumberStatusCode as varchar(10))
		,DistrictNo = cast(DistrictNo as varchar(12))
		,Postcode = cast(Postcode as varchar(8))
		,PatientAddress1 = cast(PatientAddress1 as varchar(100))
		,PatientAddress2 = cast(PatientAddress2 as varchar(100))
		,PatientAddress3 = cast(PatientAddress3 as varchar(100))
		,PatientAddress4 = cast(PatientAddress4 as varchar(100))
		,DHACode = cast(DHACode as varchar(3))
		,EthnicOriginCode = cast(EthnicOriginCode as varchar(10))
		,MaritalStatusCode = cast(MaritalStatusCode as varchar(10))
		,ReligionCode = cast(ReligionCode as varchar(10))
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(8))
		,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(8))
		,SiteCode = cast(SiteCode as varchar(10))
		,AppointmentDate = cast(AppointmentDate as smalldatetime)
		,AppointmentTime = cast(AppointmentTime as smalldatetime)
		,ClinicCode = cast(ClinicCode as varchar(10))
		,AdminCategoryCode = cast(AdminCategoryCode as varchar(10))
		,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(10))
		,ReasonForReferralCode = cast(ReasonForReferralCode as varchar(10))
		,PriorityCode = cast(PriorityCode as varchar(10))
		,FirstAttendanceFlag = cast(FirstAttendanceFlag as varchar(10))
		,DNACode = cast(DNACode as varchar(10))
		,AppointmentStatusCode = cast(AppointmentStatusCode as varchar(10))
		,CancelledByCode = cast(CancelledByCode as varchar(10))
		,TransportRequiredFlag = cast(TransportRequiredFlag as varchar(10))
		,AttendanceOutcomeCode = cast(AttendanceOutcomeCode as varchar(10))
		,AppointmentTypeCode = cast(AppointmentTypeCode as varchar(10))
		,DisposalCode = cast(DisposalCode as varchar(10))
		,ConsultantCode = cast(ConsultantCode as varchar(20))
		,SpecialtyCode = cast(SpecialtyCode as varchar(10))
		,ReferralConsultantCode = cast(ReferralConsultantCode as varchar(20))
		,ReferralSpecialtyCode = cast(ReferralSpecialtyCode as varchar(10))
		,BookingTypeCode = cast(BookingTypeCode as varchar(10))
		,CasenoteNo = cast(CasenoteNo as varchar(16))
		,AppointmentCreateDate = cast(AppointmentCreateDate as smalldatetime)
		,EpisodicGpCode = cast(EpisodicGpCode as varchar(8))
		,EpisodicGpPracticeCode = cast(EpisodicGpPracticeCode as varchar(6))
		,DoctorCode = cast(DoctorCode as varchar(20))
		,PrimaryDiagnosisCode = cast(PrimaryDiagnosisCode as varchar(10))
		,SubsidiaryDiagnosisCode = cast(SubsidiaryDiagnosisCode as varchar(10))
		,SecondaryDiagnosisCode1 = cast(SecondaryDiagnosisCode1 as varchar(10))
		,SecondaryDiagnosisCode2 = cast(SecondaryDiagnosisCode2 as varchar(10))
		,SecondaryDiagnosisCode3 = cast(SecondaryDiagnosisCode3 as varchar(10))
		,SecondaryDiagnosisCode4 = cast(SecondaryDiagnosisCode4 as varchar(10))
		,SecondaryDiagnosisCode5 = cast(SecondaryDiagnosisCode5 as varchar(10))
		,SecondaryDiagnosisCode6 = cast(SecondaryDiagnosisCode6 as varchar(10))
		,SecondaryDiagnosisCode7 = cast(SecondaryDiagnosisCode7 as varchar(10))
		,SecondaryDiagnosisCode8 = cast(SecondaryDiagnosisCode8 as varchar(10))
		,SecondaryDiagnosisCode9 = cast(SecondaryDiagnosisCode9 as varchar(10))
		,SecondaryDiagnosisCode10 = cast(SecondaryDiagnosisCode10 as varchar(10))
		,SecondaryDiagnosisCode11 = cast(SecondaryDiagnosisCode11 as varchar(10))
		,SecondaryDiagnosisCode12 = cast(SecondaryDiagnosisCode12 as varchar(10))
		,PrimaryOperationCode = cast(PrimaryOperationCode as varchar(10))
		,PrimaryProcedureDate = cast(PrimaryProcedureDate as smalldatetime)
		,SecondaryProcedureCode1 = cast(SecondaryProcedureCode1 as varchar(10))
		,SecondaryProcedureDate1 = cast(SecondaryProcedureDate1 as smalldatetime)
		,SecondaryProcedureCode2 = cast(SecondaryProcedureCode2 as varchar(10))
		,SecondaryProcedureDate2 = cast(SecondaryProcedureDate2 as smalldatetime)
		,SecondaryProcedureCode3 = cast(SecondaryProcedureCode3 as varchar(10))
		,SecondaryProcedureDate3 = cast(SecondaryProcedureDate3 as smalldatetime)
		,SecondaryProcedureCode4 = cast(SecondaryProcedureCode4 as varchar(10))
		,SecondaryProcedureDate4 = cast(SecondaryProcedureDate4 as smalldatetime)
		,SecondaryProcedureCode5 = cast(SecondaryProcedureCode5 as varchar(10))
		,SecondaryProcedureDate5 = cast(SecondaryProcedureDate5 as smalldatetime)
		,SecondaryProcedureCode6 = cast(SecondaryProcedureCode6 as varchar(10))
		,SecondaryProcedureDate6 = cast(SecondaryProcedureDate6 as smalldatetime)
		,SecondaryProcedureCode7 = cast(SecondaryProcedureCode7 as varchar(10))
		,SecondaryProcedureDate7 = cast(SecondaryProcedureDate7 as smalldatetime)
		,SecondaryProcedureCode8 = cast(SecondaryProcedureCode8 as varchar(10))
		,SecondaryProcedureDate8 = cast(SecondaryProcedureDate8 as smalldatetime)
		,SecondaryProcedureCode9 = cast(SecondaryProcedureCode9 as varchar(10))
		,SecondaryProcedureDate9 = cast(SecondaryProcedureDate9 as smalldatetime)
		,SecondaryProcedureCode10 = cast(SecondaryProcedureCode10 as varchar(10))
		,SecondaryProcedureDate10 = cast(SecondaryProcedureDate10 as smalldatetime)
		,SecondaryProcedureCode11 = cast(SecondaryProcedureCode11 as varchar(10))
		,SecondaryProcedureDate11 = cast(SecondaryProcedureDate11 as smalldatetime)
		,PurchaserCode = cast(PurchaserCode as varchar(10))
		,ProviderCode = cast(ProviderCode as varchar(10))
		,ContractSerialNo = cast(ContractSerialNo as varchar(6))
		,ReferralDate = cast(ReferralDate as smalldatetime)
		,RTTPathwayID = cast(RTTPathwayID as varchar(25))
		,RTTPathwayCondition = cast(RTTPathwayCondition as varchar(20))
		,RTTStartDate = cast(RTTStartDate as smalldatetime)
		,RTTEndDate = cast(RTTEndDate as smalldatetime)
		,RTTSpecialtyCode = cast(RTTSpecialtyCode as varchar(10))
		,RTTCurrentProviderCode = cast(RTTCurrentProviderCode as varchar(10))
		,RTTCurrentStatusCode = cast(RTTCurrentStatusCode as varchar(10))
		,RTTCurrentStatusDate = cast(RTTCurrentStatusDate as smalldatetime)
		,RTTCurrentPrivatePatientFlag = cast(RTTCurrentPrivatePatientFlag as bit)
		,RTTOverseasStatusFlag = cast(RTTOverseasStatusFlag as bit)
		,RTTPeriodStatusCode = cast(RTTPeriodStatusCode as varchar(10))
		,AppointmentCategoryCode = cast(AppointmentCategoryCode as varchar(10))
		,AppointmentCreatedBy = cast(AppointmentCreatedBy as varchar(10))
		,AppointmentCancelDate = cast(AppointmentCancelDate as smalldatetime)
		,LastRevisedDate = cast(LastRevisedDate as smalldatetime)
		,LastRevisedBy = cast(LastRevisedBy as varchar(10))
		,OverseasStatusFlag = cast(OverseasStatusFlag as varchar(10))
		,PatientChoiceCode = cast(PatientChoiceCode as varchar(10))
		,ScheduledCancelReasonCode = cast(ScheduledCancelReasonCode as varchar(10))
		,PatientCancelReason = cast(PatientCancelReason as varchar(50))
		,DischargeDate = cast(DischargeDate as smalldatetime)
		,QM08StartWaitDate = cast(QM08StartWaitDate as smalldatetime)
		,QM08EndWaitDate = cast(QM08EndWaitDate as smalldatetime)
		,DestinationSiteCode = cast(DestinationSiteCode as varchar(10))
		,EBookingReferenceNo = cast(EBookingReferenceNo as varchar(50))
		,InterfaceCode = cast(InterfaceCode as varchar(5))
		,Created = cast(Created as datetime)
		,Updated = cast(Updated as datetime)
		,ByWhom = cast(ByWhom as varchar(50))
		,LocalAdminCategoryCode = cast(LocalAdminCategoryCode as varchar(10))
		,PCTCode = cast(PCTCode as varchar(10))
		,LocalityCode = cast(LocalityCode as varchar(10))
		,AgeCode = cast(AgeCode as varchar(27))
		,HRGCode = cast(HRGCode as varchar(10))
		,Cases = cast(Cases as int)
		,LengthOfWait = cast(LengthOfWait as int)
		,IsWardAttender = cast(IsWardAttender as bit)
		,RTTActivity = cast(RTTActivity as int)
		,RTTBreachStatusCode = cast(RTTBreachStatusCode as varchar(10))
		,ClockStartDate = cast(ClockStartDate as smalldatetime)
		,RTTBreachDate = cast(RTTBreachDate as smalldatetime)
		,RTTTreated = cast(RTTTreated as int)
		,DirectorateCode = cast(DirectorateCode as varchar(5))
		,ReferralSpecialtyTypeCode = cast(ReferralSpecialtyTypeCode as varchar(10))
		,LastDNAorPatientCancelledDate = cast(LastDNAorPatientCancelledDate as smalldatetime)
		,ReferredByCode = cast(ReferredByCode as varchar(10))
		,ReferrerCode = cast(ReferrerCode as varchar(10))
		,AppointmentOutcomeCode = cast(AppointmentOutcomeCode as varchar(10))
		,MedicalStaffTypeCode = cast(MedicalStaffTypeCode as varchar(10))
		,ContextCode = cast(ContextCode as varchar(8))
		,TreatmentFunctionCode = cast(TreatmentFunctionCode as varchar(10))
		,CommissioningSerialNo = cast(CommissioningSerialNo as varchar(10))
		,DerivedFirstAttendanceFlag = cast(DerivedFirstAttendanceFlag as varchar(1))
		,AttendanceIdentifier = cast(AttendanceIdentifier as varchar(40))
		,RegisteredGpAtAppointmentCode = cast(RegisteredGpAtAppointmentCode as varchar(8))
		,RegisteredGpPracticeAtAppointmentCode = cast(RegisteredGpPracticeAtAppointmentCode as varchar(6))
		,ReferredByConsultantCode = cast(ReferredByConsultantCode as varchar(10))
		,ReferredByGpCode = cast(ReferredByGpCode as varchar(10))
		,ReferredByGdpCode = cast(ReferredByGdpCode as varchar(10))
		,PseudoPostcode = cast(PseudoPostcode as varchar (8))
		,CCGCode = cast(CCGCode as varchar (10))
		,EpisodicPostcode  = cast(EpisodicPostcode as varchar (8))
		,GpCodeAtAppointment = cast(GpCodeAtAppointment as varchar (8))
		,GpPracticeCodeAtAppointment = cast(GpPracticeCodeAtAppointment as varchar (8))
		,PostcodeAtAppointment = cast(PostcodeAtAppointment as varchar (8))
		,EpisodicSiteCode = cast(EpisodicSiteCode as varchar(5))
		,AppointmentResidenceCCGCode = cast(AppointmentResidenceCCGCode as varchar)
	from
		ETL.TLoadCenOPBaseEncounter Encounter
	) Activity

CREATE UNIQUE CLUSTERED INDEX IX_TLoadCenOPBaseEncounter ON #Encounter
	(
	 EncounterRecno
	,ContextCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(AppointmentDate)
	,@LoadEndDate = MAX(AppointmentDate)
from
	#Encounter


merge
	OP.BaseEncounter target
using
	(
	select
		 EncounterRecno
		,SourceUniqueID
		,SourcePatientNo
		,SourceEncounterNo
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,NHSNumber
		,NHSNumberStatusCode
		,DistrictNo
		,Postcode
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,DHACode
		,EthnicOriginCode
		,MaritalStatusCode
		,ReligionCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,SiteCode
		,AppointmentDate
		,AppointmentTime
		,ClinicCode
		,AdminCategoryCode
		,SourceOfReferralCode
		,ReasonForReferralCode
		,PriorityCode
		,FirstAttendanceFlag
		,DNACode
		,AppointmentStatusCode
		,CancelledByCode
		,TransportRequiredFlag
		,AttendanceOutcomeCode
		,AppointmentTypeCode
		,DisposalCode
		,ConsultantCode
		,SpecialtyCode
		,ReferralConsultantCode
		,ReferralSpecialtyCode
		,BookingTypeCode
		,CasenoteNo
		,AppointmentCreateDate
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,DoctorCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,PrimaryOperationCode
		,PrimaryProcedureDate
		,SecondaryProcedureCode1
		,SecondaryProcedureDate1
		,SecondaryProcedureCode2
		,SecondaryProcedureDate2
		,SecondaryProcedureCode3
		,SecondaryProcedureDate3
		,SecondaryProcedureCode4
		,SecondaryProcedureDate4
		,SecondaryProcedureCode5
		,SecondaryProcedureDate5
		,SecondaryProcedureCode6
		,SecondaryProcedureDate6
		,SecondaryProcedureCode7
		,SecondaryProcedureDate7
		,SecondaryProcedureCode8
		,SecondaryProcedureDate8
		,SecondaryProcedureCode9
		,SecondaryProcedureDate9
		,SecondaryProcedureCode10
		,SecondaryProcedureDate10
		,SecondaryProcedureCode11
		,SecondaryProcedureDate11
		,PurchaserCode
		,ProviderCode
		,ContractSerialNo
		,ReferralDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,AppointmentCategoryCode
		,AppointmentCreatedBy
		,AppointmentCancelDate
		,LastRevisedDate
		,LastRevisedBy
		,OverseasStatusFlag
		,PatientChoiceCode
		,ScheduledCancelReasonCode
		,PatientCancelReason
		,DischargeDate
		,QM08StartWaitDate
		,QM08EndWaitDate
		,DestinationSiteCode
		,EBookingReferenceNo
		,InterfaceCode
		,Created
		,Updated
		,ByWhom
		,LocalAdminCategoryCode
		,PCTCode
		,LocalityCode
		,AgeCode
		,HRGCode
		,Cases
		,LengthOfWait
		,IsWardAttender
		,RTTActivity
		,RTTBreachStatusCode
		,ClockStartDate
		,RTTBreachDate
		,RTTTreated
		,DirectorateCode
		,ReferralSpecialtyTypeCode
		,LastDNAorPatientCancelledDate
		,ReferredByCode
		,ReferrerCode
		,AppointmentOutcomeCode
		,MedicalStaffTypeCode
		,ContextCode
		,TreatmentFunctionCode
		,CommissioningSerialNo
		,DerivedFirstAttendanceFlag
		,AttendanceIdentifier
		,RegisteredGpAtAppointmentCode
		,RegisteredGpPracticeAtAppointmentCode
		,ReferredByConsultantCode
		,ReferredByGpCode
		,ReferredByGdpCode
		,PseudoPostcode
		,CCGCode 
		,EpisodicPostCode
		,GpCodeAtAppointment 
		,GpPracticeCodeAtAppointment
		,PostcodeAtAppointment
		,EpisodicSiteCode
		,AppointmentResidenceCCGCode
	from
		#Encounter Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno

	when not matched by source
	and	target.ContextCode = 'CEN||PAS'
	and	target.AppointmentDate between @LoadStartDate and @LoadEndDate
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceEncounterNo
			,PatientTitle
			,PatientForename
			,PatientSurname
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,NHSNumber
			,NHSNumberStatusCode
			,DistrictNo
			,Postcode
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,DHACode
			,EthnicOriginCode
			,MaritalStatusCode
			,ReligionCode
			,RegisteredGpCode
			,RegisteredGpPracticeCode
			,SiteCode
			,AppointmentDate
			,AppointmentTime
			,ClinicCode
			,AdminCategoryCode
			,SourceOfReferralCode
			,ReasonForReferralCode
			,PriorityCode
			,FirstAttendanceFlag
			,DNACode
			,AppointmentStatusCode
			,CancelledByCode
			,TransportRequiredFlag
			,AttendanceOutcomeCode
			,AppointmentTypeCode
			,DisposalCode
			,ConsultantCode
			,SpecialtyCode
			,ReferralConsultantCode
			,ReferralSpecialtyCode
			,BookingTypeCode
			,CasenoteNo
			,AppointmentCreateDate
			,EpisodicGpCode
			,EpisodicGpPracticeCode
			,DoctorCode
			,PrimaryDiagnosisCode
			,SubsidiaryDiagnosisCode
			,SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			,PrimaryOperationCode
			,PrimaryProcedureDate
			,SecondaryProcedureCode1
			,SecondaryProcedureDate1
			,SecondaryProcedureCode2
			,SecondaryProcedureDate2
			,SecondaryProcedureCode3
			,SecondaryProcedureDate3
			,SecondaryProcedureCode4
			,SecondaryProcedureDate4
			,SecondaryProcedureCode5
			,SecondaryProcedureDate5
			,SecondaryProcedureCode6
			,SecondaryProcedureDate6
			,SecondaryProcedureCode7
			,SecondaryProcedureDate7
			,SecondaryProcedureCode8
			,SecondaryProcedureDate8
			,SecondaryProcedureCode9
			,SecondaryProcedureDate9
			,SecondaryProcedureCode10
			,SecondaryProcedureDate10
			,SecondaryProcedureCode11
			,SecondaryProcedureDate11
			,PurchaserCode
			,ProviderCode
			,ContractSerialNo
			,ReferralDate
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,RTTPeriodStatusCode
			,AppointmentCategoryCode
			,AppointmentCreatedBy
			,AppointmentCancelDate
			,LastRevisedDate
			,LastRevisedBy
			,OverseasStatusFlag
			,PatientChoiceCode
			,ScheduledCancelReasonCode
			,PatientCancelReason
			,DischargeDate
			,QM08StartWaitDate
			,QM08EndWaitDate
			,DestinationSiteCode
			,EBookingReferenceNo
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,LocalAdminCategoryCode
			,PCTCode
			,LocalityCode
			,AgeCode
			,HRGCode
			,Cases
			,LengthOfWait
			,IsWardAttender
			,RTTActivity
			,RTTBreachStatusCode
			,ClockStartDate
			,RTTBreachDate
			,RTTTreated
			,DirectorateCode
			,ReferralSpecialtyTypeCode
			,LastDNAorPatientCancelledDate
			,ReferredByCode
			,ReferrerCode
			,AppointmentOutcomeCode
			,MedicalStaffTypeCode
			,ContextCode
			,TreatmentFunctionCode
			,CommissioningSerialNo
			,DerivedFirstAttendanceFlag
			,AttendanceIdentifier
			,RegisteredGpAtAppointmentCode
			,RegisteredGpPracticeAtAppointmentCode
			,ReferredByConsultantCode
			,ReferredByGpCode
			,ReferredByGdpCode
			,PseudoPostcode
			,CCGCode 
			,EpisodicPostcode
			,GpCodeAtAppointment 
			,GpPracticeCodeAtAppointment
			,PostcodeAtAppointment
			,EpisodicSiteCode
			,AppointmentResidenceCCGCode
			)
		values
			(
			 source.EncounterRecno
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceEncounterNo
			,source.PatientTitle
			,source.PatientForename
			,source.PatientSurname
			,source.DateOfBirth
			,source.DateOfDeath
			,source.SexCode
			,source.NHSNumber
			,source.NHSNumberStatusCode
			,source.DistrictNo
			,source.Postcode
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.DHACode
			,source.EthnicOriginCode
			,source.MaritalStatusCode
			,source.ReligionCode
			,source.RegisteredGpCode
			,source.RegisteredGpPracticeCode
			,source.SiteCode
			,source.AppointmentDate
			,source.AppointmentTime
			,source.ClinicCode
			,source.AdminCategoryCode
			,source.SourceOfReferralCode
			,source.ReasonForReferralCode
			,source.PriorityCode
			,source.FirstAttendanceFlag
			,source.DNACode
			,source.AppointmentStatusCode
			,source.CancelledByCode
			,source.TransportRequiredFlag
			,source.AttendanceOutcomeCode
			,source.AppointmentTypeCode
			,source.DisposalCode
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.ReferralConsultantCode
			,source.ReferralSpecialtyCode
			,source.BookingTypeCode
			,source.CasenoteNo
			,source.AppointmentCreateDate
			,source.EpisodicGpCode
			,source.EpisodicGpPracticeCode
			,source.DoctorCode
			,source.PrimaryDiagnosisCode
			,source.SubsidiaryDiagnosisCode
			,source.SecondaryDiagnosisCode1
			,source.SecondaryDiagnosisCode2
			,source.SecondaryDiagnosisCode3
			,source.SecondaryDiagnosisCode4
			,source.SecondaryDiagnosisCode5
			,source.SecondaryDiagnosisCode6
			,source.SecondaryDiagnosisCode7
			,source.SecondaryDiagnosisCode8
			,source.SecondaryDiagnosisCode9
			,source.SecondaryDiagnosisCode10
			,source.SecondaryDiagnosisCode11
			,source.SecondaryDiagnosisCode12
			,source.PrimaryOperationCode
			,source.PrimaryProcedureDate
			,source.SecondaryProcedureCode1
			,source.SecondaryProcedureDate1
			,source.SecondaryProcedureCode2
			,source.SecondaryProcedureDate2
			,source.SecondaryProcedureCode3
			,source.SecondaryProcedureDate3
			,source.SecondaryProcedureCode4
			,source.SecondaryProcedureDate4
			,source.SecondaryProcedureCode5
			,source.SecondaryProcedureDate5
			,source.SecondaryProcedureCode6
			,source.SecondaryProcedureDate6
			,source.SecondaryProcedureCode7
			,source.SecondaryProcedureDate7
			,source.SecondaryProcedureCode8
			,source.SecondaryProcedureDate8
			,source.SecondaryProcedureCode9
			,source.SecondaryProcedureDate9
			,source.SecondaryProcedureCode10
			,source.SecondaryProcedureDate10
			,source.SecondaryProcedureCode11
			,source.SecondaryProcedureDate11
			,source.PurchaserCode
			,source.ProviderCode
			,source.ContractSerialNo
			,source.ReferralDate
			,source.RTTPathwayID
			,source.RTTPathwayCondition
			,source.RTTStartDate
			,source.RTTEndDate
			,source.RTTSpecialtyCode
			,source.RTTCurrentProviderCode
			,source.RTTCurrentStatusCode
			,source.RTTCurrentStatusDate
			,source.RTTCurrentPrivatePatientFlag
			,source.RTTOverseasStatusFlag
			,source.RTTPeriodStatusCode
			,source.AppointmentCategoryCode
			,source.AppointmentCreatedBy
			,source.AppointmentCancelDate
			,source.LastRevisedDate
			,source.LastRevisedBy
			,source.OverseasStatusFlag
			,source.PatientChoiceCode
			,source.ScheduledCancelReasonCode
			,source.PatientCancelReason
			,source.DischargeDate
			,source.QM08StartWaitDate
			,source.QM08EndWaitDate
			,source.DestinationSiteCode
			,source.EBookingReferenceNo
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,source.LocalAdminCategoryCode
			,source.PCTCode
			,source.LocalityCode
			,source.AgeCode
			,source.HRGCode
			,source.Cases
			,source.LengthOfWait
			,source.IsWardAttender
			,source.RTTActivity
			,source.RTTBreachStatusCode
			,source.ClockStartDate
			,source.RTTBreachDate
			,source.RTTTreated
			,source.DirectorateCode
			,source.ReferralSpecialtyTypeCode
			,source.LastDNAorPatientCancelledDate
			,source.ReferredByCode
			,source.ReferrerCode
			,source.AppointmentOutcomeCode
			,source.MedicalStaffTypeCode
			,source.ContextCode
			,source.TreatmentFunctionCode
			,source.CommissioningSerialNo
			,source.DerivedFirstAttendanceFlag
			,source.AttendanceIdentifier
			,source.RegisteredGpAtAppointmentCode
			,source.RegisteredGpPracticeAtAppointmentCode
			,source.ReferredByConsultantCode
			,source.ReferredByGpCode
			,source.ReferredByGdpCode
			,source.PseudoPostcode
			,source.CCGCode 
			,source.EpisodicPostCode
			,source.GpCodeAtAppointment 
			,source.GpPracticeCodeAtAppointment
			,source.PostcodeAtAppointment
			,source.EpisodicSiteCode
			,source.AppointmentResidenceCCGCode
			)

	when matched
	and	target.EncounterChecksum <> EncounterChecksum
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.PatientTitle = source.PatientTitle
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.SexCode = source.SexCode
			,target.NHSNumber = source.NHSNumber
			,target.NHSNumberStatusCode = source.NHSNumberStatusCode
			,target.DistrictNo = source.DistrictNo
			,target.Postcode = source.Postcode
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.DHACode = source.DHACode
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.ReligionCode = source.ReligionCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.SiteCode = source.SiteCode
			,target.AppointmentDate = source.AppointmentDate
			,target.AppointmentTime = source.AppointmentTime
			,target.ClinicCode = source.ClinicCode
			,target.AdminCategoryCode = source.AdminCategoryCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ReasonForReferralCode = source.ReasonForReferralCode
			,target.PriorityCode = source.PriorityCode
			,target.FirstAttendanceFlag = source.FirstAttendanceFlag
			,target.DNACode = source.DNACode
			,target.AppointmentStatusCode = source.AppointmentStatusCode
			,target.CancelledByCode = source.CancelledByCode
			,target.TransportRequiredFlag = source.TransportRequiredFlag
			,target.AttendanceOutcomeCode = source.AttendanceOutcomeCode
			,target.AppointmentTypeCode = source.AppointmentTypeCode
			,target.DisposalCode = source.DisposalCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ReferralConsultantCode = source.ReferralConsultantCode
			,target.ReferralSpecialtyCode = source.ReferralSpecialtyCode
			,target.BookingTypeCode = source.BookingTypeCode
			,target.CasenoteNo = source.CasenoteNo
			,target.AppointmentCreateDate = source.AppointmentCreateDate
			,target.EpisodicGpCode = source.EpisodicGpCode
			,target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
			,target.DoctorCode = source.DoctorCode
			,target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
			,target.SubsidiaryDiagnosisCode = source.SubsidiaryDiagnosisCode
			,target.SecondaryDiagnosisCode1 = source.SecondaryDiagnosisCode1
			,target.SecondaryDiagnosisCode2 = source.SecondaryDiagnosisCode2
			,target.SecondaryDiagnosisCode3 = source.SecondaryDiagnosisCode3
			,target.SecondaryDiagnosisCode4 = source.SecondaryDiagnosisCode4
			,target.SecondaryDiagnosisCode5 = source.SecondaryDiagnosisCode5
			,target.SecondaryDiagnosisCode6 = source.SecondaryDiagnosisCode6
			,target.SecondaryDiagnosisCode7 = source.SecondaryDiagnosisCode7
			,target.SecondaryDiagnosisCode8 = source.SecondaryDiagnosisCode8
			,target.SecondaryDiagnosisCode9 = source.SecondaryDiagnosisCode9
			,target.SecondaryDiagnosisCode10 = source.SecondaryDiagnosisCode10
			,target.SecondaryDiagnosisCode11 = source.SecondaryDiagnosisCode11
			,target.SecondaryDiagnosisCode12 = source.SecondaryDiagnosisCode12
			,target.PrimaryOperationCode = source.PrimaryOperationCode
			,target.PrimaryProcedureDate = source.PrimaryProcedureDate
			,target.SecondaryProcedureCode1 = source.SecondaryProcedureCode1
			,target.SecondaryProcedureDate1 = source.SecondaryProcedureDate1
			,target.SecondaryProcedureCode2 = source.SecondaryProcedureCode2
			,target.SecondaryProcedureDate2 = source.SecondaryProcedureDate2
			,target.SecondaryProcedureCode3 = source.SecondaryProcedureCode3
			,target.SecondaryProcedureDate3 = source.SecondaryProcedureDate3
			,target.SecondaryProcedureCode4 = source.SecondaryProcedureCode4
			,target.SecondaryProcedureDate4 = source.SecondaryProcedureDate4
			,target.SecondaryProcedureCode5 = source.SecondaryProcedureCode5
			,target.SecondaryProcedureDate5 = source.SecondaryProcedureDate5
			,target.SecondaryProcedureCode6 = source.SecondaryProcedureCode6
			,target.SecondaryProcedureDate6 = source.SecondaryProcedureDate6
			,target.SecondaryProcedureCode7 = source.SecondaryProcedureCode7
			,target.SecondaryProcedureDate7 = source.SecondaryProcedureDate7
			,target.SecondaryProcedureCode8 = source.SecondaryProcedureCode8
			,target.SecondaryProcedureDate8 = source.SecondaryProcedureDate8
			,target.SecondaryProcedureCode9 = source.SecondaryProcedureCode9
			,target.SecondaryProcedureDate9 = source.SecondaryProcedureDate9
			,target.SecondaryProcedureCode10 = source.SecondaryProcedureCode10
			,target.SecondaryProcedureDate10 = source.SecondaryProcedureDate10
			,target.SecondaryProcedureCode11 = source.SecondaryProcedureCode11
			,target.SecondaryProcedureDate11 = source.SecondaryProcedureDate11
			--,target.PurchaserCode = source.PurchaserCode
			,target.ProviderCode = source.ProviderCode
			,target.ContractSerialNo = source.ContractSerialNo
			,target.ReferralDate = source.ReferralDate
			,target.RTTPathwayID = source.RTTPathwayID
			,target.RTTPathwayCondition = source.RTTPathwayCondition
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.RTTSpecialtyCode = source.RTTSpecialtyCode
			,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
			,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
			,target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
			,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
			,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
			,target.RTTPeriodStatusCode = source.RTTPeriodStatusCode
			,target.AppointmentCategoryCode = source.AppointmentCategoryCode
			,target.AppointmentCreatedBy = source.AppointmentCreatedBy
			,target.AppointmentCancelDate = source.AppointmentCancelDate
			,target.LastRevisedDate = source.LastRevisedDate
			,target.LastRevisedBy = source.LastRevisedBy
			,target.OverseasStatusFlag = source.OverseasStatusFlag
			,target.PatientChoiceCode = source.PatientChoiceCode
			,target.ScheduledCancelReasonCode = source.ScheduledCancelReasonCode
			,target.PatientCancelReason = source.PatientCancelReason
			,target.DischargeDate = source.DischargeDate
			,target.QM08StartWaitDate = source.QM08StartWaitDate
			,target.QM08EndWaitDate = source.QM08EndWaitDate
			,target.DestinationSiteCode = source.DestinationSiteCode
			,target.EBookingReferenceNo = source.EBookingReferenceNo
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.LocalAdminCategoryCode = source.LocalAdminCategoryCode
			,target.PCTCode = source.PCTCode
			,target.LocalityCode = source.LocalityCode
			,target.AgeCode = source.AgeCode
			,target.HRGCode = source.HRGCode
			,target.Cases = source.Cases
			,target.LengthOfWait = source.LengthOfWait
			,target.IsWardAttender = source.IsWardAttender
			,target.RTTActivity = source.RTTActivity
			,target.RTTBreachStatusCode = source.RTTBreachStatusCode
			,target.ClockStartDate = source.ClockStartDate
			,target.RTTBreachDate = source.RTTBreachDate
			,target.RTTTreated = source.RTTTreated
			--,target.DirectorateCode = source.DirectorateCode
			,target.ReferralSpecialtyTypeCode = source.ReferralSpecialtyTypeCode
			,target.LastDNAorPatientCancelledDate = source.LastDNAorPatientCancelledDate
			,target.ReferredByCode = source.ReferredByCode
			,target.ReferrerCode = source.ReferrerCode
			,target.AppointmentOutcomeCode = source.AppointmentOutcomeCode
			,target.MedicalStaffTypeCode = source.MedicalStaffTypeCode
			,target.ContextCode = source.ContextCode
			,target.TreatmentFunctionCode = source.TreatmentFunctionCode
			,target.CommissioningSerialNo = source.CommissioningSerialNo
			,target.DerivedFirstAttendanceFlag = source.DerivedFirstAttendanceFlag
			,target.AttendanceIdentifier = source.AttendanceIdentifier
			,target.RegisteredGpAtAppointmentCode = source.RegisteredGpAtAppointmentCode
			,target.RegisteredGpPracticeAtAppointmentCode = source.RegisteredGpPracticeAtAppointmentCode
			,target.ReferredByConsultantCode = source.ReferredByConsultantCode
			,target.ReferredByGpCode = source.ReferredByGpCode
			,target.ReferredByGdpCode = source.ReferredByGdpCode
			,target.PseudoPostcode = source.PseudoPostcode
			,target.CCGCode =  source.CCGCode
			,target.EpisodicPostcode = source.EpisodicPostCode
			,target.GpCodeAtAppointment = source.GpCodeAtAppointment
			,target.GpPracticeCodeAtAppointment = source.GpPracticeCodeAtAppointment
			,target.PostcodeAtAppointment = source.PostcodeAtAppointment	
			,target.EpisodicSiteCode = source.EpisodicSiteCode	
			,target.AppointmentResidenceCCGCode = source.AppointmentResidenceCCGCode	

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
	into
		OP.ProcessList
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		OP.ProcessList
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



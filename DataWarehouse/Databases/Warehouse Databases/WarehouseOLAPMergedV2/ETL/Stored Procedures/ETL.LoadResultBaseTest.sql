﻿CREATE procedure [ETL].[LoadResultBaseTest]
as

-------------------------------------------------
--When		Who	What
--20150715	CH	Created procedure
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE
	(
	 MergeRecno int
	,Action nvarchar(10)
	);


merge
	Result.BaseTest target
using
	(
	select
		 TestRecno
		,ContextCode 
		,SourceUniqueID 
		,OrderSourceUniqueID
		,OrderRequestTime
		,OrderStatusCode 
		,PatientID 
		,PatientIdentifier 
		,DistrictNo 
		,CasenoteNumber 
		,NHSNumber
		,ConsultantCode
		,MainSpecialtyCode
		,SpecialtyCode
		,ProviderCode
		,LocationCode 
		,TestCode 
		,TestStatus 
		,InterfaceCode
		,TestChecksum = 
			checksum(
					 TestRecno
					,ContextCode
					,SourceUniqueID
					,OrderSourceUniqueID
					,OrderRequestTime
					,OrderStatusCode
					,PatientID
					,PatientIdentifier
					,DistrictNo
					,CasenoteNumber
					,NHSNumber
					,ConsultantCode
					,MainSpecialtyCode
					,SpecialtyCode
					,ProviderCode
					,LocationCode
					,TestCode
					,TestStatus
					,InterfaceCode
					)							
	from
		ETL.TLoadResultBaseTest
	) source
	on	source.ContextCode = target.ContextCode
	and	source.TestRecno = target.TestRecno

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			TestRecno
			,ContextCode
			,SourceUniqueID
			,OrderSourceUniqueID
			,OrderRequestTime
			,OrderStatusCode
			,PatientID
			,PatientIdentifier
			,DistrictNo
			,CasenoteNumber
			,NHSNumber
			,ConsultantCode
			,MainSpecialtyCode
			,SpecialtyCode
			,ProviderCode
			,LocationCode
			,TestCode
			,TestStatus
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,TestChecksum
			)
		values
			(
			 source.TestRecno
			,source.ContextCode
			,source.SourceUniqueID
			,source.OrderSourceUniqueID
			,source.OrderRequestTime
			,source.OrderStatusCode
			,source.PatientID
			,source.PatientIdentifier
			,source.DistrictNo
			,source.CasenoteNumber
			,source.NHSNumber
			,source.ConsultantCode
			,source.MainSpecialtyCode
			,source.SpecialtyCode
			,source.ProviderCode
			,source.LocationCode
			,source.TestCode
			,source.TestStatus
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,source.TestChecksum
			)

	when matched
	and	target.TestChecksum <> source.TestChecksum
	then
		update
		set
			 target.TestRecno = source.TestRecno
			,target.ContextCode = source.ContextCode
			,target.SourceUniqueID = source.SourceUniqueID
			,target.OrderSourceUniqueID = source.OrderSourceUniqueID
			,target.OrderRequestTime = source.OrderRequestTime
			,target.OrderStatusCode = source.OrderStatusCode
			,target.PatientID = source.PatientID
			,target.PatientIdentifier = source.PatientIdentifier
			,target.DistrictNo = source.DistrictNo
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.ConsultantCode = source.ConsultantCode
			,target.MainSpecialtyCode = source.MainSpecialtyCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ProviderCode = source.ProviderCode
			,target.LocationCode = source.LocationCode
			,target.TestCode = source.TestCode
			,target.TestStatus = source.TestStatus
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.TestChecksum = source.TestChecksum 

output
	 coalesce(inserted.MergeTestRecno, deleted.MergeTestRecno)
	,$action
	into
		@MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


--merge ProcessList in case it already has rows from a previous (failed) run
--merge
--	Result.ProcessList target
--using
--	@MergeSummary source
--on	source.MergeRecno = target.MergeRecno

--when not matched
--then
--	insert
--		(
--		 MergeRecno
--		,Action
--		)
--	values
--		(
--		 MergeRecno
--		,Action
--		)

--when matched
--then
--	update
--	set
--		target.Action = source.Action
;



select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




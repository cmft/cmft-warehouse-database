﻿
create procedure [ETL].[BuildRenalBasePatientLabPanelBoneAndMinerals]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||CV'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Renal.BasePatientLabPanelBoneAndMinerals target
using [$(Warehouse)].Renal.PatientLabPanelBoneAndMinerals source
on	source.PatientLabPanelBoneAndMineralsRecno = target.PatientLabPanelBoneAndMineralsRecno

when matched and not
(
	isnull(target.[SourceUniqueID], '') = isnull(source.[SourceUniqueID], '')
	and isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[LabTestObjectID], 0) = isnull(source.[LabTestObjectID], 0)
	and isnull(target.[LabTestDate], getdate()) = isnull(source.[LabTestDate], getdate())
	and isnull(target.[LabTestQualifier], '') = isnull(source.[LabTestQualifier], '')
	and isnull(target.[LabTestStatus], '') = isnull(source.[LabTestStatus], '')
	and isnull(target.[AluminumNum], 0) = isnull(source.[AluminumNum], 0)
	and isnull(target.[AluminumStr], '') = isnull(source.[AluminumStr], '')
	and isnull(target.[AluminumFlg], '') = isnull(source.[AluminumFlg], '')
	and isnull(target.[AluminumUnstimulatedNum], 0) = isnull(source.[AluminumUnstimulatedNum], 0)
	and isnull(target.[AluminumUnstimulatedStr], '') = isnull(source.[AluminumUnstimulatedStr], '')
	and isnull(target.[AluminumUnstimulatedFlg], '') = isnull(source.[AluminumUnstimulatedFlg], '')
	and isnull(target.[AluminumStimulatedNum], 0) = isnull(source.[AluminumStimulatedNum], 0)
	and isnull(target.[AluminumStimulatedStr], '') = isnull(source.[AluminumStimulatedStr], '')
	and isnull(target.[AluminumStimulatedFlg], '') = isnull(source.[AluminumStimulatedFlg], '')
	and isnull(target.[AlbuminNum], 0) = isnull(source.[AlbuminNum], 0)
	and isnull(target.[AlbuminStr], '') = isnull(source.[AlbuminStr], '')
	and isnull(target.[AlbuminFlg], '') = isnull(source.[AlbuminFlg], '')
	and isnull(target.[AlkPhosNum], 0) = isnull(source.[AlkPhosNum], 0)
	and isnull(target.[AlkPhosStr], '') = isnull(source.[AlkPhosStr], '')
	and isnull(target.[AlkPhosFlg], '') = isnull(source.[AlkPhosFlg], '')
	and isnull(target.[CalciumNum], 0) = isnull(source.[CalciumNum], 0)
	and isnull(target.[CalciumStr], '') = isnull(source.[CalciumStr], '')
	and isnull(target.[CalciumFlg], '') = isnull(source.[CalciumFlg], '')
	and isnull(target.[CaxPhosNum], 0) = isnull(source.[CaxPhosNum], 0)
	and isnull(target.[CaxPhosStr], '') = isnull(source.[CaxPhosStr], '')
	and isnull(target.[CaxPhosFlg], '') = isnull(source.[CaxPhosFlg], '')
	and isnull(target.[IonizedCaNum], 0) = isnull(source.[IonizedCaNum], 0)
	and isnull(target.[IonizedCaStr], '') = isnull(source.[IonizedCaStr], '')
	and isnull(target.[IonizedCaFlg], '') = isnull(source.[IonizedCaFlg], '')
	and isnull(target.[PhosNum], 0) = isnull(source.[PhosNum], 0)
	and isnull(target.[PhosStr], '') = isnull(source.[PhosStr], '')
	and isnull(target.[PhosFlg], '') = isnull(source.[PhosFlg], '')
	and isnull(target.[PTHIntactNum], 0) = isnull(source.[PTHIntactNum], 0)
	and isnull(target.[PTHIntactStr], '') = isnull(source.[PTHIntactStr], '')
	and isnull(target.[PTHIntactFlg], '') = isnull(source.[PTHIntactFlg], '')
	and isnull(target.[CAadjNum], 0) = isnull(source.[CAadjNum], 0)
	and isnull(target.[CAadjStr], '') = isnull(source.[CAadjStr], '')
	and isnull(target.[CAadjFlg], '') = isnull(source.[CAadjFlg], '')
	and isnull(target.[CaxPhosadjNum], 0) = isnull(source.[CaxPhosadjNum], 0)
	and isnull(target.[CaxPhosadjStr], '') = isnull(source.[CaxPhosadjStr], '')
	and isnull(target.[CaxPhosadjFlg], '') = isnull(source.[CaxPhosadjFlg], '')
	and isnull(target.[BIPTHNum], 0) = isnull(source.[BIPTHNum], 0)
	and isnull(target.[BIPTHStr], '') = isnull(source.[BIPTHStr], '')
	and isnull(target.[BIPTHFlg], '') = isnull(source.[BIPTHFlg], '')
)
then update set
	target.[SourceUniqueID] = source.[SourceUniqueID]
	,target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[LabTestObjectID] = source.[LabTestObjectID]
	,target.[LabTestDate] = source.[LabTestDate]
	,target.[LabTestQualifier] = source.[LabTestQualifier]
	,target.[LabTestStatus] = source.[LabTestStatus]
	,target.[AluminumNum] = source.[AluminumNum]
	,target.[AluminumStr] = source.[AluminumStr]
	,target.[AluminumFlg] = source.[AluminumFlg]
	,target.[AluminumUnstimulatedNum] = source.[AluminumUnstimulatedNum]
	,target.[AluminumUnstimulatedStr] = source.[AluminumUnstimulatedStr]
	,target.[AluminumUnstimulatedFlg] = source.[AluminumUnstimulatedFlg]
	,target.[AluminumStimulatedNum] = source.[AluminumStimulatedNum]
	,target.[AluminumStimulatedStr] = source.[AluminumStimulatedStr]
	,target.[AluminumStimulatedFlg] = source.[AluminumStimulatedFlg]
	,target.[AlbuminNum] = source.[AlbuminNum]
	,target.[AlbuminStr] = source.[AlbuminStr]
	,target.[AlbuminFlg] = source.[AlbuminFlg]
	,target.[AlkPhosNum] = source.[AlkPhosNum]
	,target.[AlkPhosStr] = source.[AlkPhosStr]
	,target.[AlkPhosFlg] = source.[AlkPhosFlg]
	,target.[CalciumNum] = source.[CalciumNum]
	,target.[CalciumStr] = source.[CalciumStr]
	,target.[CalciumFlg] = source.[CalciumFlg]
	,target.[CaxPhosNum] = source.[CaxPhosNum]
	,target.[CaxPhosStr] = source.[CaxPhosStr]
	,target.[CaxPhosFlg] = source.[CaxPhosFlg]
	,target.[IonizedCaNum] = source.[IonizedCaNum]
	,target.[IonizedCaStr] = source.[IonizedCaStr]
	,target.[IonizedCaFlg] = source.[IonizedCaFlg]
	,target.[PhosNum] = source.[PhosNum]
	,target.[PhosStr] = source.[PhosStr]
	,target.[PhosFlg] = source.[PhosFlg]
	,target.[PTHIntactNum] = source.[PTHIntactNum]
	,target.[PTHIntactStr] = source.[PTHIntactStr]
	,target.[PTHIntactFlg] = source.[PTHIntactFlg]
	,target.[CAadjNum] = source.[CAadjNum]
	,target.[CAadjStr] = source.[CAadjStr]
	,target.[CAadjFlg] = source.[CAadjFlg]
	,target.[CaxPhosadjNum] = source.[CaxPhosadjNum]
	,target.[CaxPhosadjStr] = source.[CaxPhosadjStr]
	,target.[CaxPhosadjFlg] = source.[CaxPhosadjFlg]
	,target.[BIPTHNum] = source.[BIPTHNum]
	,target.[BIPTHStr] = source.[BIPTHStr]
	,target.[BIPTHFlg] = source.[BIPTHFlg]
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	 [SourceUniqueID]
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[LabTestObjectID]
	,[LabTestDate]
	,[LabTestQualifier]
	,[LabTestStatus]
	,[AluminumNum]
	,[AluminumStr]
	,[AluminumFlg]
	,[AluminumUnstimulatedNum]
	,[AluminumUnstimulatedStr]
	,[AluminumUnstimulatedFlg]
	,[AluminumStimulatedNum]
	,[AluminumStimulatedStr]
	,[AluminumStimulatedFlg]
	,[AlbuminNum]
	,[AlbuminStr]
	,[AlbuminFlg]
	,[AlkPhosNum]
	,[AlkPhosStr]
	,[AlkPhosFlg]
	,[CalciumNum]
	,[CalciumStr]
	,[CalciumFlg]
	,[CaxPhosNum]
	,[CaxPhosStr]
	,[CaxPhosFlg]
	,[IonizedCaNum]
	,[IonizedCaStr]
	,[IonizedCaFlg]
	,[PhosNum]
	,[PhosStr]
	,[PhosFlg]
	,[PTHIntactNum]
	,[PTHIntactStr]
	,[PTHIntactFlg]
	,[CAadjNum]
	,[CAadjStr]
	,[CAadjFlg]
	,[CaxPhosadjNum]
	,[CaxPhosadjStr]
	,[CaxPhosadjFlg]
	,[BIPTHNum]
	,[BIPTHStr]
	,[BIPTHFlg]
	,[PatientLabPanelBoneAndMineralsRecno]
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[SourceUniqueID]
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[LabTestObjectID]
	,source.[LabTestDate]
	,source.[LabTestQualifier]
	,source.[LabTestStatus]
	,source.[AluminumNum]
	,source.[AluminumStr]
	,source.[AluminumFlg]
	,source.[AluminumUnstimulatedNum]
	,source.[AluminumUnstimulatedStr]
	,source.[AluminumUnstimulatedFlg]
	,source.[AluminumStimulatedNum]
	,source.[AluminumStimulatedStr]
	,source.[AluminumStimulatedFlg]
	,source.[AlbuminNum]
	,source.[AlbuminStr]
	,source.[AlbuminFlg]
	,source.[AlkPhosNum]
	,source.[AlkPhosStr]
	,source.[AlkPhosFlg]
	,source.[CalciumNum]
	,source.[CalciumStr]
	,source.[CalciumFlg]
	,source.[CaxPhosNum]
	,source.[CaxPhosStr]
	,source.[CaxPhosFlg]
	,source.[IonizedCaNum]
	,source.[IonizedCaStr]
	,source.[IonizedCaFlg]
	,source.[PhosNum]
	,source.[PhosStr]
	,source.[PhosFlg]
	,source.[PTHIntactNum]
	,source.[PTHIntactStr]
	,source.[PTHIntactFlg]
	,source.[CAadjNum]
	,source.[CAadjStr]
	,source.[CAadjFlg]
	,source.[CaxPhosadjNum]
	,source.[CaxPhosadjStr]
	,source.[CaxPhosadjFlg]
	,source.[BIPTHNum]
	,source.[BIPTHStr]
	,source.[BIPTHFlg]
	,source.[PatientLabPanelBoneAndMineralsRecno]
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿
CREATE Procedure ETL.ImportResultAcuteKidneyInjuryAlert 
	@PeriodStart date = null, @PeriodEnd date = null

as


declare @Start date = coalesce(@PeriodStart,dateadd(day,-750,getdate()))
declare @End date = coalesce(@PeriodEnd,getdate())


exec APC.BuildBaseEncounterBaseResult

exec AE.BuildBaseEncounterBaseResult 

exec Result.BuildWrkAcuteKidneyInjuryAlert @Start ,@End

exec ETL.BuildResultBaseAcuteKidneyInjuryAlertNational

exec ETL.BuildResultBaseAcuteKidneyInjuryAlert


﻿
CREATE procedure [ETL].[BuildCenGUMBaseEncounter]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge GUM.BaseEncounter target
using 
	(
	select
		EncounterRecno
		,AppointmentDate
		,AppointmentTime
		,PatientIdentifier
		,Postcode
		,ClinicID
		,AppointmentTypeID
		,AttendanceStatusID
		,InterfaceCode
		,ContextCode
		,Created
		,Updated
		,ByWhom
	from
		ETL.TLoadCenGUMBaseEncounter

		) source
	on	source.EncounterRecno = target.EncounterRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CEN||MCSH'
	then delete

	when not matched 
	then
		insert
			(
			 EncounterRecno
			,AppointmentDate
			,AppointmentTime
			,PatientIdentifier
			,Postcode
			,ClinicID
			,AppointmentTypeID
			,AttendanceStatusID
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)

		values
			(
			 source.EncounterRecno
			,source.AppointmentDate
			,source.AppointmentTime
			,source.PatientIdentifier
			,source.Postcode
			,source.ClinicID
			,source.AppointmentTypeID
			,source.AttendanceStatusID
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.AppointmentDate, getdate()) = isnull(source.AppointmentDate, getdate())
		and isnull(target.AppointmentTime, getdate()) = isnull(source.AppointmentTime, getdate())
		and isnull(target.PatientIdentifier, '') = isnull(source.PatientIdentifier, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.ClinicID, 0) = isnull(source.ClinicID, 0)
		and isnull(target.AppointmentTypeID, 0) = isnull(source.AppointmentTypeID, 0)
		and isnull(target.AttendanceStatusID, 0) = isnull(source.AttendanceStatusID, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')		)
	
	then 
		update 
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.AppointmentDate = source.AppointmentDate
			,target.AppointmentTime = source.AppointmentTime
			,target.PatientIdentifier = source.PatientIdentifier
			,target.Postcode = source.Postcode
			,target.ClinicID = source.ClinicID
			,target.AppointmentTypeID = source.AppointmentTypeID
			,target.AttendanceStatusID = source.AttendanceStatusID
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Created = source.Created
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


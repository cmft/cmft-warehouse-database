﻿
CREATE Procedure ETL.BuildResultBaseAcuteKidneyInjuryAlertNational
As

-- Rule 1 -- Lowest in last 7 days
-- Previous Result 0-7 days find lowest of all results within the 7 day period (advised by Leonard Ebah 25/09/2014)

Select
	Result.MergeResultRecno
	,Result.EffectiveTime
	,Result.ResultTime
	,Result.Result
	,Result.PatientSequence
	,PreviousResultEffectiveTime = PreviousResult.EffectiveTime
	,PreviousResultTime = PreviousResult.ResultTime
	,PreviousResult = PreviousResult.Result
	,PreviousPatientSequence = PreviousResult.PatientSequence
into 
	#ResultLowest7days
from
	Result.WrkAcuteKidneyInjuryAlert Result

inner join Result.WrkAcuteKidneyInjuryAlert PreviousResult
on Result.DistrictNo = PreviousResult.DistrictNo
and Result.MergeResultRecno <> PreviousResult.MergeResultRecno
and PreviousResult.EffectiveTime < Result.EffectiveTime
and datediff(day,PreviousResult.EffectiveTime,Result.EffectiveTime) between 0 and 7


CREATE NONCLUSTERED INDEX [IX_ResultLowest7Temp_MergeResultRecno_PreviousResult_PreviousPatientSequence]
ON [dbo].[#ResultLowest7days] ([MergeResultRecno],[PreviousResult],[PreviousPatientSequence])



-- Rule 2 Median

select
	Result.MergeResultRecno
	,C1Result = Result.Result
	,PreviousResult = PreviousResult.Result	
	,OrderNo = row_number() over (partition by Result.MergeResultRecno order by PreviousResult.Result)
into
	#ResultMedian
from
	Result.WrkAcuteKidneyInjuryAlert Result

inner join Result.WrkAcuteKidneyInjuryAlert PreviousResult
on Result.DistrictNo = PreviousResult.DistrictNo

where 
	Result.MergeResultRecno not in 
			(
			select
				Latest.MergeResultRecno
			from 
				Result.WrkAcuteKidneyInjuryAlert Latest
			
			inner join Result.WrkAcuteKidneyInjuryAlert Previous
			on Latest.DistrictNo = Previous.DistrictNo
			and Latest.MergeResultRecno <> Previous.MergeResultRecno
			and Previous.EffectiveTime < Latest.EffectiveTime
			and datediff(day,Previous.EffectiveTime, Latest.EffectiveTime) between 0 and 7
			)
and datediff(day,PreviousResult.EffectiveTime,Result.EffectiveTime) between 8 and 365


-- Rule 4 -- 48hr rule

select
	Result.MergeResultRecno
	,Result.EffectiveTime
	,Result.ResultTime
	,Result.Result
	,Result.PatientSequence
	,PreviousEffectiveTime = PreviousResult.EffectiveTime
	,PreviousResultTime = PreviousResult.ResultTime
	,PreviousResult = PreviousResult.Result				
	,PreviousPatientSequence = PreviousResult.PatientSequence
into
	#Result48Change
from
	Result.WrkAcuteKidneyInjuryAlert Result

inner join Result.WrkAcuteKidneyInjuryAlert PreviousResult
on Result.DistrictNo = PreviousResult.DistrictNo
and Result.MergeResultRecno <> PreviousResult.MergeResultRecno
and PreviousResult.EffectiveTime < Result.EffectiveTime
and datediff(mi,PreviousResult.EffectiveTime,Result.EffectiveTime) between 0 and 2880


CREATE NONCLUSTERED INDEX [IX_Result48Change_MergeResultRecno_PreviousResult_PreviousPatientSequence]
ON [dbo].[#Result48Change] ([MergeResultRecno],[PreviousResult],[PreviousPatientSequence])	



Truncate table Result.BaseAcuteKidneyInjuryAlertNational

Insert into Result.BaseAcuteKidneyInjuryAlertNational
	
Select
	MergeResultRecno
	,C1Result  
	,RatioDescription 
	,ResultForRatio 
	,RVRatio 
	,Alert
from
	(
	select
		Low.MergeResultRecno
		,C1Result = Result
		,RatioDescription = 'Lowest0-7days'
		,ResultForRatio = 
			case
				when Result < PreviousResult
				then Result
				else PreviousResult
			end
		,RVRatio = 
			cast(Result as decimal (12,6))
			/
			cast(
				case
					when Result < PreviousResult
					then Result
					else PreviousResult
				end
			as decimal(12,6))
		,Change.Alert
	from
		#ResultLowest7days Low

	left join
		(
		Select
			MergeResultRecno
			,Alert = 
				case
					when Result - PreviousResult > 26 then 'AKI 1'
					else Null
				end
		from
			#Result48Change Change
		where
			not exists
				(
				Select
					1
				from
					#Result48Change Lowest
				where
					Lowest.MergeResultRecno = Change.MergeResultRecno
				and Lowest.PreviousResult < Change.PreviousResult
				)
		and not exists
				(
				Select
					1
				from
					#Result48Change Latest
				where
					Latest.MergeResultRecno = Change.MergeResultRecno
				and Latest.PreviousResult = Change.PreviousResult
				and Latest.PreviousPatientSequence > Change.PreviousPatientSequence
				)
			) Change
	on Low.MergeResultRecno = Change.MergeResultRecno
	
	where
		not exists -- Lowest
			(
			Select 
				1
			from
				#ResultLowest7days Lowest
			where
				Lowest.MergeResultRecno = Low.MergeResultRecno
			and Lowest.PreviousResult < Low.PreviousResult
			)
	and	not exists -- Latest
			(
			Select 
				1
			from
				#ResultLowest7days Latest
			where
				Latest.MergeResultRecno = Low.MergeResultRecno
			and Latest.PreviousResult = Low.PreviousResult
			and Latest.PreviousPatientSequence > Low.PreviousPatientSequence
			)
	union
	
	Select
		MergeResultRecno = CTE.MergeResultRecno
		,C1Result 
		,RatioDescription = 'Median8-365days'
		,ResultForRatio = PreviousResult -- where the order = median, ie the median result
		,RVRatio = cast(C1Result as decimal (12,6))
					/
					cast(PreviousResult as decimal (12,6))
		,Alert = null
	from 
		#ResultMedian CTE
	inner join
		(select 
			MergeResultRecno
			,MedianValue = cast(round(((max(OrderNo)+0.5)/2),0) as int)
		from 
			#ResultMedian
		group by 
			MergeResultRecno
		)Median
	on CTE.MergeResultRecno = Median.MergeResultRecno
	and CTE.OrderNo = Median.MedianValue

	union
	
	select 
		MergeResultRecno
		,C1Result = Result.Result
		,RatioDescription = 'Baseline'
		,ResultForRatio = 
			case 
				when SexCode = 1 then 80
				when SexCode = 2 then 60
				else 70
			end
		,RVRatio = 
			case 
				when SexCode = 1 then 
										cast(Result.Result as decimal (12,6))
										/
										cast(80 as decimal(12,6))
				when SexCode = 2 then
										cast(Result.Result as decimal (12,6))
										/
										cast(60 as decimal(12,6))
				else 
					cast(Result.Result as decimal (12,6))
					/
					cast(70 as decimal(12,6))
			end
		,Alert = null
	from
		Result.WrkAcuteKidneyInjuryAlert Result
	where 
		Result.MergeResultRecno not in 
				(select 
					Latest.MergeResultRecno
				from 
					Result.WrkAcuteKidneyInjuryAlert Latest
				
				inner join Result.WrkAcuteKidneyInjuryAlert Previous
				on Latest.DistrictNo = Previous.DistrictNo
				and Latest.MergeResultRecno <> Previous.MergeResultRecno
				and Previous.EffectiveTime < Latest.EffectiveTime
				and datediff(day,Previous.EffectiveTime, Latest.EffectiveTime) between 0 and 365
				)

		) NationalAlert




﻿
CREATE procedure [ETL].[ImportCenAPCBaseCriticalCarePeriod] as

--import the data
exec ETL.BuildCenAPCBaseCriticalCarePeriod


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseCriticalCarePeriod]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCBaseCriticalCarePeriodReference 'CEN||PAS'
exec ETL.BuildAPCBaseCriticalCarePeriodReference 'CMFT||MID' --'CEN||MID'


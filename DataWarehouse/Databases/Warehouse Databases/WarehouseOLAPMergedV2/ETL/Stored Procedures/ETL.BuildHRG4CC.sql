﻿










CREATE procedure [ETL].[BuildHRG4CC] as

/* Process CC Encounter */

-- Delete records with no associated CC Encounter record

delete from APC.HRG4CriticalCareEncounter
where
	not exists
	(
	select
		1
	from
		APC.BaseCriticalCarePeriod Encounter
	where
		Encounter.MergeEncounterRecno = HRG4CriticalCareEncounter.MergeEncounterRecno
	)

-- Delete records that are about to be reinserted

delete from APC.HRG4CriticalCareEncounter
where
	exists
	(
	select
		1
	from
		ETL.HRG4CCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4CriticalCareEncounter.MergeEncounterRecno
	)


insert into APC.HRG4CriticalCareEncounter
	(
	 MergeEncounterRecno
	,HRGCode
	,CriticalCareDays
	,CriticalCareWarningFlag
	,Created
	,ByWhom

	)
select
	 Encounter.MergeEncounterRecno
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,CriticalCareDays
	,case when Encounter.CriticalCareWarningFlag = '' then null else Encounter.CriticalCareWarningFlag end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4CCEncounter Encounter


/* Process CC Quality */

-- Delete records with no associated CC Encounter record

delete from APC.HRG4CriticalCareQuality
where
	not exists
	(
	select
		1
	from
		APC.BaseCriticalCarePeriod Encounter
	where
		Encounter.MergeEncounterRecno = HRG4CriticalCareQuality.MergeEncounterRecno
	)


-- Delete records that might be reinserted (if we don't use this method then rows in quality table will remain even if sucessfully processed subsequently)

delete from APC.HRG4CriticalCareQuality
where
	exists
	(
	select
		1
	from
		ETL.HRG4CCEncounter Encounter
	where
		Encounter.MergeEncounterRecno = HRG4CriticalCareQuality.MergeEncounterRecno
	)



-- Delete records that are about to be reinserted

--delete from APC.HRG4CriticalCareQuality
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.HRG4CCQuality Quality

--	inner join ETL.HRG4CCEncounter Encounter
--	on	Encounter.RowNo = Quality.RowNo
--	and Encounter.FinancialYear = Quality.FinancialYear

--	where
--		Encounter.MergeEncounterRecno = HRG4CriticalCareQuality.MergeEncounterRecno
--	)


insert into APC.HRG4CriticalCareQuality
	(
	 MergeEncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,Quality.SequenceNo
	,case when Quality.QualityTypeCode = '' then null else Quality.QualityTypeCode end
	,case when Quality.QualityCode = '' then null else Quality.QualityCode end
	,case when Quality.QualityMessage = '' then null else Quality.QualityMessage end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4CCQuality Quality

	inner join ETL.HRG4CCEncounter Encounter
	on	Encounter.RowNo = Quality.RowNo
	and Encounter.FinancialYear = Quality.FinancialYear












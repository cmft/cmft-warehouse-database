﻿
CREATE procedure [ETL].[BuildCenPatientBaseDuplication]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||PAS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Patient.BaseDuplication target
using
	(
		select
			[PatientDuplicationID]
			,[SourcePatientNo]
			,[MasterSourcePatientNo]
			,[RegistrationDate]
			,[IsEarliestRegistration]
			,[ContextCode]
		from
			ETL.TLoadCenPatientBaseDuplication
	) source
	
on	source.PatientDuplicationID = target.PatientDuplicationID
and	source.ContextCode = target.ContextCode

when not matched by source
and	target.ContextCode = 'CEN||PAS'
then delete

when not matched 
then insert
(
	 [PatientDuplicationID]
	,[ContextCode]
	,[SourcePatientNo]
	,[MasterSourcePatientNo]
	,[RegistrationDate]
	,IsEarliestRegistration
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[PatientDuplicationID]
	,source.[ContextCode]
	,source.[SourcePatientNo]
	,source.[MasterSourcePatientNo]
	,source.[RegistrationDate]
	,source.IsEarliestRegistration
	,getdate()
	,getdate()
	,suser_name()
)


when matched and not
(
	isnull(target.[SourcePatientNo], 0) = isnull(source.[SourcePatientNo], 0)
	and isnull(target.[MasterSourcePatientNo], 0) = isnull(source.[MasterSourcePatientNo], 0)
	and isnull(target.[RegistrationDate], getdate()) = isnull(source.[RegistrationDate], getdate())
	and isnull(target.[IsEarliestRegistration], '') = isnull(source.[IsEarliestRegistration], '')
)
	
then update

set
	target.[SourcePatientNo] = source.[SourcePatientNo]
	,target.[MasterSourcePatientNo] = source.[MasterSourcePatientNo]
	,target.[RegistrationDate] = source.[RegistrationDate]
	,target.IsEarliestRegistration = source.IsEarliestRegistration
	,target.Updated = getdate()
	,target.ByWhom = suser_name()

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿

CREATE procedure [ETL].[BuildCenAPCBaseCriticalCarePeriod]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	APC.BaseCriticalCarePeriod target
using
	(
	select
		 SourceUniqueID = cast(SourceUniqueID as varchar)
		,SourceSpellNo = cast(SourceSpellNo as varchar)
		,SourcePatientNo = cast(SourcePatientNo as varchar)
		,StartDate = cast(StartDate as smalldatetime)
		,StartTime = cast(StartTime as smalldatetime)
		,EndDate = cast(EndDate as smalldatetime)
		,EndTime = cast(EndTime as smalldatetime)
		,AdvancedCardiovascularSupportDays = cast(AdvancedCardiovascularSupportDays as smallint)
		,AdvancedRespiratorySupportDays = cast(AdvancedRespiratorySupportDays as smallint)
		,BasicCardiovascularSupportDays = cast(BasicCardiovascularSupportDays as smallint)
		,BasicRespiratorySupportDays = cast(BasicRespiratorySupportDays as smallint)
		,CriticalCareLevel2Days = cast(CriticalCareLevel2Days as smallint)
		,CriticalCareLevel3Days = cast(CriticalCareLevel3Days as smallint)
		,DermatologicalSupportDays = cast(DermatologicalSupportDays as smallint)
		,LiverSupportDays = cast(LiverSupportDays as smallint)
		,NeurologicalSupportDays = cast(NeurologicalSupportDays as smallint)
		,RenalSupportDays = cast(RenalSupportDays as smallint)
		,CreatedByUser = cast(CreatedByUser as varchar)
		,CreatedByTime = cast(CreatedByTime as varchar)
		,LocalIdentifier = cast(LocalIdentifier as varchar)
		,LocationCode = cast(LocationCode as varchar)
		,StatusCode = cast(StatusCode as varchar)
		,TreatmentFunctionCode = cast(TreatmentFunctionCode as varchar)
		,PlannedAcpPeriod = cast(PlannedAcpPeriod as varchar)
		,InterfaceCode = cast(InterfaceCode as varchar)
		,CasenoteNumber = cast(CasenoteNumber as varchar)
		,WardCode = cast(WardCode as varchar)
		,AdmissionDate = cast(AdmissionDate as date)
		,SiteCode = cast(SiteCode as varchar)
		,NHSNumber = cast(NHSNumber as varchar)
		,UnitFunctionCode = cast(UnitFunctionCode as varchar)
		,ContextCode = cast(ContextCode as varchar)
		,Created = cast(Created as datetime)
		,ByWhom = cast(ByWhom as varchar)
	from
		ETL.TLoadCenAPCBaseCriticalCarePeriod Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	(
			target.ContextCode = 'CEN||PAS'
		or	target.ContextCode = 'CMFT||MID' --'CEN||MID'
		)
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,SourceSpellNo
			,SourcePatientNo
			,StartDate
			,StartTime
			,EndDate
			,EndTime
			,AdvancedCardiovascularSupportDays
			,AdvancedRespiratorySupportDays
			,BasicCardiovascularSupportDays
			,BasicRespiratorySupportDays
			,CriticalCareLevel2Days
			,CriticalCareLevel3Days
			,DermatologicalSupportDays
			,LiverSupportDays
			,NeurologicalSupportDays
			,RenalSupportDays
			,CreatedByUser
			,CreatedByTime
			,LocalIdentifier
			,LocationCode
			,StatusCode
			,TreatmentFunctionCode
			,PlannedAcpPeriod
			,InterfaceCode
			,CasenoteNumber
			,WardCode
			,AdmissionDate
			,SiteCode
			,NHSNumber
			,UnitFunctionCode
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 SourceUniqueID
			,SourceSpellNo
			,SourcePatientNo
			,StartDate
			,StartTime
			,EndDate
			,EndTime
			,AdvancedCardiovascularSupportDays
			,AdvancedRespiratorySupportDays
			,BasicCardiovascularSupportDays
			,BasicRespiratorySupportDays
			,CriticalCareLevel2Days
			,CriticalCareLevel3Days
			,DermatologicalSupportDays
			,LiverSupportDays
			,NeurologicalSupportDays
			,RenalSupportDays
			,CreatedByUser
			,CreatedByTime
			,LocalIdentifier
			,LocationCode
			,StatusCode
			,TreatmentFunctionCode
			,PlannedAcpPeriod
			,InterfaceCode
			,CasenoteNumber
			,WardCode
			,AdmissionDate
			,SiteCode
			,NHSNumber
			,UnitFunctionCode
			,ContextCode
			,Created
			,ByWhom
			)

	when matched
	and not
		(
			isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.StartDate, getdate()) = isnull(source.StartDate, getdate())
		and isnull(target.StartTime, getdate()) = isnull(source.StartTime, getdate())
		and isnull(target.EndDate, getdate()) = isnull(source.EndDate, getdate())
		and isnull(target.EndTime, getdate()) = isnull(source.EndTime, getdate())
		and isnull(target.AdvancedCardiovascularSupportDays, 0) = isnull(source.AdvancedCardiovascularSupportDays, 0)
		and isnull(target.AdvancedRespiratorySupportDays, 0) = isnull(source.AdvancedRespiratorySupportDays, 0)
		and isnull(target.BasicCardiovascularSupportDays, 0) = isnull(source.BasicCardiovascularSupportDays, 0)
		and isnull(target.BasicRespiratorySupportDays, 0) = isnull(source.BasicRespiratorySupportDays, 0)
		and isnull(target.CriticalCareLevel2Days, 0) = isnull(source.CriticalCareLevel2Days, 0)
		and isnull(target.CriticalCareLevel3Days, 0) = isnull(source.CriticalCareLevel3Days, 0)
		and isnull(target.DermatologicalSupportDays, 0) = isnull(source.DermatologicalSupportDays, 0)
		and isnull(target.LiverSupportDays, 0) = isnull(source.LiverSupportDays, 0)
		and isnull(target.NeurologicalSupportDays, 0) = isnull(source.NeurologicalSupportDays, 0)
		and isnull(target.RenalSupportDays, 0) = isnull(source.RenalSupportDays, 0)
		and isnull(target.CreatedByUser, '') = isnull(source.CreatedByUser, '')
		and isnull(target.CreatedByTime, '') = isnull(source.CreatedByTime, '')
		and isnull(target.LocalIdentifier, '') = isnull(source.LocalIdentifier, '')
		and isnull(target.LocationCode, '') = isnull(source.LocationCode, '')
		and isnull(target.StatusCode, '') = isnull(source.StatusCode, '')
		and isnull(target.TreatmentFunctionCode, '') = isnull(source.TreatmentFunctionCode, '')
		and isnull(target.PlannedAcpPeriod, '') = isnull(source.PlannedAcpPeriod, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.AdmissionDate, getdate()) = isnull(source.AdmissionDate, getdate())
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.UnitFunctionCode, '') = isnull(source.UnitFunctionCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.Created, getdate()) = isnull(source.Created, getdate())
		and isnull(target.ByWhom, '') = isnull(source.ByWhom, '')
		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.SourceSpellNo = source.SourceSpellNo
			,target.SourcePatientNo = source.SourcePatientNo
			,target.StartDate = source.StartDate
			,target.StartTime = source.StartTime
			,target.EndDate = source.EndDate
			,target.EndTime = source.EndTime
			,target.AdvancedCardiovascularSupportDays = source.AdvancedCardiovascularSupportDays
			,target.AdvancedRespiratorySupportDays = source.AdvancedRespiratorySupportDays
			,target.BasicCardiovascularSupportDays = source.BasicCardiovascularSupportDays
			,target.BasicRespiratorySupportDays = source.BasicRespiratorySupportDays
			,target.CriticalCareLevel2Days = source.CriticalCareLevel2Days
			,target.CriticalCareLevel3Days = source.CriticalCareLevel3Days
			,target.DermatologicalSupportDays = source.DermatologicalSupportDays
			,target.LiverSupportDays = source.LiverSupportDays
			,target.NeurologicalSupportDays = source.NeurologicalSupportDays
			,target.RenalSupportDays = source.RenalSupportDays
			,target.CreatedByUser = source.CreatedByUser
			,target.CreatedByTime = source.CreatedByTime
			,target.LocalIdentifier = source.LocalIdentifier
			,target.LocationCode = source.LocationCode
			,target.StatusCode = source.StatusCode
			,target.TreatmentFunctionCode = source.TreatmentFunctionCode
			,target.PlannedAcpPeriod = source.PlannedAcpPeriod
			,target.InterfaceCode = source.InterfaceCode
			,target.CasenoteNumber = source.CasenoteNumber
			,target.WardCode = source.WardCode
			,target.AdmissionDate = source.AdmissionDate
			,target.SiteCode = source.SiteCode
			,target.NHSNumber = source.NHSNumber
			,target.UnitFunctionCode = source.UnitFunctionCode
			,target.ContextCode = source.ContextCode
			,target.Created = source.Created
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



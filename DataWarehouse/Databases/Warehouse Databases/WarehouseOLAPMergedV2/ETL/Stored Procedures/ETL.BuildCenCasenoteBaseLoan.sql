﻿create procedure [ETL].BuildCenCasenoteBaseLoan
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


select
	 SourceUniqueID
	,SourcePatientNo = cast(SourcePatientNo as varchar(20))
	,CasenoteNumber = cast(CasenoteNumber as varchar(20))
	,BorrowerCode = cast(BorrowerCode as varchar(6))
	,SequenceNo = cast(SequenceNo as int)
	,TransactionTime = cast(TransactionTime as smalldatetime)
	,LoanTime = cast(LoanTime as smalldatetime)
	,ExpectedReturnDate = cast(ExpectedReturnDate as date)
	,ReturnTime = cast(ReturnTime as smalldatetime)
	,Comment = cast(Comment as varchar(30))
	,LoanReason = cast(LoanReason as varchar(50))
	,UserId = cast(UserId as varchar(3))
	,ContextCode = cast(ContextCode as varchar(8))
into
	#CasenoteBaseLoan
from
	ETL.TLoadCenCasenoteBaseLoan


CREATE UNIQUE CLUSTERED INDEX IX_TLoadCenCasenoteBaseLoan ON #CasenoteBaseLoan
	(
	 SourcePatientNo
	,CasenoteNumber
	,SequenceNo
	,LoanTime
	,ContextCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


merge
	Casenote.BaseLoan target
using
	(
	select
		 SourceUniqueID
		,SourcePatientNo
		,CasenoteNumber
		,BorrowerCode
		,SequenceNo
		,TransactionTime
		,LoanTime
		,ExpectedReturnDate
		,ReturnTime
		,Comment
		,LoanReason
		,UserId
		,ContextCode
	from
		#CasenoteBaseLoan
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourcePatientNo = target.SourcePatientNo
	and	source.CasenoteNumber = target.CasenoteNumber
	and	source.SequenceNo = target.SequenceNo
	and	source.LoanTime = target.LoanTime

	when not matched by source
	and	target.ContextCode = 'CEN||PAS'
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,SourcePatientNo
			,CasenoteNumber
			,BorrowerCode
			,SequenceNo
			,TransactionTime
			,LoanTime
			,ExpectedReturnDate
			,ReturnTime
			,Comment
			,LoanReason
			,UserId
			,ContextCode
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.CasenoteNumber
			,source.BorrowerCode
			,source.SequenceNo
			,source.TransactionTime
			,source.LoanTime
			,source.ExpectedReturnDate
			,source.ReturnTime
			,source.Comment
			,source.LoanReason
			,source.UserId
			,source.ContextCode
			)

	when matched
	and not
		(
			isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.BorrowerCode, '') = isnull(source.BorrowerCode, '')
		and isnull(target.TransactionTime, getdate()) = isnull(source.TransactionTime, getdate())
		and isnull(target.ExpectedReturnDate, getdate()) = isnull(source.ExpectedReturnDate, getdate())
		and isnull(target.ReturnTime, getdate()) = isnull(source.ReturnTime, getdate())
		and isnull(target.Comment, '') = isnull(source.Comment, '')
		and isnull(target.LoanReason, '') = isnull(source.LoanReason, '')
		and isnull(target.UserId, '') = isnull(source.UserId, '')
		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.BorrowerCode = source.BorrowerCode
			,target.TransactionTime = source.TransactionTime
			,target.ExpectedReturnDate = source.ExpectedReturnDate
			,target.ReturnTime = source.ReturnTime
			,target.Comment = source.Comment
			,target.LoanReason = source.LoanReason
			,target.UserId = source.UserId


output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



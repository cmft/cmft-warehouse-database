﻿



CREATE proc [ETL].[BuildResusBaseEncounter]

as

set dateformat dmy


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Resus.BaseEncounter target
using
	(
	select
		EventID
		,SourcePatientNo
		,DistrictNo
		,NHSNumber
		,DateOfBirth
		,AdmissionReasonID
		,AdmissionDate
		,PresentingECGID
		,Comments
		,EmergencyBleepMeeting
		,CallID
		,CallOutReasonID
		,CallOutTime
		,LocationID
		,RespondingTeamID
		,OutcomeID
		,OutcomeTime
		,FinalOutcomeID
		,FinalOutcomeTime
		,AuditTypeID
		,InterfaceCode
		,ContextCode
		,EncounterChecksum = 
		checksum(
			EventID
			,SourcePatientNo
			,DistrictNo
			,NHSNumber
			,DateOfBirth
			,AdmissionReasonID
			,AdmissionDate
			,PresentingECGID
			,Comments
			,EmergencyBleepMeeting
			,CallID
			,CallOutReasonID
			,CallOutTime
			,LocationID
			,RespondingTeamID
			,OutcomeID
			,OutcomeTime
			,FinalOutcomeID
			,FinalOutcomeTime
			,AuditTypeID
			,InterfaceCode
			,ContextCode
			)
				
	from
		ETL.TLoadResusBaseEncounter
	) source
	on	source.EventID = target.EventID
	and	source.ContextCode = target.ContextCode
	

	when not matched by source
	and	target.ContextCode = 'CMFT||CARD'

	then delete

	when not matched
	then
		insert
			(
			EventID
			,SourcePatientNo
			,DistrictNo
			,NHSNumber
			,DateOfBirth
			,AdmissionReasonID
			,AdmissionDate
			,PresentingECGID
			,Comments
			,EmergencyBleepMeeting
			,CallID
			,CallOutReasonID
			,CallOutTime
			,LocationID
			,RespondingTeamID
			,OutcomeID
			,OutcomeTime
			,FinalOutcomeID
			,FinalOutcomeTime
			,AuditTypeID
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			,EncounterChecksum
		  )
		values
			(
			 source.EventID
			,source.SourcePatientNo
			,source.DistrictNo
			,source.NHSNumber
			,source.DateOfBirth
			,source.AdmissionReasonID
			,source.AdmissionDate
			,source.PresentingECGID
			,source.Comments
			,source.EmergencyBleepMeeting
			,source.CallID
			,source.CallOutReasonID
			,source.CallOutTime
			,source.LocationID
			,source.RespondingTeamID
			,source.OutcomeID
			,source.OutcomeTime
			,source.FinalOutcomeID
			,source.FinalOutcomeTime
			,source.AuditTypeID
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			,source.EncounterChecksum
			)

	when matched
	and isnull(target.EncounterChecksum, 0) != isnull(source.EncounterChecksum, 0)
	then
		update
		set
			target.EventID = source.EventID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.DistrictNo = source.DistrictNo
			,target.NHSNumber = source.NHSNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.AdmissionReasonID = source.AdmissionReasonID
			,target.AdmissionDate = source.AdmissionDate
			,target.PresentingECGID = source.PresentingECGID
			,target.Comments = source.Comments
			,target.EmergencyBleepMeeting = source.EmergencyBleepMeeting
			,target.CallID = source.CallID
			,target.CallOutReasonID = source.CallOutReasonID
			,target.CallOutTime = source.CallOutTime
			,target.LocationID = source.LocationID
			,target.RespondingTeamID = source.RespondingTeamID
			,target.OutcomeID = source.OutcomeID
			,target.OutcomeTime = source.OutcomeTime
			,target.FinalOutcomeID = source.FinalOutcomeID
			,target.FinalOutcomeTime = source.FinalOutcomeTime
			,target.AuditTypeID = source.AuditTypeID
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.EncounterChecksum = source.EncounterChecksum
			
output
	$action into @MergeSummary
;
select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime




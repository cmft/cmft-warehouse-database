﻿CREATE procedure [ETL].[BuildTheatreBaseTimetableTemplateReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	Theatre.BaseTimetableTemplateReference
where
	not exists
	(
	select
		1
	from
		Theatre.BaseTimetableTemplate
	where
		BaseTimetableTemplate.MergeRecno = BaseTimetableTemplateReference.MergeRecno
	)
and	BaseTimetableTemplateReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Theatre.BaseTimetableTemplateReference target
using
	(
	select
		 Encounter.MergeRecno
		,ContextID = @ContextID
		,TheatreID = Theatre.SourceTheatreID
		,AnaesthetistID = Anaesthetist.SourceStaffID
		,ConsultantID = Consultant.SourceStaffID
		,SpecialtyID = Specialty.SourceSpecialtyID
	from
		Theatre.BaseTimetableTemplate Encounter

	left join Theatre.Theatre
	on	Theatre.SourceTheatreCode = cast(coalesce(Encounter.TheatreCode, '-1') as varchar)
	and	Theatre.SourceContextCode = Encounter.ContextCode

	left join Theatre.Staff Anaesthetist
	on	Anaesthetist.SourceStaffCode = cast(coalesce(Encounter.AnaesthetistCode, '-1') as varchar)
	and	Anaesthetist.SourceContextCode = Encounter.ContextCode

	left join Theatre.Staff Consultant
	on	Consultant.SourceStaffCode = cast(coalesce(Encounter.ConsultantCode, '-1') as varchar)
	and	Consultant.SourceContextCode = Encounter.ContextCode

	left join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = cast(coalesce(Encounter.SpecialtyCode, '-1') as varchar)
	and	Specialty.SourceContextCode = Encounter.ContextCode

	where
		Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			 MergeRecno
			,ContextID
			,TheatreID
			,AnaesthetistID
			,ConsultantID
			,SpecialtyID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.TheatreID
			,source.AnaesthetistID
			,source.ConsultantID
			,source.SpecialtyID
			)

	when matched
	and not
		(
			isnull(target.AnaesthetistID, 0) = isnull(source.AnaesthetistID, 0)
		and isnull(target.ConsultantID, 0) = isnull(source.ConsultantID, 0)
		and isnull(target.SpecialtyID, 0) = isnull(source.SpecialtyID, 0)
		and isnull(target.TheatreID, 0) = isnull(source.TheatreID, 0)
		)
	then
		update
		set
			 target.TheatreID = source.TheatreID
			,target.AnaesthetistID = source.AnaesthetistID
			,target.ConsultantID = source.ConsultantID
			,target.SpecialtyID = source.SpecialtyID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Context: ' + @ContextCode +
		', Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


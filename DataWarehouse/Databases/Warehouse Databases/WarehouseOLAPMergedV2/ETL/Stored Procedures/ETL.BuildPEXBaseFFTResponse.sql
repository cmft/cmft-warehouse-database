﻿


CREATE procedure [ETL].[BuildPEXBaseFFTResponse]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	PEX.BaseFFTResponse target
using
	(
	select 
		MergeResponseRecno = BaseResponse.[MergeResponseRecno]
		,Question = Question.SourceQuestion
		,AnswerID = BaseResponse.[AnswerID]
		,FollowupAnswer = Followup.[Answer]
		,IsDischargeID = Discharge.[AnswerID]
		,SurveyID = BaseResponse.[SurveyID]
		,LocationID = BaseResponse.[LocationID]
		,DirectorateCode = BaseResponse.DirectorateCode
		,SurveyTime = BaseResponse.SurveyTime
		,ResponseTime = BaseResponse.ResponseTime
		,ContextCode = BaseResponse.[ContextCode]
		,Created = BaseResponse.[Created]
		,Updated = BaseResponse.[Updated]
		,ByWhom = BaseResponse.[ByWhom]
		
	from
		[PEX].[BaseResponse]
	 
	left outer join [PEX].[BaseResponse] Discharge
	on	BaseResponse.SurveyTakenID = Discharge.SurveyTakenID
	and BaseResponse.SurveyID = Discharge.SurveyID
	and Discharge.QuestionTypeID = 4

	left outer join [PEX].[BaseResponse] Followup
	on	BaseResponse.SurveyTakenID = Followup.SurveyTakenID
	and BaseResponse.SurveyID = Followup.SurveyID
	and Followup.QuestionTypeID = 3

	inner join WH.Question
	on	Question.SourceQuestionCode = coalesce(BaseResponse.QuestionID, '-1') 
	and	Question.SourceContextCode = BaseResponse.ContextCode
	  
	where 
		BaseResponse.QuestionTypeID = 2
	and BaseResponse.[AnswerID] is not null	
	
	--GC
	--10/08/2105
	--Need to speak with Colin as to why discharge join has caused duplicate
	and BaseResponse.MergeResponseRecno <> '7648927'
		
	) source
	on	source.MergeResponseRecno = target.MergeResponseRecno

	when not matched by source
	
	then delete

	when not matched
	then
		insert
			(
			[MergeResponseRecno]
			,[Question]
			,[AnswerID]
			,[FollowupAnswer]
			,[IsDischargeID]
			,[SurveyID]
			,[LocationID]
			,[DirectorateCode]
			,[SurveyTime]
			,[ResponseTime]
			,[ContextCode]
			,[Created]
			,[Updated]
			,[ByWhom]
			)
		values
			(
			 source.[MergeResponseRecno]
			,source.[Question]
			,source.[AnswerID]
			,source.[FollowupAnswer]
			,source.[IsDischargeID]
			,source.[SurveyID]
			,source.[LocationID]
			,source.[DirectorateCode]
			,source.[SurveyTime]
			,source.[ResponseTime]
			,source.[ContextCode]
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.MergeResponseRecno, 0) = isnull(source.MergeResponseRecno, 0)
		and	isnull(target.Question, 0) = isnull(source.Question, 0)
		and	isnull(target.AnswerID, 0) = isnull(source.AnswerID, 0)	
		and	isnull(target.FollowupAnswer, 0) = isnull(source.FollowupAnswer, 0)			
		and	isnull(target.IsDischargeID, 0) = isnull(source.IsDischargeID, 0)
		and	isnull(target.SurveyID, 0) = isnull(source.SurveyID, 0)
		and	isnull(target.LocationID, 0) = isnull(source.LocationID, 0)		
		and isnull(target.DirectorateCode, 0) = isnull(source.DirectorateCode, 0)				
		and isnull(target.SurveyTime, getdate()) = isnull(source.SurveyTime, getdate())	
		and isnull(target.ResponseTime, getdate()) = isnull(source.ResponseTime, getdate())				
		and	isnull(target.ContextCode, 0) = isnull(source.ContextCode, 0)

		)
	then
		update
		set
			target.MergeResponseRecno = source.MergeResponseRecno
			,target.Question = source.Question
			,target.AnswerID = source.AnswerID
			,target.FollowupAnswer = source.FollowupAnswer	
			,target.IsDischargeID = source.IsDischargeID		
			,target.SurveyID = source.SurveyID
			,target.LocationID = source.LocationID			
			,target.DirectorateCode = source.DirectorateCode
			,target.SurveyTime = source.SurveyTime			
			,target.ResponseTime = source.ResponseTime	
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


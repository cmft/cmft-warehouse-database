﻿

CREATE procedure [ETL].[BuildAPCBaseVTEConditionReference] 
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseVTEConditionReference
			where
				BaseVTEConditionReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseVTEConditionReference
where
	not exists
	(
	select
		1
	from
		APC.BaseVTECondition
	where
		BaseVTECondition.MergeVTEConditionRecno = BaseVTEConditionReference.MergeVTEConditionRecno
	)
and	BaseVTEConditionReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseVTEConditionReference target
using
	(
	select
		BaseVTECondition.MergeVTEConditionRecno
		,ContextID = Context.ContextID
		,DiagnosisDateID =
			coalesce(
				 DiagnosisDate.DateID

				,case
				when BaseVTECondition.DiagnosisDate is null
				then NullDate.DateID

				when BaseVTECondition.DiagnosisDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,MedicationStartDateID =
			coalesce(
				 MedicationStartDate.DateID

				,case
				when BaseVTECondition.MedicationStartDate is null
				then NullDate.DateID

				when BaseVTECondition.MedicationStartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,VTETypeID = VTEType.SourceVTETypeID
		,WardID = Ward.SourceWardID	
		,BaseVTECondition.Created
		,BaseVTECondition.Updated
		,BaseVTECondition.ByWhom
	from
		APC.BaseVTECondition

	inner join WH.Context
	on	Context.ContextCode = BaseVTECondition.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(BaseVTECondition.WardCode, '-1')
	and	Ward.SourceContextCode = BaseVTECondition.ContextCode

	inner join APC.VTEType
	on	VTEType.SourceVTETypeCode = coalesce(BaseVTECondition.VTETypeID, '-1')
	and	VTEType.SourceContextCode = BaseVTECondition.ContextCode

	left join WH.Calendar DiagnosisDate
	on	DiagnosisDate.TheDate = BaseVTECondition.DiagnosisDate

	left join WH.Calendar MedicationStartDate
	on	MedicationStartDate.TheDate = BaseVTECondition.MedicationStartDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseVTECondition.Updated > @LastUpdated
	and	BaseVTECondition.ContextCode = @ContextCode

	) source
	on	source.MergeVTEConditionRecno = target.MergeVTEConditionRecno

	when not matched
	then
		insert
			(
			MergeVTEConditionRecno
			,ContextID
			,DiagnosisDateID
			,MedicationStartDateID
			,VTETypeID
			,WardID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeVTEConditionRecno
			,source.ContextID
			,source.DiagnosisDateID
			,source.MedicationStartDateID
			,source.VTETypeID
			,source.WardID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeVTEConditionRecno = source.MergeVTEConditionRecno
			,target.ContextID = source.ContextID
			,target.DiagnosisDateID = source.DiagnosisDateID
			,target.MedicationStartDateID = source.MedicationStartDateID
			,target.VTETypeID = source.VTETypeID
			,target.WardID = source.WardID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






﻿
CREATE procedure [ETL].[BuildCenRFBaseEncounter]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	RF.BaseEncounter target
using
	(
	select
		 EncounterRecno = cast(EncounterRecno as int)
		,SourceUniqueID = cast(SourceUniqueID as varchar(50))
		,SourcePatientNo = cast(SourcePatientNo as varchar(20))
		,SourceEncounterNo = cast(SourceEncounterNo as varchar(20))
		,PatientTitle = cast(PatientTitle as nvarchar(20))
		,PatientForename = cast(PatientForename as nvarchar(40))
		,PatientSurname = cast(PatientSurname as nvarchar(60))
		,DateOfBirth = cast(DateOfBirth as datetime)
		,DateOfDeath = cast(DateOfDeath as smalldatetime)
		,AgeCode = cast(AgeCode as varchar(27))
		,SexCode = cast(SexCode as nvarchar(2))
		,NHSNumber = cast(NHSNumber as varchar(17))
		,DistrictNo = cast(DistrictNo as varchar(12))
		,Postcode = cast(Postcode as varchar(8))
		,PatientAddress1 = cast(PatientAddress1 as varchar(25))
		,PatientAddress2 = cast(PatientAddress2 as varchar(25))
		,PatientAddress3 = cast(PatientAddress3 as varchar(25))
		,PatientAddress4 = cast(PatientAddress4 as varchar(25))
		,DHACode = cast(DHACode as varchar(3))
		,EthnicOriginCode = cast(EthnicOriginCode as varchar(4))
		,MaritalStatusCode = cast(MaritalStatusCode as varchar(1))
		,ReligionCode = cast(ReligionCode as varchar(4))
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(8))
		,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(8))
		,EpisodicGpCode = cast(EpisodicGpCode as varchar(8))
		,EpisodicGpPracticeCode = cast(EpisodicGpPracticeCode as varchar(6))
		,EpisodicGdpCode = cast(EpisodicGdpCode as varchar(8))
		,SiteCode = cast(SiteCode as varchar(5))
		,ConsultantCode = cast(ConsultantCode as varchar(20))
		,SpecialtyCode = cast(SpecialtyCode as varchar(5))
		,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(5))
		,PriorityCode = cast(PriorityCode as varchar(5))
		,ReferralDate = cast(ReferralDate as date)
		,DischargeDate = cast(DischargeDate as date)
		,DischargeTime = cast(DischargeTime as datetime)
		,DischargeReasonCode = cast(DischargeReasonCode as varchar(4))
		,DischargeReason = cast(DischargeReason as varchar(30))
		,AdminCategoryCode = cast(AdminCategoryCode as varchar(3))
		,ContractSerialNo = cast(ContractSerialNo as varchar(6))
		,RTTPathwayID = cast(RTTPathwayID as varchar(25))
		,RTTPathwayCondition = cast(RTTPathwayCondition as varchar(20))
		,RTTStartDate = cast(RTTStartDate as smalldatetime)
		,RTTEndDate = cast(RTTEndDate as smalldatetime)
		,RTTSpecialtyCode = cast(RTTSpecialtyCode as varchar(10))
		,RTTCurrentProviderCode = cast(RTTCurrentProviderCode as varchar(5))
		,RTTCurrentStatusCode = cast(RTTCurrentStatusCode as varchar(10))
		,RTTCurrentStatusDate = cast(RTTCurrentStatusDate as smalldatetime)
		,RTTCurrentPrivatePatientFlag = cast(RTTCurrentPrivatePatientFlag as bit)
		,RTTOverseasStatusFlag = cast(RTTOverseasStatusFlag as bit)
		,NextFutureAppointmentDate = cast(NextFutureAppointmentDate as smalldatetime)
		,ReferralComment = cast(ReferralComment as varchar(25))
		,InterfaceCode = cast(InterfaceCode as varchar(5))
		,ReasonForReferralCode = cast(ReasonForReferralCode as varchar(10))
		,SourceOfReferralGroupCode = cast(SourceOfReferralGroupCode as varchar(10))
		,DirectorateCode = cast(DirectorateCode as varchar(5))
		,CasenoteNo = cast(CasenoteNo as varchar(16))
		,PCTCode = cast(PCTCode as nvarchar(12))
		,ContextCode = cast(ContextCode as varchar(8))
		,ResidenceCCGCode = cast(ResidenceCCGCode as varchar(10))
		,CCGCode = cast(CCGCode as varchar)

	from
		ETL.TLoadCenRFBaseEncounter Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno

	when not matched by source
	and	target.ContextCode = 'CEN||PAS'
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceEncounterNo
			,PatientTitle
			,PatientForename
			,PatientSurname
			,DateOfBirth
			,DateOfDeath
			,AgeCode
			,SexCode
			,NHSNumber
			,DistrictNo
			,Postcode
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,DHACode
			,EthnicOriginCode
			,MaritalStatusCode
			,ReligionCode
			,RegisteredGpCode
			,RegisteredGpPracticeCode
			,EpisodicGpCode
			,EpisodicGpPracticeCode
			,EpisodicGdpCode
			,SiteCode
			,ConsultantCode
			,SpecialtyCode
			,SourceOfReferralCode
			,PriorityCode
			,ReferralDate
			,DischargeDate
			,DischargeTime
			,DischargeReasonCode
			,DischargeReason
			,AdminCategoryCode
			,ContractSerialNo
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,NextFutureAppointmentDate
			,ReferralComment
			,InterfaceCode
			,ReasonForReferralCode
			,SourceOfReferralGroupCode
			,DirectorateCode
			,CasenoteNo
			,PCTCode
			,ContextCode
			,ResidenceCCGCode
			,CCGCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 EncounterRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceEncounterNo
			,PatientTitle
			,PatientForename
			,PatientSurname
			,DateOfBirth
			,DateOfDeath
			,AgeCode
			,SexCode
			,NHSNumber
			,DistrictNo
			,Postcode
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,DHACode
			,EthnicOriginCode
			,MaritalStatusCode
			,ReligionCode
			,RegisteredGpCode
			,RegisteredGpPracticeCode
			,EpisodicGpCode
			,EpisodicGpPracticeCode
			,EpisodicGdpCode
			,SiteCode
			,ConsultantCode
			,SpecialtyCode
			,SourceOfReferralCode
			,PriorityCode
			,ReferralDate
			,DischargeDate
			,DischargeTime
			,DischargeReasonCode
			,DischargeReason
			,AdminCategoryCode
			,ContractSerialNo
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,NextFutureAppointmentDate
			,ReferralComment
			,InterfaceCode
			,ReasonForReferralCode
			,SourceOfReferralGroupCode
			,DirectorateCode
			,CasenoteNo
			,PCTCode
			,ContextCode
			,ResidenceCCGCode
			,CCGCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceEncounterNo, '') = isnull(source.SourceEncounterNo, '')
		and isnull(target.PatientTitle, '') = isnull(source.PatientTitle, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.AgeCode, '') = isnull(source.AgeCode, '')
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.PatientAddress1, '') = isnull(source.PatientAddress1, '')
		and isnull(target.PatientAddress2, '') = isnull(source.PatientAddress2, '')
		and isnull(target.PatientAddress3, '') = isnull(source.PatientAddress3, '')
		and isnull(target.PatientAddress4, '') = isnull(source.PatientAddress4, '')
		and isnull(target.DHACode, '') = isnull(source.DHACode, '')
		and isnull(target.EthnicOriginCode, '') = isnull(source.EthnicOriginCode, '')
		and isnull(target.MaritalStatusCode, '') = isnull(source.MaritalStatusCode, '')
		and isnull(target.ReligionCode, '') = isnull(source.ReligionCode, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredGpPracticeCode, '') = isnull(source.RegisteredGpPracticeCode, '')
		and isnull(target.EpisodicGpCode, '') = isnull(source.EpisodicGpCode, '')
		and isnull(target.EpisodicGpPracticeCode, '') = isnull(source.EpisodicGpPracticeCode, '')
		and isnull(target.EpisodicGdpCode, '') = isnull(source.EpisodicGdpCode, '')
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		and isnull(target.SourceOfReferralCode, '') = isnull(source.SourceOfReferralCode, '')
		and isnull(target.PriorityCode, '') = isnull(source.PriorityCode, '')
		and isnull(target.ReferralDate, getdate()) = isnull(source.ReferralDate, getdate())
		and isnull(target.DischargeDate, getdate()) = isnull(source.DischargeDate, getdate())
		and isnull(target.DischargeTime, getdate()) = isnull(source.DischargeTime, getdate())
		and isnull(target.DischargeReasonCode, '') = isnull(source.DischargeReasonCode, '')
		and isnull(target.DischargeReason, '') = isnull(source.DischargeReason, '')
		and isnull(target.AdminCategoryCode, '') = isnull(source.AdminCategoryCode, '')
		and isnull(target.ContractSerialNo, '') = isnull(source.ContractSerialNo, '')
		and isnull(target.RTTPathwayID, '') = isnull(source.RTTPathwayID, '')
		and isnull(target.RTTPathwayCondition, '') = isnull(source.RTTPathwayCondition, '')
		and isnull(target.RTTStartDate, getdate()) = isnull(source.RTTStartDate, getdate())
		and isnull(target.RTTEndDate, getdate()) = isnull(source.RTTEndDate, getdate())
		and isnull(target.RTTSpecialtyCode, '') = isnull(source.RTTSpecialtyCode, '')
		and isnull(target.RTTCurrentProviderCode, '') = isnull(source.RTTCurrentProviderCode, '')
		and isnull(target.RTTCurrentStatusCode, '') = isnull(source.RTTCurrentStatusCode, '')
		and isnull(target.RTTCurrentStatusDate, getdate()) = isnull(source.RTTCurrentStatusDate, getdate())
		and isnull(target.RTTCurrentPrivatePatientFlag, 0) = isnull(source.RTTCurrentPrivatePatientFlag, 0)
		and isnull(target.RTTOverseasStatusFlag, 0) = isnull(source.RTTOverseasStatusFlag, 0)
		and isnull(target.NextFutureAppointmentDate, getdate()) = isnull(source.NextFutureAppointmentDate, getdate())
		and isnull(target.ReferralComment, '') = isnull(source.ReferralComment, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ReasonForReferralCode, '') = isnull(source.ReasonForReferralCode, '')
		and isnull(target.SourceOfReferralGroupCode, '') = isnull(source.SourceOfReferralGroupCode, '')
		and isnull(target.DirectorateCode, '') = isnull(source.DirectorateCode, '')
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.PCTCode, '') = isnull(source.PCTCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.ResidenceCCGCode, '') =  isnull(source.ResidenceCCGCode, '')
		and isnull(target.CCGCode, '') =  isnull(source.CCGCode, '')
		)
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.PatientTitle = source.PatientTitle
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.AgeCode = source.AgeCode
			,target.SexCode = source.SexCode
			,target.NHSNumber = source.NHSNumber
			,target.DistrictNo = source.DistrictNo
			,target.Postcode = source.Postcode
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.DHACode = source.DHACode
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.ReligionCode = source.ReligionCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.EpisodicGpCode = source.EpisodicGpCode
			,target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
			,target.EpisodicGdpCode = source.EpisodicGdpCode
			,target.SiteCode = source.SiteCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.PriorityCode = source.PriorityCode
			,target.ReferralDate = source.ReferralDate
			,target.DischargeDate = source.DischargeDate
			,target.DischargeTime = source.DischargeTime
			,target.DischargeReasonCode = source.DischargeReasonCode
			,target.DischargeReason = source.DischargeReason
			,target.AdminCategoryCode = source.AdminCategoryCode
			,target.ContractSerialNo = source.ContractSerialNo
			,target.RTTPathwayID = source.RTTPathwayID
			,target.RTTPathwayCondition = source.RTTPathwayCondition
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.RTTSpecialtyCode = source.RTTSpecialtyCode
			,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
			,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
			,target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
			,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
			,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
			,target.NextFutureAppointmentDate = source.NextFutureAppointmentDate
			,target.ReferralComment = source.ReferralComment
			,target.InterfaceCode = source.InterfaceCode
			,target.ReasonForReferralCode = source.ReasonForReferralCode
			,target.SourceOfReferralGroupCode = source.SourceOfReferralGroupCode
			,target.DirectorateCode = source.DirectorateCode
			,target.CasenoteNo = source.CasenoteNo
			,target.PCTCode = source.PCTCode
			,target.ContextCode = source.ContextCode
			,target.ResidenceCCGCode =  source.ResidenceCCGCode
			,target.CCGCode =  source.CCGCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


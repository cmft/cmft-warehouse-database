﻿CREATE procedure [ETL].[ImportCenTheatre] as

exec ETL.ImportCenTheatreBaseOperationDetail
exec ETL.ImportCenTheatreBaseProcedureDetail
exec ETL.ImportCenTheatreBaseCancellation
exec ETL.ImportCenTheatreBasePatientBooking
exec ETL.ImportCenTheatreBaseSession
exec ETL.ImportCenTheatreBaseTimetableDetail
exec ETL.ImportCenTheatreBaseTimetableTemplate


-- Ensure all consultant codes are also brought into ReferenceMap as consultants (default mapping is to theatre staff)

declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Theatre].[ConsultantActivity]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember

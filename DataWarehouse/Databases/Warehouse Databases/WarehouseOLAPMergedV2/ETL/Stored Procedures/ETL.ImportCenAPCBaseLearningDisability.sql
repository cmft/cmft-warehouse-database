﻿
CREATE procedure [ETL].[ImportCenAPCBaseLearningDisability] as

--import the data
exec ETL.BuildCenAPCBaseLearningDisability


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseLearningDisability]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
 exec ETL.BuildAPCBaseLearningDisabilityReference 'CEN||BEDMAN'



﻿CREATE procedure [ETL].[BuildComplaintBaseComplaintReference]
	@ContextCode varchar(20) 
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	
declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Complaint.BaseComplaintReference
			where
				BaseComplaintReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Complaint.BaseComplaintReference
where
	not exists
	(
	select
		1
	from
		Complaint.BaseComplaint
	where
		BaseComplaint.MergeComplaintRecno = BaseComplaintReference.MergeComplaintRecno
	)
and	BaseComplaintReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Complaint.BaseComplaintReference target
using
	(
	select
		Complaint.MergeComplaintRecno
		,ContextID = Context.ContextID
		,ReceiptDateID =
			coalesce(
				 ReceiptDate.DateID

				,case
				when Complaint.ReceiptDate is null
				then NullDate.DateID

				when Complaint.ReceiptDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,CaseTypeID = CaseType.SourceCaseTypeID
		,CategoryID = Category.SourceCategoryID
		,CategoryTypeID = CategoryType.SourceCategoryTypeID
		,GradeID = Grade.SourceGradeID
		,DepartmentID = Department.SourceDepartmentID
		,WardID = Ward.SourceWardID
		,ResponseDateID =
			coalesce(
				 ResponseDate.DateID

				,case
				when Complaint.ResponseDate is null
				then NullDate.DateID

				when Complaint.ResponseDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,ConsentTargetDateID =
			coalesce(
				 ConsentTarget.DateID

				,case
				when Complaint.ConsentTargetDate is null
				then NullDate.DateID

				when Complaint.ConsentTargetDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)		
		,TargetDate7DayID =
			coalesce(
				 Target1.DateID

				,case
				when Complaint.TargetDate7Day is null
				then NullDate.DateID

				when Complaint.TargetDate7Day < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)		
		,TargetDate14DayID =
			coalesce(
				 Target2.DateID

				,case
				when Complaint.TargetDate14Day is null
				then NullDate.DateID

				when Complaint.TargetDate14Day < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)	
		,TargetDate25DayID =
			coalesce(
				 Target3.DateID

				,case
				when Complaint.TargetDate25Day is null
				then NullDate.DateID

				when Complaint.TargetDate25Day < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)		
		,TargetDate40DayID =
			coalesce(
				 Target4.DateID

				,case
				when Complaint.TargetDate40Day is null
				then NullDate.DateID

				when Complaint.TargetDate40Day < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)		
		,Complaint.Created
		,Complaint.Updated
		,Complaint.ByWhom
	from
		Complaint.BaseComplaint Complaint

	inner join WH.Context
	on	Context.ContextCode = Complaint.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(Complaint.WardCode, '-1')
	and	Ward.SourceContextCode = Complaint.ContextCode

	inner join Complaint.CaseType
	on	CaseType.SourceCaseTypeCode = coalesce(Complaint.CaseTypeCode, '-1')
	and	CaseType.SourceContextCode = Complaint.ContextCode

	inner join Complaint.Category
	on	Category.SourceCategoryCode = coalesce(Complaint.CategoryCode, '-1')
	and	Category.SourceContextCode = Complaint.ContextCode

	inner join Complaint.CategoryType
	on	CategoryType.SourceCategoryTypeCode = coalesce(Complaint.CategoryTypeCode, '-1')
	and	CategoryType.SourceContextCode = Complaint.ContextCode

	inner join Complaint.Grade
	on	Grade.SourceGradeCode = coalesce(Complaint.GradeCode, '-1')
	and	Grade.SourceContextCode = Complaint.ContextCode

	inner join WH.Department
	on	Department.SourceDepartmentCode = coalesce(Complaint.DepartmentCode, '-1')
	and	Department.SourceContextCode = Complaint.ContextCode

	left join WH.Calendar ReceiptDate
	on	ReceiptDate.TheDate = Complaint.ReceiptDate

	left join WH.Calendar ResponseDate
	on	ResponseDate.TheDate = Complaint.ResponseDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join WH.Calendar ConsentTarget
	on	ConsentTarget.TheDate = Complaint.ConsentTargetDate

	left join WH.Calendar Target1
	on	Target1.TheDate = Complaint.TargetDate7Day
	
	left join WH.Calendar Target2
	on	Target2.TheDate = Complaint.TargetDate14Day

	left join WH.Calendar Target3
	on	Target3.TheDate = Complaint.TargetDate25Day
	
	left join WH.Calendar Target4
	on	Target4.TheDate = Complaint.TargetDate40Day

	where
		Complaint.Updated > @LastUpdated
	and	Complaint.ContextCode = @ContextCode

	) source
	on	source.MergeComplaintRecno = target.MergeComplaintRecno

	when not matched
	then
		insert
			(
			MergeComplaintRecno
			,ContextID
			,ReceiptDateID
			,CaseTypeID
			,CategoryID
			,CategoryTypeID
			,GradeID
			,DepartmentID
			,WardID
			,ResponseDateID
			,ConsentTargetDateID
			,TargetDate7DayID
			,TargetDate14DayID
			,TargetDate25DayID
			,TargetDate40DayID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeComplaintRecno
			,source.ContextID
			,source.ReceiptDateID
			,source.CaseTypeID
			,source.CategoryID
			,source.CategoryTypeID
			,source.GradeID
			,source.DepartmentID
			,source.WardID
			,source.ResponseDateID
			,source.ConsentTargetDateID
			,source.TargetDate7DayID
			,source.TargetDate14DayID
			,source.TargetDate25DayID
			,source.TargetDate40DayID		
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeComplaintRecno = source.MergeComplaintRecno
			,target.ContextID = source.ContextID
			,target.ReceiptDateID = source.ReceiptDateID
			,target.CaseTypeID = source.CaseTypeID
			,target.CategoryID = source.CategoryID
			,target.CategoryTypeID = source.CategoryTypeID
			,target.GradeID = source.GradeID
			,target.DepartmentID = source.DepartmentID
			,target.WardID = source.WardID
			,target.ResponseDateID = source.ResponseDateID
			,target.ConsentTargetDateID = source.ConsentTargetDateID
			,target.TargetDate7DayID = source.TargetDate7DayID
			,target.TargetDate14DayID	 = source.TargetDate14DayID		
			,target.TargetDate25DayID = source.TargetDate25DayID
			,target.TargetDate40DayID	 = source.TargetDate40DayID			
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



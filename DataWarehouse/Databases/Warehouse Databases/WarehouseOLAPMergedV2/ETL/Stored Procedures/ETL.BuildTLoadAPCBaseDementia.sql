﻿

CREATE procedure [ETL].[BuildTLoadAPCBaseDementia] 

as

Truncate table ETL.TLoadAPCBaseDementia

insert into ETL.TLoadAPCBaseDementia
	(
	 DementiaRecNo	
	,GlobalProviderSpellNo	
	,ProviderSpellNo	
	,SpellID	
	,SourceUniqueID	
	,SourcePatientNo	
	,SourceSpellNo	
	,AdmissionWardCode	
	,RecordedDate	
	,RecordedTime	
	,KnownToHaveDementia	
	,ForgetfulnessQuestionAnswered	
	,AssessmentScore	
	,Investigation	
	,AssessmentNotPerformedReason	
	,ReferralSentDate	
	,InterfaceCode	
	,ContextCode	
	,Created	
	,Updated	
	,ByWhom
	,SequenceNo
	)
select
	 DementiaRecno	
	,GlobalProviderSpellNo	
	,ProviderSpellNo	
	,SpellID	
	,SourceUniqueID	
	,SourcePatientNo	
	,SourceSpellNo	
	,AdmissionWardCode	
	,RecordedDate	
	,RecordedTime	
	,KnownToHaveDementia	
	,ForgetfulnessQuestionAnswered	
	,AssessmentScore	
	,Investigation	
	,AssessmentNotPerformedReason	
	,ReferralSentDate	
	,InterfaceCode	
	,ContextCode	
	,Created	
	,Updated	
	,ByWhom
	,SequenceNo = ROW_NUMBER() over (partition by GlobalProviderSpellNo
									order by RecordedTime
									)

from
	(
	--Central
	Select
		 DementiaRecno	
		,GlobalProviderSpellNo = cast (GlobalProviderSpellNo as varchar(50))
		,ProviderSpellNo = cast (ProviderSpellNo as varchar(50))
		,SpellID = CAST(SpellID as varchar(50))	
		,SourceUniqueID	
		,SourcePatientNo	
		,SourceSpellNo = CAST(SourceSpellNo as varchar(50))		
		,AdmissionWardCode	
		,RecordedDate	
		,RecordedTime	
		,KnownToHaveDementia	
		,ForgetfulnessQuestionAnswered	
		,AssessmentScore	
		,Investigation	
		,AssessmentNotPerformedReason	
		,ReferralSentDate	
		,InterfaceCode	
		,ContextCode	
		,Created	
		,Updated	
		,ByWhom
	from
		ETL.TImportCenAPCBaseDementia
	
	union
	
	--Trafford
	select
		 DementiaRecno = SourceUniqueID	 -- FormID
		,GlobalProviderSpellNo 
		,ProviderSpellNo	
		,SpellID	
		,SourceUniqueID	
		,SourcePatientNo	
		,SourceSpellNo	
		,AdmissionWardCode	
		,RecordedDate	
		,RecordedTime	
		,KnownToHaveDementia	
		,ForgetfulnessQuestionAnswered	
		,AssessmentScore	
		,Investigation	
		,AssessmentNotPerformedReason	
		,ReferralSentDate	
		,InterfaceCode	
		,ContextCode	
		,getdate()
		,getdate()
		,suser_name()
	from
		ETL.TImportTraAPCBaseDementia
	) 
	Encounter




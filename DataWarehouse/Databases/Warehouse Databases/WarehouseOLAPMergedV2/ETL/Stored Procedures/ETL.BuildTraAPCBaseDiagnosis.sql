﻿CREATE procedure [ETL].[BuildTraAPCBaseDiagnosis]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

delete
from
	APC.BaseDiagnosis

from
	APC.BaseDiagnosis

inner join APC.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
and	BaseEncounter.ContextCode = 'TRA||UG'

where
	not exists
	(
	select
		1
	from
		ETL.TLoadTraAPCBaseDiagnosis
	where
		TLoadTraAPCBaseDiagnosis.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	)

select
	@deleted = @@ROWCOUNT

--remove orphaned records
delete
from
	APC.BaseDiagnosis
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	)

and	not exists
	(
	select
		1
	from
		APC.BaseEncounterHistory
	where
		BaseEncounterHistory.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	)

select
	@deleted = @@ROWCOUNT + @deleted


declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	APC.BaseDiagnosis target
using
	(
	select
		 MergeEncounterRecno
		,SequenceNo 
		,DiagnosisCode 
	from
		ETL.TLoadTraAPCBaseDiagnosis
	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno
	and	source.SequenceNo = target.SequenceNo

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,SequenceNo
			,DiagnosisCode
			,Created
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.SequenceNo
			,source.DiagnosisCode
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			target.DiagnosisCode = source.DiagnosisCode
		)
	then
		update
		set
			 target.DiagnosisCode = source.DiagnosisCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
--	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


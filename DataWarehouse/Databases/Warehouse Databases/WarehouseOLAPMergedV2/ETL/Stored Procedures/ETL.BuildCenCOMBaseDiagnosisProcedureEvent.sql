﻿CREATE procedure [ETL].[BuildCenCOMBaseDiagnosisProcedureEvent]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	COM.BaseDiagnosisProcedureEvent target
using
	(
	select
		 SourceUniqueID
		,DiagnosisProcedureClassificationID
		,SourcePatientNo
		,MedicalProblemLevelID
		,DiagnosisProcedureSubTypeID
		,DiagnosisProcedureID
		,NHSIdentifier
		,Comment
		,DiagnosisProcedure
		,EncounterSourceUniqueID
		,SourceEntityCode
		,DiagnosisProcedureTime
		,DiagnosisProcedureTypeCode
		,CreatedTime
		,ModifiedTime
		,CreatedByID
		,ModifiedByID
		,DiagnosisProcedureCode
		,DiagnosisProcedureInternalCode
		,SupplementaryCode
		,SupplementaryInternalCode
		,ArchiveFlag
		,StartTime
		,EndTime
		,UpdateTime
		,StaffTeamID
		,ProfessionalCarerID
		,Duration
		,ConfidentialFlag
		,Dosage
		,DosageUnitID
		,LocationOnBodyID
		,DosageFrequency
		,FrequencyDosageUnitID
		,CancelledTime
		,RulesID
		,Value
		,ProblemFlag
		,LateralityID
		,SourceEpisodeID
		,PeriodAdministeredID
		,ReasonAdministeredID
		,AnaestheticCategoryID
		,CauseOfDeathFlag
		,EventSequenceNo
		,CodingAuthorisationFlag
		,LatchedCodingAuthorisationFlag
		,ActionToTake
		,AlertSeverityID
		,ContractedProcedureFlag
		,SpecialtyID
		,HostitalServiceFlag
		,DiagnosisProcedureEndTime
		,ProcedureLocationID
		,TheatreBodyRegionID
		,IndividualCodingStatusID
		,AnaestheticTypeID
		,CodingOnsetTypeID
		,VisitPurposeID
		,HealthOrganisationOwnerID
		,Created
		,Updated
		,ByWhom
		,ContextCode
	from
		ETL.TLoadCenCOMBaseDiagnosisProcedureEvent
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.ContextCode = 'CEN||IPM'
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,DiagnosisProcedureClassificationID
			,SourcePatientNo
			,MedicalProblemLevelID
			,DiagnosisProcedureSubTypeID
			,DiagnosisProcedureID
			,NHSIdentifier
			,Comment
			,DiagnosisProcedure
			,EncounterSourceUniqueID
			,SourceEntityCode
			,DiagnosisProcedureTime
			,DiagnosisProcedureTypeCode
			,CreatedTime
			,ModifiedTime
			,CreatedByID
			,ModifiedByID
			,DiagnosisProcedureCode
			,DiagnosisProcedureInternalCode
			,SupplementaryCode
			,SupplementaryInternalCode
			,ArchiveFlag
			,StartTime
			,EndTime
			,UpdateTime
			,StaffTeamID
			,ProfessionalCarerID
			,Duration
			,ConfidentialFlag
			,Dosage
			,DosageUnitID
			,LocationOnBodyID
			,DosageFrequency
			,FrequencyDosageUnitID
			,CancelledTime
			,RulesID
			,Value
			,ProblemFlag
			,LateralityID
			,SourceEpisodeID
			,PeriodAdministeredID
			,ReasonAdministeredID
			,AnaestheticCategoryID
			,CauseOfDeathFlag
			,EventSequenceNo
			,CodingAuthorisationFlag
			,LatchedCodingAuthorisationFlag
			,ActionToTake
			,AlertSeverityID
			,ContractedProcedureFlag
			,SpecialtyID
			,HostitalServiceFlag
			,DiagnosisProcedureEndTime
			,ProcedureLocationID
			,TheatreBodyRegionID
			,IndividualCodingStatusID
			,AnaestheticTypeID
			,CodingOnsetTypeID
			,VisitPurposeID
			,HealthOrganisationOwnerID
			,Created
			,Updated
			,ByWhom
			,ContextCode
			)
		values
			(
			 source.SourceUniqueID
			,source.DiagnosisProcedureClassificationID
			,source.SourcePatientNo
			,source.MedicalProblemLevelID
			,source.DiagnosisProcedureSubTypeID
			,source.DiagnosisProcedureID
			,source.NHSIdentifier
			,source.Comment
			,source.DiagnosisProcedure
			,source.EncounterSourceUniqueID
			,source.SourceEntityCode
			,source.DiagnosisProcedureTime
			,source.DiagnosisProcedureTypeCode
			,source.CreatedTime
			,source.ModifiedTime
			,source.CreatedByID
			,source.ModifiedByID
			,source.DiagnosisProcedureCode
			,source.DiagnosisProcedureInternalCode
			,source.SupplementaryCode
			,source.SupplementaryInternalCode
			,source.ArchiveFlag
			,source.StartTime
			,source.EndTime
			,source.UpdateTime
			,source.StaffTeamID
			,source.ProfessionalCarerID
			,source.Duration
			,source.ConfidentialFlag
			,source.Dosage
			,source.DosageUnitID
			,source.LocationOnBodyID
			,source.DosageFrequency
			,source.FrequencyDosageUnitID
			,source.CancelledTime
			,source.RulesID
			,source.Value
			,source.ProblemFlag
			,source.LateralityID
			,source.SourceEpisodeID
			,source.PeriodAdministeredID
			,source.ReasonAdministeredID
			,source.AnaestheticCategoryID
			,source.CauseOfDeathFlag
			,source.EventSequenceNo
			,source.CodingAuthorisationFlag
			,source.LatchedCodingAuthorisationFlag
			,source.ActionToTake
			,source.AlertSeverityID
			,source.ContractedProcedureFlag
			,source.SpecialtyID
			,source.HostitalServiceFlag
			,source.DiagnosisProcedureEndTime
			,source.ProcedureLocationID
			,source.TheatreBodyRegionID
			,source.IndividualCodingStatusID
			,source.AnaestheticTypeID
			,source.CodingOnsetTypeID
			,source.VisitPurposeID
			,source.HealthOrganisationOwnerID
			,source.Created
			,source.Updated
			,source.ByWhom
			,source.ContextCode
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.DiagnosisProcedureClassificationID = source.DiagnosisProcedureClassificationID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.MedicalProblemLevelID = source.MedicalProblemLevelID
			,target.DiagnosisProcedureSubTypeID = source.DiagnosisProcedureSubTypeID
			,target.DiagnosisProcedureID = source.DiagnosisProcedureID
			,target.NHSIdentifier = source.NHSIdentifier
			,target.Comment = source.Comment
			,target.DiagnosisProcedure = source.DiagnosisProcedure
			,target.EncounterSourceUniqueID = source.EncounterSourceUniqueID
			,target.SourceEntityCode = source.SourceEntityCode
			,target.DiagnosisProcedureTime = source.DiagnosisProcedureTime
			,target.DiagnosisProcedureTypeCode = source.DiagnosisProcedureTypeCode
			,target.CreatedTime = source.CreatedTime
			,target.ModifiedTime = source.ModifiedTime
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.DiagnosisProcedureCode = source.DiagnosisProcedureCode
			,target.DiagnosisProcedureInternalCode = source.DiagnosisProcedureInternalCode
			,target.SupplementaryCode = source.SupplementaryCode
			,target.SupplementaryInternalCode = source.SupplementaryInternalCode
			,target.ArchiveFlag = source.ArchiveFlag
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.UpdateTime = source.UpdateTime
			,target.StaffTeamID = source.StaffTeamID
			,target.ProfessionalCarerID = source.ProfessionalCarerID
			,target.Duration = source.Duration
			,target.ConfidentialFlag = source.ConfidentialFlag
			,target.Dosage = source.Dosage
			,target.DosageUnitID = source.DosageUnitID
			,target.LocationOnBodyID = source.LocationOnBodyID
			,target.DosageFrequency = source.DosageFrequency
			,target.FrequencyDosageUnitID = source.FrequencyDosageUnitID
			,target.CancelledTime = source.CancelledTime
			,target.RulesID = source.RulesID
			,target.Value = source.Value
			,target.ProblemFlag = source.ProblemFlag
			,target.LateralityID = source.LateralityID
			,target.SourceEpisodeID = source.SourceEpisodeID
			,target.PeriodAdministeredID = source.PeriodAdministeredID
			,target.ReasonAdministeredID = source.ReasonAdministeredID
			,target.AnaestheticCategoryID = source.AnaestheticCategoryID
			,target.CauseOfDeathFlag = source.CauseOfDeathFlag
			,target.EventSequenceNo = source.EventSequenceNo
			,target.CodingAuthorisationFlag = source.CodingAuthorisationFlag
			,target.LatchedCodingAuthorisationFlag = source.LatchedCodingAuthorisationFlag
			,target.ActionToTake = source.ActionToTake
			,target.AlertSeverityID = source.AlertSeverityID
			,target.ContractedProcedureFlag = source.ContractedProcedureFlag
			,target.SpecialtyID = source.SpecialtyID
			,target.HostitalServiceFlag = source.HostitalServiceFlag
			,target.DiagnosisProcedureEndTime = source.DiagnosisProcedureEndTime
			,target.ProcedureLocationID = source.ProcedureLocationID
			,target.TheatreBodyRegionID = source.TheatreBodyRegionID
			,target.IndividualCodingStatusID = source.IndividualCodingStatusID
			,target.AnaestheticTypeID = source.AnaestheticTypeID
			,target.CodingOnsetTypeID = source.CodingOnsetTypeID
			,target.VisitPurposeID = source.VisitPurposeID
			,target.HealthOrganisationOwnerID = source.HealthOrganisationOwnerID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿
Create Procedure ETL.BuildMortalityResponseBase

as

Insert into Mortality.ResponseBase
	(
	Response
	,OriginalResponse
	,YesNoResponse
	)
		
select 
	 Response =
		case
			when Response.YesNoResponse = 1 then
				Case
					when Response.Response = '' then 'No Response YN'
					when Response.Response = '0' then 'No'
					when Response.Response = '1' then 'Yes'
					else left(Response.Response, 4096)
				end
			when Response.Response = '' then 'No Response'
			else left(Response.Response, 4096)
		end
	,OriginalResponse = Response.Response
	,YesNoResponse
from
	(
	select distinct
		BaseReview.Response
		,Question.YesNoResponse 
	from
		Mortality.BaseReview
	
	inner join Mortality.FormType
	on FormType.SourceFormTypeCode = BaseReview.FormTypeCode	
	
	inner join Mortality.Question
	on	Question.Question = BaseReview.Question
	and Question.QuestionNo = BaseReview.QuestionNo
	and Question.FormTypeID = FormType.SourceFormTypeID 
		-- and FormType.SourceContextCode = BaseReview.ContextCode
	) Response
where
	not exists
		(
		Select
			1
		from
			Mortality.ResponseBase Present
		where
			Present.OriginalResponse = Response.Response
		and Present.YesNoResponse = Response.YesNoResponse
		)
	

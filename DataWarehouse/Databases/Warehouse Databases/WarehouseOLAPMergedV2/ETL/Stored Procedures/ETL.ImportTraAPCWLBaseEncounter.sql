﻿create procedure [ETL].[ImportTraAPCWLBaseEncounter] as

--import the data
exec ETL.BuildTraAPCWLBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APCWL].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCWLBaseEncounterReference 'TRA||UG'

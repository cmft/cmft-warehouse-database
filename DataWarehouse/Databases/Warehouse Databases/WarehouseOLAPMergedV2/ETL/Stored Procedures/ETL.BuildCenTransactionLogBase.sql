﻿CREATE procedure [ETL].[BuildCenTransactionLogBase]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @inserted int

select
	 TransactionRecno
	,TransactionID
	,ColumnID
	,ActivityID
	,ActivityTypeID
	,UserID
	,ActionType
	,TransactionTime
	,DistrictNo
	,SourceSpellNo
	,ActivityTime
	,TransactionSourceEncounterNo
	,CurrentSourceEncounterNo
	,NewValue
	,OldValue
	,Created
	,ContextCode
into
	#TransactionBase
from
	ETL.TLoadCenTransactionLogBase TransactionBase

ALTER TABLE #TransactionBase ADD  CONSTRAINT [PK_Base_xxx] PRIMARY KEY CLUSTERED 
(
	[TransactionRecno] ASC,
	[ContextCode] ASC
)


insert
into
	TransactionLog.Base
(
	 TransactionRecno
	,TransactionID
	,ColumnID
	,ActivityID
	,ActivityTypeID
	,UserID
	,ActionType
	,TransactionTime
	,DistrictNo
	,SourceSpellNo
	,ActivityTime
	,TransactionSourceEncounterNo
	,CurrentSourceEncounterNo
	,NewValue
	,OldValue
	,Created
	,ContextCode
)
select
	 TransactionRecno
	,TransactionID
	,ColumnID
	,ActivityID
	,ActivityTypeID
	,UserID
	,ActionType
	,TransactionTime
	,DistrictNo
	,SourceSpellNo
	,ActivityTime
	,TransactionSourceEncounterNo
	,CurrentSourceEncounterNo
	,NewValue
	,OldValue
	,Created
	,ContextCode
from
	#TransactionBase TransactionBase
where
	not exists
	(
	select
		1
	from
		TransactionLog.Base
	where
		Base.TransactionRecno = TransactionBase.TransactionRecno
	and	Base.ContextCode = TransactionBase.ContextCode
	)


select
	 @inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



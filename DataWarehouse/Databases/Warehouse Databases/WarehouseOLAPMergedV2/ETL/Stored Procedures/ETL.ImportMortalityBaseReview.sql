﻿


CREATE procedure [ETL].[ImportMortalityBaseReview] as

--import the data

Exec ETL.BuildTLoadMortalityBaseReview

Exec ETL.LoadMortalityBaseReview

declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Mortality].[BaseReview]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember

Exec [ETL].[BuildMortalityQuestionBase] 

Exec [ETL].[BuildMortalityResponseBase] 


--Build the BaseReference table
 exec ETL.BuildMortalityBaseReviewReference 'CMFT||MORT' 
 exec ETL.BuildMortalityBaseReviewReference 'CMFT||MORTARCHV' 

-- Create Bridging table between APC and Mortality Reviews
exec [APC].[BuildBaseEncounterBaseReview]



﻿


CREATE procedure [ETL].[BuildNeonatalBaseBeddayReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Neonatal.BaseBeddayReference
			where
				BaseBeddayReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Neonatal.BaseBeddayReference
where
	not exists
	(
	select
		1
	from
		Neonatal.BaseBedday
	where
		BaseBedday.MergeBeddayRecno = BaseBeddayReference.MergeBeddayRecno
	)
and	BaseBeddayReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Neonatal.BaseBeddayReference target
using
	(
	select
		 Bedday.MergeBeddayRecno
		,ContextID = Context.ContextID
		,Bedday.Created
		,Bedday.Updated
		,Bedday.ByWhom
	from
		Neonatal.BaseBedday Bedday

	inner join WH.Context
	on	Context.ContextCode = Bedday.ContextCode

	where
		Bedday.Updated > @LastUpdated
	and	Bedday.ContextCode = @ContextCode

	) source
	on	source.MergeBeddayRecno = target.MergeBeddayRecno

	when not matched
	then
		insert
			(
			 MergeBeddayRecno
			,ContextID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeBeddayRecno
			,source.ContextID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			target.ContextID = source.ContextID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime




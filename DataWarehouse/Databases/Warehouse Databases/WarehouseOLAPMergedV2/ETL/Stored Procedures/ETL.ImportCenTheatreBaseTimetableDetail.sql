﻿create procedure [ETL].[ImportCenTheatreBaseTimetableDetail] as

--import the data
exec ETL.BuildCenTheatreBaseTimetableDetail


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Theatre].[BaseTimetableDetail]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildTheatreBaseTimetableDetailReference 'CEN||ORMIS'



﻿CREATE procedure [ETL].[LoadAEBaseInvestigationReference]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	AE.BaseInvestigationReference target
using
	(
	select
		 ProcessList.MergeRecno
		,ProcessList.Action
		,Context.ContextID
		,InvestigationID = Investigation.SourceInvestigationID
	from
		AE.ProcessList ProcessList

	left join AE.BaseInvestigation
	on	BaseInvestigation.MergeRecno = ProcessList.MergeRecno

	left join AE.Investigation
	on	Investigation.SourceInvestigationCode = cast(coalesce(BaseInvestigation.InvestigationCode, '-1') as varchar)
	and	Investigation.SourceContextCode = BaseInvestigation.ContextCode

	left join WH.Context
	on	Context.ContextCode = BaseInvestigation.ContextCode
	
	where
		ProcessList.Dataset = 'Investigation'

	) source
	on	source.MergeRecno = target.MergeRecno

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	then

		insert
			(
			 MergeRecno
			,ContextID
			,InvestigationID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.InvestigationID
			)

	when matched
	and	source.Action <> 'DELETE'
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.InvestigationID = source.InvestigationID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


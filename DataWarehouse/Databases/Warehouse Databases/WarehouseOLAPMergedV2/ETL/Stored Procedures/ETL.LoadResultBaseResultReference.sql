﻿CREATE procedure [ETL].[LoadResultBaseResultReference]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Result.BaseResultReference target
using
	(
	select
		 ProcessList.MergeRecno
		,ProcessList.Action
		,BaseResult.ResultRecno
		,BaseResult.ContextCode
		,Context.ContextID
		,ConsultantID = Consultant.SourceConsultantID
		,MainSpecialtyID = MainSpecialty.SourceSpecialtyID
		,DisciplineID = Discipline.SourceDisciplineID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,LocationID = Location.SourceLocationID
		,ResultSetID =  ResultSet.SourceResultSetID
		,SampleTypeID = null

		,EffectiveDateID =
			coalesce(
				 EffectiveDate.DateID

				,case
				when BaseResult.EffectiveTime is null
				then NullDate.DateID

				when cast(BaseResult.EffectiveTime as date) < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ResultID = Result.SourceResultID
		,UnitOfMeasurementID = null
		,ResultDateID = 
			coalesce(
				 ResultDate.DateID

				,case
				when BaseResult.ResultTime is null
				then NullDate.DateID

				when cast(BaseResult.ResultTime as date) < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,BaseResult.Created
		,BaseResult.Updated
		,BaseResult.ByWhom
	from
		Result.ProcessList

	left join Result.BaseResult
	on	BaseResult.MergeResultRecno = ProcessList.MergeRecno

	left join WH.Calendar EffectiveDate
	on	EffectiveDate.TheDate = cast(BaseResult.EffectiveTime as date)

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = coalesce(BaseResult.ConsultantCode, '-1')
	and	Consultant.SourceContextCode = BaseResult.ContextCode

	left join WH.Specialty MainSpecialty
	on	MainSpecialty.SourceSpecialtyCode = coalesce(BaseResult.MainSpecialtyCode, '-1')
	and	MainSpecialty.SourceContextCode = BaseResult.ContextCode

	left join Result.Discipline
	on	Discipline.SourceDisciplineCode = coalesce(BaseResult.DisciplineCode, '-1')
	and	Discipline.SourceContextCode = BaseResult.ContextCode

	left join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(BaseResult.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = BaseResult.ContextCode

	left join WH.Location Location
	on	Location.SourceLocationCode = coalesce(BaseResult.LocationCode, '-1')
	and	Location.SourceContextCode = BaseResult.ContextCode

	left join Result.ResultSet
	on	ResultSet.SourceResultSetCode = coalesce(BaseResult.UniqueResultSetCode, '-1')
	and	ResultSet.SourceContextCode = BaseResult.ContextCode

	left join Result.Result
	on	Result.SourceResultCode = coalesce(BaseResult.UniqueResultCode, '-1')
	and	Result.SourceContextCode = BaseResult.ContextCode

	left join WH.Calendar ResultDate
	on	ResultDate.TheDate = cast(BaseResult.ResultTime as date)

	left join WH.Context
	on	Context.ContextCode = BaseResult.ContextCode

	) source
	on	source.MergeRecno = target.MergeResultRecno

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	and	source.Action = 'INSERT'
	then
		insert
			(
			MergeResultRecno
			,ResultRecno
			,ContextCode
			,ContextID
			,ConsultantID
			,MainSpecialtyID
			,DisciplineID
			,SpecialtyID
			,LocationID
			,ResultSetID
			,SampleTypeID
			,EffectiveDateID
			,ResultID
			,UnitOfMeasurementID
			,ResultDateID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeRecno
			,source.ResultRecno
			,source.ContextCode
			,source.ContextID
			,source.ConsultantID
			,source.MainSpecialtyID
			,source.DisciplineID
			,source.SpecialtyID
			,source.LocationID
			,source.ResultSetID
			,source.SampleTypeID
			,source.EffectiveDateID
			,source.ResultID
			,source.UnitOfMeasurementID
			,source.ResultDateID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and	source.Action = 'UPDATE'
	then
		update
		set
			 target.MergeResultRecno = source.MergeRecno
			,target.ResultRecno = source.ResultRecno
			,target.ContextCode = source.ContextCode
			,target.ContextID = source.ContextID
			,target.ConsultantID = source.ConsultantID
			,target.MainSpecialtyID = source.MainSpecialtyID
			,target.DisciplineID = source.DisciplineID
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.ResultSetID = source.ResultSetID
			,target.SampleTypeID = source.SampleTypeID
			,target.EffectiveDateID = source.EffectiveDateID
			,target.ResultID = source.ResultID
			,target.UnitOfMeasurementID = source.UnitOfMeasurementID
			,target.ResultDateID = source.ResultDateID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿

CREATE Procedure [ETL].[BuildResultBaseAcuteKidneyInjuryAlert]

as

Truncate table Result.BaseAcuteKidneyInjuryAlert		
Insert Into Result.BaseAcuteKidneyInjuryAlert		
	(
	MergeResultRecno
	,SourcePatientNo
	,ContextCode
	,DistrictNo
	,CasenoteNumber
	,PatientSurname
	,PatientForename
	,PatientSequence
	,SpellSequence
	,GlobalProviderSpellNo
	,LinkageType 
	,LinkingMergeEncounterRecno 
	,ResultAPCLinked
	,AdmissionTime
	,PatientCategoryCode
	,WardEpisodeStart
	,WardEpisodeEnd
	,DischargeTime
	,DischargeMethodCode
	,DateOfDeath
	,LocalLocationCode
	,LocalLocation
	,SexCode
	,DateOfBirth
	,AgeOnAdmission
	,AgeAtTest
	,ResultTime
	,EffectiveTime
	,Result
	,PreviousResultTime 
	,PreviousResultEffectiveTime 
	,PreviousResult
	,ResultIntervalDays 
	,ResultChange
	,CurrentToPreviousResultRatio 
	,ResultRateOfChangePer24Hr 
	,BaselineResultRecno
	,BaselineResult 
	,BaselineEffectiveTime
	,BaselineResultTime 
	,BaselineToCurrentDays 
	,BaselineToCurrentChange 
	,CurrentToBaselineResultRatio 
	
	,C1Result
	,RatioDescription
	,ResultForRatio
	,RVRatio
	,Alert 
	,RenalPatient
	,Stage
	--,InHospitalAKI
	,CurrentInpatient
	,CurrentWard 
	)
	
Select
	AKI.MergeResultRecno
	,AKI.SourcePatientNo
	,AKI.ContextCode
	,AKI.DistrictNo
	,AKI.CasenoteNumber
	,AKI.PatientSurname
	,AKI.PatientForename
	,AKI.PatientSequence
	,AKI.SpellSequence
	,AKI.GlobalProviderSpellNo
	,AKI.LinkageType 
	,AKI.LinkingMergeEncounterRecno 
	,AKI.ResultAPCLinked
	,AKI.AdmissionTime
	,AKI.PatientCategoryCode
	,AKI.WardEpisodeStart
	,AKI.WardEpisodeEnd
	,AKI.DischargeTime
	,AKI.DischargeMethodCode
	,AKI.DateOfDeath
	,AKI.LocalLocationCode
	,AKI.LocalLocation
	,AKI.SexCode
	,AKI.DateOfBirth
	,AKI.AgeOnAdmission
	,AKI.AgeAtTest
	,AKI.ResultTime
	,AKI.EffectiveTime
	,AKI.Result
	,PreviousResultTime 
	,PreviousResultEffectiveTime 
	,PreviousResult
	,ResultIntervalDays 
	,ResultChange
	,CurrentToPreviousResultRatio 
	,ResultRateOfChangePer24Hr 
	,BaselineResultRecno
	,BaselineResult 
	,BaselineEffectiveTime
	,BaselineResultTime 
	,BaselineToCurrentDays 
	,BaselineToCurrentChange 
	,CurrentToBaselineResultRatio 
	
	,NationalAKI.C1Result
	,NationalAKI.RatioDescription
	,NationalAKI.ResultForRatio
	,NationalAKI.RVRatio
	,NationalAlert = 
		case
			when NationalAKI.RatioDescription = 'Baseline' then
				case
					when NationalAKI.C1Result < RVRatio then 'Low'
					when NationalAKI.C1Result = RVRatio then 'NoFlag'
					else 'Suggest'
				end
			when NationalAKI.RatioDescription in ('Lowest0-7days','Median8-365days') then
				case
					when NationalAKI.RVRatio >=3.0 then 'AKI 3'
					when NationalAKI.RVRatio >=1.5 then
						case
							when AgeAtTest <18 and SexCode = 1 and NationalAKI.C1Result > (80*3) then 'AKI 3'
							when AgeAtTest <18 and SexCode = 2 and NationalAKI.C1Result > (60*3) then 'AKI 3'
							when AgeAtTest <18 and NationalAKI.C1Result > (70*3) then 'AKI 3'
							when AgeAtTest >=18 and NationalAKI.C1Result > 354 then 'AKI 3'
							when NationalAKI.RVRatio >=2.0 then 'AKI 2'
							else 'AKI 1'
						end
					when Alert is not null then Alert
					when NationalAKI.RatioDescription = 'Lowest0-7days' and NationalAKI.C1Result - NationalAKI.ResultForRatio > 26 then 'No Alert Repeat'
					else 'No Alert'
				end
			else null
		end
	,AKI.RenalPatient
	
	,Stage = case
				when CurrentToBaselineResultRatio <1.5 then 'Stage0'
				when CurrentToBaselineResultRatio <2.0 then 
					case
						when BaselineToCurrentDays >= 181 then 'Exclusion'
						when AKI.SexCode = 2 and BaselineResult < 44 then 'Exclusion'
						when AKI.SexCode = 1 and BaselineResult < 58 then 'Exclusion'
						else 'Stage1'
					end
				when CurrentToBaselineResultRatio <3.0 then 'Stage2'
				else 'Stage3'
			end	
	

	,CurrentInpatient = 
			Case
				when APCEncounter.GlobalProviderSpellNo is not null then 1
				else 0
			end
			
	,CurrentWard = APCEncounter.WardCode

from
	(
	Select
		Result.MergeResultRecno
		,Result.SourcePatientNo
		,Result.ContextCode
		,Result.DistrictNo
		,Result.CasenoteNumber
		,Result.PatientSurname
		,Result.PatientForename
		,Result.PatientSequence
		,Result.SpellSequence
		,Result.GlobalProviderSpellNo
		,Result.LinkageType 
		,Result.LinkingMergeEncounterRecno 
		,Result.ResultAPCLinked
		,Result.AdmissionTime
		,Result.PatientCategoryCode
		,Result.WardEpisodeStart
		,Result.WardEpisodeEnd
		,Result.DischargeTime
		,Result.DischargeMethodCode
		,Result.DateOfDeath
		,Result.LocalLocationCode
		,Result.LocalLocation
		,Result.SexCode
		,Result.DateOfBirth
		,Result.AgeOnAdmission
		,Result.AgeAtTest
		,Result.ResultTime
		,Result.EffectiveTime
		,Result.Result
		,PreviousResultTime = PreviousResult.ResultTime
		,PreviousResultEffectiveTime = PreviousResult.EffectiveTime
		,PreviousResult = PreviousResult.Result
		
		,ResultIntervalDays = datediff(minute, PreviousResult.ResultTime, Result.ResultTime ) / 1440.0

		,ResultChange =	
			Result.Result
			-
			PreviousResult.Result

		,CurrentToPreviousResultRatio =	
			cast(Result.Result as float) 
			/ 
			cast(PreviousResult.Result as float)

		,ResultRateOfChangePer24Hr = 
			case 
				when Result.ResultTime = PreviousResult.ResultTime then 0.0
				else
					(
						cast(Result.Result as float) 
						- 
						cast(PreviousResult.Result as float)
					)
					/
					(
						datediff(minute, PreviousResult.ResultTime, Result.ResultTime ) 
						/ 
						1440.0
					)
			end
			
		,BaselineResultRecno = BaselineMergeResultRecno
		
		,BaselineResult = 
					Case
						when Result.Result <= BaselineResult.BaselineResult then Result.Result
						else BaselineResult.BaselineResult
					end
			
		,BaselineEffectiveTime = 
					Case
						when Result.Result <= BaselineResult.BaselineResult then Result.EffectiveTime
						else BaselineResult.BaselineEffectiveTime
					end
				
		,BaselineResultTime = 
					Case
						when Result.Result <= BaselineResult.BaselineResult then Result.ResultTime
						else BaselineResult.BaselineResultTime
					end
			
		,BaselineToCurrentDays = 
			DATEDIFF (DAY,
						Case
							when Result.Result <= BaselineResult.BaselineResult then Result.EffectiveTime
							else BaselineResult.BaselineEffectiveTime
						end
						,Result.ResultTime
						)
		,BaselineToCurrentChange = 
			cast(Result.Result as int)
			-
				Case
					when Result.Result <= BaselineResult.BaselineResult then Result.Result
					else BaselineResult.BaselineResult
				end
					
		,CurrentToBaselineResultRatio =
			cast(Result.Result as float)
			/
			cast(
					Case
						when Result.Result <= BaselineResult.BaselineResult then Result.Result
						else BaselineResult.BaselineResult
					end
				as float
				)	
		

			,Result.RenalPatient
			
	from
		Result.WrkAcuteKidneyInjuryAlert Result

	left join Result.WrkAcuteKidneyInjuryAlert PreviousResult
	on Result.DistrictNo = PreviousResult.DistrictNo
	and Result.PatientSequence = (PreviousResult.PatientSequence + 1)

	left join
		(
		Select 
			Baseline.MergeResultRecno
			,BaselineMergeResultRecno
			,BaselineResult = 
				Case
					when Baseline.BaselineResult < DefaultBaseline.DefaultBaselineResult then Baseline.BaselineResult
					else coalesce
								(
								DefaultBaseline.DefaultBaselineResult
								,Baseline.BaselineResult
								,case
									when SexCode = 1 then 80
									when SexCode = 2 then 60
									else 70
								end
								)
				end
			,BaselineEffectiveTime = 
				Case
					when Baseline.BaselineResult < DefaultBaseline.DefaultBaselineResult then Baseline.BaselineEffectiveTime
					else coalesce
								(
								DefaultBaseline.DefaultBaselineEffectiveTime
								,Baseline.BaselineEffectiveTime
								,dateadd(month,-6,EffectiveTime)
								)
				end
			,BaselineResultTime = 
				Case
					when Baseline.BaselineResult < DefaultBaseline.DefaultBaselineResult then Baseline.BaselineResultTime
					else coalesce
								(
								DefaultBaseline.DefaultBaselineResultTime
								,Baseline.BaselineResultTime
								,dateadd(month,-6,ResultTime)
								)
				end
		
		from
			(
			select 
				MergeResultRecno = KeyResult.MergeResultRecno
				,GlobalProviderSpellNo = KeyResult.GlobalProviderSpellNo
				,SexCode = KeyResult.SexCode
				,EffectiveTime = KeyResult.EffectiveTime
				,ResultTime = KeyResult.ResultTime
				,BaselineMergeResultRecno = Baseline.MergeResultRecno
				,BaselineEffectiveTime = Baseline.EffectiveTime
				,BaselineResultTime = Baseline.ResultTime
				,BaselineResult = Baseline.Result  -- check to see if any results come through > Result.Result
			from
				Result.WrkAcuteKidneyInjuryAlert Baseline
			
			right join Result.WrkAcuteKidneyInjuryAlert KeyResult
			on Baseline.DistrictNo = KeyResult.DistrictNo
			and Baseline.PatientSequence < KeyResult.PatientSequence
			and	Baseline.EffectiveTime between dateadd(month , -6 , KeyResult.EffectiveTime) and KeyResult.EffectiveTime
			and not exists -- lowest result in last 6 months
					(
					select
						1
					from
						Result.WrkAcuteKidneyInjuryAlert LowerBaselineResult
					where
						LowerBaselineResult.DistrictNo = KeyResult.DistrictNo
					and LowerBaselineResult.PatientSequence < KeyResult.PatientSequence
					and	LowerBaselineResult.EffectiveTime between dateadd(month , -6 , KeyResult.EffectiveTime) and KeyResult.EffectiveTime
					and	LowerBaselineResult.Result < Baseline.Result		
					)
			and	not exists -- latest lowest result in last 6 months
					(
					select
						1
					from
						Result.WrkAcuteKidneyInjuryAlert LaterBaselineResult
					where
						LaterBaselineResult.DistrictNo = KeyResult.DistrictNo
					and LaterBaselineResult.PatientSequence < KeyResult.PatientSequence
					and	LaterBaselineResult.EffectiveTime between dateadd(month , -6 , KeyResult.EffectiveTime) and KeyResult.EffectiveTime
					and	LaterBaselineResult.Result = Baseline.Result
					and	LaterBaselineResult.PatientSequence > Baseline.PatientSequence
					)
			) 
			Baseline

		left join
			(
			Select 
				MergeResultRecno
				,GlobalProviderSpellNo
				,DefaultBaselineResult = 
					case 
						when SexCode = 1 then 80
						when SexCode = 2 then 60
						else 70
					end
				,DefaultBaselineEffectiveTime = dateadd(month,-6,EffectiveTime)
				,DefaultBaselineResultTime = dateadd(month,-6,ResultTime)
			from
				Result.WrkAcuteKidneyInjuryAlert DefaultResult
			where
				SpellSequence = 1
			and not exists
				(
				Select
					1
				from
					Result.WrkAcuteKidneyInjuryAlert Previous
				where 
					Previous.DistrictNo = DefaultResult.DistrictNo
				and (Previous.PatientSequence + 1) = DefaultResult.PatientSequence
				and Previous.EffectiveTime between dateadd(month , -6 , DefaultResult.EffectiveTime) and DefaultResult.EffectiveTime
				)

			)
			DefaultBaseline
		on Baseline.GlobalProviderSpellNo = DefaultBaseline.GlobalProviderSpellNo
		)
		BaselineResult
	on Result.MergeResultRecno = BaselineResult.MergeResultRecno
	
	)
	AKI	
	
left join Result.BaseAcuteKidneyInjuryAlertNational NationalAKI
on AKI.MergeResultRecno = NationalAKI.MergeResultRecno

left join 
	(
	Select 
		GlobalProviderSpellNo
		,BaseWardStay.WardCode
	from
		APC.Encounter

	inner join APC.BaseWardStay
	on Encounter.SourcePatientNo = BaseWardStay.SourcePatientNo
	and Encounter.SourceSpellNo = BaseWardStay.SourceSpellNo
	and Encounter.ContextCode = BaseWardStay.ContextCode
	and BaseWardStay.EndTime is null
	and Encounter.EpisodeEndTime is null
	) 
	APCEncounter
on AKI.GlobalProviderSpellNo = APCEncounter.GlobalProviderSpellNo




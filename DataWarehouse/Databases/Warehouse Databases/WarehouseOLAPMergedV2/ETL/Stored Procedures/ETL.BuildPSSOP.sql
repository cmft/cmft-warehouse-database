﻿







CREATE proc [ETL].[BuildPSSOP]

as

/* Process OP Encounter */

-- Delete records with no associated OP Encounter record

delete from OP.PSSEncounter
where
	not exists
	(
	select
		1
	from
		OP.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSEncounter.MergeEncounterRecno
	)

-- Delete records that are about to be reinserted

delete from OP.PSSEncounter
where
	exists
	(
	select
		1
	from
		ETL.PSSOPEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSEncounter.MergeEncounterRecno
	)

insert into	OP.PSSEncounter
	(
	MergeEncounterRecno
	,[NationalProgrammeOfCareServiceCode]
	,[ServiceLine]
	,Created
	,ByWhom
	)
select
	MergeEncounterRecno
	,[NationalProgrammeOfCareServiceCode]
	,[ServiceLineCode]
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.PSSOPEncounter Encounter
where
	coalesce([NationalProgrammeOfCareServiceCode], '') <> ''
and	coalesce([ServiceLineCode], '') <> ''



/* Process Quality */

-- Delete records with no associated OP Encounter record

delete from OP.PSSQuality
where
	not exists
	(
	select
		1
	from
		OP.BaseEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSQuality.MergeEncounterRecno
	)


-- Delete records that are about to be reinserted

--delete from OP.PSSQuality
--where
--	exists
--	(
--	select
--		1
--	from
--		ETL.PSSOPQuality Quality

--	inner join ETL.PSSOPEncounter Encounter
--	on	Encounter.RowNo = Quality.RowNo

--	where
--		Encounter.MergeEncounterRecno = PSSQuality.MergeEncounterRecno
--	)

-- Delete records that might be reinserted (if we don't use this method then rows in quality table will remain even if sucessfully processed subsequently)

delete from OP.PSSQuality
where
	exists
	(
	select
		1
	from
		ETL.PSSOPEncounter Encounter
	where
		Encounter.MergeEncounterRecno = PSSQuality.MergeEncounterRecno
	)


insert into OP.PSSQuality
	(
	 MergeEncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	,Created
	,ByWhom
	)
select
	 MergeEncounterRecno
	,SequenceNo
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.PSSOPQuality Quality

inner join ETL.PSSOPEncounter Encounter
on	Quality.RowNo = Encounter.RowNo
and	Quality.FinancialYear = Encounter.FinancialYear










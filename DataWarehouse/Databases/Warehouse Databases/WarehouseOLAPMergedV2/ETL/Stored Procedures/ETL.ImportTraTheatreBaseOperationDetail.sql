﻿CREATE procedure [ETL].[ImportTraTheatreBaseOperationDetail] as

--import the data
exec ETL.BuildTraTheatreBaseOperationDetail


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Theatre].[BaseOperationDetail]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildTheatreBaseOperationDetailReference 'TRA||ORMIS'

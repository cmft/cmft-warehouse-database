﻿





CREATE procedure [ETL].[BuildDiagnosticWLBase] as

/* 
==============================================================================================
Description: 

When		Who			What
20150915	Paul Egan	APCWL:	Changed IsCancelled and IsBreach logic
20151006	Paul Egan	RAD:	Changed waiting time and breach logic to adjusted (i.e. use clock resets)
20151007	Paul Egan	OPWL:	Uncommented entire OPWL union, changed IsBreach logic, excluded new follow-ups (for now)
20151013	Paul Egan	APCWL:	Changed Operation from NULL to Intended Procedure
20151014	Paul Egan	All:	Added WaitingList
===============================================================================================
*/


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table DiagnosticWL.Base


insert
into
	DiagnosticWL.Base
(
	 SourceMergeRecno
	,ContextCode
	,CensusDate
	,DirectorateCode
	,CCGCode
	,WaitTypeCode
	,NationalExamCode
	,WeeksWaiting
	,ExamCode
	,DataSourceID
	,SpecialtyCode
	,ProfessionalCarerCode
	,AgeCode
	,WeeksWaitingAtTCI
	,ServiceRequestTypeCode
	,IsCancelled
	,SourcePatientNo
	,SourceUniqueID
	,PatientSurname
	,PatientForename
	,DistrictNo
	,LengthOfWaitDays
	,RequestDate
	,IsBookedBeyondBreach
	,IsBreach
	,ToBeSeenByDate
	,EventDate
	,Casenote
	,ExpectedAdmissionDate
	,Operation
	,WaitingList
)
select
	 Diagnostic.SourceMergeRecno
	,Diagnostic.ContextCode
	,Diagnostic.CensusDate
	,Diagnostic.DirectorateCode
	,Diagnostic.CCGCode
	,Diagnostic.WaitTypeCode
	,Diagnostic.NationalExamCode
	,Diagnostic.WeeksWaiting
	,Diagnostic.ExamCode
	,DataSource.DataSourceID
	,Diagnostic.SpecialtyCode
	,Diagnostic.ProfessionalCarerCode
	,Diagnostic.AgeCode
	,Diagnostic.WeeksWaitingAtTCI
	,Diagnostic.ServiceRequestTypeCode
	,Diagnostic.IsCancelled
	,Diagnostic.SourcePatientNo
	,Diagnostic.SourceUniqueID
	,Diagnostic.PatientSurname
	,Diagnostic.PatientForename
	,Diagnostic.DistrictNo
	,Diagnostic.LengthOfWaitDays
	,Diagnostic.RequestDate
	,Diagnostic.IsBookedBeyondBreach
	,Diagnostic.IsBreach
	,Diagnostic.ToBeSeenByDate
	,Diagnostic.EventDate
	,Diagnostic.Casenote
	,Diagnostic.ExpectedAdmissionDate
	,Diagnostic.Operation
	,Diagnostic.WaitingList
from
	(
	select
		 SourceMergeRecno = Base.MergeRecno
		,Base.ContextCode
		,Base.CensusDate
		,Base.DirectorateCode
		,NationalExamCode = Base.DiagnosticNationalExamCode
		,Base.WaitTypeCode

		,CCGCode =
			coalesce(
				 Base.CCGCode
				,Base.ResidenceCCGCode
			)

		,WeeksWaiting =
			Base.DaysWaitingAdjusted / 7

		,Base.ExamCode

		,DataSourceCode = 'RAD'

		,Base.SpecialtyCode
		,ProfessionalCarerCode = null
		,Base.AgeCode

		,WeeksWaitingAtTCI =
			DATEDIFF(
				 day
				,Base.ClockResetDate
				,case when Base.EventDate < Base.ClockResetDate then null else Base.EventDate end
			) / 7

		,ServiceRequestTypeCode =
			case
			when Base.Planned = 'Y' --Planned
			then '02' --Planned test required

			when Base.PatientTypeCode = 'A' --Inpatient
			then '04' --Test/procedure following Elective Admission

			else '01' --Waiting list for test/procedure
			end

		,IsCancelled = 0

		,Base.SourcePatientNo
		,Base.SourceUniqueID
		,Base.PatientSurname
		,Base.PatientForename
		,Base.DistrictNo
		,LengthOfWaitDays = Base.DaysWaitingAdjusted
		,Base.RequestDate

		,IsBookedBeyondBreach =
			case
			when Base.Planned = 'Y' --Planned
			then 'No'
			when datediff(day, Base.ClockResetDate, Base.EventDate) >= 42 then 'Yes'
			else 'No'
			end

		,IsBreach =
			case
			when Base.Planned = 'Y' --Planned
			then 'No'
			else
				case
				when Base.DaysWaitingAdjusted >= 42	then 'Yes'
				else 'No'
				end
			end

		,ToBeSeenByDate = cast(DATEADD(DAY, 42 - Base.DaysWaitingAdjusted, Base.CensusDate) as date)

		,EventDate = cast(Base.EventDate as date)

		,Casenote = 'CRIS No: ' + Base.SourcePatientNo
		,ExpectedAdmissionDate = null
		,Operation = null
		,WaitingList = null

	from
		DiagnosticWL.BaseRadiology Base

	union all

	select
		 Base.MergeEncounterRecno
		,Base.ContextCode
		,cast(Base.CensusDate as date)
		,Base.DirectorateCode
		,DiagnosticNationalExamCode = Allocation.SourceAllocationID
		,Base.WaitTypeCode

		,CommissionerCode =
			coalesce(
				 Base.CCGCode
				,Base.ResidenceCCGCode
			)

		,WeeksWaiting =
			Base.LengthOfWait / 7

		,ExamCode = null

		,DataSourceCode = 'APCWL'

		,Base.SpecialtyCode
		,ProfessionalCarerCode = Base.ConsultantCode
		,Base.AgeCode

		,WeeksWaitingAtTCI =
			DATEDIFF(
				 day
				,Base.DateOnWaitingList
				,case when Base.TCIDate < Base.DateOnWaitingList then null else Base.TCIDate end
			) / 7

		,ServiceRequestTypeCode =
			case
			when Base.WaitTypeCode = '20' -- Planned
			then '02' --Planned test required
			else '01' --Waiting list for test/procedure
			end

		,IsCancelled =
			case
			when Base.WLStatus = 'Preadmit Cancel'	--Changed from 'WL Cancel' 20150915 Paul Egan
			then 1
			else 0
			end

		,Base.SourcePatientNo
		,Base.SourceUniqueID
		,Base.PatientSurname
		,Base.PatientForename
		,Base.DistrictNo
		,LengthOfWaitDays = Base.LengthOfWait
		,RequestDate = cast(Base.DateOnWaitingList as date)

		,IsBookedBeyondBreach =
			case
			when 
				DATEDIFF(
					 day
					,Base.DateOnWaitingList
					,case when Base.TCIDate < Base.DateOnWaitingList then null else Base.TCIDate end
				)
				 >= 42 then 'Yes'
			else 'No'
			end

		/* Changed from 'No' 20150915 Paul Egan */
		,IsBreach =
			case
			when Base.CensusDate > cast(DATEADD(DAY, 42 - Base.LengthOfWait, Base.CensusDate) as date)
				then 'Yes'
			else 'No'
			end

		,ToBeSeenByDate = cast(DATEADD(DAY, 42 - Base.LengthOfWait, Base.CensusDate) as date)

		,EventDate =
			cast(
				case cast(Base.TCIDate as date)
				when '1 Jan 1900' then null
				else cast(Base.TCIDate as date)
				end
			 as date)

		,Casenote = Base.CasenoteNumber
		,ExpectedAdmissionDate = cast(Base.ExpectedAdmissionDate as date)
		,Operation =	-- Changed from NULL Paul Egan 20151013
			case 
				when Operation.Operation like '%specified'
					then Operation.OperationCode + right(Operation.OperationGroup, len(Operation.OperationGroup) - 3)
				else Operation.Operation
			end
		,WaitingList = Base.WaitingListCode
	from
		APCWL.BaseEncounter Base

	inner join Allocation.DatasetAllocation
	on	DatasetAllocation.DatasetCode = 'APCWAIT'
	and	DatasetAllocation.AllocationTypeID = 1
	and	DatasetAllocation.DatasetRecno = Base.MergeEncounterRecno

	inner join Allocation.Allocation
	on	Allocation.AllocationID = DatasetAllocation.AllocationID
	and	Allocation.SourceAllocationID <> '#'
	
	/* Added Paul Egan 20151013 */
	left join WH.Operation
	on Operation.OperationCode = Base.IntendedPrimaryOperationCode

	union all

	select
		 Base.MergeEncounterRecno
		,Base.ContextCode
		,Base.CensusDate
		,Base.DirectorateCode
		,DiagnosticNationalExamCode = Allocation.SourceAllocationID
		,Base.WaitTypeCode

		,CommissionerCode =
			coalesce(
				 Base.CCGCode
				,Base.ResidenceCCGCode
			)

		,WeeksWaiting =
			Base.LengthOfWait / 7

		,ExamCode = null

		,DataSourceCode = 'OPWL'

		,Base.SpecialtyCode
		,ProfessionalCarerCode = Base.ConsultantCode
		,Base.AgeCode

		,WeeksWaitingAtTCI =
			DATEDIFF(
				 day
				,Base.DateOnWaitingList
				,case when Base.AppointmentDate < Base.DateOnWaitingList then null else Base.AppointmentDate end
			) / 7

		,ServiceRequestTypeCode =
			case
			when Base.WaitTypeCode = '60' -- Planned
			then '02' --Planned test required
			else '01' --Waiting list for test/procedure
			end

		,IsCancelled =
			case
			when Base.CancelledBy is null
			then 0
			else 1
			end

		,Base.SourcePatientNo
		,Base.SourceUniqueID
		,Base.PatientSurname
		,Base.PatientForename
		,Base.DistrictNo
		,LengthOfWaitDays = Base.LengthOfWait
		,RequestDate = Base.DateOnWaitingList

		,IsBookedBeyondBreach =
			case
			when 
				DATEDIFF(
					 day
					,Base.DateOnWaitingList
					,case when Base.AppointmentDate < Base.DateOnWaitingList then null else Base.AppointmentDate end
				)
				 >= 42 then 'Yes'
			else 'No'
			end
		
		/* Changed from 'No' 20151007 Paul Egan */
		,IsBreach =
			case
			when Base.CensusDate > cast(DATEADD(DAY, 42 - Base.LengthOfWait, Base.CensusDate) as date)
				then 'Yes'
			else 'No'
			end

		,ToBeSeenByDate = cast(DATEADD(DAY, 42 - Base.LengthOfWait, Base.CensusDate) as date)

		,EventDate = cast(Base.AppointmentDate as date)

		,Casenote = Base.CasenoteNo
		,ExpectedAdmissionDate = cast(Base.ExpectedAdmissionDate as date)
		,Base.Operation
		,WaitingList = Base.WaitingListCode
	from
		OPWL.BaseEncounter Base

	inner join Allocation.DatasetAllocation
	on	DatasetAllocation.DatasetCode = 'OPWAIT'
	and	DatasetAllocation.AllocationTypeID = 1
	and	DatasetAllocation.DatasetRecno = Base.MergeEncounterRecno

	inner join Allocation.Allocation
	on	Allocation.AllocationID = DatasetAllocation.AllocationID
	and	Allocation.SourceAllocationID <> '#'
	
	where 
		DataSourceID = 1  -- Paul Egan 20151008 - currently excluding new follow-ups added by Phil Orrell, but need to check with services this is correct

	) Diagnostic

inner join Diagnostic.DataSource
on	DataSource.DataSourceCode = Diagnostic.DataSourceCode
;


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats










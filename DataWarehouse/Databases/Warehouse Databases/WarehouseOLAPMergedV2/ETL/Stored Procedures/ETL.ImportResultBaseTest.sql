﻿CREATE procedure [ETL].[ImportResultBaseTest] as

-------------------------------------------------
--When		Who	What
--20150715	CH	Created procedure
-------------------------------------------------

--load CWS and ICEs into TLoad table
exec ETL.BuildTLoadResultBaseTest

--load encounters and generate process list
exec ETL.LoadResultBaseTest

--generate missing reference data
declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Result].[BaseTest]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember

--load reference values
exec ETL.LoadResultBaseTestReference 'CEN||ICE'
exec ETL.LoadResultBaseTestReference 'CEN||GPICE'
exec ETL.LoadResultBaseTestReference 'CEN||PAS'



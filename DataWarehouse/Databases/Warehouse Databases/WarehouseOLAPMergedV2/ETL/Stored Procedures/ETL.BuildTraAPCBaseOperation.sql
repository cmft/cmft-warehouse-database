﻿CREATE procedure [ETL].[BuildTraAPCBaseOperation]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

delete
from
	APC.BaseOperation

from
	APC.BaseOperation

inner join APC.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseOperation.MergeEncounterRecno
and	BaseEncounter.ContextCode = 'TRA||UG'

where
	not exists
	(
	select
		1
	from
		ETL.TLoadTraAPCBaseOperation
	where
		TLoadTraAPCBaseOperation.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	)

select
	@deleted = @@ROWCOUNT

--remove orphaned records
delete
from
	APC.BaseOperation
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseOperation.MergeEncounterRecno
	)

and	not exists
	(
	select
		1
	from
		APC.BaseEncounterHistory
	where
		BaseEncounterHistory.MergeEncounterRecno = BaseOperation.MergeEncounterRecno
	)

select
	@deleted = @@ROWCOUNT + @deleted

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	APC.BaseOperation target
using
	(
	select
		 MergeEncounterRecno
		,SequenceNo 
		,OperationCode 
		,OperationDate
	from
		ETL.TLoadTraAPCBaseOperation
	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno
	and	source.SequenceNo = target.SequenceNo

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,SequenceNo
			,OperationCode
			,OperationDate
			,Created
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.SequenceNo
			,source.OperationCode
			,source.OperationDate
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			target.OperationCode = source.OperationCode
		)
	then
		update
		set
			 target.OperationCode = source.OperationCode
			,target.OperationDate = source.OperationDate
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
--	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

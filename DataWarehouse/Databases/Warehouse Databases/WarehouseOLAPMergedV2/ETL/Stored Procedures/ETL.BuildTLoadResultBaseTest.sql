﻿
CREATE procedure [ETL].[BuildTLoadResultBaseTest] as

truncate table ETL.TLoadResultBaseTest


insert
into
	ETL.TLoadResultBaseTest
(
	TestRecno
	,ContextCode
	,SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode
	,MainSpecialtyCode
	,ProviderCode
	,LocationCode
	,TestCode
	,TestStatus
	,InterfaceCode
)

select
	TestRecno
	,ContextCode
	,SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode
	,MainSpecialtyCode
	,ProviderCode
	,LocationCode
	,TestCode
	,TestStatus
	,InterfaceCode
from
	ETL.TImportCenResultBaseCENICETest


insert
into
	ETL.TLoadResultBaseTest
(
	TestRecno
	,ContextCode
	,SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode
	,MainSpecialtyCode
	,ProviderCode
	,LocationCode
	,TestCode
	,TestStatus
	,InterfaceCode
)

select
	TestRecno
	,ContextCode
	,SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode
	,MainSpecialtyCode
	,ProviderCode
	,LocationCode
	,TestCode
	,TestStatus
	,InterfaceCode
from
	ETL.TImportCenResultBaseGPICETest


insert
into
	ETL.TLoadResultBaseTest
(
	TestRecno
	,ContextCode
	,SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode
	,MainSpecialtyCode
	,ProviderCode
	,LocationCode
	,TestCode
	,TestStatus
	,InterfaceCode
)

select
	TestRecno
	,ContextCode
	,SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode
	,MainSpecialtyCode
	,ProviderCode
	,LocationCode
	,TestCode
	,TestStatus
	,InterfaceCode
from
	ETL.TImportCenResultBaseCWSTest



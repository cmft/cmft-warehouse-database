﻿CREATE procedure [ETL].[ImportCenAPCBaseFall] as

--import the data
exec ETL.BuildCenAPCBaseFall


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseFall]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
 exec ETL.BuildAPCBaseFallReference 'CEN||BEDMAN'


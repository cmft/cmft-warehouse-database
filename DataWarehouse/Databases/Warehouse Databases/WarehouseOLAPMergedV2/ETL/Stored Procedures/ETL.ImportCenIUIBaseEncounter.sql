﻿CREATE procedure [ETL].[ImportCenIUIBaseEncounter] as

--import the data
exec ETL.BuildCenIUIBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[IUI].[BaseEncounter]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildIUIBaseEncounterReference 'CEN||IUI'


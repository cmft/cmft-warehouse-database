﻿CREATE procedure [ETL].[LoadResultBaseTestReference] 
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Result.BaseTestReference
			where
				BaseTestReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Result.BaseTestReference
where
	not exists
	(
	select
		1
	from
		Result.BaseTest
	where
		BaseTest.MergeTestRecno = BaseTestReference.MergeTestRecno
	)
and	BaseTestReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT

merge
	Result.BaseTestReference target
using
	(
	select
		 BaseTest.MergeTestRecno
		,Context.ContextID
		,OrderRequestDateID =
			coalesce(
				 OrderRequestDate.DateID

				,case
				when BaseTest.OrderRequestTime is null
				then NullDate.DateID

				when cast(BaseTest.OrderRequestTime  as date) < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,OrderStatusID = OrderStatus.SourceOrderStatusID
		,ConsultantID = Consultant.SourceConsultantID
		,MainSpecialtyID = MainSpecialty.SourceSpecialtyID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,ProviderID = ServiceProvider.SourceServiceProviderID
		,LocationID = Location.SourceLocationID

		,TestID = Test.SourceTestID
		,BaseTest.Created
		,BaseTest.Updated
		,BaseTest.ByWhom
	from
		Result.BaseTest

	left join WH.Calendar OrderRequestDate
	on	OrderRequestDate.TheDate = cast(BaseTest.OrderRequestTime as date)

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = coalesce(cast(BaseTest.ConsultantCode as varchar(100)), '-1')
	and	Consultant.SourceContextCode = BaseTest.ContextCode

	left join WH.Specialty MainSpecialty
	on	MainSpecialty.SourceSpecialtyCode = coalesce(cast(BaseTest.MainSpecialtyCode as varchar(100)), '-1')
	and	MainSpecialty.SourceContextCode = BaseTest.ContextCode

	left join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(cast(BaseTest.SpecialtyCode as varchar(100)), '-1')
	and	Specialty.SourceContextCode = BaseTest.ContextCode

	left join Result.ServiceProvider
	on	ServiceProvider.SourceServiceProviderCode = coalesce(cast(BaseTest.ProviderCode as varchar(100)), '-1')
	and	ServiceProvider.SourceContextCode = BaseTest.ContextCode

	left join WH.Location
	on	Location.SourceLocationCode = coalesce(cast(BaseTest.LocationCode as varchar(100)), '-1')
	and	Location.SourceContextCode = BaseTest.ContextCode

	left join Result.OrderStatus
	on	OrderStatus.SourceOrderStatusCode = coalesce(cast(BaseTest.OrderStatusCode as varchar(100)), '-1')
	and	OrderStatus.SourceContextCode = BaseTest.ContextCode
	
	left join Result.Test
	on	Test.SourceTestCode = coalesce(cast(BaseTest.TestCode as varchar(100)) , '-1')
	and Test.SourceContextCode = BaseTest.ContextCode
	
	left join WH.Context
	on	Context.ContextCode = BaseTest.ContextCode

	) source
	on	source.MergeTestRecno = target.MergeTestRecno

	when not matched
	then
		insert
			(
			MergeTestRecno
			,ContextID
			,OrderRequestDateID
			,OrderStatusID
			,ConsultantID
			,MainSpecialtyID
			,SpecialtyID
			,ProviderID
			,LocationID

			,TestID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeTestRecno
			,source.ContextID
			,source.OrderRequestDateID
			,source.OrderStatusID
			,source.ConsultantID
			,source.MainSpecialtyID
			,source.SpecialtyID
			,source.ProviderID
			,source.LocationID
			,source.TestID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeTestRecno = source.MergeTestRecno
			,target.ContextID = source.ContextID
			,target.OrderRequestDateID = source.OrderRequestDateID
			,target.OrderStatusID = source.OrderStatusID
			,target.ConsultantID = source.ConsultantID
			,target.MainSpecialtyID = source.MainSpecialtyID
			,target.SpecialtyID = source.SpecialtyID
			,target.ProviderID = source.ProviderID
			,target.LocationID = source.LocationID
			,target.TestID = source.TestID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

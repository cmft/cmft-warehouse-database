﻿
CREATE procedure [ETL].[BuildCenAPCBaseEncounter]
as

/************************************************************************************************
20140731	RR	Added Reportable into Source, use in Insert, but do not use in Update
				ie if the record is new default to 1 (set in view), but don't update as 
				any recorded as non reportable yesterday will update to 1 and we don't want this.
20150729	RR	Removed Mortality following new process for Mortality reviews
*************************************************************************************************/

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 EncounterRecno = cast(EncounterRecno as bigint)
	,SourceUniqueID = cast(SourceUniqueID as varchar)
	,SourcePatientNo = cast(SourcePatientNo as varchar)
	,SourceSpellNo = cast(SourceSpellNo as varchar)
	,SourceEncounterNo = cast(SourceEncounterNo as varchar)
	,ProviderSpellNo = cast(ProviderSpellNo as varchar)
	,PatientTitle = cast(PatientTitle as varchar)
	,PatientForename = cast(PatientForename as varchar)
	,PatientSurname = cast(PatientSurname as varchar)
	,DateOfBirth = cast(DateOfBirth as datetime)
	,DateOfDeath = cast(DateOfDeath as smalldatetime)
	,SexCode = cast(SexCode as varchar)
	,NHSNumber = cast(NHSNumber as varchar)
	,Postcode = cast(Postcode as varchar)
	,PatientAddress1 = cast(PatientAddress1 as varchar)
	,PatientAddress2 = cast(PatientAddress2 as varchar)
	,PatientAddress3 = cast(PatientAddress3 as varchar)
	,PatientAddress4 = cast(PatientAddress4 as varchar)
	,DHACode = cast(DHACode as varchar)
	,EthnicOriginCode = cast(EthnicOriginCode as varchar)
	,MaritalStatusCode = cast(MaritalStatusCode as varchar)
	,ReligionCode = cast(ReligionCode as varchar)
	,DateOnWaitingList = cast(DateOnWaitingList as smalldatetime)
	,AdmissionDate = cast(AdmissionDate as smalldatetime)
	,DischargeDate = cast(DischargeDate as smalldatetime)
	,EpisodeStartDate = cast(EpisodeStartDate as smalldatetime)
	,EpisodeEndDate = cast(EpisodeEndDate as smalldatetime)
	,StartSiteCode = cast(StartSiteCode as varchar)
	,StartWardTypeCode = cast(StartWardTypeCode as varchar)
	,EndSiteCode = cast(EndSiteCode as varchar)
	,EndWardTypeCode = cast(EndWardTypeCode as varchar)
	,RegisteredGpCode = cast(RegisteredGpCode as varchar)
	,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar)
	,SiteCode = cast(SiteCode as varchar)
	,AdmissionMethodCode = cast(AdmissionMethodCode as varchar)
	,AdmissionSourceCode = cast(AdmissionSourceCode as varchar)
	,PatientClassificationCode = cast(PatientClassificationCode as varchar)
	,ManagementIntentionCode = cast(ManagementIntentionCode as varchar)
	,DischargeMethodCode = cast(DischargeMethodCode as varchar)
	,DischargeDestinationCode = cast(DischargeDestinationCode as varchar)
	,AdminCategoryCode = cast(AdminCategoryCode as varchar)
	,ConsultantCode = cast(ConsultantCode as varchar)
	,SpecialtyCode = cast(SpecialtyCode as varchar)
	,LastEpisodeInSpellIndicator = cast(LastEpisodeInSpellIndicator as varchar)
	,FirstRegDayOrNightAdmit = cast(FirstRegDayOrNightAdmit as varchar)
	,NeonatalLevelOfCare = cast(NeonatalLevelOfCare as varchar)
	,PASHRGCode = cast(PASHRGCode as varchar)
	,PrimaryDiagnosisCode = cast(PrimaryDiagnosisCode as varchar)
	,SubsidiaryDiagnosisCode = cast(SubsidiaryDiagnosisCode as varchar)
	,SecondaryDiagnosisCode1 = cast(SecondaryDiagnosisCode1 as varchar)
	,SecondaryDiagnosisCode2 = cast(SecondaryDiagnosisCode2 as varchar)
	,SecondaryDiagnosisCode3 = cast(SecondaryDiagnosisCode3 as varchar)
	,SecondaryDiagnosisCode4 = cast(SecondaryDiagnosisCode4 as varchar)
	,SecondaryDiagnosisCode5 = cast(SecondaryDiagnosisCode5 as varchar)
	,SecondaryDiagnosisCode6 = cast(SecondaryDiagnosisCode6 as varchar)
	,SecondaryDiagnosisCode7 = cast(SecondaryDiagnosisCode7 as varchar)
	,SecondaryDiagnosisCode8 = cast(SecondaryDiagnosisCode8 as varchar)
	,SecondaryDiagnosisCode9 = cast(SecondaryDiagnosisCode9 as varchar)
	,SecondaryDiagnosisCode10 = cast(SecondaryDiagnosisCode10 as varchar)
	,SecondaryDiagnosisCode11 = cast(SecondaryDiagnosisCode11 as varchar)
	,SecondaryDiagnosisCode12 = cast(SecondaryDiagnosisCode12 as varchar)
	,SecondaryDiagnosisCode13 = cast(SecondaryDiagnosisCode13 as varchar)
	,PrimaryProcedureCode = cast(PrimaryProcedureCode as varchar)
	,PrimaryProcedureDate = cast(PrimaryProcedureDate as smalldatetime)
	,SecondaryProcedureCode1 = cast(SecondaryProcedureCode1 as varchar)
	,SecondaryProcedureDate1 = cast(SecondaryProcedureDate1 as smalldatetime)
	,SecondaryProcedureCode2 = cast(SecondaryProcedureCode2 as varchar)
	,SecondaryProcedureDate2 = cast(SecondaryProcedureDate2 as smalldatetime)
	,SecondaryProcedureCode3 = cast(SecondaryProcedureCode3 as varchar)
	,SecondaryProcedureDate3 = cast(SecondaryProcedureDate3 as smalldatetime)
	,SecondaryProcedureCode4 = cast(SecondaryProcedureCode4 as varchar)
	,SecondaryProcedureDate4 = cast(SecondaryProcedureDate4 as smalldatetime)
	,SecondaryProcedureCode5 = cast(SecondaryProcedureCode5 as varchar)
	,SecondaryProcedureDate5 = cast(SecondaryProcedureDate5 as smalldatetime)
	,SecondaryProcedureCode6 = cast(SecondaryProcedureCode6 as varchar)
	,SecondaryProcedureDate6 = cast(SecondaryProcedureDate6 as smalldatetime)
	,SecondaryProcedureCode7 = cast(SecondaryProcedureCode7 as varchar)
	,SecondaryProcedureDate7 = cast(SecondaryProcedureDate7 as smalldatetime)
	,SecondaryProcedureCode8 = cast(SecondaryProcedureCode8 as varchar)
	,SecondaryProcedureDate8 = cast(SecondaryProcedureDate8 as smalldatetime)
	,SecondaryProcedureCode9 = cast(SecondaryProcedureCode9 as varchar)
	,SecondaryProcedureDate9 = cast(SecondaryProcedureDate9 as smalldatetime)
	,SecondaryProcedureCode10 = cast(SecondaryProcedureCode10 as varchar)
	,SecondaryProcedureDate10 = cast(SecondaryProcedureDate10 as smalldatetime)
	,SecondaryProcedureCode11 = cast(SecondaryProcedureCode11 as varchar)
	,SecondaryProcedureDate11 = cast(SecondaryProcedureDate11 as smalldatetime)
	,OperationStatusCode = cast(OperationStatusCode as varchar)
	,ContractSerialNo = cast(ContractSerialNo as varchar)
	,CodingCompleteDate = cast(CodingCompleteDate as smalldatetime)
	,PurchaserCode = cast(PurchaserCode as varchar)
	,ProviderCode = cast(ProviderCode as varchar)
	,EpisodeStartTime = cast(EpisodeStartTime as smalldatetime)
	,EpisodeEndTime = cast(EpisodeEndTime as smalldatetime)
	,RegisteredGdpCode = cast(RegisteredGdpCode as varchar)
	,EpisodicGpCode = cast(EpisodicGpCode as varchar)
	,InterfaceCode = cast(InterfaceCode as varchar)
	,CasenoteNumber = cast(CasenoteNumber as varchar)
	,NHSNumberStatusCode = cast(NHSNumberStatusCode as varchar)
	,AdmissionTime = cast(AdmissionTime as smalldatetime)
	,DischargeTime = cast(DischargeTime as smalldatetime)
	,TransferFrom = cast(TransferFrom as varchar)
	,DistrictNo = cast(DistrictNo as varchar)
	,ExpectedLOS = cast(ExpectedLOS as int)
	,MRSAFlag = cast(MRSAFlag as varchar)
	,RTTPathwayID = cast(RTTPathwayID as varchar)
	,RTTPathwayCondition = cast(RTTPathwayCondition as varchar)
	,RTTStartDate = cast(RTTStartDate as smalldatetime)
	,RTTEndDate = cast(RTTEndDate as smalldatetime)
	,RTTSpecialtyCode = cast(RTTSpecialtyCode as varchar)
	,RTTCurrentProviderCode = cast(RTTCurrentProviderCode as varchar)
	,RTTCurrentStatusCode = cast(RTTCurrentStatusCode as varchar)
	,RTTCurrentStatusDate = cast(RTTCurrentStatusDate as smalldatetime)
	,RTTCurrentPrivatePatientFlag = cast(RTTCurrentPrivatePatientFlag as bit)
	,RTTOverseasStatusFlag = cast(RTTOverseasStatusFlag as bit)
	,RTTPeriodStatusCode = cast(RTTPeriodStatusCode as varchar)
	,IntendedPrimaryProcedureCode = cast(IntendedPrimaryProcedureCode as varchar)
	,Operation = cast(Operation as varchar)
	,Research1 = cast(Research1 as varchar)
	,CancelledElectiveAdmissionFlag = cast(CancelledElectiveAdmissionFlag as varchar)
	,LocalAdminCategoryCode = cast(LocalAdminCategoryCode as varchar)
	,EpisodicGpPracticeCode = cast(EpisodicGpPracticeCode as varchar)
	,PCTCode = cast(PCTCode as varchar)
	,LocalityCode = cast(LocalityCode as varchar)
	,AgeCode = cast(AgeCode as varchar)
	--,HRGCode = cast(HRGCode as varchar)
	--,SpellHRGCode = cast(SpellHRGCode as varchar)
	,CategoryCode = cast(CategoryCode as varchar)
	,LOE = cast(LOE as smallint)
	,LOS = cast(LOS as smallint)
	,Cases = cast(Cases as smallint)
	,FirstEpisodeInSpellIndicator = cast(FirstEpisodeInSpellIndicator as bit)
	,RTTActivity = cast(RTTActivity as bit)
	,RTTBreachStatusCode = cast(RTTBreachStatusCode as varchar)
	,DiagnosticProcedure = cast(DiagnosticProcedure as bit)
	,RTTTreated = cast(RTTTreated as bit)
	,EpisodeStartTimeOfDay = cast(EpisodeStartTimeOfDay as int)
	,EpisodeEndTimeOfDay = cast(EpisodeEndTimeOfDay as int)
	,AdmissionTimeOfDay = cast(AdmissionTimeOfDay as int)
	,DischargeTimeOfDay = cast(DischargeTimeOfDay as int)
	--,StartDirectorateCode = cast(StartDirectorateCode as varchar)
	--,EndDirectorateCode = cast(EndDirectorateCode as varchar)
	,ResidencePCTCode = cast(ResidencePCTCode as varchar)
	,ISTAdmissionSpecialtyCode = cast(ISTAdmissionSpecialtyCode as varchar)
	,ISTAdmissionDemandTime = cast(ISTAdmissionDemandTime as smalldatetime)
	,ISTDischargeTime = cast(ISTDischargeTime as smalldatetime)
	,ISTAdmissionDemandTimeOfDay = cast(ISTAdmissionDemandTimeOfDay as int)
	,ISTDischargeTimeOfDay = cast(ISTDischargeTimeOfDay as int)
	,PatientCategoryCode = cast(PatientCategoryCode as varchar)
	,Research2 = cast(Research2 as varchar)
	,ClinicalCodingStatus = cast(ClinicalCodingStatus as varchar)
	,ClinicalCodingCompleteDate = cast(ClinicalCodingCompleteDate as smalldatetime)
	,ClinicalCodingCompleteTime = cast(ClinicalCodingCompleteTime as smalldatetime)
	,ReferredByCode = cast(ReferredByCode as varchar)
	,ReferrerCode = cast(ReferrerCode as varchar)
	,DecidedToAdmitDate = cast(DecidedToAdmitDate as smalldatetime)
	,PsychiatricPatientStatusCode = cast(PsychiatricPatientStatusCode as varchar)
	,LegalStatusClassificationCode = cast(LegalStatusClassificationCode as varchar)
	,ReferringConsultantCode = cast(ReferringConsultantCode as varchar)
	,DurationOfElectiveWait = cast(DurationOfElectiveWait as int)
	,CarerSupportIndicator = cast(CarerSupportIndicator as varchar)
	,DischargeReadyDate = cast(DischargeReadyDate as smalldatetime)
	,ContextCode = cast(ContextCode as varchar)
	,EddCreatedTime = cast(EddCreatedTime as datetime)
	,ExpectedDateofDischarge = cast(ExpectedDateOfDischarge as smalldatetime)
	,EddCreatedByConsultantFlag = cast(EddCreatedByConsultantFlag as bit)
	,EddInterfaceCode = cast(EddInterfaceCode as varchar)
	,VTECategoryCode = cast(VTECategoryCode as char)
	,Ambulatory = cast(Ambulatory as bit)
	,PseudoPostcode = cast(PseudoPostcode as varchar)
	,CCGCode = cast(CCGCode as varchar)
	,GpCodeAtDischarge = cast(GpCodeAtDischarge as varchar)
	,GpPracticeCodeAtDischarge = cast(GpPracticeCodeAtDischarge as varchar)
	,PostcodeAtDischarge = cast(PostcodeAtDischarge as varchar)
	,ResidenceCCGCode = cast(ResidenceCCGCode as varchar)
	,Reportable = cast(Reportable as bit)
	,CharlsonIndex = cast(CharlsonIndex as int)
into
	#Encounter
from
	ETL.TLoadCenAPCBaseEncounter Encounter

CREATE UNIQUE CLUSTERED INDEX IX_TLoadCenOPBaseEncounter ON #Encounter
	(
	 EncounterRecno
	,ContextCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


merge
	APC.BaseEncounter target
using
	(
	select
		 EncounterRecno
		,SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,SourceEncounterNo
		,ProviderSpellNo
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,NHSNumber
		,Postcode
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,DHACode
		,EthnicOriginCode
		,MaritalStatusCode
		,ReligionCode
		,DateOnWaitingList
		,AdmissionDate
		,DischargeDate
		,EpisodeStartDate
		,EpisodeEndDate
		,StartSiteCode
		,StartWardTypeCode
		,EndSiteCode
		,EndWardTypeCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,SiteCode
		,AdmissionMethodCode
		,AdmissionSourceCode
		,PatientClassificationCode
		,ManagementIntentionCode
		,DischargeMethodCode
		,DischargeDestinationCode
		,AdminCategoryCode
		,ConsultantCode
		,SpecialtyCode
		,LastEpisodeInSpellIndicator
		,FirstRegDayOrNightAdmit
		,NeonatalLevelOfCare
		,PASHRGCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,SecondaryDiagnosisCode13
		,PrimaryProcedureCode
		,PrimaryProcedureDate
		,SecondaryProcedureCode1
		,SecondaryProcedureDate1
		,SecondaryProcedureCode2
		,SecondaryProcedureDate2
		,SecondaryProcedureCode3
		,SecondaryProcedureDate3
		,SecondaryProcedureCode4
		,SecondaryProcedureDate4
		,SecondaryProcedureCode5
		,SecondaryProcedureDate5
		,SecondaryProcedureCode6
		,SecondaryProcedureDate6
		,SecondaryProcedureCode7
		,SecondaryProcedureDate7
		,SecondaryProcedureCode8
		,SecondaryProcedureDate8
		,SecondaryProcedureCode9
		,SecondaryProcedureDate9
		,SecondaryProcedureCode10
		,SecondaryProcedureDate10
		,SecondaryProcedureCode11
		,SecondaryProcedureDate11
		,OperationStatusCode
		,ContractSerialNo
		,CodingCompleteDate
		,PurchaserCode
		,ProviderCode
		,EpisodeStartTime
		,EpisodeEndTime
		,RegisteredGdpCode
		,EpisodicGpCode
		,InterfaceCode
		,CasenoteNumber
		,NHSNumberStatusCode
		,AdmissionTime
		,DischargeTime
		,TransferFrom
		,DistrictNo
		,ExpectedLOS
		,MRSAFlag
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,IntendedPrimaryProcedureCode
		,Operation
		,Research1
		,CancelledElectiveAdmissionFlag
		,LocalAdminCategoryCode
		,EpisodicGpPracticeCode
		,PCTCode
		,LocalityCode
		,AgeCode
		--,HRGCode
		--,SpellHRGCode
		,CategoryCode
		,LOE
		,LOS
		,Cases
		,FirstEpisodeInSpellIndicator
		,RTTActivity
		,RTTBreachStatusCode
		,DiagnosticProcedure
		,RTTTreated
		,EpisodeStartTimeOfDay
		,EpisodeEndTimeOfDay
		,AdmissionTimeOfDay
		,DischargeTimeOfDay
		--,StartDirectorateCode
		--,EndDirectorateCode
		,ResidencePCTCode
		,ISTAdmissionSpecialtyCode
		,ISTAdmissionDemandTime
		,ISTDischargeTime
		,ISTAdmissionDemandTimeOfDay
		,ISTDischargeTimeOfDay
		,PatientCategoryCode
		,Research2
		,ClinicalCodingStatus
		,ClinicalCodingCompleteDate
		,ClinicalCodingCompleteTime
		,ReferredByCode
		,ReferrerCode
		,DecidedToAdmitDate
		,PsychiatricPatientStatusCode
		,LegalStatusClassificationCode
		,ReferringConsultantCode
		,DurationOfElectiveWait
		,CarerSupportIndicator
		,DischargeReadyDate
		,ContextCode
		,EddCreatedTime
		,ExpectedDateOfDischarge
		,EddCreatedByConsultantFlag
		,EddInterfaceCode
		,VTECategoryCode
		,Ambulatory
		,PseudoPostcode
		,CCGCode 
		,GpCodeAtDischarge
		,GpPracticeCodeAtDischarge
		,PostcodeAtDischarge
		,ResidenceCCGCode
		,Reportable
		,CharlsonIndex
	from
		#Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno

	when not matched by source
	and	target.ContextCode = 'CEN||PAS'
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,SourceEncounterNo
			,ProviderSpellNo
			,PatientTitle
			,PatientForename
			,PatientSurname
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,NHSNumber
			,Postcode
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,DHACode
			,EthnicOriginCode
			,MaritalStatusCode
			,ReligionCode
			,DateOnWaitingList
			,AdmissionDate
			,DischargeDate
			,EpisodeStartDate
			,EpisodeEndDate
			,StartSiteCode
			,StartWardTypeCode
			,EndSiteCode
			,EndWardTypeCode
			,RegisteredGpCode
			,RegisteredGpPracticeCode
			,SiteCode
			,AdmissionMethodCode
			,AdmissionSourceCode
			,PatientClassificationCode
			,ManagementIntentionCode
			,DischargeMethodCode
			,DischargeDestinationCode
			,AdminCategoryCode
			,ConsultantCode
			,SpecialtyCode
			,LastEpisodeInSpellIndicator
			,FirstRegDayOrNightAdmit
			,NeonatalLevelOfCare
			,PASHRGCode
			,PrimaryDiagnosisCode
			,SubsidiaryDiagnosisCode
			,SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			,SecondaryDiagnosisCode13
			,PrimaryProcedureCode
			,PrimaryProcedureDate
			,SecondaryProcedureCode1
			,SecondaryProcedureDate1
			,SecondaryProcedureCode2
			,SecondaryProcedureDate2
			,SecondaryProcedureCode3
			,SecondaryProcedureDate3
			,SecondaryProcedureCode4
			,SecondaryProcedureDate4
			,SecondaryProcedureCode5
			,SecondaryProcedureDate5
			,SecondaryProcedureCode6
			,SecondaryProcedureDate6
			,SecondaryProcedureCode7
			,SecondaryProcedureDate7
			,SecondaryProcedureCode8
			,SecondaryProcedureDate8
			,SecondaryProcedureCode9
			,SecondaryProcedureDate9
			,SecondaryProcedureCode10
			,SecondaryProcedureDate10
			,SecondaryProcedureCode11
			,SecondaryProcedureDate11
			,OperationStatusCode
			,ContractSerialNo
			,CodingCompleteDate
			,PurchaserCode
			,ProviderCode
			,EpisodeStartTime
			,EpisodeEndTime
			,RegisteredGdpCode
			,EpisodicGpCode
			,InterfaceCode
			,CasenoteNumber
			,NHSNumberStatusCode
			,AdmissionTime
			,DischargeTime
			,TransferFrom
			,DistrictNo
			,ExpectedLOS
			,MRSAFlag
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,RTTPeriodStatusCode
			,IntendedPrimaryProcedureCode
			,Operation
			,Research1
			,CancelledElectiveAdmissionFlag
			,LocalAdminCategoryCode
			,EpisodicGpPracticeCode
			,PCTCode
			,LocalityCode
			,AgeCode
			--,HRGCode
			--,SpellHRGCode
			,CategoryCode
			,LOE
			,LOS
			,Cases
			,FirstEpisodeInSpellIndicator
			,RTTActivity
			,RTTBreachStatusCode
			,DiagnosticProcedure
			,RTTTreated
			,EpisodeStartTimeOfDay
			,EpisodeEndTimeOfDay
			,AdmissionTimeOfDay
			,DischargeTimeOfDay
			--,StartDirectorateCode
			--,EndDirectorateCode
			,ResidencePCTCode
			,ISTAdmissionSpecialtyCode
			,ISTAdmissionDemandTime
			,ISTDischargeTime
			,ISTAdmissionDemandTimeOfDay
			,ISTDischargeTimeOfDay
			,PatientCategoryCode
			,Research2
			,ClinicalCodingStatus
			,ClinicalCodingCompleteDate
			,ClinicalCodingCompleteTime
			,ReferredByCode
			,ReferrerCode
			,DecidedToAdmitDate
			,PsychiatricPatientStatusCode
			,LegalStatusClassificationCode
			,ReferringConsultantCode
			,DurationOfElectiveWait
			,CarerSupportIndicator
			,DischargeReadyDate
			,ContextCode
			,EddCreatedTime
			,ExpectedDateOfDischarge
			,EddCreatedByConsultantFlag
			,EddInterfaceCode
			,VTECategoryCode
			,Ambulatory
			,Created
			,Updated
			,ByWhom
			,PseudoPostcode
			,CCGCode 
			,GpCodeAtDischarge
			,GpPracticeCodeAtDischarge
			,PostcodeAtDischarge
			,ResidenceCCGCode
			,Reportable
			,CharlsonIndex
			)
		values
			(
			 source.EncounterRecno
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.SourceEncounterNo
			,source.ProviderSpellNo
			,source.PatientTitle
			,source.PatientForename
			,source.PatientSurname
			,source.DateOfBirth
			,source.DateOfDeath
			,source.SexCode
			,source.NHSNumber
			,source.Postcode
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.DHACode
			,source.EthnicOriginCode
			,source.MaritalStatusCode
			,source.ReligionCode
			,source.DateOnWaitingList
			,source.AdmissionDate
			,source.DischargeDate
			,source.EpisodeStartDate
			,source.EpisodeEndDate
			,source.StartSiteCode
			,source.StartWardTypeCode
			,source.EndSiteCode
			,source.EndWardTypeCode
			,source.RegisteredGpCode
			,source.RegisteredGpPracticeCode
			,source.SiteCode
			,source.AdmissionMethodCode
			,source.AdmissionSourceCode
			,source.PatientClassificationCode
			,source.ManagementIntentionCode
			,source.DischargeMethodCode
			,source.DischargeDestinationCode
			,source.AdminCategoryCode
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.LastEpisodeInSpellIndicator
			,source.FirstRegDayOrNightAdmit
			,source.NeonatalLevelOfCare
			,source.PASHRGCode
			,source.PrimaryDiagnosisCode
			,source.SubsidiaryDiagnosisCode
			,source.SecondaryDiagnosisCode1
			,source.SecondaryDiagnosisCode2
			,source.SecondaryDiagnosisCode3
			,source.SecondaryDiagnosisCode4
			,source.SecondaryDiagnosisCode5
			,source.SecondaryDiagnosisCode6
			,source.SecondaryDiagnosisCode7
			,source.SecondaryDiagnosisCode8
			,source.SecondaryDiagnosisCode9
			,source.SecondaryDiagnosisCode10
			,source.SecondaryDiagnosisCode11
			,source.SecondaryDiagnosisCode12
			,source.SecondaryDiagnosisCode13
			,source.PrimaryProcedureCode
			,source.PrimaryProcedureDate
			,source.SecondaryProcedureCode1
			,source.SecondaryProcedureDate1
			,source.SecondaryProcedureCode2
			,source.SecondaryProcedureDate2
			,source.SecondaryProcedureCode3
			,source.SecondaryProcedureDate3
			,source.SecondaryProcedureCode4
			,source.SecondaryProcedureDate4
			,source.SecondaryProcedureCode5
			,source.SecondaryProcedureDate5
			,source.SecondaryProcedureCode6
			,source.SecondaryProcedureDate6
			,source.SecondaryProcedureCode7
			,source.SecondaryProcedureDate7
			,source.SecondaryProcedureCode8
			,source.SecondaryProcedureDate8
			,source.SecondaryProcedureCode9
			,source.SecondaryProcedureDate9
			,source.SecondaryProcedureCode10
			,source.SecondaryProcedureDate10
			,source.SecondaryProcedureCode11
			,source.SecondaryProcedureDate11
			,source.OperationStatusCode
			,source.ContractSerialNo
			,source.CodingCompleteDate
			,source.PurchaserCode
			,source.ProviderCode
			,source.EpisodeStartTime
			,source.EpisodeEndTime
			,source.RegisteredGdpCode
			,source.EpisodicGpCode
			,source.InterfaceCode
			,source.CasenoteNumber
			,source.NHSNumberStatusCode
			,source.AdmissionTime
			,source.DischargeTime
			,source.TransferFrom
			,source.DistrictNo
			,source.ExpectedLOS
			,source.MRSAFlag
			,source.RTTPathwayID
			,source.RTTPathwayCondition
			,source.RTTStartDate
			,source.RTTEndDate
			,source.RTTSpecialtyCode
			,source.RTTCurrentProviderCode
			,source.RTTCurrentStatusCode
			,source.RTTCurrentStatusDate
			,source.RTTCurrentPrivatePatientFlag
			,source.RTTOverseasStatusFlag
			,source.RTTPeriodStatusCode
			,source.IntendedPrimaryProcedureCode
			,source.Operation
			,source.Research1
			,source.CancelledElectiveAdmissionFlag
			,source.LocalAdminCategoryCode
			,source.EpisodicGpPracticeCode
			,source.PCTCode
			,source.LocalityCode
			,source.AgeCode
			--,source.HRGCode
			--,source.SpellHRGCode
			,source.CategoryCode
			,source.LOE
			,source.LOS
			,source.Cases
			,source.FirstEpisodeInSpellIndicator
			,source.RTTActivity
			,source.RTTBreachStatusCode
			,source.DiagnosticProcedure
			,source.RTTTreated
			,source.EpisodeStartTimeOfDay
			,source.EpisodeEndTimeOfDay
			,source.AdmissionTimeOfDay
			,source.DischargeTimeOfDay
			--,source.StartDirectorateCode
			--,source.EndDirectorateCode
			,source.ResidencePCTCode
			,source.ISTAdmissionSpecialtyCode
			,source.ISTAdmissionDemandTime
			,source.ISTDischargeTime
			,source.ISTAdmissionDemandTimeOfDay
			,source.ISTDischargeTimeOfDay
			,source.PatientCategoryCode
			,source.Research2
			,source.ClinicalCodingStatus
			,source.ClinicalCodingCompleteDate
			,source.ClinicalCodingCompleteTime
			,source.ReferredByCode
			,source.ReferrerCode
			,source.DecidedToAdmitDate
			,source.PsychiatricPatientStatusCode
			,source.LegalStatusClassificationCode
			,source.ReferringConsultantCode
			,source.DurationOfElectiveWait
			,source.CarerSupportIndicator
			,source.DischargeReadyDate
			,source.ContextCode
			,source.EddCreatedTime
			,source.ExpectedDateOfDischarge
			,source.EddCreatedByConsultantFlag
			,source.EddInterfaceCode
			,source.VTECategoryCode
			,source.Ambulatory
			,getdate()
			,getdate()
			,suser_name()
			,source.PseudoPostcode
			,source.CCGCode 
			,source.GpCodeAtDischarge
			,source.GpPracticeCodeAtDischarge
			,source.PostcodeAtDischarge
			,source.ResidenceCCGCode
			,source.Reportable
			,source.CharlsonIndex
			)

	when matched
	and not
		(
			isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.SourceEncounterNo, '') = isnull(source.SourceEncounterNo, '')
		and isnull(target.ProviderSpellNo, '') = isnull(source.ProviderSpellNo, '')
		and isnull(target.PatientTitle, '') = isnull(source.PatientTitle, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.PatientAddress1, '') = isnull(source.PatientAddress1, '')
		and isnull(target.PatientAddress2, '') = isnull(source.PatientAddress2, '')
		and isnull(target.PatientAddress3, '') = isnull(source.PatientAddress3, '')
		and isnull(target.PatientAddress4, '') = isnull(source.PatientAddress4, '')
		and isnull(target.DHACode, '') = isnull(source.DHACode, '')
		and isnull(target.EthnicOriginCode, '') = isnull(source.EthnicOriginCode, '')
		and isnull(target.MaritalStatusCode, '') = isnull(source.MaritalStatusCode, '')
		and isnull(target.ReligionCode, '') = isnull(source.ReligionCode, '')
		and isnull(target.DateOnWaitingList, getdate()) = isnull(source.DateOnWaitingList, getdate())
		and isnull(target.AdmissionDate, getdate()) = isnull(source.AdmissionDate, getdate())
		and isnull(target.DischargeDate, getdate()) = isnull(source.DischargeDate, getdate())
		and isnull(target.EpisodeStartDate, getdate()) = isnull(source.EpisodeStartDate, getdate())
		and isnull(target.EpisodeEndDate, getdate()) = isnull(source.EpisodeEndDate, getdate())
		and isnull(target.StartSiteCode, '') = isnull(source.StartSiteCode, '')
		and isnull(target.StartWardTypeCode, '') = isnull(source.StartWardTypeCode, '')
		and isnull(target.EndSiteCode, '') = isnull(source.EndSiteCode, '')
		and isnull(target.EndWardTypeCode, '') = isnull(source.EndWardTypeCode, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredGpPracticeCode, '') = isnull(source.RegisteredGpPracticeCode, '')
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.AdmissionMethodCode, '') = isnull(source.AdmissionMethodCode, '')
		and isnull(target.AdmissionSourceCode, '') = isnull(source.AdmissionSourceCode, '')
		and isnull(target.PatientClassificationCode, '') = isnull(source.PatientClassificationCode, '')
		and isnull(target.ManagementIntentionCode, '') = isnull(source.ManagementIntentionCode, '')
		and isnull(target.DischargeMethodCode, '') = isnull(source.DischargeMethodCode, '')
		and isnull(target.DischargeDestinationCode, '') = isnull(source.DischargeDestinationCode, '')
		and isnull(target.AdminCategoryCode, '') = isnull(source.AdminCategoryCode, '')
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		and isnull(target.LastEpisodeInSpellIndicator, '') = isnull(source.LastEpisodeInSpellIndicator, '')
		and isnull(target.FirstRegDayOrNightAdmit, '') = isnull(source.FirstRegDayOrNightAdmit, '')
		and isnull(target.NeonatalLevelOfCare, '') = isnull(source.NeonatalLevelOfCare, '')
		and isnull(target.PASHRGCode, '') = isnull(source.PASHRGCode, '')
		and isnull(target.PrimaryDiagnosisCode, '') = isnull(source.PrimaryDiagnosisCode, '')
		and isnull(target.SubsidiaryDiagnosisCode, '') = isnull(source.SubsidiaryDiagnosisCode, '')
		and isnull(target.SecondaryDiagnosisCode1, '') = isnull(source.SecondaryDiagnosisCode1, '')
		and isnull(target.SecondaryDiagnosisCode2, '') = isnull(source.SecondaryDiagnosisCode2, '')
		and isnull(target.SecondaryDiagnosisCode3, '') = isnull(source.SecondaryDiagnosisCode3, '')
		and isnull(target.SecondaryDiagnosisCode4, '') = isnull(source.SecondaryDiagnosisCode4, '')
		and isnull(target.SecondaryDiagnosisCode5, '') = isnull(source.SecondaryDiagnosisCode5, '')
		and isnull(target.SecondaryDiagnosisCode6, '') = isnull(source.SecondaryDiagnosisCode6, '')
		and isnull(target.SecondaryDiagnosisCode7, '') = isnull(source.SecondaryDiagnosisCode7, '')
		and isnull(target.SecondaryDiagnosisCode8, '') = isnull(source.SecondaryDiagnosisCode8, '')
		and isnull(target.SecondaryDiagnosisCode9, '') = isnull(source.SecondaryDiagnosisCode9, '')
		and isnull(target.SecondaryDiagnosisCode10, '') = isnull(source.SecondaryDiagnosisCode10, '')
		and isnull(target.SecondaryDiagnosisCode11, '') = isnull(source.SecondaryDiagnosisCode11, '')
		and isnull(target.SecondaryDiagnosisCode12, '') = isnull(source.SecondaryDiagnosisCode12, '')
		and isnull(target.SecondaryDiagnosisCode13, '') = isnull(source.SecondaryDiagnosisCode13, '')
		and isnull(target.PrimaryProcedureCode, '') = isnull(source.PrimaryProcedureCode, '')
		and isnull(target.PrimaryProcedureDate, getdate()) = isnull(source.PrimaryProcedureDate, getdate())
		and isnull(target.SecondaryProcedureCode1, '') = isnull(source.SecondaryProcedureCode1, '')
		and isnull(target.SecondaryProcedureDate1, getdate()) = isnull(source.SecondaryProcedureDate1, getdate())
		and isnull(target.SecondaryProcedureCode2, '') = isnull(source.SecondaryProcedureCode2, '')
		and isnull(target.SecondaryProcedureDate2, getdate()) = isnull(source.SecondaryProcedureDate2, getdate())
		and isnull(target.SecondaryProcedureCode3, '') = isnull(source.SecondaryProcedureCode3, '')
		and isnull(target.SecondaryProcedureDate3, getdate()) = isnull(source.SecondaryProcedureDate3, getdate())
		and isnull(target.SecondaryProcedureCode4, '') = isnull(source.SecondaryProcedureCode4, '')
		and isnull(target.SecondaryProcedureDate4, getdate()) = isnull(source.SecondaryProcedureDate4, getdate())
		and isnull(target.SecondaryProcedureCode5, '') = isnull(source.SecondaryProcedureCode5, '')
		and isnull(target.SecondaryProcedureDate5, getdate()) = isnull(source.SecondaryProcedureDate5, getdate())
		and isnull(target.SecondaryProcedureCode6, '') = isnull(source.SecondaryProcedureCode6, '')
		and isnull(target.SecondaryProcedureDate6, getdate()) = isnull(source.SecondaryProcedureDate6, getdate())
		and isnull(target.SecondaryProcedureCode7, '') = isnull(source.SecondaryProcedureCode7, '')
		and isnull(target.SecondaryProcedureDate7, getdate()) = isnull(source.SecondaryProcedureDate7, getdate())
		and isnull(target.SecondaryProcedureCode8, '') = isnull(source.SecondaryProcedureCode8, '')
		and isnull(target.SecondaryProcedureDate8, getdate()) = isnull(source.SecondaryProcedureDate8, getdate())
		and isnull(target.SecondaryProcedureCode9, '') = isnull(source.SecondaryProcedureCode9, '')
		and isnull(target.SecondaryProcedureDate9, getdate()) = isnull(source.SecondaryProcedureDate9, getdate())
		and isnull(target.SecondaryProcedureCode10, '') = isnull(source.SecondaryProcedureCode10, '')
		and isnull(target.SecondaryProcedureDate10, getdate()) = isnull(source.SecondaryProcedureDate10, getdate())
		and isnull(target.SecondaryProcedureCode11, '') = isnull(source.SecondaryProcedureCode11, '')
		and isnull(target.SecondaryProcedureDate11, getdate()) = isnull(source.SecondaryProcedureDate11, getdate())
		and isnull(target.OperationStatusCode, '') = isnull(source.OperationStatusCode, '')
		and isnull(target.ContractSerialNo, '') = isnull(source.ContractSerialNo, '')
		and isnull(target.CodingCompleteDate, getdate()) = isnull(source.CodingCompleteDate, getdate())
		--and isnull(target.PurchaserCode, '') = isnull(source.PurchaserCode, '') --assigned later
		and isnull(target.ProviderCode, '') = isnull(source.ProviderCode, '')
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.EpisodeEndTime, getdate()) = isnull(source.EpisodeEndTime, getdate())
		and isnull(target.RegisteredGdpCode, '') = isnull(source.RegisteredGdpCode, '')
		and isnull(target.EpisodicGpCode, '') = isnull(source.EpisodicGpCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.NHSNumberStatusCode, '') = isnull(source.NHSNumberStatusCode, '')
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.DischargeTime, getdate()) = isnull(source.DischargeTime, getdate())
		and isnull(target.TransferFrom, '') = isnull(source.TransferFrom, '')
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.ExpectedLOS, 0) = isnull(source.ExpectedLOS, 0)
		and isnull(target.MRSAFlag, '') = isnull(source.MRSAFlag, '')
		and isnull(target.RTTPathwayID, '') = isnull(source.RTTPathwayID, '')
		and isnull(target.RTTPathwayCondition, '') = isnull(source.RTTPathwayCondition, '')
		and isnull(target.RTTStartDate, getdate()) = isnull(source.RTTStartDate, getdate())
		and isnull(target.RTTEndDate, getdate()) = isnull(source.RTTEndDate, getdate())
		and isnull(target.RTTSpecialtyCode, '') = isnull(source.RTTSpecialtyCode, '')
		and isnull(target.RTTCurrentProviderCode, '') = isnull(source.RTTCurrentProviderCode, '')
		and isnull(target.RTTCurrentStatusCode, '') = isnull(source.RTTCurrentStatusCode, '')
		and isnull(target.RTTCurrentStatusDate, getdate()) = isnull(source.RTTCurrentStatusDate, getdate())
		and isnull(target.RTTCurrentPrivatePatientFlag, 0) = isnull(source.RTTCurrentPrivatePatientFlag, 0)
		and isnull(target.RTTOverseasStatusFlag, 0) = isnull(source.RTTOverseasStatusFlag, 0)
		and isnull(target.RTTPeriodStatusCode, '') = isnull(source.RTTPeriodStatusCode, '')
		and isnull(target.IntendedPrimaryProcedureCode, '') = isnull(source.IntendedPrimaryProcedureCode, '')
		and isnull(target.Operation, '') = isnull(source.Operation, '')
		and isnull(target.Research1, '') = isnull(source.Research1, '')
		and isnull(target.CancelledElectiveAdmissionFlag, '') = isnull(source.CancelledElectiveAdmissionFlag, '')
		and isnull(target.LocalAdminCategoryCode, '') = isnull(source.LocalAdminCategoryCode, '')
		and isnull(target.EpisodicGpPracticeCode, '') = isnull(source.EpisodicGpPracticeCode, '')
		and isnull(target.PCTCode, '') = isnull(source.PCTCode, '')
		and isnull(target.LocalityCode, '') = isnull(source.LocalityCode, '')
		and isnull(target.AgeCode, '') = isnull(source.AgeCode, '')
		--and isnull(target.HRGCode, '') = isnull(source.HRGCode, '') -- DG 2.6.14 - HRG process stamps value on record. Will differ to classic process (which should be decommissioned)
		--and isnull(target.SpellHRGCode, '') = isnull(source.SpellHRGCode, '') -- DG 2.6.14 - HRG process stamps value on record. Will differ to classic process (which should be decommissioned)
		and isnull(target.CategoryCode, '') = isnull(source.CategoryCode, '')
		and isnull(target.LOE, 0) = isnull(source.LOE, 0)
		and isnull(target.LOS, 0) = isnull(source.LOS, 0)
		and isnull(target.Cases, 0) = isnull(source.Cases, 0)
		and isnull(target.FirstEpisodeInSpellIndicator, 0) = isnull(source.FirstEpisodeInSpellIndicator, 0)
		and isnull(target.RTTActivity, 0) = isnull(source.RTTActivity, 0)
		and isnull(target.RTTBreachStatusCode, '') = isnull(source.RTTBreachStatusCode, '')
		and isnull(target.DiagnosticProcedure, 0) = isnull(source.DiagnosticProcedure, 0)
		and isnull(target.RTTTreated, 0) = isnull(source.RTTTreated, 0)
		and isnull(target.EpisodeStartTimeOfDay, 0) = isnull(source.EpisodeStartTimeOfDay, 0)
		and isnull(target.EpisodeEndTimeOfDay, 0) = isnull(source.EpisodeEndTimeOfDay, 0)
		and isnull(target.AdmissionTimeOfDay, 0) = isnull(source.AdmissionTimeOfDay, 0)
		and isnull(target.DischargeTimeOfDay, 0) = isnull(source.DischargeTimeOfDay, 0)
		--and isnull(target.StartDirectorateCode, '') = isnull(source.StartDirectorateCode, '') -- DG 2.6.14 - Allocation stamps value on record. Will differ to classic process to assign directorate code (which should be decommissioned when all dependancies dealt with 
		--and isnull(target.EndDirectorateCode, '') = isnull(source.EndDirectorateCode, '') -- DG 2.6.14 - Allocation stamps value on record. Will differ to classic process to assign directorate code (which should be decommissioned when all dependancies dealt with  
		and isnull(target.ResidencePCTCode, '') = isnull(source.ResidencePCTCode, '')
		and isnull(target.ISTAdmissionSpecialtyCode, '') = isnull(source.ISTAdmissionSpecialtyCode, '')
		and isnull(target.ISTAdmissionDemandTime, getdate()) = isnull(source.ISTAdmissionDemandTime, getdate())
		and isnull(target.ISTDischargeTime, getdate()) = isnull(source.ISTDischargeTime, getdate())
		and isnull(target.ISTAdmissionDemandTimeOfDay, 0) = isnull(source.ISTAdmissionDemandTimeOfDay, 0)
		and isnull(target.ISTDischargeTimeOfDay, 0) = isnull(source.ISTDischargeTimeOfDay, 0)
		and isnull(target.PatientCategoryCode, '') = isnull(source.PatientCategoryCode, '')
		and isnull(target.Research2, '') = isnull(source.Research2, '')
		and isnull(target.ClinicalCodingStatus, '') = isnull(source.ClinicalCodingStatus, '')
		and isnull(target.ClinicalCodingCompleteDate, getdate()) = isnull(source.ClinicalCodingCompleteDate, getdate())
		and isnull(target.ClinicalCodingCompleteTime, getdate()) = isnull(source.ClinicalCodingCompleteTime, getdate())
		and isnull(target.ReferredByCode, '') = isnull(source.ReferredByCode, '')
		and isnull(target.ReferrerCode, '') = isnull(source.ReferrerCode, '')
		and isnull(target.DecidedToAdmitDate, getdate()) = isnull(source.DecidedToAdmitDate, getdate())
		and isnull(target.PsychiatricPatientStatusCode, '') = isnull(source.PsychiatricPatientStatusCode, '')
		and isnull(target.LegalStatusClassificationCode, '') = isnull(source.LegalStatusClassificationCode, '')
		and isnull(target.ReferringConsultantCode, '') = isnull(source.ReferringConsultantCode, '')
		and isnull(target.DurationOfElectiveWait, 0) = isnull(source.DurationOfElectiveWait, 0)
		and isnull(target.CarerSupportIndicator, '') = isnull(source.CarerSupportIndicator, '')
		and isnull(target.DischargeReadyDate, getdate()) = isnull(source.DischargeReadyDate, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.EddCreatedTime, getdate()) = isnull(source.EddCreatedTime, getdate())
		and isnull(target.ExpectedDateOfDischarge, getdate()) = isnull(source.ExpectedDateOfDischarge, getdate())
		and isnull(target.EddCreatedByConsultantFlag, 0) = isnull(source.EddCreatedByConsultantFlag, 0)
		and isnull(target.EddInterfaceCode, '') = isnull(source.EddInterfaceCode, '')
		and isnull(target.VTECategoryCode, '') = isnull(source.VTECategoryCode, '')
		and isnull(target.Ambulatory, 0) = isnull(source.Ambulatory, 0)
		and isnull(target.PseudoPostcode, '') = isnull(source.PseudoPostcode, '')
		and isnull(target.CCGCode, '') =  isnull(source.CCGCode, '')
		and isnull(target.GpCodeAtDischarge, '') = isnull(source.GpCodeAtDischarge, '')
		and isnull(target.GpPracticeCodeAtDischarge, '') = isnull(source.GpPracticeCodeAtDischarge, '')
		and isnull(target.PostcodeAtDischarge, '') = isnull(source.PostcodeAtDischarge, '')
		and isnull(target.ResidenceCCGCode, '') =  isnull(source.ResidenceCCGCode, '')
		and isnull(target.CharlsonIndex, 0) =  isnull(source.CharlsonIndex, 0)


		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.ProviderSpellNo = source.ProviderSpellNo
			,target.PatientTitle = source.PatientTitle
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.SexCode = source.SexCode
			,target.NHSNumber = source.NHSNumber
			,target.Postcode = source.Postcode
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.DHACode = source.DHACode
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.ReligionCode = source.ReligionCode
			,target.DateOnWaitingList = source.DateOnWaitingList
			,target.AdmissionDate = source.AdmissionDate
			,target.DischargeDate = source.DischargeDate
			,target.EpisodeStartDate = source.EpisodeStartDate
			,target.EpisodeEndDate = source.EpisodeEndDate
			,target.StartSiteCode = source.StartSiteCode
			,target.StartWardTypeCode = source.StartWardTypeCode
			,target.EndSiteCode = source.EndSiteCode
			,target.EndWardTypeCode = source.EndWardTypeCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.SiteCode = source.SiteCode
			,target.AdmissionMethodCode = source.AdmissionMethodCode
			,target.AdmissionSourceCode = source.AdmissionSourceCode
			,target.PatientClassificationCode = source.PatientClassificationCode
			,target.ManagementIntentionCode = source.ManagementIntentionCode
			,target.DischargeMethodCode = source.DischargeMethodCode
			,target.DischargeDestinationCode = source.DischargeDestinationCode
			,target.AdminCategoryCode = source.AdminCategoryCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.LastEpisodeInSpellIndicator = source.LastEpisodeInSpellIndicator
			,target.FirstRegDayOrNightAdmit = source.FirstRegDayOrNightAdmit
			,target.NeonatalLevelOfCare = source.NeonatalLevelOfCare
			,target.PASHRGCode = source.PASHRGCode
			,target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
			,target.SubsidiaryDiagnosisCode = source.SubsidiaryDiagnosisCode
			,target.SecondaryDiagnosisCode1 = source.SecondaryDiagnosisCode1
			,target.SecondaryDiagnosisCode2 = source.SecondaryDiagnosisCode2
			,target.SecondaryDiagnosisCode3 = source.SecondaryDiagnosisCode3
			,target.SecondaryDiagnosisCode4 = source.SecondaryDiagnosisCode4
			,target.SecondaryDiagnosisCode5 = source.SecondaryDiagnosisCode5
			,target.SecondaryDiagnosisCode6 = source.SecondaryDiagnosisCode6
			,target.SecondaryDiagnosisCode7 = source.SecondaryDiagnosisCode7
			,target.SecondaryDiagnosisCode8 = source.SecondaryDiagnosisCode8
			,target.SecondaryDiagnosisCode9 = source.SecondaryDiagnosisCode9
			,target.SecondaryDiagnosisCode10 = source.SecondaryDiagnosisCode10
			,target.SecondaryDiagnosisCode11 = source.SecondaryDiagnosisCode11
			,target.SecondaryDiagnosisCode12 = source.SecondaryDiagnosisCode12
			,target.SecondaryDiagnosisCode13 = source.SecondaryDiagnosisCode13
			,target.PrimaryProcedureCode = source.PrimaryProcedureCode
			,target.PrimaryProcedureDate = source.PrimaryProcedureDate
			,target.SecondaryProcedureCode1 = source.SecondaryProcedureCode1
			,target.SecondaryProcedureDate1 = source.SecondaryProcedureDate1
			,target.SecondaryProcedureCode2 = source.SecondaryProcedureCode2
			,target.SecondaryProcedureDate2 = source.SecondaryProcedureDate2
			,target.SecondaryProcedureCode3 = source.SecondaryProcedureCode3
			,target.SecondaryProcedureDate3 = source.SecondaryProcedureDate3
			,target.SecondaryProcedureCode4 = source.SecondaryProcedureCode4
			,target.SecondaryProcedureDate4 = source.SecondaryProcedureDate4
			,target.SecondaryProcedureCode5 = source.SecondaryProcedureCode5
			,target.SecondaryProcedureDate5 = source.SecondaryProcedureDate5
			,target.SecondaryProcedureCode6 = source.SecondaryProcedureCode6
			,target.SecondaryProcedureDate6 = source.SecondaryProcedureDate6
			,target.SecondaryProcedureCode7 = source.SecondaryProcedureCode7
			,target.SecondaryProcedureDate7 = source.SecondaryProcedureDate7
			,target.SecondaryProcedureCode8 = source.SecondaryProcedureCode8
			,target.SecondaryProcedureDate8 = source.SecondaryProcedureDate8
			,target.SecondaryProcedureCode9 = source.SecondaryProcedureCode9
			,target.SecondaryProcedureDate9 = source.SecondaryProcedureDate9
			,target.SecondaryProcedureCode10 = source.SecondaryProcedureCode10
			,target.SecondaryProcedureDate10 = source.SecondaryProcedureDate10
			,target.SecondaryProcedureCode11 = source.SecondaryProcedureCode11
			,target.SecondaryProcedureDate11 = source.SecondaryProcedureDate11
			,target.OperationStatusCode = source.OperationStatusCode
			,target.ContractSerialNo = source.ContractSerialNo
			,target.CodingCompleteDate = source.CodingCompleteDate
			,target.PurchaserCode = source.PurchaserCode
			,target.ProviderCode = source.ProviderCode
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.EpisodeEndTime = source.EpisodeEndTime
			,target.RegisteredGdpCode = source.RegisteredGdpCode
			,target.EpisodicGpCode = source.EpisodicGpCode
			,target.InterfaceCode = source.InterfaceCode
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumberStatusCode = source.NHSNumberStatusCode
			,target.AdmissionTime = source.AdmissionTime
			,target.DischargeTime = source.DischargeTime
			,target.TransferFrom = source.TransferFrom
			,target.DistrictNo = source.DistrictNo
			,target.ExpectedLOS = source.ExpectedLOS
			,target.MRSAFlag = source.MRSAFlag
			,target.RTTPathwayID = source.RTTPathwayID
			,target.RTTPathwayCondition = source.RTTPathwayCondition
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.RTTSpecialtyCode = source.RTTSpecialtyCode
			,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
			,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
			,target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
			,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
			,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
			,target.RTTPeriodStatusCode = source.RTTPeriodStatusCode
			,target.IntendedPrimaryProcedureCode = source.IntendedPrimaryProcedureCode
			,target.Operation = source.Operation
			,target.Research1 = source.Research1
			,target.CancelledElectiveAdmissionFlag = source.CancelledElectiveAdmissionFlag
			,target.LocalAdminCategoryCode = source.LocalAdminCategoryCode
			,target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
			,target.PCTCode = source.PCTCode
			,target.LocalityCode = source.LocalityCode
			,target.AgeCode = source.AgeCode
			--,target.HRGCode = source.HRGCode -- DG 2.6.14 - See comment above
			--,target.SpellHRGCode = source.SpellHRGCode -- DG 2.6.14 - See comment above
			,target.CategoryCode = source.CategoryCode
			,target.LOE = source.LOE
			,target.LOS = source.LOS
			,target.Cases = source.Cases
			,target.FirstEpisodeInSpellIndicator = source.FirstEpisodeInSpellIndicator
			,target.RTTActivity = source.RTTActivity
			,target.RTTBreachStatusCode = source.RTTBreachStatusCode
			,target.DiagnosticProcedure = source.DiagnosticProcedure
			,target.RTTTreated = source.RTTTreated
			,target.EpisodeStartTimeOfDay = source.EpisodeStartTimeOfDay
			,target.EpisodeEndTimeOfDay = source.EpisodeEndTimeOfDay
			,target.AdmissionTimeOfDay = source.AdmissionTimeOfDay
			,target.DischargeTimeOfDay = source.DischargeTimeOfDay
			--,target.StartDirectorateCode = source.StartDirectorateCode -- DG 2.6.14 - See comment above
			--,target.EndDirectorateCode = source.EndDirectorateCode -- DG 2.6.14 - See comment above
			,target.ResidencePCTCode = source.ResidencePCTCode
			,target.ISTAdmissionSpecialtyCode = source.ISTAdmissionSpecialtyCode
			,target.ISTAdmissionDemandTime = source.ISTAdmissionDemandTime
			,target.ISTDischargeTime = source.ISTDischargeTime
			,target.ISTAdmissionDemandTimeOfDay = source.ISTAdmissionDemandTimeOfDay
			,target.ISTDischargeTimeOfDay = source.ISTDischargeTimeOfDay
			,target.PatientCategoryCode = source.PatientCategoryCode
			,target.Research2 = source.Research2
			,target.ClinicalCodingStatus = source.ClinicalCodingStatus
			,target.ClinicalCodingCompleteDate = source.ClinicalCodingCompleteDate
			,target.ClinicalCodingCompleteTime = source.ClinicalCodingCompleteTime
			,target.ReferredByCode = source.ReferredByCode
			,target.ReferrerCode = source.ReferrerCode
			,target.DecidedToAdmitDate = source.DecidedToAdmitDate
			,target.PsychiatricPatientStatusCode = source.PsychiatricPatientStatusCode
			,target.LegalStatusClassificationCode = source.LegalStatusClassificationCode
			,target.ReferringConsultantCode = source.ReferringConsultantCode
			,target.DurationOfElectiveWait = source.DurationOfElectiveWait
			,target.CarerSupportIndicator = source.CarerSupportIndicator
			,target.DischargeReadyDate = source.DischargeReadyDate
			,target.EddCreatedTime = source.EddCreatedTime
			,target.ExpectedDateOfDischarge = source.ExpectedDateOfDischarge
			,target.EddCreatedByConsultantFlag = source.EddCreatedByConsultantFlag
			,target.EddInterfaceCode = source.EddInterfaceCode
			,target.VTECategoryCode = source.VTECategoryCode
			,target.Ambulatory = source.Ambulatory
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.PseudoPostcode = source.PseudoPostcode
			,target.CCGCode =  source.CCGCode
			,target.GpCodeAtDischarge = source.GpCodeAtDischarge
			,target.GpPracticeCodeAtDischarge = source.GpPracticeCodeAtDischarge
			,target.PostcodeAtDischarge = source.PostcodeAtDischarge
			,target.ResidenceCCGCode =  source.ResidenceCCGCode
			,target.CharlsonIndex =  source.CharlsonIndex




output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




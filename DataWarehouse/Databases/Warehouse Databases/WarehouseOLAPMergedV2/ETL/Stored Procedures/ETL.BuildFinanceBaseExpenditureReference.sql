﻿


CREATE procedure [ETL].[BuildFinanceBaseExpenditureReference] 
	@ContextCode varchar(20)
	-- 'CMFT||ORCL'
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context  
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Finance.BaseExpenditureReference
			where
				BaseExpenditureReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge ???
delete
from
	Finance.BaseExpenditureReference
where
	not exists
	(
	select
		1
	from
		Finance.BaseExpenditure
	where
		BaseExpenditure.MergeExpenditureRecno = BaseExpenditureReference.MergeExpenditureRecno
	)
and	BaseExpenditureReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Finance.BaseExpenditureReference target
using
	(
	select
		BaseExpenditure.MergeExpenditureRecno
		,ContextID = Context.ContextID
		,CensusDateID =
			coalesce(
				 CensusDate.DateID

				,case
				when BaseExpenditure.CensusDate is null
				then NullDate.DateID

				when BaseExpenditure.CensusDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,BaseExpenditure.Created
		,BaseExpenditure.Updated
		,BaseExpenditure.ByWhom
	from
		Finance.BaseExpenditure

	inner join WH.Context
	on	Context.ContextCode = BaseExpenditure.ContextCode

	left join WH.Calendar CensusDate
	on	CensusDate.TheDate = BaseExpenditure.CensusDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseExpenditure.Updated > @LastUpdated
	and	BaseExpenditure.ContextCode = @ContextCode

	) source
	on	source.MergeExpenditureRecno = target.MergeExpenditureRecno

	when not matched
	then
		insert
			(
			MergeExpenditureRecno
			,ContextID
			,CensusDateID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeExpenditureRecno
			,source.ContextID
			,source.CensusDateID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeExpenditureRecno = source.MergeExpenditureRecno
			,target.ContextID = source.ContextID
			,target.CensusDateID = source.CensusDateID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats








﻿create procedure [ETL].[ImportCenCOM] as

exec ETL.ImportCenCOMBaseCaseload
exec ETL.ImportCenCOMBaseDiagnosisProcedureEvent
exec ETL.ImportCenCOMBaseEncounter
exec ETL.ImportCenCOMBaseReferral
exec ETL.ImportCenCOMBaseWait
exec ETL.ImportCenCOMBaseWardStay



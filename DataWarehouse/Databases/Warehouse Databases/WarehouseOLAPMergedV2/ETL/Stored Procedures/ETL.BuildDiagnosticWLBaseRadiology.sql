﻿
CREATE procedure [ETL].[BuildDiagnosticWLBaseRadiology]
as

/* 
==============================================================================================
Description:

When		Who			What
20150925	Paul Egan	Added clock reset columns to merge.
===============================================================================================
*/


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

--clear out old snapshots
select
	Calendar.TheDate
into
	#Calendar
from
	WH.Calendar
where
	Calendar.TheDate in
	(
--Month end snapshots
	select
		max(TheDate)
	from
		WH.Calendar
	group by
		 year(TheDate)
		,month(TheDate)

	union
--snapshots in the last fourteen days
	select
		TheDate
	from
		WH.Calendar
	where
		TheDate between dateadd(day , -14 , getdate()) and getdate()

	union
--Sunday snapshots
	select
		TheDate
	from
		WH.Calendar
	where
		datename(dw, TheDate) = 'Sunday'
	)

delete
from
	DiagnosticWL.BaseRadiology
where 
	not exists
	(
	select
		1
	from
		#Calendar
	where
		BaseRadiology.CensusDate = TheDate
	)


select @deleted = @@ROWCOUNT


merge
	DiagnosticWL.BaseRadiology target
using
	(
	select
		 SourceUniqueID
		,CensusDate = cast(CensusDate as date)
		,SourcePatientNo = cast(SourcePatientNo as varchar(20))
		,PCTCode = cast(PCTCode as varchar(20))
		,SiteCode = cast(SiteCode as varchar(50))
		,WaitTypeCode = cast(WaitTypeCode as varchar(2))
		,WLStatus = cast(WLStatus as varchar(50))
		,LengthOfWaitDays = cast(LengthOfWaitDays as int)
		,ExamCode = cast(ExamCode as varchar(50))
		,PatientTypeCode = cast(PatientTypeCode as varchar(20))
		,DirectorateCode = cast(DirectorateCode as varchar(5))
		,DiagnosticNationalExamCode = cast(DiagnosticNationalExamCode as varchar(10))
		,RequestDate = cast(RequestDate as date)
		,EventDate = cast(EventDate as date)
		,DateOfBirth = cast(DateOfBirth as date)
		,DistrictNo = cast(DistrictNo as varchar(8))
		,PatientSurname = cast(PatientSurname as varchar(50))
		,PatientForename = cast(PatientForename as varchar(50))
		,SpecialtyCode = cast(SpecialtyCode as varchar(20))
		,EncounterRecno = cast(EncounterRecno as int)
		,PatientTitle = cast(PatientTitle as varchar(10))
		,SexCode = cast(SexCode as varchar(5))
		,NHSNumber = cast(NHSNumber as varchar(17))
		,PatientAddress1 = cast(PatientAddress1 as varchar(50))
		,PatientAddress2 = cast(PatientAddress2 as varchar(50))
		,PatientAddress3 = cast(PatientAddress3 as varchar(50))
		,PatientAddress4 = cast(PatientAddress4 as varchar(50))
		,Postcode = cast(Postcode as varchar(10))
		,DaysWaitingAdjusted = cast(DaysWaitingAdjusted as int)
		,ModalityCode = cast(ModalityCode as varchar(5))
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(20))
		,RegisteredPracticeCode = cast(RegisteredPracticeCode as varchar(20))
		,PriorityCode = cast(PriorityCode as varchar(20))
		,Comment = cast(Comment as varchar(4000))
		,ExamGroupCode1 = cast(ExamGroupCode1 as varchar(20))
		,ExamGroupCode2 = cast(ExamGroupCode2 as varchar(20))
		,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(20))
		,ReferrerCode = cast(ReferrerCode as varchar(20))
		,ReferralLocationCode = cast(ReferralLocationCode as varchar(20))
		,WaitingButNotPlanned = cast(WaitingButNotPlanned as varchar(5))
		,Planned = cast(Planned as varchar(5))
		,Scheduled = cast(Scheduled as varchar(5))
		,Waiting = cast(Waiting as varchar(5))
		,AgeCode = cast(AgeCode as varchar(50))
		,WithAppointment = cast(WithAppointment as int)
		,WLStatusTypeCode = cast(WLStatusTypeCode as varchar(10))
		,CCGCode = cast(CCGCode as nvarchar(12))
		,ResidenceCCGCode = cast(ResidenceCCGCode as nvarchar(10))
		,ContextCode = cast(ContextCode as varchar(20))
		,Created = cast(Created as datetime)
		,Updated = cast(Updated as datetime)
		,ByWhom = cast(ByWhom as varchar(50))
		
		/* Added Paul Egan 20150925 */
		,ClockResetDate = cast(ClockResetDate as date)
		,BreachDate = cast(BreachDate as date)
		,EventSourceUniqueID = cast(EventSourceUniqueID as varchar(20))
		,ExamSourceUniqueID = cast(ExamSourceUniqueID as varchar(20))
	from
		ETL.TLoadDiagnosticWLBaseRadiology Encounter
	where 
		exists
		(
		select
			1
		from
			#Calendar
		where
			#Calendar.TheDate = Encounter.CensusDate
		)

	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourceUniqueID = target.SourceUniqueID
	and	source.CensusDate = target.CensusDate


	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CensusDate
			,SourcePatientNo
			,PCTCode
			,SiteCode
			,WaitTypeCode
			,WLStatus
			,LengthOfWaitDays
			,ExamCode
			,PatientTypeCode
			,DirectorateCode
			,DiagnosticNationalExamCode
			,RequestDate
			,EventDate
			,DateOfBirth
			,DistrictNo
			,PatientSurname
			,PatientForename
			,SpecialtyCode
			,EncounterRecno
			,PatientTitle
			,SexCode
			,NHSNumber
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,Postcode
			,DaysWaitingAdjusted
			,ModalityCode
			,RegisteredGpCode
			,RegisteredPracticeCode
			,PriorityCode
			,Comment
			,ExamGroupCode1
			,ExamGroupCode2
			,SourceOfReferralCode
			,ReferrerCode
			,ReferralLocationCode
			,WaitingButNotPlanned
			,Planned
			,Scheduled
			,Waiting
			,AgeCode
			,WithAppointment
			,WLStatusTypeCode
			,CCGCode
			,ResidenceCCGCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			,ClockResetDate
			,BreachDate
			,EventSourceUniqueID
			,ExamSourceUniqueID
			)
		values
			(
			 source.SourceUniqueID
			,source.CensusDate
			,source.SourcePatientNo
			,source.PCTCode
			,source.SiteCode
			,source.WaitTypeCode
			,source.WLStatus
			,source.LengthOfWaitDays
			,source.ExamCode
			,source.PatientTypeCode
			,source.DirectorateCode
			,source.DiagnosticNationalExamCode
			,source.RequestDate
			,source.EventDate
			,source.DateOfBirth
			,source.DistrictNo
			,source.PatientSurname
			,source.PatientForename
			,source.SpecialtyCode
			,source.EncounterRecno
			,source.PatientTitle
			,source.SexCode
			,source.NHSNumber
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.Postcode
			,source.DaysWaitingAdjusted
			,source.ModalityCode
			,source.RegisteredGpCode
			,source.RegisteredPracticeCode
			,source.PriorityCode
			,source.Comment
			,source.ExamGroupCode1
			,source.ExamGroupCode2
			,source.SourceOfReferralCode
			,source.ReferrerCode
			,source.ReferralLocationCode
			,source.WaitingButNotPlanned
			,source.Planned
			,source.Scheduled
			,source.Waiting
			,source.AgeCode
			,source.WithAppointment
			,source.WLStatusTypeCode
			,source.CCGCode
			,source.ResidenceCCGCode
			,source.ContextCode
			,source.Created
			,source.Updated
			,source.ByWhom
			,source.ClockResetDate
			,source.BreachDate
			,source.EventSourceUniqueID
			,source.ExamSourceUniqueID
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.PCTCode, '') = isnull(source.PCTCode, '')
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.WaitTypeCode, '') = isnull(source.WaitTypeCode, '')
		and isnull(target.WLStatus, '') = isnull(source.WLStatus, '')
		and isnull(target.LengthOfWaitDays, 0) = isnull(source.LengthOfWaitDays, 0)
		and isnull(target.ExamCode, '') = isnull(source.ExamCode, '')
		and isnull(target.PatientTypeCode, '') = isnull(source.PatientTypeCode, '')
		and isnull(target.DirectorateCode, '') = isnull(source.DirectorateCode, '')
		and isnull(target.DiagnosticNationalExamCode, '') = isnull(source.DiagnosticNationalExamCode, '')
		and isnull(target.RequestDate, getdate()) = isnull(source.RequestDate, getdate())
		and isnull(target.EventDate, getdate()) = isnull(source.EventDate, getdate())
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		and isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.PatientTitle, '') = isnull(source.PatientTitle, '')
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.PatientAddress1, '') = isnull(source.PatientAddress1, '')
		and isnull(target.PatientAddress2, '') = isnull(source.PatientAddress2, '')
		and isnull(target.PatientAddress3, '') = isnull(source.PatientAddress3, '')
		and isnull(target.PatientAddress4, '') = isnull(source.PatientAddress4, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.DaysWaitingAdjusted, 0) = isnull(source.DaysWaitingAdjusted, 0)
		and isnull(target.ModalityCode, '') = isnull(source.ModalityCode, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredPracticeCode, '') = isnull(source.RegisteredPracticeCode, '')
		and isnull(target.PriorityCode, '') = isnull(source.PriorityCode, '')
		and isnull(target.Comment, '') = isnull(source.Comment, '')
		and isnull(target.ExamGroupCode1, '') = isnull(source.ExamGroupCode1, '')
		and isnull(target.ExamGroupCode2, '') = isnull(source.ExamGroupCode2, '')
		and isnull(target.SourceOfReferralCode, '') = isnull(source.SourceOfReferralCode, '')
		and isnull(target.ReferrerCode, '') = isnull(source.ReferrerCode, '')
		and isnull(target.ReferralLocationCode, '') = isnull(source.ReferralLocationCode, '')
		and isnull(target.WaitingButNotPlanned, '') = isnull(source.WaitingButNotPlanned, '')
		and isnull(target.Planned, '') = isnull(source.Planned, '')
		and isnull(target.Scheduled, '') = isnull(source.Scheduled, '')
		and isnull(target.Waiting, '') = isnull(source.Waiting, '')
		and isnull(target.AgeCode, '') = isnull(source.AgeCode, '')
		and isnull(target.WithAppointment, 0) = isnull(source.WithAppointment, 0)
		and isnull(target.WLStatusTypeCode, '') = isnull(source.WLStatusTypeCode, '')
		and isnull(target.CCGCode, '') = isnull(source.CCGCode, '')
		and isnull(target.ResidenceCCGCode, '') = isnull(source.ResidenceCCGCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.Created, getdate()) = isnull(source.Created, getdate())
		and isnull(target.Updated, getdate()) = isnull(source.Updated, getdate())
		and isnull(target.ByWhom, '') = isnull(source.ByWhom, '')
		and isnull(target.ClockResetDate, getdate()) = isnull(source.ClockResetDate, getdate())
		and isnull(target.BreachDate, getdate()) = isnull(source.BreachDate, getdate())
		and isnull(target.EventSourceUniqueID, '') = isnull(source.EventSourceUniqueID, '')
		and isnull(target.ExamSourceUniqueID, '') = isnull(source.ExamSourceUniqueID, '')

		)
	then
		update
		set
			 target.SourcePatientNo = source.SourcePatientNo
			,target.PCTCode = source.PCTCode
			,target.SiteCode = source.SiteCode
			,target.WaitTypeCode = source.WaitTypeCode
			,target.WLStatus = source.WLStatus
			,target.LengthOfWaitDays = source.LengthOfWaitDays
			,target.ExamCode = source.ExamCode
			,target.PatientTypeCode = source.PatientTypeCode
			,target.DirectorateCode = source.DirectorateCode
			,target.DiagnosticNationalExamCode = source.DiagnosticNationalExamCode
			,target.RequestDate = source.RequestDate
			,target.EventDate = source.EventDate
			,target.DateOfBirth = source.DateOfBirth
			,target.DistrictNo = source.DistrictNo
			,target.PatientSurname = source.PatientSurname
			,target.PatientForename = source.PatientForename
			,target.SpecialtyCode = source.SpecialtyCode
			,target.EncounterRecno = source.EncounterRecno
			,target.PatientTitle = source.PatientTitle
			,target.SexCode = source.SexCode
			,target.NHSNumber = source.NHSNumber
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.Postcode = source.Postcode
			,target.DaysWaitingAdjusted = source.DaysWaitingAdjusted
			,target.ModalityCode = source.ModalityCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredPracticeCode = source.RegisteredPracticeCode
			,target.PriorityCode = source.PriorityCode
			,target.Comment = source.Comment
			,target.ExamGroupCode1 = source.ExamGroupCode1
			,target.ExamGroupCode2 = source.ExamGroupCode2
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ReferrerCode = source.ReferrerCode
			,target.ReferralLocationCode = source.ReferralLocationCode
			,target.WaitingButNotPlanned = source.WaitingButNotPlanned
			,target.Planned = source.Planned
			,target.Scheduled = source.Scheduled
			,target.Waiting = source.Waiting
			,target.AgeCode = source.AgeCode
			,target.WithAppointment = source.WithAppointment
			,target.WLStatusTypeCode = source.WLStatusTypeCode
			,target.CCGCode = source.CCGCode
			,target.ResidenceCCGCode = source.ResidenceCCGCode
			,target.ContextCode = source.ContextCode
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
			,target.ClockResetDate = source.ClockResetDate
			,target.BreachDate = source.BreachDate
			,target.EventSourceUniqueID = source.EventSourceUniqueID
			,target.ExamSourceUniqueID = source.ExamSourceUniqueID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	--,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		--,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



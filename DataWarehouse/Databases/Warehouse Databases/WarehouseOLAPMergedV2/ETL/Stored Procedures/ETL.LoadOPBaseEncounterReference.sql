﻿CREATE procedure [ETL].[LoadOPBaseEncounterReference]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	OP.BaseEncounterReference target
using
	(
	select
		 ProcessList.MergeRecno
		,ProcessList.Action
		,Context.ContextID
		,SexID = Sex.SourceSexID
		,AgeID = Age.AgeID
		,NHSNumberStatusID = NHSNumberStatus.SourceNHSNumberStatusID
		,EthnicOriginID = EthnicOrigin.SourceEthnicCategoryID
		,MaritalStatusID = MaritalStatus.SourceMaritalStatusID
		,ReligionID = Religion.SourceReligionID
		,SiteID = Site.SourceSiteID

		,AppointmentDateID =
			coalesce(
				 AppointmentDate.DateID

				,case
				when Encounter.AppointmentDate is null
				then NullDate.DateID

				when Encounter.AppointmentDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,ClinicID = Clinic.SourceClinicID
		,AdminCategoryID = AdminCategory.SourceValueID
		,SourceOfReferralID = SourceOfReferral.SourceSourceOfReferralID
		,ReasonForReferralID = ReasonForReferral.SourceReasonForReferralID
		,PriorityID = PriorityType.SourcePriorityTypeID
		,FirstAttendanceID = FirstAttendance.SourceFirstAttendanceID
		,AttendanceStatusID = AttendanceStatus.SourceAttendanceStatusID
		,AttendanceOutcomeID = AttendanceOutcome.SourceAttendanceOutcomeID
		,AppointmentTypeID = AppointmentType.SourceAppointmentTypeID
		,ConsultantID = Consultant.SourceConsultantID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,ReferralConsultantID = ReferralConsultant.SourceConsultantID
		,ReferralSpecialtyID = ReferralSpecialty.SourceSpecialtyID
		,RTTSpecialtyID = RTTSpecialty.SourceSpecialtyID
		,RTTCurrentStatusID = RTTCurrentStatus.SourceRTTStatusID
		,ReferringConsultantID = ReferringConsultant.SourceConsultantID
		,TreatmentFunctionID = TreatmentFunction.SourceSpecialtyID
		,DerivedFirstAttendanceID = DerivedFirstAttendance.SourceFirstAttendanceID
		,AppointmentResidenceCCGID = ResidenceCCG.CCGID
		,DoctorID = Doctor.SourceDoctorID

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		OP.ProcessList

	left join OP.BaseEncounter Encounter
	on	Encounter.MergeEncounterRecno = ProcessList.MergeRecno

	left join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.SexCode, '-1') as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	left join WH.Age
	on	Age.AgeCode = Encounter.AgeCode

	left join WH.NHSNumberStatus NHSNumberStatus
	on	NHSNumberStatus.SourceNHSNumberStatusCode = cast(coalesce(Encounter.NHSNumberStatusCode, '-1') as varchar)
	and	NHSNumberStatus.SourceContextCode = Encounter.ContextCode

	left join WH.EthnicCategory EthnicOrigin
	on	EthnicOrigin.SourceEthnicCategoryCode = cast(coalesce(Encounter.EthnicOriginCode, '-1') as varchar)
	and	EthnicOrigin.SourceContextCode = Encounter.ContextCode

	left join WH.MaritalStatus MaritalStatus
	on	MaritalStatus.SourceMaritalStatusCode = cast(coalesce(Encounter.MaritalStatusCode, '-1') as varchar)
	and	MaritalStatus.SourceContextCode = Encounter.ContextCode

	left join WH.Religion Religion
	on	Religion.SourceReligionCode = cast(coalesce(Encounter.ReligionCode, '-1') as varchar)
	and	Religion.SourceContextCode = Encounter.ContextCode

	left join WH.Site Site
	on	Site.SourceSiteCode = cast(coalesce(Encounter.SiteCode, '-1') as varchar)
	and	Site.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar AppointmentDate
	on	AppointmentDate.TheDate = Encounter.AppointmentDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join OP.Clinic Clinic
	on	Clinic.SourceClinicCode = coalesce(Encounter.ClinicCode, '-1')
	and	Clinic.SourceContextCode = Encounter.ContextCode

	left join WH.AdminCategory AdminCategory
	on	AdminCategory.AdminCategoryID = coalesce(Encounter.AdminCategoryCode, '-1')
	and	AdminCategory.ContextCode = Encounter.ContextCode

	left join OP.SourceOfReferral SourceOfReferral
	on	SourceOfReferral.SourceSourceOfReferralCode = coalesce(Encounter.SourceOfReferralCode, '-1')
	and	SourceOfReferral.SourceContextCode = Encounter.ContextCode

	left join OP.ReasonForReferral ReasonForReferral
	on	ReasonForReferral.SourceReasonForReferralCode = coalesce(Encounter.ReasonForReferralCode, '-1')
	and	ReasonForReferral.SourceContextCode = Encounter.ContextCode

	left join WH.PriorityType PriorityType
	on	PriorityType.SourcePriorityTypeCode = coalesce(Encounter.PriorityCode, '-1')
	and	PriorityType.SourceContextCode = Encounter.ContextCode

	left join OP.FirstAttendance FirstAttendance
	on	FirstAttendance.SourceFirstAttendanceCode = coalesce(Encounter.FirstAttendanceFlag, '-1')
	and	FirstAttendance.SourceContextCode = Encounter.ContextCode

	left join OP.AttendanceStatus AttendanceStatus
	on	AttendanceStatus.SourceAttendanceStatusCode = coalesce(Encounter.DNACode, '-1')
	and	AttendanceStatus.SourceContextCode = Encounter.ContextCode

	left join OP.AttendanceOutcome AttendanceOutcome
	on	AttendanceOutcome.SourceAttendanceOutcomeCode = coalesce(Encounter.AttendanceOutcomeCode, '-1')
	and	AttendanceOutcome.SourceContextCode = Encounter.ContextCode

	left join OP.AppointmentType AppointmentType
	on	AppointmentType.SourceAppointmentTypeCode = coalesce(Encounter.AppointmentTypeCode, '-1')
	and	AppointmentType.SourceContextCode = Encounter.ContextCode

	left join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = coalesce(Encounter.ConsultantCode, '-1')
	and	Consultant.SourceContextCode = Encounter.ContextCode

	left join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(Encounter.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = Encounter.ContextCode

	left join WH.Consultant ReferralConsultant
	on	ReferralConsultant.SourceConsultantCode = coalesce(Encounter.ReferralConsultantCode, '-1')
	and	ReferralConsultant.SourceContextCode = Encounter.ContextCode

	left join WH.Specialty ReferralSpecialty
	on	ReferralSpecialty.SourceSpecialtyCode = coalesce(Encounter.ReferralSpecialtyCode, '-1')
	and	ReferralSpecialty.SourceContextCode = Encounter.ContextCode

	left join WH.Specialty RTTSpecialty
	on	RTTSpecialty.SourceSpecialtyCode = coalesce(Encounter.RTTSpecialtyCode, '-1')
	and	RTTSpecialty.SourceContextCode = Encounter.ContextCode

	left join WH.RTTStatus RTTCurrentStatus
	on	RTTCurrentStatus.SourceRTTStatusCode = coalesce(Encounter.RTTCurrentStatusCode, '-1')
	and	RTTCurrentStatus.SourceContextCode = Encounter.ContextCode

	left join WH.Consultant ReferringConsultant
	on	ReferringConsultant.SourceConsultantCode = coalesce(Encounter.ReferredByConsultantCode, '-1')
	and	ReferringConsultant.SourceContextCode = Encounter.ContextCode

	left join WH.Specialty TreatmentFunction
	on	TreatmentFunction.SourceSpecialtyCode = coalesce(Encounter.TreatmentFunctionCode, '-1')
	and	TreatmentFunction.SourceContextCode = Encounter.ContextCode

	left join OP.FirstAttendance DerivedFirstAttendance
	on	DerivedFirstAttendance.SourceFirstAttendanceCode = coalesce(Encounter.DerivedFirstAttendanceFlag, '-1')
	and	DerivedFirstAttendance.SourceContextCode = Encounter.ContextCode

	left join WH.CCG ResidenceCCG
	on	ResidenceCCG.CCGCode = coalesce(Encounter.AppointmentResidenceCCGCode , 'N/A')

	left join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	left join OP.Doctor Doctor
	on	Doctor.SourceDoctorCode = cast(isnull(Encounter.DoctorCode, '-1') as varchar)
	and	Doctor.SourceContextCode = Encounter.ContextCode

	) source
	on	source.MergeRecno = target.MergeEncounterRecno

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	and	source.Action = 'INSERT'
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,SexID
			,AgeID
			,NHSNumberStatusID
			,EthnicOriginID
			,MaritalStatusID
			,ReligionID
			,SiteID
			,AppointmentDateID
			,ClinicID
			,AdminCategoryID
			,SourceOfReferralID
			,ReasonForReferralID
			,PriorityID
			,FirstAttendanceID
			,AttendanceStatusID
			,AttendanceOutcomeID
			,AppointmentTypeID
			,ConsultantID
			,SpecialtyID
			,ReferralConsultantID
			,ReferralSpecialtyID
			,RTTSpecialtyID
			,RTTCurrentStatusID
			,ReferringConsultantID
			,TreatmentFunctionID
			,DerivedFirstAttendanceID
			,AppointmentResidenceCCGID
			,DoctorID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.SexID
			,source.AgeID
			,source.NHSNumberStatusID
			,source.EthnicOriginID
			,source.MaritalStatusID
			,source.ReligionID
			,source.SiteID
			,source.AppointmentDateID
			,source.ClinicID
			,source.AdminCategoryID
			,source.SourceOfReferralID
			,source.ReasonForReferralID
			,source.PriorityID
			,source.FirstAttendanceID
			,source.AttendanceStatusID
			,source.AttendanceOutcomeID
			,source.AppointmentTypeID
			,source.ConsultantID
			,source.SpecialtyID
			,source.ReferralConsultantID
			,source.ReferralSpecialtyID
			,source.RTTSpecialtyID
			,source.RTTCurrentStatusID
			,source.ReferringConsultantID
			,source.TreatmentFunctionID
			,source.DerivedFirstAttendanceID
			,source.AppointmentResidenceCCGID
			,source.DoctorID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and	source.Action = 'UPDATE'
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeRecno
			,target.ContextID = source.ContextID
			,target.SexID = source.SexID
			,target.AgeID = source.AgeID
			,target.NHSNumberStatusID = source.NHSNumberStatusID
			,target.EthnicOriginID = source.EthnicOriginID
			,target.MaritalStatusID = source.MaritalStatusID
			,target.ReligionID = source.ReligionID
			,target.SiteID = source.SiteID
			,target.AppointmentDateID = source.AppointmentDateID
			,target.ClinicID = source.ClinicID
			,target.AdminCategoryID = source.AdminCategoryID
			,target.SourceOfReferralID = source.SourceOfReferralID
			,target.ReasonForReferralID = source.ReasonForReferralID
			,target.PriorityID = source.PriorityID
			,target.FirstAttendanceID = source.FirstAttendanceID
			,target.AttendanceStatusID = source.AttendanceStatusID
			,target.AttendanceOutcomeID = source.AttendanceOutcomeID
			,target.AppointmentTypeID = source.AppointmentTypeID
			,target.ConsultantID = source.ConsultantID
			,target.SpecialtyID = source.SpecialtyID
			,target.ReferralConsultantID = source.ReferralConsultantID
			,target.ReferralSpecialtyID = source.ReferralSpecialtyID
			,target.RTTSpecialtyID = source.RTTSpecialtyID
			,target.RTTCurrentStatusID = source.RTTCurrentStatusID
			,target.ReferringConsultantID = source.ReferringConsultantID
			,target.TreatmentFunctionID = source.TreatmentFunctionID
			,target.DerivedFirstAttendanceID = source.DerivedFirstAttendanceID
			,target.AppointmentResidenceCCGID = source.AppointmentResidenceCCGID
			,target.DoctorID = source.DoctorID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

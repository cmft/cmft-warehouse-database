﻿

CREATE proc [ETL].[BuildCenPatientBaseSpecialRegister]
as

/****************************************************************************************
	Stored procedure : [ETL].[BuildCenPatientBaseSpecialRegister]
	Description		 : Merge new/updated Special Register data into production

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	22/08/2014	Paul Egan       Initial Coding
	19/10/2015	Rachel Royston	Updated
*****************************************************************************************/

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||PAS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge 
	Patient.BaseSpecialRegister target 
using
	(
	select
		 SpecialRegisterRecNo
		,SourcePatientNo
		,SpecialRegisterCode
		,DistrictNo
		,NHSNumber
		,EnteredDate
		,ActivityTime
		,SourceEpisodeNo
		,ContextCode
	from
		ETL.TLoadCenPatientBaseSpecialRegister
	) source
		
on source.SourcePatientNo = target.SourcePatientNo
and source.SpecialRegisterCode = target.SpecialRegisterCode
and source.ContextCode = target.ContextCode
	
when not matched by source
and target.ContextCode = @ContextCode

	then delete
	
when not matched by target
	then insert
		(
		 SpecialRegisterRecno
		,SourcePatientNo
		,SpecialRegisterCode
		,DistrictNo
		,NHSNumber
		,EnteredDate
		,ActivityTime
		,SourceEpisodeNo
		,ContextCode
		,CreatedTime
		,CreatedByWhom
		,UpdatedTime
		,UpdatedByWhom
		)
		values
		(
		 source.SpecialRegisterRecNo
		,source.SourcePatientNo
		,source.SpecialRegisterCode
		,source.DistrictNo
		,source.NHSNumber
		,source.EnteredDate
		,source.ActivityTime
		,source.SourceEpisodeNo
		,source.ContextCode
		,getdate()
		,suser_name()	
		,getdate()
		,suser_name()
		)
		
when matched and not
	(
		isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
	and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
	and isnull(target.EnteredDate, '19000101') = isnull(source.EnteredDate, '19000101')
	and isnull(target.ActivityTime, '19000101') = isnull(source.ActivityTime, '19000101')
	and isnull(target.SourceEpisodeNo, '') = isnull(source.SourceEpisodeNo, '')
	)
	then update
		set
			 target.DistrictNo = source.DistrictNo
			,target.NHSNumber = source.NHSNumber
			,target.EnteredDate = source.EnteredDate
			,target.ActivityTime = source.ActivityTime
			,target.SourceEpisodeNo = source.SourceEpisodeNo
			,target.UpdatedTime = getdate()
			,target.UpdatedByWhom = suser_name()


output $Action into @MergeSummary	
;

/* Stats */
select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary
;

select @Elapsed = datediff(minute,@StartTime,getdate());

select @Stats = 

	'Deleted ' + convert(varchar(10), @deleted)  + 
	', Updated '  + convert(varchar(10), @updated) +  
	', Inserted '  + convert(varchar(10), @inserted) + 
	', Elapsed ' + convert(varchar(10), @Elapsed) + ' Mins' 


print @Stats;

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime;


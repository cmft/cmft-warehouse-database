﻿
CREATE procedure [ETL].[BuildHRGBase] as

-- PrimaryKey HRGID
-- Index on HRGCode
Insert into WH.HRGBase
	(
	HRGCode	
	,HRG	
	,HRGChapter	
	,HRGSubChapter	
	,HighCost
	)
Select distinct
	HRG_Code	
	,HRG_Description	
	,HRG_Chapter	
	,HRG_Subchapter	
	,High_Cost
from 
	[$(Contract_Reporting)].ID_Lookup.HRG
where 
	not exists
		(
		Select
			1
		from 
			WH.HRGBase
		where
			HRGBase.HRGCode = HRG.HRG_Code
		)

		
	


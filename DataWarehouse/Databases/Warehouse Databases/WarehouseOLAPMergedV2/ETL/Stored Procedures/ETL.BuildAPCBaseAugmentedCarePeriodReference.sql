﻿CREATE procedure [ETL].[BuildAPCBaseAugmentedCarePeriodReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseAugmentedCarePeriodReference
			where
				BaseAugmentedCarePeriodReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseAugmentedCarePeriodReference
where
	not exists
	(
	select
		1
	from
		APC.BaseAugmentedCarePeriod
	where
		BaseAugmentedCarePeriod.MergeEncounterRecno = BaseAugmentedCarePeriodReference.MergeEncounterRecno
	)
and	BaseAugmentedCarePeriodReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseAugmentedCarePeriodReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,ConsultantID = Consultant.SourceConsultantID
		,SpecialtyID = Specialty.SourceSpecialtyID

		,StartDateID =
			coalesce(
				 StartDate.DateID

				,case
				when Encounter.StartDate is null
				then NullDate.DateID

				when Encounter.StartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,EndDateID =
			coalesce(
				 EndDate.DateID

				,case
				when Encounter.EndDate is null
				then NullDate.DateID

				when Encounter.EndDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		APC.BaseAugmentedCarePeriod Encounter

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	inner join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = coalesce(Encounter.ConsultantCode, '-1')
	and	Consultant.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(Encounter.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar StartDate
	on	StartDate.TheDate = Encounter.StartDate

	left join WH.Calendar EndDate
	on	EndDate.TheDate = Encounter.EndDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,ConsultantID
			,SpecialtyID
			,StartDateID
			,EndDateID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.ConsultantID
			,source.SpecialtyID
			,source.StartDateID
			,source.EndDateID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.ConsultantID = source.ConsultantID
			,target.SpecialtyID = source.SpecialtyID
			,target.StartDateID = source.StartDateID
			,target.EndDateID = source.EndDateID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

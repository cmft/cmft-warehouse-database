﻿CREATE procedure [ETL].[BuildCenTheatreBaseOperationDetail]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Theatre.BaseOperationDetail target
using
	(
	select
		 DistrictNo
		,SourceUniqueID
		,PatientAgeInYears
		,OperationDay
		,OperationStartDate
		,TheatreCode
		,OperationDate
		,OperationEndDate
		,DateOfBirth
		,OperationTypeCode
		,OperationOrder
		,Forename
		,InSuiteTime
		,Surname
		,InAnaestheticTime
		,InRecoveryTime
		,BiohazardFlag
		,ASAScoreCode
		,UnplannedReturnReasonCode
		,ReviewFlag
		,OperationDetailSourceUniqueID
		,Operation
		,PatientDiedFlag
		,CauseOfDeath
		,DiagnosisCode
		,PatientAgeInYearsMonths
		,PatientAgeInYearsDays
		,SexCode
		,TransferredToWardCode
		,PatientSourceUniqueNo
		,PatientClassificationCode
		,PatientBookingSourceUniqueID
		,SpecialtyCode
		,FromWardCode
		,ToWardCode
		,ConsultantCode
		,Address1
		,Address3
		,Postcode
		,HomeTelephone
		,BusinessTelephone
		,Surgeon1Code
		,Surgeon2Code
		,Surgeon3Code
		,Anaesthetist1Code
		,Anaesthetist2Code
		,Anaesthetist3Code
		,ScoutNurseCode
		,AnaesthetistNurseCode
		,InstrumentNurseCode
		,Technician1Code
		,Technician2Code
		,Technician3Code
		,SessionNumber
		,CampusCode
		,SpecimenCode
		,DischargeTime
		,StaffInChargeCode
		,EscortReturnedTime
		,OperationCancelledFlag
		,DelayCode
		,AnaestheticInductionTime
		,ReadyToDepartTime
		,OtherCommentCode
		,PACUClassificationCode
		,PostOperationInstructionCode
		,ClinicalPriorityCode
		,EmergencyOperationSurgeonCode
		,SecondaryStaffCode
		,HospitalDischargeDate
		,HospitalEpisodeNo
		,UnplannedReturnToTheatreFlag
		,AdmissionTime
		,WaitingListSourceUniqueNo
		,AdmissionTypeCode
		,CalfCompressorUsedFlag
		,OperatingRoomReadyTime
		,DressingTime
		,NursingSetupCompleteTime
		,LastUpdated
		,ProceduralStatusCode
		,ACCNumber
		,ACCIndicatorCode
		,ACCCode
		,UpdatedFlag
		,PatientAddressStatus
		,OperationKey
		,ExtubationTime
		,Address2
		,Address4
		,SentForTime
		,PorterLeftTime
		,PorterCode
		,SecondaryTheatreCode
		,SecondaryConsultantCode
		,SecondaryOrderNumberCode
		,SecondarySessionNumber
		,AdditionalFlag
		,DictatedSurgeonNoteCode
		,AnaestheticReadyTime
		,PreparationReadyTime
		,SessionSourceUniqueID
		,CombinedCaseSessionSourceUniqueID
		,IncisionDetailCode
		,NextOperationAnaestheticInductionTime
	from
		ETL.TLoadCenTheatreBaseOperationDetail
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	and	target.ContextCode = 'CEN||ORMIS'

	when not matched by source
	and	target.ContextCode = 'CEN||ORMIS'
	then delete

	when not matched
	then
		insert
			(
			 DistrictNo
			,SourceUniqueID
			,PatientAgeInYears
			,OperationDay
			,OperationStartDate
			,TheatreCode
			,OperationDate
			,OperationEndDate
			,DateOfBirth
			,OperationTypeCode
			,OperationOrder
			,Forename
			,InSuiteTime
			,Surname
			,InAnaestheticTime
			,InRecoveryTime
			,BiohazardFlag
			,ASAScoreCode
			,UnplannedReturnReasonCode
			,ReviewFlag
			,OperationDetailSourceUniqueID
			,Operation
			,PatientDiedFlag
			,CauseOfDeath
			,DiagnosisCode
			,PatientAgeInYearsMonths
			,PatientAgeInYearsDays
			,SexCode
			,TransferredToWardCode
			,PatientSourceUniqueNo
			,PatientClassificationCode
			,PatientBookingSourceUniqueID
			,SpecialtyCode
			,FromWardCode
			,ToWardCode
			,ConsultantCode
			,Address1
			,Address3
			,Postcode
			,HomeTelephone
			,BusinessTelephone
			,Surgeon1Code
			,Surgeon2Code
			,Surgeon3Code
			,Anaesthetist1Code
			,Anaesthetist2Code
			,Anaesthetist3Code
			,ScoutNurseCode
			,AnaesthetistNurseCode
			,InstrumentNurseCode
			,Technician1Code
			,Technician2Code
			,Technician3Code
			,SessionNumber
			,CampusCode
			,SpecimenCode
			,DischargeTime
			,StaffInChargeCode
			,EscortReturnedTime
			,OperationCancelledFlag
			,DelayCode
			,AnaestheticInductionTime
			,ReadyToDepartTime
			,OtherCommentCode
			,PACUClassificationCode
			,PostOperationInstructionCode
			,ClinicalPriorityCode
			,EmergencyOperationSurgeonCode
			,SecondaryStaffCode
			,HospitalDischargeDate
			,HospitalEpisodeNo
			,UnplannedReturnToTheatreFlag
			,AdmissionTime
			,WaitingListSourceUniqueNo
			,AdmissionTypeCode
			,CalfCompressorUsedFlag
			,OperatingRoomReadyTime
			,DressingTime
			,NursingSetupCompleteTime
			,LastUpdated
			,ProceduralStatusCode
			,ACCNumber
			,ACCIndicatorCode
			,ACCCode
			,UpdatedFlag
			,PatientAddressStatus
			,OperationKey
			,ExtubationTime
			,Address2
			,Address4
			,SentForTime
			,PorterLeftTime
			,PorterCode
			,SecondaryTheatreCode
			,SecondaryConsultantCode
			,SecondaryOrderNumberCode
			,SecondarySessionNumber
			,AdditionalFlag
			,DictatedSurgeonNoteCode
			,AnaestheticReadyTime
			,PreparationReadyTime
			,SessionSourceUniqueID
			,CombinedCaseSessionSourceUniqueID
			,IncisionDetailCode
			,ContextCode
			,NextOperationAnaestheticInductionTime
			)
		values
			(
			 source.DistrictNo
			,source.SourceUniqueID
			,source.PatientAgeInYears
			,source.OperationDay
			,source.OperationStartDate
			,source.TheatreCode
			,source.OperationDate
			,source.OperationEndDate
			,source.DateOfBirth
			,source.OperationTypeCode
			,source.OperationOrder
			,source.Forename
			,source.InSuiteTime
			,source.Surname
			,source.InAnaestheticTime
			,source.InRecoveryTime
			,source.BiohazardFlag
			,source.ASAScoreCode
			,source.UnplannedReturnReasonCode
			,source.ReviewFlag
			,source.OperationDetailSourceUniqueID
			,source.Operation
			,source.PatientDiedFlag
			,source.CauseOfDeath
			,source.DiagnosisCode
			,source.PatientAgeInYearsMonths
			,source.PatientAgeInYearsDays
			,source.SexCode
			,source.TransferredToWardCode
			,source.PatientSourceUniqueNo
			,source.PatientClassificationCode
			,source.PatientBookingSourceUniqueID
			,source.SpecialtyCode
			,source.FromWardCode
			,source.ToWardCode
			,source.ConsultantCode
			,source.Address1
			,source.Address3
			,source.Postcode
			,source.HomeTelephone
			,source.BusinessTelephone
			,source.Surgeon1Code
			,source.Surgeon2Code
			,source.Surgeon3Code
			,source.Anaesthetist1Code
			,source.Anaesthetist2Code
			,source.Anaesthetist3Code
			,source.ScoutNurseCode
			,source.AnaesthetistNurseCode
			,source.InstrumentNurseCode
			,source.Technician1Code
			,source.Technician2Code
			,source.Technician3Code
			,source.SessionNumber
			,source.CampusCode
			,source.SpecimenCode
			,source.DischargeTime
			,source.StaffInChargeCode
			,source.EscortReturnedTime
			,source.OperationCancelledFlag
			,source.DelayCode
			,source.AnaestheticInductionTime
			,source.ReadyToDepartTime
			,source.OtherCommentCode
			,source.PACUClassificationCode
			,source.PostOperationInstructionCode
			,source.ClinicalPriorityCode
			,source.EmergencyOperationSurgeonCode
			,source.SecondaryStaffCode
			,source.HospitalDischargeDate
			,source.HospitalEpisodeNo
			,source.UnplannedReturnToTheatreFlag
			,source.AdmissionTime
			,source.WaitingListSourceUniqueNo
			,source.AdmissionTypeCode
			,source.CalfCompressorUsedFlag
			,source.OperatingRoomReadyTime
			,source.DressingTime
			,source.NursingSetupCompleteTime
			,source.LastUpdated
			,source.ProceduralStatusCode
			,source.ACCNumber
			,source.ACCIndicatorCode
			,source.ACCCode
			,source.UpdatedFlag
			,source.PatientAddressStatus
			,source.OperationKey
			,source.ExtubationTime
			,source.Address2
			,source.Address4
			,source.SentForTime
			,source.PorterLeftTime
			,source.PorterCode
			,source.SecondaryTheatreCode
			,source.SecondaryConsultantCode
			,source.SecondaryOrderNumberCode
			,source.SecondarySessionNumber
			,source.AdditionalFlag
			,source.DictatedSurgeonNoteCode
			,source.AnaestheticReadyTime
			,source.PreparationReadyTime
			,source.SessionSourceUniqueID
			,source.CombinedCaseSessionSourceUniqueID
			,source.IncisionDetailCode
			,'CEN||ORMIS'
			,source.NextOperationAnaestheticInductionTime
			)

	when matched
	and not
		(
			isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and isnull(target.PatientAgeInYears, 0) = isnull(source.PatientAgeInYears, 0)
		and isnull(target.OperationDay, '') = isnull(source.OperationDay, '')
		and isnull(target.OperationStartDate, getdate()) = isnull(source.OperationStartDate, getdate())
		and isnull(target.TheatreCode, 0) = isnull(source.TheatreCode, 0)
		and isnull(target.OperationDate, getdate()) = isnull(source.OperationDate, getdate())
		and isnull(target.OperationEndDate, getdate()) = isnull(source.OperationEndDate, getdate())
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.OperationTypeCode, '') = isnull(source.OperationTypeCode, '')
		and isnull(target.OperationOrder, 0) = isnull(source.OperationOrder, 0)
		and isnull(target.Forename, '') = isnull(source.Forename, '')
		and isnull(target.InSuiteTime, getdate()) = isnull(source.InSuiteTime, getdate())
		and isnull(target.Surname, '') = isnull(source.Surname, '')
		and isnull(target.InAnaestheticTime, getdate()) = isnull(source.InAnaestheticTime, getdate())
		and isnull(target.InRecoveryTime, getdate()) = isnull(source.InRecoveryTime, getdate())
		and isnull(target.BiohazardFlag, 0) = isnull(source.BiohazardFlag, 0)
		and isnull(target.ASAScoreCode, 0) = isnull(source.ASAScoreCode, 0)
		and isnull(target.UnplannedReturnReasonCode, 0) = isnull(source.UnplannedReturnReasonCode, 0)
		and isnull(target.ReviewFlag, 0) = isnull(source.ReviewFlag, 0)
		and isnull(target.OperationDetailSourceUniqueID, 0) = isnull(source.OperationDetailSourceUniqueID, 0)
		and isnull(target.Operation, '') = isnull(source.Operation, '')
		and isnull(target.PatientDiedFlag, 0) = isnull(source.PatientDiedFlag, 0)
		and isnull(target.CauseOfDeath, '') = isnull(source.CauseOfDeath, '')
		and isnull(target.DiagnosisCode, 0) = isnull(source.DiagnosisCode, 0)
		and isnull(target.PatientAgeInYearsMonths, 0) = isnull(source.PatientAgeInYearsMonths, 0)
		and isnull(target.PatientAgeInYearsDays, 0) = isnull(source.PatientAgeInYearsDays, 0)
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.TransferredToWardCode, '') = isnull(source.TransferredToWardCode, '')
		and isnull(target.PatientSourceUniqueNo, 0) = isnull(source.PatientSourceUniqueNo, 0)
		and isnull(target.PatientClassificationCode, 0) = isnull(source.PatientClassificationCode, 0)
		and isnull(target.PatientBookingSourceUniqueID, 0) = isnull(source.PatientBookingSourceUniqueID, 0)
		and isnull(target.SpecialtyCode, 0) = isnull(source.SpecialtyCode, 0)
		and isnull(target.FromWardCode, 0) = isnull(source.FromWardCode, 0)
		and isnull(target.ToWardCode, 0) = isnull(source.ToWardCode, 0)
		and isnull(target.ConsultantCode, 0) = isnull(source.ConsultantCode, 0)
		and isnull(target.Address1, '') = isnull(source.Address1, '')
		and isnull(target.Address3, '') = isnull(source.Address3, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.HomeTelephone, '') = isnull(source.HomeTelephone, '')
		and isnull(target.BusinessTelephone, '') = isnull(source.BusinessTelephone, '')
		and isnull(target.Surgeon1Code, 0) = isnull(source.Surgeon1Code, 0)
		and isnull(target.Surgeon2Code, 0) = isnull(source.Surgeon2Code, 0)
		and isnull(target.Surgeon3Code, 0) = isnull(source.Surgeon3Code, 0)
		and isnull(target.Anaesthetist1Code, 0) = isnull(source.Anaesthetist1Code, 0)
		and isnull(target.Anaesthetist2Code, 0) = isnull(source.Anaesthetist2Code, 0)
		and isnull(target.Anaesthetist3Code, 0) = isnull(source.Anaesthetist3Code, 0)
		and isnull(target.ScoutNurseCode, 0) = isnull(source.ScoutNurseCode, 0)
		and isnull(target.AnaesthetistNurseCode, 0) = isnull(source.AnaesthetistNurseCode, 0)
		and isnull(target.InstrumentNurseCode, 0) = isnull(source.InstrumentNurseCode, 0)
		and isnull(target.Technician1Code, 0) = isnull(source.Technician1Code, 0)
		and isnull(target.Technician2Code, 0) = isnull(source.Technician2Code, 0)
		and isnull(target.Technician3Code, 0) = isnull(source.Technician3Code, 0)
		and isnull(target.SessionNumber, 0) = isnull(source.SessionNumber, 0)
		and isnull(target.CampusCode, 0) = isnull(source.CampusCode, 0)
		and isnull(target.SpecimenCode, 0) = isnull(source.SpecimenCode, 0)
		and isnull(target.DischargeTime, getdate()) = isnull(source.DischargeTime, getdate())
		and isnull(target.StaffInChargeCode, 0) = isnull(source.StaffInChargeCode, 0)
		and isnull(target.EscortReturnedTime, getdate()) = isnull(source.EscortReturnedTime, getdate())
		and isnull(target.OperationCancelledFlag, 0) = isnull(source.OperationCancelledFlag, 0)
		and isnull(target.DelayCode, 0) = isnull(source.DelayCode, 0)
		and isnull(target.AnaestheticInductionTime, getdate()) = isnull(source.AnaestheticInductionTime, getdate())
		and isnull(target.ReadyToDepartTime, getdate()) = isnull(source.ReadyToDepartTime, getdate())
		and isnull(target.OtherCommentCode, 0) = isnull(source.OtherCommentCode, 0)
		and isnull(target.PACUClassificationCode, 0) = isnull(source.PACUClassificationCode, 0)
		and isnull(target.PostOperationInstructionCode, 0) = isnull(source.PostOperationInstructionCode, 0)
		and isnull(target.ClinicalPriorityCode, 0) = isnull(source.ClinicalPriorityCode, 0)
		and isnull(target.EmergencyOperationSurgeonCode, 0) = isnull(source.EmergencyOperationSurgeonCode, 0)
		and isnull(target.SecondaryStaffCode, 0) = isnull(source.SecondaryStaffCode, 0)
		and isnull(target.HospitalDischargeDate, getdate()) = isnull(source.HospitalDischargeDate, getdate())
		and isnull(target.HospitalEpisodeNo, '') = isnull(source.HospitalEpisodeNo, '')
		and isnull(target.UnplannedReturnToTheatreFlag, 0) = isnull(source.UnplannedReturnToTheatreFlag, 0)
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.WaitingListSourceUniqueNo, 0) = isnull(source.WaitingListSourceUniqueNo, 0)
		and isnull(target.AdmissionTypeCode, 0) = isnull(source.AdmissionTypeCode, 0)
		and isnull(target.CalfCompressorUsedFlag, 0) = isnull(source.CalfCompressorUsedFlag, 0)
		and isnull(target.OperatingRoomReadyTime, getdate()) = isnull(source.OperatingRoomReadyTime, getdate())
		and isnull(target.DressingTime, getdate()) = isnull(source.DressingTime, getdate())
		and isnull(target.NursingSetupCompleteTime, getdate()) = isnull(source.NursingSetupCompleteTime, getdate())
		and isnull(target.LastUpdated, getdate()) = isnull(source.LastUpdated, getdate())
		and isnull(target.ProceduralStatusCode, '') = isnull(source.ProceduralStatusCode, '')
		and isnull(target.ACCNumber, '') = isnull(source.ACCNumber, '')
		and isnull(target.ACCIndicatorCode, '') = isnull(source.ACCIndicatorCode, '')
		and isnull(target.ACCCode, 0) = isnull(source.ACCCode, 0)
		and isnull(target.UpdatedFlag, 0) = isnull(source.UpdatedFlag, 0)
		and isnull(target.PatientAddressStatus, '') = isnull(source.PatientAddressStatus, '')
		and isnull(target.OperationKey, 0) = isnull(source.OperationKey, 0)
		and isnull(target.ExtubationTime, getdate()) = isnull(source.ExtubationTime, getdate())
		and isnull(target.Address2, '') = isnull(source.Address2, '')
		and isnull(target.Address4, '') = isnull(source.Address4, '')
		and isnull(target.SentForTime, getdate()) = isnull(source.SentForTime, getdate())
		and isnull(target.PorterLeftTime, getdate()) = isnull(source.PorterLeftTime, getdate())
		and isnull(target.PorterCode, 0) = isnull(source.PorterCode, 0)
		and isnull(target.SecondaryTheatreCode, 0) = isnull(source.SecondaryTheatreCode, 0)
		and isnull(target.SecondaryConsultantCode, 0) = isnull(source.SecondaryConsultantCode, 0)
		and isnull(target.SecondaryOrderNumberCode, 0) = isnull(source.SecondaryOrderNumberCode, 0)
		and isnull(target.SecondarySessionNumber, 0) = isnull(source.SecondarySessionNumber, 0)
		and isnull(target.AdditionalFlag, 0) = isnull(source.AdditionalFlag, 0)
		and isnull(target.DictatedSurgeonNoteCode, 0) = isnull(source.DictatedSurgeonNoteCode, 0)
		and isnull(target.AnaestheticReadyTime, getdate()) = isnull(source.AnaestheticReadyTime, getdate())
		and isnull(target.PreparationReadyTime, getdate()) = isnull(source.PreparationReadyTime, getdate())
		and isnull(target.SessionSourceUniqueID, 0) = isnull(source.SessionSourceUniqueID, 0)
		and isnull(target.CombinedCaseSessionSourceUniqueID, 0) = isnull(source.CombinedCaseSessionSourceUniqueID, 0)
		and isnull(target.IncisionDetailCode, 0) = isnull(source.IncisionDetailCode, 0)
		and isnull(target.NextOperationAnaestheticInductionTime, getdate()) = isnull(source.NextOperationAnaestheticInductionTime, getdate())
		)
	then
		update
		set
			 target.DistrictNo = source.DistrictNo
			,target.SourceUniqueID = source.SourceUniqueID
			,target.PatientAgeInYears = source.PatientAgeInYears
			,target.OperationDay = source.OperationDay
			,target.OperationStartDate = source.OperationStartDate
			,target.TheatreCode = source.TheatreCode
			,target.OperationDate = source.OperationDate
			,target.OperationEndDate = source.OperationEndDate
			,target.DateOfBirth = source.DateOfBirth
			,target.OperationTypeCode = source.OperationTypeCode
			,target.OperationOrder = source.OperationOrder
			,target.Forename = source.Forename
			,target.InSuiteTime = source.InSuiteTime
			,target.Surname = source.Surname
			,target.InAnaestheticTime = source.InAnaestheticTime
			,target.InRecoveryTime = source.InRecoveryTime
			,target.BiohazardFlag = source.BiohazardFlag
			,target.ASAScoreCode = source.ASAScoreCode
			,target.UnplannedReturnReasonCode = source.UnplannedReturnReasonCode
			,target.ReviewFlag = source.ReviewFlag
			,target.OperationDetailSourceUniqueID = source.OperationDetailSourceUniqueID
			,target.Operation = source.Operation
			,target.PatientDiedFlag = source.PatientDiedFlag
			,target.CauseOfDeath = source.CauseOfDeath
			,target.DiagnosisCode = source.DiagnosisCode
			,target.PatientAgeInYearsMonths = source.PatientAgeInYearsMonths
			,target.PatientAgeInYearsDays = source.PatientAgeInYearsDays
			,target.SexCode = source.SexCode
			,target.TransferredToWardCode = source.TransferredToWardCode
			,target.PatientSourceUniqueNo = source.PatientSourceUniqueNo
			,target.PatientClassificationCode = source.PatientClassificationCode
			,target.PatientBookingSourceUniqueID = source.PatientBookingSourceUniqueID
			,target.SpecialtyCode = source.SpecialtyCode
			,target.FromWardCode = source.FromWardCode
			,target.ToWardCode = source.ToWardCode
			,target.ConsultantCode = source.ConsultantCode
			,target.Address1 = source.Address1
			,target.Address3 = source.Address3
			,target.Postcode = source.Postcode
			,target.HomeTelephone = source.HomeTelephone
			,target.BusinessTelephone = source.BusinessTelephone
			,target.Surgeon1Code = source.Surgeon1Code
			,target.Surgeon2Code = source.Surgeon2Code
			,target.Surgeon3Code = source.Surgeon3Code
			,target.Anaesthetist1Code = source.Anaesthetist1Code
			,target.Anaesthetist2Code = source.Anaesthetist2Code
			,target.Anaesthetist3Code = source.Anaesthetist3Code
			,target.ScoutNurseCode = source.ScoutNurseCode
			,target.AnaesthetistNurseCode = source.AnaesthetistNurseCode
			,target.InstrumentNurseCode = source.InstrumentNurseCode
			,target.Technician1Code = source.Technician1Code
			,target.Technician2Code = source.Technician2Code
			,target.Technician3Code = source.Technician3Code
			,target.SessionNumber = source.SessionNumber
			,target.CampusCode = source.CampusCode
			,target.SpecimenCode = source.SpecimenCode
			,target.DischargeTime = source.DischargeTime
			,target.StaffInChargeCode = source.StaffInChargeCode
			,target.EscortReturnedTime = source.EscortReturnedTime
			,target.OperationCancelledFlag = source.OperationCancelledFlag
			,target.DelayCode = source.DelayCode
			,target.AnaestheticInductionTime = source.AnaestheticInductionTime
			,target.ReadyToDepartTime = source.ReadyToDepartTime
			,target.OtherCommentCode = source.OtherCommentCode
			,target.PACUClassificationCode = source.PACUClassificationCode
			,target.PostOperationInstructionCode = source.PostOperationInstructionCode
			,target.ClinicalPriorityCode = source.ClinicalPriorityCode
			,target.EmergencyOperationSurgeonCode = source.EmergencyOperationSurgeonCode
			,target.SecondaryStaffCode = source.SecondaryStaffCode
			,target.HospitalDischargeDate = source.HospitalDischargeDate
			,target.HospitalEpisodeNo = source.HospitalEpisodeNo
			,target.UnplannedReturnToTheatreFlag = source.UnplannedReturnToTheatreFlag
			,target.AdmissionTime = source.AdmissionTime
			,target.WaitingListSourceUniqueNo = source.WaitingListSourceUniqueNo
			,target.AdmissionTypeCode = source.AdmissionTypeCode
			,target.CalfCompressorUsedFlag = source.CalfCompressorUsedFlag
			,target.OperatingRoomReadyTime = source.OperatingRoomReadyTime
			,target.DressingTime = source.DressingTime
			,target.NursingSetupCompleteTime = source.NursingSetupCompleteTime
			,target.LastUpdated = source.LastUpdated
			,target.ProceduralStatusCode = source.ProceduralStatusCode
			,target.ACCNumber = source.ACCNumber
			,target.ACCIndicatorCode = source.ACCIndicatorCode
			,target.ACCCode = source.ACCCode
			,target.UpdatedFlag = source.UpdatedFlag
			,target.PatientAddressStatus = source.PatientAddressStatus
			,target.OperationKey = source.OperationKey
			,target.ExtubationTime = source.ExtubationTime
			,target.Address2 = source.Address2
			,target.Address4 = source.Address4
			,target.SentForTime = source.SentForTime
			,target.PorterLeftTime = source.PorterLeftTime
			,target.PorterCode = source.PorterCode
			,target.SecondaryTheatreCode = source.SecondaryTheatreCode
			,target.SecondaryConsultantCode = source.SecondaryConsultantCode
			,target.SecondaryOrderNumberCode = source.SecondaryOrderNumberCode
			,target.SecondarySessionNumber = source.SecondarySessionNumber
			,target.AdditionalFlag = source.AdditionalFlag
			,target.DictatedSurgeonNoteCode = source.DictatedSurgeonNoteCode
			,target.AnaestheticReadyTime = source.AnaestheticReadyTime
			,target.PreparationReadyTime = source.PreparationReadyTime
			,target.SessionSourceUniqueID = source.SessionSourceUniqueID
			,target.CombinedCaseSessionSourceUniqueID = source.CombinedCaseSessionSourceUniqueID
			,target.IncisionDetailCode = source.IncisionDetailCode
			,target.NextOperationAnaestheticInductionTime = source.NextOperationAnaestheticInductionTime

output
	$action into @MergeSummary
;


if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

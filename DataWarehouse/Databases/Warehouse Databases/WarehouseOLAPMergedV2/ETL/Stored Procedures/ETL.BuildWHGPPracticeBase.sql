﻿

CREATE Procedure [ETL].[BuildWHGPPracticeBase]

as

Truncate table WH.GPPracticeBase

Insert into WH.GPPracticeBase
	(
	GpPracticeCode 
	,GpPracticeName 
	,ROCode 
	,HACode
	,AddressLine1 
	,AddressLine2 
	,AddressLine3 
	,AddressLine4 
	,AddressLine5 
	,Postcode
	,PracticeOpenDate
	,PracticeCloseDate
	,StatusCode 
	,OrganisationSubTypeCode 
	,ParentOrganisationCode 
	,JoinParentDate 
	,LeftParentDate 
	,ContactTelephone
	,ContactName 
	,AmendedRecordIndicator 
	,ElectronicCorrespondenceActivatedTime
	,ElectronicCorrespondenceDeactivatedTime
	,Created
	,Updated
	,ByWhom
	)

select
	GpPracticeCode = [Organisation Code]
	,GpPracticeName = [Organisation Name]
	,ROCode = [RO Code]
	,HACode = [HA Code]
	,AddressLine1 = [Address Line 1]
	,AddressLine2 = [Address Line 2]
	,AddressLine3 = [Address Line 3]
	,AddressLine4 = [Address Line 4]
	,AddressLine5 = [Address Line 5]
	,Postcode
	,PracticeOpenDate = [Open Date]
	,PracticeCloseDate = [Close Date]
	,StatusCode = [Status Code]
	,OrganisationSubTypeCode = [Organisation Sub Type Code]
	,ParentOrganisationCode = [Parent Organisation Code]
	,JoinParentDate = [Join Parent Date]
	,LeftParentDate = [Left Parent Date]
	,ContactTelephone = [Contact Telephone Number]
	,ContactName = [Contact Name]
	,AmendedRecordIndicator = [Amended Record Indicator]
	,ElectronicCorrespondenceActivatedTime = DateActivated
	,ElectronicCorrespondenceDeactivatedTime = DateDeActiviated
	,getdate()
	,getdate()
	,suser_name()
from
	[$(Organisation)].ODS.[General Medical Practice]	
	
left join [$(SmallDatasets)].[EDT].[ActivePractice] 
on [General Medical Practice].[Organisation Code] = [ActivePractice].PracticeCode


﻿CREATE procedure [ETL].[ImportTraAPCBaseWardStay] as

--import the data
exec ETL.BuildTraAPCBaseWardStay

declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseWardStay]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCBaseWardStayReference 'TRA||UG'


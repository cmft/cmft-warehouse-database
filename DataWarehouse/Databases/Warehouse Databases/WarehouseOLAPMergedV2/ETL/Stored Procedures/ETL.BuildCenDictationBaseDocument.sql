﻿



CREATE procedure [ETL].[BuildCenDictationBaseDocument]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@LoadStartDate date = dateadd(month, -12, getdate())


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Dictation.BaseDocument target
using
	(
	select
		DocumentRecno
		,SourceUniqueID
		,SourcePatientNo
		,DocumentID
		,DocumentDescription 
		,DocumentTypeCode 
		,AuthorCode 
		,TypedTime 
		,DictatedTime 
		,DictatedByCode
		,IssuedTime 
		,IssuedByCode
		,SignedTime
		,SignedByCode 
		,SignedStatusCode 
		,DepartmentCode
		,DocumentXMLTemplateCode 
		,DocumentXML = cast(DocumentXML as varchar(max))
		,TransmissionTime
		,TransmissionDestinationCode
		,ContextCode
	from
		ETL.TLoadCenDictationDocument Document

	) source
	on	source.DocumentRecno = target.DocumentRecno
	and	source.ContextCode = target.ContextCode
	
	when not matched by source
	and	target.ContextCode = 'CEN||MEDI'

	then delete

	when not matched
	then
		insert
			(
			DocumentRecno
			,SourceUniqueID
			,SourcePatientNo
			,DocumentID
			,DocumentDescription
			,DocumentTypeCode
			,AuthorCode
			,TypedTime
			,DictatedTime
			,DictatedByCode
			,IssuedTime
			,IssuedByCode
			,SignedTime
			,SignedByCode
			,SignedStatusCode
			,DepartmentCode
			,DocumentXMLTemplateCode
			,DocumentXML
			,TransmissionTime
			,TransmissionDestinationCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.DocumentRecno
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.DocumentID
			,source.DocumentDescription
			,source.DocumentTypeCode
			,source.AuthorCode
			,source.TypedTime
			,source.DictatedTime
			,source.DictatedByCode
			,source.IssuedTime
			,source.IssuedByCode
			,source.SignedTime
			,source.SignedByCode
			,source.SignedStatusCode
			,source.DepartmentCode
			,source.DocumentXMLTemplateCode
			,source.DocumentXML
			,source.TransmissionTime
			,source.TransmissionDestinationCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	isnull(target.DocumentRecno, '') = isnull(source.DocumentRecno, '')
		and	isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and	isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.DocumentID, '') = isnull(source.DocumentID, '')
		and isnull(target.DocumentDescription, '') = isnull(source.DocumentDescription, '')
		and isnull(target.DocumentTypeCode,'') = isnull(source.DocumentTypeCode, '')
		and isnull(target.AuthorCode, '') = isnull(source.AuthorCode, '')
		and isnull(target.TypedTime, getdate()) = isnull(source.TypedTime, getdate())
		and isnull(target.DictatedTime, getdate()) = isnull(source.DictatedTime, getdate())
		and isnull(target.DictatedByCode, '') = isnull(source.DictatedByCode, '')
		and isnull(target.IssuedTime, getdate()) = isnull(source.IssuedTime, getdate())
		and isnull(target.IssuedByCode, '') = isnull(source.IssuedByCode, '')
		and isnull(target.SignedTime, getdate()) = isnull(source.SignedTime, getdate())
		and isnull(target.SignedByCode, '') = isnull(source.SignedByCode, '')
		and isnull(target.SignedStatusCode, '') = isnull(source.SignedStatusCode, '')
		and isnull(target.DepartmentCode, '') = isnull(source.DepartmentCode, '')
		and isnull(target.DocumentXMLTemplateCode, '') = isnull(source.DocumentXMLTemplateCode, '')
		and cast(isnull(target.DocumentXML, '') as varchar(max)) = isnull(source.DocumentXML, '')
		and isnull(target.TransmissionTime, getdate()) = isnull(source.TransmissionTime, getdate())
		and isnull(target.TransmissionDestinationCode, '') = isnull(source.TransmissionDestinationCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			target.DocumentRecno = source.DocumentRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.DocumentID = source.DocumentID
			,target.DocumentDescription = source.DocumentDescription
			,target.DocumentTypeCode = source.DocumentTypeCode
			,target.AuthorCode = source.AuthorCode
			,target.TypedTime = source.TypedTime
			,target.DictatedTime = source.DictatedTime
			,target.DictatedByCode = source.DictatedByCode
			,target.IssuedTime = source.IssuedTime
			,target.IssuedByCode = source.IssuedByCode
			,target.SignedTime = source.SignedTime
			,target.SignedByCode = source.SignedByCode
			,target.SignedStatusCode = source.SignedStatusCode
			,target.DepartmentCode = source.DepartmentCode
			,target.DocumentXMLTemplateCode = source.DocumentXMLTemplateCode
			,target.DocumentXML = source.DocumentXML
			,target.TransmissionTime = source.TransmissionTime
			,target.TransmissionDestinationCode = source.TransmissionDestinationCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime



﻿create procedure [ETL].[ImportCenSCRBaseCarePlan] as

--import the data
exec ETL.BuildCenSCRBaseCarePlan


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[SCR].[BasecarePlan]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
--exec ETL.BuildSCRBaseCarePlanReference 'CEN||SCR'


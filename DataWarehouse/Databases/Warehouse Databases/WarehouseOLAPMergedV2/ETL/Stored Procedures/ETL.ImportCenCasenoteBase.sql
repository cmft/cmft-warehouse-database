﻿CREATE procedure [ETL].[ImportCenCasenoteBase] as

--import the data
exec ETL.BuildCenCasenoteBase


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Casenote].[Base]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember

--Build the BaseReference table
exec ETL.BuildCasenoteBaseReference 'CEN||PAS'


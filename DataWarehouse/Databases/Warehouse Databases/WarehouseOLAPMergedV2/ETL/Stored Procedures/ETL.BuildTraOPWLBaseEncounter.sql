﻿
CREATE procedure [ETL].[BuildTraOPWLBaseEncounter]
as

/* 
==============================================================================================
Description:	

When		Who			What
20150427	Paul Egan	Added CCGCode and ResidenceCCGCode
20150519	Colin		EthnicOrginCode
===============================================================================================
*/




declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

--clear out old snapshots
delete
from
	OPWL.BaseEncounter
where 
	not exists
	(
	select
		1
	from
		(
	--Month end snapshots
		select
			Calendar.TheDate
		from
			WH.Calendar
		where
			Calendar.TheDate in
			(
			select
				max(TheDate)
			from
				WH.Calendar
			group by
				 year(TheDate)
				,month(TheDate)

			union
	--snapshots in the last fourteen days
			select
				TheDate
			from
				WH.Calendar
			where
				TheDate between dateadd(day , -14 , getdate()) and getdate()

			union
	--Sunday snapshots
			select
				TheDate
			from
				WH.Calendar
			where
				datename(dw, TheDate) = 'Sunday'
			)
		) Calendar
		where
			BaseEncounter.CensusDate = TheDate
	)
and	BaseEncounter.ContextCode =  'TRA||UG'

select @deleted = @@ROWCOUNT


merge
	OPWL.BaseEncounter target
using
	(
	select
		 AdminCategoryCode = cast(AdminCategoryCode as varchar(10))
		,AgeOnAppointment = cast(AgeOnAppointment as tinyint)
		,AppointmentDate = cast(AppointmentDate as datetime)
		,AppointmentNote = cast(AppointmentNote as varchar(1024))
		,AppointmentID = cast(AppointmentID as varchar(50))
		,AppointmentStatusCode = cast(AppointmentStatusCode as varchar(10))
		,AppointmentTypeCode = cast(AppointmentTypeCode as varchar(10))
		,APPReferralDate = cast(APPReferralDate as datetime)
		,AttendanceID = cast(AttendanceID as varchar(50))
		,AttendanceOutcomeCode = cast(AttendanceOutcomeCode as varchar(10))
		,CarerSupportIndicator = cast(CarerSupportIndicator as varchar(10))
		,CasenoteNo = cast(CasenoteNo as varchar(50))
		,CensusDate = cast(CensusDate as datetime)
		,ClinicCode = cast(ClinicCode as varchar(50))
		,ClinicFunctionCode = cast(ClinicFunctionCode as varchar(50))
		,CommissionerCode = cast(CommissionerCode as varchar(10))
		,CommissionerReferenceNo = cast(CommissionerReferenceNo as varchar(50))
		,CommissioningSerialNo = cast(CommissioningSerialNo as varchar(50))
		,ContactChangeDate = cast(ContactChangeDate as datetime)
		,ContactChangeTime = cast(ContactChangeTime as datetime)
		,ContactCreationDate = cast(ContactCreationDate as datetime)
		,ContextCode = cast(ContextCode as varchar(7))
		,DateOfBirth = cast(DateOfBirth as datetime)
		,DateOfDeath = cast(DateOfDeath as datetime)
		,DepartmentCode = cast(DepartmentCode as varchar(10))
		,DistrictNo = cast(DistrictNo as varchar(50))
		,DistrictNoOrganisationCode = cast(DistrictNoOrganisationCode as varchar(10))
		,DNACode = cast(DNACode as varchar(10))
		,EarliestReasonableOfferDate = cast(EarliestReasonableOfferDate as datetime)
		,EncounterRecno = cast(EncounterRecno as int)
		,EpisodeChangeDate = cast(EpisodeChangeDate as datetime)
		,EpisodeChangeTime = cast(EpisodeChangeTime as datetime)
		,EthnicCategoryCode = cast(EthnicCategoryCode as varchar(10))
		,EthnicOriginCode = cast(EthnicOriginCode as varchar(10))
		,FirstAttendanceCode = cast(FirstAttendanceCode as int)
		,HRGVersionCode = cast(HRGVersionCode as varchar(10))
		,ICDDiagnosisSchemeCode = cast(ICDDiagnosisSchemeCode as varchar(10))
		,IsDeleted = cast(IsDeleted as bit)
		,LastDNAOrCancelledDate = cast(LastDNAOrCancelledDate as datetime)
		,LocationClassCode = cast(LocationClassCode as varchar(10))
		,LocationTypeCode = cast(LocationTypeCode as varchar(10))
		,MaritalStatusCode = cast(MaritalStatusCode as varchar(10))
		,MedicalStaffTypeCode = cast(MedicalStaffTypeCode as varchar(10))
		,NHSNumber = cast(NHSNumber as varchar(17))
		,NHSNumberStatusId = cast(NHSNumberStatusId as varchar(10))
		,NHSServiceAgreementLineNo = cast(NHSServiceAgreementLineNo as varchar(50))
		,OPCSProcedureSchemeCode = cast(OPCSProcedureSchemeCode as varchar(10))
		,OperationStatusCode = cast(OperationStatusCode as varchar(10))
		,OriginalPathwayId = cast(OriginalPathwayId as varchar(50))
		,OriginalPathwayIdIssuerCode = cast(OriginalPathwayIdIssuerCode as varchar(10))
		,OriginalRTTEndDate = cast(OriginalRTTEndDate as datetime)
		,OriginalRTTStartDate = cast(OriginalRTTStartDate as datetime)
		,OriginalRTTStatusCode = cast(OriginalRTTStatusCode as varchar(10))
		,OverseasVisitorCode = cast(OverseasVisitorCode as varchar(10))
		,PASDGVPCode = cast(PASDGVPCode as varchar(10))
		,PASHRGCode = cast(PASHRGCode as varchar(10))
		,PASUpdateDate = cast(PASUpdateDate as datetime)
		,PathwayId = cast(PathwayId as varchar(50))
		,PathwayIdIssuerCode = cast(PathwayIdIssuerCode as varchar(10))
		,PatientAddress1 = cast(PatientAddress1 as varchar(100))
		,PatientAddress2 = cast(PatientAddress2 as varchar(100))
		,PatientAddress3 = cast(PatientAddress3 as varchar(100))
		,PatientAddress4 = cast(PatientAddress4 as varchar(100))
		,PatientForename = cast(PatientForename as varchar(50))
		,PatientSurname = cast(PatientSurname as varchar(50))
		,PatientTitle = cast(PatientTitle as varchar(50))
		,PCTCode = cast(PCTCode as varchar(10))
		,Postcode = cast(Postcode as varchar(10))
		,PrimaryDiagnosisCode = cast(PrimaryDiagnosisCode as varchar(50))
		,PrimaryDiagnosisReadCode = cast(PrimaryDiagnosisReadCode as varchar(10))
		,PrimaryProcedureCode = cast(PrimaryProcedureCode as varchar(50))
		,PrimaryProcedureDate = cast(PrimaryProcedureDate as datetime)
		,PrimaryProcedureReadCode = cast(PrimaryProcedureReadCode as varchar(10))
		,PriorityTypeCode = cast(PriorityTypeCode as varchar(10))
		,ProviderCode = cast(ProviderCode as varchar(10))
		,ProviderReferenceNo = cast(ProviderReferenceNo as varchar(50))
		,ReadDiagnosisSchemeCode = cast(ReadDiagnosisSchemeCode as varchar(10))
		,ReadProcedureSchemeCode = cast(ReadProcedureSchemeCode as varchar(10))
		,ReferralRequestReceivedDate = cast(ReferralRequestReceivedDate as datetime)
		,ReferrerCode = cast(ReferrerCode as varchar(20))
		,ReferrerOrganisationCode = cast(ReferrerOrganisationCode as varchar(10))
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(10))
		,ResidencePCTCode = cast(ResidencePCTCode as varchar(10))
		,RTTEndDate = cast(RTTEndDate as datetime)
		,RTTStartDate = cast(RTTStartDate as datetime)
		,RTTStatusCode = cast(RTTStatusCode as varchar(10))
		,ServiceTypeRequestedCode = cast(ServiceTypeRequestedCode as varchar(10))
		,SexCode = cast(SexCode as varchar(10))
		,SiteCode = cast(SiteCode as varchar(10))
		,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(10))
		,SourcePatientNo = cast(SourcePatientNo as varchar(50))
		,SourceUniqueID = cast(SourceUniqueID as varchar(50))
		,TreatmentFunctionCode = cast(TreatmentFunctionCode as varchar(10))
		,TrustNo = cast(TrustNo as varchar(50))
		,UniqueBookingReferenceNo = cast(UniqueBookingReferenceNo as varchar(50))
		,WaitingListCode = cast(WaitingListCode as varchar(3))
		,ConsultantCode = cast(ConsultantCode as varchar(50))
		,SpecialtyCode = cast(SpecialtyCode as varchar(10))
		,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(10))
		,AgeCode = cast(AgeCode as varchar(27))
		,Cases = cast(Cases as int)
		,WaitTypeCode = cast(WaitTypeCode as varchar(2))
		,StatusCode = cast(StatusCode as varchar(2))
		,OPCSCoded = cast(OPCSCoded as int)
		,WithRTTStartDate = cast(WithRTTStartDate as int)
		,WithRTTStatusCode = cast(WithRTTStatusCode as int)
		,WithExpectedAdmissionDate = cast(WithExpectedAdmissionDate as int)
		,RTTActivity = cast(RTTActivity as bit)
		,DiagnosticProcedure = cast(DiagnosticProcedure as int)
		,DirectorateCode = cast(DirectorateCode as varchar(5))
		,DurationAtTCIDateCode = cast(DurationAtTCIDateCode as varchar(3))
		,LengthOfWait = cast(LengthOfWait as int)
		,DurationCode = cast(DurationCode as varchar(3))
		,PASSpecialtyCode = cast(PASSpecialtyCode as varchar(10))
		,WithRTTOpenPathway = cast(WithRTTOpenPathway as int)
		,DateOnWaitingList = cast(DateOnWaitingList as datetime)
		,OriginalDateOnWaitingList = cast(OriginalDateOnWaitingList as datetime)
		,Referraldate = cast(Referraldate as datetime)
		,WithAppointmentDate = cast(WithAppointmentDate as int)
		,DurationAtAppointmentDateCode = cast(DurationAtAppointmentDateCode as varchar(3))
		,CCGCode = cast(CCGCode as varchar(10))
		,ResidenceCCGCode = cast(ResidenceCCGCode as varchar(10))
		,DataSourceID = cast(DataSourceID as int)
	from
		ETL.TLoadTraOPWLBaseEncounter Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno


	when not matched
	then
		insert
			(
			 AdminCategoryCode
			,AgeOnAppointment
			,AppointmentDate
			,AppointmentNote
			,AppointmentID
			,AppointmentStatusCode
			,AppointmentTypeCode
			,APPReferralDate
			,AttendanceID
			,AttendanceOutcomeCode
			,CarerSupportIndicator
			,CasenoteNo
			,CensusDate
			,ClinicCode
			,ClinicFunctionCode
			,CommissionerCode
			,CommissionerReferenceNo
			,CommissioningSerialNo
			,ContactChangeDate
			,ContactChangeTime
			,ContactCreationDate
			,ContextCode
			,DateOfBirth
			,DateOfDeath
			,DepartmentCode
			,DistrictNo
			,DistrictNoOrganisationCode
			,DNACode
			,EarliestReasonableOfferDate
			,EncounterRecno
			,EpisodeChangeDate
			,EpisodeChangeTime
			,EthnicCategoryCode
			,EthnicOriginCode
			,FirstAttendanceCode
			,HRGVersionCode
			,ICDDiagnosisSchemeCode
			,IsDeleted
			,LastDNAOrCancelledDate
			,LocationClassCode
			,LocationTypeCode
			,MaritalStatusCode
			,MedicalStaffTypeCode
			,NHSNumber
			,NHSNumberStatusId
			,NHSServiceAgreementLineNo
			,OPCSProcedureSchemeCode
			,OperationStatusCode
			,OriginalPathwayId
			,OriginalPathwayIdIssuerCode
			,OriginalRTTEndDate
			,OriginalRTTStartDate
			,OriginalRTTStatusCode
			,OverseasVisitorCode
			,PASDGVPCode
			,PASHRGCode
			,PASUpdateDate
			,PathwayId
			,PathwayIdIssuerCode
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,PatientForename
			,PatientSurname
			,PatientTitle
			,PCTCode
			,Postcode
			,PrimaryDiagnosisCode
			,PrimaryDiagnosisReadCode
			,PrimaryProcedureCode
			,PrimaryProcedureDate
			,PrimaryProcedureReadCode
			,PriorityTypeCode
			,ProviderCode
			,ProviderReferenceNo
			,ReadDiagnosisSchemeCode
			,ReadProcedureSchemeCode
			,ReferralRequestReceivedDate
			,ReferrerCode
			,ReferrerOrganisationCode
			,RegisteredGpCode
			,ResidencePCTCode
			,RTTEndDate
			,RTTStartDate
			,RTTStatusCode
			,ServiceTypeRequestedCode
			,SexCode
			,SiteCode
			,SourceOfReferralCode
			,SourcePatientNo
			,SourceUniqueID
			,TreatmentFunctionCode
			,TrustNo
			,UniqueBookingReferenceNo
			,WaitingListCode
			,ConsultantCode
			,SpecialtyCode
			,RegisteredGpPracticeCode
			,AgeCode
			,Cases
			,WaitTypeCode
			,StatusCode
			,OPCSCoded
			,WithRTTStartDate
			,WithRTTStatusCode
			,WithExpectedAdmissionDate
			,RTTActivity
			,DiagnosticProcedure
			,DirectorateCode
			,DurationAtTCIDateCode
			,LengthOfWait
			,DurationCode
			,PASSpecialtyCode
			,WithRTTOpenPathway
			,DateOnWaitingList
			,OriginalDateOnWaitingList
			,ReferralDate
			,WithAppointmentDate
			,DurationAtAppointmentDateCode
			,CCGCode
			,ResidenceCCGCode
			,Created
			,Updated
			,ByWhom
			,DataSourceID
			)
		values
			(
			 source.AdminCategoryCode
			,source.AgeOnAppointment
			,source.AppointmentDate
			,source.AppointmentNote
			,source.AppointmentID
			,source.AppointmentStatusCode
			,source.AppointmentTypeCode
			,source.APPReferralDate
			,source.AttendanceID
			,source.AttendanceOutcomeCode
			,source.CarerSupportIndicator
			,source.CasenoteNo
			,source.CensusDate
			,source.ClinicCode
			,source.ClinicFunctionCode
			,source.CommissionerCode
			,source.CommissionerReferenceNo
			,source.CommissioningSerialNo
			,source.ContactChangeDate
			,source.ContactChangeTime
			,source.ContactCreationDate
			,source.ContextCode
			,source.DateOfBirth
			,source.DateOfDeath
			,source.DepartmentCode
			,source.DistrictNo
			,source.DistrictNoOrganisationCode
			,source.DNACode
			,source.EarliestReasonableOfferDate
			,source.EncounterRecno
			,source.EpisodeChangeDate
			,source.EpisodeChangeTime
			,source.EthnicCategoryCode
			,source.EthnicOriginCode
			,source.FirstAttendanceCode
			,source.HRGVersionCode
			,source.ICDDiagnosisSchemeCode
			,source.IsDeleted
			,source.LastDNAOrCancelledDate
			,source.LocationClassCode
			,source.LocationTypeCode
			,source.MaritalStatusCode
			,source.MedicalStaffTypeCode
			,source.NHSNumber
			,source.NHSNumberStatusId
			,source.NHSServiceAgreementLineNo
			,source.OPCSProcedureSchemeCode
			,source.OperationStatusCode
			,source.OriginalPathwayId
			,source.OriginalPathwayIdIssuerCode
			,source.OriginalRTTEndDate
			,source.OriginalRTTStartDate
			,source.OriginalRTTStatusCode
			,source.OverseasVisitorCode
			,source.PASDGVPCode
			,source.PASHRGCode
			,source.PASUpdateDate
			,source.PathwayId
			,source.PathwayIdIssuerCode
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.PatientForename
			,source.PatientSurname
			,source.PatientTitle
			,source.PCTCode
			,source.Postcode
			,source.PrimaryDiagnosisCode
			,source.PrimaryDiagnosisReadCode
			,source.PrimaryProcedureCode
			,source.PrimaryProcedureDate
			,source.PrimaryProcedureReadCode
			,source.PriorityTypeCode
			,source.ProviderCode
			,source.ProviderReferenceNo
			,source.ReadDiagnosisSchemeCode
			,source.ReadProcedureSchemeCode
			,source.ReferralRequestReceivedDate
			,source.ReferrerCode
			,source.ReferrerOrganisationCode
			,source.RegisteredGpCode
			,source.ResidencePCTCode
			,source.RTTEndDate
			,source.RTTStartDate
			,source.RTTStatusCode
			,source.ServiceTypeRequestedCode
			,source.SexCode
			,source.SiteCode
			,source.SourceOfReferralCode
			,source.SourcePatientNo
			,source.SourceUniqueID
			,source.TreatmentFunctionCode
			,source.TrustNo
			,source.UniqueBookingReferenceNo
			,source.WaitingListCode
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.RegisteredGpPracticeCode
			,source.AgeCode
			,source.Cases
			,source.WaitTypeCode
			,source.StatusCode
			,source.OPCSCoded
			,source.WithRTTStartDate
			,source.WithRTTStatusCode
			,source.WithExpectedAdmissionDate
			,source.RTTActivity
			,source.DiagnosticProcedure
			,source.DirectorateCode
			,source.DurationAtTCIDateCode
			,source.LengthOfWait
			,source.DurationCode
			,source.PASSpecialtyCode
			,source.WithRTTOpenPathway
			,source.DateOnWaitingList
			,source.OriginalDateOnWaitingList
			,source.Referraldate
			,source.WithAppointmentDate
			,source.DurationAtAppointmentDateCode
			,source.CCGCode
			,source.ResidenceCCGCode
			,getdate()
			,getdate()
			,suser_name()
			,source.DataSourceID
			)

	when matched
	and not
		(
			isnull(target.AdminCategoryCode, '') = isnull(source.AdminCategoryCode, '')
		and isnull(target.AgeOnAppointment, 0) = isnull(source.AgeOnAppointment, 0)
		and isnull(target.AppointmentDate, getdate()) = isnull(source.AppointmentDate, getdate())
		and isnull(target.AppointmentNote, '') = isnull(source.AppointmentNote, '')
		and isnull(target.AppointmentID, 0) = isnull(source.AppointmentID, 0)
		and isnull(target.AppointmentStatusCode, '') = isnull(source.AppointmentStatusCode, '')
		and isnull(target.AppointmentTypeCode, '') = isnull(source.AppointmentTypeCode, '')
		and isnull(target.APPReferralDate, getdate()) = isnull(source.APPReferralDate, getdate())
		and isnull(target.AttendanceID, '') = isnull(source.AttendanceID, '')
		and isnull(target.AttendanceOutcomeCode, '') = isnull(source.AttendanceOutcomeCode, '')
		and isnull(target.CarerSupportIndicator, '') = isnull(source.CarerSupportIndicator, '')
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.CensusDate, getdate()) = isnull(source.CensusDate, getdate())
		and isnull(target.ClinicCode, '') = isnull(source.ClinicCode, '')
		and isnull(target.ClinicFunctionCode, '') = isnull(source.ClinicFunctionCode, '')
		and isnull(target.CommissionerCode, '') = isnull(source.CommissionerCode, '')
		and isnull(target.CommissionerReferenceNo, '') = isnull(source.CommissionerReferenceNo, '')
		and isnull(target.CommissioningSerialNo, '') = isnull(source.CommissioningSerialNo, '')
		and isnull(target.ContactChangeDate, getdate()) = isnull(source.ContactChangeDate, getdate())
		and isnull(target.ContactChangeTime, getdate()) = isnull(source.ContactChangeTime, getdate())
		and isnull(target.ContactCreationDate, getdate()) = isnull(source.ContactCreationDate, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.DepartmentCode, '') = isnull(source.DepartmentCode, '')
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.DistrictNoOrganisationCode, '') = isnull(source.DistrictNoOrganisationCode, '')
		and isnull(target.DNACode, '') = isnull(source.DNACode, '')
		and isnull(target.EarliestReasonableOfferDate, getdate()) = isnull(source.EarliestReasonableOfferDate, getdate())
		and isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.EpisodeChangeDate, getdate()) = isnull(source.EpisodeChangeDate, getdate())
		and isnull(target.EpisodeChangeTime, getdate()) = isnull(source.EpisodeChangeTime, getdate())
		and isnull(target.EthnicCategoryCode, '') = isnull(source.EthnicCategoryCode, '')
		and isnull(target.EthnicOriginCode, '') = isnull(source.EthnicOriginCode, '')
		and isnull(target.FirstAttendanceCode, 0) = isnull(source.FirstAttendanceCode, 0)
		and isnull(target.HRGVersionCode, '') = isnull(source.HRGVersionCode, '')
		and isnull(target.ICDDiagnosisSchemeCode, '') = isnull(source.ICDDiagnosisSchemeCode, '')
		and isnull(target.IsDeleted, 0) = isnull(source.IsDeleted, 0)
		and isnull(target.LastDNAOrCancelledDate, getdate()) = isnull(source.LastDNAOrCancelledDate, getdate())
		and isnull(target.LocationClassCode, '') = isnull(source.LocationClassCode, '')
		and isnull(target.LocationTypeCode, '') = isnull(source.LocationTypeCode, '')
		and isnull(target.MaritalStatusCode, '') = isnull(source.MaritalStatusCode, '')
		and isnull(target.MedicalStaffTypeCode, '') = isnull(source.MedicalStaffTypeCode, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.NHSNumberStatusId, '') = isnull(source.NHSNumberStatusId, '')
		and isnull(target.NHSServiceAgreementLineNo, '') = isnull(source.NHSServiceAgreementLineNo, '')
		and isnull(target.OPCSProcedureSchemeCode, '') = isnull(source.OPCSProcedureSchemeCode, '')
		and isnull(target.OperationStatusCode, '') = isnull(source.OperationStatusCode, '')
		and isnull(target.OriginalPathwayId, '') = isnull(source.OriginalPathwayId, '')
		and isnull(target.OriginalPathwayIdIssuerCode, '') = isnull(source.OriginalPathwayIdIssuerCode, '')
		and isnull(target.OriginalRTTEndDate, getdate()) = isnull(source.OriginalRTTEndDate, getdate())
		and isnull(target.OriginalRTTStartDate, getdate()) = isnull(source.OriginalRTTStartDate, getdate())
		and isnull(target.OriginalRTTStatusCode, '') = isnull(source.OriginalRTTStatusCode, '')
		and isnull(target.OverseasVisitorCode, '') = isnull(source.OverseasVisitorCode, '')
		and isnull(target.PASDGVPCode, '') = isnull(source.PASDGVPCode, '')
		and isnull(target.PASHRGCode, '') = isnull(source.PASHRGCode, '')
		and isnull(target.PASUpdateDate, getdate()) = isnull(source.PASUpdateDate, getdate())
		and isnull(target.PathwayId, '') = isnull(source.PathwayId, '')
		and isnull(target.PathwayIdIssuerCode, '') = isnull(source.PathwayIdIssuerCode, '')
		and isnull(target.PatientAddress1, '') = isnull(source.PatientAddress1, '')
		and isnull(target.PatientAddress2, '') = isnull(source.PatientAddress2, '')
		and isnull(target.PatientAddress3, '') = isnull(source.PatientAddress3, '')
		and isnull(target.PatientAddress4, '') = isnull(source.PatientAddress4, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.PatientTitle, '') = isnull(source.PatientTitle, '')
		and isnull(target.PCTCode, '') = isnull(source.PCTCode, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.PrimaryDiagnosisCode, '') = isnull(source.PrimaryDiagnosisCode, '')
		and isnull(target.PrimaryDiagnosisReadCode, '') = isnull(source.PrimaryDiagnosisReadCode, '')
		and isnull(target.PrimaryProcedureCode, '') = isnull(source.PrimaryProcedureCode, '')
		and isnull(target.PrimaryProcedureDate, getdate()) = isnull(source.PrimaryProcedureDate, getdate())
		and isnull(target.PrimaryProcedureReadCode, '') = isnull(source.PrimaryProcedureReadCode, '')
		and isnull(target.PriorityTypeCode, '') = isnull(source.PriorityTypeCode, '')
		and isnull(target.ProviderCode, '') = isnull(source.ProviderCode, '')
		and isnull(target.ProviderReferenceNo, '') = isnull(source.ProviderReferenceNo, '')
		and isnull(target.ReadDiagnosisSchemeCode, '') = isnull(source.ReadDiagnosisSchemeCode, '')
		and isnull(target.ReadProcedureSchemeCode, '') = isnull(source.ReadProcedureSchemeCode, '')
		and isnull(target.ReferralRequestReceivedDate, getdate()) = isnull(source.ReferralRequestReceivedDate, getdate())
		and isnull(target.ReferrerCode, '') = isnull(source.ReferrerCode, '')
		and isnull(target.ReferrerOrganisationCode, '') = isnull(source.ReferrerOrganisationCode, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.ResidencePCTCode, '') = isnull(source.ResidencePCTCode, '')
		and isnull(target.RTTEndDate, getdate()) = isnull(source.RTTEndDate, getdate())
		and isnull(target.RTTStartDate, getdate()) = isnull(source.RTTStartDate, getdate())
		and isnull(target.RTTStatusCode, '') = isnull(source.RTTStatusCode, '')
		and isnull(target.ServiceTypeRequestedCode, '') = isnull(source.ServiceTypeRequestedCode, '')
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.SourceOfReferralCode, '') = isnull(source.SourceOfReferralCode, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.TreatmentFunctionCode, '') = isnull(source.TreatmentFunctionCode, '')
		and isnull(target.TrustNo, '') = isnull(source.TrustNo, '')
		and isnull(target.UniqueBookingReferenceNo, '') = isnull(source.UniqueBookingReferenceNo, '')
		and isnull(target.WaitingListCode, '') = isnull(source.WaitingListCode, '')
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		and isnull(target.RegisteredGpPracticeCode, '') = isnull(source.RegisteredGpPracticeCode, '')
		and isnull(target.AgeCode, '') = isnull(source.AgeCode, '')
		and isnull(target.Cases, 0) = isnull(source.Cases, 0)
		and isnull(target.WaitTypeCode, '') = isnull(source.WaitTypeCode, '')
		and isnull(target.StatusCode, '') = isnull(source.StatusCode, '')
		and isnull(target.OPCSCoded, 0) = isnull(source.OPCSCoded, 0)
		and isnull(target.WithRTTStartDate, 0) = isnull(source.WithRTTStartDate, 0)
		and isnull(target.WithRTTStatusCode, 0) = isnull(source.WithRTTStatusCode, 0)
		and isnull(target.WithExpectedAdmissionDate, 0) = isnull(source.WithExpectedAdmissionDate, 0)
		and isnull(target.RTTActivity, 0) = isnull(source.RTTActivity, 0)
		and isnull(target.DiagnosticProcedure, 0) = isnull(source.DiagnosticProcedure, 0)
		and isnull(target.DirectorateCode, 0) = isnull(source.DirectorateCode, 0)
		and isnull(target.DurationAtTCIDateCode, '') = isnull(source.DurationAtTCIDateCode, '')
		and isnull(target.LengthOfWait, 0) = isnull(source.LengthOfWait, 0)
		and isnull(target.DurationCode, '') = isnull(source.DurationCode, '')
		and isnull(target.PASSpecialtyCode, '') = isnull(source.PASSpecialtyCode, '')
		and isnull(target.WithRTTOpenPathway, 0) = isnull(source.WithRTTOpenPathway, 0)
		and isnull(target.DateOnWaitingList, getdate()) = isnull(source.DateOnWaitingList, getdate())
		and isnull(target.OriginalDateOnWaitingList, getdate()) = isnull(source.OriginalDateOnWaitingList, getdate())
		and isnull(target.ReferralDate, getdate()) = isnull(source.Referraldate, getdate())
		and isnull(target.WithAppointmentDate, 0) = isnull(source.WithAppointmentDate, 0)
		and isnull(target.DurationAtAppointmentDateCode, '') = isnull(source.DurationAtAppointmentDateCode, '')
		and isnull(target.CCGCode, '') = isnull(source.CCGCode, '')
		and isnull(target.ResidenceCCGCode, '') = isnull(source.ResidenceCCGCode, '')
		)
	then
		update
		set
			 target.AdminCategoryCode = source.AdminCategoryCode
			,target.AgeOnAppointment = source.AgeOnAppointment
			,target.AppointmentDate = source.AppointmentDate
			,target.AppointmentNote = source.AppointmentNote
			,target.AppointmentID = source.AppointmentID
			,target.AppointmentStatusCode = source.AppointmentStatusCode
			,target.AppointmentTypeCode = source.AppointmentTypeCode
			,target.APPReferralDate = source.APPReferralDate
			,target.AttendanceID = source.AttendanceID
			,target.AttendanceOutcomeCode = source.AttendanceOutcomeCode
			,target.CarerSupportIndicator = source.CarerSupportIndicator
			,target.CasenoteNo = source.CasenoteNo
			,target.CensusDate = source.CensusDate
			,target.ClinicCode = source.ClinicCode
			,target.ClinicFunctionCode = source.ClinicFunctionCode
			,target.CommissionerCode = source.CommissionerCode
			,target.CommissionerReferenceNo = source.CommissionerReferenceNo
			,target.CommissioningSerialNo = source.CommissioningSerialNo
			,target.ContactChangeDate = source.ContactChangeDate
			,target.ContactChangeTime = source.ContactChangeTime
			,target.ContactCreationDate = source.ContactCreationDate
			,target.ContextCode = source.ContextCode
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.DepartmentCode = source.DepartmentCode
			,target.DistrictNo = source.DistrictNo
			,target.DistrictNoOrganisationCode = source.DistrictNoOrganisationCode
			,target.DNACode = source.DNACode
			,target.EarliestReasonableOfferDate = source.EarliestReasonableOfferDate
			,target.EncounterRecno = source.EncounterRecno
			,target.EpisodeChangeDate = source.EpisodeChangeDate
			,target.EpisodeChangeTime = source.EpisodeChangeTime
			,target.EthnicCategoryCode = source.EthnicCategoryCode
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.FirstAttendanceCode = source.FirstAttendanceCode
			,target.HRGVersionCode = source.HRGVersionCode
			,target.ICDDiagnosisSchemeCode = source.ICDDiagnosisSchemeCode
			,target.IsDeleted = source.IsDeleted
			,target.LastDNAOrCancelledDate = source.LastDNAOrCancelledDate
			,target.LocationClassCode = source.LocationClassCode
			,target.LocationTypeCode = source.LocationTypeCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.MedicalStaffTypeCode = source.MedicalStaffTypeCode
			,target.NHSNumber = source.NHSNumber
			,target.NHSNumberStatusId = source.NHSNumberStatusId
			,target.NHSServiceAgreementLineNo = source.NHSServiceAgreementLineNo
			,target.OPCSProcedureSchemeCode = source.OPCSProcedureSchemeCode
			,target.OperationStatusCode = source.OperationStatusCode
			,target.OriginalPathwayId = source.OriginalPathwayId
			,target.OriginalPathwayIdIssuerCode = source.OriginalPathwayIdIssuerCode
			,target.OriginalRTTEndDate = source.OriginalRTTEndDate
			,target.OriginalRTTStartDate = source.OriginalRTTStartDate
			,target.OriginalRTTStatusCode = source.OriginalRTTStatusCode
			,target.OverseasVisitorCode = source.OverseasVisitorCode
			,target.PASDGVPCode = source.PASDGVPCode
			,target.PASHRGCode = source.PASHRGCode
			,target.PASUpdateDate = source.PASUpdateDate
			,target.PathwayId = source.PathwayId
			,target.PathwayIdIssuerCode = source.PathwayIdIssuerCode
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.PatientTitle = source.PatientTitle
			,target.PCTCode = source.PCTCode
			,target.Postcode = source.Postcode
			,target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
			,target.PrimaryDiagnosisReadCode = source.PrimaryDiagnosisReadCode
			,target.PrimaryProcedureCode = source.PrimaryProcedureCode
			,target.PrimaryProcedureDate = source.PrimaryProcedureDate
			,target.PrimaryProcedureReadCode = source.PrimaryProcedureReadCode
			,target.PriorityTypeCode = source.PriorityTypeCode
			,target.ProviderCode = source.ProviderCode
			,target.ProviderReferenceNo = source.ProviderReferenceNo
			,target.ReadDiagnosisSchemeCode = source.ReadDiagnosisSchemeCode
			,target.ReadProcedureSchemeCode = source.ReadProcedureSchemeCode
			,target.ReferralRequestReceivedDate = source.ReferralRequestReceivedDate
			,target.ReferrerCode = source.ReferrerCode
			,target.ReferrerOrganisationCode = source.ReferrerOrganisationCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.ResidencePCTCode = source.ResidencePCTCode
			,target.RTTEndDate = source.RTTEndDate
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTStatusCode = source.RTTStatusCode
			,target.ServiceTypeRequestedCode = source.ServiceTypeRequestedCode
			,target.SexCode = source.SexCode
			,target.SiteCode = source.SiteCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceUniqueID = source.SourceUniqueID
			,target.TreatmentFunctionCode = source.TreatmentFunctionCode
			,target.TrustNo = source.TrustNo
			,target.UniqueBookingReferenceNo = source.UniqueBookingReferenceNo
			,target.WaitingListCode = source.WaitingListCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.AgeCode = source.AgeCode
			,target.Cases = source.Cases
			,target.WaitTypeCode = source.WaitTypeCode
			,target.StatusCode = source.StatusCode
			,target.OPCSCoded = source.OPCSCoded
			,target.WithRTTStartDate = source.WithRTTStartDate
			,target.WithRTTStatusCode = source.WithRTTStatusCode
			,target.WithExpectedAdmissionDate = source.WithExpectedAdmissionDate
			,target.RTTActivity = source.RTTActivity
			,target.DiagnosticProcedure = source.DiagnosticProcedure
			,target.DirectorateCode = source.DirectorateCode
			,target.DurationAtTCIDateCode = source.DurationAtTCIDateCode
			,target.LengthOfWait = source.LengthOfWait
			,target.DurationCode = source.DurationCode
			,target.PASSpecialtyCode = source.PASSpecialtyCode
			,target.WithRTTOpenPathway = source.WithRTTOpenPathway
			,target.DateOnWaitingList = source.DateOnWaitingList
			,target.OriginalDateOnWaitingList = source.OriginalDateOnWaitingList
			,target.ReferralDate = source.Referraldate
			,target.WithAppointmentDate = source.WithAppointmentDate
			,target.DurationAtAppointmentDateCode = source.DurationAtAppointmentDateCode
			,target.CCGCode = source.CCGCode
			,target.ResidenceCCGCode = source.ResidenceCCGCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


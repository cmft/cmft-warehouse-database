﻿

CREATE procedure [ETL].[BuildCenAPCWLBaseWaitingListEntry]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||PAS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge APCWL.BaseWaitingListEntry target
using ETL.TLoadCenAPCWLBaseWaitingListEntry source
on	source.SourceUniqueID = target.SourceUniqueID

when matched and not
(
	isnull(target.[ActivityDate], getdate()) = isnull(source.[ActivityDate], getdate())
	and isnull(target.[ActivityTime], getdate()) = isnull(source.[ActivityTime], getdate())
	and isnull(target.[CasenoteNumber], '') = isnull(source.[CasenoteNumber], '')
	and isnull(target.[ConsultantCode], '') = isnull(source.[ConsultantCode], '')
	and isnull(target.[DiagnosticGroupCode], '') = isnull(source.[DiagnosticGroupCode], '')
	and isnull(target.[DistrictNo], '') = isnull(source.[DistrictNo], '')
	and isnull(target.[SourceEntityRecno], '') = isnull(source.[SourceEntityRecno], '')
	and isnull(target.[SourcePatientNo], '') = isnull(source.[SourcePatientNo], '')
	and isnull(target.[IntendedPrimaryOperationCode], '') = isnull(source.[IntendedPrimaryOperationCode], '')
	and isnull(target.[ManagementIntentionCode], '') = isnull(source.[ManagementIntentionCode], '')
	and isnull(target.[SiteCode], '') = isnull(source.[SiteCode], '')
	and isnull(target.[AdmissionMethodCode], '') = isnull(source.[AdmissionMethodCode], '')
	and isnull(target.[Operation], '') = isnull(source.[Operation], '')
	and isnull(target.[SpecialtyCode], '') = isnull(source.[SpecialtyCode], '')
	and isnull(target.[WaitingListCode], '') = isnull(source.[WaitingListCode], '')
	and isnull(target.[ContractSerialNumber], '') = isnull(source.[ContractSerialNumber], '')
	and isnull(target.[PurchaserCode], '') = isnull(source.[PurchaserCode], '')
	and isnull(target.[EpisodicGpCode], '') = isnull(source.[EpisodicGpCode], '')
	and isnull(target.[InterfaceCode], '') = isnull(source.[InterfaceCode], '')	
)
	
then update set
	target.[ActivityDate] = source.[ActivityDate]
	,target.[ActivityTime] = source.[ActivityTime]
	,target.[CasenoteNumber] = source.[CasenoteNumber]
	,target.[ConsultantCode] = source.[ConsultantCode]
	,target.[DiagnosticGroupCode] = source.[DiagnosticGroupCode]
	,target.[DistrictNo] = source.[DistrictNo]
	,target.[SourceEntityRecno] = source.[SourceEntityRecno]
	,target.[SourcePatientNo] = source.[SourcePatientNo]
	,target.[IntendedPrimaryOperationCode] = source.[IntendedPrimaryOperationCode]
	,target.[ManagementIntentionCode] = source.[ManagementIntentionCode]
	,target.[SiteCode] = source.[SiteCode]
	,target.[AdmissionMethodCode] = source.[AdmissionMethodCode]
	,target.[Operation] = source.[Operation]
	,target.[SpecialtyCode] = source.[SpecialtyCode]
	,target.[WaitingListCode] = source.[WaitingListCode]
	,target.[ContractSerialNumber] = source.[ContractSerialNumber]
	,target.[PurchaserCode] = source.[PurchaserCode]
	,target.[EpisodicGpCode] = source.[EpisodicGpCode]
	,target.[InterfaceCode] = source.[InterfaceCode]
	,target.ContextCode = @ContextCode	
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	 [SourceUniqueID]
	,[ActivityDate]
	,[ActivityTime]
	,[CasenoteNumber]
	,[ConsultantCode]
	,[DiagnosticGroupCode]
	,[DistrictNo]
	,[SourceEntityRecno]
	,[SourcePatientNo]
	,[IntendedPrimaryOperationCode]
	,[ManagementIntentionCode]
	,[SiteCode]
	,[AdmissionMethodCode]
	,[Operation]
	,[SpecialtyCode]
	,[WaitingListCode]
	,[ContractSerialNumber]
	,[PurchaserCode]
	,[EpisodicGpCode]
	,[InterfaceCode]	
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[SourceUniqueID]
	,source.[ActivityDate]
	,source.[ActivityTime]
	,source.[CasenoteNumber]
	,source.[ConsultantCode]
	,source.[DiagnosticGroupCode]
	,source.[DistrictNo]
	,source.[SourceEntityRecno]
	,source.[SourcePatientNo]
	,source.[IntendedPrimaryOperationCode]
	,source.[ManagementIntentionCode]
	,source.[SiteCode]
	,source.[AdmissionMethodCode]
	,source.[Operation]
	,source.[SpecialtyCode]
	,source.[WaitingListCode]
	,source.[ContractSerialNumber]
	,source.[PurchaserCode]
	,source.[EpisodicGpCode]
	,source.[InterfaceCode]	
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


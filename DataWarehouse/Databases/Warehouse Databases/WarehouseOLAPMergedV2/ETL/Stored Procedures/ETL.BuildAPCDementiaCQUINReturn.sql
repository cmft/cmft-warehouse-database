﻿


CREATE Procedure [ETL].[BuildAPCDementiaCQUINReturn] 
-- Default for daily reporting, if return, enter start and end dates and set report type = 1
      @MonthStartDate datetime = Null
      ,@MonthEndDate datetime  = Null
      ,@ReportType int = 0
      
as


declare
      @StartTime datetime = getdate()
      ,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
      ,@Elapsed int
      ,@Stats varchar(max)
      
  
Declare @LocalStart datetime = coalesce(
                                                      @MonthStartDate
                                                      ,dateadd(month, datediff(month, -1, getdate()) - 2, 0) 
                                                      )
            ,@LocalEnd datetime = coalesce (
                                                      (@MonthEndDate + '23:59')
                                                      ,DATEADD(ss, -1, DATEADD(month, DATEDIFF(month, 0, getdate()), 0)) 
                                                      )
            ,@CurrentMonthStart datetime = dateadd(day,-(DAY(getdate())-1),cast(getdate()as date))
            ,@Census datetime = dateadd(day, 0, datediff(day, 0, getdate()))  

                              
If @ReportType = 0
Begin

      exec ETL.LoadAPCDementiaCQUINMonthly @LocalStart,@LocalEnd


      Truncate table APC.DementiaCQUINCurrent
      Insert into APC.DementiaCQUINCurrent
            (
            ReportingMonth 
            ,GlobalProviderSpellNo
            ,Directorate
            ,Division
            ,LocalAdmissionMethod
            ,AdmissionDate
            ,AdmissionTime
            ,AdmissionWardCode
            ,DischargeDate
            ,DischargeTime
            ,LocalDischargeMethod
            ,PatientSurname
            ,SexCode
            ,NHSNumber
            ,DistrictNo
            ,CasenoteNumber
            ,LatestWard 
            ,LatestSpecialtyCode 
            ,LatestConsultantCode 
            ,AgeOnAdmission 
            ,LoS 
            ,OriginalSubmissionTime 
            ,OriginalResponse 
            ,LatestKnownDementiaRecordedTime
            ,LatestKnownToHaveDementiaResponse  
            ,LatestResponseTime     
            ,LatestResponse
            ,LatestAssessmentTime   
            ,LatestAssessmentScore  
            ,LatestInvestigationTime      
            ,LatestReferralSentDate
            ,LatestReferralRecordedTime
            ,LatestAssessmentNotPerformedReason 
            ,LatestAssessmentNotPerformedRecordedTime
            ,Q1Denominator
            ,Q1Numerator
            ,Q2Denominator
            ,Q2Numerator
            ,Q3Denominator
            ,Q3Numerator
            ,FreezeDate 
            ,ByWhom 
            ,DementiaUpdated
            ,WardUpdated
            )

      Select
            Monthly.ReportingMonth
            ,Monthly.GlobalProviderSpellNo
            ,Directorate
            ,Division
            ,LocalAdmissionMethod
            ,AdmissionDate
            ,Monthly.AdmissionTime
            ,AdmissionWardCode
            ,DischargeDate
            ,Monthly.DischargeTime
            ,LocalDischargeMethod
            ,Monthly.PatientSurname
            ,SexCode
            ,NHSNumber
            ,Monthly.DistrictNo
            ,Monthly.CasenoteNumber
            ,LatestWard 
            ,LatestSpecialtyCode
            ,LatestConsultantCode 
            ,AgeOnAdmission 
            ,LoS
            ,Monthly.OriginalSubmissionTime 
            ,Monthly.OriginalResponse 
            ,Monthly.LatestKnownDementiaRecordedTime
            ,Monthly.LatestKnownToHaveDementiaResponse  
            ,Monthly.LatestResponseTime     
            ,Monthly.LatestResponse
            ,Monthly.LatestAssessmentTime   
            ,Monthly.LatestAssessmentScore  
            ,Monthly.LatestInvestigationTime      
            ,Monthly.LatestReferralSentDate
            ,Monthly.LatestReferralRecordedTime
            ,Monthly.LatestAssessmentNotPerformedReason 
            ,Monthly.LatestAssessmentNotPerformedRecordedTime
            
            ,Q1Denominator = coalesce(ValidatedQ1Denominator ,Monthly.Q1Denominator)
            ,Q1Numerator = coalesce(ValidatedQ1Numerator ,Monthly.Q1Numerator)
            ,Q2Denominator = coalesce(ValidatedQ2Denominator ,Monthly.Q2Denominator)
            ,Q2Numerator = coalesce(ValidatedQ2Numerator ,Monthly.Q2Numerator)
            ,Q3Denominator = coalesce(ValidatedQ3Denominator ,Monthly.Q3Denominator)
            ,Q3Numerator = coalesce(ValidatedQ3Numerator ,Monthly.Q3Numerator)
            ,FreezeDate
            ,ByWhom 
            ,DementiaUpdated
            ,WardUpdated
		from 
			APC.DementiaCQUINMonthly Monthly

		left join APC.DementiaValidation Validate
		on Monthly.ReportingMonth = Validate.ReportingMonth
		and Monthly.GlobalProviderSpellNo = Validate.GlobalProviderSpellNo
		and coalesce(Monthly.Q1Denominator,'') = coalesce(Validate.Q1Denominator,'')
		and coalesce(Monthly.Q1Numerator,'') = coalesce(Validate.Q1Numerator,'')
		and coalesce(Monthly.Q2Denominator,'') = coalesce(Validate.Q2Denominator,'')
		and coalesce(Monthly.Q2Numerator,'') = coalesce(Validate.Q2Numerator,'')
		and coalesce(Monthly.Q3Denominator,'') = coalesce(Validate.Q3Denominator,'')
		and coalesce(Monthly.Q3Numerator,'') = coalesce(Validate.Q3Numerator,'')
	      
      If @LocalStart <> @CurrentMonthStart
      Begin
                        
            exec ETL.LoadAPCDementiaCQUINMonthly @CurrentMonthStart,@Census

            Insert into APC.DementiaCQUINCurrent
                  (
                  ReportingMonth 
                  ,GlobalProviderSpellNo
                  ,Directorate
                  ,Division
                  ,LocalAdmissionMethod
                  ,AdmissionDate
                  ,AdmissionTime
                  ,AdmissionWardCode
                  ,DischargeDate
                  ,DischargeTime
                  ,LocalDischargeMethod
                  ,PatientSurname
                  ,SexCode
                  ,NHSNumber
                  ,DistrictNo
                  ,CasenoteNumber
                  ,LatestWard 
                  ,LatestSpecialtyCode 
                  ,LatestConsultantCode 
                  ,AgeOnAdmission 
                  ,LoS 
                  ,OriginalSubmissionTime 
                  ,OriginalResponse 
                  ,LatestKnownDementiaRecordedTime
                  ,LatestKnownToHaveDementiaResponse  
                  ,LatestResponseTime     
                  ,LatestResponse
                  ,LatestAssessmentTime   
                  ,LatestAssessmentScore  
                  ,LatestInvestigationTime      
                  ,LatestReferralSentDate
                  ,LatestReferralRecordedTime
                  ,LatestAssessmentNotPerformedReason 
                  ,LatestAssessmentNotPerformedRecordedTime
                  ,Q1Denominator
                  ,Q1Numerator
                  ,Q2Denominator
                  ,Q2Numerator
                  ,Q3Denominator
                  ,Q3Numerator
                  ,FreezeDate 
                  ,ByWhom 
                  ,DementiaUpdated
				,WardUpdated
                  )

            Select
                  Monthly.ReportingMonth
                  ,Monthly.GlobalProviderSpellNo
                  ,Directorate
                  ,Division
                  ,LocalAdmissionMethod
                  ,AdmissionDate
                  ,Monthly.AdmissionTime
                  ,AdmissionWardCode
                  ,DischargeDate
                  ,Monthly.DischargeTime
                  ,LocalDischargeMethod
                  ,Monthly.PatientSurname
                  ,SexCode
                  ,NHSNumber
                  ,Monthly.DistrictNo
                  ,Monthly.CasenoteNumber
                  ,LatestWard 
                  ,LatestSpecialtyCode
                  ,LatestConsultantCode 
                  ,AgeOnAdmission 
                  ,LoS
                  ,Monthly.OriginalSubmissionTime 
                  ,Monthly.OriginalResponse 
                  ,Monthly.LatestKnownDementiaRecordedTime
                  ,Monthly.LatestKnownToHaveDementiaResponse  
                  ,Monthly.LatestResponseTime     
                  ,Monthly.LatestResponse
                  ,Monthly.LatestAssessmentTime   
                  ,Monthly.LatestAssessmentScore  
                  ,Monthly.LatestInvestigationTime      
                  ,Monthly.LatestReferralSentDate
                  ,Monthly.LatestReferralRecordedTime
                  ,Monthly.LatestAssessmentNotPerformedReason 
                  ,Monthly.LatestAssessmentNotPerformedRecordedTime
                  
                  ,Q1Denominator = coalesce(ValidatedQ1Denominator ,Monthly.Q1Denominator)
				  ,Q1Numerator = coalesce(ValidatedQ1Numerator ,Monthly.Q1Numerator)
				  ,Q2Denominator = coalesce(ValidatedQ2Denominator ,Monthly.Q2Denominator)
				  ,Q2Numerator = coalesce(ValidatedQ2Numerator ,Monthly.Q2Numerator)
				  ,Q3Denominator = coalesce(ValidatedQ3Denominator ,Monthly.Q3Denominator)
				  ,Q3Numerator = coalesce(ValidatedQ3Numerator ,Monthly.Q3Numerator)
                  ,FreezeDate 
                  ,ByWhom 
                  ,DementiaUpdated
				,WardUpdated
                  
            from 
                  APC.DementiaCQUINMonthly Monthly
            
            left join APC.DementiaValidation Validate
			on Monthly.ReportingMonth = Validate.ReportingMonth
			and Monthly.GlobalProviderSpellNo = Validate.GlobalProviderSpellNo
            and coalesce(Monthly.Q1Denominator,'') = coalesce(Validate.Q1Denominator,'')
		    and coalesce(Monthly.Q1Numerator,'') = coalesce(Validate.Q1Numerator,'')
		    and coalesce(Monthly.Q2Denominator,'') = coalesce(Validate.Q2Denominator,'')
		    and coalesce(Monthly.Q2Numerator,'') = coalesce(Validate.Q2Numerator,'')
		    and coalesce(Monthly.Q3Denominator,'') = coalesce(Validate.Q3Denominator,'')
		    and coalesce(Monthly.Q3Numerator,'') = coalesce(Validate.Q3Numerator,'')
      end

exec ETL.LoadAPCDementiaCQUINValidation

exec ETL.LoadAPCDementiaCurrent

end




If @ReportType = 1
Begin 
      
      exec ETL.LoadAPCDementiaCQUINMonthly @LocalStart,@LocalEnd

      Insert into APC.DementiaCQUINFreeze
            (
            ReportingMonth 
            ,GlobalProviderSpellNo
            ,Directorate
            ,Division
            ,LocalAdmissionMethod
            ,AdmissionDate
            ,AdmissionTime
            ,AdmissionWardCode
            ,DischargeDate
            ,DischargeTime
            ,LocalDischargeMethod
            ,PatientSurname
            ,SexCode
            ,NHSNumber
            ,DistrictNo
            ,CasenoteNumber
            ,LatestWard 
            ,LatestSpecialtyCode 
            ,LatestConsultantCode 
            ,AgeOnAdmission 
            ,LoS 
            ,OriginalSubmissionTime 
            ,OriginalResponse 
            ,LatestKnownDementiaRecordedTime
            ,LatestKnownToHaveDementiaResponse  
            ,LatestResponseTime     
            ,LatestResponse
            ,LatestAssessmentTime   
            ,LatestAssessmentScore  
            ,LatestInvestigationTime      
            ,LatestReferralSentDate
            ,LatestReferralRecordedTime
            ,LatestAssessmentNotPerformedReason 
            ,LatestAssessmentNotPerformedRecordedTime
            ,Q1Denominator
            ,Q1Numerator
            ,Q2Denominator
            ,Q2Numerator
            ,Q3Denominator
            ,Q3Numerator
            ,FreezeDate 
            ,ByWhom 
            ,DementiaUpdated
            ,WardUpdated
            )

      Select
            Monthly.ReportingMonth
            ,Monthly.GlobalProviderSpellNo
            ,Monthly.Directorate
            ,Monthly.Division
            ,Monthly.LocalAdmissionMethod
            ,Monthly.AdmissionDate
            ,Monthly.AdmissionTime
            ,AdmissionWardCode
            ,DischargeDate
            ,Monthly.DischargeTime
            ,LocalDischargeMethod
            ,Monthly.PatientSurname
            ,SexCode
            ,NHSNumber
            ,Monthly.DistrictNo
            ,Monthly.CasenoteNumber
            ,LatestWard 
            ,LatestSpecialtyCode
            ,LatestConsultantCode 
            ,AgeOnAdmission 
            ,LoS
            ,Monthly.OriginalSubmissionTime 
            ,Monthly.OriginalResponse 
            ,Monthly.LatestKnownDementiaRecordedTime
            ,Monthly.LatestKnownToHaveDementiaResponse  
            ,Monthly.LatestResponseTime     
            ,Monthly.LatestResponse
            ,Monthly.LatestAssessmentTime   
            ,Monthly.LatestAssessmentScore  
            ,Monthly.LatestInvestigationTime      
            ,Monthly.LatestReferralSentDate
            ,Monthly.LatestReferralRecordedTime
            ,Monthly.LatestAssessmentNotPerformedReason 
            ,Monthly.LatestAssessmentNotPerformedRecordedTime
            
            ,Q1Denominator = coalesce(ValidatedQ1Denominator ,Monthly.Q1Denominator)
            ,Q1Numerator = coalesce(ValidatedQ1Numerator ,Monthly.Q1Numerator)
            ,Q2Denominator = coalesce(ValidatedQ2Denominator ,Monthly.Q2Denominator)
            ,Q2Numerator = coalesce(ValidatedQ2Numerator ,Monthly.Q2Numerator)
            ,Q3Denominator = coalesce(ValidatedQ3Denominator ,Monthly.Q3Denominator)
            ,Q3Numerator = coalesce(ValidatedQ3Numerator ,Monthly.Q3Numerator)
            ,FreezeDate 
            ,ByWhom 
            ,DementiaUpdated
            ,WardUpdated
      from 
            APC.DementiaCQUINMonthly Monthly
      
      left join APC.DementiaValidation Validate
	  on Monthly.ReportingMonth = Validate.ReportingMonth
	  and Monthly.GlobalProviderSpellNo = Validate.GlobalProviderSpellNo
      and coalesce(Monthly.Q1Denominator,'') = coalesce(Validate.Q1Denominator,'')
	  and coalesce(Monthly.Q1Numerator,'') = coalesce(Validate.Q1Numerator,'')
	  and coalesce(Monthly.Q2Denominator,'') = coalesce(Validate.Q2Denominator,'')
	  and coalesce(Monthly.Q2Numerator,'') = coalesce(Validate.Q2Numerator,'')
	  and coalesce(Monthly.Q3Denominator,'') = coalesce(Validate.Q3Denominator,'')
	  and coalesce(Monthly.Q3Numerator,'') = coalesce(Validate.Q3Numerator,'')
end


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
      @Stats =
            'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
      @ProcedureName
      ,@Stats



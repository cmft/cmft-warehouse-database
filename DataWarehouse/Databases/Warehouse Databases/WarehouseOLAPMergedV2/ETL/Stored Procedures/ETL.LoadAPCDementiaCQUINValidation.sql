﻿

CREATE Procedure [ETL].[LoadAPCDementiaCQUINValidation]

as

-- bring APC admissions and potential Q2 and Q3 into a static table, to be run as part of the job.

Truncate table APC.DementiaCQUINValidation
Insert into APC.DementiaCQUINValidation
	(
	GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,PatientSurname
	,Sex
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard 
	,LatestSpecialtyCode 
	,LatestConsultantCode 
	,AgeOnAdmission
	,[LoS(hrs)]
	,Admission
	,DementiaUpdated
	,WardUpdated
	)
Select	
	Encounter.GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,PatientSurname
	,NationalSex
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard = WardStay.WardCode
	,LatestSpecialtyCode = SpecialtyCode
	,LatestConsultantCode = ConsultantCode
	,AgeOnAdmission = 
		dbo.f_GetAge (DateOfBirth, AdmissionDate)
	,[LoS(hrs)] = cast((datediff(mi,AdmissionTime,getdate())/60) as int)
	,Admission = 1	
	,DementiaUpdated = Dementia.Updated
	,WardUpdated = WardStay.Updated
from 
	APC.Encounter 

inner join APC.BaseWardStay WardStay
on Encounter.ProviderSpellNo = WardStay.ProviderSpellNo
and not exists
	(
	select
		1
	from 
		APC.BaseWardStay Latest
	where 
		Latest.ProviderSpellNo = WardStay.ProviderSpellNo
	and	Latest.StartTime > WardStay.StartTime
	)

inner join APC.AdmissionMethod
on Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   
				
inner join APC.PatientClassification
on Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID 

inner join WH.Sex
on Encounter.SexID = Sex.SourceSexID

left join APC.BaseDementia Dementia
on Encounter.GlobalProviderSpellNo = Dementia.GlobalProviderSpellNo

left join WH.Directorate
on Directorate.DirectorateCode = coalesce(EndDirectorateCode,StartDirectorateCode)

where	
	DischargeDate is null
and OriginalResponse is null
and dbo.f_GetAge (DateOfBirth, AdmissionDate)>=75 
and not exists -- Spell level count, latest event to get Latest ward, etc
		(
		Select
			1
		from
			APC.Encounter Latest
		where
			Latest.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
		and Latest.GlobalEpisodeNo > Encounter.GlobalEpisodeNo
		)
and DateOfDeath is null 
and	AdmissionMethod.AdmissionType = 'Emergency' -- Non Elective admissions only
and NationalAdmissionMethodCode <> '81' -- Exclude Transfers
and PatientClassification.NationalPatientClassificationCode not in ('3','4') -- Exclude Regular admissions 


Insert into APC.DementiaCQUINValidation
	(
	GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,PatientSurname
	,Sex
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard
	,LatestSpecialtyCode 
	,LatestConsultantCode
	,AgeOnAdmission 
	,LoS 
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	,Assessment 
	,Referral 
	,DementiaUpdated 
	,WardUpdated 
	)
Select	
	Encounter.GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,PatientSurname
	,Sex = Sex.NationalSex
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard = WardStay.WardCode
	,LatestSpecialtyCode = SpecialtyCode
	,LatestConsultantCode = ConsultantCode
	,AgeOnAdmission = 
		dbo.f_GetAge (DateOfBirth, AdmissionDate)
	,LoS = 
		datediff(day,AdmissionDate,coalesce(DischargeDate,getdate()))
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	,Assessment = 
		case
			when LatestAssessmentTime is null then 1
			when LatestAssessmentScore <=7
				and LatestInvestigationTime is null then 1
			else 0
		end
	,Referral = 
		case
			when LatestAssessmentScore <=7
			and LatestReferralRecordedTime is null then 1
			else 0
		end
	,DementiaUpdated = Dementia.Updated
	,WardUpdated = WardStay.Updated

from 
	APC.Encounter 

inner join APC.AdmissionMethod
on Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   
				
inner join APC.PatientClassification
on Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID 

inner join APC.BaseDementia  Dementia
on Encounter.GlobalProviderSpellNo = Dementia.GlobalProviderSpellNo

inner join WH.Sex
on Encounter.SexID = Sex.SourceSexID

left join APC.BaseWardStay WardStay
on Encounter.ProviderSpellNo = WardStay.ProviderSpellNo
and not exists
	(
	select
		1
	from 
		APC.BaseWardStay Latest
	where 
		Latest.ProviderSpellNo = WardStay.ProviderSpellNo
	and	Latest.StartTime > WardStay.StartTime
	)

where 
	DischargeDate is null
and LatestResponse = 1
and LatestKnownToHaveDementiaResponse = 0
and 
	(
		LatestAssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
	or 
		LatestAssessmentNotPerformedReason  is null
	)
and	
	(
		LatestAssessmentTime is null
	or 
		LatestInvestigationTime is null
	or 
		LatestReferralRecordedTime is null
	)
	
and dbo.f_GetAge (DateOfBirth, AdmissionDate)>=75 -- Patients 75yrs and over on Admission
and	datediff(mi,AdmissionTime,coalesce(DischargeTime,getdate())) >4320 -- LoS >72hrs
and not exists
		(
		Select
			1
		from
			APC.Encounter Latest
		where
			Latest.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
		and Latest.GlobalEpisodeNo > Encounter.GlobalEpisodeNo
		)
and DateOfDeath is null 
and	AdmissionMethod.AdmissionType = 'Emergency' -- Non Elective admissions only
and NationalAdmissionMethodCode <> '81' -- Exclude Transfers
and PatientClassification.NationalPatientClassificationCode not in ('3','4') -- Exclude Regular admissions 


﻿

CREATE procedure [ETL].[BuildAPCBaseCriticalCarePeriodReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseCriticalCarePeriodReference
			where
				BaseCriticalCarePeriodReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseCriticalCarePeriodReference
where
	not exists
	(
	select
		1
	from
		APC.BaseCriticalCarePeriod
	where
		BaseCriticalCarePeriod.MergeEncounterRecno = BaseCriticalCarePeriodReference.MergeEncounterRecno
	)
and	BaseCriticalCarePeriodReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseCriticalCarePeriodReference target
using
	(
	select
		 Encounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,SiteID = Site.SourceSiteID
		,WardID = Ward.SourceWardID
		,TreatmentFunctionID = Specialty.SourceSpecialtyID

		,StartDateID =
			coalesce(
				 StartDate.DateID

				,case
				when Encounter.StartDate is null
				then NullDate.DateID

				when Encounter.StartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,EndDateID =
			coalesce(
				 EndDate.DateID

				,case
				when Encounter.EndDate is null
				then NullDate.DateID

				when Encounter.EndDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,Encounter.Created
		,Encounter.ByWhom
	from
		APC.BaseCriticalCarePeriod Encounter

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	left outer join APC.Ward Ward
	on	Ward.SourceWardCode = coalesce(Encounter.WardCode, '-1')
	and	Ward.SourceContextCode = Encounter.ContextCode

	left outer join WH.Site Site
	on	Site.SourceSiteCode = coalesce(Encounter.SiteCode, '-1')
	and	Site.SourceContextCode = Encounter.ContextCode

	left outer join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(Encounter.TreatmentFunctionCode, '-1')
	and	Specialty.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar StartDate
	on	StartDate.TheDate = Encounter.StartDate

	left join WH.Calendar EndDate
	on	EndDate.TheDate = Encounter.EndDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		Encounter.Created > @LastUpdated
	and	Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,SiteID
			,WardID
			,TreatmentFunctionID
			,StartDateID
			,EndDateID
			,Created
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.SiteID
			,source.WardID
			,source.TreatmentFunctionID
			,source.StartDateID
			,source.EndDateID
			,source.Created
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Created = source.Created
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.SiteID = source.SiteID
			,target.WardID = source.WardID
			,target.TreatmentFunctionID  = source.TreatmentFunctionID
			,target.StartDateID  = source.StartDateID
			,target.EndDateID  = source.EndDateID
			,target.Created = source.Created
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



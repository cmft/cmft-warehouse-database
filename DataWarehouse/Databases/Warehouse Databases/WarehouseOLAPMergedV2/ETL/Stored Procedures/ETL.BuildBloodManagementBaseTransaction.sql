﻿


CREATE procedure [ETL].[BuildBloodManagementBaseTransaction]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge BloodManagement.BaseTransaction target
using 
	(
	select
		TransactionRecno
		,TransactionID
		,TransactionTypeID
		,TransactionTime 
		,ConditionID
		,BloodUniqueID 
		,DonorNumber 
		,BloodProductID
		,BloodProductVolume 
		,BloodGroupID
		,ReservationTime
		,ExpiryTime
		,FateID 
		,SiteID 
		,LocationID
		,PatientID
		,HospitalNumber
		,NHSNumber 
		,LastName
		,FirstName
		,DateOfBirth 
		,GenderCode
		,SystemUserID
		,InterfaceCode 
		,ContextCode 
		,Created
		,Updated
		,ByWhom
	from
		ETL.TLoadBloodManagementBaseTransaction
	) source
	on	source.TransactionRecno = target.TransactionRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CMFT||BLDTRK'
	then delete

	when not matched 
	then
		insert
			(
			TransactionRecno
			,TransactionID
			,TransactionTypeID
			,TransactionTime 
			,ConditionID
			,BloodUniqueID 
			,DonorNumber 
			,BloodProductID
			,BloodProductVolume 
			,BloodGroupID
			,ReservationTime
			,ExpiryTime
			,FateID 
			,SiteID 
			,LocationID
			,PatientID
			,HospitalNumber
			,NHSNumber 
			,LastName
			,FirstName
			,DateOfBirth 
			,GenderCode 
			,SystemUserID
			,InterfaceCode 
			,ContextCode 
			,Created
			,Updated
			,ByWhom
			)

		values
			(
			source.TransactionRecno
			,source.TransactionID
			,source.TransactionTypeID
			,source.TransactionTime 
			,source.ConditionID
			,source.BloodUniqueID 
			,source.DonorNumber 
			,source.BloodProductID
			,source.BloodProductVolume 
			,source.BloodGroupID
			,source.ReservationTime
			,source.ExpiryTime
			,source.FateID 
			,source.SiteID 
			,source.LocationID
			,source.PatientID
			,source.HospitalNumber
			,source.NHSNumber 
			,source.LastName
			,source.FirstName
			,source.DateOfBirth 
			,source.GenderCode 
			,source.SystemUserID
			,source.InterfaceCode
			,source.ContextCode 
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.TransactionTypeID, 0) = isnull(source.TransactionTypeID, 0)
		and isnull(target.TransactionTime, getdate()) = isnull(source.TransactionTime, getdate())
		and isnull(target.ConditionID, 0) = isnull(source.ConditionID,0)  
		and isnull(target.BloodUniqueID, '') = isnull(source.BloodUniqueID, '')  
		and isnull(target.DonorNumber, '') = isnull(source.DonorNumber, '')
		and isnull(target.BloodProductID, '')  = isnull(source.BloodProductID, '') 
		and isnull(target.BloodProductVolume, 0)  = isnull(source.BloodProductVolume, 0) 
		and isnull(target.BloodGroupID, 0) = isnull(source.BloodGroupID, 0)
		and isnull(target.ReservationTime, getdate()) = isnull(source.ReservationTime, getdate())
		and isnull(target.ExpiryTime, getdate()) = isnull(source.ExpiryTime, getdate())
		and isnull(target.FateID, 0) = isnull(source.FateID,0)
		and isnull(target.SiteID, 0)  = isnull(source.SiteID, 0)  
		and isnull(target.LocationID, 0)  = isnull(source.LocationID, 0)  
		and isnull(target.PatientID, 0) = isnull(source.PatientID, 0)
		and isnull(target.HospitalNumber, '')  = isnull(source.HospitalNumber, '')  
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.LastName, '') = isnull(source.LastName, '')
		and isnull(target.FirstName, '') = isnull(source.FirstName, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.GenderCode, 0) = isnull(source.GenderCode, 0)
		and isnull(target.SystemUserID, 0)  = isnull(source.SystemUserID, 0)  
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	
	then 
		update 
		set
			target.TransactionTypeID = source.TransactionTypeID
			,target.TransactionTime = source.TransactionTime
			,target.ConditionID = source.ConditionID
			,target.BloodUniqueID = source.BloodUniqueID
			,target.DonorNumber = source.DonorNumber
			,target.BloodProductID = source.BloodProductID
			,target.BloodProductVolume = source.BloodProductVolume
			,target.BloodGroupID = source.BloodGroupID
			,target.ReservationTime = source.ReservationTime
			,target.ExpiryTime = source.ExpiryTime
			,target.FateID = source.FateID
			,target.SiteID = source.SiteID
			,target.LocationID = source.LocationID
			,target.PatientID = source.PatientID
			,target.HospitalNumber = source.HospitalNumber
			,target.NHSNumber = source.NHSNumber
			,target.LastName = source.LastName
			,target.FirstName = source.FirstName
			,target.DateOfBirth = source.DateOfBirth
			,target.GenderCode = source.GenderCode
			,target.SystemUserID = source.SystemUserID
			,target.InterfaceCode = source.InterfaceCode
			,target.Created = source.Created
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime




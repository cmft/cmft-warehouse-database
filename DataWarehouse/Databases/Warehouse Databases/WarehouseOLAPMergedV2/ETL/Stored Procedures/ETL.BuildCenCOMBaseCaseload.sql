﻿CREATE procedure [ETL].[BuildCenCOMBaseCaseload]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	COM.BaseCaseload target
using
	(
	select
		 CaseLoadID
		,ReferralID
		,RoleTypeID
		,StatusID
		,AllocationTime
		,AllocationDate
		,AllocationReasonID
		,InterventionlevelID
		,DischargeTime
		,DischargeDate
		,Outcome
		,AllocationProfessionalCarerID
		,AllocationSpecialtyID
		,AllocationStaffTeamID
		,ResponsibleHealthOrganisation
		,DischargeReasonID
		,PatientSourceID
		,PatientSourceSystemUniqueID
		,PatientNHSNumber
		,PatientNHSNumberStatusIndicator
		,PatientTitleID
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,PatientPostcode
		,PatientDateOfBirth
		,PatientDateOfDeath
		,PatientSexID
		,PatientEthnicGroupID
		,PatientCurrentRegisteredPracticeCode
		,PatientEncounterRegisteredPracticeCode
		,PatientCurrentRegisteredGPCode
		,PatientEncounterRegisteredGPCode
		,CreatedTime
		,ModifiedTime
		,CreatedByID
		,ModifiedByID
		,ArchiveFlag
		,AgeCode
		,HealthOrgOwnerID
		,Created
		,Updated
		,ByWhom
		,ContextCode
	from
		ETL.TLoadCenCOMBaseCaseload Caseload
	) source
	on	source.ContextCode = target.ContextCode
	and	source.CaseLoadID = target.CaseloadID

	when not matched by source
	and	target.ContextCode = 'CEN||IPM'
	then delete

	when not matched
	then
		insert
			(
			 CaseloadID
			,ReferralID
			,RoleTypeID
			,StatusID
			,AllocationTime
			,AllocationDate
			,AllocationReasonID
			,InterventionlevelID
			,DischargeTime
			,DischargeDate
			,Outcome
			,AllocationProfessionalCarerID
			,AllocationSpecialtyID
			,AllocationStaffTeamID
			,ResponsibleHealthOrganisation
			,DischargeReasonID
			,PatientSourceID
			,PatientSourceSystemUniqueID
			,PatientNHSNumber
			,PatientNHSNumberStatusIndicator
			,PatientTitleID
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,PatientPostcode
			,PatientDateOfBirth
			,PatientDateOfDeath
			,PatientSexID
			,PatientEthnicGroupID
			,PatientCurrentRegisteredPracticeCode
			,PatientEncounterRegisteredPracticeCode
			,PatientCurrentRegisteredGPCode
			,PatientEncounterRegisteredGPCode
			,CreatedTime
			,ModifiedTime
			,CreatedByID
			,ModifiedByID
			,ArchiveFlag
			,AgeCode
			,HealthOrgOwnerID
			,Created
			,Updated
			,ByWhom
			,ContextCode
			)
		values
			(
			 source.CaseLoadID
			,source.ReferralID
			,source.RoleTypeID
			,source.StatusID
			,source.AllocationTime
			,source.AllocationDate
			,source.AllocationReasonID
			,source.InterventionlevelID
			,source.DischargeTime
			,source.DischargeDate
			,source.Outcome
			,source.AllocationProfessionalCarerID
			,source.AllocationSpecialtyID
			,source.AllocationStaffTeamID
			,source.ResponsibleHealthOrganisation
			,source.DischargeReasonID
			,source.PatientSourceID
			,source.PatientSourceSystemUniqueID
			,source.PatientNHSNumber
			,source.PatientNHSNumberStatusIndicator
			,source.PatientTitleID
			,source.PatientForename
			,source.PatientSurname
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.PatientPostcode
			,source.PatientDateOfBirth
			,source.PatientDateOfDeath
			,source.PatientSexID
			,source.PatientEthnicGroupID
			,source.PatientCurrentRegisteredPracticeCode
			,source.PatientEncounterRegisteredPracticeCode
			,source.PatientCurrentRegisteredGPCode
			,source.PatientEncounterRegisteredGPCode
			,source.CreatedTime
			,source.ModifiedTime
			,source.CreatedByID
			,source.ModifiedByID
			,source.ArchiveFlag
			,source.AgeCode
			,source.HealthOrgOwnerID
			,source.Created
			,source.Updated
			,source.ByWhom
			,source.ContextCode
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.ReferralID = source.ReferralID
			,target.RoleTypeID = source.RoleTypeID
			,target.StatusID = source.StatusID
			,target.AllocationTime = source.AllocationTime
			,target.AllocationDate = source.AllocationDate
			,target.AllocationReasonID = source.AllocationReasonID
			,target.InterventionlevelID = source.InterventionlevelID
			,target.DischargeTime = source.DischargeTime
			,target.DischargeDate = source.DischargeDate
			,target.Outcome = source.Outcome
			,target.AllocationProfessionalCarerID = source.AllocationProfessionalCarerID
			,target.AllocationSpecialtyID = source.AllocationSpecialtyID
			,target.AllocationStaffTeamID = source.AllocationStaffTeamID
			,target.ResponsibleHealthOrganisation = source.ResponsibleHealthOrganisation
			,target.DischargeReasonID = source.DischargeReasonID
			,target.PatientSourceID = source.PatientSourceID
			,target.PatientSourceSystemUniqueID = source.PatientSourceSystemUniqueID
			,target.PatientNHSNumber = source.PatientNHSNumber
			,target.PatientNHSNumberStatusIndicator = source.PatientNHSNumberStatusIndicator
			,target.PatientTitleID = source.PatientTitleID
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.PatientPostcode = source.PatientPostcode
			,target.PatientDateOfBirth = source.PatientDateOfBirth
			,target.PatientDateOfDeath = source.PatientDateOfDeath
			,target.PatientSexID = source.PatientSexID
			,target.PatientEthnicGroupID = source.PatientEthnicGroupID
			,target.PatientCurrentRegisteredPracticeCode = source.PatientCurrentRegisteredPracticeCode
			,target.PatientEncounterRegisteredPracticeCode = source.PatientEncounterRegisteredPracticeCode
			,target.PatientCurrentRegisteredGPCode = source.PatientCurrentRegisteredGPCode
			,target.PatientEncounterRegisteredGPCode = source.PatientEncounterRegisteredGPCode
			,target.CreatedTime = source.CreatedTime
			,target.ModifiedTime = source.ModifiedTime
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.ArchiveFlag = source.ArchiveFlag
			,target.AgeCode = source.AgeCode
			,target.HealthOrgOwnerID = source.HealthOrgOwnerID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

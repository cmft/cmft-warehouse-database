﻿

CREATE procedure [ETL].[BuildMortalityBaseReviewReference] --'CMFT||MORT' / 'CMFT||MORTARCHV'
      @ContextCode varchar(20)
as

declare
      @StartTime datetime = getdate()
      ,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
      ,@Elapsed int
      ,@Stats varchar(max)

declare
      @deleted int
      ,@inserted int
      ,@updated int

declare
      @MergeSummary TABLE(Action nvarchar(10));

declare
      @ContextID int =
            (
            select
                 Context.ContextID
            from
                  WH.Context 
            where
                  Context.ContextCode = @ContextCode
            )

declare
      @LastUpdated datetime =
            coalesce(
                  (
                  select
                        MAX(Updated)
                  from
                        Mortality.BaseReviewReference
                  where
                        BaseReviewReference.ContextID = @ContextID
                  )
                  ,'1 jan 1900'
            )


--more efficient to delete here than in the merge
delete
from
      Mortality.BaseReviewReference
where
      not exists
      (
      select
            1
      from
            Mortality.BaseReview
      where
            BaseReview.MergeRecno = BaseReviewReference.MergeRecno
      )
and   BaseReviewReference.ContextID = @ContextID 

select
      @deleted = @@ROWCOUNT

-- Did set up the following in the Source but it was taking too long when introducing the join to Question and Response.  Time improved significantly when creating the temp table and using this in the source to link to Response.
select 
	BaseReview.MergeRecno
	,ContextID = Context.ContextID
	,FormTypeID = FormType.SourceFormTypeID
	,ReviewStatusID = ReviewStatus.SourceReviewStatusID
	,SystemUserID = SystemUser.SystemUserID
	,QuestionID = Question.QuestionID
	--,ResponseID = Response.ResponseID
	,ClassificationGradeID = ClassificationGrade.ClassificationGradeID
	
	,BaseReview.Response 
	,Question.YesNoResponse 
	 
	,BaseReview.Created
	,BaseReview.Updated
	,BaseReview.ByWhom
into 
	#Mortality
from
	Mortality.BaseReview

inner join WH.Context
on    Context.ContextCode = BaseReview.ContextCode

left join Mortality.FormType
on    FormType.SourceFormTypeCode = cast(coalesce(BaseReview.FormTypeCode, '-1') as varchar)
and   FormType.SourceContextCode = BaseReview.ContextCode

left join Mortality.ReviewStatus
on    ReviewStatus.SourceReviewStatusCode = BaseReview.ReviewStatusCode
and   ReviewStatus.SourceContextCode = BaseReview.ContextCode

left join WH.[SystemUser] 
on    [SystemUser].UserID = cast(coalesce(BaseReview.PrimaryReviewerCode, '-1') as varchar)
and   [SystemUser].ContextCode = BaseReview.ContextCode

left join Mortality.ClassificationGrade
on ClassificationGrade.ClassificationGradeCode = coalesce(BaseReview.ClassificationGrade ,'N/A')

left join Mortality.Question
on Question.Question = coalesce(BaseReview.Question,'ReviewNotStarted')
and Question.QuestionNo = BaseReview.QuestionNo
and Question.FormTypeID = FormType.SourceFormTypeID

where
	( 
		BaseReview.Updated > @LastUpdated
	or 
		@LastUpdated is null
	)
and   BaseReview.ContextCode = @ContextCode


merge
      Mortality.BaseReviewReference target
using
	(
    select 
		MergeRecno
		,ContextID
		,FormTypeID
		,ReviewStatusID
		,SystemUserID
		,QuestionID 
		,ResponseID = Response.ResponseID
		,ClassificationGradeID 
		,Created
		,Updated
		,ByWhom
	from 
		#Mortality
		      
	left join Mortality.Response
	on Response.Response = 
		case
			when #Mortality.Response = '' and #Mortality.YesNoResponse = 0 then 'No Response'
			when #Mortality.Response = '' and #Mortality.YesNoResponse = 1 then 'No Response YN'
			when #Mortality.Response = '0' and #Mortality.YesNoResponse = 1  then 'No'
			when #Mortality.Response = '1' and #Mortality.YesNoResponse = 1 then 'Yes'
			else coalesce(left(#Mortality.Response,4096),'Review Not Started')
		end
	and Response.YesNoResponse = #Mortality.YesNoResponse

      ) source
      on    source.MergeRecno = target.MergeRecno

      when not matched
      then
            insert
				(
				MergeRecno
				,ContextID
				,FormTypeID
				,ReviewStatusID 
				,SystemUserID 
				,QuestionID
				,ResponseID
				,ClassificationGradeID

				,Created
				,Updated
				,ByWhom
				)
            values
                (
				 source.MergeRecno
				,source.ContextID
				,source.FormTypeID
				,source.ReviewStatusID 
				,source.SystemUserID 
				,source.QuestionID
				,source.ResponseID
				,source.ClassificationGradeID

				,source.Created
				,source.Updated
				,source.ByWhom
				)

      when matched
      and not
            (
				isnull(target.ContextID, 0) = isnull(source.ContextID, 0)
			and isnull(target.FormTypeID, 0) = isnull(source.FormTypeID, 0)
			and isnull(target.ReviewStatusID, 0) = isnull(source.ReviewStatusID, 0)
			and isnull(target.SystemUserID, 0) = isnull(source.SystemUserID,0)  
			and isnull(target.QuestionID, 0) = isnull(source.QuestionID, 0)  
			and isnull(target.ResponseID, 0) = isnull(source.ResponseID, 0)
			and isnull(target.ClassificationGradeID, 0)  = isnull(source.ClassificationGradeID, 0) 
			and isnull(target.Updated, getdate())  = isnull(source.Updated, getdate()) 
			and isnull(target.ByWhom, '')  = isnull(source.ByWhom, '') 
		    )
      then
            update
            set
                  target.MergeRecno = source.MergeRecno
                  ,target.ContextID = source.ContextID
                  ,target.FormTypeID = source.FormTypeID
                  ,target.ReviewStatusID = source.ReviewStatusID
                  ,target.SystemUserID = source.SystemUserID
                  ,target.QuestionID = source.QuestionID
                  ,target.ResponseID = source.ResponseID
                  ,target.ClassificationGradeID = source.ClassificationGradeID
                  ,target.Updated = source.Updated
                  ,target.ByWhom = source.ByWhom
                
                  
            
output
      $action into @MergeSummary
;


select
      @inserted = sum(Inserted)
      ,@updated = sum(Updated)
from
      (
      select
            Inserted = case when Action = 'INSERT' then 1 else 0 end
            ,Updated = case when Action = 'UPDATE' then 1 else 0 end
      from
            @MergeSummary
      ) MergeSummary

select
      @Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
      @Stats =
            'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
            ', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
            ', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
            ', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
      @ProcedureName
      ,@Stats




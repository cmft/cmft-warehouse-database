﻿
--USE [WarehouseOLAPMergedV2]

CREATE Procedure [ETL].[LoadAPCBaseDementia]
as


select
	DementiaRecno = DementiaRecNo
	,Dementia.GlobalProviderSpellNo
	,ProviderSpellNo
	,SpellID
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,AdmissionWardCode
	,InterfaceCode
	,OriginalSubmissionTime = Original.RecordedTime
	,OriginalResponse = 
		case
			when LatestKnownDementia.KnownToHaveDementia = 1 then 'DementiaKnown'
			when Original.ForgetfulnessQuestionAnswered = 1 then 'AnswerYes'
			when Original.ForgetfulnessQuestionAnswered = 0 then 'AnswerNo'
			else null
		end
	,LatestKnownDementiaRecordedTime = LatestKnownDementia.RecordedTime
	,LatestKnownToHaveDementiaResponse = LatestKnownDementia.KnownToHaveDementia
	,LatestResponseTime =LatestResponse.RecordedTime
	,LatestResponse = LatestResponse.ForgetfulnessQuestionAnswered
	,LatestAssessmentTime = LatestAssessment.RecordedTime
	,LatestAssessmentScore = LatestAssessment.AssessmentScore
	,LatestInvestigationTime = LatestInvestigation.RecordedTime
	,LatestReferralSentDate = Referral.ReferralSentDate
	,LatestReferralRecordedTime = Referral.RecordedTime
	,LatestAssessmentNotPerformedReason = AssessmentNotPerformed.AssessmentNotPerformedReason
	,LatestAssessmentNotPerformedRecordedTime = AssessmentNotPerformed.RecordedTime
	,ContextCode
	,Created
	,Updated
	,ByWhom
into
	#Dementia
from 
	ETL.TLoadAPCBaseDementia Dementia

left join	
	(
	Select
		GlobalProviderSpellNo
		,RecordedTime
		,ForgetfulnessQuestionAnswered 
	from 
		ETL.TLoadAPCBaseDementia 
	where 
		ForgetfulnessQuestionAnswered in (0,1)
	and not exists
			(
			select
				1
			from
				ETL.TLoadAPCBaseDementia Earliest
			where
				Earliest.GlobalProviderSpellNo = TLoadAPCBaseDementia.GlobalProviderSpellNo
			and Earliest.ForgetfulnessQuestionAnswered in (0,1)
			and Earliest.SequenceNo < TLoadAPCBaseDementia.SequenceNo
			)
	) 
	Original
on Dementia.GlobalProviderSpellNo = Original.GlobalProviderSpellNo

left join	
	(
	Select
		GlobalProviderSpellNo
		,RecordedTime
		,KnownToHaveDementia
	from 
		ETL.TLoadAPCBaseDementia 
	where 
		KnownToHaveDementia in (0,1)
	and not exists
			(
			select
				1
			from
				ETL.TLoadAPCBaseDementia Latest
			where
				Latest.GlobalProviderSpellNo = TLoadAPCBaseDementia.GlobalProviderSpellNo
			and Latest.KnownToHaveDementia in (0,1)
			and Latest.SequenceNo > TLoadAPCBaseDementia.SequenceNo
			)
	) 
	LatestKnownDementia
on Dementia.GlobalProviderSpellNo = LatestKnownDementia.GlobalProviderSpellNo

left join	
	(
	Select
		GlobalProviderSpellNo
		,RecordedTime
		,ForgetfulnessQuestionAnswered
	from 
		ETL.TLoadAPCBaseDementia 
	where 
		ForgetfulnessQuestionAnswered in (0,1)
	and not exists
			(
			select
				1
			from
				ETL.TLoadAPCBaseDementia Latest
			where
				Latest.GlobalProviderSpellNo = TLoadAPCBaseDementia.GlobalProviderSpellNo
			and Latest.ForgetfulnessQuestionAnswered in (0,1)
			and Latest.SequenceNo > TLoadAPCBaseDementia.SequenceNo
			)
	) 
	LatestResponse
on Dementia.GlobalProviderSpellNo = LatestResponse.GlobalProviderSpellNo

left join	
	(
	Select
		GlobalProviderSpellNo
		,RecordedTime
		,AssessmentScore
	from 
		ETL.TLoadAPCBaseDementia 
	where 
		AssessmentScore is not null
	and not exists
			(
			select
				1
			from
				ETL.TLoadAPCBaseDementia Latest
			where
				Latest.GlobalProviderSpellNo = TLoadAPCBaseDementia.GlobalProviderSpellNo
			and Latest.AssessmentScore is not null
			and Latest.SequenceNo > TLoadAPCBaseDementia.SequenceNo
			)
	) 
	LatestAssessment
on Dementia.GlobalProviderSpellNo = LatestAssessment.GlobalProviderSpellNo

left join	
	(
	Select
		GlobalProviderSpellNo
		,RecordedTime
	from 
		ETL.TLoadAPCBaseDementia 
	where 
		Investigation = 1
	and not exists
			(
			select
				1
			from
				ETL.TLoadAPCBaseDementia Latest
			where
				Latest.GlobalProviderSpellNo = TLoadAPCBaseDementia.GlobalProviderSpellNo
			and Latest.Investigation = 1
			and Latest.SequenceNo > TLoadAPCBaseDementia.SequenceNo
			)
	) 
	LatestInvestigation
on Dementia.GlobalProviderSpellNo = LatestInvestigation.GlobalProviderSpellNo

left join	
	(
	Select
		GlobalProviderSpellNo
		,RecordedTime
		,ReferralSentDate
	from 
		ETL.TLoadAPCBaseDementia 
	where 
		ReferralSentDate is not null
	and not exists
			(
			select
				1
			from
				ETL.TLoadAPCBaseDementia Latest
			where
				Latest.GlobalProviderSpellNo = TLoadAPCBaseDementia.GlobalProviderSpellNo
			and Latest.ReferralSentDate is not null
			and Latest.SequenceNo > TLoadAPCBaseDementia.SequenceNo
			)
	) 
	Referral
on Dementia.GlobalProviderSpellNo = Referral.GlobalProviderSpellNo

left join	
	(
	Select
		GlobalProviderSpellNo
		,RecordedTime
		,AssessmentNotPerformedReason
	from 
		ETL.TLoadAPCBaseDementia 
	where 
		AssessmentNotPerformedReason is not null
	and not exists
			(
			select
				1
			from
				ETL.TLoadAPCBaseDementia Latest
			where
				Latest.GlobalProviderSpellNo = TLoadAPCBaseDementia.GlobalProviderSpellNo
			and Latest.AssessmentNotPerformedReason is not null
			and Latest.SequenceNo > TLoadAPCBaseDementia.SequenceNo
			)
	) 
	AssessmentNotPerformed
on Dementia.GlobalProviderSpellNo = AssessmentNotPerformed.GlobalProviderSpellNo

where 
	not exists
			(
			select
				1
			from
				ETL.TLoadAPCBaseDementia Earliest
			where
				Earliest.GlobalProviderSpellNo = Dementia.GlobalProviderSpellNo
			and Earliest.SequenceNo < Dementia.SequenceNo
			)
	
	
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE	(Action nvarchar(10));

merge
	APC.BaseDementia target

using
	(
	select
		 DementiaRecno
		 ,GlobalProviderSpellNo
		,ProviderSpellNo
		,SpellID
		,SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,AdmissionWardCode
		,InterfaceCode
		,OriginalSubmissionTime
		,OriginalResponse
		,LatestKnownDementiaRecordedTime 
		,LatestKnownToHaveDementiaResponse
		,LatestResponseTime 
		,LatestResponse
		,LatestAssessmentTime
		,LatestAssessmentScore
		,LatestInvestigationTime
		,LatestReferralSentDate 
		,LatestReferralRecordedTime
		,LatestAssessmentNotPerformedReason 
		,LatestAssessmentNotPerformedRecordedTime 
		,ContextCode
		,Created
		,Updated
		,ByWhom
	from
		#Dementia
	) source
	on	source.GlobalProviderSpellNo = target.GlobalProviderSpellNo
	
	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			DementiaRecno
			,GlobalProviderSpellNo
			,ProviderSpellNo
			,SpellID
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,AdmissionWardCode
			,InterfaceCode
			,OriginalSubmissionTime
			,OriginalResponse
			,LatestKnownDementiaRecordedTime 
			,LatestKnownToHaveDementiaResponse
			,LatestResponseTime 
			,LatestResponse
			,LatestAssessmentTime
			,LatestAssessmentScore
			,LatestInvestigationTime
			,LatestReferralSentDate 
			,LatestReferralRecordedTime
			,LatestAssessmentNotPerformedReason 
			,LatestAssessmentNotPerformedRecordedTime
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.DementiaRecno
			,source.GlobalProviderSpellNo
			,source.ProviderSpellNo
			,source.SpellID
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.AdmissionWardCode
			,source.InterfaceCode
			,source.OriginalSubmissionTime
			,source.OriginalResponse
			,source.LatestKnownDementiaRecordedTime 
			,source.LatestKnownToHaveDementiaResponse
			,source.LatestResponseTime 
			,source.LatestResponse
			,source.LatestAssessmentTime
			,source.LatestAssessmentScore
			,source.LatestInvestigationTime
			,source.LatestReferralSentDate 
			,source.LatestReferralRecordedTime
			,source.LatestAssessmentNotPerformedReason 
			,source.LatestAssessmentNotPerformedRecordedTime  
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.DementiaRecno, '') = isnull(source.DementiaRecno, '')
		and isnull(target.ProviderSpellNo, '') = isnull(source.ProviderSpellNo, '')
		and isnull(target.SpellID, '') = isnull(source.SpellID, '')
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.AdmissionWardCode, '') = isnull(source.AdmissionWardCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.OriginalSubmissionTime, getdate()) = isnull(source.OriginalSubmissionTime, getdate())
		and isnull(target.OriginalResponse, '') = isnull(source.OriginalResponse, '')
		and isnull(target.LatestKnownDementiaRecordedTime, getdate()) = isnull(source.LatestKnownDementiaRecordedTime, getdate())
		and isnull(target.LatestKnownToHaveDementiaResponse, 0) = isnull(source.LatestKnownToHaveDementiaResponse, 0)
		and isnull(target.LatestResponseTime, getdate()) = isnull(source.LatestResponseTime, getdate())
		and isnull(target.LatestResponse, 0) = isnull(source.LatestResponse, 0)
		and isnull(target.LatestAssessmentTime, getdate()) = isnull(source.LatestAssessmentTime, getdate())
		and isnull(target.LatestAssessmentScore, 0) = isnull(source.LatestAssessmentScore, 0)
		and isnull(target.LatestInvestigationTime, getdate()) = isnull(source.LatestInvestigationTime, getdate())
		and isnull(target.LatestReferralSentDate, getdate()) = isnull(source.LatestReferralSentDate, getdate())
		and isnull(target.LatestReferralRecordedTime, getdate()) = isnull(source.LatestReferralRecordedTime, getdate())
		and isnull(target.LatestAssessmentNotPerformedReason, '') = isnull(source.LatestAssessmentNotPerformedReason, '')
		and isnull(target.LatestAssessmentNotPerformedRecordedTime , getdate()) = isnull(source.LatestAssessmentNotPerformedRecordedTime , getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)

	then
		update
		set
			 target.DementiaRecno = source.DementiaRecno
			,target.ProviderSpellNo = source.ProviderSpellNo
			,target.SpellID = source.SpellID
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.AdmissionWardCode = source.AdmissionWardCode
			,target.InterfaceCode = source.InterfaceCode
			,target.OriginalSubmissionTime = source.OriginalSubmissionTime
			,target.OriginalResponse = source.OriginalResponse
			,target.LatestKnownDementiaRecordedTime = source.LatestKnownDementiaRecordedTime
			,target.LatestKnownToHaveDementiaResponse = source.LatestKnownToHaveDementiaResponse
			,target.LatestResponseTime = source.LatestResponseTime
			,target.LatestResponse = source.LatestResponse
			,target.LatestAssessmentTime = source.LatestAssessmentTime
			,target.LatestAssessmentScore = source.LatestAssessmentScore
			,target.LatestInvestigationTime = source.LatestInvestigationTime
			,target.LatestReferralSentDate = source.LatestReferralSentDate
			,target.LatestReferralRecordedTime = source.LatestReferralRecordedTime
			,target.LatestAssessmentNotPerformedReason = source.LatestAssessmentNotPerformedReason
			,target.LatestAssessmentNotPerformedRecordedTime  = source.LatestAssessmentNotPerformedRecordedTime 
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
	
	
	output $action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

	

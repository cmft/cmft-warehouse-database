﻿
create procedure ETL.BuildAPCWardBase

as

truncate table APC.WardBase

insert
into
	APC.WardBase
(
	 WardCode
	,ContextCode
	,Ward
	,SiteCode
)
select
	 Ward.WardCode
	,ContextCode = 'CEN||PAS'
	,Ward.Ward
	,Ward.SiteCode
from
	[$(Warehouse)].PAS.Ward

union all

select
	 Ward.WardCode
	,ContextCode = 'TRA||UG'
	,Ward.WardName
	,SiteCode = 'RM401'
from
	[$(TraffordWarehouse)].dbo.WardBase Ward


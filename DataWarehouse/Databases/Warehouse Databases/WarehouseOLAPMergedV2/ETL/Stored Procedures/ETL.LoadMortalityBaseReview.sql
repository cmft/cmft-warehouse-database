﻿
CREATE procedure [ETL].[LoadMortalityBaseReview]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Mortality.BaseReview target
using
	(
	select
		 SourceUniqueID
		 ,SourcePatientNo	
		 ,EpisodeStartTime	
		 ,APCContextCode	
		 ,FormTypeCode
		 ,MortalityReviewAdded 	
		 ,ReviewStatusCode	
		 ,PrimaryReviewAssignedTime	
		 ,PrimaryReviewCompletedTime	
		 ,PrimaryReviewerCode	
		 ,SecondaryReviewAssignedTime	
		 ,SecondaryReviewCompletedTime	
		 ,SecondaryReviewerCode	
		 ,ClassificationGrade = cast(ClassificationGrade as varchar)
		 ,RequiresFurtherInvestigation 
		 ,QuestionNo	
		 ,Question	
		 ,Response	
		 ,ContextCode	
		 ,ReviewCheckSum
	from
		ETL.TLoadMortalityBaseReview
	) source
	on	source.SourcePatientNo = target.SourcePatientNo
	and source.FormTypeCode = target.FormTypeCode
	and source.QuestionNo = target.QuestionNo
	and source.ContextCode = target.ContextCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			 ,SourcePatientNo	
			 ,EpisodeStartTime	
			 ,APCContextCode	
			 ,FormTypeCode	
			 ,MortalityReviewAdded 
			 ,ReviewStatusCode	
			 ,PrimaryReviewAssignedTime	
			 ,PrimaryReviewCompletedTime	
			 ,PrimaryReviewerCode	
			 ,SecondaryReviewAssignedTime	
			 ,SecondaryReviewCompletedTime	
			 ,SecondaryReviewerCode	
			 ,ClassificationGrade
			 ,RequiresFurtherInvestigation
			 ,QuestionNo	
			 ,Question	
			 ,Response	
			 ,ContextCode	
			 ,ReviewCheckSum
			 ,Created
			 ,Updated
			 ,ByWhom
			)
		values
			(
			  source.SourceUniqueID
			 ,source.SourcePatientNo	
			 ,source.EpisodeStartTime	
			 ,source.APCContextCode	
			 ,source.FormTypeCode	
			 ,source.MortalityReviewAdded 
			 ,source.ReviewStatusCode	
			 ,source.PrimaryReviewAssignedTime	
			 ,source.PrimaryReviewCompletedTime	
			 ,source.PrimaryReviewerCode	
			 ,source.SecondaryReviewAssignedTime	
			 ,source.SecondaryReviewCompletedTime	
			 ,source.SecondaryReviewerCode	
			 ,source.ClassificationGrade
			 ,source.RequiresFurtherInvestigation
			 ,source.QuestionNo	
			 ,source.Question	
			 ,source.Response	
			 ,source.ContextCode	
			 ,source.ReviewCheckSum
			 ,getdate()
			 ,getdate()
			 ,system_user
			)

	when matched
	and	target.ReviewCheckSum <> source.ReviewCheckSum
	then
		update	
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.APCContextCode = source.APCContextCode
			,target.FormTypeCode = source.FormTypeCode
			,target.MortalityReviewAdded = source.MortalityReviewAdded 
			,target.ReviewStatusCode = source.ReviewStatusCode
			,target.PrimaryReviewAssignedTime = source.PrimaryReviewAssignedTime
			,target.PrimaryReviewCompletedTime = source.PrimaryReviewCompletedTime
			,target.PrimaryReviewerCode = source.PrimaryReviewerCode
			,target.SecondaryReviewAssignedTime = source.SecondaryReviewAssignedTime
			,target.SecondaryReviewCompletedTime = source.SecondaryReviewCompletedTime
			,target.SecondaryReviewerCode = source.SecondaryReviewerCode
			,target.ClassificationGrade = source.ClassificationGrade
			,target.RequiresFurtherInvestigation = source.RequiresFurtherInvestigation
			,target.QuestionNo = source.QuestionNo
			,target.Question = source.Question
			,target.Response = source.Response
			,target.ContextCode = source.ContextCode
			,target.ReviewCheckSum = source.ReviewCheckSum
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



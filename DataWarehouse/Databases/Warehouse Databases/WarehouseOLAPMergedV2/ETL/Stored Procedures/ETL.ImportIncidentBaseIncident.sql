﻿CREATE procedure [ETL].[ImportIncidentBaseIncident] as

--import the data
exec ETL.BuildIncidentBaseIncident


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Incident].[BaseIncident]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildIncidentBaseIncidentReference 'CMFT||ULYSS'


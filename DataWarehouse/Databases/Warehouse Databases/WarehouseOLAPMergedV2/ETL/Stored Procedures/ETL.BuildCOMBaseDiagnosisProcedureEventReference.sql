﻿CREATE procedure ETL.BuildCOMBaseDiagnosisProcedureEventReference
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10))

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				COM.BaseDiagnosisProcedureEventReference
			)
			,'1 jan 1900'
		)
declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


--more efficient to delete here than in the merge
delete
from
	COM.BaseDiagnosisProcedureEventReference
where
	not exists
	(
	select
		1
	from
		COM.BaseDiagnosisProcedureEvent
	where
		BaseDiagnosisProcedureEvent.MergeEncounterRecno = BaseDiagnosisProcedureEventReference.MergeEncounterRecno
	)
and	ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	COM.BaseDiagnosisProcedureEventReference target
using
	(
	select
		 DiagnosisProcedureEvent.MergeEncounterRecno
		,DiagnosisProcedureID = DiagnosisProcedure.SourceValueID
		,Created
		,Updated
		,ByWhom
	from
		COM.BaseDiagnosisProcedureEvent DiagnosisProcedureEvent

	inner join COM.DiagnosisProcedure
	on	DiagnosisProcedure.DiagnosisProcedureID = coalesce(DiagnosisProcedureEvent.DiagnosisProcedureID, -1)
	and	DiagnosisProcedure.ContextCode = DiagnosisProcedureEvent.ContextCode

	where
		DiagnosisProcedureEvent.Updated > @LastUpdated
	and	DiagnosisProcedureEvent.ContextCode = @ContextCode
	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,ContextID
			,DiagnosisProcedureID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,@ContextID
			,source.DiagnosisProcedureID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		and	target.DiagnosisProcedureID = source.DiagnosisProcedureID
		)
	then
		update
		set
			 target.DiagnosisProcedureID = source.DiagnosisProcedureID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

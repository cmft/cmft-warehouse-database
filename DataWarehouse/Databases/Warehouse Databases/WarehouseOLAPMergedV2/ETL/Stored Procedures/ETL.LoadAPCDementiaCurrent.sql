﻿




CREATE Procedure [ETL].[LoadAPCDementiaCurrent]

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	


Truncate table APC.DementiaCurrent
Insert into	APC.DementiaCurrent

Select	
	Encounter.GlobalProviderSpellNo
	,AdmissionType
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,DischargeDate
	,DischargeTime
	,LocalDischargeMethod
	,DateOfDeath
	,PatientSurname
	,SexCode
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard = WardStay.WardCode
	,LatestSpecialtyCode = SpecialtyCode
	,LatestConsultantCode = ConsultantCode
	,AgeOnAdmission = 
		dbo.f_GetAge (DateOfBirth, AdmissionDate)
	,AgeCategory =
		Case
			when dbo.f_GetAge (DateOfBirth, AdmissionDate) < 65 then '<65yrs'
			when dbo.f_GetAge (DateOfBirth, AdmissionDate) < 75 then '65 to <75yrs'
			else '75+'
		end
	,LoS = 
		datediff(day,AdmissionDate,coalesce(DischargeDate,getdate()))
	,OriginalSubmissionTime
	,OriginalResponse
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	
	,CurrentInpatient = 
		case
			when DischargeDate is null then 1
			else 0
		end
	,CQUIN	=
		case
			when  
				dbo.f_GetAge (DateOfBirth, AdmissionDate) >= 75
			and	datediff(mi,AdmissionTime,coalesce(DischargeTime,getdate())) >4320 -- LoS >72hrs
			and DateOfDeath is null
			and	AdmissionMethod.AdmissionType = 'Emergency' 
			and	NationalAdmissionMethodCode <> '81' 
			and PatientClassification.NationalPatientClassificationCode not in ('3','4')
			then 1
			else 0
		end
	,Division 
	,DementiaUpdated = Dementia.Updated
	,WardUpdated = WardStay.Updated
from 
	APC.BaseDementia Dementia

inner join APC.Encounter Encounter
on Encounter.GlobalProviderSpellNo = Dementia.GlobalProviderSpellNo

inner join APC.AdmissionMethod
on Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   
				
inner join APC.PatientClassification
on Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID 

inner join WH.Directorate
on coalesce(Encounter.EndDirectorateCode,Encounter.StartDirectorateCode) = Directorate.DirectorateCode

left join APC.DischargeMethod
on Encounter.DischargeMethodID = DischargeMethod.SourceDischargeMethodID

left join APC.BaseWardStay WardStay
on Encounter.ProviderSpellNo = WardStay.ProviderSpellNo
and not exists
	(
	select
		1
	from 
		APC.BaseWardStay Latest
	where 
		Latest.ProviderSpellNo = WardStay.ProviderSpellNo
	and	Latest.StartTime > WardStay.StartTime
	)


where
	not exists
	(
	Select
		1
	from
		APC.Encounter Latest
	where
		Latest.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
	and Latest.GlobalEpisodeNo > Encounter.GlobalEpisodeNo
	)




select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



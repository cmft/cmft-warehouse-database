﻿

CREATE procedure ETL.BuildAPCBaseFallReference
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseFallReference
			where
				BaseFallReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseFallReference
where
	not exists
	(
	select
		1
	from
		APC.BaseFall
	where
		BaseFall.MergeFallRecno = BaseFallReference.MergeFallRecno
	)
and	BaseFallReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseFallReference target
using
	(
	select
		BaseFall.MergeFallRecno
		,ContextID = Context.ContextID
		,FallDateID =
			coalesce(
				 FallDate.DateID

				,case
				when BaseFall.FallDate is null
				then NullDate.DateID

				when BaseFall.FallDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		
		,InitialSeverityID = FallSeverity.SourceFallSeverityID		
		,WardID = Ward.SourceWardID	
		,BaseFall.Created
		,BaseFall.Updated
		,BaseFall.ByWhom
	from
		APC.BaseFall

	inner join WH.Context
	on	Context.ContextCode = BaseFall.ContextCode

	inner join APC.FallSeverity
	on	FallSeverity.SourceFallSeverityCode = coalesce(BaseFall.InitialSeverityID, '-1')
	and	FallSeverity.SourceContextCode = BaseFall.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(BaseFall.WardCode, '-1')
	and	Ward.SourceContextCode = BaseFall.ContextCode

	left join WH.Calendar FallDate
	on	FallDate.TheDate = BaseFall.FallDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseFall.Updated > @LastUpdated
	and	BaseFall.ContextCode = @ContextCode

	) source
	on	source.MergeFallRecno = target.MergeFallRecno

	when not matched
	then
		insert
			(
			MergeFallRecno
			,ContextID
			,FallDateID
			,InitialSeverityID
			,WardID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeFallRecno
			,source.ContextID
			,source.FallDateID
			,source.InitialSeverityID
			,source.WardID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeFallRecno = source.MergeFallRecno
			,target.ContextID = source.ContextID
			,target.FallDateID = source.FallDateID
			,target.InitialSeverityID = source.InitialSeverityID
			,target.WardID = source.WardID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






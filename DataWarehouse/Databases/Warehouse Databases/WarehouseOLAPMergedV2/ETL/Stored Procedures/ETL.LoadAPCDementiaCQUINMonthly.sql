﻿

CREATE Procedure [ETL].[LoadAPCDementiaCQUINMonthly] @PeriodStart datetime ,@PeriodEnd datetime

as 

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	

-- Create the Monthly Dementia figures
Truncate table APC.DementiaCQUINMonthly
Insert into APC.DementiaCQUINMonthly	
	(
	ReportingMonth 
	,MergeEncounterRecno
	,MergeDementiaRecno
	,GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,DischargeDate
	,DischargeTime
	,LocalDischargeMethod
	,Directorate
	,Division
	,PatientSurname
	,SexCode
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard 
	,LatestSpecialtyCode
	,LatestConsultantCode
	,AgeOnAdmission
	,LoS 
	,OriginalSubmissionTime	
	,OriginalResponse	
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	
	,Q1Denominator 
	,Q1Numerator 
	,Q2Denominator
	,Q2Numerator 
	,Q3Denominator
	,Q3Numerator
	,FreezeDate 
	,ByWhom 
	,DementiaUpdated
	,WardUpdated
	)

Select	
	ReportingMonth = CAST(@PeriodEnd as DATE)
	,Encounter.MergeEncounterRecno
	,MergeDementiaRecno
	,Encounter.GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,DischargeDate
	,DischargeTime
	,LocalDischargeMethod
	,Directorate
	,Division
	
	,PatientSurname
	,SexCode
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard = WardStay.WardCode
	,LatestSpecialtyCode = SpecialtyCode
	,LatestConsultantCode = ConsultantCode
	,AgeOnAdmission = 
		dbo.f_GetAge (DateOfBirth, AdmissionDate)
	,LoS = 
		datediff(day,AdmissionDate,coalesce(DischargeDate,getdate()))
	,OriginalSubmissionTime	
	,OriginalResponse	
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	
	,Q1Denominator =
		case 
			when dateadd(mi,4320,AdmissionTime) between @PeriodStart and @PeriodEnd
			then 1
			else null
		end
	,Q1Numerator =
		case 
			when dateadd(mi,4320,AdmissionTime) between @PeriodStart and @PeriodEnd
			then
				case
					when 
						OriginalResponse = 'DementiaKnown'
					or
						datediff(mi,AdmissionTime,OriginalSubmissionTime)<=4320
					then 1
					else 0
				end
			else null
		end
			
	,Q2Denominator =
		case
			when LatestResponse = 1
				and LatestKnownToHaveDementiaResponse = 0
				and 
					(
						LatestAssessmentTime between @PeriodStart and @PeriodEnd
					or
						(
							LatestAssessmentTime is null
						and 
							DischargeDate between @PeriodStart and @PeriodEnd
						and
							(
								LatestAssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
							or
								LatestAssessmentNotPerformedReason is null
							)
						)
					)
			then 1
			else 0
		end
	,Q2Numerator =
		case
			when LatestResponse = 1
				and LatestKnownToHaveDementiaResponse = 0
				and 
					(
					LatestAssessmentTime between @PeriodStart and @PeriodEnd
					or
						(
							LatestAssessmentTime is null
						and 
							DischargeDate between @PeriodStart and @PeriodEnd
						and
							(
								LatestAssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
							or
								LatestAssessmentNotPerformedReason is null
							)
						)
					)
				then
					case
						when 
							LatestAssessmentScore>=8
						or
							(
								LatestAssessmentScore <=7
							and 
								LatestInvestigationTime is not null
							) 
						then 1
						else 0
					end
				else null
			end
			
	,Q3Denominator =
		case 
			when LatestResponse = 1
				and LatestKnownToHaveDementiaResponse = 0
				and LatestAssessmentScore between 0 and 7 
				and 
					(
						LatestReferralRecordedTime between @PeriodStart and @PeriodEnd
					or
						(
							LatestReferralRecordedTime is null 
						and 
							DischargeDate between @PeriodStart and @PeriodEnd
						and
							(
								LatestAssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
							or
								LatestAssessmentNotPerformedReason is null
							)
						)
					)
			then 1
			else 0
		end
	,Q3Numerator =
		case 
			when LatestResponse = 1
				and LatestKnownToHaveDementiaResponse = 0
				and LatestAssessmentScore between 0 and 7 
				and LatestReferralRecordedTime between @PeriodStart and @PeriodEnd
					
			then 1
			else 0
		end
	,FreezeDate = GETDATE()
	,ByWhom = suser_name()
	,DementiaUpdated = Dementia.Updated
	,WardUpdated = WardStay.Updated
from 
	APC.Encounter 
	
inner join APC.AdmissionMethod
on Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   

inner join APC.PatientClassification
on Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID 

left join APC.BaseWardStay WardStay
on Encounter.ProviderSpellNo = WardStay.ProviderSpellNo
and not exists
	(
	select
		1
	from 
		APC.BaseWardStay Latest
	where 
		Latest.ProviderSpellNo = WardStay.ProviderSpellNo
	and	Latest.StartTime > WardStay.StartTime
	)

left join APC.DischargeMethod
on Encounter.DischargeMethodID = DischargeMethod.SourceDischargeMethodID

left join WH.Directorate
on Directorate.DirectorateCode = coalesce(EndDirectorateCode,StartDirectorateCode)

left join APC.BaseDementia Dementia
on Encounter.GlobalProviderSpellNo = Dementia.GlobalProviderSpellNo

where 
	(
		dateadd(mi,4320,AdmissionTime) between @PeriodStart and @PeriodEnd
	or
		LatestAssessmentTime between @PeriodStart and @PeriodEnd
	or 
		LatestReferralRecordedTime between @PeriodStart and @PeriodEnd
	or 
		DischargeTime between @PeriodStart and @PeriodEnd
	)
and 
	dbo.f_GetAge (DateOfBirth, AdmissionDate)>=75 -- Patients 75yrs and over on Admission
and	
	datediff(mi,AdmissionTime,coalesce(DischargeTime,getdate())) >4320 -- LoS >72hrs
and 
	DateOfDeath is null 
and	
	AdmissionMethod.AdmissionType = 'Emergency' -- Non Elective admissions only
and 
	NationalAdmissionMethodCode <> '81' -- Exclude Transfers
and 
	PatientClassification.NationalPatientClassificationCode not in ('3','4') -- Exclude Regular admissions 
and 
	--FirstEpisodeInSpellIndicator = 1 -- Spell level count, not episode, changed from 1st to latest to get latest ward
	not exists
		(
		Select
			1
		from
			APC.Encounter Latest
		where
			Latest.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
		and Latest.GlobalEpisodeNo > Encounter.GlobalEpisodeNo
		)

If @PeriodEnd in (select LastDayOfMonth from WH.Calendar)
Begin

-- Identify those which need validating by Service
declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.DementiaValidation target
using
	(
	select 
		ReportingMonth
		,GlobalProviderSpellNo
		,CurrentIP = Case when DischargeDate is null then 1 else 0 end
		,DistrictNo
		,CasenoteNumber
		,PatientSurname
		,AdmissionTime
		,DischargeTime
		,Q1Denominator	
		,Q1Numerator	
		,Q2Denominator	
		,Q2Numerator	
		,Q3Denominator	
		,Q3Numerator
		,LatestKnownDementiaRecordedTime	
		,LatestKnownToHaveDementiaResponse	
		,LatestResponseTime	
		,LatestResponse	
		,LatestAssessmentTime	
		,LatestAssessmentScore	
		,LatestInvestigationTime	
		,LatestReferralSentDate	
		,LatestReferralRecordedTime	
		,LatestAssessmentNotPerformedReason	
		,LatestAssessmentNotPerformedRecordedTime
	from 
		APC.DementiaCQUINMonthly
	where 
		(Q1Denominator = 1 and Q1Numerator = 0)
	or
		(Q2Denominator = 1 and Q2Numerator = 0)
	or
		(Q3Denominator = 1 and Q3Numerator = 0)
	) source
	on	source.ReportingMonth = target.ReportingMonth
	and source.GlobalProviderSpellNo = target.GlobalProviderSpellNo
	and coalesce(source.Q1Denominator,'') = coalesce(target.Q1Denominator,'')
	and coalesce(source.Q1Numerator,'') = coalesce(target.Q1Numerator,'')
	and coalesce(source.Q2Denominator,'') = coalesce(target.Q2Denominator,'')
	and coalesce(source.Q2Numerator,'') = coalesce(target.Q2Numerator,'')
	and coalesce(source.Q3Denominator,'') = coalesce(target.Q3Denominator,'')
	and coalesce(source.Q3Numerator,'') = coalesce(target.Q3Numerator,'')
	
	when not matched by source
	and	target.ReportingMonth = @PeriodEnd 

	then delete

	when not matched
	then
		insert
			(
			ReportingMonth
			,GlobalProviderSpellNo
			,CurrentIP
			,AddedForValidation 
			,RecordUpdated 
			,DistrictNo
			,CasenoteNumber
			,PatientSurname
			,AdmissionTime
			,DischargeTime
			,Q1Denominator	
			,Q1Numerator	
			,Q2Denominator	
			,Q2Numerator	
			,Q3Denominator	
			,Q3Numerator
			,LatestKnownDementiaRecordedTime	
			,LatestKnownToHaveDementiaResponse	
			,LatestResponseTime	
			,LatestResponse	
			,LatestAssessmentTime	
			,LatestAssessmentScore	
			,LatestInvestigationTime	
			,LatestReferralSentDate	
			,LatestReferralRecordedTime	
			,LatestAssessmentNotPerformedReason	
			,LatestAssessmentNotPerformedRecordedTime
			)
		Values
			(
			 source.ReportingMonth
			,source.GlobalProviderSpellNo
			,source.CurrentIP
			,getdate() 
			,getdate()
			,source.DistrictNo
			,source.CasenoteNumber
			,source.PatientSurname
			,source.AdmissionTime
			,source.DischargeTime
			,source.Q1Denominator	
			,source.Q1Numerator	
			,source.Q2Denominator	
			,source.Q2Numerator	
			,source.Q3Denominator	
			,source.Q3Numerator
			,source.LatestKnownDementiaRecordedTime	
			,source.LatestKnownToHaveDementiaResponse	
			,source.LatestResponseTime	
			,source.LatestResponse	
			,source.LatestAssessmentTime	
			,source.LatestAssessmentScore	
			,source.LatestInvestigationTime	
			,source.LatestReferralSentDate	
			,source.LatestReferralRecordedTime	
			,source.LatestAssessmentNotPerformedReason	
			,source.LatestAssessmentNotPerformedRecordedTime
			)
		
	when matched
	and not
		(
			isnull(target.LatestKnownDementiaRecordedTime, getdate()) = isnull(source.LatestKnownDementiaRecordedTime, getdate())
		and isnull(target.LatestKnownToHaveDementiaResponse, '') = isnull(source.LatestKnownToHaveDementiaResponse, '')
		and isnull(target.LatestResponseTime, getdate()) = isnull(source.LatestResponseTime, getdate())
		and isnull(target.LatestResponse, '') = isnull(source.LatestResponse, '')
		and isnull(target.LatestAssessmentTime, getdate()) = isnull(source.LatestAssessmentTime, getdate())
		and isnull(target.LatestAssessmentScore, 99) = isnull(source.LatestAssessmentScore, 99)
		and isnull(target.LatestInvestigationTime, getdate()) = isnull(source.LatestInvestigationTime, getdate())
		and isnull(target.LatestReferralSentDate, '') = isnull(source.LatestReferralSentDate, '')
		and isnull(target.LatestReferralRecordedTime, getdate()) = isnull(source.LatestReferralRecordedTime, getdate())
		and isnull(target.LatestAssessmentNotPerformedReason, '') = isnull(source.LatestAssessmentNotPerformedReason, '')
		and isnull(target.LatestAssessmentNotPerformedRecordedTime, getdate()) = isnull(source.LatestAssessmentNotPerformedRecordedTime, getdate())
		and isnull(target.CurrentIP, '') = isnull(source.CurrentIP, '')
		)
	then
		update
		set
			target.LatestKnownDementiaRecordedTime = source.LatestKnownDementiaRecordedTime
			,target.LatestKnownToHaveDementiaResponse = source.LatestKnownToHaveDementiaResponse
			,target.LatestResponseTime = source.LatestResponseTime
			,target.LatestResponse = source.LatestResponse
			,target.LatestAssessmentTime = source.LatestAssessmentTime
			,target.LatestAssessmentScore = source.LatestAssessmentScore
			,target.LatestInvestigationTime = source.LatestInvestigationTime
			,target.LatestReferralSentDate = source.LatestReferralSentDate
			,target.LatestReferralRecordedTime = source.LatestReferralRecordedTime
			,target.LatestAssessmentNotPerformedReason = source.LatestAssessmentNotPerformedReason
			,target.LatestAssessmentNotPerformedRecordedTime = source.LatestAssessmentNotPerformedRecordedTime
			,target.RecordUpdated = getdate()
			,target.CurrentIP = source.CurrentIP
			
output
	$action into @MergeSummary
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

End


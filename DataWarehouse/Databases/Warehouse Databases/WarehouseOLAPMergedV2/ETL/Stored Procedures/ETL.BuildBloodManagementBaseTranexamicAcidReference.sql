﻿




CREATE procedure [ETL].[BuildBloodManagementBaseTranexamicAcidReference] -- 'CMFT||RECALL'
	@ContextCode varchar(15)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				BloodManagement.BaseTranexamicAcidReference
			where
				BaseTranexamicAcidReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	BloodManagement.BaseTranexamicAcidReference
where
	not exists
	(
	select
		1
	from
		BloodManagement.BaseTranexamicAcid
	where
		BaseTranexamicAcid.MergeTranexamicAcidRecno = BaseTranexamicAcidReference.MergeTranexamicAcidRecno
	)
and	BaseTranexamicAcidReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


--select top 100 * from BloodManagement.BaseTranexamicAcid

merge
	BloodManagement.BaseTranexamicAcidReference target
using
	(
	select
		BaseTranexamicAcid.MergeTranexamicAcidRecno
		,ContextID = Context.ContextID
		,StartDateID = coalesce(
								 StartDate.DateID
								,case
									when BaseTranexamicAcid.DrugAdministeredDate is null then NullDate.DateID
									else FutureDate.DateID
								end
								)
		-- For those which go through reference map, they are in WH.Member, need context code
		,LocationID = SourceLocationID 
		,SpecialtyID = SourceSpecialtyID 
		,ConsultantID = SourceConsultantID 
		
		,BaseTranexamicAcid.Created
		,BaseTranexamicAcid.Updated
		,BaseTranexamicAcid.ByWhom
	from
		BloodManagement.BaseTranexamicAcid

	inner join WH.Context
	on	Context.ContextCode = BaseTranexamicAcid.ContextCode

	left join WH.Calendar StartDate
	on	StartDate.TheDate = BaseTranexamicAcid.DrugAdministeredDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'
	
	left join WH.Location
	on	Location.SourceLocationCode = coalesce(BaseTranexamicAcid.SessionLocationKey, '-1')
	and	Location.SourceContextCode = BaseTranexamicAcid.ContextCode
	-- select * from BloodManagement.BaseTranexamicAcid
	
	left join WH.Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(BaseTranexamicAcid.SpecialtyKey, '-1')
	and	Specialty.SourceContextCode = BaseTranexamicAcid.ContextCode

	left join WH.Consultant
	on	Consultant.SourceConsultantCode = coalesce(BaseTranexamicAcid.ConsultantKey, '-1')
	and	Consultant.SourceContextCode = BaseTranexamicAcid.ContextCode

	where
		BaseTranexamicAcid.Updated > @LastUpdated
	and	BaseTranexamicAcid.ContextCode = @ContextCode

	) source
	on	source.MergeTranexamicAcidRecno = target.MergeTranexamicAcidRecno

	when not matched
	then
		insert
			(
			MergeTranexamicAcidRecno
			,ContextID 
			,StartDateID 
			,LocationID 
			,SpecialtyID
			,ConsultantID
	
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeTranexamicAcidRecno
			,source.ContextID 
			,source.StartDateID 
			,source.LocationID 
			,source.SpecialtyID
			,source.ConsultantID
				
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeTranexamicAcidRecno = source.MergeTranexamicAcidRecno
			,target.ContextID = source.ContextID
			,target.StartDateID = source.StartDateID
			,target.LocationID = source.LocationID
			,target.SpecialtyID = source.SpecialtyID
			,target.ConsultantID = source.ConsultantID
			
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
			
		
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats









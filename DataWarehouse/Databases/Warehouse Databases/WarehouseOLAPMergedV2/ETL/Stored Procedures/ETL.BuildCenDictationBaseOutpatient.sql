﻿


CREATE procedure [ETL].[BuildCenDictationBaseOutpatient]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Dictation.BaseOutpatient target
using
	(
	select
		OutpatientRecno 
		,SourcePatientNo
		,AppointmentTime 
		,DoctorCode 
		,InterfaceCode 
		,ContextCode

	from
		ETL.TLoadCenDictationOutpatient Outpatient
	) source
	on	source.OutpatientRecno = target.OutpatientRecno
	and	source.ContextCode = target.ContextCode
	

	when not matched by source
	and	target.ContextCode = 'CEN||MEDI'

	then delete

	when not matched
	then
		insert
			(
			OutpatientRecno
			,SourcePatientNo
			,AppointmentTime
			,DoctorCode
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.OutpatientRecno
			,source.SourcePatientNo
			,source.AppointmentTime
			,source.DoctorCode
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	
			isnull(target.OutpatientRecno, 0) = isnull(source.OutpatientRecno, 0)
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and	isnull(target.AppointmentTime, getdate()) = isnull(source.AppointmentTime, getdate())
		and isnull(target.DoctorCode, '') = isnull(source.DoctorCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			target.OutpatientRecno = source.OutpatientRecno
			,target.SourcePatientNo = source.SourcePatientNo
			,target.AppointmentTime = source.AppointmentTime
			,target.DoctorCode = source.DoctorCode
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


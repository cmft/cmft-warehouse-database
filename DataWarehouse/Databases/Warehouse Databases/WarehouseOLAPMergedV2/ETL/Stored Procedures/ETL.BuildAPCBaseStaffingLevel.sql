﻿
CREATE procedure [ETL].[BuildAPCBaseStaffingLevel]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge APC.BaseStaffingLevel target
using 
	(
	select
		StaffingLevelRecno
		,DivisionCode
		,WardCode
		,CensusDate
		,ShiftID
		,RegisteredNursePlan
		,RegisteredNurseActual
		,NonRegisteredNursePlan
		,NonRegisteredNurseActual
		,Comments
		,SeniorComments
		,InterfaceCode
		,ContextCode
	from
		ETL.TLoadAPCBaseStaffingLevel

		) source
	on	source.StaffingLevelRecno = target.StaffingLevelRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CMFT||IPATH'
	then delete

	when not matched 
	then
		insert
			(
			StaffingLevelRecno
			,DivisionCode
			,WardCode
			,CensusDate
			,ShiftID
			,RegisteredNursePlan
			,RegisteredNurseActual
			,NonRegisteredNursePlan
			,NonRegisteredNurseActual
			,Comments
			,SeniorComments
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)

		values
			(
			source.StaffingLevelRecno
			,source.DivisionCode
			,source.WardCode
			,source.CensusDate
			,source.ShiftID
			,source.RegisteredNursePlan
			,source.RegisteredNurseActual
			,source.NonRegisteredNursePlan
			,source.NonRegisteredNurseActual
			,source.Comments
			,source.SeniorComments
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			 isnull(target.StaffingLevelRecno, 0) = isnull(source.StaffingLevelRecno, 0)
		 and isnull(target.DivisionCode, '') = isnull(source.DivisionCode, '')
		 and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		 and isnull(target.CensusDate, getdate()) = isnull(source.CensusDate, getdate())
		 and isnull(target.ShiftID, 0) = isnull(source.ShiftID, 0)
		 and isnull(target.RegisteredNursePlan, 0) = isnull(source.RegisteredNursePlan, 0)
		 and isnull(target.RegisteredNurseActual, 0) = isnull(source.RegisteredNurseActual, 0)
		 and isnull(target.NonRegisteredNursePlan, 0) = isnull(source.NonRegisteredNursePlan, 0)
		 and isnull(target.NonRegisteredNurseActual, 0) = isnull(source.NonRegisteredNurseActual, 0)
		 and isnull(target.Comments, '') = isnull(source.Comments, '')
		 and isnull(target.SeniorComments, '') = isnull(source.SeniorComments, '')
		 and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		 and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	
	then 
		update 
		set
			 target.StaffingLevelRecno = source.StaffingLevelRecno
			,target.DivisionCode = source.DivisionCode
			,target.WardCode = source.WardCode
			,target.CensusDate = source.CensusDate
			,target.ShiftID = source.ShiftID
			,target.RegisteredNursePlan = source.RegisteredNursePlan
			,target.RegisteredNurseActual = source.RegisteredNurseActual
			,target.NonRegisteredNursePlan = source.NonRegisteredNursePlan
			,target.NonRegisteredNurseActual = source.NonRegisteredNurseActual
			,target.Comments = source.Comments
			,target.SeniorComments = source.SeniorComments
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


﻿CREATE procedure [ETL].[ImportComplaintBaseComplaint] as

--import the data
exec ETL.BuildComplaintBaseComplaint


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Complaint].[BaseComplaint]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildComplaintBaseComplaintReference 'CMFT||ULYSS'


--Create Outstanding Complaints List
exec ETL.BuildComplaintBaseOutstandingComplaint




﻿CREATE procedure [ETL].[BuildTransactionLogBaseReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

insert
into
	TransactionLog.BaseReference
(
	 MergeRecno
	,ContextID
	,SystemUserID
)
select
	 Base.MergeRecno
	,@ContextID
	,SystemUser.SystemUserID
from
	TransactionLog.Base

inner join WH.SystemUser
on	SystemUser.UserID = coalesce(Base.UserID, '-1')
and	SystemUser.ContextCode = Base.ContextCode

where
	not exists
	(
	select
		1
	from
		TransactionLog.BaseReference
	where
		BaseReference.MergeRecno = Base.MergeRecno
	and	BaseReference.ContextID = @ContextID
	)

select
	 @inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

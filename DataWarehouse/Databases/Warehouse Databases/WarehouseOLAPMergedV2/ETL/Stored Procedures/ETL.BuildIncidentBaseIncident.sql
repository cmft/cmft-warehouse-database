﻿
CREATE procedure [ETL].[BuildIncidentBaseIncident]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Incident.BaseIncident target
using 
	(
	select
		IncidentRecno
		,SourceIncidentCode
		,IncidentCode
		,IncidentNumber
		,Description
		,IncidentDate
		,IncidentTime
		,ReportedDate
		,ReportedTime
		,ReceiptDate
		,CauseCode1
		,CauseCode2
		,IncidentTypeCode
		,ContributingFactorCode
		,SignedByCode
		,IncidentGradeCode
		,CompletedByCode
		,SeverityCode
		,LikelihoodCode
		,RiskRatingCode
		,RiskScore
		,SpecialtyCode
		,PatientSafety
		,CauseGroupCode
		,SeriousUntowardIncident
		,ResearchOrTrialProject
		,InitialSeverityCode
		,InitialLikelihoodCode
		,InitialRiskRatingCode
		,InitialRiskScore
		,SecurityIncidentReportingSystemReportable
		,StatusTypeCode
		,NeverEvent
		,OrganisationCode
		,SiteTypeCode
		,SiteCode
		,SourceDivisionCode
		,SourceDirectorateCode
		,LocationCode
		,DepartmentCode
		,WardCode
		,SequenceNo
		,PersonCode
		,PersonTypeCode
		,EntityTypeCode
		,DateOfBirth
		,SexCode
		,CasenoteNumber
		,NHSNumber
		,InterfaceCode
		,ContextCode
		,Created
		,Updated
		,ByWhom
	from
		ETL.TLoadIncidentBaseIncident

		) source
	on	source.IncidentRecno = target.IncidentRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CMFT||ULYSS'
	then delete

	when not matched 
	then
		insert
			(
			IncidentRecno
			,SourceIncidentCode
			,IncidentCode
			,IncidentNumber
			,Description
			,IncidentDate
			,IncidentTime
			,ReportedDate
			,ReportedTime
			,ReceiptDate
			,CauseCode1
			,CauseCode2
			,IncidentTypeCode
			,ContributingFactorCode
			,SignedByCode
			,IncidentGradeCode
			,CompletedByCode
			,SeverityCode
			,LikelihoodCode
			,RiskRatingCode
			,RiskScore
			,SpecialtyCode
			,PatientSafety
			,CauseGroupCode
			,SeriousUntowardIncident
			,ResearchOrTrialProject
			,InitialSeverityCode
			,InitialLikelihoodCode
			,InitialRiskRatingCode
			,InitialRiskScore
			,SecurityIncidentReportingSystemReportable
			,StatusTypeCode
			,NeverEvent
			,OrganisationCode
			,SiteTypeCode
			,SiteCode
			,SourceDivisionCode
			,SourceDirectorateCode
			,LocationCode
			,DepartmentCode
			,WardCode
			,SequenceNo
			,PersonCode
			,PersonTypeCode
			,EntityTypeCode
			,DateOfBirth
			,SexCode
			,CasenoteNumber
			,NHSNumber
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)

		values
			(
			 source.IncidentRecno
			,source.SourceIncidentCode
			,source.IncidentCode
			,source.IncidentNumber
			,source.Description
			,source.IncidentDate
			,source.IncidentTime
			,source.ReportedDate
			,source.ReportedTime
			,source.ReceiptDate
			,source.CauseCode1
			,source.CauseCode2
			,source.IncidentTypeCode
			,source.ContributingFactorCode
			,source.SignedByCode
			,source.IncidentGradeCode
			,source.CompletedByCode
			,source.SeverityCode
			,source.LikelihoodCode
			,source.RiskRatingCode
			,source.RiskScore
			,source.SpecialtyCode
			,source.PatientSafety
			,source.CauseGroupCode
			,source.SeriousUntowardIncident
			,source.ResearchOrTrialProject
			,source.InitialSeverityCode
			,source.InitialLikelihoodCode
			,source.InitialRiskRatingCode
			,source.InitialRiskScore
			,source.SecurityIncidentReportingSystemReportable
			,source.StatusTypeCode
			,source.NeverEvent
			,source.OrganisationCode
			,source.SiteTypeCode
			,source.SiteCode
			,source.SourceDivisionCode
			,source.SourceDirectorateCode
			,source.LocationCode
			,source.DepartmentCode
			,source.WardCode
			,source.SequenceNo
			,source.PersonCode
			,source.PersonTypeCode
			,source.EntityTypeCode
			,source.DateOfBirth
			,source.SexCode
			,source.CasenoteNumber
			,source.NHSNumber
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			 isnull(target.IncidentRecno, 0) = isnull(source.IncidentRecno, 0)
		 and isnull(target.SourceIncidentCode, 0) = isnull(source.SourceIncidentCode, 0)
		 and isnull(target.IncidentCode, cast('' as varbinary(50))) = isnull(source.IncidentCode, cast('' as varbinary(50)))
		 and isnull(target.IncidentNumber, 0) = isnull(source.IncidentNumber, 0)
		 and isnull(target.Description, '') = isnull(source.Description, '')
		 and isnull(target.IncidentDate, getdate()) = isnull(source.IncidentDate, getdate())
		 and isnull(target.IncidentTime, getdate()) = isnull(source.IncidentTime, getdate())
		 and isnull(target.ReportedDate, getdate()) = isnull(source.ReportedDate, getdate())
		 and isnull(target.ReportedTime, getdate()) = isnull(source.ReportedTime, getdate())
		 and isnull(target.ReceiptDate, getdate()) = isnull(source.ReceiptDate, getdate())
		 and isnull(target.CauseCode1, cast('' as varbinary(50))) = isnull(source.CauseCode1, cast('' as varbinary(50)))
		 and isnull(target.CauseCode2, cast('' as varbinary(50))) = isnull(source.CauseCode2, cast('' as varbinary(50)))
		 and isnull(target.IncidentTypeCode, cast('' as varbinary(50))) = isnull(source.IncidentTypeCode, cast('' as varbinary(50)))
		 and isnull(target.ContributingFactorCode, '') = isnull(source.ContributingFactorCode, '')
		 and isnull(target.SignedByCode, '') = isnull(source.SignedByCode, '')
		 and isnull(target.IncidentGradeCode, cast('' as varbinary(50))) = isnull(source.IncidentGradeCode, cast('' as varbinary(50)))
		 and isnull(target.CompletedByCode, '') = isnull(source.CompletedByCode, '')
		 and isnull(target.SeverityCode, cast('' as varbinary(50))) = isnull(source.SeverityCode, cast('' as varbinary(50)))
		 and isnull(target.LikelihoodCode, '') = isnull(source.LikelihoodCode, '')
		 and isnull(target.RiskRatingCode, '') = isnull(source.RiskRatingCode, '')
		 and isnull(target.RiskScore, 0) = isnull(source.RiskScore, 0)
		 and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		 and isnull(target.PatientSafety, '') = isnull(source.PatientSafety, '')
		 and isnull(target.CauseGroupCode, cast('' as varbinary(50))) = isnull(source.CauseGroupCode, cast('' as varbinary(50)))
		 and isnull(target.SeriousUntowardIncident, '') = isnull(source.SeriousUntowardIncident, '')
		 and isnull(target.ResearchOrTrialProject, '') = isnull(source.ResearchOrTrialProject, '')
		 and isnull(target.InitialSeverityCode, '') = isnull(source.InitialSeverityCode, '')
		 and isnull(target.InitialLikelihoodCode, '') = isnull(source.InitialLikelihoodCode, '')
		 and isnull(target.InitialRiskRatingCode, '') = isnull(source.InitialRiskRatingCode, '')
		 and isnull(target.InitialRiskScore, '') = isnull(source.InitialRiskScore, '')
		 and isnull(target.SecurityIncidentReportingSystemReportable, '') = isnull(source.SecurityIncidentReportingSystemReportable, '')
		 and isnull(target.StatusTypeCode, '') = isnull(source.StatusTypeCode, '')
		 and isnull(target.NeverEvent, '') = isnull(source.NeverEvent, '')
		 and isnull(target.OrganisationCode, '') = isnull(source.OrganisationCode, '')
		 and isnull(target.SiteTypeCode, '') = isnull(source.SiteTypeCode, '')
		 and isnull(target.SiteCode, cast('' as varbinary(50))) = isnull(source.SiteCode, cast('' as varbinary(50)))
		 and isnull(target.SourceDivisionCode,  cast('' as varbinary(50))) = isnull(source.SourceDivisionCode, cast('' as varbinary(50)))
		 and isnull(target.SourceDirectorateCode, cast('' as varbinary(50))) = isnull(source.SourceDirectorateCode, cast('' as varbinary(50)))
		 and isnull(target.LocationCode, cast('' as varbinary(50))) = isnull(source.LocationCode, '')
		 and isnull(target.DepartmentCode, cast('' as varbinary(50))) = isnull(source.DepartmentCode, cast('' as varbinary(50)))
		 and isnull(target.WardCode, cast('' as varbinary(50))) = isnull(source.WardCode, cast('' as varbinary(50)))
		 and isnull(target.SequenceNo, 0) = isnull(source.SequenceNo, 0)
		 and isnull(target.PersonCode, '') = isnull(source.PersonCode, '')
		 and isnull(target.PersonTypeCode, '') = isnull(source.PersonTypeCode, '')
		 and isnull(target.EntityTypeCode, 0) = isnull(source.EntityTypeCode, 0)
		 and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate()) 
		 and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		 and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		 and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		 and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		 and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')

		)
	
	then 
		update 
		set
			 target.IncidentRecno = source.IncidentRecno
			,target.SourceIncidentCode = source.SourceIncidentCode
			,target.IncidentCode = source.IncidentCode
			,target.IncidentNumber = source.IncidentNumber
			,target.Description = source.Description
			,target.IncidentDate = source.IncidentDate
			,target.IncidentTime = source.IncidentTime
			,target.ReportedDate = source.ReportedDate
			,target.ReportedTime = source.ReportedTime
			,target.ReceiptDate = source.ReceiptDate
			,target.CauseCode1 = source.CauseCode1
			,target.CauseCode2 = source.CauseCode2
			,target.IncidentTypeCode = source.IncidentTypeCode
			,target.ContributingFactorCode = source.ContributingFactorCode
			,target.SignedByCode = source.SignedByCode
			,target.IncidentGradeCode = source.IncidentGradeCode
			,target.CompletedByCode = source.CompletedByCode
			,target.SeverityCode = source.SeverityCode
			,target.LikelihoodCode = source.LikelihoodCode
			,target.RiskRatingCode = source.RiskRatingCode
			,target.RiskScore = source.RiskScore
			,target.SpecialtyCode = source.SpecialtyCode
			,target.PatientSafety = source.PatientSafety
			,target.CauseGroupCode = source.CauseGroupCode
			,target.SeriousUntowardIncident = source.SeriousUntowardIncident
			,target.ResearchOrTrialProject = source.ResearchOrTrialProject
			,target.InitialSeverityCode = source.InitialSeverityCode
			,target.InitialLikelihoodCode = source.InitialLikelihoodCode
			,target.InitialRiskRatingCode = source.InitialRiskRatingCode
			,target.InitialRiskScore = source.InitialRiskScore
			,target.SecurityIncidentReportingSystemReportable = source.SecurityIncidentReportingSystemReportable
			,target.StatusTypeCode = source.StatusTypeCode
			,target.NeverEvent = source.NeverEvent
			,target.OrganisationCode = source.OrganisationCode
			,target.SiteTypeCode = source.SiteTypeCode
			,target.SiteCode = source.SiteCode
			,target.SourceDivisionCode = source.SourceDivisionCode
			,target.SourceDirectorateCode = source.SourceDirectorateCode
			,target.LocationCode = source.LocationCode
			,target.DepartmentCode = source.DepartmentCode
			,target.WardCode = source.WardCode
			,target.SequenceNo = source.SequenceNo
			,target.PersonCode = source.PersonCode
			,target.PersonTypeCode = source.PersonTypeCode
			,target.EntityTypeCode = source.EntityTypeCode
			,target.DateOfBirth = source.DateOfBirth
			,target.SexCode = source.SexCode
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


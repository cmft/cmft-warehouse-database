﻿


CREATE procedure [ETL].[ImportCenDictation] as

exec ETL.ImportCenDictationBaseDocument
exec ETL.ImportCenDictationBaseInpatient
exec APC.BuildBaseEncounterBaseDocument --'CEN||PAS','CEN||MEDI'
exec ETL.ImportCenDictationBaseOutpatient



﻿
CREATE procedure [ETL].[BuildRenalBaseRenalPatient]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||CV'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Renal.BaseRenalPatient target
using [$(Warehouse)].Renal.RenalPatient source
on	source.RenalPatientRecno = target.RenalPatientRecno

when matched and not
(
	isnull(target.[FirstName], '') = isnull(source.[FirstName], '')
	and isnull(target.[MiddleName], '') = isnull(source.[MiddleName], '')
	and isnull(target.[LastName], '') = isnull(source.[LastName], '')
	and isnull(target.[Title], '') = isnull(source.[Title], '')
	and isnull(target.[FullName], '') = isnull(source.[FullName], '')
	and isnull(target.[ExtendedName], '') = isnull(source.[ExtendedName], '')
	and isnull(target.[ShortName], '') = isnull(source.[ShortName], '')
	and isnull(target.[Initials], '') = isnull(source.[Initials], '')
	and isnull(target.[Signature], '') = isnull(source.[Signature], '')
	and isnull(target.[MedicalRecordNo], '') = isnull(source.[MedicalRecordNo], '')
	and isnull(target.[NationalHealthServiceNo], '') = isnull(source.[NationalHealthServiceNo], '')
	and isnull(target.[DateOfBirth], getdate()) = isnull(source.[DateOfBirth], getdate())
	and isnull(target.[BirthPlace], '') = isnull(source.[BirthPlace], '')
	and isnull(target.[DateOfDeath], getdate()) = isnull(source.[DateOfDeath], getdate())
	and isnull(target.[CauseOfDeath], '') = isnull(source.[CauseOfDeath], '')
	and isnull(target.[EMailAddress], '') = isnull(source.[EMailAddress], '')
	and isnull(target.[MaritalStatus], '') = isnull(source.[MaritalStatus], '')
	and isnull(target.[Age], 0) = isnull(source.[Age], 0)
	and isnull(target.[Sex], '') = isnull(source.[Sex], '')
	and isnull(target.[EducationLevel], '') = isnull(source.[EducationLevel], '')
	and isnull(target.[PrimaryPhone], '') = isnull(source.[PrimaryPhone], '')
	and isnull(target.[PrimaryPhoneObjectID], 0) = isnull(source.[PrimaryPhoneObjectID], 0)
	and isnull(target.[MailingAddress], '') = isnull(source.[MailingAddress], '')
	and isnull(target.[MailingAddressObjectID], 0) = isnull(source.[MailingAddressObjectID], 0)
	and isnull(target.[Race], '') = isnull(source.[Race], '')
	and isnull(target.[Ethnicity], '') = isnull(source.[Ethnicity], '')
	and isnull(target.[Religion], '') = isnull(source.[Religion], '')
	and isnull(target.[BloodGroup], '') = isnull(source.[BloodGroup], '')
	and isnull(target.[BloodRhesus], '') = isnull(source.[BloodRhesus], '')
	and isnull(target.[DiabeticStatus], '') = isnull(source.[DiabeticStatus], '')
	and isnull(target.[KidneyFunction], '') = isnull(source.[KidneyFunction], '')
	and isnull(target.[PrimaryCaregiver], '') = isnull(source.[PrimaryCaregiver], '')
	and isnull(target.[MedicalPractice], '') = isnull(source.[MedicalPractice], '')
	and isnull(target.[MedicalPracticeObjectID], 0) = isnull(source.[MedicalPracticeObjectID], 0)
	and isnull(target.[MedicalPracticeAddress], '') = isnull(source.[MedicalPracticeAddress], '')
	and isnull(target.[GeneralPractitioner], '') = isnull(source.[GeneralPractitioner], '')
	and isnull(target.[GeneralPractitionerFirstName], '') = isnull(source.[GeneralPractitionerFirstName], '')
	and isnull(target.[GeneralPractitionerLastName], '') = isnull(source.[GeneralPractitionerLastName], '')
	and isnull(target.[HearingImpairment], '') = isnull(source.[HearingImpairment], '')
	and isnull(target.[SightImpairment], '') = isnull(source.[SightImpairment], '')
	and isnull(target.[SpeechImpairment], '') = isnull(source.[SpeechImpairment], '')
	and isnull(target.[InterpreterRequired], '') = isnull(source.[InterpreterRequired], '')
	and isnull(target.[IsAmbulatory], '') = isnull(source.[IsAmbulatory], '')
	and isnull(target.[AmbulatoryStatus], '') = isnull(source.[AmbulatoryStatus], '')
	and isnull(target.[WheelchairStatus], '') = isnull(source.[WheelchairStatus], '')
	and isnull(target.[OxygenRequired], '') = isnull(source.[OxygenRequired], '')
	and isnull(target.[PrimaryCareGiverObjectID], 0) = isnull(source.[PrimaryCareGiverObjectID], 0)
	and isnull(target.[GeneralPractitionerObjectID], 0) = isnull(source.[GeneralPractitionerObjectID], 0)
	and isnull(target.[PrimaryPharmacyObjectID], 0) = isnull(source.[PrimaryPharmacyObjectID], 0)
	and isnull(target.[PrimaryTransportObjectID], 0) = isnull(source.[PrimaryTransportObjectID], 0)
	and isnull(target.[VeteranStatus], '') = isnull(source.[VeteranStatus], '')
	and isnull(target.[AdminCategory], '') = isnull(source.[AdminCategory], '')
	and isnull(target.[PrimaryContactPersonName], '') = isnull(source.[PrimaryContactPersonName], '')
	and isnull(target.[PrimaryContactPersonObjectID], 0) = isnull(source.[PrimaryContactPersonObjectID], 0)
	and isnull(target.[PrimaryRenalModalityStartDate], getdate()) = isnull(source.[PrimaryRenalModalityStartDate], getdate())
	and isnull(target.[PrimaryRenalModalityStopDate], getdate()) = isnull(source.[PrimaryRenalModalityStopDate], getdate())
	and isnull(target.[PrimaryRenalModality], '') = isnull(source.[PrimaryRenalModality], '')
	and isnull(target.[PrimaryRenalModalitySetting], '') = isnull(source.[PrimaryRenalModalitySetting], '')
	and isnull(target.[PrimaryRenalModalityDialysisProvider], '') = isnull(source.[PrimaryRenalModalityDialysisProvider], '')
	and isnull(target.[PrimaryRenalModalityObjectID], 0) = isnull(source.[PrimaryRenalModalityObjectID], 0)
	and isnull(target.[PrimaryRenalModalityDialysisProviderObjectID], 0) = isnull(source.[PrimaryRenalModalityDialysisProviderObjectID], 0)
	and isnull(target.[PrimaryKidneyTransplantStatusObjectID], 0) = isnull(source.[PrimaryKidneyTransplantStatusObjectID], 0)
	and isnull(target.[IsolationStatus], '') = isnull(source.[IsolationStatus], '')
	and isnull(target.[IsActive], 0) = isnull(source.[IsActive], 0)
	and isnull(target.[RegistrationDate], getdate()) = isnull(source.[RegistrationDate], getdate())
	and isnull(target.[InactiveDate], getdate()) = isnull(source.[InactiveDate], getdate())
	and isnull(target.[ReasonInactive], '') = isnull(source.[ReasonInactive], '')
	and isnull(target.[PrimaryNephrologist], '') = isnull(source.[PrimaryNephrologist], '')
	and isnull(target.[PrimaryNephrologistObjectID], 0) = isnull(source.[PrimaryNephrologistObjectID], 0)
	and isnull(target.[WeeklyCalendar], '') = isnull(source.[WeeklyCalendar], '')
	and isnull(target.[PrimaryRenalDiagnosis], '') = isnull(source.[PrimaryRenalDiagnosis], '')
	and isnull(target.[RenalDiagnosisDateOfOnset], getdate()) = isnull(source.[RenalDiagnosisDateOfOnset], getdate())
	and isnull(target.[OtherRenalDiagnosis], '') = isnull(source.[OtherRenalDiagnosis], '')
	and isnull(target.[RenalDiagnosisCode], '') = isnull(source.[RenalDiagnosisCode], '')
	and isnull(target.[RenalDiagnosisDescription], '') = isnull(source.[RenalDiagnosisDescription], '')
	and isnull(target.[RegistrationHeight], 0) = isnull(source.[RegistrationHeight], 0)
	and isnull(target.[RegistrationHeightUnits], '') = isnull(source.[RegistrationHeightUnits], '')
	and isnull(target.[RegistrationHeightInCm], 0) = isnull(source.[RegistrationHeightInCm], 0)
	and isnull(target.[RegistrationWeight], 0) = isnull(source.[RegistrationWeight], 0)
	and isnull(target.[RegistrationWeightUnits], '') = isnull(source.[RegistrationWeightUnits], '')
	and isnull(target.[RegistrationWeightInKg], 0) = isnull(source.[RegistrationWeightInKg], 0)
	and isnull(target.[Remarks], '') = isnull(source.[Remarks], '')
	and isnull(target.[DateFirstSeenByPhysician], getdate()) = isnull(source.[DateFirstSeenByPhysician], getdate())
	and isnull(target.[PreDialysisFollowUp], '') = isnull(source.[PreDialysisFollowUp], '')
	and isnull(target.[PreDialysisErythropoietin], '') = isnull(source.[PreDialysisErythropoietin], '')
	and isnull(target.[UKRROptOut], 0) = isnull(source.[UKRROptOut], 0)
	and isnull(target.[PediatricPatient], 0) = isnull(source.[PediatricPatient], 0)
	and isnull(target.[GeneticRelationshipBetweenParents], 0) = isnull(source.[GeneticRelationshipBetweenParents], 0)
	and isnull(target.[SingleAdultResponsible], 0) = isnull(source.[SingleAdultResponsible], 0)
	and isnull(target.[OtherFamilyMembersAlsoESRF], 0) = isnull(source.[OtherFamilyMembersAlsoESRF], 0)
	and isnull(target.[OtherFamilyMembersSameRenalDisease], 0) = isnull(source.[OtherFamilyMembersSameRenalDisease], 0)
	and isnull(target.[DateReferredToAdultRenalServices], getdate()) = isnull(source.[DateReferredToAdultRenalServices], getdate())
	and isnull(target.[DateRegisteredWithBAPN], getdate()) = isnull(source.[DateRegisteredWithBAPN], getdate())
	and isnull(target.[PrimaryHDScheduleObjectID], 0) = isnull(source.[PrimaryHDScheduleObjectID], 0)
	and isnull(target.[PrimaryVascularAccessObjectID], 0) = isnull(source.[PrimaryVascularAccessObjectID], 0)
)
then update set
	target.[FirstName] = source.[FirstName]
	,target.[MiddleName] = source.[MiddleName]
	,target.[LastName] = source.[LastName]
	,target.[Title] = source.[Title]
	,target.[FullName] = source.[FullName]
	,target.[ExtendedName] = source.[ExtendedName]
	,target.[ShortName] = source.[ShortName]
	,target.[Initials] = source.[Initials]
	,target.[Signature] = source.[Signature]
	,target.[MedicalRecordNo] = source.[MedicalRecordNo]
	,target.[NationalHealthServiceNo] = source.[NationalHealthServiceNo]
	,target.[DateOfBirth] = source.[DateOfBirth]
	,target.[BirthPlace] = source.[BirthPlace]
	,target.[DateOfDeath] = source.[DateOfDeath]
	,target.[CauseOfDeath] = source.[CauseOfDeath]
	,target.[EMailAddress] = source.[EMailAddress]
	,target.[MaritalStatus] = source.[MaritalStatus]
	,target.[Age] = source.[Age]
	,target.[Sex] = source.[Sex]
	,target.[EducationLevel] = source.[EducationLevel]
	,target.[PrimaryPhone] = source.[PrimaryPhone]
	,target.[PrimaryPhoneObjectID] = source.[PrimaryPhoneObjectID]
	,target.[MailingAddress] = source.[MailingAddress]
	,target.[MailingAddressObjectID] = source.[MailingAddressObjectID]
	,target.[Race] = source.[Race]
	,target.[Ethnicity] = source.[Ethnicity]
	,target.[Religion] = source.[Religion]
	,target.[BloodGroup] = source.[BloodGroup]
	,target.[BloodRhesus] = source.[BloodRhesus]
	,target.[DiabeticStatus] = source.[DiabeticStatus]
	,target.[KidneyFunction] = source.[KidneyFunction]
	,target.[PrimaryCaregiver] = source.[PrimaryCaregiver]
	,target.[MedicalPractice] = source.[MedicalPractice]
	,target.[MedicalPracticeObjectID] = source.[MedicalPracticeObjectID]
	,target.[MedicalPracticeAddress] = source.[MedicalPracticeAddress]
	,target.[GeneralPractitioner] = source.[GeneralPractitioner]
	,target.[GeneralPractitionerFirstName] = source.[GeneralPractitionerFirstName]
	,target.[GeneralPractitionerLastName] = source.[GeneralPractitionerLastName]
	,target.[HearingImpairment] = source.[HearingImpairment]
	,target.[SightImpairment] = source.[SightImpairment]
	,target.[SpeechImpairment] = source.[SpeechImpairment]
	,target.[InterpreterRequired] = source.[InterpreterRequired]
	,target.[IsAmbulatory] = source.[IsAmbulatory]
	,target.[AmbulatoryStatus] = source.[AmbulatoryStatus]
	,target.[WheelchairStatus] = source.[WheelchairStatus]
	,target.[OxygenRequired] = source.[OxygenRequired]
	,target.[PrimaryCareGiverObjectID] = source.[PrimaryCareGiverObjectID]
	,target.[GeneralPractitionerObjectID] = source.[GeneralPractitionerObjectID]
	,target.[PrimaryPharmacyObjectID] = source.[PrimaryPharmacyObjectID]
	,target.[PrimaryTransportObjectID] = source.[PrimaryTransportObjectID]
	,target.[VeteranStatus] = source.[VeteranStatus]
	,target.[AdminCategory] = source.[AdminCategory]
	,target.[PrimaryContactPersonName] = source.[PrimaryContactPersonName]
	,target.[PrimaryContactPersonObjectID] = source.[PrimaryContactPersonObjectID]
	,target.[PrimaryRenalModalityStartDate] = source.[PrimaryRenalModalityStartDate]
	,target.[PrimaryRenalModalityStopDate] = source.[PrimaryRenalModalityStopDate]
	,target.[PrimaryRenalModality] = source.[PrimaryRenalModality]
	,target.[PrimaryRenalModalitySetting] = source.[PrimaryRenalModalitySetting]
	,target.[PrimaryRenalModalityDialysisProvider] = source.[PrimaryRenalModalityDialysisProvider]
	,target.[PrimaryRenalModalityObjectID] = source.[PrimaryRenalModalityObjectID]
	,target.[PrimaryRenalModalityDialysisProviderObjectID] = source.[PrimaryRenalModalityDialysisProviderObjectID]
	,target.[PrimaryKidneyTransplantStatusObjectID] = source.[PrimaryKidneyTransplantStatusObjectID]
	,target.[IsolationStatus] = source.[IsolationStatus]
	,target.[IsActive] = source.[IsActive]
	,target.[RegistrationDate] = source.[RegistrationDate]
	,target.[InactiveDate] = source.[InactiveDate]
	,target.[ReasonInactive] = source.[ReasonInactive]
	,target.[PrimaryNephrologist] = source.[PrimaryNephrologist]
	,target.[PrimaryNephrologistObjectID] = source.[PrimaryNephrologistObjectID]
	,target.[WeeklyCalendar] = source.[WeeklyCalendar]
	,target.[PrimaryRenalDiagnosis] = source.[PrimaryRenalDiagnosis]
	,target.[RenalDiagnosisDateOfOnset] = source.[RenalDiagnosisDateOfOnset]
	,target.[OtherRenalDiagnosis] = source.[OtherRenalDiagnosis]
	,target.[RenalDiagnosisCode] = source.[RenalDiagnosisCode]
	,target.[RenalDiagnosisDescription] = source.[RenalDiagnosisDescription]
	,target.[RegistrationHeight] = source.[RegistrationHeight]
	,target.[RegistrationHeightUnits] = source.[RegistrationHeightUnits]
	,target.[RegistrationHeightInCm] = source.[RegistrationHeightInCm]
	,target.[RegistrationWeight] = source.[RegistrationWeight]
	,target.[RegistrationWeightUnits] = source.[RegistrationWeightUnits]
	,target.[RegistrationWeightInKg] = source.[RegistrationWeightInKg]
	,target.[Remarks] = source.[Remarks]
	,target.[DateFirstSeenByPhysician] = source.[DateFirstSeenByPhysician]
	,target.[PreDialysisFollowUp] = source.[PreDialysisFollowUp]
	,target.[PreDialysisErythropoietin] = source.[PreDialysisErythropoietin]
	,target.[UKRROptOut] = source.[UKRROptOut]
	,target.[PediatricPatient] = source.[PediatricPatient]
	,target.[GeneticRelationshipBetweenParents] = source.[GeneticRelationshipBetweenParents]
	,target.[SingleAdultResponsible] = source.[SingleAdultResponsible]
	,target.[OtherFamilyMembersAlsoESRF] = source.[OtherFamilyMembersAlsoESRF]
	,target.[OtherFamilyMembersSameRenalDisease] = source.[OtherFamilyMembersSameRenalDisease]
	,target.[DateReferredToAdultRenalServices] = source.[DateReferredToAdultRenalServices]
	,target.[DateRegisteredWithBAPN] = source.[DateRegisteredWithBAPN]
	,target.[PrimaryHDScheduleObjectID] = source.[PrimaryHDScheduleObjectID]
	,target.[PrimaryVascularAccessObjectID] = source.[PrimaryVascularAccessObjectID]
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	SourceUniqueID
	,[FirstName]
	,[MiddleName]
	,[LastName]
	,[Title]
	,[FullName]
	,[ExtendedName]
	,[ShortName]
	,[Initials]
	,[Signature]
	,[MedicalRecordNo]
	,[NationalHealthServiceNo]
	,[DateOfBirth]
	,[BirthPlace]
	,[DateOfDeath]
	,[CauseOfDeath]
	,[EMailAddress]
	,[MaritalStatus]
	,[Age]
	,[Sex]
	,[EducationLevel]
	,[PrimaryPhone]
	,[PrimaryPhoneObjectID]
	,[MailingAddress]
	,[MailingAddressObjectID]
	,[Race]
	,[Ethnicity]
	,[Religion]
	,[BloodGroup]
	,[BloodRhesus]
	,[DiabeticStatus]
	,[KidneyFunction]
	,[PrimaryCaregiver]
	,[MedicalPractice]
	,[MedicalPracticeObjectID]
	,[MedicalPracticeAddress]
	,[GeneralPractitioner]
	,[GeneralPractitionerFirstName]
	,[GeneralPractitionerLastName]
	,[HearingImpairment]
	,[SightImpairment]
	,[SpeechImpairment]
	,[InterpreterRequired]
	,[IsAmbulatory]
	,[AmbulatoryStatus]
	,[WheelchairStatus]
	,[OxygenRequired]
	,[PrimaryCareGiverObjectID]
	,[GeneralPractitionerObjectID]
	,[PrimaryPharmacyObjectID]
	,[PrimaryTransportObjectID]
	,[VeteranStatus]
	,[AdminCategory]
	,[PrimaryContactPersonName]
	,[PrimaryContactPersonObjectID]
	,[PrimaryRenalModalityStartDate]
	,[PrimaryRenalModalityStopDate]
	,[PrimaryRenalModality]
	,[PrimaryRenalModalitySetting]
	,[PrimaryRenalModalityDialysisProvider]
	,[PrimaryRenalModalityObjectID]
	,[PrimaryRenalModalityDialysisProviderObjectID]
	,[PrimaryKidneyTransplantStatusObjectID]
	,[IsolationStatus]
	,[IsActive]
	,[RegistrationDate]
	,[InactiveDate]
	,[ReasonInactive]
	,[PrimaryNephrologist]
	,[PrimaryNephrologistObjectID]
	,[WeeklyCalendar]
	,[PrimaryRenalDiagnosis]
	,[RenalDiagnosisDateOfOnset]
	,[OtherRenalDiagnosis]
	,[RenalDiagnosisCode]
	,[RenalDiagnosisDescription]
	,[RegistrationHeight]
	,[RegistrationHeightUnits]
	,[RegistrationHeightInCm]
	,[RegistrationWeight]
	,[RegistrationWeightUnits]
	,[RegistrationWeightInKg]
	,[Remarks]
	,[DateFirstSeenByPhysician]
	,[PreDialysisFollowUp]
	,[PreDialysisErythropoietin]
	,[UKRROptOut]
	,[PediatricPatient]
	,[GeneticRelationshipBetweenParents]
	,[SingleAdultResponsible]
	,[OtherFamilyMembersAlsoESRF]
	,[OtherFamilyMembersSameRenalDisease]
	,[DateReferredToAdultRenalServices]
	,[DateRegisteredWithBAPN]
	,[PrimaryHDScheduleObjectID]
	,[PrimaryVascularAccessObjectID]
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.SourceUniqueID
	,source.[FirstName]
	,source.[MiddleName]
	,source.[LastName]
	,source.[Title]
	,source.[FullName]
	,source.[ExtendedName]
	,source.[ShortName]
	,source.[Initials]
	,source.[Signature]
	,source.[MedicalRecordNo]
	,source.[NationalHealthServiceNo]
	,source.[DateOfBirth]
	,source.[BirthPlace]
	,source.[DateOfDeath]
	,source.[CauseOfDeath]
	,source.[EMailAddress]
	,source.[MaritalStatus]
	,source.[Age]
	,source.[Sex]
	,source.[EducationLevel]
	,source.[PrimaryPhone]
	,source.[PrimaryPhoneObjectID]
	,source.[MailingAddress]
	,source.[MailingAddressObjectID]
	,source.[Race]
	,source.[Ethnicity]
	,source.[Religion]
	,source.[BloodGroup]
	,source.[BloodRhesus]
	,source.[DiabeticStatus]
	,source.[KidneyFunction]
	,source.[PrimaryCaregiver]
	,source.[MedicalPractice]
	,source.[MedicalPracticeObjectID]
	,source.[MedicalPracticeAddress]
	,source.[GeneralPractitioner]
	,source.[GeneralPractitionerFirstName]
	,source.[GeneralPractitionerLastName]
	,source.[HearingImpairment]
	,source.[SightImpairment]
	,source.[SpeechImpairment]
	,source.[InterpreterRequired]
	,source.[IsAmbulatory]
	,source.[AmbulatoryStatus]
	,source.[WheelchairStatus]
	,source.[OxygenRequired]
	,source.[PrimaryCareGiverObjectID]
	,source.[GeneralPractitionerObjectID]
	,source.[PrimaryPharmacyObjectID]
	,source.[PrimaryTransportObjectID]
	,source.[VeteranStatus]
	,source.[AdminCategory]
	,source.[PrimaryContactPersonName]
	,source.[PrimaryContactPersonObjectID]
	,source.[PrimaryRenalModalityStartDate]
	,source.[PrimaryRenalModalityStopDate]
	,source.[PrimaryRenalModality]
	,source.[PrimaryRenalModalitySetting]
	,source.[PrimaryRenalModalityDialysisProvider]
	,source.[PrimaryRenalModalityObjectID]
	,source.[PrimaryRenalModalityDialysisProviderObjectID]
	,source.[PrimaryKidneyTransplantStatusObjectID]
	,source.[IsolationStatus]
	,source.[IsActive]
	,source.[RegistrationDate]
	,source.[InactiveDate]
	,source.[ReasonInactive]
	,source.[PrimaryNephrologist]
	,source.[PrimaryNephrologistObjectID]
	,source.[WeeklyCalendar]
	,source.[PrimaryRenalDiagnosis]
	,source.[RenalDiagnosisDateOfOnset]
	,source.[OtherRenalDiagnosis]
	,source.[RenalDiagnosisCode]
	,source.[RenalDiagnosisDescription]
	,source.[RegistrationHeight]
	,source.[RegistrationHeightUnits]
	,source.[RegistrationHeightInCm]
	,source.[RegistrationWeight]
	,source.[RegistrationWeightUnits]
	,source.[RegistrationWeightInKg]
	,source.[Remarks]
	,source.[DateFirstSeenByPhysician]
	,source.[PreDialysisFollowUp]
	,source.[PreDialysisErythropoietin]
	,source.[UKRROptOut]
	,source.[PediatricPatient]
	,source.[GeneticRelationshipBetweenParents]
	,source.[SingleAdultResponsible]
	,source.[OtherFamilyMembersAlsoESRF]
	,source.[OtherFamilyMembersSameRenalDisease]
	,source.[DateReferredToAdultRenalServices]
	,source.[DateRegisteredWithBAPN]
	,source.[PrimaryHDScheduleObjectID]
	,source.[PrimaryVascularAccessObjectID]
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

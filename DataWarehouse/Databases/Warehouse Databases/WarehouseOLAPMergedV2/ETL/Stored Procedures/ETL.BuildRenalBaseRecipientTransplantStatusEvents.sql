﻿
CREATE procedure [ETL].[BuildRenalBaseRecipientTransplantStatusEvents]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||CV'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Renal.BaseRecipientTransplantStatusEvents target
using [$(Warehouse)].Renal.RecipientTransplantStatusEvents source
on	source.RecipientTransplantStatusEventsRecno = target.RecipientTransplantStatusEventsRecno

when matched and not
(
	isnull(target.[SourceUniqueID], '') = isnull(source.[SourceUniqueID], '')
	and isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[StartDate], getdate()) = isnull(source.[StartDate], getdate())
	and isnull(target.[StopDate], getdate()) = isnull(source.[StopDate], getdate())
	and isnull(target.[DaysElapsed], 0) = isnull(source.[DaysElapsed], 0)
	and isnull(target.[AgeAtEvent], 0) = isnull(source.[AgeAtEvent], 0)
	and isnull(target.[TimelineEvent], '') = isnull(source.[TimelineEvent], '')
	and isnull(target.[TimelineEventDetailCode], '') = isnull(source.[TimelineEventDetailCode], '')
	and isnull(target.[InformationSource], '') = isnull(source.[InformationSource], '')
	and isnull(target.[Notes], '') = isnull(source.[Notes], '')
	and isnull(target.[RecipientUKTNumber], '') = isnull(source.[RecipientUKTNumber], '')
	and isnull(target.[ReferralObjectID], 0) = isnull(source.[ReferralObjectID], 0)
	and isnull(target.[ReferralStartDate], getdate()) = isnull(source.[ReferralStartDate], getdate())
	and isnull(target.[ReferralStopDate], getdate()) = isnull(source.[ReferralStopDate], getdate())
	and isnull(target.[ConsultantObjectID], 0) = isnull(source.[ConsultantObjectID], 0)
	and isnull(target.[Consultant], '') = isnull(source.[Consultant], '')
	and isnull(target.[Provider], '') = isnull(source.[Provider], '')
	and isnull(target.[DonorType], '') = isnull(source.[DonorType], '')
	and isnull(target.[TransplantPriority], '') = isnull(source.[TransplantPriority], '')
	and isnull(target.[StatusDetail], '') = isnull(source.[StatusDetail], '')
	and isnull(target.[StatusReason], '') = isnull(source.[StatusReason], '')
)
then update set
	target.[SourceUniqueID] = source.[SourceUniqueID]
	,target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[StartDate] = source.[StartDate]
	,target.[StopDate] = source.[StopDate]
	,target.[DaysElapsed] = source.[DaysElapsed]
	,target.[AgeAtEvent] = source.[AgeAtEvent]
	,target.[TimelineEvent] = source.[TimelineEvent]
	,target.[TimelineEventDetailCode] = source.[TimelineEventDetailCode]
	,target.[InformationSource] = source.[InformationSource]
	,target.[Notes] = source.[Notes]
	,target.[RecipientUKTNumber] = source.[RecipientUKTNumber]
	,target.[ReferralObjectID] = source.[ReferralObjectID]
	,target.[ReferralStartDate] = source.[ReferralStartDate]
	,target.[ReferralStopDate] = source.[ReferralStopDate]
	,target.[ConsultantObjectID] = source.[ConsultantObjectID]
	,target.[Consultant] = source.[Consultant]
	,target.[Provider] = source.[Provider]
	,target.[DonorType] = source.[DonorType]
	,target.[TransplantPriority] = source.[TransplantPriority]
	,target.[StatusDetail] = source.[StatusDetail]
	,target.[StatusReason] = source.[StatusReason]	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	 [SourceUniqueID]
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[StartDate]
	,[StopDate]
	,[DaysElapsed]
	,[AgeAtEvent]
	,[TimelineEvent]
	,[TimelineEventDetailCode]
	,[InformationSource]
	,[Notes]
	,[RecipientUKTNumber]
	,[ReferralObjectID]
	,[ReferralStartDate]
	,[ReferralStopDate]
	,[ConsultantObjectID]
	,[Consultant]
	,[Provider]
	,[DonorType]
	,[TransplantPriority]
	,[StatusDetail]
	,[StatusReason]
	,[RecipientTransplantStatusEventsRecno]
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[SourceUniqueID]
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[StartDate]
	,source.[StopDate]
	,source.[DaysElapsed]
	,source.[AgeAtEvent]
	,source.[TimelineEvent]
	,source.[TimelineEventDetailCode]
	,source.[InformationSource]
	,source.[Notes]
	,source.[RecipientUKTNumber]
	,source.[ReferralObjectID]
	,source.[ReferralStartDate]
	,source.[ReferralStopDate]
	,source.[ConsultantObjectID]
	,source.[Consultant]
	,source.[Provider]
	,source.[DonorType]
	,source.[TransplantPriority]
	,source.[StatusDetail]
	,source.[StatusReason]
	,source.[RecipientTransplantStatusEventsRecno]
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

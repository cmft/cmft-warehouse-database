﻿create procedure [ETL].[BuildCenAPCBaseAugmentedCarePeriod]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	APC.BaseAugmentedCarePeriod target
using
	(
	select
		 EncounterRecno = cast(EncounterRecno as int)
		,SourceUniqueID = cast(SourceUniqueID as varchar)
		,SourcePatientNo = cast(SourcePatientNo as varchar)
		,SourceSpellNo = cast(SourceSpellNo as varchar)
		,ProviderSpellNo = cast(ProviderSpellNo as varchar)
		,ConsultantCode = cast(ConsultantCode as varchar)
		,AcpDisposalCode = cast(AcpDisposalCode as varchar)
		,LocalIdentifier = cast(LocalIdentifier as varchar)
		,LocationCode = cast(LocationCode as varchar)
		,OutcomeIndicator = cast(OutcomeIndicator as varchar)
		,Source = cast(Source as varchar)
		,SpecialtyCode = cast(SpecialtyCode as varchar)
		,Status = cast(Status as varchar)
		,StartDate = cast(StartDate as date)
		,StartTime = cast(StartTime as datetime)
		,EndDate = cast(EndDate as date)
		,EndTime = cast(EndTime as datetime)
		,AdvancedRespiratorySystemIndicator = cast(AdvancedRespiratorySystemIndicator as varchar)
		,BasicRespiratorySystemIndicator = cast(BasicRespiratorySystemIndicator as varchar)
		,CirculatorySystemIndicator = cast(CirculatorySystemIndicator as varchar)
		,NeurologicalSystemIndicator = cast(NeurologicalSystemIndicator as varchar)
		,RenalSystemIndicator = cast(RenalSystemIndicator as varchar)
		,NoOfSupportSystemsUsed = cast(NoOfSupportSystemsUsed as varchar)
		,HighDependencyCareLevelDays = cast(HighDependencyCareLevelDays as varchar)
		,IntensiveCareLevelDays = cast(IntensiveCareLevelDays as varchar)
		,PlannedAcpPeriodIndicator = cast(PlannedAcpPeriodIndicator as varchar)
		,ReviseByUserId = cast(ReviseByUserId as varchar)
		,ReviseTime = cast(ReviseTime as varchar)
		,ContextCode = cast(ContextCode as varchar)
	from
		ETL.TLoadCenAPCBaseAugmentedCarePeriod Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.ContextCode = 'CEN||PAS'
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,ProviderSpellNo
			,ConsultantCode
			,AcpDisposalCode
			,LocalIdentifier
			,LocationCode
			,OutcomeIndicator
			,Source
			,SpecialtyCode
			,Status
			,StartDate
			,StartTime
			,EndDate
			,EndTime
			,AdvancedRespiratorySystemIndicator
			,BasicRespiratorySystemIndicator
			,CirculatorySystemIndicator
			,NeurologicalSystemIndicator
			,RenalSystemIndicator
			,NoOfSupportSystemsUsed
			,HighDependencyCareLevelDays
			,IntensiveCareLevelDays
			,PlannedAcpPeriodIndicator
			,ReviseByUserId
			,ReviseTime
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 EncounterRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,ProviderSpellNo
			,ConsultantCode
			,AcpDisposalCode
			,LocalIdentifier
			,LocationCode
			,OutcomeIndicator
			,Source
			,SpecialtyCode
			,Status
			,StartDate
			,StartTime
			,EndDate
			,EndTime
			,AdvancedRespiratorySystemIndicator
			,BasicRespiratorySystemIndicator
			,CirculatorySystemIndicator
			,NeurologicalSystemIndicator
			,RenalSystemIndicator
			,NoOfSupportSystemsUsed
			,HighDependencyCareLevelDays
			,IntensiveCareLevelDays
			,PlannedAcpPeriodIndicator
			,ReviseByUserId
			,ReviseTime
			,ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.ProviderSpellNo, '') = isnull(source.ProviderSpellNo, '')
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.AcpDisposalCode, '') = isnull(source.AcpDisposalCode, '')
		and isnull(target.LocalIdentifier, '') = isnull(source.LocalIdentifier, '')
		and isnull(target.LocationCode, '') = isnull(source.LocationCode, '')
		and isnull(target.OutcomeIndicator, '') = isnull(source.OutcomeIndicator, '')
		and isnull(target.Source, '') = isnull(source.Source, '')
		and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		and isnull(target.Status, '') = isnull(source.Status, '')
		and isnull(target.StartDate, getdate()) = isnull(source.StartDate, getdate())
		and isnull(target.StartTime, getdate()) = isnull(source.StartTime, getdate())
		and isnull(target.EndDate, getdate()) = isnull(source.EndDate, getdate())
		and isnull(target.EndTime, getdate()) = isnull(source.EndTime, getdate())
		and isnull(target.AdvancedRespiratorySystemIndicator, '') = isnull(source.AdvancedRespiratorySystemIndicator, '')
		and isnull(target.BasicRespiratorySystemIndicator, '') = isnull(source.BasicRespiratorySystemIndicator, '')
		and isnull(target.CirculatorySystemIndicator, '') = isnull(source.CirculatorySystemIndicator, '')
		and isnull(target.NeurologicalSystemIndicator, '') = isnull(source.NeurologicalSystemIndicator, '')
		and isnull(target.RenalSystemIndicator, '') = isnull(source.RenalSystemIndicator, '')
		and isnull(target.NoOfSupportSystemsUsed, '') = isnull(source.NoOfSupportSystemsUsed, '')
		and isnull(target.HighDependencyCareLevelDays, '') = isnull(source.HighDependencyCareLevelDays, '')
		and isnull(target.IntensiveCareLevelDays, '') = isnull(source.IntensiveCareLevelDays, '')
		and isnull(target.PlannedAcpPeriodIndicator, '') = isnull(source.PlannedAcpPeriodIndicator, '')
		and isnull(target.ReviseByUserId, '') = isnull(source.ReviseByUserId, '')
		and isnull(target.ReviseTime, '') = isnull(source.ReviseTime, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.ProviderSpellNo = source.ProviderSpellNo
			,target.ConsultantCode = source.ConsultantCode
			,target.AcpDisposalCode = source.AcpDisposalCode
			,target.LocalIdentifier = source.LocalIdentifier
			,target.LocationCode = source.LocationCode
			,target.OutcomeIndicator = source.OutcomeIndicator
			,target.Source = source.Source
			,target.SpecialtyCode = source.SpecialtyCode
			,target.Status = source.Status
			,target.StartDate = source.StartDate
			,target.StartTime = source.StartTime
			,target.EndDate = source.EndDate
			,target.EndTime = source.EndTime
			,target.AdvancedRespiratorySystemIndicator = source.AdvancedRespiratorySystemIndicator
			,target.BasicRespiratorySystemIndicator = source.BasicRespiratorySystemIndicator
			,target.CirculatorySystemIndicator = source.CirculatorySystemIndicator
			,target.NeurologicalSystemIndicator = source.NeurologicalSystemIndicator
			,target.RenalSystemIndicator = source.RenalSystemIndicator
			,target.NoOfSupportSystemsUsed = source.NoOfSupportSystemsUsed
			,target.HighDependencyCareLevelDays = source.HighDependencyCareLevelDays
			,target.IntensiveCareLevelDays = source.IntensiveCareLevelDays
			,target.PlannedAcpPeriodIndicator = source.PlannedAcpPeriodIndicator
			,target.ReviseByUserId = source.ReviseByUserId
			,target.ReviseTime = source.ReviseTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

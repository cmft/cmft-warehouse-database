﻿

CREATE procedure [ETL].[BuildHRBaseSummary]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge HR.BaseSummary target
using 
	(
	select
		SummaryRecno
		,OrganisationCode
		,WardCode
		,CensusDate
		,AppraisalRequired
		,AppraisalCompleted
		,ClinicalMandatoryTrainingRequired
		,ClinicalMandatoryTrainingCompleted
		,CorporateMandatoryTrainingRequired
		,CorporateMandatoryTrainingCompleted
		,RecentAppointment
		,LongTermAppointment
		,SicknessAbsenceFTE
		,SicknessEstablishmentFTE
		,Headcount3mthRolling
		,FTE3mthRolling
		,StartersHeadcount3mthRolling
		,StartersFTE3mthRolling
		,LeaversHeadcount3mthRolling
		,LeaversFTE3mthRolling
		,BudgetWTE
		,ContractedWTE
		,ClinicalBudgetWTE
		,ClinicalContractedWTE
		,ACAgencySpend
		,InterfaceCode
		,ContextCode
		,Created
		,Updated
		,ByWhom	
		,HeadcountMonthly
		,FTEMonthly
		,StartersHeadcountMonthly
		,StartersFTEMonthly
		,LeaversHeadcountMonthly
		,LeaversFTEMonthly
		,BMERecentAppointment
		,BMELongTermAppointment
		,ESRSIP 
		,GeneralLedgerEst	
		,ESRSIPBand5 
		,GeneralLedgerEstBand5 	
		
	from
		ETL.TLoadHRBaseSummary

		) source
	on	source.SummaryRecno = target.SummaryRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CMFT||ESREXT'
	then delete

	when not matched 
	then
		insert
			(
			SummaryRecno
			,OrganisationCode
			,WardCode
			,CensusDate
			,AppraisalRequired
			,AppraisalCompleted
			,ClinicalMandatoryTrainingRequired
			,ClinicalMandatoryTrainingCompleted
			,CorporateMandatoryTrainingRequired
			,CorporateMandatoryTrainingCompleted
			,RecentAppointment
			,LongTermAppointment
			,SicknessAbsenceFTE
			,SicknessEstablishmentFTE
			,Headcount3mthRolling
			,FTE3mthRolling
			,StartersHeadcount3mthRolling
			,StartersFTE3mthRolling
			,LeaversHeadcount3mthRolling
			,LeaversFTE3mthRolling
			,BudgetWTE
			,ContractedWTE
			,ClinicalBudgetWTE
			,ClinicalContractedWTE
			,ACAgencySpend
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			,HeadcountMonthly
			,FTEMonthly
			,StartersHeadcountMonthly
			,StartersFTEMonthly
			,LeaversHeadcountMonthly
			,LeaversFTEMonthly
			,BMERecentAppointment
			,BMELongTermAppointment
			,ESRSIP 
			,GeneralLedgerEst	
			,ESRSIPBand5 
			,GeneralLedgerEstBand5 
			)

		values
			(
			 source.SummaryRecno
			,source.OrganisationCode
			,source.WardCode
			,source.CensusDate
			,source.AppraisalRequired
			,source.AppraisalCompleted
			,source.ClinicalMandatoryTrainingRequired
			,source.ClinicalMandatoryTrainingCompleted
			,source.CorporateMandatoryTrainingRequired
			,source.CorporateMandatoryTrainingCompleted
			,source.RecentAppointment
			,source.LongTermAppointment
			,source.SicknessAbsenceFTE
			,source.SicknessEstablishmentFTE
			,source.Headcount3mthRolling
			,source.FTE3mthRolling
			,source.StartersHeadcount3mthRolling
			,source.StartersFTE3mthRolling
			,source.LeaversHeadcount3mthRolling
			,source.LeaversFTE3mthRolling
			,source.BudgetWTE
			,source.ContractedWTE
			,source.ClinicalBudgetWTE
			,source.ClinicalContractedWTE
			,source.ACAgencySpend
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			,source.HeadcountMonthly
			,source.FTEMonthly
			,source.StartersHeadcountMonthly
			,source.StartersFTEMonthly
			,source.LeaversHeadcountMonthly
			,source.LeaversFTEMonthly
			,source.BMERecentAppointment
			,source.BMELongTermAppointment
			,source.ESRSIP 
			,source.GeneralLedgerEst	
			,source.ESRSIPBand5 
			,source.GeneralLedgerEstBand5 
			)

	when matched
	and not
		(
			
			isnull(target.SummaryRecno, 0) = isnull(source.SummaryRecno, 0)
		and	isnull(target.OrganisationCode, 0) = isnull(source.OrganisationCode, 0)
		and isnull(target.WardCode, 0) = isnull(source.WardCode, 0)
		and isnull(target.CensusDate, getdate()) = isnull(source.CensusDate, getdate())
		and isnull(target.AppraisalRequired, 0) = isnull(source.AppraisalRequired, 0)
		and isnull(target.AppraisalCompleted, 0) = isnull(source.AppraisalCompleted, 0)
		and isnull(target.ClinicalMandatoryTrainingRequired, 0) = isnull(source.ClinicalMandatoryTrainingRequired, 0)
		and isnull(target.ClinicalMandatoryTrainingCompleted, 0) = isnull(source.ClinicalMandatoryTrainingCompleted, 0)
		and isnull(target.CorporateMandatoryTrainingRequired, 0) = isnull(source.CorporateMandatoryTrainingRequired, 0)
		and isnull(target.CorporateMandatoryTrainingCompleted, 0) = isnull(source.CorporateMandatoryTrainingCompleted, 0)
		and isnull(target.RecentAppointment, 0) = isnull(source.RecentAppointment, 0)
		and isnull(target.LongTermAppointment, 0) = isnull(source.LongTermAppointment, 0)
		and isnull(target.SicknessAbsenceFTE, 0) = isnull(source.SicknessAbsenceFTE, 0)
		and isnull(target.SicknessEstablishmentFTE, 0) = isnull(source.SicknessEstablishmentFTE, 0)
		and isnull(target.Headcount3mthRolling, 0) = isnull(source.Headcount3mthRolling, 0)
		and isnull(target.FTE3mthRolling, 0) = isnull(source.FTE3mthRolling, 0)
		and isnull(target.StartersHeadcount3mthRolling, 0) = isnull(source.StartersHeadcount3mthRolling, 0)
		and isnull(target.StartersFTE3mthRolling, 0) = isnull(source.StartersFTE3mthRolling, 0)
		and isnull(target.LeaversHeadcount3mthRolling, 0) = isnull(source.LeaversHeadcount3mthRolling, 0)
		and isnull(target.LeaversFTE3mthRolling, 0) = isnull(source.LeaversFTE3mthRolling, 0)
		and isnull(target.BudgetWTE, 0) = isnull(source.BudgetWTE, 0)
		and isnull(target.ContractedWTE, 0) = isnull(source.ContractedWTE, 0)
		and isnull(target.ClinicalBudgetWTE, 0) = isnull(source.ClinicalBudgetWTE, 0)
		and isnull(target.ClinicalContractedWTE, 0) = isnull(source.ClinicalContractedWTE, 0)
		and isnull(target.ACAgencySpend, 0) = isnull(source.ACAgencySpend, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.HeadcountMonthly, 0) = isnull(source.HeadcountMonthly, 0)
		and isnull(target.FTEMonthly, 0) = isnull(source.FTEMonthly, 0)
		and isnull(target.StartersHeadcountMonthly, 0) = isnull(source.StartersHeadcountMonthly, 0)
		and isnull(target.StartersFTEMonthly, 0) = isnull(source.StartersFTEMonthly, 0)
		and isnull(target.LeaversHeadcountMonthly, 0) = isnull(source.LeaversHeadcountMonthly, 0)
		and isnull(target.LeaversFTEMonthly, 0) = isnull(source.LeaversFTEMonthly, 0)
		and isnull(target.BMERecentAppointment, 0) = isnull(source.BMERecentAppointment, 0)
		and isnull(target.BMELongTermAppointment, 0) = isnull(source.BMELongTermAppointment, 0)
		
		and isnull(target.ESRSIP, 0) = isnull(source.ESRSIP, 0)
		and isnull(target.GeneralLedgerEst, 0) = isnull(source.GeneralLedgerEst, 0)
		and isnull(target.ESRSIPBand5, 0) = isnull(source.ESRSIPBand5, 0)
		and isnull(target.GeneralLedgerEstBand5, 0) = isnull(source.GeneralLedgerEstBand5, 0)
		
		)
	
	then 
		update 
		set
			target.OrganisationCode = source.OrganisationCode 
			,target.WardCode = source.WardCode
			,target.CensusDate = source.CensusDate
			,target.AppraisalRequired = source.AppraisalRequired
			,target.AppraisalCompleted = source.AppraisalCompleted
			,target.ClinicalMandatoryTrainingRequired = source.ClinicalMandatoryTrainingRequired
			,target.ClinicalMandatoryTrainingCompleted = source.ClinicalMandatoryTrainingCompleted
			,target.CorporateMandatoryTrainingRequired = source.CorporateMandatoryTrainingRequired
			,target.CorporateMandatoryTrainingCompleted = source.CorporateMandatoryTrainingCompleted
			,target.RecentAppointment = source.RecentAppointment
			,target.LongTermAppointment = source.LongTermAppointment
			,target.SicknessAbsenceFTE = source.SicknessAbsenceFTE
			,target.SicknessEstablishmentFTE = source.SicknessEstablishmentFTE
			,target.Headcount3mthRolling = source.Headcount3mthRolling
			,target.FTE3mthRolling = source.FTE3mthRolling
			,target.StartersHeadcount3mthRolling = source.StartersHeadcount3mthRolling
			,target.StartersFTE3mthRolling = source.StartersFTE3mthRolling
			,target.LeaversHeadcount3mthRolling = source.LeaversHeadcount3mthRolling
			,target.LeaversFTE3mthRolling = source.LeaversFTE3mthRolling
			,target.BudgetWTE = source.BudgetWTE
			,target.ContractedWTE = source.ContractedWTE
			,target.ClinicalBudgetWTE = source.ClinicalBudgetWTE
			,target.ClinicalContractedWTE = source.ClinicalContractedWTE
			,target.ACAgencySpend = source.ACAgencySpend
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Created = source.Created
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.HeadcountMonthly = source.HeadcountMonthly
			,target.FTEMonthly = source.FTEMonthly
			,target.StartersHeadcountMonthly = source.StartersHeadcountMonthly
			,target.StartersFTEMonthly = source.StartersFTEMonthly
			,target.LeaversHeadcountMonthly = source.LeaversHeadcountMonthly
			,target.LeaversFTEMonthly = source.LeaversFTEMonthly
			,target.BMERecentAppointment = source.BMERecentAppointment
			,target.BMELongTermAppointment = source.BMELongTermAppointment
			
			,target.ESRSIP = source.ESRSIP
			,target.GeneralLedgerEst = source.GeneralLedgerEst
			,target.ESRSIPBand5 = source.ESRSIPBand5
			,target.GeneralLedgerEstBand5 = source.GeneralLedgerEstBand5

		
output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


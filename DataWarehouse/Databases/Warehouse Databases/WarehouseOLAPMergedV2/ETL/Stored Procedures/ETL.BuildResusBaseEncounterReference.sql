﻿

CREATE procedure [ETL].[BuildResusBaseEncounterReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Resus.BaseEncounterReference
			where
				BaseEncounterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Resus.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		Resus.BaseEncounter
	where
		BaseEncounter.MergeRecno = BaseEncounterReference.MergeRecno
	)
and	BaseEncounterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Resus.BaseEncounterReference target
using
	(
	select
		BaseEncounter.MergeRecno
		,ContextID = Context.ContextID
		,AdmissionReasonID
		,AdmissionDateID =
			coalesce(
				 AdmissionDate.DateID

				,case
				when BaseEncounter.AdmissionDate is null
				then NullDate.DateID

				when BaseEncounter.AdmissionDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,PresentingECGID = null
		,CallOutReasonID = CallOutReason.SourceCallOutReasonID
		,CallOutDateID =
			coalesce(
				 CallOutDate.DateID

				,case
				when BaseEncounter.CallOutTime is null
				then NullDate.DateID

				when BaseEncounter.CallOutTime < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,LocationID = Location.SourceLocationID
		,RespondingTeamID = null
		,OutcomeID = null
		,OutcomeDateID = null
		,FinalOutcomeID = null
		,FinalOutcomeDateID = null
		,AuditTypeID = AuditType.SourceAuditTypeID
		,BaseEncounter.Created
		,BaseEncounter.Updated
		,BaseEncounter.ByWhom
	from
		Resus.BaseEncounter

	inner join WH.Context
	on	Context.ContextCode = BaseEncounter.ContextCode

	inner join WH.Location
	on	Location.SourceLocationCode  = coalesce(BaseEncounter.LocationID, '-1')
	and	Location.SourceContextCode = BaseEncounter.ContextCode

	inner join Resus.CallOutReason
	on	CallOutReason.SourceCallOutReasonCode  = coalesce(BaseEncounter.CallOutReasonID, '-1')
	and	CallOutReason.SourceContextCode = BaseEncounter.ContextCode

	inner join WH.AuditType
	on	AuditType.SourceAuditTypeCode  = coalesce(BaseEncounter.AuditTypeID, '-1')
	and	AuditType.SourceContextCode = BaseEncounter.ContextCode

	left join WH.Calendar AdmissionDate
	on	AdmissionDate.TheDate = BaseEncounter.AdmissionDate

	left join WH.Calendar CallOutDate
	on	CallOutDate.TheDate = cast(BaseEncounter.CallOutTime as date)

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseEncounter.Updated > @LastUpdated
	and	BaseEncounter.ContextCode = @ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			MergeRecno
			,ContextID
			,AdmissionReasonID
			,AdmissionDateID
			,PresentingECGID
			,CallOutReasonID
			,CallOutDateID
			,LocationID
			,RespondingTeamID
			,OutcomeID
			,OutcomeDateID
			,FinalOutcomeID
			,FinalOutcomeDateID
			,AuditTypeID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.AdmissionReasonID
			,source.AdmissionDateID
			,source.PresentingECGID
			,source.CallOutReasonID
			,source.CallOutDateID
			,source.LocationID
			,source.RespondingTeamID
			,source.OutcomeID
			,source.OutcomeDateID
			,source.FinalOutcomeID
			,source.FinalOutcomeDateID
			,source.AuditTypeID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeRecno = source.MergeRecno
			,target.ContextID = source.ContextID
			,target.AdmissionReasonID = source.AdmissionReasonID
			,target.AdmissionDateID = source.AdmissionDateID
			,target.PresentingECGID = source.PresentingECGID
			,target.CallOutReasonID = source.CallOutReasonID
			,target.CallOutDateID = source.CallOutDateID
			,target.LocationID = source.LocationID
			,target.RespondingTeamID = source.RespondingTeamID
			,target.OutcomeID = source.OutcomeID
			,target.OutcomeDateID = source.OutcomeDateID
			,target.FinalOutcomeID = source.FinalOutcomeID
			,target.FinalOutcomeDateID = source.FinalOutcomeDateID
			,target.AuditTypeID = source.AuditTypeID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






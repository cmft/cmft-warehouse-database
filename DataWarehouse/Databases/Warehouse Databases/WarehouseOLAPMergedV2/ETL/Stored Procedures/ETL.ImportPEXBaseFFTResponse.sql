﻿
CREATE  procedure [ETL].[ImportPEXBaseFFTResponse]

 as

 --temp
truncate table PEX.BaseFFTResponse
truncate table PEX.BaseFFTResponseReference

--Build the Base table
exec ETL.BuildPEXBaseFFTResponse


--Build the BaseReference table
exec ETL.BuildPEXBaseFFTResponseReference 'CMFT||CRT'
exec ETL.BuildPEXBaseFFTResponseReference 'CMFT||HOSP'
exec ETL.BuildPEXBaseFFTResponseReference 'CMFT||ENVOY'

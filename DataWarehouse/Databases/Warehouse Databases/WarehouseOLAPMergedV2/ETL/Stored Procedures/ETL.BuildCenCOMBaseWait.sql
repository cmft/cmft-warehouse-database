﻿CREATE procedure [ETL].[BuildCenCOMBaseWait]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	COM.BaseWait target
using
	(
	select
		 EncounterRecno
		,SourceUniqueID
		,SourceEncounterNo
		,CensusDate
		,ReferredBySpecialtyID
		,ReferredToSpecialtyID
		,SourceofReferralID
		,ReferralReceivedTime
		,ReferralReceivedDate
		,ReasonforReferralID
		,CancelledTime
		,CancelledReasonID
		,ReferralSentTime
		,UrgencyID
		,ReferralPriorityID
		,AuthoriedTime
		,ReferralClosedTime
		,ClosureReasonID
		,ReferredByProfessionalCarerID
		,ReferredByStaffTeamID
		,ReferredByHealthOrgID
		,ReferredToProfessionalCarerID
		,ReferredToHealthOrgID
		,ReferredToStaffTeamID
		,OutcomeOfReferralID
		,ReferralStatusID
		,RejectionID
		,TypeofReferralID
		,PatientSourceID
		,PatientSourceSystemUniqueID
		,PatientNHSNumber
		,PatientNHSNumberStatusIndicator
		,PatientLocalIdentifier
		,PatientPASIdentifier
		,PatientIdentifier
		,PatientFacilityIdentifier
		,PatientTitleID
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,PatientPostcode
		,PatientDateOfBirth
		,PatientDateOfDeath
		,PatientSexID
		,PatientEthnicGroupID
		,PatientSpokenLanguageID
		,PatientReligionID
		,PatientMaritalStatusID
		,ProfessionalCarerCurrentID
		,ProfessionalCarerHealthOrgCurrentID
		,ProfessionalCarerParentHealthOrgCurrentID
		,ProfessionalCarerScheduleID
		,ProfessionalCarerHealthOrgScheduleID
		,ProfessionalCarerParentHealthOrgScheduleID
		,ProfessionalCarerReferralID
		,ProfessionalCarerHealthOrgReferralID
		,ProfessionalCarerParentHealthOrgReferralID
		,RTTStatusDate
		,RTTPatientPathwayID
		,RTTStartTime
		,RTTStartFlag
		,RTTStatusID
		,CreatedTime
		,ModifiedTime
		,CreatedByID
		,ModifiedByID
		,ArchiveFlag
		,ParentID
		,HealthOrgOwner
		,RequestedServiceID
		,ScheduleID
		,ScheduleSpecialtyID
		,ScheduleStaffTeamID
		,ScheduleProfessionalCarerID
		,ScheduleSeenByProfessionalCarerID
		,ScheduleStartTime
		,ScheduleStartDate
		,ScheduleEndTime
		,ScheduleEndDate
		,ScheduleArrivedTime
		,ScheduleArrivedDate
		,ScheduleSeenTime
		,ScheduleSeenDate
		,ScheduleDepartedTime
		,ScheduleDepartedDate
		,ScheduleAttendedID
		,ScheduleOutcomeID
		,ScheduleVisitID
		,ScheduleContactTypeID
		,ScheduleTypeID
		,ScheduleReasonID
		,ScheduleCanceledReasonID
		,ScheduleCanceledTime
		,ScheduleCanceledDate
		,ScheduleCancelledByID
		,ScheduleMoveReasonID
		,ScheduleMovetime
		,ScheduleServicePointID
		,ScheduleServicePointSessionID
		,ScheduleMoveCount
		,ScheduleLocationTypeID
		,ScheduleLocationDescription
		,ScheduleLocationTypeHealthOrgID
		,ScheduleWaitingListID
		,KornerWait
		,BreachDate
		,NationalBreachDate
		,BreachDays
		,NationalBreachDays
		,RTTBreachDate
		,ClockStartDate
		,ScheduleOutcomeCode
		,PatientDeathIndicator
		,CountOfDNAs
		,CountOfHospitalCancels
		,CountOfPatientCancels
		,LastAppointmentFlag
		,AdditionFlag
		,FuturePatientCancelDate
		,PCTCode
		,StartWaitDate
		,EndWaitDate
		,BookedTime
		,DerivedFirstAttendanceFlag
		,DerivedWaitFlag
		,WeeksWaiting
		,AgeCode
		,LengthOfWait
		,WithAppointment
		,BookedBeyondBreach
		,NearBreach
		,DurationCode
		,RTTActivity
		,RTTBreachStatusCode
		,DurationAtAppointmentDateCode
		,WithRTTOpenPathway
		,ContextCode
		,Created
		,Updated
		,ByWhom
	from
		ETL.TLoadCenCOMBaseWait Wait
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno

	when not matched by source
	and	target.ContextCode = 'CEN||IPM'
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,SourceEncounterNo
			,CensusDate
			,ReferredBySpecialtyID
			,ReferredToSpecialtyID
			,SourceofReferralID
			,ReferralReceivedTime
			,ReferralReceivedDate
			,ReasonforReferralID
			,CancelledTime
			,CancelledReasonID
			,ReferralSentTime
			,UrgencyID
			,ReferralPriorityID
			,AuthoriedTime
			,ReferralClosedTime
			,ClosureReasonID
			,ReferredByProfessionalCarerID
			,ReferredByStaffTeamID
			,ReferredByHealthOrgID
			,ReferredToProfessionalCarerID
			,ReferredToHealthOrgID
			,ReferredToStaffTeamID
			,OutcomeOfReferralID
			,ReferralStatusID
			,RejectionID
			,TypeofReferralID
			,PatientSourceID
			,PatientSourceSystemUniqueID
			,PatientNHSNumber
			,PatientNHSNumberStatusIndicator
			,PatientLocalIdentifier
			,PatientPASIdentifier
			,PatientIdentifier
			,PatientFacilityIdentifier
			,PatientTitleID
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,PatientPostcode
			,PatientDateOfBirth
			,PatientDateOfDeath
			,PatientSexID
			,PatientEthnicGroupID
			,PatientSpokenLanguageID
			,PatientReligionID
			,PatientMaritalStatusID
			,ProfessionalCarerCurrentID
			,ProfessionalCarerHealthOrgCurrentID
			,ProfessionalCarerParentHealthOrgCurrentID
			,ProfessionalCarerScheduleID
			,ProfessionalCarerHealthOrgScheduleID
			,ProfessionalCarerParentHealthOrgScheduleID
			,ProfessionalCarerReferralID
			,ProfessionalCarerHealthOrgReferralID
			,ProfessionalCarerParentHealthOrgReferralID
			,RTTStatusDate
			,RTTPatientPathwayID
			,RTTStartTime
			,RTTStartFlag
			,RTTStatusID
			,CreatedTime
			,ModifiedTime
			,CreatedByID
			,ModifiedByID
			,ArchiveFlag
			,ParentID
			,HealthOrgOwner
			,RequestedServiceID
			,ScheduleID
			,ScheduleSpecialtyID
			,ScheduleStaffTeamID
			,ScheduleProfessionalCarerID
			,ScheduleSeenByProfessionalCarerID
			,ScheduleStartTime
			,ScheduleStartDate
			,ScheduleEndTime
			,ScheduleEndDate
			,ScheduleArrivedTime
			,ScheduleArrivedDate
			,ScheduleSeenTime
			,ScheduleSeenDate
			,ScheduleDepartedTime
			,ScheduleDepartedDate
			,ScheduleAttendedID
			,ScheduleOutcomeID
			,ScheduleVisitID
			,ScheduleContactTypeID
			,ScheduleTypeID
			,ScheduleReasonID
			,ScheduleCanceledReasonID
			,ScheduleCanceledTime
			,ScheduleCanceledDate
			,ScheduleCancelledByID
			,ScheduleMoveReasonID
			,ScheduleMovetime
			,ScheduleServicePointID
			,ScheduleServicePointSessionID
			,ScheduleMoveCount
			,ScheduleLocationTypeID
			,ScheduleLocationDescription
			,ScheduleLocationTypeHealthOrgID
			,ScheduleWaitingListID
			,KornerWait
			,BreachDate
			,NationalBreachDate
			,BreachDays
			,NationalBreachDays
			,RTTBreachDate
			,ClockStartDate
			,ScheduleOutcomeCode
			,PatientDeathIndicator
			,CountOfDNAs
			,CountOfHospitalCancels
			,CountOfPatientCancels
			,LastAppointmentFlag
			,AdditionFlag
			,FuturePatientCancelDate
			,PCTCode
			,StartWaitDate
			,EndWaitDate
			,BookedTime
			,DerivedFirstAttendanceFlag
			,DerivedWaitFlag
			,WeeksWaiting
			,AgeCode
			,LengthOfWait
			,WithAppointment
			,BookedBeyondBreach
			,NearBreach
			,DurationCode
			,RTTActivity
			,RTTBreachStatusCode
			,DurationAtAppointmentDateCode
			,WithRTTOpenPathway
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.EncounterRecno
			,source.SourceUniqueID
			,source.SourceEncounterNo
			,source.CensusDate
			,source.ReferredBySpecialtyID
			,source.ReferredToSpecialtyID
			,source.SourceofReferralID
			,source.ReferralReceivedTime
			,source.ReferralReceivedDate
			,source.ReasonforReferralID
			,source.CancelledTime
			,source.CancelledReasonID
			,source.ReferralSentTime
			,source.UrgencyID
			,source.ReferralPriorityID
			,source.AuthoriedTime
			,source.ReferralClosedTime
			,source.ClosureReasonID
			,source.ReferredByProfessionalCarerID
			,source.ReferredByStaffTeamID
			,source.ReferredByHealthOrgID
			,source.ReferredToProfessionalCarerID
			,source.ReferredToHealthOrgID
			,source.ReferredToStaffTeamID
			,source.OutcomeOfReferralID
			,source.ReferralStatusID
			,source.RejectionID
			,source.TypeofReferralID
			,source.PatientSourceID
			,source.PatientSourceSystemUniqueID
			,source.PatientNHSNumber
			,source.PatientNHSNumberStatusIndicator
			,source.PatientLocalIdentifier
			,source.PatientPASIdentifier
			,source.PatientIdentifier
			,source.PatientFacilityIdentifier
			,source.PatientTitleID
			,source.PatientForename
			,source.PatientSurname
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.PatientPostcode
			,source.PatientDateOfBirth
			,source.PatientDateOfDeath
			,source.PatientSexID
			,source.PatientEthnicGroupID
			,source.PatientSpokenLanguageID
			,source.PatientReligionID
			,source.PatientMaritalStatusID
			,source.ProfessionalCarerCurrentID
			,source.ProfessionalCarerHealthOrgCurrentID
			,source.ProfessionalCarerParentHealthOrgCurrentID
			,source.ProfessionalCarerScheduleID
			,source.ProfessionalCarerHealthOrgScheduleID
			,source.ProfessionalCarerParentHealthOrgScheduleID
			,source.ProfessionalCarerReferralID
			,source.ProfessionalCarerHealthOrgReferralID
			,source.ProfessionalCarerParentHealthOrgReferralID
			,source.RTTStatusDate
			,source.RTTPatientPathwayID
			,source.RTTStartTime
			,source.RTTStartFlag
			,source.RTTStatusID
			,source.CreatedTime
			,source.ModifiedTime
			,source.CreatedByID
			,source.ModifiedByID
			,source.ArchiveFlag
			,source.ParentID
			,source.HealthOrgOwner
			,source.RequestedServiceID
			,source.ScheduleID
			,source.ScheduleSpecialtyID
			,source.ScheduleStaffTeamID
			,source.ScheduleProfessionalCarerID
			,source.ScheduleSeenByProfessionalCarerID
			,source.ScheduleStartTime
			,source.ScheduleStartDate
			,source.ScheduleEndTime
			,source.ScheduleEndDate
			,source.ScheduleArrivedTime
			,source.ScheduleArrivedDate
			,source.ScheduleSeenTime
			,source.ScheduleSeenDate
			,source.ScheduleDepartedTime
			,source.ScheduleDepartedDate
			,source.ScheduleAttendedID
			,source.ScheduleOutcomeID
			,source.ScheduleVisitID
			,source.ScheduleContactTypeID
			,source.ScheduleTypeID
			,source.ScheduleReasonID
			,source.ScheduleCanceledReasonID
			,source.ScheduleCanceledTime
			,source.ScheduleCanceledDate
			,source.ScheduleCancelledByID
			,source.ScheduleMoveReasonID
			,source.ScheduleMovetime
			,source.ScheduleServicePointID
			,source.ScheduleServicePointSessionID
			,source.ScheduleMoveCount
			,source.ScheduleLocationTypeID
			,source.ScheduleLocationDescription
			,source.ScheduleLocationTypeHealthOrgID
			,source.ScheduleWaitingListID
			,source.KornerWait
			,source.BreachDate
			,source.NationalBreachDate
			,source.BreachDays
			,source.NationalBreachDays
			,source.RTTBreachDate
			,source.ClockStartDate
			,source.ScheduleOutcomeCode
			,source.PatientDeathIndicator
			,source.CountOfDNAs
			,source.CountOfHospitalCancels
			,source.CountOfPatientCancels
			,source.LastAppointmentFlag
			,source.AdditionFlag
			,source.FuturePatientCancelDate
			,source.PCTCode
			,source.StartWaitDate
			,source.EndWaitDate
			,source.BookedTime
			,source.DerivedFirstAttendanceFlag
			,source.DerivedWaitFlag
			,source.WeeksWaiting
			,source.AgeCode
			,source.LengthOfWait
			,source.WithAppointment
			,source.BookedBeyondBreach
			,source.NearBreach
			,source.DurationCode
			,source.RTTActivity
			,source.RTTBreachStatusCode
			,source.DurationAtAppointmentDateCode
			,source.WithRTTOpenPathway
			,source.ContextCode
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.CensusDate = source.CensusDate
			,target.ReferredBySpecialtyID = source.ReferredBySpecialtyID
			,target.ReferredToSpecialtyID = source.ReferredToSpecialtyID
			,target.SourceofReferralID = source.SourceofReferralID
			,target.ReferralReceivedTime = source.ReferralReceivedTime
			,target.ReferralReceivedDate = source.ReferralReceivedDate
			,target.ReasonforReferralID = source.ReasonforReferralID
			,target.CancelledTime = source.CancelledTime
			,target.CancelledReasonID = source.CancelledReasonID
			,target.ReferralSentTime = source.ReferralSentTime
			,target.UrgencyID = source.UrgencyID
			,target.ReferralPriorityID = source.ReferralPriorityID
			,target.AuthoriedTime = source.AuthoriedTime
			,target.ReferralClosedTime = source.ReferralClosedTime
			,target.ClosureReasonID = source.ClosureReasonID
			,target.ReferredByProfessionalCarerID = source.ReferredByProfessionalCarerID
			,target.ReferredByStaffTeamID = source.ReferredByStaffTeamID
			,target.ReferredByHealthOrgID = source.ReferredByHealthOrgID
			,target.ReferredToProfessionalCarerID = source.ReferredToProfessionalCarerID
			,target.ReferredToHealthOrgID = source.ReferredToHealthOrgID
			,target.ReferredToStaffTeamID = source.ReferredToStaffTeamID
			,target.OutcomeOfReferralID = source.OutcomeOfReferralID
			,target.ReferralStatusID = source.ReferralStatusID
			,target.RejectionID = source.RejectionID
			,target.TypeofReferralID = source.TypeofReferralID
			,target.PatientSourceID = source.PatientSourceID
			,target.PatientSourceSystemUniqueID = source.PatientSourceSystemUniqueID
			,target.PatientNHSNumber = source.PatientNHSNumber
			,target.PatientNHSNumberStatusIndicator = source.PatientNHSNumberStatusIndicator
			,target.PatientLocalIdentifier = source.PatientLocalIdentifier
			,target.PatientPASIdentifier = source.PatientPASIdentifier
			,target.PatientIdentifier = source.PatientIdentifier
			,target.PatientFacilityIdentifier = source.PatientFacilityIdentifier
			,target.PatientTitleID = source.PatientTitleID
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.PatientPostcode = source.PatientPostcode
			,target.PatientDateOfBirth = source.PatientDateOfBirth
			,target.PatientDateOfDeath = source.PatientDateOfDeath
			,target.PatientSexID = source.PatientSexID
			,target.PatientEthnicGroupID = source.PatientEthnicGroupID
			,target.PatientSpokenLanguageID = source.PatientSpokenLanguageID
			,target.PatientReligionID = source.PatientReligionID
			,target.PatientMaritalStatusID = source.PatientMaritalStatusID
			,target.ProfessionalCarerCurrentID = source.ProfessionalCarerCurrentID
			,target.ProfessionalCarerHealthOrgCurrentID = source.ProfessionalCarerHealthOrgCurrentID
			,target.ProfessionalCarerParentHealthOrgCurrentID = source.ProfessionalCarerParentHealthOrgCurrentID
			,target.ProfessionalCarerScheduleID = source.ProfessionalCarerScheduleID
			,target.ProfessionalCarerHealthOrgScheduleID = source.ProfessionalCarerHealthOrgScheduleID
			,target.ProfessionalCarerParentHealthOrgScheduleID = source.ProfessionalCarerParentHealthOrgScheduleID
			,target.ProfessionalCarerReferralID = source.ProfessionalCarerReferralID
			,target.ProfessionalCarerHealthOrgReferralID = source.ProfessionalCarerHealthOrgReferralID
			,target.ProfessionalCarerParentHealthOrgReferralID = source.ProfessionalCarerParentHealthOrgReferralID
			,target.RTTStatusDate = source.RTTStatusDate
			,target.RTTPatientPathwayID = source.RTTPatientPathwayID
			,target.RTTStartTime = source.RTTStartTime
			,target.RTTStartFlag = source.RTTStartFlag
			,target.RTTStatusID = source.RTTStatusID
			,target.CreatedTime = source.CreatedTime
			,target.ModifiedTime = source.ModifiedTime
			,target.CreatedByID = source.CreatedByID
			,target.ModifiedByID = source.ModifiedByID
			,target.ArchiveFlag = source.ArchiveFlag
			,target.ParentID = source.ParentID
			,target.HealthOrgOwner = source.HealthOrgOwner
			,target.RequestedServiceID = source.RequestedServiceID
			,target.ScheduleID = source.ScheduleID
			,target.ScheduleSpecialtyID = source.ScheduleSpecialtyID
			,target.ScheduleStaffTeamID = source.ScheduleStaffTeamID
			,target.ScheduleProfessionalCarerID = source.ScheduleProfessionalCarerID
			,target.ScheduleSeenByProfessionalCarerID = source.ScheduleSeenByProfessionalCarerID
			,target.ScheduleStartTime = source.ScheduleStartTime
			,target.ScheduleStartDate = source.ScheduleStartDate
			,target.ScheduleEndTime = source.ScheduleEndTime
			,target.ScheduleEndDate = source.ScheduleEndDate
			,target.ScheduleArrivedTime = source.ScheduleArrivedTime
			,target.ScheduleArrivedDate = source.ScheduleArrivedDate
			,target.ScheduleSeenTime = source.ScheduleSeenTime
			,target.ScheduleSeenDate = source.ScheduleSeenDate
			,target.ScheduleDepartedTime = source.ScheduleDepartedTime
			,target.ScheduleDepartedDate = source.ScheduleDepartedDate
			,target.ScheduleAttendedID = source.ScheduleAttendedID
			,target.ScheduleOutcomeID = source.ScheduleOutcomeID
			,target.ScheduleVisitID = source.ScheduleVisitID
			,target.ScheduleContactTypeID = source.ScheduleContactTypeID
			,target.ScheduleTypeID = source.ScheduleTypeID
			,target.ScheduleReasonID = source.ScheduleReasonID
			,target.ScheduleCanceledReasonID = source.ScheduleCanceledReasonID
			,target.ScheduleCanceledTime = source.ScheduleCanceledTime
			,target.ScheduleCanceledDate = source.ScheduleCanceledDate
			,target.ScheduleCancelledByID = source.ScheduleCancelledByID
			,target.ScheduleMoveReasonID = source.ScheduleMoveReasonID
			,target.ScheduleMovetime = source.ScheduleMovetime
			,target.ScheduleServicePointID = source.ScheduleServicePointID
			,target.ScheduleServicePointSessionID = source.ScheduleServicePointSessionID
			,target.ScheduleMoveCount = source.ScheduleMoveCount
			,target.ScheduleLocationTypeID = source.ScheduleLocationTypeID
			,target.ScheduleLocationDescription = source.ScheduleLocationDescription
			,target.ScheduleLocationTypeHealthOrgID = source.ScheduleLocationTypeHealthOrgID
			,target.ScheduleWaitingListID = source.ScheduleWaitingListID
			,target.KornerWait = source.KornerWait
			,target.BreachDate = source.BreachDate
			,target.NationalBreachDate = source.NationalBreachDate
			,target.BreachDays = source.BreachDays
			,target.NationalBreachDays = source.NationalBreachDays
			,target.RTTBreachDate = source.RTTBreachDate
			,target.ClockStartDate = source.ClockStartDate
			,target.ScheduleOutcomeCode = source.ScheduleOutcomeCode
			,target.PatientDeathIndicator = source.PatientDeathIndicator
			,target.CountOfDNAs = source.CountOfDNAs
			,target.CountOfHospitalCancels = source.CountOfHospitalCancels
			,target.CountOfPatientCancels = source.CountOfPatientCancels
			,target.LastAppointmentFlag = source.LastAppointmentFlag
			,target.AdditionFlag = source.AdditionFlag
			,target.FuturePatientCancelDate = source.FuturePatientCancelDate
			,target.PCTCode = source.PCTCode
			,target.StartWaitDate = source.StartWaitDate
			,target.EndWaitDate = source.EndWaitDate
			,target.BookedTime = source.BookedTime
			,target.DerivedFirstAttendanceFlag = source.DerivedFirstAttendanceFlag
			,target.DerivedWaitFlag = source.DerivedWaitFlag
			,target.WeeksWaiting = source.WeeksWaiting
			,target.AgeCode = source.AgeCode
			,target.LengthOfWait = source.LengthOfWait
			,target.WithAppointment = source.WithAppointment
			,target.BookedBeyondBreach = source.BookedBeyondBreach
			,target.NearBreach = source.NearBreach
			,target.DurationCode = source.DurationCode
			,target.RTTActivity = source.RTTActivity
			,target.RTTBreachStatusCode = source.RTTBreachStatusCode
			,target.DurationAtAppointmentDateCode = source.DurationAtAppointmentDateCode
			,target.WithRTTOpenPathway = source.WithRTTOpenPathway
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

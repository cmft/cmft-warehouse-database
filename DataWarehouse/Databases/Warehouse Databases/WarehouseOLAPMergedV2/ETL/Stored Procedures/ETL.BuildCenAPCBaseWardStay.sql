﻿create procedure ETL.[BuildCenAPCBaseWardStay]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	APC.BaseWardStay target
using
	(
	select
		 ContextCode = cast(ContextCode as varchar)
		,SourceUniqueID = cast(SourceUniqueID as varchar)
		,SourcePatientNo = cast(SourcePatientNo as varchar)
		,SourceSpellNo = cast(SourceSpellNo as varchar)
		,ProviderSpellNo = cast(ProviderSpellNo as varchar)
		,StartDate = cast(StartDate as smalldatetime)
		,EndDate = cast(EndDate as smalldatetime)
		,StartTime = cast(StartTime as smalldatetime)
		,EndTime = cast(EndTime as smalldatetime)
		,SiteCode = cast(SiteCode as varchar)
		,WardCode = cast(WardCode as varchar)
		,StartActivityCode = cast(StartActivityCode as varchar)
		,EndActivityCode = cast(EndActivityCode as varchar)
	from
		ETL.TLoadCenAPCBaseWardStay Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.ContextCode = 'CEN||PAS'
	then delete

	when not matched
	then
		insert
			(
			 ContextCode
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,ProviderSpellNo
			,StartDate
			,EndDate
			,StartTime
			,EndTime
			,SiteCode
			,WardCode
			,StartActivityCode
			,EndActivityCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 ContextCode
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,ProviderSpellNo
			,StartDate
			,EndDate
			,StartTime
			,EndTime
			,SiteCode
			,WardCode
			,StartActivityCode
			,EndActivityCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.ProviderSpellNo, '') = isnull(source.ProviderSpellNo, '')
		and isnull(target.StartDate, getdate()) = isnull(source.StartDate, getdate())
		and isnull(target.EndDate, getdate()) = isnull(source.EndDate, getdate())
		and isnull(target.StartTime, getdate()) = isnull(source.StartTime, getdate())
		and isnull(target.EndTime, getdate()) = isnull(source.EndTime, getdate())
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.StartActivityCode, '') = isnull(source.StartActivityCode, '')
		and isnull(target.EndActivityCode, '') = isnull(source.EndActivityCode, '')
		)
	then
		update
		set
			 target.ContextCode = source.ContextCode
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.ProviderSpellNo = source.ProviderSpellNo
			,target.StartDate = source.StartDate
			,target.EndDate = source.EndDate
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.SiteCode = source.SiteCode
			,target.WardCode = source.WardCode
			,target.StartActivityCode = source.StartActivityCode
			,target.EndActivityCode = source.EndActivityCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

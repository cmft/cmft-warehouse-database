﻿

create procedure [ETL].[BuildAPCBaseStaffingLevelReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseStaffingLevelReference
			where
				BaseStaffingLevelReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseStaffingLevelReference
where
	not exists
	(
	select
		1
	from
		APC.BaseStaffingLevel
	where
		BaseStaffingLevel.MergeStaffingLevelRecno = BaseStaffingLevelReference.MergeStaffingLevelRecno
	)
and	BaseStaffingLevelReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseStaffingLevelReference target
using
	(
	select
		BaseStaffingLevel.MergeStaffingLevelRecno
		,ContextID = Context.ContextID
		,CensusDateID =
			coalesce(
				 CensusDate.DateID

				,case
				when BaseStaffingLevel.CensusDate is null
				then NullDate.DateID

				when BaseStaffingLevel.CensusDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,WardID = Ward.SourceWardID
		,ShiftID = Shift.SourceShiftID			
		,BaseStaffingLevel.Created
		,BaseStaffingLevel.Updated
		,BaseStaffingLevel.ByWhom
	from
		APC.BaseStaffingLevel

	inner join WH.Context
	on	Context.ContextCode = BaseStaffingLevel.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(BaseStaffingLevel.WardCode, '-1')
	and	Ward.SourceContextCode = BaseStaffingLevel.ContextCode

	inner join WH.Shift
	on	Shift.SourceShiftCode = coalesce(BaseStaffingLevel.ShiftID, '-1')
	and	Shift.SourceContextCode = BaseStaffingLevel.ContextCode

	left join WH.Calendar CensusDate
	on	CensusDate.TheDate = BaseStaffingLevel.CensusDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseStaffingLevel.Updated > @LastUpdated
	and	BaseStaffingLevel.ContextCode = @ContextCode

	) source
	on	source.MergeStaffingLevelRecno = target.MergeStaffingLevelRecno

	when not matched
	then
		insert
			(
			MergeStaffingLevelRecno
			,ContextID
			,CensusDateID
			,WardID
			,ShiftID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeStaffingLevelRecno
			,source.ContextID
			,source.CensusDateID
			,source.WardID
			,source.ShiftID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeStaffingLevelRecno = source.MergeStaffingLevelRecno
			,target.ContextID = source.ContextID
			,target.CensusDateID = source.CensusDateID
			,target.WardID = source.WardID
			,target.ShiftID = source.ShiftID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






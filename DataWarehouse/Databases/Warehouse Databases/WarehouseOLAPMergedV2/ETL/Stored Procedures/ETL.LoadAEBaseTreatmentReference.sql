﻿CREATE procedure [ETL].[LoadAEBaseTreatmentReference]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	AE.BaseTreatmentReference target
using
	(
	select
		 ProcessList.MergeRecno
		,ProcessList.Action
		,Context.ContextID
		,TreatmentID = Treatment.SourceTreatmentID
	from
		AE.ProcessList ProcessList

	left join AE.BaseTreatment
	on	BaseTreatment.MergeRecno = ProcessList.MergeRecno

	left join AE.Treatment
	on	Treatment.SourceTreatmentCode = cast(coalesce(BaseTreatment.TreatmentCode, '-1') as varchar)
	and	Treatment.SourceContextCode = BaseTreatment.ContextCode

	left join WH.Context
	on	Context.ContextCode = BaseTreatment.ContextCode
	
	where
		ProcessList.Dataset = 'Treatment'

	) source
	on	source.MergeRecno = target.MergeRecno

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	then

		insert
			(
			 MergeRecno
			,ContextID
			,TreatmentID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.TreatmentID
			)

	when matched
	and	source.Action <> 'DELETE'
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.TreatmentID = source.TreatmentID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


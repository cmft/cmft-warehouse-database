﻿




CREATE procedure [ETL].[BuildBloodManagementBaseCellSalvageReference] --'CMFT||RECALL'
	@ContextCode varchar(15)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	
declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				BloodManagement.BaseCellSalvageReference
			where
				BaseCellSalvageReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	BloodManagement.BaseCellSalvageReference
where
	not exists
	(
	select
		1
	from
		BloodManagement.BaseCellSalvage
	where
		BaseCellSalvage.MergeRecno = BaseCellSalvageReference.MergeRecno
	)
and	BaseCellSalvageReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


--select top 100 * from BloodManagement.BaseCellSalvage

merge
	BloodManagement.BaseCellSalvageReference target
using
	(
	select 
		BaseCellSalvage.MergeRecno
		,ContextID = Context.ContextID
		,LocationID = SourceLocationID 
		,SpecialtyID = SourceSpecialtyID 
		,ConsultantID = SourceConsultantID 
		
		,BaseCellSalvage.Created
		,BaseCellSalvage.Updated
		,BaseCellSalvage.ByWhom
	from
		BloodManagement.BaseCellSalvage

	inner join WH.Context
	on	Context.ContextCode = BaseCellSalvage.ContextCode

	left join WH.Location
	on	Location.SourceLocationCode = coalesce(BaseCellSalvage.SessionLocationCode, '-1')
	and	Location.SourceContextCode = BaseCellSalvage.ContextCode
	
	left join WH.Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(BaseCellSalvage.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = BaseCellSalvage.ContextCode

	left join WH.Consultant
	on	Consultant.SourceConsultantCode = coalesce(BaseCellSalvage.ConsultantCode, '-1')
	and	Consultant.SourceContextCode = BaseCellSalvage.ContextCode

	where
		BaseCellSalvage.Updated > @LastUpdated
	and	
	BaseCellSalvage.ContextCode = @ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno

	when not matched
	then
		insert
			(
			MergeRecno
			,ContextID 
			,LocationID 
			,SpecialtyID
			,ConsultantID
	
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeRecno
			,source.ContextID 
			,source.LocationID 
			,source.SpecialtyID
			,source.ConsultantID
				
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeRecno = source.MergeRecno
			,target.ContextID = source.ContextID
			,target.LocationID = source.LocationID
			,target.SpecialtyID = source.SpecialtyID
			,target.ConsultantID = source.ConsultantID
			
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
			
		
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats








﻿CREATE procedure [ETL].[BuildTraTheatreBaseProcedureDetail]

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Theatre.BaseProcedureDetail target
using
	(
	select
		 SequenceNo
		,SourceUniqueID
		,SupervisingSurgeon1Code
		,SupervisingSurgeon2Code
		,SupervisingSurgeon3Code
		,SupervisingAnaesthetistNurseCode
		,SupervisingScoutNurseCode
		,SupervisingInstrumentNurseCode
		,SupervisingTechnician1Code
		,SupervisingTechnician2Code
		,ProcedureConfirmedFlag
		,CMBSItemCode
		,Unused2
		,SupervisingAnaesthetist2Code
		,Unused3
		,SupervisingAnaesthetist3Code
		,ProcedureStartTime
		,ProcedureEndTime
		,ProcedureDescription
		,WoundType1Code
		,WoundType2Code
		,WoundType3Code
		,ProcedureCode
		,OperationDetailSourceUniqueID
		,SupervisingAnaesthetist1Code
		,Unused4
		,Unused5
		,Unused6
		,LastUpdated
		,LogDetail
		,Surgeon1Code
		,Surgeon2Code
		,Surgeon3Code
		,Anaesthetist1Code
		,Anaesthetist2Code
		,Anaesthetist3Code
		,ScoutNurseCode
		,InstrumentNurseCode
		,AnaesthetistNurseCode
		,Technician1Code
		,Technician2Code
		,ChangeOverAnaesthetistNurseCode
		,ChangeOverScoutNurseCode
		,ChangeOverInstrumentNurseCode
		,InventoryRecordCreatedFlag
		,PrincipleDiagnosisFlag
		,WoundType4Code
		,PrimaryProcedureFlag
		,WoundType5Code
		,WoundType6Code
		,PANCode
		,PADCode
		,RecordTimesCode
		,RecoverySourceUniqueID
		,ProcedureItemSourceUniqueID
	from
		ETL.TLoadTraTheatreBaseProcedureDetail
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	and	target.ContextCode = 'TRA||ORMIS'

	when not matched by source
	and	target.ContextCode = 'TRA||ORMIS'
	then delete

	when not matched
	then
		insert
			(
			 SequenceNo
			,SourceUniqueID
			,SupervisingSurgeon1Code
			,SupervisingSurgeon2Code
			,SupervisingSurgeon3Code
			,SupervisingAnaesthetistNurseCode
			,SupervisingScoutNurseCode
			,SupervisingInstrumentNurseCode
			,SupervisingTechnician1Code
			,SupervisingTechnician2Code
			,ProcedureConfirmedFlag
			,CMBSItemCode
			,Unused2
			,SupervisingAnaesthetist2Code
			,Unused3
			,SupervisingAnaesthetist3Code
			,ProcedureStartTime
			,ProcedureEndTime
			,ProcedureDescription
			,WoundType1Code
			,WoundType2Code
			,WoundType3Code
			,ProcedureCode
			,OperationDetailSourceUniqueID
			,SupervisingAnaesthetist1Code
			,Unused4
			,Unused5
			,Unused6
			,LastUpdated
			,LogDetail
			,Surgeon1Code
			,Surgeon2Code
			,Surgeon3Code
			,Anaesthetist1Code
			,Anaesthetist2Code
			,Anaesthetist3Code
			,ScoutNurseCode
			,InstrumentNurseCode
			,AnaesthetistNurseCode
			,Technician1Code
			,Technician2Code
			,ChangeOverAnaesthetistNurseCode
			,ChangeOverScoutNurseCode
			,ChangeOverInstrumentNurseCode
			,InventoryRecordCreatedFlag
			,PrincipleDiagnosisFlag
			,WoundType4Code
			,PrimaryProcedureFlag
			,WoundType5Code
			,WoundType6Code
			,PANCode
			,PADCode
			,RecordTimesCode
			,RecoverySourceUniqueID
			,ProcedureItemSourceUniqueID
			,ContextCode
			)
		values
			(
			 source.SequenceNo
			,source.SourceUniqueID
			,source.SupervisingSurgeon1Code
			,source.SupervisingSurgeon2Code
			,source.SupervisingSurgeon3Code
			,source.SupervisingAnaesthetistNurseCode
			,source.SupervisingScoutNurseCode
			,source.SupervisingInstrumentNurseCode
			,source.SupervisingTechnician1Code
			,source.SupervisingTechnician2Code
			,source.ProcedureConfirmedFlag
			,source.CMBSItemCode
			,source.Unused2
			,source.SupervisingAnaesthetist2Code
			,source.Unused3
			,source.SupervisingAnaesthetist3Code
			,source.ProcedureStartTime
			,source.ProcedureEndTime
			,source.ProcedureDescription
			,source.WoundType1Code
			,source.WoundType2Code
			,source.WoundType3Code
			,source.ProcedureCode
			,source.OperationDetailSourceUniqueID
			,source.SupervisingAnaesthetist1Code
			,source.Unused4
			,source.Unused5
			,source.Unused6
			,source.LastUpdated
			,source.LogDetail
			,source.Surgeon1Code
			,source.Surgeon2Code
			,source.Surgeon3Code
			,source.Anaesthetist1Code
			,source.Anaesthetist2Code
			,source.Anaesthetist3Code
			,source.ScoutNurseCode
			,source.InstrumentNurseCode
			,source.AnaesthetistNurseCode
			,source.Technician1Code
			,source.Technician2Code
			,source.ChangeOverAnaesthetistNurseCode
			,source.ChangeOverScoutNurseCode
			,source.ChangeOverInstrumentNurseCode
			,source.InventoryRecordCreatedFlag
			,source.PrincipleDiagnosisFlag
			,source.WoundType4Code
			,source.PrimaryProcedureFlag
			,source.WoundType5Code
			,source.WoundType6Code
			,source.PANCode
			,source.PADCode
			,source.RecordTimesCode
			,source.RecoverySourceUniqueID
			,source.ProcedureItemSourceUniqueID
			,'TRA||ORMIS'
			)

	when matched
	and not
		(
			target.SequenceNo = source.SequenceNo
		and target.SupervisingSurgeon1Code = source.SupervisingSurgeon1Code
		and target.SupervisingSurgeon2Code = source.SupervisingSurgeon2Code
		and target.SupervisingSurgeon3Code = source.SupervisingSurgeon3Code
		and target.SupervisingAnaesthetistNurseCode = source.SupervisingAnaesthetistNurseCode
		and target.SupervisingScoutNurseCode = source.SupervisingScoutNurseCode
		and target.SupervisingInstrumentNurseCode = source.SupervisingInstrumentNurseCode
		and target.SupervisingTechnician1Code = source.SupervisingTechnician1Code
		and target.SupervisingTechnician2Code = source.SupervisingTechnician2Code
		and target.ProcedureConfirmedFlag = source.ProcedureConfirmedFlag
		and target.CMBSItemCode = source.CMBSItemCode
		and target.Unused2 = source.Unused2
		and target.SupervisingAnaesthetist2Code = source.SupervisingAnaesthetist2Code
		and target.Unused3 = source.Unused3
		and target.SupervisingAnaesthetist3Code = source.SupervisingAnaesthetist3Code
		and target.ProcedureStartTime = source.ProcedureStartTime
		and target.ProcedureEndTime = source.ProcedureEndTime
		and target.ProcedureDescription = source.ProcedureDescription
		and target.WoundType1Code = source.WoundType1Code
		and target.WoundType2Code = source.WoundType2Code
		and target.WoundType3Code = source.WoundType3Code
		and target.ProcedureCode = source.ProcedureCode
		and target.OperationDetailSourceUniqueID = source.OperationDetailSourceUniqueID
		and target.SupervisingAnaesthetist1Code = source.SupervisingAnaesthetist1Code
		and target.Unused4 = source.Unused4
		and target.Unused5 = source.Unused5
		and target.Unused6 = source.Unused6
		and target.LastUpdated = source.LastUpdated
		and target.LogDetail = source.LogDetail
		and target.Surgeon1Code = source.Surgeon1Code
		and target.Surgeon2Code = source.Surgeon2Code
		and target.Surgeon3Code = source.Surgeon3Code
		and target.Anaesthetist1Code = source.Anaesthetist1Code
		and target.Anaesthetist2Code = source.Anaesthetist2Code
		and target.Anaesthetist3Code = source.Anaesthetist3Code
		and target.ScoutNurseCode = source.ScoutNurseCode
		and target.InstrumentNurseCode = source.InstrumentNurseCode
		and target.AnaesthetistNurseCode = source.AnaesthetistNurseCode
		and target.Technician1Code = source.Technician1Code
		and target.Technician2Code = source.Technician2Code
		and target.ChangeOverAnaesthetistNurseCode = source.ChangeOverAnaesthetistNurseCode
		and target.ChangeOverScoutNurseCode = source.ChangeOverScoutNurseCode
		and target.ChangeOverInstrumentNurseCode = source.ChangeOverInstrumentNurseCode
		and target.InventoryRecordCreatedFlag = source.InventoryRecordCreatedFlag
		and target.PrincipleDiagnosisFlag = source.PrincipleDiagnosisFlag
		and target.WoundType4Code = source.WoundType4Code
		and target.PrimaryProcedureFlag = source.PrimaryProcedureFlag
		and target.WoundType5Code = source.WoundType5Code
		and target.WoundType6Code = source.WoundType6Code
		and target.PANCode = source.PANCode
		and target.PADCode = source.PADCode
		and target.RecordTimesCode = source.RecordTimesCode
		and target.RecoverySourceUniqueID = source.RecoverySourceUniqueID
		and target.ProcedureItemSourceUniqueID = source.ProcedureItemSourceUniqueID
		)
	then
		update
		set
			 target.SequenceNo = source.SequenceNo
			,target.SupervisingSurgeon1Code = source.SupervisingSurgeon1Code
			,target.SupervisingSurgeon2Code = source.SupervisingSurgeon2Code
			,target.SupervisingSurgeon3Code = source.SupervisingSurgeon3Code
			,target.SupervisingAnaesthetistNurseCode = source.SupervisingAnaesthetistNurseCode
			,target.SupervisingScoutNurseCode = source.SupervisingScoutNurseCode
			,target.SupervisingInstrumentNurseCode = source.SupervisingInstrumentNurseCode
			,target.SupervisingTechnician1Code = source.SupervisingTechnician1Code
			,target.SupervisingTechnician2Code = source.SupervisingTechnician2Code
			,target.ProcedureConfirmedFlag = source.ProcedureConfirmedFlag
			,target.CMBSItemCode = source.CMBSItemCode
			,target.Unused2 = source.Unused2
			,target.SupervisingAnaesthetist2Code = source.SupervisingAnaesthetist2Code
			,target.Unused3 = source.Unused3
			,target.SupervisingAnaesthetist3Code = source.SupervisingAnaesthetist3Code
			,target.ProcedureStartTime = source.ProcedureStartTime
			,target.ProcedureEndTime = source.ProcedureEndTime
			,target.ProcedureDescription = source.ProcedureDescription
			,target.WoundType1Code = source.WoundType1Code
			,target.WoundType2Code = source.WoundType2Code
			,target.WoundType3Code = source.WoundType3Code
			,target.ProcedureCode = source.ProcedureCode
			,target.OperationDetailSourceUniqueID = source.OperationDetailSourceUniqueID
			,target.SupervisingAnaesthetist1Code = source.SupervisingAnaesthetist1Code
			,target.Unused4 = source.Unused4
			,target.Unused5 = source.Unused5
			,target.Unused6 = source.Unused6
			,target.LastUpdated = source.LastUpdated
			,target.LogDetail = source.LogDetail
			,target.Surgeon1Code = source.Surgeon1Code
			,target.Surgeon2Code = source.Surgeon2Code
			,target.Surgeon3Code = source.Surgeon3Code
			,target.Anaesthetist1Code = source.Anaesthetist1Code
			,target.Anaesthetist2Code = source.Anaesthetist2Code
			,target.Anaesthetist3Code = source.Anaesthetist3Code
			,target.ScoutNurseCode = source.ScoutNurseCode
			,target.InstrumentNurseCode = source.InstrumentNurseCode
			,target.AnaesthetistNurseCode = source.AnaesthetistNurseCode
			,target.Technician1Code = source.Technician1Code
			,target.Technician2Code = source.Technician2Code
			,target.ChangeOverAnaesthetistNurseCode = source.ChangeOverAnaesthetistNurseCode
			,target.ChangeOverScoutNurseCode = source.ChangeOverScoutNurseCode
			,target.ChangeOverInstrumentNurseCode = source.ChangeOverInstrumentNurseCode
			,target.InventoryRecordCreatedFlag = source.InventoryRecordCreatedFlag
			,target.PrincipleDiagnosisFlag = source.PrincipleDiagnosisFlag
			,target.WoundType4Code = source.WoundType4Code
			,target.PrimaryProcedureFlag = source.PrimaryProcedureFlag
			,target.WoundType5Code = source.WoundType5Code
			,target.WoundType6Code = source.WoundType6Code
			,target.PANCode = source.PANCode
			,target.PADCode = source.PADCode
			,target.RecordTimesCode = source.RecordTimesCode
			,target.RecoverySourceUniqueID = source.RecoverySourceUniqueID
			,target.ProcedureItemSourceUniqueID = source.ProcedureItemSourceUniqueID

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿CREATE procedure [ETL].[BuildDiagnosticWLBaseRadiologyReference]
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)


merge
	DiagnosticWL.BaseRadiologyReference target
using
	(
	select
		 Encounter.MergeRecno
		,ContextID = Context.ContextID

		,ExamID = Exam.SourceExamID

		,CensusDateID =
			coalesce(
				 CensusDate.DateID
				,NullDate.DateID
			)

		,SpecialtyID = Specialty.SourceSpecialtyID
		,SiteID = Site.SourceSiteID
		,SexID = Sex.SourceSexID

	from
		DiagnosticWL.BaseRadiology Encounter

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	inner join Diagnostic.Exam Exam
	on	Exam.SourceExamCode = cast(coalesce(Encounter.ExamCode, '-1') as varchar)
	and	Exam.SourceContextCode = Encounter.ContextCode

	inner join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = cast(coalesce(Encounter.SpecialtyCode, '-1') as varchar)
	and	Specialty.SourceContextCode = Encounter.ContextCode

	inner join WH.Site Site
	on	Site.SourceSiteCode = cast(coalesce(Encounter.SiteCode, '-1') as varchar)
	and	Site.SourceContextCode = Encounter.ContextCode

	inner join WH.Sex Sex
	on	Sex.SourceSexCode = cast(coalesce(Encounter.SexCode, '-1') as varchar)
	and	Sex.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar CensusDate
	on	CensusDate.TheDate = Encounter.CensusDate

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		Encounter.ContextCode = @ContextCode

	) source
	on	source.MergeRecno = target.MergeRecno


	when not matched by source
	and	target.ContextID = @ContextID
	then
		delete

	when not matched
	then
		insert
			(
			 MergeRecno
			,ContextID
			,ExamID
			,CensusDateID
			,SpecialtyID
			,SiteID
			,SexID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.ExamID
			,source.CensusDateID
			,source.SpecialtyID
			,source.SiteID
			,source.SexID
			)


	when matched
	and not
		(
			isnull(target.ContextID, 0) = isnull(source.ContextID, 0)
		and isnull(target.ExamID, 0) = isnull(source.ExamID, 0)
		and isnull(target.CensusDateID, 0) = isnull(source.CensusDateID, 0)
		and isnull(target.SpecialtyID, 0) = isnull(source.SpecialtyID, 0)
		and isnull(target.SiteID, 0) = isnull(source.SiteID, 0)
		and isnull(target.SexID, 0) = isnull(source.SexID, 0)
		)
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.ExamID = source.ExamID
			,target.CensusDateID = source.CensusDateID
			,target.SpecialtyID = source.SpecialtyID
			,target.SiteID = source.SiteID
			,target.SexID = source.SexID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

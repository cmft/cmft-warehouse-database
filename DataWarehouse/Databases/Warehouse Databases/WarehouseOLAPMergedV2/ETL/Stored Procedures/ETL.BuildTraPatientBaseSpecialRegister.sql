﻿


CREATE proc [ETL].[BuildTraPatientBaseSpecialRegister]
as

/****************************************************************************************
	Stored procedure : [ETL].[BuildCenPatientBaseSpecialRegister]
	Description		 : Merge new/updated Special Register data into production

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	20/10/2015	Rachel Royston	
*****************************************************************************************/

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'TRA||UG'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge 
	Patient.BaseSpecialRegister target 
using
	(
	select
		 SpecialRegisterRecno
		,SourcePatientNo
		,SpecialRegisterCode
		,Alert
		,DistrictNo
		,NHSNumber
		,EnteredDate = EnteredTime
		,LastModifiedTime
		,StartDate
		,EndDate
		,Active
		,ContextCode
	from
		ETL.TLoadTraPatientBaseSpecialRegister
	) source
		
on source.SpecialRegisterRecno = target.SpecialRegisterRecno
and source.ContextCode = target.ContextCode
	
when not matched by source
and target.ContextCode = @ContextCode

	then delete
	
when not matched by target
	then insert
		(
		 SpecialRegisterRecno
		,SourcePatientNo
		,SpecialRegisterCode
		,Alert
		,DistrictNo
		,NHSNumber
		,EnteredDate
		,LastModifiedTime
		,StartDate
		,EndDate
		,Active
		,ContextCode
		,CreatedTime
		,CreatedByWhom
		,UpdatedTime
		,UpdatedByWhom
		)
		values
		(
		 source.SpecialRegisterRecno
		,source.SourcePatientNo
		,source.SpecialRegisterCode
		,source.Alert
		,source.DistrictNo
		,source.NHSNumber
		,source.EnteredDate
		,source.LastModifiedTime
		,source.StartDate
		,source.EndDate
		,source.Active
		,source.ContextCode
		,getdate()
		,suser_name()	
		,getdate()
		,suser_name()
		)
		
when matched and not
	(
		isnull(target.SourcePatientNo, 0) = isnull(source.SourcePatientNo, 0)
	and isnull(target.SpecialRegisterCode, 0) = isnull(source.SpecialRegisterCode, 0)
	and isnull(target.Alert,0) = isnull(source.Alert, 0)
	and isnull(target.DistrictNo, 0) = isnull(source.DistrictNo, 0)
	and isnull(target.NHSNumber, 0) = isnull(source.NHSNumber, 0)
	and isnull(target.EnteredDate, getdate()) = isnull(source.EnteredDate, getdate())
	and isnull(target.LastModifiedTime, getdate()) = isnull(source.LastModifiedTime, getdate())
	and isnull(target.StartDate, getdate()) = isnull(source.StartDate, getdate())
	and isnull(target.EndDate, getdate()) = isnull(source.EndDate, getdate())
	and isnull(target.Active, 0) = isnull(source.Active, 0)
	)
	then update
		set
			 target.SourcePatientNo = source.SourcePatientNo
			,target.SpecialRegisterCode = source.SpecialRegisterCode
			,target.Alert = source.Alert
			,target.DistrictNo = source.DistrictNo
			,target.NHSNumber = source.NHSNumber
			,target.EnteredDate = source.EnteredDate
			,target.LastModifiedTime = source.LastModifiedTime
			,target.StartDate = source.StartDate
			,target.EndDate = source.EndDate
			,target.Active = source.Active
			,target.UpdatedTime = getdate()
			,target.UpdatedByWhom = suser_name()


output $Action into @MergeSummary	
;

/* Stats */
select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary
;

select @Elapsed = datediff(minute,@StartTime,getdate());

select @Stats = 

	'Deleted ' + convert(varchar(10), @deleted)  + 
	', Updated '  + convert(varchar(10), @updated) +  
	', Inserted '  + convert(varchar(10), @inserted) + 
	', Elapsed ' + convert(varchar(10), @Elapsed) + ' Mins' 


print @Stats;

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime;


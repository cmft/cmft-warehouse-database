﻿CREATE procedure [ETL].[ImportResultBaseResult] as

-------------------------------------------------
--When		Who	What
--20150508	DG	Created procedure
-------------------------------------------------

--load CWS and ICEs into TLoad table
exec ETL.BuildTLoadResultBaseResult

--load encounters and generate process list
exec ETL.LoadResultBaseResult

--generate missing reference data
declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Result].[BaseResult]'
	,@ProcessListTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Result].[ProcessList]'
	,@NewRows int

declare	@ActivityTableOverride varchar(max) = 
	'(
	select
		Result.*
	from
		' + @ActivityTable + ' Result

	inner join ' + @ProcessListTable + ' ProcessList
	on	ProcessList.MergeRecno = Result.MergeResultRecno
	)'

exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output,@Debug = 0 ,@ActivityTableOverride = @ActivityTableOverride

if @NewRows > 0
	exec WH.BuildMember

--load reference values
exec ETL.LoadResultBaseResultReference

----clean up process list
--exec ETL.BuildResultProcessListArchive	CH 04/08/15 removed from here and added as a step after allocations in the job
﻿CREATE TABLE [Recoding].[WrkFCEConfiguration] (
    [OriginalSpell]          BIT          NULL,
    [MergeRecno]             INT          NULL,
    [ProviderSpellNo]        VARCHAR (50) NULL,
    [EpisodeNo]              VARCHAR (20) NULL,
    [PseudoProviderSpellNo]  VARCHAR (30) NULL,
    [SequenceNo]             INT          NULL,
    [SecondaryOperationCode] VARCHAR (10) NULL
);


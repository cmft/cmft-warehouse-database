﻿CREATE TABLE [Recoding].[APCSpell] (
    [FCEConfigurationRecno]         INT          NOT NULL,
    [HRGCode]                VARCHAR (10) NULL,
    [DominantProcedureCode]  VARCHAR (20) NULL,
    [PrimaryDiagnosisCode]   VARCHAR (10) NULL,
    [SecondaryDiagnosisCode] VARCHAR (10) NULL,
    [EpisodeCount]           SMALLINT     NULL,
    [LOS]                    SMALLINT     NULL,
    [ReportingSpellLOS]      SMALLINT     NULL,
    [Trimpoint]              SMALLINT     NULL,
    [ExcessBeddays]          SMALLINT     NULL,
    [CCDays]                 SMALLINT     NULL,
    [ProgramBudgetCode]      VARCHAR (10) NULL,
    CONSTRAINT [PK_Recoding_APCSpell] PRIMARY KEY CLUSTERED (FCEConfigurationRecno ASC)
);


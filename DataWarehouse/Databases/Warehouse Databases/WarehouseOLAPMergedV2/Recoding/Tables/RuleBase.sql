﻿CREATE TABLE [Recoding].[RuleBase] (
    [RuleBaseRecno]                   INT           IDENTITY (1, 1) NOT NULL,
    [Template]                        VARCHAR (256) NOT NULL,
    [LeadingSecondaryOperationCode]   VARCHAR (256) NULL,
    [FollowingSecondaryOperationCode] VARCHAR (256) NULL,
    [ExceptionCode1]                  VARCHAR (256) NULL,
    [ExceptionCode2]                  VARCHAR (256) NULL,
    [RuleBaseListCode]                VARCHAR (256) NULL,
    CONSTRAINT [PK_Recoding_RuleBase_1] PRIMARY KEY CLUSTERED ([RuleBaseRecno] ASC)
);


﻿CREATE TABLE [Recoding].[RuleBaseList] (
    [RuleBaseListCode] VARCHAR (256) NOT NULL,
    [ListCode]         VARCHAR (256) NOT NULL,
    CONSTRAINT [PK_RuleBaseList_1] PRIMARY KEY CLUSTERED ([RuleBaseListCode] ASC, [ListCode] ASC)
);


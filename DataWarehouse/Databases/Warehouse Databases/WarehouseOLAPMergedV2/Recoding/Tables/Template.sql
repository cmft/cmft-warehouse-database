﻿CREATE TABLE [Recoding].[Template] (
    [Template] VARCHAR (255) NOT NULL,
    [Active]   BIT           CONSTRAINT [DF_Recoding_Template_Active] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Recoding_Template_1] PRIMARY KEY CLUSTERED ([Template] ASC)
);


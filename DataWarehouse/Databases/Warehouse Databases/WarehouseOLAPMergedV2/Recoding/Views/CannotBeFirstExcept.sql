﻿CREATE view [Recoding].[CannotBeFirstExcept] as

select
	 Leading.MergeRecno
	,Leading.PseudoProviderSpellNo
from
	Recoding.WrkFCEConfiguration Leading

inner join Recoding.RuleBase
on	RuleBase.Template = 'Recoding.CannotBeFirstExcept'

where
	Leading.SecondaryOperationCode like RuleBase.LeadingSecondaryOperationCode + '%'
and	Leading.SequenceNo = 0

and	exists
	(
	select
		1
	from
		Recoding.WrkFCEConfiguration LaterProcedure
	where
		LaterProcedure.MergeRecno = Leading.MergeRecno
	and	LaterProcedure.PseudoProviderSpellNo = Leading.PseudoProviderSpellNo
	and	LaterProcedure.SequenceNo > Leading.SequenceNo
	and	left(LaterProcedure.SecondaryOperationCode, 1) not in
		(
		select
			RuleBaseList.ListCode
		from
			Recoding.RuleBaseList
		where
			RuleBaseList.RuleBaseListCode = RuleBase.RuleBaseListCode
		)
	)

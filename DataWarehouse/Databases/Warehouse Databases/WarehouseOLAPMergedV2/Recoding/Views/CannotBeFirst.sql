﻿CREATE view Recoding.[CannotBeFirst] as

select
	 Leading.MergeRecno
	,Leading.PseudoProviderSpellNo
from
	Recoding.WrkFCEConfiguration Leading

inner join Recoding.RuleBase
on	RuleBase.Template = 'Recoding.CannotBeFirst'

where
	Leading.SecondaryOperationCode like RuleBase.LeadingSecondaryOperationCode + '%'
and	Leading.SequenceNo = 0
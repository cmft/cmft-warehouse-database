﻿create view [Recoding].[CannotBeBefore] as

select
	 Leading.MergeRecno
	,Leading.PseudoProviderSpellNo
from
	Recoding.WrkFCEConfiguration Leading

inner join Recoding.RuleBase
on	RuleBase.Template = 'Recoding.CannotBeBefore'

where
	Leading.SecondaryOperationCode like RuleBase.LeadingSecondaryOperationCode + '%'

and	exists
	(
	select
		1
	from
		Recoding.WrkFCEConfiguration LaterProcedure
	where
		LaterProcedure.MergeRecno = Leading.MergeRecno
	and	LaterProcedure.PseudoProviderSpellNo = Leading.PseudoProviderSpellNo
	and	LaterProcedure.SequenceNo > Leading.SequenceNo
	and	LaterProcedure.SecondaryOperationCode like RuleBase.FollowingSecondaryOperationCode + '%'
	)
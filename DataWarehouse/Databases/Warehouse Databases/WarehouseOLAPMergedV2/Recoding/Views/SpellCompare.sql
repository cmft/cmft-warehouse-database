﻿CREATE VIEW Recoding.[SpellCompare] as

with APCSpellPriced
as
	(
	select
		 FCEConfiguration.FCEConfigurationRecno
		,APCSpell.HRGCode

		,Tariff =
			coalesce(
				case
				when
					(
						AdmissionMethod.NationalAdmissionMethodCode between '21' and '28'
					or	AdmissionMethod.NationalAdmissionMethodCode = '81'
					)
				and	PatientClassification.NationalPatientClassificationCode = '1'
				then Tariff.NonElectiveSpellTariff

				when
					AdmissionMethod.NationalAdmissionMethodCode between '11' and '13'
				and	PatientClassification.NationalPatientClassificationCode = '1'
				then Tariff.ElectiveSpellTariff

				when
					AdmissionMethod.NationalAdmissionMethodCode between '11' and '13'
				and	PatientClassification.NationalPatientClassificationCode = '2'
				then Tariff.DaycaseTariff

				end

				,Tariff.CombinedDaycaseElectiveTariff
			)

		,APCSpell.DominantProcedureCode
		,FCEConfiguration.OriginalSpell
		,FCEConfiguration.ProviderSpellNo
		,FCEConfiguration.EpisodeNo
		,FCEConfiguration.PseudoProviderSpellNo
		,FCEConfiguration.PrimaryOperationCode
		,FCEConfiguration.SecondaryOperationCode1
		,FCEConfiguration.SecondaryOperationCode2
		,FCEConfiguration.SecondaryOperationCode3
		,FCEConfiguration.SecondaryOperationCode4
		,FCEConfiguration.SecondaryOperationCode5
		,FCEConfiguration.SecondaryOperationCode6
		,FCEConfiguration.SecondaryOperationCode7
		,FCEConfiguration.SecondaryOperationCode8
		,FCEConfiguration.SecondaryOperationCode9
		,FCEConfiguration.SecondaryOperationCode10
		,FCEConfiguration.SecondaryOperationCode11
		,FCEConfiguration.SecondaryOperationCode12
		,FCEConfiguration.SecondaryOperationCode13
		,FCEConfiguration.SecondaryOperationCode14
		,FCEConfiguration.SecondaryOperationCode15
		,FCEConfiguration.SecondaryOperationCode16
	from
		Recoding.APCSpell

	inner join Recoding.FCEConfiguration
	on	FCEConfiguration.FCEConfigurationRecno = APCSpell.FCEConfigurationRecno

	inner join APC.BaseEncounterReference
	on	BaseEncounterReference.MergeEncounterRecno = FCEConfiguration.MergeRecno

	left join APC.AdmissionMethod
	on	AdmissionMethod.SourceAdmissionMethodID = BaseEncounterReference.AdmissionMethodID

	left join APC.PatientClassification
	on	SourcePatientClassificationID = BaseEncounterReference.PatientClassificationID

	inner join HRG43.APCOPProcedureTariff Tariff
	on	Tariff.HRGCode = APCSpell.HRGCode
)


select
	 OriginalSpellTariff = OriginalSpell.Tariff
	,SwitchTariff = APCSpellPriced.Tariff
	,TariffDifference = APCSpellPriced.Tariff - OriginalSpell.Tariff
	,OriginalSpellHRGCode = OriginalSpell.HRGCode
	,SwitchHRGCode = APCSpellPriced.HRGCode
	,APCSpellPriced.ProviderSpellNo
	,APCSpellPriced.FCEConfigurationRecno
from
	APCSpellPriced

inner join APCSpellPriced OriginalSpell
on	OriginalSpell.ProviderSpellNo = APCSpellPriced.ProviderSpellNo
and	OriginalSpell.OriginalSpell = 1
and	OriginalSpell.Tariff < APCSpellPriced.Tariff

and	not exists
	(
	select
		1
	from
		APCSpellPriced Previous
	where
		Previous.ProviderSpellNo = APCSpellPriced.ProviderSpellNo
	and	(
			Previous.Tariff > APCSpellPriced.Tariff
		or	(
				Previous.Tariff = APCSpellPriced.Tariff
			and	Previous.FCEConfigurationRecno > APCSpellPriced.FCEConfigurationRecno
			)
		)
	)


﻿CREATE view Recoding.[MustBeFirst] as

select
	 Leading.MergeRecno
	,Leading.PseudoProviderSpellNo
from
	Recoding.WrkFCEConfiguration Leading

inner join Recoding.RuleBase
on	RuleBase.Template = 'Recoding.MustBeFirst'

where
	Leading.SecondaryOperationCode like RuleBase.LeadingSecondaryOperationCode + '%'

and	Leading.SequenceNo > 0
﻿CREATE view [Recoding].[APCInputFile] as

with

SecondaryDiagnoses as (
	select
		MergeRecno,
		DIAG_01 = [00],
		DIAG_02 = [01], DIAG_03 = [02], DIAG_04 = [03], DIAG_05 = [04], DIAG_06 = [05],
		DIAG_07 = [06], DIAG_08 = [07], DIAG_09 = [08], DIAG_10 = [09], DIAG_11 = [10],
		DIAG_12 = [11], DIAG_13 = [12], DIAG_14 = [13], DIAG_15 = [14], DIAG_16 = [15],
		DIAG_17 = [16], DIAG_18 = [17], DIAG_19 = [18], DIAG_20 = [19], DIAG_21 = [20],
		DIAG_22 = [21], DIAG_23 = [22], DIAG_24 = [23], DIAG_25 = [24], DIAG_26 = [25],
		DIAG_27 = [26], DIAG_28 = [27], DIAG_29 = [28], DIAG_30 = [29], DIAG_31 = [30],
		DIAG_32 = [31], DIAG_33 = [32], DIAG_34 = [33], DIAG_35 = [34], DIAG_36 = [35],
		DIAG_37 = [36], DIAG_38 = [37], DIAG_39 = [38], DIAG_40 = [39], DIAG_41 = [40],
		DIAG_42 = [41], DIAG_43 = [42], DIAG_44 = [43], DIAG_45 = [44], DIAG_46 = [45],
		DIAG_47 = [46], DIAG_48 = [47], DIAG_49 = [48], DIAG_50 = [49], DIAG_51 = [50],
		DIAG_52 = [51], DIAG_53 = [52], DIAG_54 = [53], DIAG_55 = [54], DIAG_56 = [55],
		DIAG_57 = [56], DIAG_58 = [57], DIAG_59 = [58], DIAG_60 = [59], DIAG_61 = [60],
		DIAG_62 = [61], DIAG_63 = [62], DIAG_64 = [63], DIAG_65 = [64], DIAG_66 = [65],
		DIAG_67 = [66], DIAG_68 = [67], DIAG_69 = [68], DIAG_70 = [69], DIAG_71 = [70],
		DIAG_72 = [71], DIAG_73 = [72], DIAG_74 = [73], DIAG_75 = [74], DIAG_76 = [75],
		DIAG_77 = [76], DIAG_78 = [77], DIAG_79 = [78], DIAG_80 = [79], DIAG_81 = [80],
		DIAG_82 = [81], DIAG_83 = [82], DIAG_84 = [83], DIAG_85 = [84], DIAG_86 = [85],
		DIAG_87 = [86], DIAG_88 = [87], DIAG_89 = [88], DIAG_90 = [89], DIAG_91 = [90],
		DIAG_92 = [91], DIAG_93 = [92], DIAG_94 = [93], DIAG_95 = [94], DIAG_96 = [95],
		DIAG_97 = [96], DIAG_98 = [97], DIAG_99 = [98]
	from (
		select
			MergeRecno = SecondaryDiagnosis.MergeEncounterRecno
			,DiagnosisCode = left(replace(replace(SecondaryDiagnosis.DiagnosisCode, '.', ''), ',', ''), 5)
			,SecondaryDiagnosis.SequenceNo
		from
			APC.BaseDiagnosis SecondaryDiagnosis
		where
			charindex('/', SecondaryDiagnosis.DiagnosisCode) = 0 --SUS removes morphology codes before grouping (ICD codes containing “/”)
	) a
	pivot (
		max(DiagnosisCode) for SequenceNo in (
			[00],
			[01], [02], [03], [04], [05], [06], [07], [08], [09], [10],
			[11], [12], [13], [14], [15], [16], [17], [18], [19], [20],
			[21], [22], [23], [24], [25], [26], [27], [28], [29], [30],
			[31], [32], [33], [34], [35], [36], [37], [38], [39], [40],
			[41], [42], [43], [44], [45], [46], [47], [48], [49], [50],
			[51], [52], [53], [54], [55], [56], [57], [58], [59], [60],
			[61], [62], [63], [64], [65], [66], [67], [68], [69], [70],
			[71], [72], [73], [74], [75], [76], [77], [78], [79], [80],
			[81], [82], [83], [84], [85], [86], [87], [88], [89], [90],
			[91], [92], [93], [94], [95], [96], [97], [98], [99]
		)
	) p
)

select
	 PROCODET = 'RW3' --Encounter.ProviderCode
	,PROVSPNO = left(Encounter.GlobalProviderSpellNo + '|' + FCEConfiguration.PseudoProviderSpellNo, 50)
	,EPIORDER = FCEConfiguration.EpisodeNo
	,STARTAGE = [$(CMFT)].Dates.GetAge(
								Encounter.DateOfBirth
								,Encounter.EpisodeStartDate
								)
	,SEX = Sex.NationalSexCode		
	,CLASSPAT = PatientClassification.NationalPatientClassificationCode
	,ADMISORC = AdmissionSource.NationalAdmissionSourceCode
	,ADMIMETH = AdmissionMethod.NationalAdmissionMethodCode
	,DISDEST = DischargeDestination.NationalDischargeDestinationCode
	,DISMETH = DischargeMethod.NationalDischargeMethodCode
	,EPIDUR =
			case
				when datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate) - coalesce(Bedday.Bedday, 0) <= 0 
				then 0
				else datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate) - coalesce(Bedday.Bedday, 0)
			end		
	,MAINSPEF = Consultant.MainSpecialtyCode
	,NEOCARE = NeonatalLevelOfCare.NationalNeonatalLevelOfCareCode
	,TRETSPEF = TreatmentFunction.NationalSpecialtyCode

	,DIAG_01 = SecondaryDiagnoses.DIAG_01
	,DIAG_02 = SecondaryDiagnoses.DIAG_02
	,DIAG_03 = SecondaryDiagnoses.DIAG_03
	,DIAG_04 = SecondaryDiagnoses.DIAG_04
	,DIAG_05 = SecondaryDiagnoses.DIAG_05
	,DIAG_06 = SecondaryDiagnoses.DIAG_06
	,DIAG_07 = SecondaryDiagnoses.DIAG_07
	,DIAG_08 = SecondaryDiagnoses.DIAG_08
	,DIAG_09 = SecondaryDiagnoses.DIAG_09
	,DIAG_10 = SecondaryDiagnoses.DIAG_10
	,DIAG_11 = SecondaryDiagnoses.DIAG_11
	,DIAG_12 = SecondaryDiagnoses.DIAG_12
	,DIAG_13 = SecondaryDiagnoses.DIAG_13
	,DIAG_14 = SecondaryDiagnoses.DIAG_14
	,DIAG_15 = SecondaryDiagnoses.DIAG_15
	,DIAG_16 = SecondaryDiagnoses.DIAG_16

	,OPER_01 = FCEConfiguration.PrimaryOperationCode
	,OPER_02 = FCEConfiguration.SecondaryOperationCode1
	,OPER_03 = FCEConfiguration.SecondaryOperationCode2
	,OPER_04 = FCEConfiguration.SecondaryOperationCode3
	,OPER_05 = FCEConfiguration.SecondaryOperationCode4
	,OPER_06 = FCEConfiguration.SecondaryOperationCode5
	,OPER_07 = FCEConfiguration.SecondaryOperationCode6
	,OPER_08 = FCEConfiguration.SecondaryOperationCode7
	,OPER_09 = FCEConfiguration.SecondaryOperationCode8
	,OPER_10 = FCEConfiguration.SecondaryOperationCode9
	,OPER_11 = FCEConfiguration.SecondaryOperationCode10
	,OPER_12 = FCEConfiguration.SecondaryOperationCode11

	,CRITICALCAREDAYS = 0
	,REHABILITATIONDAYS = 0
	,SPECIALISTPALLIATIVECAREDAYS = 0
	,MergeEncounterRecno = FCEConfiguration.FCEConfigurationRecno --unique configuration record number
	,FinancialYear = 
				case
				when DischargeDate.FinancialYear = '2015/2016' -- using 14/15 grouper for 15/16 data - confirmed by Phil Huitson 7 Apr 2015
				then '2014/2015'
				else DischargeDate.FinancialYear 
				end

from
	APC.BaseEncounter Encounter

inner join Recoding.FCEConfiguration
on	FCEConfiguration.MergeRecno = Encounter.MergeEncounterRecno

inner join APC.BaseEncounterReference EncounterReference
on	Encounter.MergeEncounterRecno = EncounterReference.MergeEncounterRecno 

inner join APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

left join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join APC.BaseEncounterReference AdmissionReference
on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Discharge.MergeEncounterRecno

left outer join WH.Specialty TreatmentFunction
on	TreatmentFunction.SourceSpecialtyID = EncounterReference.SpecialtyID

left outer join WH.Consultant
on	Consultant.SourceConsultantID = EncounterReference.ConsultantID

left outer join APC.AdmissionSource
on	AdmissionSource.SourceAdmissionSourceID = AdmissionReference.AdmissionSourceID

left outer join APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = AdmissionReference.AdmissionMethodID

left outer join WH.Calendar DischargeDate
on	DischargeDate.DateID = DischargeReference.DischargeDateID

left outer join WH.Sex Sex
on	Sex.SourceSexID = EncounterReference.SexID

left outer join APC.PatientClassification
on	SourcePatientClassificationID = EncounterReference.PatientClassificationID

left outer join APC.DischargeDestination  -- should be inner performance reasons 
on	SourceDischargeDestinationID = DischargeReference.DischargeDestinationID

left outer join APC.DischargeMethod -- should be inner performance reasons 
on	SourceDischargeMethodID = DischargeReference.DischargeMethodID

left outer join APC.NeonatalLevelOfCare -- should be inner performance reasons 
on	SourceNeonatalLevelOfCareID = EncounterReference.NeonatalLevelOfCareID

left join SecondaryDiagnoses
on	SecondaryDiagnoses.MergeRecno = Encounter.MergeEncounterRecno

left outer join
	(
	select
		MergeAPCEncounterRecno
		,Bedday = count(*)
	from
		[APC].[BedDay]
	group by
		MergeAPCEncounterRecno
				
	) Bedday
on	Bedday.MergeAPCEncounterRecno = Encounter.MergeEncounterRecno


		

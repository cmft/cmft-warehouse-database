﻿CREATE view [Recoding].[CannotBeFirstAndBefore] as

select
	 Leading.MergeRecno
	,Leading.PseudoProviderSpellNo
from
	Recoding.WrkFCEConfiguration Leading

inner join Recoding.RuleBase
on	RuleBase.Template = 'Recoding.CannotBeFirstAndBefore'

where
	Leading.SecondaryOperationCode like RuleBase.LeadingSecondaryOperationCode + '%'
and	Leading.SequenceNo = 0

and	exists
	(
	select
		1
	from
		Recoding.WrkFCEConfiguration LaterProcedure
	where
		LaterProcedure.MergeRecno = Leading.MergeRecno
	and	LaterProcedure.PseudoProviderSpellNo = Leading.PseudoProviderSpellNo
	and	LaterProcedure.SequenceNo > Leading.SequenceNo
	and	LaterProcedure.SecondaryOperationCode like RuleBase.FollowingSecondaryOperationCode + '%'
	)

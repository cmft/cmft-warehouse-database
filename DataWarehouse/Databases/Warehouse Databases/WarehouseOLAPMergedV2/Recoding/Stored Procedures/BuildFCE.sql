﻿CREATE procedure [Recoding].[BuildFCE]
	 @from date = null
	,@to date = null
as

select
	@from =
		coalesce(
			 @from

--go to the 1st of the previous month if the day number > 19
--otherwise go to the month before that

			,DATEADD(
				day
				,(datepart(dd, GETDATE()) - 1) * -1
				,DATEADD(
					month

					,case
					when datepart(dd, GETDATE()) > 19
					then -1
					else -2
					end

					,CAST(getdate() as date)
				)
			)
		)

	,@to =
		coalesce(
			 @to
			,getdate()
		)

select
	 MergeRecno = Operation.MergeEncounterRecno
	,SequenceNo = Operation.SequenceNo --subsequent code expects PrimaryOperationCode to have a SequenceNo of 0
	,OperationCode = left(replace(replace(Operation.OperationCode, '.', ''), ',', ''), 4)
into
	#APCProcedure
from
	APC.Encounter APCEncounter

inner join APC.Operation Operation
on	Operation.MergeEncounterRecno = APCEncounter.MergeEncounterRecno

where
	APCEncounter.DischargeDate between @from and @to


create table #OperationSwitch
(
	 MergeRecno int
	,InSequenceNo int
	,InOperationCode varchar(10) 
	,OutSequenceNo int
	,OutOperationCode varchar(10) 
)



declare @SQL varchar(max)

declare @MergeRecno int
declare @SequenceNo int
declare @OperationCode varchar(10)

declare @InitialMergeRecno int
declare @InitialSequenceNo int
declare @InitialOperationCode varchar(10)

declare switchcursor cursor fast_forward for

--(Global) spells with at least 1 episode with >1 operation that could be worthwhile switching
select
	 MergeRecno = BaseEncounter.MergeEncounterRecno
	,Operation.SequenceNo
	,OperationCode = left(replace(replace(Operation.OperationCode, '.', ''), ',', ''), 4)
from
	APC.BaseEncounter

inner join APC.Operation
on	Operation.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

where
	BaseEncounter.Reportable = 1

and	exists
	(
	select
		1
	from
		(
		select
			 Encounter.GlobalProviderSpellNo
			,Encounter.GlobalEpisodeNo
			,Hierarchy.HierarchyValue
		from
			APC.BaseEncounter Encounter

	   inner join APC.BaseEncounter Admission
	   on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
	   and	Admission.GlobalEpisodeNo = 1

	   inner join APC.Spell
	   on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

	   inner join APC.BaseEncounter Discharge
	   on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno			

		inner join APC.Operation
		on	Operation.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
		
		inner join HRG43.Hierarchy
		on	Hierarchy.HierarchyID = 'ProcHier'
		and	Hierarchy.HierarchyTypeCode = 'OPCS'
		and	Hierarchy.HierarchyCode = left(replace(replace(Operation.OperationCode, '.', ''), ',', ''), 4)
		and	Hierarchy.HierarchyValue > 2

		where
			Discharge.DischargeDate between @from and @to
		and Encounter.Reportable = 1

		group by
			 Encounter.GlobalProviderSpellNo
			,Encounter.GlobalEpisodeNo
			,Hierarchy.HierarchyValue
		having
			COUNT(*) > 1
		) Encounter
	where
		Encounter.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
	)
order by
	 MergeRecno
	,SequenceNo
	,OperationCode

OPEN switchcursor

FETCH NEXT FROM switchcursor

INTO
	 @InitialMergeRecno
	,@InitialSequenceNo
	,@InitialOperationCode

FETCH NEXT FROM switchcursor

INTO
	 @MergeRecno
	,@SequenceNo
	,@OperationCode

WHILE @@FETCH_STATUS = 0

BEGIN

	if @InitialMergeRecno = @MergeRecno
	begin
		if @InitialOperationCode <> @OperationCode
			insert
			into
				#OperationSwitch
			(
				 MergeRecno
				,InSequenceNo
				,InOperationCode
				,OutSequenceNo
				,OutOperationCode
			)
			select
				 @InitialMergeRecno
				,@InitialSequenceNo
				,@OperationCode
				,@SequenceNo
				,@InitialOperationCode
	end
	else
	begin
		select
			 @InitialMergeRecno = @MergeRecno
			,@InitialSequenceNo = @SequenceNo
			,@InitialOperationCode = @OperationCode
	end
	
	FETCH NEXT FROM switchcursor
	INTO
		 @MergeRecno
		,@SequenceNo
		,@OperationCode

END
  
CLOSE switchcursor
DEALLOCATE switchcursor


truncate table Recoding.FCEConfiguration

;
with SecondaryOperations as (
	select
		MergeRecno

		,OPER_00 = [00]
		,OPER_01 = [01]
		,OPER_02 = [02]
		,OPER_03 = [03]
		,OPER_04 = [04]
		,OPER_05 = [05]
		,OPER_06 = [06]
		,OPER_07 = [07]
		,OPER_08 = [08]
		,OPER_09 = [09]
		,OPER_10 = [10]
		,OPER_11 = [11]
		,OPER_12 = [12]
		,OPER_13 = [13]
		,OPER_14 = [14]
		,OPER_15 = [15]
		,OPER_16 = [16]

	from (
		select
			MergeRecno = SecondaryOperation.MergeEncounterRecno
			,SequenceNo = SecondaryOperation.SequenceNo
			,OperationCode = left(replace(replace(SecondaryOperation.OperationCode, '.', ''), ',', ''), 4)
		from
			APC.Operation SecondaryOperation
	) a
	pivot (
		max(OperationCode) for SequenceNo in (
			[00], 
			[01], [02], [03], [04], [05], 
			[06], [07], [08], [09], [10], 
			[11], [12], [13], [14], [15], [16]
		)
	) p
)

insert
into
	Recoding.FCEConfiguration
(
	 OriginalSpell
	,MergeRecno
	,ProviderSpellNo
	,EpisodeNo
	,PseudoProviderSpellNo
	,PrimaryOperationCode
	,SecondaryOperationCode1
	,SecondaryOperationCode2
	,SecondaryOperationCode3
	,SecondaryOperationCode4
	,SecondaryOperationCode5
	,SecondaryOperationCode6
	,SecondaryOperationCode7
	,SecondaryOperationCode8
	,SecondaryOperationCode9
	,SecondaryOperationCode10
	,SecondaryOperationCode11
	,SecondaryOperationCode12
	,SecondaryOperationCode13
	,SecondaryOperationCode14
	,SecondaryOperationCode15
	,SecondaryOperationCode16
	)
select distinct
	 OriginalSpell = CAST(0 as bit)
	,SpellFCE.MergeEncounterRecno
	,SpellFCE.GlobalProviderSpellNo
	,SpellFCE.GlobalEpisodeNo

	,PseudoProviderSpellNo =
		CAST(
			ROW_NUMBER()
			over(
				partition by
					 SpellFCE.GlobalProviderSpellNo
					,SpellFCE.GlobalEpisodeNo

				order by
					 SpellFCE.GlobalProviderSpellNo
					,SpellFCE.GlobalEpisodeNo
			)
			as varchar
		)

	,PrimaryOperationCode =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 0
			then #OperationSwitch.InOperationCode
			else SecondaryOperations.OPER_00
			end
		else SecondaryOperations.OPER_00
		end

	,SecondaryOperationCode1 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 1
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 1
			then #OperationSwitch.OutOperationCode
			else SecondaryOperations.OPER_01
			end
		else SecondaryOperations.OPER_01
		end

	,SecondaryOperationCode2 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 2
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 2
			then #OperationSwitch.OutOperationCode
			else SecondaryOperations.OPER_02
			end
		else SecondaryOperations.OPER_02
		end

	,SecondaryOperationCode3 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 3
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 3
			then #OperationSwitch.OutOperationCode
			else SecondaryOperations.OPER_03
			end
		else SecondaryOperations.OPER_03
		end

	,SecondaryOperationCode4 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 4
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 4
			then #OperationSwitch.OutOperationCode
			else SecondaryOperations.OPER_04
			end
		else SecondaryOperations.OPER_04
		end

	,SecondaryOperationCode5 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 5
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 5
			then #OperationSwitch.OutOperationCode
			else SecondaryOperations.OPER_05
			end
		else SecondaryOperations.OPER_05
		end

	,SecondaryOperationCode6 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 6
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 6
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_06
		end
		else SecondaryOperations.OPER_06
		end

	,SecondaryOperationCode7 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 7
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 7
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_07
		end
		else SecondaryOperations.OPER_07
		end

	,SecondaryOperationCode8 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 8
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 8
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_08
		end
		else SecondaryOperations.OPER_08
		end

	,SecondaryOperationCode9 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 9
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 9
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_09
		end
		else SecondaryOperations.OPER_09
		end

	,SecondaryOperationCode10 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 10
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 10
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_10
		end
		else SecondaryOperations.OPER_10
		end

	,SecondaryOperationCode11 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 11
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 11
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_11
		end
		else SecondaryOperations.OPER_11
		end

	,SecondaryOperationCode12 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 12
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 12
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_12
		end
		else SecondaryOperations.OPER_12
		end

	,SecondaryOperationCode13 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 13
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 13
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_13
		end
		else SecondaryOperations.OPER_13
		end

	,SecondaryOperationCode14 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 14
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 14
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_14
		end
		else SecondaryOperations.OPER_14
		end

	,SecondaryOperationCode15 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 15
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 15
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_15
		end
		else SecondaryOperations.OPER_15
		end

	,SecondaryOperationCode16 =
		case
		when #OperationSwitch.MergeRecno = SpellFCE.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 16
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 16
		then #OperationSwitch.OutOperationCode
		else SecondaryOperations.OPER_16
		end
		else SecondaryOperations.OPER_16
		end

from
	(
	select distinct
		*
	from
		#OperationSwitch
	) #OperationSwitch

inner join APC.Encounter FCE
on	FCE.MergeEncounterRecno = #OperationSwitch.MergeRecno

inner join APC.Encounter SpellFCE
on	SpellFCE.GlobalProviderSpellNo = FCE.GlobalProviderSpellNo

left join SecondaryOperations
on	SecondaryOperations.MergeRecno = SpellFCE.MergeEncounterRecno

--add original fces
;
with SecondaryOperations as (
	select
		MergeRecno

		,OPER_00 = [00]
		,OPER_01 = [01]
		,OPER_02 = [02]
		,OPER_03 = [03]
		,OPER_04 = [04]
		,OPER_05 = [05]
		,OPER_06 = [06]
		,OPER_07 = [07]
		,OPER_08 = [08]
		,OPER_09 = [09]
		,OPER_10 = [10]
		,OPER_11 = [11]
		,OPER_12 = [12]
		,OPER_13 = [13]
		,OPER_14 = [14]
		,OPER_15 = [15]
		,OPER_16 = [16]

	from (
		select
			MergeRecno = SecondaryOperation.MergeEncounterRecno
			,SequenceNo = SecondaryOperation.SequenceNo
			,OperationCode = left(replace(replace(SecondaryOperation.OperationCode, '.', ''), ',', ''), 4)
		from
			APC.Operation SecondaryOperation
	) a
	pivot (
		max(OperationCode) for SequenceNo in (
			[00], 
			[01], [02], [03], [04], [05], 
			[06], [07], [08], [09], [10], 
			[11], [12], [13], [14], [15], [16]
		)
	) p
)

insert
into
	Recoding.FCEConfiguration
(
	 OriginalSpell
	,MergeRecno
	,ProviderSpellNo
	,EpisodeNo
	,PseudoProviderSpellNo
	,PrimaryOperationCode
	,SecondaryOperationCode1
	,SecondaryOperationCode2
	,SecondaryOperationCode3
	,SecondaryOperationCode4
	,SecondaryOperationCode5
	,SecondaryOperationCode6
	,SecondaryOperationCode7
	,SecondaryOperationCode8
	,SecondaryOperationCode9
	,SecondaryOperationCode10
	,SecondaryOperationCode11
	,SecondaryOperationCode12
	,SecondaryOperationCode13
	,SecondaryOperationCode14
	,SecondaryOperationCode15
	,SecondaryOperationCode16
)
select
	 OriginalSpell = CAST(1 as bit)
	,FCE.MergeEncounterRecno
	,FCE.GlobalProviderSpellNo
	,FCE.GlobalEpisodeNo

	,PseudoProviderSpellNo = 0
	
	,PrimaryOperationCode =
		SecondaryOperations.OPER_00

	,SecondaryOperationCode1 =
		SecondaryOperations.OPER_01

	,SecondaryOperationCode2 =
		SecondaryOperations.OPER_02

	,SecondaryOperationCode3 =
		SecondaryOperations.OPER_03

	,SecondaryOperationCode4 =
		SecondaryOperations.OPER_04

	,SecondaryOperationCode5 =
		SecondaryOperations.OPER_05

	,SecondaryOperationCode6 =
		SecondaryOperations.OPER_06

	,SecondaryOperationCode7 =
		SecondaryOperations.OPER_07

	,SecondaryOperationCode8 =
		SecondaryOperations.OPER_08

	,SecondaryOperationCode9 =
		SecondaryOperations.OPER_09

	,SecondaryOperationCode10 =
		SecondaryOperations.OPER_10

	,SecondaryOperationCode11 =
		SecondaryOperations.OPER_11

	,SecondaryOperationCode12 =
		SecondaryOperations.OPER_12

	,SecondaryOperationCode13 =
		SecondaryOperations.OPER_13

	,SecondaryOperationCode14 =
		SecondaryOperations.OPER_14

	,SecondaryOperationCode15 =
		SecondaryOperations.OPER_15

	,SecondaryOperationCode16 =
		SecondaryOperations.OPER_16

from
	APC.Encounter FCE

left join SecondaryOperations
on	SecondaryOperations.MergeRecno = FCE.MergeEncounterRecno

where
	exists
	(
	select
		1
	from
		Recoding.FCEConfiguration
	where
		FCEConfiguration.ProviderSpellNo = FCE.GlobalProviderSpellNo
	)


exec Recoding.RemoveInvalidConfiguration

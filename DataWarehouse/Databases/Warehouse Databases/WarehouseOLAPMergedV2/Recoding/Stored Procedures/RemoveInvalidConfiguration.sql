﻿CREATE procedure [Recoding].[RemoveInvalidConfiguration] as


--This procedure removes any invalid pseudo spells
 

--unpivot the generated FCEConfiguration data to Recoding.WrkFCEConfiguration

truncate table Recoding.WrkFCEConfiguration

insert
into
	Recoding.WrkFCEConfiguration
(
	 OriginalSpell
	,MergeRecno
	,ProviderSpellNo
	,EpisodeNo
	,PseudoProviderSpellNo
	,SequenceNo
	,SecondaryOperationCode
)
select
	 Unpivoted.OriginalSpell
	,Unpivoted.MergeRecno
	,Unpivoted.ProviderSpellNo
	,Unpivoted.EpisodeNo
	,Unpivoted.PseudoProviderSpellNo

	,SequenceNo =
		SUBSTRING(Unpivoted.SecondaryOperationCodeID, 23, 2)

	,Unpivoted.SecondaryOperationCode
from 
    (
	select
		 OriginalSpell
		,MergeRecno
		,ProviderSpellNo
		,EpisodeNo
		,PseudoProviderSpellNo
		,SecondaryOperationCode0 = PrimaryOperationCode
		,SecondaryOperationCode1
		,SecondaryOperationCode2
		,SecondaryOperationCode3
		,SecondaryOperationCode4
		,SecondaryOperationCode5
		,SecondaryOperationCode6
		,SecondaryOperationCode7
		,SecondaryOperationCode8
		,SecondaryOperationCode9
		,SecondaryOperationCode10
		,SecondaryOperationCode11
		,SecondaryOperationCode12
		,SecondaryOperationCode13
		,SecondaryOperationCode14
		,SecondaryOperationCode15
		,SecondaryOperationCode16
		,SecondaryOperationCode17
		,SecondaryOperationCode18
		,SecondaryOperationCode19
		,SecondaryOperationCode20
		,SecondaryOperationCode21
		,SecondaryOperationCode22
		,SecondaryOperationCode23
		,SecondaryOperationCode24
	from
		Recoding.FCEConfiguration

	) FCEConfiguration

UNPIVOT 
    (SecondaryOperationCode FOR SecondaryOperationCodeID IN 
    (
		 SecondaryOperationCode0
		,SecondaryOperationCode1
		,SecondaryOperationCode2
		,SecondaryOperationCode3
		,SecondaryOperationCode4
		,SecondaryOperationCode5
		,SecondaryOperationCode6
		,SecondaryOperationCode7
		,SecondaryOperationCode8
		,SecondaryOperationCode9
		,SecondaryOperationCode10
		,SecondaryOperationCode11
		,SecondaryOperationCode12
		,SecondaryOperationCode13
		,SecondaryOperationCode14
		,SecondaryOperationCode15
		,SecondaryOperationCode16
		,SecondaryOperationCode17
		,SecondaryOperationCode18
		,SecondaryOperationCode19
		,SecondaryOperationCode20
		,SecondaryOperationCode21
		,SecondaryOperationCode22
		,SecondaryOperationCode23
		,SecondaryOperationCode24

	) 
) AS Unpivoted


--build a table of exclusions...
declare @Template varchar(256)
declare @WrkSQL varchar(max)

truncate table Recoding.WrkExclusion


declare TemplateCursor cursor fast_forward for

select distinct
	 Template.Template
from
	Recoding.Template
where  	
	Template.Active = 1 
order by
	Template.Template

OPEN TemplateCursor

FETCH NEXT FROM TemplateCursor

INTO
	 @Template

WHILE @@FETCH_STATUS = 0

BEGIN

	select
		@WrkSQL =
			'
			insert
			into
				Recoding.WrkExclusion 
				(
				 MergeRecno
				,PseudoProviderSpellNo
				) 
			select
				 MergeRecno
				,PseudoProviderSpellNo
			from '
			 + @Template

	print ''
	print convert(varchar, getdate(), 113) + ' - ' + @Template + ' - ' + @WrkSQL

	execute(@WrkSQL)

	FETCH NEXT FROM TemplateCursor
	INTO @Template

END
  
CLOSE TemplateCursor
DEALLOCATE TemplateCursor


--delete invalid combinations

--begin transaction

delete
from
	Recoding.FCEConfiguration
where
	exists
	(
	select
		1
	from
		Recoding.WrkExclusion
		
	inner join Recoding.FCEConfiguration Spell
	on	Spell.MergeRecno = WrkExclusion.MergeRecno
	
	where
		Spell.ProviderSpellNo = FCEConfiguration.ProviderSpellNo
	and	WrkExclusion.PseudoProviderSpellNo = FCEConfiguration.PseudoProviderSpellNo
	)
and	FCEConfiguration.OriginalSpell = 0

--rollback transaction
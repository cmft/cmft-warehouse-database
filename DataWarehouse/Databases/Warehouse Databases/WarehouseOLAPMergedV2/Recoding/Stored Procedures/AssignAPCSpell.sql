﻿CREATE procedure [Recoding].[AssignAPCSpell] as


--process APC Spell Configuration
truncate table Recoding.APCSpell

INSERT INTO Recoding.APCSpell
	(
	 FCEConfigurationRecno
	,HRGCode
	,DominantProcedureCode
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode
	,EpisodeCount
	,LOS
	,ReportingSpellLOS
	,Trimpoint
	,ExcessBeddays
	,CCDays
	,ProgramBudgetCode
	)
select
	 FCEConfigurationRecno =
		coalesce(
			(
			select top 1
				FCEConfiguration.FCEConfigurationRecno
			from
				Recoding.FCEConfiguration
			where
				FCEConfiguration.ProviderSpellNo = Encounter.ProviderSpellNo
			and	FCEConfiguration.PseudoProviderSpellNo = Encounter.PseudoProviderSpellNo
			and	FCEConfiguration.OriginalSpell = 0

			--which episodes have changed from original
			and	not exists
				(
				select
					1
				from
					Recoding.FCEConfiguration OriginalEpisode
				where
					OriginalEpisode.ProviderSpellNo = FCEConfiguration.ProviderSpellNo
				and	OriginalEpisode.EpisodeNo = FCEConfiguration.EpisodeNo
				and	OriginalEpisode.OriginalSpell = 1
				and
					CHECKSUM(
						 OriginalEpisode.PrimaryOperationCode
						,OriginalEpisode.SecondaryOperationCode1
						,OriginalEpisode.SecondaryOperationCode2
						,OriginalEpisode.SecondaryOperationCode3
						,OriginalEpisode.SecondaryOperationCode4
						,OriginalEpisode.SecondaryOperationCode5
						,OriginalEpisode.SecondaryOperationCode6
						,OriginalEpisode.SecondaryOperationCode7
						,OriginalEpisode.SecondaryOperationCode8
						,OriginalEpisode.SecondaryOperationCode9
						,OriginalEpisode.SecondaryOperationCode10
						,OriginalEpisode.SecondaryOperationCode11
						,OriginalEpisode.SecondaryOperationCode12
						,OriginalEpisode.SecondaryOperationCode13
						,OriginalEpisode.SecondaryOperationCode14
						,OriginalEpisode.SecondaryOperationCode15
						,OriginalEpisode.SecondaryOperationCode16
					)
					=
					CHECKSUM(
						 FCEConfiguration.PrimaryOperationCode
						,FCEConfiguration.SecondaryOperationCode1
						,FCEConfiguration.SecondaryOperationCode2
						,FCEConfiguration.SecondaryOperationCode3
						,FCEConfiguration.SecondaryOperationCode4
						,FCEConfiguration.SecondaryOperationCode5
						,FCEConfiguration.SecondaryOperationCode6
						,FCEConfiguration.SecondaryOperationCode7
						,FCEConfiguration.SecondaryOperationCode8
						,FCEConfiguration.SecondaryOperationCode9
						,FCEConfiguration.SecondaryOperationCode10
						,FCEConfiguration.SecondaryOperationCode11
						,FCEConfiguration.SecondaryOperationCode12
						,FCEConfiguration.SecondaryOperationCode13
						,FCEConfiguration.SecondaryOperationCode14
						,FCEConfiguration.SecondaryOperationCode15
						,FCEConfiguration.SecondaryOperationCode16
					)

				)

			)
			,(
			select top 1
				OriginalEpisode.FCEConfigurationRecno
			from
				ETL.HRG4APCEncounter

			inner join Recoding.FCEConfiguration OriginalEpisode
			on	OriginalEpisode.FCEConfigurationRecno = HRG4APCEncounter.MergeEncounterRecno
			
			where
				OriginalEpisode.ProviderSpellNo = Encounter.ProviderSpellNo
			and	OriginalEpisode.PseudoProviderSpellNo = Encounter.PseudoProviderSpellNo
			and	OriginalEpisode.PrimaryOperationCode is not null
			)
		)

	,case when Spell.HRGCode = '' then null else Spell.HRGCode end
	,case when Spell.DominantOperationCode = '' then null else Spell.DominantOperationCode end
	,case when Spell.PrimaryDiagnosisCode = '' then null else Spell.PrimaryDiagnosisCode end
	,case when Spell.SecondaryDiagnosisCode = '' then null else Spell.SecondaryDiagnosisCode end
	,Spell.EpisodeCount
	,Spell.LOS
	,ReportingSpellLOS = null
	,Trimpoint = null
	,ExcessBeddays = null
	,CCDays = null
	,ProgramBudgetCode = null
from
	ETL.HRG4APCSpell Spell

inner join (
	select
		 HRG4APCEncounter.RowNo
		,OriginalEpisode.ProviderSpellNo
		,OriginalEpisode.PseudoProviderSpellNo
		,HRG4APCEncounter.MergeEncounterRecno
	from
		ETL.HRG4APCEncounter

	inner join Recoding.FCEConfiguration OriginalEpisode
	on	OriginalEpisode.FCEConfigurationRecno = HRG4APCEncounter.MergeEncounterRecno

	) Encounter
on	Spell.RowNo = Encounter.RowNo

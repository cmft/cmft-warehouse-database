﻿create view Casenote.[Location] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceLocationID = SourceValueID
	,SourceLocationCode = SourceValueCode
	,SourceLocation = SourceValue
	,LocalLocationID = LocalValueID
	,LocalLocationCode = LocalValueCode
	,LocalLocation = LocalValue
	,NationalLocationID = NationalValueID
	,NationalLocationCode = NationalValueCode
	,NationalLocation = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'CASENOTELOCATION'







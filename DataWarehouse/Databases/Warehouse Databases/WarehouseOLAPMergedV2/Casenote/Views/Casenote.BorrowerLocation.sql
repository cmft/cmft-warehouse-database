﻿create view Casenote.BorrowerLocation as

select
	 BorrowerLocationID = Location.SourceLocationID
	,BorrowerLocation = Location.SourceLocation
	,ServicePointID =
		(
		select
			min(ServicePoint.SourceServicePointID)
		from
			WH.ServicePoint
		where
			ServicePoint.SourceServicePointCode = '-1'
		)
	,RecordTypeID = 4 --At Base
from
	Casenote.Location

union all

select
	 BorrowerID = Borrower.SourceBorrowerID
	,Borrower = Borrower.SourceBorrower
	,ServicePointID = Borrower.SourceServicePointID
	,RecordTypeID = 1 --Loan
from
	Casenote.Borrower

union all

select
	 -1
	,'Outpatient Appointment'
	,ServicePointID =
		(
		select
			min(ServicePoint.SourceServicePointID)
		from
			WH.ServicePoint
		where
			ServicePoint.SourceServicePointCode = '-1'
		)
	,RecordTypeID = 2 --Outpatient

union all

select
	 -2
	,'Admitted Patient Care'
	,ServicePointID =
		(
		select
			min(ServicePoint.SourceServicePointID)
		from
			WH.ServicePoint
		where
			ServicePoint.SourceServicePointCode = '-1'
		)
	,RecordTypeID = 3 --Admitted Patient Care


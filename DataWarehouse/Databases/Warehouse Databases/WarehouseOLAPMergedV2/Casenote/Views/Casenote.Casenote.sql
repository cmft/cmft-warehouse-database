﻿
CREATE view [Casenote].[Casenote] as

select
	 MergeRecno
	,SourcePatientNo
	,CasenoteNumber
	,AllocatedDate
	,CasenoteLocationCode
	,CasenoteLocationDate
	,CasenoteStatus
	,WithdrawnDate
	,ContextCode
from
	Casenote.Base

union all

select
	 MergeRecno = -1
	,SourcePatientNo = 'N/A'
	,CasenoteNumber = 'N/A'
	,AllocatedDate = null
	,CasenoteLocationCode = 'N/A'
	,CasenoteLocationDate = null
	,CasenoteStatus = 'N/A'
	,WithdrawnDate = null
	,ContextCode = 'N/A'


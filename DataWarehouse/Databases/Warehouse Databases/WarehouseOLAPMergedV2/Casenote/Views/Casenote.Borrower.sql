﻿

CREATE view [Casenote].[Borrower] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,Member.SourceContextID
	,SourceBorrowerID = Member.SourceValueID
	,SourceBorrowerCode = Member.SourceValueCode
	,SourceBorrower = Member.SourceValue
	,LocalBorrowerID = Member.LocalValueID
	,LocalBorrowerCode = Member.LocalValueCode
	,LocalBorrower = Member.LocalValue
	,NationalBorrowerID = Member.NationalValueID
	,NationalBorrowerCode = Member.NationalValueCode
	,NationalBorrower = Member.NationalValue

	,SourceServicePointID =
		coalesce(
			 BorrowerServicePointMap.SourceServicePointID
			,ServicePoint.SourceServicePointID
		)
from
	WH.Member

left join Casenote.BorrowerServicePointMap
on	BorrowerServicePointMap.SourceBorrowerID = Member.SourceValueID

inner join WH.ServicePoint
on	ServicePoint.SourceServicePointCode = '-1'
and not exists
	(
	select
		1
	from
		WH.ServicePoint Previous
	where
		Previous.SourceServicePointCode = '-1'
	and	Previous.SourceServicePointID < ServicePoint.SourceServicePointID
	)

where
	Member.AttributeCode = 'BORROWER'







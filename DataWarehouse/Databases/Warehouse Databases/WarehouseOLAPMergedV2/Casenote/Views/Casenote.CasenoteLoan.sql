﻿create view Casenote.CasenoteLoan as

select
	 MergeRecno
--	,SourceUniqueID
	,SourcePatientNo
	,CasenoteNumber
	,BorrowerCode
	,SequenceNo
	,TransactionTime
	,LoanTime
	,ExpectedReturnDate
	,ReturnTime
	,Comment
	,LoanReason
	,UserId
	,ContextCode
from
	Casenote.BaseLoan

union all

select
	 MergeRecno = -1
	,null
	,null
	,null
	,null
	,null
	,null
	,null
	,null
	,null
	,null
	,null

	,null

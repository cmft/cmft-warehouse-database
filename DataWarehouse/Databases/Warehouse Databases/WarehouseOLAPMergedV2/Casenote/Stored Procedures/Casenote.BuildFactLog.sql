﻿CREATE procedure [Casenote].[BuildFactLog] as

--CCB 2016-01-04
--Code is invalid but may be some use

--declare
--	 @StartTime datetime = getdate()
--	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
--	,@Elapsed int
--	,@Stats varchar(max)


--declare 
--	@MissingServicePointID int =
--		(
--		select
--			min(ServicePoint.SourceServicePointID)
--		from
--			WH.ServicePoint
--		where
--			ServicePoint.SourceServicePointCode = '-1'
--		)


--select
--	 BaseLoan.MergeRecno
--	,NextLoanMergeRecno = NextLoan.MergeRecno
--	,NextLoanAfterAllocatedDateMergeRecno = NextLoanAfterAllocatedDate.MergeRecno
--	,LastLoanMergeRecno = LastLoan.MergeRecno
--into
--	#LoanDetail
--from
--	Casenote.BaseLoan

--inner join Casenote.Base
--on	Base.MergeRecno = BaseLoan.MergeRecno

--left join Casenote.BaseLoan NextLoan
--on	NextLoan.SourcePatientNo = BaseLoan.SourcePatientNo
--and	NextLoan.CasenoteNumber = BaseLoan.CasenoteNumber
--and	NextLoan.ContextCode = BaseLoan.ContextCode
--and	NextLoan.LoanTime > BaseLoan.ReturnTime
--and	not exists
--	(
--	select
--		1
--	from
--		Casenote.BaseLoan Previous
--	where
--		Previous.SourcePatientNo = BaseLoan.SourcePatientNo
--	and	Previous.CasenoteNumber = BaseLoan.CasenoteNumber
--	and	Previous.ContextCode = BaseLoan.ContextCode
--	and	Previous.LoanTime > BaseLoan.ReturnTime
--	and	Previous.LoanTime < NextLoan.LoanTime
--	)


--left join Casenote.BaseLoan NextLoanAfterAllocatedDate
--on	NextLoanAfterAllocatedDate.SourcePatientNo = Base.SourcePatientNo
--and	NextLoanAfterAllocatedDate.CasenoteNumber = Base.CasenoteNumber
--and	NextLoanAfterAllocatedDate.ContextCode = Base.ContextCode
--and	NextLoanAfterAllocatedDate.LoanTime > Base.AllocatedDate
--and	not exists
--	(
--	select
--		1
--	from
--		Casenote.BaseLoan Previous
--	where
--		Previous.SourcePatientNo = Base.SourcePatientNo
--	and	Previous.CasenoteNumber = Base.CasenoteNumber
--	and	Previous.ContextCode = Base.ContextCode
--	and	Previous.LoanTime > Base.AllocatedDate
--	and	Previous.LoanTime < NextLoanAfterAllocatedDate.LoanTime
--	)


--left join Casenote.BaseLoan LastLoan
--on	LastLoan.SourcePatientNo = Base.SourcePatientNo
--and	LastLoan.CasenoteNumber = Base.CasenoteNumber
--and	LastLoan.ContextCode = Base.ContextCode
--and	not exists
--	(
--	select
--		1
--	from
--		Casenote.BaseLoan Previous
--	where
--		Previous.SourcePatientNo = Base.SourcePatientNo
--	and	Previous.CasenoteNumber = Base.CasenoteNumber
--	and	Previous.ContextCode = Base.ContextCode
--	and	Previous.LoanTime > LastLoan.LoanTime
--	)
--and	LastLoan.ReturnTime is not null


--truncate table Casenote.FactLog

--IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Casenote].[FactLog]') AND name = N'IX_FactLog')
--DROP INDEX [IX_FactLog] ON [Casenote].[FactLog] WITH ( ONLINE = OFF )



--insert
--into
--	Casenote.FactLog
--(
--	 MergeRecno
--	,ContextID
--	,CasenoteMergeRecno
--	,BorrowerLocationID
--	,SystemUserID
--	,TransactionDateID
--	,LoanDateID
--	,ExpectedReturnDateID
--	,ReturnDateID
--	,TransactionTimeBand
--	,LoanTimeBand
--	,ReturnTimeBand
--	,ReturnStatusID
--	,LoanDays
--	,ServicePointID
--	,AtAppropriateServicePoint
--	,LoanToEncounterMinutes
--	,EncounterToReturnMinutes
--	,RecordTypeID
--)
--select
--	 MergeRecno
--	,ContextID
--	,CasenoteMergeRecno
--	,BorrowerID
--	,SystemUserID
--	,TransactionDateID
--	,LoanDateID
--	,ExpectedReturnDateID
--	,ReturnDateID
--	,TransactionTimeBand
--	,LoanTimeBand
--	,ReturnTimeBand
--	,ReturnStatusID
--	,LoanDays
--	,ServicePointID = @MissingServicePointID
--	,AtAppropriateServicePoint = 0
--	,LoanToEncounterMinutes = null
--	,EncounterToReturnMinutes = null
--	,RecordTypeID = 1 --Loan

--from
--	Casenote.FactLoan


----gaps between loans (assume back at base)
--insert
--into
--	Casenote.FactLog
--(
--	 MergeRecno
--	,ContextID
--	,CasenoteMergeRecno
--	,BorrowerLocationID
--	,SystemUserID
--	,TransactionDateID
--	,LoanDateID
--	,ExpectedReturnDateID
--	,ReturnDateID
--	,TransactionTimeBand
--	,LoanTimeBand
--	,ReturnTimeBand
--	,ReturnStatusID
--	,LoanDays
--	,ServicePointID
--	,AtAppropriateServicePoint
--	,LoanToEncounterMinutes
--	,EncounterToReturnMinutes
--	,RecordTypeID
--)
--select
--	 BaseLoan.MergeRecno
--	,FactLoan.ContextID
--	,CasenoteMergeRecno = FactLoan.CasenoteMergeRecno
--	,BorrowerLocationID = Casenote.CasenoteLocationID
--	,SystemUserID = SystemUser.SystemUserID
--	,TransactionDateID = FactLoan.ReturnDateID
--	,LoanDateID = FactLoan.ReturnDateID
--	,ExpectedReturnDateID = NextFactLoan.LoanTimeBand
--	,ReturnDateID = NextFactLoan.LoanDateID
--	,TransactionTimeBand = FactLoan.ReturnTimeBand
--	,LoanTimeBand = FactLoan.ReturnTimeBand
--	,ReturnTimeBand = NextFactLoan.LoanTimeBand
--	,ReturnStatusID = 5	--On-time

--	,LoanDays =
--		DATEDIFF(
--			day
--			,BaseLoan.ReturnTime
--			,NextLoan.LoanTime
--		)

--	,ServicePointID = @MissingServicePointID
--	,AtAppropriateServicePoint = 0
--	,LoanToEncounterMinutes = null
--	,EncounterToReturnMinutes = null
--	,RecordTypeID = 4 --At Base

--from
--	Casenote.BaseLoan

--inner join #LoanDetail NextLoanMap
--on	NextLoanMap.MergeRecno = BaseLoan.MergeRecno

--inner join Casenote.BaseLoan NextLoan
--on	NextLoan.MergeRecno = NextLoanMap.NextLoanMergeRecno

--inner join Casenote.FactLoan
--on	FactLoan.MergeRecno = BaseLoan.MergeRecno

--inner join Casenote.FactLoan NextFactLoan
--on	NextFactLoan.MergeRecno = NextLoan.MergeRecno

--inner join Casenote.Fact Casenote
--on	Casenote.MergeRecno = FactLoan.CasenoteMergeRecno

--inner join WH.SystemUser
--on	SystemUser.ContextID = FactLoan.ContextID
--and	SystemUser.UserID = '-1'

--where
--	BaseLoan.ReturnTime is not null
--and	not exists
--	(
--	select
--		1
--	from
--		Casenote.BaseLoan NextLoan
--	where
--		NextLoan.CasenoteNumber = BaseLoan.CasenoteNumber
--	and	NextLoan.LoanTime = BaseLoan.ReturnTime
--	)


----Allocated date to 1st loan or withdrawal
--insert
--into
--	Casenote.FactLog
--(
--	 MergeRecno
--	,ContextID
--	,CasenoteMergeRecno
--	,BorrowerLocationID
--	,SystemUserID
--	,TransactionDateID
--	,LoanDateID
--	,ExpectedReturnDateID
--	,ReturnDateID
--	,TransactionTimeBand
--	,LoanTimeBand
--	,ReturnTimeBand
--	,ReturnStatusID
--	,LoanDays
--	,ServicePointID
--	,AtAppropriateServicePoint
--	,LoanToEncounterMinutes
--	,EncounterToReturnMinutes
--	,RecordTypeID
--)
--select
--	 Base.MergeRecno
--	,Fact.ContextID
--	,CasenoteMergeRecno = Fact.MergeRecno
--	,BorrowerID = Fact.CasenoteLocationID
--	,SystemUserID = SystemUser.SystemUserID
--	,TransactionDateID = Fact.AllocatedDateID
--	,LoanDateID = Fact.AllocatedDateID

--	,ExpectedReturnDateID = 
--		coalesce(
--			 NextFactLoan.LoanDateID
--			,Fact.WithdrawnDateID
--			,Fact.AllocatedDateID
--		)

--	,ReturnDateID =
--		coalesce(
--			 NextFactLoan.LoanDateID
--			,Fact.WithdrawnDateID
--			,Fact.AllocatedDateID
--		)

--	,TransactionTimeBand = -1
--	,LoanTimeBand = 0

--	,ReturnTimeBand =
--		coalesce(
--			 NextFactLoan.LoanTimeBand
--			,-1
--		)

--	,ReturnStatusID = 5	--On-time

--	,LoanDays =
--		DATEDIFF(
--			day
--			,Base.AllocatedDate
--			,NextLoan.LoanTime
--		)

--	,ServicePointID = @MissingServicePointID
--	,AtAppropriateServicePoint = 0
--	,LoanToEncounterMinutes = null
--	,EncounterToReturnMinutes = null
--	,RecordTypeID = 4 --At Base

--from
--	Casenote.Base

--inner join Casenote.Fact
--on	Fact.MergeRecno = Base.MergeRecno

--inner join #LoanDetail NextLoanMap
--on	NextLoanMap.MergeRecno = Base.MergeRecno

--inner join Casenote.BaseLoan NextLoan
--on	NextLoan.MergeRecno = NextLoanMap.NextLoanAfterAllocatedDateMergeRecno

--left join Casenote.FactLoan NextFactLoan
--on	NextFactLoan.MergeRecno = NextLoan.MergeRecno

--inner join WH.SystemUser
--on	SystemUser.ContextID = Fact.ContextID
--and	SystemUser.UserID = '-1'


----last loan assume returned to base
--insert
--into
--	Casenote.FactLog
--(
--	 MergeRecno
--	,ContextID
--	,CasenoteMergeRecno
--	,BorrowerLocationID
--	,SystemUserID
--	,TransactionDateID
--	,LoanDateID
--	,ExpectedReturnDateID
--	,ReturnDateID
--	,TransactionTimeBand
--	,LoanTimeBand
--	,ReturnTimeBand
--	,ReturnStatusID
--	,LoanDays
--	,ServicePointID
--	,AtAppropriateServicePoint
--	,LoanToEncounterMinutes
--	,EncounterToReturnMinutes
--	,RecordTypeID
--)
--select
--	 Base.MergeRecno
--	,Fact.ContextID
--	,CasenoteMergeRecno = Fact.MergeRecno
--	,BorrowerLocationID = Fact.CasenoteLocationID
--	,SystemUserID = SystemUser.SystemUserID
--	,TransactionDateID = LastFactLoan.ReturnDateID
--	,LoanDateID = LastFactLoan.ReturnDateID

--	,ExpectedReturnDateID = 
--		case
--		when Fact.Withdrawn = 1
--		then Fact.WithdrawnDateID
--		else Today.DateID
--		end

--	,ReturnDateID =
--		case
--		when Fact.Withdrawn = 1
--		then Fact.WithdrawnDateID
--		else Today.DateID
--		end

--	,TransactionTimeBand = -1
--	,LoanTimeBand = LastFactLoan.ReturnTimeBand

--	,ReturnTimeBand =
--		case
--		when Fact.Withdrawn = 1
--		then 0
--		else -1
--		end

--	,ReturnStatusID = 5	--On-time

--	,LoanDays =
--		DATEDIFF(
--			day
--			,LastLoan.ReturnTime
--			,coalesce(
--				 Base.WithdrawnDate
--				,Today.TheDate
--			)
--		)

--	,ServicePointID = @MissingServicePointID
--	,AtAppropriateServicePoint = 0
--	,LoanToEncounterMinutes = null
--	,EncounterToReturnMinutes = null
--	,RecordTypeID = 4 --At Base

--from
--	Casenote.Base

--inner join Casenote.Fact
--on	Fact.MergeRecno = Base.MergeRecno

--inner join #LoanDetail NextLoanMap
--on	NextLoanMap.MergeRecno = Base.MergeRecno

--inner join Casenote.BaseLoan LastLoan
--on	LastLoan.MergeRecno = NextLoanMap.LastLoanMergeRecno

--inner join Casenote.FactLoan LastFactLoan
--on	LastFactLoan.MergeRecno = LastLoan.MergeRecno

--inner join WH.Calendar Today
--on	Today.TheDate = CAST(getdate() as date)

--inner join WH.SystemUser
--on	SystemUser.ContextID = Fact.ContextID
--and	SystemUser.UserID = '-1'



----Outpatient Appointments
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#FactEncounter'))
--drop table #FactEncounter

--select
--	 FactEncounter.MergeEncounterRecno
--	,FactEncounter.ContextID
--	,FactEncounter.CasenoteMergeRecno
--	,FactEncounter.EncounterDateID
--	,ServicePointID = FactEncounter.ClinicID
--	,LengthOfEncounter = 0

--	,EncounterTime = BaseEncounter.AppointmentTime
--	,EncounterEndTime = BaseEncounter.AppointmentTime
--into
--	#FactEncounter
--from
--	OP.FactEncounter

--inner join OP.BaseEncounter
--on	BaseEncounter.MergeEncounterRecno = FactEncounter.MergeEncounterRecno

--inner join OP.Metric
--on	Metric.MetricID = FactEncounter.MetricID
--and	Metric.MetricCode = 'CHKAPP'

--inner join Casenote.Fact
--on	Fact.MergeRecno = FactEncounter.CasenoteMergeRecno

--where
--	FactEncounter.CasenoteMergeRecno <> -1


--insert
--into
--	Casenote.FactLog
--(
--	 MergeRecno
--	,ContextID
--	,CasenoteMergeRecno
--	,BorrowerLocationID
--	,SystemUserID
--	,TransactionDateID
--	,LoanDateID
--	,ExpectedReturnDateID
--	,ReturnDateID
--	,TransactionTimeBand
--	,LoanTimeBand
--	,ReturnTimeBand
--	,ReturnStatusID
--	,LoanDays
--	,ServicePointID
--	,AtAppropriateServicePoint
--	,LoanToEncounterMinutes
--	,EncounterToReturnMinutes
--	,RecordTypeID
--)
--select
--	 Encounter.MergeEncounterRecno
--	,Encounter.ContextID
--	,Encounter.CasenoteMergeRecno

--	,BorrowerLocationID =
--		coalesce(
--			CurrentLoan.BorrowerID
--			,
--			(
--			select
--				Borrower.SourceBorrowerID
--			from
--				Casenote.Borrower
--			where
--				Borrower.SourceContextID = Encounter.ContextID
--			and	Borrower.SourceBorrowerCode = '-1'
--			)
--		)

--	,SystemUserID = SystemUser.SystemUserID
--	,TransactionDateID = Encounter.EncounterDateID
--	,LoanDateID = Encounter.EncounterDateID
--	,ExpectedReturnDateID = Encounter.EncounterDateID
--	,ReturnDateID = Encounter.EncounterDateID
--	,TransactionTimeBand = -1
--	,LoanTimeBand = -1
--	,ReturnTimeBand = -1
--	,ReturnStatusID = 3	--No expected return date
--	,LoanDays = null
--	,Encounter.ServicePointID

--	,AtAppropriateServicePoint =
--		case
--		when BorrowerLocation.ServicePointID = Encounter.ServicePointID then 1
--		else 0
--		end

--	,LoanToEncounterMinutes =
--		DATEDIFF(
--			 minute
--			,CurrentLoan.LoanTime
--			,Encounter.EncounterTime
--		)

--	,EncounterToReturnMinutes =
--		DATEDIFF(
--			 minute
--			,Encounter.EncounterTime
--			,CurrentLoan.ReturnTime
--		)

--	,RecordTypeID = 2 --Outpatient
--from
--	#FactEncounter Encounter

--left join 
--	(
--	select
--		 FactLoan.CasenoteMergeRecno
--		,BaseLoan.LoanTime
--		,ReturnTime = coalesce(BaseLoan.ReturnTime, getdate())
--		,FactLoan.BorrowerID
--	from
--		Casenote.BaseLoan

--	inner join Casenote.FactLoan
--	on	FactLoan.MergeRecno = BaseLoan.MergeRecno

--	) CurrentLoan
--on	CurrentLoan.CasenoteMergeRecno = Encounter.CasenoteMergeRecno
--and	CurrentLoan.LoanTime <= Encounter.EncounterTime
--and	CurrentLoan.ReturnTime > Encounter.EncounterTime
--and	not exists
--	(
--	select
--		1
--	from
--		(
--		select
--			 FactLoan.CasenoteMergeRecno
--			,BaseLoan.LoanTime
--			,ReturnTime = coalesce(BaseLoan.ReturnTime, getdate())
--			,FactLoan.BorrowerID
--		from
--			Casenote.BaseLoan

--		inner join Casenote.FactLoan
--		on	FactLoan.MergeRecno = BaseLoan.MergeRecno

--		) Previous
--	where
--		Previous.CasenoteMergeRecno = Encounter.CasenoteMergeRecno
--	and	Previous.LoanTime <= Encounter.EncounterTime
--	and	Previous.ReturnTime > Encounter.EncounterTime
--	and	Previous.LoanTime > CurrentLoan.LoanTime
--	)

--left join Casenote.BorrowerLocation
--on	BorrowerLocation.BorrowerLocationID =
--		coalesce(
--			CurrentLoan.BorrowerID
--			,
--			(
--			select
--				Borrower.SourceBorrowerID
--			from
--				Casenote.Borrower
--			where
--				Borrower.SourceContextID = Encounter.ContextID
--			and	Borrower.SourceBorrowerCode = '-1'
--			)
--		)

--inner join WH.SystemUser
--on	SystemUser.ContextID = Encounter.ContextID
--and	SystemUser.UserID = '-1'


----Admissions
--truncate table #FactEncounter

--insert
--into
--	#FactEncounter
--select
--	 FactEncounter.MergeEncounterRecno
--	,FactEncounter.ContextID
--	,FactEncounter.CasenoteMergeRecno
--	,FactEncounter.EncounterDateID
--	,ServicePointID = FactEncounter.WardID
--	,FactEncounter.LengthOfEncounter

--	,BaseEncounter.AdmissionTime
--	,BaseEncounter.DischargeTime
--from
--	APC.FactEncounter

--inner join APC.BaseEncounter
--on	BaseEncounter.MergeEncounterRecno = FactEncounter.MergeEncounterRecno

--inner join APC.Metric
--on	Metric.MetricID = FactEncounter.MetricID
--and	Metric.MetricCode = 'CHKADM'

--inner join Casenote.Fact
--on	Fact.MergeRecno = FactEncounter.CasenoteMergeRecno

--where
--	FactEncounter.CasenoteMergeRecno <> -1


--insert
--into
--	Casenote.FactLog
--(
--	 MergeRecno
--	,ContextID
--	,CasenoteMergeRecno
--	,BorrowerLocationID
--	,SystemUserID
--	,TransactionDateID
--	,LoanDateID
--	,ExpectedReturnDateID
--	,ReturnDateID
--	,TransactionTimeBand
--	,LoanTimeBand
--	,ReturnTimeBand
--	,ReturnStatusID
--	,LoanDays
--	,ServicePointID
--	,AtAppropriateServicePoint
--	,LoanToEncounterMinutes
--	,EncounterToReturnMinutes
--	,RecordTypeID
--)
--select
--	 Encounter.MergeEncounterRecno
--	,Encounter.ContextID
--	,Encounter.CasenoteMergeRecno

--	,BorrowerLocationID =
--		coalesce(
--			CurrentLoan.BorrowerID
--			,
--			(
--			select
--				Borrower.SourceBorrowerID
--			from
--				Casenote.Borrower
--			where
--				Borrower.SourceContextID = Encounter.ContextID
--			and	Borrower.SourceBorrowerCode = '-1'
--			)
--		)

--	,SystemUserID = SystemUser.SystemUserID
--	,TransactionDateID = Encounter.EncounterDateID
--	,LoanDateID = Encounter.EncounterDateID
--	,ExpectedReturnDateID = Encounter.EncounterDateID
--	,ReturnDateID = Encounter.EncounterDateID
--	,TransactionTimeBand = -1
--	,LoanTimeBand = -1
--	,ReturnTimeBand = -1
--	,ReturnStatusID = 3	--No expected return date
--	,LoanDays = Encounter.LengthOfEncounter
--	,Encounter.ServicePointID

--	,AtAppropriateServicePoint =
--		case
--		when BorrowerLocation.ServicePointID = Encounter.ServicePointID then 1
--		else 0
--		end

--	,LoanToEncounterMinutes =
--		DATEDIFF(
--			 minute
--			,CurrentLoan.LoanTime
--			,Encounter.EncounterTime
--		)

--	,EncounterToReturnMinutes =
--		DATEDIFF(
--			 minute
--			,Encounter.EncounterEndTime
--			,CurrentLoan.ReturnTime
--		)

--	,RecordTypeID = 3 -- APC
--from
--	#FactEncounter Encounter

--left join 
--	(
--	select
--		 FactLoan.CasenoteMergeRecno
--		,BaseLoan.LoanTime
--		,ReturnTime = coalesce(BaseLoan.ReturnTime, getdate())
--		,FactLoan.BorrowerID
--	from
--		Casenote.BaseLoan

--	inner join Casenote.FactLoan
--	on	FactLoan.MergeRecno = BaseLoan.MergeRecno

--	) CurrentLoan
--on	CurrentLoan.CasenoteMergeRecno = Encounter.CasenoteMergeRecno
--and	CurrentLoan.LoanTime <= Encounter.EncounterTime
--and	CurrentLoan.ReturnTime > Encounter.EncounterTime
--and	not exists
--	(
--	select
--		1
--	from
--		(
--		select
--			 FactLoan.CasenoteMergeRecno
--			,BaseLoan.LoanTime
--			,ReturnTime = coalesce(BaseLoan.ReturnTime, getdate())
--			,FactLoan.BorrowerID
--		from
--			Casenote.BaseLoan

--		inner join Casenote.FactLoan
--		on	FactLoan.MergeRecno = BaseLoan.MergeRecno

--		) Previous
--	where
--		Previous.CasenoteMergeRecno = Encounter.CasenoteMergeRecno
--	and	Previous.LoanTime <= Encounter.EncounterTime
--	and	Previous.ReturnTime > Encounter.EncounterTime
--	and	Previous.LoanTime > CurrentLoan.LoanTime
--	)

--left join Casenote.BorrowerLocation
--on	BorrowerLocation.BorrowerLocationID =
--		coalesce(
--			CurrentLoan.BorrowerID
--			,
--			(
--			select
--				Borrower.SourceBorrowerID
--			from
--				Casenote.Borrower
--			where
--				Borrower.SourceContextID = Encounter.ContextID
--			and	Borrower.SourceBorrowerCode = '-1'
--			)
--		)

--inner join WH.SystemUser
--on	SystemUser.ContextID = Encounter.ContextID
--and	SystemUser.UserID = '-1'


--CREATE NONCLUSTERED INDEX [IX_FactLog] ON [Casenote].[FactLog] 
--(
--	[CasenoteMergeRecno] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

--select
--	@Stats =
--		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


--print @Stats

--exec [$(Warehouse)].dbo.WriteAuditLogEvent
--	 @ProcedureName
--	,@Stats


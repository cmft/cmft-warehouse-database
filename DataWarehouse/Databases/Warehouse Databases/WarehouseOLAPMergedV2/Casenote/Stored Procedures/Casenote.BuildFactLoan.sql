﻿CREATE procedure Casenote.BuildFactLoan as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

truncate table Casenote.FactLoan

insert
into
	Casenote.FactLoan
(
	 MergeRecno
	,ContextID
	,CasenoteMergeRecno
	,BorrowerID
	,SystemUserID
	,TransactionDateID
	,LoanDateID
	,ExpectedReturnDateID
	,ReturnDateID
	,TransactionTimeBand
	,LoanTimeBand
	,ReturnTimeBand
	,ReturnStatusID
	,LoanDays
)
select
	 BaseLoan.MergeRecno
	,BaseLoanReference.ContextID
	,CasenoteMergeRecno = Base.MergeRecno
	,BaseLoanReference.BorrowerID
	,BaseLoanReference.SystemUserID

	,TransactionDateID =
		coalesce(
			 TransactionDate.DateID

			,case
			when BaseLoan.TransactionTime is null
			then NullDate.DateID

			when cast(BaseLoan.TransactionTime as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,LoanDateID =
		coalesce(
			 LoanDate.DateID

			,case
			when BaseLoan.LoanTime is null
			then NullDate.DateID

			when cast(BaseLoan.LoanTime as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,ExpectedReturnDateID =
		coalesce(
			 ExpectedReturnDate.DateID

			,case
			when BaseLoan.ExpectedReturnDate is null
			then NullDate.DateID

			when BaseLoan.ExpectedReturnDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,ReturnDateID =
		coalesce(
			 ReturnDate.DateID

			,case
			when BaseLoan.ReturnTime is null
			then NullDate.DateID

			when cast(BaseLoan.ReturnTime as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,TransactionTimeBand = coalesce(datediff(minute, cast(BaseLoan.TransactionTime as date), BaseLoan.TransactionTime), -1)
	,LoanTimeBand = coalesce(datediff(minute, cast(BaseLoan.LoanTime as date), BaseLoan.LoanTime), -1)
	,ReturnTimeBand = coalesce(datediff(minute, cast(BaseLoan.ReturnTime as date), BaseLoan.ReturnTime), -1)

	,ReturnStatusID =
		case
		when BaseLoan.ReturnTime is null then 4 --not returned
		when BaseLoan.ExpectedReturnDate is null then 3 --no expected return date
		when cast(BaseLoan.ReturnTime as date) < BaseLoan.ExpectedReturnDate then 1 --early
		when cast(BaseLoan.ReturnTime as date) > BaseLoan.ExpectedReturnDate then 2 --late
		else 5 --on-time
		end

	,LoanDays =
		DATEDIFF(
			 day
			,BaseLoan.LoanTime
			,coalesce(
				 BaseLoan.ReturnTime
				,getdate()
			)
		)

from
	Casenote.BaseLoan

inner join Casenote.BaseLoanReference
on	BaseLoanReference.MergeRecno = BaseLoan.MergeRecno

inner join Casenote.Base
on	Base.SourcePatientNo = BaseLoan.SourcePatientNo
and	Base.CasenoteNumber = BaseLoan.CasenoteNumber

left join WH.Calendar TransactionDate
on	TransactionDate.TheDate = cast(BaseLoan.TransactionTime as date)

left join WH.Calendar LoanDate
on	LoanDate.TheDate = cast(BaseLoan.LoanTime as date)

left join WH.Calendar ExpectedReturnDate
on	ExpectedReturnDate.TheDate = BaseLoan.ExpectedReturnDate

left join WH.Calendar ReturnDate
on	ReturnDate.TheDate = cast(BaseLoan.ReturnTime as date)

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'


union all

select top 1
	 MergeRecno = -1
	,BaseLoanReference.ContextID
	,CasenoteMergeRecno = Base.MergeRecno
	,BaseLoanReference.BorrowerID
	,BaseLoanReference.SystemUserID

	,TransactionDateID =
		coalesce(
			 TransactionDate.DateID

			,case
			when BaseLoan.TransactionTime is null
			then NullDate.DateID

			when cast(BaseLoan.TransactionTime as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,LoanDateID =
		coalesce(
			 LoanDate.DateID

			,case
			when BaseLoan.LoanTime is null
			then NullDate.DateID

			when cast(BaseLoan.LoanTime as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,ExpectedReturnDateID =
		coalesce(
			 ExpectedReturnDate.DateID

			,case
			when BaseLoan.ExpectedReturnDate is null
			then NullDate.DateID

			when BaseLoan.ExpectedReturnDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,ReturnDateID =
		coalesce(
			 ReturnDate.DateID

			,case
			when BaseLoan.ReturnTime is null
			then NullDate.DateID

			when cast(BaseLoan.ReturnTime as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,TransactionTimeBand = -1
	,LoanTimeBand = -1
	,ReturnTimeBand = -1

	,ReturnStatusID = 3

	,LoanDays = null
from
	Casenote.BaseLoan

inner join Casenote.BaseLoanReference
on	BaseLoanReference.MergeRecno = BaseLoan.MergeRecno

inner join Casenote.Base
on	Base.SourcePatientNo = BaseLoan.SourcePatientNo
and	Base.CasenoteNumber = BaseLoan.CasenoteNumber

left join WH.Calendar TransactionDate
on	TransactionDate.TheDate = cast(BaseLoan.TransactionTime as date)

left join WH.Calendar LoanDate
on	LoanDate.TheDate = cast(BaseLoan.LoanTime as date)

left join WH.Calendar ExpectedReturnDate
on	ExpectedReturnDate.TheDate = BaseLoan.ExpectedReturnDate

left join WH.Calendar ReturnDate
on	ReturnDate.TheDate = cast(BaseLoan.ReturnTime as date)

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



﻿CREATE procedure [Casenote].[BuildFact] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

truncate table Casenote.Fact

insert
into
	Casenote.Fact
(

	 MergeRecno
	,ContextID
	,CasenoteLocationID
	,AllocatedDateID
	,CasenoteLocationDateID
	,WithdrawnDateID
	,Withdrawn
)
select
	 Base.MergeRecno
	,BaseReference.ContextID
	,BaseReference.CasenoteLocationID

	,AllocatedDateID =
		coalesce(
			 AllocatedDate.DateID

			,case
			when Base.AllocatedDate is null
			then NullDate.DateID

			when Base.AllocatedDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,CasenoteLocationDateID =
		coalesce(
			 CasenoteLocationDate.DateID

			,case
			when Base.CasenoteLocationDate is null
			then NullDate.DateID

			when Base.CasenoteLocationDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,WithdrawnDateID =
		coalesce(
			 WithdrawnDate.DateID

			,case
			when Base.WithdrawnDate is null
			then NullDate.DateID

			when Base.WithdrawnDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,Withdrawn =
		CAST(
			case
			when Base.CasenoteStatus = 'WITHDRAWN'
			then 1
			else 0
			end
			as bit
		)
from
	Casenote.Base

inner join Casenote.BaseReference
on	BaseReference.MergeRecno = Base.MergeRecno

left join WH.Calendar AllocatedDate
on	AllocatedDate.TheDate = Base.AllocatedDate

left join WH.Calendar CasenoteLocationDate
on	CasenoteLocationDate.TheDate = Base.CasenoteLocationDate

left join WH.Calendar WithdrawnDate
on	WithdrawnDate.TheDate = Base.WithdrawnDate

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'


union all

select top 1
	 MergeRecno = -1
	,BaseReference.ContextID
	,BaseReference.CasenoteLocationID

	,AllocatedDateID =
		coalesce(
			 AllocatedDate.DateID

			,case
			when Base.AllocatedDate is null
			then NullDate.DateID

			when Base.AllocatedDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,CasenoteLocationDateID =
		coalesce(
			 CasenoteLocationDate.DateID

			,case
			when Base.CasenoteLocationDate is null
			then NullDate.DateID

			when Base.CasenoteLocationDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,WithdrawnDateID =
		coalesce(
			 WithdrawnDate.DateID

			,case
			when Base.WithdrawnDate is null
			then NullDate.DateID

			when Base.WithdrawnDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,Withdrawn =
		CAST(
			case
			when Base.CasenoteStatus = 'WITHDRAWN'
			then 1
			else 0
			end
			as bit
		)
from
	Casenote.Base

inner join Casenote.BaseReference
on	BaseReference.MergeRecno = Base.MergeRecno

left join WH.Calendar AllocatedDate
on	AllocatedDate.TheDate = Base.AllocatedDate

left join WH.Calendar CasenoteLocationDate
on	CasenoteLocationDate.TheDate = Base.CasenoteLocationDate

left join WH.Calendar WithdrawnDate
on	WithdrawnDate.TheDate = Base.WithdrawnDate

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



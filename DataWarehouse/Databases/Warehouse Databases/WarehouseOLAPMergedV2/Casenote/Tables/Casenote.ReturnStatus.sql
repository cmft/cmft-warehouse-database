﻿CREATE TABLE [Casenote].[ReturnStatus] (
    [ReturnStatusID] INT           NOT NULL,
    [ReturnStatus]   VARCHAR (255) NULL,
    CONSTRAINT [PK_ReturnStatus] PRIMARY KEY CLUSTERED ([ReturnStatusID] ASC)
);


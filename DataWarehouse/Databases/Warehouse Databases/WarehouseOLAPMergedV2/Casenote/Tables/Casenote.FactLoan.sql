﻿CREATE TABLE [Casenote].[FactLoan] (
    [MergeRecno]           INT NOT NULL,
    [ContextID]            INT NOT NULL,
    [CasenoteMergeRecno]   INT NOT NULL,
    [BorrowerID]           INT NOT NULL,
    [SystemUserID]         INT NULL,
    [TransactionDateID]    INT NULL,
    [LoanDateID]           INT NULL,
    [ExpectedReturnDateID] INT NULL,
    [ReturnDateID]         INT NULL,
    [TransactionTimeBand]  INT NULL,
    [LoanTimeBand]         INT NULL,
    [ReturnTimeBand]       INT NULL,
    [ReturnStatusID]       INT NULL,
    [LoanDays]             INT NULL,
    CONSTRAINT [PK_FactLoan] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_FactLoan]
    ON [Casenote].[FactLoan]([CasenoteMergeRecno] ASC);


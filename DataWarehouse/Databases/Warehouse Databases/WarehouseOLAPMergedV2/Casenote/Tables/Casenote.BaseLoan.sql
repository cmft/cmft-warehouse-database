﻿CREATE TABLE [Casenote].[BaseLoan] (
    [MergeRecno]         INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]     VARCHAR (254) NOT NULL,
    [SourcePatientNo]    VARCHAR (20)  NOT NULL,
    [CasenoteNumber]     VARCHAR (20)  NOT NULL,
    [BorrowerCode]       VARCHAR (6)   NULL,
    [SequenceNo]         INT           NOT NULL,
    [TransactionTime]    SMALLDATETIME NULL,
    [LoanTime]           SMALLDATETIME NOT NULL,
    [ExpectedReturnDate] DATE          NULL,
    [ReturnTime]         SMALLDATETIME NULL,
    [Comment]            VARCHAR (30)  NULL,
    [LoanReason]         VARCHAR (50)  NULL,
    [UserId]             VARCHAR (3)   NULL,
    [ContextCode]        VARCHAR (8)   NOT NULL,
    CONSTRAINT [PK_BaseLoan] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [CasenoteNumber] ASC, [SequenceNo] ASC, [LoanTime] ASC, [ContextCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BaseLoan]
    ON [Casenote].[BaseLoan]([MergeRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Casenote_BaseLoan_02]
    ON [Casenote].[BaseLoan]([CasenoteNumber] ASC, [LoanTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Casenote_BaseLoan_01]
    ON [Casenote].[BaseLoan]([ReturnTime] ASC)
    INCLUDE([CasenoteNumber], [MergeRecno]);


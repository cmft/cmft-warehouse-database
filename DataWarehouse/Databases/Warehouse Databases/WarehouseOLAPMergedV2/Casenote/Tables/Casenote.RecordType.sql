﻿CREATE TABLE [Casenote].[RecordType] (
    [RecordTypeID]     INT           NOT NULL,
    [RecordType]       VARCHAR (255) NOT NULL,
    [BackgroundColour] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_RecordType] PRIMARY KEY CLUSTERED ([RecordTypeID] ASC)
);


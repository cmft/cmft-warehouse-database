﻿CREATE TABLE [Casenote].[FactLog] (
    [MergeRecno]                INT NOT NULL,
    [ContextID]                 INT NOT NULL,
    [CasenoteMergeRecno]        INT NOT NULL,
    [BorrowerLocationID]        INT NOT NULL,
    [SystemUserID]              INT NOT NULL,
    [TransactionDateID]         INT NOT NULL,
    [LoanDateID]                INT NOT NULL,
    [ExpectedReturnDateID]      INT NOT NULL,
    [ReturnDateID]              INT NOT NULL,
    [TransactionTimeBand]       INT NOT NULL,
    [LoanTimeBand]              INT NOT NULL,
    [ReturnTimeBand]            INT NOT NULL,
    [ReturnStatusID]            INT NOT NULL,
    [LoanDays]                  INT NULL,
    [ServicePointID]            INT NOT NULL,
    [AtAppropriateServicePoint] BIT NOT NULL,
    [LoanToEncounterMinutes]    INT NULL,
    [EncounterToReturnMinutes]  INT NULL,
    [RecordTypeID]              INT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_FactLog]
    ON [Casenote].[FactLog]([CasenoteMergeRecno] ASC);


﻿CREATE TABLE [Casenote].[BorrowerServicePointMap] (
    [SourceBorrowerID]     INT NOT NULL,
    [SourceServicePointID] INT NOT NULL,
    CONSTRAINT [PK_BorrowerServicePointMap] PRIMARY KEY CLUSTERED ([SourceBorrowerID] ASC)
);


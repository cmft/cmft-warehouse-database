﻿CREATE TABLE [Casenote].[Fact] (
    [MergeRecno]             INT NOT NULL,
    [ContextID]              INT NOT NULL,
    [CasenoteLocationID]     INT NOT NULL,
    [AllocatedDateID]        INT NULL,
    [CasenoteLocationDateID] INT NULL,
    [WithdrawnDateID]        INT NULL,
    [Withdrawn]              BIT NULL,
    CONSTRAINT [PK_Fact] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


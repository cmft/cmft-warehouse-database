﻿CREATE TABLE [Casenote].[BaseLoanReference] (
    [MergeRecno]   INT NOT NULL,
    [ContextID]    INT NOT NULL,
    [BorrowerID]   INT NOT NULL,
    [SystemUserID] INT NULL,
    CONSTRAINT [PK_Casenote_BaseLoan_Reference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


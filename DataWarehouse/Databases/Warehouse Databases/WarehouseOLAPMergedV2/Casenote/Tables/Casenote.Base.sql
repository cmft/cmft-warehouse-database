﻿CREATE TABLE [Casenote].[Base] (
    [MergeRecno]           INT          IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]      VARCHAR (20) NOT NULL,
    [CasenoteNumber]       VARCHAR (20) NOT NULL,
    [AllocatedDate]        DATE         NULL,
    [CasenoteLocationCode] VARCHAR (4)  NULL,
    [CasenoteLocationDate] DATE         NULL,
    [CasenoteStatus]       VARCHAR (10) NULL,
    [WithdrawnDate]        DATE         NULL,
    [ContextCode]          VARCHAR (8)  NOT NULL,
    CONSTRAINT [PK_Base_2] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [CasenoteNumber] ASC, [ContextCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Base_1]
    ON [Casenote].[Base]([MergeRecno] ASC);


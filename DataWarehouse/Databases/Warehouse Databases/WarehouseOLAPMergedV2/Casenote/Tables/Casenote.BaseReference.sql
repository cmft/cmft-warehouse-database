﻿CREATE TABLE [Casenote].[BaseReference] (
    [MergeRecno]         INT NOT NULL,
    [ContextID]          INT NOT NULL,
    [CasenoteLocationID] INT NOT NULL,
    CONSTRAINT [PK_Casenote_Base_Reference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


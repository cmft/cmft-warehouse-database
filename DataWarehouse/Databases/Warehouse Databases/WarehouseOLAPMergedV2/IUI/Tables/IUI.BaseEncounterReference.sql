﻿CREATE TABLE [IUI].[BaseEncounterReference] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [TreatmentDateID]     INT           NULL,
    [OutcomeID]           INT           NOT NULL,
    [TreatmentID]         INT           NOT NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_IUI_Base_Encounter_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


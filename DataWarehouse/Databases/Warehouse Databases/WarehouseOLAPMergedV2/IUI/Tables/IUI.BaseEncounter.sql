﻿CREATE TABLE [IUI].[BaseEncounter] (
    [MergeEncounterRecno] INT          IDENTITY (1, 1) NOT NULL,
    [EncounterRecno]      INT          NOT NULL,
    [DateOfBirth]         DATE         NULL,
    [NHSNumber]           VARCHAR (50) NULL,
    [CasenoteNumber]      VARCHAR (50) NULL,
    [PatientName]         VARCHAR (50) NULL,
    [AttemptNumber]       INT          NULL,
    [CommissionerCode]    VARCHAR (50) NULL,
    [TreatmentDate]       DATE         NULL,
    [OutcomeID]           INT          NULL,
    [TreatmentID]         INT          NULL,
    [InterfaceCode]       VARCHAR (20) NULL,
    [ContextCode]         VARCHAR (8)  NOT NULL,
    [Created]             DATETIME     NULL,
    [Updated]             DATETIME     NULL,
    [ByWhom]              VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseIUIE__4C9E43CC46BF0388] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


﻿







CREATE view [OPWL].[WaitingList] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceWaitingListID = SourceValueID
	,SourceWaitingListCode = SourceValueCode
	,SourceWaitingList = SourceValue
	,LocalWaitingListID = LocalValueID
	,LocalWaitingListCode = LocalValueCode
	,LocalWaitingList = LocalValue
	,NationalWaitingListID = NationalValueID
	,NationalWaitingListCode = NationalValueCode
	,NationalWaitingList = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'OPWLIST'










﻿CREATE procedure [OPWL].[BuildFactEncounter] as


declare
       @StartTime datetime = getdate()
       ,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
       ,@Elapsed int
       ,@Stats varchar(max)


declare @MissingSubSpecialtyID int =
       (
       select
              SubSpecialty.SubSpecialtyID
       from
              WH.SubSpecialty
       where
              SubSpecialty.SubSpecialty = 'Unassigned'
       )

truncate table OPWL.FactEncounter

INSERT INTO OPWL.FactEncounter
(
       MergeEncounterRecno
       ,WaitTargetID
       ,BreachDateID
       ,AgeID
       ,SexID
       ,CensusDateID
       ,ConsultantID
       ,SpecialtyID
       ,SiteID
       ,WaitTypeID
       ,AppointmentDateID
       ,DirectorateID
       ,ContextID
       ,WaitingListID
       ,SourceOfReferralID
       ,DurationID
       ,WaitStatusID
       ,AppointmentDateDurationID
       ,FirstAttendanceID
       ,ServiceID
       ,SubSpecialtyID
       ,IsDiagnosticProcedure
       ,WithAppointmentDate
       ,WithRTTOpenPathway
       ,BookedBeyondBreach
       ,NearBreach
       ,Cases
       ,LengthOfWait
       ,WithRTTStartDate
       ,EthnicCategoryID
       ,ReligionID
)

select
       Encounter.MergeEncounterRecno

       ,WaitTargetID =
              WaitTarget.WaitTargetID

       ,BreachDateID =
              case
              when Encounter.WaitTypeCode in ('60' , '70') -- no breach date for planned or private patients
              then NullDate.DateID
              else
                     coalesce(
                           WaitTarget.BreachDateID
                           ,NullDate.DateID
                     )
              end

       ,Reference.AgeID
       ,Reference.SexID
       ,Reference.CensusDateID
       ,Reference.ConsultantID
       ,SpecialtyID = Reference.PASSpecialtyID
       ,Reference.SiteID
       ,Reference.WaitTypeID
       ,Reference.AppointmentDateID
       ,Directorate.DirectorateID
       ,Reference.ContextID
       ,Reference.WaitingListID
       ,Reference.SourceOfReferralID

       ,Reference.DurationID
       ,Reference.WaitStatusID
       ,Reference.AppointmentDateDurationID
       ,Reference.FirstAttendanceID

       ,ServiceID = Service.AllocationID

       ,SubSpecialtyID = 
              @MissingSubSpecialtyID

       ,IsDiagnosticProcedure = cast(Encounter.DiagnosticProcedure as bit)
       ,WithAppointmentDate = cast(Encounter.WithAppointmentDate as bit)
       ,WithRTTOpenPathway = cast(Encounter.WithRTTOpenPathway as bit)

       ,BookedBeyondBreach =
              cast(
                     case
                     when
                           Encounter.AppointmentDate >= BreachDate.TheDate
                     and    WaitTarget.BreachDateID != NullDate.DateID
                     and    Encounter.AppointmentDate is not null
                     then 1
                     else 0
                     end
                     as bit
              )

       ,NearBreach =
              cast(
                     case
                     when
                           datediff(
                                  month
                                  ,Encounter.AppointmentDate
                                  ,BreachDate.TheDate
                           ) < 2
                     then 1
                     else 0
                     end
                     as bit
              )

       ,Encounter.Cases
       ,Encounter.LengthOfWait
       ,Encounter.WithRTTStartDate
       ,EthnicCategoryID = Reference.EthnicOriginID
       ,Reference.ReligionID
from
       OPWL.BaseEncounter Encounter

left join OPWL.BaseEncounterReference Reference
on     Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join Allocation.DatasetAllocation NationalExam
on     NationalExam.AllocationTypeID = 1
and    NationalExam.DatasetCode = 'OPWAIT'
and    NationalExam.DatasetRecno = Encounter.MergeEncounterRecno

left join Allocation.DatasetAllocation Service
on     Service.AllocationTypeID = 2
and    Service.DatasetCode = 'OPWAIT'
and    Service.DatasetRecno = Encounter.MergeEncounterRecno

left join WH.Directorate
on     Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')

left join WH.Calendar NullDate
on     NullDate.TheDate = '1 jan 1900'


--different versions of records based on various breach targets
left join
       (
       select
              Encounter.MergeEncounterRecno
              ,WaitTargetID = 5
              ,BreachDateID = 
                     (
                     select
                           DateID
                     from
                           WH.Calendar with (nolock)
                     where
                           TheDate = 
                                  dateadd(
                                         day
                                         ,7 * 5
                                         ,dateadd(
                                                day
                                                ,-1 * Encounter.LengthOfWait
                                                --,Census.CensusDate
                                                ,Census.TheDate
                                         )
                                  )
                     )
       from
              OPWL.BaseEncounter Encounter

       left join OPWL.BaseEncounterReference Reference
       on     Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

       --left join WH.Census
       --on   Census.CensusDateID = Reference.CensusDateID

       left join WH.Calendar Census
       on     Census.DateID = Reference.CensusDateID

       union

       select
              Encounter.MergeEncounterRecno
              ,WaitTargetID = 11
              ,BreachDateID = 
                     (
                     select
                           DateID
                     from
                           WH.Calendar with (nolock)
                     where
                           TheDate = 
                                  dateadd(
                                         day
                                         ,7 * 11
                                         ,dateadd(
                                                day
                                                ,-1 * Encounter.LengthOfWait
                                                --,Census.CensusDate
                                                ,Census.TheDate
                                         )
                                  )
                     )
       from
              OPWL.BaseEncounter Encounter

       left join OPWL.BaseEncounterReference Reference
       on     Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

       --left join WH.Census
       --on   Census.CensusDateID = Reference.CensusDateID

       left join WH.Calendar Census
       on     Census.DateID = Reference.CensusDateID

       union

       select
              Encounter.MergeEncounterRecno
              ,WaitTargetID = 13
              ,BreachDateID = 
                     (
                     select
                           DateID
                     from
                           WH.Calendar with (nolock)
                     where
                           TheDate = 
                                  dateadd(
                                         day
                                         ,7 * 13
                                         ,dateadd(
                                                day
                                                ,-1 * Encounter.LengthOfWait
                                                --,Census.CensusDate
                                                ,Census.TheDate
                                         )
                                  )
                     )
       from
              OPWL.BaseEncounter Encounter

       left join OPWL.BaseEncounterReference Reference
       on     Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

       --left join WH.Census
       --on   Census.CensusDateID = Reference.CensusDateID

       left join WH.Calendar Census
       on     Census.DateID = Reference.CensusDateID

       ) WaitTarget
on     WaitTarget.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WH.Calendar BreachDate
on     BreachDate.DateID = WaitTarget.BreachDateID

where
       not
       (
              Encounter.ContextCode = 'CEN||PAS'
       and (
                     Encounter.SpecialtyCode in ( 'PSCH' , 'OPFI' , 'OPOD' , 'OPRE' , 'CADI' , 'RDCS' , 'RPES' , 'COPD','COPT')
              or     Encounter.SourceOfReferralCode in ('WD' , 'WDS')
              )             
       )



select
       @Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
       @Stats =
              'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
       @ProcedureName
       ,@Stats


﻿


Create view [Allocation].[ContractPODByDirectorateHRGFirstAttendanceFlag]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractPODByDirectorateHRGFirstAttendanceFlag'

where
	AllocationTemplateDataset.DirectorateCode = WrkAllocateDataset.DirectorateCode
and AllocationTemplateDataset.NationalFirstAttendanceCode = WrkAllocateDataset.NationalFirstAttendanceCode
and exists
	(
	select
		1
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.RuleBaseList = AllocationTemplateDataset.RuleBaseList1
	and	WrkAllocateDataset.HRGCode = RuleBaseList.RuleBaseListValue
	)
and WrkAllocateDataset.DatasetStartDate between  -- AppointmentTime
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
	and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	) 











﻿







CREATE View [Allocation].[AllocationTemplateDataset] WITH SCHEMABINDING
as

select
	 Priority =
		coalesce(
			 Allocation.Priority
			,Template.Priority
		)

	,Allocation.Active
	,Allocation.AllocationTypeID
	,RuleBase.RuleBaseRecno
	,RuleBase.DatasetCode
	,RuleBase.SourceSpecialtyCode
	,RuleBase.NationalSpecialtyCode
	,RuleBase.SourceClinicCode
	,RuleBase.NationalSiteCode
	,RuleBase.AdmissionTypeCode
	,RuleBase.ProcedureCode
	,RuleBase.Template
	,RuleBase.OldServiceID
	,RuleBase.Division
	,RuleBase.SourceContextCode
	,RuleBase.SourceConsultantCode
	,RuleBase.SourceSourceOfReferralCode
	,RuleBase.NationalFirstAttendanceCode
	,RuleBase.DiagnosticNationalExamCode
	,RuleBase.SourceWaitingListCode
	,RuleBase.ConsultantNationalMainSpecialtyCode
	,RuleBase.AllocationID
	,RuleBase.DiagnosisCode
	,RuleBase.PatientCategoryCode
	,RuleBase.StartWardCode
	,RuleBase.EndWardCode
	,RuleBase.PatientClassificationCode
	,RuleBase.StartSiteCode
	,RuleBase.EndSiteCode
	,RuleBase.Duration
	,RuleBase.StartDirectorateCode
	,RuleBase.AdmissionMethodCode
	,RuleBase.OtherProcedureCode
	,RuleBase.SecondaryOperationCode1
	,RuleBase.SecondaryOperationCode2
	,RuleBase.SecondaryOperationCode3
	,RuleBase.Boolean
	,RuleBase.HRGCode
	,RuleBase.DirectorateCode
	,RuleBase.NationalAdministrativeCategoryCode
	,RuleBase.ReferralSpecialtyTypeCode
	,RuleBase.CommissionerCode
	,RuleBase.NationalSexCode
	,RuleBase.ContractSerialNo
	,RuleBase.NationalNeonatalLevelOfCareCode
	,RuleBase.FirstEpisodeInSpellIndicator
	,RuleBase.SourceIntendedManagementCode
	,RuleBase.SourceAppointmentTypeCode
	,RuleBase.WardCode
	,RuleBase.SourceSiteCode
	,RuleBase.NationalSourceOfReferralCode
	,RuleBase.FromDate
	,RuleBase.ToDate
	,RuleBase.SourceAppointmentType
	,RuleBase.RuleBaseList1
	,RuleBase.RuleBaseList2
	,RuleBase.RuleBaseList3
	,RuleBase.CancerSite
	,RuleBase.DepartmentCode
	,RuleBase.SourceAppointmentStatusCode
	,RuleBase.NationalAttendanceStatusCode
	,RuleBase.ProcedureCoded
	,RuleBase.DischargeMethodCode
	,RuleBase.DischargeDestinationCode
	,RuleBase.DischargeDate
	,RuleBase.NeonatalWardStay
	,RuleBase.DepartmentTypeCode
	,RuleBase.Age
	,RuleBase.NationalAttendanceDisposalCode
	,RuleBase.LOS
	,RuleBase.LocalAdministrativeCategoryCode
	,RuleBase.NationalAdmissionMethodCode
	,RuleBase.SpecialServiceCode
	,RuleBase.NationalDischargeMethodCode
	,RuleBase.NationalAttendanceCategoryCode
	,RuleBase.Reportable
	,RuleBase.OperationDayOfWeek 
	,RuleBase.SessionPeriod 
	,RuleBase.TheatreSurgeonCode 
	,RuleBase.CutOffTime
	,RuleBase.StatusCode
	,RuleBase.SourceAdminCategoryCode 
	,RuleBase.SourceFirstAttendanceCode
	,RuleBase.Theatre
from   	
	Allocation.RuleBase

inner join Allocation.TemplateDataset
on	TemplateDataset.Template =  RuleBase.Template
and	TemplateDataset.DatasetCode = RuleBase.DatasetCode
and	TemplateDataset.Active = 1

inner join Allocation.Allocation
on	Allocation.AllocationID = RuleBase.AllocationID
and	Allocation.Active = 1

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template
and	Template.AllocationTypeID = Allocation.AllocationTypeID

/*
USE [WarehouseOLAPMergedV2]
GO

SET ARITHABORT ON
GO

SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_NULLS ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_WARNINGS ON
GO

SET NUMERIC_ROUNDABORT OFF
GO

CREATE UNIQUE CLUSTERED INDEX [IX_AllocationTemplateDataset] ON [Allocation].[AllocationTemplateDataset] 
(
	[RuleBaseRecno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
*/













GO
CREATE UNIQUE CLUSTERED INDEX [IX_AllocationTemplateDataset]
    ON [Allocation].[AllocationTemplateDataset]([RuleBaseRecno] ASC);


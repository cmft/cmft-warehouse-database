﻿CREATE view Allocation.VTEByAnyDiagnosis
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.VTEByAnyDiagnosis'

where
	exists
	(
	select
		1
	from
		Allocation.WrkAllocateDatasetDiagnosis
	where
		WrkAllocateDatasetDiagnosis.DatasetRecno = WrkAllocateDataset.DatasetRecno
	and	WrkAllocateDatasetDiagnosis.DatasetCode = WrkAllocateDataset.DatasetCode
	and	WrkAllocateDatasetDiagnosis.DiagnosisCode = AllocationTemplateDataset.DiagnosisCode
	)

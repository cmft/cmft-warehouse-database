﻿
CREATE view [Allocation].[ContractFlagBySourceConsultant]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractFlagBySourceConsultant'

where
	AllocationTemplateDataset.SourceConsultantCode = WrkAllocateDataset.SourceConsultantCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode


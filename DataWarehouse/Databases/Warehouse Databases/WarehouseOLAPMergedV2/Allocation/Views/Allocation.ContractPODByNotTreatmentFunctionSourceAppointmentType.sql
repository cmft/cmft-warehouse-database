﻿
CREATE view [Allocation].[ContractPODByNotTreatmentFunctionSourceAppointmentType]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractPODByNotTreatmentFunctionSourceAppointmentType'

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

and not exists
	(
	select
		1
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.RuleBaseList = AllocationTemplateDataset.RuleBaseList1
	and	RuleBaseList.RuleBaseListValue = WrkAllocateDataset.NationalSpecialtyCode
	)

and exists
	(
	select
		1
	from
		OP.AppointmentType
	where
		AppointmentType.SourceAppointmentTypeCode = WrkAllocateDataset.SourceAppointmentTypeCode
	and	AppointmentType.SourceAppointmentType like '%' + AllocationTemplateDataset.SourceAppointmentType + '%'
	)


and	WrkAllocateDataset.DatasetStartDate between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
		and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	) 

﻿


CREATE View [Allocation].[DischargeLetterExclusionByPrimaryProcedurePatientCategory]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.DischargeLetterExclusionByPrimaryProcedurePatientCategory'

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and WrkAllocateDataset.ProcedureCode = AllocationTemplateDataset.ProcedureCode
and WrkAllocateDataset.PatientCategoryCode = AllocationTemplateDataset.PatientCategoryCode




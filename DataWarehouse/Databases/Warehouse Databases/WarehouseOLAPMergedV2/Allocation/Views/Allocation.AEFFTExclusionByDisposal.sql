﻿








CREATE view [Allocation].[AEFFTExclusionByDisposal]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.AEFFTExclusionByDisposal'

where
	AllocationTemplateDataset.NationalAttendanceDisposalCode = WrkAllocateDataset.NationalAttendanceDisposalCode
and
	WrkAllocateDataset.DatasetStartDate		
					between	
						AllocationTemplateDataset.FromDate
					and	coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999')







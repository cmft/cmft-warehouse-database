﻿
CREATE view [Allocation].[ServiceByNextEpisodeSpecialty]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.ServiceByNextEpisodeSpecialty'
and	AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and	AllocationTemplateDataset.Division = WrkAllocateDataset.Division
and	AllocationTemplateDataset.AdmissionTypeCode = WrkAllocateDataset.AdmissionTypeCode

inner join APC.BaseEncounter Encounter
on	Encounter.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno
and	Encounter.FirstEpisodeInSpellIndicator = 1

inner join APC.BaseEncounter NextEncounter
on	NextEncounter.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	NextEncounter.GlobalEpisodeNo = Encounter.GlobalEpisodeNo + 1
and	NextEncounter.SpecialtyCode <> Encounter.SpecialtyCode

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = NextEncounter.SpecialtyCode
and	Specialty.SourceContextCode = NextEncounter.ContextCode

inner join WH.ServiceBase
on	ServiceBase.NationalSpecialtyCode = Specialty.NationalSpecialtyCode
and	ServiceBase.Active = 1
and	not exists
	(
	select
		1
	from
		WH.ServiceBase Previous
	where
		Previous.NationalSpecialtyCode = Specialty.NationalSpecialtyCode
	and	Previous.Active = 1
	and	(
			Previous.Priority < ServiceBase.Priority
		or	(
				Previous.Priority = ServiceBase.Priority
			and	Previous.ServiceID < ServiceBase.ServiceID

			)
		)
	)

inner join Allocation.Allocation
on	Allocation.SourceAllocationID = ServiceBase.ServiceID
and	Allocation.AllocationTypeID = 2 --Service


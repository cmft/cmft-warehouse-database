﻿







CREATE view [Allocation].[APCExclusionByActivityBroker]

as
	
select
	 OriginWarehouse.DatasetRecno
	,OriginWarehouse.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority

from 		
	[$(ActivityBroker)].AB.Pathway Pathway	
		
inner join  [$(ActivityBroker)].AB.[APCSpell] OriginBroker 		
on Pathway.OriginAPCSpellID = OriginBroker.APCSpellID		

inner join Allocation.WrkAllocateDataset OriginWarehouse		
on replace(OriginBroker.SourceUniqueID,'||','/') = OriginWarehouse.ProviderSpellNo  -- When Jabran brings through the datasets, need to do something to update this field?
and OriginWarehouse.DatasetCode = 'APC'

-- Need to join to Destination to ensure an admission has been recorded for the Destination site also, ie a genuine duplicate.
inner join [$(ActivityBroker)].AB.[APCSpell] DestinationBroker		
on Pathway.DestinationAPCSpellID = DestinationBroker.APCSpellID		
		
inner join Allocation.WrkAllocateDataset DestinationWarehouse		
on DestinationBroker.SourceUniqueID = DestinationWarehouse.SourceUniqueID	
and DestinationWarehouse.DatasetCode = 'APC'	

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = OriginWarehouse.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.APCExclusionByActivityBroker'
		
where 	
	Pathway.PathwayTypeID = 1	
	




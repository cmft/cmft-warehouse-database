﻿


CREATE View [Allocation].[DischargeLetterExclusionByNationalSpecialtyPatientCategory]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.DischargeLetterExclusionByNationalSpecialtyPatientCategory'

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and AllocationTemplateDataset.PatientCategoryCode = WrkAllocateDataset.PatientCategoryCode
and datediff(day,WrkAllocateDataset.DateOfBirth,WrkAllocateDataset.AdmissionDate)/365.25 >= 19






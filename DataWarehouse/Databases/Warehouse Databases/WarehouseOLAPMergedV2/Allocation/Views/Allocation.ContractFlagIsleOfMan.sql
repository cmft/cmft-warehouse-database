﻿
CREATE view [Allocation].[ContractFlagIsleOfMan]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Priority =
		coalesce(
			 Template.Priority
			,Allocation.Priority
		)
from
	Allocation.WrkAllocateDataset

inner join Allocation.TemplateDataset
on	TemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	TemplateDataset.Active = 1
and	TemplateDataset.Template = 'Allocation.ContractFlagIsleOfMan'

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template

inner join Allocation.Allocation
on	Allocation.SourceAllocationID = 'YK1'
and	Allocation.AllocationTypeID = 9

where
	WrkAllocateDataset.ContractSerialNo like 'OSV%'
and	WrkAllocateDataset.ContractSerialNo <> 'OSV00A'




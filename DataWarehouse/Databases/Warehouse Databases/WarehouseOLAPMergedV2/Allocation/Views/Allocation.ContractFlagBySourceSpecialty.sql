﻿


CREATE view [Allocation].[ContractFlagBySourceSpecialty]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractFlagBySourceSpecialty'

where
	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

and	WrkAllocateDataset.DatasetStartDate between  -- AppointmentTime
		AllocationTemplateDataset.FromDate
	and	coalesce(
			 AllocationTemplateDataset.ToDate
			,'31 dec 9999'
		) 



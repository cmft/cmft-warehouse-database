﻿


CREATE View [Allocation].[DischargeLetterExclusionByDivisionNationalSpecialtyWard]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.DischargeLetterExclusionByDivisionNationalSpecialtyWard'

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and WrkAllocateDataset.DischargeDivisionCode = AllocationTemplateDataset.Division
and WrkAllocateDataset.NationalSpecialtyCode < AllocationTemplateDataset.NationalSpecialtyCode
and AllocationTemplateDataset.EndWardCode = WrkAllocateDataset.EndWardCode

and	WrkAllocateDataset.DatasetEndDate between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
		and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	) 



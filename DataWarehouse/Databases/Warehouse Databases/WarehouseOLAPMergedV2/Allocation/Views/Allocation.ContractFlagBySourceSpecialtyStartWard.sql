﻿

CREATE view [Allocation].[ContractFlagBySourceSpecialtyStartWard]

as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

where
	AllocationTemplateDataset.Template = 'Allocation.ContractFlagBySourceSpecialtyStartWard'
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.StartWardCode = WrkAllocateDataset.StartWardCode
and	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode



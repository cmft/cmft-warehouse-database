﻿











CREATE view [Allocation].[APCExclusionBySourceSiteSourceSpecialtySourceWard]

as

select 
	DatasetRecno = NonReportableDatasetRecno
	,Base.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from 		
	Allocation.APCExclusionDuplicateAdmission Base		
	
inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = Base.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.APCExclusionBySourceSiteSourceSpecialtySourceWard'

left join APC.DuplicateEpisodeValidation
on ReportableSourceUniqueID = DuplicateEpisodeValidation.SourceUniqueID
and ValidationOutcome = 2

where 
	DuplicateEpisodeValidation.SourceUniqueID is null -- don't want to make anything non reportable if DQ team have validated both spells and marked the opposing record as non reportable
and
	(
		(
			ReportableSourceSiteCode = AllocationTemplateDataset.StartSiteCode
		and ReportableSourceSpecialtyCode = AllocationTemplateDataset.SourceSpecialtyCode
		and ReportableSourceWardCode = AllocationTemplateDataset.StartWardCode
		and AllocationTemplateDataset.Reportable = 1
		)
	or
		(
			NonReportableSourceSiteCode = AllocationTemplateDataset.StartSiteCode
		and NonReportableSourceSpecialtyCode = AllocationTemplateDataset.SourceSpecialtyCode
		and NonReportableSourceWardCode = AllocationTemplateDataset.StartWardCode
		and AllocationTemplateDataset.Reportable = 0
		)
	)
	


--1478
--3mins







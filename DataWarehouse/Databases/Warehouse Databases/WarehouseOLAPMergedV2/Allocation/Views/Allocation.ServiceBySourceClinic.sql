﻿

CREATE view [Allocation].[ServiceBySourceClinic]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.ServiceBySourceClinic'
and	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

where 
	WrkAllocateDataset.DatasetStartDate between  -- AppointmentTime
		AllocationTemplateDataset.FromDate
	and	coalesce(
			 AllocationTemplateDataset.ToDate
			,'31 dec 9999'
		) 



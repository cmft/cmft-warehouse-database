﻿



CREATE view [Allocation].[RuleBaseComparison]

as

select
	[RuleBaseRecno]
	,[DatasetCode]
	,[SourceSpecialtyCode]
	,[NationalSpecialtyCode]
	,[SourceClinicCode]
	,[NationalSiteCode]
	,[AdmissionTypeCode]
	,[ProcedureCode]
	,[Template]
	,[OldServiceID]
	,[Division]
	,[SourceContextCode]
	,[SourceConsultantCode]
	,[SourceSourceOfReferralCode]
	,[NationalFirstAttendanceCode]
	,[DiagnosticNationalExamCode]
	,[SourceWaitingListCode]
	,[ConsultantNationalMainSpecialtyCode]
	--,RuleBase.[AllocationID]
	,Allocation.SourceAllocationID
	,Allocation.Allocation
	,Allocation.Priority
	,[DiagnosisCode]
	,[PatientCategoryCode]
	,[StartWardCode]
	,[EndWardCode]
	,[PatientClassificationCode]
	,[StartSiteCode]
	,[EndSiteCode]
	,[Duration]
	,[StartDirectorateCode]
	,[AdmissionMethodCode]
	,[OtherProcedureCode]
	,[SecondaryOperationCode1]
	,[SecondaryOperationCode2]
	,[SecondaryOperationCode3]
	,[Boolean]
	,[HRGCode]
	,[DirectorateCode]
	,[NationalAdministrativeCategoryCode]
	,[ReferralSpecialtyTypeCode]
	,[CommissionerCode]
	,[NationalSexCode]
	,[ContractSerialNo]
	,[NationalNeonatalLevelOfCareCode]
	,[FirstEpisodeInSpellIndicator]
	,[SourceIntendedManagementCode]
	,[SourceAppointmentTypeCode]
	,[WardCode]
	,[SourceSiteCode]
	,[FromDate]
	,[ToDate]
	,[NationalSourceOfReferralCode]
	,RuleBase.[Created]
	,RuleBase.[ByWhom]
	--,[SourceAppointmentType]
	--,[RuleBaseList1]
	--,[RuleBaseList2]
	--,[RuleBaseList3]
	--,[CancerSite]
	--,[DepartmentCode]
	
	
	,RuleBaseUniqueKey = 
						cast(
								(
								coalesce(RuleBase.[DatasetCode], '') 
								+ '||' + 
								coalesce([SourceSpecialtyCode], '') 
								+ '||' + 
								coalesce([NationalSpecialtyCode], '') 
								+ '||' + 
								coalesce([SourceClinicCode], '') 
								+ '||' + 
								coalesce([NationalSiteCode], '') 
								+ '||' + 
								coalesce([AdmissionTypeCode], '')  
								+ '||' + 
								coalesce([ProcedureCode], '') 
								+ '||' +  
								coalesce(RuleBase.[Template], '') 
								+ '||' + 
								coalesce(cast([OldServiceID] as varchar), '') 
								+ '||' +  
								coalesce([Division], '') 
								+ '||' +  
								coalesce([SourceContextCode], '') 
								+ '||' + 
								coalesce([SourceConsultantCode], '')  
								+ '||' + 
								coalesce([SourceSourceOfReferralCode], '')  
								+ '||' + 
								coalesce([NationalFirstAttendanceCode], '')
								+ '||' +   
								coalesce([DiagnosticNationalExamCode], '')  
								+ '||' + 
								coalesce([SourceWaitingListCode], '')  
								+ '||' + 
								coalesce([ConsultantNationalMainSpecialtyCode], '')  
								+ '||' + 
								--coalesce(cast(RuleBase.[AllocationID] as varchar), '') 
								--+ '||' + 
								--+ '||' +  
								coalesce(Allocation.SourceAllocationID, '')
								+ '||' +  
								coalesce(Allocation.Allocation, '')
								+ '||' +  
								coalesce(cast(Allocation.Priority as varchar), '')
								+ '||' +  
								coalesce([DiagnosisCode], '') 
								+ '||' +  
								coalesce([PatientCategoryCode], '')  
								+ '||' + 
								coalesce([StartWardCode], '') 
								+ '||' + 
								coalesce([EndWardCode], '') 
								+ '||' + 
								coalesce([PatientClassificationCode], '') 
								+ '||' + 
								coalesce([StartSiteCode], '')  
								+ '||' + 
								coalesce([EndSiteCode], '')  
								+ '||' + 
								coalesce(cast([Duration] as varchar), '') 
								+ '||' + 
								coalesce([StartDirectorateCode], '')  
								+ '||' + 
								coalesce([AdmissionMethodCode], '')  
								+ '||' + 
								coalesce([OtherProcedureCode], '')  
								+ '||' + 
								coalesce([SecondaryOperationCode1], '')  
								+ '||' + 
								coalesce([SecondaryOperationCode2], '') 
								+ '||' + 
								coalesce([SecondaryOperationCode3], '')  
								+ '||' + 
								coalesce(cast([Boolean] as varchar), '') 
								+ '||' + 
								coalesce([HRGCode], '')  
								+ '||' + 
								coalesce([DirectorateCode], '')  
								+ '||' + 
								coalesce([NationalAdministrativeCategoryCode], '')  
								+ '||' + 
								coalesce([ReferralSpecialtyTypeCode], '')  
								+ '||' + 
								coalesce([CommissionerCode], '')  
								+ '||' + 
								coalesce([NationalSexCode], '')  
								+ '||' + 
								coalesce([ContractSerialNo], '')  
								+ '||' + 
								coalesce([NationalNeonatalLevelOfCareCode], '')   
								+ '||' + 
								coalesce([FirstEpisodeInSpellIndicator], '')   
								+ '||' + 
								coalesce([SourceIntendedManagementCode], '')  
								+ '||' + 
								coalesce([SourceAppointmentTypeCode], '')   
								+ '||' + 
								coalesce([WardCode], '')   
								+ '||' + 
								coalesce([SourceSiteCode], '')   
								+ '||' + 
								cast(coalesce([FromDate], '') as varchar)   
								+ '||' + 
								cast(coalesce([ToDate], '')  as varchar)    
								+ '||' + 
								coalesce([NationalSourceOfReferralCode], '')   
								--+ '||' + 
								--coalesce([SourceAppointmentType], '')  
								--+ '||' + 
								--coalesce([RuleBaseList1], '')   
								--+ '||' + 
								--coalesce([RuleBaseList2], '')  
								--+ '||' + 
								--coalesce([RuleBaseList3], '')   
								--+ '||' + 
								--coalesce([CancerSite], '')   
								--+ '||' + 
								--coalesce([DepartmentCode], '')   
								)
							as varchar(400)
							)
							
							
from
	[Allocation].[RuleBase]
inner join [Allocation].Allocation
on	[RuleBase].AllocationID = Allocation.AllocationID



﻿

CREATE view [Allocation].[ContractFlagByStartWard]

as

select
	 WrkAllocateDataset.DatasetRecno 
	,WrkAllocateDataset.DatasetCode 
	,AllocationID = AllocationTemplateDataset.AllocationID 
	,AllocationTemplateDataset.Priority 
from
	Allocation.WrkAllocateDataset 

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

where
	AllocationTemplateDataset.Template = 'Allocation.ContractFlagByStartWard'
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.StartWardCode = WrkAllocateDataset.StartWardCode



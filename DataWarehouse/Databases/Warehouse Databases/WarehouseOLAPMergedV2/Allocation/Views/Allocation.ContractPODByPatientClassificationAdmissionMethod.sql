﻿CREATE view [Allocation].[ContractPODByPatientClassificationAdmissionMethod]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20141112	RR created.  Request by Phil Huitson, to allocate where procedure in any position
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractPODByPatientClassificationAdmissionMethod'

where
	WrkAllocateDataset.PatientClassificationCode = AllocationTemplateDataset.PatientClassificationCode
and (
		left(WrkAllocateDataset.NationalAdmissionMethodCode,1) = AllocationTemplateDataset.NationalAdmissionMethodCode
	or
		AllocationTemplateDataset.NationalAdmissionMethodCode is null
	)

﻿






CREATE view [Allocation].[APCFFTExclusionByLOSNotAssessmentWard]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.APCFFTExclusionByLOSNotAssessmentWard'

where
	AllocationTemplateDataset.LOS > WrkAllocateDataset.LOS
and
	WrkAllocateDataset.DischargeDate		
					between	
						AllocationTemplateDataset.FromDate
					and	coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999')	
and not exists
	(
	select
		1
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.RuleBaseList = AllocationTemplateDataset.RuleBaseList1
	and	WrkAllocateDataset.EndWardCode = RuleBaseList.RuleBaseListValue
	)





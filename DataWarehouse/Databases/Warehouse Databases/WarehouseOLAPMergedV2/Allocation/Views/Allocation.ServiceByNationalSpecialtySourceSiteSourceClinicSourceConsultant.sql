﻿


create view [Allocation].[ServiceByNationalSpecialtySourceSiteSourceClinicSourceConsultant]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.ServiceByNationalSpecialtySourceSiteSourceClinicSourceConsultant'
and	AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and	AllocationTemplateDataset.SourceSiteCode = WrkAllocateDataset.SourceSiteCode
and	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and	AllocationTemplateDataset.SourceConsultantCode = WrkAllocateDataset.SourceConsultantCode

where 
	WrkAllocateDataset.DatasetStartDate between  -- AppointmentTime
		AllocationTemplateDataset.FromDate
	and	coalesce(
			 AllocationTemplateDataset.ToDate
			,'31 dec 9999'
		) 




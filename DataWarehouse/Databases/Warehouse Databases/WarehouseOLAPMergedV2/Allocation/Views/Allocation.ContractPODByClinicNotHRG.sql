﻿
CREATE view [Allocation].[ContractPODByClinicNotHRG]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractPODByClinicNotHRG'

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and not exists
	(
	select
		1
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.RuleBaseList = AllocationTemplateDataset.RuleBaseList1
	and	WrkAllocateDataset.HRGCode = RuleBaseList.RuleBaseListValue
	)



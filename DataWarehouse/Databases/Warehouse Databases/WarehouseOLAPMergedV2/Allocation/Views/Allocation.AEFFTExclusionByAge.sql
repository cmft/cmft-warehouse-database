﻿





CREATE view [Allocation].[AEFFTExclusionByAge]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.AEFFTExclusionByAge'

where
	AllocationTemplateDataset.Age > [$(CMFT)].Dates.GetAge(WrkAllocateDataset.DateOfBirth,WrkAllocateDataset.DepartureTime)








﻿CREATE view Allocation.ServiceByNationalSpecialtyDivisionSourceClinic
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.ServiceByNationalSpecialtyDivisionSourceClinic'
and	AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and	AllocationTemplateDataset.Division = WrkAllocateDataset.Division
and	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

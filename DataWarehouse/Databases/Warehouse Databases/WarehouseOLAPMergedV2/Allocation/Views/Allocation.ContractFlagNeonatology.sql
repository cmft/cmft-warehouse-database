﻿CREATE view [Allocation].ContractFlagNeonatology
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Priority =
		coalesce(
			 Template.Priority
			,Allocation.Priority
		)
from
	Allocation.WrkAllocateDataset

inner join Allocation.TemplateDataset
on	TemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	TemplateDataset.Active = 1
and	TemplateDataset.Template = 'Allocation.ContractFlagNeonatology'

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template

inner join Allocation.Allocation
on	Allocation.SourceAllocationID = 'NEONATOLOGY'
and	Allocation.AllocationTypeID = 9

where
	WrkAllocateDataset.NationalSpecialtyCode = '422' 	
and not exists 	
	(		
	select
		1
	from
		APC.BaseWardStay

	inner join APC.BaseEncounter
	on	BaseEncounter.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno

	where	
		BaseWardStay.WardCode = 'NICT'
	and BaseWardStay.ProviderSpellNo = BaseEncounter.ProviderSpellNo
	and	BaseWardStay.ContextCode = BaseEncounter.ContextCode
	)	

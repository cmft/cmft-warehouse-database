﻿


CREATE view [Allocation].[ContractFlagBySourceSpecialtyHRG]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractFlagBySourceSpecialtyHRG'

where
	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and	AllocationTemplateDataset.HRGCode = left(WrkAllocateDataset.HRGCode,2)

and	WrkAllocateDataset.DatasetStartDate between  -- AppointmentTime
		AllocationTemplateDataset.FromDate
	and	coalesce(
			 AllocationTemplateDataset.ToDate
			,'31 dec 9999'
		) 


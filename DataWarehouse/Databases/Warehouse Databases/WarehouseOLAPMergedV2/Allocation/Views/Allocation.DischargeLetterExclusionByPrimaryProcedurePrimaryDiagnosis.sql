﻿


CREATE View [Allocation].[DischargeLetterExclusionByPrimaryProcedurePrimaryDiagnosis]

as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.DischargeLetterExclusionByPrimaryProcedurePrimaryDiagnosis'

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and (
	left(WrkAllocateDataset.ProcedureCode,3) = AllocationTemplateDataset.ProcedureCode
	or left(WrkAllocateDataset.DiagnosisCode,3) = AllocationTemplateDataset.DiagnosisCode
	or WrkAllocateDataset.DiagnosisCode = AllocationTemplateDataset.DiagnosisCode
	)




﻿

CREATE view [Allocation].[DirectorateByORMISSessionORMISClinician]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Priority =
		coalesce(
			 Allocation.Priority
			,Template.Priority
		)
from
	Allocation.WrkAllocateDataset

inner join Allocation.TemplateDataset
on	TemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	TemplateDataset.Template = 'Allocation.DirectorateByORMISSessionORMISClinician'
and	TemplateDataset.Active = 1

inner join Allocation.RuleBase
on	RuleBase.Template = TemplateDataset.Template
and	RuleBase.DatasetCode = 'DIRECT' --the Directorate allocation rules are held once under this DatasetCode

inner join Allocation.Allocation
on	Allocation.AllocationID = RuleBase.AllocationID
and	Allocation.Active = 1

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template
and	Template.AllocationTypeID = Allocation.AllocationTypeID

where
	RuleBase.SourceSiteCode = WrkAllocateDataset.SourceSiteCode
and	RuleBase.FirstEpisodeInSpellIndicator = WrkAllocateDataset.GlobalEpisodeNo
and	RuleBase.OperationDayOfWeek = WrkAllocateDataset.OperationDayOfWeek
and	RuleBase.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and	RuleBase.AdmissionTypeCode = WrkAllocateDataset.AdmissionTypeCode
and	
	(
		RuleBase.TheatreSurgeonCode = WrkAllocateDataset.TheatreSurgeonCode
	or
		RuleBase.SourceConsultantCode = WrkAllocateDataset.TheatreConsultantCode
	)
and 
	(
		RuleBase.SessionPeriod = WrkAllocateDataset.SessionPeriod
	or
		RuleBase.SessionPeriod is null
	)
and	WrkAllocateDataset.ActivityDate between
		coalesce (RuleBase.FromDate, '1 Jan 1900')
	and	coalesce(RuleBase.ToDate,'31 dec 9999')




	


﻿
CREATE view [Allocation].[ContractPODByManagementIntention]

as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractPODByManagementIntention'

where
	WrkAllocateDataset.SourceIntendedManagementCode = AllocationTemplateDataset.SourceIntendedManagementCode
and	WrkAllocateDataset.SourceContextCode = AllocationTemplateDataset.SourceContextCode



﻿

CREATE view [Allocation].[ContractPODBySourceSpecialtySourceSourceOfReferralSourceAppointmentStatus]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractPODBySourceSpecialtySourceSourceOfReferralSourceAppointmentStatus'

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and	AllocationTemplateDataset.SourceSourceOfReferralCode = WrkAllocateDataset.SourceSourceOfReferralCode
and	AllocationTemplateDataset.SourceAppointmentStatusCode = WrkAllocateDataset.SourceAppointmentStatusCode




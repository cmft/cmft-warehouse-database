﻿CREATE view [Allocation].[ContractPODByAttendanceCategory]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority

from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractPODByAttendanceCategory'

inner join Allocation.Allocation
on AllocationTemplateDataset.AllocationID = Allocation.AllocationID
where
	AllocationTemplateDataset.NationalAttendanceCategoryCode = WrkAllocateDataset.NationalAttendanceCategoryCode
and WrkAllocateDataset.ArrivalTime >= AllocationTemplateDataset.FromDate
and 
	(WrkAllocateDataset.ArrivalTime <= AllocationTemplateDataset.ToDate
	or
	AllocationTemplateDataset.ToDate is null
	)

﻿



CREATE view [Allocation].[VTEByStartWardEndWard]

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.VTEByStartWardEndWard'

where
	WrkAllocateDataset.StartWardCode = AllocationTemplateDataset.StartWardCode
and	WrkAllocateDataset.EndWardCode = AllocationTemplateDataset.EndWardCode
and	WrkAllocateDataset.SourceContextCode = AllocationTemplateDataset.SourceContextCode




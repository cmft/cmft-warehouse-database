﻿


Create View [Allocation].[DischargeLetterExclusionByDivisionSourceSpecialtyNeonatalWardStay]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.DischargeLetterExclusionByDivisionSourceSpecialtyNeonatalWardStay'

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and AllocationTemplateDataset.Division = WrkAllocateDataset.DischargeDivisionCode
--and AllocationTemplateDataset.NationalNeonatalLevelOfCareCode = WrkAllocateDataset.NationalNeonatalLevelOfCareCode
and AllocationTemplateDataset.NeonatalWardStay = WrkAllocateDataset.NeonatalWardStay






﻿CREATE view Allocation.ServiceByNationalSpecialtySourceConsultantDivision
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.ServiceByNationalSpecialtySourceConsultantDivision'
and	AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and	AllocationTemplateDataset.SourceConsultantCode = WrkAllocateDataset.SourceConsultantCode
and	AllocationTemplateDataset.Division = WrkAllocateDataset.Division
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

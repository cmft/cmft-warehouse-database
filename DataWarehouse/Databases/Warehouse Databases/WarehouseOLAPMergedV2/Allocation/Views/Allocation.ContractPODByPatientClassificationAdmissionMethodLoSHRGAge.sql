﻿
CREATE view [Allocation].[ContractPODByPatientClassificationAdmissionMethodLoSHRGAge]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority

from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractPODByPatientClassificationAdmissionMethodLoSHRGAge'

where
	WrkAllocateDataset.PatientClassificationCode = AllocationTemplateDataset.PatientClassificationCode
and	left(WrkAllocateDataset.NationalAdmissionMethodCode,1) = AllocationTemplateDataset.NationalAdmissionMethodCode
and WrkAllocateDataset.LOS < AllocationTemplateDataset.LOS
and 
	(datediff(day,WrkAllocateDataset.DateOfBirth,WrkAllocateDataset.AdmissionDate)/365.25) >= AllocationTemplateDataset.Age

and exists
	(
	select
		1
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.RuleBaseList = AllocationTemplateDataset.RuleBaseList1
	and	WrkAllocateDataset.HRGCode = RuleBaseList.RuleBaseListValue
	)

and WrkAllocateDataset.DischargeDate between FromDate and coalesce(ToDate,'31 dec 9999'	) 


﻿








CREATE view [Allocation].[APCFFTExclusionByPatientClassification]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.APCFFTExclusionByPatientClassification'

where
	AllocationTemplateDataset.PatientClassificationCode = WrkAllocateDataset.PatientClassificationCode
and
	WrkAllocateDataset.DischargeDate		
					between	
						AllocationTemplateDataset.FromDate
					and	coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999')







﻿


CREATE View [Allocation].[DischargeLetterExclusionByPatientClassification]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.DischargeLetterExclusionByPatientClassification'

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and AllocationTemplateDataset.PatientClassificationCode = WrkAllocateDataset.PatientClassificationCode




﻿
CREATE view [Allocation].[VTEByStartDirectorateStartWard]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.VTEByStartDirectorateStartWard'

where
	WrkAllocateDataset.StartDirectorateCode = AllocationTemplateDataset.StartDirectorateCode
and	WrkAllocateDataset.StartWardCode = AllocationTemplateDataset.StartWardCode
and 
	(WrkAllocateDataset.AdmissionTime < AllocationTemplateDataset.ToDate
	or AllocationTemplateDataset.ToDate is null
	)

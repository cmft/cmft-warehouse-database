﻿CREATE view Allocation.ServiceByNationalSpecialtySourceClinicNationalFirstAttendancePrimaryProcedure
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.ServiceByNationalSpecialtySourceClinicNationalFirstAttendancePrimaryProcedure'
and	AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and	AllocationTemplateDataset.NationalFirstAttendanceCode = WrkAllocateDataset.NationalFirstAttendanceCode
and	AllocationTemplateDataset.ProcedureCode = WrkAllocateDataset.ProcedureCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

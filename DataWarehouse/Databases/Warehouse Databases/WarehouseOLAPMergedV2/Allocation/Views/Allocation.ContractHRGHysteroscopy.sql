﻿CREATE view Allocation.ContractHRGHysteroscopy 

as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode


	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.Allocation
on	Allocation.Active = 1
and	Allocation.SourceAllocationID = 'HYST'
and	Allocation.AllocationTypeID =  6

where
	WrkAllocateDataset.DatasetCode = 'OP'
and	exists
		(
		select
			1
		from
			Allocation.WrkAllocateDatasetOperation
		where 
			WrkAllocateDataset.DatasetRecno = WrkAllocateDatasetOperation.DatasetRecno
		and WrkAllocateDatasetOperation.DatasetCode = 'OP'
		and left(WrkAllocateDatasetOperation.OperationCode, 3) = 'Q18'
		)
and	exists
		(
		select
			1
		from
			Allocation.WrkAllocateDatasetOperation
		where
			WrkAllocateDataset.DatasetRecno = WrkAllocateDatasetOperation.DatasetRecno
		and WrkAllocateDatasetOperation.DatasetCode = 'OP'
		and WrkAllocateDatasetOperation.OperationCode = 'Q55.5'
		)

					



--whereDatasetRecno
--	exists
--	(
--	select
--		1
--	from
--		Allocation.WrkAllocateDatasetOperation
--	where
--		WrkAllocateDatasetOperation.DatasetRecno = WrkAllocateDataset.DatasetRecno
--	and	WrkAllocateDatasetOperation.DatasetCode = WrkAllocateDataset.DatasetCode
--	and	WrkAllocateDatasetOperation.OperationCode = AllocationTemplateDataset.ProcedureCode
--	)
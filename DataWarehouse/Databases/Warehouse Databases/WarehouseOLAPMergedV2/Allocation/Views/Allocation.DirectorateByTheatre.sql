﻿
CREATE view [Allocation].[DirectorateByTheatre]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID

	,Priority =
		coalesce(
			 Allocation.Priority
			,Template.Priority
		)

from
	Allocation.WrkAllocateDataset

inner join Allocation.TemplateDataset
on	TemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	TemplateDataset.Template = 'Allocation.DirectorateByTheatre'
and	TemplateDataset.Active = 1

inner join Allocation.RuleBase
on	RuleBase.Template = TemplateDataset.Template
and	RuleBase.DatasetCode = 'DIRECT' --the Directorate allocation rules are held once under this DatasetCode

inner join Allocation.Allocation
on	Allocation.AllocationID = RuleBase.AllocationID
and	Allocation.Active = 1

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template
and	Template.AllocationTypeID = Allocation.AllocationTypeID

where
	WrkAllocateDataset.Theatre = RuleBase.Theatre

and	(WrkAllocateDataset.ActivityDate between
		RuleBase.FromDate and coalesce(RuleBase.ToDate,'31 dec 9999')
	)


﻿
CREATE view [Allocation].[ContractFlagOverseas]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Priority =
		coalesce(
			 Template.Priority
			,Allocation.Priority
		)
from
	Allocation.WrkAllocateDataset

inner join Allocation.TemplateDataset
on	TemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	TemplateDataset.Active = 1
and	TemplateDataset.Template = 'Allocation.ContractFlagOverseas'

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template

inner join Allocation.Allocation
on	Allocation.SourceAllocationID = 'OSV'
and	Allocation.AllocationTypeID = 9

where
	WrkAllocateDataset.ContractSerialNo in
	(
	 'X98'
	,'OSV00A'
	)

or	(
		WrkAllocateDataset.Postcode like 'ZZ99%'
	and WrkAllocateDataset.Postcode not in 
		(
		 'ZZ99 3WZ'
		,'ZZ99 3VZ'
		,'ZZ99 3CZ'
		)
	)


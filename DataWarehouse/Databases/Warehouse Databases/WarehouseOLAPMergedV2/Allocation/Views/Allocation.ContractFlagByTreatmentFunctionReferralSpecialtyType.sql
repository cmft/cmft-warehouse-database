﻿
CREATE view [Allocation].[ContractFlagByTreatmentFunctionReferralSpecialtyType]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractFlagByTreatmentFunctionReferralSpecialtyType'

where
	(
		AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
	or	AllocationTemplateDataset.NationalSpecialtyCode is null
	)
and	AllocationTemplateDataset.ReferralSpecialtyTypeCode = WrkAllocateDataset.ReferralSpecialtyTypeCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode




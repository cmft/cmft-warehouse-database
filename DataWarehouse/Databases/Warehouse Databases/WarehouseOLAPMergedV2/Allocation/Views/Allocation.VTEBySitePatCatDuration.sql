﻿CREATE view Allocation.VTEBySitePatCatDuration
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.VTEBySitePatCatDuration'

where
	WrkAllocateDataset.StartSiteCode = AllocationTemplateDataset.StartSiteCode
and	WrkAllocateDataset.EndSiteCode = AllocationTemplateDataset.EndSiteCode
and	(
		WrkAllocateDataset.PatientCategoryCode = AllocationTemplateDataset.PatientCategoryCode
	or	datediff(
			 minute
			,WrkAllocateDataset.DatasetStartDate
			,WrkAllocateDataset.DatasetEndDate
		)
		<
		AllocationTemplateDataset.Duration
	)

﻿
CREATE view [Allocation].[PurchaserCodeBySpecialistCommissioningInclusion] as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
from
	Allocation.WrkAllocateDataset

inner join APC.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno

inner join [$(Organisation)].ODS.CCG CCGMap
on CCGMap.OrganisationCode = BaseEncounter.CCGCode


inner join Allocation.Allocation
on	Allocation.Active = 1
and Allocation.AllocationTypeID = 11
and	Allocation.SourceAllocationID = CCGMap.HACode

where

	WrkAllocateDataset.DatasetCode = 'APC' 

	and exists

	(
		select
			GlobalProviderSpellNo
		from
			APC.BaseEncounter BaseEncounterCheck

		inner join APC.PSSSpell
		on PSSSpell.MergeEncounterRecno = BaseEncounterCheck.MergeEncounterRecno 

		where 
			BaseEncounterCheck.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo

	)

	union all


	select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
from
	Allocation.WrkAllocateDataset

inner join OP.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno

inner join [$(Organisation)].ODS.CCG CCGMap
on CCGMap.OrganisationCode = BaseEncounter.CCGCode


inner join Allocation.Allocation
on	Allocation.Active = 1
and Allocation.AllocationTypeID = 11
and	Allocation.SourceAllocationID = CCGMap.HACode

where

	WrkAllocateDataset.DatasetCode = 'OP' 

	and exists

	(
		select 

			MergeEncounterRecno

		from

		OP.PSSEncounter 
		
		where 
		PSSEncounter.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

	)


﻿CREATE view [Allocation].[DirectorateByWard]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID

	,Priority =
		coalesce(
			 Allocation.Priority
			,Template.Priority
		)

from
	Allocation.WrkAllocateDataset

inner join Allocation.TemplateDataset
on	TemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	TemplateDataset.Template = 'Allocation.DirectorateByWard'
and	TemplateDataset.Active = 1

inner join Allocation.RuleBase
on	RuleBase.Template = TemplateDataset.Template
and	RuleBase.DatasetCode = 'DIRECT' --the Directorate allocation rules are held once under this DatasetCode

inner join Allocation.Allocation
on	Allocation.AllocationID = RuleBase.AllocationID
and	Allocation.Active = 1

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template
and	Template.AllocationTypeID = Allocation.AllocationTypeID

where
	exists 	
		(		
		select
			1
		from
			APC.BaseWardStay

		inner join APC.BaseEncounter
		on	BaseEncounter.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno

		where	
			BaseWardStay.WardCode = 'NICT'
		and BaseWardStay.ProviderSpellNo = BaseEncounter.ProviderSpellNo
		and	BaseWardStay.ContextCode = BaseEncounter.ContextCode
		)	
and
	RuleBase.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	(WrkAllocateDataset.ActivityDate between
		RuleBase.FromDate and coalesce(RuleBase.ToDate,'31 dec 9999')
	or
	WrkAllocateDataset.ActivityDate is null -- End Directorate, this could be null if patient still an IP, ie Activity Date = Episode End Date, which could be null
	)

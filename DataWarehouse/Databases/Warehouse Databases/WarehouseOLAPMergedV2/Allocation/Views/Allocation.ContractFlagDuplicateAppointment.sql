﻿CREATE view Allocation.ContractFlagDuplicateAppointment as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.Allocation
on	Allocation.Active = 1
and	Allocation.Allocation = 'Duplicate Appointment'


/* Needs re writing ASAP */

inner join
		(
		select
			BaseEncounter.MergeEncounterRecno
			,SequenceNo = 
				case
					when BaseEncounter.ContextCode = 'TRA||UG'
					then
						case
							when SourceAppointmentType like '%Pre-op%'
							and
								(
									select 
										count (*)
									from
										OP.BaseEncounter EncounterDuplicate

									inner join	OP.BaseEncounterReference
									on	EncounterDuplicate.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

									inner join	OP.AppointmentType
									on AppointmentType.SourceAppointmentTypeID = AppointmentTypeID
                       
									where
										AppointmentType.SourceAppointmentType like '%Pre-Op%' 
									and
										EncounterDuplicate.SourcePatientNo = BaseEncounter.SourcePatientNo
									and
										EncounterDuplicate.AppointmentDate = BaseEncounter.AppointmentDate
									and
										EncounterDuplicate.ContextCode = BaseEncounter.ContextCode						
								) = 1

							then 1
							else
								row_number() over (
													partition by 
														BaseEncounter.DistrictNo
														,BaseEncounter.AppointmentDate 
														,Specialty.NationalSpecialtyCode 
													order by 
														DistrictNo
														/* Procedure Codes */
														,case
															when OPProcedureHRG.HRGCode is not null
															then 0
															else 1
														end
														/* First Attendance */
														,FirstAttendance.NationalFirstAttendanceCode 
														/* Ensure SPN Nurse Appointment Selected last */
														,case
															when AppointmentType.SourceAppointmentType like '%Pre-op%'
															then 1
															else 0
														end
														,case 
															when AppointmentType.SourceAppointmentType like '%(SpN)%'
															then 1
															else 0
														end
													)
							end
					when BaseEncounter.ContextCode = 'CEN||PAS'
					then
						case
							when Clinic.SourceClinicCode in ('GOLDFLD','HUMP', 'HUMPHREY', 'MTCAM', 'MTCPM','MTCEVE', 'MTCLOG', 'MTCOCT')
							and NationalFirstAttendanceCode = '2'
							then
							row_number() over (
												partition by 
														BaseEncounter.SourcePatientNo
														,BaseEncounter.SourceEncounterNo
														,BaseEncounter.AppointmentDate
														--,Clinic.SourceClinicCode
												order by 
														/* Procedure Codes */
														case
															when BaseEncounter.PrimaryOperationCode is null
															then 1
															else 0
														end
														,AppointmentTime
														,BaseEncounter.EncounterRecno
												)
							else 1
						end
					end
		from
			OP.BaseEncounter

		inner join	OP.BaseEncounterReference
		on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

		inner join	OP.AppointmentType
		on AppointmentType.SourceAppointmentTypeID = AppointmentTypeID

		inner join	OP.Clinic
		on Clinic.SourceClinicID = ClinicID

		inner join	WH.Specialty
		on Specialty.SourceSpecialtyID = ReferralSpecialtyID

		inner join	OP.FirstAttendance
		on FirstAttendance.SourceFirstAttendanceID = DerivedFirstAttendanceID

		inner join OP.HRG4Encounter
		on HRG4Encounter.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

		left outer join
						(
						select
							HRGCode = EntityCode
						from
							[$(Warehouse)].dbo.EntityLookup
						where
							EntityTypeCode = 'OPPROCEDUREHRG'
						) OPProcedureHRG

		on	OPProcedureHRG.HRGCode = HRG4Encounter.HRGCode

		) Duplicate

		on	Duplicate.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno
		and	Duplicate.SequenceNo > 1


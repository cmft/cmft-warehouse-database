﻿

CREATE view [Allocation].[ServiceByAvastin] as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.Allocation
on	Allocation.Active = 1
and	Allocation.Allocation = '130 - Avastin'

inner join OP.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno

where
	WrkAllocateDataset.NationalSpecialtyCode = '130'
and	WrkAllocateDataset.NationalFirstAttendanceCode = '2'
and	WrkAllocateDataset.ProcedureCode = 'C79.4'
and	WrkAllocateDataset.SourceContextCode = 'CEN||PAS'
and	(
		(
			WrkAllocateDataset.SourceClinicCode in
			(
			 'MTCAM'
			,'MTCEVE'
			,'GMAN'
			)
		and	WrkAllocateDataset.DatasetStartDate < '14 May 2012'
		)
	or	(
			WrkAllocateDataset.SourceClinicCode = 'AVASTIN'
		and	WrkAllocateDataset.DatasetStartDate > '13 May 2012'
		)
	)

--the operation tables need to come across to the OLAP database to simplify this query...
and	(
		exists
		(
		select
			1
		from
			[$(Warehouse)].OP.Operation
		where
			Operation.OPSourceUniqueID = BaseEncounter.SourceUniqueID
		and	BaseEncounter.ContextCode = 'CEN||PAS'
		and	Operation.OperationCode = 'X93.1'
		)

--this appears to be a Central only rule
	--or	exists
	--	(
	--	select
	--		1
	--	from
	--		TraffordWarehouse.dbo.OPProcedure
	--	where
	--		OPProcedure.OPSourceUniqueID = Base.SourceUniqueID
	--	and	Base.ContextCode = 'TRA||UG'
	--	and	OPProcedure.ProcedureCode = 'X93.1'
	--	)
	)



﻿


Create view [Allocation].[ContractFlagBySourceContext]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractFlagBySourceContext'


where 
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and WrkAllocateDataset.DatasetStartDate between  -- AppointmentTime
	coalesce(
		AllocationTemplateDataset.FromDate 
		,'1 Jan 1900'
		)
and	
	coalesce(
		AllocationTemplateDataset.ToDate
		,'31 dec 9999'	
		) 




﻿CREATE view Allocation.VTEByNationalSpecialtyAdmissionMethod
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.VTEByNationalSpecialtyAdmissionMethod'

where
	WrkAllocateDataset.NationalSpecialtyCode = AllocationTemplateDataset.NationalSpecialtyCode
and	WrkAllocateDataset.AdmissionMethodCode = AllocationTemplateDataset.AdmissionMethodCode

﻿



CREATE view [Allocation].[AEExclusionByPriorAttendance]

as
	
select
	 CentralAttendance.DatasetRecno
	,CentralAttendance.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset CentralAttendance
	
inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = CentralAttendance.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.AEExclusionByPriorAttendance'
	
where 	
	CentralAttendance.SourceContextCode = 'CEN||SYM'
and CentralAttendance.StartSiteCode = 'RW3MR'	
and CentralAttendance.ArrivalTimeAdjusted >= '28 Nov 2013'	
and exists
			(
				
			select 		
				1
			from		
				Allocation.WrkAllocateDataset TraffordAttendance	
					
			where		
				TraffordAttendance.SourceContextCode = 'TRA||SYM' 	
			--and	TraffordAttendance.NationalAttendanceDisposalCode = '01'	
			and	TraffordAttendance.SourceAttendanceDisposalCode = '7084' -- Admitted To Central Manchester Site	
			and TraffordAttendance.ArrivalTime >= '28 Nov 2013'	
			and	TraffordAttendance.NHSNumber = CentralAttendance.NHSNumber			
			and	datediff(hour, TraffordAttendance.DepartureTime, CentralAttendance.ArrivalTimeAdjusted) between 0 and 6
				
		) 	



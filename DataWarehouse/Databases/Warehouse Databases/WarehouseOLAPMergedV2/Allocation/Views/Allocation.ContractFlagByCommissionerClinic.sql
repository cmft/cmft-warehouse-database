﻿
CREATE view [Allocation].[ContractFlagByCommissionerClinic]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

where
	AllocationTemplateDataset.Template = 'Allocation.ContractFlagByCommissionerClinic'
and	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and	AllocationTemplateDataset.CommissionerCode = WrkAllocateDataset.CommissionerCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode



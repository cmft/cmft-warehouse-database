﻿





CREATE view [Allocation].[APCExclusionBySourceSiteSourceSpecialty]

as
	
select 
	 DatasetRecno = NonReportableDatasetRecno
	,Base.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from 		
	Allocation.APCExclusionDuplicateAdmission Base		
	
inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = Base.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.APCExclusionBySourceSiteSourceSpecialty'

left join APC.DuplicateEpisodeValidation
on ReportableSourceUniqueID = DuplicateEpisodeValidation.SourceUniqueID
and DuplicateEpisodeValidation.ValidationOutcome = 2

where 
	DuplicateEpisodeValidation.SourceUniqueID is null -- don't want to make anything non reportable if DQ team have validated both spells and marked the opposing record as non reportable
and 
	(
		(
			ReportableSourceSiteCode = AllocationTemplateDataset.StartSiteCode
		and ReportableSourceSpecialtyCode = AllocationTemplateDataset.SourceSpecialtyCode
		and AllocationTemplateDataset.Reportable = 1
		)
	or
		(
			NonReportableSourceSiteCode = AllocationTemplateDataset.StartSiteCode
		and NonReportableSourceSpecialtyCode = AllocationTemplateDataset.SourceSpecialtyCode
		and AllocationTemplateDataset.Reportable = 0
		)
	)
	
--48

--3mins - reduced to 0 seconds by using the table






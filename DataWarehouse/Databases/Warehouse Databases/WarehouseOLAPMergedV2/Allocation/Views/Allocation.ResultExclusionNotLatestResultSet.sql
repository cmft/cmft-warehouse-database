﻿









CREATE view [Allocation].[ResultExclusionNotLatestResultSet]

as
       

select 
       WrkAllocateDataset.DatasetRecno
       ,WrkAllocateDataset.DatasetCode
       ,AllocationID = AllocationTemplateDataset.AllocationID
       ,AllocationTemplateDataset.Priority
       --,WrkAllocateDataset.SourceContextCode
from
       Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on     AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and    AllocationTemplateDataset.Active = 1
and    AllocationTemplateDataset.Template = 'Allocation.ResultExclusionNotLatestResultSet' 
and	   AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

where
	exists  
       (
       select
              1
       from
              Result.BaseResult Later
       where
           Later.ResultSetSourceUniqueID = WrkAllocateDataset.ResultSetSourceUniqueID
       and Later.ResultTime > WrkAllocateDataset.ResultTime
       and Later.ResultSetStatusCode <> AllocationTemplateDataset.StatusCode
       and Later.ContextCode = AllocationTemplateDataset.SourceContextCode
       ) 

and	   
	  WrkAllocateDataset.StatusCode <> AllocationTemplateDataset.StatusCode










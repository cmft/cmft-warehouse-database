﻿




CREATE view [Allocation].[APCExclusionByDuplicateAdmission]

as


select 
	 DatasetRecno = Base.NonReportableDatasetRecno
	,Base.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority

from 
	Allocation.APCExclusionDuplicateAdmission Base		
--	Allocation.WrkAllocateDataset Base 

--inner join Allocation.WrkAllocateDataset Duplicate
--on Base.NHSNumber = Duplicate.NHSNumber
--and	Base.AdmissionDate = Duplicate.AdmissionDate
--and Base.GlobalProviderSpellNo <> Duplicate.GlobalProviderSpellNo
--and Base.StartSiteCode <> Duplicate.StartSiteCode
--and	Base.AdmissionDate >='20140401'

inner join APC.DuplicateEpisodeValidation
--on Base.DatasetRecno = DuplicateEpisodeValidation.MergeEncounterRecno
on Base.NonReportableSourceUniqueID = DuplicateEpisodeValidation.SourceUniqueID

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = Base.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.APCExclusionByDuplicateAdmission'

where ValidationOutcome = 2


--select * from APC.DuplicateEpisodeValidationOutcome
--Reportable	ReportableDescription
--1	Reportable
--2	NonReportable
--3	ToBeValidated










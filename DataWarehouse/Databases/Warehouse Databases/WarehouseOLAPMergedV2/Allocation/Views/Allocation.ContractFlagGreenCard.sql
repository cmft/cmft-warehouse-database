﻿
CREATE view [Allocation].[ContractFlagGreenCard]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Priority =
		coalesce(
			 Allocation.Priority
			,Template.Priority
		)
from
	Allocation.WrkAllocateDataset

inner join Allocation.TemplateDataset
on	TemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	TemplateDataset.Active = 1
and	TemplateDataset.Template = 'Allocation.ContractFlagGreenCard'

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template

inner join Allocation.Allocation
on	Allocation.SourceAllocationID = 'GREENCARD'
and	Allocation.AllocationTypeID = 9

inner join OP.AppointmentType
on	AppointmentType.SourceAppointmentTypeCode = WrkAllocateDataset.SourceAppointmentTypeCode

where
	AppointmentType.SourceAppointmentType like '%green card%'
and	WrkAllocateDataset.SourceContextCode = 'TRA||UG'




﻿






CREATE view [Allocation].[ResultExclusionPreCutOffDate]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ResultExclusionPreCutOffDate'	
and AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

where
	AllocationTemplateDataset.CutOffTime > WrkAllocateDataset.ResultTime





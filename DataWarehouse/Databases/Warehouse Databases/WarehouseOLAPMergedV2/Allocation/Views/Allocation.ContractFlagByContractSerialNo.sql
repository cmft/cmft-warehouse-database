﻿
CREATE view [Allocation].[ContractFlagByContractSerialNo]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

where
	AllocationTemplateDataset.Template = 'Allocation.ContractFlagByContractSerialNo'
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

and	WrkAllocateDataset.ContractSerialNo like

	case

	when left(AllocationTemplateDataset.ContractSerialNo, 1) = '%'
	then '%' + right(AllocationTemplateDataset.ContractSerialNo, LEN(AllocationTemplateDataset.ContractSerialNo) -1)

	when right(AllocationTemplateDataset.ContractSerialNo, 1) = '%'
	then left(AllocationTemplateDataset.ContractSerialNo, LEN(AllocationTemplateDataset.ContractSerialNo) -1) + '%'

	else AllocationTemplateDataset.ContractSerialNo
	end




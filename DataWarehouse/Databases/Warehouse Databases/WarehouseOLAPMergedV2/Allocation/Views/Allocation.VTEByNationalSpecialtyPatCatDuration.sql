﻿CREATE view Allocation.VTEByNationalSpecialtyPatCatDuration
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.VTEByNationalSpecialtyPatCatDuration'

where
	WrkAllocateDataset.NationalSpecialtyCode = AllocationTemplateDataset.NationalSpecialtyCode
and	(
		WrkAllocateDataset.PatientCategoryCode = AllocationTemplateDataset.PatientCategoryCode
	or	datediff(
			 minute
			,WrkAllocateDataset.DatasetStartDate
			,WrkAllocateDataset.DatasetEndDate
		) < AllocationTemplateDataset.Duration
	)

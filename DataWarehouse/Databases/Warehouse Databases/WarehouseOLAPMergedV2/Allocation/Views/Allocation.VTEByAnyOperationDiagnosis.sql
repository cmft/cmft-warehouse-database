﻿CREATE view Allocation.VTEByAnyOperationDiagnosis
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.VTEByAnyOperationDiagnosis'

where
	exists
	(
	select
		1
	from
		Allocation.WrkAllocateDatasetOperation
	where
		WrkAllocateDatasetOperation.DatasetRecno = WrkAllocateDataset.DatasetRecno
	and	WrkAllocateDatasetOperation.DatasetCode = WrkAllocateDataset.DatasetCode
	and	WrkAllocateDatasetOperation.OperationCode = AllocationTemplateDataset.ProcedureCode
	)

and	exists
	(
	select
		1
	from
		Allocation.WrkAllocateDatasetDiagnosis
	where
		WrkAllocateDatasetDiagnosis.DatasetRecno = WrkAllocateDataset.DatasetRecno
	and	WrkAllocateDatasetDiagnosis.DatasetCode = WrkAllocateDataset.DatasetCode
	and	WrkAllocateDatasetDiagnosis.DiagnosisCode = AllocationTemplateDataset.DiagnosisCode
	)

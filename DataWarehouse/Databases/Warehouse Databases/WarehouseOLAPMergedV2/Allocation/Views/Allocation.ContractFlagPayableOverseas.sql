﻿CREATE view [Allocation].ContractFlagPayableOverseas
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Priority =
		coalesce(
			 Template.Priority
			,Allocation.Priority
		)
from
	Allocation.WrkAllocateDataset

inner join Allocation.TemplateDataset
on	TemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	TemplateDataset.Active = 1
and	TemplateDataset.Template = 'Allocation.ContractFlagPayableOverseas'

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template

inner join Allocation.Allocation
on	Allocation.SourceAllocationID = 'OSVP'
and	Allocation.AllocationTypeID = 9

where
	left(coalesce(WrkAllocateDataset.PseudoPostcode, WrkAllocateDataset.PostcodeAtDischarge), 4) = 'ZZ99'		
and	right(coalesce(WrkAllocateDataset.PseudoPostcode, WrkAllocateDataset.PostcodeAtDischarge), 3) not in		
	(
	--non chargeable
	 '2AZ'
	,'2CZ'
	,'2DZ'
	,'2EZ'
	,'2PZ'
	,'3AZ'
	,'4BZ'
	,'4CZ'
	,'4EZ'
	,'4FZ'
	,'4GZ'
	,'4HZ'
	,'4JZ'
	,'4LZ'
	,'4MZ'
	,'4PZ'
	,'4QZ'
	,'4RZ'
	,'4UZ'
	,'4XZ'
	,'4YZ'
	,'5BZ'
	,'5UZ'
	,'5XZ'
	,'5YZ'
	,'6AZ'
	,'7LZ'
	,'7RZ'
	,'7SZ'
	,'3BZ'
	,'4CZ'
	,'5AZ'
	,'5BZ'
	,'5NZ'
	,'5QZ'
	,'5SZ'
	,'5TZ'
	,'5VZ'
	,'6GZ'
	,'6HZ'
	,'6MZ'
	,'6RZ'
	,'6RZ'
	,'6RZ'
	,'6RZ'
	,'6UZ'
	,'6UZ'
	,'7JZ'
	,'7KZ'
	,'7MZ'
	,'7NZ'
	,'7PZ'
	,'7QZ'
	,'7UZ'
	,'7VZ'
	,'7XZ'
	,'7YZ'
	,'7ZZ'
	,'9TZ'
	,'3WZ'
	,'3CZ'
	,'3VZ'
	)


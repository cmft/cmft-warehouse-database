﻿create view Allocation.APCExclusionBySalfordActivity

as
	
select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority

from 		
	
		
 Allocation.WrkAllocateDataset 	WrkAllocateDataset	

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.APCExclusionBySalfordActivity'
		
where 	
	
	WrkAllocateDataset.SourceContextCode = AllocationTemplateDataset.SourceContextCode
and WrkAllocateDataset.SourceAdminCategoryCode = AllocationTemplateDataset.SourceAdminCategoryCode










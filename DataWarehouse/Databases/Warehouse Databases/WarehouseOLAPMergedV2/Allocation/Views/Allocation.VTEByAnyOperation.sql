﻿
CREATE view [Allocation].[VTEByAnyOperation]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

and	AllocationTemplateDataset.Template = 'Allocation.VTEByAnyOperation'

where
	exists
	(
	select
		1
	from
		Allocation.WrkAllocateDatasetOperation
	where
		WrkAllocateDatasetOperation.DatasetRecno = WrkAllocateDataset.DatasetRecno
	and	WrkAllocateDatasetOperation.DatasetCode = WrkAllocateDataset.DatasetCode
	and	WrkAllocateDatasetOperation.OperationCode = AllocationTemplateDataset.ProcedureCode
	)
and 
	(WrkAllocateDataset.AdmissionTime < AllocationTemplateDataset.ToDate
	or AllocationTemplateDataset.ToDate is null
	)


﻿
CREATE view [Allocation].[ContractHRGByClinicHRG]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractHRGByClinicHRG'

where
	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and	AllocationTemplateDataset.HRGCode = WrkAllocateDataset.HRGCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode


and	WrkAllocateDataset.DatasetStartDate
between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
		and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	) 

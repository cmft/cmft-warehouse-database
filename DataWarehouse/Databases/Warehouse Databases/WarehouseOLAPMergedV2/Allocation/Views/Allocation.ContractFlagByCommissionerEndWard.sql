﻿
CREATE view [Allocation].[ContractFlagByCommissionerEndWard]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1

where
	AllocationTemplateDataset.Template = 'Allocation.ContractFlagByCommissionerEndWard'
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.EndWardCode = WrkAllocateDataset.EndWardCode

and	(
		AllocationTemplateDataset.CommissionerCode = WrkAllocateDataset.CommissionerCode
	or	AllocationTemplateDataset.CommissionerCode is null
	)




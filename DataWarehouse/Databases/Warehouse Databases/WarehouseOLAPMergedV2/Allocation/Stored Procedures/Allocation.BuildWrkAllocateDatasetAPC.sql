﻿



CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPC]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20141112		RR	Added WrkAllocateDatasetOperation & Diagnosis (replicated the process for VTE)
-- 20141112		RR	Added new field - LocalAdministrativeCategoryCode
-- 20141112		RR	Added new field - NationalAdmissionMethodCode
-- 20141112		RR	Added new field - SpecialServiceCode (Specialist Services Best Practice Tariff to identify POD - NELSD - advised by PH)
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'APC'
	
delete
from
	Allocation.WrkAllocateDatasetOperation
where
	DatasetCode = 'APC'

delete
from
	Allocation.WrkAllocateDatasetDiagnosis
where
	DatasetCode = 'APC'


insert into Allocation.WrkAllocateDataset
	(
	DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceSpecialtyCode
	,NationalSpecialtyCode
	,NationalSiteCode
	,SourceContextCode
	,Division
	,AdmissionTypeCode
	,DiagnosisCode
	,ProcedureCode
	,HRGCode
	,NationalAdministrativeCategoryCode
	,ContractSerialNo
	,NationalNeonatalLevelOfCareCode
	,FirstEpisodeInSpellIndicator
	,SourceIntendedManagementCode
	,AdmissionMethodCode
	,StartWardCode
	,EndWardCode
	,CommissionerCode
	,Postcode
	,PseudoPostcode
	,PostcodeAtDischarge
	,EpisodeStartDate
	,DateOfBirth
	,LOS
	
	,DischargeMethodCode
	,DischargeDestinationCode
	,PatientClassificationCode
	,DischargeDate
	,ProviderSpellNo
	,SourceUniqueID
	,AdmissionDate
	,SourceEncounterNo
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,NeonatalWardStay
	,DischargeDivisionCode
	,PatientCategoryCode
	,LastEpisodeInSpellIndicator
	,LocalAdministrativeCategoryCode
	,NationalAdmissionMethodCode
	,EndSiteCode
)
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APC'
	,DatasetStartDate = Encounter.EpisodeStartDate
	,DatasetEndDate = Encounter.EpisodeEndDate
	,SpecialtyCode = left(Specialty.SourceSpecialtyCode, 10)
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
	,SiteCode = left(Site.NationalSiteCode, 10)
	,ContextCode = Encounter.ContextCode
	,Division = Directorate.Division
	,AdmissionMethod.AdmissionTypeCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryProcedureCode

	,HRGCode =
		(
		select
			 HRG4Spell.HRGCode
		from
			APC.HRG4Spell
		
		inner join APC.BaseEncounter Spell
		on	Spell.MergeEncounterRecno = HRG4Spell.MergeEncounterRecno

		where
			Spell.ProviderSpellNo = Encounter.GlobalProviderSpellNo
		)

	,NationalAdministrativeCategoryCode = AdministrativeCategory.NationalAdministrativeCategoryCode
	,Encounter.ContractSerialNo
	,NationalNeonatalLevelOfCareCode = NeonatalLevelOfCare.NationalNeonatalLevelOfCareCode
	,FirstEpisodeInSpellIndicator = CAST(case when Admission.MergeEncounterRecno = Encounter.MergeEncounterRecno then 1 else 0 end as bit)
	,Admission.ManagementIntentionCode
	,Admission.AdmissionMethodCode
	,Encounter.StartWardTypeCode -- Admission.StartWardTypeCode
	,Encounter.EndWardTypeCode -- Discharge.EndWardTypeCode
	,Encounter.CCGCode
	,Encounter.Postcode
	,Encounter.PseudoPostcode
	,Encounter.PostcodeAtDischarge
	,Encounter.EpisodeStartDate
	,Encounter.DateOfBirth
	,Encounter.LOS
	
	,Discharge.DischargeMethodCode
	,Discharge.DischargeDestinationCode
	,Encounter.PatientClassificationCode
	,Discharge.DischargeDate
	,Encounter.ProviderSpellNo	
	,Encounter.SourceUniqueID	
	,Admission.AdmissionDate	
	,Encounter.SourceEncounterNo	
	,Encounter.GlobalProviderSpellNo	
	,Encounter.GlobalEpisodeNo
	,NeonatalWardStay =
			case
				when exists				
					(select
							1
					from
						APC.BaseWardStay
					where
						Encounter.ProviderSpellNo = BaseWardStay.ProviderSpellNo
					and	BaseWardStay.WardCode = '68'
					)
				then 1
			else 0
			end
			
	,DischargeDivisionCode = DischargeDirectorate.DivisionCode
	,Encounter.PatientCategoryCode
	,LastEpisodeInSpellIndicator = 
			CAST(
				case 
					when Discharge.MergeEncounterRecno = Encounter.MergeEncounterRecno then 1 
					else 0 
				end 
			as bit)
	,LocalAdministrativeCategoryCode = Encounter.LocalAdminCategoryCode 
	,NationalAdmissionMethodCode = AdmissionMethod.NationalAdmissionMethodCode
	,Encounter.EndSiteCode

from
	APC.BaseEncounter Encounter

inner join APC.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

inner join APC.BaseEncounterReference AdmissionReference
on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

left join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

left outer join WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

left outer join APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = AdmissionReference.AdmissionMethodID

--left outer join WH.Site
--on	Site.SourceSiteID = AdmissionReference.StartSiteID

left outer join WH.Site
on	Site.SourceSiteID = Reference.StartSiteID

left outer join WH.Directorate
on	Directorate.DirectorateCode = coalesce(Admission.StartDirectorateCode, 'N/A')

left outer join WH.AdministrativeCategory 
on	AdministrativeCategory.SourceAdministrativeCategoryID = Reference.AdminCategoryID

left outer join APC.NeonatalLevelOfCare
on	NeonatalLevelOfCare.SourceNeonatalLevelOfCareID = Reference.NeonatalLevelOfCareID

left outer join APC.IntendedManagement
on	IntendedManagement.SourceIntendedManagementID = Reference.IntendedManagementID

left outer join WH.Directorate DischargeDirectorate
on	DischargeDirectorate.DirectorateCode = coalesce(Discharge.EndDirectorateCode, 'N/A')

where
	@ProcessAll = 1

or	exists
	(
	select
		1
	from
		APC.ProcessList
	where
		ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	and	@ProcessAll = 0
	)



	
--now generate the list of procedures....
insert into Allocation.WrkAllocateDatasetOperation
	(
	 DatasetRecno
	,DatasetCode
	,SequenceNo
	,OperationCode
	)
select
	 DatasetRecno = BaseOperation.MergeEncounterRecno
	,DatasetCode = 'APC'
	,BaseOperation.SequenceNo
	,BaseOperation.OperationCode
from
	APC.BaseOperation

inner join Allocation.WrkAllocateDataset
on	WrkAllocateDataset.DatasetRecno = BaseOperation.MergeEncounterRecno
and	WrkAllocateDataset.DatasetCode = 'APC'


--now generate the list of diagnoses....
insert into Allocation.WrkAllocateDatasetDiagnosis
	(
	 DatasetRecno
	,DatasetCode
	,SequenceNo
	,DiagnosisCode
	)
select
	 DatasetRecno = BaseDiagnosis.MergeEncounterRecno
	,DatasetCode = 'APC'
	,BaseDiagnosis.SequenceNo
	,BaseDiagnosis.DiagnosisCode
from
	APC.BaseDiagnosis

inner join Allocation.WrkAllocateDataset
on	WrkAllocateDataset.DatasetRecno = BaseDiagnosis.MergeEncounterRecno
and	WrkAllocateDataset.DatasetCode = 'APC'












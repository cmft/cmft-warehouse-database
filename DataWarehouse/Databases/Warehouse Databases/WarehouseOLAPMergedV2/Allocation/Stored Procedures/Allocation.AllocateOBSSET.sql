﻿

CREATE procedure [Allocation].[AllocateOBSSET]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetObservationSet

-- Execute allocation
exec Allocation.ExecuteAllocation 'OBSSET' , 10 --Directorate


update
	Observation.BaseObservationSet
set
	DirectorateCode = Allocation.SourceAllocationID
from
	Observation.BaseObservationSet

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseObservationSet.MergeObservationSetRecno
and	DatasetAllocation.DatasetCode = 'OBSSET'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseObservationSet.DirectorateCode != Allocation.SourceAllocationID
or	BaseObservationSet.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



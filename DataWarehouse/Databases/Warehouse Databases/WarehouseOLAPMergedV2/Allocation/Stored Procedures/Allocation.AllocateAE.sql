﻿

CREATE procedure [Allocation].[AllocateAE]
	@ProcessAll bit = 0
as

-- 20141114 RR added step to update A&E first, FU (AllocationTypeID = 5 - Contract POD)

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)




exec Allocation.BuildWrkAllocateDatasetAE @ProcessAll
exec Allocation.ExecuteAllocation 'AE' , 12 --'AE Exclusion'


update
	BaseEncounter
set
	Reportable = 0

from
	AE.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'AE'
and	DatasetAllocation.AllocationTypeID = 12 --'AE Exclusion'

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	coalesce(BaseEncounter.Reportable, 1) = 1 -- will overwrite existing positive value or null (if exists) in reportable column
and
	Allocation.SourceAllocationID = 'AEDUPLICATE'
	
	
exec Allocation.ExecuteAllocation 'AE' , 15

exec Allocation.ExecuteAllocation 'AE' , 10

-- 20141114 RR added step to update A&E first, FU
exec Allocation.ExecuteAllocation 'AE' , 5 --Contract POD
-- 20150507 RR added step to update PCEC/WIC as per request from Anthony Smith/Phil Huitson.
exec Allocation.ExecuteAllocation 'AE' , 6 --Contract HRG

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetQCR] as


delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'QCR'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DepartmentCode
	,SourceContextCode
) 
select
	 DatasetRecno = BaseAuditAnswer.AuditAnswerRecno
	,DatasetCode = 'QCR'
	,DatasetStartDate = BaseAuditAnswer.AuditDate
	,DepartmentCode = LocationCode 
	,SourceContextCode = BaseAuditAnswer.ContextCode

from
	QCR.BaseAuditAnswer BaseAuditAnswer

where
      BaseAuditAnswer.ContextCode = 'CEN||QCR'
      
      
﻿




CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPCExclusion]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

-- 20141127	RR	added WardCode


delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'APC'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceUniqueID
	,NHSNumber
	,SourceSpecialtyCode
	,ProviderSpellNo
	,SourceEncounterNo
	,FirstEpisodeInSpellIndicator
	,StartSiteCode
	,AdmissionDate 
	,AdmissionTime
	,AdmissionMethodCode
	,PatientCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryProcedureCode
	,IntendedPrimaryProcedureCode
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,StartWardCode
	,SourceContextCode
	,SourceAdminCategoryCode
)
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APC'
	,EpisodeStartDate
	,SourceUniqueID
	,NHSNumber
	,SourceSpecialtyCode = SpecialtyCode
	,ProviderSpellNo
	,SourceEncounterNo
	,FirstEpisodeInSpellIndicator
	,StartSiteCode
	,AdmissionDate 
	,AdmissionTime
	,AdmissionMethodCode
	,PatientCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryProcedureCode
	,IntendedPrimaryProcedureCode
	,GlobalProviderSpellNo
	,GlobalEpisodeNo	
	,StartWardTypeCode
	,SourceContextCode = ContextCode
	,SourceAdminCategoryCode = AdminCategoryCode	
from
	APC.BaseEncounter Encounter

﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetPEX] as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'PEX'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,LocationCode
	,SourceContextCode
) 
select
	 DatasetRecno = BaseResponse.MergeResponseRecno
	,DatasetCode = 'PEX'
	,BaseResponse.ResponseDate
	,LocationCode = cast(BaseResponse.LocationID as varchar(100))
	,SourceContextCode = BaseResponse.ContextCode

from
	PEX.BaseResponse 
where
      ContextCode in ('CMFT||ENVOY','CMFT||HOSP','CMFT||CRT')

	









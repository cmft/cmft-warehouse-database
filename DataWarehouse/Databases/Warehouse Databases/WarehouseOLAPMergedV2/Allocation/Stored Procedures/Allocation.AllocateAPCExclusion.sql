﻿

CREATE proc [Allocation].[AllocateAPCExclusion]

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)



exec Allocation.BuildWrkAllocateDatasetAPCExclusion

-- Create global temp table 
-- The join below was being referenced in the views, as the number of views have grown the queries have become very expensive, so updated to a temp table to reduce the expense(??)

Truncate Table	Allocation.APCExclusionDuplicateAdmission
Insert into	Allocation.APCExclusionDuplicateAdmission

Select
	 DatasetCode = 'APC'
	,ReportableDatasetRecno = Reportable.DatasetRecno
	,ReportableSourceUniqueID = Reportable.SourceUniqueID
	,ReportableProviderSpellNo = Reportable.ProviderSpellNo
	,ReportableSourceSiteCode = Reportable.StartSiteCode
	,ReportableSourceSpecialtyCode = Reportable.SourceSpecialtyCode
	,ReportableSourceWardCode = Reportable.StartWardCode
	,NonReportableDatasetRecno = NonReportable.DatasetRecno
	,NonReportableSourceUniqueID = NonReportable.SourceUniqueID
	,NonReportableProviderSpellNo = NonReportable.ProviderSpellNo
	,NonReportableSourceSiteCode = NonReportable.StartSiteCode
	,NonReportableSourceSpecialtyCode = NonReportable.SourceSpecialtyCode
	,NonReportableSourceWardCode = NonReportable.StartWardCode
from 		
	Allocation.WrkAllocateDataset Reportable 

inner join Allocation.WrkAllocateDataset NonReportable
on Reportable.NHSNumber = NonReportable.NHSNumber
and	Reportable.AdmissionDate = NonReportable.AdmissionDate
and Reportable.GlobalProviderSpellNo <> NonReportable.GlobalProviderSpellNo
and Reportable.StartSiteCode <> NonReportable.StartSiteCode
and	Reportable.AdmissionDate >='20140401'

where	
	Reportable.AdmissionMethodCode in ('WL','BL','PL','11-WL','12-BL','13-PL')
and NonReportable.AdmissionMethodCode in ('WL','BL','PL','11-WL','12-BL','13-PL')



exec Allocation.ExecuteAllocation 'APC' , 13 --Activity Broker
											-- Duplicates


-- Reset Reportable flag to account for any amendments made on the activity broker

update
	APC.BaseEncounter
set
	Reportable = 1

update
	APC.BaseEncounter
set
	Reportable = 0
from
	APC.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'APC'
and	DatasetAllocation.AllocationTypeID = 13

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID
and	Allocation.SourceAllocationID in ('APCDUPLICATE','APCDQDUPLICATE','APCDUPLICATEDQRULE','SalfordActivity')

exec [APC].[BuildDuplicateEpisodeValidation] 




select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


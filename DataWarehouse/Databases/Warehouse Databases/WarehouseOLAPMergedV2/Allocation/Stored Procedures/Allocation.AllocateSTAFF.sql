﻿

CREATE procedure [Allocation].[AllocateSTAFF]
as



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetSTAFF

-- Execute allocation
exec Allocation.ExecuteAllocation 'STAFF' , 10 --Directorate


update
	APC.BaseStaffingLevel
set
	DirectorateCode = Allocation.SourceAllocationID

from
	APC.BaseStaffingLevel

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseStaffingLevel.StaffingLevelRecno
and	DatasetAllocation.DatasetCode = 'STAFF'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseStaffingLevel.DirectorateCode != Allocation.SourceAllocationID
or	BaseStaffingLevel.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



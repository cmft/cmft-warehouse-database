﻿
CREATE procedure [Allocation].[AllocateINC]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetINC

-- Execute allocation
exec Allocation.ExecuteAllocation 'INC' , 10 --Directorate


update
	Incident.BaseIncident
set
	DirectorateCode = Allocation.SourceAllocationID
from
	Incident.BaseIncident

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseIncident.MergeIncidentRecno
and	DatasetAllocation.DatasetCode = 'INC'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseIncident.DirectorateCode != Allocation.SourceAllocationID
or	BaseIncident.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


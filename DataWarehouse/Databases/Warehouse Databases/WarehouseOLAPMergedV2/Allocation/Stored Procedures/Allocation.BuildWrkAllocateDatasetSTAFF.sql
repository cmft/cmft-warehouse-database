﻿
CREATE procedure [Allocation].[BuildWrkAllocateDatasetSTAFF] as


delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'STAFF'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,Division
	,SourceContextCode
) 
select
	 DatasetRecno = BaseStaffingLevel.StaffingLevelRecno
	,DatasetCode = 'STAFF'
	,BaseStaffingLevel.CensusDate
	,Division = DivisionCode
	,SourceContextCode = BaseStaffingLevel.ContextCode

from
	APC.BaseStaffingLevel

where
      BaseStaffingLevel.ContextCode = 'CMFT||IPATH' 
      


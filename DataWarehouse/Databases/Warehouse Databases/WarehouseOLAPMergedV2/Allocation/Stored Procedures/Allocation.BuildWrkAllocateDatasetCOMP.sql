﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetCOMP] as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'COMP'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DepartmentCode
	,SourceContextCode
) 
select
	 DatasetRecno = Complaint.MergeComplaintRecno
	,DatasetCode = 'COMP'
	,Complaint.ReceiptDate
	,DepartmentCode = Complaint.DepartmentCode 
	,SourceContextCode = Complaint.ContextCode

from
	Complaint.BaseComplaint Complaint
where
      ContextCode = 'CMFT||ULYSS'
	









﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetRFDirectorate]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'RF'


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode

	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'RF'

	,SourceSiteCode = Encounter.SiteCode
	,SourceSpecialtyCode = Encounter.SpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.ReferralDate
	,SourceContextCode = Encounter.ContextCode
from
	RF.BaseEncounter Encounter

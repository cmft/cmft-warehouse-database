﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetRESULT] as


delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'RESULT'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,StatusCode
	,ResultTime
	,SourceContextCode
	,ResultSetSourceUniqueID
) 
select
	 DatasetRecno = BaseResult.MergeResultRecno
	,DatasetCode = 'RESULT'
	,BaseResult.ResultSetStatusCode	
	,BaseResult.ResultTime
	,SourceContextCode = BaseResult.ContextCode
	,BaseResult.ResultSetSourceUniqueID

from
	Result.BaseResult

inner join (
			select
				ResultSet.ResultSetSourceUniqueID
				,ResultSet.ContextCode
			from 
				Result.BaseResult ResultSet

			inner join Result.ProcessList
			on ResultSet.MergeResultRecno = ProcessList.MergeRecno
			
			group by
				ResultSet.ResultSetSourceUniqueID
				,ResultSet.ContextCode
			
			) ProcessListResultSet

on	ProcessListResultSet.ResultSetSourceUniqueID = BaseResult.ResultSetSourceUniqueID
and ProcessListResultSet.ContextCode = BaseResult.ContextCode






	









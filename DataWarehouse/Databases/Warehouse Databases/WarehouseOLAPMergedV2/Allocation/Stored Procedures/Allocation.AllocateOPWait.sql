﻿CREATE procedure [Allocation].[AllocateOPWait]

@CensusDate date = null

--with execute as 'cmmc\dan.forster'


as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

delete from Allocation.DatasetAllocation
where
	DatasetCode = 'OPWAIT'
and	not exists
	(
	select
		1
	from
		OPWL.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = DatasetAllocation.DatasetRecno
	)


exec Allocation.BuildWrkAllocateDatasetOPWaitDirectorate @CensusDate
exec Allocation.ExecuteAllocation 'OPWAIT' , 10 --Directorate

update
	OPWL.BaseEncounter
set
	DirectorateCode = Allocation.SourceAllocationID
from
	OPWL.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'OPWAIT'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseEncounter.DirectorateCode != Allocation.SourceAllocationID
or	BaseEncounter.DirectorateCode is null


exec Allocation.BuildWrkAllocateDatasetOPWait @CensusDate
exec Allocation.ExecuteAllocation 'OPWAIT' , 2 --Service
exec Allocation.ExecuteAllocation 'OPWAIT' , 1 --DiagnostcNationalExam


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿





CREATE procedure [Allocation].[BuildWrkAllocateDatasetCOMR]
as

----------------------------------------------------------------------------
---- Copyright Gecko Technologies Ltd. 2003    --
----------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'COMR'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceSpecialtyCode
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'COMR'
	,DatasetStartDate = Encounter.ReferralReceivedDate
	,DatasetEndDate = null
	,SpecialtyCode = SpecialtyCode
	,ContextCode = Encounter.ContextCode

from
	COM.BaseReferral Encounter

inner join COM.BaseReferralReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left outer join COM.Specialty Specialty
on	Specialty.SourceValueID = Reference.ReferredToSpecialtyID










﻿CREATE procedure [Allocation].[AllocateAPC]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20140716 RR added allocation 13 APC Reportable
-- 20140916	PDO	Introduced ProcessList
-- 20141010 RR added allocation 14 Discharge Letter Not applicable
-- 20141202 CH added FFT Exclusion Type 15 

--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

--Start Directorate
exec Allocation.BuildWrkAllocateDatasetAPCStartDirectorate @ProcessAll
exec Allocation.ExecuteAllocation 'APC' , 10 --Directorate

update
	APC.BaseEncounter
set
	StartDirectorateCode = Allocation.SourceAllocationID
from
	APC.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'APC'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

--inner join APC.ProcessList
--on	ProcessList.MergeRecno = BaseEncounter.MergeEncounterRecno

where
	BaseEncounter.StartDirectorateCode != Allocation.SourceAllocationID
or	BaseEncounter.StartDirectorateCode is null


--End Directorate
exec Allocation.BuildWrkAllocateDatasetAPCEndDirectorate @ProcessAll
exec Allocation.ExecuteAllocation 'APC' , 10 --Directorate

update
	APC.BaseEncounter
set
	EndDirectorateCode = Allocation.SourceAllocationID
from
	APC.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'APC'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

--inner join APC.ProcessList
--on	ProcessList.MergeRecno = BaseEncounter.MergeEncounterRecno

where
	BaseEncounter.EndDirectorateCode != Allocation.SourceAllocationID
or	BaseEncounter.EndDirectorateCode is null



exec Allocation.BuildWrkAllocateDatasetAPC @ProcessAll
exec Allocation.ExecuteAllocation 'APC' , 2 --'Service'
exec Allocation.ExecuteAllocation 'APC' , 1 --DiagnosticNationalExam

exec Allocation.ExecuteAllocation 'APC' , 5 --Contract POD
exec Allocation.ExecuteAllocation 'APC' , 6 --Contract HRG
exec Allocation.ExecuteAllocation 'APC' , 9 --Contract Flag


--20141010 RR added
exec Allocation.ExecuteAllocation 'APC' , 14 --Discharge Letter Exclusion
-- Need additional field in APC.BaseEncounter to update with results from this.
-- select * from Allocation.Allocation where left(Allocation,22) = 'DischargeLetterExclude'
--select AllocationID,Priority,count(*) 
--from Allocation.WrkAllocation 
--group by AllocationID,Priority
-- Highest volume AllocationID = 2088749 -- Regular daycases


exec Allocation.ExecuteAllocation 'APC' , 11 --Purchaser Code

update
	APC.BaseEncounter
set
	PurchaserCode = Allocation.SourceAllocationID

from
	APC.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'APC'
and	DatasetAllocation.AllocationTypeID = 11

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

--inner join APC.ProcessList
--on	ProcessList.MergeRecno = BaseEncounter.MergeEncounterRecno

where
	BaseEncounter.PurchaserCode != Allocation.SourceAllocationID


exec Allocation.ExecuteAllocation 'APC' , 15 --FFT Ineligible Patients

	
exec Allocation.BuildWrkAllocateDatasetAPCVTE @ProcessAll
exec Allocation.ExecuteAllocation 'APC' , 3 --'VTE Exclusion'





select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


﻿

CREATE procedure [Allocation].[AllocateFERTILITY]
as



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetFERTILITY

-- Execute allocation

exec Allocation.ExecuteAllocation 'FERTILITY' , 2 --Service
exec Allocation.ExecuteAllocation 'FERTILITY' , 5 --Contract Point of Delivery
exec Allocation.ExecuteAllocation 'FERTILITY' , 6 --Contract HRG
exec Allocation.ExecuteAllocation 'FERTILITY' , 9 --Contract Flag
exec Allocation.ExecuteAllocation 'FERTILITY' , 10 --Directorate


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



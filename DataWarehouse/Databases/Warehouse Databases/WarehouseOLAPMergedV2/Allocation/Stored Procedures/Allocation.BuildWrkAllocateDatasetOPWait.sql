﻿
CREATE procedure [Allocation].[BuildWrkAllocateDatasetOPWait]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'OPWAIT'

select
       @CensusDate = 
              coalesce
                     (@CensusDate,
                           (
                           select
                                  max(BaseEncounter.CensusDate)
                           from
                                  OPWL.BaseEncounter  	       
                           )
                     )


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSpecialtyCode
	,NationalSpecialtyCode
	,SourceClinicCode
	,ProcedureCode
	,SourceContextCode
	,Division
	,SourceConsultantCode
	,SourceSourceOfReferralCode
	,NationalFirstAttendanceCode
	,ConsultantNationalMainSpecialtyCode
	,SourceWaitingListCode
	,NationalSiteCode
	,SourceSiteCode
)
select 
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'OPWAIT'
	,DatasetStartDate = Encounter.AppointmentDate
	,SourceSpecialtyCode = left(Specialty.SourceSpecialtyCode, 10)
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
	,ClinicCode = Encounter.ClinicCode
	,ProcedureCode = Encounter.PrimaryProcedureCode
	,ContextCode = Encounter.ContextCode
	,Division = Directorate.Division
	,Consultant.SourceConsultantCode
	,SourceOfReferral.SourceSourceOfReferralCode
	,FirstAttendance.NationalFirstAttendanceCode
	,ConsultantNationalMainSpecialtyCode = Consultant.MainSpecialtyCode
	,SourceWaitingListCode = WaitingList.SourceWaitingListCode
	,NationalSiteCode = left(Site.NationalSiteCode, 10)
	,SiteCode = left(Site.SourceSiteCode, 10)

from
	OPWL.BaseEncounter Encounter

inner join OPWL.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

left join WH.Directorate
on	Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')

left join WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

left join OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

left join OP.FirstAttendance
on	FirstAttendance.SourceFirstAttendanceID = Reference.FirstAttendanceID

left join OPWL.WaitingList
on	WaitingList.SourceWaitingListID = Reference.WaitingListID

left join WH.Site
on Site.SourceSiteID = Reference.SiteID

where
	Encounter.CensusDate = @CensusDate

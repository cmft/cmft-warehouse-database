﻿
CREATE procedure [Allocation].[AllocateGUM]
as



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetGUM

-- Execute allocation

exec Allocation.ExecuteAllocation 'GUM' , 2 --Service
exec Allocation.ExecuteAllocation 'GUM' , 5 --Contract Point of Delivery
exec Allocation.ExecuteAllocation 'GUM' , 6 --Contract HRG
exec Allocation.ExecuteAllocation 'GUM' , 9 --Contract Flag
exec Allocation.ExecuteAllocation 'GUM' , 10 --Directorate


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


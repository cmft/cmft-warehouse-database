﻿
CREATE procedure [Allocation].[BuildWrkAllocateDatasetAlert] as

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'ALERT'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSpecialtyCode
	,SourceContextCode
	,ActivityDate
) 
select
	 DatasetRecno = Alert.MergeAlertRecno
	,DatasetCode = 'ALERT'
	,DatasetStartDate = Alert.CreatedDate
	,SourceSpecialtyCode = Specialty.SpecialtyID
	,SourceContextCode = Alert.ContextCode
	,ActivityDate = CreatedDate
from
	Observation.BaseAlert Alert

inner join
	[$(Warehouse)].Observation.Specialty		
on	Specialty.SpecialtyID = Alert.SpecialtyID

where
      Alert.ContextCode = 'CEN||PTRACK'

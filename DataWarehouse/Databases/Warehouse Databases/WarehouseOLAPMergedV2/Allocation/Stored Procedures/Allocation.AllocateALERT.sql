﻿
create procedure [Allocation].[AllocateALERT]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetAlert

-- Execute allocation
exec Allocation.ExecuteAllocation 'ALERT' , 10 --Directorate


update
	Observation.BaseAlert
set
	DirectorateCode = Allocation.SourceAllocationID
from
	Observation.BaseAlert

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseAlert.MergeAlertRecno
and	DatasetAllocation.DatasetCode = 'ALERT'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseAlert.DirectorateCode != Allocation.SourceAllocationID
or	BaseAlert.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


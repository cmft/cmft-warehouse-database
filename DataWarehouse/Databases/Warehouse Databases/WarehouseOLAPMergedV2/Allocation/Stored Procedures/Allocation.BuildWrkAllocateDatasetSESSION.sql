﻿




Create procedure [Allocation].[BuildWrkAllocateDatasetSESSION]
	
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20151006	RR	Created
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'SESSION'
	

insert into Allocation.WrkAllocateDataset
	(
	DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,ActivityDate
	,SourceContextCode
	,Theatre
)
select
	 DatasetRecno = Session.MergeRecno
	,DatasetCode = 'SESSION'
	,DatasetStartDate = CAST(StartTime as date)
	,DatasetEndDate = CAST(EndTime as date)
	,ActivityDate = CAST(StartTime as date)
	,SourceContextCode = ContextCode
	,Theatre = OperatingSuite
from
	Theatre.BaseSession Session
	
inner join Theatre.BaseSessionReference Reference
on Session.MergeRecno = Reference.MergeRecno

left join Theatre.Theatre
on Reference.TheatreID = Theatre.SourceTheatreID
	
	

﻿
CREATE procedure [Allocation].[BuildWrkAllocateDatasetObservationSet] as

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'OBSSET'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSpecialtyCode
	,SourceContextCode
	,ActivityDate
) 
select
	 DatasetRecno = Observation.MergeObservationSetRecno
	,DatasetCode = 'OBSSET'
	,DatasetStartDate = Observation.StartDate
	,SourceSpecialtyCode = Specialty.SpecialtyID
	,SourceContextCode = Observation.ContextCode
	,ActivityDate = TakenDate

from
	Observation.BaseObservationSet Observation

inner join
	[$(Warehouse)].Observation.Specialty		
on	Specialty.SpecialtyID = Observation.SpecialtyID

where
      Observation.ContextCode = 'CEN||PTRACK'

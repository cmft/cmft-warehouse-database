﻿create procedure [Allocation].[AllocateRF]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


delete from Allocation.DatasetAllocation
where
	DatasetCode = 'RF'
and	not exists
	(
	select
		1
	from
		RF.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = DatasetAllocation.DatasetRecno
	)


exec Allocation.BuildWrkAllocateDatasetRFDirectorate
exec Allocation.ExecuteAllocation 'RF' , 10 --Directorate

update
	RF.BaseEncounter
set
	DirectorateCode = Allocation.SourceAllocationID
from
	RF.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'RF'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseEncounter.DirectorateCode != Allocation.SourceAllocationID
or	BaseEncounter.DirectorateCode is null



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

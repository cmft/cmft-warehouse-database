﻿
CREATE proc [Allocation].[BuildCOMBaseEncounterAllocation]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table COM.BaseEncounterAllocation

insert into COM.BaseEncounterAllocation

(
	MergeEncounterRecno
	,ServiceID
)

select --top 100
	BaseEncounter.MergeEncounterRecno
	,ServiceID

from
	COM.BaseEncounter

inner join
	
	(
		select
			MergeEncounterRecno = DatasetRecno 
			,ServiceID = AllocationID
		from
			Allocation.DatasetAllocation  
		where
			DatasetCode = 'COM'
		and
			AllocationTypeID = 2 --Service
				
	) ServiceAllocation
	
	on BaseEncounter.MergeEncounterRecno = ServiceAllocation.MergeEncounterRecno


	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

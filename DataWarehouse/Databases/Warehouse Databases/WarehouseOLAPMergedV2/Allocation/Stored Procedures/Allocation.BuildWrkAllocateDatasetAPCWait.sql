﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPCWait]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'APCWAIT'

select
       @CensusDate = 
              coalesce
                     (@CensusDate,
                           (
                           select
                                  max(BaseEncounter.CensusDate)
                           from
                                  APCWL.BaseEncounter 
                           )
                     )

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceSpecialtyCode
	,NationalSpecialtyCode
	,NationalSiteCode
	,SourceContextCode
	,Division
	,AdmissionTypeCode
	,ProcedureCode
	,NationalAdmissionMethodCode
	,SourceIntendedManagementCode
)

select 
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APCWAIT'
	,DatasetStartDate = Encounter.CensusDate
	,DatasetEndDate = null
	,SpecialtyCode = left(Specialty.SourceSpecialtyCode, 10)
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
	,SiteCode = null
	,ContextCode = Encounter.ContextCode
	,Division = Directorate.Division
	,AdmissionMethod.AdmissionTypeCode
	,Encounter.IntendedPrimaryOperationCode
	,NationalAdmissionMethodCode = AdmissionMethod.NationalAdmissionMethodCode
	,SourceIntendedManagementCode = Encounter.ManagementIntentionCode
from
	APCWL.BaseEncounter Encounter

inner join APCWL.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

left join APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = Reference.AdmissionMethodID

left join WH.Directorate
on	Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')

where
	Encounter.CensusDate = @CensusDate


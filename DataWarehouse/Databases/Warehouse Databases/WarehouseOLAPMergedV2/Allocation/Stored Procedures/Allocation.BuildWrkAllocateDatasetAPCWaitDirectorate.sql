﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPCWaitDirectorate]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'APCWAIT'
	
select
       @CensusDate = 
              coalesce
                     (@CensusDate,
                           (
                           select
                                  max(BaseEncounter.CensusDate)
                           from
                                  APCWL.BaseEncounter 
                           )
                     )

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,PatientCategoryCode
	,WardCode
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APCWAIT'
	,Encounter.CensusDate
	,SourceSiteCode = Encounter.SiteCode
	,SourceSpecialtyCode = Encounter.SpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.CensusDate

	,PatientCategoryCode =
		[$(Warehouse)].dbo.f_get_patient_category(
			 AdmissionMethod.InternalCode
			,ManagementIntention.InternalCode
			,Encounter.AddedToWaitingListTime
			,NULL
		)

	,Encounter.WardCode
	,Encounter.ContextCode
from
	APCWL.BaseEncounter Encounter

left join [$(Warehouse)].PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

left join [$(Warehouse)].PAS.ManagementIntention ManagementIntention
on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode

where
	Encounter.CensusDate = @CensusDate

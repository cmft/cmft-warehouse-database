﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPCVTE]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

-- 20141030 RR added AdmissionDate

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'APC'

delete
from
	Allocation.WrkAllocateDatasetOperation
where
	DatasetCode = 'APC'

delete
from
	Allocation.WrkAllocateDatasetDiagnosis
where
	DatasetCode = 'APC'



insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceSpecialtyCode
	,NationalSpecialtyCode
	,NationalSiteCode
	,SourceContextCode
	,Division
	,AdmissionTypeCode
	,ProcedureCode
	,PatientCategoryCode
	,StartWardCode
	,EndWardCode
	,StartSiteCode
	,EndSiteCode
	,StartDirectorateCode
	,AdmissionMethodCode
	,PatientClassificationCode
	,Ambulatory
	,AdmissionTime
	,DischargeTime
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APC'
	,DatasetStartDate = Encounter.EpisodeStartTime
	,DatasetEndDate = Encounter.EpisodeEndTime
	,SpecialtyCode = left(Specialty.SourceSpecialtyCode, 10)
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
	,SiteCode = left(Site.NationalSiteCode, 10)
	,ContextCode = Encounter.ContextCode
	,Division = Directorate.Division
	,AdmissionMethod.AdmissionTypeCode
	,Encounter.PrimaryProcedureCode
	,Encounter.PatientCategoryCode
	,Ward.SourceWardCode
	,EndWard.SourceWardCode
	,Site.SourceSiteCode
	,EndSite.SourceSiteCode
	,StartDirectorateCode = Encounter.StartDirectorateCode
	,AdmissionMethodCode = AdmissionMethod.SourceAdmissionMethodCode
	,PatientClassificationCode = PatientClassification.SourcePatientClassificationID
	,Encounter.Ambulatory
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
from
	APC.BaseEncounter Encounter

inner join APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Encounter.MergeEncounterRecno

left join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

left join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Discharge.MergeEncounterRecno

left join WH.Specialty
on	Specialty.SourceSpecialtyID = BaseEncounterReference.SpecialtyID

left join APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = BaseEncounterReference.AdmissionMethodID

left join WH.Site
on	Site.SourceSiteID = BaseEncounterReference.StartSiteID

left join WH.Directorate
on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

left join WH.Ward
on	Ward.SourceWardID = BaseEncounterReference.StartWardID

left join WH.Ward EndWard
on	EndWard.SourceWardCode = Discharge.EndWardTypeCode
and	EndWard.SourceContextCode = Discharge.ContextCode

left join WH.Site EndSite
on	EndSite.SourceSiteID = DischargeReference.EndSiteID

left join WH.PatientClassification
on	PatientClassification.SourcePatientClassificationID = BaseEncounterReference.PatientClassificationID
and	PatientClassification.SourceContextCode = Encounter.ContextCode

where
	Encounter.GlobalEpisodeNo = 1
and
	(
	@ProcessAll = 1

	or	exists
		(
		select
			1
		from
			APC.ProcessList
		where
			ProcessList.MergeRecno = Encounter.MergeEncounterRecno
		and	@ProcessAll = 0
		)
	)
	


--now generate the list of procedures....
insert into Allocation.WrkAllocateDatasetOperation
	(
	 DatasetRecno
	,DatasetCode
	,SequenceNo
	,OperationCode
	)
select
	 DatasetRecno = BaseOperation.MergeEncounterRecno
	,DatasetCode = 'APC'
	,BaseOperation.SequenceNo
	,BaseOperation.OperationCode
from
	APC.BaseOperation

inner join Allocation.WrkAllocateDataset
on	WrkAllocateDataset.DatasetRecno = BaseOperation.MergeEncounterRecno
and	WrkAllocateDataset.DatasetCode = 'APC'


--now generate the list of diagnoses....
insert into Allocation.WrkAllocateDatasetDiagnosis
	(
	 DatasetRecno
	,DatasetCode
	,SequenceNo
	,DiagnosisCode
	)
select
	 DatasetRecno = BaseDiagnosis.MergeEncounterRecno
	,DatasetCode = 'APC'
	,BaseDiagnosis.SequenceNo
	,BaseDiagnosis.DiagnosisCode
from
	APC.BaseDiagnosis

inner join Allocation.WrkAllocateDataset
on	WrkAllocateDataset.DatasetRecno = BaseDiagnosis.MergeEncounterRecno
and	WrkAllocateDataset.DatasetCode = 'APC'


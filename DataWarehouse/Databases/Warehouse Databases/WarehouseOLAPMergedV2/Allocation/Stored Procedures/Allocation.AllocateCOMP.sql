﻿
CREATE procedure [Allocation].[AllocateCOMP]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetCOMP

-- Execute allocation
exec Allocation.ExecuteAllocation 'COMP' , 10 --Directorate


update
	Complaint.BaseComplaint
set
	DirectorateCode = Allocation.SourceAllocationID
from
	Complaint.BaseComplaint

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseComplaint.MergeComplaintRecno
and	DatasetAllocation.DatasetCode = 'COMP'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseComplaint.DirectorateCode != Allocation.SourceAllocationID
or	BaseComplaint.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


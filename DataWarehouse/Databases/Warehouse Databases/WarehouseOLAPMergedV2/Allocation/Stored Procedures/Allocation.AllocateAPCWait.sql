﻿CREATE procedure [Allocation].[AllocateAPCWait]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

delete from Allocation.DatasetAllocation
where
	DatasetCode = 'APCWAIT'
and	not exists
	(
	select
		1
	from
		APCWL.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = DatasetAllocation.DatasetRecno
	)

exec Allocation.BuildWrkAllocateDatasetAPCWaitDirectorate @CensusDate
exec Allocation.ExecuteAllocation 'APCWAIT' , 10 --Directorate

update
	APCWL.BaseEncounter
set
	DirectorateCode = Allocation.SourceAllocationID
from
	APCWL.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'APCWAIT'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseEncounter.DirectorateCode != Allocation.SourceAllocationID
or	BaseEncounter.DirectorateCode is null


exec Allocation.BuildWrkAllocateDatasetAPCWait @CensusDate
exec Allocation.ExecuteAllocation 'APCWAIT' , 2 --Service
exec Allocation.ExecuteAllocation 'APCWAIT' , 1 --DiagnosticNationalExam
exec Allocation.ExecuteAllocation 'APCWAIT' , 5 --POD 

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

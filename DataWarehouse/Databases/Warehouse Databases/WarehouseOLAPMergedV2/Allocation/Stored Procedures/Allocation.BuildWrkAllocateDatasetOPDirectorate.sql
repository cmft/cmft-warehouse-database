﻿

CREATE procedure [Allocation].[BuildWrkAllocateDatasetOPDirectorate]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'OP'


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,SourceClinicCode
	,SourceContextCode
	,PatientCategoryCode
	,NationalSpecialtyCode
	
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'OP'
	,DatasetStartDate = Encounter.AppointmentDate
	,SourceSiteCode =
		coalesce(
			 Encounter.EpisodicSiteCode
			,Encounter.SiteCode
		)

	,SourceSpecialtyCode = Encounter.ReferralSpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.AppointmentDate
	,SourceClinicCode = Encounter.ClinicCode
	,SourceContextCode = Encounter.ContextCode

	,PatientCategoryCode =
		case
		when FirstAttendance.NationalFirstAttendanceCode = '1'
		then 'NEW'
		else 'REVIEW'
		end
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
from
	OP.BaseEncounter Encounter

inner join OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join OP.FirstAttendance FirstAttendance
on	FirstAttendance.SourceFirstAttendanceCode = Encounter.FirstAttendanceFlag
and Encounter.ContextCode = FirstAttendance.SourceContextCode

left outer join WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.ReferralSpecialtyID

where
	@ProcessAll = 1

or	exists
	(
	select
		1
	from
		OP.ProcessList
	where
		ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	and	@ProcessAll = 0
	)



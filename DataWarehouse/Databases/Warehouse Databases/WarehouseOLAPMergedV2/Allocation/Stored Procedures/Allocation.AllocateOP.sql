﻿CREATE procedure [Allocation].[AllocateOP]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


exec Allocation.BuildWrkAllocateDatasetOPDirectorate @ProcessAll
exec Allocation.ExecuteAllocation 'OP' , 10 --Directorate

update
	OP.BaseEncounter
set
	DirectorateCode = Allocation.SourceAllocationID
from
	OP.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'OP'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseEncounter.DirectorateCode != Allocation.SourceAllocationID
or	BaseEncounter.DirectorateCode is null


exec Allocation.BuildWrkAllocateDatasetOP @ProcessAll

exec Allocation.ExecuteAllocation 'OP' , 2 --Service
exec Allocation.ExecuteAllocation 'OP' , 1 --DiagnosticNationalExam



exec Allocation.ExecuteAllocation 'OP' , 5 --Contract POD
exec Allocation.ExecuteAllocation 'OP' , 6 --Contract HRG
exec Allocation.ExecuteAllocation 'OP' , 9 --Contract Flag

exec Allocation.ExecuteAllocation 'OP' , 11 --Purchaser Code


update
	OP.BaseEncounter
set
	PurchaserCode = Allocation.SourceAllocationID

from
	OP.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'OP'
and	DatasetAllocation.AllocationTypeID = 11

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseEncounter.PurchaserCode != Allocation.SourceAllocationID


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿




Create procedure [Allocation].[BuildWrkAllocateDatasetOPERATION]
	
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20151006	RR	Created
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'OPERATION'
	

insert into Allocation.WrkAllocateDataset
	(
	DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,ActivityDate
	,SourceContextCode
	,Theatre
)
select
	 DatasetRecno = OperationDetail.MergeRecno
	,DatasetCode = 'OPERATION'
	,DatasetStartDate = OperationDate
	,DatasetEndDate = OperationDate
	,ActivityDate = OperationDate
	,SourceContextCode = ContextCode
	,Theatre = OperatingSuite
	

from
	Theatre.BaseOperationDetail OperationDetail
	
inner join Theatre.BaseOperationDetailReference Reference
on OperationDetail.MergeRecno = Reference.MergeRecno

left join Theatre.Theatre
on Reference.TheatreID = Theatre.SourceTheatreID
	
	

﻿
CREATE procedure [Allocation].[BuildWrkAllocateDatasetAE]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'AE'


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceContextCode
	,StartSiteCode
	,EndSiteCode
	,SourceSiteCode
	,NationalAttendanceDisposalCode
	,SourceAttendanceDisposalCode
	,NHSNumber
	,DepartureTime
	,ArrivalTime
	,ArrivalTimeAdjusted
	,DateOfBirth
	,DepartmentTypeCode
	,NationalAttendanceCategoryCode
	
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'AE'
	,DatasetStartDate = Encounter.ArrivalDate
	,DatasetEndDate = null
	,ContextCode = Encounter.ContextCode
	,StartSiteCode = Encounter.SiteCode
	,EndSiteCode = Encounter.SiteCode
	,SourceSiteCode = SiteCode
	,AttendanceDisposal.NationalAttendanceDisposalCode
	,AttendanceDisposal.SourceAttendanceDisposalCode
	,NHSNumber
	,DepartureTime
	,ArrivalTime
	,ArrivalTimeAdjusted
	,DateOfBirth
	,DepartmentTypeCode
	,NationalAttendanceCategoryCode

from
	AE.BaseEncounter Encounter

inner join AE.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join AE.AttendanceDisposal		
on	AttendanceDisposal.SourceAttendanceDisposalID = Reference.AttendanceDisposalID

inner join AE.AttendanceCategory		
on	AttendanceCategory.SourceAttendanceCategoryID = Reference.AttendanceCategoryID

where
	@ProcessAll = 1

or	exists
	(
	select
		1
	from
		AE.ProcessList
	where
		ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	and	ProcessList.Dataset = 'Encounter'
	and	@ProcessAll = 0
	)








﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPCEndDirectorate]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

--	20150120	RR	Added join to ORMIS to identify Surgery activity held at Trafford.
--					A link between APC and ORMIS has been found for 85% of Operations (using Apr to Sep 2014)
--					APC Merge Recno is a primary key, an episode can have multiple operations so decision made to bring through the first operation in the episode.  (Still 12 dups out of 1million, relates to DQ issues on ORMIS - duplicates on system - therefore added sequence no to bridging table to bring through the first.)


delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'APC'


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,WardCode
	,PatientCategoryCode
	,HRGCode
	,SourceContextCode
	
	,OperationDetailMergeRecno 
	,OperationDetailSourceUniqueID 
	,Theatre 
	,SessionPeriod
	,TheatreSurgeonCode 
	,TheatreConsultantCode 
	,OperationDayOfWeek 
	,AdmissionTypeCode 

) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APC'
	,DatasetStartDate = Encounter.EpisodeEndDate 
	,Encounter.GlobalProviderSpellNo
	,Encounter.GlobalEpisodeNo
	,SourceSiteCode = Encounter.EndSiteCode -- Discharge.EndSiteCode
	,SourceSpecialtyCode = Encounter.SpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.EpisodeEndDate
	,WardCode = Encounter.EndWardTypeCode -- Discharge.EndWardTypeCode
	,Encounter.PatientCategoryCode

	,HRGCode =
		(
		select
			 HRG4Spell.HRGCode
		from
			APC.HRG4Spell

		inner join APC.BaseEncounter Spell
		on	Spell.MergeEncounterRecno = HRG4Spell.MergeEncounterRecno

		where
			Spell.ProviderSpellNo = Encounter.GlobalProviderSpellNo
		)

	,Encounter.ContextCode
	
	,OperationDetailMergeRecno = Operation.MergeRecno
	,OperationDetailSourceUniqueID = Operation.SourceUniqueID 
	,Theatre = Theatre.OperatingSuite
	,SessionPeriod = SessionPeriod.SessionPeriod
	,TheatreSurgeonCode = StaffSurgeon.SourceStaffCode
	,TheatreConsultantCode = StaffConsultant.SourceStaffCode
	,OperationDayOfWeek = datename(dw,OperationDate)
	,AdmissionTypeCode  = LocalAdmissionTypeCode

from
	APC.BaseEncounter Encounter

inner join APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

-- Decision made approx Nov 2014 to pull End Directorate at episode level, therefore no reason to link to Spell and Discharge, but added criteria for Episode End Date instead.
--inner join APC.Spell
--on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

--left join APC.BaseEncounter Discharge
--on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

left join APC.BaseEncounterOperationDetail Bridge
on Encounter.MergeEncounterRecno = Bridge.MergeEncounterRecno
and SequenceNo = 1

left join Theatre.BaseOperationDetail Operation
on Bridge.MergeOperationDetailRecno = Operation.MergeRecno

left join Theatre.BaseOperationDetailReference
on	BaseOperationDetailReference.MergeRecno = Operation.MergeRecno

left join Theatre.Staff StaffSurgeon
on BaseOperationDetailReference.SurgeonID = StaffSurgeon.SourceStaffID

left join Theatre.Staff StaffConsultant
on BaseOperationDetailReference.ConsultantID = StaffConsultant.SourceStaffID

left join Theatre.BaseSession
on	BaseSession.SourceUniqueID = Operation.SessionSourceUniqueID
and	BaseSession.ContextCode = Operation.ContextCode

left join Theatre.SessionPeriod
on	SessionPeriod.SessionPeriodID =
	case
	when
		left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29'
	and	left(convert(varchar, BaseSession.EndTime, 8), 5) > '16:29' then 1 --All Day

	when left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29' then 2 --Morning
	when left(convert(varchar, BaseSession.StartTime, 8), 5) between '12:30' and '16:29' then 3 --Afternoon
	when left(convert(varchar, BaseSession.StartTime, 8), 5) between '16:30' and '23:59' then 4 --Evening
	else -1
	end

left join Theatre.Theatre
on BaseOperationDetailReference.TheatreID = Theatre.SourceTheatreID

left join Theatre.AdmissionType
on BaseOperationDetailReference.AdmissionTypeID = AdmissionType.SourceAdmissionTypeID

where
	Encounter.EpisodeEndDate is not null
	
and	(
	@ProcessAll = 1

	or	exists
		(
		select
			1
		from
			APC.ProcessList
		where
			ProcessList.MergeRecno = Encounter.MergeEncounterRecno
		and	@ProcessAll = 0
		)
	)

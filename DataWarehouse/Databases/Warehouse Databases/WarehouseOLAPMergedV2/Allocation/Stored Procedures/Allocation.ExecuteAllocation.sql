﻿CREATE procedure [Allocation].[ExecuteAllocation] 
	 @DatasetCode varchar (10)
	,@AllocationTypeID smallint
as

--------------------------------------------------------------------------
-- Copyright  Technologies Ltd. 2003    --
--------------------------------------------------------------------------

--prevent parallel running - isolate per dataset/allocation type
--this allows the truncation of the WrkAllocation table and subsequent reset of identity column
set transaction isolation level serializable

begin transaction

	declare
		 @StartTime datetime = getdate()
		,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		,@Elapsed int
		,@Stats varchar(max)


	declare
		 @localDatasetCode varchar (10) = @DatasetCode
		,@localAllocationTypeID smallint = @AllocationTypeID

	declare @Template varchar(128)
	declare @WrkSQL varchar(max)
	declare @Processed int
	declare @Allocated int 
	declare @Failed int

	select @StartTime = getdate()

	--delete
	--from
	--	Allocation.WrkAllocation
	--where
	--	DatasetCode = @localDatasetCode

	truncate table Allocation.WrkAllocation


	declare TemplateCursor cursor fast_forward for

	select distinct
		 TemplateDataset.Template
		,Template.AllocationTypeID
	from
		Allocation.TemplateDataset

	inner join Allocation.Template
	on	TemplateDataset.Template = Template.Template

	where  	
		TemplateDataset.DatasetCode = @localDatasetCode
	and TemplateDataset.Active = 1 
	and	Template.AllocationTypeID = @localAllocationTypeID

	order by
		TemplateDataset.Template

	OPEN TemplateCursor

	FETCH NEXT FROM TemplateCursor

	INTO
		 @Template
		,@localAllocationTypeID

	WHILE @@FETCH_STATUS = 0

	BEGIN

		select
			@WrkSQL =
				'Insert into Allocation.WrkAllocation (DatasetRecno, DatasetCode, AllocationID, Priority) 
				select DatasetRecno, DatasetCode, AllocationID, Priority from '
				+ @Template + ' where DatasetCode = ''' + @localDatasetCode + ''''
		
		print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL

		execute(@WrkSQL)

		FETCH NEXT FROM TemplateCursor
		INTO @Template, @localAllocationTypeID
	END
	  
	CLOSE TemplateCursor
	DEALLOCATE TemplateCursor


	delete
	from
		Allocation.DatasetAllocation
	where
		exists
		(
		select
			1
		from
			Allocation.WrkAllocateDataset
		where
			WrkAllocateDataset.DatasetRecno = DatasetAllocation.DatasetRecno
		and	WrkAllocateDataset.DatasetCode = DatasetAllocation.DatasetCode
		)
	and	DatasetAllocation.DatasetCode = @localDatasetCode
	and	DatasetAllocation.AllocationTypeID = @localAllocationTypeID


	insert into Allocation.DatasetAllocation
		(
		 DatasetRecno
		,AllocationTypeID
		,DatasetCode
		,AllocationID
		)
	select
 		 WrkAllocation.DatasetRecno
		,AllocationTypeID = @localAllocationTypeID
 		,WrkAllocation.DatasetCode
		,AllocationID
	from 
		Allocation.WrkAllocation
	where
		not exists
		(
		select
			1
		from
			Allocation.WrkAllocation Previous
		where
			Previous.DatasetRecno = WrkAllocation.DatasetRecno
		and	(
				Previous.Priority < WrkAllocation.Priority
			or
				(
					Previous.Priority = WrkAllocation.Priority 
				and	Previous.AllocationID < WrkAllocation.AllocationID
				)
			or
				(
					Previous.Priority = WrkAllocation.Priority 
				and	Previous.AllocationID = WrkAllocation.AllocationID
				and	Previous.AllocateServiceRecno < WrkAllocation.AllocateServiceRecno
				)
			)
		)


	select
		@Processed = count(*)
	from
		Allocation.WrkAllocateDataset
	where
		DatasetCode = @localDatasetCode

	select 
		@Allocated = count(*) 
	from 
		Allocation.WrkAllocateDataset
	where
		DatasetCode = @localDatasetCode
	and	exists 
		(
		select
			1
		from 
			Allocation.DatasetAllocation
		where
    		DatasetAllocation.DatasetRecno = WrkAllocateDataset.DatasetRecno
		and DatasetCode  = @localDatasetCode
		and	AllocationTypeID = @localAllocationTypeID
		)


	select @Failed = @Processed - @Allocated

	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select
		@Stats =
			'Dataset ' + @DatasetCode +
			', Allocation type ' + 
			(
			select
				AllocationType.AllocationType
			from
				Allocation.AllocationType
			where
				AllocationType.AllocationTypeID = @AllocationTypeID
			)
			 +
			', Processed ' + CONVERT(varchar(10), @Processed) +
			', Allocated ' + CONVERT(varchar(10), @Allocated) +
			', Failed ' + CONVERT(varchar(10), @Failed) +
			', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


	print @Stats

	exec [$(Warehouse)].dbo.WriteAuditLogEvent
		 @ProcedureName
		,@Stats

commit


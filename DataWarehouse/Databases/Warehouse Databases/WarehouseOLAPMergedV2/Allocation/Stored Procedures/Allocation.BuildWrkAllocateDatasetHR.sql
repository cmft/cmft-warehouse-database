﻿

CREATE procedure [Allocation].[BuildWrkAllocateDatasetHR] as


delete from Allocation.WrkAllocateDataset
where
	DatasetCode = 'HR'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DepartmentCode
	,SourceContextCode
) 
select
	 DatasetRecno = BaseSummary.MergeSummaryRecno
	,DatasetCode = 'HR'
	,CensusDate
	,DepartmentCode = convert(varchar,OrganisationCode)
	,SourceContextCode = BaseSummary.ContextCode

from
	HR.BaseSummary BaseSummary

where
     BaseSummary.ContextCode = 'CMFT||ESREXT'
          


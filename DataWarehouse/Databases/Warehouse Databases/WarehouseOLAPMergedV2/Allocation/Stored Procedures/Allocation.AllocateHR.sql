﻿

CREATE procedure [Allocation].[AllocateHR]
as



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetHR

-- Execute allocation
exec Allocation.ExecuteAllocation 'HR' , 10 --Directorate


update
	HR.BaseSummary
set
	DirectorateCode = Allocation.SourceAllocationID
from
	HR.BaseSummary

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseSummary.MergeSummaryRecno
and	DatasetAllocation.DatasetCode = 'HR'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseSummary.DirectorateCode != Allocation.SourceAllocationID
or	BaseSummary.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



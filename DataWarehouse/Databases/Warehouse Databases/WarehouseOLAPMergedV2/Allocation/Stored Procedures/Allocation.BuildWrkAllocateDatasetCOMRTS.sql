﻿






CREATE procedure [Allocation].[BuildWrkAllocateDatasetCOMRTS]
as

----------------------------------------------------------------------------
---- Copyright Gecko Technologies Ltd. 2003    --
----------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'COM'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceSpecialtyCode
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'COM'
	,DatasetStartDate = null
	,DatasetEndDate = null
	,SpecialtyCode = SpecialtyCode
	,ContextCode = Encounter.ContextCode

from
	COM.BaseEncounter Encounter

inner join COM.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left outer join COM.Specialty Specialty
on	Specialty.SourceValueID = Reference.ReferredToSpecialtyID











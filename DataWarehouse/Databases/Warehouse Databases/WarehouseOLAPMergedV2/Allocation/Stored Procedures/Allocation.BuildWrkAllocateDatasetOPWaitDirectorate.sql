﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetOPWaitDirectorate]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'OPWAIT'


select
       @CensusDate = 
              coalesce
                     (@CensusDate,
                           (
                           select
                                  max(BaseEncounter.CensusDate)
                           from
                                  OPWL.BaseEncounter 
                           )
                     )


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,SourceClinicCode
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'OPWAIT'
	,DatasetStartDate = Encounter.CensusDate
	,SourceSiteCode = Encounter.SiteCode
	,SourceSpecialtyCode = Encounter.PASSpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.CensusDate
	,SourceClinicCode = Encounter.ClinicCode
	,SourceContextCode = Encounter.ContextCode
from
	OPWL.BaseEncounter Encounter
where
	Encounter.CensusDate = @CensusDate

﻿
CREATE procedure [Allocation].[AllocatePEX]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetPEX

-- Execute allocation
exec Allocation.ExecuteAllocation 'PEX' , 10 --Directorate


update
	PEX.BaseResponse
set
	DirectorateCode = Allocation.SourceAllocationID
from
	PEX.BaseResponse

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseResponse.MergeResponseRecno
and	DatasetAllocation.DatasetCode = 'PEX'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseResponse.DirectorateCode != Allocation.SourceAllocationID
or	BaseResponse.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


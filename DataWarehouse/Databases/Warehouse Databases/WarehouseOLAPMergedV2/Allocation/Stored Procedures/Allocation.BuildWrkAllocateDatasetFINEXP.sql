﻿

CREATE procedure [Allocation].[BuildWrkAllocateDatasetFINEXP] as


delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'FINEXP'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,Division
	,SourceContextCode
) 
select
	 DatasetRecno = BaseExpenditure.MergeExpenditureRecno
	,DatasetCode = 'FINEXP'
	,BaseExpenditure.CensusDate
	,Division = Division
	,SourceContextCode = BaseExpenditure.ContextCode
from
	Finance.BaseExpenditure

      



﻿
create procedure [Allocation].[AllocateQCR]
as



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetQCR

-- Execute allocation
exec Allocation.ExecuteAllocation 'QCR' , 10 --Directorate


update
	QCR.BaseAuditAnswer
set
	DirectorateCode = Allocation.SourceAllocationID
from
	QCR.BaseAuditAnswer

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseAuditAnswer.AuditAnswerRecno
and	DatasetAllocation.DatasetCode = 'QCR'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseAuditAnswer.DirectorateCode != Allocation.SourceAllocationID
or	BaseAuditAnswer.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


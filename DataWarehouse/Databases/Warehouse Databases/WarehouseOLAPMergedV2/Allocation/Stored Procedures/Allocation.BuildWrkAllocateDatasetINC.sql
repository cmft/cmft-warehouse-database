﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetINC] as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'INC'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DepartmentCode
	,SourceContextCode
) 
select
	 DatasetRecno = Incident.MergeIncidentRecno
	,DatasetCode = 'INC'
	,Incident.IncidentDate
	,DepartmentCode = Incident.DepartmentCode 
	,SourceContextCode = Incident.ContextCode

from
	Incident.BaseIncident Incident
where
      ContextCode = 'CMFT||ULYSS'

	









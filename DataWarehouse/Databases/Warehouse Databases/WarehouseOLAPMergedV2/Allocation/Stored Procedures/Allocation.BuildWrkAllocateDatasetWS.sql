﻿
CREATE procedure [Allocation].[BuildWrkAllocateDatasetWS] 
	
@ProcessAll bit = 1

as

declare @FromDate date = dateadd(month, -3, getdate())


delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'WS'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSiteCode
	,WardCode
	,SourceContextCode
) 
select
	 DatasetRecno = MergeEncounterRecno
	,DatasetCode = 'WS'
	,DatasetStartDate = StartDate
	,SiteCode = SiteCode
	,WardCode = WardCode
	,SourceContextCode = ContextCode
from
	APC.BaseWardStay

where
	@ProcessAll = 1
or	(
		Updated >= @FromDate
	and @ProcessAll = 0
	)


	




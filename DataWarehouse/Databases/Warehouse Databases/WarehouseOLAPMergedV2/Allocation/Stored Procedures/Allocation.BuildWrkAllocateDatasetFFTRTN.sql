﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetFFTRTN] as


delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'FFTRTN'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,WardCode
	,SourceSiteCode
	,SourceContextCode
) 
select
	 DatasetRecno = BaseFriendsFamilyTestReturn.FFTRecno
	,DatasetCode = 'FFTRTN'
	,BaseFriendsFamilyTestReturn.CensusDate
	,WardCode
	,SourceSiteCode = HospitalSiteCode
	,SourceContextCode = BaseFriendsFamilyTestReturn.ContextCode

from
	PEX.BaseFriendsFamilyTestReturn

where
      BaseFriendsFamilyTestReturn.ContextCode = 'CMFT||FFTRTN'
--and	WardCode is not null    --was there to not bring AE return data through the process, but removed as rules added to rule base
      

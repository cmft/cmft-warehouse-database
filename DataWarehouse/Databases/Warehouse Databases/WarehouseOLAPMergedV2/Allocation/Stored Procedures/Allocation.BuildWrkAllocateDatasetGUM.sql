﻿
CREATE procedure [Allocation].[BuildWrkAllocateDatasetGUM] as

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'GUM'

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceClinicCode
	,SourceAppointmentTypeCode
	,ActivityDate
	,SourceContextCode
) 
select
	 DatasetRecno = MergeEncounterRecno
	,DatasetCode = 'GUM'
	,DatasetStartDate = AppointmentDate
	,SourceClinicCode = ClinicID
	,SourceAppointmentTypeCode = AppointmentTypeID
	,ActivityDate = AppointmentDate
	,SourceContextCode = ContextCode

from
	GUM.BaseEncounter 

﻿CREATE proc [Allocation].[BuildCOMBaseReferralAllocation]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

truncate table COM.BaseReferralAllocation

insert into COM.BaseReferralAllocation

(
	MergeEncounterRecno
	,ServiceID
)

select --top 100
	BaseReferral.MergeEncounterRecno
	,ServiceID

from
	COM.BaseReferral

inner join
	
	(
		select
			MergeEncounterRecno = DatasetRecno 
			,ServiceID = AllocationID
		from
			Allocation.DatasetAllocation  
		where
			DatasetCode = 'COMR'
		and
			AllocationTypeID = 2 --Service
				
	) ServiceAllocation
	
	on BaseReferral.MergeEncounterRecno = ServiceAllocation.MergeEncounterRecno
	


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
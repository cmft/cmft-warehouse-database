﻿

CREATE procedure [Allocation].[BuildWrkAllocateDatasetFERTILITY] as

delete
from
	Allocation.WrkAllocateDataset
where
	DatasetCode = 'FERTILITY'


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,TreatmentType
	,SourceContextCode
) 
select
	 DatasetRecno = MergeRecno
	,DatasetCode = 'FERTILITY'
	,DatasetStartDate = EncounterDate 
	,TreatmentType
	,SourceContextCode = ContextCode

from
	OP.BaseFertility
where
	EncounterDate is not null


﻿create procedure [Allocation].[AllocateWS]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


-- Write data to work table
exec Allocation.BuildWrkAllocateDatasetWS

-- Execute allocation
exec Allocation.ExecuteAllocation 'WS' , 10 --Directorate


update
	BaseWardStay
set
	DirectorateCode = Allocation.SourceAllocationID
from
	APC.BaseWardStay

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseWardStay.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'WS'
and	DatasetAllocation.AllocationTypeID = 10

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseWardStay.DirectorateCode != Allocation.SourceAllocationID
or	BaseWardStay.DirectorateCode is null

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
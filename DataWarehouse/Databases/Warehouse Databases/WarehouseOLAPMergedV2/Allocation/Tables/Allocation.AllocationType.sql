﻿CREATE TABLE [Allocation].[AllocationType] (
    [AllocationTypeID] SMALLINT      IDENTITY (1, 1) NOT NULL,
    [AllocationType]   VARCHAR (128) NOT NULL,
    [ContextCode]      VARCHAR (50)  NULL,
    [Created]          DATETIME2 (3) CONSTRAINT [DF_AllocationAllocationTypeCreated] DEFAULT (getdate()) NULL,
    [ByWhom]           VARCHAR (50)  CONSTRAINT [DF_AllocationAllocationTypeByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_TemplateType_1] PRIMARY KEY CLUSTERED ([AllocationTypeID] ASC),
    CONSTRAINT [FK_TemplateType_TemplateType] FOREIGN KEY ([AllocationTypeID]) REFERENCES [Allocation].[AllocationType] ([AllocationTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AllocationType]
    ON [Allocation].[AllocationType]([ContextCode] ASC);


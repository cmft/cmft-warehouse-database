﻿CREATE TABLE [Allocation].[Allocation] (
    [AllocationID]       INT           IDENTITY (1, 1) NOT NULL,
    [SourceAllocationID] VARCHAR (100) NOT NULL,
    [Allocation]         VARCHAR (200) NOT NULL,
    [AllocationTypeID]   SMALLINT      NOT NULL,
    [Priority]           INT           NULL,
    [Active]             BIT           NULL,
    [Created]            DATETIME2 (3) CONSTRAINT [DF_AllocationAllocationCreated] DEFAULT (getdate()) NULL,
    [ByWhom]             VARCHAR (50)  CONSTRAINT [DF_AllocationAllocationByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_Allocation] PRIMARY KEY CLUSTERED ([AllocationID] ASC)
);


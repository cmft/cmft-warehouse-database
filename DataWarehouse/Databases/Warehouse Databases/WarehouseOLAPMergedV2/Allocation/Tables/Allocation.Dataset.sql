﻿CREATE TABLE [Allocation].[Dataset] (
    [DatasetCode] VARCHAR (10)  NOT NULL,
    [Dataset]     VARCHAR (255) NULL,
    CONSTRAINT [PK_Dataset] PRIMARY KEY CLUSTERED ([DatasetCode] ASC) WITH (FILLFACTOR = 90)
);


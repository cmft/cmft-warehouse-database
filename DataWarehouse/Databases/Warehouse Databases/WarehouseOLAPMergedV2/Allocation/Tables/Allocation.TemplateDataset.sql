﻿CREATE TABLE [Allocation].[TemplateDataset] (
    [Template]    VARCHAR (128) NOT NULL,
    [DatasetCode] VARCHAR (10)  NOT NULL,
    [Active]      BIT           NOT NULL,
    [Created]     DATETIME      CONSTRAINT [DF_AllocationTemplateDatasetCreated] DEFAULT (getdate()) NULL,
    [ByWhom]      VARCHAR (50)  CONSTRAINT [DF_AllocationTemplateDatasetByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_TemplateDataset] PRIMARY KEY CLUSTERED ([Template] ASC, [DatasetCode] ASC),
    CONSTRAINT [FK_TemplateDataset_Template] FOREIGN KEY ([Template]) REFERENCES [Allocation].[Template] ([Template]) ON UPDATE CASCADE
);


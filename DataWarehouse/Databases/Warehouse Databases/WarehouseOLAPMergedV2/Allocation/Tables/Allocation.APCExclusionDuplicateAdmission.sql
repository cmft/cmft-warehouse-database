﻿CREATE TABLE [Allocation].[APCExclusionDuplicateAdmission] (
    [DatasetCode]                      VARCHAR (3)  NOT NULL,
    [ReportableDatasetRecno]           INT          NOT NULL,
    [ReportableSourceUniqueID]         VARCHAR (50) NULL,
    [ReportableProviderSpellno]        VARCHAR (50) NULL,
    [ReportableSourceSiteCode]         VARCHAR (10) NULL,
    [ReportableSourceSpecialtyCode]    VARCHAR (20) NULL,
    [ReportableSourceWardCode]         VARCHAR (10) NULL,
    [NonReportableDatasetRecno]        INT          NOT NULL,
    [NonReportableSourceUniqueID]      VARCHAR (50) NULL,
    [NonReportableProviderSpellno]     VARCHAR (50) NULL,
    [NonReportableSourceSiteCode]      VARCHAR (10) NULL,
    [NonReportableSourceSpecialtyCode] VARCHAR (20) NULL,
    [NonReportableSourceWardCode]      VARCHAR (10) NULL
);


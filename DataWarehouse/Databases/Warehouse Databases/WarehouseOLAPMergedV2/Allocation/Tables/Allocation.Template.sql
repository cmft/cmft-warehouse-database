﻿CREATE TABLE [Allocation].[Template] (
    [Template]            VARCHAR (128) NOT NULL,
    [TemplateDescription] VARCHAR (255) NULL,
    [AllocationTypeID]    SMALLINT      NOT NULL,
    [Priority]            INT           NULL,
    [TemplateID]          INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED ([Template] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Template_TemplateType] FOREIGN KEY ([AllocationTypeID]) REFERENCES [Allocation].[AllocationType] ([AllocationTypeID])
);


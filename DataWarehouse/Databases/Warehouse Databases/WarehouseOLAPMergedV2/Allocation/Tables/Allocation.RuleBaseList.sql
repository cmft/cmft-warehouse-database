﻿CREATE TABLE [Allocation].[RuleBaseList] (
    [RuleBaseList]      VARCHAR (255) NOT NULL,
    [RuleBaseListValue] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_RuleBaseList] PRIMARY KEY CLUSTERED ([RuleBaseList] ASC, [RuleBaseListValue] ASC)
);


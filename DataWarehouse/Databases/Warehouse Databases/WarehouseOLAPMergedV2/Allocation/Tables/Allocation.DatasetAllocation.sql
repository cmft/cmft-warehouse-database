﻿CREATE TABLE [Allocation].[DatasetAllocation] (
    [AllocationTypeID] SMALLINT     NOT NULL,
    [DatasetRecno]     BIGINT       NOT NULL,
    [DatasetCode]      VARCHAR (10) NOT NULL,
    [AllocationID]     INT          NOT NULL,
    [TemplateID]       INT          NULL,
    CONSTRAINT [PK_DatasetAllocation] PRIMARY KEY CLUSTERED ([AllocationTypeID] ASC, [DatasetRecno] ASC, [DatasetCode] ASC),
    CONSTRAINT [FK_DatasetAllocation_Dataset] FOREIGN KEY ([DatasetCode]) REFERENCES [Allocation].[Dataset] ([DatasetCode]),
    CONSTRAINT [FK_DatasetAllocation_TemplateType] FOREIGN KEY ([AllocationTypeID]) REFERENCES [Allocation].[AllocationType] ([AllocationTypeID])
);


GO
CREATE NONCLUSTERED INDEX [IX_DatasetAllocation__DatasetCode__ATID_DSRecNo]
    ON [Allocation].[DatasetAllocation]([DatasetCode] ASC)
    INCLUDE([AllocationTypeID], [DatasetRecno])
    ON [WOMv2_Allocation];


GO
CREATE NONCLUSTERED INDEX [IX_DatasetAllocation_ATI-DC_dral]
    ON [Allocation].[DatasetAllocation]([AllocationTypeID] ASC, [DatasetCode] ASC)
    INCLUDE([DatasetRecno], [AllocationID])
    ON [WOMv2_Allocation];


GO
CREATE NONCLUSTERED INDEX [IX_Allocation_DA]
    ON [Allocation].[DatasetAllocation]([DatasetRecno] ASC, [DatasetCode] ASC, [AllocationID] ASC)
    ON [WOMv2_Allocation];


﻿CREATE TABLE [Allocation].[WrkAllocation] (
    [AllocateServiceRecno] INT          IDENTITY (1, 1) NOT NULL,
    [DatasetCode]          VARCHAR (10) NOT NULL,
    [DatasetRecno]         INT          NOT NULL,
    [AllocationID]         INT          NOT NULL,
    [Priority]             INT          NOT NULL,
    [TemplateID]           INT          NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Allocation_WrkAllocation_1]
    ON [Allocation].[WrkAllocation]([DatasetCode] ASC)
    INCLUDE([AllocateServiceRecno], [DatasetRecno], [AllocationID], [Priority]);


﻿CREATE TABLE [Allocation].[DatasetAllocationHistory] (
    [AllocationTypeID] SMALLINT     NOT NULL,
    [DatasetRecno]     BIGINT       NOT NULL,
    [DatasetCode]      VARCHAR (10) NOT NULL,
    [AllocationID]     INT          NOT NULL
);


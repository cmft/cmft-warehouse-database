﻿
create view [APC].[DischargeSummaryExclusionReason]

as

select
	 DischargeSummaryExclusionReasonID = Allocation.AllocationID
	,SourceDischargeSummaryExclusionReasonID = Allocation.SourceAllocationID
	,DischargeSummaryExclusionReason = Allocation.Allocation
from
	Allocation.Allocation

where
	Allocation.AllocationTypeID = 14


﻿



CREATE view [APC].[OperationStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceOperationStatusID = SourceValueID
	,SourceOperationStatusCode = SourceValueCode
	,SourceOperationStatus = SourceValue
	,LocalOperationStatusID = LocalValueID
	,LocalOperationStatusCode = LocalValueCode
	,LocalOperationStatus = LocalValue
	,NationalOperationStatusID = NationalValueID
	,NationalOperationStatusCode = NationalValueCode
	,NationalOperationStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'OPESTA'






﻿








CREATE view [APC].[EncounterNotReportable] as

select
	 Encounter.MergeEncounterRecno
	,Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceSpellNo
	,Encounter.SourceEncounterNo
	,Encounter.ProviderSpellNo
	,Encounter.GlobalProviderSpellNo
	,Encounter.GlobalEpisodeNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.DateOnWaitingList
	,Encounter.AdmissionDate
	,Encounter.DischargeDate
	,Encounter.EpisodeStartDate
	,Encounter.EpisodeEndDate
	,Encounter.StartSiteCode
	,Encounter.StartWardTypeCode
	,Encounter.EndSiteCode
	,Encounter.EndWardTypeCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.SiteCode
	,Encounter.AdmissionMethodCode
	,Encounter.AdmissionSourceCode
	,Encounter.PatientClassificationCode
	,Encounter.ManagementIntentionCode
	,Encounter.DischargeMethodCode
	,Encounter.DischargeDestinationCode
	,Encounter.AdminCategoryCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.NationalLastEpisodeInSpellIndicator
	,Encounter.FirstRegDayOrNightAdmit
	,Encounter.NeonatalLevelOfCare
	,Encounter.PASHRGCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.SecondaryDiagnosisCode13
	,Encounter.PrimaryProcedureCode
	,Encounter.PrimaryProcedureDate
	,Encounter.SecondaryProcedureCode1
	,Encounter.SecondaryProcedureDate1
	,Encounter.SecondaryProcedureCode2
	,Encounter.SecondaryProcedureDate2
	,Encounter.SecondaryProcedureCode3
	,Encounter.SecondaryProcedureDate3
	,Encounter.SecondaryProcedureCode4
	,Encounter.SecondaryProcedureDate4
	,Encounter.SecondaryProcedureCode5
	,Encounter.SecondaryProcedureDate5
	,Encounter.SecondaryProcedureCode6
	,Encounter.SecondaryProcedureDate6
	,Encounter.SecondaryProcedureCode7
	,Encounter.SecondaryProcedureDate7
	,Encounter.SecondaryProcedureCode8
	,Encounter.SecondaryProcedureDate8
	,Encounter.SecondaryProcedureCode9
	,Encounter.SecondaryProcedureDate9
	,Encounter.SecondaryProcedureCode10
	,Encounter.SecondaryProcedureDate10
	,Encounter.SecondaryProcedureCode11
	,Encounter.SecondaryProcedureDate11
	,Encounter.OperationStatusCode
	,Encounter.ContractSerialNo
	,Encounter.CodingCompleteDate
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.EpisodeStartTime
	,Encounter.EpisodeEndTime
	,Encounter.RegisteredGdpCode
	,Encounter.EpisodicGpCode
	,Encounter.InterfaceCode
	,Encounter.CasenoteNumber
	,Encounter.NHSNumberStatusCode
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.TransferFrom
	,Encounter.DistrictNo
	,Encounter.ExpectedLOS
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.IntendedPrimaryProcedureCode
	,Encounter.Operation
	,Encounter.Research1
	,Encounter.CancelledElectiveAdmissionFlag
	,Encounter.LocalAdminCategoryCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.LocalityCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.SpellHRGCode
	,Encounter.CategoryCode
	,Encounter.LOE
	,Encounter.LOS
	,Encounter.Cases
	,Encounter.FirstEpisodeInSpellIndicator 
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeStartTimeOfDay
	,Encounter.EpisodeEndTimeOfDay
	,Encounter.AdmissionTimeOfDay
	,Encounter.DischargeTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.EndDirectorateCode
	,Encounter.ResidencePCTCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTime
	,Encounter.ISTDischargeTime
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay
	,Encounter.PatientCategoryCode
	,Encounter.Research2
	,Encounter.ClinicalCodingStatus
	,Encounter.ClinicalCodingCompleteDate
	,Encounter.ClinicalCodingCompleteTime
	,Encounter.ReferredByCode
	,Encounter.ReferrerCode
	,Encounter.DecidedToAdmitDate
	,Encounter.PsychiatricPatientStatusCode
	,Encounter.LegalStatusClassificationCode
	,Encounter.ReferringConsultantCode
	,Encounter.DurationOfElectiveWait
	,Encounter.CarerSupportIndicator
	,Encounter.DischargeReadyDate
	,Encounter.ContextCode
	,Encounter.EddCreatedTime
	,Encounter.ExpectedDateOfDischarge
	,Encounter.EddCreatedByConsultantFlag
	,Encounter.EddInterfaceCode
	,Encounter.VTECategoryCode
	,Encounter.Ambulatory
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.PseudoPostcode
	,Encounter.CCGCode
	,Encounter.EpisodicPostcode
	,Encounter.GpCodeAtDischarge
	,Encounter.GpPracticeCodeAtDischarge
	,Encounter.PostcodeAtDischarge
	,Encounter.ResidenceCCGCode
	,Encounter.ContextID
	,Encounter.AdmissionMethodID
	,Encounter.AdmissionSourceID
	,Encounter.DischargeDestinationID
	,Encounter.DischargeMethodID
	,Encounter.IntendedManagementID
	,Encounter.ISTAdmissionSpecialtyID
	,Encounter.NeonatalLevelOfCareID
	,Encounter.PatientClassificationID
	,Encounter.VTECategoryID
	,Encounter.StartWardID
	,Encounter.AgeID
	,Encounter.AdmissionDateID
	,Encounter.DischargeDateID
	,Encounter.EpisodeStartDateID
	,Encounter.EpisodeEndDateID
	,Encounter.ConsultantID
	,Encounter.SexID
	,Encounter.StartSiteID
	,Encounter.EndSiteID
	,Encounter.SpecialtyID
	,Encounter.NHSNumberStatusID
	,Encounter.OperationStatusID
	,Encounter.EthnicOriginID
	,Encounter.MaritalStatusID
	,Encounter.SiteID
	,Encounter.AdminCategoryID
	,Encounter.FirstRegDayOrNightAdmitID
	,Encounter.RTTCurrentStatusID
	,Encounter.RTTPeriodStatusID
	,Encounter.ReferrerID
	,Encounter.ReferringConsultantID
	,Encounter.CarerSupportIndicatorID
	,Encounter.EndWardID
	,Encounter.ResidenceCCGID
		
	,ContractPointOfDeliveryCode = ContractPointOfDelivery.SourceAllocationID
	,ContractFlagCode = ContractFlag.SourceAllocationID
	,ContractHRGCode = ContractHRG.SourceAllocationID
	,ContractSpecialtyCode = ContractSpecialty.SourceAllocationID
	,CharlsonIndex
		
from
	(	
	select
		 BaseEncounter.MergeEncounterRecno
		,BaseEncounter.EncounterRecno
		,BaseEncounter.SourceUniqueID
		,BaseEncounter.SourcePatientNo
		,BaseEncounter.SourceSpellNo
		,BaseEncounter.SourceEncounterNo
		,BaseEncounter.ProviderSpellNo
		,BaseEncounter.GlobalProviderSpellNo
		,BaseEncounter.GlobalEpisodeNo
		,BaseEncounter.PatientTitle
		,BaseEncounter.PatientForename
		,BaseEncounter.PatientSurname
		,BaseEncounter.DateOfBirth
		,BaseEncounter.DateOfDeath
		,BaseEncounter.SexCode
		,BaseEncounter.NHSNumber
		,BaseEncounter.Postcode
		,BaseEncounter.PatientAddress1
		,BaseEncounter.PatientAddress2
		,BaseEncounter.PatientAddress3
		,BaseEncounter.PatientAddress4
		,BaseEncounter.DHACode
		,BaseEncounter.EthnicOriginCode
		,BaseEncounter.MaritalStatusCode
		,BaseEncounter.ReligionCode

		,DateOnWaitingList = Admission.DateOnWaitingList
		,AdmissionDate = Admission.AdmissionDate
		,DischargeDate = Discharge.DischargeDate

		,BaseEncounter.EpisodeStartDate
		,BaseEncounter.EpisodeEndDate

		,StartSiteCode = Admission.StartSiteCode

		,BaseEncounter.StartWardTypeCode

		,EndSiteCode = Discharge.EndSiteCode

		,BaseEncounter.EndWardTypeCode

		,BaseEncounter.RegisteredGpCode
		,BaseEncounter.RegisteredGpPracticeCode
		,BaseEncounter.SiteCode

		,AdmissionMethodCode = Admission.AdmissionMethodCode
		,AdmissionSourceCode = Admission.AdmissionSourceCode
		,PatientClassificationCode = Admission.PatientClassificationCode
		,ManagementIntentionCode = Admission.ManagementIntentionCode
		,DischargeMethodCode = Discharge.DischargeMethodCode
		,DischargeDestinationCode = Discharge.DischargeDestinationCode
		,AdminCategoryCode = Admission.AdminCategoryCode

		,BaseEncounter.ConsultantCode
		,BaseEncounter.SpecialtyCode

		,NationalLastEpisodeInSpellIndicator = 
			case
			when Discharge.DischargeDate is null
			then 9

			when BaseEncounter.MergeEncounterRecno = Discharge.MergeEncounterRecno
			then 1

			else 2
			end

		,FirstRegDayOrNightAdmit = Admission.FirstRegDayOrNightAdmit

		,BaseEncounter.NeonatalLevelOfCare
		,BaseEncounter.PASHRGCode
		,BaseEncounter.PrimaryDiagnosisCode
		,BaseEncounter.SubsidiaryDiagnosisCode
		,BaseEncounter.SecondaryDiagnosisCode1
		,BaseEncounter.SecondaryDiagnosisCode2
		,BaseEncounter.SecondaryDiagnosisCode3
		,BaseEncounter.SecondaryDiagnosisCode4
		,BaseEncounter.SecondaryDiagnosisCode5
		,BaseEncounter.SecondaryDiagnosisCode6
		,BaseEncounter.SecondaryDiagnosisCode7
		,BaseEncounter.SecondaryDiagnosisCode8
		,BaseEncounter.SecondaryDiagnosisCode9
		,BaseEncounter.SecondaryDiagnosisCode10
		,BaseEncounter.SecondaryDiagnosisCode11
		,BaseEncounter.SecondaryDiagnosisCode12
		,BaseEncounter.SecondaryDiagnosisCode13
		,BaseEncounter.PrimaryProcedureCode
		,BaseEncounter.PrimaryProcedureDate
		,BaseEncounter.SecondaryProcedureCode1
		,BaseEncounter.SecondaryProcedureDate1
		,BaseEncounter.SecondaryProcedureCode2
		,BaseEncounter.SecondaryProcedureDate2
		,BaseEncounter.SecondaryProcedureCode3
		,BaseEncounter.SecondaryProcedureDate3
		,BaseEncounter.SecondaryProcedureCode4
		,BaseEncounter.SecondaryProcedureDate4
		,BaseEncounter.SecondaryProcedureCode5
		,BaseEncounter.SecondaryProcedureDate5
		,BaseEncounter.SecondaryProcedureCode6
		,BaseEncounter.SecondaryProcedureDate6
		,BaseEncounter.SecondaryProcedureCode7
		,BaseEncounter.SecondaryProcedureDate7
		,BaseEncounter.SecondaryProcedureCode8
		,BaseEncounter.SecondaryProcedureDate8
		,BaseEncounter.SecondaryProcedureCode9
		,BaseEncounter.SecondaryProcedureDate9
		,BaseEncounter.SecondaryProcedureCode10
		,BaseEncounter.SecondaryProcedureDate10
		,BaseEncounter.SecondaryProcedureCode11
		,BaseEncounter.SecondaryProcedureDate11
		,BaseEncounter.OperationStatusCode
		,BaseEncounter.ContractSerialNo
		,BaseEncounter.CodingCompleteDate
		,BaseEncounter.PurchaserCode
		,BaseEncounter.ProviderCode
		,BaseEncounter.EpisodeStartTime
		,BaseEncounter.EpisodeEndTime
		,BaseEncounter.RegisteredGdpCode
		,BaseEncounter.EpisodicGpCode
		,BaseEncounter.InterfaceCode
		,BaseEncounter.CasenoteNumber
		,BaseEncounter.NHSNumberStatusCode

		,AdmissionTime = Admission.AdmissionTime
		,DischargeTime = Discharge.DischargeTime
		,TransferFrom = Admission.TransferFrom

		,BaseEncounter.DistrictNo
		,BaseEncounter.ExpectedLOS
		,BaseEncounter.MRSAFlag
		,BaseEncounter.RTTPathwayID
		,BaseEncounter.RTTPathwayCondition
		,BaseEncounter.RTTStartDate
		,BaseEncounter.RTTEndDate
		,BaseEncounter.RTTSpecialtyCode
		,BaseEncounter.RTTCurrentProviderCode
		,BaseEncounter.RTTCurrentStatusCode
		,BaseEncounter.RTTCurrentStatusDate
		,BaseEncounter.RTTCurrentPrivatePatientFlag
		,BaseEncounter.RTTOverseasStatusFlag
		,BaseEncounter.RTTPeriodStatusCode
		,BaseEncounter.IntendedPrimaryProcedureCode
		,BaseEncounter.Operation
		,BaseEncounter.Research1
		,BaseEncounter.CancelledElectiveAdmissionFlag
		,BaseEncounter.LocalAdminCategoryCode
		,BaseEncounter.EpisodicGpPracticeCode
		,BaseEncounter.PCTCode
		,BaseEncounter.LocalityCode
		,BaseEncounter.AgeCode
		,BaseEncounter.HRGCode
		,BaseEncounter.SpellHRGCode
		,BaseEncounter.CategoryCode
		,BaseEncounter.LOE

		,LOS =
			DATEDIFF(
				 DAY
				,Admission.AdmissionDate
				,Discharge.DischargeDate
			)

		,BaseEncounter.Cases

		,FirstEpisodeInSpellIndicator = 
			cast(
				case
				when BaseEncounter.GlobalEpisodeNo = 1
				then 1
				else 0
				end

				as bit
			)
		,BaseEncounter.RTTActivity
		,BaseEncounter.RTTBreachStatusCode
		,BaseEncounter.DiagnosticProcedure
		,BaseEncounter.RTTTreated
		,BaseEncounter.EpisodeStartTimeOfDay
		,BaseEncounter.EpisodeEndTimeOfDay

		,AdmissionTimeOfDay = Admission.AdmissionTimeOfDay
		,DischargeTimeOfDay = Discharge.DischargeTimeOfDay

		,BaseEncounter.StartDirectorateCode
		,BaseEncounter.EndDirectorateCode
		,BaseEncounter.ResidencePCTCode

		,ISTAdmissionSpecialtyCode = Admission.ISTAdmissionSpecialtyCode
		,ISTAdmissionDemandTime = Admission.ISTAdmissionDemandTime
		,ISTDischargeTime = Discharge.ISTDischargeTime
		,ISTAdmissionDemandTimeOfDay = Admission.ISTAdmissionDemandTimeOfDay
		,ISTDischargeTimeOfDay = Discharge.ISTDischargeTimeOfDay

		,BaseEncounter.PatientCategoryCode
		,BaseEncounter.Research2
		,BaseEncounter.ClinicalCodingStatus
		,BaseEncounter.ClinicalCodingCompleteDate
		,BaseEncounter.ClinicalCodingCompleteTime
		,BaseEncounter.ReferredByCode
		,BaseEncounter.ReferrerCode
		,BaseEncounter.DecidedToAdmitDate
		,BaseEncounter.PsychiatricPatientStatusCode
		,BaseEncounter.LegalStatusClassificationCode
		,BaseEncounter.ReferringConsultantCode
		,BaseEncounter.DurationOfElectiveWait
		,BaseEncounter.CarerSupportIndicator
		,BaseEncounter.DischargeReadyDate
		,BaseEncounter.ContextCode
		,BaseEncounter.EddCreatedTime
		,BaseEncounter.ExpectedDateOfDischarge
		,BaseEncounter.EddCreatedByConsultantFlag
		,BaseEncounter.EddInterfaceCode
		,BaseEncounter.VTECategoryCode
		,BaseEncounter.Ambulatory
		,BaseEncounter.Created
		,BaseEncounter.Updated
		,BaseEncounter.ByWhom
		,BaseEncounter.PseudoPostcode
		--,BaseEncounter.CCGCode
		,CCGCode = Discharge.CCGCode
		,BaseEncounter.EpisodicPostcode
		,Discharge.GpCodeAtDischarge
		,Discharge.GpPracticeCodeAtDischarge
		,Discharge.PostcodeAtDischarge
		,BaseEncounter.ResidenceCCGCode

		,BaseEncounterReference.ContextID
		,AdmissionReference.AdmissionMethodID
		,AdmissionReference.AdmissionSourceID
		,DischargeReference.DischargeDestinationID
		,DischargeReference.DischargeMethodID
		,AdmissionReference.IntendedManagementID
		,AdmissionReference.ISTAdmissionSpecialtyID
		,BaseEncounterReference.NeonatalLevelOfCareID
		,AdmissionReference.PatientClassificationID
		,BaseEncounterReference.VTECategoryID
		,BaseEncounterReference.StartWardID
		,BaseEncounterReference.AgeID
		,AdmissionReference.AdmissionDateID
		,DischargeReference.DischargeDateID
		,BaseEncounterReference.EpisodeStartDateID
		,BaseEncounterReference.EpisodeEndDateID
		,BaseEncounterReference.ConsultantID
		,BaseEncounterReference.SexID
		,AdmissionReference.StartSiteID
		,DischargeReference.EndSiteID
		,BaseEncounterReference.SpecialtyID
		,BaseEncounterReference.NHSNumberStatusID
		,BaseEncounterReference.OperationStatusID
		,BaseEncounterReference.EthnicOriginID
		,BaseEncounterReference.MaritalStatusID
		,BaseEncounterReference.SiteID
		,AdmissionReference.AdminCategoryID
		,AdmissionReference.FirstRegDayOrNightAdmitID
		,BaseEncounterReference.RTTCurrentStatusID
		,BaseEncounterReference.RTTPeriodStatusID
		,BaseEncounterReference.ReferrerID
		,BaseEncounterReference.ReferringConsultantID
		,BaseEncounterReference.CarerSupportIndicatorID
		,BaseEncounterReference.EndWardID
		,BaseEncounterReference.ResidenceCCGID
		
		,ContractPointOfDeliveryID = PointOfDeliveryAllocation.AllocationID
		,ContractFlagID = FlagAllocation.AllocationID
		,ContractHRGID = HRGAllocation.AllocationID
		,ContractSpecialtyID = SpecialtyAllocation.AllocationID
		,BaseEncounter.CharlsonIndex
		
	from
		APC.BaseEncounter

	inner join APC.BaseEncounterReference
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

	inner join APC.BaseEncounter Admission
	on	Admission.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
	and	Admission.GlobalEpisodeNo = 1

	inner join APC.BaseEncounterReference AdmissionReference
	on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

	left join APC.Spell
	on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

	left join APC.BaseEncounter Discharge
	on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

	left join APC.BaseEncounterReference DischargeReference
	on	DischargeReference.MergeEncounterRecno = Discharge.MergeEncounterRecno

	left join Allocation.DatasetAllocation PointOfDeliveryAllocation
	on	PointOfDeliveryAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
	and PointOfDeliveryAllocation.AllocationTypeID = 5
	and PointOfDeliveryAllocation.DatasetCode = 'APC'

	left join Allocation.DatasetAllocation FlagAllocation
	on	FlagAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
	and FlagAllocation.AllocationTypeID = 9
	and FlagAllocation.DatasetCode = 'APC'

	left join Allocation.DatasetAllocation HRGAllocation
	on	HRGAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
	and HRGAllocation.AllocationTypeID = 6
	and HRGAllocation.DatasetCode = 'APC'

	left join Allocation.DatasetAllocation SpecialtyAllocation
	on	SpecialtyAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
	and SpecialtyAllocation.AllocationTypeID = 7
	and SpecialtyAllocation.DatasetCode = 'APC'
	
	where
		BaseEncounter.Reportable = 0

	) Encounter

left join Allocation.Allocation ContractPointOfDelivery
on ContractPointOfDelivery.AllocationID = Encounter.ContractPointOfDeliveryID

left join Allocation.Allocation ContractFlag
on ContractFlag.AllocationID = Encounter.ContractFlagID

left join Allocation.Allocation ContractHRG
on ContractHRG.AllocationID = Encounter.ContractHRGID

left join Allocation.Allocation ContractSpecialty
on  ContractSpecialty.AllocationID = Encounter.ContractSpecialtyID













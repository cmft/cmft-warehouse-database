﻿
create view APC.BedDaySpecialtyConsultant

as

select
	[SourceConsultantCode]
	,[SourceSpecialtyCode]
	,[HRGCode]
	,[PODCode]
	,[ContextCode]
	,[FromDate]
	,[ToDate]
from
	[APC].[BedDaySpecialtyConsultantBase]

﻿




CREATE view [APC].[SHMI]

as

select
	 [MergeEncounterRecno]
	,[Died]
	,[Survived]
	,[DateOfDeath]
	,[DischargeDate]
	,[PatientAge]
	,[PatientAgeCategoryCode]
	,[AdmissionMethodCode]
	,[AdmissionMethodCategoryCode]
	,[SexCode]
	,[SexCategoryCode]
	,[PrimaryDiagnosisCode]
	,[DiagnosisGroupCode]
	,[DiagnosisGroupCategoryCode]
	,[CharlsonIndex]
	,[CharlsonIndexCategoryCode]
	,[YearIndexCode]
	,[RiskScore]
	,ExternalRiskScore
from
	[APC].[BaseSHMI]
where
	DominantForDiagnosis = 1








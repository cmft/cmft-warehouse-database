﻿







CREATE view [APC].[FactActivityBedday]

as

-- 20150612	RR	changed to reference APC.Encounter rather than APC.BaseEncounter


-- Non realtime - bed days against discharge date


select
	 MergeEncounterRecno = BedDay.MergeAPCEncounterRecno
	,EncounterDateID = Base.DischargeDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'APC'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlagAllocation.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,DirectorateID = Directorate.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,ServiceID = Service.AllocationID
	,HRGID = HRGBase.HRGID
	,ContextID = Base.ContextID
	,Cases = 1
	,Value = cast(null as float)
from
	APC.BedDay

inner join APC.Encounter Base
on	Base.MergeEncounterRecno = BedDay.MergeAPCEncounterRecno

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = BedDay.PODCode

inner join Allocation.DatasetAllocation Service
on	Service.DatasetRecno = Base.MergeEncounterRecno
and	Service.DatasetCode = 'APC'
and	Service.AllocationTypeID = 2

inner join Allocation.Allocation PointOfDelivery
on	PointOfDelivery.SourceAllocationID = BedDay.PODCode
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.DatasetAllocation ContractFlag
on	ContractFlag.DatasetRecno = Base.MergeEncounterRecno
and	ContractFlag.DatasetCode = 'APC'
and	ContractFlag.AllocationTypeID = 9

inner join Allocation.Allocation ContractFlagAllocation
on	ContractFlag.AllocationID = ContractFlagAllocation.AllocationID

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	Base.DischargeDateID = Calendar.DateID

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(BedDay.HRGCode, 'N/A')

left join APC.BaseCriticalCarePeriod
on	BedDay.MergeDatasetEncounterRecno = BaseCriticalCarePeriod.MergeEncounterRecno 
and	BedDay.DatasetCode = 'CC'

left join WH.Specialty
on	case
	when BedDay.PODCode = 'NEONATAL'
	then '192'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode in ('Trafford-CriticalCare', 'NEONATAL', 'Trafford-ICU', 'CMFT-HDU', 'CMFT-ICU', 'CMFT-CriticalCare')
	then '192'
	when BedDay.PODCode in ('REHAB-TRA', 'REHAB-CEN', 'REHAB', 'NRU-P','INRU-A'
	--20150514 RR added following request by PH
	,'INRU','PNRU'
	)
	then '314'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-CDU'
	then '501'
	when BedDay.PODCode = 'AKU'
	then '361'
	when BedDay.PODCode = 'ConCare'
	then '318'
	when BedDay.PODCode in ('HDUP', 'HDU-P(Outlying)', 'PICU', 'HDU-P', 'LTTU'
	--20150514 RR added following request by PH
	,'HDU','OutlyingHDU'
	)
	then '242'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-ERU'
	then '100'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-CSITU'
	then '172'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-MHDU'
	then '340'
	when BedDay.PODCode in ('GALAXY','CAMHS-T4')
	then '711'
	else '-1'
	end = Specialty.SourceSpecialtyCode
and	Base.ContextCode = Specialty.SourceContextCode

left join APC.BaseWardStay
on	BedDay.MergeDatasetEncounterRecno = BaseWardStay.MergeEncounterRecno 
and	BedDay.DatasetCode = 'WS'

left join APC.BaseWardStayReference
on	BaseWardStayReference.MergeEncounterRecno = BaseWardStay.MergeEncounterRecno 

left join WH.Site
on	Site.SourceSiteID = BaseWardStayReference.SiteID

left join WH.Directorate					
on	case				
	when BedDay.PODCode = 'NEONATAL'				
	then '33'				
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode in ('Trafford-CriticalCare', 'NEONATAL', 'Trafford-ICU', 'CMFT-HDU', 'CMFT-ICU', 'CMFT-CriticalCare')				
	then '92'				
	when 
		BedDay.PODCode in ('REHAB-TRA', 'REHAB-CEN', 'REHAB', 'NRU-P','INRU-A','INRU','PNRU') 
		and Site.NationalSiteCode = 'RW3MR'		
	then '26'		
	when 
		BedDay.PODCode in ('REHAB-TRA', 'REHAB-CEN', 'REHAB', 'NRU-P','INRU-A','INRU','PNRU') 
		and Site.NationalSiteCode = 'RW3TR'		
	then '82'		
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-CDU'				
	then '34'				
	when BedDay.PODCode = 'AKU'				
	then '24'				
	when BedDay.PODCode = 'ConCare'				
	then '26'				
	when 
		BedDay.PODCode in ('HDUP', 'HDU-P(Outlying)', 'PICU', 'HDU-P', 'LTTU','HDU','OutlyingHDU')				
	then '4'	
	when BedDay.PODCode = 'LTTU'				
	then '3'					
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-ERU'				
	then '15'				
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-CSITU'				
	then '12'				
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-MHDU'				
	then '26'				
	when BedDay.PODCode in ('GALAXY','CAMHS-T4')				
	then '62'				
	else 'N/A'				
	end = Directorate.DirectorateCode		


where
	not exists
			(
			select
				1
			from
				APC.BedDayWardBase
			where
				BedDayWardBase.PODCode = BedDay.PODCode
			and	BedDayWardBase.IsRealtimePOD = 1
			)

and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')


union all

-- Realtime - aggregate bed days against last day of month


select
	 MergeEncounterRecno = BedDay.MergeAPCEncounterRecno
	,EncounterDateID = LastOfMonth.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'APC'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable = cast(1 as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,DirectorateID = Directorate.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,ServiceID = Service.AllocationID
	,HRGID = HRGBase.HRGID
	,ContextID = Base.ContextID
	,Cases = 1
	,Value = cast(null as float)
from
	APC.BedDay

inner join APC.Encounter Base
on	Base.MergeEncounterRecno = BedDay.MergeAPCEncounterRecno

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = BedDay.PODCode

inner join Allocation.DatasetAllocation Service
on	Service.DatasetRecno = Base.MergeEncounterRecno
and	Service.DatasetCode = 'APC'
and	Service.AllocationTypeID = 2

inner join Allocation.Allocation PointOfDelivery
on	PointOfDelivery.SourceAllocationID = BedDay.PODCode
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.DatasetAllocation ContractFlag
on	ContractFlag.DatasetRecno = Base.MergeEncounterRecno
and	ContractFlag.DatasetCode = 'APC'
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	BedDay.CensusDate = Calendar.TheDate

inner join WH.Calendar LastOfMonth
on	Calendar.LastDayOfMonth = LastOfMonth.TheDate

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(BedDay.HRGCode, 'N/A')

left join APC.BaseCriticalCarePeriod
on	BedDay.MergeDatasetEncounterRecno = BaseCriticalCarePeriod.MergeEncounterRecno 
and	BedDay.DatasetCode = 'CC'

left join WH.Specialty
on	case
	when BedDay.PODCode = 'NEONATAL'
	then '192'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode in ('Trafford-CriticalCare', 'NEONATAL', 'Trafford-ICU', 'CMFT-HDU', 'CMFT-ICU', 'CMFT-CriticalCare')
	then '192'
	when BedDay.PODCode in ('REHAB-TRA', 'REHAB-CEN', 'REHAB', 'NRU-P','INRU-A','INRU','PNRU')
	then '314'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-CDU'
	then '501'
	when BedDay.PODCode = 'AKU'
	then '361'
	when BedDay.PODCode = 'ConCare'
	then '318'
	when BedDay.PODCode in ('HDUP', 'HDU-P(Outlying)', 'PICU', 'HDU-P', 'LTTU','HDU','OutlyingHDU')
	then '242'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-ERU'
	then '100'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-CSITU'
	then '172'
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-MHDU'
	then '340'
	when BedDay.PODCode in ('GALAXY','CAMHS-T4')
	then '711'
	else '-1'
	end = Specialty.SourceSpecialtyCode
and	Base.ContextCode = Specialty.SourceContextCode

left join APC.BaseWardStay
on	BedDay.MergeDatasetEncounterRecno = BaseWardStay.MergeEncounterRecno 
and	BedDay.DatasetCode = 'WS'

left join APC.BaseWardStayReference
on	BaseWardStayReference.MergeEncounterRecno = BaseWardStay.MergeEncounterRecno 

left join WH.Site
on	Site.SourceSiteID = BaseWardStayReference.SiteID

left join WH.Directorate					
on	case				
	when BedDay.PODCode = 'NEONATAL'				
	then '33'				
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode in ('Trafford-CriticalCare', 'NEONATAL', 'Trafford-ICU', 'CMFT-HDU', 'CMFT-ICU', 'CMFT-CriticalCare')				
	then '92'				
	when 
		BedDay.PODCode in ('REHAB-TRA', 'REHAB-CEN', 'REHAB', 'NRU-P','INRU-A','INRU','PNRU') 
		and Site.NationalSiteCode = 'RW3MR'		
	then '26'		
	when 
		BedDay.PODCode in ('REHAB-TRA', 'REHAB-CEN', 'REHAB', 'NRU-P','INRU-A','INRU','PNRU') 
		and Site.NationalSiteCode = 'RW3TR'		
	then '82'		
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-CDU'				
	then '34'				
	when BedDay.PODCode = 'AKU'				
	then '24'				
	when BedDay.PODCode = 'ConCare'				
	then '26'				
	when 
		BedDay.PODCode in ('HDUP', 'HDU-P(Outlying)', 'PICU', 'HDU-P', 'LTTU','HDU','OutlyingHDU')				
	then '4'	
	when BedDay.PODCode = 'LTTU'				
	then '3'					
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-ERU'				
	then '15'				
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-CSITU'				
	then '12'				
	when BedDay.PODCode = 'CRITICALCARE' and BaseCriticalCarePeriod.WardCode ='CMFT-MHDU'				
	then '26'				
	when BedDay.PODCode in ('GALAXY','CAMHS-T4')				
	then '62'				
	else 'N/A'				
	end = Directorate.DirectorateCode		


where
	exists
			(
			select
				1
			from
				APC.BedDayWardBase
			where
				BedDayWardBase.PODCode = BedDay.PODCode
			and	BedDayWardBase.IsRealtimePOD = 1
			)

and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 BedDay.Contracting_ID
	,EncounterDateID = Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'APC'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end
					as bit)
	,Reportable = 
			cast(
				case 
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				when PoD_Code in ('NELXBD', 'NELNEXBD', 'ELXBD', 'BPT', 'BPT-UC', 'RT-UB')
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	--,SpecialtyID = Specialty.NationalSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Context.ContextID
	,Cases = Activity_Actual
	,Value = Price_Actual

from
	SLAM.Encounter BedDay

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = BedDay.PoD_Code

inner join Allocation.Allocation PointOfDelivery
on	PointOfDelivery.SourceAllocationID = BedDay.PoD_Code
and	PointOfDelivery.AllocationTypeID = 5

left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = BedDay.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	BedDay.Spell_Discharge_Date = Calendar.TheDate

inner join WH.Directorate
on	Directorate.DirectorateCode = BedDay.Directorate_Code

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = BedDay.Treatment_Function_Code
and	Specialty.SourceContextCode = BedDay.ContextCode

--inner join 
--		(
--		select distinct
--			NationalSpecialtyID
--			,NationalSpecialtyCode
--		from
--			WH.Specialty
--		) Specialty
--on	Specialty.NationalSpecialtyCode = BedDay.Treatment_Function_Code

inner join WH.Context
on	BedDay.ContextCode = Context.ContextCode

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(BedDay.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = BedDay.Feed_Name
--and LatestExtractMonth.Extract_Type = BedDay.Extract_Type
--and LatestExtractMonth.Extract_Month = BedDay.Extract_Month
--and LatestExtractMonth.Extract_Year = BedDay.Extract_Year

where
	BedDay.Feed_Name in 
						(
						'Beddays_Non_Realtime'
						,'Beddays_Realtime'
						)
and	BedDay.Extract_Type = 'Freeze'
--and	BedDay.Is_Current = 1
and	BedDay.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')





































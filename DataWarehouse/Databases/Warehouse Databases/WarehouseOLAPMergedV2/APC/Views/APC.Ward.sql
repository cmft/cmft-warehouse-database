﻿

CREATE view [APC].[Ward] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourceWardID = Member.SourceValueID
	,SourceWardCode = Member.SourceValueCode
	,SourceWard = Member.SourceValueCode + ' - ' + Member.SourceValue
	,LocalWardID = Member.LocalValueID
	,LocalWardCode = Member.LocalValueCode
	,LocalWard = Member.LocalValue
	,NationalWardID = Member.NationalValueID
	,NationalWardCode = Member.NationalValueCode
	,NationalWard = Member.NationalValue

	,Site.SourceSiteID
FROM
	WH.Member

left join APC.WardBase Ward
on	Ward.WardCode = Member.SourceValueCode
and	Ward.ContextCode = Member.SourceContextCode

left join WH.Site
on	Site.SourceSiteCode = coalesce(Ward.SiteCode, '-1')
and	Site.SourceContextCode = Member.SourceContextCode

where 
	Member.AttributeCode = 'WARD'


﻿


create view [APC].[PressureUlcerCategory] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourcePressureUlcerCategoryID = Member.SourceValueID
	,SourcePressureUlcerCategoryCode = Member.SourceValueCode
	,SourcePressureUlcerCategory = Member.SourceValue
	,LocalPressureUlcerCategoryID = Member.LocalValueID
	,LocalPressureUlcerCategoryCode = Member.LocalValueCode
	,LocalPressureUlcerCategory = Member.LocalValue
	,NationalPressureUlcerCategoryID = Member.NationalValueID
	,NationalPressureUlcerCategoryCode = Member.NationalValueCode
	,NationalPressureUlcerCategory = Member.NationalValue

FROM
	WH.Member

where 
	Member.AttributeCode = 'PRESSULCRCAT'



﻿



			CREATE view [APC].[Wait] as

			select 
				 EncounterRecno
				,SourceEncounterRecno
				,InterfaceCode
				,SourcePatientNo
				,SourceEncounterNo
				,CensusDateID
				,PatientSurname
				,PatientForename
				,PatientTitle
				,SexID
				,DateOfBirth
				,DateOfDeath
				,NHSNumber
				,DistrictNo
				,MaritalStatusID
				,ReligionID
				,Postcode
				,PatientsAddress1
				,PatientsAddress2
				,PatientsAddress3
				,PatientsAddress4
				,DHACode
				,HomePhone
				,WorkPhone
				,EthnicOriginID
				,ConsultantID
				,SpecialtyID
				,ManagementIntentionID
				,AdmissionMethodID
				,PriorityID
				,WaitingListID
				,CommentClinical
				,CommentNonClinical
				,IntendedPrimaryProcedureCode
				,Operation
				,SiteID
				,WardID
				,WLStatus
				,ProviderCode
				,PurchaserCode
				,ContractSerialNumber
				,CancelledBy
				,BookingTypeCode
				,CasenoteNumber
				,OriginalDateOnWaitingList
				,DateOnWaitingList
				,TCIDateID
				,KornerWait
				,CountOfDaysSuspended
				,SuspensionStartDate
				,SuspensionEndDate
				,SuspensionReasonCode
				,SuspensionReason
				,EpisodicGpCode
				,RegisteredGpPracticeCode
				,EpisodicGpPracticeCode
				,RegisteredGpCode
				,RegisteredPracticeCode
				,SourceTreatmentFunctionID
				,TreatmentFunctionID
				,NationalSpecialtyID
				,PCTCode
				,BreachDate
				,ExpectedAdmissionDate
				,ReferralDate
				,ExpectedLOS
				,ProcedureDate
				,FutureCancellationDate
				,TheatreKey
				,TheatreInterfaceCode
				,EpisodeNo
				,BreachDays
				,MRSA
				,RTTPathwayID
				,RTTPathwayCondition
				,RTTStartDate
				,RTTEndDate
				,RTTSpecialtyID
				,RTTCurrentProviderCode
				,RTTCurrentStatusID
				,RTTCurrentStatusDate
				,RTTCurrentPrivatePatientFlag
				,RTTOverseasStatusFlag
				,NationalBreachDate
				,NationalBreachDays
				,DerivedBreachDays
				,DerivedClockStartDate
				,DerivedBreachDate
				,RTTBreachDate
				,RTTDiagnosticBreachDate
				,NationalDiagnosticBreachDate
				,SocialSuspensionDays
				,AgeCode
				,Cases
				,LengthOfWait
				,DurationCode
				,ConsultantSpecialtyRecno
				,WaitTypeCode
				,WaitType
				,StatusCode
				,CategoryCode
				,AddedToWaitingListTime
				,AddedLastWeek
				,WaitingListAddMinutes
				,OPCSCoded
				,WithRTTStartDate
				,WithRTTStatusCode
				,WithExpectedAdmissionDate
				,SourceUniqueID
				,PASSpecialtyID
				,AdminCategoryID
				,RTTActivity
				,RTTBreachStatusCode
				,DiagnosticProcedure
				,DirectorateID
				,EpisodeSpecialtyID
				,GuaranteedAdmissionDate
				,DurationAtTCIDateCode
				,WithTCIDate
				,WithGuaranteedAdmissionDate
				,RTTPathwayStartDateCurrent
				,RTTWeekBandReturnCode 
				,RTTDaysWaiting 
				,AdmissionReason
				,ContextCode
				,ContextID
				,AppointmentTypeID
			from 
				APC.BaseWait








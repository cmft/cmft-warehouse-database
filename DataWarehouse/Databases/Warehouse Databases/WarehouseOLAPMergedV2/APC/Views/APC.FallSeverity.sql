﻿


create view [APC].[FallSeverity] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourceFallSeverityID = Member.SourceValueID
	,SourceFallSeverityCode = Member.SourceValueCode
	,SourceFallSeverity = Member.SourceValue
	,LocalFallSeverityID = Member.LocalValueID
	,LocalFallSeverityCode = Member.LocalValueCode
	,LocalFallSeverity = Member.LocalValue
	,NationalFallSeverityID = Member.NationalValueID
	,NationalFallSeverityCode = Member.NationalValueCode
	,NationalFallSeverity = Member.NationalValue

FROM
	WH.Member

where 
	Member.AttributeCode = 'FALLSEV'



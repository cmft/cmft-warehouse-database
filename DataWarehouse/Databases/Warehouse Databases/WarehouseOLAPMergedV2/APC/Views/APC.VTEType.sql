﻿


create view [APC].[VTEType] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourceVTETypeID = Member.SourceValueID
	,SourceVTETypeCode = Member.SourceValueCode
	,SourceVTEType = Member.SourceValue
	,LocalVTETypeID = Member.LocalValueID
	,LocalVTETypeCode = Member.LocalValueCode
	,LocalVTEType = Member.LocalValue
	,NationalVTETypeID = Member.NationalValueID
	,NationalVTETypeCode = Member.NationalValueCode
	,NationalVTEType = Member.NationalValue

FROM
	WH.Member

where 
	Member.AttributeCode = 'VTETYPE'



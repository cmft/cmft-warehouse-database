﻿



CREATE view [APC].[DischargeMethod] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceDischargeMethodID = SourceValueID
	,SourceDischargeMethodCode = SourceValueCode
	,SourceDischargeMethod = SourceValue
	,LocalDischargeMethodID = LocalValueID
	,LocalDischargeMethodCode = LocalValueCode
	,LocalDischargeMethod = LocalValue
	,NationalDischargeMethodID = NationalValueID
	,NationalDischargeMethodCode = NationalValueCode
	,NationalDischargeMethod = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'DISMTH'






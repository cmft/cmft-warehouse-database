﻿



CREATE view [APC].[DischargeDestination] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceDischargeDestinationID = SourceValueID
	,SourceDischargeDestinationCode = SourceValueCode
	,SourceDischargeDestination = SourceValue
	,LocalDischargeDestinationID = LocalValueID
	,LocalDischargeDestinationCode = LocalValueCode
	,LocalDischargeDestination = LocalValue
	,NationalDischargeDestinationID = NationalValueID
	,NationalDischargeDestinationCode = NationalValueCode
	,NationalDischargeDestination = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'DISDES'






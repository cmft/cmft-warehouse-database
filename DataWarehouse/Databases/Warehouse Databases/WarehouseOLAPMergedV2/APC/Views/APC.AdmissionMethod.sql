﻿







CREATE view [APC].[AdmissionMethod] as

select
	 SourceContextCode
	,SourceContext
	,SourceAdmissionMethodID
	,SourceAdmissionMethodCode
	,SourceAdmissionMethod
	,LocalAdmissionMethodID
	,LocalAdmissionMethodCode
	,LocalAdmissionMethod
	,NationalAdmissionMethodID
	,NationalAdmissionMethodCode
	,NationalAdmissionMethod
	,AdmissionTypeCode
	,AdmissionType = 
		case AdmissionTypeCode
		when 'EL' then 'Elective'
		when 'EM' then 'Emergency'
		when 'MAT' then 'Maternity'
		when 'OTH' then 'Other Non-Elective'
		end
from
	(
	SELECT
		 SourceContextCode
		,SourceContext
		,SourceAdmissionMethodID = SourceValueID
		,SourceAdmissionMethodCode = SourceValueCode
		,SourceAdmissionMethod = SourceValue
		,LocalAdmissionMethodID = LocalValueID
		,LocalAdmissionMethodCode = LocalValueCode
		,LocalAdmissionMethod = LocalValue
		,NationalAdmissionMethodID = NationalValueID
		,NationalAdmissionMethodCode = NationalValueCode
		,NationalAdmissionMethod = NationalValue
		,AdmissionTypeCode = 
			case NationalValueCode
			when '11' then 'EL'
			when '12' then 'EL'
			when '13' then 'EL'
			when '21' then 'EM'
			when '22' then 'EM'
			when '23' then 'EM'
			when '24' then 'EM'
			when '28' then 'EM'
			when '31' then 'MAT'
			when '32' then 'MAT'
			when '81' then 'OTH'
			when '82' then 'OTH'
			when '83' then 'OTH'
			when '99' then 'OTH'
			else 'OTH'
			end
	FROM
		WH.Member
	where
		AttributeCode = 'ADMMTH'
	) AdmissionMethod









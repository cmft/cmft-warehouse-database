﻿


CREATE view [APC].[BedDayWard]

as

select
	[SourceWardCode]
	,[PODCode]
	,[IsRealtimePOD]
	,[HRGCode]
	,[ContextCode]
	,FromDate
	,ToDate
from
	[APC].[BedDayWardBase]



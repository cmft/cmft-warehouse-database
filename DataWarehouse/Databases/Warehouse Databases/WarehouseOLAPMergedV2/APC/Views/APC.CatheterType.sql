﻿


create view [APC].[CatheterType] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourceCatheterTypeID = Member.SourceValueID
	,SourceCatheterTypeCode = Member.SourceValueCode
	,SourceCatheterType = Member.SourceValue
	,LocalCatheterTypeID = Member.LocalValueID
	,LocalCatheterTypeCode = Member.LocalValueCode
	,LocalCatheterType = Member.LocalValue
	,NationalCatheterTypeID = Member.NationalValueID
	,NationalCatheterTypeCode = Member.NationalValueCode
	,NationalCatheterType = Member.NationalValue

FROM
	WH.Member

where 
	Member.AttributeCode = 'CATHTYPE'



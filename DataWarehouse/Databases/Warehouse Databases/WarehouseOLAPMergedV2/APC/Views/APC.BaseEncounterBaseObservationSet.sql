﻿


CREATE view [APC].[BaseEncounterBaseObservationSet] as 

--Uses the link table already in classic and translates the recnos to merge recnos

select
	BaseEncounter.MergeEncounterRecno
	,BaseObservationSet.MergeObservationSetRecno
from
	[$(Warehouse)].APC.EncounterObservationSet

inner join APC.BaseEncounter
on	EncounterObservationSet.EncounterRecno = BaseEncounter.EncounterRecno
and	BaseEncounter.ContextCode = 'CEN||PAS'

inner join Observation.BaseObservationSet
on	EncounterObservationSet.ObservationSetRecno = BaseObservationSet.ObservationSetRecno



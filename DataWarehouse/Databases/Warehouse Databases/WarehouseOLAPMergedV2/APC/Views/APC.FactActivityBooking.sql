﻿

















CREATE view [APC].[FactActivityBooking] as

select
	 WaitingList.MergeEncounterRecno
	,WaitingList.TCIDateID
	,SourceID = ActivitySource.SourceID
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable = 1
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
				--(
				--select
				--	AllocationID
				--from
				--	Allocation.Allocation 
				--where
				--	SourceAllocationID = '-1'
				--and	AllocationTypeID = 5
				--)
	,WaitingList.DirectorateID
	,SpecialtyID = WaitingList.SpecialtyID
	,WaitingList.ServiceID
	,WaitingList.Cases
	,WaitingList.CensusDateID

from
	APCWL.FactEncounter WaitingList

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 
--admissions
		case
		when WaitingList.IntendedManagementID in
			(
			select
				 IntendedManagement.SourceIntendedManagementID
			from
				APC.IntendedManagement
			where
				IntendedManagement.NationalIntendedManagementCode = '1'	--Inpatient
			)
		then 'IESPL'
		else 'DCSPL'
		end

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	WaitingList.TCIDateID = Calendar.DateID

inner join WH.Specialty
on	Specialty.SourceSpecialtyID = WaitingList.SpecialtyID

inner join WH.Census
on	WaitingList.CensusDateID = Census.CensusDateID
and CensusDate = 
	(
	select
		max(CensusDate)
	from
		APCWL.FactEncounter WaitingList

	inner join WH.Census
	on	WaitingList.CensusDateID = Census.CensusDateID
	)
	
inner join Allocation.DatasetAllocation PointOfDelivery
on	PointOfDelivery.DatasetRecno = WaitingList.MergeEncounterRecno
and	PointOfDelivery.DatasetCode = 'APCWAIT'
and	PointOfDelivery.AllocationTypeID = 5

where
	(
		WaitingList.WaitTypeID = 10 --Active
	or	WaitingList.WaitTypeID = 20 --Planned
	)
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

----exclude well babies
--and	not exists
--	(
--	select
--		1
--	from
--		APCWL.FactEncounter WellBaby

--	inner join APC.Metric
--	on	Metric.MetricID = WellBaby.MetricID
--	and	Metric.MetricCode = 'WELLBABY'

--	where
--		WellBaby.MergeEncounterRecno = WaitingList.MergeEncounterRecno
--	)

--exclude specialties
and	WaitingList.SpecialtyID not in
	(
	select
		 Specialty.SourceSpecialtyID
	from
		WH.Specialty
	where
		(
--exclude IVF
			(
				Specialty.SourceSpecialtyCode = 'GYN1'
			and	Specialty.SourceContextCode = 'CEN||PAS'
			)
--exclude bowel screening
		or
			(
				Specialty.SourceSpecialtyCode = 'BOWL'
			and	Specialty.SourceContextCode = 'CEN||PAS'
			)
--exclude national specialties
		or
			Specialty.NationalSpecialtyCode in
			(
			 '990' --"Other" specialties
			,'192' --Critical care
			)
		)
	)

--exclude SCBU
and	not
	(
		WaitingList.SpecialtyID in
		(
		select
			 Specialty.SourceSpecialtyID
		from
			WH.Specialty
		where
			Specialty.NationalSpecialtyCode in
			(
			 '422' --Neonatology
			,'424' --Well Babies
			)
		)
	and	WaitingList.DirectorateID in
		(
		select
			 Directorate.DirectorateID
		from
			WH.Directorate
		where
			Directorate.DirectorateCode =
				'30' --Women and Children's
		)
	)

and	WaitingList.WaitTargetID = 26

and	WaitingList.TCIDateID !=
		(
		select
			DateID
		from
			WH.Calendar
		where
			TheDate = '1 Jan 1900' --load TCIs only
		)
















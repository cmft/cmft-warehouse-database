﻿create view
APC.BedDaySpecialty

as

select
	[SourceSpecialtyCode]
	,[HRGCode]
	,[PODCode]
	,[ContextCode]
	,[FromDate]
	,[ToDate]
from
	[APC].[BedDaySpecialtyBase]

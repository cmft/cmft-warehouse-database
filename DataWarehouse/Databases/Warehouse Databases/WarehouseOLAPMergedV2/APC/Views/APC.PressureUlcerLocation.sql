﻿


create view [APC].[PressureUlcerLocation] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourcePressureUlcerLocationID = Member.SourceValueID
	,SourcePressureUlcerLocationCode = Member.SourceValueCode
	,SourcePressureUlcerLocation = Member.SourceValue
	,LocalPressureUlcerLocationID = Member.LocalValueID
	,LocalPressureUlcerLocationCode = Member.LocalValueCode
	,LocalPressureUlcerLocation = Member.LocalValue
	,NationalPressureUlcerLocationID = Member.NationalValueID
	,NationalPressureUlcerLocationCode = Member.NationalValueCode
	,NationalPressureUlcerLocation = Member.NationalValue

FROM
	WH.Member

where 
	Member.AttributeCode = 'PRESSULCRLOC'



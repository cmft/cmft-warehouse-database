﻿
create view APC.Operation

as

SELECT [MergeEncounterRecno]
      ,[SequenceNo]
      ,[OperationCode]
      ,[OperationDate]
      ,[Created]
      ,[Updated]
      ,[ByWhom]
  FROM [APC].[BaseOperation]

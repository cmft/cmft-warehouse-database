﻿



CREATE view [APC].[FactReadmission] as

select
	 ReadmissionTypeID 
	,ReadmissionIntervalDays 
	,ReadmissionEncounterMergeEncounterRecno 
	,ReadmissionDirectorateID 
	,ReadmissionAdmissionMethodID 
	,ReadmissionSpecialtyID 
	,ReadmissionAdmissionDateID 
	,ReadmissionDischargeDateID
	,ReadmissionConsultantID 
	,ReadmissionSiteID 
	,ReadmissionAgeID 
	,ReadmissionCCGID
	,ReadmissionPatientCategoryID
	,ReadmissionStartWardID
	,InitialAdmissionEncounterMergeEncounterRecno 
	,InitialAdmissionDischargeDateID 
	,InitialAdmissionConsultantID 
	,InitialAdmissionDirectorateID 
	,InitialAdmissionSpecialtyID 
	,InitialAdmissionSiteID 
	,InitialAdmissionAdmissionMethodID 
	,InitialAdmissionAgeID 
	,InitialAdmissionCCGID
	,InitialAdmissionPatientCategoryID
	,InitialAdmissionEndWardID
	,TrustWide

	,ReadmittedToSameSpecialty =
		cast(
			case
			when ReadmissionSpecialtyID = InitialAdmissionSpecialtyID
			then 1
			else 0
			end

			as bit
		)

	,ReadmittedToSameWard =
		cast(
			case
			when ReadmissionStartWardID = InitialAdmissionEndWardID
			then 1
			else 0
			end

			as bit
		)
from
	APC.FactReadmissionBase FactReadmission
where
	not exists
	(
	select
		1
	from
		APC.FactReadmissionBase Previous
	where
		Previous.InitialAdmissionEncounterMergeEncounterRecno = FactReadmission.InitialAdmissionEncounterMergeEncounterRecno
	and	Previous.ReadmissionTypeID = FactReadmission.ReadmissionTypeID
	and	Previous.TrustWide = FactReadmission.TrustWide
	and	Previous.ReadmissionEncounterMergeEncounterRecno <> FactReadmission.ReadmissionEncounterMergeEncounterRecno
	and	(
			Previous.ReadmissionAdmissionTime < FactReadmission.ReadmissionAdmissionTime
		or	(
				Previous.ReadmissionAdmissionTime = FactReadmission.ReadmissionAdmissionTime
			and	Previous.ReadmissionEncounterMergeEncounterRecno < FactReadmission.ReadmissionEncounterMergeEncounterRecno
			)
		)
	)

and	not exists
	(
	select
		1
	from
		APC.FactReadmissionBase Previous
	where
		Previous.ReadmissionEncounterMergeEncounterRecno = FactReadmission.ReadmissionEncounterMergeEncounterRecno
	and	Previous.ReadmissionTypeID = FactReadmission.ReadmissionTypeID
	and	Previous.TrustWide = FactReadmission.TrustWide
	and	Previous.InitialAdmissionEncounterMergeEncounterRecno <> FactReadmission.InitialAdmissionEncounterMergeEncounterRecno
	and	(
			Previous.InitialAdmissionAdmissionTime > FactReadmission.InitialAdmissionAdmissionTime
		or	(
				Previous.InitialAdmissionAdmissionTime = FactReadmission.InitialAdmissionAdmissionTime
			and	Previous.InitialAdmissionEncounterMergeEncounterRecno > FactReadmission.InitialAdmissionEncounterMergeEncounterRecno
			)
		)
	)





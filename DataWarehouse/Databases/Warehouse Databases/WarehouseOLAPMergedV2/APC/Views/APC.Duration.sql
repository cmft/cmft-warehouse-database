﻿
CREATE view [APC].[Duration]
as

select
	[DurationCode]
	,[OccupiedBedDaysCode]
	,CodingLengthOfStayBand = 
				case
					when [DurationCode] between 0 and 4
					then '0-4 Days'
					when [DurationCode] between 5 and 9
					then '5-9 Days'
					when [DurationCode] between 10 and 14
					then '10-14 Days'
					when [DurationCode] between 15 and 19
					then '15-19 Days'
					when [DurationCode] >= 20
					then '20 Days+'
				end
from
	[APC].[DurationBase]



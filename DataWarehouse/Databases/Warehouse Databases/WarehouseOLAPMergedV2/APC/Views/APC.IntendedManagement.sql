﻿



CREATE view [APC].[IntendedManagement] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceIntendedManagementID = SourceValueID
	,SourceIntendedManagementCode = SourceValueCode
	,SourceIntendedManagement = SourceValue
	,LocalIntendedManagementID = LocalValueID
	,LocalIntendedManagementCode = LocalValueCode
	,LocalIntendedManagement = LocalValue
	,NationalIntendedManagementID = NationalValueID
	,NationalIntendedManagementCode = NationalValueCode
	,NationalIntendedManagement = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'MGTINT'






﻿



CREATE view [APC].[NeonatalLevelOfCare] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceNeonatalLevelOfCareID = SourceValueID
	,SourceNeonatalLevelOfCareCode = SourceValueCode
	,SourceNeonatalLevelOfCare = SourceValue
	,LocalNeonatalLevelOfCareID = LocalValueID
	,LocalNeonatalLevelOfCareCode = LocalValueCode
	,LocalNeonatalLevelOfCare = LocalValue
	,NationalNeonatalLevelOfCareID = NationalValueID
	,NationalNeonatalLevelOfCareCode = NationalValueCode
	,NationalNeonatalLevelOfCare = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'NEOLEV'






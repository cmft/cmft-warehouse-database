﻿









CREATE view [APC].[FactActivityDialysis]

as

-- 20150612	RR	changed to reference APC.Encounter rather than APC.BaseEncounter


select
	 Admission.MergeEncounterRecno
	,Admission.EncounterDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'APC'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlagAllocation.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.PointOfDeliveryID 
	,ContractFlagID = ContractFlag.AllocationID 
	--,DirectorateID = Admission.EndDirectorateID
	,DirectorateID = Directorate.DirectorateID -- Phil Huitson requested
	--,SpecialtyID = Admission.SpecialtyID
	,SpecialtyID = Specialty.SourceSpecialtyID -- Phil Huitson requested
	,Admission.ServiceID
	,HRGID = HRGBase.HRGID
	,Admission.ContextID
	,Admission.Cases
	,Value = null

from
	APC.FactEncounter Admission

--inner join APC.BaseEncounter Base
--on	Base.MergeEncounterRecno = Admission.MergeEncounterRecno
--and Base.Reportable = 1

--inner join APC.BaseEncounterReference BaseReference
--on	BaseReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.Encounter Base
on	Base.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.BaseWardStay
on	BaseWardStay.ProviderSpellNo = Base.ProviderSpellNo
and	BaseWardStay.ContextCode = Base.ContextCode

inner join APC.Metric APCMetric
on	APCMetric.MetricID = Admission.MetricID
and	APCMetric.MetricCode in
	(
	 'DCSPL'
	,'EMSPL'
	,'IESPL'
	,'MATSPL'
	,'NEOTHSPL'
	--,'RNSPL
	,'RDSPL' -- 20150518 discussed with PH, Dialysis activity coming through as Regular days.
	)

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 
		case
		when DischargeDate < '1 Apr 2015' then 'RDHOSP-A'
		else 'RDHOSP'
		end

inner join WH.PointOfDelivery
on	PointOfDeliveryCode = 
		case
		when DischargeDate < '1 Apr 2015' then 'RDHOSP-A'
		else 'RDHOSP'
		end

inner join Allocation.DatasetAllocation ContractFlag
on	ContractFlag.DatasetRecno = Admission.MergeEncounterRecno
and	ContractFlag.DatasetCode = 'APC'
and	ContractFlag.AllocationTypeID = 9

inner join Allocation.Allocation ContractFlagAllocation
on	ContractFlag.AllocationID = ContractFlagAllocation.AllocationID

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	Admission.EncounterDateID = Calendar.DateID

inner join APC.HRG4Encounter -- need hrgs in fact main tables!
on HRG4Encounter.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(HRG4Encounter.HRGCode, 'N/A')

--inner join WH.Specialty
--on	Specialty.SourceSpecialtyID = Admission.SpecialtyID

inner join WH.Specialty -- Phil Huitson requested
on	Specialty.SourceSpecialtyCode = 'NEPH'
and	Specialty.SourceContextCode = 'CEN||PAS'

inner join WH.Directorate -- Phil Huitson requested
on	Directorate.DirectorateCode = '24' 

where
	BaseWardStay.WardCode in 
							(
							'IHNM'
							,'IHP'
							,'IHT'
							,'IHW'
							,'IHM'
							,'TDU'
							,'IHA'
							,'IH'
							,'IHS'
							)		
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')					
								
union all

select
	 Admission.Contracting_ID
	,Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'APC'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end
					as bit)
	,Reportable = 
			cast(
				case 
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				when PoD_Code in ('NELXBD', 'NELNEXBD', 'ELXBD', 'BPT', 'BPT-UC', 'RT-UB')
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	--,SpecialtyID = Specialty.NationalSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Context.ContextID
	,Cases = Activity_Actual
	,Value = Price_Actual

from
	SLAM.Encounter Admission

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 
		case
			when Admission.Spell_Discharge_Date < '1 Apr 2015' then 'RDHOSP-A'
			else 'RDHOSP'
		end

left join Allocation.Allocation PointOfDelivery -- need default POD codes in allocations
on	PointOfDelivery.SourceAllocationID = Admission.PoD_Code
and	PointOfDelivery.AllocationTypeID = 5

left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = Admission.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	Admission.Spell_Discharge_Date = Calendar.TheDate

inner join WH.Directorate
on	Directorate.DirectorateCode = Admission.Directorate_Code

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = Admission.Internal_Specialty_Code
and	Specialty.SourceContextCode = Admission.ContextCode

inner join WH.Context
on	Admission.ContextCode = Context.ContextCode

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(Admission.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = Admission.Feed_Name
--and LatestExtractMonth.Extract_Type = Admission.Extract_Type
--and LatestExtractMonth.Extract_Month = Admission.Extract_Month
--and LatestExtractMonth.Extract_Year = Admission.Extract_Year

where
	Admission.Feed_Name = 'Dialysis'
and	Admission.Extract_Type = 'Freeze'
--and	Admission.Is_Current = 1
and	Admission.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')





















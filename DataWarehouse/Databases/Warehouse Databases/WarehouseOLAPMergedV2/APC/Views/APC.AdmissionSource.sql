﻿




CREATE view [APC].[AdmissionSource] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAdmissionSourceID = SourceValueID
	,SourceAdmissionSourceCode = SourceValueCode
	,SourceAdmissionSource = SourceValue
	,LocalAdmissionSourceID = LocalValueID
	,LocalAdmissionSourceCode = LocalValueCode
	,LocalAdmissionSource = LocalValue
	,NationalAdmissionSourceID = NationalValueID
	,NationalAdmissionSourceCode = NationalValueCode
	,NationalAdmissionSource = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ADMSRC'







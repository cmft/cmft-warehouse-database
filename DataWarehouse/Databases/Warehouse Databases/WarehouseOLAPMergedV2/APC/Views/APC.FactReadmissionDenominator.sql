﻿







CREATE view [APC].[FactReadmissionDenominator] as

select
	 ReadmissionTypeID = 2 --CQUIN

	,SpellEncounterMergeEncounterRecno = Discharge.MergeEncounterRecno
	,SpellAdmissionDateID = DischargeReference.AdmissionDateID
	,SpellDischargeDateID = DischargeReference.DischargeDateID
	,SpellConsultantID = SpellEncounterReference.ConsultantID
	,SpellDirectorateID = SpellEncounterDirectorate.DirectorateID
	,SpellSpecialtyID = SpellEncounterReference.SpecialtyID
	,SpellSiteID = SpellEncounterReference.StartSiteID
	,SpellAdmissionMethodID = SpellEncounterReference.AdmissionMethodID
	,SpellAgeID = SpellEncounterReference.AgeID
	,SpellCCGID = SpellEncounterReference.CCGID
	,SpellEndWardID = SpellEncounterReference.EndWardID
from
	APC.BaseEncounter SpellEncounter

inner join APC.BaseEncounterReference SpellEncounterReference
on	SpellEncounterReference.MergeEncounterRecno = SpellEncounter.MergeEncounterRecno
and SpellEncounter.Reportable = 1

inner join APC.AdmissionMethod SpellAdmissionMethod
on	SpellAdmissionMethod.SourceAdmissionMethodID = SpellEncounterReference.AdmissionMethodID

inner join APC.DischargeMethod SpellDischargeMethod
on	SpellDischargeMethod.SourceDischargeMethodID = SpellEncounterReference.DischargeMethodID

inner join WH.Specialty SpellSpecialty
on	SpellSpecialty.SourceSpecialtyID = SpellEncounterReference.SpecialtyID

inner join APC.PatientClassification SpellPatientClassification
on	SpellPatientClassification.SourcePatientClassificationID = SpellEncounterReference.PatientClassificationID

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = SpellEncounter.MergeEncounterRecno

inner join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join WH.Directorate SpellEncounterDirectorate
on	SpellEncounterDirectorate.DirectorateCode = Discharge.EndDirectorateCode

where
	SpellEncounter.GlobalEpisodeNo = 1

and	SpellPatientClassification.NationalPatientClassificationCode not in ('2', '3', '4') --daycase, regular day, regular night
and	SpellDischargeMethod.NationalDischargeMethodCode <> '4' --died

and	SpellSpecialty.NationalSpecialtyCode not in
	(
	 '501'
	,'560'
	,'610'
	,'700'
	,'701'
	,'702'
	,'703'
	,'704'
	,'705'
	,'706'
	,'707'
	,'708'
	,'709'
	,'710'
	,'711'
	,'712'
	,'713'
	,'714'
	,'715'
	)

and	left(SpellEncounter.PrimaryDiagnosisCode, 1) <> 'O'

and	not exists
	(
	select
		1
	from
		APC.BaseEncounter

	inner join APC.BaseDiagnosis
	on	BaseDiagnosis.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	and BaseEncounter.Reportable = 1
	
	where
		BaseEncounter.SourcePatientNo = SpellEncounter.SourcePatientNo
	and	BaseEncounter.ContextCode = SpellEncounter.ContextCode
	and	datediff(day, BaseEncounter.AdmissionDate, SpellEncounter.AdmissionDate) between 0 and 365
	and	BaseEncounter.AdmissionTime <= SpellEncounter.AdmissionTime

	and	(
			left(BaseDiagnosis.DiagnosisCode, 3) between 'C00' and 'C97'
		or	left(BaseDiagnosis.DiagnosisCode, 3) between 'D37' and 'D48'
		or	BaseDiagnosis.DiagnosisCode = 'Z51.1'
		)
	)


union all


select
	 ReadmissionTypeID = 1 --Commissioning (30 Day)

	,SpellEncounterMergeEncounterRecno = Discharge.MergeEncounterRecno
	,SpellAdmissionDateID = DischargeReference.AdmissionDateID
	,SpellDischargeDateID = DischargeReference.DischargeDateID
	,SpellConsultantID = SpellEncounterReference.ConsultantID
	,SpellDirectorateID = SpellEncounterDirectorate.DirectorateID
	,SpellSpecialtyID = SpellEncounterReference.SpecialtyID
	,SpellSiteID = SpellEncounterReference.StartSiteID
	,SpellAdmissionMethodID = SpellEncounterReference.AdmissionMethodID
	,SpellAgeID = SpellEncounterReference.AgeID
	,SpellCCGID = SpellEncounterReference.CCGID
	,SpellEndWardID = SpellEncounterReference.EndWardID
from
	APC.BaseEncounter SpellEncounter

inner join APC.BaseEncounterReference SpellEncounterReference
on	SpellEncounterReference.MergeEncounterRecno = SpellEncounter.MergeEncounterRecno
and SpellEncounter.Reportable = 1

inner join APC.AdmissionMethod SpellAdmissionMethod
on	SpellAdmissionMethod.SourceAdmissionMethodID = SpellEncounterReference.AdmissionMethodID

inner join WH.Specialty SpellSpecialty
on	SpellSpecialty.SourceSpecialtyID = SpellEncounterReference.SpecialtyID

inner join APC.PatientClassification SpellPatientClassification
on	SpellPatientClassification.SourcePatientClassificationID = SpellEncounterReference.PatientClassificationID

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = SpellEncounter.MergeEncounterRecno

inner join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join WH.Directorate SpellEncounterDirectorate
on	SpellEncounterDirectorate.DirectorateCode = Discharge.EndDirectorateCode

inner join APC.DischargeMethod SpellDischargeMethod
on	SpellDischargeMethod.SourceDischargeMethodID = DischargeReference.DischargeMethodID

--the HRG grouped encounter may not be the discharge encounter so link through via the ProviderSpellNo
inner join APC.BaseEncounter SpellHRGEncounter
on	SpellHRGEncounter.GlobalProviderSpellNo = SpellEncounter.GlobalProviderSpellNo

inner join APC.HRG4Spell SpellHRG4Spell
on	SpellHRG4Spell.MergeEncounterRecno = SpellHRGEncounter.MergeEncounterRecno

where
	SpellEncounter.GlobalEpisodeNo = 1
and	SpellEncounter.DischargeDate is not null


--Exclusions...

--Either the initial admission or readmission is cross-border activity
and	not exists
	(
	select
		1
	from
		[$(Organisation)].ODS.Postcode
	where
		Postcode.Postcode = SpellEncounter.Postcode
	and	(
			left(Postcode.DistrictOfResidence, 1) in ('S', 'Z')
		or	left(Postcode.CCGCode, 1) = '7'
		)
	)

--Either the initial admission or readmission has an unbundled HRG in subchapter SB or SC (chemotherapy or radiotherapy)
and	not exists
	(
	select
		1
	from
		APC.HRG4Unbundled

	inner join APC.BaseEncounter
	on	BaseEncounter.GlobalProviderSpellNo = SpellEncounter.ProviderSpellNo
	and	BaseEncounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
	and BaseEncounter.Reportable = 1
	
	where
		left(HRG4Unbundled.HRGCode, 2) in ('SB', 'SC') --chemotherapy or radiotherapy
	)

--Either the initial admission or readmission has a spell primary diagnosis of C00-C97 or D37-D48
and	SpellHRG4Spell.PrimaryDiagnosisCode not between 'C00' and 'C97'
and	SpellHRG4Spell.PrimaryDiagnosisCode not between 'D37' and 'D48'

--Either the initial admission or the readmission has a spell core HRG in chapter N (obstetric medicine)
and	left(SpellHRG4Spell.HRGCode, 1) <> 'N' --(obstetric medicine)

--Readmission spell is excluded from PbR (e.g. via TFC or HRG exclusions)
and	SpellHRG4Spell.HRGCode not in
	(
	select
		EntityLookup.EntityCode
	from
		Utility.EntityLookup
	where
		EntityLookup.EntityTypeCode = 'HRGEXCLUSIONS'
	)

and	SpellSpecialty.NationalSpecialtyCode not in
	(
	select
		EntityLookup.EntityCode
	from
		Utility.EntityLookup
	where
		EntityLookup.EntityTypeCode = 'TFCEXCLUSIONS'
	)

and	not exists
	(
	select
		1
	from
		Allocation.DatasetAllocation
	where
	--DatasetAllocation.AllocationTypeID = '4'
	--and
		DatasetAllocation.DatasetCode = 'APC'
	and	DatasetAllocation.DatasetRecno = SpellEncounter.MergeEncounterRecno
	--and	DatasetAllocation.AllocationID <> 2088516 --N/A
	and	DatasetAllocation.AllocationID in(2088688, 2088746, 2088801,2088773,2088774)     

	)

--The readmission is an emergency transfer (admission method code 2B)
and	SpellAdmissionMethod.NationalAdmissionMethodCode <> '28'

--The readmission has a start age of less than 4
and	datediff(
		 year
		,SpellEncounter.DateOfBirth
		,SpellEncounter.AdmissionDate
	) -
	case
	when DATEPART(month, SpellEncounter.DateOfBirth) > DATEPART(month, SpellEncounter.AdmissionDate)
	then 0
	else 1
	end

	 > 3

--The patient is being readmitted having self discharged against clinical advice (included in discharge method code 2)
and	SpellDischargeMethod.NationalDischargeMethodCode <> '2'

--The patient is receiving renal dialysis
and	not
	(
		SpellPatientClassification.NationalPatientClassificationCode in( '4','3')	--Regular Day Admission
	--and	SpellSpecialty.SourceSpecialtyCode like 'IH%'
	--and	SpellSpecialty.NationalSpecialtyCode in ('259', '361')

--Renal Dialysis Excluded HRGs (13/14):
	and	SpellHRG4Spell.HRGCode in
		(
		 'LA08E'
		,'LD01A'
		,'LD02A'
		,'LD03A'
		,'LD04A'
		,'LD05A'
		,'LD06A'
		,'LD07A'
		,'LD08A'
		,'LD09A'
		,'LD10A'
		,'LD11A'
		,'LD12A'
		,'LD13A'
		,'LD01B'
		,'LD02B'
		,'LD03B'
		,'LD04B'
		,'LD05B'
		,'LD06B'
		,'LD07B'
		,'LD08B'
		,'LD09B'
		,'LD10B'
		,'LD11B'
		,'LD12B'
		,'LD13B'
		)
	)


--The patient is being readmitted subsequent to a transplant
and not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.GlobalProviderSpellNo = SpellEncounter.GlobalProviderSpellNo
	and BaseEncounter.Reportable = 1

--Transplant excluded HRGs (13/14):
	and	SpellHRG4Spell.HRGCode in
		(
		 'DZ01Z'
		,'EA01Z'
		,'EA02Z'
		,'GA01A'
		,'GA01B'
		,'GA01C'
		,'GA12Z'
		,'LA01A'
		,'LA01B'
		,'LA02A'
		,'LA02B'
		,'LA03A'
		,'LA03B'
		,'LA11Z'
		,'LA12A'
		,'LA12B'
		,'LA13A'
		,'LA13B'
		,'LA14Z'
		,'SA19A'
		,'SA19B'
		,'SA20A'
		,'SA20B'
		,'SA21A'
		,'SA21B'
		,'SA22A'
		,'SA22B'
		,'SA23A'
		,'SA23B'
		,'SA26A'
		,'SA26B'
		,'SA27A'
		,'SA27B'
		,'SA28A'
		,'SA28B'
		)
	)


union all

select
	 ReadmissionTypeID = 3 --7 Day Readmissions

	,SpellEncounterMergeEncounterRecno = Discharge.MergeEncounterRecno
	,SpellAdmissionDateID = DischargeReference.AdmissionDateID
	,SpellDischargeDateID = DischargeReference.DischargeDateID
	,SpellConsultantID = SpellEncounterReference.ConsultantID
	,SpellDirectorateID = SpellEncounterDirectorate.DirectorateID
	,SpellSpecialtyID = SpellEncounterReference.SpecialtyID
	,SpellSiteID = SpellEncounterReference.StartSiteID
	,SpellAdmissionMethodID = SpellEncounterReference.AdmissionMethodID
	,SpellAgeID = SpellEncounterReference.AgeID
	,SpellCCGID = SpellEncounterReference.CCGID
	,SpellEndWardID = SpellEncounterReference.EndWardID
from
	APC.BaseEncounter SpellEncounter

inner join APC.BaseEncounterReference SpellEncounterReference
on	SpellEncounterReference.MergeEncounterRecno = SpellEncounter.MergeEncounterRecno
and SpellEncounter.Reportable = 1

inner join APC.AdmissionMethod SpellAdmissionMethod
on	SpellAdmissionMethod.SourceAdmissionMethodID = SpellEncounterReference.AdmissionMethodID

inner join APC.DischargeMethod SpellDischargeMethod
on	SpellDischargeMethod.SourceDischargeMethodID = SpellEncounterReference.DischargeMethodID

inner join WH.Specialty SpellSpecialty
on	SpellSpecialty.SourceSpecialtyID = SpellEncounterReference.SpecialtyID

inner join APC.PatientClassification SpellPatientClassification
on	SpellPatientClassification.SourcePatientClassificationID = SpellEncounterReference.PatientClassificationID

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = SpellEncounter.MergeEncounterRecno

inner join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join WH.Directorate SpellEncounterDirectorate
on	SpellEncounterDirectorate.DirectorateCode = Discharge.EndDirectorateCode

where
	SpellEncounter.GlobalEpisodeNo = 1


union all

select
	 ReadmissionTypeID = 4 --48 Hour Readmissions

	,SpellEncounterMergeEncounterRecno = Discharge.MergeEncounterRecno
	,SpellAdmissionDateID = DischargeReference.AdmissionDateID
	,SpellDischargeDateID = DischargeReference.DischargeDateID
	,SpellConsultantID = SpellEncounterReference.ConsultantID
	,SpellDirectorateID = SpellEncounterDirectorate.DirectorateID
	,SpellSpecialtyID = SpellEncounterReference.SpecialtyID
	,SpellSiteID = SpellEncounterReference.StartSiteID
	,SpellAdmissionMethodID = SpellEncounterReference.AdmissionMethodID
	,SpellAgeID = SpellEncounterReference.AgeID
	,SpellCCGID = SpellEncounterReference.CCGID
	,SpellEndWardID = SpellEncounterReference.EndWardID
from
	APC.BaseEncounter SpellEncounter

inner join APC.BaseEncounterReference SpellEncounterReference
on	SpellEncounterReference.MergeEncounterRecno = SpellEncounter.MergeEncounterRecno
and SpellEncounter.Reportable = 1

inner join APC.AdmissionMethod SpellAdmissionMethod
on	SpellAdmissionMethod.SourceAdmissionMethodID = SpellEncounterReference.AdmissionMethodID

inner join APC.DischargeMethod SpellDischargeMethod
on	SpellDischargeMethod.SourceDischargeMethodID = SpellEncounterReference.DischargeMethodID

inner join WH.Specialty SpellSpecialty
on	SpellSpecialty.SourceSpecialtyID = SpellEncounterReference.SpecialtyID

inner join APC.PatientClassification SpellPatientClassification
on	SpellPatientClassification.SourcePatientClassificationID = SpellEncounterReference.PatientClassificationID

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = SpellEncounter.MergeEncounterRecno

inner join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join WH.Directorate SpellEncounterDirectorate
on	SpellEncounterDirectorate.DirectorateCode = Discharge.EndDirectorateCode

where
	SpellEncounter.GlobalEpisodeNo = 1









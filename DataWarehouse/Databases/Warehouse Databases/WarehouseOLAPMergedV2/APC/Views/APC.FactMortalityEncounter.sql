﻿






CREATE View [APC].[FactMortalityEncounter]

as


with DeceasedMetric
	(
	 MetricID
	)
as
	(
	select
		 MetricID
	from
		APC.Metric
	where
		MetricCode = 'Death'
		
	union all

	select
		 Metric.MetricID
	from
		APC.Metric

	inner join DeceasedMetric
	on	DeceasedMetric.MetricID = Metric.MetricParentID

	)


Select 
	FactEncounter.MergeEncounterRecno	
	,EncounterDateID	
	,FactEncounter.MetricID	
	,ConsultantID	
	,SpecialtyID	
	,SiteID	
	,AgeID	
	,AdmissionMethodID	
	,FactEncounter.RTTActivity	
	,FactEncounter.RTTTreated	
	,EncounterTimeOfDay	
	,DirectorateID	
	,SexID	
	,ISTAdmissionSpecialtyID	
	,FactEncounter.ISTAdmissionDemandTimeOfDay	
	,FactEncounter.ISTDischargeTimeOfDay	
	,ContextID	
	,AdmissionSourceID	
	,DischargeDestinationID	
	,DischargeMethodID	
	,IntendedManagementID	
	,NeonatalLevelOfCareID	
	,PatientClassificationID	
	,SubSpecialtyID	
	,CodingComplete	
	,VTE	
	,VTECategoryID	
	,VTEExclusionReasonID	
	,VTECompleteAndOrExclusion	
	,StartWardID	
	,EndWardID	
	,NationalExamID	
	,ServiceID	
	,CCGID	
	,PrimaryDiagnosisID	
	,FactEncounter.Cases	
	,LengthOfEncounter	
	,CasenoteMergeRecno	
	,ResidenceCCGID	
	,MergeDocumentRecno	
	,DischargeSummaryStatusID	
	,DischargeSummarySignedByID	
	,DischargeSummaryProductionTimeID	
	,DischargeSummaryExclusionReasonID	
	,DischargeSummaryRequired	
	,DischargeSummarySignedTimeID	
	,GPPracticeID	
	,ElectronicCorrespondenceActivated	
	,FactEncounter.CharlsonIndex	
	,PrimaryProcedureID	
	,HRGID	
	,ReligionID	
	,EthnicCategoryID	
	,MortalityReviewStatusID = coalesce(
									MortalityReview.ReviewStatusID
									,-1 -- Default, Not available
									)
	,PrimaryReviewOutstanding = coalesce(MortalityReview.PrimaryReviewOutstanding,convert(bit, 0)) -- If not available to review, default to 0
from 
	DeceasedMetric

inner join APC.FactEncounter
on DeceasedMetric.MetricID = FactEncounter.MetricID

inner join APC.BaseEncounter
on FactEncounter.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

left join
	(
	select 
		BaseEncounterBaseReview.MergeEncounterRecno
		,SourceUniqueID
		,ReviewStatusID
		,PrimaryReviewOutstanding = 
			CAST(
				case 
					when LocalReviewStatus in ('Completed','NotRequired') then 0
					when PrimaryReviewCompletedTime is not null then 0
					else 1
				end								
			as bit)
		,SourcePatientNo
		,EpisodeStartTime
		,APCContextCode
	from
		Mortality.BaseReview
	
	inner join Mortality.BaseReviewReference
	on BaseReview.MergeRecno = BaseReviewReference.MergeRecno
	
	inner join Mortality.ReviewStatus
	on BaseReviewReference.ReviewStatusID = ReviewStatus.SourceReviewStatusID
	
	inner join APC.BaseEncounterBaseReview
	on BaseEncounterBaseReview.MergeReviewRecno = BaseReview.MergeRecno

	where
		QuestionNo in (0,1)
	and not exists
			(
			select
				1
			from
				Mortality.BaseReview Latest
			where
				Latest.SourcePatientNo = BaseReview.SourcePatientNo
			and	Latest.SourceUniqueID > BaseReview.SourceUniqueID
			)
	) MortalityReview

on	MortalityReview.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno







﻿


create view [APC].[VTEAssessmentNotRequiredReason] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourceVTEAssessmentNotRequiredReasonID = Member.SourceValueID
	,SourceVTEAssessmentNotRequiredReasonCode = Member.SourceValueCode
	,SourceVTEAssessmentNotRequiredReason = Member.SourceValue
	,LocalVTEAssessmentNotRequiredReasonID = Member.LocalValueID
	,LocalVTEAssessmentNotRequiredReasonCode = Member.LocalValueCode
	,LocalVTEAssessmentNotRequiredReason = Member.LocalValue
	,NationalVTEAssessmentNotRequiredReasonID = Member.NationalValueID
	,NationalVTEAssessmentNotRequiredReasonCode = Member.NationalValueCode
	,NationalVTEAssessmentNotRequiredReason = Member.NationalValue

FROM
	WH.Member

where 
	Member.AttributeCode = 'VTEASSNOTREQDREASON'



﻿




CREATE view [APC].[LastEpisodeInSpell] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceLastEpisodeInSpellID = SourceValueID
	,SourceLastEpisodeInSpellCode = SourceValueCode
	,SourceLastEpisodeInSpell = SourceValue
	,LocalLastEpisodeInSpellID = LocalValueID
	,LocalLastEpisodeInSpellCode = LocalValueCode
	,LocalLastEpisodeInSpell = LocalValue
	,NationalLastEpisodeInSpellID = NationalValueID
	,NationalLastEpisodeInSpellCode = NationalValueCode
	,NationalLastEpisodeInSpell = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'LSTEPI'







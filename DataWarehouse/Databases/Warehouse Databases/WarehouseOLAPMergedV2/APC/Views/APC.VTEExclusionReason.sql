﻿create view APC.VTEExclusionReason as

select
	 VTEExclusionReasonID = Allocation.AllocationID
	,SourceVTEExclusionReasonID = Allocation.SourceAllocationID
	,VTEExclusionReason = Allocation.Allocation
from
	Allocation.Allocation

where
	Allocation.AllocationTypeID = 3



﻿

CREATE view [APC].[FactActivity] as

-- 20150612	RR	changed to reference APC.Encounter rather than APC.BaseEncounter

select
	 Admission.MergeEncounterRecno
	,Admission.EncounterDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'APC'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlagAllocation.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,DirectorateID = Admission.DirectorateID
	,SpecialtyID = Admission.SpecialtyID
	--,SpecialtyID = Specialty.NationalSpecialtyID
	,Admission.ServiceID
	,HRGID = HRGBase.HRGID
	,Admission.ContextID
	,Admission.Cases
	,Value = null
from
	APC.FactEncounter Admission

--inner join APC.BaseEncounter Base
--on	Base.MergeEncounterRecno = Admission.MergeEncounterRecno
--and Base.Reportable = 1

--inner join APC.BaseEncounterReference BaseReference
--on	BaseReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.Encounter Base
on Base.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.Metric APCMetric
on	APCMetric.MetricID = Admission.MetricID
and	APCMetric.MetricCode in
	(
	 'DCSPL'
	,'EMSPL'
	,'IESPL'
	,'MATSPL'
	,'NEOTHSPL'
	--,'RNSPL
	--,'RDSPL'
	)

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = APCMetric.MetricCode

-- 20150518 RR POD and HRG allocation should be linked to the Discharge episode (originally the admitting episode)

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

inner join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join Allocation.DatasetAllocation PointOfDelivery
on	PointOfDelivery.DatasetRecno = Discharge.MergeEncounterRecno
and	PointOfDelivery.DatasetCode = 'APC'
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.DatasetAllocation ContractFlag
on	ContractFlag.DatasetRecno = Discharge.MergeEncounterRecno
and	ContractFlag.DatasetCode = 'APC'
and	ContractFlag.AllocationTypeID = 9

--inner join Allocation.DatasetAllocation PointOfDelivery
--on	PointOfDelivery.DatasetRecno = Admission.MergeEncounterRecno
--and	PointOfDelivery.DatasetCode = 'APC'
--and	PointOfDelivery.AllocationTypeID = 5

--inner join Allocation.DatasetAllocation ContractFlag
--on	ContractFlag.DatasetRecno = Admission.MergeEncounterRecno
--and	ContractFlag.DatasetCode = 'APC'
--and	ContractFlag.AllocationTypeID = 9

inner join Allocation.Allocation ContractFlagAllocation
on	ContractFlag.AllocationID = ContractFlagAllocation.AllocationID

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	Admission.EncounterDateID = Calendar.DateID

--inner join APC.HRG4Encounter -- need hrgs in fact main tables!
--on HRG4Encounter.MergeEncounterRecno = Admission.MergeEncounterRecno 

-- 20150518	RR	discussed with PH, advised to bring through Spell HRG rather than admitting episode HRGs
inner join APC.HRG4Spell -- need hrgs in fact main tables!
on HRG4Spell.MergeEncounterRecno = Admission.MergeEncounterRecno 

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(HRG4Spell.HRGCode, 'N/A')

inner join WH.Specialty
on	Specialty.SourceSpecialtyID = Admission.SpecialtyID

where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 Base.MergeEncounterRecno
	,BaseReference.TreatmentDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'IUI'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable = cast(1 as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,Directorate.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,Service.AllocationID
	,HRGID = HRGBase.HRGID
	,BaseReference.ContextID
	,Cases = 1
	,Value = null

from
	IUI.BaseEncounter Base

inner join IUI.BaseEncounterReference BaseReference
on	BaseReference.MergeEncounterRecno = Base.MergeEncounterRecno

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = '502'
and	Specialty.SourceContextCode = Base.ContextCode

inner join Allocation.Allocation Service
on	Service.SourceAllocationID = '104'
and	Service.AllocationTypeID = 2

inner join WH.Directorate
on	Directorate.DirectorateCode = '35'

inner join Allocation.Allocation PointOfDelivery
on	PointOfDelivery.SourceAllocationID = 'FERTILITY'
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.Allocation ContractFlag
on	ContractFlag.SourceAllocationID = '-1'
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 'IESPL'

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	BaseReference.TreatmentDateID = Calendar.DateID

inner join WH.Treatment
on	BaseReference.TreatmentID = Treatment.SourceValueID

inner join WH.HRGBase
on	case Treatment.Treatment
	when 'Natural' then 'MC09Z'
	when 'Gonadotrophins' then 'MC07Z'
	else 'UZ01Z'
	end	= HRGCode                                       

where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 Admission.Contracting_ID
	,Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'APC'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end
					as bit)
	,Reportable = 
			cast(
				case 
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				when PoD_Code in ('NELXBD', 'NELNEXBD', 'ELXBD', 'BPT', 'BPT-UC', 'RT-UB')
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	--,SpecialtyID = Specialty.NationalSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Context.ContextID
	,Cases = Activity_Actual
	,Value = Price_Actual

from
	SLAM.Encounter Admission

inner join
		(
		select distinct
			NationalAdmissionMethodCode
			,AdmissionTypeCode
		from
			APC.AdmissionMethod
		) NationalAdmissionMethod
on	NationalAdmissionMethod.NationalAdmissionMethodCode = cast(Admission.AdmMethod as varchar)

inner join APC.Metric APCMetric
on	APCMetric.MetricCode =
	case 
	when NationalAdmissionMethod.AdmissionTypeCode = 'EL'
	then
		case Patient_Classification
		when '1' then 'IESPL'
		when '2' then 'DCSPL'
		when '3' then 'RDSPL'
		when '4' then 'RNSPL'
		end
	else
		case NationalAdmissionMethod.AdmissionTypeCode 
		when 'EM' then 'EMSPL'
		when 'MAT' then 'MATSPL'
		when 'OTH' then	'NEOTHSPL'
		end
	end

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = APCMetric.MetricCode

-- 20150518 RR discussed with Phil, POD Code relates to admitting, (changed above to discharge) Phil advised to leave as is, but we may need to change in the future.
left join Allocation.Allocation PointOfDelivery -- need default POD codes in allocations
on	PointOfDelivery.SourceAllocationID = Admission.PoD_Code
and	PointOfDelivery.AllocationTypeID = 5

left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = Admission.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	Admission.Spell_Discharge_Date = Calendar.TheDate

inner join WH.Directorate
on	Directorate.DirectorateCode = Admission.Directorate_Code

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = Admission.Internal_Specialty_Code
and	Specialty.SourceContextCode = Admission.ContextCode

--inner join 
--		(
--		select distinct
--			NationalSpecialtyID
--			,NationalSpecialtyCode
--		from
--			WH.Specialty
--		) Specialty
--on	Specialty.NationalSpecialtyCode = Admission.Treatment_Function_Code

inner join WH.Context
on	Admission.ContextCode = Context.ContextCode

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(Admission.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = Admission.Feed_Name
--and LatestExtractMonth.Extract_Type = Admission.Extract_Type
--and LatestExtractMonth.Extract_Month = Admission.Extract_Month
--and LatestExtractMonth.Extract_Year = Admission.Extract_Year

where
	Admission.Feed_Name = 'APC'
and	Admission.Extract_Type = 'Freeze'
--and	Admission.Is_Current = 1
and	Admission.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')













































﻿




CREATE view [APC].[PatientClassification] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourcePatientClassificationID = SourceValueID
	,SourcePatientClassificationCode = SourceValueCode
	,SourcePatientClassification = SourceValue
	,LocalPatientClassificationID = LocalValueID
	,LocalPatientClassificationCode = LocalValueCode
	,LocalPatientClassification = LocalValue
	,NationalPatientClassificationID = NationalValueID
	,NationalPatientClassificationCode = NationalValueCode
	,NationalPatientClassification = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'PATCLA'





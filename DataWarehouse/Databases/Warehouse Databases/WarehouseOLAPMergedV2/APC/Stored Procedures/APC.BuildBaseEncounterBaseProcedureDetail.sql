﻿
create proc [APC].[BuildBaseEncounterBaseProcedureDetail]

as

--	20150116	RR	Update date (previously done in Warehouse, then in WOMV2 but only accommodated Cen.  Updated to link to Trafford
--					Some ProcedureDetailRecno's coming through multiple times, but this is due to DQ issues.  (Testing 135/180556 = 0.07%)

Truncate table APC.BaseEncounterProcedureDetail

declare @start datetime
set @start = DATEADD(Year,-3,getdate())

;
With EncounterProcedureDetailCTE
	(
	MergeEncounterRecno
	,EncounterRecno
	,EpisodeStartTime
	,OperationDetailSourceUniqueID
	,MergeOperationDetailRecno
	,OperationStartDate
	,ProcedureDetailSourceUniqueID
	,MergeProcedureDetailRecno
	,ProcedureStartTime
	,EncounterContextCode
	,ProcedureDetailContextCode
	,SequenceNo
	)
as
	(
	Select
		MergeEncounterRecno
		,EncounterRecno
		,EpisodeStartTime
		,OperationDetailSourceUniqueID 
		,MergeOperationDetailRecno
		,OperationStartDate
		,ProcedureDetailSourceUniqueID
		,MergeProcedureDetailRecno
		,ProcedureStartTime
		,EncounterContextCode
		,ProcedureDetailContextCode
		,SequenceNo = row_number() over (partition by MergeEncounterRecno,ProcedureDetailSourceUniqueID
										order by MergeEncounterRecno,ProcedureDetailSourceUniqueID
										)
	from
		(
		select
			 Encounter.MergeEncounterRecno
			,Encounter.EncounterRecno
			,EpisodeStartTime
			,OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID
			,MergeOperationDetailRecno = OperationDetail.MergeRecno
			,OperationStartDate
			,ProcedureDetailSourceUniqueID = ProcedureDetail.SourceUniqueID
			,MergeProcedureDetailRecno = ProcedureDetail.MergeRecno
			,ProcedureStartTime
			,EncounterContextCode = Encounter.ContextCode
			,ProcedureDetailContextCode = ProcedureDetail.ContextCode
		from
			APC.BaseEncounter Encounter

		inner join Theatre.BasePatientBooking PatientBooking
		on	PatientBooking.NHSNumber = replace(Encounter.NHSNumber, ' ', '')

		inner join Theatre.BaseOperationDetail OperationDetail
		on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
		--and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

		inner join Theatre.BaseProcedureDetail ProcedureDetail
		on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

		where 
			Reportable = 1
		and EpisodeStartDate >=@start
		and OperationDetail.OperationEndDate >= @start--is not null  --null OperationEndTime indicates cancelled procedure
		and	ProcedureStartTime between EpisodeStartTime and coalesce(EpisodeEndTime,getdate())
		and PatientBooking.NHSNumber is not null
		and PatientBooking.NHSNumber <>''


		union
		select
			 Encounter.MergeEncounterRecno
			,Encounter.EncounterRecno
			,EpisodeStartTime
			,OperationDetail.SourceUniqueID
			,OperationDetail.MergeRecno
			,OperationStartDate
			,ProcedureDetail.SourceUniqueID
			,ProcedureDetail.MergeRecno
			,ProcedureStartTime
			,EncounterContextCode = Encounter.ContextCode
			,ProcedureDetailContextCode = ProcedureDetail.ContextCode
		from
			APC.BaseEncounter Encounter

		inner join Theatre.BasePatientBooking PatientBooking
		on	replace(PatientBooking.CasenoteNumber, 'Y', '/') = Encounter.CasenoteNumber

		inner join Theatre.BaseOperationDetail OperationDetail
		on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
		--and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

		inner join Theatre.BaseProcedureDetail ProcedureDetail
		on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

		where 
			Reportable = 1
		and EpisodeStartDate >=@start
		and OperationDetail.OperationEndDate >= @start--is not null  --null OperationEndTime indicates cancelled procedure
		and	ProcedureStartTime between EpisodeStartTime and coalesce(EpisodeEndTime,getdate())
		and PatientBooking.CasenoteNumber is not null
		and PatientBooking.CasenoteNumber <>''

		union
		select
			 Encounter.MergeEncounterRecno
			,Encounter.EncounterRecno
			,EpisodeStartTime
			,OperationDetail.SourceUniqueID
			,OperationDetail.MergeRecno
			,OperationStartDate
			,ProcedureDetail.SourceUniqueID
			,ProcedureDetail.MergeRecno
			,ProcedureStartTime
			,EncounterContextCode = Encounter.ContextCode
			,ProcedureDetailContextCode = ProcedureDetail.ContextCode
		from
			APC.BaseEncounter Encounter

		inner join Theatre.BasePatientBooking PatientBooking
		on	PatientBooking.DistrictNo = Encounter.DistrictNo

		inner join Theatre.BaseOperationDetail OperationDetail
		on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
		--and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

		inner join Theatre.BaseProcedureDetail ProcedureDetail
		on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

		where 
			Reportable = 1
		and EpisodeStartDate >=@start
		and OperationDetail.OperationEndDate >= @start--is not null  --null OperationEndTime indicates cancelled procedure
		and	ProcedureStartTime between EpisodeStartTime and coalesce(EpisodeEndTime,getdate())
		and PatientBooking.DistrictNo is not null
		and PatientBooking.DistrictNo <>''	
				
		union
		select
			 Encounter.MergeEncounterRecno
			,Encounter.EncounterRecno
			,EpisodeStartTime
			,OperationDetail.SourceUniqueID
			,OperationDetail.MergeRecno
			,OperationStartDate
			,ProcedureDetail.SourceUniqueID
			,ProcedureDetail.MergeRecno
			,ProcedureStartTime
			,EncounterContextCode = Encounter.ContextCode
			,ProcedureDetailContextCode = ProcedureDetail.ContextCode
		from
			APC.BaseEncounter Encounter

		inner join Theatre.BasePatientBooking PatientBooking
		on
			(
				PatientBooking.Surname = Encounter.PatientSurname
			and	PatientBooking.Forename = Encounter.PatientForename
			and	PatientBooking.SexCode = Encounter.SexCode
			and	PatientBooking.DateOfBirth = Encounter.DateOfBirth
			)

		inner join Theatre.BaseOperationDetail OperationDetail
		on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
		--and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

		inner join Theatre.BaseProcedureDetail ProcedureDetail
		on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

		where 
			Reportable = 1
		and EpisodeStartDate >=@start
		and OperationDetail.OperationEndDate >= @start--is not null  --null OperationEndTime indicates cancelled procedure
		and	ProcedureStartTime between EpisodeStartTime and coalesce(EpisodeEndTime,getdate())
		) Encounter
	)


Insert into APC.BaseEncounterProcedureDetail
(
	MergeEncounterRecno
	,EncounterRecno
	,OperationDetailSourceUniqueID
	,MergeOperationDetailRecno
	,ProcedureDetailSourceUniqueID
	,MergeProcedureDetailRecno
	,EncounterContextCode
	,ProcedureDetailContextCode 
	,OperationStartDate
)

select 
	MergeEncounterRecno
	,EncounterRecno
	,OperationDetailSourceUniqueID
	,MergeOperationDetailRecno
	,ProcedureDetailSourceUniqueID
	,MergeProcedureDetailRecno
	,EncounterContextCode
	,ProcedureDetailContextCode 
	,OperationStartDate
from
	EncounterProcedureDetailCTE
where
	SequenceNo = 1
and not exists
	(
	select	
		1
	from 
		EncounterProcedureDetailCTE Latest
	where
		EncounterProcedureDetailCTE.ProcedureDetailSourceUniqueID = Latest.ProcedureDetailSourceUniqueID
	and Latest.EpisodeStartTime > EncounterProcedureDetailCTE.EpisodeStartTime
	)
		
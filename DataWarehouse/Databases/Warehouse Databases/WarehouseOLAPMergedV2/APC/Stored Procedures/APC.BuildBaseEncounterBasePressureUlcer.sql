﻿
CREATE proc [APC].[BuildBaseEncounterBasePressureUlcer]

as

truncate table APC.BaseEncounterBasePressureUlcer


insert into APC.BaseEncounterBasePressureUlcer
(
	MergeEncounterRecno
	,MergePressureUlcerRecno
)


select
	MergeEncounterRecno
	,MergePressureUlcerRecno
from
	(
	select
		MergeEncounterRecno
		,MergePressureUlcerRecno
	from
		APC.BaseEncounter
	inner join APC.BasePressureUlcer
	on	BasePressureUlcer.SourcePatientNo = BaseEncounter.SourcePatientNo
	and	BasePressureUlcer.IdentifiedTime between BaseEncounter.EpisodeStartTime and BaseEncounter.EpisodeEndTime
	and	BasePressureUlcer.ContextCode = 'CEN||BEDMAN'
	and	BaseEncounter.ContextCode = 'CEN||PAS'

	-- Trafford here...

	) BaseEncounterBasePressureUlcer



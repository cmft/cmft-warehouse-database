﻿
CREATE proc [APC].[BuildBedDay]

as

declare @start date = '1 apr 2009'
		,@end date = getdate()

truncate table APC.BedDay


exec APC.BuildWardBedday @start, @end

--exec APC.BuildNonRealtimeBedday @start, @end

exec APC.BuildAugmentedCareBedday @start, @end

--exec APC.BuildRealtimeBedday @start, @end

exec APC.BuildNeonatalBedday @start, @end

exec APC.BuildCriticalCareBedday @start, @end

exec APC.BuildSpecialtyBedday @start, @end

exec APC.BuildSpecialtyConsultantBedday @start, @end

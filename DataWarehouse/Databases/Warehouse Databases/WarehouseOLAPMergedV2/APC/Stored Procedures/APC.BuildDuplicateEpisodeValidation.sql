﻿
CREATE Procedure [APC].[BuildDuplicateEpisodeValidation]  As

-- 20141007 RR updated from CTE to temp table as there were issues with the running time (discussed with DG and JR)

select 
	OriginalRecno = OriginWarehouse.DatasetRecno
	,DestinationRecno = DestinationWarehouse.DatasetRecno
into
	#ActivityBrokerDuplicate	
from 		
	[$(ActivityBroker)].AB.Pathway Pathway	
		
inner join  [$(ActivityBroker)].AB.[APCSpell] OriginBroker 		
on Pathway.OriginAPCSpellID = OriginBroker.APCSpellID		

inner join Allocation.WrkAllocateDataset OriginWarehouse		
on replace(OriginBroker.SourceUniqueID,'||','/') = OriginWarehouse.ProviderSpellNo  
and OriginWarehouse.DatasetCode = 'APC'

-- Need to join to Destination to ensure an admission has been recorded for the Destination site also, ie a genuine duplicate.
inner join [$(ActivityBroker)].AB.[APCSpell] DestinationBroker		
on Pathway.DestinationAPCSpellID = DestinationBroker.APCSpellID		
		
inner join Allocation.WrkAllocateDataset DestinationWarehouse		
on DestinationBroker.SourceUniqueID = DestinationWarehouse.SourceUniqueID	
and DestinationWarehouse.DatasetCode = 'APC'	

where 	
	Pathway.PathwayTypeID = 1


CREATE INDEX IX_ActivityBrokerDuplicate_OriginalRecno ON #ActivityBrokerDuplicate(OriginalRecno)	
CREATE INDEX IX_ActivityBrokerDuplicate_DestinationRecno ON #ActivityBrokerDuplicate(DestinationRecno)	


Insert into APC.DuplicateEpisodeValidation (SourceUniqueID,ProviderSpellno,ValidationOutcome)

select distinct
	SourceUniqueID = Base.ReportableSourceUniqueID
	,ProviderSpellno = Base.ReportableProviderSpellno
	,3
from 		
	Allocation.APCExclusionDuplicateAdmission Base
where
	not exists 
	(select
		1
	from 
		#ActivityBrokerDuplicate Original
	where
		Original.OriginalRecno = Base.ReportableDatasetRecno
	)
and not exists
	(select
		1
	from
		#ActivityBrokerDuplicate Destination
	where
		Destination.DestinationRecno = Base.ReportableDatasetRecno
	)
and not exists
	(select
		1
	from
		APC.DuplicateEpisodeValidation
	where
		--Base.DatasetRecno = DuplicateEpisodeValidation.MergeEncounterRecno -- 20141007 RR changed, due to mergeencounterrecno's changing
		Base.ReportableSourceUniqueID = DuplicateEpisodeValidation.SourceUniqueID
	)

-- and not one of the rules
-- rule states not reportable
and not exists
	(select
		1
	from
		Allocation.APCExclusionBySourceSiteSourceSpecialty
	where 
		APCExclusionBySourceSiteSourceSpecialty.DatasetRecno = Base.NonReportableDatasetRecno
	)
and not exists
-- reportable record where the rule has identified the corresponding non reportable episode
	(select
		1
	from
		Allocation.APCExclusionBySourceSiteSourceSpecialty
		,Allocation.APCExclusionDuplicateAdmission NonReportable
	where 
		APCExclusionBySourceSiteSourceSpecialty.DatasetRecno = NonReportable.NonReportableDatasetRecno
		and NonReportable.ReportableDatasetRecno = Base.ReportableDatasetRecno
	)
-- rule states not reportable
and not exists
	(select
		1
	from
		Allocation.APCExclusionBySourceSiteSourceSpecialtySourceWard
	where 
		APCExclusionBySourceSiteSourceSpecialtySourceWard.DatasetRecno = Base.NonReportableDatasetRecno
	)
and not exists
-- reportable record where the rule has identified the corresponding non reportable episode
	(select
		1
	from
		Allocation.APCExclusionBySourceSiteSourceSpecialtySourceWard
		,Allocation.APCExclusionDuplicateAdmission NonReportable
	where 
		APCExclusionBySourceSiteSourceSpecialtySourceWard.DatasetRecno = NonReportable.NonReportableDatasetRecno
		and NonReportable.ReportableDatasetRecno = Base.ReportableDatasetRecno
	)


-- Ouctome is currently To be Validated but

-- a) they are no longer in the list of duplicates, an episode may of been deleted/amended

Update 
	DuplicateEpisodeValidation
set 
	ValidationOutcome = 4
from 
	APC.DuplicateEpisodeValidation

left join APC.BaseEncounter
on DuplicateEpisodeValidation.SourceUniqueID = BaseEncounter.SourceUniqueID 

left join Allocation.APCExclusionDuplicateAdmission 
on DuplicateEpisodeValidation.SourceUniqueID = APCExclusionDuplicateAdmission.ReportableSourceUniqueID

where 
	DuplicateEpisodeValidation.ValidationOutcome = 3
and 
	(
		APCExclusionDuplicateAdmission.ReportableSourceUniqueID is null
	or
		BaseEncounter.SourceUniqueID is null
	)


-- b) they've been picked up elsewhere
Update 
	DuplicateEpisodeValidation
set 
	ValidationOutcome = 4
from 
	APC.DuplicateEpisodeValidation

inner join Allocation.APCExclusionDuplicateAdmission 
on DuplicateEpisodeValidation.SourceUniqueID = APCExclusionDuplicateAdmission.ReportableSourceUniqueID

inner join APC.BaseEncounter Reportable
on Reportable.SourceUniqueID = APCExclusionDuplicateAdmission.ReportableSourceUniqueID 


where 
	DuplicateEpisodeValidation.ValidationOutcome = 3
and Reportable = 0

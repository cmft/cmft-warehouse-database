﻿
CREATE Procedure [APC].[BuildBaseEncounterBaseSpecialRegister]

as

Truncate table APC.BaseEncounterBaseSpecialRegister

insert into APC.BaseEncounterBaseSpecialRegister

(
	MergeEncounterRecno
	,MergeSpecialRegisterRecno
)
select 
	MergeEncounterRecno
	,MergeSpecialRegisterRecno
from 
	(
	-- Special Register Code has been identified at the same site Cen / Tra
	select
		BaseEncounter.MergeEncounterRecno
		,BaseSpecialRegister.MergeSpecialRegisterRecno
	from
		APC.BaseEncounter

	inner join Patient.BaseSpecialRegister
	on BaseSpecialRegister.SourcePatientNo = BaseEncounter.SourcePatientNo
	and	BaseSpecialRegister.ContextCode = BaseEncounter.ContextCode
	and coalesce(Active,1) = 1
	and	cast(
			coalesce(
					BaseEncounter.EpisodeEndTime
					,getdate()
					)
			as date
			)
		between coalesce(
						BaseSpecialRegister.ActivityTime
						,BaseSpecialRegister.StartDate
						,BaseSpecialRegister.EnteredDate
						)
				and 
				coalesce(
						BaseSpecialRegister.EndDate
						,getdate()
						)
	
	union

	-- Special Register has been recorded at the Trafford site but patient has had an IP episode at Central
	select
		CurrentInpatientCentral.MergeEncounterRecno
		,BaseSpecialRegister.MergeSpecialRegisterRecno
	from
		APC.BaseEncounter CurrentInpatientCentral

	inner join [$(Chameleon)].Patient.PatientApplicationIdentity CentralIdentity with (nolock)
	on CentralIdentity.PatientApplicationIdentityValue = CurrentInpatientCentral.DistrictNo --COLLATE Latin1_General_CI_AS 
	and	CentralIdentity.SourceApplicationIdentifierId = 1 --DistrictNumber

	inner join [$(Chameleon)].Patient.PatientApplicationIdentity TraffordIdentity with (nolock)
	on	TraffordIdentity.PatientId = CentralIdentity.PatientId
	and	TraffordIdentity.SourceApplicationIdentifierId = 2 --EPMINumber

	inner join Patient.BaseSpecialRegister 
	on	TraffordIdentity.PatientApplicationIdentityValue = BaseSpecialRegister.SourcePatientNo --COLLATE Latin1_General_CI_AS 
	and left(BaseSpecialRegister.ContextCode,3) = 'TRA'
	and coalesce(Active,1) = 1
	and coalesce(
			CurrentInpatientCentral.EpisodeEndDate 
			,getdate()
			) 
			between BaseSpecialRegister.StartDate 
				and coalesce(
							BaseSpecialRegister.EndDate
							,getdate()
							)
	where 
		CurrentInpatientCentral.ContextCode = 'CEN||PAS'

	union

	-- Special Register has been recorded at the Central site but patient has had an IP episode at Trafford
	select
		CurrentInpatientTrafford.MergeEncounterRecno
		,BaseSpecialRegister.MergeSpecialRegisterRecno
	from
		APC.BaseEncounter CurrentInpatientTrafford

	inner join [$(Chameleon)].Patient.PatientApplicationIdentity TraffordIdentity with (nolock)
	on	TraffordIdentity.PatientApplicationIdentityValue = CurrentInpatientTrafford.SourcePatientNo -- COLLATE Latin1_General_CI_AS 
	and	TraffordIdentity.SourceApplicationIdentifierId = 2 --EPMINumber
							
	inner join [$(Chameleon)].Patient.PatientApplicationIdentity CentralIdentity with (nolock)
	on	CentralIdentity.PatientId = TraffordIdentity.PatientId
	and	CentralIdentity.SourceApplicationIdentifierId = 1 --DistrictNo
				
	inner join Patient.BaseSpecialRegister
	on	CentralIdentity.PatientApplicationIdentityValue = BaseSpecialRegister.DistrictNo -- COLLATE Latin1_General_CI_AS 
	and left(BaseSpecialRegister.ContextCode,3) = 'CEN'
	and coalesce(Active,1) = 1
	and coalesce(
			CurrentInpatientTrafford.EpisodeEndDate 
			,getdate()
			) 
			between coalesce(
							BaseSpecialRegister.ActivityTime 
							,BaseSpecialRegister.StartDate
							,BaseSpecialRegister.EnteredDate
							)
				and coalesce(
							BaseSpecialRegister.EndDate
							,getdate()
							)
	where 
		CurrentInpatientTrafford.ContextCode = 'TRA||UG'

	) A



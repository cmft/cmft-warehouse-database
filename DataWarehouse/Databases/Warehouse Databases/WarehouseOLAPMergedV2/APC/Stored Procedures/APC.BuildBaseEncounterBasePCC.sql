﻿

CREATE procedure APC.BuildBaseEncounterBasePCC

as

Truncate table APC.BaseEncounterBasePCC

Insert into APC.BaseEncounterBasePCC

select 
	MergeEncounterRecno
	,PeriodRecno
from 
	PCC.Period

inner join APC.Encounter Encounter
on Period.NHSNumber = Encounter.NHSNumber 
and	Encounter.EpisodeStartTime < coalesce(Period.DischargeTime,getdate())
and	coalesce(Encounter.EpisodeEndTime,getdate())> Period.AdmissionTime
and	not exists
	(
	select
		1
	from
		APC.Encounter Previous
	where
		Period.NHSNumber = Previous.NHSNumber 
	and	Previous.EpisodeStartTime < coalesce(Period.DischargeTime,getdate())
	and	coalesce(Previous.EpisodeEndTime,getdate()) > Period.AdmissionTime
	and	(
			Previous.GlobalEpisodeNo < Encounter.GlobalEpisodeNo
		or	(
				Previous.GlobalEpisodeNo = Encounter.GlobalEpisodeNo		
			and	Previous.MergeEncounterRecno < Encounter.MergeEncounterRecno
			)
		)
	)
where 
	Period.NHSNumber is not null


Insert into APC.BaseEncounterBasePCC

select 
	MergeEncounterRecno
	,PeriodRecno
from 
	PCC.Period

inner join APC.Encounter Encounter
on Period.CasenoteNo = Encounter.CasenoteNumber
and	Encounter.EpisodeStartTime < coalesce(Period.DischargeTime,getdate())
and coalesce(Encounter.EpisodeEndTime,getdate()) > Period.AdmissionTime
and	not exists
	(
	select
		1
	from
		APC.Encounter Previous
	where
		Period.CasenoteNo = Previous.CasenoteNumber 
	and	Previous.EpisodeStartTime < coalesce(Period.DischargeTime,getdate())
	and	coalesce(Previous.EpisodeEndTime,getdate()) > Period.AdmissionTime
	and	(
			Previous.GlobalEpisodeNo < Encounter.GlobalEpisodeNo
		or	(
				Previous.GlobalEpisodeNo = Encounter.GlobalEpisodeNo		
			and	Previous.MergeEncounterRecno < Encounter.MergeEncounterRecno
			)
		)
	)
where 
	not exists
		(
		Select
			1
		from
			APC.BaseEncounterBasePCC Present
		where
			Present.PeriodRecno = Period.PeriodRecno
		)
			


﻿
CREATE procedure [APC].[BuildReadmission] as


select
	 Discharge.MergeEncounterRecno
	,Discharge.DistrictNo
	,Discharge.NHSNumber
	,Admission.AdmissionTime
	,Discharge.ContextCode
	,Discharge.DischargeDate
	,Discharge.DischargeTime	/* Added Paul Egan 20/05/2014 */
into
	#InitialAdmission
from
	APC.BaseEncounter Discharge

inner join APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Discharge.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1
and Admission.Reportable = 1
where
	not exists --get last episode of the global spell
	(
	select
		1
	from
		APC.BaseEncounter Previous
	where
		Previous.GlobalProviderSpellNo = Discharge.GlobalProviderSpellNo
	and	Previous.GlobalEpisodeNo > Discharge.GlobalEpisodeNo
	and Previous.Reportable = 1
	)

select
	 Admission.MergeEncounterRecno
	,Admission.DistrictNo
	,Admission.NHSNumber
	,Admission.AdmissionTime
	,Admission.ContextCode
	,Admission.AdmissionDate
into
	#Readmission
from
	APC.BaseEncounter Admission
where
	Admission.GlobalEpisodeNo = 1
and Admission.Reportable = 1


truncate table APC.Readmission

insert into APC.Readmission
(
	 ReadmissionRecno
	,InitialAdmissionRecno
	,TrustWide
)
select
	 Readmission.MergeEncounterRecno
	,InitialAdmission.MergeEncounterRecno
	,TrustWide = CAST(0 as bit)
from
	#Readmission Readmission

inner join #InitialAdmission InitialAdmission
on	InitialAdmission.DistrictNo = Readmission.DistrictNo
and	InitialAdmission.ContextCode = Readmission.ContextCode
/* Changed to reomve duplicate spells and site transfers as readmissions. Paul Egan 20140520 */
--and	InitialAdmission.AdmissionTime < Readmission.AdmissionTime
 and    InitialAdmission.DischargeTime < Readmission.AdmissionTime


inner join APC.BaseEncounterReference ReadmissionEncounterReference
on	ReadmissionEncounterReference.MergeEncounterRecno = Readmission.MergeEncounterRecno

left join APC.AdmissionMethod ReadmissionAdmissionAdmissionMethod
on	ReadmissionAdmissionAdmissionMethod.SourceAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID

where
	ReadmissionAdmissionAdmissionMethod.NationalAdmissionMethodCode in ('21', '22', '23', '24', '25', '28')
and	datediff(day, InitialAdmission.DischargeDate, Readmission.AdmissionDate) < 30


union all

select
	 Readmission.MergeEncounterRecno
	,InitialAdmission.MergeEncounterRecno
	,TrustWide = CAST(1 as bit)
from
	#Readmission Readmission

inner join #InitialAdmission InitialAdmission
on	InitialAdmission.NHSNumber = Readmission.NHSNumber
/* Changed to reomve duplicate spells and site transfers as readmissions. Paul Egan 20140520 */
--and	InitialAdmission.AdmissionTime < Readmission.AdmissionTime
 and    InitialAdmission.DischargeTime < Readmission.AdmissionTime


inner join APC.BaseEncounterReference ReadmissionEncounterReference
on	ReadmissionEncounterReference.MergeEncounterRecno = Readmission.MergeEncounterRecno

left join APC.AdmissionMethod ReadmissionAdmissionAdmissionMethod
on	ReadmissionAdmissionAdmissionMethod.SourceAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID

where
	ReadmissionAdmissionAdmissionMethod.NationalAdmissionMethodCode in ('21', '22', '23', '24', '25', '28')
and	datediff(day, InitialAdmission.DischargeDate, Readmission.AdmissionDate) < 30


drop table #InitialAdmission
drop table #Readmission



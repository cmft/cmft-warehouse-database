﻿
CREATE PROCEDURE [APC].[BuildPatientSourceSystemFlag] AS

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


--remove flags that no longer apply
delete
from
	Flag.PatientSourceSystemFlag
where
	PatientSourceSystemFlag.SourceSystemID = 1
and	PatientSourceSystemFlag.FlagID = 1
and	not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		exists
		(
		select
			1
		from
			APC.BaseDiagnosis
		where
			BaseDiagnosis.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
		and	BaseDiagnosis.DiagnosisCode between 'F70.0' and 'F79.9'  --learning disability (confirm with Gareth Jones)
		)
	and	BaseEncounter.ContextCode = PatientSourceSystemFlag.ContextCode
	and	BaseEncounter.SourcePatientNo = PatientSourceSystemFlag.PatientID
	and	BaseEncounter.EpisodeStartDate = PatientSourceSystemFlag.StartDate
	)

select
	 @deleted = @@rowcount


--this inserts every new occurance of this flag - is that correct or do we just want the 1st (or last)?
insert
into
	Flag.PatientSourceSystemFlag
	(
	SourceSystemID
	,ContextCode
	,PatientID
	,StartDate
	,FlagID
	,EndDate
	)
select
	SourceSystemID
	,ContextCode
	,PatientID
	,StartDate
	,FlagID
	,EndDate
from
	(
	select distinct
		SourceSystemID = 1 --diagnosis
		,ContextCode = BaseEncounter.ContextCode
		,PatientID = BaseEncounter.SourcePatientNo
		,StartDate = BaseEncounter.EpisodeStartDate
		,FlagID = 1 --learning disability
		,EndDate = null
	from
		APC.BaseEncounter

	--inner join APC.ProcessList
	--on	ProcessList.MergeRecno = BaseEncounter.MergeEncounterRecno

	where
		exists
		(
		select
			1
		from
			APC.BaseDiagnosis
		where
			BaseDiagnosis.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
		and	BaseDiagnosis.DiagnosisCode between 'F70.0' and 'F79.9'  --learning disability (confirm with Gareth Jones)
		)
	) Flags
where
	not exists
		(
		select
			1
		from
			Flag.PatientSourceSystemFlag
		where
			PatientSourceSystemFlag.SourceSystemID = Flags.SourceSystemID
		and	PatientSourceSystemFlag.ContextCode = Flags.ContextCode
		and	PatientSourceSystemFlag.PatientID = Flags.PatientID
		and	PatientSourceSystemFlag.StartDate = Flags.StartDate
		and	PatientSourceSystemFlag.FlagID = Flags.FlagID
		)

select
	 @inserted = @@rowcount
	 
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



RETURN 0


﻿

-- Please note Recall contains Cen and Tra data, when linking below, some joins can't be found, sometimes duplicates are found (Reportable = 1 reduces duplicates, but some may be with DQ team still to validate)
-- The fact table will need to recognise where the Recall Encounter cannot be linked to APC and where Duplicates have been found.
-- please note, incorrect link has been found (small numbers), eg Trafford Theatre linked to Cen episode, Traf episode not picked up because Cen Casenote number had been used, and DoB had been recorded incorrectly (discussed with DG)


CREATE proc [APC].[BuildBaseEncounterBaseTransaction]	

as

Truncate table APC.BaseEncounterBaseTransaction

Insert into APC.BaseEncounterBaseTransaction
(
	MergeEncounterRecno
	,MergeTransactionRecno
)

select
	MergeEncounterRecno
	,MergeTransactionRecno = BaseTransaction.MergeRecno
from
	APC.BaseEncounter
	
inner join BloodManagement.BaseTransaction
on	BaseEncounter.CasenoteNumber = BaseTransaction.HospitalNumber
and TransactionTypeID <> 3
and	BaseTransaction.TransactionTime between BaseEncounter.EpisodeStartTime and coalesce(BaseEncounter.EpisodeEndTime, getdate())

where
	BaseEncounter.Reportable = 1
	
and not exists
		(
		select 
			1
		from
			APC.BaseEncounter Earliest
		where
			Earliest.CasenoteNumber = BaseTransaction.HospitalNumber
		and TransactionTypeID <> 3
		and	BaseTransaction.TransactionTime between Earliest.EpisodeStartTime and coalesce(Earliest.EpisodeEndTime, getdate())
		and Earliest.MergeEncounterRecno < BaseEncounter.MergeEncounterRecno
		-- and Earliest.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
		--and Earliest.GlobalEpisodeNo < BaseEncounter.GlobalEpisodeNo
		)

Insert into APC.BaseEncounterBaseTransaction
(
	MergeEncounterRecno
	,MergeTransactionRecno
)

select
	MergeEncounterRecno
	,MergeTransactionRecno = BaseTransaction.MergeRecno
from
	APC.BaseEncounter
	
inner join BloodManagement.BaseTransaction
on	BaseEncounter.NHSNumber = BaseTransaction.NHSNumber
and TransactionTypeID <> 3
and	BaseTransaction.TransactionTime between BaseEncounter.EpisodeStartTime and coalesce(BaseEncounter.EpisodeEndTime, getdate())

where
	BaseEncounter.Reportable = 1
and not exists
	(
	select
		1	
	from
		APC.BaseEncounterBaseTransaction Present
	where
		Present.MergeTransactionRecno = BaseTransaction.MergeRecno
	)
	
and not exists
		(
		select 
			1
		from
			APC.BaseEncounter Earliest
		where
			Earliest.NHSNumber = BaseTransaction.NHSNumber
		and TransactionTypeID <> 3
		and	BaseTransaction.TransactionTime between Earliest.EpisodeStartTime and coalesce(Earliest.EpisodeEndTime, getdate())
		and Earliest.MergeEncounterRecno < BaseEncounter.MergeEncounterRecno
		-- and Earliest.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
		--and Earliest.GlobalEpisodeNo < BaseEncounter.GlobalEpisodeNo
		)

Insert into APC.BaseEncounterBaseTransaction
(
	MergeEncounterRecno
	,MergeTransactionRecno
)

select
	MergeEncounterRecno
	,MergeTransactionRecno = BaseTransaction.MergeRecno
from
	APC.BaseEncounter
	
inner join BloodManagement.BaseTransaction
on	(
		BaseEncounter.PatientSurname = BaseTransaction.LastName 
	and BaseEncounter.PatientForename = BaseTransaction.FirstName 
	and BaseEncounter.DateOfBirth = BaseTransaction.DateOfBirth
	)
and TransactionTypeID <> 3
and	BaseTransaction.TransactionTime between BaseEncounter.EpisodeStartTime and coalesce(BaseEncounter.EpisodeEndTime, getdate())

where
	BaseEncounter.Reportable = 1
and not exists
	(
	select
		1	
	from
		APC.BaseEncounterBaseTransaction Present
	where
		Present.MergeTransactionRecno = BaseTransaction.MergeRecno
	)
	-- above takes 1sec, when running with the below not exists, increases to 5mins?!?!?!?
and not exists
		(
		select 
			1
		from
			APC.BaseEncounter Earliest
		where
			(
				Earliest.PatientSurname = BaseTransaction.LastName 
			and Earliest.PatientForename = BaseTransaction.FirstName 
			and Earliest.DateOfBirth = BaseTransaction.DateOfBirth
			)
		and BaseTransaction.TransactionTypeID <> 3
		and	BaseTransaction.TransactionTime between Earliest.EpisodeStartTime and coalesce(Earliest.EpisodeEndTime, getdate())
		and Earliest.MergeEncounterRecno < BaseEncounter.MergeEncounterRecno
		--and Earliest.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
		--and Earliest.GlobalEpisodeNo < BaseEncounter.GlobalEpisodeNo
		)

	
---------------------------------------------------------------------------------------------------------------

-- Request transactions only (methodology differs slightly as the request can be made before the spell starts)

Insert into APC.BaseEncounterBaseTransaction
(
	MergeEncounterRecno
	,MergeTransactionRecno
)

select
	MergeEncounterRecno
	,MergeRecno
from
	APC.BaseEncounter
	
inner join BloodManagement.BaseTransaction
on	BaseEncounter.CasenoteNumber = BaseTransaction.HospitalNumber
and TransactionTypeID = 3
and	BaseTransaction.TransactionTime between dateadd(day,-4,BaseEncounter.EpisodeStartTime) and coalesce(BaseEncounter.EpisodeEndTime, getdate())

where
	BaseEncounter.Reportable = 1
	
and not exists
		(
		select 
			1
		from
			APC.BaseEncounter Latest
		where
			Latest.CasenoteNumber = BaseTransaction.HospitalNumber
		and TransactionTypeID = 3
		and	BaseTransaction.TransactionTime between dateadd(day,-4,Latest.EpisodeStartTime) and coalesce(Latest.EpisodeEndTime, getdate())
		and Latest.MergeEncounterRecno > BaseEncounter.MergeEncounterRecno
		--and Latest.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
		--and Latest.GlobalEpisodeNo > BaseEncounter.GlobalEpisodeNo
		)



Insert into APC.BaseEncounterBaseTransaction
(
	MergeEncounterRecno
	,MergeTransactionRecno
)

select
	MergeEncounterRecno
	,MergeRecno
from
	APC.BaseEncounter
	
inner join BloodManagement.BaseTransaction
on	BaseEncounter.NHSNumber = BaseTransaction.NHSNumber
and TransactionTypeID = 3
and	BaseTransaction.TransactionTime between dateadd(day,-4,BaseEncounter.EpisodeStartTime) and coalesce(BaseEncounter.EpisodeEndTime, getdate())

where
	BaseEncounter.Reportable = 1
and not exists
	(
	select
		1	
	from
		APC.BaseEncounterBaseTransaction Present
	where
		Present.MergeTransactionRecno = BaseTransaction.MergeRecno
	)
	
and not exists
		(
		select 
			1
		from
			APC.BaseEncounter Latest
		where
			Latest.NHSNumber = BaseTransaction.NHSNumber
		and TransactionTypeID = 3
		and	BaseTransaction.TransactionTime between dateadd(day,-4,Latest.EpisodeStartTime) and coalesce(Latest.EpisodeEndTime, getdate())
		and Latest.MergeEncounterRecno > BaseEncounter.MergeEncounterRecno
		--and Latest.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
		--and Latest.GlobalEpisodeNo > BaseEncounter.GlobalEpisodeNo
		)


Insert into APC.BaseEncounterBaseTransaction
(
	MergeEncounterRecno
	,MergeTransactionRecno
)

select
	MergeEncounterRecno
	,MergeRecno
from
	APC.BaseEncounter
	
inner join BloodManagement.BaseTransaction
on	(
		BaseEncounter.PatientSurname = BaseTransaction.LastName 
	and BaseEncounter.PatientForename = BaseTransaction.FirstName 
	and BaseEncounter.DateOfBirth = BaseTransaction.DateOfBirth
	)
and TransactionTypeID = 3
and	BaseTransaction.TransactionTime between dateadd(day,-4,BaseEncounter.EpisodeStartTime) and coalesce(BaseEncounter.EpisodeEndTime, getdate())

where
	BaseEncounter.Reportable = 1
and not exists
	(
	select
		1	
	from
		APC.BaseEncounterBaseTransaction Present
	where
		Present.MergeTransactionRecno = BaseTransaction.MergeRecno
	)
and not exists
		(
		select 
			1
		from
			APC.BaseEncounter Latest
		where
			(
				Latest.PatientSurname = BaseTransaction.LastName 
			and Latest.PatientForename = BaseTransaction.FirstName 
			and Latest.DateOfBirth = BaseTransaction.DateOfBirth
			)
		and TransactionTypeID = 3
		and	BaseTransaction.TransactionTime between dateadd(day,-4,Latest.EpisodeStartTime) and coalesce(Latest.EpisodeEndTime, getdate())
		and Latest.MergeEncounterRecno > BaseEncounter.MergeEncounterRecno
		--and Latest.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
		--and Latest.GlobalEpisodeNo > BaseEncounter.GlobalEpisodeNo
		)


		
	
	

	;

 

/****** Object:  StoredProcedure [APC].[BuildBaseEncounterBaseCellSalvage]    Script Date: 03/03/2015 13:00:02 ******/
SET ANSI_NULLS ON

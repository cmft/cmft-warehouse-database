﻿
create proc [APC].[BuildBaseEncounterBaseOperationDetail]

as

--	20150116	RR	used the ProcedureDetail table, instead of running the same expensive joins twice.
--					There are some dups still coming through (0.07%) but these should be picked up by DQ team or ActivityBroker dups using a separate report

Truncate table APC.BaseEncounterOperationDetail
Insert into APC.BaseEncounterOperationDetail
(
	MergeEncounterRecno
	,OperationDetailSourceUniqueID
	,MergeOperationDetailRecno
	,EncounterContextCode
	,ProcedureDetailContextCode
	,SequenceNo 
)

Select  
	MergeEncounterRecno
	,OperationDetailSourceUniqueID
	,MergeOperationDetailRecno
	,EncounterContextCode
	,ProcedureDetailContextCode
	,SequenceNumber = row_number() over (partition by MergeEncounterRecno
											order by OperationStartDate
											)
	--121588
from
	(
	Select  
		MergeEncounterRecno
		,OperationDetailSourceUniqueID
		,MergeOperationDetailRecno
		,ProcedureDetailSourceUniqueID
		,OperationStartDate
		,EncounterContextCode
		,ProcedureDetailContextCode
		,SequenceNo = row_number() over (partition by MergeEncounterRecno,OperationDetailSourceUniqueID
											order by MergeEncounterRecno,OperationDetailSourceUniqueID
											)
	from APC.BaseEncounterProcedureDetail
	)OperationDetail
where
	SequenceNo = 1
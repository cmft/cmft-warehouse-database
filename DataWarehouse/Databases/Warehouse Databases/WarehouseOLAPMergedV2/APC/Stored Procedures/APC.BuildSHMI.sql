﻿
CREATE proc [APC].[BuildSHMI]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int

-- 20150113	DG	Updated RiskModel with the new baseline
-- 20150115	RR	AgeGroup has replaced StartAge as a Predictor.  brought AgeGroup into the Risk Calculation (discussed with Tom,Gareth S and James W)
--			Darren,  I've commented out ExternalRiskScore as the TLoad view doesn't exist in Live??  Was this just for testing, wasn't sure so left it in, but commented out. 


truncate table APC.BaseSHMI;

select
	*
into
	#Diagnosis
from
	WH.Diagnosis

CREATE NONCLUSTERED INDEX [#Diagnosis_01]
ON [dbo].[#Diagnosis] ([DiagnosisCode])
INCLUDE ([DiagnosisGroupCode],[DiagnosisGroupSHMICode])

select --top 10 
	 Encounter.MergeEncounterRecno
	,Admission.SourcePatientNo
	,ProviderSpellNo = Encounter.GlobalProviderSpellNo
	,Encounter.ContextCode
	,Encounter.SourceEncounterNo
	,Encounter.GlobalEpisodeNo
	,Admission.AdmissionDate
	,Admission.AdmissionTime
	,Discharge.DischargeDate
	,Encounter.DateOfBirth
	,DateOfDeath = 
				coalesce(
					Encounter.DateOfDeath
					,BaseDeath.DateOfDeath
					)
	,AdmissionMethod.NationalAdmissionMethodCode
	,Sex.NationalSexCode
	,PrimaryDiagnosisCode = 
				coalesce(
					left(Encounter.PrimaryDiagnosisCode, 6)	
					,'R69.X'
					)
	,DiagnosisGroupCode = Diagnosis.DiagnosisGroupCode
	,DiagnosisGroupCategoryCode = Diagnosis.DiagnosisGroupSHMICode
	,Encounter.CharlsonIndex
into
	#SHMI
from
	APC.BaseEncounter Encounter

inner join APC.BaseEncounterReference Reference
on	Encounter.MergeEncounterRecno = Reference.MergeEncounterRecno
and Reportable = 1

inner join APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

inner join APC.BaseEncounterReference AdmissionReference
on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

inner join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Discharge.MergeEncounterRecno

inner join WH.Sex Sex
on	Reference.SexID = Sex.SourceSexID

inner join APC.AdmissionMethod AdmissionMethod
on	AdmissionReference.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID

inner join APC.PatientClassification PatientClassification
on Reference.PatientClassificationID = PatientClassification.SourcePatientClassificationID

inner join APC.DischargeMethod DischargeMethod
on DischargeReference.DischargeMethodID = DischargeMethod.SourceDischargeMethodID

--left join [$(Warehouse)].dbo.EntityXref ClinicalClassification
--on	left(Encounter.PrimaryDiagnosisCode, 6) = ClinicalClassification.EntityCode
--and	ClinicalClassification.EntityTypeCode = 'DIAGNOSISCODE'
--and ClinicalClassification.XrefEntityTypeCode = 'CCSCATEGORYCODE'

--left join [$(Warehouse)].dbo.EntityXref ClinicalClassificationMap
--on	ClinicalClassification.XrefEntityCode = ClinicalClassificationMap.XrefEntityCode
--and ClinicalClassificationMap.EntityTypeCode = 'SHMICCSCATEGORYCODE'
--and ClinicalClassificationMap.XrefEntityTypeCode = 'CCSCATEGORYCODE'

left join #Diagnosis Diagnosis
on	Diagnosis.DiagnosisCode = left(Encounter.PrimaryDiagnosisCode, 6)

left join WH.BaseDeath
on	BaseDeath.NHSNumber = Encounter.NHSNumber

where
	NationalPatientClassificationCode not in ('2','3','4') -- exclude day cases and regular day and night admissions
and NationalDischargeMethodCode not in ('5') -- Exclude Stillbirths
and Discharge.DischargeDate is not null
	
	
insert into APC.BaseSHMI
(
	[MergeEncounterRecno]
	,DominantForDiagnosis
	,[Survived]
	,[Died]
	,[DateOfDeath]
	,[DischargeDate]
	,[PatientAge]
	,[PatientAgeCategoryCode]
	,[AdmissionMethodCode]
	,[AdmissionMethodCategoryCode]
	,[SexCode]
	,[SexCategoryCode]
	,[PrimaryDiagnosisCode]
	,[DiagnosisGroupCode]
	,[DiagnosisGroupCategoryCode]
	,[CharlsonIndex]
	,[CharlsonIndexCategoryCode]
	,[YearIndexCode]
	,[RiskScore]
	--,ExternalRiskScore
)
	
	select --top 100 
		Encounter.MergeEncounterRecno	
		,DominantForDiagnosis = 
				case
				-- search for second episode in spell if none, 1st fce is dominant
				when 
					not exists
					(
					select
						1
					from
						#SHMI EncounterNext
					where
						Encounter.ProviderSpellNo = EncounterNext.ProviderSpellNo
					and EncounterNext.GlobalEpisodeNo = 2		
					)														
				then 1	
				-- if first episode primary diagnosis is not sign or symptom use it							
				when
					GlobalEpisodeNo = 1 
				and	left(PrimaryDiagnosisCode, 1) <> 'R'					
				then 1	
				-- if first episode primary diagnosis is sign or symptom and so is second use the first											
				when
					GlobalEpisodeNo = 1 
				and	left(PrimaryDiagnosisCode, 1) = 'R'
				and exists
					(
					select
						1
					from
						#SHMI EncounterNext
					where
						Encounter.ProviderSpellNo = EncounterNext.ProviderSpellNo
					and	EncounterNext.GlobalEpisodeNo = 2			
					and	left(EncounterNext.PrimaryDiagnosisCode, 1) = 'R'
					)								
				then 1	
				-- if second episode is not sign and symptom but the first is use the second
				when
					GlobalEpisodeNo = 2 
				and	left(PrimaryDiagnosisCode, 1) <> 'R'
				and	exists
					(
					select
						1
					from
						#SHMI EncounterPrevious
					where
						Encounter.ProviderSpellNo = EncounterPrevious.ProviderSpellNo
					and EncounterPrevious.GlobalEpisodeNo = 1
					and left(EncounterPrevious.PrimaryDiagnosisCode, 1) = 'R'
					)
				then 1
				else 0
				end							
		,Survived = 
				case
				when datediff(day, Encounter.DischargeDate, DateOfDeath) <= 30
				and Encounter.AdmissionDate <= DateOfDeath
				and not exists
					(
					select
						1
					from
						#SHMI EncounterNext
					where
						EncounterNext.SourcePatientNo = Encounter.SourcePatientNo
					and EncounterNext.AdmissionTime > Encounter.AdmissionTime
					and	EncounterNext.ContextCode = Encounter.ContextCode
					)								
				then 0
				else 1
				end
		,Died = 
				case
				when datediff(day, Encounter.DischargeDate, DateOfDeath) <= 30
				and Encounter.AdmissionDate <= DateOfDeath
				and not exists
					(
					select
						1
					from
						#SHMI EncounterNext
					where
						EncounterNext.SourcePatientNo = Encounter.SourcePatientNo
					and EncounterNext.AdmissionTime > Encounter.AdmissionTime
					and EncounterNext.ContextCode = Encounter.ContextCode
					)								
				then 1
				else 0
				end
		,DateOfDeath
		,DischargeDate = Encounter.DischargeDate
		,PatientAge = 
				case
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 1
				then CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate)
				when datediff(day, DateOfBirth, Encounter.AdmissionDate) between 273 and 364 then 7007
				when datediff(day, DateOfBirth, Encounter.AdmissionDate) between 182 and 272 then 7006
				when datediff(day, DateOfBirth, Encounter.AdmissionDate) between 91 and 181 then 7005
				when datediff(day, DateOfBirth, Encounter.AdmissionDate) between 29 and 90 then 7004
				when datediff(day, DateOfBirth, Encounter.AdmissionDate) between 7 and 28 then 7003
				when datediff(day, DateOfBirth, Encounter.AdmissionDate) between 1 and 6 then 7002
				when datediff(day, DateOfBirth, Encounter.AdmissionDate) < 1 then 7001
				end			
		,PatientAgeCategoryCode = 
				case
				when DateOfBirth is null then '21'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 90 then '20'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 85 then '19'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 80 then '18'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 75 then '17'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 70 then '16'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 65 then '15'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 60 then '14'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 55 then '13'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 50 then '12'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 45 then '11'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 40 then '10'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 35 then '9'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 30 then '8'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 25 then '7'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 20 then '6'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 15 then '5'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 10 then '4'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 5 then '3'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 1 then '2'
				when CMFT.Dates.GetAge(DateOfBirth, Encounter.AdmissionDate) >= 0 then '1'
				end
		,AdmissionMethodCode = NationalAdmissionMethodCode
		,AdmissionMethodCategoryCode = 
				case
				when NationalAdmissionMethodCode in ('11','12','13')
				then 1
				when NationalAdmissionMethodCode in ('21','22','23','24','28','31','32','81','82','83','84','89','98','25','2A','2B','2C','2D' )
				then 3
				else 2
				end
		,SexCode = NationalSexCode
		,SexCategoryCode = 
				case
				when NationalSexCode in ('1','2')
				then NationalSexCode
				else 3
				end
		,PrimaryDiagnosisCode 
		,DiagnosisGroupCode
		,DiagnosisGroupCategoryCode
		,CharlsonIndex = CharlsonIndex
		,CharlsonIndexCategoryCode = 
				case
				when CharlsonIndex <= 0
				then '1'
				when CharlsonIndex between 1 and 5
				then '2'
				when CharlsonIndex > 5
				then '3'
				end
		,YearIndexCode = 1
		,RiskScore = cast(0 as float)
	from
		#SHMI Encounter
				
select
	@inserted = @@ROWCOUNT	


/* now assign risk score */

update SHMI
set
	RiskScore =
			exp(
				coalesce(RiskModelAdmission.[ParameterEstimate], 0)
				+
				coalesce(RiskModelCharlson.[ParameterEstimate], 0)
				+
				coalesce(RiskModelGender.[ParameterEstimate], 0)
				+
				coalesce(RiskModelStartAge.[ParameterEstimate], 0)
				+
				coalesce(RiskModelAgeGroup.[ParameterEstimate], 0) -- RR added
				+
				coalesce(RiskModelIntercept.[ParameterEstimate], 0)
				+
				coalesce(RiskModelYearIndex.[ParameterEstimate], 0)
				)
				/
				(
				1 
				+
				exp(
				coalesce(RiskModelAdmission.[ParameterEstimate], 0)
				+
				coalesce(RiskModelCharlson.[ParameterEstimate], 0)
				+
				coalesce(RiskModelGender.[ParameterEstimate], 0)
				+
				coalesce(RiskModelStartAge.[ParameterEstimate], 0)
				+
				coalesce(RiskModelAgeGroup.[ParameterEstimate], 0) -- RR added
				+
				coalesce(RiskModelIntercept.[ParameterEstimate], 0)
				+
				coalesce(RiskModelYearIndex.[ParameterEstimate], 0)
				)
				)
					
from
	APC.BaseSHMI SHMI

left outer join
	[$(Warehouse)].WH.MortalityRiskModel RiskModelAdmission
on	RiskModelAdmission.DiagnosisGroupCode = SHMI.DiagnosisGroupCategoryCode
and	RiskModelAdmission.Predictor = 'ADMIMETH_' + cast(SHMI.AdmissionMethodCategoryCode as varchar(2))
and	SHMI.DischargeDate between RiskModelAdmission.FromDate and coalesce(RiskModelAdmission.ToDate, getdate())

left outer join
	[$(Warehouse)].WH.MortalityRiskModel RiskModelCharlson
on	RiskModelCharlson.DiagnosisGroupCode = SHMI.DiagnosisGroupCategoryCode
and	RiskModelCharlson.Predictor = 'CHARLSON_INDEX_' + cast(SHMI.CharlsonIndexCategoryCode as varchar(2))
and	SHMI.DischargeDate between RiskModelCharlson.FromDate and coalesce(RiskModelCharlson.ToDate, getdate())

left outer join
	[$(Warehouse)].WH.MortalityRiskModel RiskModelGender
on	RiskModelGender.DiagnosisGroupCode = SHMI.DiagnosisGroupCategoryCode
and	RiskModelGender.Predictor = 'GENDER_' + cast(SHMI.SexCategoryCode as varchar(2))
and	SHMI.DischargeDate between RiskModelGender.FromDate and coalesce(RiskModelGender.ToDate, getdate())

left outer join
	[$(Warehouse)].WH.MortalityRiskModel RiskModelIntercept
on	RiskModelIntercept.DiagnosisGroupCode = SHMI.DiagnosisGroupCategoryCode
and	RiskModelIntercept.Predictor = 'INTERCEPT'
and	SHMI.DischargeDate between RiskModelIntercept.FromDate and coalesce(RiskModelIntercept.ToDate, getdate())

left outer join
	[$(Warehouse)].WH.MortalityRiskModel RiskModelStartAge
on	RiskModelStartAge.DiagnosisGroupCode = SHMI.DiagnosisGroupCategoryCode
and	RiskModelStartAge.Predictor = 'STARTAGE_' + cast(SHMI.PatientAgeCategoryCode as varchar(2))
and	SHMI.DischargeDate between RiskModelStartAge.FromDate and coalesce(RiskModelStartAge.ToDate, getdate())

left outer join
	[$(Warehouse)].WH.MortalityRiskModel RiskModelAgeGroup -- RR added
on	RiskModelAgeGroup.DiagnosisGroupCode = SHMI.DiagnosisGroupCategoryCode
and	RiskModelAgeGroup.Predictor = 'AGE_GROUP_' + cast(SHMI.PatientAgeCategoryCode as varchar(2))
and	SHMI.DischargeDate between RiskModelAgeGroup.FromDate and coalesce(RiskModelAgeGroup.ToDate, getdate())

left outer join
	[$(Warehouse)].WH.MortalityRiskModel RiskModelYearIndex
on	RiskModelYearIndex.DiagnosisGroupCode = SHMI.DiagnosisGroupCategoryCode
and	RiskModelYearIndex.Predictor = 'YEAR_INDEX_' + cast(SHMI.YearIndexCode as varchar(2))
and	SHMI.DischargeDate between RiskModelYearIndex.FromDate and coalesce(RiskModelYearIndex.ToDate, getdate())
	


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		--'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿

CREATE proc [APC].[BuildSpecialtyConsultantBedday]

(
	@Start date
	,@End date
)

as

if object_id('tempdb..#Bedday') is not null
drop table #Bedday

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LocalStart date = @Start
	,@LocalEnd date = @End

declare
	@inserted int

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode = 'APC'
	,PODCode
	,HRGCode
	,CensusDate
	,StartTime
into
	#Bedday
from
	(	
	select
		MergeAPCEncounterRecno = BaseEncounter.MergeEncounterRecno
		,MergeDatasetEncounterRecno = BaseEncounter.MergeEncounterRecno
		,PODCode = BedDaySpecialtyConsultant.PODCode
		,HRGCode = BedDaySpecialtyConsultant.HRGCode
		,CensusDate = Calendar.TheDate
		,BaseEncounter.EpisodeStartTime
		,BaseEncounter.EpisodeEndTime
		,BaseEncounter.ContextCode
		,ProviderSpellNo = BaseEncounter.ProviderSpellNo
		,BaseWardStay.StartTime
	from 
		WH.Calendar Calendar

	inner join APC.BaseEncounter
	on 	Calendar.TheDate >= BaseEncounter.EpisodeStartDate
	and Calendar.TheDate <= coalesce(
									BaseEncounter.EpisodeEndDate
									,@LocalEnd
									)
	and BaseEncounter.Reportable = 1
	
	inner join APC.BedDaySpecialtyConsultant
	on	BaseEncounter.ConsultantCode = BedDaySpecialtyConsultant.SourceConsultantCode
	and BaseEncounter.SpecialtyCode  = BedDaySpecialtyConsultant.SourceSpecialtyCode
	and BaseEncounter.ContextCode = BedDaySpecialtyConsultant.ContextCode
	and Calendar.TheDate between BedDaySpecialtyConsultant.FromDate and coalesce(BedDaySpecialtyConsultant.ToDate, getdate()) 

	inner join APC.BaseWardStay
	on 	Calendar.TheDate >= BaseWardStay.StartDate
	and Calendar.TheDate <= coalesce(
									BaseWardStay.EndDate
									,@LocalEnd
									)
	and	BaseEncounter.ProviderSpellNo = BaseWardStay.ProviderSpellNo
	and BaseEncounter.ContextCode = BaseWardStay.ContextCode
	and	not
			(
				BaseWardStay.WardCode = 'W16' -- neuro rehab which should be counted seperately so does not attract bedday
			and	BaseWardStay.ContextCode = 'TRA||UG'
			)

	where
		Calendar.TheDate between @LocalStart and @LocalEnd

	) Bedday

insert into APC.BedDay

(
	[MergeAPCEncounterRecno]
	,[MergeDatasetEncounterRecno]
	,[DatasetCode]
	,[PODCode]
	,[HRGCode]
	,[CensusDate]
)

select
	MergeAPCEncounterRecno 
	,MergeDatasetEncounterRecno 
	,DatasetCode
	,PODCode
	,HRGCode 
	,CensusDate
from
	#Bedday Bedday
where
	not exists
			(
			select
				1
			from
				#Bedday BeddayLater
			where
				BeddayLater.MergeAPCEncounterRecno = Bedday.MergeAPCEncounterRecno
			and	BeddayLater.MergeDatasetEncounterRecno = Bedday.MergeDatasetEncounterRecno
			and BeddayLater.CensusDate = Bedday.CensusDate
			and BeddayLater.StartTime > Bedday.StartTime
			)
		

	

select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
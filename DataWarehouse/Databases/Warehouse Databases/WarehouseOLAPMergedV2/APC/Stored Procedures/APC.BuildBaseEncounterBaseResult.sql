﻿

CREATE Procedure [APC].[BuildBaseEncounterBaseResult]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @inserted int
	 ,@deleted int

	 
-- Insert ResultRecnos which are in the latest Result archive
Select
	MergeRecno
into 
	#ResultProcessList
from 
	Result.ProcessListArchive
where
	ArchiveTime = (Select MAX(ArchiveTime) from Result.ProcessListArchive)

-- Insert ResultRecno which are linked to an APC encounter which are in the latest APC archive
Insert into #ResultProcessList
Select
	MergeResultRecno
from
	APC.BaseEncounterBaseResult

inner join APC.ProcessListArchive
on BaseEncounterBaseResult.MergeEncounterRecno = ProcessListArchive.MergeRecno

where
	ArchiveTime = (Select MAX(ArchiveTime) from APC.ProcessListArchive)
and not exists
	(
	Select 
		1	
	from
		#ResultProcessList
	where
		#ResultProcessList.MergeRecno = BaseEncounterBaseResult.MergeResultRecno
	)

-- Insert ResultRecno of any records which are linked to APC but now the APC encounter recno no longer exists
Insert into #ResultProcessList
Select
	MergeResultRecno
from
	APC.BaseEncounterBaseResult
where 
	not exists
		(
		Select
			1
		from
			APC.Encounter
		where 
			BaseEncounterBaseResult.MergeEncounterRecno = Encounter.MergeEncounterRecno
		)
and not exists
	(
	Select
		1
	from
		#ResultProcessList Present
	where
		Present.MergeRecno = BaseEncounterBaseResult.MergeResultRecno
	)



-- Delete where the MergeResultRecno no longer exists in the Result table 
-- or 
-- the MergeResultRecno is in the temporary table created above as we'll re insert

delete
from
	APC.BaseEncounterBaseResult
where 
	not exists
		(
		Select
			1
		from
			Result.BaseResult
		where 
			BaseEncounterBaseResult.MergeResultRecno = BaseResult.MergeResultRecno
		)
or exists
	(
	Select
		1
	from
		#ResultProcessList
	where
		#ResultProcessList.MergeRecno = BaseEncounterBaseResult.MergeResultRecno
	)	

Select
	@deleted = @@ROWCOUNT


Select 
	MergeEncounterRecno
	,MergeResultRecno
	,EpisodeStartTime
into 
	#APCResult
from
	(
	Select
		Encounter.MergeEncounterRecno
		,BaseResult.MergeResultRecno
		,Encounter.EpisodeStartTime
	from 
		Result.BaseResult
	
	inner join dbo.#ResultProcessList
	on BaseResult.MergeResultRecno = #ResultProcessList.MergeRecno
	
	inner join APC.Encounter
	on BaseResult.DistrictNo = Encounter.DistrictNo
	and BaseResult.EffectiveTime between Encounter.EpisodeStartTime and coalesce(Encounter.EpisodeEndTime,getdate())
	and Encounter.ContextCode = 'CEN||PAS'
	
	where
		BaseResult.DistrictNo is not null
	
	union

	Select
		Encounter.MergeEncounterRecno
		,BaseResult.MergeResultRecno
		,Encounter.EpisodeStartTime
	from 
		Result.BaseResult

	inner join dbo.#ResultProcessList
	on BaseResult.MergeResultRecno = #ResultProcessList.MergeRecno
	
	inner join APC.Encounter
	on BaseResult.NHSNumber = Encounter.NHSNumber
	and BaseResult.EffectiveTime between Encounter.EpisodeStartTime and coalesce(Encounter.EpisodeEndTime,getdate())

	where
		BaseResult.NHSNumber is not null
			
	union

	Select
		Encounter.MergeEncounterRecno
		,BaseResult.MergeResultRecno
		,Encounter.EpisodeStartTime
	from 
		Result.BaseResult

	inner join dbo.#ResultProcessList
	on BaseResult.MergeResultRecno = #ResultProcessList.MergeRecno
	
	inner join APC.Encounter
	on BaseResult.CasenoteNumber = Encounter.CasenoteNumber
	and BaseResult.EffectiveTime between Encounter.EpisodeStartTime and coalesce(Encounter.EpisodeEndTime,getdate())
	and Encounter.ContextCode = 'CEN||PAS'
	
	where
		BaseResult.CasenoteNumber is not null
	) 
	APCResult
	

CREATE CLUSTERED INDEX [IX_APCTempTable_MergeResultRecno_EpisodeStartTime_MergeEncounterRecno]
ON [dbo].[#APCResult] ([MergeResultRecno],[EpisodeStartTime],[MergeEncounterRecno])	


Insert Into APC.BaseEncounterBaseResult
	(
	MergeEncounterRecno
	,MergeResultRecno
	)
Select 
	MergeEncounterRecno
	,MergeResultRecno
from 
	#APCResult
where
	not exists
		(
		Select	
			1
		from 
			#APCResult Latest
		where
			Latest.MergeResultRecno = #APCResult.MergeResultRecno
		and Latest.EpisodeStartTime > #APCResult.EpisodeStartTime
		)
and	not exists	
		(
		Select
			1
		from 
			#APCResult Latest
		where
			Latest.MergeResultRecno = #APCResult.MergeResultRecno
			and Latest.EpisodeStartTime = #APCResult.EpisodeStartTime
			and Latest.MergeEncounterRecno > #APCResult.MergeEncounterRecno
		)

select
	@inserted = @@ROWCOUNT
	
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Deleted: ' +   cast(coalesce(@deleted, 0) as varchar) +
		', Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


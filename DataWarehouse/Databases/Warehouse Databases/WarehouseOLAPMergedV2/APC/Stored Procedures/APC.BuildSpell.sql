﻿

CREATE procedure [APC].[BuildSpell] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

update
	APC.BaseEncounter
set
	 GlobalProviderSpellNo = null
	,GlobalEpisodeNo = null


declare @TransferInMergeRecno int
declare @TransferOutMergeRecno int
declare @TransferInAdmissionTime datetime

declare CrossSiteSpellCursor cursor fast_forward for

select distinct
	 CrossSiteTransfer.TransferInMergeRecno
	,CrossSiteTransfer.TransferOutMergeRecno
	,TransferIn.AdmissionTime
from
	(
	--Transfer from Central to Trafford
	select
		 TransferOutMergeRecno = BaseEncounter.MergeEncounterRecno
		,TransferInMergeRecno =
			(
			select
				min(TransferIn.MergeEncounterRecno)
			from
				APC.BaseEncounter TransferIn

			inner join [$(Chameleon)].Patient.PatientApplicationIdentity DestinationIdentity with (nolock)
			on	DestinationIdentity.PatientApplicationIdentityValue = TransferIn.SourcePatientNo
			and	DestinationIdentity.SourceApplicationIdentifierId = 2 --EPMINumber
						
			inner join [$(Chameleon)].Patient.PatientApplicationIdentity SourceIdentity with (nolock)
			on	SourceIdentity.PatientId = DestinationIdentity.PatientId
			and	SourceIdentity.SourceApplicationIdentifierId = 1 --DistrictNo
			and	SourceIdentity.PatientApplicationIdentityValue = BaseEncounter.DistrictNo

			where
				TransferIn.ContextCode = 'TRA||UG'
			and	TransferIn.AdmissionSourceCode in
				(
				 '51-TRINCEN' --should be this but no data
				--,'51-TRINCEN' --Transfer From Cen Site
				--,'52-TM' --Transferred Nhs Maternity Hospital
				)

			and	datediff(minute, BaseEncounter.DischargeTime, TransferIn.AdmissionTime) between -240 and 240 --8 hours
			)

	from
		APC.BaseEncounter
	where
		BaseEncounter.ContextCode = 'CEN||PAS'
	and	BaseEncounter.DischargeDestinationCode = 'TF' --Trans To Trafford
	
	and	not exists
		(
		select
			1
		from
			[$(ActivityBroker)].AB.ExcludedEncounter
		where
			ExcludedEncounter.SourceUniqueID = BaseEncounter.SourceUniqueID
		and	ExcludedEncounter.EPRApplicationContextID = 1
		and	ExcludedEncounter.EncounterTypeCode = 'APCSpellTransfer'
		)

	union all

	--Transfer from Trafford to Central
	select
		 TransferOutMergeRecno = BaseEncounter.MergeEncounterRecno
		,TransferInMergeRecno =
			(
			select
				min(TransferIn.MergeEncounterRecno)
			from
				APC.BaseEncounter TransferIn

			inner join [$(Chameleon)].Patient.PatientApplicationIdentity DestinationIdentity with (nolock)
			on	DestinationIdentity.PatientApplicationIdentityValue = TransferIn.DistrictNo
			and	DestinationIdentity.SourceApplicationIdentifierId = 1 --DistrictNumber
			
			inner join [$(Chameleon)].Patient.PatientApplicationIdentity SourceIdentity with (nolock)
			on	SourceIdentity.PatientId = DestinationIdentity.PatientId
			and	SourceIdentity.SourceApplicationIdentifierId = 2 --EPMINumber
			and	SourceIdentity.PatientApplicationIdentityValue = BaseEncounter.SourcePatientNo

			where
				TransferIn.ContextCode = 'CEN||PAS'
			and	TransferIn.AdmissionSourceCode = 'TF' --Trans From Trafford
			and	datediff(minute, BaseEncounter.DischargeTime, TransferIn.AdmissionTime) between -240 and 240 -- +/- 4 hours
			)

	from
		APC.BaseEncounter
	where
		BaseEncounter.ContextCode = 'TRA||UG'
	and	BaseEncounter.DischargeDestinationCode in
		(
		 '51-TFTToCE' --should be this but no data
		--,'51-TG' --08-Nhs Transferred General Hospital
		--,'52-TM' --09-Nhs Transferred Maternity Hospital
		)
	

	and	not exists
		(
		select
			1
		from
			[$(ActivityBroker)].AB.ExcludedEncounter
		where
			ExcludedEncounter.SourceUniqueID = BaseEncounter.SourceUniqueID
		and	ExcludedEncounter.EPRApplicationContextID = 2
		and	ExcludedEncounter.EncounterTypeCode = 'APCSpellTransfer'
		)
	) CrossSiteTransfer

inner join APC.BaseEncounter TransferIn
on	TransferIn.MergeEncounterRecno = CrossSiteTransfer.TransferInMergeRecno

where
	CrossSiteTransfer.TransferInMergeRecno is not null

order by
	TransferIn.AdmissionTime


OPEN CrossSiteSpellCursor

FETCH NEXT FROM CrossSiteSpellCursor
INTO
	 @TransferInMergeRecno
	,@TransferOutMergeRecno
	,@TransferInAdmissionTime

WHILE @@FETCH_STATUS = 0

BEGIN

	update
		APC.BaseEncounter
	set
		GlobalProviderSpellNo =
		coalesce(
			 BaseEncounter.GlobalProviderSpellNo
			,(
			select
				TransferOut.ProviderSpellNo
			from
				APC.BaseEncounter TransferOut
			where
				TransferOut.MergeEncounterRecno = @TransferOutMergeRecno
			)
		)
	from
		APC.BaseEncounter
	where
		BaseEncounter.ProviderSpellNo =
			(
			select
				TransferIn.ProviderSpellNo
			from
				APC.BaseEncounter TransferIn
			where
				TransferIn.MergeEncounterRecno = @TransferInMergeRecno
			)


	FETCH NEXT FROM CrossSiteSpellCursor
	INTO
		 @TransferInMergeRecno
		,@TransferOutMergeRecno
		,@TransferInAdmissionTime

END
  
CLOSE CrossSiteSpellCursor
DEALLOCATE CrossSiteSpellCursor


--Assign the GlobalProviderSpellNo for all other episodes and derive the GlobalEpisodeNo
update
	APC.BaseEncounter
set
	GlobalProviderSpellNo =
		Activity.GlobalProviderSpellNo

	,GlobalEpisodeNo =
		Activity.GlobalEpisodeNo
from
	APC.BaseEncounter

inner join
	(
	select
		 BaseEncounter.MergeEncounterRecno
		,GlobalProviderSpellNo =
			coalesce(BaseEncounter.GlobalProviderSpellNo, BaseEncounter.ProviderSpellNo)
			

		,GlobalEpisodeNo =
			ROW_NUMBER()
			over(
				partition by
					coalesce(BaseEncounter.GlobalProviderSpellNo, BaseEncounter.ProviderSpellNo)
				order by
					BaseEncounter.EpisodeStartTime
			)
	from
		APC.BaseEncounter
	) Activity
on	Activity.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno



truncate table APC.Spell

insert
into
	APC.Spell
(
	 AdmissionEpisodeMergeRecno
	,DischargeEpisodeMergeRecno
)
select
	 AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno
	,DischargeEpisodeMergeRecno = Discharge.MergeEncounterRecno
from
	APC.BaseEncounter Admission

left join APC.BaseEncounter Discharge
on	Discharge.GlobalProviderSpellNo = Admission.GlobalProviderSpellNo
and Discharge.Reportable = 1

and	not exists
	(
	select
		1
	from
		APC.BaseEncounter Previous
	where
		Previous.GlobalProviderSpellNo = Admission.GlobalProviderSpellNo
	and	Previous.GlobalEpisodeNo > Discharge.GlobalEpisodeNo
	and Previous.Reportable = 1
	)

where
	Admission.GlobalEpisodeNo = 1
and Admission.Reportable = 1


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


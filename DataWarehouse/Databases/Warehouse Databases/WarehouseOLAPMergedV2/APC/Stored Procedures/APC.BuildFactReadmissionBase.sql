﻿CREATE procedure [APC].[BuildFactReadmissionBase] as

set transaction isolation level read uncommitted

 

truncate table APC.FactReadmissionBase 

insert
into
	APC.FactReadmissionBase
(
	 ReadmissionTypeID 
	,ReadmissionIntervalDays 
	,ReadmissionEncounterMergeEncounterRecno 
	,ReadmissionDirectorateID 
	,ReadmissionAdmissionMethodID 
	,ReadmissionSpecialtyID 
	,ReadmissionAdmissionDateID 
	,ReadmissionDischargeDateID
	,ReadmissionConsultantID 
	,ReadmissionSiteID 
	,ReadmissionAgeID 
	,ReadmissionCCGID
	,ReadmissionPatientCategoryID
	,ReadmissionStartWardID
	,ReadmissionAdmissionTime
	,InitialAdmissionEncounterMergeEncounterRecno 
	,InitialAdmissionDischargeDateID 
	,InitialAdmissionConsultantID 
	,InitialAdmissionDirectorateID 
	,InitialAdmissionSpecialtyID 
	,InitialAdmissionSiteID 
	,InitialAdmissionAdmissionMethodID 
	,InitialAdmissionAgeID 
	,InitialAdmissionCCGID 
	,InitialAdmissionPatientCategoryID
	,InitialAdmissionEndWardID
	,TrustWide 
	,InitialAdmissionAdmissionTime
)
select
	 ReadmissionTypeID = 2 --CQUIN - 28 day readmission

	,ReadmissionIntervalDays =
		datediff(
			 day
			,InitialAdmissionEncounter.DischargeDate
			,ReadmissionEncounter.AdmissionDate
		)

	,ReadmissionEncounterMergeEncounterRecno = ReadmissionEncounter.MergeEncounterRecno
	,ReadmissionDirectorateID = ReadmissionEncounterDirectorate.DirectorateID
	,ReadmissionAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID
	,ReadmissionSpecialtyID = ReadmissionEncounterReference.SpecialtyID
	,ReadmissionAdmissionDateID = ReadmissionEncounterReference.AdmissionDateID
	,ReadmissionDischargeDateID = ReadmissionEncounterReference.DischargeDateID
	,ReadmissionConsultantID = ReadmissionEncounterReference.ConsultantID
	,ReadmissionSiteID = ReadmissionEncounterReference.StartSiteID
	,ReadmissionAgeID = ReadmissionEncounterReference.AgeID
	,ReadmissionCCGID = ReadmissionEncounterReference.CCGID
	,ReadmissionPatientCategoryID = ReadmissionPatientCategory.PatientCategoryID
	,ReadmissionStartWardID = ReadmissionEncounterReference.StartWardID

	,ReadmissionAdmissionTime = ReadmissionEncounter.AdmissionTime

	,InitialAdmissionEncounterMergeEncounterRecno = InitialAdmissionEncounter.MergeEncounterRecno
	,InitialAdmissionDischargeDateID = InitialAdmissionEncounterReference.DischargeDateID
	,InitialAdmissionConsultantID = InitialAdmissionEncounterReference.ConsultantID
	,InitialAdmissionDirectorateID = InitialAdmissionEncounterDirectorate.DirectorateID
	,InitialAdmissionSpecialtyID = InitialAdmissionEncounterReference.SpecialtyID
	,InitialAdmissionSiteID = InitialAdmissionEncounterReference.StartSiteID
	,InitialAdmissionAdmissionMethodID = InitialAdmissionEncounterReference.AdmissionMethodID
	,InitialAdmissionAgeID = InitialAdmissionEncounterReference.AgeID
	,InitialAdmissionCCGID = InitialAdmissionEncounterReference.CCGID
	,InitialAdmissionPatientCategoryID = InitialAdmissionPatientCategory.PatientCategoryID
	,InitialAdmissionEndWardID = InitialAdmissionEncounterReference.EndWardID

	,Readmission.TrustWide

	,InitialAdmissionAdmissionTime = InitialAdmissionEncounter.AdmissionTime
from
	APC.Readmission

inner join APC.BaseEncounter ReadmissionEncounter
on	ReadmissionEncounter.MergeEncounterRecno = Readmission.ReadmissionRecno
and ReadmissionEncounter.Reportable = 1

inner join APC.BaseEncounterReference ReadmissionEncounterReference
on	ReadmissionEncounterReference.MergeEncounterRecno = Readmission.ReadmissionRecno

inner join APC.BaseEncounter AdmissionEncounter
on	AdmissionEncounter.MergeEncounterRecno = Readmission.InitialAdmissionRecno
and AdmissionEncounter.Reportable = 1

inner join APC.BaseEncounter InitialAdmissionEncounter
on	InitialAdmissionEncounter.GlobalProviderSpellNo = AdmissionEncounter.GlobalProviderSpellNo
and	InitialAdmissionEncounter.GlobalEpisodeNo = 1
and InitialAdmissionEncounter.Reportable = 1

inner join APC.BaseEncounterReference InitialAdmissionEncounterReference
on	InitialAdmissionEncounterReference.MergeEncounterRecno = InitialAdmissionEncounter.MergeEncounterRecno

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = InitialAdmissionEncounter.MergeEncounterRecno

inner join APC.BaseEncounter InitialDischargeEncounter
on	InitialDischargeEncounter.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno
and InitialDischargeEncounter.Reportable = 1

inner join APC.BaseEncounterReference InitialDischargeEncounterReference
on	InitialDischargeEncounterReference.MergeEncounterRecno = InitialDischargeEncounter.MergeEncounterRecno

inner join APC.AdmissionMethod ReadmissionAdmissionAdmissionMethod
on	ReadmissionAdmissionAdmissionMethod.SourceAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID

inner join APC.AdmissionMethod InitialAdmissionAdmissionMethod
on	InitialAdmissionAdmissionMethod.SourceAdmissionMethodID = InitialAdmissionEncounterReference.AdmissionMethodID

inner join WH.Specialty ReadmissionAdmissionSpecialty
on	ReadmissionAdmissionSpecialty.SourceSpecialtyID = ReadmissionEncounterReference.SpecialtyID

inner join WH.Specialty InitialAdmissionSpecialty
on	InitialAdmissionSpecialty.SourceSpecialtyID = InitialAdmissionEncounterReference.SpecialtyID

inner join APC.PatientClassification ReadmissionAdmissionPatientClassification
on	ReadmissionAdmissionPatientClassification.SourcePatientClassificationID = ReadmissionEncounterReference.PatientClassificationID

inner join WH.Directorate InitialAdmissionEncounterDirectorate
on	InitialAdmissionEncounterDirectorate.DirectorateCode = InitialDischargeEncounter.EndDirectorateCode

inner join WH.Directorate ReadmissionEncounterDirectorate
on	ReadmissionEncounterDirectorate.DirectorateCode = ReadmissionEncounter.StartDirectorateCode

inner join APC.PatientCategory InitialAdmissionPatientCategory
on	InitialAdmissionPatientCategory.PatientCategoryCode = InitialAdmissionEncounter.PatientCategoryCode

inner join APC.PatientCategory ReadmissionPatientCategory
on	ReadmissionPatientCategory.PatientCategoryCode = ReadmissionEncounter.PatientCategoryCode

where
	ReadmissionAdmissionAdmissionMethod.NationalAdmissionMethodCode in ('21', '22', '23', '24', '28')
and	datediff(day, InitialDischargeEncounter.DischargeDate, ReadmissionEncounter.AdmissionDate) < 28

and	InitialAdmissionAdmissionMethod.NationalAdmissionMethodCode in ('11', '12', '13', '21', '22', '23', '24', '28', '31', '32', '81', '82', '83', '84', '89')
and	InitialAdmissionSpecialty.NationalSpecialtyCode not in
	(
	 '501'
	,'560'
	,'610'
	,'700'
	,'701'
	,'702'
	,'703'
	,'704'
	,'705'
	,'706'
	,'707'
	,'708'
	,'709'
	,'710'
	,'711'
	,'712'
	,'713'
	,'714'
	,'715'
	)

and	ReadmissionAdmissionSpecialty.NationalSpecialtyCode not in
	(
	 '501'
	,'560'
	,'610'
	,'700'
	,'701'
	,'702'
	,'703'
	,'704'
	,'705'
	,'706'
	,'707'
	,'708'
	,'709'
	,'710'
	,'711'
	,'712'
	,'713'
	,'714'
	,'715'
	)

and	not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.GlobalProviderSpellNo = InitialAdmissionEncounter.GlobalProviderSpellNo
	and	left(BaseEncounter.PrimaryDiagnosisCode, 1) = 'O'
	and BaseEncounter.Reportable = 1
	)

and	not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.GlobalProviderSpellNo = ReadmissionEncounter.GlobalProviderSpellNo
	and	left(BaseEncounter.PrimaryDiagnosisCode, 1) = 'O'
	and BaseEncounter.Reportable = 1
	)

and	not exists
	(
	select
		1
	from
		APC.BaseEncounter

	inner join APC.BaseDiagnosis
	on	BaseDiagnosis.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	and BaseEncounter.Reportable = 1
	
	where
		BaseEncounter.SourcePatientNo = InitialAdmissionEncounter.SourcePatientNo
	and	BaseEncounter.ContextCode = InitialAdmissionEncounter.ContextCode
	and	datediff(day, BaseEncounter.AdmissionDate, InitialAdmissionEncounter.AdmissionDate) between 0 and 365
	and	BaseEncounter.AdmissionTime <= InitialAdmissionEncounter.AdmissionTime

	and	(
			left(BaseDiagnosis.DiagnosisCode, 3) between 'C00' and 'C97'
		or	left(BaseDiagnosis.DiagnosisCode, 3) between 'D37' and 'D48'
		or	BaseDiagnosis.DiagnosisCode = 'Z51.1'
		)
	)

and	not exists
	(
	select
		1
	from
		APC.BaseEncounter

	inner join APC.BaseDiagnosis
	on	BaseDiagnosis.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	and BaseEncounter.Reportable = 1

	where
		BaseEncounter.SourcePatientNo = ReadmissionEncounter.SourcePatientNo
	and	BaseEncounter.ContextCode = ReadmissionEncounter.ContextCode
	and	datediff(day, BaseEncounter.AdmissionDate, ReadmissionEncounter.AdmissionDate) between 0 and 365
	and	BaseEncounter.AdmissionTime <= ReadmissionEncounter.AdmissionTime

	and	(
			left(BaseDiagnosis.DiagnosisCode, 3) between 'C00' and 'C97'
		or	left(BaseDiagnosis.DiagnosisCode, 3) between 'D37' and 'D48'
		or	BaseDiagnosis.DiagnosisCode = 'Z51.1'
		)
	)

and	ReadmissionAdmissionPatientClassification.NationalPatientClassificationCode not in ('3', '4')

union all

select
	 ReadmissionTypeID = 1 --Commissioning - 30 day readmission

	,ReadmissionIntervalDays =
		datediff(
			 day
			,InitialAdmissionEncounter.DischargeDate
			,ReadmissionEncounter.AdmissionDate
		)

	,ReadmissionEncounterMergeEncounterRecno = ReadmissionEncounter.MergeEncounterRecno
	,ReadmissionDirectorateID = ReadmissionEncounterDirectorate.DirectorateID
	,ReadmissionAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID
	,ReadmissionSpecialtyID = ReadmissionEncounterReference.SpecialtyID
	,ReadmissionAdmissionDateID = ReadmissionEncounterReference.AdmissionDateID
	,ReadmissionDischargeDateID = ReadmissionEncounterReference.DischargeDateID
	,ReadmissionConsultantID = ReadmissionEncounterReference.ConsultantID
	,ReadmissionSiteID = ReadmissionEncounterReference.StartSiteID
	,ReadmissionAgeID = ReadmissionEncounterReference.AgeID
	,ReadmissionCCGID = ReadmissionEncounterReference.CCGID
	,ReadmissionPatientCategoryID = ReadmissionPatientCategory.PatientCategoryID
	,ReadmissionStartWardID = ReadmissionEncounterReference.StartWardID

	,ReadmissionAdmissionTime = ReadmissionEncounter.AdmissionTime

	,InitialAdmissionEncounterMergeEncounterRecno = InitialAdmissionEncounter.MergeEncounterRecno
	,InitialAdmissionDischargeDateID = InitialDischargeEncounterReference.DischargeDateID
	,InitialAdmissionConsultantID = InitialAdmissionEncounterReference.ConsultantID
	,InitialAdmissionDirectorateID = InitialAdmissionEncounterDirectorate.DirectorateID
	,InitialAdmissionSpecialtyID = InitialAdmissionEncounterReference.SpecialtyID
	,InitialAdmissionSiteID = InitialAdmissionEncounterReference.StartSiteID
	,InitialAdmissionAdmissionMethodID = InitialAdmissionEncounterReference.AdmissionMethodID
	,InitialAdmissionAgeID = InitialAdmissionEncounterReference.AgeID
	,InitialAdmissionCCGID = InitialAdmissionEncounterReference.CCGID
	,InitialAdmissionPatientCategoryID = InitialAdmissionPatientCategory.PatientCategoryID
	,InitialAdmissionEndWardID = InitialAdmissionEncounterReference.EndWardID

	,Readmission.TrustWide

	,InitialAdmissionAdmissionTime = InitialAdmissionEncounter.AdmissionTime
from
	APC.Readmission

inner join APC.BaseEncounter ReadmissionEncounter
on	ReadmissionEncounter.MergeEncounterRecno = Readmission.ReadmissionRecno
and ReadmissionEncounter.Reportable = 1

inner join APC.BaseEncounterReference ReadmissionEncounterReference
on	ReadmissionEncounterReference.MergeEncounterRecno = Readmission.ReadmissionRecno

inner join APC.BaseEncounter AdmissionEncounter
on	AdmissionEncounter.MergeEncounterRecno = Readmission.InitialAdmissionRecno
and AdmissionEncounter.Reportable = 1

inner join APC.BaseEncounter InitialAdmissionEncounter
on	InitialAdmissionEncounter.GlobalProviderSpellNo = AdmissionEncounter.GlobalProviderSpellNo
and	InitialAdmissionEncounter.GlobalEpisodeNo = 1
and InitialAdmissionEncounter.Reportable = 1

inner join APC.BaseEncounterReference InitialAdmissionEncounterReference
on	InitialAdmissionEncounterReference.MergeEncounterRecno = InitialAdmissionEncounter.MergeEncounterRecno

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = InitialAdmissionEncounter.MergeEncounterRecno

inner join APC.BaseEncounter InitialDischargeEncounter
on	InitialDischargeEncounter.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno
and InitialDischargeEncounter.Reportable = 1

inner join APC.BaseEncounterReference InitialDischargeEncounterReference
on	InitialDischargeEncounterReference.MergeEncounterRecno = InitialDischargeEncounter.MergeEncounterRecno

inner join APC.AdmissionMethod ReadmissionAdmissionAdmissionMethod
on	ReadmissionAdmissionAdmissionMethod.SourceAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID

inner join APC.AdmissionMethod InitialAdmissionAdmissionMethod
on	InitialAdmissionAdmissionMethod.SourceAdmissionMethodID = InitialAdmissionEncounterReference.AdmissionMethodID

inner join WH.Specialty ReadmissionAdmissionSpecialty
on	ReadmissionAdmissionSpecialty.SourceSpecialtyID = ReadmissionEncounterReference.SpecialtyID

inner join WH.Specialty InitialAdmissionSpecialty
on	InitialAdmissionSpecialty.SourceSpecialtyID = InitialAdmissionEncounterReference.SpecialtyID

inner join APC.PatientClassification InitialAdmissionPatientClassification
on	InitialAdmissionPatientClassification.SourcePatientClassificationID = InitialAdmissionEncounterReference.PatientClassificationID

inner join APC.PatientClassification ReadmissionAdmissionPatientClassification
on	ReadmissionAdmissionPatientClassification.SourcePatientClassificationID = ReadmissionEncounterReference.PatientClassificationID

inner join WH.Directorate InitialAdmissionEncounterDirectorate
on	InitialAdmissionEncounterDirectorate.DirectorateCode = InitialDischargeEncounter.EndDirectorateCode

inner join WH.Directorate ReadmissionEncounterDirectorate
on	ReadmissionEncounterDirectorate.DirectorateCode = ReadmissionEncounter.StartDirectorateCode

--the HRG grouped encounter may not be the discharge encounter so link through via the GlobalProviderSpellNo
inner join APC.BaseEncounter HRGEncounter
on	HRGEncounter.ProviderSpellNo = InitialAdmissionEncounter.GlobalProviderSpellNo

inner join APC.HRG4Spell InitialAdmissionHRG4Spell
on	InitialAdmissionHRG4Spell.MergeEncounterRecno = HRGEncounter.MergeEncounterRecno

--the HRG spell table always contains the 1st episode of the spell
inner join APC.HRG4Spell ReadmissionHRG4Spell
on	ReadmissionHRG4Spell.MergeEncounterRecno = ReadmissionEncounter.MergeEncounterRecno

inner join APC.DischargeMethod InitialAdmissionDischargeMethod
on	InitialAdmissionDischargeMethod.SourceDischargeMethodID = InitialDischargeEncounterReference.DischargeMethodID

inner join APC.PatientCategory InitialAdmissionPatientCategory
on	InitialAdmissionPatientCategory.PatientCategoryCode = InitialAdmissionEncounter.PatientCategoryCode

inner join APC.PatientCategory ReadmissionPatientCategory
on	ReadmissionPatientCategory.PatientCategoryCode = ReadmissionEncounter.PatientCategoryCode

where
	ReadmissionAdmissionAdmissionMethod.NationalAdmissionMethodCode in ('21', '22', '23', '24', '25', '28')
and	datediff(day, InitialDischargeEncounter.DischargeDate, ReadmissionEncounter.AdmissionDate) < 30

--Transfers...
and	case
	when
		ReadmissionAdmissionAdmissionMethod.NationalAdmissionMethodCode = '81' --Transfer Of Any Admitted Patient From Other Hospital Provider Other Than In An Emergency
	then
		case
		when
			InitialAdmissionAdmissionMethod.NationalAdmissionMethodCode = '81' --Transfer Of Any Admitted Patient From Other Hospital Provider Other Than In An Emergency
		then 0
		else 1
		end

	else 1
	end = 1


--Exclusions...

--Either the initial admission or readmission is cross-border activity
and	not exists
	(
	select
		1
	from
		[$(Organisation)].ODS.Postcode
	where
		Postcode.Postcode = InitialAdmissionEncounter.Postcode
	and	(
			left(Postcode.DistrictOfResidence, 1) in ('S', 'Z')
		or	left(Postcode.CCGCode, 1) = '7'
		)
	)

and	not exists
	(
	select
		1
	from
		[$(Organisation)].ODS.Postcode
	where
		Postcode.Postcode = ReadmissionEncounter.Postcode
	and	(
			left(Postcode.DistrictOfResidence, 1) in ('S', 'Z')
		or	left(Postcode.CCGCode, 1) = '7'
		)
	)

--Either the initial admission spell or readmission spell has an unbundled HRG in subchapter SB or SC (chemotherapy or radiotherapy)
and	not exists
	(
	select
		1
	from
		APC.HRG4Unbundled

	inner join APC.BaseEncounter
	on	BaseEncounter.ProviderSpellNo = InitialAdmissionEncounter.GlobalProviderSpellNo
	and	BaseEncounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
	and BaseEncounter.Reportable = 1
	
	where
		left(HRG4Unbundled.HRGCode, 2) in ('SB', 'SC') --chemotherapy or radiotherapy
	)

and	not exists
	(
	select
		1
	from
		APC.HRG4Unbundled

	inner join APC.BaseEncounter
	on	BaseEncounter.ProviderSpellNo = ReadmissionEncounter.GlobalProviderSpellNo
	and	BaseEncounter.MergeEncounterRecno = HRG4Unbundled.MergeEncounterRecno
	and BaseEncounter.Reportable = 1
	
	where
		left(HRG4Unbundled.HRGCode, 2) in ('SB', 'SC') --chemotherapy or radiotherapy
	)

--Either the initial admission or readmission has a spell primary diagnosis of C00-C97 or D37-D48
and	InitialAdmissionHRG4Spell.PrimaryDiagnosisCode not between 'C00' and 'C97'
and	InitialAdmissionHRG4Spell.PrimaryDiagnosisCode not between 'D37' and 'D48'

and	ReadmissionHRG4Spell.PrimaryDiagnosisCode not between 'C00' and 'C97'
and	ReadmissionHRG4Spell.PrimaryDiagnosisCode not between 'D37' and 'D48'

--Either the initial admission or the readmission has a spell core HRG in chapter N (obstetric medicine)
and	left(InitialAdmissionHRG4Spell.HRGCode, 1) <> 'N' --(obstetric medicine)
and	left(ReadmissionHRG4Spell.HRGCode, 1) <> 'N' --(obstetric medicine)

--Readmission spell is excluded from PbR (e.g. via TFC or HRG exclusions)
and	ReadmissionHRG4Spell.HRGCode not in
	(
	select
		EntityLookup.EntityCode
	from
		Utility.EntityLookup
	where
		EntityLookup.EntityTypeCode = 'HRGEXCLUSIONS'
	)

and	ReadmissionAdmissionSpecialty.NationalSpecialtyCode not in
	(
	select
		EntityLookup.EntityCode
	from
		Utility.EntityLookup
	where
		EntityLookup.EntityTypeCode = 'TFCEXCLUSIONS'
	)

and	not exists
	(
	select
		1
	from
		Allocation.DatasetAllocation

	inner join Allocation.AllocationType
	on	AllocationType.AllocationTypeID = DatasetAllocation.AllocationTypeID

	inner join Allocation.Allocation
	on	Allocation.AllocationID = DatasetAllocation.AllocationID

	where
		--AllocationType.AllocationType = 'Contract Exclusion' --4
	--and	
		DatasetAllocation.DatasetCode = 'APC'
	and	DatasetAllocation.DatasetRecno = Readmission.ReadmissionRecno
	--and	Allocation.SourceAllocationID <> '-1' --N/A
	and	DatasetAllocation.AllocationID in(2088688, 2088746, 2088801,2088773,2088774)    
	)


--The readmission is an emergency transfer (admission method code 2B)
and	ReadmissionAdmissionAdmissionMethod.NationalAdmissionMethodCode <> '28'

--The readmission has a start age of less than 4
and	datediff(
		 year
		,ReadmissionEncounter.DateOfBirth
		,ReadmissionEncounter.AdmissionDate
	) -
	case
	when DATEPART(month, ReadmissionEncounter.DateOfBirth) > DATEPART(month, ReadmissionEncounter.AdmissionDate)
	then 1
	else 0
	end

	 > 3

--The patient is being readmitted having self discharged against clinical advice (included in discharge method code 2)
and	InitialAdmissionDischargeMethod.NationalDischargeMethodCode <> '2'

--The patient is receiving renal dialysis
and	not
	(
		InitialAdmissionPatientClassification.NationalPatientClassificationCode in( '4','3')	--Regular Day Admission
	--and	InitialAdmissionSpecialty.SourceSpecialtyCode like 'IH%'
	--and	InitialAdmissionSpecialty.NationalSpecialtyCode in ('259', '361')

--Renal Dialysis Excluded HRGs (13/14):
	and	ReadmissionHRG4Spell.HRGCode in
		(
		 'LA08E'
		,'LD01A'
		,'LD02A'
		,'LD03A'
		,'LD04A'
		,'LD05A'
		,'LD06A'
		,'LD07A'
		,'LD08A'
		,'LD09A'
		,'LD10A'
		,'LD11A'
		,'LD12A'
		,'LD13A'
		,'LD01B'
		,'LD02B'
		,'LD03B'
		,'LD04B'
		,'LD05B'
		,'LD06B'
		,'LD07B'
		,'LD08B'
		,'LD09B'
		,'LD10B'
		,'LD11B'
		,'LD12B'
		,'LD13B'
		)
	)

--The patient is receiving renal dialysis
--and	not
--	(
--		InitialAdmissionPatientClassification.NationalPatientClassificationCode = '4'	--Regular night Admission
--	and	InitialAdmissionSpecialty.SourceSpecialtyCode like 'IH%'
--	and	InitialAdmissionSpecialty.NationalSpecialtyCode in ('259', '361')
--	)

--and	not
--	(
--		ReadmissionAdmissionPatientClassification.NationalPatientClassificationCode = '3'	--Regular Day Admission
--	and	ReadmissionAdmissionSpecialty.SourceSpecialtyCode like 'IH%'
--	and	ReadmissionAdmissionSpecialty.NationalSpecialtyCode in ('259', '361')
--	)

--The patient is being readmitted subsequent to a transplant
and not exists
	(
	select
		1
	from
		APC.BaseEncounter
	where
		BaseEncounter.GlobalProviderSpellNo = InitialAdmissionEncounter.GlobalProviderSpellNo
	and BaseEncounter.Reportable = 1

--Transplant excluded HRGs (13/14):
	and	ReadmissionHRG4Spell.HRGCode in
		(
		 'DZ01Z'
		,'EA01Z'
		,'EA02Z'
		,'GA01A'
		,'GA01B'
		,'GA01C'
		,'GA12Z'
		,'LA01A'
		,'LA01B'
		,'LA02A'
		,'LA02B'
		,'LA03A'
		,'LA03B'
		,'LA11Z'
		,'LA12A'
		,'LA12B'
		,'LA13A'
		,'LA13B'
		,'LA14Z'
		,'SA19A'
		,'SA19B'
		,'SA20A'
		,'SA20B'
		,'SA21A'
		,'SA21B'
		,'SA22A'
		,'SA22B'
		,'SA23A'
		,'SA23B'
		,'SA26A'
		,'SA26B'
		,'SA27A'
		,'SA27B'
		,'SA28A'
		,'SA28B'
		)
	)

and	ReadmissionEncounter.DischargeDate is not null


union all

select
	 ReadmissionTypeID = 3 --7 Day Readmissions

	,ReadmissionIntervalDays =
		datediff(
			 day
			,InitialAdmissionEncounter.DischargeDate
			,ReadmissionEncounter.AdmissionDate
		)

	,ReadmissionEncounterMergeEncounterRecno = ReadmissionEncounter.MergeEncounterRecno
	,ReadmissionDirectorateID = ReadmissionEncounterDirectorate.DirectorateID
	,ReadmissionAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID
	,ReadmissionSpecialtyID = ReadmissionEncounterReference.SpecialtyID
	,ReadmissionAdmissionDateID = ReadmissionEncounterReference.AdmissionDateID
	,ReadmissionDischargeDateID = ReadmissionEncounterReference.DischargeDateID
	,ReadmissionConsultantID = ReadmissionEncounterReference.ConsultantID
	,ReadmissionSiteID = ReadmissionEncounterReference.StartSiteID
	,ReadmissionAgeID = ReadmissionEncounterReference.AgeID
	,ReadmissionCCGID = ReadmissionEncounterReference.CCGID
	,ReadmissionPatientCategoryID = ReadmissionPatientCategory.PatientCategoryID
	,ReadmissionStartWardID = ReadmissionEncounterReference.StartWardID

	,ReadmissionAdmissionTime = ReadmissionEncounter.AdmissionTime

	,InitialAdmissionEncounterMergeEncounterRecno = InitialAdmissionEncounter.MergeEncounterRecno
	,InitialAdmissionDischargeDateID = InitialAdmissionEncounterReference.DischargeDateID
	,InitialAdmissionConsultantID = InitialAdmissionEncounterReference.ConsultantID
	,InitialAdmissionDirectorateID = InitialAdmissionEncounterDirectorate.DirectorateID
	,InitialAdmissionSpecialtyID = InitialAdmissionEncounterReference.SpecialtyID
	,InitialAdmissionSiteID = InitialAdmissionEncounterReference.StartSiteID
	,InitialAdmissionAdmissionMethodID = InitialAdmissionEncounterReference.AdmissionMethodID
	,InitialAdmissionAgeID = InitialAdmissionEncounterReference.AgeID
	,InitialAdmissionCCGID = InitialAdmissionEncounterReference.CCGID
	,InitialAdmissionPatientCategoryID = InitialAdmissionPatientCategory.PatientCategoryID
	,InitialAdmissionEndWardID = InitialAdmissionEncounterReference.EndWardID

	,Readmission.TrustWide

	,InitialAdmissionAdmissionTime = InitialAdmissionEncounter.AdmissionTime
from
	APC.Readmission

inner join APC.BaseEncounter ReadmissionEncounter
on	ReadmissionEncounter.MergeEncounterRecno = Readmission.ReadmissionRecno
and ReadmissionEncounter.Reportable = 1

inner join APC.BaseEncounterReference ReadmissionEncounterReference
on	ReadmissionEncounterReference.MergeEncounterRecno = Readmission.ReadmissionRecno

inner join APC.BaseEncounter AdmissionEncounter
on	AdmissionEncounter.MergeEncounterRecno = Readmission.InitialAdmissionRecno
and AdmissionEncounter.Reportable = 1

inner join APC.BaseEncounter InitialAdmissionEncounter
on	InitialAdmissionEncounter.GlobalProviderSpellNo = AdmissionEncounter.GlobalProviderSpellNo
and	InitialAdmissionEncounter.GlobalEpisodeNo = 1
and InitialAdmissionEncounter.Reportable = 1

inner join APC.BaseEncounterReference InitialAdmissionEncounterReference
on	InitialAdmissionEncounterReference.MergeEncounterRecno = InitialAdmissionEncounter.MergeEncounterRecno

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = InitialAdmissionEncounter.MergeEncounterRecno

inner join APC.BaseEncounter InitialDischargeEncounter
on	InitialDischargeEncounter.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno
and InitialDischargeEncounter.Reportable = 1

inner join APC.BaseEncounterReference InitialDischargeEncounterReference
on	InitialDischargeEncounterReference.MergeEncounterRecno = InitialDischargeEncounter.MergeEncounterRecno

inner join APC.AdmissionMethod ReadmissionAdmissionAdmissionMethod
on	ReadmissionAdmissionAdmissionMethod.SourceAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID

inner join APC.AdmissionMethod InitialAdmissionAdmissionMethod
on	InitialAdmissionAdmissionMethod.SourceAdmissionMethodID = InitialAdmissionEncounterReference.AdmissionMethodID

inner join WH.Specialty ReadmissionAdmissionSpecialty
on	ReadmissionAdmissionSpecialty.SourceSpecialtyID = ReadmissionEncounterReference.SpecialtyID

inner join WH.Specialty InitialAdmissionSpecialty
on	InitialAdmissionSpecialty.SourceSpecialtyID = InitialAdmissionEncounterReference.SpecialtyID

inner join APC.PatientClassification ReadmissionAdmissionPatientClassification
on	ReadmissionAdmissionPatientClassification.SourcePatientClassificationID = ReadmissionEncounterReference.PatientClassificationID

inner join WH.Directorate InitialAdmissionEncounterDirectorate
on	InitialAdmissionEncounterDirectorate.DirectorateCode = InitialDischargeEncounter.EndDirectorateCode

inner join WH.Directorate ReadmissionEncounterDirectorate
on	ReadmissionEncounterDirectorate.DirectorateCode = ReadmissionEncounter.StartDirectorateCode

inner join APC.PatientCategory InitialAdmissionPatientCategory
on	InitialAdmissionPatientCategory.PatientCategoryCode = InitialAdmissionEncounter.PatientCategoryCode

inner join APC.PatientCategory ReadmissionPatientCategory
on	ReadmissionPatientCategory.PatientCategoryCode = ReadmissionEncounter.PatientCategoryCode

where
	ReadmissionAdmissionAdmissionMethod.NationalAdmissionMethodCode in ('21', '22', '23', '24', '28')
and	datediff(day, InitialDischargeEncounter.DischargeDate, ReadmissionEncounter.AdmissionDate) < 7


union all

select
	 ReadmissionTypeID = 4 --48 Hour Readmissions

	,ReadmissionIntervalDays =
		datediff(
			 day
			,InitialAdmissionEncounter.DischargeDate
			,ReadmissionEncounter.AdmissionDate
		)

	,ReadmissionEncounterMergeEncounterRecno = ReadmissionEncounter.MergeEncounterRecno
	,ReadmissionDirectorateID = ReadmissionEncounterDirectorate.DirectorateID
	,ReadmissionAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID
	,ReadmissionSpecialtyID = ReadmissionEncounterReference.SpecialtyID
	,ReadmissionAdmissionDateID = ReadmissionEncounterReference.AdmissionDateID
	,ReadmissionDischargeDateID = ReadmissionEncounterReference.DischargeDateID
	,ReadmissionConsultantID = ReadmissionEncounterReference.ConsultantID
	,ReadmissionSiteID = ReadmissionEncounterReference.StartSiteID
	,ReadmissionAgeID = ReadmissionEncounterReference.AgeID
	,ReadmissionCCGID = ReadmissionEncounterReference.CCGID
	,ReadmissionPatientCategoryID = ReadmissionPatientCategory.PatientCategoryID
	,ReadmissionStartWardID = ReadmissionEncounterReference.StartWardID

	,ReadmissionAdmissionTime = ReadmissionEncounter.AdmissionTime

	,InitialAdmissionEncounterMergeEncounterRecno = InitialAdmissionEncounter.MergeEncounterRecno
	,InitialAdmissionDischargeDateID = InitialAdmissionEncounterReference.DischargeDateID
	,InitialAdmissionConsultantID = InitialAdmissionEncounterReference.ConsultantID
	,InitialAdmissionDirectorateID = InitialAdmissionEncounterDirectorate.DirectorateID
	,InitialAdmissionSpecialtyID = InitialAdmissionEncounterReference.SpecialtyID
	,InitialAdmissionSiteID = InitialAdmissionEncounterReference.StartSiteID
	,InitialAdmissionAdmissionMethodID = InitialAdmissionEncounterReference.AdmissionMethodID
	,InitialAdmissionAgeID = InitialAdmissionEncounterReference.AgeID
	,InitialAdmissionCCGID = InitialAdmissionEncounterReference.CCGID
	,InitialAdmissionPatientCategoryID = InitialAdmissionPatientCategory.PatientCategoryID
	,InitialAdmissionEndWardID = InitialAdmissionEncounterReference.EndWardID

	,Readmission.TrustWide

	,InitialAdmissionAdmissionTime = InitialAdmissionEncounter.AdmissionTime
from
	APC.Readmission

inner join APC.BaseEncounter ReadmissionEncounter
on	ReadmissionEncounter.MergeEncounterRecno = Readmission.ReadmissionRecno
and ReadmissionEncounter.Reportable = 1

inner join APC.BaseEncounterReference ReadmissionEncounterReference
on	ReadmissionEncounterReference.MergeEncounterRecno = Readmission.ReadmissionRecno

inner join APC.BaseEncounter AdmissionEncounter
on	AdmissionEncounter.MergeEncounterRecno = Readmission.InitialAdmissionRecno
and AdmissionEncounter.Reportable = 1

inner join APC.BaseEncounter InitialAdmissionEncounter
on	InitialAdmissionEncounter.GlobalProviderSpellNo = AdmissionEncounter.GlobalProviderSpellNo
and	InitialAdmissionEncounter.GlobalEpisodeNo = 1
and InitialAdmissionEncounter.Reportable = 1

inner join APC.BaseEncounterReference InitialAdmissionEncounterReference
on	InitialAdmissionEncounterReference.MergeEncounterRecno = InitialAdmissionEncounter.MergeEncounterRecno

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = InitialAdmissionEncounter.MergeEncounterRecno

inner join APC.BaseEncounter InitialDischargeEncounter
on	InitialDischargeEncounter.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno
and InitialDischargeEncounter.Reportable = 1

inner join APC.BaseEncounterReference InitialDischargeEncounterReference
on	InitialDischargeEncounterReference.MergeEncounterRecno = InitialDischargeEncounter.MergeEncounterRecno

inner join APC.AdmissionMethod ReadmissionAdmissionAdmissionMethod
on	ReadmissionAdmissionAdmissionMethod.SourceAdmissionMethodID = ReadmissionEncounterReference.AdmissionMethodID

inner join APC.AdmissionMethod InitialAdmissionAdmissionMethod
on	InitialAdmissionAdmissionMethod.SourceAdmissionMethodID = InitialAdmissionEncounterReference.AdmissionMethodID

inner join WH.Specialty ReadmissionAdmissionSpecialty
on	ReadmissionAdmissionSpecialty.SourceSpecialtyID = ReadmissionEncounterReference.SpecialtyID

inner join WH.Specialty InitialAdmissionSpecialty
on	InitialAdmissionSpecialty.SourceSpecialtyID = InitialAdmissionEncounterReference.SpecialtyID

inner join APC.PatientClassification ReadmissionAdmissionPatientClassification
on	ReadmissionAdmissionPatientClassification.SourcePatientClassificationID = ReadmissionEncounterReference.PatientClassificationID

inner join WH.Directorate InitialAdmissionEncounterDirectorate
on	InitialAdmissionEncounterDirectorate.DirectorateCode = InitialDischargeEncounter.EndDirectorateCode

inner join WH.Directorate ReadmissionEncounterDirectorate
on	ReadmissionEncounterDirectorate.DirectorateCode = ReadmissionEncounter.StartDirectorateCode

inner join APC.PatientCategory InitialAdmissionPatientCategory
on	InitialAdmissionPatientCategory.PatientCategoryCode = InitialAdmissionEncounter.PatientCategoryCode

inner join APC.PatientCategory ReadmissionPatientCategory
on	ReadmissionPatientCategory.PatientCategoryCode = ReadmissionEncounter.PatientCategoryCode

where
	ReadmissionAdmissionAdmissionMethod.NationalAdmissionMethodCode in ('21', '22', '23', '24', '28')
and	datediff(day, InitialDischargeEncounter.DischargeDate, ReadmissionEncounter.AdmissionDate) < 2


option (maxdop 4)

			 ;

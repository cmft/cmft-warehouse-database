﻿

CREATE proc [APC].[BuildBaseEncounterBaseLearningDisability]

as

truncate table APC.BaseEncounterBaseLearningDisability

insert into APC.BaseEncounterBaseLearningDisability
(
	MergeEncounterRecno
	,MergeLearningDisabilityRecno
)

select
	MergeEncounterRecno
	,MergeLearningDisabilityRecno
from
	(
	select
		MergeEncounterRecno
		,MergeLearningDisabilityRecno
	from
		APC.BaseEncounter
	inner join APC.BaseLearningDisability
	on	BaseLearningDisability.SourcePatientNo = BaseEncounter.SourcePatientNo
	and	
		(
			BaseEncounter.EpisodeEndTime >= BaseLearningDisability.AssessmentTime
		or
			BaseEncounter.EpisodeEndTime is null
		)
	and	BaseLearningDisability.ContextCode = 'CEN||BEDMAN'
	and	BaseEncounter.ContextCode = 'CEN||PAS'

	-- Trafford here...

	) BaseEncounterBaseLearningDisability

			


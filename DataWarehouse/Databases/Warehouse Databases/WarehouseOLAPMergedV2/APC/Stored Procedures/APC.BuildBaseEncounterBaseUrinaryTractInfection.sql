﻿
CREATE proc [APC].[BuildBaseEncounterBaseUrinaryTractInfection]

as

truncate table APC.BaseEncounterBaseUrinaryTractInfection

insert into APC.BaseEncounterBaseUrinaryTractInfection
(
	MergeEncounterRecno
	,MergeUrinaryTractInfectionRecno
)

select
	MergeEncounterRecno
	,MergeUrinaryTractInfectionRecno
from
	(
	select
		MergeEncounterRecno
		,MergeUrinaryTractInfectionRecno
	from
		APC.BaseEncounter
	inner join APC.BaseUrinaryTractInfection
	on	BaseUrinaryTractInfection.SourcePatientNo = BaseEncounter.SourcePatientNo
	and	BaseUrinaryTractInfection.SymptomStartTime between BaseEncounter.EpisodeStartTime and BaseEncounter.EpisodeEndTime
	and	BaseUrinaryTractInfection.ContextCode = 'CEN||BEDMAN'
	and	BaseEncounter.ContextCode = 'CEN||PAS'

	-- Trafford here...

	) BaseEncounterBaseUrinaryTractInfection




﻿
CREATE proc [APC].[BuildBaseEncounterBaseVTEAssessment]


as

truncate table APC.BaseEncounterBaseVTEAssessment

insert into APC.BaseEncounterBaseVTEAssessment

select
	MergeEncounterRecno
	,MergeVTEAssessmentRecno
from
	(	

	select
		MergeEncounterRecno
		,MergeVTEAssessmentRecno
	from
		APC.BaseEncounter
	inner join APC.BaseVTEAssessment
	on	BaseVTEAssessment.SourcePatientNo = BaseEncounter.SourcePatientNo
	and	BaseVTEAssessment.AssessmentTime between BaseEncounter.EpisodeStartTime and BaseEncounter.EpisodeEndTime
	and	BaseVTEAssessment.ContextCode = 'CEN||BEDMAN'
	and	BaseEncounter.ContextCode = 'CEN||PAS'

	-- Trafford here...

	) BaseEncounterBaseVTEAssessment




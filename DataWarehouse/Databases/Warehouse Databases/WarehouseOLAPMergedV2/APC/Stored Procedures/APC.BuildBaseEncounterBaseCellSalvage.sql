﻿


-- Please note Recall contains Cen and Tra data, when linking below, some joins can't be found, sometimes duplicates are found (Reportable = 1 reduces duplicates, but some may be with DQ team still to validate)
-- The fact table will need to recognise where the Recall Encounter cannot be linked to APC and where Duplicates have been found.
-- please note, incorrect link has been found (small numbers), eg Trafford Theatre linked to Cen episode, Traf episode not picked up because Cen Casenote number had been used, and DoB had been recorded incorrectly (discussed with DG)


CREATE proc [APC].[BuildBaseEncounterBaseCellSalvage]	
	
as

truncate table APC.BaseEncounterBaseCellSalvage

insert into APC.BaseEncounterBaseCellSalvage
(
	MergeEncounterRecno
	,MergeCellSalvageRecno
)

select
	MergeEncounterRecno
	,MergeRecno
from
	APC.BaseEncounter
	
inner join BloodManagement.BaseCellSalvage
on	
	(
		BaseEncounter.CasenoteNumber = BaseCellSalvage.HospitalNumber
	or	(
			BaseEncounter.PatientSurname = BaseCellSalvage.LastName 
		and BaseEncounter.PatientForename = BaseCellSalvage.FirstName 
		and BaseEncounter.DateOfBirth = BaseCellSalvage.DateOfBirth
		)
	)
and	BaseCellSalvage.StartTime between BaseEncounter.EpisodeStartTime and coalesce(BaseEncounter.EpisodeEndTime, getdate())

where
	BaseEncounter.Reportable = 1
and not exists
		(
		select 
			1
		from
			APC.BaseEncounter Earliest
		where
			(
			Earliest.CasenoteNumber = BaseCellSalvage.HospitalNumber
		or	(
				BaseEncounter.PatientSurname = BaseCellSalvage.LastName 
			and BaseEncounter.PatientForename = BaseCellSalvage.FirstName 
			and BaseEncounter.DateOfBirth = BaseCellSalvage.DateOfBirth
			)
			)
		and	BaseCellSalvage.StartTime between Earliest.EpisodeStartTime and coalesce(Earliest.EpisodeEndTime, getdate())
		and Earliest.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
		and Earliest.GlobalEpisodeNo < BaseEncounter.GlobalEpisodeNo
		)

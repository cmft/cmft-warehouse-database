﻿


CREATE proc [APC].[BuildBaseEncounterBaseReview]

as

truncate table APC.BaseEncounterBaseReview

insert into APC.BaseEncounterBaseReview
(
	MergeEncounterRecno
	,MergeReviewRecno
)

Select
	MergeEncounterRecno
	,MergeReviewRecno 
from
	APC.BaseEncounter Encounter
inner join
	(
	select
		GlobalProviderSpellNo
		,MergeReviewRecno = BaseReview.MergeRecno
	from
		APC.BaseEncounter Encounter
	
	inner join Mortality.BaseReview
	on	BaseReview.SourcePatientNo = Encounter.SourcePatientNo
	and	BaseReview.EpisodeStartTime = Encounter.EpisodeStartTime 
	and	BaseReview.APCContextCode = Encounter.ContextCode
	
	where
		Reportable = 1
	) Review
on Encounter.GlobalProviderSpellNo = Review.GlobalProviderSpellNo
where
	not exists
	(
	Select
		1
	from
		APC.BaseEncounter Latest
	where
		Latest.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
	and Latest.GlobalEpisodeNo > Encounter.GlobalEpisodeNo
	)







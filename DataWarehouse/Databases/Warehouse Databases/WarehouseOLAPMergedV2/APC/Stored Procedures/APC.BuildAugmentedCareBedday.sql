﻿
CREATE proc [APC].[BuildAugmentedCareBedday]

(
	@Start date
	,@End date
)

as


/********************************************************************************************************************
06/05/2015	RR	added case statement requested by Phil Huitson for POD
				also changed from APC.BaseEncounter to APC.Encounter, discussed with Phil (for Global Discharge Date)
*********************************************************************************************************************/

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LocalStart date = @Start
	,@LocalEnd date = @End

declare
	@inserted int


if object_id('tempdb..#Bedday') is not null
drop table #Bedday

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode = 'ACP'
	,PODCode
	,HRGCode
	,CensusDate
	,StartTime
	,EndTime
	,ContextCode
	,GlobalProviderSpellNo
into
	#Bedday
from
	(	

	select
		MergeAPCEncounterRecno = Encounter.MergeEncounterRecno
		,MergeDatasetEncounterRecno = BaseAugmentedCarePeriod.MergeEncounterRecno
		,PODCode = 
			case
				when Encounter.DischargeDate < '20150401' then 'HDU-P(Outlying)' -- 20150515 RR removed space between '..P (..', checked other tables & procs and this doesn't have a space in.
				else 'OutlyingHDU'
			end
		,HRGCode = 
			case
				when Encounter.DischargeDate < '20150401' then 'HDU-P(Outlying)' -- as with POD
				else 'OutlyingHDU'
			end
		,CensusDate = Calendar.TheDate
		,BaseAugmentedCarePeriod.StartTime
		,BaseAugmentedCarePeriod.EndTime
		,BaseAugmentedCarePeriod.ContextCode
		,Encounter.GlobalProviderSpellNo

	from 
		WH.Calendar

		inner join APC.BaseAugmentedCarePeriod
		on	Calendar.TheDate >= BaseAugmentedCarePeriod.StartDate
		and	Calendar.TheDate <= BaseAugmentedCarePeriod.EndDate
                           
		inner join [$(Warehouse)].PAS.AugmentedCareLocation -- need to map
		on	BaseAugmentedCarePeriod.LocationCode = AugmentedCareLocation.LocationCode

		left outer join APC.BedDayWard
		on	AugmentedCareLocation.WardCode = BedDayWard.SourceWardCode
		and	BedDayWard.ContextCode = 'CEN||PAS'
		and Calendar.TheDate between BedDayWard.FromDate and coalesce(BedDayWard.ToDate, getdate()) 

		inner join APC.BaseEncounter Encounter
		on	Calendar.TheDate >= Encounter.EpisodeStartDate
		and	Calendar.TheDate <= Encounter.EpisodeEndDate
		and	Encounter.ProviderSpellNo = BaseAugmentedCarePeriod.ProviderSpellNo
		and	Encounter.ContextCode = BaseAugmentedCarePeriod.ContextCode
        and Encounter.Reportable = 1
        
  --      inner join APC.Encounter
		--on	Calendar.TheDate >= Encounter.EpisodeStartDate
		--and	Calendar.TheDate <= Encounter.EpisodeEndDate
		--and	Encounter.ProviderSpellNo = BaseAugmentedCarePeriod.ProviderSpellNo
		--and	Encounter.ContextCode = BaseAugmentedCarePeriod.ContextCode
                                                                          
		where
			Calendar.TheDate between @LocalStart and @LocalEnd
		and	AugmentedCareLocation.LocationCode <> '81'
		and AugmentedCareLocation.SiteCode in 
											(
											'BHCH' 
											,'RMCH'
											,'NRMC'
											)
		and BedDayWard.SourceWardCode is null
		and	BaseAugmentedCarePeriod.EndDate is not null

			/*  
				remove potential to double count at episode junctions - 
				where one episode ends and another starts on same day
				the Bedday is apportioned to the later episode
			*/

		and
			not exists
					(
					select
						1
					from 
						WH.Calendar CalendarNext

					inner join APC.BaseEncounter BaseEncounterNext

					on	CalendarNext.TheDate >= BaseEncounterNext.EpisodeStartDate
					and	CalendarNext.TheDate <= BaseEncounterNext.EpisodeEndDate
												
					where
						CalendarNext.TheDate = Calendar.TheDate
					and BaseEncounterNext.GlobalEpisodeNo > Encounter.GlobalEpisodeNo
					and BaseEncounterNext.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
					--and BaseEncounterNext.ContextCode = BaseEncounter.ContextCode
					)

		) Bedday

insert into APC.BedDay

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode
	,PODCode
	,HRGCode
	,CensusDate
from
	#Bedday Bedday

where
	/*  
		remove potential to double count with multiple
		rows in ward stay table with overlapping dates
		with ward code attracting same pod code
	*/

	not exists
			(
				select
					1
				from
					#Bedday BeddayNext
				where
					BeddayNext.CensusDate = Bedday.CensusDate
					and BeddayNext.StartTime > Bedday.StartTime
					and BeddayNext.PODCode = Bedday.PODCode
					and BeddayNext.ContextCode = Bedday.ContextCode
					and BeddayNext.GlobalProviderSpellNo = Bedday.GlobalProviderSpellNo
			)

select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

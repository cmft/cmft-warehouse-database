﻿

CREATE proc [APC].[BuildNeonatalBedday]

(
	@StartDate date
	,@EndDate date
)

as

declare @LocalStartDate date = @StartDate
declare @LocalEndDate date = @EndDate 

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int

insert into APC.BedDay

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode = 'NEO'
	,PODCode
	,HRGCode
	,CensusDate
from
	(	
	select
		MergeAPCEncounterRecno = BaseEncounter.MergeEncounterRecno
		,MergeDatasetEncounterRecno = BaseBedday.BeddayRecno
		,PODCode = 'NEONATAL'
		,HRGCode = BaseBedday.HRGCode
		,CensusDate = BaseBedday.CareDate
	from
		Neonatal.BaseBedday

	inner join Neonatal.BaseEncounter NeonatalEncounter
	on BaseBedday.EpisodeID = NeonatalEncounter.EpisodeID

	inner join APC.BaseEncounter
	on	BaseBedday.CareDate >= BaseEncounter.EpisodeStartDate
	and	BaseBedday.CareDate <= BaseEncounter.EpisodeEndDate
	and	NeonatalEncounter.NHSNumber = replace(BaseEncounter.NHSNumber,' ','')
    and BaseEncounter.Reportable = 1
                                                                
	where
		BaseBedday.CareDate between @LocalStartDate and @LocalEndDate

	/*  
		remove potential to double count at episode junctions - 
		where one episode ends and another starts on same day
		the Bedday is apportioned to the later episode
	*/

	and	BaseBedday.HRGCode is not null
	and	not exists
					(
					select
						1
					from 
						Neonatal.BaseBedday BaseBeddayNext

					inner join APC.BaseEncounter BaseEncounterNext
					on	BaseBeddayNext.CareDate >= BaseEncounterNext.EpisodeStartDate
					and	BaseBeddayNext.CareDate <= BaseEncounterNext.EpisodeEndDate
                    and BaseEncounterNext.Reportable = 1
                    
					--where
					--	BaseBeddayNext.CareDate = BaseBedday.CareDate
					--and BaseEncounterNext.GlobalEpisodeNo > BaseEncounter.GlobalEpisodeNo
					--and BaseEncounterNext.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
					--and BaseEncounterNext.ContextCode = BaseEncounter.ContextCode

					--DG - 28.1.15 - Reinstated as now performs well? 	
					--GC 09.04.15 Reinstated
					where
						BaseBeddayNext.CareDate = BaseBedday.CareDate
					and	BaseEncounterNext.SourceEncounterNo > BaseEncounter.SourceEncounterNo
					and BaseEncounterNext.ProviderSpellNo = BaseEncounter.ProviderSpellNo
					and BaseEncounterNext.ContextCode = BaseEncounter.ContextCode
					---- DG - 27.1.15 - had to reinstate this block as using the global attributes was causing parallelism and massive performance problems
					---- the procs on dev and test work fine with the globals and the proc on production ran OK yesterday. This differs to the versions on dev and test

					)
	) Bedday
		
		
select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
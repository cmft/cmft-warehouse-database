﻿



CREATE proc [APC].[BuildWardBedday]

(
	@Start date
	,@End date
)

as

-- 20150515	RR	advised by PH that if a real time POD then the POD assigned should be based on Discharge Date else Census Date (previously everything was Census Date)
--				I've added Discharge Date to temp table and created a CTE to use in bottom query..

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LocalStart date = @Start
	,@LocalEnd date = @End

declare
	@inserted int

if object_id('tempdb..#Bedday') is not null
drop table #Bedday

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode = 'WS'
	,PODCode
	,HRGCode
	,CensusDate
	,StartTime
	,EndTime
	,WardCode
	,ContextCode
	,GlobalProviderSpellNo
	,DischargeDate

into
	#Bedday
from
	(	
	select
		MergeAPCEncounterRecno = BaseEncounter.MergeEncounterRecno
		,MergeDatasetEncounterRecno = BaseWardStay.MergeEncounterRecno
		,PODCode = BedDayWard.PODCode
		,HRGCode = BedDayWard.HRGCode
		,CensusDate = Calendar.TheDate
		,BaseWardStay.StartTime
		,BaseWardStay.EndTime
		,BaseWardStay.WardCode
		,BaseEncounter.ContextCode
		,GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
		,DischargeDate = BaseEncounter.DischargeDate
	from 
		WH.Calendar Calendar

		inner join APC.BaseWardStay
		on 	Calendar.TheDate >= BaseWardStay.StartDate
		and Calendar.TheDate <= --BaseWardStay.EndDate

										--case
										--	when EndActivityCode = 'ADS' -- if admission suspended then takes start date of next ward stay and subtracts 1 day (bed is kept for patient while away from hospital)
										--	then 
										--		(
										--			select 
										--				dateadd(day, -1, StartDate)
										--			from
										--				APC.BaseWardStay WardStayNext
										--			where
										--				WardStayNext.ProviderSpellNo = BaseWardStay.ProviderSpellNo
										--				and WardStayNext.ContextCode = BaseWardStay.ContextCode
										--				and WardStayNext.StartTime > BaseWardStay.StartTime
										--				and
										--					not exists
										--								(
										--									select
										--										1
										--									from
										--										APC.BaseWardStay WardStayLater
										--									where
										--										WardStayNext.ProviderSpellNo = WardStayLater.ProviderSpellNo
										--										and WardStayNext.ContextCode = WardStayLater.ContextCode
										--										and WardStayLater.StartTime > BaseWardStay.StartTime
										--										and WardStayLater.StartTime < WardStayNext.StartTime
										--								)
														
										--			)
										--	else BaseWardStay.EndDate
										--end
										--,@end

								/* faster! */
								coalesce(
									case
										when EndActivityCode = 'ADS'
										then (
												select 
													top 1 dateadd(day, -1, StartDate)
												from
													APC.BaseWardStay WardStayNext
												where
													WardStayNext.ProviderSpellNo = BaseWardStay.ProviderSpellNo
													and WardStayNext.StartDate > BaseWardStay.StartDate
													and WardStayNext.ContextCode = BaseWardStay.ContextCode 
												order by 
													StartDate asc	
											)
										else BaseWardStay.EndDate
									end 
									,
									@End
									)

		inner join APC.BedDayWard
		on	BaseWardStay.WardCode = BedDayWard.SourceWardCode
		and BaseWardStay.ContextCode = BedDayWard.ContextCode
		and Calendar.TheDate between BedDayWard.FromDate and coalesce(BedDayWard.ToDate, getdate()) 
	
		inner join APC.BaseEncounter
		on 	Calendar.TheDate >= BaseEncounter.EpisodeStartDate
		and Calendar.TheDate <= coalesce(
										BaseEncounter.EpisodeEndDate
										,@End
										)
		and BaseEncounter.ProviderSpellNo = BaseWardStay.ProviderSpellNo
		and BaseEncounter.ContextCode = BaseWardStay.ContextCode
		and BaseEncounter.Reportable = 1
		
		where
			Calendar.TheDate between @LocalStart and @LocalEnd

		/*	
			remove potential to double count at episode junctions - 
			where one episode ends and another starts on same day
			the bed day is apportioned to the later episode
		*/

		and
			not exists
					(
						select
							1
						from 
							WH.Calendar CalendarNext

						inner join [APC].[BaseEncounter] BaseEncounterNext
						on 	CalendarNext.TheDate >= BaseEncounterNext.EpisodeStartDate
						and CalendarNext.TheDate <= coalesce(
															BaseEncounterNext.EpisodeEndDate
															,@End
															)
						and BaseEncounterNext.Reportable = 1
						where
							CalendarNext.TheDate = Calendar.TheDate
						and BaseEncounterNext.GlobalEpisodeNo > BaseEncounter.GlobalEpisodeNo
						and BaseEncounterNext.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
						--and BaseEncounterNext.ContextCode = BaseEncounter.ContextCode
			
					)

			) Bedday


;
With BedDayCTE
	(
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode
	,CensusDate
	,PODCode
	,HRGCode
	,StartTime
	,ContextCode
	,GlobalProviderSpellNo
	)
as
	(
	Select 
		#Bedday.MergeAPCEncounterRecno
		,MergeDatasetEncounterRecno
		,DatasetCode
		,CensusDate
		,PODCode = 
				Case
					When IsRealtimePOD = 1 then #Bedday.PODCode
					else BedDayWard.PODCode
				end	
		,HRGCode = 
				Case
					When IsRealtimePOD = 1 then #Bedday.HRGCode
					else BedDayWard.HRGCode
				end
		,StartTime
		,#Bedday.ContextCode
		,GlobalProviderSpellNo
	from 
		#Bedday
	left join APC.BedDayWard
	on	#Bedday.WardCode = BeddayWard.SourceWardCode
	and #Bedday.ContextCode = BeddayWard.ContextCode
	and coalesce(#Bedday.DischargeDate,getdate()) between 
			BeddayWard.FromDate and coalesce(BeddayWard.ToDate, getdate()) 
	) 




insert into APC.BedDay

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode
	,PODCode
	,HRGCode
	,CensusDate
from
	BedDayCTE Bedday

where
	/*  
		remove potential to double count with multiple
		rows in ward stay table with overlapping dates
		with ward code attracting same pod code
	*/

	not exists
			(
				select
					1
				from
					BedDayCTE BeddayNext
				where
					BeddayNext.CensusDate = Bedday.CensusDate
					and BeddayNext.StartTime > Bedday.StartTime
					and BeddayNext.PODCode = Bedday.PODCode
					and BeddayNext.ContextCode = Bedday.ContextCode
					and BeddayNext.GlobalProviderSpellNo = Bedday.GlobalProviderSpellNo
			)
			
select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


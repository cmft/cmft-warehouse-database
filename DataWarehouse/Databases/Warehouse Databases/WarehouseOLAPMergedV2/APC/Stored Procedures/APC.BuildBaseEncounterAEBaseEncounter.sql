﻿
/* ***** Match logic in proc [Warehouse].[APC].[BuildEncounterAEEncounter] ***** */

-- An AE MergeEncounterRecno can have multiple APCMergeEncounterRecnos as it's pulling through all for the spell.
-- If we only want the first we can add, FirstEpisodeInSpellIndicator = 1 ??

-- use WarehouseOLAPMergedV2

-- drop view APC.BaseEncounterAEBaseEncounter 
CREATE Procedure [APC].[BuildBaseEncounterAEBaseEncounter] 

As

set nocount on
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

Truncate table APC.BaseEncounterAEBaseEncounter 

--Insert into APC.BaseEncounterAEBaseEncounter 

select 
	 
	 AEMergeEncounterRecno = AEEncounter.MergeEncounterRecno
	,APCMergeEncounterRecno = APCEncounter.MergeEncounterRecno
	,APCSourcePatientNo = APCEncounter.SourcePatientNo
	,APCEpisodeNo = APCEncounter.SourceSpellNo
	
	,APCEncounter.DistrictNo
	,APCEncounter.ContextCode
	,AEEncounter.DepartureTime
	,AdmissionMethodCode
	,AdmissionTime
	,AttendanceDisposalCode

	
	into #AEEncounterTemp
	
from
	AE.BaseEncounter AEEncounter

inner join APC.Encounter APCEncounter
on APCEncounter.DistrictNo = AEEncounter.DistrictNo
and left(APCEncounter.ContextCode,3) = left(AEEncounter.ContextCode,3)
and APCEncounter.FirstEpisodeInSpellIndicator = 1 -- Let RR know if this is taken out as she will need to change an AKI query so duplicates don't come through
and AEEncounter.Reportable = 1 
and APCEncounter.AdmissionMethodCode = 'AE'
and AEEncounter.AttendanceDisposalCode = '01'   -- Admitted
and abs(datediff(hour, AEEncounter.DepartureTime, APCEncounter.AdmissionTime)) <= 24

Insert into APC.BaseEncounterAEBaseEncounter

select 

	AEMergeEncounterRecno 
	,APCMergeEncounterRecno 
	,APCSourcePatientNo 
	,APCEpisodeNo 

from #AEEncounterTemp

where 
	not exists -- pulls closest APC event to A&E
		(
		select 
			1
		from
			APC.BaseEncounter  Encounter
			
				inner join APC.BaseEncounterReference
			on Encounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
			and Encounter.Reportable = 1
		where 
			Encounter.DistrictNo = #AEEncounterTemp.DistrictNo
		and left(Encounter.ContextCode,3) = left(#AEEncounterTemp.ContextCode,3)
		and Encounter.AdmissionMethodCode = #AEEncounterTemp.AdmissionMethodCode
		and abs(datediff(minute, #AEEncounterTemp.DepartureTime, Encounter.AdmissionTime)) < abs(datediff(minute, #AEEncounterTemp.DepartureTime, #AEEncounterTemp.AdmissionTime))
		)
 and not exists -- pulls closest A&E event to APC, ie max A&E departure prior to admission time
	(
	select 
		1
	from
		AE.BaseEncounter  Encounter
	where 
		Encounter.DistrictNo = #AEEncounterTemp.DistrictNo
	and left(Encounter.ContextCode,3) = left(#AEEncounterTemp.ContextCode,3)
	and Encounter.AttendanceDisposalCode = #AEEncounterTemp.AttendanceDisposalCode
	and abs(datediff(minute, Encounter.DepartureTime, #AEEncounterTemp.AdmissionTime)) < abs(datediff(minute, #AEEncounterTemp.DepartureTime, #AEEncounterTemp.AdmissionTime))
	)
	
	drop table  #AEEncounterTemp

--select 
--	AEMergeEncounterRecno = AEEncounter.MergeEncounterRecno
--	,APCMergeEncounterRecno = APCEncounter.MergeEncounterRecno
--	,APCSourcePatientNo = APCEncounter.SourcePatientNo
--	,APCEpisodeNo = APCEncounter.SourceSpellNo
--from
--	AE.BaseEncounter AEEncounter

--inner join APC.Encounter APCEncounter
--on APCEncounter.DistrictNo = AEEncounter.DistrictNo
--and left(APCEncounter.ContextCode,3) = left(AEEncounter.ContextCode,3)
--and APCEncounter.FirstEpisodeInSpellIndicator = 1 -- Let RR know if this is taken out as she will need to change an AKI query so duplicates don't come through
--and AEEncounter.Reportable = 1 
--and APCEncounter.AdmissionMethodCode = 'AE'
--and AEEncounter.AttendanceDisposalCode = '01'   -- Admitted
--and abs(datediff(hour, AEEncounter.DepartureTime, APCEncounter.AdmissionTime)) <= 24
--where 
--	not exists -- pulls closest APC event to A&E
--		(
--		select 
--			1
--		from
--			APC.Encounter  
--		where 
--			Encounter.DistrictNo = APCEncounter.DistrictNo
--		and left(Encounter.ContextCode,3) = left(APCEncounter.ContextCode,3)
--		and Encounter.AdmissionMethodCode = APCEncounter.AdmissionMethodCode
--		and abs(datediff(minute, AEEncounter.DepartureTime, Encounter.AdmissionTime)) < abs(datediff(minute, AEEncounter.DepartureTime, APCEncounter.AdmissionTime))
--		)
--and not exists -- pulls closest A&E event to APC, ie max A&E departure prior to admission time
--	(
--	select 
--		1
--	from
--		AE.BaseEncounter  Encounter
--	where 
--		Encounter.DistrictNo = AEEncounter.DistrictNo
--	and left(Encounter.ContextCode,3) = left(AEEncounter.ContextCode,3)
--	and Encounter.AttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
--	and abs(datediff(minute, Encounter.DepartureTime, APCEncounter.AdmissionTime)) < abs(datediff(minute, AEEncounter.DepartureTime, APCEncounter.AdmissionTime))
--	)


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


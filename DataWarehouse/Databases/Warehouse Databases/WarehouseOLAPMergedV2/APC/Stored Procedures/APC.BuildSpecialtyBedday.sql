﻿

CREATE proc [APC].[BuildSpecialtyBedday]

(
	@Start date
	,@End date
)

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LocalStart date = @Start
	,@LocalEnd date = @End

declare
	@inserted int

insert into APC.BedDay

(
	[MergeAPCEncounterRecno]
	,[MergeDatasetEncounterRecno]
	,[DatasetCode]
	,[PODCode]
	,[HRGCode]
	,[CensusDate]
)

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode = 'APC'
	,PODCode
	,HRGCode
	,CensusDate
from
	(	
	select
		MergeAPCEncounterRecno = BaseEncounter.MergeEncounterRecno
		,MergeDatasetEncounterRecno = BaseEncounter.MergeEncounterRecno
		,PODCode = BedDaySpecialty.PODCode
		,HRGCode = BedDaySpecialty.HRGCode
		,CensusDate = Calendar.TheDate
		,BaseEncounter.EpisodeStartTime
		,BaseEncounter.EpisodeEndTime
		,BaseEncounter.ContextCode
		,ProviderSpellNo = BaseEncounter.ProviderSpellNo
	from 
		WH.Calendar Calendar


		inner join APC.BaseEncounter
		on 	Calendar.TheDate >= BaseEncounter.EpisodeStartDate
		and Calendar.TheDate <= coalesce(
										BaseEncounter.EpisodeEndDate
										,@LocalEnd
										)
		and BaseEncounter.Reportable = 1
		
		inner join APC.BedDaySpecialty
		on	BaseEncounter.SpecialtyCode  = BedDaySpecialty.SourceSpecialtyCode
		and BaseEncounter.ContextCode = BedDaySpecialty.ContextCode
		and Calendar.TheDate between BedDaySpecialty.FromDate and coalesce(BedDaySpecialty.ToDate, getdate()) 
	
		where
			Calendar.TheDate between @LocalStart and @LocalEnd

	) Bedday


select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
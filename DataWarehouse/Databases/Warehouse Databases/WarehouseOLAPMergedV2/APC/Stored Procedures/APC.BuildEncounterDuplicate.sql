﻿CREATE procedure [APC].[BuildEncounterDuplicate] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare @SourceContextID int =
	(select ContextID from ReferenceMapV2.Map.Context where ContextCode	= 'CEN||PAS')

declare	@DuplicateContextID int =
	(select ContextID from ReferenceMapV2.Map.Context where ContextCode	= 'TRA||UG')

delete
from
	APC.EncounterDuplicate
where
	SourceContextID = @SourceContextID
and	DuplicateContextID = @DuplicateContextID


insert
into
	APC.EncounterDuplicate
(
	 SourceContextID
	,SourceRecno
	,DuplicateContextID
	,DuplicateRecno
)
--this identifies duplicates at Trafford that originated at Central...
--add a union to identify duplicates at Central that originated at Trafford
select
	 SourceContextID = @SourceContextID
	,SourceRecno = CMFT.EncounterRecno
	,DuplicateContextID = @DuplicateContextID
	,DuplicateRecno = TGH.EncounterRecno
from
	[$(TraffordWarehouse)].dbo.APCEncounter TGH

inner join
	(
	select
		 NHSNumber = replace(Encounter.NHSNumber, ' ', '')
		,Encounter.AdmissionDate
		,Encounter.EncounterRecno
	from
		[$(Warehouse)].APC.Encounter Encounter
	where
		exists
		(
		select
			1
		from
			[$(Warehouse)].APC.WardStay           
		where
			Encounter.ProviderSpellNo = WardStay.ProviderSpellNo
		and	WardStay.WardCode in ('SUBR', 'SUBS', 'SUBT', 'SUBW', 'SUBY', 'SUBZ')
		)

	) CMFT
on	TGH.NHSNumber = CMFT.NHSNumber
and	cast(TGH.AdmissionDate as date) = CMFT.AdmissionDate


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

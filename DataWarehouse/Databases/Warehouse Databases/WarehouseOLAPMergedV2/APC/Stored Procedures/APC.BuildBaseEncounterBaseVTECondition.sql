﻿
CREATE proc [APC].[BuildBaseEncounterBaseVTECondition]

as

truncate table APC.BaseEncounterBaseVTECondition


insert into APC.BaseEncounterBaseVTECondition
(
	MergeEncounterRecno
	,MergeVTEConditionRecno
)

select
	MergeEncounterRecno
	,MergeVTEConditionRecno
from
	(
	
	select
		MergeEncounterRecno
		,MergeVTEConditionRecno
	from
		APC.BaseEncounter
	inner join APC.BaseVTECondition
	on	BaseVTECondition.SourcePatientNo = BaseEncounter.SourcePatientNo
	and	BaseVTECondition.DiagnosisDate between BaseEncounter.EpisodeStartTime and BaseEncounter.EpisodeEndTime
	and	BaseVTECondition.ContextCode = 'CEN||BEDMAN'
	and	BaseEncounter.ContextCode = 'CEN||PAS'

	-- Trafford here...

	) BaseEncounterBaseVTECondition



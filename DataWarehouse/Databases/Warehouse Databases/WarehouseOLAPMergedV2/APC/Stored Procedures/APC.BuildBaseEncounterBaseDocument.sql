﻿


CREATE proc [APC].[BuildBaseEncounterBaseDocument]

as

truncate table APC.BaseEncounterBaseDocument
	
insert into APC.BaseEncounterBaseDocument
(
	MergeEncounterRecno
	,MergeDocumentRecno
	,SequenceNo
)

select
	MergeEncounterRecno
	,MergeDocumentRecno
	,SequenceNo
from
	(
	select
		MergeEncounterRecno
		,MergeDocumentRecno
		,SequenceNo
	from
		(
		select
			MergeEncounterRecno
			,MergeDocumentRecno
			,SequenceNo = row_number() over (
											partition by
												MergeEncounterRecno
											order by
												BaseDocument.TransmissionTime
												,BaseDocument.SignedTime
												,BaseDocument.TypedTime
											)
		from
			APC.BaseEncounter
			
		inner join Dictation.BaseInpatient 
		on	BaseInpatient.SourcePatientNo = BaseEncounter.SourcePatientNo
		and	BaseInpatient.AdmissionTime = BaseEncounter.AdmissionTime 
		and BaseInpatient.ContextCode = 'CEN||MEDI'

		inner join Dictation.BaseDocument
		on	BaseDocument.SourcePatientNo = BaseInpatient.SourcePatientNo
		and	convert(datetime, substring(BaseDocument.DocumentID, 10, 8), 112) = cast(BaseInpatient.AdmissionTime as date)
		and	substring(BaseDocument.DocumentID, 18, 2) = BaseInpatient.SpellSequenceNo
		and	BaseDocument.DocumentTypeCode in ('F', 'I')
		and rtrim(BaseDocument.DocumentID) != 'F2008022375' -- duff dsdocname in Medisec (Test Patient?)
		and	BaseDocument.ContextCode = 'CEN||MEDI'

		) EncounterDocument

	union 

	select
		MergeEncounterRecno
		,MergeDocumentRecno
		,SequenceNo
	from
		(
		select
			MergeEncounterRecno
			,MergeDocumentRecno
			,SequenceNo = row_number() over 
										(
										partition by 
											MergeEncounterRecno
										order by
											BaseDocument.TransmissionTime
											,BaseDocument.SignedTime
											,BaseDocument.TypedTime
										) 
		from
			APC.BaseEncounter
			
		inner join Dictation.BaseInpatient 
		on	BaseInpatient.SourcePatientNo = BaseEncounter.SourcePatientNo
		and	BaseInpatient.AdmissionTime = BaseEncounter.AdmissionTime 
		and	BaseInpatient.ContextCode = 'TRA||EPR'

		inner join Dictation.BaseDocument
		on	BaseDocument.DocumentID = BaseInpatient.DocumentID
		and	BaseDocument.ContextCode = 'TRA||EPR'

		) EncounterDocument

	) BaseEncounterBaseDocument
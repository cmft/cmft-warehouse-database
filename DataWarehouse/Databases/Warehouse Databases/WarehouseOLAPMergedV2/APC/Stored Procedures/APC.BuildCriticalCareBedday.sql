﻿
--declare @local_startdate date = '1 apr 2009'
--		,@local_enddate date =  '31 jun 2013'


CREATE proc [APC].[BuildCriticalCareBedday] 

(
	@Start date
	,@End date
)

as

declare
	--@local_startdate date,
	--@local_enddate date,
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LocalStart date = @Start
	,@LocalEnd date = @End


declare
	@inserted int


if object_id('tempdb..#Bedday') is not null
drop table #Bedday

--select  @local_startdate = @Start,
--		@local_enddate = @End

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode = 'CC'
	,PODCode
	,HRGCode
	,CensusDate
	,StartTime
	,EndTime
	,CasenoteNumber
	,WardCode 
into
	#Bedday

from
	--(	

	--select
	--	MergeAPCEncounterRecno = BaseEncounter.MergeEncounterRecno
	--	,MergeDatasetEncounterRecno = BaseCriticalCarePeriod.MergeEncounterRecno
	--	,PODCode = WardCode
	--	,HRGCode = HRG4CriticalCareEncounter.HRGCode
	--	,CensusDate = Calendar.TheDate
	--	,BaseCriticalCarePeriod.StartTime
	--	,BaseCriticalCarePeriod.EndTime
	--	,BaseCriticalCarePeriod.CasenoteNumber
	--	,BaseCriticalCarePeriod.WardCode 

	--from 
	--	WH.Calendar

	--	inner join APC.BaseCriticalCarePeriod
	--	on	Calendar.TheDate >= BaseCriticalCarePeriod.StartDate

	--	and	Calendar.TheDate <= BaseCriticalCarePeriod.EndDate

	--	inner join APC.HRG4CriticalCareEncounter
	--	on BaseCriticalCarePeriod.MergeEncounterRecno = HRG4CriticalCareEncounter.MergeEncounterRecno

	--	inner join APC.BaseEncounter
	--	on	Calendar.TheDate >= BaseEncounter.EpisodeStartDate
	--	and	Calendar.TheDate <= BaseEncounter.EpisodeEndDate

	--	and	
	--		(
	--			BaseEncounter.CasenoteNumber = BaseCriticalCarePeriod.CasenoteNumber
	--		or
	--			BaseEncounter.NHSNumber = BaseCriticalCarePeriod.NHSNumber
	--		)
			
                                                                          
	--	where
	--		Calendar.TheDate between @local_startdate and @local_enddate

	--		/*  
	--			remove potential to double count at episode junctions - 
	--			where one episode ends and another starts on same day
	--			the Bedday is apportioned to the later episode
	--		*/

	--	   and
	--			not exists
	--						(
	--							select
	--								1
	--							from 
	--								WH.Calendar CalendarNext

	--							inner join APC.BaseEncounter BaseEncounterNext

	--							on	CalendarNext.TheDate >= BaseEncounterNext.EpisodeStartDate
	--							and	CalendarNext.TheDate <= BaseEncounterNext.EpisodeEndDate
                           
	--							where
	--								CalendarNext.TheDate = Calendar.TheDate
	--							and BaseEncounterNext.SourceEncounterNo > BaseEncounter.SourceEncounterNo
	--							and BaseEncounterNext.ProviderSpellNo = BaseEncounter.ProviderSpellNo
	--							and BaseEncounterNext.ContextCode = BaseEncounter.ContextCode
	--						)

	--	) Bedday
	
	

(
	select
		MergeAPCEncounterRecno = BaseEncounter.MergeEncounterRecno
		,MergeDatasetEncounterRecno = BaseCriticalCarePeriod.MergeEncounterRecno
		,PODCode = 'CRITICALCARE' --WardCode
		,HRGCode = HRG4CriticalCareEncounter.HRGCode
		,CensusDate = Calendar.TheDate
		,BaseCriticalCarePeriod.StartTime
		,BaseCriticalCarePeriod.EndTime
		,BaseCriticalCarePeriod.CasenoteNumber
		,BaseCriticalCarePeriod.WardCode 

	from 
		WH.Calendar

		inner join APC.BaseCriticalCarePeriod
		on	Calendar.TheDate >= BaseCriticalCarePeriod.StartDate

		and	Calendar.TheDate <= BaseCriticalCarePeriod.EndDate

		inner join APC.HRG4CriticalCareEncounter
		on BaseCriticalCarePeriod.MergeEncounterRecno = HRG4CriticalCareEncounter.MergeEncounterRecno

		inner join APC.BaseEncounter
		on	Calendar.TheDate >= BaseEncounter.EpisodeStartDate
		and	Calendar.TheDate <= BaseEncounter.EpisodeEndDate
		and BaseEncounter.Reportable = 1
		and	BaseEncounter.CasenoteNumber = BaseCriticalCarePeriod.CasenoteNumber			
                                                                          
		where
			Calendar.TheDate between @LocalStart and @LocalEnd
			
		and
			not exists
						(
							select
								1
							from 
								WH.Calendar CalendarNext

							inner join APC.BaseEncounter BaseEncounterNext

							on	CalendarNext.TheDate >= BaseEncounterNext.EpisodeStartDate
							and	CalendarNext.TheDate <= BaseEncounterNext.EpisodeEndDate
							
							where
								CalendarNext.TheDate = Calendar.TheDate
							and BaseEncounterNext.GlobalEpisodeNo > BaseEncounter.GlobalEpisodeNo
							and BaseEncounterNext.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
							--and BaseEncounterNext.ContextCode = BaseEncounter.ContextCode
						)		
	union -- remove any duplicates as searching on two patient ids, performs much better than original method
			
	select
		MergeAPCEncounterRecno = BaseEncounter.MergeEncounterRecno
		,MergeDatasetEncounterRecno = BaseCriticalCarePeriod.MergeEncounterRecno
		,PODCode = 'CRITICALCARE' --WardCode
		,HRGCode = HRG4CriticalCareEncounter.HRGCode
		,CensusDate = Calendar.TheDate
		,BaseCriticalCarePeriod.StartTime
		,BaseCriticalCarePeriod.EndTime
		,BaseCriticalCarePeriod.CasenoteNumber
		,BaseCriticalCarePeriod.WardCode 

	from 
		WH.Calendar

		inner join APC.BaseCriticalCarePeriod
		on	Calendar.TheDate >= BaseCriticalCarePeriod.StartDate

		and	Calendar.TheDate <= BaseCriticalCarePeriod.EndDate

		inner join APC.HRG4CriticalCareEncounter
		on BaseCriticalCarePeriod.MergeEncounterRecno = HRG4CriticalCareEncounter.MergeEncounterRecno

		inner join APC.BaseEncounter
		on	Calendar.TheDate >= BaseEncounter.EpisodeStartDate
		and	Calendar.TheDate <= BaseEncounter.EpisodeEndDate
		and BaseEncounter.Reportable = 1
		and BaseEncounter.NHSNumber = BaseCriticalCarePeriod.NHSNumber	
                                                                          
		where
			Calendar.TheDate between @LocalStart and @LocalEnd		
		and
			not exists
						(
							select
								1
							from 
								WH.Calendar CalendarNext

							inner join APC.BaseEncounter BaseEncounterNext

							on	CalendarNext.TheDate >= BaseEncounterNext.EpisodeStartDate
							and	CalendarNext.TheDate <= BaseEncounterNext.EpisodeEndDate
													
							where
								CalendarNext.TheDate = Calendar.TheDate
							and BaseEncounterNext.GlobalEpisodeNo > BaseEncounter.GlobalEpisodeNo
							and BaseEncounterNext.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
							--and BaseEncounterNext.ContextCode = BaseEncounter.ContextCode
						)

		) Bedday

insert into APC.BedDay

select
	MergeAPCEncounterRecno
	,MergeDatasetEncounterRecno
	,DatasetCode
	,PODCode
	,HRGCode
	,CensusDate
from
	#Bedday Bedday

where
	/*  
		remove potential to double count with multiple
		rows in ward stay table with overlapping dates
		with ward code attracting same pod code
	*/

	not exists
			(
				select
					1
				from
					#Bedday BeddayNext
				where
					BeddayNext.CensusDate = Bedday.CensusDate
				and BeddayNext.StartTime > Bedday.StartTime
				and BeddayNext.PODCode = Bedday.PODCode
				and BeddayNext.CasenoteNumber = Bedday.CasenoteNumber


			)

select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
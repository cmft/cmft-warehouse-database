﻿


CREATE procedure [APC].[BuildFactEncounter] as

-- 201409	PDO	added link to APC ProcessList
-- 20141013 RR	added DischargeSummaryFields
-- 20141118 RR	Brought through OrganisationID (Practice) and ElectronicallyActivatedFlag
-- 20150819	RR	Added additional Discharge Summary fields

	
	
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare @MissingVTEExclusionReasonID int =
	(
	select
		Allocation.AllocationID
	from
		Allocation.Allocation
	where
		Allocation.SourceAllocationID = '-1'
	and	Allocation.AllocationTypeID = 3
	)

declare @MissingVTECategoryID int =
	(
	select
		VTECategory.VTECategoryID
	from
		APC.VTECategory
	where
		VTECategory.VTECategoryCode = '-1'
	)

declare @MissingSubSpecialtyID int =
	(
	select
		SubSpecialty.SubSpecialtyID
	from
		WH.SubSpecialty
	where
		SubSpecialty.SubSpecialty = 'Unassigned'
	)


declare @MissingDiagnosisID int =
	(
	select
		DiagnosisBase.DiagnosisID
	from
		WH.DiagnosisBase
	where
		DiagnosisBase.DiagnosisCode = 'N/A'
	)

declare @MissingPrimaryProcedureID int =
	(
	select
		Operation.OperationID
	from
		WH.Operation
	where
		Operation.OperationCode = 'N/A'
	)

declare @MissingCentralDischargeSummarySignedByID int =
	(
	select
		SystemUser.SystemUserID
	from
		WH.SystemUser
	where
		SystemUser.Username = 'Not Specified'
	and ContextCode = 'CEN||MEDI'
	)

declare @MissingTraffordDischargeSummarySignedByID int =
	(
	select
		SystemUser.SystemUserID
	from
		WH.SystemUser
	where
		SystemUser.Username = 'Not Specified'
	and ContextCode = 'TRA||EPR'
	)

declare @MissingCentralDischargeSummaryStatusID int =
	(
	select
		DocumentStatus.SourceDocumentStatusID
	from
		WH.DocumentStatus
	where
		DocumentStatus.SourceDocumentStatus = 'Not Specified'
	and SourceContextCode = 'CEN||MEDI' 
	)

declare @MissingTraffordDischargeSummaryStatusID int =
	(
	select
		DocumentStatus.SourceDocumentStatusID
	from
		WH.DocumentStatus
	where
		DocumentStatus.SourceDocumentStatus = 'Not Specified'
	and SourceContextCode = 'TRA||EPR'
	)



declare @MissingDischargeSummaryProductionTimeID int =
	(
	select
		TimeBand.TimeBandID
	from
		Dictation.TimeBand
	where
		TimeBand.MinuteBand = 'N/A'
	)

declare @MissingDischargeSummaryTransmissionTimeID int =
	(
	select
		TimeBand.TimeBandID
	from
		Dictation.TimeBand
	where
		TimeBand.MinuteBand = 'N/A'
	)
	
declare @MissingGPPracticeID int =
	(
	select
		GPPractice.GpPracticeID
	from
		WH.GPPractice
	where
		GPPractice.GpPracticeCode = 'N/A'
	)
--generate a work table to speed up subsequent processing

--Checksum
select
	 Encounter.MergeEncounterRecno
	,EncounterStartDateID = EncounterReference.EpisodeStartDateID
	,EncounterEndDateID = EncounterReference.EpisodeEndDateID
	,AdmissionReference.AdmissionDateID
	,DischargeReference.DischargeDateID
	,Metric.MetricID
	,EncounterReference.ConsultantID
	,EncounterReference.SpecialtyID
	,AdmissionReference.StartSiteID
	,DischargeReference.EndSiteID
	,AdmissionReference.AgeID
	,AdmissionReference.AdmissionMethodID
	,RTTActivity = coalesce(Encounter.RTTActivity, 0)
	,RTTTreated = coalesce(Encounter.RTTTreated, 0)
	,EncounterStartTimeOfDay = Encounter.EpisodeStartTimeOfDay
	,EncounterEndTimeOfDay = Encounter.EpisodeStartTimeOfDay
	,Admission.AdmissionTimeOfDay
	,Discharge.DischargeTimeOfDay
	,StartDirectorateID = StartDirectorate.DirectorateID
	,EndDirectorateID = EndDirectorate.DirectorateID
	,EncounterReference.SexID
	,AdmissionReference.ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay = coalesce(Admission.ISTAdmissionDemandTimeOfDay, -1)
	,ISTDischargeTimeOfDay = coalesce(Discharge.ISTDischargeTimeOfDay, -1)
	,EncounterReference.ContextID
	,AdmissionReference.AdmissionSourceID
	,DischargeReference.DischargeDestinationID
	,DischargeReference.DischargeMethodID
	,AdmissionReference.IntendedManagementID
	,EncounterReference.NeonatalLevelOfCareID
	,AdmissionReference.PatientClassificationID

	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode


	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end

	,VTE =
		case

		when
			(
				(
					Encounter.AgeCode like '%Years'
				and	left(Encounter.AgeCode , 2) > '17'
				)
			or	Encounter.AgeCode = '99+'
			)
		and not --regular day or night
			(
				AdmissionMethod.AdmissionTypeCode = 'EL' --elective
			and	PatientClassification.NationalPatientClassificationCode in ( '3' , '4' )
			)

		then 1

		else 0
		end

	,VTECategoryID = EncounterReference.VTECategoryID

	,VTEExclusionReasonID =
		coalesce(
			 VTEExclusion.AllocationID
			,@MissingVTEExclusionReasonID
		)

	,VTECompleteAndOrExclusion =
		case
		when
			Encounter.VTECategoryCode = 'C'
		or	coalesce(
				VTEExclusion.AllocationID
				,@MissingVTEExclusionReasonID
			) != @MissingVTEExclusionReasonID
		then 1
		else 0
		end

	,StartWardID = EncounterReference.StartWardID
	,EndWardID = EncounterReference.EndWardID

	,AdmissionWardID = AdmissionReference.StartWardID
	,DischargeWardID = DischargeReference.EndWardID

	,NationalExamID = NationalExam.AllocationID
	,ServiceID = Service.AllocationID

	,CCGID = EncounterReference.CCGID
	
 	,PrimaryDiagnosisID = 
		coalesce(
			 Diagnosis.DiagnosisID
			,@MissingDiagnosisID
		)

 	,PrimaryProcedureID = 
		coalesce(
			 PrimaryProcedure.OperationID
			,@MissingPrimaryProcedureID
		)

	,Encounter.Cases
	,LengthOfEncounter = Encounter.LOE

	,LengthOfSpell =
		DATEDIFF(
			 DAY
			,Admission.AdmissionDate
			,Discharge.DischargeDate
		)

	,EncounterReference.ResidenceCCGID
	
	,EpisodicHRGID = EpisodicHRG.HRGID
	,SpellHRGID = SpellHRG.HRGID


--work variables
	,Encounter.EpisodeEndDate
	,AdmissionMethod.AdmissionTypeCode
	,PatientClassification.NationalPatientClassificationCode

	,FirstEpisodeInSpellIndicator =
		cast(
			case
			when Encounter.GlobalEpisodeNo = 1
			then 1
			else 0
			end

			as bit
		)

	,LastEpisodeInSpellIndicator =
		case
		when Discharge.DischargeDate is null
		then 9

		when Encounter.MergeEncounterRecno = Discharge.MergeEncounterRecno
		then 1

		else 2
		end

	,Encounter.AgeCode
	,Discharge.DischargeDate
	,NeonatalLevelOfCare.NationalNeonatalLevelOfCareCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryProcedureCode
	,DischargeMethod.NationalDischargeMethodCode
	
-- 20141013 RR added the following for DischargeSummary
	,MergeDocumentRecno = 
				coalesce(
					BaseEncounterBaseDocument.MergeDocumentRecno		
					,'-1'
				)
	,DischargeSummaryStatusID = 
				coalesce(
					DocumentReference.SignedStatusID
					,
					case
					when Encounter.ContextCode = 'CEN||PAS'
					then @MissingCentralDischargeSummaryStatusID
					when Encounter.ContextCode = 'TRA||UG'
					then @MissingTraffordDischargeSummaryStatusID
					end
				)
	,DischargeSummarySignedByID = 
				coalesce(
					DocumentReference.SignedByID
					,
					case
					when Encounter.ContextCode = 'CEN||PAS'
					then @MissingCentralDischargeSummarySignedByID
					when Encounter.ContextCode = 'TRA||UG'
					then @MissingTraffordDischargeSummarySignedByID
					end
				)
	,DischargeSummaryProductionTimeID = 
				coalesce(
					TimeBand.TimeBandID
					,@MissingDischargeSummaryProductionTimeID
				)
	,DischargeSummaryExclusionReasonID = DischargeSummaryExclusion.AllocationID
	,DischargeSummaryRequired = 
								case
								when DischargeSummaryAllocation.Allocation = 'N/A' 
								then 1
								else 0
								end
	
	--20141118 RR added following fields
	
	,DischargeSummarySignedTimeID = 
				coalesce(
					SignedBand.TimeBandID
					,@MissingDischargeSummaryProductionTimeID
				)
	,GPPracticeID = 
					coalesce(
						GPPractice.GpPracticeID
						,@MissingGPPracticeID
						)
	,ElectronicCorrespondenceActivated = 
		case
			when Activated.GpPracticeID is not null then 1
			else 0
		end
	,CharlsonIndex = 
		coalesce(
			Encounter.CharlsonIndex
			,0
			)
	
	,EthnicCategoryID = EncounterReference.EthnicOriginID
	,ReligionID = EncounterReference.ReligionID
	
	,DischargeSummaryTransmissionTimeID = 
				coalesce(
					TransmissionTimeBand.TimeBandID
					,@MissingDischargeSummaryTransmissionTimeID
				)
	,Transmitted = 
		case
			when Document.TransmissionTime is not null then 1
			else 0
		end
	,DischargeSummarySigned = 
		coalesce(
			case
				when Document.SignedTime is not null then 1
				else 0
			end
			,0)
into
	#ChecksumEncounter
from
	APC.BaseEncounter Encounter

inner join APC.BaseEncounterReference EncounterReference
on	EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno
and Encounter.Reportable = 1

inner join APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

inner join APC.BaseEncounterReference AdmissionReference
on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

inner join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join Allocation.DatasetAllocation NationalExam
on	NationalExam.AllocationTypeID = 1
and	NationalExam.DatasetCode = 'APC'
and	NationalExam.DatasetRecno = Encounter.MergeEncounterRecno

inner join Allocation.DatasetAllocation Service
on	Service.AllocationTypeID = 2
and	Service.DatasetCode = 'APC'
and	Service.DatasetRecno = Encounter.MergeEncounterRecno

left join Allocation.DatasetAllocation VTEExclusion
on	VTEExclusion.AllocationTypeID = 3
and	VTEExclusion.DatasetCode = 'APC'
and	VTEExclusion.DatasetRecno= Encounter.MergeEncounterRecno

inner join WH.Directorate StartDirectorate
on	StartDirectorate.DirectorateCode = Encounter.StartDirectorateCode

inner join WH.Directorate EndDirectorate
on	EndDirectorate.DirectorateCode = Encounter.EndDirectorateCode

inner join APC.Metric
on	Metric.MetricCode = 'CHK'

left join WH.DiagnosisBase Diagnosis
on coalesce(left(Encounter.PrimaryDiagnosisCode, 6), 'N/A') = Diagnosis.DiagnosisCode

left join WH.Operation PrimaryProcedure
on coalesce(Encounter.PrimaryProcedureCode, 'N/A') = PrimaryProcedure.OperationCode

inner join APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = AdmissionReference.AdmissionMethodID

inner join APC.PatientClassification
on	PatientClassification.SourcePatientClassificationID = AdmissionReference.PatientClassificationID

left join APC.NeonatalLevelOfCare
on	NeonatalLevelOfCare.SourceNeonatalLevelOfCareID = EncounterReference.NeonatalLevelOfCareID

left join APC.DischargeMethod
on	DischargeMethod.SourceDischargeMethodID = DischargeReference.DischargeMethodID

--inner join APC.ProcessList DG - 24.10.14 Removed constraint as we are truncating table so this approch does not work...
--on Encounter.MergeEncounterRecno = ProcessList.MergeRecno

-- 20141013 RR added the following for Discharge Summary
inner join Allocation.DatasetAllocation DischargeSummaryExclusion
on	DischargeSummaryExclusion.AllocationTypeID = 14
and	DischargeSummaryExclusion.DatasetCode = 'APC'
and	DischargeSummaryExclusion.DatasetRecno= Encounter.MergeEncounterRecno

inner join Allocation.Allocation DischargeSummaryAllocation
on DischargeSummaryExclusion.AllocationID = DischargeSummaryAllocation.AllocationID
and DischargeSummaryAllocation.AllocationTypeID = 14

left join APC.BaseEncounterBaseDocument 
on	BaseEncounterBaseDocument.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	BaseEncounterBaseDocument.SequenceNo = 1

left join Dictation.BaseDocument Document
on BaseEncounterBaseDocument.MergeDocumentRecno = Document.MergeDocumentRecno

left join Dictation.BaseDocumentReference DocumentReference
on DocumentReference.MergeDocumentRecno = Document.MergeDocumentRecno

left join Dictation.TimeBand
on	case 
	when datediff(minute, Encounter.DischargeTime, coalesce(Document.TransmissionTime, Document.SignedTime)) < 0 then -1
	when datediff(minute, Encounter.DischargeTime, coalesce(Document.TransmissionTime, Document.SignedTime)) > 4321 then 4321
	else datediff(minute, Encounter.DischargeTime, coalesce(Document.TransmissionTime, Document.SignedTime))
	end	 = TimeBand.TimeBandID

left join Dictation.TimeBand TransmissionTimeBand
on	case 
	when datediff(minute, Encounter.DischargeTime, Document.TransmissionTime) < 0 then -1
	when datediff(minute, Encounter.DischargeTime, Document.TransmissionTime) > 4321 then 4321
	else datediff(minute, Encounter.DischargeTime, Document.TransmissionTime)
	end	 = TransmissionTimeBand.TimeBandID


left join Dictation.TimeBand SignedBand
on	case 
	when datediff(minute, Encounter.DischargeTime, Document.SignedTime) < 0 then -1
	when datediff(minute, Encounter.DischargeTime, Document.SignedTime) > 4321 then 4321
	else datediff(minute, Encounter.DischargeTime, Document.SignedTime)
	end	 = SignedBand.TimeBandID

left join WH.GPPractice
on Encounter.GpPracticeCodeAtDischarge = GPPractice.GpPracticeCode
and not exists
	(
	select
		1
	from
		WH.GPPractice Latest
	where 
		Latest.GpPracticeCode = GPPractice.GpPracticeCode
	and Latest.GpPracticeID > GPPractice.GpPracticeID
	)

left join WH.GPPractice Activated
on	Encounter.GpPracticeCodeAtDischarge = Activated.GpPracticeCode
and Encounter.EpisodeStartDate between Activated.ElectronicCorrespondenceActivatedTime and coalesce(Activated.ElectronicCorrespondenceDeactivatedTime, getdate())
and not exists
	(
	select
		1
	from
		WH.GPPractice Later
	where 
		Later.GpPracticeCode = GPPractice.GpPracticeCode
	and Later.GpPracticeID > GPPractice.GpPracticeID
	and Encounter.EpisodeStartDate between Later.ElectronicCorrespondenceActivatedTime and coalesce(Later.ElectronicCorrespondenceDeactivatedTime, getdate())
	)

left join APC.HRG4Encounter
on	HRG4Encounter.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WH.HealthcareResourceGroup4 EpisodicHRG
on	EpisodicHRG.HRGCode = HRG4Encounter.HRGCode

left join APC.HRG4Spell
on	HRG4Spell.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WH.HealthcareResourceGroup4 SpellHRG
on	SpellHRG.HRGCode = HRG4Spell.HRGCode


truncate table APC.FactEncounter

insert into APC.FactEncounter
(
	 MergeEncounterRecno
	,EncounterDateID
	,MetricID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay
	,DirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,SubSpecialtyID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,CasenoteMergeRecno
	,Cases
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID
	,EthnicCategoryID
	,ReligionID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
)
select
	 MergeEncounterRecno
	,EncounterDateID
	,MetricID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,@MissingSubSpecialtyID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID

	,CasenoteMergeRecno =
		coalesce(
			(
			select
				Base.MergeRecno
			from
				Casenote.Base
			where
				Base.SourcePatientNo = Encounter.SourcePatientNo
			and	Base.CasenoteNumber = Encounter.CasenoteNumber
			and	Base.ContextCode = Encounter.ContextCode
			)
			,-1
		)

	,Cases
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID
	,EthnicCategoryID
	,ReligionID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
(
--Checksum
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID
	,MetricID
	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned

from
	#ChecksumEncounter Encounter

union all

-- FCE Begin

--FCE Checksum
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'CHKFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null

union all

--Inpatient Elective FCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'IEFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '1'
and	Encounter.AdmissionTypeCode = 'EL' --elective

union all

--Day Case FCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'DCFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '2'
and	Encounter.AdmissionTypeCode = 'EL' --elective


union all

--Regular Day FCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RDFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '3'
and	Encounter.AdmissionTypeCode = 'EL' --elective

union all

--Regular Night FCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RNFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '4'
and	Encounter.AdmissionTypeCode = 'EL' --elective

union all

--Other FCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'NEOTHFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.AdmissionTypeCode = 'OTH' --elective

union all

--Maternity FCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'MATFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.AdmissionTypeCode = 'MAT' --elective

union all

--Emergency FCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'EMFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '1'
and	Encounter.AdmissionTypeCode = 'EM' --emergency


-- FCE End

union all

---- Admissions Begin

----Admissions Checksum
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'CHKADM'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Inpatient Elective Admissions
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'IEADM'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	Encounter.NationalPatientClassificationCode = '1'
and	Encounter.AdmissionTypeCode = 'EL'

union all

--Day Case Admissions
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'DCADM'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	Encounter.NationalPatientClassificationCode = '2'
and	Encounter.AdmissionTypeCode = 'EL'

union all

--Regular Day Admissions
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RDADM'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	Encounter.NationalPatientClassificationCode = '3'
and	Encounter.AdmissionTypeCode = 'EL'

union all

--Regular Night Admissions
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RNADM'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	Encounter.NationalPatientClassificationCode = '4'
and	Encounter.AdmissionTypeCode = 'EL'

union all

--Other Admissions
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'NEOTHADM'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	Encounter.AdmissionTypeCode = 'OTH'

union all

--Maternity Admissions
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'MATADM'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	Encounter.AdmissionTypeCode = 'MAT'

union all

--Emergency Admissions
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'EMADM'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE
	,VTECategoryID
	,VTEExclusionReasonID
	,VTECompleteAndOrExclusion
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID
	,DischargeSummaryRequired 
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	Encounter.AdmissionTypeCode = 'EM'

--Admissions End

union all

--Discharges Begin

--Discharges Checksum
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'CHKDIS'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1

union all

--Inpatient Elective Discharges
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'IEDIS'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.NationalPatientClassificationCode = '1'
and	Encounter.AdmissionTypeCode = 'EL' --elective

union all

--Day Case Discharges
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'DCDIS'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.NationalPatientClassificationCode = '2'
and	Encounter.AdmissionTypeCode = 'EL' --elective

union all

--Regular Day Discharges
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RDDIS'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.NationalPatientClassificationCode = '3'
and	Encounter.AdmissionTypeCode = 'EL' --elective

union all

--Regular Night Discharges
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RNDIS'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.NationalPatientClassificationCode = '4'
and	Encounter.AdmissionTypeCode = 'EL' --elective

union all

--Other Discharges
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'NEOTHDIS'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.AdmissionTypeCode = 'OTH' --elective

union all

--Maternity Discharges
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'MATDIS'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.AdmissionTypeCode = 'MAT' --maternity

union all

--Emergency Discharges
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'EMDIS'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.AdmissionTypeCode = 'EM' --emergency

----Discharges End

union all

-- Births Begin

--Well Babies
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'WELLBABY'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.NationalNeonatalLevelOfCareCode in ('0' , '8') --normal or not specified
and	Encounter.PrimaryDiagnosisCode like 'Z38%'

union all

--Unwell Babies
select
	 MergeEncounterRecno
	,EncounterDateID = AdmissionDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'ILLBABY'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = StartSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
	,StartDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.NationalNeonatalLevelOfCareCode not in ('0' , '8') --normal or not specified
and	Encounter.PrimaryDiagnosisCode like 'Z38%'

-- Births End

union all

-- Deaths Begin

--Died
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'DIED'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.NationalDischargeMethodCode = '4'

union all

--Stillbirths
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'STILL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = Encounter.DischargeTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,AdmissionWardID
	,DischargeWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.LastEpisodeInSpellIndicator = 1
and	Encounter.NationalDischargeMethodCode = '5'


--Deaths End

union all


-- FFCE Begin

--FFCE Checksum
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'CHKFFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Inpatient Elective FFCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'IEFFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '1'
and	Encounter.AdmissionTypeCode = 'EL' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Day Case FFCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'DCFFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '2'
and	Encounter.AdmissionTypeCode = 'EL' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'


union all

--Regular Day FFCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RDFFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '3'
and	Encounter.AdmissionTypeCode = 'EL' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Regular Night FFCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RNFFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '4'
and	Encounter.AdmissionTypeCode = 'EL' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Other FFCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'NEOTHFFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.AdmissionTypeCode = 'OTH' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Maternity FFCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'MATFFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.AdmissionTypeCode = 'MAT' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Emergency FFCEs
select
	 MergeEncounterRecno
	,EncounterDateID = EncounterEndDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'EMFFCE'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfEncounter
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = EpisodicHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.EpisodeEndDate is not null
and	Encounter.NationalPatientClassificationCode = '1'
and	Encounter.AdmissionTypeCode = 'EM' --emergency
and	Encounter.FirstEpisodeInSpellIndicator = 'true'


-- FFCE End


union all


-- SPL Begin

--SPL Checksum
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'CHKSPL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Inpatient Elective SPLs
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'IESPL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.NationalPatientClassificationCode = '1'
and	Encounter.AdmissionTypeCode = 'EL' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Day Case SPLs
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'DCSPL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.NationalPatientClassificationCode = '2'
and	Encounter.AdmissionTypeCode = 'EL' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'


union all

--Regular Day SPLs
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RDSPL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.NationalPatientClassificationCode = '3'
and	Encounter.AdmissionTypeCode = 'EL' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Regular Night SPLs
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'RNSPL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.NationalPatientClassificationCode = '4'
and	Encounter.AdmissionTypeCode = 'EL' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Other SPLs
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'NEOTHSPL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.AdmissionTypeCode = 'OTH' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Maternity SPLs
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'MATSPL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.AdmissionTypeCode = 'MAT' --elective
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Emergency SPLs
select
	 MergeEncounterRecno
	,EncounterDateID = DischargeDateID

	,MetricID =
		(
		select
			Metric.MetricID
		from
			APC.Metric
		where
			Metric.MetricCode = 'EMSPL'
		)

	,ConsultantID
	,SpecialtyID
	,SiteID = EndSiteID
	,AgeID
	,AdmissionMethodID
	,RTTActivity
	,RTTTreated
	,EncounterTimeOfDay = EncounterEndTimeOfDay
	,EndDirectorateID
	,SexID
	,ISTAdmissionSpecialtyID
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,ContextID
	,AdmissionSourceID
	,DischargeDestinationID
	,DischargeMethodID
	,IntendedManagementID
	,NeonatalLevelOfCareID
	,PatientClassificationID
	,CodingComplete
	,VTE = 0
	,VTECategoryID = @MissingVTECategoryID
	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
	,VTECompleteAndOrExclusion = 0
	,StartWardID
	,EndWardID
	,NationalExamID
	,ServiceID
	,CCGID
	,PrimaryDiagnosisID
	,Cases
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNumber
	,Encounter.ContextCode
	,LengthOfSpell
	,ResidenceCCGID
	,MergeDocumentRecno 
	,DischargeSummaryStatusID 
	,DischargeSummarySignedByID 
	,DischargeSummaryProductionTimeID 
	,DischargeSummaryExclusionReasonID 
	,DischargeSummaryRequired
	,DischargeSummarySignedTimeID 
	,GPPracticeID 
	,ElectronicCorrespondenceActivated 
	,CharlsonIndex
	,PrimaryProcedureID
	,HRGID = SpellHRGID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,DischargeSummaryTransmissionTimeID
	,Transmitted
	,DischargeSummarySigned
from
	#ChecksumEncounter Encounter
where
	Encounter.DischargeDate is not null
and	Encounter.NationalPatientClassificationCode = '1'
and	Encounter.AdmissionTypeCode = 'EM' --emergency
and	Encounter.FirstEpisodeInSpellIndicator = 'true'


-- SPL End


--union all

----SHMI
--select
--	 Encounter.MergeEncounterRecno
--	,EncounterDateID = EncounterReference.DischargeDateID
--	,Metric.MetricID
--	,EncounterReference.ConsultantID
--	,EncounterReference.SpecialtyID
--	,SiteID = EncounterReference.StartSiteID
--	,EncounterReference.AgeID
--	,EncounterReference.AdmissionMethodID
--	,RTTActivity = coalesce(Encounter.RTTActivity, 0)
--	,RTTTreated = coalesce(Encounter.RTTTreated, 0)
--	,EncounterTimeOfDay = Encounter.AdmissionTimeOfDay
--	,EndDirectorateID = Directorate.DirectorateID
--	,EncounterReference.SexID
--	,EncounterReference.ISTAdmissionSpecialtyID
--	,ISTAdmissionDemandTimeOfDay = coalesce(Encounter.ISTAdmissionDemandTimeOfDay, -1)
--	,ISTDischargeTimeOfDay = coalesce(Encounter.ISTDischargeTimeOfDay, -1)
--	,EncounterReference.ContextID
--	,EncounterReference.AdmissionSourceID
--	,EncounterReference.DischargeDestinationID
--	,EncounterReference.DischargeMethodID
--	,EncounterReference.IntendedManagementID
--	,EncounterReference.NeonatalLevelOfCareID
--	,EncounterReference.PatientClassificationID

--	,Encounter.SourcePatientNo
	--,Encounter.CasenoteNumber
	--,Encounter.ContextCode


--	,CodingComplete =
--		case
--		when Encounter.ClinicalCodingCompleteDate is null
--		then 0
--		else 1
--		end

--	,VTE = 0
--	,VTECategoryID = @MissingVTECategoryID
--	,VTEExclusionReasonID = @MissingVTEExclusionReasonID
--	,VTECompleteAndOrExclusion = 0
--	,WardID = EncounterReference.StartWardID

--	,NationalExamID = NationalExam.AllocationID
	--,ServiceID = Service.AllocationID

	--,CCGID = EncounterReference.CCGID

	--,Encounter.Cases
--	,LengthOfEncounter
--	,ResidenceCCGID = Encounter.LOS
--from
--	APC.BaseEncounter Encounter

--inner join APC.BaseEncounterReference EncounterReference
--on	EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

--inner join Allocation.DatasetAllocation NationalExam
--on	NationalExam.AllocationTypeID = 1
--and	NationalExam.DatasetCode = 'APC'
--and	NationalExam.DatasetRecno = Encounter.MergeEncounterRecno

--inner join Allocation.DatasetAllocation Service
--on	Service.AllocationTypeID = 2
--and	Service.DatasetCode = 'APC'
--and	Service.DatasetRecno = Encounter.MergeEncounterRecno

--inner join WH.Directorate
--on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

--inner join APC.Metric
--on	Metric.MetricCode = 'SHMIFCE'

--inner join
--		(
--			select
--				EncounterRecno
--				,DominantForDiagnosis = 
--								case
--									when -- search for second episode in spell if none 1st fce is dominant
--										not exists
--											(									
--												select
--													1
--												from
--													(
--													select
--														ProviderSpellNo
--														,ContextID
--														,EpisodeSequenceNo = row_number() over (partition by ContextID, ProviderSpellNo order by SourceEncounterNo)         
--													from
--														APC.Encounter
--														) encounternext
--												where
--													encounter.ProviderSpellNo = encounternext.ProviderSpellNo
--												and encounter.ContextID = encounternext.ContextID
--												and encounternext.EpisodeSequenceNo = 2						
--											)
--									then 1
--									when 
--										row_number() over (partition by ContextID, ProviderSpellNo order by SourceEncounterNo) = 1 -- if first episode primary diagnosis is not sign or symptom use it
--										and	left(encounter.PrimaryDiagnosisCode, 1) <> 'R'
--									then 1
--									when 
--										row_number() over (partition by ContextID, ProviderSpellNo order by SourceEncounterNo) = 1 -- if first episode primary diagnosis is sign or symptom and so is second use the first
--									and	left(encounter.PrimaryDiagnosisCode, 1) = 'R'
--									and 
--										exists
--											(
--												select
--													1
--												from
--													(
--													select
--														ProviderSpellNo
--														,ContextID
--														,PrimaryDiagnosisCode
--														,EpisodeSequenceNo = row_number() over (partition by ContextID, ProviderSpellNo order by SourceEncounterNo)         
--													from
--														APC.Encounter
--														) encounternext
--												where
--														encounter.ProviderSpellNo = encounternext.ProviderSpellNo
--												and encounter.ContextID = encounternext.ContextID
--												and encounternext.EpisodeSequenceNo = 2			
--												and left(encounternext.PrimaryDiagnosisCode, 1) = 'R'
--											)
--									then 1
--									when row_number() over (partition by ContextID, ProviderSpellNo order by SourceEncounterNo) = 2 -- if second episode is not sign and symptom but the first is use the second
--									and	left(encounter.PrimaryDiagnosisCode, 1) <> 'R'
--									and	exists
--											(
--												select
--													1
--												from
--													(
--													select
--														ProviderSpellNo
--														,ContextID
--														,PrimaryDiagnosisCode
--														,EpisodeSequenceNo = row_number() over (partition by ContextID, ProviderSpellNo order by SourceEncounterNo)         
--													from
--														APC.Encounter
--													) encounterprevious
--												where
--													encounter.ProviderSpellNo = encounterprevious.ProviderSpellNo
--													and encounter.ContextID = encounterprevious.ContextID
--													and encounterprevious.EpisodeSequenceNo = 1
--													and left(encounterprevious.PrimaryDiagnosisCode, 1) = 'R'
--											)
--									then 1
--									else null
--								end
--			from
--				APC.BaseEncounter Encounter

--			inner join APC.PatientClassification PatientClassification
--			on Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID

--			inner join APC.DischargeMethod DischargeMethod
--			on Encounter.DischargeMethodID = DischargeMethod.SourceDischargeMethodID

--			--inner join WH.Calendar CalendarDischarge
--			--on Encounter.DischargeDateID = CalendarDischarge.DateID

--			where 
--			--	CalendarDischarge.TheDate between '1 apr 2012' and '30 may 2012'
--			--and
--				coalesce(NationalPatientClassificationCode,'') not in ('2','3','4') -- exclude day cases and regular day and night admissions
--			and
--				[NationalDischargeMethodCode] not in ('5') -- Exclude Stillbirths

--			) SHMI

--	on	SHMI.DominantForDiagnosis = 1
--	and Encounter.MergeEncounterRecno = SHMI.MergeEncounterRecno

--where
--	Encounter.DischargeDate is not null
) Encounter


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






﻿
CREATE proc [APC].[BuildBaseEncounterBaseFall]

as

truncate table APC.BaseEncounterBaseFall

insert into APC.BaseEncounterBaseFall
(
	MergeEncounterRecno
	,MergeFallRecno
)

select
	MergeEncounterRecno
	,MergeFallRecno
from
	(
	select
		MergeEncounterRecno
		,MergeFallRecno
	from
		APC.BaseEncounter
	inner join APC.BaseFall
	on	BaseFall.SourcePatientNo = BaseEncounter.SourcePatientNo
	and	BaseFall.FallTime between BaseEncounter.EpisodeStartTime and BaseEncounter.EpisodeEndTime
	and	BaseFall.ContextCode = 'CEN||BEDMAN'
	and	BaseEncounter.ContextCode = 'CEN||PAS'

	-- Trafford here...

	) BaseEncounterBaseFall



﻿



	/******************************************************************************
	**  Name: APC.fn_HasDiagnosis
	**  Purpose: Returns a table of Spells where any episode in the spell has one or more
	**           diagnosis codes that match the input parameter pattern
	**
	**	Parameters
	**
	**      1   - Pattern : Format this for a LIKE statement eg 'F01%' or 'F1[0-9].7' for F10.7, F11.7, ... F19.7 etc
	**      2	- Delimiter : Any character used to delimit more than one pattern in parameter 1
	**                   eg  a pipe (|) in the parameter 1 value 'F01%|F1[0-9].7'
	**
	**	NOTE - Please maintain this function via Visual Studio 
	**         located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 17.07.12     MH        Created
	** 24.10.12     MH        Additional column to denote if matched diagnosis code was primary or secondary
	******************************************************************************/
	
CREATE FUNCTION [APC].[fn_HasDiagnosis]
(
	 @DiagnosisPattern	VARCHAR(100)
	,@PatternDelimiter	CHAR(1)
)
RETURNS TABLE AS  
RETURN
(
	SELECT --DISTINCT	
		 ProviderSpellNo = GlobalProviderSpellNo
		,IsAPrimaryDiagnosis		= MAX(CASE WHEN PrimaryDiagnosisCode LIKE CODES.DiagnosisCode THEN 1 ELSE 0 END)
	
	FROM
		APC.BaseEncounter E
	CROSS APPLY
	(
		SELECT
			DiagnosisCode = DiagnosisCode + '%'			-- To handle "daggers" and other code extensions
		FROM 
			[$(Warehouse)].PAS.Diagnosis
			CROSS APPLY Utility.fn_ListToTable(@DiagnosisPattern, @PatternDelimiter) T
		WHERE 
			DiagnosisCode LIKE T.item
	) CODES
	
	WHERE	
			PrimaryDiagnosisCode		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode1		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode2		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode3		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode4		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode5		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode6		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode7		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode8		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode9		LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode10	LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode11	LIKE CODES.DiagnosisCode
		OR	SecondaryDiagnosisCode12	LIKE CODES.DiagnosisCode	
		
	GROUP BY
		GlobalProviderSpellNo	
)




﻿CREATE TABLE [APC].[BaseEncounterBaseDrugAdministered] (
    [MergeEncounterRecno] INT NOT NULL,
    [MergeDrugRecno]      INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseDrug] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeDrugRecno] ASC)
);


﻿CREATE TABLE [APC].[WardBase] (
    [WardCode]    VARCHAR (20)  NOT NULL,
    [ContextCode] VARCHAR (8)   NOT NULL,
    [Ward]        VARCHAR (255) NULL,
    [SiteCode]    VARCHAR (10)  NULL,
    CONSTRAINT [PK_WardBase] PRIMARY KEY CLUSTERED ([WardCode] ASC, [ContextCode] ASC)
);


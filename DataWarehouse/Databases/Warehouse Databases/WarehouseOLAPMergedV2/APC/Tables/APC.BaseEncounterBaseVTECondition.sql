﻿CREATE TABLE [APC].[BaseEncounterBaseVTECondition] (
    [MergeEncounterRecno]    INT NOT NULL,
    [MergeVTEConditionRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseVTECondition ] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeVTEConditionRecno] ASC)
);


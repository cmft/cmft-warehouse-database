﻿CREATE TABLE [APC].[ProcessListArchive] (
    [MergeRecno]  INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    [ArchiveTime] DATETIME      NOT NULL,
    CONSTRAINT [PK_ProcessListArchive] PRIMARY KEY CLUSTERED ([MergeRecno] ASC, [ArchiveTime] ASC)
);


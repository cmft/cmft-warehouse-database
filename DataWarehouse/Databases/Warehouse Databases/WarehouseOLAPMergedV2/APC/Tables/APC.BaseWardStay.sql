﻿CREATE TABLE [APC].[BaseWardStay] (
    [MergeEncounterRecno] INT           IDENTITY (1, 1) NOT NULL,
    [ContextCode]         VARCHAR (10)  NOT NULL,
    [SourceUniqueID]      VARCHAR (50)  NOT NULL,
    [SourcePatientNo]     VARCHAR (20)  NOT NULL,
    [SourceSpellNo]       VARCHAR (20)  NOT NULL,
    [ProviderSpellNo]     VARCHAR (50)  NOT NULL,
    [StartDate]           SMALLDATETIME NOT NULL,
    [EndDate]             SMALLDATETIME NULL,
    [StartTime]           SMALLDATETIME NULL,
    [EndTime]             SMALLDATETIME NULL,
    [SiteCode]            VARCHAR (5)   NULL,
    [WardCode]            VARCHAR (10)  NULL,
    [StartActivityCode]   VARCHAR (10)  NULL,
    [EndActivityCode]     VARCHAR (10)  NULL,
    [Created]             DATETIME      NOT NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NOT NULL,
    [DirectorateCode]     VARCHAR (5)   NULL,
    CONSTRAINT [PK_BaseWardStay_1] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseWardStay_1]
    ON [APC].[BaseWardStay]([ProviderSpellNo] ASC, [StartTime] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_APCBaseWardStay_WC_SD_SAC__PSN]
    ON [APC].[BaseWardStay]([WardCode] ASC, [StartDate] ASC, [StartActivityCode] ASC)
    INCLUDE([ProviderSpellNo])
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_Tmp__EndDate]
    ON [APC].[BaseWardStay]([EndDate] ASC)
    INCLUDE([MergeEncounterRecno], [ContextCode], [ProviderSpellNo], [StartDate], [StartTime], [EndTime]);


GO
CREATE NONCLUSTERED INDEX [IX_APCBE_SPN-SSN-ST]
    ON [APC].[BaseWardStay]([SourcePatientNo] ASC, [SourceSpellNo] ASC, [StartTime] ASC)
    INCLUDE([MergeEncounterRecno]);


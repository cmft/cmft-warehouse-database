﻿CREATE TABLE [APC].[DementiaCQUINCurrent] (
    [ReportingMonth]                           DATE            NULL,
    [GlobalProviderSpellNo]                    VARCHAR (50)    NULL,
    [Directorate]                              VARCHAR (50)    NULL,
    [Division]                                 VARCHAR (50)    NULL,
    [LocalAdmissionMethod]                     VARCHAR (200)   NULL,
    [AdmissionDate]                            SMALLDATETIME   NULL,
    [AdmissionTime]                            SMALLDATETIME   NULL,
    [AdmissionWardCode]                        VARCHAR (10)    NULL,
    [DischargeDate]                            SMALLDATETIME   NULL,
    [DischargeTime]                            SMALLDATETIME   NULL,
    [LocalDischargeMethod]                     VARCHAR (200)   NULL,
    [PatientSurname]                           VARCHAR (80)    NULL,
    [SexCode]                                  VARCHAR (1)     NULL,
    [NHSNumber]                                VARCHAR (17)    NULL,
    [DistrictNo]                               VARCHAR (50)    NULL,
    [CasenoteNumber]                           VARCHAR (20)    NULL,
    [LatestWard]                               VARCHAR (10)    NULL,
    [LatestSpecialtyCode]                      VARCHAR (10)    NULL,
    [LatestConsultantCode]                     VARCHAR (50)    NULL,
    [AgeOnAdmission]                           NUMERIC (18, 6) NULL,
    [LoS]                                      INT             NULL,
    [OriginalSubmissionTime]                   DATETIME        NULL,
    [OriginalResponse]                         VARCHAR (20)    NULL,
    [LatestKnownDementiaRecordedTime]          DATETIME        NULL,
    [LatestKnownToHaveDementiaResponse]        INT             NULL,
    [LatestResponseTime]                       DATETIME        NULL,
    [LatestResponse]                           INT             NULL,
    [LatestAssessmentTime]                     DATETIME        NULL,
    [LatestAssessmentScore]                    INT             NULL,
    [LatestInvestigationTime]                  DATETIME        NULL,
    [LatestReferralSentDate]                   VARCHAR (20)    NULL,
    [LatestReferralRecordedTime]               DATETIME        NULL,
    [LatestAssessmentNotPerformedReason]       VARCHAR (60)    NULL,
    [LatestAssessmentNotPerformedRecordedTime] DATETIME        NULL,
    [Q1Denominator]                            INT             NULL,
    [Q1Numerator]                              INT             NULL,
    [Q2Denominator]                            INT             NOT NULL,
    [Q2Numerator]                              INT             NULL,
    [Q3Denominator]                            INT             NOT NULL,
    [Q3Numerator]                              INT             NOT NULL,
    [FreezeDate]                               DATETIME        NOT NULL,
    [ByWhom]                                   NVARCHAR (128)  NULL,
    [DementiaUpdated]                          DATETIME        NULL,
    [WardUpdated]                              DATETIME        NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_DCQFR_Q1Nu_RMonth]
    ON [APC].[DementiaCQUINCurrent]([Q1Numerator] ASC, [ReportingMonth] ASC)
    INCLUDE([GlobalProviderSpellNo], [LocalAdmissionMethod], [AdmissionDate], [AdmissionTime], [AdmissionWardCode], [DischargeDate], [DischargeTime], [LocalDischargeMethod], [PatientSurname], [SexCode], [NHSNumber], [DistrictNo], [CasenoteNumber], [LatestWard], [LatestSpecialtyCode], [LatestConsultantCode], [AgeOnAdmission], [LoS], [OriginalSubmissionTime], [OriginalResponse], [LatestKnownDementiaRecordedTime], [LatestKnownToHaveDementiaResponse], [LatestResponseTime], [LatestResponse], [LatestAssessmentTime], [LatestAssessmentScore], [LatestInvestigationTime], [LatestReferralSentDate], [LatestReferralRecordedTime], [LatestAssessmentNotPerformedReason], [LatestAssessmentNotPerformedRecordedTime], [Q1Denominator], [Q2Denominator], [Q2Numerator], [Q3Denominator], [Q3Numerator]);


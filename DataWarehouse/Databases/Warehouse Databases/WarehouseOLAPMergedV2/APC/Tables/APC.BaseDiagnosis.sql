﻿CREATE TABLE [APC].[BaseDiagnosis] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [DiagnosisCode]       VARCHAR (10)  NOT NULL,
    [Created]             DATETIME      NOT NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseDiagnosis] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseDiagnosis-mer]
    ON [APC].[BaseDiagnosis]([DiagnosisCode] ASC)
    INCLUDE([MergeEncounterRecno]);


﻿CREATE TABLE [APC].[BaseWardStayBaseResult] (
    [MergeEncounterRecno] INT NOT NULL,
    [MergeResultRecno]    INT NOT NULL,
    CONSTRAINT [PK__BaseWard__55CEE8013D759070] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeResultRecno] ASC)
);


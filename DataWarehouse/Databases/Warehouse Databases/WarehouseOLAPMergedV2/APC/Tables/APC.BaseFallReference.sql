﻿CREATE TABLE [APC].[BaseFallReference] (
    [MergeFallRecno]    INT          NOT NULL,
    [ContextID]         INT          NULL,
    [FallDateID]        INT          NULL,
    [InitialSeverityID] INT          NULL,
    [WardID]            INT          NULL,
    [Created]           DATETIME     NULL,
    [Updated]           DATETIME     NULL,
    [ByWhom]            VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseFall__C8F71D5250728AF4] PRIMARY KEY CLUSTERED ([MergeFallRecno] ASC)
);


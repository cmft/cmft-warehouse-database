﻿CREATE TABLE [APC].[BaseCriticalCarePeriodReference] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [SiteID]              INT           NULL,
    [WardID]              INT           NULL,
    [TreatmentFunctionID] INT           NULL,
    [StartDateID]         INT           NULL,
    [EndDateID]           INT           NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_APC_Base_CriticalCarePeriod_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


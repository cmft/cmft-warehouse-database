﻿CREATE TABLE [APC].[HRG4Flag] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          INT           NOT NULL,
    [FlagCode]            VARCHAR (50)  NULL,
    [Created]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_HRG4Flag] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


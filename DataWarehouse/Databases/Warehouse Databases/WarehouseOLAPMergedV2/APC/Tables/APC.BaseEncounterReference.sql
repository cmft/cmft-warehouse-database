﻿CREATE TABLE [APC].[BaseEncounterReference] (
    [MergeEncounterRecno]       INT           NOT NULL,
    [ContextID]                 INT           NOT NULL,
    [AdmissionMethodID]         INT           NULL,
    [AdmissionSourceID]         INT           NULL,
    [DischargeDestinationID]    INT           NULL,
    [DischargeMethodID]         INT           NULL,
    [IntendedManagementID]      INT           NULL,
    [ISTAdmissionSpecialtyID]   INT           NULL,
    [NeonatalLevelOfCareID]     INT           NULL,
    [PatientClassificationID]   INT           NULL,
    [VTECategoryID]             INT           NULL,
    [StartWardID]               INT           NULL,
    [AgeID]                     INT           NULL,
    [AdmissionDateID]           INT           NULL,
    [DischargeDateID]           INT           NULL,
    [EpisodeStartDateID]        INT           NULL,
    [EpisodeEndDateID]          INT           NULL,
    [ConsultantID]              INT           NULL,
    [SexID]                     INT           NULL,
    [StartSiteID]               INT           NULL,
    [EndSiteID]                 INT           NULL,
    [SpecialtyID]               INT           NULL,
    [NHSNumberStatusID]         INT           NULL,
    [OperationStatusID]         INT           NULL,
    [EthnicOriginID]            INT           NULL,
    [MaritalStatusID]           INT           NULL,
    [SiteID]                    INT           NULL,
    [AdminCategoryID]           INT           NULL,
    [LastEpisodeInSpellID]      INT           NULL,
    [FirstRegDayOrNightAdmitID] INT           NULL,
    [RTTCurrentStatusID]        INT           NULL,
    [RTTPeriodStatusID]         INT           NULL,
    [ReferrerID]                INT           NULL,
    [ReferringConsultantID]     INT           NULL,
    [CarerSupportIndicatorID]   INT           NULL,
    [EndWardID]                 INT           NULL,
    [Created]                   DATETIME      NULL,
    [Updated]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (255) NULL,
    [CCGID]                     INT           NULL,
    [ResidenceCCGID]            INT           NULL,
    [StartLocationID]           INT           NULL,
    [EndLocationID]             INT           NULL,
    [ReligionID]                INT           NULL,
    CONSTRAINT [PK_APC_Base_Encounter_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BaseEncounterReference_DischargeMethod]
    ON [APC].[BaseEncounterReference]([DischargeMethodID] ASC)
    INCLUDE([MergeEncounterRecno], [AdmissionMethodID], [AdmissionSourceID], [DischargeDestinationID], [NeonatalLevelOfCareID], [PatientClassificationID], [DischargeDateID], [EpisodeStartDateID], [ConsultantID], [SexID], [SpecialtyID]);


GO
CREATE NONCLUSTERED INDEX [APC_BaseEncounterReference_1]
    ON [APC].[BaseEncounterReference]([AdmissionMethodID] ASC)
    INCLUDE([AgeID], [CCGID], [ConsultantID], [DischargeDateID], [MergeEncounterRecno], [SpecialtyID], [StartSiteID]);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseEncounterReference_1]
    ON [APC].[BaseEncounterReference]([AdmissionMethodID] ASC)
    INCLUDE([ConsultantID], [LastEpisodeInSpellID], [MergeEncounterRecno], [PatientClassificationID], [SpecialtyID]);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseEncounterRef-SID]
    ON [APC].[BaseEncounterReference]([SpecialtyID] ASC)
    INCLUDE([MergeEncounterRecno], [ConsultantID], [EthnicOriginID]);


GO
CREATE NONCLUSTERED INDEX [IX_EBR_AMI]
    ON [APC].[BaseEncounterReference]([AdmissionMethodID] ASC)
    INCLUDE([MergeEncounterRecno], [AgeID], [AdmissionDateID], [ConsultantID], [StartSiteID], [SpecialtyID], [CCGID]);


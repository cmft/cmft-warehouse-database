﻿CREATE TABLE [APC].[HRG4Spell] (
    [MergeEncounterRecno]    INT           NOT NULL,
    [HRGCode]                VARCHAR (50)  NULL,
    [GroupingMethodFlag]     VARCHAR (50)  NULL,
    [DominantOperationCode]  VARCHAR (50)  NULL,
    [PrimaryDiagnosisCode]   VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode] VARCHAR (50)  NULL,
    [EpisodeCount]           VARCHAR (50)  NULL,
    [LOS]                    VARCHAR (50)  NULL,
    [PBCCode]                VARCHAR (50)  NULL,
    [Created]                DATETIME      NULL,
    [ByWhom]                 VARCHAR (255) NULL,
    CONSTRAINT [PK_HRG4Spell] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


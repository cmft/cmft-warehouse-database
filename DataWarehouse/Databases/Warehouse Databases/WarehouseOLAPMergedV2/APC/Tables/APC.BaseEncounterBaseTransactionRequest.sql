﻿CREATE TABLE [APC].[BaseEncounterBaseTransactionRequest] (
    [MergeEncounterRecno]   INT NOT NULL,
    [MergeTransactionRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseTransactionRequest] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeTransactionRecno] ASC)
);


﻿CREATE TABLE [APC].[BedDayWardBase] (
    [SourceWardCode] VARCHAR (10) NOT NULL,
    [PODCode]        VARCHAR (20) NULL,
    [IsRealtimePOD]  BIT          CONSTRAINT [DF__BedDayLoc__IsRea__2DD28ED6] DEFAULT ((0)) NULL,
    [HRGCode]        VARCHAR (10) NULL,
    [ContextCode]    VARCHAR (10) NOT NULL,
    [FromDate]       DATE         CONSTRAINT [DF__BedDayLoc__FromD__7D2535DA] DEFAULT ('1 jan 1900') NOT NULL,
    [ToDate]         DATE         NULL,
    CONSTRAINT [PK_BedDayWard] PRIMARY KEY CLUSTERED ([SourceWardCode] ASC, [ContextCode] ASC, [FromDate] ASC)
);


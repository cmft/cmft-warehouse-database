﻿CREATE TABLE [APC].[HRG4CriticalCareEncounter] (
    [MergeEncounterRecno]     INT           NOT NULL,
    [HRGCode]                 VARCHAR (50)  NULL,
    [CriticalCareDays]        INT           NULL,
    [CriticalCareWarningFlag] VARCHAR (10)  NULL,
    [Created]                 DATETIME      NULL,
    [ByWhom]                  VARCHAR (255) NULL,
    CONSTRAINT [PK_HRG4CriticalCareEncounter] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


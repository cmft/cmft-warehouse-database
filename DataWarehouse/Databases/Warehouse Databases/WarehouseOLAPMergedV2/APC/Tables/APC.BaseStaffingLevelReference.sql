﻿CREATE TABLE [APC].[BaseStaffingLevelReference] (
    [MergeStaffingLevelRecno] INT          NOT NULL,
    [ContextID]               INT          NULL,
    [CensusDateID]            INT          NULL,
    [WardID]                  INT          NULL,
    [ShiftID]                 INT          NULL,
    [Created]                 DATETIME     NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseStaf__A53A2A6A49266EE7] PRIMARY KEY CLUSTERED ([MergeStaffingLevelRecno] ASC)
);


﻿CREATE TABLE [APC].[BaseEncounterBaseUrinaryTractInfection] (
    [MergeEncounterRecno]             INT NOT NULL,
    [MergeUrinaryTractInfectionRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseUrinaryTractInfection] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeUrinaryTractInfectionRecno] ASC)
);


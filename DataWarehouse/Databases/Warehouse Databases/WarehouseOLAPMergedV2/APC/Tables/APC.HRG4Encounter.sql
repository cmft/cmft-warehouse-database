﻿CREATE TABLE [APC].[HRG4Encounter] (
    [MergeEncounterRecno]   INT           NOT NULL,
    [HRGCode]               VARCHAR (50)  NULL,
    [GroupingMethodFlag]    VARCHAR (50)  NULL,
    [DominantOperationCode] VARCHAR (50)  NULL,
    [PBCCode]               VARCHAR (50)  NULL,
    [LOE]                   INT           NULL,
    [Created]               DATETIME      NULL,
    [ByWhom]                VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


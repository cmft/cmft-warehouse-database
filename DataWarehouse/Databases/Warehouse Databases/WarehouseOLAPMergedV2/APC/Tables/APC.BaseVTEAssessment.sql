﻿CREATE TABLE [APC].[BaseVTEAssessment] (
    [MergeVTEAssessmentRecno]            INT          IDENTITY (1, 1) NOT NULL,
    [VTEAssessmentRecno]                 INT          NOT NULL,
    [SourceUniqueID]                     INT          NOT NULL,
    [SourcePatientNo]                    VARCHAR (8)  NULL,
    [SourceSpellNo]                      VARCHAR (5)  NULL,
    [NoAssessmentReasonID]               INT          NULL,
    [AssessmentDate]                     DATE         NULL,
    [AssessmentTime]                     DATETIME     NULL,
    [AtRisk]                             INT          NOT NULL,
    [PreventativeMedicationStartDate]    DATE         NULL,
    [PreventativeMedicationStartTime]    DATETIME     NULL,
    [PreventativeNursingActionStartDate] DATE         NULL,
    [PreventativeNursingActionStartTime] DATETIME     NULL,
    [WardCode]                           VARCHAR (4)  NOT NULL,
    [InterfaceCode]                      VARCHAR (6)  NOT NULL,
    [ContextCode]                        VARCHAR (11) NOT NULL,
    [Created]                            DATETIME     NULL,
    [Updated]                            DATETIME     NULL,
    [ByWhom]                             VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseVTEA__8DCCA9F12C352A7E] PRIMARY KEY CLUSTERED ([MergeVTEAssessmentRecno] ASC)
);


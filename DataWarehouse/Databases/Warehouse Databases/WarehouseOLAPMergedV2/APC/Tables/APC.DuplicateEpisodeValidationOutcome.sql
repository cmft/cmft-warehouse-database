﻿CREATE TABLE [APC].[DuplicateEpisodeValidationOutcome] (
    [Reportable]            INT          NOT NULL,
    [ReportableDescription] VARCHAR (50) NULL,
    CONSTRAINT [PK_APC_DuplicateValidationOutcome] PRIMARY KEY NONCLUSTERED ([Reportable] ASC)
);


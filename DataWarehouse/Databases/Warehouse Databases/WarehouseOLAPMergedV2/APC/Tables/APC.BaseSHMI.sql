﻿CREATE TABLE [APC].[BaseSHMI] (
    [MergeEncounterRecno]         INT          NOT NULL,
    [DominantForDiagnosis]        BIT          NULL,
    [Survived]                    BIT          NULL,
    [Died]                        BIT          NULL,
    [DateOfDeath]                 DATE         NULL,
    [DischargeDate]               DATE         NULL,
    [PatientAge]                  INT          NULL,
    [PatientAgeCategoryCode]      INT          NULL,
    [AdmissionMethodCode]         VARCHAR (20) NULL,
    [AdmissionMethodCategoryCode] INT          NOT NULL,
    [SexCode]                     VARCHAR (20) NULL,
    [SexCategoryCode]             INT          NULL,
    [PrimaryDiagnosisCode]        VARCHAR (6)  NULL,
    [DiagnosisGroupCode]          INT          NULL,
    [DiagnosisGroupCategoryCode]  INT          NULL,
    [CharlsonIndex]               INT          NOT NULL,
    [CharlsonIndexCategoryCode]   INT          NOT NULL,
    [YearIndexCode]               INT          NOT NULL,
    [RiskScore]                   FLOAT (53)   NULL,
    [ExternalRiskScore]           FLOAT (53)   NULL,
    CONSTRAINT [PK_BaseSHMI] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


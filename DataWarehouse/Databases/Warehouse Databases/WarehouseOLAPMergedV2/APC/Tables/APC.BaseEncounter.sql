﻿CREATE TABLE [APC].[BaseEncounter] (
    [MergeEncounterRecno]            INT           IDENTITY (1, 1) NOT NULL,
    [EncounterRecno]                 BIGINT        NOT NULL,
    [SourceUniqueID]                 VARCHAR (50)  NULL,
    [SourcePatientNo]                VARCHAR (20)  NULL,
    [SourceSpellNo]                  VARCHAR (20)  NULL,
    [SourceEncounterNo]              VARCHAR (20)  NULL,
    [ProviderSpellNo]                VARCHAR (50)  NULL,
    [PatientTitle]                   VARCHAR (50)  NULL,
    [PatientForename]                VARCHAR (80)  NULL,
    [PatientSurname]                 VARCHAR (80)  NULL,
    [DateOfBirth]                    DATETIME      NULL,
    [DateOfDeath]                    SMALLDATETIME NULL,
    [SexCode]                        VARCHAR (1)   NULL,
    [NHSNumber]                      VARCHAR (17)  NULL,
    [Postcode]                       VARCHAR (10)  NULL,
    [PatientAddress1]                VARCHAR (50)  NULL,
    [PatientAddress2]                VARCHAR (50)  NULL,
    [PatientAddress3]                VARCHAR (50)  NULL,
    [PatientAddress4]                VARCHAR (50)  NULL,
    [DHACode]                        VARCHAR (3)   NULL,
    [EthnicOriginCode]               VARCHAR (10)  NULL,
    [MaritalStatusCode]              VARCHAR (10)  NULL,
    [ReligionCode]                   VARCHAR (4)   NULL,
    [DateOnWaitingList]              SMALLDATETIME NULL,
    [AdmissionDate]                  SMALLDATETIME NULL,
    [DischargeDate]                  SMALLDATETIME NULL,
    [EpisodeStartDate]               SMALLDATETIME NULL,
    [EpisodeEndDate]                 SMALLDATETIME NULL,
    [StartSiteCode]                  VARCHAR (10)  NULL,
    [StartWardTypeCode]              VARCHAR (10)  NULL,
    [EndSiteCode]                    VARCHAR (10)  NULL,
    [EndWardTypeCode]                VARCHAR (10)  NULL,
    [RegisteredGpCode]               VARCHAR (10)  NULL,
    [RegisteredGpPracticeCode]       VARCHAR (10)  NULL,
    [SiteCode]                       VARCHAR (10)  NULL,
    [AdmissionMethodCode]            VARCHAR (10)  NULL,
    [AdmissionSourceCode]            VARCHAR (10)  NULL,
    [PatientClassificationCode]      VARCHAR (10)  NULL,
    [ManagementIntentionCode]        VARCHAR (10)  NULL,
    [DischargeMethodCode]            VARCHAR (10)  NULL,
    [DischargeDestinationCode]       VARCHAR (10)  NULL,
    [AdminCategoryCode]              VARCHAR (10)  NULL,
    [ConsultantCode]                 VARCHAR (50)  NULL,
    [SpecialtyCode]                  VARCHAR (10)  NULL,
    [LastEpisodeInSpellIndicator]    VARCHAR (1)   NULL,
    [FirstRegDayOrNightAdmit]        VARCHAR (1)   NULL,
    [NeonatalLevelOfCare]            VARCHAR (10)  NULL,
    [PASHRGCode]                     VARCHAR (10)  NULL,
    [PrimaryDiagnosisCode]           VARCHAR (50)  NULL,
    [SubsidiaryDiagnosisCode]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode1]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode2]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode3]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode4]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode5]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode6]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode7]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode8]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode9]        VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode10]       VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode11]       VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode12]       VARCHAR (50)  NULL,
    [SecondaryDiagnosisCode13]       VARCHAR (50)  NULL,
    [PrimaryProcedureCode]           VARCHAR (50)  NULL,
    [PrimaryProcedureDate]           SMALLDATETIME NULL,
    [SecondaryProcedureCode1]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate1]        SMALLDATETIME NULL,
    [SecondaryProcedureCode2]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate2]        SMALLDATETIME NULL,
    [SecondaryProcedureCode3]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate3]        SMALLDATETIME NULL,
    [SecondaryProcedureCode4]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate4]        SMALLDATETIME NULL,
    [SecondaryProcedureCode5]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate5]        SMALLDATETIME NULL,
    [SecondaryProcedureCode6]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate6]        SMALLDATETIME NULL,
    [SecondaryProcedureCode7]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate7]        SMALLDATETIME NULL,
    [SecondaryProcedureCode8]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate8]        SMALLDATETIME NULL,
    [SecondaryProcedureCode9]        VARCHAR (50)  NULL,
    [SecondaryProcedureDate9]        SMALLDATETIME NULL,
    [SecondaryProcedureCode10]       VARCHAR (50)  NULL,
    [SecondaryProcedureDate10]       SMALLDATETIME NULL,
    [SecondaryProcedureCode11]       VARCHAR (50)  NULL,
    [SecondaryProcedureDate11]       SMALLDATETIME NULL,
    [OperationStatusCode]            VARCHAR (10)  NULL,
    [ContractSerialNo]               VARCHAR (6)   NULL,
    [CodingCompleteDate]             SMALLDATETIME NULL,
    [PurchaserCode]                  VARCHAR (10)  NULL,
    [ProviderCode]                   VARCHAR (10)  NULL,
    [EpisodeStartTime]               SMALLDATETIME NULL,
    [EpisodeEndTime]                 SMALLDATETIME NULL,
    [RegisteredGdpCode]              VARCHAR (8)   NULL,
    [EpisodicGpCode]                 VARCHAR (10)  NULL,
    [InterfaceCode]                  VARCHAR (5)   NULL,
    [CasenoteNumber]                 VARCHAR (20)  NULL,
    [NHSNumberStatusCode]            VARCHAR (10)  NULL,
    [AdmissionTime]                  SMALLDATETIME NULL,
    [DischargeTime]                  SMALLDATETIME NULL,
    [TransferFrom]                   VARCHAR (14)  NULL,
    [DistrictNo]                     VARCHAR (50)  NULL,
    [ExpectedLOS]                    INT           NULL,
    [MRSAFlag]                       VARCHAR (50)  NULL,
    [RTTPathwayID]                   VARCHAR (25)  NULL,
    [RTTPathwayCondition]            VARCHAR (20)  NULL,
    [RTTStartDate]                   SMALLDATETIME NULL,
    [RTTEndDate]                     SMALLDATETIME NULL,
    [RTTSpecialtyCode]               VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]         VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]           VARCHAR (10)  NULL,
    [RTTCurrentStatusDate]           SMALLDATETIME NULL,
    [RTTCurrentPrivatePatientFlag]   BIT           NULL,
    [RTTOverseasStatusFlag]          BIT           NULL,
    [RTTPeriodStatusCode]            VARCHAR (10)  NULL,
    [IntendedPrimaryProcedureCode]   VARCHAR (10)  NULL,
    [Operation]                      VARCHAR (255) NULL,
    [Research1]                      VARCHAR (10)  NULL,
    [CancelledElectiveAdmissionFlag] VARCHAR (10)  NULL,
    [LocalAdminCategoryCode]         VARCHAR (10)  NULL,
    [EpisodicGpPracticeCode]         VARCHAR (10)  NULL,
    [PCTCode]                        VARCHAR (10)  NULL,
    [LocalityCode]                   VARCHAR (10)  NULL,
    [AgeCode]                        VARCHAR (27)  NULL,
    [HRGCode]                        VARCHAR (10)  NULL,
    [SpellHRGCode]                   VARCHAR (10)  NULL,
    [CategoryCode]                   VARCHAR (2)   NULL,
    [LOE]                            SMALLINT      NULL,
    [LOS]                            SMALLINT      NULL,
    [Cases]                          SMALLINT      NULL,
    [FirstEpisodeInSpellIndicator]   BIT           NULL,
    [RTTActivity]                    BIT           NULL,
    [RTTBreachStatusCode]            VARCHAR (10)  NULL,
    [DiagnosticProcedure]            BIT           NULL,
    [RTTTreated]                     BIT           NULL,
    [EpisodeStartTimeOfDay]          INT           NULL,
    [EpisodeEndTimeOfDay]            INT           NULL,
    [AdmissionTimeOfDay]             INT           NULL,
    [DischargeTimeOfDay]             INT           NULL,
    [StartDirectorateCode]           VARCHAR (5)   NULL,
    [EndDirectorateCode]             VARCHAR (5)   NULL,
    [ResidencePCTCode]               VARCHAR (10)  NULL,
    [ISTAdmissionSpecialtyCode]      VARCHAR (5)   NULL,
    [ISTAdmissionDemandTime]         SMALLDATETIME NULL,
    [ISTDischargeTime]               SMALLDATETIME NULL,
    [ISTAdmissionDemandTimeOfDay]    INT           NULL,
    [ISTDischargeTimeOfDay]          INT           NULL,
    [PatientCategoryCode]            VARCHAR (2)   NULL,
    [Research2]                      VARCHAR (50)  NULL,
    [ClinicalCodingStatus]           VARCHAR (6)   NULL,
    [ClinicalCodingCompleteDate]     SMALLDATETIME NULL,
    [ClinicalCodingCompleteTime]     SMALLDATETIME NULL,
    [ReferredByCode]                 VARCHAR (3)   NULL,
    [ReferrerCode]                   VARCHAR (20)  NULL,
    [DecidedToAdmitDate]             SMALLDATETIME NULL,
    [PsychiatricPatientStatusCode]   VARCHAR (1)   NULL,
    [LegalStatusClassificationCode]  VARCHAR (2)   NULL,
    [ReferringConsultantCode]        VARCHAR (20)  NULL,
    [DurationOfElectiveWait]         INT           NULL,
    [CarerSupportIndicator]          VARCHAR (3)   NULL,
    [DischargeReadyDate]             SMALLDATETIME NULL,
    [ContextCode]                    VARCHAR (10)  NOT NULL,
    [EddCreatedTime]                 DATETIME      NULL,
    [ExpectedDateOfDischarge]        SMALLDATETIME NULL,
    [EddCreatedByConsultantFlag]     BIT           NULL,
    [EddInterfaceCode]               VARCHAR (5)   NULL,
    [VTECategoryCode]                CHAR (1)      NULL,
    [Ambulatory]                     BIT           NULL,
    [Created]                        DATETIME      NULL,
    [Updated]                        DATETIME      NULL,
    [ByWhom]                         VARCHAR (255) NULL,
    [PseudoPostcode]                 VARCHAR (10)  NULL,
    [CCGCode]                        VARCHAR (10)  NULL,
    [GpCodeAtDischarge]              VARCHAR (8)   NULL,
    [GpPracticeCodeAtDischarge]      VARCHAR (8)   NULL,
    [PostcodeAtDischarge]            VARCHAR (8)   NULL,
    [EpisodicPostcode]               VARCHAR (8)   NULL,
    [MortalityReviewStatusCode]      INT           NULL,
    [GlobalProviderSpellNo]          VARCHAR (50)  NULL,
    [GlobalEpisodeNo]                INT           NULL,
    [ResidenceCCGCode]               VARCHAR (10)  NULL,
    [Reportable]                     BIT           NULL,
    [CharlsonIndex]                  INT           NULL,
    CONSTRAINT [PK_APC_BaseEncounter] PRIMARY KEY NONCLUSTERED ([MergeEncounterRecno] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_BaseEncounter_1]
    ON [APC].[BaseEncounter]([EncounterRecno] ASC, [ContextCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseEncounter_1]
    ON [APC].[BaseEncounter]([ContextCode] ASC, [NHSNumber] ASC)
    INCLUDE([MergeEncounterRecno], [EncounterRecno], [SourceUniqueID]);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseEncounter_2]
    ON [APC].[BaseEncounter]([SourcePatientNo] ASC, [AdmissionTime] ASC, [FirstEpisodeInSpellIndicator] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseEncounter_3]
    ON [APC].[BaseEncounter]([ContextCode] ASC, [ProviderSpellNo] ASC, [SourceEncounterNo] ASC, [PrimaryDiagnosisCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_BaseEncounter-CC]
    ON [APC].[BaseEncounter]([ContextCode] ASC)
    INCLUDE([MergeEncounterRecno], [SourceSpellNo], [SourceEncounterNo], [DistrictNo], [DischargeTime]);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseEncounter-ASC-CC]
    ON [APC].[BaseEncounter]([AdmissionSourceCode] ASC, [ContextCode] ASC)
    INCLUDE([AdmissionTime], [DistrictNo], [MergeEncounterRecno]);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BaseEncounter-GEN]
    ON [APC].[BaseEncounter]([GlobalEpisodeNo] ASC)
    INCLUDE([Ambulatory], [ContextCode], [EpisodeEndTime], [EpisodeStartTime], [MergeEncounterRecno], [PatientCategoryCode], [PrimaryProcedureCode], [StartDirectorateCode]);


GO
CREATE NONCLUSTERED INDEX [APC_BaseEncounter_4]
    ON [APC].[BaseEncounter]([GlobalProviderSpellNo] ASC, [GlobalEpisodeNo] ASC);


GO
CREATE NONCLUSTERED INDEX [APC_BaseEncounter_1]
    ON [APC].[BaseEncounter]([ProviderSpellNo] ASC)
    INCLUDE([MergeEncounterRecno]);


GO
CREATE NONCLUSTERED INDEX [IDX_APC_BaseEncounter-spn_est_cc]
    ON [APC].[BaseEncounter]([SourcePatientNo] ASC, [EpisodeStartTime] ASC, [ContextCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APC_BE-rep_est]
    ON [APC].[BaseEncounter]([Reportable] ASC, [EpisodeStartTime] ASC)
    INCLUDE([MergeEncounterRecno], [EncounterRecno], [SourcePatientNo], [SourceSpellNo], [DateOfBirth], [EpisodeStartDate], [EpisodeEndDate], [StartWardTypeCode], [EndWardTypeCode], [ConsultantCode], [SpecialtyCode], [PrimaryDiagnosisCode], [SubsidiaryDiagnosisCode], [SecondaryDiagnosisCode1], [SecondaryDiagnosisCode2], [SecondaryDiagnosisCode3], [SecondaryDiagnosisCode4], [SecondaryDiagnosisCode5], [SecondaryDiagnosisCode6], [SecondaryDiagnosisCode7], [SecondaryDiagnosisCode8], [SecondaryDiagnosisCode9], [SecondaryDiagnosisCode10], [SecondaryDiagnosisCode11], [SecondaryDiagnosisCode12], [SecondaryDiagnosisCode13], [PrimaryProcedureCode], [PrimaryProcedureDate], [CasenoteNumber], [FirstEpisodeInSpellIndicator], [PatientCategoryCode], [ClinicalCodingCompleteDate], [ContextCode], [EddCreatedTime], ExpectedDateOfDischarge, [VTECategoryCode], [MortalityReviewStatusCode], [GlobalProviderSpellNo], [GlobalEpisodeNo]);


GO
CREATE NONCLUSTERED INDEX [ix_be_rep]
    ON [APC].[BaseEncounter]([Reportable] ASC)
    INCLUDE([MergeEncounterRecno], [EpisodeStartTime], [EpisodeEndTime], [CasenoteNumber], [NHSNumber], [PatientForename], [PatientSurname], [DateOfBirth]);


GO
CREATE NONCLUSTERED INDEX [IX_BE-EST]
    ON [APC].[BaseEncounter]([EpisodeStartTime] ASC)
    INCLUDE([MergeEncounterRecno], [EncounterRecno], [PatientTitle], [PatientForename], [PatientSurname], [DateOfBirth], [NHSNumber], [Postcode], [DateOnWaitingList], [RegisteredGpCode], [RegisteredGpPracticeCode], [PASHRGCode], [PrimaryDiagnosisCode], [PrimaryProcedureCode], [PurchaserCode], [EpisodeEndTime], [AdmissionTime], [DischargeTime], [RTTPathwayID], [PCTCode], [ReferredByCode], [ContextCode]);


GO
CREATE NONCLUSTERED INDEX [IX_BE-GEN]
    ON [APC].[BaseEncounter]([GlobalEpisodeNo] ASC)
    INCLUDE([MergeEncounterRecno], [PrimaryProcedureCode], [EpisodeStartTime], [EpisodeEndTime], [AdmissionTime], [DischargeTime], [StartDirectorateCode], [PatientCategoryCode], [ContextCode], [Ambulatory]);


GO
CREATE NONCLUSTERED INDEX [IX_BE-ESDEEE]
    ON [APC].[BaseEncounter]([EpisodeStartDate] ASC, [EpisodeEndDate] ASC)
    INCLUDE([SourceEncounterNo], [ProviderSpellNo], [ContextCode]);


GO
CREATE NONCLUSTERED INDEX [IX_bm_be_pspfdobmerest]
    ON [APC].[BaseEncounter]([PatientForename] ASC, [PatientSurname] ASC, [DateOfBirth] ASC, [MergeEncounterRecno] ASC, [EpisodeStartTime] ASC)
    INCLUDE([EpisodeEndTime]);


GO
CREATE NONCLUSTERED INDEX [IX_BE_SC-REP]
    ON [APC].[BaseEncounter]([SiteCode] ASC, [Reportable] ASC)
    INCLUDE([MergeEncounterRecno], [SourcePatientNo], [SourceSpellNo], [DateOfBirth], [EpisodeStartTime], [EpisodeEndTime], [GlobalProviderSpellNo]);


GO
CREATE NONCLUSTERED INDEX [IX_BE_GEN]
    ON [APC].[BaseEncounter]([GlobalEpisodeNo] ASC)
    INCLUDE([MergeEncounterRecno], [AdmissionTime], [GlobalProviderSpellNo]);


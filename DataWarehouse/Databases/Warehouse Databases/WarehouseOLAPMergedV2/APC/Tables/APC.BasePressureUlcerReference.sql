﻿CREATE TABLE [APC].[BasePressureUlcerReference] (
    [MergePressureUlcerRecno] INT          NOT NULL,
    [ContextID]               INT          NULL,
    [IdentifiedDateID]        INT          NULL,
    [HealedDateID]            INT          NULL,
    [LocationID]              INT          NULL,
    [CategoryID]              INT          NULL,
    [WardID]                  INT          NULL,
    [Created]                 DATETIME     NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) NULL,
    CONSTRAINT [PK__BasePres__4A1B2EA059FBF52E] PRIMARY KEY CLUSTERED ([MergePressureUlcerRecno] ASC)
);


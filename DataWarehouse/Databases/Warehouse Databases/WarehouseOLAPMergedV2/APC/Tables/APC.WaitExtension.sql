﻿CREATE TABLE [APC].[WaitExtension] (
    [EncounterRecNo]                INT           NOT NULL,
    [ActivitySpecialty]             VARCHAR (100) NULL,
    [ActivityNationalSpecialtyCode] VARCHAR (20)  NULL,
    [ActivityAdmissionTypeCode]     VARCHAR (20)  NULL,
    [DiagnosticReportLineCode]      VARCHAR (3)   NULL,
    [DiagnosticNationalExamCode]    VARCHAR (3)   NULL
);


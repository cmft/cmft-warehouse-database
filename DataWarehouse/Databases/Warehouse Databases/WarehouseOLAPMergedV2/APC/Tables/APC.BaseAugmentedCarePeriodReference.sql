﻿CREATE TABLE [APC].[BaseAugmentedCarePeriodReference] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [ConsultantID]        INT           NULL,
    [SpecialtyID]         INT           NULL,
    [StartDateID]         INT           NULL,
    [EndDateID]           INT           NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_APC_Base_AugmentedCarePeriod_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


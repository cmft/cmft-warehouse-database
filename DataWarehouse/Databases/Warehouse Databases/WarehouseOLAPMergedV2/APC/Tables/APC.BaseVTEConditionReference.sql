﻿CREATE TABLE [APC].[BaseVTEConditionReference] (
    [MergeVTEConditionRecno] INT          NOT NULL,
    [ContextID]              INT          NULL,
    [DiagnosisDateID]        INT          NULL,
    [MedicationStartDateID]  INT          NULL,
    [VTETypeID]              INT          NULL,
    [WardID]                 INT          NULL,
    [Created]                DATETIME     NULL,
    [Updated]                DATETIME     NULL,
    [ByWhom]                 VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseVTEC__15A9D0C82FD0B138] PRIMARY KEY CLUSTERED ([MergeVTEConditionRecno] ASC)
);


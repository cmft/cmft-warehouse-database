﻿CREATE TABLE [APC].[DuplicateEpisodeValidation] (
    [SourceUniqueID]    VARCHAR (50) NOT NULL,
    [ProviderSpellno]   VARCHAR (50) NOT NULL,
    [ValidationOutcome] INT          NOT NULL,
    CONSTRAINT [PK_APC_DuplicateValidation] PRIMARY KEY NONCLUSTERED ([SourceUniqueID] ASC),
    CONSTRAINT [FK_APC_DuplicateEpisodeOutcome] FOREIGN KEY ([ValidationOutcome]) REFERENCES [APC].[DuplicateEpisodeValidationOutcome] ([Reportable])
);


﻿CREATE TABLE [APC].[BasePressureUlcer] (
    [MergePressureUlcerRecno] INT          IDENTITY (1, 1) NOT NULL,
    [PressureUlcerRecno]      INT          NOT NULL,
    [SourceUniqueID]          INT          NOT NULL,
    [SourcePatientNo]         VARCHAR (8)  NULL,
    [SourceSpellNo]           VARCHAR (5)  NULL,
    [LocationID]              INT          NULL,
    [IdentifiedDate]          DATE         NULL,
    [IdentifiedTime]          DATETIME     NULL,
    [HealedDate]              DATE         NULL,
    [HealedTime]              DATETIME     NULL,
    [CategoryID]              INT          NULL,
    [Validated]               BIT          NULL,
    [OccuredOnAnotherWard]    BIT          NULL,
    [WardCode]                VARCHAR (20) NOT NULL,
    [InterfaceCode]           VARCHAR (10) NULL,
    [ContextCode]             VARCHAR (11) NOT NULL,
    [Created]                 DATETIME     NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) NULL,
    CONSTRAINT [PK__BasePres__4A1B2EA0249408B6] PRIMARY KEY CLUSTERED ([MergePressureUlcerRecno] ASC)
);


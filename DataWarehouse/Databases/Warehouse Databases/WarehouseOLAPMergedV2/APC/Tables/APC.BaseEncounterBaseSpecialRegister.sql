﻿CREATE TABLE [APC].[BaseEncounterBaseSpecialRegister] (
    [MergeEncounterRecno]       INT NOT NULL,
    [MergeSpecialRegisterRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseSpecialRegister] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeSpecialRegisterRecno] ASC)
);


﻿CREATE TABLE [APC].[BedDaySpecialtyConsultantBase] (
    [SourceConsultantCode] VARCHAR (20) NOT NULL,
    [SourceSpecialtyCode]  VARCHAR (20) NOT NULL,
    [HRGCode]              VARCHAR (10) NOT NULL,
    [PODCode]              VARCHAR (10) NOT NULL,
    [ContextCode]          VARCHAR (20) NOT NULL,
    [FromDate]             DATE         CONSTRAINT [DF__BedDaySpe__FromD__098B0CBF] DEFAULT ('1 jan 1900') NULL,
    [ToDate]               DATE         NULL,
    CONSTRAINT [PK_BedDaySpecialtyConsultantBase] PRIMARY KEY CLUSTERED ([SourceConsultantCode] ASC, [SourceSpecialtyCode] ASC, [ContextCode] ASC)
);


﻿CREATE TABLE [APC].[PSSSpell] (
    [MergeEncounterRecno]                INT           NOT NULL,
    [NationalProgrammeOfCareServiceCode] VARCHAR (50)  NULL,
    [ServiceLineCode]                    VARCHAR (50)  NULL,
    [Created]                            DATETIME      NULL,
    [ByWhom]                             VARCHAR (255) NULL,
    CONSTRAINT [PK_PSSSpell] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


﻿CREATE TABLE [APC].[BaseEncounterBaseLearningDisability] (
    [MergeEncounterRecno]          INT NOT NULL,
    [MergeLearningDisabilityRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseLearningDisability] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeLearningDisabilityRecno] ASC)
);


﻿CREATE TABLE [APC].[BaseEncounterBasePressureUlcer] (
    [MergeEncounterRecno]     INT NOT NULL,
    [MergePressureUlcerRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBasePressureUlcer] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergePressureUlcerRecno] ASC)
);


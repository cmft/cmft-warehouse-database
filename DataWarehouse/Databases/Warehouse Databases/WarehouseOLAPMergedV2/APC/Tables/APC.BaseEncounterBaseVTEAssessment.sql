﻿CREATE TABLE [APC].[BaseEncounterBaseVTEAssessment] (
    [MergeEncounterRecno]     INT NOT NULL,
    [MergeVTEAssessmentRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseVTEAssessment] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeVTEAssessmentRecno] ASC)
);


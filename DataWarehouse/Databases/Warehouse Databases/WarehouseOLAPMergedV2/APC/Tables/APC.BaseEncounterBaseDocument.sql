﻿CREATE TABLE [APC].[BaseEncounterBaseDocument] (
    [MergeEncounterRecno] INT NOT NULL,
    [MergeDocumentRecno]  INT NOT NULL,
    [SequenceNo]          INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseDocument] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeDocumentRecno] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [APC].[BaseEncounterProcedureDetail] (
    [MergeEncounterRecno]           INT          NOT NULL,
    [EncounterRecno]                INT          NOT NULL,
    [OperationDetailSourceUniqueID] NUMERIC (9)  NOT NULL,
    [MergeOperationDetailRecno]     INT          NOT NULL,
    [ProcedureDetailSourceUniqueID] NUMERIC (9)  NOT NULL,
    [MergeProcedureDetailRecno]     INT          NOT NULL,
    [EncounterContextCode]          VARCHAR (10) NOT NULL,
    [ProcedureDetailContextCode]    VARCHAR (10) NOT NULL,
    [OperationStartDate]            DATETIME     NULL,
    CONSTRAINT [PK_BaseEncounterProcedureDetail] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [EncounterRecno] ASC, [OperationDetailSourceUniqueID] ASC, [MergeOperationDetailRecno] ASC, [ProcedureDetailSourceUniqueID] ASC, [MergeProcedureDetailRecno] ASC, [EncounterContextCode] ASC, [ProcedureDetailContextCode] ASC)
);


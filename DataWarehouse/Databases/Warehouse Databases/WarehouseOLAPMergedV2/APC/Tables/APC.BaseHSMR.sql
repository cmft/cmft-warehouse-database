﻿CREATE TABLE [APC].[BaseHSMR](
	[MergeHSMRRecno] [int] IDENTITY(1,1) NOT NULL,
	[HSMRRecno] [int] NULL,
	[SourceUniqueID] [varchar](20) NOT NULL,
	[DistrictNo] [varchar](20) NULL,
	[AdmissionDate] [date] NULL,
	[DischargeDate] [date] NULL,
	[ConsultantCode] [varchar](20) NULL,
	[RiskScore] [float] NULL,
	[Died] [bit] NULL,
	[InterfaceCode] [varchar](20) NULL,
	[ContextCode] [varchar](12) NOT NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NULL
) ON [PRIMARY]

GO

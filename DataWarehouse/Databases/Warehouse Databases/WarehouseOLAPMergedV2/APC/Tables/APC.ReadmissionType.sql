﻿CREATE TABLE [APC].[ReadmissionType] (
    [ReadmissionTypeID] INT          NOT NULL,
    [ReadmissionType]   VARCHAR (50) NULL,
    CONSTRAINT [PK_ReadmissionType] PRIMARY KEY CLUSTERED ([ReadmissionTypeID] ASC)
);


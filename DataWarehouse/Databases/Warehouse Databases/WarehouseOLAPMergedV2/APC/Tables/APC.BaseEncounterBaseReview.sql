﻿CREATE TABLE [APC].[BaseEncounterBaseReview] (
    [MergeEncounterRecno] INT NOT NULL,
    [MergeReviewRecno]    INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseReview] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeReviewRecno] ASC)
);


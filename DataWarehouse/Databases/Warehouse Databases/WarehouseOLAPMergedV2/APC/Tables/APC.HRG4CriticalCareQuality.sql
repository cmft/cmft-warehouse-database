﻿CREATE TABLE [APC].[HRG4CriticalCareQuality] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [QualityTypeCode]     VARCHAR (50)  NULL,
    [QualityCode]         VARCHAR (50)  NULL,
    [QualityMessage]      VARCHAR (255) NULL,
    [Created]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_HRG4CriticalCareQuality] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


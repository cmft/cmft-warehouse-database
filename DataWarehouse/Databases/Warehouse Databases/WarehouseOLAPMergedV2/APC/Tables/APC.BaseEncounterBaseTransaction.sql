﻿CREATE TABLE [APC].[BaseEncounterBaseTransaction] (
    [MergeEncounterRecno]   INT NOT NULL,
    [MergeTransactionRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseTransaction] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeTransactionRecno] ASC)
);


﻿CREATE TABLE [APC].[BaseOperation] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [OperationCode]       VARCHAR (10)  NOT NULL,
    [OperationDate]       SMALLDATETIME NULL,
    [Created]             DATETIME      NOT NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseOperation] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BaseOperation_OCode]
    ON [APC].[BaseOperation]([OperationCode] ASC);


﻿CREATE TABLE [APC].[BedDaySpecialtyBase] (
    [SourceSpecialtyCode] VARCHAR (20) NOT NULL,
    [HRGCode]             VARCHAR (10) NOT NULL,
    [PODCode]             VARCHAR (10) NOT NULL,
    [ContextCode]         VARCHAR (20) NOT NULL,
    [FromDate]            DATE         CONSTRAINT [DF__BedDaySpe__FromD__03D23369] DEFAULT ('1 jan 1900') NULL,
    [ToDate]              DATE         NULL,
    CONSTRAINT [PK_BedDaySpecialtyBase] PRIMARY KEY CLUSTERED ([SourceSpecialtyCode] ASC, [ContextCode] ASC)
);


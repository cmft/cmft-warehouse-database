﻿CREATE TABLE [APC].[BaseUrinaryTractInfectionReference] (
    [MergeUrinaryTractInfectionRecno] INT          NOT NULL,
    [ContextID]                       INT          NULL,
    [SymptomStartDateID]              INT          NULL,
    [SymptomEndDateID]                INT          NULL,
    [UrinaryTestRequestedDateID]      INT          NULL,
    [UrinaryTestReceivedDateID]       INT          NULL,
    [TreatmentStartDateID]            INT          NULL,
    [TreatmentEndDateID]              INT          NULL,
    [CatheterInsertedDateID]          INT          NULL,
    [CatheterRemovedDateID]           INT          NULL,
    [CatheterTypeID]                  INT          NULL,
    [WardID]                          INT          NULL,
    [Created]                         DATETIME     NULL,
    [Updated]                         DATETIME     NULL,
    [ByWhom]                          VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseUrin__CBEF8D3160A8F2BD] PRIMARY KEY CLUSTERED ([MergeUrinaryTractInfectionRecno] ASC)
);


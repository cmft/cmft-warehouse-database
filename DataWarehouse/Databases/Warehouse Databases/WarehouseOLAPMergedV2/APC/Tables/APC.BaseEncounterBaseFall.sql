﻿CREATE TABLE [APC].[BaseEncounterBaseFall] (
    [MergeEncounterRecno] INT NOT NULL,
    [MergeFallRecno]      INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseFall] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeFallRecno] ASC)
);


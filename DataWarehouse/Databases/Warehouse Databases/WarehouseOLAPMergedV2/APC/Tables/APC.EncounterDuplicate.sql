﻿CREATE TABLE [APC].[EncounterDuplicate] (
    [SourceContextID]    INT NOT NULL,
    [SourceRecno]        INT NOT NULL,
    [DuplicateContextID] INT NOT NULL,
    [DuplicateRecno]     INT NOT NULL,
    CONSTRAINT [PK_EncounterDuplicate] PRIMARY KEY CLUSTERED ([SourceContextID] ASC, [SourceRecno] ASC, [DuplicateContextID] ASC, [DuplicateRecno] ASC)
);


﻿CREATE TABLE [APC].[Readmission] (
    [ReadmissionRecno]      INT NOT NULL,
    [InitialAdmissionRecno] INT NOT NULL,
    [TrustWide]             BIT NOT NULL,
    CONSTRAINT [PK_Readmission] PRIMARY KEY CLUSTERED ([ReadmissionRecno] ASC, [InitialAdmissionRecno] ASC, [TrustWide] ASC)
);


﻿CREATE TABLE [APC].[BaseWardStayReference] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [SiteID]              INT           NULL,
    [WardID]              INT           NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_APC_Base_WardStay_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


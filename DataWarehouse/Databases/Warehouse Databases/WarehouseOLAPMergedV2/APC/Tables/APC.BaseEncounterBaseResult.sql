﻿CREATE TABLE [APC].[BaseEncounterBaseResult] (
    [MergeEncounterRecno] INT NOT NULL,
    [MergeResultRecno]    INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseResult] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeResultRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_APCBaseEncounterBaseResultMergeResultRecno]
    ON [APC].[BaseEncounterBaseResult]([MergeResultRecno] ASC);


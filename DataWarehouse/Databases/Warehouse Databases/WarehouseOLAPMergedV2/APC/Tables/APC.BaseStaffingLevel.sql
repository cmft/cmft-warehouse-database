﻿CREATE TABLE [APC].[BaseStaffingLevel] (
    [MergeStaffingLevelRecno]  INT           IDENTITY (1, 1) NOT NULL,
    [StaffingLevelRecno]       INT           NULL,
    [DivisionCode]             VARCHAR (20)  NULL,
    [DirectorateCode]          VARCHAR (20)  NULL,
    [WardCode]                 VARCHAR (20)  NULL,
    [CensusDate]               DATE          NULL,
    [ShiftID]                  INT           NULL,
    [RegisteredNursePlan]      INT           NULL,
    [RegisteredNurseActual]    INT           NULL,
    [NonRegisteredNursePlan]   INT           NULL,
    [NonRegisteredNurseActual] INT           NULL,
    [Comments]                 VARCHAR (MAX) NULL,
    [SeniorComments]           VARCHAR (MAX) NULL,
    [InterfaceCode]            VARCHAR (20)  NULL,
    [ContextCode]              VARCHAR (11)  NOT NULL,
    [Created]                  DATETIME      NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (50)  NULL,
    CONSTRAINT [PK__BaseStaf__A53A2A6A4555DE03] PRIMARY KEY CLUSTERED ([MergeStaffingLevelRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [APCBaseStaffingLevel_StaffingLevelRecno_DirectorateCode]
    ON [APC].[BaseStaffingLevel]([StaffingLevelRecno] ASC)
    INCLUDE([DirectorateCode]);


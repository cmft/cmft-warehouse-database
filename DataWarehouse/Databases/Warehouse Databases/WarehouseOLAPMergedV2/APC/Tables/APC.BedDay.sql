﻿CREATE TABLE [APC].[BedDay] (
    [MergeAPCEncounterRecno]     INT          NOT NULL,
    [MergeDatasetEncounterRecno] INT          NOT NULL,
    [DatasetCode]                VARCHAR (20) NULL,
    [PODCode]                    VARCHAR (50) NULL,
    [HRGCode]                    VARCHAR (20) NULL,
    [CensusDate]                 DATE         NOT NULL,
    CONSTRAINT [PK_BedDay] PRIMARY KEY CLUSTERED ([MergeAPCEncounterRecno] ASC, [MergeDatasetEncounterRecno] ASC, [CensusDate] ASC)
);


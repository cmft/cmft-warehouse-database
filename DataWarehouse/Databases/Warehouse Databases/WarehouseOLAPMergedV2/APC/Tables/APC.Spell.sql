﻿CREATE TABLE [APC].[Spell] (
    [AdmissionEpisodeMergeRecno] INT NOT NULL,
    [DischargeEpisodeMergeRecno] INT NULL,
    CONSTRAINT [PK_Spell] PRIMARY KEY CLUSTERED ([AdmissionEpisodeMergeRecno] ASC)
);


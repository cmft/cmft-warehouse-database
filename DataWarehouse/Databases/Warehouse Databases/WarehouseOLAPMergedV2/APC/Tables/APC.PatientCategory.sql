﻿CREATE TABLE [APC].[PatientCategory] (
    [PatientCategoryID]   INT          NOT NULL,
    [PatientCategoryCode] VARCHAR (2)  NULL,
    [PatientCategory]     VARCHAR (50) NULL,
    CONSTRAINT [PK_PatientCategory] PRIMARY KEY CLUSTERED ([PatientCategoryID] ASC)
);


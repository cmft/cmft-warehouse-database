﻿CREATE TABLE [APC].[BaseEncounterBaseTranexamicAcid] (
    [MergeEncounterRecno]      INT NOT NULL,
    [MergeTranexamicAcidRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseTranexamicAcid] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeTranexamicAcidRecno] ASC)
);


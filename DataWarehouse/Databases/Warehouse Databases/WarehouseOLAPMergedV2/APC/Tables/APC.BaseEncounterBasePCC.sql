﻿CREATE TABLE [APC].[BaseEncounterBasePCC] (
    [MergeEncounterRecno] INT NOT NULL,
    [PeriodRecno]         INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBasePCC] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [PeriodRecno] ASC)
);


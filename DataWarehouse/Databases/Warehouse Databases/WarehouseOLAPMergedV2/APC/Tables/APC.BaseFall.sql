﻿CREATE TABLE [APC].[BaseFall] (
    [MergeFallRecno]       INT          IDENTITY (1, 1) NOT NULL,
    [FallRecno]            INT          NULL,
    [SourceUniqueID]       INT          NOT NULL,
    [SourcePatientNo]      VARCHAR (8)  NULL,
    [SourceSpellNo]        VARCHAR (5)  NULL,
    [FallDate]             DATE         NULL,
    [FallTime]             DATETIME     NULL,
    [InitialSeverityID]    INT          NULL,
    [Validated]            BIT          NULL,
    [OccuredOnAnotherWard] BIT          NULL,
    [WardCode]             VARCHAR (10) NOT NULL,
    [InterfaceCode]        VARCHAR (10) NOT NULL,
    [ContextCode]          VARCHAR (11) NOT NULL,
    [Created]              DATETIME     NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseFall__C8F71D5220C377D2] PRIMARY KEY CLUSTERED ([MergeFallRecno] ASC)
);


﻿CREATE TABLE [APC].[BaseVTEAssessmentReference] (
    [MergeVTEAssessmentRecno]              INT          NOT NULL,
    [ContextID]                            INT          NULL,
    [AssessmentDateID]                     INT          NULL,
    [PreventativeMedicationStartDateID]    INT          NULL,
    [PreventativeNursingActionStartDateID] INT          NULL,
    [NoAssessmentReasonID]                 INT          NULL,
    [WardID]                               INT          NULL,
    [Created]                              DATETIME     NULL,
    [Updated]                              DATETIME     NULL,
    [ByWhom]                               VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseVTEA__8DCCA9F16E02EDDB] PRIMARY KEY CLUSTERED ([MergeVTEAssessmentRecno] ASC)
);


﻿CREATE TABLE [APC].[VTECategory] (
    [VTECategoryID]   INT          IDENTITY (1, 1) NOT NULL,
    [VTECategoryCode] VARCHAR (10) NOT NULL,
    [VTECategory]     VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_VTECategory] PRIMARY KEY CLUSTERED ([VTECategoryID] ASC)
);


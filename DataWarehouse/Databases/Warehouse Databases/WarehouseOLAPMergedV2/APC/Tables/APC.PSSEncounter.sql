﻿CREATE TABLE [APC].[PSSEncounter] (
    [MergeEncounterRecno]                INT           NOT NULL,
    [NationalProgrammeOfCareServiceCode] VARCHAR (50)  NULL,
    [ServiceLineCode]                    VARCHAR (50)  NULL,
    [ServiceLineCodeList]                VARCHAR (50)  NULL,
    [Created]                            DATETIME      NULL,
    [ByWhom]                             VARCHAR (255) NULL,
    CONSTRAINT [PK_PSSEncounter] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


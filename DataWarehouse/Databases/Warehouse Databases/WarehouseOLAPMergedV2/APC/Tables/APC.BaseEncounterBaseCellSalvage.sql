﻿CREATE TABLE [APC].[BaseEncounterBaseCellSalvage] (
    [MergeEncounterRecno]   INT NOT NULL,
    [MergeCellSalvageRecno] INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBaseCellSalvage] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeCellSalvageRecno] ASC)
);


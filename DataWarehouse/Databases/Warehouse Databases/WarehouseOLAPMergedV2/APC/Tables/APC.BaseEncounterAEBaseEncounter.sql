﻿CREATE TABLE [APC].[BaseEncounterAEBaseEncounter] (
    [AEMergeEncounterRecno]  INT          NOT NULL,
    [APCMergeEncounterRecno] INT          NOT NULL,
    [APCSourcePatientNo]     INT          NOT NULL,
    [APCEpisodeNo]           VARCHAR (20) NULL,
    CONSTRAINT [PK_BaseEncounterBaseAEEncounter] PRIMARY KEY CLUSTERED ([APCMergeEncounterRecno] ASC, [AEMergeEncounterRecno] ASC)
);


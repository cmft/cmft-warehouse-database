﻿CREATE TABLE [APC].[BaseLearningDisabilityReference] (
    [MergeLearningDisabilityRecno] INT          NOT NULL,
    [ContextID]                    INT          NULL,
    [AssessmentDateID]             INT          NULL,
    [WardID]                       INT          NULL,
    [Created]                      DATETIME     NULL,
    [Updated]                      DATETIME     NULL,
    [ByWhom]                       VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseLear__DD317BE772736894] PRIMARY KEY CLUSTERED ([MergeLearningDisabilityRecno] ASC)
);


﻿CREATE TABLE [APC].[BaseEncounterOperationDetail] (
    [MergeEncounterRecno]           INT          NOT NULL,
    [OperationDetailSourceUniqueID] NUMERIC (9)  NOT NULL,
    [MergeOperationDetailRecno]     INT          NOT NULL,
    [EncounterContextCode]          VARCHAR (10) NOT NULL,
    [ProcedureDetailContextCode]    VARCHAR (10) NOT NULL,
    [SequenceNo]                    INT          NOT NULL,
    CONSTRAINT [PK_BaseEncounterOperationDetail] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [OperationDetailSourceUniqueID] ASC, [MergeOperationDetailRecno] ASC, [EncounterContextCode] ASC, [ProcedureDetailContextCode] ASC)
);


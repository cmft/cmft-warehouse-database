﻿CREATE TABLE [APC].[BaseLearningDisability] (
    [MergeLearningDisabilityRecno]  INT          IDENTITY (1, 1) NOT NULL,
    [LearningDisabilityRecno]       INT          NULL,
    [SourceUniqueID]                INT          NOT NULL,
    [SourcePatientNo]               VARCHAR (8)  NULL,
    [SourceSpellNo]                 VARCHAR (5)  NULL,
    [AssessmentDate]                DATE         NULL,
    [AssessmentTime]                DATETIME     NULL,
    [KnownLearningDisability]       BIT          NULL,
    [ReasonableAdjustmentsCarePlan] BIT          NULL,
    [LDPassport]                    BIT          NULL,
    [WardCode]                      VARCHAR (10) NOT NULL,
    [InterfaceCode]                 VARCHAR (10) NOT NULL,
    [ContextCode]                   VARCHAR (11) NOT NULL,
    [Created]                       DATETIME     NULL,
    [Updated]                       DATETIME     NULL,
    [ByWhom]                        VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseLear__DD317BE76DAEB377] PRIMARY KEY CLUSTERED ([MergeLearningDisabilityRecno] ASC)
);


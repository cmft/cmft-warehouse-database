﻿
CREATE proc [Observation].[BuildReportReference] as 

truncate table Observation.ReportReference

insert Observation.ReportReference
(
ReferenceID
,Reference
,ReportReference
,ReferenceTypeCode
,ContextCode
)


select
	ReferenceID 
	,Reference 
	,ReportReference
	,ReferenceTypeCode 
	,ContextCode 

from
	(
	select
		 ReferenceID = SourceValueID
		,Reference = SourceValue
		,ReportReference = LocalValue
		,ReferenceTypeCode = AttributeCode
		,ContextCode = SourceContextCode
	from
		WH.Member AlertSeverity
	where
		AttributeCode = 'ALRTSVR'

	union

	select
		 ReferenceID = SourceValueID
		,Reference = SourceValue
		,ReportReference = LocalValue
		,ReferenceTypeCode = AttributeCode
		,ContextCode = SourceContextCode
	from
		WH.Member ObservationStatus
	where
		AttributeCode = 'OBSSTATUS'

	union

	select
		 ReferenceID = SourceValueID
		,Reference = SourceValue
		,ReportReference = LocalValue
		,ReferenceTypeCode = AttributeCode
		,ContextCode = SourceContextCode
	from
		WH.Member AssessedStatus
	where
		AttributeCode = 'ASSESSEDSTATUS'

	union

	select
		 ReferenceID = DurationID
		,Reference = AlertDurationBand
		,ReportReference = AlertDurationBand
		,ReferenceTypeCode = 'ALDUR'
		,ContextCode = 'PTRACK' 
	from
		Observation.Duration

	union

	select
		 ReferenceID = DurationID
		,Reference = AlertChainDurationBand
		,ReportReference = AlertChainDurationBand
		,ReferenceTypeCode = 'CHDUR'
		,ContextCode = 'PTRACK' 
	from
		Observation.Duration

	union

	select
		 ReferenceID = SourceValueID
		,Reference = SourceValue
		,ReportReference = SourceValue
		,ReferenceTypeCode = AttributeCode
		,ContextCode = SourceContextCode
	from
		WH.Member AssessedStatus
	where
		AttributeCode = 'CALLOUTREASON'

	) ReportReference
﻿
CREATE procedure [Observation].[BuildFactObservationSet] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@RowsInserted int


select @StartTime = getdate()

truncate table Observation.FactObservationSet

insert Observation.FactObservationSet
(
MergeObservationSetRecno
,SpecialtyID
,LocationID
,ObservationNotTakenReasonID
,StartDateID
,StartTimeOfDayCode
,TakenDateID
,TakenTimeOfDayCode
,OverallRiskIndexID
,OverallAssessedStatusID
,AlertSeverityID
,DueTimeStatusID
,FirstObservationSet
,LatestObservationSet
,Discharged
,OutOfHours
,AgeID
,ContextID
,Cases
,AdmissionToObservationSetMinutes
,LocationStartToObservationSetMinutes
)


select --top 10 
	BaseObservationSet.MergeObservationSetRecno
	,SpecialtyID = BaseObservationSetReference.SpecialtyID
	,LocationID = BaseObservationSetReference.LocationID
	,ObservationNotTakenReasonID = BaseObservationSetReference.ObservationNotTakenReasonID
	--,ClinicianPresentSeniorityID = BaseObservationSetReference.ClinicianPresentSeniorityID
	,StartDateID = StartDateID
	,StartTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseObservationSet.StartTime), 0)
							,BaseObservationSet.StartTime
							)
						,-1
						)
	,TakenDateID = TakenDateID
	,TakenTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseObservationSet.TakenTime), 0)
							,BaseObservationSet.TakenTime
							)
						,-1
						)
	,OverallRiskIndexID = BaseObservationSetReference.OverallRiskIndexID
	,OverallAssessedStatusID = BaseObservationSetReference.OverallAssessedStatusID		
	,AlertSeverityID = BaseObservationSetReference.AlertSeverityID
	,DueTimeStatusID = BaseObservationSetReference.DueTimeStatusID
	,FirstObservationSet = 
				case
				when not exists
						(
						select
							1
						from
							Observation.BaseObservationSet ObservationSetEarlier
						where
							ObservationSetEarlier.AdmissionSourceUniqueID = BaseObservationSet.AdmissionSourceUniqueID
						and ObservationSetEarlier.SourceUniqueID < BaseObservationSet.SourceUniqueID
						)
				then 1
				else 0
				end
	,LatestObservationSet = 
				case
				when not exists
						(
						select
							1
						from
							Observation.BaseObservationSet ObservationSetLater
						where
							ObservationSetLater.AdmissionSourceUniqueID = BaseObservationSet.AdmissionSourceUniqueID
						and ObservationSetLater.SourceUniqueID > BaseObservationSet.SourceUniqueID
						)
				then 1
				else 0
				end
	,Discharged = 
			case
			when BaseAdmission.DischargeTime is not null
			then 1
			else 0
			end
	,OutOfHours = 
			case
			when datename(dw, BaseObservationSet.StartTime) in ('Saturday', 'Sunday') then 1
			when cast(BaseObservationSet.StartTime as time) < '8:00' then 1
			when cast(BaseObservationSet.StartTime as time) > '17:00' then 1
			when exists
				(
				select
					1
				from
					[$(Warehouse)].WH.Holiday
				where
					StartDate = Holiday.HolidayDate
				)
			then 1
			else 0
			end
	,AgeID = 
			case
			when BaseObservationSet.DateOfBirth > BaseObservationSet.StartTime
			then 0
			when (month(BaseObservationSet.StartTime) * 100) + day(BaseObservationSet.StartTime) >= (month(BaseObservationSet.DateOfBirth) * 100) + day(BaseObservationSet.DateOfBirth)
			then datediff(year, BaseObservationSet.DateOfBirth, BaseObservationSet.StartTime)
			else datediff(year, BaseObservationSet.DateOfBirth, BaseObservationSet.StartTime) - 1
			end

	,ContextID = BaseObservationSetReference.ContextID
	,Cases = 1
	,AdmissionToObservationSetMinutes = datediff(minute, BaseAdmission.AdmissionTime, coalesce(BaseObservationSet.StartTime, getdate()))
	,LocationStartToObservationSetMinutes = datediff(minute, BaseObservationSet.LocationStartTime, coalesce(BaseObservationSet.StartTime, getdate()))
from
	Observation.BaseObservationSet

inner join Observation.BaseObservationSetReference
on	BaseObservationSet.MergeObservationSetRecno = BaseObservationSetReference.MergeObservationSetRecno

inner join Observation.BaseAdmission
on	BaseObservationSet.AdmissionSourceUniqueID = BaseAdmission.SourceUniqueID


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @ProcedureName = name from sysobjects where id = @@procid

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	

﻿
CREATE procedure [Observation].[BuildFactAlert] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@RowsInserted int


select @StartTime = getdate()

truncate table Observation.FactAlert

insert Observation.FactAlert
(
MergeAlertRecno
,SpecialtyID
,LocationID
,TypeID
,ReasonID
,CreatedDateID
,CreatedTimeOfDayCode
,AttendedByUserID
,SeverityID
,ClosedDateID
,ClosedTimeOfDayCode
,ClosureReasonID
,ClosedByUserID
,DurationID
,Discharged
,OutOfHours
,AgeID
,ContextID
,Cases
,AlertMinutes
)


select
	BaseAlert.MergeAlertRecno
	,SpecialtyID = BaseAlertReference.SpecialtyID
	,LocationID = BaseAlertReference.LocationID
	,TypeID	= BaseAlertReference.TypeID
	,ReasonID = BaseAlertReference.ReasonID	
	,CreatedDateID = BaseAlertReference.CreatedDateID
	,CreatedTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseAlert.CreatedTime), 0)
							,BaseAlert.CreatedTime
						)
						,-1
					)
	--,BedsideDueDateID = BaseAlertReference.BedsideDueDateID
	--,BedsideDueTimeOfDayCode =
	--				coalesce(
	--					datediff(
	--							minute
	--						,dateadd(day, datediff(day, 0, BaseAlert.BedsideDueTime), 0)
	--						,BaseAlert.BedsideDueTime
	--					)
	--					,-1
	--				)
	--,EscalationDateID = BaseAlertReference.EscalationDateID
	--,EscalationTimeOfDayCode =
	--				coalesce(
	--					datediff(
	--						 minute
	--						,dateadd(day, datediff(day, 0, BaseAlert.EscalationTime), 0)
	--						,BaseAlert.EscalationTime
	--					)
	--					,-1
	--				)
	--,AcceptedDateID = BaseAlertReference.AcceptedDateID
	--,AcceptedTimeOfDayCode =
	--				coalesce(
	--					datediff(
	--						 minute
	--						,dateadd(day, datediff(day, 0, BaseAlert.AcceptedTime), 0)
	--						,BaseAlert.AcceptedTime
	--					)
	--					,-1
	--				)
	,AttendedByUserID = BaseAlertReference.AttendedByUserID			
	--,AttendanceTypeID = BaseAlertReference.AttendanceTypeID
	,SeverityID = BaseAlertReference.SeverityID
	--,CurrentClinicianSeniorityID = BaseAlertReference.ClinicianSeniorityID
	,ClosedDateID = BaseAlertReference.ClosedDateID
	,ClosedTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseAlert.ClosedTime), 0)
							,BaseAlert.ClosedTime
						)
						,-1
					)
	,ClosureReasonID = BaseAlertReference.ClosureReasonID
	,ClosedByUserID = BaseAlertReference.ClosedByUserID
	,DurationID =
				coalesce(
					case 
						when datediff(minute,BaseAlert.CreatedTime, coalesce(BaseAlert.ClosedTime, getdate())) < 0 then -1
						when datediff(minute,BaseAlert.CreatedTime, coalesce(BaseAlert.ClosedTime, getdate())) > 1440 then 1440
						else datediff(minute,BaseAlert.CreatedTime, coalesce(BaseAlert.ClosedTime, getdate()))
					end
					, -2
					)
	,Discharged = 
				case
				when BaseAdmission.DischargeTime is not null
				then 1
				else 0
				end
	,OutOfHours = 
				case
				when datename(dw, BaseAlert.CreatedTime) in ('Saturday', 'Sunday') then 1
				when cast(BaseAlert.CreatedTime as time) < '8:00' then 1
				when cast(BaseAlert.CreatedTime as time) > '17:00' then 1
				when exists
					(
					select
						1
					from
						[$(Warehouse)].WH.Holiday
					where
						BaseAlert.CreatedDate = Holiday.HolidayDate
					)
				then 1
				else 0
				end
	,AgeID = 
				case
				when BaseAlert.DateOfBirth > BaseAlert.CreatedTime
				then 0
				when (month(BaseAlert.CreatedTime) * 100) + day(BaseAlert.CreatedTime) >= (month(BaseAlert.DateOfBirth) * 100) + day(BaseAlert.DateOfBirth)
				then datediff(year, BaseAlert.DateOfBirth, BaseAlert.CreatedTime)
				else datediff(year, BaseAlert.DateOfBirth, BaseAlert.CreatedTime) - 1
				end	
	,BaseAlertReference.ContextID
	,Cases = 1
	,AlertMinutes = datediff(minute,BaseAlert.CreatedTime, coalesce(BaseAlert.ClosedTime, getdate()))
from
	Observation.BaseAlert

inner join Observation.BaseAlertReference
on	BaseAlert.MergeAlertRecno = BaseAlertReference.MergeAlertRecno

inner join Observation.BaseAdmission
on	BaseAdmission.SourceUniqueID = BaseAlert.AdmissionSourceUniqueID


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @ProcedureName = name from sysobjects where id = @@procid

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	

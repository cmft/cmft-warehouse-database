﻿
create procedure Observation.BuildFactAlertChain as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@RowsInserted int


select @StartTime = getdate()

truncate table Observation.FactAlertChain

insert Observation.FactAlertChain
(
MergeObservationSetRecno
,SpecialtyID
,LocationID
,StartDateID
,StartTimeOfDayCode
,OverallRiskIndexID
,OverallAssessedStatusID
,DurationID
,Discharged
,OutOfHours
,AgeID
,ContextID
,Cases
,AlertChainMinutes
)

select
	 BaseObservationSet.MergeObservationSetRecno
	,SpecialtyID = BaseObservationSetReference.SpecialtyID
	,LocationID = BaseObservationSetReference.LocationID
	,StartDateID = BaseObservationSetReference.StartDateID
	,StartTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseObservationSet.StartTime), 0)
							,BaseObservationSet.StartTime
							)
						,-1
						)
	,OverallRiskIndexID = BaseObservationSetReference.OverallRiskIndexID
	,OverallAssessedStatusID = BaseObservationSetReference.OverallAssessedStatusID
	,DurationID = 
				coalesce(
						case 
						when AlertChainMinutes < 0 then -1
						when AlertChainMinutes > 1440 then 1440
						else AlertChainMinutes
						end
					, -2
					)
	,Discharged = 
			case
			when BaseAdmission.DischargeTime is not null
			then 1
			else 0
			end
	,OutOfHours = 
			case
			when datename(dw, BaseObservationSet.StartTime) in ('Saturday', 'Sunday') then 1
			when cast(BaseObservationSet.StartTime as time) < '8:00' then 1
			when cast(BaseObservationSet.StartTime as time) > '17:00' then 1
			when exists
				(
				select
					1
				from
					[$(Warehouse)].WH.Holiday
				where
					StartDate = Holiday.HolidayDate
				)
			then 1
			else 0
			end
	,AgeID = 
			case
			when BaseObservationSet.DateOfBirth > BaseObservationSet.StartTime
			then 0
			when (month(BaseObservationSet.StartTime) * 100) + day(BaseObservationSet.StartTime) >= (month(BaseObservationSet.DateOfBirth) * 100) + day(BaseObservationSet.DateOfBirth)
			then datediff(year, BaseObservationSet.DateOfBirth, BaseObservationSet.StartTime)
			else datediff(year, BaseObservationSet.DateOfBirth, BaseObservationSet.StartTime) - 1
			end
	,ContextID = BaseObservationSetReference.ContextID
	,Cases = 1
	,AlertChainMinutes
from
	Observation.BaseObservationSet

inner join Observation.BaseObservationSetReference
on	BaseObservationSetReference.MergeObservationSetRecno = BaseObservationSet.MergeObservationSetRecno

inner join Observation.BaseAdmission
on	BaseAdmission.SourceUniqueID = BaseObservationSet.AdmissionSourceUniqueID

where
	LastInAlertChain = 1



select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @ProcedureName = name from sysobjects where id = @@procid

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	

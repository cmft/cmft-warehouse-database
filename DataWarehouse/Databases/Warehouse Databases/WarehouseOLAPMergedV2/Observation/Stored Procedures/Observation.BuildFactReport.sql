﻿
CREATE proc [Observation].[BuildFactReport] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@RowsInserted int


select @StartTime = getdate()

truncate table Observation.FactReport

insert Observation.FactReport
(
MetricID
,ReportEncounterRecno
,SpecialtyID
,LocationID
,DateID
,TimeOfDayCode
,DimensionOneID
,DimensionTwoID
,OutOfHours
,Cases
)

select --top 1000
	MetricID
	,EncounterRecno
	,SpecialtyID
	,LocationID
	,DateID
	,TimeOfDayCode
	,DimensionOneID 
	,DimensionTwoID 
	,OutOfHours
	,Cases
from
	(
	select
		MetricID = 1 -- Admission
		,EncounterRecno = ReportEncounter.ReportEncounterRecno
		,SpecialtyID
		,LocationID
		,DateID
		,TimeOfDayCode
		,DimensionOneID = null
		,DimensionTwoID = null
		,OutOfHours
		,Cases
	from
		Observation.FactAdmission

	inner join Observation.ReportEncounter
	on	ReportEncounter.MergeRecno = FactAdmission.MergeAdmissionRecno
	and ReportEncounter.ReportEncounterTypeID = 3

	where
		MetricID = 1 -- Admission

	union

	select
		MetricID = 2 -- Alert
		,EncounterRecno = ReportEncounter.ReportEncounterRecno
		,SpecialtyID
		,LocationID
		,DateID = FactAlert.CreatedDateID
		,TimeOfDayCode = FactAlert.ClosedTimeOfDayCode	,DimensionOneID = AlertSeverity.ReportReferenceID
		,DimensionTwoID = AlertDuration.ReportReferenceID
		,OutOfHours
		,Cases
	from
		Observation.FactAlert

	inner join Observation.ReportReference AlertSeverity
	on	AlertSeverity.ReferenceID = FactAlert.SeverityID
	and	AlertSeverity.ReferenceTypeCode = 'ALRTSVR'

	inner join Observation.ReportReference AlertDuration
	on	AlertDuration.ReferenceID = FactAlert.DurationID
	and	AlertDuration.ReferenceTypeCode = 'ALDUR'

	inner join Observation.ReportEncounter
	on	ReportEncounter.MergeRecno = FactAlert.MergeAlertRecno
	and ReportEncounter.ReportEncounterTypeID = 1

	union

	select
		MetricID = 4 -- Observation Set
		,EncounterRecno = ReportEncounter.ReportEncounterRecno
		,SpecialtyID
		,LocationID
		,DateID = FactObservationSet.StartDateID
		,TimeOfDayCode = FactObservationSet.StartTimeOfDayCode	
		,DimensionOneID = ObservationStatus.ReportReferenceID
		,DimensionTwoID = AssessedStatus.ReportReferenceID
		,OutOfHours
		,Cases
	from
		Observation.FactObservationSet

	inner join Observation.ReportReference ObservationStatus
	on	ObservationStatus.ReferenceID = FactObservationSet.DueTimeStatusID
	and	ObservationStatus.ReferenceTypeCode = 'OBSSTATUS'

	inner join Observation.ReportReference AssessedStatus
	on	AssessedStatus.ReferenceID = FactObservationSet.OverallAssessedStatusID
	and	AssessedStatus.ReferenceTypeCode = 'ASSESSEDSTATUS'

	inner join Observation.ReportEncounter
	on	ReportEncounter.MergeRecno = FactObservationSet.MergeObservationSetRecno
	and ReportEncounter.ReportEncounterTypeID = 2

	where
		exists
		--only include observation sets which were carried out
			(
			select
				*
			from
				Observation.ObservationNotTakenReason
			where
				FactObservationSet.ObservationNotTakenReasonID = SourceObservationNotTakenReasonID
			and ObservationNotTakenReason.SourceObservationNotTakenReasonCode = '-1'
			)

	union

	select
		MetricID = 3 -- Alert Chain
		,EncounterRecno = ReportEncounter.ReportEncounterRecno
		,SpecialtyID
		,LocationID
		,DateID = FactAlertChain.StartDateID
		,TimeOfDayCode = FactAlertChain.StartTimeOfDayCode	
		,DimensionOneID = AssessedStatus.ReportReferenceID
		,DimensionTwoID = ChainDuration.ReportReferenceID
		,OutOfHours
		,Cases
	from
		Observation.FactAlertChain

	inner join Observation.ReportReference AssessedStatus
	on	AssessedStatus.ReferenceID = FactAlertChain.OverallAssessedStatusID
	and	AssessedStatus.ReferenceTypeCode = 'ASSESSEDSTATUS'

	inner join Observation.ReportReference ChainDuration
	on	ChainDuration.ReferenceID = FactAlertChain.DurationID
	and	ChainDuration.ReferenceTypeCode = 'CHDUR'

	inner join Observation.ReportEncounter
	on	ReportEncounter.MergeRecno = FactAlertChain.MergeObservationSetRecno
	and ReportEncounter.ReportEncounterTypeID = 2

	union

	select
		MetricID = 5 -- Resus
		,EncounterRecno = null
		,SpecialtyID = -- no specialty attribute for resus data. No unknown members in model!
			(
			select
				SourceSpecialtyID
			from
				WH.Specialty
			where
				SourceSpecialtyCode = '-1'
			and SourceContextCode = 'CEN||PTRACK'
			)
		,LocationID
		,DateID = FactEncounter.CallOutDateID
		,TimeOfDayCode = FactEncounter.CallOutTimeOfDayCode	
		,DimensionOneID = CallOutReason.ReportReferenceID
		,DimensionTwoID = null
		,OutOfHours
		,Cases
	from
		Resus.FactEncounter

	inner join Observation.ReportReference CallOutReason
	on	CallOutReason.ReferenceID = FactEncounter.CallOutReasonID
	and	CallOutReason.ReferenceTypeCode = 'CALLOUTREASON'

	) FactReport




select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @ProcedureName = name from sysobjects where id = @@procid

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	

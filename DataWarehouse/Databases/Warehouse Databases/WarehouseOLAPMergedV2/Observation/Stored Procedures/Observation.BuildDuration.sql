﻿
CREATE proc Observation.BuildDuration as 


truncate table Observation.Duration

declare @Minute int
		,@Extent int

set @Minute = 0
set @Extent = 1440;

with ObservationDuration
as
(
select 
	DurationID = @Minute
union all
select
	DurationID + 1
from
	ObservationDuration
where
	DurationID <= @Extent
)


insert into Observation.Duration

(
DurationID
,MinuteBand
,AlertDurationBand
,AlertChainDurationBand
)


select 
	DurationID = DurationID
	,MinuteBand = DurationID
	,AlertDurationBand = 
						case
							when DurationID >= 0 and DurationID < 60 then '00:00<01:00'
							when DurationID >= 60 and DurationID < 90 then '01:00<01:30'
							when DurationID >= 90 then '01:30+'
						end
	,AlertChainDurationBand = 
						case
							when DurationID >= 0 and DurationID < 180 then '00:00<03:00'
							when DurationID >= 180 and DurationID < 300 then '03:00<05:00'
							when DurationID >= 300 then '05:00+'
						end
from
	ObservationDuration

option (maxrecursion 0)


insert into Observation.Duration
(
DurationID
,MinuteBand
,AlertDurationBand
,AlertChainDurationBand
)

select	
	-1
	,-1
	,'<00:00'
	,'<00:00'
	
insert into Observation.Duration
(
DurationID
,MinuteBand
,AlertDurationBand
,AlertChainDurationBand
)
select	
	-2
	,-2
	,'N/A'
	,'N/A'










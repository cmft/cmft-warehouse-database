﻿
create procedure [Observation].[BuildFactObservationProfile] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@RowsInserted int


select @StartTime = getdate()

truncate table Observation.FactObservationProfile

insert Observation.FactObservationProfile
(
MergeObservationProfileRecno
,SpecialtyID
,LocationID
,ProfileTemplateID
,ProfileReasonID
,StartDateID
,StartTimeOfDayCode
,EndDateID
,EndTimeOfDayCode
,FirstObservationProfile
,LatestObservationProfile
,ObservationProfileDurationID
,ObservationProfileToDischargeDurationID
,Discharged
,OutOfHours
,AgeID
,ContextID
,Cases
,ObservationProfileDurationHours
,AdmissionToObservationProfileHours
,ObservationProfileToDischargeHours
)
select --top 10
	BaseObservationProfile.MergeObservationProfileRecno
	,SpecialtyID = BaseObservationProfileReference.SpecialtyID
	,LocationID = BaseObservationProfileReference.LocationID
	--,ProfileID = BaseObservationProfileReference.ProfileID	
	,ProfileTemplateID = BaseObservationProfileReference.ProfileTemplateID	
	--,TemplatePriorityID = BaseObservationProfileReference.TemplatePriorityID
	--,DrivingTableID = BaseObservationProfileReference.DrivingTableID
	,ProfileReasonID = BaseObservationProfileReference.ProfileReasonID
	,StartDateID = BaseObservationProfileReference.StartDateID
	,StartTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseObservationProfile.StartTime), 0)
							,BaseObservationProfile.StartTime
						)
						,-1
					)
	,EndDateID = BaseObservationProfileReference.EndDateID
	,EndTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseObservationProfile.EndTime), 0)
							,BaseObservationProfile.EndTime
						)
						,-1
					)
	,FirstObservationProfile = 
				case
				when not exists
						(
						select
							1
						from
							Observation.BaseObservationProfile ObservationProfileEarlier
						where
							ObservationProfileEarlier.AdmissionSourceUniqueID = BaseObservationProfile.AdmissionSourceUniqueID
						and ObservationProfileEarlier.SourceUniqueID < BaseObservationProfile.SourceUniqueID
						)
				then 1
				else 0
				end
	,LatestObservationProfile = 
				case
				when not exists
						(
						select
							1
						from
							Observation.BaseObservationProfile ObservationProfileLater
						where
							ObservationProfileLater.AdmissionSourceUniqueID = BaseObservationProfile.AdmissionSourceUniqueID
						and ObservationProfileLater.SourceUniqueID > BaseObservationProfile.SourceUniqueID
						)
				then 1
				else 0
				end
	,ObservationProfileDurationID = 
				coalesce(
						case 
						when datediff(hour, BaseObservationProfile.StartTime, coalesce(BaseObservationProfile.EndTime, getdate())) < 0 then -1
						else datediff(hour, BaseObservationProfile.StartTime, coalesce(BaseObservationProfile.EndTime, getdate()))
						end
					, -2
					)
	,ObservationProfileToDischargeDurationID = 
				coalesce(
						case 
						when datediff(hour, BaseObservationProfile.StartTime, coalesce(BaseAdmission.DischargeTime, getdate())) < 0 then -1
						else datediff(hour, BaseObservationProfile.StartTime, coalesce(BaseAdmission.DischargeTime, getdate()))
						end
					, -2
					)
	,Discharged = 
				case
				when BaseAdmission.DischargeTime is not null
				then 1
				else 0
				end
	,OutOfHours = 
				case
				when datename(dw, BaseObservationProfile.StartTime) in ('Saturday', 'Sunday') then 1
				when cast(BaseObservationProfile.StartTime as time) < '8:00' then 1
				when cast(BaseObservationProfile.StartTime as time) > '17:00' then 1
				when exists
					(
					select
						1
					from
						[$(Warehouse)].WH.Holiday
					where
						cast(StartTime as date) = Holiday.HolidayDate
					)
				then 1
				else 0
				end
	,AgeID = 
				case
				when BaseObservationProfile.DateOfBirth > StartTime
				then 0
				when (month(BaseObservationProfile.StartTime) * 100) + day(StartTime) >= (month(BaseObservationProfile.DateOfBirth) * 100) + day(BaseObservationProfile.DateOfBirth)
				then datediff(year, BaseObservationProfile.DateOfBirth, BaseObservationProfile.StartTime)
				else datediff(year, BaseObservationProfile.DateOfBirth, BaseObservationProfile.StartTime) - 1
				end
	,ContextID = BaseObservationProfileReference.ContextID
	,Cases = 1
	,ObservationProfileDurationHours = datediff(hour, BaseObservationProfile.StartTime, coalesce(BaseObservationProfile.EndTime, getdate()))
	,AdmissionToObservationProfileHours = datediff(hour, BaseAdmission.AdmissionTime, BaseObservationProfile.StartTime)
	,ObservationProfileToDischargeHours = datediff(hour, BaseObservationProfile.StartTime, coalesce(BaseAdmission.DischargeTime, getdate()))
from
	Observation.BaseObservationProfile

inner join Observation.BaseObservationProfileReference
on	BaseObservationProfileReference.MergeObservationProfileRecno = BaseObservationProfile.MergeObservationProfileRecno

inner join .Observation.BaseAdmission
on	BaseObservationProfile.AdmissionSourceUniqueID = BaseAdmission.SourceUniqueID


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @ProcedureName = name from sysobjects where id = @@procid

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	

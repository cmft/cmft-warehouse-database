﻿

CREATE proc Observation.[BuildAge] as

declare @Year int
		,@Extent int

set @Year = 0
set @Extent = 998;


truncate table Observation.Age;

with Age
as
(
select 
	AgeCode = @Year

union all

select
	AgeCode + 1
from
	Age
where
	AgeCode <= @Extent
)

insert into Observation.Age

(
AgeID
,Age
,AgeBand
)

select 
	AgeID = AgeCode
	,Age = AgeCode
	,AgeBand = 
			case
			when AgeCode >= 0 and AgeCode < 1 then '< 1 Year'
			when AgeCode >= 1 and AgeCode < 10 then '1 - 9 Years'
			when AgeCode >= 5 and AgeCode < 20 then '10 - 19 Years'
			when AgeCode >= 20 and AgeCode < 30 then '20 - 29 Years'
			when AgeCode >= 30 and AgeCode < 40 then '30 - 39 Years'
			when AgeCode >= 40 and AgeCode < 50 then '40 - 49 Years'
			when AgeCode >= 50 and AgeCode < 55 then '50 - 54 Years'
			when AgeCode >= 55 and AgeCode < 60 then '55 - 59 Years'
			when AgeCode >= 60 and AgeCode < 65 then '60 - 64 Years'
			when AgeCode >= 65 and AgeCode < 70 then '65 - 69 Years'
			when AgeCode >= 70 and AgeCode < 75 then '70 - 74 Years'
			when AgeCode >= 75 and AgeCode < 80 then '75 - 79 Years'
			when AgeCode >= 80 and AgeCode < 85 then '80 - 84 Years'
			when AgeCode >= 85 then '85 Years +'
			end
from
	Age

option (maxrecursion 0)

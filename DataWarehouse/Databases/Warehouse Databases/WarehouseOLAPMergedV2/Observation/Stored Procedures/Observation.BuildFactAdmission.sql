﻿
CREATE procedure [Observation].[BuildFactAdmission] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@RowsInserted int


select @StartTime = getdate()

truncate table Observation.FactAdmission;

with Admission
(
MergeAdmissionRecno
,SexID 
,AdmissionDateID 
,AdmissionTimeOfDayCode 
,StartSpecialtyID 
,StartLocationID 
,DischargeDateID
,DischargeTimeOfDayCode 
,EndSpecialtyID 
,EndLocationID 
,TreatmentOutcomeID 
,DischargeDestinationID 
,Discharged
,AdmissionOutOfHours 
,DischargeOutOfHours 
,AdmissionAgeID 
,DischargeAgeID 
,ContextID
,Cases 
,LengthOfStay 
,DischargeTime
)

as
	(
	select --top 10 
		BaseAdmission.MergeAdmissionRecno
		,SexID = BaseAdmissionReference.SexID
		,AdmissionDateID = BaseAdmissionReference.AdmissionDateID
		,AdmissionTimeOfDayCode =
						coalesce(
							datediff(
									minute
								,dateadd(day, datediff(day, 0, BaseAdmission.AdmissionTime), 0)
								,BaseAdmission.AdmissionTime
							)
							,-1
						)
		,StartSpecialtyID = BaseAdmissionReference.StartSpecialtyID
		,StartLocationID = BaseAdmissionReference.StartLocationID
		,DischargeDateID = BaseAdmissionReference.DischargeDateID
		,DischargeTimeOfDayCode =
						coalesce(
							datediff(
									minute
								,dateadd(day, datediff(day, 0, BaseAdmission.DischargeTime), 0)
								,BaseAdmission.DischargeTime
							)
							,-1
						)
		,EndSpecialtyID = BaseAdmissionReference.EndSpecialtyID
		,EndLocationID = BaseAdmissionReference.EndLocationID
		,TreatmentOutcomeID = BaseAdmissionReference.TreatmentOutcomeID
		,DischargeDestinationID = BaseAdmissionReference.DischargeDestinationID
		,Discharged = 
					case
					when BaseAdmission.DischargeTime is not null
					then 1
					else 0
					end
		,AdmissionOutOfHours = 
					case
					when datename(dw, BaseAdmission.AdmissionTime) in ('Saturday', 'Sunday') then 1
					when cast(BaseAdmission.AdmissionTime as time) < '8:00' then 1
					when cast(BaseAdmission.AdmissionTime as time) > '17:00' then 1
					when exists
						(
						select
							1
						from
							[$(Warehouse)].WH.Holiday
						where
							cast(BaseAdmission.AdmissionTime as date) = Holiday.HolidayDate
						)
					then 1
					else 0
					end
		,DischargeOutOfHours = 
					case
					when datename(dw, BaseAdmission.DischargeTime) in ('Saturday', 'Sunday') then 1
					when cast(BaseAdmission.DischargeTime as time) < '8:00' then 1
					when cast(BaseAdmission.DischargeTime as time) > '17:00' then 1
					when exists
						(
						select
							1
						from
							[$(Warehouse)].WH.Holiday
						where
							cast(BaseAdmission.DischargeTime as date) = Holiday.HolidayDate
						)
					then 1
					else 0
					end
		,AdmissionAgeID = 
					case
					when DateOfBirth > AdmissionTime
					then 0
					when (month(AdmissionTime) * 100) + day(AdmissionTime) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
					then datediff(year, DateOfBirth, AdmissionTime)
					else datediff(year, DateOfBirth, AdmissionTime) - 1
					end
		,DischargeAgeID = 
					case
					when DateOfBirth > DischargeTime
					then 0
					when (month(DischargeTime) * 100) + day(DischargeTime) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
					then datediff(year, DateOfBirth, DischargeTime)
					else datediff(year, DateOfBirth, DischargeTime) - 1
					end
		,BaseAdmissionReference.ContextID
		,Cases = 1
		,LengthOfStay = datediff(hour, BaseAdmission.AdmissionTime, coalesce(BaseAdmission.DischargeTime, getdate())) / cast(24 as float)
		,BaseAdmission.DischargeTime 
	from
		Observation.BaseAdmission
	inner join Observation.BaseAdmissionReference
	on	BaseAdmission.MergeAdmissionRecno = BaseAdmissionReference.MergeAdmissionRecno
	)

insert Observation.FactAdmission
(
MetricID
,MergeAdmissionRecno
,SexID
,DateID
,TimeOfDayCode
,SpecialtyID
,LocationID
,TreatmentOutcomeID
,DischargeDestinationID
,Discharged
,OutOfHours
,AgeID
,ContextID
,Cases
,LengthOfStay
)

select
	MetricID
	,MergeAdmissionRecno
	,SexID 
	,DateID  
	,TimeOfDayCode 
	,SpecialtyID
	,LocationID 
	,TreatmentOutcomeID 
	,DischargeDestinationID 
	,Discharged
	,OutOfHours
	,AgeID 
	,ContextID	
	,Cases 
	,LengthOfStay 
from
	(
	select
		MetricID = 1 -- Admission
		,MergeAdmissionRecno
		,SexID 
		,DateID = AdmissionDateID 
		,TimeOfDayCode = AdmissionTimeOfDayCode 
		,SpecialtyID = StartSpecialtyID 
		,LocationID = StartLocationID 
		,TreatmentOutcomeID 
		,DischargeDestinationID 
		,Discharged
		,OutOfHours = AdmissionOutOfHours 
		,AgeID = AdmissionAgeID 
		,ContextID	
		,Cases 
		,LengthOfStay 
	from
		Admission

	union 

	select
		MetricID = 2 -- Current
		,MergeAdmissionRecno
		,SexID 
		,DateID = AdmissionDateID 
		,TimeOfDayCode = AdmissionTimeOfDayCode 
		,SpecialtyID = EndSpecialtyID 
		,LocationID = EndLocationID 
		,TreatmentOutcomeID 
		,DischargeDestinationID 
		,Discharged
		,OutOfHours = AdmissionOutOfHours 
		,AgeID = AdmissionAgeID 
		,ContextID	
		,Cases 
		,LengthOfStay 
	from
		Admission
	where
		DischargeTime is null

	union

	select
		MetricID = 3 -- Discharge
		,MergeAdmissionRecno
		,SexID 
		,DateID = DischargeDateID 
		,TimeOfDayCode = DischargeTimeOfDayCode 
		,SpecialtyID = EndSpecialtyID 
		,LocationID = EndLocationID 
		,TreatmentOutcomeID 
		,DischargeDestinationID 
		,Discharged
		,OutOfHours = DischargeOutOfHours 
		,AgeID = DischargeAgeID 
		,ContextID	
		,Cases 
		,LengthOfStay 
	from
		Admission
	where
		DischargeTime is not null
	) Admission

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @ProcedureName = name from sysobjects where id = @@procid

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	

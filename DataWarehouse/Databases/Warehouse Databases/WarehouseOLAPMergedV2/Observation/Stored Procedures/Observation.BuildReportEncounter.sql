﻿

CREATE proc [Observation].[BuildReportEncounter] as 

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@RowsInserted int


select @StartTime = getdate()

truncate table Observation.ReportEncounter

insert Observation.ReportEncounter
(
ReportEncounterTypeID
,MergeRecno
,SourceUniqueID
,CasenoteNumber
,DateOfBirth
,CreatedTime
,BedsideDueTime
,EscalationTime
,AcceptedTime
,ClosedTime
)
          
select
	1 -- Alert
	,MergeAlertRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,CreatedTime
	,BedsideDueTime
	,EscalationTime
	,AcceptedTime
	,ClosedTime
from
	Observation.Alert

select @RowsInserted = @@rowcount

insert Observation.ReportEncounter
(
ReportEncounterTypeID
,MergeRecno
,SourceUniqueID
,CasenoteNumber
,DateOfBirth
,StartTime
,TakenTime
,OverallRiskIndexCode
,DueTime
,LocationStartTime
,AdmissionTime
,DischargeTime
)
   
select
	2 -- Observation Set
	,MergeObservationSetRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,StartTime
	,TakenTime
	,OverallRiskIndexCode
	,DueTime
	,LocationStartTime
	,AdmissionTime
	,DischargeTime
from
	Observation.ObservationSet

select @RowsInserted = @RowsInserted + @@rowcount

insert Observation.ReportEncounter
(
ReportEncounterTypeID
,MergeRecno
,SourceUniqueID
,CasenoteNumber
,DateOfBirth
,AdmissionTime
,DischargeTime
)
   
select
	3 -- Admission
	,MergeAdmissionRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,AdmissionTime
	,DischargeTime
from
	Observation.Admission

select @RowsInserted = @RowsInserted + @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @ProcedureName = name from sysobjects where id = @@procid

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	





﻿CREATE TABLE [Observation].[FactAlert] (
    [MergeAlertRecno]      INT NOT NULL,
    [SpecialtyID]          INT NULL,
    [LocationID]           INT NULL,
    [TypeID]               INT NULL,
    [ReasonID]             INT NULL,
    [CreatedDateID]        INT NULL,
    [CreatedTimeOfDayCode] INT NULL,
    [AttendedByUserID]     INT NULL,
    [SeverityID]           INT NULL,
    [ClosedDateID]         INT NULL,
    [ClosedTimeOfDayCode]  INT NULL,
    [ClosureReasonID]      INT NULL,
    [ClosedByUserID]       INT NULL,
    [DurationID]           INT NULL,
    [Discharged]           BIT NULL,
    [OutOfHours]           BIT NOT NULL,
    [AgeID]                INT NULL,
    [ContextID]            INT NULL,
    [Cases]                INT NOT NULL,
    [AlertMinutes]         INT NULL,
    CONSTRAINT [PK__FactAler__D288D65B066552A8] PRIMARY KEY CLUSTERED ([MergeAlertRecno] ASC)
);


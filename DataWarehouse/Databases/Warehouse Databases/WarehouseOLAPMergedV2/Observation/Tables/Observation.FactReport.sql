﻿CREATE TABLE [Observation].[FactReport] (
    [MetricID]             INT NOT NULL,
    [ReportEncounterRecno] INT NULL,
    [SpecialtyID]          INT NULL,
    [LocationID]           INT NULL,
    [DateID]               INT NULL,
    [TimeOfDayCode]        INT NULL,
    [DimensionOneID]       INT NULL,
    [DimensionTwoID]       INT NULL,
    [OutOfHours]           BIT NOT NULL,
    [Cases]                INT NOT NULL
);


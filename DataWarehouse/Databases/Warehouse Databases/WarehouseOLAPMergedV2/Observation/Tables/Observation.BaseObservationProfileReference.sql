﻿CREATE TABLE [Observation].[BaseObservationProfileReference] (
    [MergeObservationProfileRecno] INT          NOT NULL,
    [ContextID]                    INT          NULL,
    [SpecialtyID]                  INT          NOT NULL,
    [LocationID]                   INT          NOT NULL,
    [ProfileID]                    INT          NULL,
    [ProfileTemplateID]            INT          NOT NULL,
    [TemplatePriorityID]           INT          NULL,
    [DrivingTableID]               INT          NULL,
    [ProfileReasonID]              INT          NOT NULL,
    [StartDateID]                  INT          NOT NULL,
    [EndDateID]                    INT          NULL,
    [Created]                      DATETIME     NULL,
    [Updated]                      DATETIME     NULL,
    [ByWhom]                       VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseObse__574F9C3F4B799704] PRIMARY KEY CLUSTERED ([MergeObservationProfileRecno] ASC)
);


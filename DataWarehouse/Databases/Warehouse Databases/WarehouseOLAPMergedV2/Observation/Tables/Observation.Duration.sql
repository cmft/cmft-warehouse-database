﻿CREATE TABLE [Observation].[Duration] (
    [DurationID]             INT          NOT NULL,
    [MinuteBand]             INT          NULL,
    [AlertDurationBand]      VARCHAR (20) NULL,
    [AlertChainDurationBand] VARCHAR (20) NULL,
    CONSTRAINT [PK__Duration__AF77E816784C3D7B] PRIMARY KEY CLUSTERED ([DurationID] ASC)
);


﻿CREATE TABLE [Observation].[BaseAdmissionReference] (
    [MergeAdmissionRecno]    INT          NOT NULL,
    [ContextID]              INT          NULL,
    [SexID]                  INT          NULL,
    [AdmissionDateID]        INT          NULL,
    [StartSpecialtyID]       INT          NULL,
    [StartLocationID]        INT          NULL,
    [DischargeDateID]        INT          NULL,
    [EndSpecialtyID]         INT          NULL,
    [EndLocationID]          INT          NULL,
    [TreatmentOutcomeID]     INT          NULL,
    [DischargeDestinationID] INT          NULL,
    [Created]                DATETIME     NULL,
    [Updated]                DATETIME     NULL,
    [ByWhom]                 VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseAdmi__82C7D73F14E87C29] PRIMARY KEY CLUSTERED ([MergeAdmissionRecno] ASC)
);


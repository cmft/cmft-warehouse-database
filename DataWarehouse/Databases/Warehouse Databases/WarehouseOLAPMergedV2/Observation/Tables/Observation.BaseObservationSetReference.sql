﻿CREATE TABLE [Observation].[BaseObservationSetReference] (
    [MergeObservationSetRecno]        INT           NOT NULL,
    [ContextID]                       INT           NULL,
    [SpecialtyID]                     INT           NULL,
    [LocationID]                      INT           NULL,
    [ObservationProfileApplicationID] INT           NULL,
    [ObservationNotTakenReasonID]     INT           NULL,
    [ClinicianPresentSeniorityID]     INT           NULL,
    [StartDateID]                     INT           NULL,
    [TakenDateID]                     INT           NULL,
    [OverallRiskIndexID]              INT           NULL,
    [OverallAssessedStatusID]         INT           NULL,
    [AlertSeverityID]                 INT           NULL,
    [DueTimeStatusID]                 INT           NULL,
    [Created]                         DATETIME      NULL,
    [Updated]                         DATETIME      NULL,
    [ByWhom]                          VARCHAR (255) NULL,
    CONSTRAINT [PK__BaseObse__9DDA3E770D7C648B] PRIMARY KEY CLUSTERED ([MergeObservationSetRecno] ASC)
);


﻿CREATE TABLE [Observation].[BaseObservationSet] (
    [MergeObservationSetRecno]             INT          IDENTITY (1, 1) NOT NULL,
    [ObservationSetRecno]                  INT          NOT NULL,
    [SourceUniqueID]                       INT          NOT NULL,
    [CasenoteNumber]                       VARCHAR (20) NULL,
    [DateOfBirth]                          DATE         NULL,
    [SpecialtyID]                          INT          NULL,
    [LocationID]                           INT          NULL,
    [LocationStartTime]                    DATETIME     NULL,
    [ReplacedSourceUniqueID]               INT          NULL,
    [CurrentObservationSetFlag]            BIT          NULL,
    [AdditionalObservationSetFlag]         BIT          NULL,
    [AdmissionSourceUniqueID]              INT          NULL,
    [EarlyWarningScoreRegimeApplicationID] INT          NULL,
    [ObservationProfileApplicationID]      INT          NULL,
    [ObservationNotTakenReasonID]          INT          NULL,
    [ClinicianPresentSeniorityID]          INT          NULL,
    [StartDate]                            DATE         NULL,
    [StartTime]                            DATETIME     NULL,
    [TakenDate]                            DATE         NULL,
    [TakenTime]                            DATETIME     NULL,
    [OverallRiskIndexCode]                 INT          NULL,
    [OverallAssessedStatusID]              INT          NULL,
    [AlertSeverityID]                      INT          NULL,
    [DueTime]                              DATETIME     NULL,
    [DueTimeStatusID]                      INT          NULL,
    [DueTimeCreatedBySourceUniqueID]       INT          NULL,
    [AlertChainID]                         INT          NULL,
    [FirstInAlertChain]                    BIT          NULL,
    [LastInAlertChain]                     BIT          NULL,
    [AlertChainMinutes]                    INT          NULL,
    [DirectorateCode]                      VARCHAR (20) NULL,
    [LastModifiedTime]                     DATETIME     NULL,
    [ContextCode]                          VARCHAR (50) NULL,
    [Created]                              DATETIME     NULL,
    [Updated]                              DATETIME     NULL,
    [ByWhom]                               VARCHAR (50) NULL,
    CONSTRAINT [PK_BaseObservationSet] PRIMARY KEY CLUSTERED ([MergeObservationSetRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ObservationSet_ObservationSetRecno_ContextCode]
    ON [Observation].[BaseObservationSet]([ObservationSetRecno] ASC, [ContextCode] ASC);


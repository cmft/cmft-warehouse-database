﻿CREATE TABLE [Observation].[FactAlertChain] (
    [MergeObservationSetRecno] INT NOT NULL,
    [SpecialtyID]              INT NULL,
    [LocationID]               INT NULL,
    [StartDateID]              INT NULL,
    [StartTimeOfDayCode]       INT NULL,
    [OverallRiskIndexID]       INT NULL,
    [OverallAssessedStatusID]  INT NULL,
    [DurationID]               INT NULL,
    [Discharged]               BIT NOT NULL,
    [OutOfHours]               BIT NOT NULL,
    [AgeID]                    INT NULL,
    [ContextID]                INT NULL,
    [Cases]                    INT NOT NULL,
    [AlertChainMinutes]        INT NULL,
    CONSTRAINT [PK__FactAler__9DDA3E7767E0CB88] PRIMARY KEY CLUSTERED ([MergeObservationSetRecno] ASC)
);


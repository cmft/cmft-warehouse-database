﻿CREATE TABLE [Observation].[ReportReference] (
    [ReportReferenceID] INT           IDENTITY (1, 1) NOT NULL,
    [ReferenceID]       INT           NOT NULL,
    [Reference]         VARCHAR (200) NULL,
    [ReportReference]   VARCHAR (200) NULL,
    [ReferenceTypeCode] VARCHAR (20)  NOT NULL,
    [ContextCode]       VARCHAR (20)  NOT NULL,
    CONSTRAINT [PK__ReportRe__A4863026500941F7] PRIMARY KEY CLUSTERED ([ReportReferenceID] ASC),
    CONSTRAINT [UQ__ReportRe__6047752D52E5AEA2] UNIQUE NONCLUSTERED ([ReferenceID] ASC, [ReferenceTypeCode] ASC)
);


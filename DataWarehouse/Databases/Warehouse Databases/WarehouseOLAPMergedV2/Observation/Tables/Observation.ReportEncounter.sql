﻿CREATE TABLE [Observation].[ReportEncounter] (
    [ReportEncounterRecno]  INT          IDENTITY (1, 1) NOT NULL,
    [ReportEncounterTypeID] INT          NULL,
    [MergeRecno]            INT          NULL,
    [SourceUniqueID]        INT          NOT NULL,
    [CasenoteNumber]        VARCHAR (20) NULL,
    [DateOfBirth]           DATE         NULL,
    [CreatedTime]           DATETIME     NULL,
    [BedsideDueTime]        DATETIME     NULL,
    [EscalationTime]        DATETIME     NULL,
    [AcceptedTime]          DATETIME     NULL,
    [ClosedTime]            DATETIME     NULL,
    [StartTime]             DATETIME     NULL,
    [TakenTime]             DATETIME     NULL,
    [OverallRiskIndexCode]  INT          NULL,
    [DueTime]               DATETIME     NULL,
    [LocationStartTime]     DATETIME     NULL,
    [AdmissionTime]         DATETIME     NULL,
    [DischargeTime]         DATETIME     NULL,
    CONSTRAINT [PK__Encounte__78180EC157AA63BF] PRIMARY KEY CLUSTERED ([ReportEncounterRecno] ASC),
    CONSTRAINT [UQ__ReportEn__0C576A960294C1C4] UNIQUE NONCLUSTERED ([ReportEncounterTypeID] ASC, [MergeRecno] ASC)
);


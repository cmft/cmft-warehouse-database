﻿CREATE TABLE [Observation].[BaseAlertReference] (
    [MergeAlertRecno]      INT           NOT NULL,
    [ContextID]            INT           NULL,
    [SpecialtyID]          INT           NULL,
    [LocationID]           INT           NULL,
    [TypeID]               INT           NULL,
    [ReasonID]             INT           NULL,
    [CreatedDateID]        INT           NULL,
    [BedsideDueDateID]     INT           NULL,
    [EscalationDateID]     INT           NULL,
    [AcceptedDateID]       INT           NULL,
    [AttendedByUserID]     INT           NULL,
    [AttendanceTypeID]     INT           NULL,
    [SeverityID]           INT           NULL,
    [ClinicianSeniorityID] INT           NULL,
    [ClosedDateID]         INT           NULL,
    [ClosureReasonID]      INT           NULL,
    [ClosedByUserID]       INT           NULL,
    [Created]              DATETIME      NULL,
    [Updated]              DATETIME      NULL,
    [ByWhom]               VARCHAR (255) NULL,
    CONSTRAINT [PK__BaseAler__6D6CF88E1625EDCE] PRIMARY KEY CLUSTERED ([MergeAlertRecno] ASC)
);


﻿CREATE TABLE [Observation].[ReportEncounterType] (
    [EncounterTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [EncounterType]   VARCHAR (20) NULL,
    CONSTRAINT [PK__Encounte__0C18E6E05B7AF4A3] PRIMARY KEY CLUSTERED ([EncounterTypeID] ASC)
);


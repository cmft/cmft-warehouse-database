﻿CREATE TABLE [Observation].[Age] (
    [AgeID]   INT          NOT NULL,
    [Age]     INT          NULL,
    [AgeBand] VARCHAR (20) NULL,
    CONSTRAINT [PK__AgeBase__875454C2747BAC97] PRIMARY KEY CLUSTERED ([AgeID] ASC)
);


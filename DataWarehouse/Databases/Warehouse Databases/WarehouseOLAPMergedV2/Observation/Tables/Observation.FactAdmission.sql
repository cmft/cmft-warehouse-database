﻿CREATE TABLE [Observation].[FactAdmission] (
    [MetricID]               INT        NOT NULL,
    [MergeAdmissionRecno]    INT        NOT NULL,
    [SexID]                  INT        NULL,
    [DateID]                 INT        NULL,
    [TimeOfDayCode]          INT        NULL,
    [SpecialtyID]            INT        NULL,
    [LocationID]             INT        NULL,
    [TreatmentOutcomeID]     INT        NULL,
    [DischargeDestinationID] INT        NULL,
    [Discharged]             BIT        NOT NULL,
    [OutOfHours]             BIT        NOT NULL,
    [AgeID]                  INT        NULL,
    [ContextID]              INT        NULL,
    [Cases]                  INT        NOT NULL,
    [LengthOfStay]           FLOAT (53) NULL,
    CONSTRAINT [PK_FactAdmission] PRIMARY KEY CLUSTERED ([MetricID] ASC, [MergeAdmissionRecno] ASC)
);


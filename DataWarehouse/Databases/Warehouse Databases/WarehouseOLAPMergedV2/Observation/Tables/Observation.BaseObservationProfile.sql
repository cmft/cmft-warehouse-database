﻿CREATE TABLE [Observation].[BaseObservationProfile] (
    [MergeObservationProfileRecno] INT          IDENTITY (1, 1) NOT NULL,
    [ObservationProfileRecno]      INT          NOT NULL,
    [SourceUniqueID]               INT          NOT NULL,
    [CasenoteNumber]               VARCHAR (20) NOT NULL,
    [DateOfBirth]                  DATETIME     NOT NULL,
    [SpecialtyID]                  INT          NOT NULL,
    [LocationID]                   INT          NOT NULL,
    [AdmissionSourceUniqueID]      INT          NOT NULL,
    [ProfileID]                    INT          NOT NULL,
    [ProfileTemplateID]            INT          NULL,
    [TemplatePriorityID]           INT          NULL,
    [DrivingTableCode]             VARCHAR (5)  NULL,
    [DrivingTableID]               INT          NULL,
    [ProfileReasonID]              INT          NOT NULL,
    [StepNumber]                   INT          NOT NULL,
    [IterationNumber]              INT          NOT NULL,
    [StartTime]                    DATETIME     NOT NULL,
    [EndTime]                      DATETIME     NULL,
    [CreatedBy]                    INT          NOT NULL,
    [CreatedTime]                  DATETIME     NOT NULL,
    [LastModifiedBy]               INT          NOT NULL,
    [LastModifiedTime]             DATETIME     NOT NULL,
    [Active]                       BIT          NOT NULL,
    [ContextCode]                  VARCHAR (11) NOT NULL,
    [Created]                      DATETIME     NULL,
    [Updated]                      DATETIME     NULL,
    [ByWhom]                       VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseObse__574F9C3F3B432F3B] PRIMARY KEY CLUSTERED ([MergeObservationProfileRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BaseObservationProfile_ObservationProfileRecno_ContextCode]
    ON [Observation].[BaseObservationProfile]([ObservationProfileRecno] ASC, [ContextCode] ASC);


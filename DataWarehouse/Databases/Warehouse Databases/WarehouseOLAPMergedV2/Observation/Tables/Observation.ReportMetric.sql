﻿CREATE TABLE [Observation].[ReportMetric] (
    [MetricID] INT          IDENTITY (1, 1) NOT NULL,
    [Metric]   VARCHAR (20) NULL,
    CONSTRAINT [PK__ReportMe__561056450F2FA2D3] PRIMARY KEY CLUSTERED ([MetricID] ASC)
);


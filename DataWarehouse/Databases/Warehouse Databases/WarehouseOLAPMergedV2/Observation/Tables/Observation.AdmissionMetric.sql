﻿CREATE TABLE [Observation].[AdmissionMetric] (
    [MetricID] INT          IDENTITY (1, 1) NOT NULL,
    [Metric]   VARCHAR (20) NULL,
    CONSTRAINT [PK__Admissio__5610564543D8753C] PRIMARY KEY CLUSTERED ([MetricID] ASC)
);


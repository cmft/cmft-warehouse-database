﻿

create view [Observation].[Profile] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceProfileID = SourceValueID
	,SourceProfileCode = SourceValueCode
	,SourceProfile = SourceValue
	,LocalProfileID = LocalValueID
	,LocalProfileCode = LocalValueCode
	,LocalProfile = LocalValue
	,NationalProfileID = NationalValueID
	,NationalProfileCode = NationalValueCode
	,NationalProfile = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'PROFILE'






﻿


CREATE view [Observation].[Admission] as

select
	MergeAdmissionRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,AdmissionTime
	,DischargeTime
from
	Observation.BaseAdmission


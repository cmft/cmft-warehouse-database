﻿



CREATE view [Observation].[OverallAssessedStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceOverallAssessedStatusID = SourceValueID
	,SourceOverallAssessedStatusCode = SourceValueCode
	,SourceOverallAssessedStatus = SourceValue
	,LocalOverallAssessedStatusID = LocalValueID
	,LocalOverallAssessedStatusCode = LocalValueCode
	,LocalOverallAssessedStatus = LocalValue
	,NationalOverallAssessedStatusID = NationalValueID
	,NationalOverallAssessedStatusCode = NationalValueCode
	,NationalOverallAssessedStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ASSESSEDSTATUS'








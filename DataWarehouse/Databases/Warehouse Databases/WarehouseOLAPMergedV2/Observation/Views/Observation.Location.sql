﻿





CREATE view [Observation].[Location] as 

select 
	
	SourceContextCode
	,SourceLocationID = Location.SourceLocationID
	,SourceLocation = Location.SourceLocation
	,LocalLocation = Location.LocalLocation
	,DirectorateCode = coalesce(Directorate.DirectorateCode, '9')
	,Directorate = coalesce(Directorate.Directorate, 'Unassigned')
	,Division = coalesce(Directorate.Division, 'Unassigned')
	,DivisionGroup = coalesce(Directorate.DivisionGroup, 'Unassigned')
from
	WH.Location

left join WH.LocationDirectorate
on	Location.LocalLocationID = LocationDirectorate.LocationID

left join WH.Directorate
on	Directorate.DirectorateID = LocationDirectorate.DirectorateID

where
	SourceContextCode in ('CEN||PTRACK', 'CMFT||CARD')
and	not exists
		(
		select
			1
		from
			WH.LocationDirectorate Later
		where
			Later.LocationID = LocationDirectorate.LocationID
		and Later.StartDate > LocationDirectorate.StartDate
		)











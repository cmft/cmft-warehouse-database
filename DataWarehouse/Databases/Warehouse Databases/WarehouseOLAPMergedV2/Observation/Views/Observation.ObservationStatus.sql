﻿
create view Observation.[ObservationStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceObservationStatusID = SourceValueID
	,SourceObservationStatusCode = SourceValueCode
	,SourceObservationStatus = SourceValue
	,LocalObservationStatusID = LocalValueID
	,LocalObservationStatusCode = LocalValueCode
	,LocalObservationStatus = LocalValue
	,NationalObservationStatusID = NationalValueID
	,NationalObservationStatusCode = NationalValueCode
	,NationalObservationStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'OBSSTATUS'





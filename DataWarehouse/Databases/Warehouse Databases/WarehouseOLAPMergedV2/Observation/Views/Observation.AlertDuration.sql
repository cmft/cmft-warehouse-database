﻿

create view [Observation].[AlertDuration] as

select
	DurationID
	,MinuteBand
	,DurationBand = AlertDurationBand
from
	Observation.Duration



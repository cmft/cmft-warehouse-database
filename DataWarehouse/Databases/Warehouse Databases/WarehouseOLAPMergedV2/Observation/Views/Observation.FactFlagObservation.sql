﻿

--/****** Object:  View [dbo].[FactObservationFlagObservation]    Script Date: 11/12/2015 13:29:09 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO








CREATE view [Observation].[FactFlagObservation] as

select --top 100
	FactObservationSet.MergeObservationSetRecno
	,Observation.ObservationRecno
	,FactObservationSet.SpecialtyID
	,FactObservationSet.LocationID
	,FactObservationSet.StartDateID
	,FactObservationSet.StartTimeOfDayCode
	,FactObservationSet.DueTimeStatusID
	,FactObservationSet.OverallRiskIndexID
	,FactObservationSet.OverallAssessedStatusID
	,FactObservationSet.OutOfHours
	,FactObservationSet.AgeID
	,MeasureID = 
				coalesce(
					Observation.MeasureID
					,-1
				)

	,FactObservationSet.ContextID
	,FlagCode = Observation.FlagCode
	,Observations = 1
from
	 Observation.FactObservationSet

inner join Observation.BaseObservationSet
on	FactObservationSet.MergeObservationSetRecno = BaseObservationSet.MergeObservationSetRecno

inner join [$(Warehouse)].Observation.Observation  -- too many records to keep rebuilding in WHOLAP. Will address this when moving to WOMv2
on Observation.ObservationSetSourceUniqueID = BaseObservationSet.SourceUniqueID

where
	Observation.FlagCode is not null









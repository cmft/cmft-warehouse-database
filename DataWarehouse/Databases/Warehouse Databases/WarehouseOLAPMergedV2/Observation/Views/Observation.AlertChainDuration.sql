﻿

create view [Observation].[AlertChainDuration] as

select
	DurationID
	,MinuteBand
	,DurationBand = AlertChainDurationBand
from
	Observation.Duration

﻿


CREATE view [Observation].[Measure]

as

select
	MeasureID
	,ParentMeasureID
	,MeasureCode
	,Measure
	,ObservationMeasureLabel = MeasureCode + ' - ' + Measure
	,Complex
	,Unit
	,DataTypeCode
	,Format
	,MinimumValue
	,MaximumValue
	,ListCode
	,StartTime
	,EndTime
	,Deleted
from
	[$(Warehouse)].Observation.MeasureBase


union all

select
	-1
	,-1
	,'N/A'
	,'N/A'
	,'N/A'
	,'0'
	,'N/A'
	,'N/A'
	,'N/A'
	,null
	,null
	,null
	,null
	,null
	,'0'



















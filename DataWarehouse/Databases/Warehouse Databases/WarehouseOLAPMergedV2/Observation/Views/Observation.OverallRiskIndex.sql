﻿

CREATE view Observation.[OverallRiskIndex] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceOverallRiskIndexID = SourceValueID
	,SourceOverallRiskIndexCode = SourceValueCode
	,SourceOverallRiskIndex = SourceValue
	,LocalOverallRiskIndexID = LocalValueID
	,LocalOverallRiskIndexCode = LocalValueCode
	,LocalOverallRiskIndex = LocalValue
	,NationalOverallRiskIndexID = NationalValueID
	,NationalOverallRiskIndexCode = NationalValueCode
	,NationalOverallRiskIndex = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'RISKINX'






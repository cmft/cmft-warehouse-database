﻿

create view [Observation].[ProfileReason] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceProfileReasonID = SourceValueID
	,SourceProfileReasonCode = SourceValueCode
	,SourceProfileReason = SourceValue
	,LocalProfileReasonID = LocalValueID
	,LocalProfileReasonCode = LocalValueCode
	,LocalProfileReason = LocalValue
	,NationalProfileReasonID = NationalValueID
	,NationalProfileReasonCode = NationalValueCode
	,NationalProfileReason = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'PROFILEREASON'






﻿







CREATE view Observation.[FactNumericObservation] as

select --top 100
	FactObservationSet.MergeObservationSetRecno
	,Observation.ObservationRecno
	,FactObservationSet.SpecialtyID
	,FactObservationSet.LocationID
	,FactObservationSet.StartDateID
	,FactObservationSet.StartTimeOfDayCode
	,FactObservationSet.DueTimeStatusID
	,FactObservationSet.OverallRiskIndexID
	,FactObservationSet.OverallAssessedStatusID
	,FactObservationSet.OutOfHours
	,FactObservationSet.AgeID
	,MeasureID = 
				coalesce(
					Observation.MeasureID
					,-1
				)
	,FactObservationSet.ContextID
	,Result = Observation.NumericResult
	,Observations = 1
from
	 Observation.FactObservationSet

inner join Observation.BaseObservationSet
on	FactObservationSet.MergeObservationSetRecno = BaseObservationSet.MergeObservationSetRecno

inner join [$(Warehouse)].Observation.Observation
on Observation.ObservationSetSourceUniqueID = BaseObservationSet.SourceUniqueID

where
	Observation.NumericResult is not null








﻿

CREATE view Observation.[AlertType] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAlertTypeID = SourceValueID
	,SourceAlertTypeCode = SourceValueCode
	,SourceAlertType = SourceValue
	,LocalAlertTypeID = LocalValueID
	,LocalAlertTypeCode = LocalValueCode
	,LocalAlertType = LocalValue
	,NationalAlertTypeID = NationalValueID
	,NationalAlertTypeCode = NationalValueCode
	,NationalAlertType = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ALRTTYPE'






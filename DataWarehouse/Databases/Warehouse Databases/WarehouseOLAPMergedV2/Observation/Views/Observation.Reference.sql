﻿
create view Observation.Reference as

select
	ReferenceID
	,ReferenceCode
	,Reference = Description
	,DomainCode
from
	[$(Warehouse)].Observation.ReferenceBase

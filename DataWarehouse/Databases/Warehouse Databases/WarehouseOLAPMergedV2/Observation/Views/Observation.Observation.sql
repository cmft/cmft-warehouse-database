﻿
create view Observation.Observation as 

select
	 ObservationRecno
	,SourceUniqueID
	,ObservationSetSourceUniqueID
	,MeasureID
	,NumericResult
	,ListID
	,FlagCode
	,CreatedByUserID
	,CreatedTime
	,ModifiedByUserID
	,LastModifiedTime
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
from
	[$(Warehouse)].Observation.Observation

﻿






CREATE view Observation.[ObservationList]

as

select
	ListID 
	,ListCode 
    ,List 
    ,ListLabel = ListCode + ' - ' + List

from
	[$(Warehouse)].Observation.List

union all

select
	-1
	,'N/A'
	,'N/A'
	,'N/A'









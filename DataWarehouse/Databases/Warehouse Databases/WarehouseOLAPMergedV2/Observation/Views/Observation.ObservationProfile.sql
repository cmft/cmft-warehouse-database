﻿create view Observation.ObservationProfile as 
select 
	MergeObservationProfileRecno
	,CasenoteNumber
	,DateOfBirth
	,DrivingTableCode
	,StepNumber
	,IterationNumber
	,StartTime
	,EndTime
	,Active
from
	Observation.BaseObservationProfile
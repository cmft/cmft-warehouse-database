﻿



CREATE view [Observation].[ObservationSet] as

select
	MergeObservationSetRecno
	,BaseObservationSet.SourceUniqueID
	,BaseObservationSet.CasenoteNumber
	,BaseObservationSet.DateOfBirth
	,StartTime
	,TakenTime
	,OverallRiskIndexCode
	,DueTime
	,LocationStartTime
	,AdmissionTime
	,DischargeTime
from
	Observation.BaseObservationSet

inner join Observation.BaseAdmission
on	BaseObservationSet.AdmissionSourceUniqueID = BaseAdmission.SourceUniqueID


























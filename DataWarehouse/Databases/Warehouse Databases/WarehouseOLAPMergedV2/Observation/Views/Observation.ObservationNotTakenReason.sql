﻿

create view [Observation].ObservationNotTakenReason as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceObservationNotTakenReasonID = SourceValueID
	,SourceObservationNotTakenReasonCode = SourceValueCode
	,SourceObservationNotTakenReason = SourceValue
	,LocalObservationNotTakenReasonID = LocalValueID
	,LocalObservationNotTakenReasonCode = LocalValueCode
	,LocalObservationNotTakenReason = LocalValue
	,NationalObservationNotTakenReasonID = NationalValueID
	,NationalObservationNotTakenReasonCode = NationalValueCode
	,NationalObservationNotTakenReason = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'OBSNOTTAKENREASON'






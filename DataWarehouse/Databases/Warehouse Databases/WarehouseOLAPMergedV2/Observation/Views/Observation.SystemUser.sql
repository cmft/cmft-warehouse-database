﻿
create view Observation.SystemUser as 

select
	WHSystemUser.SystemUserID
	,WHSystemUser.UserID
	,WHSystemUser.ContextCode
	,WHSystemUser.ContextID
	,WHSystemUser.Username
	,UserUserGroup.UserGroup
from
	WH.SystemUser WHSystemUser

left join [$(Warehouse)].Observation.UserUserGroup
on	WHSystemUser.UserID = UserUserGroup.UserID

where
	WHSystemUser.ContextCode = 'CEN||PTRACK'
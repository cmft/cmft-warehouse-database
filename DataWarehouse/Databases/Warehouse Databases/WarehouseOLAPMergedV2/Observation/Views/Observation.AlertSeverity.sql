﻿

CREATE view Observation.[AlertSeverity] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAlertSeverityID = SourceValueID
	,SourceAlertSeverityCode = SourceValueCode
	,SourceAlertSeverity = SourceValue
	,LocalAlertSeverityID = LocalValueID
	,LocalAlertSeverityCode = LocalValueCode
	,LocalAlertSeverity = LocalValue
	,NationalAlertSeverityID = NationalValueID
	,NationalAlertSeverityCode = NationalValueCode
	,NationalAlertSeverity = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ALRTSVR'






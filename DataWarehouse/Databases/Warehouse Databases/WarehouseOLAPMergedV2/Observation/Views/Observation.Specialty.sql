﻿









CREATE view [Observation].[Specialty] as 

select
	Specialty.SourceContextCode
	,Specialty.SourceSpecialtyID
	,SourceSpecialty = Specialty.SourceSpecialty
	,LocalSpecialty = Specialty.LocalSpecialty
	,Directorate = 
			replace(
				coalesce(
					DirectorateAllocation.Directorate
					, 'Unassigned'
				)
				,'N/A'
				,'Unassigned'
				)
	,Division = 
			replace(
				coalesce(
					DirectorateAllocation.Division
					, 'Unassigned'
				)
				,'N/A'
				,'Unassigned'
				)
	,DivisionGroup = 
			replace(
				coalesce(
					DirectorateAllocation.DivisionGroup
					, 'Unassigned'
				)
				,'N/A'
				,'Unassigned'
				)
from
	WH.Specialty 

left join 
	(
	select
		RuleBase.SourceSpecialtyCode
		,RuleBase.SourceContextCode
		,Directorate.Directorate
		,Directorate.Division
		,Directorate.DivisionGroup
	from
		Allocation.RuleBase

	inner join Allocation.Allocation
	on	Allocation.AllocationID = RuleBase.AllocationID

	inner join WH.Directorate
	on	Allocation.SourceAllocationID = Directorate.DirectorateCode

	where
		not exists
			(
			select
				1
			from
				Allocation.RuleBase Later
			where
				Later.SourceSpecialtyCode =  RuleBase.SourceSpecialtyCode
			and	Later.SourceContextCode = RuleBase.SourceContextCode
			and	Later.RuleBaseRecno > RuleBase.RuleBaseRecno
			)

	) DirectorateAllocation

on	DirectorateAllocation.SourceSpecialtyCode = Specialty.SourceSpecialtyCode
and	DirectorateAllocation.SourceContextCode = Specialty.SourceContextCode

where
	Specialty.SourceContextCode = 'CEN||PTRACK'
--and SourceSpecialtyID = 3299136














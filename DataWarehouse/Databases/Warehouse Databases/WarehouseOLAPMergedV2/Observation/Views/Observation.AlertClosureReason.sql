﻿
create view Observation.[AlertClosureReason] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAlertClosureReasonID = SourceValueID
	,SourceAlertClosureReasonCode = SourceValueCode
	,SourceAlertClosureReason = SourceValue
	,LocalAlertClosureReasonID = LocalValueID
	,LocalAlertClosureReasonCode = LocalValueCode
	,LocalAlertClosureReason = LocalValue
	,NationalAlertClosureReasonID = NationalValueID
	,NationalAlertClosureReasonCode = NationalValueCode
	,NationalAlertClosureReason = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ALRTCLOSRSN'





﻿

create view [Observation].[AlertReason] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAlertReasonID = SourceValueID
	,SourceAlertReasonCode = SourceValueCode
	,SourceAlertReason = SourceValue
	,LocalAlertReasonID = LocalValueID
	,LocalAlertReasonCode = LocalValueCode
	,LocalAlertReason = LocalValue
	,NationalAlertReasonID = NationalValueID
	,NationalAlertReasonCode = NationalValueCode
	,NationalAlertReason = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ALRTRSN'






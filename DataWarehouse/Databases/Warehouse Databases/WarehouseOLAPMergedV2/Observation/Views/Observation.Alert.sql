﻿

CREATE view [Observation].[Alert] as

select
	MergeAlertRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,CreatedTime
	,BedsideDueTime
	,EscalationTime
	,AcceptedTime
	,ClosedTime
from
	Observation.BaseAlert






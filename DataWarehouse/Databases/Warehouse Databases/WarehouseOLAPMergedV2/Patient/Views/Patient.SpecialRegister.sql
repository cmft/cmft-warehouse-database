﻿

create view Patient.SpecialRegister as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceSpecialRegisterID = SourceValueID
	,SourceSpecialRegisterCode = SourceValueCode
	,SourceSpecialRegister = SourceValue
	,LocalSpecialRegisterID = LocalValueID
	,LocalSpecialRegisterCode = LocalValueCode
	,LocalSpecialRegister = LocalValue
	,NationalSpecialRegisterID = NationalValueID
	,NationalSpecialRegisterCode = NationalValueCode
	,NationalSpecialRegister = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'SpecialRegister'








﻿CREATE PROCEDURE [Patient].[BuildPatientSourceSystemFlag] AS


--	20150724	RR	changed StartDate = ActivityDate to StartDate = coalesce(ActivityDate,EnteredDate).  The flag table doesn't allow nulls and not every recording of LD will be associated to an actual activity, so if no activity date, use date recorded on system.

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--remove flags that no longer apply
delete
from
	Flag.PatientSourceSystemFlag
where
	PatientSourceSystemFlag.SourceSystemID = 3
and	PatientSourceSystemFlag.FlagID = 1
and	not exists
	(
	select
		1
	from
		Patient.BaseSpecialRegister
	
	inner join Patient.SpecialRegister
	on BaseSpecialRegister.SpecialRegisterCode = SpecialRegister.SourceSpecialRegisterCode
	and BaseSpecialRegister.ContextCode = SpecialRegister.SourceContextCode
	
	where
		SpecialRegister.LocalSpecialRegister = 'Learning Disability' --learning disability
	and coalesce(Active,1) = 1
	and	BaseSpecialRegister.ContextCode = PatientSourceSystemFlag.ContextCode
	and	BaseSpecialRegister.SourcePatientNo = PatientSourceSystemFlag.PatientID
	and	cast(coalesce(BaseSpecialRegister.ActivityTime,BaseSpecialRegister.StartDate,EnteredDate) as date) = PatientSourceSystemFlag.StartDate
	)

select
	 @deleted = @@rowcount


--this inserts every new occurance of this flag - is that correct or do we just want the 1st (or last)?
insert into	Flag.PatientSourceSystemFlag
	(
	SourceSystemID
	,ContextCode
	,PatientID
	,StartDate
	,FlagID
	,EndDate
	,DistrictNo
	)
select
	SourceSystemID
	,ContextCode
	,PatientID
	,StartDate
	,FlagID
	,EndDate
	,DistrictNo
from
	(
	select 
		SourceSystemID = 3 --special register
		,ContextCode = BaseSpecialRegister.ContextCode
		,PatientID = BaseSpecialRegister.SourcePatientNo
		,StartDate = coalesce(BaseSpecialRegister.ActivityTime,BaseSpecialRegister.StartDate,BaseSpecialRegister.EnteredDate)
		,FlagID = 1 --Learning disability
		,EndDate
		,DistrictNo
	from
		Patient.BaseSpecialRegister
		
	inner join Patient.SpecialRegister
	on BaseSpecialRegister.SpecialRegisterCode = SpecialRegister.SourceSpecialRegisterCode
	and BaseSpecialRegister.ContextCode = SpecialRegister.SourceContextCode
	
	where
		SpecialRegister.LocalSpecialRegister = 'Learning Disability' 
	and coalesce(Active,1) = 1
	) Flags
where
	not exists
		(
		select
			1
		from
			Flag.PatientSourceSystemFlag
		where
			PatientSourceSystemFlag.SourceSystemID = Flags.SourceSystemID
		and	PatientSourceSystemFlag.ContextCode = Flags.ContextCode
		and	PatientSourceSystemFlag.PatientID = Flags.PatientID
		and	PatientSourceSystemFlag.StartDate = Flags.StartDate
		and	PatientSourceSystemFlag.FlagID = Flags.FlagID
		)

select
	 @inserted = @@rowcount
	 
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent  @ProcedureName,@Stats

RETURN 0


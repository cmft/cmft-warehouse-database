﻿CREATE TABLE [Patient].[BaseSpecialRegisterReference] (
    [MergeSpecialRegisterRecno] INT          NOT NULL,
    [ContextID]                 INT          NULL,
    [EnteredDateID]             INT          NULL,
    [SpecialRegisterID]         INT          NULL,
    [Created]                   DATETIME     NULL,
    [Updated]                   DATETIME     NULL,
    [ByWhom]                    VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseSpec__B7EBBBB930709B0D] PRIMARY KEY CLUSTERED ([MergeSpecialRegisterRecno] ASC)
);


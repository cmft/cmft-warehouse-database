﻿CREATE TABLE [Patient].[BaseSpecialRegister] (
    [MergeSpecialRegisterRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SpecialRegisterRecno]      INT           NOT NULL,
    [SourcePatientNo]           INT           NOT NULL,
    [SpecialRegisterCode]       VARCHAR (4)   NOT NULL,
    [ContextCode]               VARCHAR (10)  NOT NULL,
    [DistrictNo]                VARCHAR (20)  NULL,
    [NHSNumber]                 VARCHAR (17)  NULL,
    [EnteredDate]               DATETIME      NULL,
    [ActivityTime]              DATETIME      NULL,
    [SourceEpisodeNo]           VARCHAR (20)  NULL,
    [CreatedTime]               DATETIME      CONSTRAINT [DF_PatientBaseSpecialRegisterCreatedTime] DEFAULT (getdate()) NULL,
    [CreatedByWhom]             VARCHAR (50)  CONSTRAINT [DF_PatientBaseSpecialRegisterCreatedByWhom] DEFAULT (suser_name()) NULL,
    [UpdatedTime]               DATETIME      CONSTRAINT [DF_PatientBaseSpecialRegisterUpdatedTime] DEFAULT (getdate()) NULL,
    [UpdatedByWhom]             VARCHAR (50)  CONSTRAINT [DF_PatientBaseSpecialRegisterUpdatedByWhom] DEFAULT (suser_name()) NULL,
    [Alert]                     VARCHAR (100) NULL,
    [StartDate]                 DATE          NULL,
    [EndDate]                   DATE          NULL,
    [LastModifiedTime]          DATETIME      NULL,
    [Active]                    BIT           NULL,
    CONSTRAINT [PK_PatientBaseSpecialRegister] PRIMARY KEY CLUSTERED ([SpecialRegisterRecno] ASC, [ContextCode] ASC)
);


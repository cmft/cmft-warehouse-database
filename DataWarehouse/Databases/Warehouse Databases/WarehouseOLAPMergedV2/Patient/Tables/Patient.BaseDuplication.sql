﻿CREATE TABLE [Patient].[BaseDuplication] (
    [MergePatientDuplicationID] INT          IDENTITY (1, 1) NOT NULL,
    [PatientDuplicationID]      INT          NOT NULL,
    [ContextCode]               VARCHAR (50) NULL,
    [SourcePatientNo]           VARCHAR (50) NULL,
    [MasterSourcePatientNo]     INT          NULL,
    [RegistrationDate]          DATE         NULL,
    [IsEarliestRegistration]    CHAR (1)     NULL,
    [Created]                   DATETIME     NULL,
    [Updated]                   DATETIME     NULL,
    [ByWhom]                    VARCHAR (50) NULL,
    CONSTRAINT [PK_BasePatientDuplication] PRIMARY KEY CLUSTERED ([MergePatientDuplicationID] ASC)
);


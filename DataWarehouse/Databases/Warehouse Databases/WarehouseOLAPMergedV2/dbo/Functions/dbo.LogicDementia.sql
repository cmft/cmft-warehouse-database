﻿
/******************************************************************************
	**  Name: LogicDementia
	**  Purpose: Logic for Dementia queries
	**
	**	Returns APC.Encounter rows for patients with Dementia within the date period
	**
	**	NOTE - Please maintain this function via Visual Studio 
	**         located in the source control database
	**
	**************************************warehousesql*****************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 17.07.12     MH        Created
	** 14.09.12     MH        Removed date filter this now needs to be applied by
	**                        the calling object
	** 24.10.12     MH        Additional column to denote if the diagnosis was primary or secondary
	******************************************************************************/
	
CREATE FUNCTION [dbo].[LogicDementia]
(
)
RETURNS TABLE AS  
RETURN
(
	SELECT 
		 SPELLSMATCHED.IsAPrimaryDiagnosis
		,BaseEncounter.*
		
	FROM
		APC.fn_HasDiagnosis('F0[0-3]%|F1[0-9].7', '|') SPELLSMATCHED
		
		INNER JOIN APC.BaseEncounter 
			ON SPELLSMATCHED.ProviderSpellNo = BaseEncounter.ProviderSpellNo

		INNER JOIN APC.BaseEncounterReference
			ON BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
		
		INNER JOIN WH.Member LASTSPELL
			ON		BaseEncounterReference.LastEpisodeInSpellID = LASTSPELL.SourceValueID 
				AND BaseEncounter.ContextCode = LASTSPELL.SourceContextCode
						
	WHERE
			LASTSPELL.LocalValueCode = '1'				-- Yes (last spell indicator)
)



﻿



CREATE FUNCTION [dbo].[f_GetAge](

@DateOfBirth AS DATETIME
,@NowDate as DATETIME=getdate)

RETURNS INT

AS

BEGIN

DECLARE @Years AS INT

DECLARE @BirthdayDate AS DATETIME

DECLARE @Age AS INT

--Calculate difference in years

SET @Years = DATEDIFF(YY,@DateOfBirth,@NowDate)

--Add years to DateOfBirth

SET @BirthdayDate = DATEADD(YY,@Years,@DateOfBirth)

--Subtract a year if birthday is after today

SET @Age = @Years -

CASE

WHEN @BirthdayDate>@NowDate THEN 1

ELSE 0

END

--Return the result

RETURN @Age

END



﻿
--drop function FiscalWeek 
Create function FiscalWeek (@startMonth varchar(2), @DateToFind datetime) returns int
as

/***************************************************************************************************************************
20150505	RR	Request - add Fiscal week to Calendar table.  Following function found
http://blog.arvixe.com/finding-week-number-in-sql-based-on-fiscal-period/  (05/05/2015 by David Bauernschmidt, Paul Alcon)
****************************************************************************************************************************/
 
Begin
 
--@StartMonth => The month number of the start start of fiscal period
--@DateToFind => The date that you want to find the fiscal week number
 
declare @firstWeek datetime
declare @weekNum int
declare @year int
 
--Step 1
set @year = datepart(year, @DateToFind)+1 
 
--Step 2: you are taking the 4th day of month of next year, this will always be in week 1
set @firstWeek = convert(datetime, str(@year)+@startMonth+'04', 102) 

 
--Step 3: Retreat to beginning of the week for the date
set @firstWeek = dateadd(day, (1-datepart(dw, @firstWeek)), @firstWeek) 
 
 --Step 4:
 --This allows you to pass in any date with year to find the fiscal week number
while @DateToFind < @firstWeek
--Repeat the above steps but for previous year
begin
  set @year = @year - 1
  set @firstWeek = convert(datetime, str(@year)+@startMonth+'04', 102)
  set @firstWeek = dateadd(day, (1-datepart(dw, @firstWeek)), @firstWeek)
end
 
set @weekNum = ((datediff(day, @firstWeek, @DateToFind)/7)+1)
 
return @weekNum
end

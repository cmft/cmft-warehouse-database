﻿create FUNCTION [dbo].[Check_NHS_number]
(
      -- Add the parameters for the function here
      @NHSNo varchar(10)
)
RETURNS bit
AS
BEGIN

--P.McNulty 09/07/2012 - uses standard internal agolrithm to check NHS Number 

IF ISNUMERIC(@NHSNo) <> 1 RETURN 0
if convert(bigint,@NHSNo) < 1  return 0

      -- Declare the return variable here
DECLARE @1 int, @2 int, @3 int, @4 int, @5 int, @6 int, @7 int, @8 int, @9 int, @10 int
Declare @Total int
Declare @R int
      -- Add the T-SQL statements to compute the return value here

select @1 = SUBSTRING(@NHSNo,1,1) * 10
select @2 = SUBSTRING(@NHSNo,2,1) * 9
select @3 = SUBSTRING(@NHSNo,3,1) * 8
select @4 = SUBSTRING(@NHSNo,4,1) * 7
select @5 = SUBSTRING(@NHSNo,5,1) * 6
select @6 = SUBSTRING(@NHSNo,6,1) * 5
select @7 = SUBSTRING(@NHSNo,7,1) * 4
select @8 = SUBSTRING(@NHSNo,8,1) * 3
select @9 = SUBSTRING(@NHSNo,9,1) * 2
select @10 = SUBSTRING(@NHSNo,10,1) 

Select @Total = @1 + @2 + @3 + @4 +@5 + @6 +@7 + @8 + @9 
 

--select @Total = round(@Total / 11,1)
--Select @Total = RIGHT(@Total,1)

select @Total = @Total % 11

Select @Total = 11 - @Total 

if @Total = 11
Select @Total = 0

if @Total = @10 and @Total <> 10  
select @R = 1
else
select @R = 0


--select @Total
      -- Return the result of the function

      
      RETURN @R
            
            
END

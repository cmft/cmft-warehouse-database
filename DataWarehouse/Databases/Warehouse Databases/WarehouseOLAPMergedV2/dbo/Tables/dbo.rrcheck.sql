﻿CREATE TABLE [dbo].[rrcheck] (
    [MergeRecno]            INT          NOT NULL,
    [ContextID]             INT          NULL,
    [FormTypeID]            INT          NULL,
    [ReviewStatusID]        INT          NULL,
    [SystemUserID]          INT          NULL,
    [QuestionID]            INT          NULL,
    [ResponseID]            INT          NULL,
    [ClassificationGradeID] INT          NULL,
    [Created]               DATETIME     NULL,
    [Updated]               DATETIME     NULL,
    [ByWhom]                VARCHAR (50) NULL
);


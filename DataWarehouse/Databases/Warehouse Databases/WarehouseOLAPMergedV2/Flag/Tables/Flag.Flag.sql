﻿CREATE TABLE [Flag].[Flag] (
    [FlagID] INT           NOT NULL,
    [Flag]   VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([FlagID] ASC)
);


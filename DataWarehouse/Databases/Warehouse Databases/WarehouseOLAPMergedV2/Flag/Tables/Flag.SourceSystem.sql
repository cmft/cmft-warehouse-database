﻿CREATE TABLE [Flag].[SourceSystem] (
    [SourceSystemID] INT           NOT NULL,
    [SourceSystem]   VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([SourceSystemID] ASC)
);


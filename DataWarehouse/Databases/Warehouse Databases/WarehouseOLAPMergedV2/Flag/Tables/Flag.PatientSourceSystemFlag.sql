﻿CREATE TABLE [Flag].[PatientSourceSystemFlag] (
    [PatientDatasetFlagRecno] INT          IDENTITY (1, 1) NOT NULL,
    [ContextCode]             VARCHAR (20) NOT NULL,
    [PatientID]               INT          NOT NULL,
    [SourceSystemID]          INT          NOT NULL,
    [StartDate]               DATE         NOT NULL,
    [EndDate]                 DATE         NULL,
    [FlagID]                  INT          NOT NULL,
    [DistrictNo]              VARCHAR (50) NULL,
    CONSTRAINT [PK_PatientSourceSystemFlag] PRIMARY KEY CLUSTERED ([SourceSystemID] ASC, [ContextCode] ASC, [PatientID] ASC, [StartDate] ASC, [FlagID] ASC),
    CONSTRAINT [FK_PatientSourceSystemFlag_Flag] FOREIGN KEY ([FlagID]) REFERENCES [Flag].[Flag] ([FlagID]),
    CONSTRAINT [FK_PatientSourceSystemFlag_SourceSystem] FOREIGN KEY ([SourceSystemID]) REFERENCES [Flag].[SourceSystem] ([SourceSystemID])
);


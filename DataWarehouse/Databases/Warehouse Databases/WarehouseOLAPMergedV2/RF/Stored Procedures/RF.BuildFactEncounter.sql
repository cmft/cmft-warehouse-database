﻿CREATE procedure [RF].[BuildFactEncounter] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare @MissingSubSpecialtyID int =
	(
	select
		SubSpecialty.SubSpecialtyID
	from
		WH.SubSpecialty
	where
		SubSpecialty.SubSpecialty = 'Unassigned'
	)


truncate table RF.FactEncounter

insert into RF.FactEncounter
(
	 MergeEncounterRecno
	,ContextID
	,AgeID
	,SexID
	,ReferralDateID
	,ConsultantID
	,SpecialtyID
	,DirectorateID
	,SiteID
	,SourceOfReferralID
	,ReasonForReferralID
	,ResidenceCCGID
	,CCGID
	,ReligionID
	,EthnicCategoryID

	,Cases
)
select
	 Encounter.MergeEncounterRecno
	,Reference.ContextID
	,Reference.AgeID
	,Reference.SexID
	,Reference.ReferralDateID
	,Reference.ConsultantID
	,Reference.SpecialtyID
	,Directorate.DirectorateID
	,Reference.SiteID
	,Reference.SourceOfReferralID
	,Reference.ReasonForReferralID
	,Reference.ResidenceCCGID
	,Reference.CCGID
	,Reference.ReligionID
	,Reference.EthnicOriginID

	,Cases = 1
from
	RF.BaseEncounter Encounter

inner join RF.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WH.Directorate
on	Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




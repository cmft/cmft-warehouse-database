﻿CREATE TABLE [RF].[BaseEncounterReference] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [AgeID]               INT           NULL,
    [SexID]               INT           NULL,
    [EthnicOriginID]      INT           NULL,
    [MaritalStatusID]     INT           NULL,
    [SiteID]              INT           NULL,
    [ReferralDateID]      INT           NULL,
    [AdminCategoryID]     INT           NULL,
    [SourceOfReferralID]  INT           NULL,
    [ReasonForReferralID] INT           NULL,
    [ConsultantID]        INT           NULL,
    [SpecialtyID]         INT           NULL,
    [RTTCurrentStatusID]  INT           NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    [ResidenceCCGID]      INT           NULL,
    [CCGID]               INT           NULL,
    [ReligionID]          INT           NULL,
    CONSTRAINT [PK_RF_Base_Encounter_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


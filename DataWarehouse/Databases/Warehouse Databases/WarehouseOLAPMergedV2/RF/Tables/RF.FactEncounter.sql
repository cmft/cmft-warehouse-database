﻿CREATE TABLE [RF].[FactEncounter] (
    [MergeEncounterRecno] INT NOT NULL,
    [ContextID]           INT NOT NULL,
    [AgeID]               INT NOT NULL,
    [SexID]               INT NOT NULL,
    [ReferralDateID]      INT NOT NULL,
    [ConsultantID]        INT NOT NULL,
    [SpecialtyID]         INT NOT NULL,
    [DirectorateID]       INT NOT NULL,
    [SiteID]              INT NOT NULL,
    [SourceOfReferralID]  INT NOT NULL,
    [ReasonForReferralID] INT NOT NULL,
    [Cases]               INT NOT NULL,
    [ResidenceCCGID]      INT NULL,
    [CCGID]               INT NULL,
    [ReligionID]          INT NULL,
    [EthnicCategoryID]    INT NULL
);


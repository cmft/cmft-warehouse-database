﻿CREATE TABLE [RF].[BaseEncounter] (
    [MergeEncounterRecno]          INT           IDENTITY (1, 1) NOT NULL,
    [EncounterRecno]               INT           NOT NULL,
    [SourceUniqueID]               VARCHAR (50)  NOT NULL,
    [SourcePatientNo]              VARCHAR (20)  NOT NULL,
    [SourceEncounterNo]            VARCHAR (20)  NOT NULL,
    [PatientTitle]                 NVARCHAR (10) NULL,
    [PatientForename]              NVARCHAR (20) NULL,
    [PatientSurname]               NVARCHAR (30) NULL,
    [DateOfBirth]                  DATETIME      NULL,
    [DateOfDeath]                  SMALLDATETIME NULL,
    [SexCode]                      NVARCHAR (1)  NULL,
    [NHSNumber]                    VARCHAR (17)  NULL,
    [DistrictNo]                   VARCHAR (12)  NULL,
    [Postcode]                     VARCHAR (8)   NULL,
    [PatientAddress1]              VARCHAR (25)  NULL,
    [PatientAddress2]              VARCHAR (25)  NULL,
    [PatientAddress3]              VARCHAR (25)  NULL,
    [PatientAddress4]              VARCHAR (25)  NULL,
    [DHACode]                      VARCHAR (3)   NULL,
    [EthnicOriginCode]             VARCHAR (4)   NULL,
    [MaritalStatusCode]            VARCHAR (1)   NULL,
    [ReligionCode]                 VARCHAR (4)   NULL,
    [RegisteredGpCode]             VARCHAR (8)   NULL,
    [RegisteredGpPracticeCode]     VARCHAR (8)   NULL,
    [EpisodicGpCode]               VARCHAR (8)   NULL,
    [EpisodicGpPracticeCode]       VARCHAR (6)   NULL,
    [EpisodicGdpCode]              VARCHAR (8)   NULL,
    [SiteCode]                     VARCHAR (5)   NULL,
    [ConsultantCode]               VARCHAR (20)  NULL,
    [SpecialtyCode]                VARCHAR (5)   NULL,
    [SourceOfReferralCode]         VARCHAR (5)   NULL,
    [PriorityCode]                 VARCHAR (5)   NULL,
    [ReferralDate]                 DATE          NULL,
    [DischargeDate]                DATE          NULL,
    [DischargeTime]                DATETIME      NULL,
    [DischargeReasonCode]          VARCHAR (4)   NULL,
    [DischargeReason]              VARCHAR (30)  NULL,
    [AdminCategoryCode]            VARCHAR (3)   NULL,
    [ContractSerialNo]             VARCHAR (6)   NULL,
    [RTTPathwayID]                 VARCHAR (25)  NULL,
    [RTTPathwayCondition]          VARCHAR (20)  NULL,
    [RTTStartDate]                 SMALLDATETIME NULL,
    [RTTEndDate]                   SMALLDATETIME NULL,
    [RTTSpecialtyCode]             VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]       VARCHAR (5)   NULL,
    [RTTCurrentStatusCode]         VARCHAR (10)  NULL,
    [RTTCurrentStatusDate]         SMALLDATETIME NULL,
    [RTTCurrentPrivatePatientFlag] BIT           NULL,
    [RTTOverseasStatusFlag]        BIT           NULL,
    [NextFutureAppointmentDate]    SMALLDATETIME NULL,
    [ReferralComment]              VARCHAR (25)  NULL,
    [InterfaceCode]                VARCHAR (5)   NULL,
    [Created]                      DATETIME      NULL,
    [Updated]                      DATETIME      NULL,
    [ByWhom]                       VARCHAR (50)  NULL,
    [ReasonForReferralCode]        VARCHAR (10)  NULL,
    [SourceOfReferralGroupCode]    VARCHAR (10)  NULL,
    [DirectorateCode]              VARCHAR (5)   NULL,
    [CasenoteNo]                   VARCHAR (16)  NULL,
    [PCTCode]                      NVARCHAR (6)  NULL,
    [ContextCode]                  VARCHAR (8)   NOT NULL,
    [AgeCode]                      VARCHAR (27)  NULL,
    [ResidenceCCGCode]             VARCHAR (10)  NULL,
    [CCGCode]                      VARCHAR (10)  NULL,
    CONSTRAINT [PK_BaseEncounter_2] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_BE_DD]
    ON [RF].[BaseEncounter]([DischargeDate] ASC)
    INCLUDE([ConsultantCode], [DirectorateCode], [EncounterRecno], [MergeEncounterRecno], [PatientForename], [PatientSurname], [ReferralDate], [SourceEncounterNo], [SourcePatientNo], [SourceUniqueID], [SpecialtyCode]);


GO
CREATE NONCLUSTERED INDEX [IDX_SourcePatientNo]
    ON [RF].[BaseEncounter]([SourcePatientNo] ASC, [DischargeTime] ASC);


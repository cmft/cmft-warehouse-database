﻿CREATE view [Complaint].[Division] as

select
	 DivisionCode
	,Division
	,SourceDivisionCode
from
	[$(Warehouse)].Complaint.Division

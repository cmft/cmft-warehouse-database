﻿CREATE view [Complaint].[Grade] as

select
	 SourceContextCode
	,SourceContext
	,SourceGradeID = SourceValueID
	,SourceGradeCode = SourceValueCode
	,SourceGrade = SourceValue
	,LocalGradeID = LocalValueID
	,LocalGradeCode = LocalValueCode
	,LocalGrade= LocalValue
	,NationalGradeID = NationalValueID
	,NationalGradeCode = NationalValueCode
	,NationalGrade = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'COMPGRADE'


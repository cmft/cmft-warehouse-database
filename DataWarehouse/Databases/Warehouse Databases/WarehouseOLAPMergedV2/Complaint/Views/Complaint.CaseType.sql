﻿


CREATE view Complaint.CaseType as

select
	 SourceContextCode
	,SourceContext
	,SourceCaseTypeID = SourceValueID
	,SourceCaseTypeCode = SourceValueCode
	,SourceCaseType = SourceValue
	,LocalCaseTypeID = LocalValueID
	,LocalCaseTypeCode = LocalValueCode
	,LocalCaseType = LocalValue
	,NationalCaseTypeID = NationalValueID
	,NationalCaseTypeCode = NationalValueCode
	,NationalCaseType = NationalValue
	FROM
		WH.Member
	where
		AttributeCode = 'COMPCASETYPE'





﻿CREATE view [Complaint].[Duration]
as

select
	 DurationID
	,DurationCode
	,DurationValue
	,Duration
	,DurationBandCode
	,DurationBand
from
	Complaint.DurationBase

﻿CREATE view [Complaint].[Directorate] as

select
	 DirectorateCode
	,Directorate
	,SourceDirectorateCode
from
	[$(Warehouse)].Complaint.Directorate

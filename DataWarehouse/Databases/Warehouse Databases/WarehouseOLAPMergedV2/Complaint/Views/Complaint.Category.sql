﻿



CREATE view [Complaint].Category as

select
	 SourceContextCode
	,SourceContext
	,SourceCategoryID = SourceValueID
	,SourceCategoryCode = SourceValueCode
	,SourceCategory = SourceValue
	,LocalCategoryID = LocalValueID
	,LocalCategoryCode = LocalValueCode
	,LocalCategory = LocalValue
	,NationalCategoryID = NationalValueID
	,NationalCategoryCode = NationalValueCode
	,NationalCategory = NationalValue
	FROM
		WH.Member
	where
		AttributeCode = 'COMPCAT'






﻿CREATE view [Complaint].[Metric] 

as

select
	MetricID
	,MetricCode
	,Metric
from
	Complaint.MetricBase

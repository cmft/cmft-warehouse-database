﻿



CREATE view [Complaint].CategoryType as

select
	 SourceContextCode
	,SourceContext
	,SourceCategoryTypeID = SourceValueID
	,SourceCategoryTypeCode = SourceValueCode
	,SourceCategoryType = SourceValue
	,LocalCategoryTypeID = LocalValueID
	,LocalCategoryTypeCode = LocalValueCode
	,LocalCategoryType = LocalValue
	,NationalCategoryTypeID = NationalValueID
	,NationalCategoryTypeCode = NationalValueCode
	,NationalCategoryType = NationalValue
	FROM
		WH.Member
	where
		AttributeCode = 'COMPCATTYPE'






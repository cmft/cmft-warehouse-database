﻿CREATE view [Complaint].[Complaint]

as

select
	 BaseComplaint.MergeComplaintRecno
	,ComplaintRecno
	,SourceComplaintCode
	,CaseResolved = cast(
						case 
						when BaseComplaint.ResolutionDate is null then 0
						else 1
						end
						as bit
						)
	,CaseNumber
	,ReceiptDateID
	,ReceiptDate
	,AcknowledgementDate
	,ComplaintDetail = '' --ComplaintDetail --ComplaintDetail = null
	,ResolutionDate
	,OutcomeDetail = '' --OutcomeDetail --OutcomeDetail = null
	,Satisfied
	,ResponseDateID
	,ResponseDate
	,ResponseDueDate
	,LetterDate
	,GradeID
	,GradeCode
	,CaseTypeID
	,CaseTypeCode
	,ConsentRequired
	,ConsentDate
	,EventDate
	,InitialContactDate
	,InitialDueDate
	,SourceTargetDate
	,SequenceNo
	,CategoryID
	,CategoryCode
	,CategoryTypeID
	,CategoryTypeCode
	,PrimaryOrSecondary
	,SourceDivision = SourceDivision.Division
	,SourceDirectorate = SourceDirectorate.Directorate
	,Directorate.DirectorateID
	,BaseComplaint.DirectorateCode
	,DepartmentID
	,DepartmentCode
	,Reopened = cast(coalesce(BaseComplaint.Reopened, 0) as bit)
	,ReopenedDetail = ''
	,ConsentTargetDateID
	,ConsentTargetDate
	,ConsentTargetMet = case 
						when ConsentDate <= ConsentTargetDate
						then 1
						else 0
						end
	,TargetDate7DayID
	,TargetDate7Day
	,TargetDate14DayID
	,TargetDate14Day
	,TargetDate25DayID
	,TargetDate25Day
	,TargetDate40DayID
	,TargetDate40Day
	,Duration --= datediff(day, ReceiptDate, coalesce(ResponseDate, cast(getdate() as date)))
	,InterfaceCode
	,ContextID
	,ContextCode
from
	Complaint.BaseComplaint

inner join Complaint.BaseComplaintReference
on BaseComplaintReference.MergeComplaintRecno = BaseComplaint.MergeComplaintRecno

left join WH.Directorate
on	Directorate.DirectorateCode = BaseComplaint.DirectorateCode

left join Complaint.Division SourceDivision
on	SourceDivision.DivisionCode = BaseComplaint.SourceDivisionCode

left join Complaint.Directorate SourceDirectorate
on	SourceDirectorate.DirectorateCode = BaseComplaint.SourceDirectorateCode


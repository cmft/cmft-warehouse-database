﻿CREATE TABLE [Complaint].[BaseComplaintReference](
	[MergeComplaintRecno] [int] NOT NULL,
	[ContextID] [int] NOT NULL,
	[ReceiptDateID] [int] NULL,
	[CaseTypeID] [int] NULL,
	[CategoryID] [int] NULL,
	[CategoryTypeID] [int] NULL,
	[GradeID] [int] NULL,
	[DirectorateID] [int] NULL,
	[DepartmentID] [int] NULL,
	[WardID] [int] NULL,
	[ResponseDateID] [int] NULL,
	[ConsentTargetDateID] [int] NULL,
	[TargetDate7DayID] [int] NULL,
	[TargetDate14DayID] [int] NULL,
	[TargetDate25DayID] [int] NULL,
	[TargetDate40DayID] [int] NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](255) NULL,
 CONSTRAINT [PK__BaseIncidentReference_MergeEncounterRecno] PRIMARY KEY CLUSTERED 
(
	[MergeComplaintRecno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



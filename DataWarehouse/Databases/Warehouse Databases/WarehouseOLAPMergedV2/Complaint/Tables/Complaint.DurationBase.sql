﻿CREATE TABLE [Complaint].[DurationBase](
	[DurationID] [int] IDENTITY(1,1) NOT NULL,
	[DurationCode] [varchar](10) NOT NULL,
	[DurationValue] [int] NOT NULL,
	[Duration] [varchar](50) NOT NULL,
	[DurationBandCode] [varchar](10) NOT NULL,
	[DurationBand] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DurationBase_1] PRIMARY KEY CLUSTERED 
(
	[DurationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

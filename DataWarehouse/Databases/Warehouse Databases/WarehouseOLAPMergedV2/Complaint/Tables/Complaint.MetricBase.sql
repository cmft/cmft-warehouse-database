﻿CREATE TABLE [Complaint].[MetricBase] (
    [MetricID]         INT          IDENTITY (1, 1) NOT NULL,
    [MetricCode]       VARCHAR (10) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentID]   INT          NULL,
    [MetricParentCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_MetricBase_1] PRIMARY KEY CLUSTERED ([MetricID] ASC)
);


﻿CREATE TABLE [Complaint].[FactComplaint] (
    [MergeComplaintRecno] INT NOT NULL,
    [ContextID]           INT NOT NULL,
    [ReceiptDateID]       INT NULL,
    [CaseTypeID]          INT NULL,
    [CategoryID]          INT NULL,
    [CategoryTypeID]      INT NULL,
    [WardID]              INT NULL,
    [LocationID]          INT NOT NULL,
    [25DayTargetDateID]   INT NULL,
    [40DayTargetDateID]   INT NULL,
    [DirectorateID]       INT NOT NULL,
    [Cases]               INT NOT NULL
);


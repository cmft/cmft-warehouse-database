﻿CREATE TABLE [COM].[BaseEncounter] (
    [MergeEncounterRecno]                    INT           IDENTITY (1, 1) NOT NULL,
    [SourceEncounterRecno]                   INT           NULL,
    [SourceEncounterID]                      NUMERIC (19)  NULL,
    [SourceUniqueID]                         NUMERIC (10)  NULL,
    [SpecialtyID]                            NUMERIC (10)  NULL,
    [StaffTeamID]                            NUMERIC (10)  NULL,
    [ProfessionalCarerID]                    NUMERIC (10)  NULL,
    [SeenByProfessionalCarerID]              NUMERIC (10)  NULL,
    [EncounterProfessionalCarerID]           NUMERIC (10)  NULL,
    [StartDate]                              DATETIME      NULL,
    [StartTime]                              DATETIME      NULL,
    [EndTime]                                DATETIME      NULL,
    [ArrivedTime]                            DATETIME      NULL,
    [SeenTime]                               DATETIME      NULL,
    [DepartedTime]                           DATETIME      NULL,
    [AttendedID]                             NUMERIC (10)  NULL,
    [OutcomeID]                              VARCHAR (30)  NULL,
    [EncounterOutcomeCode]                   VARCHAR (25)  NULL,
    [EncounterDuration]                      NUMERIC (10)  NULL,
    [ReferralID]                             NUMERIC (10)  NULL,
    [ProfessionalCarerEpisodeID]             NUMERIC (10)  NULL,
    [PatientSourceID]                        NUMERIC (10)  NULL,
    [PatientSourceSystemUniqueID]            VARCHAR (20)  NULL,
    [PatientNHSNumber]                       VARCHAR (20)  NULL,
    [PatientNHSNumberStatusIndicator]        CHAR (1)      NULL,
    [PatientIDLocalIdentifier]               VARCHAR (20)  NULL,
    [PatientIDPASNumber]                     VARCHAR (20)  NULL,
    [PatientIDPatientIdentifier]             VARCHAR (20)  NULL,
    [PatientIDPatientFacilityID]             VARCHAR (20)  NULL,
    [PatientTitleID]                         NUMERIC (20)  NULL,
    [PatientForename]                        VARCHAR (30)  NULL,
    [PatientSurname]                         VARCHAR (30)  NULL,
    [PatientAddress1]                        VARCHAR (50)  NULL,
    [PatientAddress2]                        VARCHAR (50)  NULL,
    [PatientAddress3]                        VARCHAR (50)  NULL,
    [PatientAddress4]                        VARCHAR (50)  NULL,
    [PatientPostcode]                        VARCHAR (25)  NULL,
    [PatientDateOfBirth]                     DATETIME      NULL,
    [PatientDateOfDeath]                     DATETIME      NULL,
    [PatientSexID]                           NUMERIC (10)  NULL,
    [PatientEthnicGroupID]                   NUMERIC (10)  NULL,
    [PatientSpokenLanguageID]                NUMERIC (10)  NULL,
    [PatientReligionID]                      NUMERIC (10)  NULL,
    [PatientMaritalStatusID]                 NUMERIC (10)  NULL,
    [PatientCurrentRegisteredPracticeCode]   VARCHAR (10)  NULL,
    [PatientEncounterRegisteredPracticeCode] VARCHAR (10)  NULL,
    [PatientCurrentRegisteredGPCode]         VARCHAR (10)  NULL,
    [PatientEncounterRegisteredGPCode]       VARCHAR (10)  NULL,
    [VisitID]                                NUMERIC (10)  NULL,
    [ContactTypeID]                          NUMERIC (10)  NULL,
    [EncounterTypeID]                        NUMERIC (10)  NULL,
    [ScheduleTypeID]                         NUMERIC (10)  NULL,
    [ScheduleReasonID]                       NUMERIC (10)  NULL,
    [CanceledReasonID]                       NUMERIC (10)  NULL,
    [CanceledTime]                           DATETIME      NULL,
    [CancelledByID]                          NUMERIC (10)  NULL,
    [MoveReasonID]                           NUMERIC (10)  NULL,
    [MoveTime]                               DATETIME      NULL,
    [ServicePointID]                         NUMERIC (10)  NULL,
    [ServicePointSessionID]                  NUMERIC (10)  NULL,
    [MoveCount]                              NUMERIC (4)   NULL,
    [LocationTypeID]                         NUMERIC (10)  NULL,
    [LocationDescription]                    VARCHAR (255) NULL,
    [LocationTypeHealthOrgID]                NUMERIC (10)  NULL,
    [EncounterLocationID]                    NUMERIC (20)  NULL,
    [ServicePointStaysID]                    NUMERIC (10)  NULL,
    [WaitingListID]                          NUMERIC (10)  NULL,
    [PlannedAttendees]                       NUMERIC (5)   NULL,
    [ActualAttendees]                        NUMERIC (5)   NULL,
    [ProviderSpellID]                        NUMERIC (10)  NULL,
    [RTTStatusID]                            NUMERIC (10)  NULL,
    [EarliestReasinableOfferTime]            DATETIME      NULL,
    [EncounterInterventionsCount]            NUMERIC (10)  NULL,
    [CreatedTime]                            DATETIME      NULL,
    [ModifiedTime]                           DATETIME      NULL,
    [CreatedBy]                              VARCHAR (30)  NULL,
    [ModifiedBy]                             VARCHAR (30)  NULL,
    [Archived]                               VARCHAR (1)   NULL,
    [ParentID]                               NUMERIC (10)  NULL,
    [HealthOrgOwnerID]                       NUMERIC (10)  NULL,
    [RecordStatus]                           VARCHAR (1)   NULL,
    [JointActivityFlag]                      BIT           NULL,
    [Comment]                                VARCHAR (255) NULL,
    [DerivedFirstAttendanceFlag]             TINYINT       NULL,
    [ReferralReceivedTime]                   DATETIME      NULL,
    [ReferralReceivedDate]                   DATE          NULL,
    [ReferralClosedTime]                     DATETIME      NULL,
    [ReferralClosedDate]                     DATE          NULL,
    [ReferralReasonID]                       NUMERIC (10)  NULL,
    [ReferredToSpecialtyID]                  NUMERIC (10)  NULL,
    [ReferredToStaffTeamID]                  NUMERIC (10)  NULL,
    [ReferredToProfCarerID]                  NUMERIC (10)  NULL,
    [ReferralSourceID]                       NUMERIC (10)  NULL,
    [ReferralTypeID]                         NUMERIC (10)  NULL,
    [CommissionerCode]                       VARCHAR (3)   NULL,
    [PctOfResidence]                         VARCHAR (3)   NULL,
    [LocalAuthorityCode]                     VARCHAR (4)   NULL,
    [DistrictOfResidence]                    VARCHAR (3)   NULL,
    [ContextCode]                            VARCHAR (8)   NOT NULL,
    [PatientEncounterRegisteredPracticeID]   NUMERIC (10)  NULL,
    [AgeCode]                                VARCHAR (50)  NULL,
    [Created]                                DATETIME      NULL,
    [Updated]                                DATETIME      NULL,
    [ByWhom]                                 VARCHAR (255) NULL,
    [DNAReasonID]                            NUMERIC (10)  NULL,
    [WaitDays]                               INT           NULL,
    [CalledTime]                             DATETIME      NULL,
    CONSTRAINT [PK_BaseEncounter] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Base_5]
    ON [COM].[BaseEncounter]([ReferralID] ASC, [ContextCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COM_BaseEncounter_1]
    ON [COM].[BaseEncounter]([ContextCode] ASC)
    INCLUDE([MergeEncounterRecno], [Updated], [SpecialtyID], [StaffTeamID], [ProfessionalCarerID], [StartDate], [PatientSexID], [PatientEthnicGroupID], [PatientSpokenLanguageID], [PatientReligionID], [PatientMaritalStatusID], [EncounterTypeID], [EncounterLocationID], [CreatedBy], [ModifiedBy], [ReferralReasonID], [ReferredToSpecialtyID], [ReferralSourceID], [ReferralTypeID], [PatientEncounterRegisteredPracticeID], [AgeCode]);


GO
CREATE NONCLUSTERED INDEX [IX_BaseEncounter]
    ON [COM].[BaseEncounter]([Updated] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COM_BE-ARSJAFSD]
    ON [COM].[BaseEncounter]([Archived] ASC, [RecordStatus] ASC, [JointActivityFlag] ASC, [StartDate] ASC)
    INCLUDE([MergeEncounterRecno], [StaffTeamID], [ProfessionalCarerID], [SeenByProfessionalCarerID], [EndTime], [ArrivedTime], [SeenTime], [DepartedTime], [EncounterOutcomeCode], [PatientNHSNumber], [PatientPostcode], [PatientEthnicGroupID], [PatientEncounterRegisteredPracticeCode], [ContactTypeID], [ScheduleTypeID], [CanceledReasonID], [RTTStatusID], [ContextCode], [DNAReasonID]);


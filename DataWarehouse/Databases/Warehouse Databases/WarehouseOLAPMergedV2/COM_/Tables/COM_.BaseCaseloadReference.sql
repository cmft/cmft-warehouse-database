﻿CREATE TABLE [COM].[BaseCaseloadReference] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [AllocationDateID]    INT           NULL,
    [AgeID]               INT           NULL,
    [EthnicCategoryID]    INT           NULL,
    [SexID]               INT           NULL,
    [ProfessionalCarerID] INT           NULL,
    [SpecialtyID]         INT           NULL,
    [StaffTeamID]         INT           NULL,
    [CreatedByID]         INT           NULL,
    [ModifiedByID]        INT           NULL,
    [DischargeDateID]     INT           NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseCaseloadReference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


﻿CREATE TABLE [COM].[BaseReferralAllocation] (
    [MergeEncounterRecno] INT NOT NULL,
    [ServiceID]           INT NULL,
    PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


﻿CREATE TABLE [COM].[EncounterMetricBase] (
    [MetricID]         INT          IDENTITY (1, 1) NOT NULL,
    [MetricCode]       VARCHAR (10) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentCode] VARCHAR (10) NULL
);


﻿CREATE TABLE [COM].[BaseEncounterAllocation] (
    [MergeEncounterRecno] INT NOT NULL,
    [ServiceID]           INT NULL,
    PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


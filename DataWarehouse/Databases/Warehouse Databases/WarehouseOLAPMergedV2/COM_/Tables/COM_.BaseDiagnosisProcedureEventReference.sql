﻿CREATE TABLE [COM].[BaseDiagnosisProcedureEventReference] (
    [MergeEncounterRecno]  INT           NOT NULL,
    [ContextID]            INT           NOT NULL,
    [DiagnosisProcedureID] INT           NOT NULL,
    [Created]              DATETIME      NULL,
    [Updated]              DATETIME      NULL,
    [ByWhom]               VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseDiagnosisProcedureEventReference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


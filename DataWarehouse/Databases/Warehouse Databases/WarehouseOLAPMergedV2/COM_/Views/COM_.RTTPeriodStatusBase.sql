﻿CREATE view [COM].[RTTPeriodStatusBase]

as

/****************************************************************************************
	View		: COM.RTTPeriodStatusBase
	Description : Community activity RTT period status from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	23/06/2014	Paul Egan		Inital Coding
*****************************************************************************************/


select
	RTTPeriodStatusID = RTTPeriodStatus.LookupID
	,RTTPeriodStatus = RTTPeriodStatus.LookupDescription
	,RTTPeriodStatusCode = RTTPeriodStatus.LookupCode
	,NHSRTTPeriodStatusCode = RTTPeriodStatus.LookupNHSCode
	,NationalRTTPeriodStatusCode = RTTPeriodStatus.LookupNatCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase RTTPeriodStatus
where
	RTTPeriodStatus.LookupTypeID = 'RTTST'
;

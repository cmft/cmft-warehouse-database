﻿CREATE view [COM].[ReferralReasonBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 ReferralReasonID = ReasonforReferral.LookupID
	,ReferralReasonCode = ReasonforReferral.LookupCode
	,ReferralReason  = ReasonforReferral.LookupDescription
from
	[$(Warehouse)].COM.LookupBase ReasonforReferral
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Referral
--	where
--		Referral.ReasonforReferralID = ReasonforReferral.LookupID
--	)

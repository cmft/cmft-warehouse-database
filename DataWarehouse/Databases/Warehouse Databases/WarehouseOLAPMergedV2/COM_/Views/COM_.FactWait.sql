﻿

CREATE view [COM].[FactWait] as

select
	 BaseWaitReference.MergeEncounterRecno
	,BaseWait.EncounterRecno
	,BaseWaitReference.ContextID
	,BaseWaitReference.CensusDateID
	,BaseWaitReference.ReferralReceivedDateID
	,BaseWaitReference.AgeID
	,BaseWaitReference.EthnicCategoryID
	,BaseWaitReference.MaritalStatusID
	,BaseWaitReference.SexID
	,BaseWaitReference.ProfessionalCarerID
	,BaseWaitReference.ReferralReasonID
	,BaseWaitReference.ReferralSourceID
	,BaseWaitReference.ReferralTypeID
	,BaseWaitReference.ReligionID
	,BaseWaitReference.SpecialtyID
	,BaseWaitReference.StaffTeamID
	,BaseWaitReference.CreatedByID
	,BaseWaitReference.ModifiedByID
	,BaseWaitReference.DurationID
	,WithAppointment = cast(BaseWait.WithAppointment as bit)
	,BookedBeyondBreach = cast(BaseWait.BookedBeyondBreach as bit)
	,NearBreach = cast(BaseWait.NearBreach as bit)
	,DerivedWaitFlag = cast(BaseWait.DerivedWaitFlag as bit)

	,BaseWait.LengthOfWait
	,DNAs = BaseWait.CountOfDNAs
	,HospitalCancellations = BaseWait.CountOfHospitalCancels
	,PatientCencellations = BaseWait.CountOfPatientCancels
	,ScheduleMoves = BaseWait.ScheduleMoveCount
from
	COM.BaseWaitReference

inner join COM.BaseWait
on	BaseWait.MergeEncounterRecno = BaseWaitReference.MergeEncounterRecno



﻿CREATE view [COM].[LocationTypeBase]

as

/****************************************************************************************
	View		: COM.LocationTypeBase
	Description : Community activity location types from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	20/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/


select
	LocationTypeID = LocationType.LookupID
	,LocationType = LocationType.LookupDescription
	,LocationTypeCode = LocationType.LookupCode
	,NHSLocationTypeCode = LocationType.LookupNHSCode
	,NationalLocationTypeCode = LocationType.LookupNatCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase LocationType
where
	LocationType.LookupTypeID = 'LOTYP'
;

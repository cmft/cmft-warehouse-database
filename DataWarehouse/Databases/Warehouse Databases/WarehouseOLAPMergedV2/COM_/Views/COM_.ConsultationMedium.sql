﻿


CREATE view [COM].[ConsultationMedium] as

select
	 Member.SourceValueID
	,ConsultationMediumID = Member.SourceValueCode
	,ConsultationMedium = Member.SourceValue
	,ContextCode = Member.SourceContextCode
	,ConsultationMedium.ConsultationMediumTypeGroup
	,ConsultationMedium.ConsultationMediumTypeGroupChannel
from
	WH.Member

left join COM.ConsultationMediumBase ConsultationMedium
on	ConsultationMedium.ConsultationMediumID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMCMM'






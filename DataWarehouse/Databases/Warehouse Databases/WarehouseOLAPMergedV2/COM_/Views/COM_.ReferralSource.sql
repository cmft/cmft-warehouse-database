﻿


CREATE view [COM].[ReferralSource] as

select
	 Member.SourceValueID
	,ReferralSourceID = Member.SourceValueCode
	,ReferralSource = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.ReferralSourceBase ReferralSource
--on	ReferralSource.ReferralSourceID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMREFSRCE'






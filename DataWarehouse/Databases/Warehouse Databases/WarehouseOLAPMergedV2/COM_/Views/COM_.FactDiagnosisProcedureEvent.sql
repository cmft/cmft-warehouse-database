﻿

CREATE view [COM].[FactDiagnosisProcedureEvent] as

select
	 FactEncounter.MergeEncounterRecno
	,FactEncounter.ContextID
	,FactEncounter.EncounterRecno
	,FactEncounter.SpecialtyID
	,FactEncounter.ProfessionalCarerID
	,FactEncounter.StaffTeamID
	,FactEncounter.LocationID
	,FactEncounter.StartDateID
	,FactEncounter.CreatedByID
	,FactEncounter.ModifiedByID
	,FactEncounter.ReferredToSpecialtyID
	,FactEncounter.ReferralReasonID
	,FactEncounter.ReferralTypeID
	,FactEncounter.ReferralSourceID
	,FactEncounter.SexID
	,FactEncounter.EthnicCategoryID
	,FactEncounter.ConsultationMediumID
	,FactEncounter.MaritalStatusID
	,FactEncounter.ReligionID
	,FactEncounter.SpokenLanguageID
	,FactEncounter.GPPracticeID
	,FactEncounter.AgeID
	,FactEncounter.MetricID
	,FactEncounter.JointActivityFlag
	,FactEncounter.StartTimeOfDayID

	,BaseDiagnosisProcedureEventReference.DiagnosisProcedureID
from
	COM.FactEncounter

inner join COM.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = FactEncounter.MergeEncounterRecno

inner join COM.BaseDiagnosisProcedureEvent
on	BaseDiagnosisProcedureEvent.EncounterSourceUniqueID = BaseEncounter.SourceUniqueID

inner join COM.BaseDiagnosisProcedureEventReference
on	BaseDiagnosisProcedureEventReference.MergeEncounterRecno = BaseDiagnosisProcedureEvent.MergeEncounterRecno



﻿


CREATE view [COM].[GPPractice] as

select
	 Member.SourceValueID
	,GPPracticeID = Member.SourceValueCode
	,GPPractice = GPPractice.GPPracticeCode + ' - ' + Member.SourceValue
	,ContextCode = Member.SourceContextCode
	,GPPractice.GPPracticeCode

	,PCTID = coalesce(PCT.HealthOrgID, -1)
	,PCTCode = coalesce(PCT.HealthOrgCode, 'N/A')
	,PCT = coalesce(PCT.HealthOrgCode + ' - ' + PCT.HealthOrg, 'N/A')

	,HealthAuthorityID = coalesce(HealthAuthority.HealthOrgID, -1)
	,HealthAuthorityCode = coalesce(HealthAuthority.HealthOrgCode, 'N/A')
	,HealthAuthority = coalesce(HealthAuthority.HealthOrg, 'N/A')

from
	WH.Member

--once configured, we will use the ReferenceMap database for all of this...
left join COM.GPPracticeBase GPPractice
on	GPPractice.GPPracticeID = Member.SourceValueCode

left join [$(Warehouse)].COM.HealthOrg PCT
on	PCT.HealthOrgID = GPPractice.GPPracticeParentCode

left join [$(Warehouse)].COM.HealthOrg HealthAuthority
on	HealthAuthority.HealthOrgID = PCT.HealthOrgParentID

where
	Member.AttributeCode = 'GPPRACTICE'






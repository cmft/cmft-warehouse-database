﻿create view [COM].[SystemUser] as

select
	 Member.SourceValueID
	,SystemUserID = Member.SourceValueCode
	,SystemUser.SystemUserCode
	,SystemUser = Member.SourceValue
	,SystemUser.Department
	,SystemUser.Email
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join COM.SystemUserBase SystemUser
on	SystemUser.SystemUserCode = Member.SourceValueCode

where
	Member.AttributeCode = 'SYSUSER'




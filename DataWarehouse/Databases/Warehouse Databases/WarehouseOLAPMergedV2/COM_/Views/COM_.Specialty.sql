﻿


CREATE view [COM].[Specialty] as

select
	 Member.SourceValueID
	,SpecialtyID = Member.SourceValueCode
	,Specialty.SpecialtyCode
	,Specialty = Member.SourceValue
	,Specialty.SpecialtyParentID
	,Specialty.SpecialtyHealthOrgOwner
	,ContextCode = Member.SourceContextCode
	,DirectorateID = coalesce(SpecialtyDirectorateMap.DirectorateID, -1)
	,Directorate = coalesce(Directorate.Directorate, 'N/A')
from
	WH.Member

left join COM.SpecialtyBase Specialty
on	Specialty.SpecialtyID = Member.SourceValueCode

left join COM.SpecialtyDirectorateMap
on	SpecialtyDirectorateMap.SpecialtyID = Specialty.SpecialtyID

left join WH.Directorate
on	Directorate.DirectorateID = SpecialtyDirectorateMap.DirectorateID

where
	Member.AttributeCode = 'COMSPEC'




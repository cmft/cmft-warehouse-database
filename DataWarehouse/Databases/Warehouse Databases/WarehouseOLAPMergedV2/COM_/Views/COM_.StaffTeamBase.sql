﻿create view [COM].[StaffTeamBase] as

select
	 StaffTeamID
	,StaffTeamCode
	,StaffTeam
	,StartTime
	,EndTime
	,StaffTeamDescription
	,StaffTeamSpecialtyID
	,TeamLeaderID
	,ArchiveFlag
	,StaffTeamHealthOrgOwner
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.StaffTeam


﻿
CREATE view [COM].[DiagnosisProcedureBase] as

select
	 DiagnosisProcedure.DiagnosisProcedureID
	,DiagnosisProcedure.DiagnosisProcedure
	,DiagnosisProcedure.DiagnosisProcedureCode
	,DiagnosisProcedure.DiagnosisProcedureTypeCode
	,DiagnosisProcedure.ArchiveFlag
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.DiagnosisProcedure




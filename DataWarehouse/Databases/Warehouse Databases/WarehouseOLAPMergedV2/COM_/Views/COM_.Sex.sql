﻿
CREATE view [COM].[Sex] as

select
	 Member.SourceValueID
	,SexID = Member.SourceValueCode
	,Sex = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join COM.SexBase Sex
on	Sex.SexID = Member.SourceValueCode

where
	Member.AttributeCode = 'SEX'
and	Member.SourceContextCode = 'CEN||IPM'



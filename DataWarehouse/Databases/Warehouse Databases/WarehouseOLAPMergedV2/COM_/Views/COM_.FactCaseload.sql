﻿


CREATE view [COM].[FactCaseload] as

select
	 BaseCaseloadReference.MergeEncounterRecno
	,BaseCaseloadReference.ContextID
	,EncounterRecno = BaseCaseload.MergeEncounterRecno
	,BaseCaseloadReference.SpecialtyID
	,BaseCaseloadReference.ProfessionalCarerID
	,BaseCaseloadReference.StaffTeamID
	,BaseCaseloadReference.CreatedByID
	,BaseCaseloadReference.ModifiedByID
	,EncounterDateID = BaseCaseloadReference.AllocationDateID
	,BaseCaseloadReference.SexID
	,BaseCaseloadReference.EthnicCategoryID
	,BaseCaseloadReference.AgeID
	,MetricBase.MetricID
from
	COM.BaseCaseloadReference

inner join COM.MetricBase
on	MetricBase.MetricCode = 'CLAL'

inner join COM.BaseCaseload
on	BaseCaseload.MergeEncounterRecno = BaseCaseloadReference.MergeEncounterRecno

where
	BaseCaseload.AllocationDate is not null

union all

select
	 BaseCaseloadReference.MergeEncounterRecno
	,BaseCaseloadReference.ContextID
	,EncounterRecno = BaseCaseload.MergeEncounterRecno
	,BaseCaseloadReference.SpecialtyID
	,BaseCaseloadReference.ProfessionalCarerID
	,BaseCaseloadReference.StaffTeamID
	,BaseCaseloadReference.CreatedByID
	,BaseCaseloadReference.ModifiedByID
	,EncounterDateID = BaseCaseloadReference.DischargeDateID
	,BaseCaseloadReference.SexID
	,BaseCaseloadReference.EthnicCategoryID
	,BaseCaseloadReference.AgeID
	,MetricBase.MetricID
from
	COM.BaseCaseloadReference

inner join COM.MetricBase
on	MetricBase.MetricCode = 'CLDI'

inner join COM.BaseCaseload
on	BaseCaseload.MergeEncounterRecno = BaseCaseloadReference.MergeEncounterRecno

where
	BaseCaseload.DischargeDate is not null



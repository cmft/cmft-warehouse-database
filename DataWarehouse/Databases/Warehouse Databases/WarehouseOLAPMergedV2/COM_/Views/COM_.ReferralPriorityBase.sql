﻿CREATE view [COM].[ReferralPriorityBase]

as

/****************************************************************************************
	View		: COM.ReferralPriority
	Description : Community referral priority from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	26/06/2014	Paul Egan		Inital Coding
*****************************************************************************************/


select
	ReferralPriorityID = ReferralPriority.LookupID
	,ReferralPriority = ReferralPriority.LookupDescription
	,ReferralPriorityCode = ReferralPriority.LookupCode
	,NHSReferralPriorityCode = ReferralPriority.LookupNHSCode
	,NationalReferralPriorityCode = ReferralPriority.LookupNatCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase ReferralPriority
where
	ReferralPriority.LookupTypeID = 'PRITY'
;

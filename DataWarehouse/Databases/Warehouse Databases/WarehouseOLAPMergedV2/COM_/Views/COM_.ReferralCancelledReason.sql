﻿create view [COM].[ReferralCancelledReason] as

select
	 Member.SourceValueID
	,ReferralCancelledReasonID = Member.SourceValueCode
	,ReferralCancelledReason = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.ReferralCancelledReasonBase ReferralCancelledReason
--on	ReferralCancelledReason.ReferralCancelledReasonID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMCANR'






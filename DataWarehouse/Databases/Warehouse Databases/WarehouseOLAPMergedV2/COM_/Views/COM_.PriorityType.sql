﻿
create view COM.PriorityType 


as

select
	 ReferralPriorityTypeID = ReferralType.LookupID
	,ReferralPriorityTypeCode = ReferralType.LookupCode
	,ReferralPriorityType  = ReferralType.LookupDescription
from
	[$(Warehouse)].COM.LookupBase ReferralType

	where ReferralType.LookupTypeID = 'PRITY'


	
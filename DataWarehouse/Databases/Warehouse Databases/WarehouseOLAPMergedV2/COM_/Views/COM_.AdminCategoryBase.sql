﻿create view [COM].[AdminCategoryBase] as

select
	 AdminCategoryID = AdminCategory.LookupID
	,AdminCategory = AdminCategory.LookupDescription
	,AdminCategoryCode = AdminCategory.LookupCode
	,NationalAdminCategoryCode = AdminCategory.LookupNHSCode
	,CDSAdminCategoryCode = AdminCategory.LookupCDSCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase AdminCategory
where
	AdminCategory.LookupTypeID = 'ADCAT'



﻿create view [COM].[PatientClassificationBase] as

select
	 PatientClassificationID = PatientClassification.LookupID
	,PatientClassification = PatientClassification.LookupDescription
	,PatientClassificationCode = PatientClassification.LookupCode
	,NationalPatientClassificationCode = PatientClassification.LookupNHSCode
	,CDSPatientClassificationCode = PatientClassification.LookupCDSCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase PatientClassification
where
	PatientClassification.LookupTypeID = 'PATCL'



﻿create view [COM].[AdmissionSourceBase] as

select
	 AdmissionSourceID = AdmissionSource.LookupID
	,AdmissionSource = AdmissionSource.LookupDescription
	,AdmissionSourceCode = AdmissionSource.LookupCode
	,NationalAdmissionSourceCode = AdmissionSource.LookupNHSCode
	,CDSAdmissionSourceCode = AdmissionSource.LookupCDSCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase AdmissionSource
where
	AdmissionSource.LookupTypeID = 'ADSOR'



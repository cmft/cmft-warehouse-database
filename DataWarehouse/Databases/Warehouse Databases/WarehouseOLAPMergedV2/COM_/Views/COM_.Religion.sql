﻿
create view [COM].[Religion] as

select
	 Member.SourceValueID
	,ReligionID = Member.SourceValueCode
	,Religion = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join COM.ReligionBase Religion
on	Religion.ReligionID = Member.SourceValueCode

where
	Member.AttributeCode = 'RELIGION'
and	Member.SourceContextCode = 'CEN||IPM'



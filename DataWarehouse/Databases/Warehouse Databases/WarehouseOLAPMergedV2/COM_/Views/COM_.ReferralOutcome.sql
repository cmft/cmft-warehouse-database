﻿create view [COM].[ReferralOutcome] as

select
	 Member.SourceValueID
	,ReferralOutcomeID = Member.SourceValueCode
	,ReferralOutcome = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.ReferralOutcomeBase ReferralOutcome
--on	ReferralOutcome.ReferralOutcomeID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMOUT'








﻿create view [COM].[RequestedService] as

select
	 Member.SourceValueID
	,RequestedServiceID = Member.SourceValueCode
	,RequestedService = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.RequestedServiceBase RequestedService
--on	RequestedService.RequestedServiceID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMRS'






﻿CREATE view [COM].[ReferralClosureReasonBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 ReferralClosureReasonID = ReferralClosureReason.LookupID
	,ReferralClosureReasonCode = ReferralClosureReason.LookupCode
	,ReferralClosureReason  = ReferralClosureReason.LookupDescription
from
	[$(Warehouse)].COM.LookupBase ReferralClosureReason
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Referral
--	where
--		Referral.ClosureReasonID = ReferralClosureReason.LookupID
--	)

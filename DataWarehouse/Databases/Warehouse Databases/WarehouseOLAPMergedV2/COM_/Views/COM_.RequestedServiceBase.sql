﻿CREATE view [COM].[RequestedServiceBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 RequestedServiceID = RequestedService.LookupID
	,RequestedServiceCode = RequestedService.LookupCode
	,RequestedService  = RequestedService.LookupDescription
from
	[$(Warehouse)].COM.LookupBase RequestedService
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Referral
--	where
--		Referral.RequestedServiceID = RequestedService.LookupID
--	)

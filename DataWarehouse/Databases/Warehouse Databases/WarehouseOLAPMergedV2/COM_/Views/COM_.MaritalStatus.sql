﻿
create view [COM].[MaritalStatus] as

select
	 Member.SourceValueID
	,MaritalStatusID = Member.SourceValueCode
	,MaritalStatus = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join COM.MaritalStatusBase MaritalStatus
on	MaritalStatus.MaritalStatusID = Member.SourceValueCode

where
	Member.AttributeCode = 'MARSTA'
and	Member.SourceContextCode = 'CEN||IPM'



﻿


CREATE view [COM].[ProfessionalCarerBase] as

select
	 ProfessionalCarer.ProfessionalCarerID
	,ProfessionalCarer.ProfessionalCarerCode
	,ProfessionalCarer = ProfessionalCarer.FullName
	,ProfessionalCarer.ProfessionalCarerTypeID
	,ProfessionalCarer.Title
	,ProfessionalCarer.Forename
	,ProfessionalCarer.Surname
	,ProfessionalCarer.StartTime
	,ProfessionalCarer.EndTime
	,ProfessionalCarer.PrimaryStaffTeamID
	,ProfessionalCarer.ArchiveFlag
	,ProfessionalCarer.HealthOrgOwner

	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.ProfessionalCarer



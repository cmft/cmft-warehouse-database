﻿CREATE view [COM].[EthnicCategoryBase]

as

/****************************************************************************************
	View		: COM.EthnicCategoryBase
	Description : Community activity ethnic categories from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	23/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/


select
	EthnicCategoryID = EthnicCategory.LookupID
	,EthnicCategory = EthnicCategory.LookupDescription
	,EthnicCategoryCode = EthnicCategory.LookupCode
	,NHSEthnicCategoryCode = EthnicCategory.LookupNHSCode
	,NationalEthnicCategoryCode = EthnicCategory.LookupNatCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase EthnicCategory
where
	EthnicCategory.LookupTypeID = 'ETHGR'
;

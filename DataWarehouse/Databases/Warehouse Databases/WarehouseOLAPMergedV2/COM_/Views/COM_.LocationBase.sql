﻿

CREATE view [COM].[LocationBase] as

select
	 LocationID = Location.LocationCode
	,Location.Location
	,Location.LocationClinicCode
	,Location.LocationClinic
	,Location.LocationType
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.Location


﻿create view COM.ConsultationMediumBase as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 ConsultationMediumID = ConsultationMedium.Code
	,ConsultationMediumCode = ConsultationMedium.Code
	,ConsultationMedium  = ConsultationMedium.Type
	,ConsultationMediumTypeGroup  = ConsultationMedium.TypeGroup
	,ConsultationMediumTypeGroupChannel  = ConsultationMedium.TypeGroupChannel
from
	[$(Warehouse)].COM.CaseType ConsultationMedium


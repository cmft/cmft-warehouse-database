﻿CREATE view [COM].[CaseTypeBase]

as

/****************************************************************************************
	View		: COM.CaseTypeBase
	Description : Community case type (contact method / consultation medium) from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	10/07/2014	Paul Egan		Inital Coding
*****************************************************************************************/


select
	CaseTypeID = CaseType.Code
	,CaseType = CaseType.[Type]
	,CaseTypeGroupChannel = CaseType.TypeGroupChannel
	,CaseTypeGroup = CaseType.TypeGroup
	,CaseTypeCode = NULL
	,NHSCaseTypeCode = NULL
	,NationalCaseTypeCode = NULL
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.CaseType CaseType
;

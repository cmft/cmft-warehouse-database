﻿CREATE view [COM].[AttendanceStatusBase]

as

/****************************************************************************************
	View : COM.AttendanceStatusBase
	Description : Community attendance status from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	19/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/


select
	AttendanceStatusID = AttendanceStatus.LookupID
	,AttendanceStatus = AttendanceStatus.LookupDescription
	,AttendanceStatusCode = AttendanceStatus.LookupCode
	,NHSAttendanceStatusCode = AttendanceStatus.LookupNHSCode
	,NationalAttendanceStatusCode = AttendanceStatus.LookupNatCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase AttendanceStatus
where
	AttendanceStatus.LookupTypeID = 'ATTND'

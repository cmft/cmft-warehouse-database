﻿
CREATE view [COM].[SpecialtyDirectorateMap] as

select
	 SpecialtyID = Service.SPECT_REFNO
	,Directorate.DirectorateID
from
	[$(Warehouse)].COM.Service

left join WH.Directorate
on	Directorate.DirectorateCode =
	case
	when Service.Directorate = 'Adults' then 'CA'
	when Service.Directorate = 'Childrens' then 'CC'
	when Service.Directorate = 'Specialist' then 'CS'
	else 'N/A'
	end

where
	Service.TCSDestinationID = 2
and	Service.SPECT_REFNO is not null


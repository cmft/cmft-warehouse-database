﻿create view [COM].[AdmissionMethodBase] as

select
	 AdmissionMethodID = AdmissionMethod.LookupID
	,AdmissionMethod = AdmissionMethod.LookupDescription
	,AdmissionMethodCode = AdmissionMethod.LookupCode
	,NationalAdmissionMethodCode = AdmissionMethod.LookupNHSCode
	,CDSAdmissionMethodCode = AdmissionMethod.LookupCDSCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase AdmissionMethod
where
	AdmissionMethod.LookupTypeID = 'ADMET'



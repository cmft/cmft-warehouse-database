﻿CREATE view [COM].[ReferralOutcomeBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 ReferralOutcomeID = ReferralOutcome.LookupID
	,ReferralOutcomeCode = ReferralOutcome.LookupCode
	,ReferralOutcome  = ReferralOutcome.LookupDescription
from
	[$(Warehouse)].COM.LookupBase ReferralOutcome
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Referral
--	where
--		Referral.OutcomeOfReferral = ReferralOutcome.LookupID
--	)

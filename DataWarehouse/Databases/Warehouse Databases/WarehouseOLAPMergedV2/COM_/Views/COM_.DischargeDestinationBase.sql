﻿create view [COM].[DischargeDestinationBase] as

select
	 DischargeDestinationID = DischargeDestination.LookupID
	,DischargeDestination = DischargeDestination.LookupDescription
	,DischargeDestinationCode = DischargeDestination.LookupCode
	,NationalDischargeDestinationCode = DischargeDestination.LookupNHSCode
	,CDSDischargeDestinationCode = DischargeDestination.LookupCDSCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase DischargeDestination
where
	DischargeDestination.LookupTypeID = 'DISDE'

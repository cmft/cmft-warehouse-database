﻿


create view [COM].[DiagnosisProcedure] as

select
	 Member.SourceValueID
	,DiagnosisProcedureID = Member.SourceValueCode
	,DiagnosisProcedure = Member.SourceValue
	,DiagnosisProcedure.DiagnosisProcedureCode
	,DiagnosisProcedure.DiagnosisProcedureTypeCode
	,DiagnosisProcedure.ArchiveFlag
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join COM.DiagnosisProcedureBase DiagnosisProcedure
on	DiagnosisProcedure.DiagnosisProcedureID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMDP'




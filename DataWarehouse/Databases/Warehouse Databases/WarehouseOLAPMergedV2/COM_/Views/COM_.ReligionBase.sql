﻿CREATE view [COM].[ReligionBase] as

select
	 ReligionID = Religion.LookupID
	,ReligionCode = Religion.LookupCode
	,Religion  = Religion.LookupDescription
from
	[$(Warehouse)].COM.LookupBase Religion
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Encounter
--	where
--		Encounter.PatientReligionID = Religion.LookupID
--	)

--or	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Wait
--	where
--		Wait.PatientReligionID = Religion.LookupID
--	)

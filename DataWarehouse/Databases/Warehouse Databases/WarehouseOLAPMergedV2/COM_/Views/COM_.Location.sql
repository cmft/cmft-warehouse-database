﻿

CREATE view [COM].[Location] as

select
	 Member.SourceValueID
	,LocationID = Member.SourceValueCode
	,Location = Member.SourceValue
	,Location.LocationClinicCode
	,Location.LocationClinic
	,Location.LocationType
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join COM.LocationBase Location
on	Location.LocationID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMLOC'



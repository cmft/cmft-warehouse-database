﻿

CREATE view [COM].[StaffTeam] as

select
	 Member.SourceValueID
	,StaffTeamID = Member.SourceValueCode
	,StaffTeam.StaffTeamCode
	,StaffTeam = Member.SourceValue
	,StaffTeam.StartTime
	,StaffTeam.EndTime
	,StaffTeam.StaffTeamDescription
	,StaffTeam.StaffTeamSpecialtyID
	,StaffTeam.TeamLeaderID
	,StaffTeam.ArchiveFlag
	,StaffTeam.StaffTeamHealthOrgOwner
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join COM.StaffTeamBase StaffTeam
on	StaffTeam.StaffTeamID = Member.SourceValueCode

where
	Member.AttributeCode = 'STAFFTEAM'


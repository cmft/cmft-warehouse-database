﻿
CREATE view [COM].[ReferralType] as

select
	 Member.SourceValueID
	,ReferralTypeID = Member.SourceValueCode
	,ReferralType = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.ReferralTypeBase ReferralType
--on	ReferralType.ReferralTypeID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMRT'





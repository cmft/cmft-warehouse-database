﻿CREATE view [COM].[SpokenLanguageBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 SpokenLanguageID = SpokenLanguage.LookupID
	,SpokenLanguageCode = SpokenLanguage.LookupCode
	,SpokenLanguage = SpokenLanguage.LookupDescription
from
	[$(Warehouse)].COM.LookupBase SpokenLanguage
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Encounter
--	where
--		Encounter.PatientSpokenLanguageID = SpokenLanguage.LookupID
--	)

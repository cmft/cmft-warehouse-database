﻿create view [COM].[AdmissionReasonBase] as

select
	 AdmissionReasonID = AdmissionReason.LookupID
	,AdmissionReason = AdmissionReason.LookupDescription
	,AdmissionReasonCode = AdmissionReason.LookupCode
	,NationalAdmissionReasonCode = AdmissionReason.LookupNHSCode
	,CDSAdmissionReasonCode = AdmissionReason.LookupCDSCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase AdmissionReason
where
	AdmissionReason.LookupTypeID = 'READM'

﻿CREATE view [COM].[CancellationReasonBase]

as

/****************************************************************************************
	View		: COM.CancellationReasonBase
	Description : Community cancellation reasons from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	20/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/


select
	CancellationReasonID = CancellationReason.LookupID
	,CancellationReason = CancellationReason.LookupDescription
	,CancellationReasonCode = CancellationReason.LookupCode
	,NHSCancellationReasonCode = CancellationReason.LookupNHSCode
	,NationalCancellationReasonCode = CancellationReason.LookupNatCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase CancellationReason
where
	CancellationReason.LookupTypeID = 'CANCR'
;

﻿CREATE view [COM].[ReferralSourceBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 ReferralSourceID = ReferralSource.LookupID
	,ReferralSourceCode = ReferralSource.LookupCode
	,ReferralSource  = ReferralSource.LookupDescription
from
	[$(Warehouse)].COM.LookupBase ReferralSource
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Referral
--	where
--		Referral.SourceofReferralID = ReferralSource.LookupID
--	)

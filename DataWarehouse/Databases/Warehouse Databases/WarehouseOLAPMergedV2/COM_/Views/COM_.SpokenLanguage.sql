﻿
CREATE view [COM].[SpokenLanguage] as

select
	 Member.SourceValueID
	,SpokenLanguageID = Member.SourceValueCode
	,SpokenLanguage = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.SpokenLanguageBase SpokenLanguage
--on	SpokenLanguage.SpokenLanguageID = Member.SourceValueCode

where
	Member.AttributeCode = 'LANGUAGE'






﻿CREATE view [COM].[ReferralTypeBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 ReferralTypeID = ReferralType.LookupID
	,ReferralTypeCode = ReferralType.LookupCode
	,ReferralType  = ReferralType.LookupDescription
from
	[$(Warehouse)].COM.LookupBase ReferralType
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Referral
--	where
--		Referral.TypeofReferralID = ReferralType.LookupID
--	)

﻿CREATE view [COM].[ReferralCancelledReasonBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 ReferralCancelledReasonID = ReferralCancelledReason.LookupID
	,ReferralCancelledReasonCode = ReferralCancelledReason.LookupCode
	,ReferralCancelledReason  = ReferralCancelledReason.LookupDescription
from
	[$(Warehouse)].COM.LookupBase ReferralCancelledReason
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Referral
--	where
--		Referral.CancelledReasonID = ReferralCancelledReason.LookupID
--	)

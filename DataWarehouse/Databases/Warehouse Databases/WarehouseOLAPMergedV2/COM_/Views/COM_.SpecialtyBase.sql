﻿


CREATE view [COM].[SpecialtyBase] as

select
	 SpecialtyID
	,SpecialtyCode
	,Specialty
	,SpecialtyParentID
	,SpecialtyHealthOrgOwner
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.Specialty



﻿create view [COM].[ReferralStatus] as

select
	 Member.SourceValueID
	,ReferralStatusID = Member.SourceValueCode
	,ReferralStatus = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.ReferralStatusBase ReferralStatus
--on	ReferralStatus.ReferralStatusID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMST'







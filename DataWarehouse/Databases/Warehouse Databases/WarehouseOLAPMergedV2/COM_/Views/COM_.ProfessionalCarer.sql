﻿




CREATE view [COM].[ProfessionalCarer] as

select
	 Member.SourceValueID
	,ProfessionalCarerID = Member.SourceValueCode
	,ProfessionalCarer.ProfessionalCarerCode
	,ProfessionalCarer = Member.SourceValue
	,ProfessionalCarer.ProfessionalCarerTypeID
	,ProfessionalCarer.Title
	,ProfessionalCarer.Forename
	,ProfessionalCarer.Surname
	,ProfessionalCarer.StartTime
	,ProfessionalCarer.EndTime
	,ProfessionalCarer.PrimaryStaffTeamID
	,ProfessionalCarer.ArchiveFlag
	,ProfessionalCarer.HealthOrgOwner
	,ProfessionalCarer.ContextCode
from
	WH.Member

inner join COM.ProfessionalCarerBase ProfessionalCarer
on	Member.SourceValueCode = ProfessionalCarer.ProfessionalCarerID
and Member.SourceContextCode = ProfessionalCarer.ContextCode 


where
	Member.AttributeCode = 'PROFCARER'





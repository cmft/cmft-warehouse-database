﻿


CREATE view [COM].[SexBase]

as

/****************************************************************************************
	View		: COM.SexBase
	Description : Community activity sex from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Inital Coding
	23/06/2014	Paul Egan       Added fields
*****************************************************************************************/


select
	SexID = Sex.LookupID
	,Sex = Sex.LookupDescription
	,SexCode = Sex.LookupCode
	,NHSSexCode = Sex.LookupNHSCode
	,NationalSexCode = Sex.LookupNatCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase Sex
where
	Sex.LookupTypeID = 'SEXXX'
;



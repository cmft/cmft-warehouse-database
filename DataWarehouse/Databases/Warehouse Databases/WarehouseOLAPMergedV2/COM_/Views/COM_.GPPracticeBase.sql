﻿create view COM.GPPracticeBase as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 GPPracticeID = GPPractice.HealthOrgID
	,GPPracticeCode = GPPractice.HealthOrgCode
	,GPPractice  = GPPractice.HealthOrg
	,GPPracticeParentCode  = GPPractice.HealthOrgParentID
from
	[$(Warehouse)].COM.HealthOrg GPPractice


﻿
CREATE view [COM].[FactWardStay] as

select
	 BaseWardStayReference.MergeEncounterRecno
	,BaseWardStayReference.ContextID
	,WardStay.EncounterRecno
	,EncounterDateID = BaseWardStayReference.AdmissionDateID
	,BaseWardStayReference.ProfessionalCarerID
	,BaseWardStayReference.SpecialtyID
	,BaseWardStayReference.GPPracticeID
	,BaseWardStayReference.SexID
	,BaseWardStayReference.EthnicCategoryID
	,BaseWardStayReference.AgeID
	,BaseWardStayReference.LocationID
	,BaseWardStayReference.AdmissionMethodID
	,BaseWardStayReference.AdmissionSourceID
	,BaseWardStayReference.PatientClassificationID
	,BaseWardStayReference.AdminCategoryID
	,BaseWardStayReference.ManagementIntentionID
	,BaseWardStayReference.AdmissionReasonID
	,BaseWardStayReference.DischargeDestinationID
	,BaseWardStayReference.DischargeMethodID
	,BaseWardStayReference.CreatedByID
	,BaseWardStayReference.ModifiedByID
	,MetricBase.MetricID
	,WardStay.LOS
from
	COM.BaseWardStayReference

inner join COM.BaseWardStay WardStay
on	WardStay.MergeEncounterRecno = BaseWardStayReference.MergeEncounterRecno

inner join COM.MetricBase
on	MetricBase.MetricCode = 'ADMI'

where
--first stay in spell
	case
	when not exists
		(
		select
			1
		from
			[$(Warehouse)].COM.WardStay Previous
		where
			Previous.ProviderSpellNo = WardStay.ProviderSpellNo
		and	(
				Previous.StartTime < WardStay.StartTime
			or	(
					Previous.StartTime = WardStay.StartTime
				and	Previous.SourceEncounterNo < WardStay.SourceEncounterNo
				)
			)
		)
	then 1
	else 0
	end = 1

union all

select
	 BaseWardStayReference.MergeEncounterRecno
	,BaseWardStayReference.ContextID
	,WardStay.EncounterRecno
	,BaseWardStayReference.DischargeDateID
	,BaseWardStayReference.ProfessionalCarerID
	,BaseWardStayReference.SpecialtyID
	,BaseWardStayReference.GPPracticeID
	,BaseWardStayReference.SexID
	,BaseWardStayReference.EthnicCategoryID
	,BaseWardStayReference.AgeID
	,BaseWardStayReference.LocationID
	,BaseWardStayReference.AdmissionMethodID
	,BaseWardStayReference.AdmissionSourceID
	,BaseWardStayReference.PatientClassificationID
	,BaseWardStayReference.AdminCategoryID
	,BaseWardStayReference.ManagementIntentionID
	,BaseWardStayReference.AdmissionReasonID
	,BaseWardStayReference.DischargeDestinationID
	,BaseWardStayReference.DischargeMethodID
	,BaseWardStayReference.CreatedByID
	,BaseWardStayReference.ModifiedByID
	,MetricBase.MetricID
	,WardStay.LOS
from
	COM.BaseWardStayReference

inner join COM.BaseWardStay WardStay
on	WardStay.MergeEncounterRecno = BaseWardStayReference.MergeEncounterRecno

inner join COM.MetricBase
on	MetricBase.MetricCode = 'DISC'

where
	WardStay.DischargeDate is not null
and
	case
	when WardStay.EndDate is null then 1
	when WardStay.EndDate = WardStay.DischargeDate then 1
	else 0
	end = 1

union all

select
	 BaseWardStayReference.MergeEncounterRecno
	,BaseWardStayReference.ContextID
	,WardStay.EncounterRecno
	,BaseWardStayReference.EndDateID
	,BaseWardStayReference.ProfessionalCarerID
	,BaseWardStayReference.SpecialtyID
	,BaseWardStayReference.GPPracticeID
	,BaseWardStayReference.SexID
	,BaseWardStayReference.EthnicCategoryID
	,BaseWardStayReference.AgeID
	,BaseWardStayReference.LocationID
	,BaseWardStayReference.AdmissionMethodID
	,BaseWardStayReference.AdmissionSourceID
	,BaseWardStayReference.PatientClassificationID
	,BaseWardStayReference.AdminCategoryID
	,BaseWardStayReference.ManagementIntentionID
	,BaseWardStayReference.AdmissionReasonID
	,BaseWardStayReference.DischargeDestinationID
	,BaseWardStayReference.DischargeMethodID
	,BaseWardStayReference.CreatedByID
	,BaseWardStayReference.ModifiedByID
	,MetricBase.MetricID
	,LOS = datediff(day, WardStay.StartDate, WardStay.EndDate)
from
	COM.BaseWardStayReference

inner join COM.BaseWardStay WardStay
on	WardStay.MergeEncounterRecno = BaseWardStayReference.MergeEncounterRecno

inner join COM.MetricBase
on	MetricBase.MetricCode = 'STAY'

where
	WardStay.EndDate is not null


union all

select
	 BaseWardStayReference.MergeEncounterRecno
	,BaseWardStayReference.ContextID
	,WardStay.EncounterRecno
	,BaseWardStayReference.DischargeDateID
	,BaseWardStayReference.ProfessionalCarerID
	,BaseWardStayReference.SpecialtyID
	,BaseWardStayReference.GPPracticeID
	,BaseWardStayReference.SexID
	,BaseWardStayReference.EthnicCategoryID
	,BaseWardStayReference.AgeID
	,BaseWardStayReference.LocationID
	,BaseWardStayReference.AdmissionMethodID
	,BaseWardStayReference.AdmissionSourceID
	,BaseWardStayReference.PatientClassificationID
	,BaseWardStayReference.AdminCategoryID
	,BaseWardStayReference.ManagementIntentionID
	,BaseWardStayReference.AdmissionReasonID
	,BaseWardStayReference.DischargeDestinationID
	,BaseWardStayReference.DischargeMethodID
	,BaseWardStayReference.CreatedByID
	,BaseWardStayReference.ModifiedByID
	,MetricBase.MetricID
	,WardStay.LOS
from
	COM.BaseWardStayReference

inner join COM.BaseWardStay WardStay
on	WardStay.MergeEncounterRecno = BaseWardStayReference.MergeEncounterRecno

inner join COM.MetricBase
on	MetricBase.MetricCode = 'SPEL'

where
	WardStay.DischargeDate is not null
and--first stay in spell
	case
	when not exists
		(
		select
			1
		from
			[$(Warehouse)].COM.WardStay Previous
		where
			Previous.ProviderSpellNo = WardStay.ProviderSpellNo
		and	(
				Previous.StartTime < WardStay.StartTime
			or	(
					Previous.StartTime = WardStay.StartTime
				and	Previous.SourceEncounterNo < WardStay.SourceEncounterNo
				)
			)
		)
	then 1
	else 0
	end = 1




﻿CREATE view [COM].[ActivityTypeBase]

as

/****************************************************************************************
	View		: COM.ActivityType
	Description : Community activity type from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	26/06/2014	Paul Egan		Inital Coding
*****************************************************************************************/


select
	ActivityTypeID = ActivityType.LookupID
	,ActivityType = ActivityType.LookupDescription
	,ActivityTypeCode = ActivityType.LookupCode
	,NHSActivityTypeCode = ActivityType.LookupNHSCode
	,NationalActivityTypeCode = ActivityType.LookupNatCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase ActivityType
where
	ActivityType.LookupTypeID = 'CCATC'
;

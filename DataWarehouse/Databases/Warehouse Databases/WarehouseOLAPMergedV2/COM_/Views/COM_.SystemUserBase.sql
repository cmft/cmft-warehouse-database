﻿create view [COM].[SystemUserBase] as

select
	 SystemUser.SystemUserCode
	,SystemUser.SystemUser
	,SystemUser.Department
	,SystemUser.Email
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.SystemUser




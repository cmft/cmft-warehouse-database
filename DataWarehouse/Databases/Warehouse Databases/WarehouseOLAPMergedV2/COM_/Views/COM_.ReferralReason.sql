﻿
CREATE view [COM].[ReferralReason] as

select
	 Member.SourceValueID
	,ReferralReasonID = Member.SourceValueCode
	,ReferralReason = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.ReferralReasonBase ReferralReason
--on	ReferralReason.ReferralReasonID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMRR'





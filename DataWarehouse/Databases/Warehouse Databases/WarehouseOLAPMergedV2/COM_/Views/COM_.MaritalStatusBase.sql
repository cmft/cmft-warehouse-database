﻿CREATE view [COM].[MaritalStatusBase] as

select
	 MaritalStatusID = MaritalStatus.LookupID
	,MaritalStatusCode = MaritalStatus.LookupCode
	,MaritalStatus  = MaritalStatus.LookupDescription
from
	[$(Warehouse)].COM.LookupBase MaritalStatus
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Encounter
--	where
--		Encounter.PatientMaritalStatusID = MaritalStatus.LookupID
--	)

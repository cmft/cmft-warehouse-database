﻿create view [COM].[ManagementIntentionBase] as

select
	 ManagementIntentionID = ManagementIntention.LookupID
	,ManagementIntention = ManagementIntention.LookupDescription
	,ManagementIntentionCode = ManagementIntention.LookupCode
	,NationalManagementIntentionCode = ManagementIntention.LookupNHSCode
	,CDSManagementIntentionCode = ManagementIntention.LookupCDSCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase ManagementIntention
where
	ManagementIntention.LookupTypeID = 'INMGT'

﻿CREATE view [COM].[ReferralClosureReason] as

select
	 Member.SourceValueID
	,ReferralClosureReasonID = Member.SourceValueCode
	,ReferralClosureReason = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

--left join COM.ReferralClosureReasonBase ReferralClosureReason
--on	ReferralClosureReason.ReferralClosureReasonID = Member.SourceValueCode

where
	Member.AttributeCode = 'COMRCR'






﻿create view COM.EncounterMetric as

select
	 EncounterMetricBase.MetricID
	,EncounterMetricBase.MetricCode
	,EncounterMetricBase.Metric
	,ParentMetricID = Parent.MetricID

from
	COM.EncounterMetricBase

left join COM.EncounterMetricBase Parent
on	Parent.MetricCode = EncounterMetricBase.MetricParentCode

﻿create view [COM].[DischargeMethodBase] as

select
	 DischargeMethodID = DischargeMethod.LookupID
	,DischargeMethod = DischargeMethod.LookupDescription
	,DischargeMethodCode = DischargeMethod.LookupCode
	,NationalDischargeMethodCode = DischargeMethod.LookupNHSCode
	,CDSDischargeMethodCode = DischargeMethod.LookupCDSCode
	,ContextCode = 'CEN||IPM'
from
	[$(Warehouse)].COM.LookupBase DischargeMethod
where
	DischargeMethod.LookupTypeID = 'DISMT'

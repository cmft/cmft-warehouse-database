﻿CREATE view [COM].[ReferralStatusBase] as

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
select
	 ReferralStatusID = ReferralStatus.LookupID
	,ReferralStatusCode = ReferralStatus.LookupCode
	,ReferralStatus  = ReferralStatus.LookupDescription
from
	[$(Warehouse)].COM.LookupBase ReferralStatus
--where
--	exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].COM.Referral
--	where
--		Referral.ReferralStatus = ReferralStatus.LookupID
--	)

﻿
CREATE procedure [COM].[BuildFactEncounter] as

truncate table COM.FactEncounter

insert
into
	COM.FactEncounter
(
	 MergeEncounterRecno
	,ContextID
	,EncounterRecno
	,SpecialtyID
	,ProfessionalCarerID
	,StaffTeamID
	,LocationID
	,StartDateID
	,CreatedByID
	,ModifiedByID
	,ReferredToSpecialtyID
	,ReferralReasonID
	,ReferralTypeID
	,ReferralSourceID
	,SexID
	,EthnicCategoryID
	,ConsultationMediumID
	,MaritalStatusID
	,ReligionID
	,SpokenLanguageID
	,GPPracticeID
	,AgeID
	,MetricID
	,JointActivityFlag
	,StartTimeOfDayID
	,ServiceID
	,EncounterDuration
	,Interventions
	,WaitDays
)
select
	 BaseEncounterReference.MergeEncounterRecno
	,BaseEncounterReference.ContextID
	,BaseEncounter.SourceEncounterRecno
	,BaseEncounterReference.SpecialtyID
	,BaseEncounterReference.ProfessionalCarerID
	,BaseEncounterReference.StaffTeamID
	,BaseEncounterReference.LocationID
	,BaseEncounterReference.StartDateID
	,BaseEncounterReference.CreatedByID
	,BaseEncounterReference.ModifiedByID
	,BaseEncounterReference.ReferredToSpecialtyID
	,BaseEncounterReference.ReferralReasonID
	,BaseEncounterReference.ReferralTypeID
	,BaseEncounterReference.ReferralSourceID
	,BaseEncounterReference.SexID
	,BaseEncounterReference.EthnicCategoryID
	,BaseEncounterReference.ConsultationMediumID
	,BaseEncounterReference.MaritalStatusID
	,BaseEncounterReference.ReligionID
	,BaseEncounterReference.SpokenLanguageID
	,BaseEncounterReference.GPPracticeID
	,BaseEncounterReference.AgeID
	,MetricID = EncounterMetric.MetricID
	,BaseEncounter.JointActivityFlag

	,StartTimeOfDayID =
		coalesce(
			datediff(
				 minute
				,cast(BaseEncounter.StartTime as date)
				,BaseEncounter.StartTime
			)
			,-1
		)
 	,ServiceID = Service.AllocationID
	,BaseEncounter.EncounterDuration
	,Interventions = BaseEncounter.EncounterInterventionsCount


	,BaseEncounter.WaitDays

from
	COM.BaseEncounterReference

inner join COM.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join 
	(
	select MetricCode = 'FATT', EncounterOutcomeCode = 'ATTD', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RATT', EncounterOutcomeCode = 'ATTD', DerivedFirstAttendanceFlag = 2 union all
	select MetricCode = 'FDNA', EncounterOutcomeCode = 'DNAT', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RDNA', EncounterOutcomeCode = 'DNAT', DerivedFirstAttendanceFlag = 2 union all
	select MetricCode = 'FCHOS', EncounterOutcomeCode = 'CHOS', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RCHOS', EncounterOutcomeCode = 'CHOS', DerivedFirstAttendanceFlag = 2 union all
	select MetricCode = 'FCPAT', EncounterOutcomeCode = 'CPAT', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RCPAT', EncounterOutcomeCode = 'CPAT', DerivedFirstAttendanceFlag = 2 union all
	select MetricCode = 'FCOTH', EncounterOutcomeCode = 'COTH', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RCOTH', EncounterOutcomeCode = 'COTH', DerivedFirstAttendanceFlag = 2 union all
	select MetricCode = 'FNSPD', EncounterOutcomeCode = 'NSPD', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RNSPD', EncounterOutcomeCode = 'NSPD', DerivedFirstAttendanceFlag = 2 union all
	select MetricCode = 'FPLND', EncounterOutcomeCode = 'PLND', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RPLND', EncounterOutcomeCode = 'PLND', DerivedFirstAttendanceFlag = 2 union all
	select MetricCode = 'FUACT', EncounterOutcomeCode = 'UACT', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RUACT', EncounterOutcomeCode = 'UACT', DerivedFirstAttendanceFlag = 2 union all
	select MetricCode = 'FPDCD', EncounterOutcomeCode = 'PDCD', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RPDCD', EncounterOutcomeCode = 'PDCD', DerivedFirstAttendanceFlag = 2
	) Metric
on	Metric.DerivedFirstAttendanceFlag = BaseEncounter.DerivedFirstAttendanceFlag
and Metric.EncounterOutcomeCode = BaseEncounter.EncounterOutcomeCode

inner join COM.EncounterMetric
on	EncounterMetric.MetricCode = Metric.MetricCode

inner join Allocation.DatasetAllocation Service
on	Service.AllocationTypeID = 2
and	Service.DatasetCode = 'COM'
and	Service.DatasetRecno = BaseEncounterReference.MergeEncounterRecno


union all

select
	 BaseEncounterReference.MergeEncounterRecno
	,BaseEncounterReference.ContextID
	,BaseEncounter.SourceEncounterRecno
	,BaseEncounterReference.SpecialtyID
	,BaseEncounterReference.ProfessionalCarerID
	,BaseEncounterReference.StaffTeamID
	,BaseEncounterReference.LocationID
	,BaseEncounterReference.StartDateID
	,BaseEncounterReference.CreatedByID
	,BaseEncounterReference.ModifiedByID
	,BaseEncounterReference.ReferredToSpecialtyID
	,BaseEncounterReference.ReferralReasonID
	,BaseEncounterReference.ReferralTypeID
	,BaseEncounterReference.ReferralSourceID
	,BaseEncounterReference.SexID
	,BaseEncounterReference.EthnicCategoryID
	,BaseEncounterReference.ConsultationMediumID
	,BaseEncounterReference.MaritalStatusID
	,BaseEncounterReference.ReligionID
	,BaseEncounterReference.SpokenLanguageID
	,BaseEncounterReference.GPPracticeID
	,BaseEncounterReference.AgeID
	,MetricID = EncounterMetric.MetricID
	,BaseEncounter.JointActivityFlag

	,StartTimeOfDayID =
		coalesce(
			datediff(
				 minute
				,cast(BaseEncounter.StartTime as date)
				,BaseEncounter.StartTime
			)
			,-1
		)
	,ServiceID = Service.AllocationID
	,BaseEncounter.EncounterDuration
	,Interventions = BaseEncounter.EncounterInterventionsCount
	
	,BaseEncounter.WaitDays

from
	COM.BaseEncounterReference

inner join COM.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

cross join 
	(
	select MetricCode = 'FACT', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RACT', DerivedFirstAttendanceFlag = 2
	) Metric


inner join COM.EncounterMetric
on	EncounterMetric.MetricCode = Metric.MetricCode

inner join Allocation.DatasetAllocation Service
on	Service.AllocationTypeID = 2
and	Service.DatasetCode = 'COM'
and	Service.DatasetRecno = BaseEncounterReference.MergeEncounterRecno

where 
	BaseEncounter.DerivedFirstAttendanceFlag = Metric.DerivedFirstAttendanceFlag

union all

select
	 BaseEncounterReference.MergeEncounterRecno
	,BaseEncounterReference.ContextID
	,BaseEncounter.SourceEncounterRecno
	,BaseEncounterReference.SpecialtyID
	,BaseEncounterReference.ProfessionalCarerID
	,BaseEncounterReference.StaffTeamID
	,BaseEncounterReference.LocationID
	,BaseEncounterReference.StartDateID
	,BaseEncounterReference.CreatedByID
	,BaseEncounterReference.ModifiedByID
	,BaseEncounterReference.ReferredToSpecialtyID
	,BaseEncounterReference.ReferralReasonID
	,BaseEncounterReference.ReferralTypeID
	,BaseEncounterReference.ReferralSourceID
	,BaseEncounterReference.SexID
	,BaseEncounterReference.EthnicCategoryID
	,BaseEncounterReference.ConsultationMediumID
	,BaseEncounterReference.MaritalStatusID
	,BaseEncounterReference.ReligionID
	,BaseEncounterReference.SpokenLanguageID
	,BaseEncounterReference.GPPracticeID
	,BaseEncounterReference.AgeID
	,MetricID = EncounterMetric.MetricID
	,BaseEncounter.JointActivityFlag

	,StartTimeOfDayID =
		coalesce(
			datediff(
				 minute
				,cast(BaseEncounter.StartTime as date)
				,BaseEncounter.StartTime
			)
			,-1
		)
	,ServiceID = Service.AllocationID
	,BaseEncounter.EncounterDuration
	,Interventions = BaseEncounter.EncounterInterventionsCount

	,BaseEncounter.WaitDays

from
	COM.BaseEncounterReference

inner join COM.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

cross join 
	(
	select MetricCode = 'FCANCEL', DerivedFirstAttendanceFlag = 1 union all
	select MetricCode = 'RCANCEL', DerivedFirstAttendanceFlag = 2
	) Metric


inner join COM.EncounterMetric
on	EncounterMetric.MetricCode = Metric.MetricCode

inner join Allocation.DatasetAllocation Service
on	Service.AllocationTypeID = 2
and	Service.DatasetCode = 'COM'
and	Service.DatasetRecno = BaseEncounterReference.MergeEncounterRecno

where 
	BaseEncounter.DerivedFirstAttendanceFlag = Metric.DerivedFirstAttendanceFlag
and	BaseEncounter.EncounterOutcomeCode in
	(
	 'COTH'
	,'CPAT'
	,'CHOS'
	)


union all

select
	 BaseEncounterReference.MergeEncounterRecno
	,BaseEncounterReference.ContextID
	,BaseEncounter.SourceEncounterRecno
	,BaseEncounterReference.SpecialtyID
	,BaseEncounterReference.ProfessionalCarerID
	,BaseEncounterReference.StaffTeamID
	,BaseEncounterReference.LocationID
	,BaseEncounterReference.StartDateID
	,BaseEncounterReference.CreatedByID
	,BaseEncounterReference.ModifiedByID
	,BaseEncounterReference.ReferredToSpecialtyID
	,BaseEncounterReference.ReferralReasonID
	,BaseEncounterReference.ReferralTypeID
	,BaseEncounterReference.ReferralSourceID
	,BaseEncounterReference.SexID
	,BaseEncounterReference.EthnicCategoryID
	,BaseEncounterReference.ConsultationMediumID
	,BaseEncounterReference.MaritalStatusID
	,BaseEncounterReference.ReligionID
	,BaseEncounterReference.SpokenLanguageID
	,BaseEncounterReference.GPPracticeID
	,BaseEncounterReference.AgeID
	,MetricID = EncounterMetric.MetricID
	,BaseEncounter.JointActivityFlag

	,StartTimeOfDayID =
		coalesce(
			datediff(
				 minute
				,cast(BaseEncounter.StartTime as date)
				,BaseEncounter.StartTime
			)
			,-1
		)
	,ServiceID = Service.AllocationID
	,BaseEncounter.EncounterDuration
	,Interventions = BaseEncounter.EncounterInterventionsCount

	
	,BaseEncounter.WaitDays

from
	COM.BaseEncounterReference

inner join COM.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join COM.EncounterMetric
on	EncounterMetric.MetricCode = 'CHKACT'

inner join Allocation.DatasetAllocation Service
on	Service.AllocationTypeID = 2
and	Service.DatasetCode = 'COM'
and	Service.DatasetRecno = BaseEncounterReference.MergeEncounterRecno





﻿

CREATE procedure [COM].[BuildFactReferral] as

truncate table COM.FactReferral

insert
into
	COM.FactReferral
(
	 MergeEncounterRecno
	,ContextID
	,EncounterRecno
	,ActivityDateID
	,ReferredByProfessionalCarerID
	,ReferredToProfessionalCarerID
	,ReferredByStaffTeamID
	,ReferredToStaffTeamID
	,ReferredBySpecialtyID
	,ReferredToSpecialtyID
	,ReferralCancelledReasonID
	,ReferralClosureReasonID
	,ReferralOutcomeID
	,RequestedServiceID
	,CreatedByID
	,ModifiedByID
	,ReferralReasonID
	,ReferralTypeID
	,ReferralStatusID
	,ReferralSourceID
	,SexID
	,EthnicCategoryID
	,AgeID
	,MetricID
	,ReferredByGPPracticeID
	,ReferredToGPPracticeID
	,ServiceID
)
select
	 BaseReferralReference.MergeEncounterRecno
	,BaseReferralReference.ContextID
	,BaseReferralReference.MergeEncounterRecno

	,ActivityDate =
		case

		when Fact.MetricCode = 'RREC'
		then BaseReferralReference.ReferralReceivedDateID

		when Fact.MetricCode = 'RCLO'
		then BaseReferralReference.ReferralClosedDateID

		when Fact.MetricCode = 'RCAN'
		then BaseReferralReference.ReferralCancelledDateID

		when Fact.MetricCode = 'RSEN'
		then BaseReferralReference.ReferralSentDateID

		when Fact.MetricCode = 'RAUT'
		then BaseReferralReference.ReferralAuthorisedDateID

		end

	,BaseReferralReference.ReferredByProfessionalCarerID
	,BaseReferralReference.ReferredToProfessionalCarerID
	,BaseReferralReference.ReferredByStaffTeamID
	,BaseReferralReference.ReferredToStaffTeamID
	,BaseReferralReference.ReferredBySpecialtyID
	,BaseReferralReference.ReferredToSpecialtyID
	,BaseReferralReference.ReferralCancelledReasonID
	,BaseReferralReference.ReferralClosureReasonID
	,BaseReferralReference.ReferralOutcomeID
	,BaseReferralReference.RequestedServiceID
	,BaseReferralReference.CreatedByID
	,BaseReferralReference.ModifiedByID
	,BaseReferralReference.ReferralReasonID
	,BaseReferralReference.ReferralTypeID
	,BaseReferralReference.ReferralStatusID
	,BaseReferralReference.ReferralSourceID
	,BaseReferralReference.SexID
	,BaseReferralReference.EthnicCategoryID
	,BaseReferralReference.AgeID
	,MetricID = Metric.MetricID
	,BaseReferralReference.ReferredByGPPracticeID
	,BaseReferralReference.ReferredToGPPracticeID
	,ServiceID = Service.AllocationID
from
	COM.BaseReferralReference

inner join
	(
	select
		 BaseReferral.MergeEncounterRecno
		,MetricCode = 'RREC'
	from
		COM.BaseReferral
	where
		BaseReferral.ReferralReceivedDate is not null

	union all

	select
		 BaseReferral.MergeEncounterRecno
		,MetricCode = 'RCLO'
	from
		COM.BaseReferral
	where
		BaseReferral.ReferralClosedDate is not null

	union all

	select
		 BaseReferral.MergeEncounterRecno
		,MetricCode = 'RCAN'
	from
		COM.BaseReferral
	where
		BaseReferral.CancelledDate is not null

	union all

	select
		 BaseReferral.MergeEncounterRecno
		,MetricCode = 'RSEN'
	from
		COM.BaseReferral
	where
		BaseReferral.ReferralSentDate is not null

	union all

	select
		 BaseReferral.MergeEncounterRecno
		,MetricCode = 'RAUT'
	from
		COM.BaseReferral
	where
		BaseReferral.AuthorisedDate is not null

	) Fact

on	Fact.MergeEncounterRecno = BaseReferralReference.MergeEncounterRecno

inner join COM.MetricBase Metric
on	Metric.MetricCode = Fact.MetricCode

inner join Allocation.DatasetAllocation Service
on	Service.AllocationTypeID = 2
and	Service.DatasetCode = 'COMR'
and	Service.DatasetRecno = Fact.MergeEncounterRecno




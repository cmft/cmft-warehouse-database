﻿





CREATE view Anticoagulation.[Diagnosis] as

select
	 Member.SourceValueID
	,DiagnosisCode = Member.SourceValueCode
	,Diagnosis = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member


where
	Member.AttributeCode = 'DIAGNOSIS'
and	Member.SourceContextCode = 'TRA||DAWN'









﻿CREATE TABLE [Anticoagulation].[BaseEncounter] (
    [MergeEncounterRecno] INT          IDENTITY (1, 1) NOT NULL,
    [EncounterRecno]      INT          NOT NULL,
    [SourceUniqueID]      INT          NOT NULL,
    [NHSNumber]           VARCHAR (20) NULL,
    [PatientSurname]      VARCHAR (50) NOT NULL,
    [PatientForename]     VARCHAR (50) NULL,
    [DateOfBirth]         DATE         NULL,
    [SexCode]             CHAR (1)     NULL,
    [Postcode]            VARCHAR (15) NULL,
    [ClinicID]            INT          NULL,
    [TreatmentStartDate]  DATE         NULL,
    [TestDate]            DATE         NULL,
    [Result]              FLOAT (53)   NOT NULL,
    [Dose]                FLOAT (53)   NOT NULL,
    [DiagnosisID]         INT          NOT NULL,
    [DiagnosisDate]       DATE         NULL,
    [CommissionerID]      INT          NULL,
    [GpPracticeCode]      VARCHAR (20) NULL,
    [InterfaceCode]       VARCHAR (4)  NOT NULL,
    [ContextCode]         VARCHAR (10) NOT NULL,
    [Created]             DATETIME     NULL,
    [Updated]             DATETIME     NULL,
    [ByWhom]              VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseEnco__4C9E43CC230BB2F7] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


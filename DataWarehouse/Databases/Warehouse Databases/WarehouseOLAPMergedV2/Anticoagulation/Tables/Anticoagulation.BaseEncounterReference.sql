﻿CREATE TABLE [Anticoagulation].[BaseEncounterReference] (
    [MergeEncounterRecno]  INT           NOT NULL,
    [ContextID]            INT           NOT NULL,
    [TreatmentStartDateID] INT           NULL,
    [TestDateID]           INT           NULL,
    [DiagnosisDateID]      INT           NULL,
    [ClinicID]             INT           NULL,
    [DiagnosisID]          INT           NULL,
    [CommissionerID]       INT           NULL,
    [Created]              DATETIME      NULL,
    [Updated]              DATETIME      NULL,
    [ByWhom]               VARCHAR (255) NULL,
    CONSTRAINT [Anticoagulation_BaseEncounterReference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


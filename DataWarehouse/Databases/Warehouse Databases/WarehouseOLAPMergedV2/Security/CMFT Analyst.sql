﻿CREATE ROLE [CMFT Analyst]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Dusia.Romaniuk';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\sec - sp information';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\paul.westhead';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Glyn.Wood';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\alistair.gray';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Rachel.Royston';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Colin.Hunter';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'adamblack';


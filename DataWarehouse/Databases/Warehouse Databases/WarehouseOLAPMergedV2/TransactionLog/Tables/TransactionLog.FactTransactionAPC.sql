﻿CREATE TABLE [TransactionLog].[FactTransactionAPC] (
    [MergeRecno]             INT        NOT NULL,
    [ContextID]              INT        NOT NULL,
    [ActivityKeyID]          INT        NOT NULL,
    [SystemUserID]           INT        NULL,
    [ActionTypeID]           INT        NOT NULL,
    [TransactionDateID]      INT        NULL,
    [ActivityDateID]         INT        NULL,
    [TransactionTimeBand]    INT        NULL,
    [ActivityTimeBand]       INT        NULL,
    [CodedTimeBandID]        INT        NOT NULL,
    [Activity]               INT        NOT NULL,
    [TransactionHours]       FLOAT (53) NULL,
    [APCMergeEncounterRecno] INT        NOT NULL,
    [APCMetricID]            INT        NULL,
    [DirectorateID]          INT        NULL,
    [WardID]                 INT        NULL,
    CONSTRAINT [PK_BuildFactTransactionAPC] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BuildFactTransactionAPC]
    ON [TransactionLog].[FactTransactionAPC]([APCMergeEncounterRecno] ASC);


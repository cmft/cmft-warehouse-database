﻿CREATE TABLE [TransactionLog].[ActionType] (
    [ActionTypeID]   INT          NOT NULL,
    [ActionTypeCode] VARCHAR (10) NOT NULL,
    [ActionType]     VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_ActionType] PRIMARY KEY CLUSTERED ([ActionTypeID] ASC)
);


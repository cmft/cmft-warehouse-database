﻿CREATE TABLE [TransactionLog].[BaseReference] (
    [MergeRecno]   INT NOT NULL,
    [ContextID]    INT NOT NULL,
    [SystemUserID] INT NULL,
    CONSTRAINT [PK_TransactionLog_BaseReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


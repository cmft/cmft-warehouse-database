﻿CREATE TABLE [TransactionLog].[ActivityType] (
    [ActivityTypeID] INT          NOT NULL,
    [ActivityType]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TransactionLogType] PRIMARY KEY CLUSTERED ([ActivityTypeID] ASC)
);


﻿CREATE TABLE [TransactionLog].[SourceColumn] (
    [SourceColumnID] INT          NOT NULL,
    [SourceColumn]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_SourceColumn] PRIMARY KEY CLUSTERED ([SourceColumnID] ASC)
);


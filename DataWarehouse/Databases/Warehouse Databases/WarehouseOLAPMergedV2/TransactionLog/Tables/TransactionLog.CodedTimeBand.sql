﻿CREATE TABLE [TransactionLog].[CodedTimeBand] (
    [CodedTimeBandID] INT          IDENTITY (1, 1) NOT NULL,
    [FromMinutes]     INT          NULL,
    [ToMinutes]       INT          NULL,
    [CodedTimeBand]   VARCHAR (50) NULL,
    [ActivityKeyID]   INT          NULL,
    CONSTRAINT [PK_CodedTimeBand] PRIMARY KEY CLUSTERED ([CodedTimeBandID] ASC)
);


﻿CREATE TABLE [TransactionLog].[Base] (
    [MergeRecno]                   INT           IDENTITY (1, 1) NOT NULL,
    [TransactionRecno]             INT           NOT NULL,
    [ContextCode]                  VARCHAR (10)  NOT NULL,
    [TransactionID]                BIGINT        NOT NULL,
    [ColumnID]                     INT           NOT NULL,
    [ActivityID]                   INT           NULL,
    [ActivityTypeID]               INT           NULL,
    [UserID]                       VARCHAR (20)  NULL,
    [ActionType]                   VARCHAR (10)  NULL,
    [TransactionTime]              DATETIME      NULL,
    [DistrictNo]                   VARCHAR (20)  NULL,
    [SourceSpellNo]                INT           NULL,
    [ActivityTime]                 DATETIME      NULL,
    [TransactionSourceEncounterNo] INT           NULL,
    [CurrentSourceEncounterNo]     INT           NULL,
    [NewValue]                     VARCHAR (MAX) NULL,
    [OldValue]                     VARCHAR (MAX) NULL,
    [Created]                      DATETIME2 (7) NULL,
    CONSTRAINT [PK_Base] PRIMARY KEY CLUSTERED ([TransactionRecno] ASC, [ContextCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Base]
    ON [TransactionLog].[Base]([MergeRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_SourceSpellNo]
    ON [TransactionLog].[Base]([SourceSpellNo] ASC)
    INCLUDE([ColumnID], [TransactionTime], [DistrictNo]);


GO
CREATE NONCLUSTERED INDEX [IX_TLB_AITT]
    ON [TransactionLog].[Base]([ActivityID] ASC, [TransactionTime] ASC)
    INCLUDE([MergeRecno], [ContextCode], [TransactionID], [ColumnID], [ActivityTypeID], [ActionType], [DistrictNo], [SourceSpellNo], [ActivityTime], [TransactionSourceEncounterNo], [CurrentSourceEncounterNo]);


GO
CREATE NONCLUSTERED INDEX [IX_TLB_TICI]
    ON [TransactionLog].[Base]([TransactionID] ASC, [ColumnID] ASC);


﻿CREATE TABLE [TransactionLog].[Activity] (
    [ActivityKeyID]  INT           IDENTITY (1, 1) NOT NULL,
    [ActivityTypeID] INT           NOT NULL,
    [ActivityID]     INT           NOT NULL,
    [Activity]       VARCHAR (50)  NOT NULL,
    [Comment]        VARCHAR (550) NULL,
    CONSTRAINT [PK_TransactionType] PRIMARY KEY CLUSTERED ([ActivityTypeID] ASC, [ActivityID] ASC)
);


﻿CREATE TABLE [TransactionLog].[FactTransaction] (
    [MergeRecno]          INT        NOT NULL,
    [ContextID]           INT        NOT NULL,
    [ActivityKeyID]       INT        NOT NULL,
    [SystemUserID]        INT        NULL,
    [ActionTypeID]        INT        NOT NULL,
    [TransactionDateID]   INT        NULL,
    [ActivityDateID]      INT        NULL,
    [TransactionTimeBand] INT        NULL,
    [ActivityTimeBand]    INT        NULL,
    [CodedTimeBandID]     INT        NOT NULL,
    [Activity]            INT        NOT NULL,
    [TransactionHours]    FLOAT (53) NULL,
    CONSTRAINT [PK_FactTransaction] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_ftransaction_tdateid]
    ON [TransactionLog].[FactTransaction]([TransactionDateID] ASC)
    INCLUDE([MergeRecno], [ContextID], [ActivityKeyID], [SystemUserID], [ActionTypeID], [ActivityDateID], [TransactionTimeBand], [ActivityTimeBand], [CodedTimeBandID], [Activity], [TransactionHours]);


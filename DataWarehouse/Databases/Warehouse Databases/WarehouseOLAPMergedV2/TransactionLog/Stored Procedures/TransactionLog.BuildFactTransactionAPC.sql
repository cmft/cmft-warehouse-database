﻿CREATE procedure [TransactionLog].[BuildFactTransactionAPC] as

truncate table TransactionLog.FactTransactionAPC

insert
into
	TransactionLog.FactTransactionAPC
(
	 MergeRecno
	,ContextID
	,ActivityKeyID
	,SystemUserID
	,ActionTypeID
	,TransactionDateID
	,ActivityDateID
	,TransactionTimeBand
	,ActivityTimeBand
	,CodedTimeBandID
	,Activity
	,TransactionHours
	,APCMergeEncounterRecno
	,APCMetricID
	,DirectorateID
	,WardID
)
select
	 FactTransaction.MergeRecno
	,FactTransaction.ContextID
	,FactTransaction.ActivityKeyID
	,FactTransaction.SystemUserID
	,FactTransaction.ActionTypeID
	,FactTransaction.TransactionDateID
	,FactTransaction.ActivityDateID
	,FactTransaction.TransactionTimeBand
	,FactTransaction.ActivityTimeBand
	,FactTransaction.CodedTimeBandID
	,FactTransaction.Activity
	,FactTransaction.TransactionHours

	,APCMergeEncounterRecno = BaseEncounter.MergeEncounterRecno

	,APCMetricID = Metric.MetricID

	,DirectorateID = 
		coalesce(
			case
			when Base.ActivityID = 11 --AD DSCH
			then EndDirectorate.DirectorateID
			else StartDirectorate.DirectorateID
			end

			,NullDirectorate.DirectorateID
		)

	,WardID = 
		case
		when Base.ActivityID = 11 --AD DSCH
		then BaseEncounterReference.EndWardID
		else BaseEncounterReference.StartWardID
		end

from
	TransactionLog.FactTransaction

inner join TransactionLog.Base
on	Base.MergeRecno = FactTransaction.MergeRecno

inner join APC.BaseEncounter
on	cast(BaseEncounter.DistrictNo as bigint) = Base.DistrictNo -- just until JR can sort the implict conversion issues with import
and	BaseEncounter.SourceSpellNo = Base.SourceSpellNo
and	BaseEncounter.SourceEncounterNo = Base.TransactionSourceEncounterNo
and	BaseEncounter.ContextCode = Base.ContextCode
and BaseEncounter.Reportable = 1
and	not exists
	(
	select
		1
	from
		APC.BaseEncounter Previous
	where
		cast(Previous.DistrictNo as bigint) = Base.DistrictNo -- just until JR can sort the implict conversion issues with import
	and	Previous.SourceSpellNo = Base.SourceSpellNo
	and	Previous.SourceEncounterNo = Base.TransactionSourceEncounterNo
	and	Previous.ContextCode = Base.ContextCode
	and	Previous.MergeEncounterRecno > BaseEncounter.MergeEncounterRecno
	and Previous.Reportable = 1

	)

inner join APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join APC.Metric
on	Metric.MetricCode = 'CHK'

--this fact table depends on APC.FactEncounter for some of its dimensions so this ensures we do not try to load orphaned records
inner join APC.FactEncounter
on	FactEncounter.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
and	FactEncounter.MetricID = Metric.MetricID

left join WH.Directorate StartDirectorate
on	StartDirectorate.DirectorateCode = BaseEncounter.StartDirectorateCode

left join WH.Directorate EndDirectorate
on	EndDirectorate.DirectorateCode = BaseEncounter.EndDirectorateCode

inner join WH.Directorate NullDirectorate
on	NullDirectorate.DirectorateCode = 'N/A'


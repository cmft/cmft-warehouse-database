﻿CREATE procedure [TransactionLog].[BuildFactTransaction] as

truncate table TransactionLog.FactTransaction

insert
into
	TransactionLog.FactTransaction
(
	 MergeRecno
	,ContextID
	,ActivityKeyID
	,SystemUserID
	,ActionTypeID
	,TransactionDateID
	,ActivityDateID
	,TransactionTimeBand
	,ActivityTimeBand
	,CodedTimeBandID
	,Activity
	,TransactionHours
)
select
	 Base.MergeRecno
	,BaseReference.ContextID
	,Activity.ActivityKeyID

	,BaseReference.SystemUserID

	,ActionType.ActionTypeID

	,TransactionDateID =
		coalesce(
			 TransactionDate.DateID

			,case
			when Base.TransactionTime is null
			then NullDate.DateID

			when Base.TransactionTime < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,ActivityDateID =
		coalesce(
			 ActivityDate.DateID

			,case
			when Base.ActivityTime is null
			then NullDate.DateID

			when Base.ActivityTime < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,TransactionTimeBand = datediff(minute, cast(Base.TransactionTime as date), Base.TransactionTime)
	,ActivityTimeBand = coalesce(datediff(minute, cast(Base.ActivityTime as date), Base.ActivityTime), -1)

	,CodedTimeBand.CodedTimeBandID

	,Activity = 1

	,TransactionHours = datediff(minute, Base.ActivityTime, Base.TransactionTime) / cast(60 as float)

from
	TransactionLog.Base

inner join TransactionLog.BaseReference
on	BaseReference.MergeRecno = Base.MergeRecno

inner join TransactionLog.Activity
on	Activity.ActivityID = Base.ActivityID
and	Activity.ActivityTypeID = Base.ActivityTypeID

inner join TransactionLog.ActionType
on	ActionType.ActionTypeCode = Base.ActionType

left join WH.Calendar TransactionDate
on	TransactionDate.TheDate = cast(Base.TransactionTime as date)

left join WH.Calendar ActivityDate
on	ActivityDate.TheDate = cast(Base.ActivityTime as date)

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'

inner join TransactionLog.CodedTimeBand
on
	CodedTimeBand.ActivityKeyID =
	case 
		when Activity.ActivityKeyID in (10,11) then Activity.ActivityKeyID
		else 7
	end
and (
		datediff(minute, Base.ActivityTime, Base.TransactionTime) between CodedTimeBand.FromMinutes and CodedTimeBand.ToMinutes
	or	(
			CodedTimeBand.FromMinutes is null
		and	datediff(minute, Base.ActivityTime, Base.TransactionTime) <= CodedTimeBand.ToMinutes
		)
	or	(
			CodedTimeBand.ToMinutes is null
		and	datediff(minute, Base.ActivityTime, Base.TransactionTime) >= CodedTimeBand.FromMinutes
		)
	)

where
	not exists
	(
	select
		1
	from
		TransactionLog.Base Previous
	where
		Previous.TransactionID = Base.TransactionID
	and	Previous.ColumnID < Base.ColumnID
	)


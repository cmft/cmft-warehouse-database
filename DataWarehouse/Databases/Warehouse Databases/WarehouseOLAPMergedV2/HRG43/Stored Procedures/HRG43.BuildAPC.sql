﻿CREATE procedure HRG43.BuildAPC as

/* Process APC Encounter */

-- Delete records with no associated APC Encounter record


truncate table HRG43.APCEncounter

insert into HRG43.APCEncounter
	(
	 MergeEncounterRecno
	,HRGCode
	,GroupingMethodFlag
	,DominantOperationCode
	,PBCCode
	,LOE
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,case when Encounter.HRGCode = '' then null else Encounter.HRGCode end
	,case when Encounter.GroupingMethodFlag = '' then null else Encounter.GroupingMethodFlag end
	,case when Encounter.DominantOperationCode = '' then null else Encounter.DominantOperationCode end
	,case when Encounter.PBCCode = '' then null else Encounter.PBCCode end
	,Encounter.LOE
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4APCEncounter Encounter



/* Process APC Spell */

-- Delete records with no associated APC Encounter record

truncate table HRG43.APCSpell


insert into HRG43.APCSpell
	(
	 MergeEncounterRecno
	,HRGCode
	,GroupingMethodFlag
	,DominantOperationCode
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode
	,EpisodeCount
	,LOS
	,PBCCode
	,Created
	,ByWhom
	)
select
	 Encounter.MergeEncounterRecno
	,case when Spell.HRGCode = '' then null else Spell.HRGCode end
	,case when Spell.GroupingMethodFlag = '' then null else Spell.GroupingMethodFlag end
	,case when Spell.DominantOperationCode = '' then null else Spell.DominantOperationCode end
	,case when Spell.PrimaryDiagnosisCode = '' then null else Spell.PrimaryDiagnosisCode end
	,case when Spell.SecondaryDiagnosisCode = '' then null else Spell.SecondaryDiagnosisCode end
	,Spell.EpisodeCount
	,Spell.LOS
	,case when Spell.[PBCCode] = '' then null else Spell.[PBCCode] end
	,Created = getdate()
	,ByWhom = suser_name()
from
	ETL.HRG4APCSpell Spell

	inner join ETL.HRG4APCEncounter Encounter
	on	Encounter.RowNo = Spell.RowNo
	and	Encounter.FinancialYear = Spell.FinancialYear



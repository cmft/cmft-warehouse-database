﻿CREATE procedure [HRG43].[BuildFCEConfiguration]
	 @from date
	,@to date
as


create table #OperationSwitch
(
	 MergeEncounterRecno int
	,InSequenceNo int
	,InOperationCode varchar(10)
	,OutSequenceNo int
	,OutOperationCode varchar(10)
)

declare @SQL varchar(max)

declare @MergeEncounterRecno int
declare @SequenceNo int
declare @OperationCode varchar(10)

declare @InitialMergeEncounterRecno int
declare @InitialSequenceNo int
declare @InitialOperationCode varchar(10)

declare switchcursor cursor fast_forward for

select
	 Operation.MergeEncounterRecno
	,Operation.SequenceNo
	,Operation.OperationCode
from
	(
	select
		 Encounter.ContextCode
		,Encounter.GlobalProviderSpellNo
		,Encounter.GlobalEpisodeNo
		,HierarchyValue = max(Encounter.HierarchyValue)
	from
		(
		select
			 BaseEncounter.ContextCode
			,BaseEncounter.GlobalProviderSpellNo
			,BaseEncounter.GlobalEpisodeNo
			,Hierarchy.HierarchyValue
		from
			APC.BaseEncounter

		inner join APC.Operation
		on	Operation.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
		and BaseEncounter.Reportable = 1
		
		inner join HRG43.Hierarchy
		on	Hierarchy.HierarchyID = 'ProcHier'
		and	Hierarchy.HierarchyTypeCode = 'OPCS'
		and	Hierarchy.HierarchyCode = replace(Operation.OperationCode, '.', '')
		and	Hierarchy.HierarchyValue > 2

		where
			BaseEncounter.DischargeDate between @from and @to

		group by
			 BaseEncounter.ContextCode
			,BaseEncounter.GlobalProviderSpellNo
			,BaseEncounter.GlobalEpisodeNo
			,Hierarchy.HierarchyValue
		having
			COUNT(*) > 1
		) Encounter
	group by
		 Encounter.ContextCode
		,Encounter.GlobalProviderSpellNo
		,Encounter.GlobalEpisodeNo
	) Encounter

inner join APC.BaseEncounter
on	BaseEncounter.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and BaseEncounter.Reportable = 1

inner join APC.Operation
on	Operation.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join HRG43.Hierarchy
on	Hierarchy.HierarchyID = 'ProcHier'
and	Hierarchy.HierarchyTypeCode = 'OPCS'
and	Hierarchy.HierarchyCode = replace(Operation.OperationCode, '.', '')
and	Hierarchy.HierarchyValue = Encounter.HierarchyValue



OPEN switchcursor

FETCH NEXT FROM switchcursor

INTO
	 @InitialMergeEncounterRecno
	,@InitialSequenceNo
	,@InitialOperationCode

FETCH NEXT FROM switchcursor

INTO
	 @MergeEncounterRecno
	,@SequenceNo
	,@OperationCode

WHILE @@FETCH_STATUS = 0

BEGIN

	if @InitialMergeEncounterRecno = @MergeEncounterRecno
	begin
		if @InitialOperationCode <> @OperationCode
			insert
			into
				#OperationSwitch
			(
				 MergeEncounterRecno
				,InSequenceNo
				,InOperationCode
				,OutSequenceNo
				,OutOperationCode
			)
			select
				 @InitialMergeEncounterRecno
				,@InitialSequenceNo
				,@OperationCode
				,@SequenceNo
				,@InitialOperationCode
	end
	else
	begin
		select
			 @InitialMergeEncounterRecno = @MergeEncounterRecno
			,@InitialSequenceNo = @SequenceNo
			,@InitialOperationCode = @OperationCode
	end
	
	FETCH NEXT FROM switchcursor
	INTO
		 @MergeEncounterRecno
		,@SequenceNo
		,@OperationCode

END
  
CLOSE switchcursor
DEALLOCATE switchcursor


truncate table HRG43.FCEConfiguration


insert
into
	HRG43.FCEConfiguration
(
	 OriginalSpell
	,MergeRecno
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,PseudoProviderSpellNo
	,PrimaryOperationCode
	,SecondaryOperationCode1
	,SecondaryOperationCode2
	,SecondaryOperationCode3
	,SecondaryOperationCode4
	,SecondaryOperationCode5
	,SecondaryOperationCode6
	,SecondaryOperationCode7
	,SecondaryOperationCode8
	,SecondaryOperationCode9
	,SecondaryOperationCode10
	,SecondaryOperationCode11
	,SecondaryOperationCode12
	,SecondaryOperationCode13
	,SecondaryOperationCode14
	,SecondaryOperationCode15
	,SecondaryOperationCode16
)
select distinct
	 OriginalSpell = CAST(0 as bit)
	,Spell.MergeEncounterRecno
	,Spell.GlobalProviderSpellNo
	,Spell.GlobalEpisodeNo

	,PseudoProviderSpellNo =
		CAST(
			ROW_NUMBER()
			over(
				partition by
					 Spell.GlobalProviderSpellNo
					,Spell.GlobalEpisodeNo

				order by
					 Spell.GlobalProviderSpellNo
					,Spell.GlobalEpisodeNo
			)
			as varchar
		)

	,PrimaryOperationCode =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 0
			then #OperationSwitch.InOperationCode
			else PrimaryOperation.OperationCode
			end
		else PrimaryOperation.OperationCode
		end

	,SecondaryOperationCode1 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 1
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 1
			then #OperationSwitch.OutOperationCode
			else SecondaryOperation1.OperationCode
			end
		else SecondaryOperation1.OperationCode
		end

	,SecondaryOperationCode2 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 2
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 2
			then #OperationSwitch.OutOperationCode
			else SecondaryOperation2.OperationCode
			end
		else SecondaryOperation2.OperationCode
		end

	,SecondaryOperationCode3 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 3
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 3
			then #OperationSwitch.OutOperationCode
			else SecondaryOperation3.OperationCode
			end
		else SecondaryOperation3.OperationCode
		end

	,SecondaryOperationCode4 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 4
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 4
			then #OperationSwitch.OutOperationCode
			else SecondaryOperation4.OperationCode
			end
		else SecondaryOperation4.OperationCode
		end

	,SecondaryOperationCode5 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
			case
			when #OperationSwitch.InSequenceNo = 5
			then #OperationSwitch.InOperationCode
			when #OperationSwitch.OutSequenceNo = 5
			then #OperationSwitch.OutOperationCode
			else SecondaryOperation5.OperationCode
			end
		else SecondaryOperation5.OperationCode
		end

	,SecondaryOperationCode6 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 6
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 6
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation6.OperationCode
		end
		else SecondaryOperation6.OperationCode
		end

	,SecondaryOperationCode7 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 7
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 7
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation7.OperationCode
		end
		else SecondaryOperation7.OperationCode
		end

	,SecondaryOperationCode8 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 8
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 8
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation8.OperationCode
		end
		else SecondaryOperation8.OperationCode
		end

	,SecondaryOperationCode9 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 9
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 9
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation9.OperationCode
		end
		else SecondaryOperation9.OperationCode
		end

	,SecondaryOperationCode10 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 10
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 10
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation10.OperationCode
		end
		else SecondaryOperation10.OperationCode
		end

	,SecondaryOperationCode11 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 11
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 11
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation11.OperationCode
		end
		else SecondaryOperation11.OperationCode
		end

	,SecondaryOperationCode12 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 12
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 12
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation12.OperationCode
		end
		else SecondaryOperation12.OperationCode
		end

	,SecondaryOperationCode13 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 13
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 13
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation13.OperationCode
		end
		else SecondaryOperation13.OperationCode
		end

	,SecondaryOperationCode14 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 14
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 14
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation14.OperationCode
		end
		else SecondaryOperation14.OperationCode
		end

	,SecondaryOperationCode15 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 15
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 15
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation15.OperationCode
		end
		else SecondaryOperation15.OperationCode
		end

	,SecondaryOperationCode16 =
		case
		when #OperationSwitch.MergeEncounterRecno = Spell.MergeEncounterRecno
		then
		case
		when #OperationSwitch.InSequenceNo = 16
		then #OperationSwitch.InOperationCode
		when #OperationSwitch.OutSequenceNo = 16
		then #OperationSwitch.OutOperationCode
		else SecondaryOperation16.OperationCode
		end
		else SecondaryOperation16.OperationCode
		end

from
	(
	select distinct
		*
	from
		#OperationSwitch
	) #OperationSwitch

inner join APC.BaseEncounter FCE
on	FCE.MergeEncounterRecno = #OperationSwitch.MergeEncounterRecno
and FCE.Reportable = 1

inner join APC.BaseEncounter Spell
on	Spell.GlobalProviderSpellNo = FCE.GlobalProviderSpellNo

left join APC.BaseOperation PrimaryOperation
on	PrimaryOperation.MergeEncounterRecno = Spell.MergeEncounterRecno
and	PrimaryOperation.SequenceNo = 0

left join APC.BaseOperation SecondaryOperation1
on	SecondaryOperation1.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation1.SequenceNo = 1

left join APC.BaseOperation SecondaryOperation2
on	SecondaryOperation2.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation2.SequenceNo = 2

left join APC.BaseOperation SecondaryOperation3
on	SecondaryOperation3.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation3.SequenceNo = 3

left join APC.BaseOperation SecondaryOperation4
on	SecondaryOperation4.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation4.SequenceNo = 4

left join APC.BaseOperation SecondaryOperation5
on	SecondaryOperation5.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation5.SequenceNo = 5

left join APC.BaseOperation SecondaryOperation6
on	SecondaryOperation6.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation6.SequenceNo = 6

left join APC.BaseOperation SecondaryOperation7
on	SecondaryOperation7.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation7.SequenceNo = 7

left join APC.BaseOperation SecondaryOperation8
on	SecondaryOperation8.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation8.SequenceNo = 8

left join APC.BaseOperation SecondaryOperation9
on	SecondaryOperation9.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation9.SequenceNo = 9

left join APC.BaseOperation SecondaryOperation10
on	SecondaryOperation10.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation10.SequenceNo = 10

left join APC.BaseOperation SecondaryOperation11
on	SecondaryOperation11.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation11.SequenceNo = 11

left join APC.BaseOperation SecondaryOperation12
on	SecondaryOperation12.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation12.SequenceNo = 12

left join APC.BaseOperation SecondaryOperation13
on	SecondaryOperation13.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation13.SequenceNo = 13

left join APC.BaseOperation SecondaryOperation14
on	SecondaryOperation14.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation14.SequenceNo = 14

left join APC.BaseOperation SecondaryOperation15
on	SecondaryOperation15.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation15.SequenceNo = 15

left join APC.BaseOperation SecondaryOperation16
on	SecondaryOperation16.MergeEncounterRecno = Spell.MergeEncounterRecno
and	SecondaryOperation16.SequenceNo = 16

--add original fces
insert
into
	HRG43.FCEConfiguration
(
	 OriginalSpell
	,MergeRecno
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,PseudoProviderSpellNo
	,PrimaryOperationCode
	,SecondaryOperationCode1
	,SecondaryOperationCode2
	,SecondaryOperationCode3
	,SecondaryOperationCode4
	,SecondaryOperationCode5
	,SecondaryOperationCode6
	,SecondaryOperationCode7
	,SecondaryOperationCode8
	,SecondaryOperationCode9
	,SecondaryOperationCode10
	,SecondaryOperationCode11
	,SecondaryOperationCode12
	,SecondaryOperationCode13
	,SecondaryOperationCode14
	,SecondaryOperationCode15
	,SecondaryOperationCode16
)
select
	 OriginalSpell = CAST(1 as bit)
	,FCE.MergeEncounterRecno
	,FCE.GlobalProviderSpellNo
	,FCE.GlobalEpisodeNo

	,PseudoProviderSpellNo = 0
	,PrimaryOperationCode =
		PrimaryOperation.OperationCode

	,SecondaryOperationCode1 =
		SecondaryOperation1.OperationCode

	,SecondaryOperationCode2 =
		SecondaryOperation2.OperationCode

	,SecondaryOperationCode3 =
		SecondaryOperation3.OperationCode

	,SecondaryOperationCode4 =
		SecondaryOperation4.OperationCode

	,SecondaryOperationCode5 =
		SecondaryOperation5.OperationCode

	,SecondaryOperationCode6 =
		SecondaryOperation6.OperationCode

	,SecondaryOperationCode7 =
		SecondaryOperation7.OperationCode

	,SecondaryOperationCode8 =
		SecondaryOperation8.OperationCode

	,SecondaryOperationCode9 =
		SecondaryOperation9.OperationCode

	,SecondaryOperationCode10 =
		SecondaryOperation10.OperationCode

	,SecondaryOperationCode11 =
		SecondaryOperation11.OperationCode

	,SecondaryOperationCode12 =
		SecondaryOperation12.OperationCode

	,SecondaryOperationCode13 =
		SecondaryOperation13.OperationCode

	,SecondaryOperationCode14 =
		SecondaryOperation14.OperationCode

	,SecondaryOperationCode15 =
		SecondaryOperation15.OperationCode

	,SecondaryOperationCode16 =
		SecondaryOperation16.OperationCode
from
	APC.BaseEncounter FCE

left join APC.BaseOperation PrimaryOperation
on	PrimaryOperation.MergeEncounterRecno = FCE.MergeEncounterRecno
and	PrimaryOperation.SequenceNo = 0
and FCE.Reportable = 1

left join APC.BaseOperation SecondaryOperation1
on	SecondaryOperation1.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation1.SequenceNo = 1

left join APC.BaseOperation SecondaryOperation2
on	SecondaryOperation2.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation2.SequenceNo = 2

left join APC.BaseOperation SecondaryOperation3
on	SecondaryOperation3.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation3.SequenceNo = 3

left join APC.BaseOperation SecondaryOperation4
on	SecondaryOperation4.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation4.SequenceNo = 4

left join APC.BaseOperation SecondaryOperation5
on	SecondaryOperation5.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation5.SequenceNo = 5

left join APC.BaseOperation SecondaryOperation6
on	SecondaryOperation6.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation6.SequenceNo = 6

left join APC.BaseOperation SecondaryOperation7
on	SecondaryOperation7.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation7.SequenceNo = 7

left join APC.BaseOperation SecondaryOperation8
on	SecondaryOperation8.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation8.SequenceNo = 8

left join APC.BaseOperation SecondaryOperation9
on	SecondaryOperation9.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation9.SequenceNo = 9

left join APC.BaseOperation SecondaryOperation10
on	SecondaryOperation10.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation10.SequenceNo = 10

left join APC.BaseOperation SecondaryOperation11
on	SecondaryOperation11.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation11.SequenceNo = 11

left join APC.BaseOperation SecondaryOperation12
on	SecondaryOperation12.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation12.SequenceNo = 12

left join APC.BaseOperation SecondaryOperation13
on	SecondaryOperation13.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation13.SequenceNo = 13

left join APC.BaseOperation SecondaryOperation14
on	SecondaryOperation14.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation14.SequenceNo = 14

left join APC.BaseOperation SecondaryOperation15
on	SecondaryOperation15.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation15.SequenceNo = 15

left join APC.BaseOperation SecondaryOperation16
on	SecondaryOperation16.MergeEncounterRecno = FCE.MergeEncounterRecno
and	SecondaryOperation16.SequenceNo = 16

where
	exists
	(
	select
		1
	from
		HRG43.FCEConfiguration
	where
		FCEConfiguration.GlobalProviderSpellNo = FCE.GlobalProviderSpellNo
	)

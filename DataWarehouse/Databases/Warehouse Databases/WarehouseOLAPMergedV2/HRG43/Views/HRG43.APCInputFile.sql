﻿





CREATE view [HRG43].[APCInputFile] as

select --top 100
	 PROCODET = 'RW3' --Encounter.ProviderCode
	,PROVSPNO = Encounter.GlobalProviderSpellNo + '||' + cast(FCEConfiguration.PseudoProviderSpellNo as varchar)
	,EPIORDER = Encounter.GlobalEpisodeNo
	,STARTAGE =
				case
					when
						floor(
						 datediff(
							 day
							,Encounter.DateOfBirth
							,Encounter.EpisodeStartDate
						)/365.25	
						) < 0
					then 0
					else floor(
						 datediff(
							 day
							,Encounter.DateOfBirth
							,Encounter.EpisodeStartDate
						)/365.25	
						)
				end
	

	,SEX = Sex.NationalSexCode		
	,CLASSPAT = PatientClassification.NationalPatientClassificationCode
	,ADMISORC = AdmissionSource.NationalAdmissionSourceCode
	,ADMIMETH = AdmissionMethod.NationalAdmissionMethodCode
	,DISDEST = DischargeDestination.NationalDischargeDestinationCode
	,DISMETH = DischargeMethod.NationalDischargeMethodCode
	,EPIDUR =
			case
				when datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate) - coalesce(Bedday.Bedday, 0) <= 0 
				then 0
				else datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate) - coalesce(Bedday.Bedday, 0)
			end		
	,MAINSPEF = Consultant.MainSpecialtyCode
	,NEOCARE = NeonatalLevelOfCare.NationalNeonatalLevelOfCareCode
	,TRETSPEF = TreatmentFunction.NationalSpecialtyCode
	,DIAG_01 =
			left(
				replace(PrimaryDiagnosis.DiagnosisCode, '.', '')
				,5
				) 
	,DIAG_02 =
		left(
			replace(SecondaryDiagnosis1.DiagnosisCode, '.', '')
			,5
			) 
	,DIAG_03 =
		left(
			replace(SecondaryDiagnosis2.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_04 =
		left(
			replace(SecondaryDiagnosis3.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_05 =
		left(
			replace(SecondaryDiagnosis4.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_06 =
		left(
			replace(SecondaryDiagnosis5.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_07 =
		left(
			replace(SecondaryDiagnosis6.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_08 =
		left(
			replace(SecondaryDiagnosis7.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_09 =
		left(
			replace(SecondaryDiagnosis8.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_10 =
		left(
			replace(SecondaryDiagnosis9.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_11 =
		left(
			replace(SecondaryDiagnosis10.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_12 =
		left(
			replace(SecondaryDiagnosis11.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_13 =
		left(
			replace(SecondaryDiagnosis12.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_14 =
		left(
			replace(SecondaryDiagnosis13.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_15 =
		left(
			replace(SecondaryDiagnosis14.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_16 =
		left(
			replace(SecondaryDiagnosis15.DiagnosisCode, '.', '')
			,5
			)
	,OPER_01 = 
		left(
			replace(PrimaryOperationCode, '.', '')
			,5
			)
	,OPER_02 = 
		left(
			replace(SecondaryOperationCode1, '.', '')
			,5
			)
	,OPER_03 = 
		left(
			replace(SecondaryOperationCode2, '.', '')
			,5
			)
	,OPER_04 = 
		left(
			replace(SecondaryOperationCode3, '.', '')
			,5
			)
	,OPER_05 = 
		left(
			replace(SecondaryOperationCode4, '.', '')
			,5
			)
	,OPER_06 = 
		left(
			replace(SecondaryOperationCode5, '.', '')
			,5
			)
	,OPER_07 = 
		left(
			replace(SecondaryOperationCode6, '.', '')
			,5
			)
	,OPER_08 = 
		left(
			replace(SecondaryOperationCode7, '.', '')
			,5
			)	
	,OPER_09 = 
		left(
			replace(SecondaryOperationCode8, '.', '')
			,5
			)
	,OPER_10 = 
		left(
			replace(SecondaryOperationCode9, '.', '')
			,5
			)
	,OPER_11 = 
		left(
			replace(SecondaryOperationCode10, '.', '')
			,5
			)
	,OPER_12 = 
		left(
			replace(SecondaryOperationCode11, '.', '')
			,5
			)
	--,OPER_13 = 
	--	left(
	--		replace(SecondaryOperationCode12, '.', '')
	--		,5
	--		)
	--,OPER_14 = 
	--	left(
	--		replace(SecondaryOperationCode13, '.', '')
	--		,5
	--		)
	--,OPER_15 = 
	--	left(
	--		replace(SecondaryOperationCode14, '.', '')
	--		,5
	--		)
	--,OPER_16 = 
	--	left(
	--		replace(SecondaryOperationCode15, '.', '')
	--		,5
	--		)
	--,OPER_17 = 
	--	left(
	--		replace(SecondaryOperationCode16, '.', '')
	--		,5
	--		)
	,CRITICALCAREDAYS = 0
	,REHABILITATIONDAYS = 0
	,SPECIALISTPALLIATIVECAREDAYS = 0
	,MergeEncounterRecno = FCEConfiguration.FCEConfigurationRecno
	,FinancialYear = DischargeDate.FinancialYear 

from
	APC.BaseEncounter Encounter

inner join
	APC.BaseEncounterReference EncounterReference
on	Encounter.MergeEncounterRecno = EncounterReference.MergeEncounterRecno 
and Encounter.Reportable = 1

inner join APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

left join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join APC.BaseEncounterReference AdmissionReference
on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Discharge.MergeEncounterRecno

inner join HRG43.FCEConfiguration
on	FCEConfiguration.MergeRecno = Encounter.MergeEncounterRecno

left outer join	WH.Specialty TreatmentFunction
on	TreatmentFunction.SourceSpecialtyID = EncounterReference.SpecialtyID

left outer join
	WH.Consultant
on	Consultant.SourceConsultantID = EncounterReference.ConsultantID

left outer join 
	APC.AdmissionSource
on	AdmissionSource.SourceAdmissionSourceID = AdmissionReference.AdmissionSourceID

left outer join 
	APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = AdmissionReference.AdmissionMethodID

left outer join
	WH.Calendar DischargeDate
on	DischargeDate.DateID = DischargeReference.DischargeDateID

left outer join
	WH.Sex Sex
on	Sex.SourceSexID = EncounterReference.SexID

left outer join
	APC.PatientClassification
on	SourcePatientClassificationID = EncounterReference.PatientClassificationID

left outer join -- should be inner performance reasons 
	APC.DischargeDestination
on	SourceDischargeDestinationID = DischargeReference.DischargeDestinationID

left outer join -- should be inner performance reasons 
	APC.DischargeMethod
on	SourceDischargeMethodID = DischargeReference.DischargeMethodID

left outer join -- should be inner performance reasons 
	APC.NeonatalLevelOfCare
on	SourceNeonatalLevelOfCareID = EncounterReference.NeonatalLevelOfCareID

left outer join
	APC.BaseDiagnosis PrimaryDiagnosis
on	PrimaryDiagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	PrimaryDiagnosis.SequenceNo = 0

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis1
on	SecondaryDiagnosis1.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis1.SequenceNo = 1

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis2
on	SecondaryDiagnosis2.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis2.SequenceNo = 2

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis3
on	SecondaryDiagnosis3.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis3.SequenceNo = 3

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis4
on	SecondaryDiagnosis4.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis4.SequenceNo = 4

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis5
on	SecondaryDiagnosis5.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis5.SequenceNo = 5
	
left outer join
	APC.BaseDiagnosis SecondaryDiagnosis6
on	SecondaryDiagnosis6.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis6.SequenceNo = 6

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis7
on	SecondaryDiagnosis7.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis7.SequenceNo = 7

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis8
on	SecondaryDiagnosis8.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis8.SequenceNo = 8

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis9
on	SecondaryDiagnosis9.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis9.SequenceNo = 9

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis10
on	SecondaryDiagnosis10.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis10.SequenceNo = 10

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis11
on	SecondaryDiagnosis11.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis11.SequenceNo = 11

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis12
on	SecondaryDiagnosis12.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis12.SequenceNo = 12

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis13
on	SecondaryDiagnosis13.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis13.SequenceNo = 13

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis14
on	SecondaryDiagnosis14.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis14.SequenceNo = 14

left outer join
	APC.BaseDiagnosis SecondaryDiagnosis15
on	SecondaryDiagnosis15.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis15.SequenceNo = 15

--left outer join
--	APC.BaseDiagnosis SecondaryDiagnosis16
--on	SecondaryDiagnosis16.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and	SecondaryDiagnosis16.SequenceNo = 16

left outer join
			(
				select
					MergeAPCEncounterRecno
					,Bedday = count(*)
				from
					[APC].[BedDay]
				group by
					MergeAPCEncounterRecno
				
			) Bedday
			on	MergeAPCEncounterRecno = Encounter.MergeEncounterRecno


--where 
--	Encounter.DischargeDate > dateadd(month, -3, getdate())
--and
--	CodingCompleteDate is not null
	--DischargeDate >= '1 apr 2012'


	--exists
	--	(
	--	select
	--		1
	--	from
	--		APC.BaseEncounter Spell
	--	where
	--		Spell.ProviderSpellNo = Encounter.ProviderSpellNo
	--	and Spell.ContextCode = Encounter.ContextCode
	--	and	Encounter.EpisodeEndDate > dateadd(month, -3, getdate())
	--	)


/* Pass dummy records to grouper so it doesn't error if no records passed to it */

union all

select --top 100
	PROCODET = null
	,PROVSPNO = null
	,EPIORDER = null
	,STARTAGE = null
	,SEX = null
	,CLASSPAT = null
	,ADMISORC = null
	,ADMIMETH = null
	,DISDEST = null
	,DISMETH = null
	,EPIDUR = null
	,MAINSPEF = null
	,NEOCARE = null
	,TRETSPEF = null
	,DIAG_01 = null
	,DIAG_02 = null
	,DIAG_03 = null
	,DIAG_04 = null
	,DIAG_05 = null
	,DIAG_06 = null
	,DIAG_07 = null
	,DIAG_08 = null
	,DIAG_09 = null
	,DIAG_10 = null
	,DIAG_11 = null
	,DIAG_12 = null
	,DIAG_13 = null
	,DIAG_14 = null
	,DIAG_15 = null
	,DIAG_16 = null
	,OPER_01 = null
	,OPER_02 = null
	,OPER_03 = null
	,OPER_04 = null
	,OPER_05 = null
	,OPER_06 = null
	,OPER_07 = null
	,OPER_08 = null
	,OPER_09 = null
	,OPER_10 = null
	,OPER_11 = null
	,OPER_12 = null
	--,OPER_13 = 
	--	left(
	--		replace(SecondaryOperationCode12, '.', '')
	--		,5
	--		)
	--,OPER_14 = 
	--	left(
	--		replace(SecondaryOperationCode13, '.', '')
	--		,5
	--		)
	--,OPER_15 = 
	--	left(
	--		replace(SecondaryOperationCode14, '.', '')
	--		,5
	--		)
	--,OPER_16 = 
	--	left(
	--		replace(SecondaryOperationCode15, '.', '')
	--		,5
	--		)
	--,OPER_17 = 
	--	left(
	--		replace(SecondaryOperationCode16, '.', '')
	--		,5
	--		)
	,CRITICALCAREDAYS = null
	,REHABILITATIONDAYS = null
	,SPECIALISTPALLIATIVECAREDAYS = null
	,MergeEncounterRecno  = -20142015 -- dummy record
	,FinancialYear = '2014/2015'










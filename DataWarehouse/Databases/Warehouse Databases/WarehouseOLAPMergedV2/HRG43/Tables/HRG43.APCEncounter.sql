﻿CREATE TABLE [HRG43].[APCEncounter] (
    [MergeEncounterRecno]   INT           NOT NULL,
    [HRGCode]               VARCHAR (50)  NULL,
    [GroupingMethodFlag]    VARCHAR (50)  NULL,
    [DominantOperationCode] VARCHAR (50)  NULL,
    [PBCCode]               VARCHAR (50)  NULL,
    [LOE]                   INT           NULL,
    [Created]               DATETIME      NULL,
    [ByWhom]                VARCHAR (255) NULL,
    CONSTRAINT [PK__APCEncou__4C9E43CC54382EE0] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


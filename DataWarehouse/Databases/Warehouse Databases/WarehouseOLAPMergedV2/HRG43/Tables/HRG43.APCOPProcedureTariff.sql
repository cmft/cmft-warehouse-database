﻿CREATE TABLE [HRG43].[APCOPProcedureTariff] (
    [HRGCode]                             VARCHAR (10)  NOT NULL,
    [HRG]                                 VARCHAR (255) NULL,
    [OutpatientProcedureTariff]           MONEY         NULL,
    [CombinedDaycaseElectiveTariff]       MONEY         NULL,
    [DaycaseTariff]                       MONEY         NULL,
    [ElectiveSpellTariff]                 MONEY         NULL,
    [ElectiveLongStayTrimpoint]           INT           NULL,
    [NonElectiveSpellTariff]              MONEY         NULL,
    [NonElectiveLongStayTrimpoint]        INT           NULL,
    [PerDayLongStayPayment]               MONEY         NULL,
    [ReducedShortStayEmergencyTariffFlag] VARCHAR (3)   NULL,
    [PercentAppliedInCalculation]         REAL          NULL,
    [ReducedShortStayEmergencyTariff]     MONEY         NULL,
    [BestPractice]                        BIT           NULL,
    [BestPracticeLevel]                   VARCHAR (50)  NULL,
    [TariffPeriod]                        VARCHAR (9)   NULL,
    [Created]                             DATETIME      CONSTRAINT [DF_HRG43_APCOPProcedureTariff_Created] DEFAULT (getdate()) NULL,
    [ByWhom]                              VARCHAR (50)  CONSTRAINT [DF_HRG43_APCOPProcedureTariff_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_HRG43_APCOPProcedureTariff] PRIMARY KEY CLUSTERED ([HRGCode] ASC)
);


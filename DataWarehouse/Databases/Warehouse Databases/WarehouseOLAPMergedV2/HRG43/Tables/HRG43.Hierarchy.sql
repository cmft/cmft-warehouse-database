﻿CREATE TABLE [HRG43].[Hierarchy] (
    [HierarchyID]       VARCHAR (20)  NOT NULL,
    [HierarchyTypeCode] VARCHAR (20)  NOT NULL,
    [HierarchyCode]     VARCHAR (20)  NOT NULL,
    [Hierarchy]         VARCHAR (255) NULL,
    [HierarchyValue]    INT           NULL,
    [HierarchyPeriod]   VARCHAR (9)   NULL,
    [Created]           DATETIME      CONSTRAINT [DF_HRG43_Hierarchy_Created] DEFAULT (getdate()) NULL,
    [ByWhom]            VARCHAR (50)  CONSTRAINT [DF_HRG43_Hierarchy_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_Hierarchy] PRIMARY KEY CLUSTERED ([HierarchyID] ASC, [HierarchyTypeCode] ASC, [HierarchyCode] ASC)
);


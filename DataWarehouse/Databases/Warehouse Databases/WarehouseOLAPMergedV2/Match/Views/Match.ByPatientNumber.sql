﻿CREATE VIEW [Match].[ByPatientNumber]
as

select
	WrkTargetDataset.MatchTypeID
	,TemplateID = MatchTypeTemplateRuleBase.TemplateID
	,SourceDatasetRecno = WrkSourceDataset.DatasetRecno
	,TargetDatasetRecno = WrkTargetDataset.DatasetRecno
	,Priority = MatchTypeTemplateRuleBase.Priority
from
	Match.WrkSourceDataset

inner join Match.WrkTargetDataset
on	WrkTargetDataset.PatientNumber = WrkSourceDataset.PatientNumber
and	WrkTargetDataset.StartTime >= WrkSourceDataset.StartTime
and	(
		WrkTargetDataset.EndTime is null
	or	WrkTargetDataset.EndTime <= WrkSourceDataset.EndTime
	)

inner join Match.MatchTypeTemplateRuleBase
on	MatchTypeTemplateRuleBase.MatchTypeID = WrkTargetDataset.MatchTypeID
and	MatchTypeTemplateRuleBase.SourceDatasetCode = WrkSourceDataset.DatasetCode

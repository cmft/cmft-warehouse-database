﻿CREATE TABLE [Match].[Dataset] (
    [DatasetCode] VARCHAR (10)  NOT NULL,
    [Dataset]     VARCHAR (255) NOT NULL,
    [TemplateID]  INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([DatasetCode] ASC)
);


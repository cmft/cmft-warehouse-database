﻿CREATE TABLE [Match].[RuleBase] (
    [RuleBaseRecno]    INT          IDENTITY (1, 1) NOT NULL,
    [TemplateID]       INT          NOT NULL,
    [AfterLimitHours]  INT          NULL,
    [BeforeLimitHours] INT          NULL,
    [BooleanValue]     BIT          NULL,
    [RuleListTypeCode] VARCHAR (50) NULL,
    CONSTRAINT [PK_RuleBase] PRIMARY KEY CLUSTERED ([RuleBaseRecno] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_RuleBase_Template] FOREIGN KEY ([TemplateID]) REFERENCES [Match].[Template] ([TemplateID])
);


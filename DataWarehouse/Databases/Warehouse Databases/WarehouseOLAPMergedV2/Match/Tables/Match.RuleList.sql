﻿CREATE TABLE [Match].[RuleList] (
    [RuleListTypeCode] VARCHAR (50) NOT NULL,
    [RuleListValue]    VARCHAR (50) NOT NULL,
    [ContextCode]      VARCHAR (10) NULL,
    CONSTRAINT [PK_RuleList] PRIMARY KEY CLUSTERED ([RuleListTypeCode] ASC, [RuleListValue] ASC)
);


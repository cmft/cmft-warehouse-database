﻿CREATE TABLE [Match].[WrkMatch] (
    [MatchRecno]         INT IDENTITY (1, 1) NOT NULL,
    [TemplateID]         INT NOT NULL,
    [SourceDatasetRecno] INT NOT NULL,
    [TargetDatasetRecno] INT NOT NULL,
    [Priority]           INT NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Match_WrkMatch_01]
    ON [Match].[WrkMatch]([TemplateID] ASC, [SourceDatasetRecno] ASC)
    INCLUDE([MatchRecno], [Priority]);


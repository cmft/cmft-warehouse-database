﻿CREATE TABLE [Match].[TemplateType] (
    [TemplateTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [TemplateType]     VARCHAR (255) NOT NULL,
    [TemplateTypeCode] VARCHAR (20)  NOT NULL,
    CONSTRAINT [PK_TemplateType_1] PRIMARY KEY CLUSTERED ([TemplateTypeID] ASC),
    CONSTRAINT [FK_TemplateType_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [Match].[TemplateType] ([TemplateTypeID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TemplateType_TemplateTypeCode]
    ON [Match].[TemplateType]([TemplateTypeCode] ASC);


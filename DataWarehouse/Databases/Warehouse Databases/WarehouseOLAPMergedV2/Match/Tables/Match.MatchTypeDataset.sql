﻿CREATE TABLE [Match].[MatchTypeDataset] (
    [MatchTypeID]        INT NOT NULL,
    [SourceDatasetRecno] INT NOT NULL,
    [TargetDatasetRecno] INT NOT NULL,
    [TemplateID]         INT NOT NULL,
    CONSTRAINT [PK_DatasetMatch] PRIMARY KEY CLUSTERED ([MatchTypeID] ASC, [SourceDatasetRecno] ASC, [TargetDatasetRecno] ASC),
    CONSTRAINT [FK_MatchTypeDataset_MatchType] FOREIGN KEY ([MatchTypeID]) REFERENCES [Match].[MatchType] ([MatchTypeID]),
    CONSTRAINT [FK_MatchTypeDataset_Template] FOREIGN KEY ([TemplateID]) REFERENCES [Match].[Template] ([TemplateID])
);


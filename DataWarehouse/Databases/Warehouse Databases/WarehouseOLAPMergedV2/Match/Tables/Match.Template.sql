﻿CREATE TABLE [Match].[Template] (
    [TemplateID]     INT           IDENTITY (1, 1) NOT NULL,
    [Template]       VARCHAR (128) NOT NULL,
    [Remark]         VARCHAR (255) NULL,
    [TemplateTypeID] INT           NOT NULL,
    [Priority]       INT           NOT NULL,
    CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED ([TemplateID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Template_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [Match].[TemplateType] ([TemplateTypeID])
);


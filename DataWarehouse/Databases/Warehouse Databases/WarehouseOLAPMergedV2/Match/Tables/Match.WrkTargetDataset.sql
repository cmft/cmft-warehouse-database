﻿CREATE TABLE [Match].[WrkTargetDataset] (
    [MatchTypeID]           INT           NOT NULL,
    [DatasetRecno]          INT           NOT NULL,
    [ContextCode]           VARCHAR (20)  NOT NULL,
    [StartTime]             DATETIME      NULL,
    [EndTime]               DATETIME      NULL,
    [DateOfBirth]           DATE          NULL,
    [Forename]              VARCHAR (255) NULL,
    [NHSNumber]             VARCHAR (17)  NULL,
    [PatientNumber]         VARCHAR (20)  NULL,
    [Postcode]              VARCHAR (10)  NULL,
    [Surname]               VARCHAR (255) NULL,
    [TreatmentFunctionCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_WrkTargetDataset] PRIMARY KEY CLUSTERED ([MatchTypeID] ASC, [DatasetRecno] ASC, [ContextCode] ASC)
);


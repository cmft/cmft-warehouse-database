﻿CREATE TABLE [Match].[MatchTypeTemplate] (
    [MatchTypeID] INT NOT NULL,
    [TemplateID]  INT NOT NULL,
    [Active]      BIT DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([MatchTypeID] ASC, [TemplateID] ASC),
    CONSTRAINT [FK_MatchTypeTemplate_MatchType] FOREIGN KEY ([MatchTypeID]) REFERENCES [Match].[MatchType] ([MatchTypeID]),
    CONSTRAINT [FK_MatchTypeTemplate_Template] FOREIGN KEY ([TemplateID]) REFERENCES [Match].[Template] ([TemplateID])
);


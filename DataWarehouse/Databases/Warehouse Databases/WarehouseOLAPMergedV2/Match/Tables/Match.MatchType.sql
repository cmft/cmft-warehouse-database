﻿CREATE TABLE [Match].[MatchType] (
    [MatchTypeID]       INT           IDENTITY (1, 1) NOT NULL,
    [MatchType]         VARCHAR (255) NOT NULL,
    [SourceDatasetCode] VARCHAR (10)  NOT NULL,
    [TargetDatasetCode] VARCHAR (10)  NOT NULL,
    [Active]            BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([MatchTypeID] ASC),
    CONSTRAINT [FK_Match_MatchType_Match_Dataset_Source] FOREIGN KEY ([SourceDatasetCode]) REFERENCES [Match].[Dataset] ([DatasetCode]),
    CONSTRAINT [FK_Match_MatchType_Match_Dataset_Target] FOREIGN KEY ([TargetDatasetCode]) REFERENCES [Match].[Dataset] ([DatasetCode])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MatchType_01]
    ON [Match].[MatchType]([SourceDatasetCode] ASC, [TargetDatasetCode] ASC);


﻿CREATE PROCEDURE [Match].[BuildWrkDatasetOP]
	@IsSource bit
	,@MatchTypeID int = null
as

if (@IsSource = 1)
begin

	declare @SourceDatasetCode varchar(10) =
		(
		select
			MatchType.SourceDatasetCode
		from
			Match.MatchType
		where
			MatchType.MatchTypeID = @MatchTypeID
		)


	insert
	into
		Match.WrkSourceDataset
		(
		DatasetCode
		,DatasetRecno
		,ContextCode
		,DateOfBirth
		,StartTime
		,EndTime
		,NHSNumber
		,PatientNumber
		,Forename
		,Surname
		,Postcode
		,TreatmentFunctionCode
		)
	select
		DatasetCode = @SourceDatasetCode
		,DatasetRecno = Encounter.MergeEncounterRecno
		,ContextCode = Encounter.ContextCode
		,DateOfBirth = Encounter.DateOfBirth
		,StartTime = Encounter.AppointmentDate
		,EndTime = Encounter.AppointmentDate
		,NHSNumber = cast(replace(Encounter.NHSNumber, ' ', '') as varchar(17))
		,PatientNumber = Encounter.SourcePatientNo
		,Forename = Encounter.PatientForename
		,Surname = Encounter.PatientSurname
		,Postcode = Encounter.Postcode
		,TreatmentFunctionCode = TreatmentFunction.NationalSpecialtyCode
	from
		OP.BaseEncounter Encounter

	inner join OP.BaseEncounterReference EncounterReference
	on	EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join WH.Specialty TreatmentFunction
	on	TreatmentFunction.SourceSpecialtyID = EncounterReference.SpecialtyID

end

else
	insert
	into
		Match.WrkTargetDataset
		(
		MatchTypeID
		,DatasetRecno
		,ContextCode
		,DateOfBirth
		,StartTime
		,EndTime
		,NHSNumber
		,PatientNumber
		,Forename
		,Surname
		,Postcode
		,TreatmentFunctionCode
		)
	select
		MatchTypeID = @MatchTypeID
		,DatasetRecno = Encounter.MergeEncounterRecno
		,ContextCode = Encounter.ContextCode
		,DateOfBirth = Encounter.DateOfBirth
		,StartTime = Encounter.AppointmentDate
		,EndTime = Encounter.AppointmentDate
		,NHSNumber = cast(replace(Encounter.NHSNumber, ' ', '') as varchar(17))
		,PatientNumber = Encounter.SourcePatientNo
		,Forename = Encounter.PatientForename
		,Surname = Encounter.PatientSurname
		,Postcode = Encounter.Postcode
		,TreatmentFunctionCode = TreatmentFunction.NationalSpecialtyCode
	from
		OP.BaseEncounter Encounter

	inner join OP.BaseEncounterReference EncounterReference
	on	EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join WH.Specialty TreatmentFunction
	on	TreatmentFunction.SourceSpecialtyID = EncounterReference.SpecialtyID

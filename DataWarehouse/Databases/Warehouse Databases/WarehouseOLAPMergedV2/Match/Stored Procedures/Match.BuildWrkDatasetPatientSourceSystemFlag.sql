﻿CREATE procedure [Match].BuildWrkDatasetPatientSourceSystemFlag
	@IsSource bit
	,@MatchTypeID int
as

if (@IsSource = 1)
begin

	declare @SourceDatasetCode varchar(10) =
		(
		select
			MatchType.SourceDatasetCode
		from
			Match.MatchType
		where
			MatchType.MatchTypeID = @MatchTypeID
		)


	insert
	into
		Match.WrkSourceDataset
		(
		 DatasetCode
		,DatasetRecno
		,ContextCode
		,StartTime
		,EndTime
		,PatientNumber
		)
	select
		DatasetCode = @SourceDatasetCode
		,DatasetRecno = Encounter.PatientDatasetFlagRecno
		,ContextCode = Encounter.ContextCode
		,StartTime = Encounter.StartDate
		,EndTime = Encounter.EndDate
		,PatientNumber = Encounter.PatientID
	from
		Flag.PatientSourceSystemFlag Encounter

end

else

	insert
	into
		Match.WrkTargetDataset
		(
		MatchTypeID
		,DatasetRecno
		,ContextCode
		,StartTime
		,EndTime
		,PatientNumber
		)
	select
		@MatchTypeID
		,DatasetRecno = Encounter.PatientDatasetFlagRecno
		,ContextCode = Encounter.ContextCode
		,StartTime = Encounter.StartDate
		,EndTime = Encounter.EndDate
		,PatientNumber = Encounter.PatientID
	from
		Flag.PatientSourceSystemFlag Encounter

﻿CREATE procedure [Match].BuildWrkDatasetOpenWardStay
	@IsSource bit
	,@MatchTypeID int = null
as

if (@IsSource = 1)
begin

	declare @SourceDatasetCode varchar(10) =
		(
		select
			MatchType.SourceDatasetCode
		from
			Match.MatchType
		where
			MatchType.MatchTypeID = @MatchTypeID
		)

	insert
	into
		Match.WrkSourceDataset
		(
		 DatasetCode
		,DatasetRecno
		,ContextCode
		,StartTime
		,EndTime
		,PatientNumber
		)
	select
		DatasetCode = @SourceDatasetCode
		,DatasetRecno = Encounter.MergeEncounterRecno
		,ContextCode = Encounter.ContextCode
		,StartTime = Encounter.StartDate
		,EndTime = Encounter.EndDate
		,PatientNumber = Encounter.SourcePatientNo
	from
		APC.BaseWardStay Encounter
	where
		Encounter.EndDate is null

end

else

	insert
	into
		Match.WrkTargetDataset
		(
		MatchTypeID
		,DatasetRecno
		,ContextCode
		,StartTime
		,EndTime
		,PatientNumber
		)
	select
		@MatchTypeID
		,DatasetRecno = Encounter.MergeEncounterRecno
		,ContextCode = Encounter.ContextCode
		,StartTime = Encounter.StartDate
		,EndTime = Encounter.EndDate
		,PatientNumber = Encounter.SourcePatientNo
	from
		APC.BaseWardStay Encounter
	where
		Encounter.EndDate is null

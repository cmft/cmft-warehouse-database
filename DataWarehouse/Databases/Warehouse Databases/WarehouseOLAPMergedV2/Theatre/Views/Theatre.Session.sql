﻿
CREATE view [Theatre].[Session] as

select
	 MergeRecno
	,SourceUniqueID
	,ActualSessionMinutes
	,PlannedSessionMinutes
	,StartTime
	,EndTime
	,SessionNumber
	,TheatreCode
	,ConsultantCode
	,SpecialtyCode
	,SurgeonCode1
	,SurgeonCode2
	,AnaesthetistCode1
	,AnaesthetistCode2
	,AnaesthetistCode3
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaestheticNurseCode
	,LastUpdated
	,TimetableDetailCode
	,Comment
	,SessionOrder
	,CancelledFlag
	,CancelledMinutes
	,OverrunReason
	,OverrunReasonDate
	,OverrunReasonStaffCode
	,ContextCode

	,ActualSessionStartTime = ActualSession.ActualStartTime
	,ActualSessionEndTime = ActualSession.ActualEndTime

from
	Theatre.BaseSession

left join 
	(
	select
		 SessionSourceUniqueID
		,ActualStartTime = min(AnaestheticInductionTime)
		,ActualEndTime = max(OperationEndDate)
	from
		[$(Warehouse)].Theatre.OperationDetail
	group by
		SessionSourceUniqueID
	) ActualSession
on	ActualSession.SessionSourceUniqueID = BaseSession.SourceUniqueID

union

select
	 MergeRecno = -1
	,SourceUniqueID = null
	,ActualSessionMinutes = null
	,PlannedSessionMinutes = null
	,StartTime = null
	,EndTime = null
	,SessionNumber = null
	,TheatreCode = null
	,ConsultantCode = null
	,SpecialtyCode = null
	,SurgeonCode1 = null
	,SurgeonCode2 = null
	,AnaesthetistCode1 = null
	,AnaesthetistCode2 = null
	,AnaesthetistCode3 = null
	,ScoutNurseCode = null
	,InstrumentNurseCode = null
	,AnaestheticNurseCode = null
	,LastUpdated = null
	,TimetableDetailCode = null
	,Comment = null
	,SessionOrder = null
	,CancelledFlag = null
	,CancelledMinutes = null
	,OverrunReason = null
	,OverrunReasonDate = null
	,OverrunReasonStaffCode = null
	,ContextCode = null
	,ActualSessionStartTime = null
	,ActualSessionEndTime = null


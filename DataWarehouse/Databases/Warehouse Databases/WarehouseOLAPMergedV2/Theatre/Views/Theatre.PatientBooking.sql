﻿

CREATE view [Theatre].[PatientBooking] as

select
	 MergeRecno
	,SequenceNo
	,SourceUniqueID
	,DistrictNo
	,IntendedProcedureDate
	,IntendedTheatreCode
	,CustodianDetail
	,IntendedSessionTypeCode
	,Surname
	,Forename
	,DateOfBirth
	,PatientSourceUniqueID
	,TheatreCode
	,ConsultantCode
	,ConsultantCode2
	,BiohazardFlag
	,BloodProductsRequiredFlag
	,SpecialEquipmentFlag
	,PatientAgeInYears
	,PatientAgeInMonths
	,AdmissionWardCode
	,SessionStartTime
	,PatientEpisodeNumber
	,Operation = convert(varchar(4096) , Operation)
	,PreferredOrder
	,PatientBookingStatusCode
	,PatientStatusCode
	,PatientClassificationCode2
	,AnaestheticCode
	,AnaestheticTypeCode
	,SurgeonComment
	,SurgeonSpecialtyCode
	,BedNumber
	,WaitingListSourceUniqueID
	,SessionNo
	,OperationDuration
	,ChangeOverDuration
	,SexCode
	,SurgeonCode
	,BookingEventAdmittedTime
	,SurgeryRequestedTime
	,ReadyForSurgeryTime
	,VerifiedFlag
	,PlannedReturnFlag
	,EmergencyFlag
	,TheatreSessionSort
	,EstimatedStartTime
	,IntendedSurgeonCode
	,PatientClassificationCode
	,WardCode
	,Surgeon1SpecialtyCode
	,LogLastUpdated
	,RecordLogDetails
	,WaitingDetailsSourceUniqueID
	,AnaesthetistCode
	,Address1
	,Address3
	,Postcode
	,HomeTelephone
	,BusinessTelephone
	,AgeInDays
	,AdmissionTypeCode
	,ChangedSinceLastDataTransferFlag
	,ASAScoreCode
	,Unused4
	,Unused5
	,Unused6
	,Unused7
	,Unused8
	,PathologyRequiredFlag
	,XrayRequiredFlag
	,PathologyComment
	,XrayComment
	,BloodProductsRequiredComment
	,UnitsOfBloodProductRequired
	,WardAdvisedTime
	,WardAdvisedTime2
	,ModeOfTransportCode
	,NurseRequestingTransferCode
	,NurseReceivingNotificationCode
	,OxygenRequiredFlag
	,IVFlag
	,TransferredToWardCode
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,AnaesthetisNurseCode
	,InstrumentNurseCode
	,Technician1Code
	,Technician2Code
	,Unused1
	,InTheatreTime
	,CampusCode
	,ClinicalPriorityCode
	,StaffCode
	,AdditionalCaseFlag
	,UnplannedReturnCode
	,UnplannedReturnComment
	,AdmittingStaffCode
	,ReviewFlag
	,ExternalReferenceNo
	,UserFlag
	,AdmittedTime
	,ACCFlag
	,ACCNumber
	,ACCCode
	,MajorMinorCaseFlag
	,EstimatedStartTimeFixedFlag
	,PatientLanguageCode
	,LMOCode
	,ReferrerCode
	,MaritalStatusCode
	,PatientAddressStatus
	,LastUpdated
	,OldMRN
	,EstimatedArrivalTime
	,Address2
	,Address4
	,SentForTime
	,PorterLeftTime
	,PorterCode
	,CombinedCaseOrderNumberF
	,CombinedCaseConsultantCode
	,CombinedCaseTheatreCode
	,CombinedCaseSessionNo
	,CombinedCaseEstimateStartTime
	,CombinedCaseSpecialtyCode
	,CombinedCaseSurgeonCode
	,CombinedCaseAnaesthetistCode
	,CombinedCaseFlag
	,CombinedCaseFixedTimeFlag
	,PicklistPrintedFlag
	,PicklistMergedFlag
	,BookingInformationReceivedTime
	,InitiatedByStaffName
	,RequestingSurgerySpecialtyCode
	,HSCComment
	,SessionSourceUniqueID
	,CombinedCaseSessionSourceUniqueID
	,ReasonForOrderCode
	,PrintedFlag
	,PorterDelayCode
	,NHSNumber
	,CasenoteNumber
	,ContextCode

	,AgeCode =
	case
	when datediff(day, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) is null then 'Age Unknown'
	when datediff(day, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) <= 0 then '00 Days'
	when datediff(day, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) = 1 then '01 Day'
	when datediff(day, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) > 1 and
	datediff(day, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate)), 2) + ' Days'
	
	when datediff(day, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) > 28 and
	datediff(month, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) -
	case  when datepart(day, BasePatientBooking.DateOfBirth) > datepart(day, BasePatientBooking.IntendedProcedureDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) -
	case  when datepart(day, BasePatientBooking.DateOfBirth) > datepart(day, BasePatientBooking.IntendedProcedureDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) -
	case  when datepart(day, BasePatientBooking.DateOfBirth) > datepart(day, BasePatientBooking.IntendedProcedureDate) then 1 else 0 end > 1 and
	 datediff(month, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) -
	case  when datepart(day, BasePatientBooking.DateOfBirth) > datepart(day, BasePatientBooking.IntendedProcedureDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) -
	case  when datepart(day, BasePatientBooking.DateOfBirth) > datepart(day, BasePatientBooking.IntendedProcedureDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) - 
	(
	case 
	when	(datepart(m, BasePatientBooking.DateOfBirth) > datepart(m, BasePatientBooking.IntendedProcedureDate)) 
	or
		(
			datepart(m, BasePatientBooking.DateOfBirth) = datepart(m, BasePatientBooking.IntendedProcedureDate) 
		And	datepart(d, BasePatientBooking.DateOfBirth) > datepart(d, BasePatientBooking.IntendedProcedureDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, BasePatientBooking.DateOfBirth, BasePatientBooking.IntendedProcedureDate) - 
	(
	case 
	when	(datepart(m, BasePatientBooking.DateOfBirth) > datepart(m, BasePatientBooking.IntendedProcedureDate)) 
	or
		(
			datepart(m, BasePatientBooking.DateOfBirth) = datepart(m, BasePatientBooking.IntendedProcedureDate) 
		And	datepart(d, BasePatientBooking.DateOfBirth) > datepart(d, BasePatientBooking.IntendedProcedureDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,EstimatedEndTime =
		dateadd(minute , BasePatientBooking.OperationDuration , BasePatientBooking.EstimatedStartTime)

	,EstimatedOperationStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, BasePatientBooking.EstimatedStartTime), 0)
				,BasePatientBooking.EstimatedStartTime
			)
			,-1
		)

	,EstimatedOperationEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, BasePatientBooking.EstimatedStartTime), 0)
				,BasePatientBooking.EstimatedStartTime
			) + BasePatientBooking.OperationDuration
			,-1
		)

from
	Theatre.BasePatientBooking

union

select
	 MergeRecno = -1
	,SequenceNo = null
	,SourceUniqueID = null
	,DistrictNo = null
	,IntendedProcedureDate = null
	,IntendedTheatreCode = null
	,CustodianDetail = null
	,IntendedSessionTypeCode = null
	,Surname = null
	,Forename = null
	,DateOfBirth = null
	,PatientSourceUniqueID = null
	,TheatreCode = null
	,ConsultantCode = null
	,ConsultantCode2 = null
	,BiohazardFlag = null
	,BloodProductsRequiredFlag = null
	,SpecialEquipmentFlag = null
	,PatientAgeInYears = null
	,PatientAgeInMonths = null
	,AdmissionWardCode = null
	,SessionStartTime = null
	,PatientEpisodeNumber = null
	,Operation = null
	,PreferredOrder = null
	,PatientBookingStatusCode = null
	,PatientStatusCode = null
	,PatientClassificationCode2 = null
	,AnaestheticCode = null
	,AnaestheticTypeCode = null
	,SurgeonComment = null
	,SurgeonSpecialtyCode = null
	,BedNumber = null
	,WaitingListSourceUniqueID = null
	,SessionNo = null
	,OperationDuration = null
	,ChangeOverDuration = null
	,SexCode = null
	,SurgeonCode = null
	,BookingEventAdmittedTime = null
	,SurgeryRequestedTime = null
	,ReadyForSurgeryTime = null
	,VerifiedFlag = null
	,PlannedReturnFlag = null
	,EmergencyFlag = null
	,TheatreSessionSort = null
	,EstimatedStartTime = null
	,IntendedSurgeonCode = null
	,PatientClassificationCode = null
	,WardCode = null
	,Surgeon1SpecialtyCode = null
	,LogLastUpdated = null
	,RecordLogDetails = null
	,WaitingDetailsSourceUniqueID = null
	,AnaesthetistCode = null
	,Address1 = null
	,Address3 = null
	,Postcode = null
	,HomeTelephone = null
	,BusinessTelephone = null
	,AgeInDays = null
	,AdmissionTypeCode = null
	,ChangedSinceLastDataTransferFlag = null
	,ASAScoreCode = null
	,Unused4 = null
	,Unused5 = null
	,Unused6 = null
	,Unused7 = null
	,Unused8 = null
	,PathologyRequiredFlag = null
	,XrayRequiredFlag = null
	,PathologyComment = null
	,XrayComment = null
	,BloodProductsRequiredComment = null
	,UnitsOfBloodProductRequired = null
	,WardAdvisedTime = null
	,WardAdvisedTime2 = null
	,ModeOfTransportCode = null
	,NurseRequestingTransferCode = null
	,NurseReceivingNotificationCode = null
	,OxygenRequiredFlag = null
	,IVFlag = null
	,TransferredToWardCode = null
	,Surgeon2Code = null
	,Surgeon3Code = null
	,Anaesthetist2Code = null
	,Anaesthetist3Code = null
	,ScoutNurseCode = null
	,AnaesthetisNurseCode = null
	,InstrumentNurseCode = null
	,Technician1Code = null
	,Technician2Code = null
	,Unused1 = null
	,InTheatreTime = null
	,CampusCode = null
	,ClinicalPriorityCode = null
	,StaffCode = null
	,AdditionalCaseFlag = null
	,UnplannedReturnCode = null
	,UnplannedReturnComment = null
	,AdmittingStaffCode = null
	,ReviewFlag = null
	,ExternalReferenceNo = null
	,UserFlag = null
	,AdmittedTime = null
	,ACCFlag = null
	,ACCNumber = null
	,ACCCode = null
	,MajorMinorCaseFlag = null
	,EstimatedStartTimeFixedFlag = null
	,PatientLanguageCode = null
	,LMOCode = null
	,ReferrerCode = null
	,MaritalStatusCode = null
	,PatientAddressStatus = null
	,LastUpdated = null
	,OldMRN = null
	,EstimatedArrivalTime = null
	,Address2 = null
	,Address4 = null
	,SentForTime = null
	,PorterLeftTime = null
	,PorterCode = null
	,CombinedCaseOrderNumberF = null
	,CombinedCaseConsultantCode = null
	,CombinedCaseTheatreCode = null
	,CombinedCaseSessionNo = null
	,CombinedCaseEstimateStartTime = null
	,CombinedCaseSpecialtyCode = null
	,CombinedCaseSurgeonCode = null
	,CombinedCaseAnaesthetistCode = null
	,CombinedCaseFlag = null
	,CombinedCaseFixedTimeFlag = null
	,PicklistPrintedFlag = null
	,PicklistMergedFlag = null
	,BookingInformationReceivedTime = null
	,InitiatedByStaffName = null
	,RequestingSurgerySpecialtyCode = null
	,HSCComment = null
	,SessionSourceUniqueID = null
	,CombinedCaseSessionSourceUniqueID = null
	,ReasonForOrderCode = null
	,PrintedFlag = null
	,PorterDelayCode = null
	,NHSNumber = null
	,CasenoteNumber = null
	,ContextCode = null

	,AgeCode = null
	,EstimatedEndTime = null
	,EstimatedOperationStartTimeOfDay = null
	,EstimatedOperationEndTimeOfDay = null


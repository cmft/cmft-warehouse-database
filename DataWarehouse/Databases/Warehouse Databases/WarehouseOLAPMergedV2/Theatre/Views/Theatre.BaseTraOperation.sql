﻿create  view [Theatre].[BaseTraOperation] as

select
	 OperationID = Operation.OperationCode
	,Operation.Operation
	,OperationCode =
		case
		when len(Operation.OperationCode1) = 3 then Operation.OperationCode1 + '.9'
		else Operation.OperationCode1
		end

	,ContextCode = 'TRA||ORMIS'

from
	[$(TraffordWarehouse)].Theatre.Operation

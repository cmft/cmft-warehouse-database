﻿create view [Theatre].[BaseAdmissionType] as

select
	 AdmissionType.AdmissionTypeCode
	,AdmissionType.AdmissionType

	,ContextCode = 'CEN||ORMIS'
from
	[$(Warehouse)].Theatre.AdmissionType AdmissionType



﻿CREATE view [Theatre].[ASAScore] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceASAScoreID = SourceValueID
	,SourceASAScoreCode = SourceValueCode
	,SourceASAScore = SourceValue
	,LocalASAScoreID = LocalValueID
	,LocalASAScoreCode = LocalValueCode
	,LocalASAScore = LocalValue
	,NationalASAScoreID = NationalValueID
	,NationalASAScoreCode = NationalValueCode
	,NationalASAScore = NationalValue
FROM
	WH.Member

left join 
	(
	select
		*
	from
		Theatre.BaseASAScore

	union all

	select
		*
	from
		Theatre.BaseTraASAScore

	) BaseASAScore
on	BaseASAScore.ASAScoreCode = SourceValueCode
and	BaseASAScore.ContextCode = SourceContextCode

where
	AttributeCode = 'ASASCORE'

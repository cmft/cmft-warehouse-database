﻿create view Theatre.BaseSpecialty as

select
	 SpecialtyCode
	,Specialty
	,NationalSpecialtyCode = SpecialtyCode1
from
	[$(Warehouse)].Theatre.Specialty

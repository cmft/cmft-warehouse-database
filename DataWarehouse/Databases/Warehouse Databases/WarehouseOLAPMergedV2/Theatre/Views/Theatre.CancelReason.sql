﻿CREATE  view [Theatre].[CancelReason] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceCancelReasonID = SourceValueID
	,SourceCancelReasonCode = SourceValueCode
	,SourceCancelReason = SourceValue
	,LocalCancelReasonID = LocalValueID
	,LocalCancelReasonCode = LocalValueCode
	,LocalCancelReason = LocalValue
	,NationalCancelReasonID = NationalValueID
	,NationalCancelReasonCode = NationalValueCode
	,NationalCancelReason = NationalValue

	,CancelReasonGroupCode = coalesce(BaseCancelReason.CancelReasonGroupCode, -1)
	,CancelReasonGroup = coalesce(BaseCancelReason.CancelReasonGroup, 'N/A')
FROM
	WH.Member

left join 
	(
	select
		*
	from
		Theatre.BaseCancelReason

	union all

	select
		*
	from
		Theatre.BaseTraCancelReason

	) BaseCancelReason
on	BaseCancelReason.CancelReasonCode = SourceValueCode
and	BaseCancelReason.ContextCode = SourceContextCode

where
	AttributeCode = 'THEATRECANCELREASON'

﻿

CREATE view [Theatre].[Theatre] as

/* 
==============================================================================================
Description:

When		Who			What
?			?			Initial Coding
25/03/2015	Paul Egan	Added SourceAlternativeTheatreCode for theatre booking report.
===============================================================================================
*/

select
	 SourceContextCode
	,SourceContext
	,SourceTheatreID = SourceValueID
	,SourceTheatreCode = SourceValueCode
	,SourceTheatre = SourceValue
	,LocalTheatreID = LocalValueID
	,LocalTheatreCode = LocalValueCode
	,LocalTheatre = LocalValue
	,NationalTheatreID = NationalValueID
	,NationalTheatreCode = NationalValueCode
	,NationalTheatre = NationalValue

	,OperatingSuiteCode = coalesce(BaseTheatre.OperatingSuiteCode, -1)
	,OperatingSuite = coalesce(BaseTheatre.OperatingSuite, 'N/A')
	,Colour = coalesce(BaseTheatre.Colour, 'DarkGray')
	
	,SourceAlternativeTheatreCode = BaseTheatre.TheatreCode1

from
	WH.Member

left join 
	(
	select
		*
	from
		Theatre.BaseTheatre

	union all

	select
		*
	from
		Theatre.BaseTraTheatre

	) BaseTheatre
on	BaseTheatre.TheatreCode = SourceValueCode
and	BaseTheatre.ContextCode = SourceContextCode

where
	AttributeCode = 'THEATRE'



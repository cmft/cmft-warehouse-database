﻿
CREATE view [Theatre].[ProcedureDetail] as

select
	 MergeRecno
	,SequenceNo
	,SourceUniqueID
	,SupervisingSurgeon1Code
	,SupervisingSurgeon2Code
	,SupervisingSurgeon3Code
	,SupervisingAnaesthetistNurseCode
	,SupervisingScoutNurseCode
	,SupervisingInstrumentNurseCode
	,SupervisingTechnician1Code
	,SupervisingTechnician2Code
	,ProcedureConfirmedFlag
	,CMBSItemCode
	,Unused2
	,SupervisingAnaesthetist2Code
	,Unused3
	,SupervisingAnaesthetist3Code
	,ProcedureStartTime
	,ProcedureEndTime
	,ProcedureDescription
	,WoundType1Code
	,WoundType2Code
	,WoundType3Code
	,ProcedureCode = BaseOperation.OperationCode
	,OperationDetailSourceUniqueID
	,SupervisingAnaesthetist1Code
	,Unused4
	,Unused5
	,Unused6
	,LastUpdated
	,LogDetail
	,Surgeon1Code
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist1Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaesthetistNurseCode
	,Technician1Code
	,Technician2Code
	,ChangeOverAnaesthetistNurseCode
	,ChangeOverScoutNurseCode
	,ChangeOverInstrumentNurseCode
	,InventoryRecordCreatedFlag
	,PrincipleDiagnosisFlag
	,WoundType4Code
	,PrimaryProcedureFlag
	,WoundType5Code
	,WoundType6Code
	,PANCode
	,PADCode
	,RecordTimesCode
	,RecoverySourceUniqueID
	,ProcedureItemSourceUniqueID
	,BaseProcedureDetail.ContextCode
from
	Theatre.BaseProcedureDetail

left join Theatre.BaseOperation
on	BaseOperation.OperationID = BaseProcedureDetail.ProcedureCode

union

select
	 MergeRecno = -1
	,SequenceNo = null
	,SourceUniqueID = null
	,SupervisingSurgeon1Code = null
	,SupervisingSurgeon2Code = null
	,SupervisingSurgeon3Code = null
	,SupervisingAnaesthetistNurseCode = null
	,SupervisingScoutNurseCode = null
	,SupervisingInstrumentNurseCode = null
	,SupervisingTechnician1Code = null
	,SupervisingTechnician2Code = null
	,ProcedureConfirmedFlag = null
	,CMBSItemCode = null
	,Unused2 = null
	,SupervisingAnaesthetist2Code = null
	,Unused3 = null
	,SupervisingAnaesthetist3Code = null
	,ProcedureStartTime = null
	,ProcedureEndTime = null
	,ProcedureDescription = null
	,WoundType1Code = null
	,WoundType2Code = null
	,WoundType3Code = null
	,ProcedureCode = null
	,OperationDetailSourceUniqueID = null
	,SupervisingAnaesthetist1Code = null
	,Unused4 = null
	,Unused5 = null
	,Unused6 = null
	,LastUpdated = null
	,LogDetail = null
	,Surgeon1Code = null
	,Surgeon2Code = null
	,Surgeon3Code = null
	,Anaesthetist1Code = null
	,Anaesthetist2Code = null
	,Anaesthetist3Code = null
	,ScoutNurseCode = null
	,InstrumentNurseCode = null
	,AnaesthetistNurseCode = null
	,Technician1Code = null
	,Technician2Code = null
	,ChangeOverAnaesthetistNurseCode = null
	,ChangeOverScoutNurseCode = null
	,ChangeOverInstrumentNurseCode = null
	,InventoryRecordCreatedFlag = null
	,PrincipleDiagnosisFlag = null
	,WoundType4Code = null
	,PrimaryProcedureFlag = null
	,WoundType5Code = null
	,WoundType6Code = null
	,PANCode = null
	,PADCode = null
	,RecordTimesCode = null
	,RecoverySourceUniqueID = null
	,ProcedureItemSourceUniqueID = null
	,ContextCode = null


﻿CREATE  view [Theatre].[Staff] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourceStaffID = SourceValueID
	,SourceStaffCode = SourceValueCode
	,SourceStaff = SourceValue
	,LocalStaffID = LocalValueID
	,LocalStaffCode = LocalValueCode
	,LocalStaff = LocalValue
	,NationalStaffID = NationalValueID
	,NationalStaffCode = NationalValueCode
	,NationalStaff = NationalValue

	,StaffCategoryCode = coalesce(BaseStaff.StaffCategoryCode, -1)
	,StaffCategory = coalesce(BaseStaff.StaffCategory, 'N/A')
	,StaffType = coalesce(BaseStaff.StaffType, 'N/A')

	,BaseStaff.StaffCode1

	,Specialty.SourceSpecialtyID

FROM
	WH.Member

left join 
	(
	select
		*
	from
		Theatre.BaseStaff

	union all

	select
		*
	from
		Theatre.BaseTraStaff

	) BaseStaff

on	BaseStaff.StaffCode = SourceValueCode
and	BaseStaff.ContextCode = SourceContextCode

left join WH.Specialty
on	Specialty.SourceSpecialtyCode = cast(coalesce(BaseStaff.SpecialtyCode, 0) as varchar)
and	Specialty.SourceContextCode = Member.SourceContextCode

where
	AttributeCode = 'THEATRESTAFF'

﻿create view [Theatre].[BaseTraAdmissionType] as

select
	 AdmissionType.AdmissionTypeCode
	,AdmissionType.AdmissionType

	,ContextCode = 'TRA||ORMIS'
from
	[$(Warehouse)].Theatre.AdmissionType AdmissionType

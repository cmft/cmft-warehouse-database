﻿create view [Theatre].[BaseTraASAScore] as

select
	 ASAScore.ASAScoreCode
	,ASAScore.ASAScore

	,ContextCode = 'TRA||ORMIS'
from
	[$(TraffordWarehouse)].Theatre.ASAScore ASAScore

﻿create view Theatre.BaseCancelReason as

select
	 CancelReason.CancelReasonCode
	,CancelReason.CancelReason
	,CancelReason.CancelReasonGroupCode
	,CancelReasonGroup.CancelReasonGroup

	,ContextCode = 'CEN||ORMIS'
from
	[$(Warehouse)].Theatre.CancelReason CancelReason

inner join [$(Warehouse)].Theatre.CancelReasonGroup CancelReasonGroup
on	CancelReasonGroup.CancelReasonGroupCode = CancelReason.CancelReasonGroupCode

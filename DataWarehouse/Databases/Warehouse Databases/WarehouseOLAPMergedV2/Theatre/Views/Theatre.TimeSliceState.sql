﻿create view Theatre.TimeSliceState as

select
	TimeSliceStateID = 0 , TimeSliceState = 'Not Utilised'
union
select
	TimeSliceStateID = 1 , TimeSliceState = 'Booked'
union
select
	TimeSliceStateID = 2 , TimeSliceState = 'Utilised'


﻿
CREATE view [Theatre].[BaseOperation] as

select
	 OperationID = Operation.OperationCode
	,Operation.Operation
	,OperationCode =
		case
		when len(Operation.OperationCode1) = 3 then Operation.OperationCode1 + '.9'
		else Operation.OperationCode1
		end

	,ContextCode = 'CEN||ORMIS'

from
	[$(Warehouse)].Theatre.Operation



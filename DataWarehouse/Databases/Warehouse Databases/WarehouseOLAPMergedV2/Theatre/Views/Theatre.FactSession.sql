﻿


-- 20151005	RR	SessionSourceUniqueIDs are duplicated in the OperationDetail table, need to bring in Context Code to get uniqueness.  Updated the below where joined on this field to include a join on Context (issue identified because some lists had an actual start time in 2008 (Cen) and End time in 2014 (Tra))

CREATE view [Theatre].[FactSession] as

select
	 BaseSession.MergeRecno
	,BaseSessionReference.TheatreID

	,SessionDateID =
		coalesce(
			 SessionDate.DateID

			,case
			when BaseSession.StartTime is null
			then NullDate.DateID

			when cast(BaseSession.StartTime as date) < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,BaseSessionReference.ConsultantID
	,TimetableConsultantID = coalesce(BaseTimetableDetailReference.ConsultantID, NullStaff.SourceStaffID)
	,TemplateConsultantID = coalesce(BaseTimetableTemplateReference.ConsultantID, NullStaff.SourceStaffID)

	,AnaesthetistID = coalesce(BaseSessionReference.AnaesthetistID, NullStaff.SourceStaffID)
	,TimetableAnaesthetistID = coalesce(BaseTimetableDetailReference.AnaesthetistID, NullStaff.SourceStaffID)
	,TemplateAnaesthetistID = coalesce(BaseTimetableTemplateReference.AnaesthetistID, NullStaff.SourceStaffID)

	,SpecialtyID = coalesce(BaseSessionReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)
	,TimetableSpecialtyID = coalesce(BaseTimetableDetailReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)
	,TemplateSpecialtyID = coalesce(BaseTimetableTemplateReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)


	,SessionAllocationID =
		case	
		when 
			coalesce(
				 BaseTimetableDetailReference.ConsultantID
				,BaseSessionReference.ConsultantID
				,NullStaff.SourceStaffID
			) = 
			coalesce(
				 BaseSessionReference.ConsultantID
				,NullStaff.SourceStaffID
			)
		then 1 --Same Consultant

		when 
			coalesce(
				 BaseTimetableDetailReference.SpecialtyID
				,BaseSessionReference.SpecialtyID
				,NullSpecialty.SourceSpecialtyID
			) = 
			coalesce(
				 BaseSessionReference.SpecialtyID
				,NullSpecialty.SourceSpecialtyID
			)
		then 2 --Same Specialty

		else 3 --Different Specialty
		end

	,SessionPeriod.SessionPeriodID

	,BaseSession.CancelledFlag

	,Session = 1


	,SessionDuration =
		datediff(minute, BaseSession.StartTime, BaseSession.EndTime) -
		case
		when
			left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29'
		and left(convert(varchar, BaseSession.EndTime, 8), 5) > '16:29' 
		then 60
		else 0
		end

	,PlannedSessionDuration = BaseSession.PlannedSessionMinutes

	,TheoreticalSessionDuration =
		case
		when
			ActualSession.StartTime is null 
		or	ActualSession.EndTime is null
		then null
		else
			BaseSession.PlannedSessionMinutes -
			case
			when
				left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29'
			and left(convert(varchar, BaseSession.EndTime, 8), 5) > '16:29' 
			then 60
			else 0
			end
		end

	,ActualDuration =
		datediff(
			 minute
			,ActualSession.StartTime
			,ActualSession.EndTime
		) -
--discrepency here however, although all day session duration loses 60 minutes, actual duration does not
		case
		when
			left(convert(varchar, ActualSession.StartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, ActualSession.EndTime, 8), 5) > '16:29'
		then 0
		else 0
		end

	,OverUtilisation =
		case
		when datediff(minute, BaseSession.StartTime, BaseSession.EndTime) > datediff(minute, ActualSession.StartTime, ActualSession.EndTime)
		then null
		else datediff(minute, ActualSession.StartTime, ActualSession.EndTime) - datediff(minute, BaseSession.StartTime, BaseSession.EndTime)
		end

	,UnderUtilisation =
		case
		when datediff(minute, BaseSession.StartTime, BaseSession.EndTime) < datediff(minute, ActualSession.StartTime, ActualSession.EndTime)
		then null
		else  datediff(minute, BaseSession.StartTime, BaseSession.EndTime) - datediff(minute, ActualSession.StartTime, ActualSession.EndTime)
		end

	,PlannedStartTimeOfDay = left(convert(varchar, BaseSession.StartTime, 8), 5)

	,PlannedEndTimeOfDay = left(convert(varchar, BaseSession.EndTime, 8), 5)

	,EarlyStart =
		case
		when ActualSession.StartTime < BaseSession.StartTime
		then 1
		else 0
		end

	,LateStart =
		case
		when ActualSession.StartTime > BaseSession.StartTime
		then 1
		else 0
		end

	,EarlyFinish =
		case
		when ActualSession.EndTime < BaseSession.EndTime
		then 1
		else 0
		end

	,LateFinish =
		case
		when ActualSession.EndTime > BaseSession.EndTime
		then 1
		else 0
		end

	,EarlyStartMinutes =
		case
		when
			ActualSession.StartTime < BaseSession.StartTime
		and	datediff(minute , ActualSession.StartTime , BaseSession.StartTime) <= IntegerLimit.MaximumInteger
		then datediff(minute , ActualSession.StartTime , BaseSession.StartTime)
		else 0
		end

	,LateStartMinutes =
		case
		when
			ActualSession.StartTime > BaseSession.StartTime
		and	datediff(minute , BaseSession.StartTime , ActualSession.StartTime) <= IntegerLimit.MaximumInteger
		then datediff(minute , BaseSession.StartTime , ActualSession.StartTime)
		else 0
		end

	,EarlyFinishMinutes =
		case
		when
			ActualSession.EndTime < BaseSession.EndTime
		and	datediff(minute , ActualSession.EndTime , BaseSession.EndTime) <= IntegerLimit.MaximumInteger
		then datediff(minute , ActualSession.EndTime , BaseSession.EndTime)
		else 0
		end

	,LateFinishMinutes =
		case
		when
			ActualSession.EndTime > BaseSession.EndTime
		and	datediff(minute , BaseSession.EndTime , ActualSession.EndTime) <= IntegerLimit.MaximumInteger
		then datediff(minute , BaseSession.EndTime , ActualSession.EndTime)
		else 0
		end

	,AllTurnaroundsWithin15Minutes =
	CAST(
		case
		when not exists
			(
			select
				1
			from
				Theatre.BaseOperationDetail Operation
			where
				Operation.SessionSourceUniqueID = BaseSession.SourceUniqueID
			and Operation.ContextCode = BaseSession.ContextCode
			and datediff(minute, Operation.OperationEndDate, Operation.NextOperationAnaestheticInductionTime) > 15
			)
		then 1
		else 0
		end

		as bit
	)
	,OperationType.OperationTypeID

	,BaseSessionReference.ContextID
	
	,DirectorateID = Directorate.DirectorateID

from
	Theatre.BaseSession

inner join Theatre.BaseSessionReference
on	BaseSessionReference.MergeRecno = BaseSession.MergeRecno

left join WH.Calendar SessionDate
on	SessionDate.TheDate = cast(BaseSession.StartTime as date)

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'

left join Theatre.SessionPeriod
on	SessionPeriod.SessionPeriodID =
		case
		when
			left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, BaseSession.EndTime, 8), 5) > '16:29' then 1 --All Day

		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29' then 2 --Morning
		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '12:30' and '16:29' then 3 --Afternoon
		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '16:30' and '23:59' then 4 --Evening
		else -1
		end

left join Theatre.BaseTimetableDetail
on	BaseTimetableDetail.TimetableDetailCode = BaseSession.TimetableDetailCode
and	BaseTimetableDetail.SessionNumber = BaseSession.SessionNumber
and	BaseTimetableDetail.DayNumber = datepart(dw, BaseSession.StartTime)
and	BaseTimetableDetail.ContextCode = BaseSession.ContextCode

left join Theatre.BaseTimetableDetailReference
on	BaseTimetableDetailReference.MergeRecno = BaseTimetableDetail.MergeRecno

left join Theatre.BaseTimetableTemplate
on	BaseTimetableTemplate.TimetableTemplateCode = BaseTimetableDetail.TimetableTemplateCode
and	BaseTimetableTemplate.SessionNumber = BaseSession.SessionNumber
and	BaseTimetableTemplate.DayNumber = datepart(dw, BaseSession.StartTime)
and	BaseTimetableTemplate.ContextCode = BaseSession.ContextCode

left join Theatre.BaseTimetableTemplateReference
on	BaseTimetableTemplateReference.MergeRecno = BaseTimetableTemplate.MergeRecno

left join Theatre.Staff NullStaff --Get ValueID for null staff
on	NullStaff.SourceStaffCode = '-1'
and	NullStaff.SourceContextCode = BaseSession.ContextCode

left join WH.Specialty NullSpecialty --Get ValueID for null specialty
on	NullSpecialty.SourceSpecialtyCode = '-1'
and	NullSpecialty.SourceContextCode = BaseSession.ContextCode

left join 
	(
	select
		 SessionSourceUniqueID
		 ,ContextCode
		,StartTime = min(AnaestheticInductionTime)
		,EndTime = max(OperationEndDate)
	from
		Theatre.BaseOperationDetail
	group by
		SessionSourceUniqueID
		,ContextCode
	) ActualSession
on	ActualSession.SessionSourceUniqueID = BaseSession.SourceUniqueID
and ActualSession.ContextCode = BaseSession.ContextCode

left join Theatre.OperationType
on	OperationType.OperationTypeID =
		case
		when exists
			(
			select
				1
			from
				Theatre.BaseOperationDetail
			where
				BaseOperationDetail.SessionSourceUniqueID = BaseSession.SourceUniqueID
			and BaseOperationDetail.ContextCode = BaseSession.ContextCode
			and	BaseOperationDetail.OperationTypeCode in ('ELECTIVE', 'URGENT') --Elective, Urgent
			)
		then 1

		when exists
			(
			select
				1
			from
				Theatre.BaseOperationDetail
			where
				BaseOperationDetail.SessionSourceUniqueID = BaseSession.SourceUniqueID
				and BaseOperationDetail.ContextCode = BaseSession.ContextCode
			and	BaseOperationDetail.OperationTypeCode in ('EMERGENCY') --Emergency
			)
		then 2

		else -1
		end


left join Allocation.DatasetAllocation
on BaseSession.MergeRecno = DatasetAllocation.DatasetRecno
and DatasetAllocation.AllocationTypeID = 10 -- Directorate
and DatasetAllocation.DatasetCode = 'SESSION'

left join Allocation.Allocation
on DatasetAllocation.AllocationID = Allocation.AllocationID

left join WH.Directorate
on Allocation.SourceAllocationID = Directorate.DirectorateCode

cross join
	(
	select
		 MinimumInteger = MIN(Integer.TheInteger)
		,MaximumInteger = MAX(Integer.TheInteger)
	from
		WH.Integer
	) IntegerLimit




﻿

CREATE view [Theatre].[ConsultantActivity] as 

/*
	DG - 2 Jun 2015
	Used to map the ConsultantCode to the Consultant attribute as already mapped to the 
	Theatre Staff attribute in the main tables
*/

select 
	ConsultantCode
	,ContextCode
from
	Theatre.BaseCancellation
where
	ConsultantCode is not null

union all

select 
	ConsultantCode
	,ContextCode
from
	Theatre.BaseOperationDetail
where
	ConsultantCode is not null

union all

select 
	ConsultantCode
	,ContextCode
from
	Theatre.BasePatientBooking
where
	ConsultantCode is not null

union all

select 
	ConsultantCode
	,ContextCode
from
	Theatre.BaseSession
where
	ConsultantCode is not null

union all

select 
	ConsultantCode
	,ContextCode
from
	Theatre.BaseTimetableDetail
where
	ConsultantCode is not null

union all

select 
	ConsultantCode
	,ContextCode
from
	Theatre.BaseTimetableTemplate
where
	ConsultantCode is not null

	
	



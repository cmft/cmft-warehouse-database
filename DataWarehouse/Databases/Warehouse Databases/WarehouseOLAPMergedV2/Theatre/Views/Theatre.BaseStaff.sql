﻿create view Theatre.BaseStaff as

select
	 Staff.StaffCode

	,StaffName =
		coalesce(
			coalesce(
				Staff.Forename
				,''
			) + ' ' +
			coalesce(
				Staff.Surname
				,''
			)
			,StaffCode1 + ' - No Description'
		)

	,StaffCategory.StaffCategoryCode
	,StaffCategory.StaffCategory
	,Staff.SpecialtyCode

	,StaffType =
		case
		when coalesce(StaffCode1, '') = ''
		then 'Unknown'
		when StaffCode1 like 'C[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		then
			case
			when AnaesthetistFlag = 1
			then 'Anaesthetist'
			when SurgeonFlag = 1
			then 'Surgeon'
			else 'Other Consultant'
			end
		else 'Other'
		end

	,StaffCode1

	,ContextCode = 'CEN||ORMIS'

from
	[$(Warehouse)].Theatre.Staff Staff

left join [$(Warehouse)].Theatre.StaffCategory StaffCategory
on	StaffCategory.StaffCategoryCode = Staff.StaffCategoryCode

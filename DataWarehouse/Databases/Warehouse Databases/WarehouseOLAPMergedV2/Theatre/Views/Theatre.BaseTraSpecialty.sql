﻿create view [Theatre].[BaseTraSpecialty] as

select
	 SpecialtyCode
	,Specialty
	,NationalSpecialtyCode = SpecialtyCode1
from
	[$(TraffordWarehouse)].Theatre.Specialty

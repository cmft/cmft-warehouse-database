﻿CREATE view [Theatre].[AdmissionType] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAdmissionTypeID = SourceValueID
	,SourceAdmissionTypeCode = SourceValueCode
	,SourceAdmissionType = SourceValue
	,LocalAdmissionTypeID = LocalValueID
	,LocalAdmissionTypeCode = LocalValueCode
	,LocalAdmissionType = LocalValue
	,NationalAdmissionTypeID = NationalValueID
	,NationalAdmissionTypeCode = NationalValueCode
	,NationalAdmissionType = NationalValue
FROM
	WH.Member

left join 
	(
	select
		*
	from
		Theatre.BaseAdmissionType

	union all

	select
		*
	from
		Theatre.BaseTraAdmissionType

	) BaseAdmissionType
on	BaseAdmissionType.AdmissionTypeCode = SourceValueCode
and	BaseAdmissionType.ContextCode = SourceContextCode

where
	AttributeCode = 'THEATREADMISSIONTYPE'

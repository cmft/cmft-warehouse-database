﻿create view [Theatre].[BaseTraCancelReason] as

select
	 CancelReason.CancelReasonCode
	,CancelReason.CancelReason
	,CancelReason.CancelReasonGroupCode
	,CancelReasonGroup.CancelReasonGroup

	,ContextCode = 'TRA||ORMIS'
from
	[$(TraffordWarehouse)].Theatre.CancelReason CancelReason

inner join [$(Warehouse)].Theatre.CancelReasonGroup CancelReasonGroup
on	CancelReasonGroup.CancelReasonGroupCode = CancelReason.CancelReasonGroupCode


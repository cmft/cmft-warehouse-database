﻿
CREATE view [Theatre].[OperationDetail] as

select
	 MergeRecno
	,DistrictNo
	,SourceUniqueID
	,PatientAgeInYears
	,OperationDay
	,OperationStartDate
	,TheatreCode
	,OperationDate
	,OperationEndDate
	,DateOfBirth
	,OperationTypeCode
	,OperationOrder
	,Forename
	,InSuiteTime
	,Surname
	,InAnaestheticTime
	,InRecoveryTime
	,BiohazardFlag
	,ASAScoreCode
	,UnplannedReturnReasonCode
	,ReviewFlag
	,OperationDetailSourceUniqueID
	,Operation = convert(varchar(4096) , Operation)
	,PatientDiedFlag
	,CauseOfDeath
	,DiagnosisCode
	,PatientAgeInYearsMonths
	,PatientAgeInYearsDays
	,SexCode
	,TransferredToWardCode
	,PatientSourceUniqueNo
	,PatientClassificationCode
	,PatientBookingSourceUniqueID
	,SpecialtyCode
	,FromWardCode
	,ToWardCode
	,ConsultantCode
	,Address1
	,Address3
	,Postcode
	,HomeTelephone
	,BusinessTelephone
	,Surgeon1Code
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist1Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,AnaesthetistNurseCode
	,InstrumentNurseCode
	,Technician1Code
	,Technician2Code
	,Technician3Code
	,SessionNumber
	,CampusCode
	,SpecimenCode
	,DischargeTime
	,StaffInChargeCode
	,EscortReturnedTime
	,OperationCancelledFlag
	,DelayCode
	,AnaestheticInductionTime
	,ReadyToDepartTime
	,OtherCommentCode
	,PACUClassificationCode
	,PostOperationInstructionCode
	,ClinicalPriorityCode
	,EmergencyOperationSurgeonCode
	,SecondaryStaffCode
	,HospitalDischargeDate
	,HospitalEpisodeNo
	,UnplannedReturnToTheatreFlag
	,AdmissionTime
	,WaitingListSourceUniqueNo
	,AdmissionTypeCode
	,CalfCompressorUsedFlag
	,OperatingRoomReadyTime
	,DressingTime
	,NursingSetupCompleteTime
	,LastUpdated
	,ProceduralStatusCode
	,ACCNumber
	,ACCIndicatorCode
	,ACCCode
	,UpdatedFlag
	,PatientAddressStatus
	,OperationKey
	,ExtubationTime
	,Address2
	,Address4
	,SentForTime
	,PorterLeftTime
	,PorterCode
	,SecondaryTheatreCode
	,SecondaryConsultantCode
	,SecondaryOrderNumberCode
	,SecondarySessionNumber
	,AdditionalFlag
	,DictatedSurgeonNoteCode
	,AnaestheticReadyTime
	,PreparationReadyTime
	,SessionSourceUniqueID
	,CombinedCaseSessionSourceUniqueID
	,IncisionDetailCode
	,ContextCode
	,NextOperationAnaestheticInductionTime
from
	Theatre.BaseOperationDetail

union

select
	 MergeRecno = -1
	,DistrictNo = null
	,SourceUniqueID = null
	,PatientAgeInYears = null
	,OperationDay = null
	,OperationStartDate = null
	,TheatreCode = null
	,OperationDate = null
	,OperationEndDate = null
	,DateOfBirth = null
	,OperationTypeCode = null
	,OperationOrder = null
	,Forename = null
	,InSuiteTime = null
	,Surname = null
	,InAnaestheticTime = null
	,InRecoveryTime = null
	,BiohazardFlag = null
	,ASAScoreCode = null
	,UnplannedReturnReasonCode = null
	,ReviewFlag = null
	,OperationDetailSourceUniqueID = null
	,Operation = null
	,PatientDiedFlag = null
	,CauseOfDeath = null
	,DiagnosisCode = null
	,PatientAgeInYearsMonths = null
	,PatientAgeInYearsDays = null
	,SexCode = null
	,TransferredToWardCode = null
	,PatientSourceUniqueNo = null
	,PatientClassificationCode = null
	,PatientBookingSourceUniqueID = null
	,SpecialtyCode = null
	,FromWardCode = null
	,ToWardCode = null
	,ConsultantCode = null
	,Address1 = null
	,Address3 = null
	,Postcode = null
	,HomeTelephone = null
	,BusinessTelephone = null
	,Surgeon1Code = null
	,Surgeon2Code = null
	,Surgeon3Code = null
	,Anaesthetist1Code = null
	,Anaesthetist2Code = null
	,Anaesthetist3Code = null
	,ScoutNurseCode = null
	,AnaesthetistNurseCode = null
	,InstrumentNurseCode = null
	,Technician1Code = null
	,Technician2Code = null
	,Technician3Code = null
	,SessionNumber = null
	,CampusCode = null
	,SpecimenCode = null
	,DischargeTime = null
	,StaffInChargeCode = null
	,EscortReturnedTime = null
	,OperationCancelledFlag = null
	,DelayCode = null
	,AnaestheticInductionTime = null
	,ReadyToDepartTime = null
	,OtherCommentCode = null
	,PACUClassificationCode = null
	,PostOperationInstructionCode = null
	,ClinicalPriorityCode = null
	,EmergencyOperationSurgeonCode = null
	,SecondaryStaffCode = null
	,HospitalDischargeDate = null
	,HospitalEpisodeNo = null
	,UnplannedReturnToTheatreFlag = null
	,AdmissionTime = null
	,WaitingListSourceUniqueNo = null
	,AdmissionTypeCode = null
	,CalfCompressorUsedFlag = null
	,OperatingRoomReadyTime = null
	,DressingTime = null
	,NursingSetupCompleteTime = null
	,LastUpdated = null
	,ProceduralStatusCode = null
	,ACCNumber = null
	,ACCIndicatorCode = null
	,ACCCode = null
	,UpdatedFlag = null
	,PatientAddressStatus = null
	,OperationKey = null
	,ExtubationTime = null
	,Address2 = null
	,Address4 = null
	,SentForTime = null
	,PorterLeftTime = null
	,PorterCode = null
	,SecondaryTheatreCode = null
	,SecondaryConsultantCode = null
	,SecondaryOrderNumberCode = null
	,SecondarySessionNumber = null
	,AdditionalFlag = null
	,DictatedSurgeonNoteCode = null
	,AnaestheticReadyTime = null
	,PreparationReadyTime = null
	,SessionSourceUniqueID = null
	,CombinedCaseSessionSourceUniqueID = null
	,IncisionDetailCode = null
	,ContextCode = null
	,NextOperationAnaestheticInductionTime = null


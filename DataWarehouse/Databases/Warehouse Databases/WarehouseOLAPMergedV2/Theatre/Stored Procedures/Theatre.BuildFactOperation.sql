﻿
CREATE procedure [Theatre].[BuildFactOperation] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @inserted int



truncate table Theatre.FactOperation

insert
into
	Theatre.FactOperation
(
	 MergeRecno
	,TheatreID
	,CancelReasonID
	,ConsultantID
	,SurgeonID
	,AnaesthetistID
	,OperationID
	,AgeID
	,SexID
	,AdmissionTypeID
	,OperationDateID
	,OperationTypeID
	,SessionPeriodID
	,SessionConsultantID
	,TimetableConsultantID
	,TemplateConsultantID
	,SessionAnaesthetistID
	,TimetableAnaesthetistID
	,TemplateAnaesthetistID
	,SessionSpecialtyID
	,TimetableSpecialtyID
	,TemplateSpecialtyID
	,Biohazard
	,ASAScoreID
	,OperationStartTimeOfDay
	,OperationEndTimeOfDay
	,ReceptionTime
	,ReceptionWaitTime
	,AnaestheticWaitTime
	,AnaestheticTime
	,OperationTime
	,RecoveryTime
	,ReturnTime
	,PatientTime
	,ActiveTheatreTime
	,Operation
	,PatientTimeWithinPlan
	,TurnaroundTime
	,OperationWithTurnaroundTime
	,ContextID
	,DirectorateID
)
select
	 BaseOperationDetail.MergeRecno
	,BaseOperationDetailReference.TheatreID

	,CancelReasonID =
		coalesce(
			 BaseCancellationReference.CancelReasonID
			,NullCancelReason.SourceCancelReasonID
		)

	,BaseOperationDetailReference.ConsultantID
	,BaseOperationDetailReference.SurgeonID
	,BaseOperationDetailReference.AnaesthetistID

	,OperationID =
		coalesce(
			 BaseProcedureDetailReference.OperationID
			,-1
		)

	,Age.AgeID
	,BaseOperationDetailReference.SexID
	,BaseOperationDetailReference.AdmissionTypeID

	,OperationDateID =
		coalesce(
			 OperationDate.DateID

			,case
			when BaseOperationDetail.OperationDate is null
			then NullDate.DateID

			when BaseOperationDetail.OperationDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,OperationType.OperationTypeID
	,SessionPeriod.SessionPeriodID

	,SessionConsultantID = coalesce(BaseSessionReference.ConsultantID, NullStaff.SourceStaffID)
	,TimetableConsultantID = coalesce(BaseTimetableDetailReference.ConsultantID, NullStaff.SourceStaffID)
	,TemplateConsultantID = coalesce(BaseTimetableTemplateReference.ConsultantID, NullStaff.SourceStaffID)

	,SessionAnaesthetistID = coalesce(BaseSessionReference.AnaesthetistID, NullStaff.SourceStaffID)
	,TimetableAnaesthetistID = coalesce(BaseTimetableDetailReference.AnaesthetistID, NullStaff.SourceStaffID)
	,TemplateAnaesthetistID = coalesce(BaseTimetableTemplateReference.AnaesthetistID, NullStaff.SourceStaffID)

	,SessionSpecialtyID = coalesce(BaseSessionReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)
	,TimetableSpecialtyID = coalesce(BaseTimetableDetailReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)
	,TemplateSpecialtyID = coalesce(BaseTimetableTemplateReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)

	,Biohazard = convert(bit, BaseOperationDetail.BiohazardFlag)
	,ASAScoreID = BaseOperationDetailReference.ASAScoreID

	,OperationStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,cast(BaseOperationDetail.AnaestheticInductionTime as date)
				,BaseOperationDetail.AnaestheticInductionTime
			)
			,-1
		)

	,OperationEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,cast(BaseOperationDetail.OperationEndDate as date)
				,BaseOperationDetail.OperationEndDate
			)
			,-1
		)

	,ReceptionTime =
		datediff(minute, BaseOperationDetail.SentForTime, BaseOperationDetail.InSuiteTime)

	,ReceptionWaitTime =
		datediff(minute, BaseOperationDetail.InSuiteTime, BaseOperationDetail.InAnaestheticTime)

	,AnaestheticWaitTime =
		datediff(minute, BaseOperationDetail.InAnaestheticTime, BaseOperationDetail.AnaestheticInductionTime)

	,AnaestheticTime =
		datediff(minute, BaseOperationDetail.AnaestheticInductionTime, BaseOperationDetail.AnaestheticReadyTime)

	,OperationTime =
		datediff(minute, BaseProcedureDetail.ProcedureStartTime, BaseProcedureDetail.ProcedureEndTime)

	,RecoveryTime =
		datediff(minute, BaseOperationDetail.InRecoveryTime, BaseOperationDetail.ReadyToDepartTime)

	,ReturnTime =
		datediff(minute, BaseOperationDetail.ReadyToDepartTime, BaseOperationDetail.DischargeTime)

	,PatientTime =
		datediff(minute, BaseOperationDetail.AnaestheticInductionTime, BaseProcedureDetail.ProcedureEndTime)

	,ActiveTheatreTime =
		datediff(minute, BaseOperationDetail.AnaestheticInductionTime, BaseOperationDetail.InRecoveryTime)

	,Operation = 1

	,PatientTimeWithinPlan =
		case
		when BaseOperationDetail.AnaestheticInductionTime > BaseSession.EndTime or BaseProcedureDetail.ProcedureEndTime < BaseSession.StartTime then 0
		else
			datediff(
				 minute
				,(
					select top 1
						StartTime
					from
						(
						select
							StartTime = AnaestheticInductionTime
						from
							Theatre.BaseOperationDetail OperationStart
						where
							OperationStart.SourceUniqueID = BaseOperationDetail.SourceUniqueID

						union

						select
							StartTime = StartTime
						from
							Theatre.BaseSession SessionStart
						where
							SessionStart.SourceUniqueID = BaseOperationDetail.SessionSourceUniqueID
						) StartTime
					order by
						StartTime desc
				)
				,(
					select top 1
						EndTime
					from
						(
						select
							EndTime = ProcedureEndTime
						from
							Theatre.BaseOperationDetail OperationEnd
						where
							OperationEnd.SourceUniqueID = BaseOperationDetail.SourceUniqueID

						union

						select
							EndTime = EndTime
						from
							Theatre.BaseSession SessionEnd
						where
							SessionEnd.SourceUniqueID = BaseOperationDetail.SessionSourceUniqueID
						) EndTime
					order by
						EndTime
				)
			)
		end

	,TurnaroundTime =
		case
		when BaseOperationDetail.OperationEndDate > BaseOperationDetail.NextOperationAnaestheticInductionTime
		then null
		else datediff(minute, BaseOperationDetail.OperationEndDate, BaseOperationDetail.NextOperationAnaestheticInductionTime)
		end

	,OperationWithTurnaroundTime =
		case
		when datediff(minute, BaseOperationDetail.OperationEndDate, BaseOperationDetail.NextOperationAnaestheticInductionTime) > 0
		then 1
		else null
		end

	,BaseOperationDetailReference.ContextID
	
	,DirectorateID = coalesce(Directorate.DirectorateID,15)
from
	Theatre.BaseOperationDetail

inner join Theatre.BaseOperationDetailReference
on	BaseOperationDetailReference.MergeRecno = BaseOperationDetail.MergeRecno

left join Theatre.BaseCancellation
on	BaseCancellation.OperationDetailSourceUniqueID = BaseOperationDetail.SourceUniqueID
and	BaseCancellation.ContextCode = BaseOperationDetail.ContextCode
and	not exists
	(
	select
		1
	from
		Theatre.BaseCancellation Previous
	where
		Previous.OperationDetailSourceUniqueID = BaseOperationDetail.SourceUniqueID
	and	Previous.ContextCode = BaseOperationDetail.ContextCode
	and	(
			Previous.CancellationDate > BaseCancellation.CancellationDate
		or	(
				Previous.CancellationDate = BaseCancellation.CancellationDate
			and	Previous.SourceUniqueID > BaseCancellation.SourceUniqueID
			)
		)
	)

left join Theatre.BaseCancellationReference
on	BaseCancellationReference.MergeRecno = BaseCancellation.MergeRecno

left join Theatre.CancelReason NullCancelReason --Get ValueID for null cancel reason
on	NullCancelReason.SourceCancelReasonCode = '-1'
and	NullCancelReason.SourceContextCode = BaseOperationDetail.ContextCode

left join Theatre.BaseProcedureDetail
on	BaseProcedureDetail.OperationDetailSourceUniqueID = BaseOperationDetail.SourceUniqueID
and	BaseProcedureDetail.ContextCode = BaseOperationDetail.ContextCode
and	BaseProcedureDetail.PrimaryProcedureFlag = 1
and	not exists
		(
		select
			1
		from
			Theatre.BaseProcedureDetail EarlierProcedure
		where
			EarlierProcedure.OperationDetailSourceUniqueID = BaseProcedureDetail.OperationDetailSourceUniqueID
		and	EarlierProcedure.ContextCode = BaseOperationDetail.ContextCode
		and	EarlierProcedure.PrimaryProcedureFlag = 1
		and	EarlierProcedure.SourceUniqueID < BaseProcedureDetail.SourceUniqueID -- some ops have more than one Primary Procedure!
		)
left join Theatre.BaseProcedureDetailReference
on	BaseProcedureDetailReference.MergeRecno = BaseProcedureDetail.MergeRecno

left join WH.Age
on	Age.AgeCode =
	case
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) is null then 'Age Unknown'
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) <= 0 then '00 Days'
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) = 1 then '01 Day'
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) > 1 and
	datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate)), 2) + ' Days'
	
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) > 28 and
	datediff(month, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end > 1 and
	 datediff(month, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, BaseOperationDetail.DateOfBirth,BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) - 
	(
	case 
	when	(datepart(m, BaseOperationDetail.DateOfBirth) > datepart(m, BaseOperationDetail.OperationDate)) 
	or
		(
			datepart(m, BaseOperationDetail.DateOfBirth) = datepart(m, BaseOperationDetail.OperationDate) 
		And	datepart(d, BaseOperationDetail.DateOfBirth) > datepart(d, BaseOperationDetail.OperationDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) - 
	(
	case 
	when	(datepart(m, BaseOperationDetail.DateOfBirth) > datepart(m, BaseOperationDetail.OperationDate)) 
	or
		(
			datepart(m, BaseOperationDetail.DateOfBirth) = datepart(m, BaseOperationDetail.OperationDate) 
		And	datepart(d, BaseOperationDetail.DateOfBirth) > datepart(d, BaseOperationDetail.OperationDate)
		) then 1 else 0 end
	)), 2) + ' Years'
	end


left join WH.Calendar OperationDate
on	OperationDate.TheDate = BaseOperationDetail.OperationDate

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'

left join Theatre.OperationType
on	OperationType.OperationTypeCode = coalesce(BaseOperationDetail.OperationTypeCode, '-1')

left join Theatre.BaseSession
on	BaseSession.SourceUniqueID = BaseOperationDetail.SessionSourceUniqueID
and	BaseSession.ContextCode = BaseOperationDetail.ContextCode

left join Theatre.BaseSessionReference
on	BaseSessionReference.MergeRecno = BaseSession.MergeRecno

left join Theatre.SessionPeriod
on	SessionPeriod.SessionPeriodID =
		case
		when
			left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, BaseSession.EndTime, 8), 5) > '16:29' then 1 --All Day

		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29' then 2 --Morning
		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '12:30' and '16:29' then 3 --Afternoon
		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '16:30' and '23:59' then 4 --Evening
		else -1
		end

left join Theatre.BaseTimetableDetail
on	BaseTimetableDetail.TimetableDetailCode = BaseSession.TimetableDetailCode
and	BaseTimetableDetail.SessionNumber = BaseSession.SessionNumber
and	BaseTimetableDetail.DayNumber = datepart(dw, BaseSession.StartTime)
and	BaseTimetableDetail.ContextCode = BaseSession.ContextCode

left join Theatre.BaseTimetableDetailReference
on	BaseTimetableDetailReference.MergeRecno = BaseOperationDetail.MergeRecno

left join Theatre.BaseTimetableTemplate
on	BaseTimetableTemplate.TimetableTemplateCode = BaseTimetableDetail.TimetableTemplateCode
and	BaseTimetableTemplate.SessionNumber = BaseSession.SessionNumber
and	BaseTimetableTemplate.DayNumber = datepart(dw, BaseSession.StartTime)
and	BaseTimetableTemplate.ContextCode = BaseSession.ContextCode

left join Theatre.BaseTimetableTemplateReference
on	BaseTimetableTemplateReference.MergeRecno = BaseOperationDetail.MergeRecno

left join Theatre.Staff NullStaff --Get ValueID for null staff
on	NullStaff.SourceStaffCode = '-1'
and	NullStaff.SourceContextCode = BaseOperationDetail.ContextCode

left join WH.Specialty NullSpecialty --Get ValueID for null specialty
on	NullSpecialty.SourceSpecialtyCode = '-1'
and	NullSpecialty.SourceContextCode = BaseOperationDetail.ContextCode

left join Allocation.DatasetAllocation
on BaseOperationDetail.MergeRecno = DatasetAllocation.DatasetRecno
and DatasetAllocation.AllocationTypeID = 10 -- Directorate
and DatasetAllocation.DatasetCode = 'OPERATION'

left join Allocation.Allocation
on DatasetAllocation.AllocationID = Allocation.AllocationID

left join WH.Directorate
on Allocation.SourceAllocationID = Directorate.DirectorateCode


select
	 @inserted = @@ROWCOUNT


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



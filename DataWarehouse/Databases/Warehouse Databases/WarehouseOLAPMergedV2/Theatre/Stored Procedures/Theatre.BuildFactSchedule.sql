﻿CREATE procedure [Theatre].[BuildFactSchedule] as

declare @today date

set @today = cast(getdate() as date)

/*********************************
** build time slice work tables **
*********************************/


-- operation
select


	 PatientBookingSourceUniqueID =
		case
		when BaseOperationDetail.PatientBookingSourceUniqueID = 0 then -1
		else BaseOperationDetail.PatientBookingSourceUniqueID
		end

	,BaseOperationDetail.ContextCode

	,OperationSourceUniqueID = BaseOperationDetail.SourceUniqueID

	,SessionSourceUniqueID =
		cast(coalesce(BaseSession.SourceUniqueID , -1) as int)

	,BaseOperationDetail.OperationDate
	,BaseOperationDetailReference.TheatreID
	,TimeBand.TimeBandCode
	,TimeSliceStateID = 2 --utilised

	,OperationDetailMergeRecno = BaseOperationDetail.MergeRecno

	,PatientBookingMergeRecno =
		case
		when BaseOperationDetail.PatientBookingSourceUniqueID = 0 then -1
		else 
			(
			select
				BasePatientBooking.MergeRecno
			from
				Theatre.BasePatientBooking
			where
				BasePatientBooking.SourceUniqueID = BaseOperationDetail.PatientBookingSourceUniqueID
			and	BasePatientBooking.ContextCode = BaseOperationDetail.ContextCode
			)		end

	,SessionMergeRecno =
		coalesce(
			 BaseSession.MergeRecno
			,-1
		)
into
	#wkOperationTimeSlice
from
	Theatre.BaseOperationDetail

inner join Theatre.BaseOperationDetailReference
on	BaseOperationDetailReference.MergeRecno = BaseOperationDetail.MergeRecno

inner join WH.TimeBand
on	TimeBand.TimeBandCode between 
	coalesce(
		datediff(
			 minute
			,cast(BaseOperationDetail.AnaestheticInductionTime as date)
			,BaseOperationDetail.AnaestheticInductionTime
		)
		,-1
	)

and	(
		coalesce(
			datediff(
				 minute
				,cast(BaseOperationDetail.OperationEndDate as date)
				,BaseOperationDetail.OperationEndDate
			)
			,-1
		)

	 - 1
	)

left join Theatre.BaseSession
on	BaseSession.SourceUniqueID = BaseOperationDetail.SessionSourceUniqueID
and	BaseSession.ContextCode = BaseOperationDetail.ContextCode

where
	BaseOperationDetail.OperationDate between '28 Mar 2011' and dateadd(day , -1 , @today)



-- patient booking
select
	 PatientBookingSourceUniqueID = BasePatientBooking.SourceUniqueID

	,BasePatientBooking.ContextCode

	,OperationSourceUniqueID = -1

	,SessionSourceUniqueID =
		coalesce(BaseSession.SourceUniqueID , -1)

	,BasePatientBooking.IntendedProcedureDate
	,BasePatientBookingReference.TheatreID
	,TimeBand.TimeBandCode
	,TimeSliceStateID = 1 --booked

	,OperationDetailMergeRecno = -1
	,PatientBookingMergeRecno = BasePatientBooking.MergeRecno
	,SessionMergeRecno = coalesce(BaseSession.MergeRecno, -1)

into
	#wkFutureOperationTimeSlice
from
	Theatre.BasePatientBooking

inner join Theatre.BasePatientBookingReference
on	BasePatientBookingReference.MergeRecno = BasePatientBooking.MergeRecno

inner join WH.TimeBand
on	TimeBand.TimeBandCode between 
	coalesce(
		datediff(
			 minute
			,dateadd(day, datediff(day, 0, BasePatientBooking.EstimatedStartTime), 0)
			,BasePatientBooking.EstimatedStartTime
		)
		,-1
	)

and (
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, BasePatientBooking.EstimatedStartTime), 0)
				,BasePatientBooking.EstimatedStartTime
			) + BasePatientBooking.OperationDuration
			,-1
		)
		 - 1
	)


left join Theatre.BaseSession
on	BaseSession.SourceUniqueID = BasePatientBooking.SessionSourceUniqueID
and	BaseSession.ContextCode = BasePatientBooking.ContextCode

where
	not exists
		(
		select
			1
		from
			Theatre.BaseOperationDetail
		where
			BaseOperationDetail.PatientBookingSourceUniqueID = BasePatientBooking.SourceUniqueID
		and	BaseOperationDetail.ContextCode = BasePatientBooking.ContextCode
		)
and BasePatientBooking.IntendedProcedureDate between @today and dateadd(year , 1 , @today)



-- session
select
	 PatientBookingSourceUniqueID = -1
	,BaseSession.ContextCode
	,OperationSourceUniqueID = -1
	,SessionSourceUniqueID = BaseSession.SourceUniqueID
	,SessionDate = dateadd(day, datediff(day, 0, BaseSession.StartTime), 0)
	,BaseSessionReference.TheatreID
	,TimeBand.TimeBandCode
	,TimeSliceStateID = 0 --unutilised

	,OperationDetailMergeRecno = -1
	,PatientBookingMergeRecno = -1
	,SessionMergeRecno = BaseSession.MergeRecno
into
	#wkSessionTimeSlice
from
	Theatre.BaseSession

inner join Theatre.BaseSessionReference
on	BaseSessionReference.MergeRecno = BaseSession.MergeRecno

inner join WH.TimeBand
on	TimeBand.TimeBandCode between
	coalesce(
		datediff(
			 minute
			,dateadd(day, datediff(day, 0, BaseSession.StartTime), 0)
			,BaseSession.StartTime
		)
		,-1
	)

and	(
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, BaseSession.EndTime), 0)
				,BaseSession.EndTime
			)
			,-1
		)
		 - 1
	)

where
	dateadd(day, datediff(day, 0, BaseSession.StartTime), 0) between '28 Mar 2011'
and (select max(IntendedProcedureDate) from Theatre.BasePatientBooking where IntendedProcedureDate < dateadd(year , 1 , getdate()))




/*********************
** build fact table **
*********************/



truncate table Theatre.FactSchedule

INSERT INTO Theatre.FactSchedule
(
	 PatientBookingMergeRecno
	,OperationDetailMergeRecno
	,ProcedureDetailMergeRecno
	,SessionMergeRecno
	,ScheduleDateID
	,TimeBandCode
	,TheatreID
	,TimeSliceStateID
	,ConsultantID
	,SurgeonID
	,AnaesthetistID
	,OperationID
	,AgeID
	--,SexID
	--,AdmissionTypeID
	,OperationTypeID
	,SessionPeriodID
	,SessionConsultantID
	,TimetableConsultantID
	,TemplateConsultantID
	,SessionAnaesthetistID
	,TimetableAnaesthetistID
	,TemplateAnaesthetistID
	,SessionSpecialtyID
	,TimetableSpecialtyID
	,TemplateSpecialtyID
	--,Biohazard
	--,ASAScoreID
	,IsSessionBoundary
	,Cases
	,ContextID
)


--populate only where operations, bookings or sessions exist

select
	 TimeSlice.PatientBookingMergeRecno
	,TimeSlice.OperationDetailMergeRecno

	,BaseProcedureDetailMergeRecno =
		coalesce(BaseProcedureDetail.MergeRecno , -1)

	,TimeSlice.SessionMergeRecno

	,ScheduleDateID =
		coalesce(
			 ScheduleDate.DateID

			,case
			when TimeSlice.ScheduleDate is null
			then NullDate.DateID

			when TimeSlice.ScheduleDate < CalendarStartDate.DateValue
			then PreviousDate.DateID

			else FutureDate.DateID
			end
		)

	,TimeSlice.TimeBandCode

	,TimeSlice.TheatreID

	,TimeSlice.TimeSliceStateID

	,BaseOperationDetailReference.ConsultantID
	--,EpisodicConsultantCode = coalesce(TheatreBaseOperationDetail.EpisodicConsultantCode, -1)
	,BaseOperationDetailReference.SurgeonID
	,BaseOperationDetailReference.AnaesthetistID

	,OperationID =
		coalesce(
			 BaseProcedureDetailReference.OperationID
			,-1
		)

	,Age.AgeID
	--,BaseOperationDetailReference.SexID --not used in the cube
	--,BaseOperationDetailReference.AdmissionTypeID --not used in the cube

	,OperationType.OperationTypeID
	,SessionPeriod.SessionPeriodID

	,SessionConsultantID = coalesce(BaseSessionReference.ConsultantID, NullStaff.SourceStaffID)
	,TimetableConsultantID = coalesce(BaseTimetableDetailReference.ConsultantID, NullStaff.SourceStaffID)
	,TemplateConsultantID = coalesce(BaseTimetableTemplateReference.ConsultantID, NullStaff.SourceStaffID)

	,SessionAnaesthetistID = coalesce(BaseSessionReference.AnaesthetistID, NullStaff.SourceStaffID)
	,TimetableAnaesthetistID = coalesce(BaseTimetableDetailReference.AnaesthetistID, NullStaff.SourceStaffID)
	,TemplateAnaesthetistID = coalesce(BaseTimetableTemplateReference.AnaesthetistID, NullStaff.SourceStaffID)

	,SessionSpecialtyID = coalesce(BaseSessionReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)
	,TimetableSpecialtyID = coalesce(BaseTimetableDetailReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)
	,TemplateSpecialtyID = coalesce(BaseTimetableTemplateReference.SpecialtyID, NullSpecialty.SourceSpecialtyID)

	--,Biohazard = convert(bit, BaseOperationDetail.BiohazardFlag) --not used in the cube
	--,ASAScoreID = BaseOperationDetailReference.ASAScoreID --not used in the cube

	,IsSessionBoundary =
		case
		when 
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, BaseSession.StartTime), 0)
					,BaseSession.StartTime
				)
				,-1
			)
			 is not null 
		or
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, BaseSession.EndTime), 0)
					,BaseSession.EndTime
				)
				,-1
			)
			 is not null
		then 1
		else 0
		end
	,Cases = 1

	,Context.ContextID

from
	(
	-- operation slices
	select
		 #wkOperationTimeSlice.PatientBookingMergeRecno
		,#wkOperationTimeSlice.ContextCode
		,#wkOperationTimeSlice.OperationDetailMergeRecno
		,#wkOperationTimeSlice.SessionMergeRecno
		,ScheduleDate = #wkOperationTimeSlice.OperationDate
		,#wkOperationTimeSlice.TheatreID
		,#wkOperationTimeSlice.TimeBandCode
		,#wkOperationTimeSlice.TimeSliceStateID
		,OperationSourceUniqueID
	from
		#wkOperationTimeSlice

	union all

	-- booked slices
	select
		 #wkFutureOperationTimeSlice.PatientBookingMergeRecno
		,#wkFutureOperationTimeSlice.ContextCode
		,#wkFutureOperationTimeSlice.OperationDetailMergeRecno
		,#wkFutureOperationTimeSlice.SessionMergeRecno
		,ScheduleDate = #wkFutureOperationTimeSlice.IntendedProcedureDate
		,#wkFutureOperationTimeSlice.TheatreID
		,#wkFutureOperationTimeSlice.TimeBandCode
		,#wkFutureOperationTimeSlice.TimeSliceStateID
		,OperationSourceUniqueID
	from
		#wkFutureOperationTimeSlice

	union all

	-- unutilised slices
	select
		 PatientBookingMergeRecno
		,ContextCode
		,OperationDetailMergeRecno
		,SessionMergeRecno
		,ScheduleDate = SessionDate
		,TheatreID
		,TimeBandCode
		,TimeSliceStateID
		,OperationSourceUniqueID
	from
		#wkSessionTimeSlice
	where
		not exists
			(
			select	
				1
			from
				#wkOperationTimeSlice
			where
				#wkOperationTimeSlice.TheatreID = #wkSessionTimeSlice.TheatreID
			and	#wkOperationTimeSlice.TimeBandCode = #wkSessionTimeSlice.TimeBandCode
			and	#wkOperationTimeSlice.OperationDate = #wkSessionTimeSlice.SessionDate
			)
	and	not exists
			(
			select	
				1
			from
				#wkFutureOperationTimeSlice
			where
				#wkFutureOperationTimeSlice.TheatreID = #wkSessionTimeSlice.TheatreID
			and	#wkFutureOperationTimeSlice.TimeBandCode = #wkSessionTimeSlice.TimeBandCode
			and	#wkFutureOperationTimeSlice.IntendedProcedureDate = #wkSessionTimeSlice.SessionDate
			)
	) TimeSlice

left join 
	(
	select
		 BaseSession.StartTime
		,BaseSessionReference.TheatreID
	from
		Theatre.BaseSession

	inner join Theatre.BaseSessionReference
	on	BaseSessionReference.MergeRecno = BaseSession.MergeRecno

	) SessionStart
on	dateadd(day, datediff(day, 0, SessionStart.StartTime), 0) = TimeSlice.ScheduleDate
and	coalesce(
		datediff(
			 minute
			,dateadd(day, datediff(day, 0, SessionStart.StartTime), 0)
			,SessionStart.StartTime
		)
		,-1
	)

	 = TimeSlice.TimeBandCode

and	SessionStart.TheatreID = TimeSlice.TheatreID

left join 
	(
	select
		 BaseSession.EndTime
		,BaseSessionReference.TheatreID
	from
		Theatre.BaseSession

	inner join Theatre.BaseSessionReference
	on	BaseSessionReference.MergeRecno = BaseSession.MergeRecno

	) SessionEnd
on	dateadd(day, datediff(day, 0, SessionEnd.EndTime), 0) = TimeSlice.ScheduleDate
and	(
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, SessionEnd.EndTime), 0)
				,SessionEnd.EndTime
			)
			,-1
		)
		 - 1
	)
	 = TimeSlice.TimeBandCode
and	SessionEnd.TheatreID = TimeSlice.TheatreID

left join Theatre.BaseOperationDetail
on	BaseOperationDetail.MergeRecno = TimeSlice.OperationDetailMergeRecno

left join Theatre.BaseOperationDetailReference
on	BaseOperationDetailReference.MergeRecno = BaseOperationDetail.MergeRecno

left join Theatre.BaseSession
on	BaseSession.MergeRecno = TimeSlice.SessionMergeRecno

left join Theatre.BaseSessionReference
on	BaseSessionReference.MergeRecno = BaseSession.MergeRecno

left join Theatre.BasePatientBooking
on	BasePatientBooking.MergeRecno = TimeSlice.PatientBookingMergeRecno

left join Theatre.BaseProcedureDetail
on	BaseProcedureDetail.OperationDetailSourceUniqueID = TimeSlice.OperationSourceUniqueID
and	BaseProcedureDetail.ContextCode = TimeSlice.ContextCode
and	TimeSlice.TimeBandCode between 
	coalesce(
		datediff(
			 minute
			,dateadd(day, datediff(day, 0, BaseProcedureDetail.ProcedureStartTime), 0)
			,BaseProcedureDetail.ProcedureStartTime
		)
		,-1
	)

and
	coalesce(
		datediff(
			 minute
			,dateadd(day, datediff(day, 0, BaseProcedureDetail.ProcedureEndTime), 0)
			,BaseProcedureDetail.ProcedureEndTime
		)
		,-1
	)
	- 1


left join Theatre.BaseProcedureDetailReference
on	BaseProcedureDetailReference.MergeRecno = BaseProcedureDetail.MergeRecno

left join WH.Calendar ScheduleDate
on	ScheduleDate.TheDate = TimeSlice.ScheduleDate

left join WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'

left join WH.Age
on	Age.AgeCode =
	case
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) is null then 'Age Unknown'
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) <= 0 then '00 Days'
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) = 1 then '01 Day'
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) > 1 and
	datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate)), 2) + ' Days'
	
	when datediff(day, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) > 28 and
	datediff(month, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end > 1 and
	 datediff(month, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, BaseOperationDetail.DateOfBirth,BaseOperationDetail.OperationDate) -
	case  when datepart(day, BaseOperationDetail.DateOfBirth) > datepart(day, BaseOperationDetail.OperationDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) - 
	(
	case 
	when	(datepart(m, BaseOperationDetail.DateOfBirth) > datepart(m, BaseOperationDetail.OperationDate)) 
	or
		(
			datepart(m, BaseOperationDetail.DateOfBirth) = datepart(m, BaseOperationDetail.OperationDate) 
		And	datepart(d, BaseOperationDetail.DateOfBirth) > datepart(d, BaseOperationDetail.OperationDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, BaseOperationDetail.DateOfBirth, BaseOperationDetail.OperationDate) - 
	(
	case 
	when	(datepart(m, BaseOperationDetail.DateOfBirth) > datepart(m, BaseOperationDetail.OperationDate)) 
	or
		(
			datepart(m, BaseOperationDetail.DateOfBirth) = datepart(m, BaseOperationDetail.OperationDate) 
		And	datepart(d, BaseOperationDetail.DateOfBirth) > datepart(d, BaseOperationDetail.OperationDate)
		) then 1 else 0 end
	)), 2) + ' Years'
	end

left join Theatre.OperationType
on	OperationType.OperationTypeCode = coalesce(BaseOperationDetail.OperationTypeCode, '-1')

left join Theatre.SessionPeriod
on	SessionPeriod.SessionPeriodID =
		case
		when
			left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, BaseSession.EndTime, 8), 5) > '16:29' then 1 --All Day

		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '00:00' and '12:29' then 2 --Morning
		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '12:30' and '16:29' then 3 --Afternoon
		when left(convert(varchar, BaseSession.StartTime, 8), 5) between '16:30' and '23:59' then 4 --Evening
		else -1
		end

left join Theatre.BaseTimetableDetail
on	BaseTimetableDetail.TimetableDetailCode = BaseSession.TimetableDetailCode
and	BaseTimetableDetail.SessionNumber = BaseSession.SessionNumber
and	BaseTimetableDetail.DayNumber = datepart(dw, BaseSession.StartTime)
and	BaseTimetableDetail.ContextCode = BaseSession.ContextCode

left join Theatre.BaseTimetableDetailReference
on	BaseTimetableDetailReference.MergeRecno = BaseOperationDetail.MergeRecno

left join Theatre.BaseTimetableTemplate
on	BaseTimetableTemplate.TimetableTemplateCode = BaseTimetableDetail.TimetableTemplateCode
and	BaseTimetableTemplate.SessionNumber = BaseSession.SessionNumber
and	BaseTimetableTemplate.DayNumber = datepart(dw, BaseSession.StartTime)
and	BaseTimetableTemplate.ContextCode = BaseSession.ContextCode

left join Theatre.BaseTimetableTemplateReference
on	BaseTimetableTemplateReference.MergeRecno = BaseOperationDetail.MergeRecno

cross join 
	(
	select top 1
		*
	from
		Theatre.Staff NullStaff --Get ValueID for null specialty
	where
		NullStaff.SourceStaffCode = '-1'
	) NullStaff

cross join 
	(
	select top 1
		*
	from
		WH.Specialty NullSpecialty --Get ValueID for null specialty
	where
		NullSpecialty.SourceSpecialtyCode = '-1'
	) NullSpecialty

left join WH.Context
on	Context.ContextCode = TimeSlice.ContextCode

where
	TimeSlice.TimeBandCode != -1
and	TimeSlice.ScheduleDate is not null


--GC Added 16/04/2015
--Miising booking record Applications reported to CSC
and PatientBookingMergeRecno is not null

order by
	 TimeSlice.ScheduleDate
	,TimeSlice.TheatreID
	,TimeSlice.TimeBandCode

drop table #wkOperationTimeSlice
drop table #wkSessionTimeSlice
drop table #wkFutureOperationTimeSlice
 

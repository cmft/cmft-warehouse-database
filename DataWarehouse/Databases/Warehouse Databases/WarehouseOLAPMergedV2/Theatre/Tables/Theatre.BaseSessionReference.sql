﻿CREATE TABLE [Theatre].[BaseSessionReference] (
    [MergeRecno]     INT NOT NULL,
    [ContextID]      INT NOT NULL,
    [AnaesthetistID] INT NULL,
    [ConsultantID]   INT NULL,
    [SpecialtyID]    INT NULL,
    [TheatreID]      INT NULL,
    CONSTRAINT [PK_BaseSessionReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


﻿CREATE TABLE [Theatre].[BaseTimetableDetailReference] (
    [MergeRecno]     INT NOT NULL,
    [ContextID]      INT NOT NULL,
    [AnaesthetistID] INT NULL,
    [ConsultantID]   INT NULL,
    [SpecialtyID]    INT NULL,
    [TheatreID]      INT NULL,
    CONSTRAINT [PK_BaseTimetableDetailReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


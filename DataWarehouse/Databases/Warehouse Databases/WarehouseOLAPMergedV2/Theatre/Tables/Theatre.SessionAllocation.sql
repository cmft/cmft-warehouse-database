﻿CREATE TABLE [Theatre].[SessionAllocation] (
    [SessionAllocationID] INT          NOT NULL,
    [SessionAllocation]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_SessionAllocation] PRIMARY KEY CLUSTERED ([SessionAllocationID] ASC)
);


﻿CREATE TABLE [Theatre].[BaseOperationDetailReference] (
    [MergeRecno]      INT NOT NULL,
    [ContextID]       INT NOT NULL,
    [AnaesthetistID]  INT NULL,
    [ConsultantID]    INT NULL,
    [SpecialtyID]     INT NULL,
    [TheatreID]       INT NULL,
    [SurgeonID]       INT NULL,
    [SexID]           INT NULL,
    [AdmissionTypeID] INT NULL,
    [ASAScoreID]      INT NULL,
    CONSTRAINT [PK_BaseOperationDetailReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


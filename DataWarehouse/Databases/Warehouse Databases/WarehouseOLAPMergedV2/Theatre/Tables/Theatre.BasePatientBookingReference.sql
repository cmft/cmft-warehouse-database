﻿CREATE TABLE [Theatre].[BasePatientBookingReference] (
    [MergeRecno]     INT NOT NULL,
    [ContextID]      INT NOT NULL,
    [AnaesthetistID] INT NULL,
    [ConsultantID]   INT NULL,
    [TheatreID]      INT NULL,
    CONSTRAINT [PK_BasePatientBookingReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


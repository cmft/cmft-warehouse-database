﻿CREATE TABLE [Theatre].[BaseTimetableTemplate] (
    [MergeRecno]            INT           IDENTITY (1, 1) NOT NULL,
    [TimetableTemplateCode] INT           NOT NULL,
    [SessionNumber]         TINYINT       NOT NULL,
    [DayNumber]             TINYINT       NOT NULL,
    [TheatreCode]           INT           NULL,
    [SessionStartTime]      DATETIME      NULL,
    [SessionEndTime]        DATETIME      NULL,
    [ConsultantCode]        VARCHAR (10)  NULL,
    [AnaesthetistCode]      VARCHAR (10)  NULL,
    [SpecialtyCode]         VARCHAR (10)  NULL,
    [LogLastUpdated]        DATETIME      NULL,
    [RecordLogTemplates]    VARCHAR (255) NULL,
    [ContextCode]           VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK_BaseTimetableTemplate] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


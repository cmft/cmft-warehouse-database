﻿CREATE TABLE [Theatre].[OperationType] (
    [OperationTypeID]    INT          NOT NULL,
    [OperationType]      VARCHAR (50) NULL,
    [OperationTypeCode]  VARCHAR (50) NULL,
    [OperationTypeGroup] VARCHAR (50) NULL,
    CONSTRAINT [PK_OperationType] PRIMARY KEY CLUSTERED ([OperationTypeID] ASC)
);


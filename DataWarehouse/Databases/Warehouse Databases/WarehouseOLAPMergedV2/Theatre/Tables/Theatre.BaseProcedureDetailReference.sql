﻿CREATE TABLE [Theatre].[BaseProcedureDetailReference] (
    [MergeRecno]  INT NOT NULL,
    [ContextID]   INT NOT NULL,
    [OperationID] INT NOT NULL,
    CONSTRAINT [PK_BaseProcedureDetailReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


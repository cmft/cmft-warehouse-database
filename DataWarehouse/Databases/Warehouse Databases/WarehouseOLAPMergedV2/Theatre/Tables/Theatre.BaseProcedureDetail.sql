﻿CREATE TABLE [Theatre].[BaseProcedureDetail] (
    [MergeRecno]                       INT            IDENTITY (1, 1) NOT NULL,
    [SequenceNo]                       NUMERIC (9)    NOT NULL,
    [SourceUniqueID]                   NUMERIC (9)    NOT NULL,
    [SupervisingSurgeon1Code]          NUMERIC (9)    NOT NULL,
    [SupervisingSurgeon2Code]          NUMERIC (9)    NOT NULL,
    [SupervisingSurgeon3Code]          NUMERIC (9)    NOT NULL,
    [SupervisingAnaesthetistNurseCode] NUMERIC (9)    NOT NULL,
    [SupervisingScoutNurseCode]        NUMERIC (9)    NOT NULL,
    [SupervisingInstrumentNurseCode]   NUMERIC (9)    NOT NULL,
    [SupervisingTechnician1Code]       NUMERIC (9)    NOT NULL,
    [SupervisingTechnician2Code]       NUMERIC (9)    NOT NULL,
    [ProcedureConfirmedFlag]           NUMERIC (3)    NOT NULL,
    [CMBSItemCode]                     VARCHAR (6)    NULL,
    [Unused2]                          VARCHAR (1)    NULL,
    [SupervisingAnaesthetist2Code]     NUMERIC (9)    NOT NULL,
    [Unused3]                          VARCHAR (1)    NULL,
    [SupervisingAnaesthetist3Code]     NUMERIC (9)    NOT NULL,
    [ProcedureStartTime]               DATETIME       NULL,
    [ProcedureEndTime]                 DATETIME       NULL,
    [ProcedureDescription]             VARCHAR (1024) NULL,
    [WoundType1Code]                   NUMERIC (9)    NOT NULL,
    [WoundType2Code]                   NUMERIC (9)    NOT NULL,
    [WoundType3Code]                   NUMERIC (9)    NOT NULL,
    [ProcedureCode]                    NUMERIC (9)    NOT NULL,
    [OperationDetailSourceUniqueID]    NUMERIC (9)    NOT NULL,
    [SupervisingAnaesthetist1Code]     NUMERIC (9)    NOT NULL,
    [Unused4]                          VARCHAR (1)    NULL,
    [Unused5]                          VARCHAR (1)    NULL,
    [Unused6]                          VARCHAR (1)    NULL,
    [LastUpdated]                      DATETIME       NULL,
    [LogDetail]                        VARCHAR (1)    NULL,
    [Surgeon1Code]                     NUMERIC (9)    NOT NULL,
    [Surgeon2Code]                     NUMERIC (9)    NOT NULL,
    [Surgeon3Code]                     NUMERIC (9)    NOT NULL,
    [Anaesthetist1Code]                NUMERIC (9)    NOT NULL,
    [Anaesthetist2Code]                NUMERIC (9)    NOT NULL,
    [Anaesthetist3Code]                NUMERIC (9)    NOT NULL,
    [ScoutNurseCode]                   VARBINARY (50) NOT NULL,
    [InstrumentNurseCode]              VARBINARY (50) NOT NULL,
    [AnaesthetistNurseCode]            NUMERIC (9)    NOT NULL,
    [Technician1Code]                  NUMERIC (9)    NOT NULL,
    [Technician2Code]                  NUMERIC (9)    NOT NULL,
    [ChangeOverAnaesthetistNurseCode]  VARBINARY (50) NOT NULL,
    [ChangeOverScoutNurseCode]         NUMERIC (9)    NOT NULL,
    [ChangeOverInstrumentNurseCode]    NUMERIC (9)    NOT NULL,
    [InventoryRecordCreatedFlag]       NUMERIC (3)    NOT NULL,
    [PrincipleDiagnosisFlag]           NUMERIC (3)    NOT NULL,
    [WoundType4Code]                   NUMERIC (9)    NOT NULL,
    [PrimaryProcedureFlag]             NUMERIC (3)    NOT NULL,
    [WoundType5Code]                   NUMERIC (9)    NOT NULL,
    [WoundType6Code]                   NUMERIC (9)    NOT NULL,
    [PANCode]                          NUMERIC (9)    NOT NULL,
    [PADCode]                          NUMERIC (9)    NOT NULL,
    [RecordTimesCode]                  NUMERIC (9)    NOT NULL,
    [RecoverySourceUniqueID]           NUMERIC (9)    NOT NULL,
    [ProcedureItemSourceUniqueID]      NUMERIC (9)    NOT NULL,
    [ContextCode]                      VARCHAR (10)   NOT NULL,
    CONSTRAINT [PK_BaseProcedureDetail] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BaseProcedureDetail_SourceUniqueID_PrimaryProcedureFlag_ContextCode]
    ON [Theatre].[BaseProcedureDetail]([SourceUniqueID] ASC, [PrimaryProcedureFlag] ASC, [ContextCode] ASC);


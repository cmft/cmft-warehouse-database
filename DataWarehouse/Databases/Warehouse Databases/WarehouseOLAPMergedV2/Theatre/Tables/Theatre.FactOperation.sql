﻿CREATE TABLE [Theatre].[FactOperation] (
    [MergeRecno]                  INT NOT NULL,
    [TheatreID]                   INT NULL,
    [CancelReasonID]              INT NULL,
    [ConsultantID]                INT NULL,
    [SurgeonID]                   INT NULL,
    [AnaesthetistID]              INT NULL,
    [OperationID]                 INT NULL,
    [AgeID]                       INT NULL,
    [SexID]                       INT NULL,
    [AdmissionTypeID]             INT NULL,
    [OperationDateID]             INT NULL,
    [OperationTypeID]             INT NULL,
    [SessionPeriodID]             INT NULL,
    [SessionConsultantID]         INT NULL,
    [TimetableConsultantID]       INT NULL,
    [TemplateConsultantID]        INT NULL,
    [SessionAnaesthetistID]       INT NULL,
    [TimetableAnaesthetistID]     INT NULL,
    [TemplateAnaesthetistID]      INT NULL,
    [SessionSpecialtyID]          INT NULL,
    [TimetableSpecialtyID]        INT NULL,
    [TemplateSpecialtyID]         INT NULL,
    [Biohazard]                   BIT NULL,
    [ASAScoreID]                  INT NULL,
    [OperationStartTimeOfDay]     INT NULL,
    [OperationEndTimeOfDay]       INT NULL,
    [ReceptionTime]               INT NULL,
    [ReceptionWaitTime]           INT NULL,
    [AnaestheticWaitTime]         INT NULL,
    [AnaestheticTime]             INT NULL,
    [OperationTime]               INT NULL,
    [RecoveryTime]                INT NULL,
    [ReturnTime]                  INT NULL,
    [PatientTime]                 INT NULL,
    [Operation]                   INT NOT NULL,
    [TurnaroundTime]              INT NULL,
    [PatientTimeWithinPlan]       INT NULL,
    [ContextID]                   INT NULL,
    [OperationWithTurnaroundTime] INT NULL,
    [ActiveTheatreTime]           INT NULL,
    [DirectorateID]               INT NULL,
    CONSTRAINT [PK_FactOperation] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


﻿CREATE TABLE [Theatre].[BaseTimetableDetail] (
    [MergeRecno]            INT           IDENTITY (1, 1) NOT NULL,
    [TimetableDetailCode]   INT           NOT NULL,
    [SessionNumber]         TINYINT       NOT NULL,
    [DayNumber]             TINYINT       NOT NULL,
    [StartDate]             SMALLDATETIME NULL,
    [EndDate]               SMALLDATETIME NULL,
    [TheatreCode]           INT           NULL,
    [SessionStartTime]      DATETIME      NULL,
    [SessionEndTime]        DATETIME      NULL,
    [ConsultantCode]        INT           NULL,
    [AnaesthetistCode]      INT           NULL,
    [SpecialtyCode]         INT           NULL,
    [TimetableTemplateCode] INT           NULL,
    [LogLastUpdated]        DATETIME      NULL,
    [RecordLogDetails]      VARCHAR (255) NULL,
    [SessionMinutes]        INT           NULL,
    [ContextCode]           VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK_BaseTimetableDetail] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


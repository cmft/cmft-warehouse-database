﻿CREATE TABLE [Theatre].[SessionPeriod] (
    [SessionPeriodID] INT          NOT NULL,
    [SessionPeriod]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_SessionPeriod] PRIMARY KEY CLUSTERED ([SessionPeriodID] ASC)
);


﻿CREATE TABLE [Theatre].[BaseCancelledOperationReference] (
    [MergeCancelledOperationRecno] INT          NOT NULL,
    [ContextID]                    INT          NULL,
    [WardID]                       INT          NULL,
    [CancelledOperationDateID]     INT          NULL,
    [ConsultantID]                 INT          NULL,
    [SpecialtyID]                  INT          NULL,
    [Created]                      DATETIME     NULL,
    [Updated]                      DATETIME     NULL,
    [ByWhom]                       VARCHAR (50) NULL,
    CONSTRAINT [PK_Theatre_BaseCancelledOperationReference] PRIMARY KEY NONCLUSTERED ([MergeCancelledOperationRecno] ASC)
);


﻿CREATE TABLE [Theatre].[BaseCancellationReference] (
    [MergeRecno]     INT NOT NULL,
    [ContextID]      INT NOT NULL,
    [CancelReasonID] INT NOT NULL,
    CONSTRAINT [PK_BaseCancellationReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


﻿create view [QCR].[CategoryGroup] as

select
	SourceCategoryGroupCode = CategoryGroupCode
	,SourceCategoryGroup = CategoryGroup
	FROM
		[$(Warehouse)].QCR.CategoryGroup

﻿CREATE view [QCR].[Question] as

select
	 SourceContextCode
	,SourceContext
	,SourceQuestionID = SourceValueID
	,SourceQuestionCode = SourceValueCode
	,SourceQuestion = SourceValue
	,LocalQuestionID = LocalValueID
	,LocalQuestionCode = LocalValueCode
	,LocalQuestion = LocalValue
	,NationalQuestionID = NationalValueID
	,NationalQuestionCode = NationalValueCode
	,NationalQuestion = NationalValue
	FROM
		WH.Member
	where
		AttributeCode = 'QUESTION'

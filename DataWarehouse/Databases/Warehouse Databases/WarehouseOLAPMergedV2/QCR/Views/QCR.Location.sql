﻿


CREATE view [QCR].[Location] as

select
	 SourceContextCode
	,SourceContext
	,SourceLocationID = SourceValueID
	,SourceLocationCode = SourceValueCode
	,SourceLocation = SourceValue
	,LocalLocationID = LocalValueID
	,LocalLocationCode = LocalValueCode
	,LocalLocation = LocalValue
	,NationalLocationID = NationalValueID
	,NationalLocationCode = NationalValueCode
	,NationalLocation = NationalValue
	FROM
		WH.Member
	where
		AttributeCode = 'Location'
	and SourceContextCode = 'CEN||QCR'






﻿CREATE view [QCR].[AuditType] as

select
	 SourceContextCode
	,SourceContext
	,SourceAuditTypeID = SourceValueID
	,SourceAuditTypeCode = SourceValueCode
	,SourceAuditType = SourceValue
	,LocalAuditTypeID = LocalValueID
	,LocalAuditTypeCode = LocalValueCode
	,LocalAuditType = LocalValue
	,NationalAuditTypeID = NationalValueID
	,NationalAuditTypeCode = NationalValueCode
	,NationalAuditType = NationalValue
	FROM
		WH.Member
	where
		AttributeCode = 'AUDITTYPE'

﻿create view [QCR].[CategoryType] as

select
	SourceCategoryTypeCode = CategoryTypeCode
	,SourceCategoryType = CategoryType
	FROM
		[$(Warehouse)].QCR.CategoryType

﻿create view [QCR].[Division] as

select
	SourceDivisionCode = DivisionCode
	,SourceDivision = Division
	FROM
		[$(Warehouse)].QCR.Division

﻿CREATE TABLE [QCR].[BaseAuditAnswerReference] (
    [AuditAnswerRecno] INT           NOT NULL,
    [ContextID]        INT           NOT NULL,
    [AuditDateID]      INT           NULL,
    [AuditTypeID]      INT           NULL,
    [LocationID]       INT           NULL,
    [WardID]           INT           NULL,
    [QuestionID]       INT           NULL,
    [Answer]           INT           NULL,
    [Created]          DATETIME      NULL,
    [Updated]          DATETIME      NULL,
    [ByWhom]           VARCHAR (255) NULL
);


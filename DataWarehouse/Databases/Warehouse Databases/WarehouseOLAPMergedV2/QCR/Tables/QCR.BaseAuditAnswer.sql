﻿CREATE TABLE [QCR].[BaseAuditAnswer] (
    [AuditAnswerRecno] INT           NOT NULL,
    [SourceUniqueID]   INT           NOT NULL,
    [ContextCode]      VARCHAR (255) NOT NULL,
    [AuditTime]        SMALLDATETIME NOT NULL,
    [AuditDate]        SMALLDATETIME NOT NULL,
    [LocationCode]     INT           NULL,
    [WardCode]         INT           NULL,
    [DirectorateCode]  VARCHAR (5)   NULL,
    [AuditTypeCode]    INT           NULL,
    [QuestionCode]     INT           NULL,
    [Answer]           INT           NULL,
    [Created]          DATETIME      NULL,
    [Updated]          DATETIME      NULL,
    [ByWhom]           VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseEncounter_4] PRIMARY KEY CLUSTERED ([AuditAnswerRecno] ASC)
);


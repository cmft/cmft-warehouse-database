﻿CREATE TABLE [Neonatal].[BaseBeddayReference] (
    [MergeBeddayRecno] INT           NOT NULL,
    [ContextID]        INT           NOT NULL,
    [Created]          DATETIME      NULL,
    [Updated]          DATETIME      NULL,
    [ByWhom]           VARCHAR (255) NULL,
    CONSTRAINT [PK__BaseBedd__BC65A04958739F6F] PRIMARY KEY CLUSTERED ([MergeBeddayRecno] ASC)
);


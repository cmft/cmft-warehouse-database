﻿CREATE TABLE [Neonatal].[BaseEncounterReference] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK__BaseEnco__78180EC16EACF951] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


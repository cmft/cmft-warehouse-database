﻿CREATE TABLE [Result].[BaseAcuteKidneyInjuryAlertNational] (
    [MergeResultRecno] INT              NOT NULL,
    [C1Result]         INT              NULL,
    [RatioDescription] VARCHAR (15)     NULL,
    [ResultForRatio]   INT              NULL,
    [RVRatio]          DECIMAL (31, 19) NULL,
    [Alert]            VARCHAR (5)      NULL
);


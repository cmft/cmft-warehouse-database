﻿CREATE TABLE [Result].[WrkAcuteKidneyInjuryAlert] (
    [MergeResultRecno]           INT           NOT NULL,
    [SourcePatientNo]            INT           NULL,
    [ContextCode]                VARCHAR (20)  NOT NULL,
    [DistrictNo]                 VARCHAR (10)  NULL,
    [CasenoteNumber]             VARCHAR (24)  NULL,
    [PatientSurname]             VARCHAR (100) NULL,
    [PatientForename]            VARCHAR (100) NULL,
    [LinkageType]                VARCHAR (5)   NULL,
    [LinkingMergeEncounterRecno] INT           NULL,
    [GlobalProviderSpellNo]      VARCHAR (50)  NULL,
    [EffectiveTime]              DATETIME      NULL,
    [ResultTime]                 DATETIME      NULL,
    [Result]                     INT           NULL,
    [UnitOfMeasurement]          VARCHAR (35)  NULL,
    [SexCode]                    INT           NULL,
    [DateOfBirth]                DATE          NULL,
    [AgeOnAdmission]             INT           NULL,
    [AgeAtTest]                  INT           NULL,
    [ResultAPCLinked]            INT           NOT NULL,
    [AdmissionTime]              SMALLDATETIME NULL,
    [PatientCategoryCode]        VARCHAR (2)   NULL,
    [WardEpisodeStart]           VARCHAR (10)  NULL,
    [WardEpisodeEnd]             VARCHAR (10)  NULL,
    [DischargeTime]              SMALLDATETIME NULL,
    [DischargeMethodCode]        VARCHAR (10)  NULL,
    [DateOfDeath]                SMALLDATETIME NULL,
    [PatientSequence]            BIGINT        NULL,
    [SpellSequence]              BIGINT        NULL,
    [RenalPatient]               INT           NOT NULL,
    [LocalLocationCode]          VARCHAR (50)  NULL,
    [LocalLocation]              VARCHAR (200) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_WrkAKI_DistrictNo_EffectiveTime_Result_PatientSequence]
    ON [Result].[WrkAcuteKidneyInjuryAlert]([DistrictNo] ASC, [EffectiveTime] ASC, [Result] ASC, [PatientSequence] ASC);


﻿CREATE TABLE [Result].[BaseAcuteKidneyInjuryAlert] (
    [MergeResultRecno]             INT              NOT NULL,
    [SourcePatientNo]              INT              NULL,
    [ContextCode]                  VARCHAR (20)     NOT NULL,
    [DistrictNo]                   VARCHAR (10)     NULL,
    [CasenoteNumber]               VARCHAR (24)     NULL,
    [PatientSurname]               VARCHAR (100)    NULL,
    [PatientForename]              VARCHAR (100)    NULL,
    [PatientSequence]              BIGINT           NULL,
    [SpellSequence]                BIGINT           NULL,
    [GlobalProviderSpellNo]        VARCHAR (50)     NULL,
    [LinkageType]                  VARCHAR (5)      NULL,
    [LinkingMergeEncounterRecno]   INT              NULL,
    [ResultAPCLinked]              INT              NOT NULL,
    [AdmissionTime]                SMALLDATETIME    NULL,
    [PatientCategoryCode]          VARCHAR (2)      NULL,
    [WardEpisodeStart]             VARCHAR (10)     NULL,
    [WardEpisodeEnd]               VARCHAR (10)     NULL,
    [DischargeTime]                SMALLDATETIME    NULL,
    [DischargeMethodCode]          VARCHAR (10)     NULL,
    [DateOfDeath]                  SMALLDATETIME    NULL,
    [LocalLocationCode]            VARCHAR (50)     NULL,
    [LocalLocation]                VARCHAR (200)    NULL,
    [SexCode]                      INT              NULL,
    [DateOfBirth]                  DATE             NULL,
    [AgeOnAdmission]               INT              NULL,
    [AgeAtTest]                    INT              NULL,
    [ResultTime]                   DATETIME         NULL,
    [EffectiveTime]                DATETIME         NULL,
    [Result]                       INT              NULL,
    [PreviousResultTime]           DATETIME         NULL,
    [PreviousResultEffectiveTime]  DATETIME         NULL,
    [PreviousResult]               INT              NULL,
    [ResultIntervalDays]           NUMERIC (17, 6)  NULL,
    [ResultChange]                 INT              NULL,
    [CurrentToPreviousResultRatio] FLOAT (53)       NULL,
    [ResultRateOfChangePer24Hr]    FLOAT (53)       NULL,
    [BaselineResultRecno]          INT              NULL,
    [BaselineResult]               INT              NULL,
    [BaselineEffectiveTime]        DATETIME         NULL,
    [BaselineResultTime]           DATETIME         NULL,
    [BaselineToCurrentDays]        INT              NULL,
    [BaselineToCurrentChange]      INT              NULL,
    [CurrentToBaselineResultRatio] FLOAT (53)       NULL,
    [C1Result]                     INT              NULL,
    [RatioDescription]             VARCHAR (15)     NULL,
    [ResultForRatio]               INT              NULL,
    [RVRatio]                      DECIMAL (31, 19) NULL,
    [Alert]                        VARCHAR (20)     NULL,
    [RenalPatient]                 BIT              NULL,
    [Stage]                        VARCHAR (20)     NULL,
    [InHospitalAKI]                BIT              NULL,
    [CurrentInpatient]             BIT              NULL,
    [CurrentWard]                  VARCHAR (20)     NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_BaseAKI_GlobalProviderSpellNo_CurrentToBaselineResultRatio]
    ON [Result].[BaseAcuteKidneyInjuryAlert]([GlobalProviderSpellNo] ASC, [CurrentToBaselineResultRatio] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_BaseAKI_GlobalProviderSpellNo_SpellSequence_Ratio]
    ON [Result].[BaseAcuteKidneyInjuryAlert]([GlobalProviderSpellNo] ASC, [SpellSequence] ASC, [CurrentToBaselineResultRatio] ASC);


﻿CREATE TABLE [Result].[BaseOrderReference] (
    [MergeOrderRecno]       INT           NOT NULL,
    [ContextID]             INT           NOT NULL,
    [ConsultantID]          INT           NULL,
    [MainSpecialtyID]       INT           NULL,
    [SpecialtyID]           INT           NULL,
    [ProviderID]            INT           NULL,
    [LocationID]            INT           NULL,
    [OrderPriorityID]       INT           NULL,
    [OrderStatusID]         INT           NULL,
    [OrderRequestDateID]    INT           NULL,
    [OrderReceivedDateID]   INT           NULL,
    [OrderCollectionDateID] INT           NULL,
    [Created]               DATETIME      NULL,
    [Updated]               DATETIME      NULL,
    [ByWhom]                VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseOrderReference] PRIMARY KEY NONCLUSTERED ([MergeOrderRecno] ASC)
);


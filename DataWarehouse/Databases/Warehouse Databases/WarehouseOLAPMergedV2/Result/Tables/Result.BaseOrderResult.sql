﻿CREATE TABLE [Result].[BaseOrderResult] (
    [MergeOrderResultRecno] INT          IDENTITY (1, 1) NOT NULL,
    [OrderResultRecno]      INT          NOT NULL,
    [ContextCode]           VARCHAR (8)  NOT NULL,
    [OrderSourceUniqueID]   INT          NOT NULL,
    [OrderRequestTime]      DATETIME     NULL,
    [ResultSourceUniqueID]  INT          NOT NULL,
    [InterfaceCode]         VARCHAR (10) NOT NULL,
    [Created]               DATETIME     NULL,
    [Updated]               DATETIME     NULL,
    [ByWhom]                VARCHAR (50) NULL,
    [OrderResultChecksum]   INT          NULL,
    CONSTRAINT [PK__BaseOrde__942C7DEA206F3D6E] PRIMARY KEY CLUSTERED ([MergeOrderResultRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BaseOrderResult_OrderRequestTime]
    ON [Result].[BaseOrderResult]([OrderRequestTime] ASC);


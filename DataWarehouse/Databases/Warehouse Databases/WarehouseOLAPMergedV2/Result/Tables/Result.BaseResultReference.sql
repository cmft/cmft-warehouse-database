﻿CREATE TABLE [Result].[BaseResultReference] (
    [MergeResultRecno]    INT           NOT NULL,
    [ResultRecno]         INT           NOT NULL,
    [ContextCode]         VARCHAR (20)  NOT NULL,
    [ContextID]           INT           NOT NULL,
    [ConsultantID]        INT           NULL,
    [MainSpecialtyID]     INT           NULL,
    [DisciplineID]        INT           NULL,
    [SpecialtyID]         INT           NULL,
    [LocationID]          INT           NULL,
    [ResultSetID]         INT           NULL,
    [SampleTypeID]        INT           NULL,
    [EffectiveDateID]     INT           NULL,
    [ResultID]            INT           NULL,
    [UnitOfMeasurementID] INT           NULL,
    [ResultDateID]        INT           NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseResultReference] PRIMARY KEY NONCLUSTERED ([MergeResultRecno] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_BaseResultReference_ResultRecno_ContextCode]
    ON [Result].[BaseResultReference]([ResultRecno] ASC, [ContextCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_BaseResultReference_ResultID_ResultRecno_ContextCode_LocationID]
    ON [Result].[BaseResultReference]([ResultID] ASC)
    INCLUDE([ResultRecno], [ContextCode], [LocationID]);


GO
CREATE NONCLUSTERED INDEX [IX_BaseResultReference_ResultSetID]
    ON [Result].[BaseResultReference]([ResultSetID] ASC);


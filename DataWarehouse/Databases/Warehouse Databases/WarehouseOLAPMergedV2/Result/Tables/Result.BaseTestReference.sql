﻿CREATE TABLE [Result].[BaseTestReference] (
    [MergeTestRecno]     INT           NOT NULL,
    [ContextID]          INT           NOT NULL,
    [OrderRequestDateID] INT           NULL,
    [OrderStatusID]      INT           NULL,
    [ConsultantID]       INT           NULL,
    [MainSpecialtyID]    INT           NULL,
    [SpecialtyID]        INT           NULL,
    [ProviderID]         INT           NULL,
    [LocationID]         INT           NULL,
    [TestID]             INT           NULL,
    [Created]            DATETIME      NULL,
    [Updated]            DATETIME      NULL,
    [ByWhom]             VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseTestReference] PRIMARY KEY NONCLUSTERED ([MergeTestRecno] ASC)
);


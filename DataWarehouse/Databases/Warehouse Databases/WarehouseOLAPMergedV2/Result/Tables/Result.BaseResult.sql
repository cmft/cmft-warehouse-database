﻿CREATE TABLE [Result].[BaseResult] (
    [MergeResultRecno]         INT           IDENTITY (1, 1) NOT NULL,
    [ResultRecno]              INT           NOT NULL,
    [SourceUniqueID]           VARCHAR (50)  NOT NULL,
    [SourcePatientNo]          INT           NULL,
    [DistrictNo]               VARCHAR (10)  NULL,
    [CasenoteNumber]           VARCHAR (24)  NULL,
    [NHSNumber]                VARCHAR (20)  NULL,
    [DateOfBirth]              DATE          NULL,
    [SexCode]                  VARCHAR (5)   NULL,
    [ConsultantCode]           VARCHAR (10)  NULL,
    [MainSpecialtyCode]        VARCHAR (35)  NULL,
    [DisciplineCode]           VARCHAR (10)  NULL,
    [SpecialtyCode]            VARCHAR (35)  NULL,
    [LocationCode]             VARCHAR (10)  NULL,
    [ResultSetSourceUniqueID]  VARCHAR (35)  NULL,
    [ResultSetCode]            VARCHAR (10)  NULL,
    [UniqueResultSetCode]      VARCHAR (50)  NULL,
    [ResultSetStatusCode]      VARCHAR (3)   NULL,
    [ResultSetComment]         VARCHAR (MAX) NULL,
    [SampleReferenceCode]      VARCHAR (50)  NULL,
    [SampleTypeCode]           VARCHAR (8)   NULL,
    [SampleType]               VARCHAR (MAX) NULL,
    [EffectiveTime]            DATETIME      NULL,
    [UniqueResultCode]         VARCHAR (50)  NULL,
    [ResultCode]               VARCHAR (10)  NULL,
    [Result]                   VARCHAR (70)  NULL,
    [UnitOfMeasurement]        VARCHAR (35)  NULL,
    [Abnormal]                 BIT           NULL,
    [LowerReferenceRangeValue] VARCHAR (35)  NULL,
    [UpperReferenceRangeValue] VARCHAR (35)  NULL,
    [ResultComment]            VARCHAR (MAX) NULL,
    [ResultTime]               DATETIME      NULL,
    [InterfaceCode]            VARCHAR (5)   NOT NULL,
    [ContextCode]              VARCHAR (20)  NOT NULL,
    [Created]                  DATETIME      NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (50)  NULL,
    CONSTRAINT [PK_BaseResult] PRIMARY KEY NONCLUSTERED ([MergeResultRecno] ASC) ON [PRIMARY]
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_BaseResult_ResultRecno_ContextCode]
    ON [Result].[BaseResult]([ResultRecno] ASC, [ContextCode] ASC)
    ON [Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_CasenoteNumber]
    ON [Result].[BaseResult]([CasenoteNumber] ASC)
    INCLUDE([MergeResultRecno], [EffectiveTime])
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_NHSNumber]
    ON [Result].[BaseResult]([NHSNumber] ASC)
    INCLUDE([MergeResultRecno], [EffectiveTime])
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_DistrictNo]
    ON [Result].[BaseResult]([DistrictNo] ASC)
    INCLUDE([MergeResultRecno], [EffectiveTime])
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_EffectiveTime_ContextCode]
    ON [Result].[BaseResult]([EffectiveTime] ASC, [ContextCode] ASC)
    INCLUDE([MergeResultRecno], [ResultTime], [SourcePatientNo], [UniqueResultCode])
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_ResultSetSourceUniqueID_ResultTime_ContextCode]
    ON [Result].[BaseResult]([ResultSetSourceUniqueID] ASC, [ResultTime] ASC, [ContextCode] ASC)
    INCLUDE([ResultSetStatusCode])
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_ResultSetStatusCode]
    ON [Result].[BaseResult]([ResultSetStatusCode] ASC, [ContextCode] ASC)
    INCLUDE([MergeResultRecno], [SourceUniqueID], [DistrictNo], [ResultSetSourceUniqueID], [ResultTime])
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_Result]
    ON [Result].[BaseResult]([Result] ASC)
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_ResultSetCode_ContextCode]
    ON [Result].[BaseResult]([ResultSetCode] ASC, [ContextCode] ASC)
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_ResultCode_ContextCode]
    ON [Result].[BaseResult]([ResultCode] ASC, [ContextCode] ASC)
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_UniqueResultSetCode_ContextCode]
    ON [Result].[BaseResult]([UniqueResultSetCode] ASC, [ContextCode] ASC)
    ON [WOMv2_Indexes];


GO
CREATE NONCLUSTERED INDEX [IX_BaseResult_UniqueResultCode_ContextCode]
    ON [Result].[BaseResult]([UniqueResultCode] ASC, [ContextCode] ASC)
    ON [WOMv2_Indexes];


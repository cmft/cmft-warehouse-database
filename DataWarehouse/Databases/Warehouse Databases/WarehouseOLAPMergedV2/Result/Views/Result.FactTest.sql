﻿








CREATE view [Result].[FactTest] as 

select --top 1000
	BaseTestReference.MergeTestRecno
	,ContextID = BaseTestReference.ContextID
	,BaseOrderReference.OrderRequestDateID
	,OrderRequestTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseOrder.OrderRequestTime), 0)
							,BaseOrder.OrderRequestTime
						)
						,-1
					)
	,BaseOrderReference.OrderReceivedDateID
	,OrderReceivedTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseOrder.OrderReceivedTime), 0)
							,BaseOrder.OrderReceivedTime
						)
						,-1
					)
	,BaseOrderReference.OrderCollectionDateID
	,OrderCollectionTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseOrder.OrderCollectionTime), 0)
							,BaseOrder.OrderCollectionTime
						)
						,-1
					)
	,OrderStatusID = BaseOrderReference.OrderStatusID
	,ConsultantID = BaseTestReference.ConsultantID
	,MainSpecialtyID = BaseTestReference.MainSpecialtyID
	,SpecialtyID = BaseTestReference.SpecialtyID
	,ProviderID = BaseTestReference.ProviderID
	,LocationID = BaseTestReference.LocationID
	,BaseTestReference.TestID
	,Tests = 1	
from
	Result.BaseOrderReference

inner join Result.BaseOrder
on	BaseOrderReference.MergeOrderRecno = BaseOrder.MergeOrderRecno

inner join Result.BaseTest
on	BaseTest.OrderSourceUniqueID = BaseOrder.SourceUniqueID
and	BaseTest.ContextCode = BaseOrder.ContextCode

inner join Result.BaseTestReference
on	BaseTestReference.MergeTestRecno = BaseTest.MergeTestRecno









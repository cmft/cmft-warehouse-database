﻿





CREATE view Result.[OrderStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceOrderStatusID = SourceValueID
	,SourceOrderStatusCode = SourceValueCode
	,SourceOrderStatus = SourceValue
	,LocalOrderStatusID = LocalValueID
	,LocalOrderStatusCode = LocalValueCode
	,LocalOrderStatus = LocalValue
	,NationalOrderStatusID = NationalValueID
	,NationalOrderStatusCode = NationalValueCode
	,NationalOrderStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ORDERSTATUS'






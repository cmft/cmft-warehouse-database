﻿





CREATE view Result.[OrderPriority] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceOrderPriorityID = SourceValueID
	,SourceOrderPriorityCode = SourceValueCode
	,SourceOrderPriority = SourceValue
	,LocalOrderPriorityID = LocalValueID
	,LocalOrderPriorityCode = LocalValueCode
	,LocalOrderPriority = LocalValue
	,NationalOrderPriorityID = NationalValueID
	,NationalOrderPriorityCode = NationalValueCode
	,NationalOrderPriority = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ORDERPRIORITY'






﻿


CREATE view Result.[ResultSet] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceResultSetID = SourceValueID
	,SourceResultSetCode = SourceValueCode
	,SourceResultSet = SourceValue
	,LocalResultSetID = LocalValueID
	,LocalResultSetCode = LocalValueCode
	,LocalResultSet = LocalValue
	,NationalResultSetID = NationalValueID
	,NationalResultSetCode = NationalValueCode
	,NationalResultSet = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'RESULTSET'



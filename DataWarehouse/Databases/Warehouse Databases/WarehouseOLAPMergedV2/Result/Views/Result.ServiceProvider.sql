﻿





CREATE view Result.[ServiceProvider] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceServiceProviderID = SourceValueID
	,SourceServiceProviderCode = SourceValueCode
	,SourceServiceProvider = SourceValue
	,LocalServiceProviderID = LocalValueID
	,LocalServiceProviderCode = LocalValueCode
	,LocalServiceProvider = LocalValue
	,NationalServiceProviderID = NationalValueID
	,NationalServiceProviderCode = NationalValueCode
	,NationalServiceProvider = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'SERVICEPROVIDER'






﻿





CREATE view [Result].[Test] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceTestID = SourceValueID
	,SourceTestCode = SourceValueCode
	,SourceTest = SourceValue
	,LocalTestID = LocalValueID
	,LocalTestCode = LocalValueCode
	,LocalTest = LocalValue
	,NationalTestID = NationalValueID
	,NationalTestCode = NationalValueCode
	,NationalTest = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'TEST'








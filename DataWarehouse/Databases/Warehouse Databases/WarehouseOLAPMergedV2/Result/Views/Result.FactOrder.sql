﻿






CREATE view [Result].[FactOrder] as 

select --top 1000
	BaseOrderReference.MergeOrderRecno
	,BaseOrderReference.ContextID
	,BaseOrderReference.ConsultantID
	,BaseOrderReference.MainSpecialtyID
	,BaseOrderReference.SpecialtyID
	,BaseOrderReference.ProviderID
	,BaseOrderReference.LocationID
	,BaseOrderReference.OrderPriorityID 
	,BaseOrderReference.OrderStatusID
	,BaseOrderReference.OrderRequestDateID
	,OrderRequestTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseOrder.OrderRequestTime), 0)
							,BaseOrder.OrderRequestTime
						)
						,-1
					)
	,BaseOrderReference.OrderReceivedDateID
	,OrderReceivedTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseOrder.OrderReceivedTime), 0)
							,BaseOrder.OrderReceivedTime
						)
						,-1
					)
	,BaseOrderReference.OrderCollectionDateID
	,OrderCollectionTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseOrder.OrderCollectionTime), 0)
							,BaseOrder.OrderCollectionTime
						)
						,-1
					)
	,Orders = 1
	,OrderRequestToOrderReceivedHours = datediff(hour, OrderRequestTime, coalesce(OrderReceivedTime, getdate()))
	,OrderReceivedToOrderCollectionHours = datediff(hour, OrderReceivedTime, coalesce(OrderCollectionTime, getdate()))
from
	Result.BaseOrderReference

inner join Result.BaseOrder
on	BaseOrderReference.MergeOrderRecno = BaseOrder.MergeOrderRecno







﻿





CREATE view [Result].[ReportableResult]  as

select
	BaseResult.MergeResultRecno
	,BaseResult.ResultRecno
	,SourceUniqueID
	,SourcePatientNo
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,DateOfBirth
	,SexCode
	,ConsultantCode
	,MainSpecialtyCode
	,DisciplineCode
	,SpecialtyCode
	,LocationCode
	,ResultSetSourceUniqueID
	,ResultSetCode
	,UniqueResultSetCode
	,ResultSetStatusCode
	,ResultSetComment
	,SampleReferenceCode
	,SampleTypeCode
	,SampleType
	,EffectiveTime
	,UniqueResultCode
	,ResultCode
	,Result
	,UnitOfMeasurement
	,Abnormal
	,LowerReferenceRangeValue
	,UpperReferenceRangeValue
	,ResultComment
	,ResultTime
	,InterfaceCode
	,BaseResult.ContextCode
	,ContextID
	,ConsultantID
	,MainSpecialtyID
	,DisciplineID
	,SpecialtyID
	,LocationID
	,ResultSetID
	,SampleTypeID
	,EffectiveDateID
	,ResultID
	,UnitOfMeasurementID
	,ResultDateID
	,BaseResult.Created
	,BaseResult.Updated
	,BaseResult.ByWhom
	--,ReportableResult = case 
	--					when Allocation.Allocation = 'N/A'
	--					then 1
	--					else 0 
	--					end
from
	Result.BaseResult
	
inner join Result.BaseResultReference
on	BaseResult.MergeResultRecno = BaseResultReference.MergeResultRecno

inner join Allocation.DatasetAllocation
on BaseResult.MergeResultRecno = DatasetAllocation.DatasetRecno
and DatasetAllocation.DatasetCode = 'RESULT'

inner join Allocation.Allocation
on	DatasetAllocation.AllocationID = Allocation.AllocationID
and Allocation.Allocation = 'N/A'




--select
--	BaseResult.MergeResultRecno
--	,BaseResult.ResultRecno
--	,SourceUniqueID
--	,SourcePatientNo
--	,DistrictNo
--	,CasenoteNumber
--	,NHSNumber
--	,DateOfBirth
--	,SexCode
--	,ConsultantCode
--	,MainSpecialtyCode
--	,DisciplineCode
--	,SpecialtyCode
--	,LocationCode
--	,ResultSetSourceUniqueID
--	,ResultSetCode
--	,UniqueResultSetCode
--	,ResultSetStatusCode
--	,ResultSetComment
--	,SampleReferenceCode
--	,SampleTypeCode
--	,SampleType
--	,EffectiveTime
--	,UniqueResultCode
--	,ResultCode
--	,Result
--	,UnitOfMeasurement
--	,Abnormal
--	,LowerReferenceRangeValue
--	,UpperReferenceRangeValue
--	,ResultComment
--	,ResultTime
--	,InterfaceCode
--	,BaseResult.ContextCode
--	,ContextID
--	,ConsultantID
--	,MainSpecialtyID
--	,DisciplineID
--	,SpecialtyID
--	,LocationID
--	,ResultSetID
--	,SampleTypeID
--	,EffectiveDateID
--	,ResultID
--	,UnitOfMeasurementID
--	,ResultDateID
--	,BaseResult.Created
--	,BaseResult.Updated
--	,BaseResult.ByWhom
--from
--	Result.BaseResult
--inner join Result.BaseResultReference
--on	BaseResult.MergeResultRecno = BaseResultReference.MergeResultRecno
--where
--	BaseResult.ResultSetStatusCode <> 'UN'
--and	not exists  
--	(
--	select
--		1
--	from
--		Result.BaseResult Later
--	where
--		Later.ResultSetSourceUniqueID = BaseResult.ResultSetSourceUniqueID
--	and Later.ResultTime > BaseResult.ResultTime
--	and	Later.ResultSetStatusCode <> 'UN'
--	and Later.ContextCode in ('CEN||GPICE', 'CEN||ICE')
--	) 









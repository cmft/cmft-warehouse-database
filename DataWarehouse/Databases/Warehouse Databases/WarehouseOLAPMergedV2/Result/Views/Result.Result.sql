﻿




CREATE view [Result].[Result] with schemabinding as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceResultID = SourceValueID
	,SourceResultCode = SourceValueCode
	,SourceResult = SourceValue
	,LocalResultID = LocalValueID
	,LocalResultCode = LocalValueCode
	,LocalResult = LocalValue
	,NationalResultID = NationalValueID
	,NationalResultCode = NationalValueCode
	,NationalResult = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'RESULT'





GO
CREATE UNIQUE CLUSTERED INDEX [IX_Result_SourceResultID]
    ON [Result].[Result]([SourceResultID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Result_LocalResultCode]
    ON [Result].[Result]([LocalResultCode] ASC);


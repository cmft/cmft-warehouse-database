﻿




CREATE view Result.[Discipline] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceDisciplineID = SourceValueID
	,SourceDisciplineCode = SourceValueCode
	,SourceDiscipline = SourceValue
	,LocalDisciplineID = LocalValueID
	,LocalDisciplineCode = LocalValueCode
	,LocalDiscipline = LocalValue
	,NationalDisciplineID = NationalValueID
	,NationalDisciplineCode = NationalValueCode
	,NationalDiscipline = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'Discipline'





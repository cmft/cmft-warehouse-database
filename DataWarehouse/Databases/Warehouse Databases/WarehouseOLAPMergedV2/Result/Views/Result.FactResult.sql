﻿







CREATE view [Result].[FactResult] as

select
	BaseResultReference.MergeResultRecno
	,ContextID
	,ConsultantID
	,MainSpecialtyID
	,DisciplineID
	,SpecialtyID
	,LocationID
	,ResultSetID
	--,SampleTypeID
	,EffectiveDateID
	,EffectiveTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseResult.EffectiveTime), 0)
							,BaseResult.EffectiveTime
						)
						,-1
					)
	,ResultID
	--,UnitOfMeasurementID
	,ResultDateID
	,ResultTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseResult.ResultTime), 0)
							,BaseResult.ResultTime
						)
						,-1
					)
	--,ExclusionID = Allocation.AllocationID
	,Reportable = 
			cast(
			case
			when Allocation.Allocation = 'N/A'
			then 1
			else 0
			end
			as bit)
	,Results = 1
	,EffectiveTimeToResultTimeHours = datediff(hour, EffectiveTime, coalesce(ResultTime, getdate()))
from
	Result.BaseResultReference

inner join Result.BaseResult
on	BaseResultReference.MergeResultRecno = BaseResult.MergeResultRecno

inner join Allocation.DatasetAllocation
on BaseResult.MergeResultRecno = DatasetAllocation.DatasetRecno
and DatasetAllocation.DatasetCode = 'RESULT'

inner join Allocation.Allocation
on	DatasetAllocation.AllocationID = Allocation.AllocationID
and Allocation.AllocationTypeID = 17 -- Result Exclusion
and	Allocation.Allocation = 'N/A'







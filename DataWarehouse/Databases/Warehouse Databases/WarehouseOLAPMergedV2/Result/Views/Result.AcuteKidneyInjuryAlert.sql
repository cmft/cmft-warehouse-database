﻿




CREATE view [Result].[AcuteKidneyInjuryAlert] as


SELECT 
	 AKI.MergeResultRecno
	,AKI.SourcePatientNo
	,AKI.ContextCode
	,AKI.DistrictNo
	,AKI.CasenoteNumber
	,AKI.PatientSurname
	,AKI.PatientForename
	,AKI.PatientSequence
	,AKI.SpellSequence
	,AKI.LinkageType 
	,AKI.LinkingMergeEncounterRecno 
	,AKI.ResultAPCLinked
	,AKI.GlobalProviderSpellNo
	,AKI.AdmissionTime
	,AKI.PatientCategoryCode
	,AKI.WardEpisodeStart
	,AKI.WardEpisodeEnd
	,AKI.DischargeTime
	,AKI.DischargeMethodCode
	,AKI.DateOfDeath
	,AKI.LocalLocationCode
	,AKI.LocalLocation
	,AKI.SexCode
	,AKI.DateOfBirth
	,AKI.AgeOnAdmission
	,AKI.AgeAtTest
	,AKI.ResultTime
	,AKI.EffectiveTime
	,AKI.Result
	,AKI.PreviousResultTime 
	,AKI.PreviousResultEffectiveTime 
	,AKI.PreviousResult 
	,AKI.ResultIntervalDays 
	,AKI.ResultChange 
	,AKI.CurrentToPreviousResultRatio 
	,AKI.ResultRateOfChangePer24Hr 
	,AKI.BaselineResultRecno 
	,AKI.BaselineResult 
	,AKI.BaselineEffectiveTime 
	,AKI.BaselineResultTime 
	,AKI.BaselineToCurrentDays
	,AKI.BaselineToCurrentChange
	,AKI.CurrentToBaselineResultRatio			
	,AKI.RenalPatient						
	,AKI.C1Result
	,AKI.RatioDescription
	,AKI.ResultForRatio
	,AKI.RVRatio
	,NationalAlert = AKI.Alert
	,AKI.Stage
	
	,InHospitalAKI = 
		case
			when exists
					(
					Select 
						1
					from 
						Result.BaseAcuteKidneyInjuryAlert Original

					inner join Result.BaseAcuteKidneyInjuryAlert Subsequent
					on Original.GlobalProviderSpellNo = Subsequent.GlobalProviderSpellNo
					and Subsequent.SpellSequence > Original.SpellSequence

					where 
						Original.GlobalProviderSpellNo = AKI.GlobalProviderSpellNo
					and	Original.LinkageType in ('AEAPC','APC')
					and Original.SpellSequence = 1 
					and Original.CurrentToBaselineResultRatio < 1.5
					and Subsequent.CurrentToBaselineResultRatio >= 1.5
					)
				then 1
				else 0
			end
	,AKI.CurrentInpatient
	,AKI.CurrentWard 
	,AKIFirstAlertSpellSequence = AKIFirstAlert.SpellSequence
	,AKIFirstAlertInSpellTime = 
		Case
				when AKIFirstAlert.SpellSequence = 1 then AKI.AdmissionTime
				else AKIFirstAlert.ResultTime
			end
	,AKIFirstAlertInSpellRatio = AKIFirstAlert.CurrentToBaselineResultRatio
	,AKIFirstAlertInSpellStage = AKIFirstAlert.Stage
	,AKILoS =
		datediff(day,
			Case
				when AKIFirstAlert.SpellSequence = 1 then AKI.AdmissionTime
				else AKIFirstAlert.ResultTime
			end
			,case	
				when AKIFirstAlert.GlobalProviderSpellNo IS null then null
				else coalesce(AKILastAlert.ResultTime,AKI.DischargeTime,getdate())
			end
			)
FROM
	Result.BaseAcuteKidneyInjuryAlert AKI

left join Result.BaseAcuteKidneyInjuryAlert AKIFirstAlert
on AKI.GlobalProviderSpellNo = AKIFirstAlert.GlobalProviderSpellNo
and AKIFirstAlert.Stage in ('Stage3','Stage2','Stage1')
and not exists
	(
	Select
		1
	from 
		Result.BaseAcuteKidneyInjuryAlert Earliest
	where
		Earliest.GlobalProviderSpellNo = AKIFirstAlert.GlobalProviderSpellNo
	and Earliest.Stage in ('Stage3','Stage2','Stage1')
	and Earliest.SpellSequence < AKIFirstAlert.SpellSequence
	)

left join Result.BaseAcuteKidneyInjuryAlert AKILastAlert
on AKIFirstAlert.GlobalProviderSpellNo = AKILastAlert.GlobalProviderSpellNo
and AKILastAlert.SpellSequence > AKIFirstAlert.SpellSequence
and AKILastAlert.Stage = 'Stage0'
and not exists
	(
	Select
		1
	from 
		Result.BaseAcuteKidneyInjuryAlert SubsequentAlert
	where
		SubsequentAlert.GlobalProviderSpellNo = AKILastAlert.GlobalProviderSpellNo
	and SubsequentAlert.Stage in ('Stage3','Stage2','Stage1')
	and SubsequentAlert.SpellSequence > AKILastAlert.SpellSequence
	)
and not exists
	(
	Select
		1
	from 
		Result.BaseAcuteKidneyInjuryAlert Earliest
	where
		Earliest.GlobalProviderSpellNo = AKILastAlert.GlobalProviderSpellNo
	and Earliest.Stage = 'Stage0'
	and Earliest.SpellSequence < AKILastAlert.SpellSequence
	)




	
--89,672 4secs





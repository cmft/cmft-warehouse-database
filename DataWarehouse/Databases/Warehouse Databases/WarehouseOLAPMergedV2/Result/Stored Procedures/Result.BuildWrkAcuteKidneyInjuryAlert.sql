﻿
CREATE Procedure [Result].[BuildWrkAcuteKidneyInjuryAlert] 
	@PeriodStart date = null
	,@PeriodEnd date = null

as

declare @Start date = @PeriodStart
declare @End date = @PeriodEnd


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @inserted int

-- Identify the applicable ResultRecnos
-- Results are duplicated, discussed with Prasanna who agreed we should bring through a result once (Latest result per sample time rather than actual order)

Select
	BaseResult.MergeResultRecno
	,BaseResult.ResultRecno
	,BaseResult.ContextCode 
	,ResultSetSourceUniqueID 
	,LocalResultCode
	,SourcePatientNo 
	,DistrictNo
	,CasenoteNumber
	,EffectiveTime
	,ResultTime
	,Result
	,UnitOfMeasurement
	,Reference.LocationID
	,SexCode
	,DateOfBirth
into 
	#Creatinine
from
	Result.ReportableResult BaseResult

inner join Result.BaseResultReference Reference
on BaseResult.ResultRecno = Reference.ResultRecno
and BaseResult.ContextCode = Reference.ContextCode

inner join Result.Result
on Reference.ResultID = Result.SourceResultID

where
	EffectiveTime between @Start and @End
and Result.LocalResultCode = 'CR'
and	isnumeric(BaseResult.Result) = 1
and DistrictNo is not null
and not exists  
    (
    select
          1
    from
          Result.ReportableResult Latest
    where
          Latest.SourcePatientNo = BaseResult.SourcePatientNo
    and Latest.ContextCode = BaseResult.ContextCode
    and Latest.ResultCode = BaseResult.ResultCode
    and Latest.EffectiveTime = BaseResult.EffectiveTime
    and Latest.ResultTime > BaseResult.ResultTime
    )
and not exists  
    (
    select
          1
    from
          Result.ReportableResult Latest
    where
          Latest.SourcePatientNo = BaseResult.SourcePatientNo
    and Latest.ContextCode = BaseResult.ContextCode
    and Latest.ResultCode = BaseResult.ResultCode
    and Latest.EffectiveTime = BaseResult.EffectiveTime
    and Latest.ResultTime = BaseResult.ResultTime
    and Latest.MergeResultRecno > BaseResult.MergeResultRecno
    )
	
	  
--800398
--796223

CREATE NONCLUSTERED INDEX [IX_CreatinineTemp_ContextCode_MergeResultRecno]
ON [dbo].[#Creatinine] ([ContextCode])
INCLUDE ([MergeResultRecno])

delete from	#Creatinine
where
	exists
	(
	Select
		1
	from
		APC.BaseEncounterBaseResult APCResult
		
	where
		APCResult.MergeResultRecno = #Creatinine.MergeResultRecno
	and ContextCode = 'CEN||GPICE'
	)
or exists	
	(
	Select
		1
	from
		AE.BaseEncounterBaseResult AEResult
		
	where
		AEResult.MergeResultRecno = #Creatinine.MergeResultRecno
	and ContextCode = 'CEN||GPICE'
	)



Truncate table Result.WrkAcuteKidneyInjuryAlert
Insert into	Result.WrkAcuteKidneyInjuryAlert
	(
	MergeResultRecno
	,SourcePatientNo
	,ContextCode
	,DistrictNo
	,CasenoteNumber
	,PatientSurname
	,PatientForename
	,LinkageType 
	,LinkingMergeEncounterRecno 
	,GlobalProviderSpellNo 
	,EffectiveTime
	,ResultTime
	,Result
	,UnitOfMeasurement
	,SexCode
	,DateOfBirth
	,AgeOnAdmission 
	,AgeAtTest
	,ResultAPCLinked
	,AdmissionTime
	,PatientCategoryCode
	,WardEpisodeStart
	,WardEpisodeEnd
	,DischargeTime
	,DischargeMethodCode
	,DateOfDeath
	,PatientSequence
	,SpellSequence						
	,RenalPatient
	,LocalLocationCode
	,LocalLocation 
	)
Select
	MergeResultRecno
	,SourcePatientNo
	,ContextCode
	,DistrictNo
	,CasenoteNumber
	,PatientSurname
	,PatientForename
	,LinkageType 
	,LinkingMergeEncounterRecno 
	,GlobalProviderSpellNo 
	,EffectiveTime
	,ResultTime
	,Result
	,UnitOfMeasurement
	,SexCode
	,DateOfBirth
	,AgeOnAdmission = dbo.f_GetAge(DateOfBirth,cast(coalesce(AdmissionTime,EpisodeStartTime) as date))
	,AgeAtTest = dbo.f_GetAge(DateOfBirth,cast(EffectiveTime as date))
	,ResultAPCLinked
	,AdmissionTime
	,PatientCategoryCode
	,WardEpisodeStart
	,WardEpisodeEnd
	,DischargeTime
	,DischargeMethodCode
	,DateOfDeath
	,PatientSequence = row_number() over (partition by Result.DistrictNo
											order By EffectiveTime
											) 
	,SpellSequence = row_number() over(partition by Result.GlobalProviderSpellNo
										order By EffectiveTime
										)
	,RenalPatient = 
		-- Guidance by Prasanna Hanumapura
					-- If any episode within the last 365 relates to one of the 13 specialties identified then Renal Patient
					-- If the admitting episode of the spell is one of the 23 Specialty Codes identified then Renal Spell
			Case
				when exists 
					(select
						1
					from 
						APC.Encounter Renal
					where
						Renal.DistrictNo = Result.DistrictNo -- check if any nulls
					and datediff(day,Renal.EpisodeEndTime,Result.EpisodeStartTime) between 0 and 365
					and Renal.SpecialtyCode in ('NEIT' , 'PNEP' , 'RT' , 'NPCH' , 'NED' , 'RTC' , 'RTL' , 'RTR','IH','IH2','IH4','IHD','IHP')
					)
				then 1
				when exists 
					(select
						1
					from 
						APC.Encounter Renal
					where
						Renal.GlobalProviderSpellNo = Result.GlobalProviderSpellNo
					and Renal.FirstEpisodeInSpellIndicator = 1
					and
						(	Renal.SpecialtyCode in ('NEIT' , 'PNEP' , 'NEPH' , 'RT' , 'NPCH' , 'NED' , 'NEPD' , 'RENS' , 'RTC' , 'RTL' , 'RTR')
						or 
							left(Renal.SpecialtyCode , 2) = 'IH'
						)
					)
				then 1
				else 0
			end
	,LocalLocationCode
	,LocalLocation
	
from
	(
	Select
		BaseResult.MergeResultRecno
		,BaseResult.SourcePatientNo
		,BaseResult.ContextCode
		,BaseResult.DistrictNo
		,CasenoteNumber = coalesce
							(
							APCEncounter.CasenoteNumber
							,AEEncounter.CasenoteNo
							,BaseResult.CasenoteNumber
							)
		,PatientSurname = coalesce(
							APCEncounter.PatientSurname
							,AEEncounter.PatientSurname
							)
		,PatientForename = coalesce(
							APCEncounter.PatientForename
							,AEEncounter.PatientForename
							)
		,BaseResult.EffectiveTime
		,BaseResult.ResultTime
		,Result = cast(BaseResult.Result as int)
		,BaseResult.UnitOfMeasurement
		,SexCode = 
			Case
				when BaseResult.SexCode in ('01','M','1') then 1
				when BaseResult.SexCode in ('02','F','2') then 2
				else null
			end
		,BaseResult.DateOfBirth
		,ResultAPCLinked =
			Case
				when AEAPCResult.AEMergeEncounterRecno is not null then 1
				when APCResult.MergeEncounterRecno is not null then 1
				else 0
			end
		,LinkageType =
			Case
				when APCResult.MergeEncounterRecno is not null then 'APC'
				when AEAPCResult.AEMergeEncounterRecno is not null then 'AEAPC'
				when AEResult.MergeEncounterRecno is not null then 'AE'
				else null
			end
		,LinkingMergeEncounterRecno =
			coalesce
				(
				AEAPCResult.AEMergeEncounterRecno
				,APCResult.MergeEncounterRecno
				,AEResult.MergeEncounterRecno
				,0
				)
		,GlobalProviderSpellNo = coalesce
									(
									AEAPCEncounter.GlobalProviderSpellNo
									,APCEncounter.GlobalProviderSpellNo
									,('AE' + ' ' + cast(AEEncounter.MergeEncounterRecno as varchar))									
									,('Result'+ ' ' + cast(BaseResult.MergeResultRecno as varchar))
									)
		,AdmissionTime = coalesce(
							APCEncounter.AdmissionTime 
							,AEAPCEncounter.AdmissionTime
							)
		,EpisodeStartTime = coalesce
							(
							APCEncounter.EpisodeStartTime
							,AEEncounter.ArrivalDate
							)
		,PatientCategoryCode = coalesce
							(
							APCEncounter.PatientCategoryCode
							,AEAPCEncounter.PatientCategoryCode
							)	
		,WardEpisodeStart = coalesce
							(
							APCEncounter.StartWardTypeCode
							,AEAPCEncounter.StartWardTypeCode
							)  
		,WardEpisodeEnd = coalesce
							(
							APCEncounter.EndWardTypeCode
							,AEAPCEncounter.EndWardTypeCode
							)  
		,DischargeTime = coalesce
							(
							APCEncounter.DischargeTime
							,AEAPCEncounter.DischargeTime
							)
		,DischargeMethodCode = coalesce
							(
							APCEncounter.DischargeMethodCode
							,AEAPCEncounter.DischargeMethodCode
							)
		,DateOfDeath = coalesce
							(
							APCEncounter.DateOfDeath
							,AEAPCEncounter.DateOfDeath
							)
		,LocalLocationCode
		,LocalLocation
	from
		#Creatinine BaseResult
	
	left join APC.BaseEncounterBaseResult APCResult
	on BaseResult.MergeResultRecno = APCResult.MergeResultRecno

	left join AE.BaseEncounterBaseResult AEResult
	on BaseResult.MergeResultRecno = AEResult.MergeResultRecno

	left join APC.Encounter APCEncounter
	on APCResult.MergeEncounterRecno = APCEncounter.MergeEncounterRecno

	left join AE.BaseEncounter AEEncounter
	on AEResult.MergeEncounterRecno = AEEncounter.MergeEncounterRecno
	and AEEncounter.Reportable = 1

	left join APC.BaseEncounterAEBaseEncounter AEAPCResult
	on AEResult.MergeEncounterRecno = AEAPCResult.AEMergeEncounterRecno

	left join APC.Encounter AEAPCEncounter
	on AEAPCResult.APCMergeEncounterRecno = AEAPCEncounter.MergeEncounterRecno
	
	left join WH.Location
	on BaseResult.LocationID = Location.SourceLocationID
	
	) 
	Result

select
	@inserted = @@ROWCOUNT
	
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


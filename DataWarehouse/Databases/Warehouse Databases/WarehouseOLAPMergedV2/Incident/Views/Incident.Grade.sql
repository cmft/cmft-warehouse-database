﻿


CREATE view [Incident].[Grade] as

select
	 SourceContextCode
	,SourceContext
	,SourceGradeID = SourceValueID
	,SourceGradeCode = SourceValueCode
	,SourceGrade = SourceValue
	,LocalGradeID = LocalValueID
	,LocalGradeCode = LocalValueCode
	,LocalGrade = LocalValue
	,NationalGradeD = NationalValueID
	,NationalGradeCode = NationalValueCode
	,NationalGrade = NationalValue
from
	WH.Member
where
	AttributeCode = 'INCGRADE'





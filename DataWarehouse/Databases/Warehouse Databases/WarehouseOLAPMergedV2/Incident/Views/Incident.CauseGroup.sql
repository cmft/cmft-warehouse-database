﻿

CREATE view [Incident].[CauseGroup] as

select
	 SourceContextCode
	,SourceContext
	,SourceCauseGroupID = SourceValueID
	,SourceCauseGroupCode = SourceValueCode
	,SourceCauseGroup = SourceValue
	,LocalCauseGroupID = LocalValueID
	,LocalCauseGroupCode = LocalValueCode
	,LocalCauseGroup = LocalValue
	,NationalCauseGroupID = NationalValueID
	,NationalCauseGroupCode = NationalValueCode
	,NationalCauseGroup = NationalValue
from
	WH.Member
where
	AttributeCode = 'INCCAUSEGRP'




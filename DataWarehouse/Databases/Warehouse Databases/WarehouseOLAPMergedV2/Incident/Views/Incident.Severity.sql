﻿
CREATE view [Incident].[Severity] as

select
	 SourceContextCode
	,SourceContext
	,SourceSeverityID = SourceValueID
	,SourceSeverityCode = SourceValueCode
	,SourceSeverity = SourceValue
	,LocalSeverityID = LocalValueID
	,LocalSeverityCode = LocalValueCode
	,LocalSeverity = LocalValue
	,NationalSeverityID = NationalValueID
	,NationalSeverityCode = NationalValueCode
	,NationalSeverity = NationalValue
from
	WH.Member
where
	AttributeCode = 'SEVERITY'



﻿
CREATE view [Incident].[IncidentType] as

select
	 SourceContextCode
	,SourceContext
	,SourceIncidentTypeID = SourceValueID
	,SourceIncidentTypeCode = SourceValueCode
	,SourceIncidentType = SourceValue
	,LocalIncidentTypeID = LocalValueID
	,LocalIncidentTypeCode = LocalValueCode
	,LocalIncidentType = LocalValue
	,NationalIncidentTypeID = NationalValueID
	,NationalIncidentTypeCode = NationalValueCode
	,NationalIncidentType = NationalValue
from
	WH.Member
where
	AttributeCode = 'INCIDENTTYPE'



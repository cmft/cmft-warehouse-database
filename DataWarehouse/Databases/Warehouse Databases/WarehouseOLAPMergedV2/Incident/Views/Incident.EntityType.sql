﻿

CREATE view [Incident].[EntityType] as

select
	 SourceContextCode
	,SourceContext
	,SourceIncidentEntityTypeID = SourceValueID
	,SourceIncidentEntityTypeCode = SourceValueCode
	,SourceIncidentEntityType = SourceValue
	,LocalIncidentEntityTypeID = LocalValueID
	,LocalIncidentEntityTypeCode = LocalValueCode
	,LocalIncidentEntityType = LocalValue
	,NationalIncidentEntityTypeID = NationalValueID
	,NationalIncidentEntityTypeCode = NationalValueCode
	,NationalIncidentEntityType = NationalValue
	FROM
		WH.Member
	where
		AttributeCode = 'INCENTITYTYPE'




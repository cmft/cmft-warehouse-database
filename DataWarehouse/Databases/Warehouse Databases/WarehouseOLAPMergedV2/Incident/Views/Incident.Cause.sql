﻿

CREATE view [Incident].[Cause] as

select
	 SourceContextCode
	,SourceContext
	,SourceCauseID = SourceValueID
	,SourceCauseCode = SourceValueCode
	,SourceCause = SourceValue
	,LocalCauseID = LocalValueID
	,LocalCauseCode = LocalValueCode
	,LocalCause = LocalValue
	,NationalCauseID = NationalValueID
	,NationalCauseCode = NationalValueCode
	,NationalCause = NationalValue
	FROM
		WH.Member
	where
		AttributeCode = 'INCCAUSE'




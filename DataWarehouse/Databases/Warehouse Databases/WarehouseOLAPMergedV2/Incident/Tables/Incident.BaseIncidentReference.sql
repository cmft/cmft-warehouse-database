﻿CREATE TABLE [Incident].[BaseIncidentReference] (
    [MergeIncidentRecno] INT           NOT NULL,
    [ContextID]          INT           NOT NULL,
    [IncidentDateID]     INT           NULL,
    [Cause1ID]           INT           NULL,
    [Cause2ID]           INT           NULL,
    [IncidentTypeID]     INT           NULL,
    [IncidentGradeID]    INT           NULL,
    [SeverityID]         INT           NULL,
    [CauseGroupID]       INT           NULL,
    [SiteID]             INT           NULL,
    [WardID]             INT           NULL,
    [Created]            DATETIME      NULL,
    [Updated]            DATETIME      NULL,
    [ByWhom]             VARCHAR (255) NULL,
    CONSTRAINT [PK__BaseIncidentReference_MergeEncounterRecno] PRIMARY KEY CLUSTERED ([MergeIncidentRecno] ASC)
);


﻿CREATE procedure [DQ].[BuildDQFactErrorSnapshot]
	@DatasetCode varchar(10)
as


declare @DatasetID smallint =
	(
	select
		DatasetID
	from
		DQ.Dataset
	where
		Dataset.DatasetCode = @DatasetCode
	)

delete
from
	DQ.FactErrorSnapshot
where
	FactErrorSnapshot.DateID =
	(
	select
		Calendar.DateID
	from
		WH.Calendar
	where
		Calendar.TheDate = CAST(getdate() as date)
	)
and	FactErrorSnapshot.DatasetID = @DatasetID

	
insert
into
	DQ.FactErrorSnapshot
(
	 DateID
	,ContextID
	,DatasetID
	,EncounterDateID
	,ServiceID
	,DirectorateID
	,SiteID
	,ProfessionalCarerID
	,ErrorID
	,Cases
)
select
	 Calendar.DateID
	,FactError.ContextID
	,FactError.DatasetID
	,FactError.EncounterDateID
	,FactError.ServiceID
	,FactError.DirectorateID
	,FactError.SiteID
	,FactError.ProfessionalCarerID
	,FactError.ErrorID
	,Cases = SUM(FactError.Cases)
from
	DQ.FactError

inner join WH.Calendar
on	Calendar.TheDate = CAST(getdate() as date)

where
	FactError.DatasetID = @DatasetID

group by
	 Calendar.DateID
	,FactError.ContextID
	,FactError.DatasetID
	,FactError.EncounterDateID
	,FactError.ServiceID
	,FactError.DirectorateID
	,FactError.SiteID
	,FactError.ProfessionalCarerID
	,FactError.ErrorID

﻿
CREATE procedure [DQ].[BuildWrkDataset] 
	@DatasetCode varchar(10)
as


--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20140717 RR added GPCode for Community data
--------------------------------------------------------------------------

declare @DatasetID smallint =
	(
	select
		DatasetID
	from
		DQ.Dataset
	where
		Dataset.DatasetCode = @DatasetCode
	)

delete
from
	DQ.WrkDataset
where
	DatasetID = @DatasetID



if @DatasetCode = 'AE'

	insert into DQ.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,DatasetStartDate
		,DatasetEndDate
		,ContextID
		,ContextCode
		,NationalAttendanceDisposalCode
		,NationalAttendanceCategoryCode
		,Reportable
		,SourceEncounterRecno
		,NHSNumber
		,SourceAttendanceDisposalCode
		,Postcode
		,NationalArrivalModeCode
		,AmbulanceArrivalTime
		,NationalEthnicCategoryCode
		,SiteCode
		,PatientGroupCode
		,AttendanceConclusionTime
		,DepartureTime
		,InitialAssessmentTime
		,SeenForTreatmentTime
		,RegisteredTime
		,DepartmentTypeCode
	) 
	select
		 DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,DatasetStartDate = Encounter.ArrivalDate
		,DatasetEndDate = Encounter.DepartureTime
		,ContextID = Reference.ContextID
		,ContextCode = Encounter.ContextCode
		,NationalAttendanceDisposalCode = AttendanceDisposal.NationalAttendanceDisposalCode
		,NationalAttendanceCategoryCode = AttendanceCategory.NationalAttendanceCategoryCode
		,Reportable = Encounter.Reportable
		,Encounter.EncounterRecno
		,Encounter.NHSNumber
		,AttendanceDisposal.SourceAttendanceDisposalCode
		,Encounter.Postcode
		,ArrivalMode.NationalArrivalModeCode
		,Encounter.AmbulanceArrivalTime
		,EthnicCategory.NationalEthnicCategoryCode
		,Site.NationalSiteCode
		,PatientGroup.SourcePatientGroupCode
		,Encounter.AttendanceConclusionTime
		,Encounter.DepartureTime
		,Encounter.InitialAssessmentTime

		,SeenForTreatmentTime =
			case
			when Site.NationalSiteCode = 'RW3SM'
			then Encounter.ClinicalAssessmentTime
			else Encounter.SeenForTreatmentTime
			end

		,Encounter.RegisteredTime
		,Encounter.DepartmentTypeCode
	from
		AE.BaseEncounter Encounter

	inner join AE.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join AE.AttendanceDisposal
	on	AttendanceDisposal.SourceAttendanceDisposalID = Reference.AttendanceDisposalID

	inner join AE.AttendanceCategory
	on	AttendanceCategory.SourceAttendanceCategoryID = Reference.AttendanceCategoryID

	inner join AE.ArrivalMode
	on	ArrivalMode.SourceArrivalModeID = Reference.ArrivalModeID

	inner join WH.EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryID = Reference.EthnicOriginID

	inner join WH.Site
	on	Site.SourceSiteID = Reference.SiteID

	inner join AE.PatientGroup
	on	PatientGroup.SourcePatientGroupID = Reference.PatientGroupID

	where
		Encounter.Reportable = 1
	and	AttendanceCategory.NationalAttendanceCategoryCode = '1' --First Accident And Emergency Attendance

	and	Encounter.ArrivalDate > DATEADD(year, -2, getdate())


if @DatasetCode = 'APC'

	insert into DQ.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,DatasetStartDate
		,DatasetEndDate
		,ContextID
		,ContextCode
		,SourceEncounterRecno
		,NHSNumber
		,Postcode
		,NationalEthnicCategoryCode
		,GPPracticeCode
		,GPCode
		,PurchaserCode
		,PCTCode
		,PatientTitle
		,PatientForename
		,PatientSurname
		,SexCode
		,DateOfBirth
		,Age
		,AdmissionSourceCode
		,AdmissionMethodCode
		,DischargeDestinationCode
		,DischargeMethodCode
		,PrimaryDiagnosisCode
		,DischargeDate
		,PrimaryProcedureCode
		,OperationStatusCode
		,ManagementIntentionCode
		,DateOnWaitingList
		,AdmissionDate
		,ReferrerCode
		,ReferredByCode
		,RTTPathwayID
		,SpecialtyCode
		,MainSpecialtyCode
		,SiteCode
		,CommissionerCode
		,NHSNumberStatusCode
		,Diagnosis
		,HRGCode
	) 
	select
		 DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,DatasetStartDate = Encounter.EpisodeStartTime
		,DatasetEndDate = Encounter.EpisodeEndTime
		,ContextID = Reference.ContextID
		,ContextCode = Encounter.ContextCode
		,Encounter.EncounterRecno
		,Encounter.NHSNumber
		,Encounter.Postcode
		,EthnicCategory.NationalEthnicCategoryCode
		,Encounter.RegisteredGpPracticeCode
		,Encounter.RegisteredGpCode
		,Encounter.PurchaserCode
		,Encounter.PCTCode
		,Encounter.PatientTitle
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Sex.NationalSexCode
		,DateOfBirth
		,Age = DATEDIFF(year, Encounter.DateOfBirth, Encounter.AdmissionTime)
		,AdmissionSource.NationalAdmissionSourceCode
		,AdmissionMethod.NationalAdmissionMethodCode
		,DischargeDestinationCode = DischargeDestination.NationalDischargeDestinationCode
		,DischargeMethodCode = DischargeMethod.NationalDischargeMethodCode
		,Encounter.PrimaryDiagnosisCode
		,Encounter.DischargeTime
		,Encounter.PrimaryProcedureCode
		,OperationStatusCode = OperationStatus.NationalOperationStatusCode
		,ManagementIntentionCode = IntendedManagement.NationalIntendedManagementCode
		,Encounter.DateOnWaitingList
		,Encounter.AdmissionTime
		,Reference.ReferrerID
		,Encounter.ReferredByCode
		,Encounter.RTTPathwayID
		,Specialty.NationalSpecialtyCode
		,Consultant.MainSpecialtyCode
		,Site.SourceSiteCode
		,Encounter.PurchaserCode
		,NHSNumberStatus.NationalNHSNumberStatusCode
		,Diagnosis.Diagnosis
		,Encounter.PASHRGCode
	from
		APC.BaseEncounter Encounter

	inner join APC.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno
	and Encounter.Reportable = 1

	left join WH.EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryID = Reference.EthnicOriginID

	left join WH.Sex
	on	Sex.SourceSexID = Reference.SexID

	left join WH.Specialty
	on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

	left join WH.Consultant
	on	Consultant.SourceConsultantID = Reference.ConsultantID

	left join WH.Site
	on	Site.SourceSiteID = Reference.StartSiteID

	left join WH.NHSNumberStatus
	on	NHSNumberStatus.SourceNHSNumberStatusID = Reference.NHSNumberStatusID

	left join APC.AdmissionSource
	on	AdmissionSource.SourceAdmissionSourceID = Reference.AdmissionSourceID

	left join APC.AdmissionMethod
	on	AdmissionMethod.SourceAdmissionMethodID = Reference.AdmissionMethodID

	left join APC.DischargeDestination
	on	DischargeDestination.SourceDischargeDestinationID = Reference.DischargeDestinationID

	left join APC.DischargeMethod
	on	DischargeMethod.SourceDischargeMethodID = Reference.DischargeMethodID

	left join APC.OperationStatus
	on	OperationStatus.SourceOperationStatusID = Reference.OperationStatusID

	left join APC.IntendedManagement
	on	IntendedManagement.SourceIntendedManagementID = Reference.IntendedManagementID

	left join [$(Warehouse)].PAS.Diagnosis
	on	Diagnosis.DiagnosisCode = Encounter.PrimaryDiagnosisCode

	where
		Encounter.EpisodeStartTime > DATEADD(year, -2, getdate())


if @DatasetCode = 'OP'

	insert into DQ.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,DatasetStartDate
		,DatasetEndDate
		,ContextID
		,ContextCode
		,SourceEncounterRecno
		,NHSNumber
		,Postcode
		,NationalEthnicCategoryCode
		,GPPracticeCode
		,GPCode
		,PurchaserCode
		,PCTCode
		,PatientTitle
		,PatientForename
		,PatientSurname
		,SexCode
		,DateOfBirth
		,Age
		,PrimaryDiagnosisCode
		,PrimaryProcedureCode
		,ReferredByCode
		,RTTPathwayID
		,SpecialtyCode
		,MainSpecialtyCode
		,SiteCode
		,CommissionerCode
		,NHSNumberStatusCode
		,Diagnosis
		,HRGCode
		--,AppointmentStatusCode
		,CancelledReasonCode
		--,ClinicCode
		--,AppointmentDate
	) 
	select
		 DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,DatasetStartDate = Encounter.AppointmentTime
		,DatasetEndDate = Encounter.AppointmentTime
		,ContextID = Reference.ContextID
		,ContextCode = Encounter.ContextCode
		,Encounter.EncounterRecno
		,Encounter.NHSNumber
		,Encounter.Postcode
		,EthnicCategory.NationalEthnicCategoryCode
		,Encounter.RegisteredGpPracticeCode
		,Encounter.RegisteredGpCode
		,Encounter.PurchaserCode
		,Encounter.PCTCode
		,Encounter.PatientTitle
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Sex.NationalSexCode
		,DateOfBirth
		,Age = DATEDIFF(year, Encounter.DateOfBirth, Encounter.AppointmentTime)
		,Encounter.PrimaryDiagnosisCode
		,Encounter.PrimaryOperationCode
		,Encounter.ReferredByCode
		,Encounter.RTTPathwayID
		,Specialty.NationalSpecialtyCode
		,Consultant.MainSpecialtyCode
		,Site.SourceSiteCode
		,Encounter.PurchaserCode
		,NHSNumberStatus.NationalNHSNumberStatusCode
		,Diagnosis.Diagnosis
		,Encounter.HRGCode
		--,Encounter.AppointmentStatusCode
		,Encounter.CancelledByCode
		--,Encounter.ClinicCode
		--,AppointmentDate
	from
		OP.BaseEncounter Encounter

	inner join OP.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join WH.EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryID = Reference.EthnicOriginID

	left join WH.Sex
	on	Sex.SourceSexID = Reference.SexID

	left join WH.Specialty
	on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

	left join WH.Consultant
	on	Consultant.SourceConsultantID = Reference.ConsultantID

	left join WH.Site
	on	Site.SourceSiteID = Reference.SiteID

	left join WH.NHSNumberStatus
	on	NHSNumberStatus.SourceNHSNumberStatusID = Reference.NHSNumberStatusID

	left join [$(Warehouse)].PAS.Diagnosis
	on	Diagnosis.DiagnosisCode = Encounter.PrimaryDiagnosisCode

	where
		Encounter.AppointmentDate between DATEADD(year, -2, getdate()) and GETDATE()


if @DatasetCode = 'COM'

	insert into DQ.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,DatasetStartDate
		,DatasetEndDate
		,ContextID
		,ContextCode
		,SourceEncounterRecno
		,NHSNumber
		,Postcode
		,NationalEthnicCategoryCode
		,ScheduleTypeID
		,ContactTypeCode
		,GPPracticeCode
		,ReferralSpecialtyCode
		,SpecialtyCode
		,RTTStatusCode
		,OutcomeCode
		,ArrivedTime
		,SeenTime
		,DepartedTime
		,CancelledReasonCode
		,StaffTeamCode
		,SeenByClinicianCode
		,DNAReasonID
		,ProfessionalCarerID
		,GPCode
		,ServiceAllocation 
		,ReferToServiceAllocation
	)
	select
		 DatasetRecno = Reference.MergeEncounterRecno
		,DatasetID = @DatasetID
		,DatasetStartDate = Encounter.StartDate
		,DatasetEndDate = Encounter.EndTime
		,ContextID = Reference.ContextID
		,ContextCode = Encounter.ContextCode
		,Encounter.MergeEncounterRecno
		,Encounter.PatientNHSNumber
		,Encounter.PatientPostcode
		,EthnicCategory.NationalEthnicCategoryCode
		,ScheduleTypeID = Encounter.ScheduleTypeID
		,ContactTypeCode = Encounter.ContactTypeID
		,GPPracticeCode = Encounter.PatientEncounterRegisteredPracticeCode
		,ReferralSpecialtyCode = Reference.ReferredToSpecialtyID
		,SpecialtyCode = Reference.SpecialtyID
		,RTTStatusCode = Encounter.RTTStatusID
		,OutcomeCode = Encounter.EncounterOutcomeCode
		,ArrivedTime = Encounter.ArrivedTime
		,SeenTime = Encounter.SeenTime
		,DepartedTime = Encounter.DepartedTime
		,CancelledReasonCode = Encounter.CanceledReasonID
		,StaffTeamCode = Encounter.StaffTeamID
		,SeenByClinicianCode = Encounter.SeenByProfessionalCarerID
		,Encounter.DNAReasonID
		,Encounter.ProfessionalCarerID
		,GPCode = PatientEncounterRegisteredGPCode
		,ServiceAllocation = ServiceAllocation.Allocation
		,ReferToServiceAllocation = ReferToServiceAllocation.Allocation

	from
		COM.BaseEncounter Encounter
	
	inner join COM.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join 
		(
		select
			 SourceEthnicCategoryID = LookupID
			,NationalEthnicCategoryCode = LookupNHSCode
		from 
			[$(Warehouse)].COM.LookupBase
		where
			LookupTypeID like 'ETHGR%'
		) EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryID = Encounter.PatientEthnicGroupID

	inner join Allocation.DatasetAllocation ServiceActivity
	on Encounter.MergeEncounterRecno = ServiceActivity.DatasetRecno
	and ServiceActivity.DatasetCode = 'COM'
	and ServiceActivity.AllocationTypeID = 2
	
	inner join Allocation.Allocation ServiceAllocation
	on ServiceActivity.AllocationID = ServiceAllocation.AllocationID
	and ServiceActivity.AllocationTypeID = ServiceAllocation.AllocationTypeID
	
	inner join Allocation.DatasetAllocation ReferToServiceActivity
	on Encounter.MergeEncounterRecno = ReferToServiceActivity.DatasetRecno
	and ReferToServiceActivity.DatasetCode = 'COM'
	and ReferToServiceActivity.AllocationTypeID = 16
	
	inner join Allocation.Allocation ReferToServiceAllocation
	on ReferToServiceActivity.AllocationID = ReferToServiceAllocation.AllocationID
	and ReferToServiceActivity.AllocationTypeID = ReferToServiceAllocation.AllocationTypeID
	 
	where
		Encounter.StartDate > DATEADD(year, -2, getdate())
	and Encounter.RecordStatus = 'C'
	and Encounter.JointActivityFlag = 0
	and Encounter.Archived = 'N'
	

if @DatasetCode = 'COMR'

	insert into DQ.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,DatasetStartDate
		,DatasetEndDate
		,ContextID
		,ContextCode
		,SourceEncounterRecno
		,NHSNumber
		,Postcode
		,NationalEthnicCategoryCode
		,SpecialtyCode
		,ReferralStatusCode
		,ReferralReasonCode
		,PatientID
		,StaffTeamCode
	) 
	select
		 DatasetRecno = Reference.MergeEncounterRecno
		,DatasetID = @DatasetID
		,DatasetStartDate = Encounter.ReferralReceivedDate
		,DatasetEndDate = Encounter.ReferralClosedDate
		,ContextID = Reference.ContextID
		,ContextCode = Encounter.ContextCode
		,Encounter.SourceUniqueID
		,Encounter.PatientNHSNumber
		,Encounter.PatientPostcode
		,EthnicCategory.NationalEthnicCategoryCode

		,SpecialtyCode = Reference.ReferredToSpecialtyID
		,ReferralStatusCode = Encounter.ReferralStatus
		,ReferralReasonCode = Encounter.ReasonForReferralID
		,PatientID = Encounter.PatientSourceID
		,StaffTeamCode = Reference.ReferredToStaffTeamID
	from
		COM.BaseReferral Encounter

	inner join COM.BaseReferralReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join 
		(
		select
			 SourceEthnicCategoryID = LookupID
			,NationalEthnicCategoryCode = LookupNHSCode
		from 
			[$(Warehouse)].COM.LookupBase
		where
			LookupTypeID like 'ETHGR%'
		) EthnicCategory
	on	EthnicCategory.SourceEthnicCategoryID = Encounter.PatientEthnicGroupID

	where
		Encounter.ReferralReceivedDate > DATEADD(year, -2, getdate())
	and Encounter.ArchiveFlag = 'N'


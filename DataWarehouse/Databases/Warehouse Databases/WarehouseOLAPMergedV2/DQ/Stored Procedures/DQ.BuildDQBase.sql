﻿CREATE procedure [DQ].[BuildDQBase]
	@DatasetCode varchar(10)
as

declare @DatasetID smallint =
	(
	select
		DatasetID
	from
		DQ.Dataset
	where
		Dataset.DatasetCode = @DatasetCode
	)

declare @UnassignedServiceID int =
	(
	select
		ServiceID
	from
		WH.Service
	where
		Service.Service = 'Unassigned'
	)


delete
from
	DQ.Base
where
	Base.DatasetID = @DatasetID

if @DatasetCode = 'AE'

	insert
	into
		DQ.Base
	(
		 DatasetRecno
		,ContextID
		,DatasetID
		,EncounterDateID
		,ServiceID
		,DirectorateID
		,SiteID
		,ProfessionalCarerID
		,DistrictNo
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,Postcode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,NHSNumber
		,Cases
	)
	select
		 WrkDataset.DatasetRecno
		,WrkDataset.ContextID
		,WrkDataset.DatasetID
		,EncounterDate = Reference.EncounterStartDateID
		,ServiceID = @UnassignedServiceID

		,DirectorateID =
			coalesce(
				 Directorate.DirectorateID
				,'21' --N/A
			)

		,Reference.SiteID
		,ProfessionalCarerID = Reference.StaffMemberID
		,Encounter.DistrictNo
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
		,Encounter.Postcode
		,Encounter.RegisteredGpCode
		,Encounter.RegisteredPracticeCode
		,Encounter.NHSNumber
		,Cases = 1
	from
		DQ.WrkDataset

	inner join AE.BaseEncounter Encounter
	on	Encounter.MergeEncounterRecno = WrkDataset.DatasetRecno

	inner join AE.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join WH.Site
	on	Site.SourceSiteID = Reference.SiteID

	left join WH.Directorate
	on	Directorate.DirectorateCode =
			case Site.NationalSiteCode
			when 'RW3TR' then '80' --Trafford
			when 'RW3T1' then '80' --Trafford
			when 'RW3MR' then '90' --MRI
			when 'RW3SM' then '30' --St Marys
			when 'RW3RC' then  '0' --Childrens
			when 'RW3DH' then '50' --Dental
			when 'RW3RE' then '40' --Eye
			when 'ADAS'  then '21' --N/A
			else '21'
			end


	where
		WrkDataset.DatasetID = @DatasetID



if @DatasetCode = 'APC'

	insert
	into
		DQ.Base
	(
		 DatasetRecno
		,ContextID
		,DatasetID
		,EncounterDateID
		,ServiceID
		,DirectorateID
		,SiteID
		,ProfessionalCarerID
		,DistrictNo
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,Postcode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,NHSNumber
		,Cases
	)
	select
		 WrkDataset.DatasetRecno
		,WrkDataset.ContextID
		,WrkDataset.DatasetID
		,EncounterDate = Reference.EpisodeStartDateID

		,ServiceID =
			coalesce(
				 DatasetAllocation.AllocationID
				,@UnassignedServiceID
			)

		,DirectorateID = StartDirectorate.DirectorateID
		,Reference.StartSiteID
		,ProfessionalCarerID = Reference.ConsultantID
		,Base.DistrictNo
		,Base.PatientForename
		,Base.PatientSurname
		,Base.DateOfBirth
		,Base.Postcode
		,Base.RegisteredGpCode
		,Base.RegisteredGpPracticeCode
		,Base.NHSNumber
		,Cases = 1
	from
		DQ.WrkDataset

	inner join APC.BaseEncounter Base
	on	Base.MergeEncounterRecno = WrkDataset.DatasetRecno
	and Base.Reportable = 1
	
	inner join APC.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = WrkDataset.DatasetRecno

	inner join WH.Directorate StartDirectorate
	on	StartDirectorate.DirectorateCode = Base.StartDirectorateCode

	inner join DQ.Dataset
	on	Dataset.DatasetID = WrkDataset.DatasetID

	left join Allocation.DatasetAllocation
	on	DatasetAllocation.DatasetRecno = WrkDataset.DatasetRecno
	and	DatasetAllocation.DatasetCode = Dataset.DatasetCode
	and	DatasetAllocation.AllocationTypeID = 2 --Service

	where
		WrkDataset.DatasetID = @DatasetID




if @DatasetCode = 'OP'

	insert
	into
		DQ.Base
	(
		 DatasetRecno
		,ContextID
		,DatasetID
		,EncounterDateID
		,ServiceID
		,DirectorateID
		,SiteID
		,ProfessionalCarerID
		,DistrictNo
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,Postcode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,NHSNumber
		,Cases
	)
	select
		 WrkDataset.DatasetRecno
		,WrkDataset.ContextID
		,WrkDataset.DatasetID
		,EncounterDate = Reference.AppointmentDateID

		,ServiceID =
			coalesce(
				 DatasetAllocation.AllocationID
				,@UnassignedServiceID
			)

		,DirectorateID = Directorate.DirectorateID
		,Reference.SiteID
		,ProfessionalCarerID = Reference.ConsultantID
		,Base.DistrictNo
		,Base.PatientForename
		,Base.PatientSurname
		,Base.DateOfBirth
		,Base.Postcode
		,Base.RegisteredGpCode
		,Base.RegisteredGpPracticeCode
		,Base.NHSNumber
		,Cases = 1
	from
		DQ.WrkDataset

	inner join OP.BaseEncounter Base
	on	Base.MergeEncounterRecno = WrkDataset.DatasetRecno

	inner join OP.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Base.MergeEncounterRecno

	inner join WH.Directorate
	on	Directorate.DirectorateCode = Base.DirectorateCode

	inner join DQ.Dataset
	on	Dataset.DatasetID = WrkDataset.DatasetID

	left join Allocation.DatasetAllocation
	on	DatasetAllocation.DatasetRecno = WrkDataset.DatasetRecno
	and	DatasetAllocation.DatasetCode = Dataset.DatasetCode
	and	DatasetAllocation.AllocationTypeID = 2 --Service

	where
		WrkDataset.DatasetID = @DatasetID




if @DatasetCode = 'COM'

	insert
	into
		DQ.Base
	(
		 DatasetRecno
		,ContextID
		,DatasetID
		,EncounterDateID
		,ServiceID
		,DirectorateID
		,SiteID
		,ProfessionalCarerID
		,DistrictNo
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,Postcode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,NHSNumber
		,Cases
	)
	select
		 WrkDataset.DatasetRecno
		,WrkDataset.ContextID
		,WrkDataset.DatasetID
		,EncounterDate = Reference.StartDateID

		,ServiceID = @UnassignedServiceID

		,DirectorateID =
			coalesce(
				 SpecialtyDirectorateMap.DirectorateID
				,'21' --N/A
			)

		,SiteID =
			(
			select
				Site.SourceSiteID
			from
				WH.Site
			where
				Site.SourceSiteCode = 'C'
			and	Site.SourceContextCode = 'CEN||IPM'
			)

		,Reference.ProfessionalCarerID
		,Base.PatientIDPatientFacilityID
		,Base.PatientForename
		,Base.PatientSurname
		,Base.PatientDateOfBirth
		,Base.PatientPostcode
		,Base.PatientEncounterRegisteredGPCode
		,Base.PatientCurrentRegisteredPracticeCode
		,Base.PatientNHSNumber
		,Cases = 1
	from
		DQ.WrkDataset

	inner join COM.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = WrkDataset.DatasetRecno

	inner join COM.BaseEncounter Base
	on	Base.MergeEncounterRecno = Reference.MergeEncounterRecno
	
	left join COM.SpecialtyDirectorateMap
	on	SpecialtyDirectorateMap.SpecialtyID = Base.SpecialtyID

	where
		WrkDataset.DatasetID = @DatasetID
	and	Reference.StartDateID < 
		(
		select
			Calendar.DateID
		from
			WH.Calendar
		where
			Calendar.TheDate = cast(GETDATE() as date)
		)


if @DatasetCode = 'COMR'

	insert
	into
		DQ.Base
	(
		 DatasetRecno
		,ContextID
		,DatasetID
		,EncounterDateID
		,ServiceID
		,DirectorateID
		,SiteID
		,ProfessionalCarerID
		,DistrictNo
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,Postcode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,NHSNumber
		,Cases
	)
	select
		 WrkDataset.DatasetRecno
		,WrkDataset.ContextID
		,WrkDataset.DatasetID

		,EncounterDateID =
			coalesce(
				Calendar.DateID
				,(
				select
					Calendar.DateID
				from
					WH.Calendar
				where
					Calendar.TheDate = '31 dec 9999'
				)
			)

		,ServiceID = @UnassignedServiceID

		,DirectorateID =
			coalesce(
				 SpecialtyDirectorateMap.DirectorateID
				,'21' --N/A
			)

		,SiteID =
			(
			select
				Site.SourceSiteID
			from
				WH.Site
			where
				Site.SourceSiteCode = 'C'
			and	Site.SourceContextCode = 'CEN||IPM'
			)

		,ProfessionalCarerID =
			coalesce(
				 Reference.ReferredToProfessionalCarerID
				,(
				select
					ProfessionalCarer.SourceValueID
				from
					COM.ProfessionalCarer
				where
					ProfessionalCarer.ProfessionalCarerID = '-1'
				)
			)

		,Base.PatientSourceID
		,Base.PatientForename
		,Base.PatientSurname
		,Base.PatientDateOfBirth
		,Base.PatientPostcode
		,RegisteredGpCode = null
		,RegisteredGpPracticeCode = null
		,Base.PatientNHSNumber
		,Cases = 1
	from
		DQ.WrkDataset

	inner join COM.BaseReferralReference Reference
	on	Reference.MergeEncounterRecno = WrkDataset.DatasetRecno

	inner join COM.BaseReferral Base
	on	Base.MergeEncounterRecno = Reference.MergeEncounterRecno

	left join COM.SpecialtyDirectorateMap
	on	SpecialtyDirectorateMap.SpecialtyID = Base.ReferredToSpecialtyID

	left join WH.Calendar
	on	Calendar.TheDate =
			coalesce(
				 Base.ReferralReceivedDate
				,'1 jan 1900'
			)

	where
		WrkDataset.DatasetID = @DatasetID
	and	Calendar.DateID < 
		(
		select
			Calendar.DateID
		from
			WH.Calendar
		where
			Calendar.TheDate = cast(GETDATE() as date)
		)


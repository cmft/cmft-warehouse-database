﻿CREATE procedure [DQ].[Validate]
	@DatasetCode varchar(10)
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


exec DQ.BuildWrkDataset @DatasetCode
exec DQ.ExecuteValidation @DatasetCode
exec DQ.BuildDQBase @DatasetCode

exec DQ.BuildDQFactErrorSnapshot @DatasetCode
exec DQ.BuildDQFactErrorDenominatorSnapshot @DatasetCode
exec DQ.BuildDQFactRowInErrorSnapshot @DatasetCode


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Dataset ' + @DatasetCode +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


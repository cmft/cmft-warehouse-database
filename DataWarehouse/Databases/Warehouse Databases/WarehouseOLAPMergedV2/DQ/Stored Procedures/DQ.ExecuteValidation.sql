﻿CREATE procedure [DQ].[ExecuteValidation] 
	 @DatasetCode varchar(10)
as

--------------------------------------------------------------------------
-- Copyright  Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare @ErrorID int
declare @ErrorLogic varchar(255)
declare @DenominatorID int
declare @DenominatorLogic varchar(255)
declare @WrkSQL varchar(max)


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare @Processed int
declare @Validated int 
declare @Failed int
declare @Source varchar (128)


declare @DatasetID smallint =
	(
	select
		DatasetID
	from
		DQ.Dataset
	where
		Dataset.DatasetCode = @DatasetCode
	)

--Generate Dataset Validation

delete
from
	DQ.WrkValidate
where
	DatasetID = @DatasetID


declare ErrorIDCursor cursor fast_forward for

select
	 ErrorDataset.ErrorID
	,Error.ErrorLogic
from
	DQ.ErrorDataset

inner join DQ.Error
on	Error.ErrorID = ErrorDataset.ErrorID

where  	
	ErrorDataset.DatasetID = @DatasetID
and ErrorDataset.Active = 1 

order by
	ErrorDataset.ErrorID

OPEN ErrorIDCursor

FETCH NEXT FROM ErrorIDCursor

INTO
	 @ErrorID
	,@ErrorLogic

WHILE @@FETCH_STATUS = 0

BEGIN

	select
		@WrkSQL =
			'insert into DQ.WrkValidate (DatasetID, DatasetRecno, ErrorID)' +
			' select DatasetID, DatasetRecno, ErrorID = ' + cast(@ErrorID as varchar) + ' from ' + @ErrorLogic +
			' where DatasetID = ' + cast(@DatasetID as varchar)

	print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL
	
	execute(@WrkSQL)

	FETCH NEXT FROM ErrorIDCursor
	INTO
		 @ErrorID
		,@ErrorLogic

END
  
CLOSE ErrorIDCursor
deallocate ErrorIDCursor


delete
from
	DQ.DatasetValidation
where
	DatasetID = @DatasetID


insert into DQ.DatasetValidation
	(
	 DatasetID
	,DatasetRecno
	,ErrorID
	)
select
 	 WrkValidate.DatasetID
 	,WrkValidate.DatasetRecno
	,WrkValidate.ErrorID
from 
	DQ.WrkValidate
where
	DatasetID = @DatasetID


--Generate Dataset Denominators

delete
from
	DQ.WrkDenominator
where
	DatasetID = @DatasetID


declare DenominatorIDCursor cursor fast_forward for

select
	 Denominator.DenominatorID
	,Denominator.DenominatorLogic
from
	DQ.Denominator
where
	exists
	(
	select
		1
	from
		DQ.Error

	inner join DQ.ErrorDataset
	on	ErrorDataset.ErrorID = Error.ErrorID
	and	ErrorDataset.Active = 1
	and	ErrorDataset.DatasetID = @DatasetID

	where
		Error.DenominatorID = Denominator.DenominatorID
	)
order by
	Denominator.DenominatorID

OPEN DenominatorIDCursor

FETCH NEXT FROM DenominatorIDCursor

INTO
	 @DenominatorID
	,@DenominatorLogic

WHILE @@FETCH_STATUS = 0

BEGIN

	select
		@WrkSQL =
			'insert into DQ.WrkDenominator (DatasetID, DatasetRecno, DenominatorID)' +
			' select DatasetID, DatasetRecno, DenominatorID = ' + cast(@DenominatorID as varchar) + ' from ' + @DenominatorLogic +
			' where DatasetID = ' + cast(@DatasetID as varchar)
	
	print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL

	execute(@WrkSQL)

	FETCH NEXT FROM DenominatorIDCursor
	INTO
		 @DenominatorID
		,@DenominatorLogic

END
  
CLOSE DenominatorIDCursor
deallocate DenominatorIDCursor


delete from DQ.DatasetDenominator
where
	DatasetID = @DatasetID


insert into DQ.DatasetDenominator
	(
	 DatasetID
	,DatasetRecno
	,DenominatorID
	)
select
 	 WrkDenominator.DatasetID
 	,WrkDenominator.DatasetRecno
	,WrkDenominator.DenominatorID
from 
	DQ.WrkDenominator
where
	DatasetID = @DatasetID



select
	@Processed = count(*)
from
	DQ.WrkDataset
where
	DatasetID = @DatasetID


select 
	@Validated = count(*) 
from 
	DQ.WrkDataset
where
	WrkDataset.DatasetID = @DatasetID
and	not exists 
	(
	select
		1
	from 
		DQ.WrkValidate
	where
    	WrkValidate.DatasetRecno = WrkDataset.DatasetRecno
	and WrkValidate.DatasetID  = WrkDataset.DatasetID
	)


select @Failed = @Processed - @Validated

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Dataset ' + @DatasetCode +
		', Processed ' + CONVERT(varchar(10), @Processed) +
		', Validated ' + CONVERT(varchar(10), @Validated) +
		', Failed ' + CONVERT(varchar(10), @Failed) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


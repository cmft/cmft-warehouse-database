﻿CREATE procedure [DQ].[BuildDQFactRowInErrorSnapshot]
	@DatasetCode varchar(10)
as


declare @DatasetID smallint =
	(
	select
		DatasetID
	from
		DQ.Dataset
	where
		Dataset.DatasetCode = @DatasetCode
	)

delete
from
	DQ.FactRowInErrorSnapshot
where
	FactRowInErrorSnapshot.DateID =
	(
	select
		Calendar.DateID
	from
		WH.Calendar
	where
		Calendar.TheDate = CAST(getdate() as date)
	)
and	FactRowInErrorSnapshot.DatasetID = @DatasetID


insert
into
	DQ.FactRowInErrorSnapshot
(
	 DateID
	,ContextID
	,DatasetID
	,EncounterDateID
	,ServiceID
	,DirectorateID
	,SiteID
	,ProfessionalCarerID
	,Cases
)
select
	 Calendar.DateID
	,FactRowInError.ContextID
	,FactRowInError.DatasetID
	,FactRowInError.EncounterDateID
	,FactRowInError.ServiceID
	,FactRowInError.DirectorateID
	,FactRowInError.SiteID
	,FactRowInError.ProfessionalCarerID
	,Cases = SUM(FactRowInError.Cases)
from
	DQ.FactRowInError

inner join WH.Calendar
on	Calendar.TheDate = CAST(getdate() as date)

where
	FactRowInError.DatasetID = @DatasetID

group by
	 Calendar.DateID
	,FactRowInError.ContextID
	,FactRowInError.DatasetID
	,FactRowInError.EncounterDateID
	,FactRowInError.ServiceID
	,FactRowInError.DirectorateID
	,FactRowInError.SiteID
	,FactRowInError.ProfessionalCarerID

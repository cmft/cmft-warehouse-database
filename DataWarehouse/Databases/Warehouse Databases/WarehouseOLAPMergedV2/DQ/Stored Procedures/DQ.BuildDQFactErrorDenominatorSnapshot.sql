﻿CREATE procedure [DQ].[BuildDQFactErrorDenominatorSnapshot]
	@DatasetCode varchar(10)
as


declare @DatasetID smallint =
	(
	select
		DatasetID
	from
		DQ.Dataset
	where
		Dataset.DatasetCode = @DatasetCode
	)

delete
from
	DQ.FactErrorDenominatorSnapshot
where
	FactErrorDenominatorSnapshot.DateID =
	(
	select
		Calendar.DateID
	from
		WH.Calendar
	where
		Calendar.TheDate = CAST(getdate() as date)
	)
and	FactErrorDenominatorSnapshot.DatasetID = @DatasetID

insert
into
	DQ.FactErrorDenominatorSnapshot
(
	 DateID
	,ContextID
	,DatasetID
	,EncounterDateID
	,ServiceID
	,DirectorateID
	,SiteID
	,ProfessionalCarerID
	,DenominatorID
	,DenominatorCases
)
select
	 Calendar.DateID
	,FactErrorDenominator.ContextID
	,FactErrorDenominator.DatasetID
	,FactErrorDenominator.EncounterDateID
	,FactErrorDenominator.ServiceID
	,FactErrorDenominator.DirectorateID
	,FactErrorDenominator.SiteID
	,FactErrorDenominator.ProfessionalCarerID
	,FactErrorDenominator.DenominatorID
	,Cases = SUM(FactErrorDenominator.DenominatorCases)
from
	DQ.FactErrorDenominator

inner join WH.Calendar
on	Calendar.TheDate = CAST(getdate() as date)

where
	FactErrorDenominator.DatasetID = @DatasetID

group by
	 Calendar.DateID
	,FactErrorDenominator.ContextID
	,FactErrorDenominator.DatasetID
	,FactErrorDenominator.EncounterDateID
	,FactErrorDenominator.ServiceID
	,FactErrorDenominator.DirectorateID
	,FactErrorDenominator.SiteID
	,FactErrorDenominator.ProfessionalCarerID
	,FactErrorDenominator.DenominatorID

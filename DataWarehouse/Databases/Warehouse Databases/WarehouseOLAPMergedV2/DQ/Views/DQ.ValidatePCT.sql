﻿create view DQ.ValidatePCT as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.PCTCode is null

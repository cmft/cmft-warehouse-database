﻿create view DQ.ValidateHRGInvalid as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.HRGCode like 'U%'
and	Encounter.DischargeDate is not null

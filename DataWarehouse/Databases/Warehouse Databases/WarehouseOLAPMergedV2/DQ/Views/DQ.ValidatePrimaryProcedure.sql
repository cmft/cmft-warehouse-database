﻿create view DQ.ValidatePrimaryProcedure as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.PrimaryProcedureCode is null
and	Encounter.OperationStatusCode = '1'

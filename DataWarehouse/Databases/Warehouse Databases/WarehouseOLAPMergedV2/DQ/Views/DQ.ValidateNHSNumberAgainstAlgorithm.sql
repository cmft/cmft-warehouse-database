﻿

-- 20140630 RR added criteria for ''

CREATE view [DQ].[ValidateNHSNumberAgainstAlgorithm] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	dbo.Check_NHS_number(replace(Encounter.NHSNumber, ' ', '')) = 0
and	Encounter.NHSNumber is not null
and Encounter.NHSNumber <> '' -- 20140630 RR as these are picked up with nulls in NHS No Coverage




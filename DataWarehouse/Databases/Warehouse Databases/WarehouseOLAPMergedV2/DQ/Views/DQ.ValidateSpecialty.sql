﻿create view [DQ].[ValidateSpecialty] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.SpecialtyCode is null



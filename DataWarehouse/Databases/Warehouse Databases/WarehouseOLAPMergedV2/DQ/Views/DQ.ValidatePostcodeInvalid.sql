﻿create view DQ.ValidatePostcodeInvalid as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.Postcode = 'ZZ99 3WZ'
or	not exists
	(
	select
		1
	from
		[$(Organisation)].dbo.Postcode
	where
		Postcode.Postcode = Encounter.Postcode
	)


﻿


CREATE view [DQ].[ValidateFirstInvestigation] as

--First Investigation Code Missing
select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalAttendanceDisposalCode <> '12' --exclude Did Not Wait, Self Discharge, Transfer

and	not exists
	(
	select
		1
	from
		AE.BaseInvestigation Investigation
	where
		Investigation.MergeEncounterRecno = Encounter.DatasetRecno
	and	Investigation.ContextCode = 
		(
		select
			ContextCode
		from
			WH.Context
		where
			Context.ContextID = Encounter.ContextID
		)
	and	Investigation.SequenceNo = 1
	)





﻿CREATE view [DQ].[DenominatorAEPatientGroup] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.SiteCode not in
	(
	 'RW3RE'
	,'RW3SM'
	)
and	Encounter.NationalAttendanceCategoryCode = '1' --First Accident And Emergency Attendance

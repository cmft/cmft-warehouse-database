﻿






CREATE view [DQ].[ValidateOpenReferral] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	coalesce(Encounter.DatasetEndDate, getdate()) = GETDATE()
and  not exists
	(
	select
		1
	from
		COM.BaseEncounter Activity
	where
		Activity.ReferralID = Encounter.SourceEncounterRecno
	and	Activity.ContextCode = Encounter.ContextCode
	and EncounterOutcomeCode = 'ATTD'
	and RecordStatus = 'C'
	and JointActivityFlag = 0
	and Archived = 'N'
	)









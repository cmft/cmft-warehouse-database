﻿create view [DQ].[ValidateWaitingListCode] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.WaitingListCode is null



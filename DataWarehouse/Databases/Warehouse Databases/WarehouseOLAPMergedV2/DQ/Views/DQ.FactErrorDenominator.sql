﻿



CREATE view [DQ].[FactErrorDenominator] as

select
	 Base.ContextID
	,Base.DatasetID
	,Base.EncounterDateID
	,Base.ServiceID
	,Base.DirectorateID
	,Base.SiteID
	,Base.ProfessionalCarerID

	,DatasetDenominator.DenominatorID
	,DenominatorCases = 1
from
	DQ.Base

inner join DQ.DatasetDenominator
on	DatasetDenominator.DatasetRecno = Base.DatasetRecno
and	DatasetDenominator.DatasetID = Base.DatasetID




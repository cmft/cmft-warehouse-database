﻿create view DQ.ErrorDimension as

select
	 Error.ErrorID
	,Error.Error
	,Error.ResponsibleTeamID
	,Error.ErrorLogic
	,Error.DenominatorID

	,AEError =
		case
		when AEErrorSet.ErrorID is null
		then 0
		else 1
		end

	,APCError =
		case
		when APCErrorSet.ErrorID is null
		then 0
		else 1
		end

	,OPError =
		case
		when OPErrorSet.ErrorID is null
		then 0
		else 1
		end

	,COMError =
		case
		when COMErrorSet.ErrorID is null
		then 0
		else 1
		end

	,COMRError =
		case
		when COMRErrorSet.ErrorID is null
		then 0
		else 1
		end

from
	DQ.Error

left join DQ.ErrorDataset AEErrorSet
on	AEErrorSet.ErrorID = Error.ErrorID
and	AEErrorSet.Active = 1
and	AEErrorSet.DatasetID = 1
and	Error.ResponsibleTeamID = 2

left join DQ.ErrorDataset APCErrorSet
on	APCErrorSet.ErrorID = Error.ErrorID
and	APCErrorSet.Active = 1
and	APCErrorSet.DatasetID = 2
and	Error.ResponsibleTeamID = 2

left join DQ.ErrorDataset OPErrorSet
on	OPErrorSet.ErrorID = Error.ErrorID
and	OPErrorSet.Active = 1
and	OPErrorSet.DatasetID = 3
and	Error.ResponsibleTeamID = 2

left join DQ.ErrorDataset COMErrorSet
on	COMErrorSet.ErrorID = Error.ErrorID
and	COMErrorSet.Active = 1
and	COMErrorSet.DatasetID = 4
and	Error.ResponsibleTeamID = 2

left join DQ.ErrorDataset COMRErrorSet
on	COMRErrorSet.ErrorID = Error.ErrorID
and	COMRErrorSet.Active = 1
and	COMRErrorSet.DatasetID = 5
and	Error.ResponsibleTeamID = 2

﻿



CREATE view [DQ].[ValidatePlannedAppointmentsInPast] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
--this needs further specification
	Encounter.DatasetStartDate < GETDATE()
and	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.OutcomeCode = Encounter.OutcomeCode
	and RuleBase.ScheduleTypeID = Encounter.ScheduleTypeID
	and	RuleBase.ErrorID = 109
	)







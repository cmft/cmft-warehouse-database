﻿create view DQ.Census as

select
	 Calendar.DateID
	,Calendar.TheDate
	,Calendar.LongDate
from
	WH.Calendar
where
	exists
	(
	select
		1
	from
		DQ.FactErrorSnapshot
	where
		FactErrorSnapshot.DateID = Calendar.DateID
	)

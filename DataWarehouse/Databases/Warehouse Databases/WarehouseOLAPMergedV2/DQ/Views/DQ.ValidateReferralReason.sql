﻿

CREATE view [DQ].[ValidateReferralReason] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.ReferralReasonCode in ('1156','2000667','2004133')
and	Encounter.DatasetEndDate is not null





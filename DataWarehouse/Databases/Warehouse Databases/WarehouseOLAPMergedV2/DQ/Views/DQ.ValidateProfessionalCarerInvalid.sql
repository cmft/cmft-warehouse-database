﻿create view DQ.ValidateProfessionalCarerInvalid as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.ProfessionalCarerID = 10547385	--Hv Lead Nurse


﻿create view DQ.ValidateAmbulanceArrivalTime as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalArrivalModeCode = '1'
and	(
		Encounter.AmbulanceArrivalTime is null
	or	Encounter.AmbulanceArrivalTime = ''
	)



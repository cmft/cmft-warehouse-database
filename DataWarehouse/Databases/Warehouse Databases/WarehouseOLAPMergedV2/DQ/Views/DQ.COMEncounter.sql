﻿








CREATE view [DQ].[COMEncounter] as

select
	 Encounter.SourceEncounterRecno
	,Encounter.SourceEncounterID
	,Encounter.SourceUniqueID
	,SourceSpecialtyID = Encounter.SpecialtyID
	,SourceStaffTeamID = Encounter.StaffTeamID
	,SourceProfessionalCarerID = Encounter.ProfessionalCarerID
	,Encounter.SeenByProfessionalCarerID
	,Encounter.EncounterProfessionalCarerID
	,Encounter.StartDate
	,Encounter.StartTime
	,Encounter.EndTime
	,Encounter.ArrivedTime
	,Encounter.SeenTime
	,Encounter.DepartedTime
	,Encounter.AttendedID
	,Encounter.OutcomeID
	,Encounter.EncounterOutcomeCode
	,Encounter.EncounterDuration
	,Encounter.ReferralID
	,Encounter.ProfessionalCarerEpisodeID
	,Encounter.PatientSourceID
	,Encounter.PatientSourceSystemUniqueID
	,Encounter.PatientNHSNumber
	,Encounter.PatientNHSNumberStatusIndicator
	,Encounter.PatientIDLocalIdentifier
	,Encounter.PatientIDPASNumber
	,Encounter.PatientIDPatientIdentifier
	,Encounter.PatientIDPatientFacilityID
	,Encounter.PatientTitleID
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.PatientPostcode
	,Encounter.PatientDateOfBirth
	,Encounter.PatientDateOfDeath
	,Encounter.PatientSexID
	,Encounter.PatientEthnicGroupID
	,Encounter.PatientSpokenLanguageID
	,Encounter.PatientReligionID
	,Encounter.PatientMaritalStatusID
	,Encounter.PatientCurrentRegisteredPracticeCode
	,Encounter.PatientEncounterRegisteredPracticeCode
	,Encounter.PatientCurrentRegisteredGPCode
	,Encounter.PatientEncounterRegisteredGPCode
	,Encounter.VisitID
	,Encounter.ContactTypeID
	,Encounter.EncounterTypeID
	,Encounter.ScheduleTypeID
	,Encounter.ScheduleReasonID
	,Encounter.CanceledReasonID
	,Encounter.CanceledTime
	,Encounter.CancelledByID
	,Encounter.MoveReasonID
	,Encounter.MoveTime
	,Encounter.ServicePointID
	,Encounter.ServicePointSessionID
	,Encounter.MoveCount
	,Encounter.LocationTypeID
	,Encounter.LocationDescription
	,Encounter.LocationTypeHealthOrgID
	,Encounter.EncounterLocationID
	,Encounter.ServicePointStaysID
	,Encounter.WaitingListID
	,Encounter.PlannedAttendees
	,Encounter.ActualAttendees
	,Encounter.ProviderSpellID
	,Encounter.RTTStatusID
	,Encounter.EarliestReasinableOfferTime
	,Encounter.EncounterInterventionsCount
	,Encounter.CreatedTime
	,Encounter.ModifiedTime
	,ModifiedDate = CAST(Encounter.ModifiedTime as date)
	,SourceCreatedByID = Encounter.CreatedBy
	,SourceModifiedByID = Encounter.ModifiedBy
	,Encounter.Archived
	,Encounter.ParentID
	,Encounter.HealthOrgOwnerID
	,Encounter.RecordStatus
	,Encounter.JointActivityFlag
	,Encounter.Comment
	,Encounter.DerivedFirstAttendanceFlag
	,Encounter.ReferralReceivedTime
	,Encounter.ReferralReceivedDate
	,Encounter.ReferralClosedTime
	,Encounter.ReferralClosedDate
	,Encounter.ReferralReasonID
	,SourceReferredToSpecialtyID = Encounter.ReferredToSpecialtyID
	,Encounter.ReferredToStaffTeamID
	,Encounter.ReferredToProfCarerID
	,Encounter.ReferralSourceID
	,Encounter.ReferralTypeID
	,Encounter.CommissionerCode
	,Encounter.PctOfResidence
	,Encounter.LocalAuthorityCode
	,Encounter.DistrictOfResidence
	,Encounter.ContextCode

	,Reference.ProfessionalCarerID
	,Reference.LocationID
	,Reference.StaffTeamID
	,Reference.SpecialtyID
	,Reference.MergeEncounterRecno
	,Reference.CreatedByID
	,Reference.ModifiedByID
	,Reference.ReferredToSpecialtyID
from
	COM.BaseEncounter Encounter

inner join COM.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

--where
--	exists
--	(
--	select
--		1
--	from
--		DQ.DatasetValidation
--	where
--		DatasetValidation.DatasetRecno = Reference.EncounterRecno
--	and	DatasetValidation.DatasetID =
--		(
--		select
--			DatasetID
--		from
--			DQ.Dataset
--		where
--			Dataset.DatasetCode = 'COM'
--		)
--	)










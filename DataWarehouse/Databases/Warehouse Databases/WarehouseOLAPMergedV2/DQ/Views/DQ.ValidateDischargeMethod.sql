﻿CREATE view [DQ].[ValidateDischargeMethod] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.DischargeMethodCode = Encounter.DischargeMethodCode
	and	RuleBase.ErrorID = 105
	)

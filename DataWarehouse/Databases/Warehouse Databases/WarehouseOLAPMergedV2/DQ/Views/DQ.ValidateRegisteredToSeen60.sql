﻿create view DQ.ValidateRegisteredToSeen60 as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalAttendanceDisposalCode <> '12' --exclude Did Not Wait, Self Discharge, Transfer

and	datediff(
		minute
		,Encounter.RegisteredTime
		,Encounter.SeenForTreatmentTime
	) > 60

﻿create view [DQ].[DenominatorCommunityContactDNA] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.ScheduleTypeID = 1468 --Contacts 
and	Encounter.OutcomeCode = 'DNAT'


﻿CREATE view [DQ].[ValidateDisposalInvalid] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		AE.AttendanceDisposal
	where
		AttendanceDisposal.SourceAttendanceDisposalCode = Encounter.SourceAttendanceDisposalCode
	and	AttendanceDisposal.NationalAttendanceDisposalCode = '14' --Other
	)

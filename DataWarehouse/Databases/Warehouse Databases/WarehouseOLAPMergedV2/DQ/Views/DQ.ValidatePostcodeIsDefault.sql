﻿create view DQ.ValidatePostcodeIsDefault as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.Postcode <> 'ZZ99 3WZ'
and	left(Encounter.Postcode, 4) = 'ZZ99'

﻿create view DQ.ValidateReferrer as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.ReferrerCode is null

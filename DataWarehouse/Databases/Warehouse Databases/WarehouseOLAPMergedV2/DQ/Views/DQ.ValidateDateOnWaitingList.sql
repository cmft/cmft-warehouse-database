﻿

CREATE view [DQ].[ValidateDateOnWaitingList] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.DateOnWaitingList is null
and AdmissionMethodCode in ('11','12','13')


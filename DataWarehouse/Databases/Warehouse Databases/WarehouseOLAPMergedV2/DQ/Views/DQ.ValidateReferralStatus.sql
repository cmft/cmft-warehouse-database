﻿	
CREATE view [DQ].[ValidateReferralStatus] as	
	
select	
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from	
	DQ.WrkDataset Encounter
where	
	Encounter.[ReferralStatusCode] in ('1197','-1')


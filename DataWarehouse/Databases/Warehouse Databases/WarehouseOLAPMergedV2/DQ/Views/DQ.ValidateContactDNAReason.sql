﻿
CREATE view [DQ].[ValidateContactDNAReason] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.ScheduleTypeID = 1468 --Contacts 
and	Encounter.OutcomeCode = 'DNAT'
and	Encounter.DNAReasonID = 125




﻿
CREATE view [DQ].[ValidateRehabWard] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

inner join APC.BaseEncounter Base
on	Base.MergeEncounterRecno = Encounter.DatasetRecno

inner join APC.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.DatasetRecno

inner join APC.Ward
on	Ward.SourceWardID = Reference.StartWardID

where
	Encounter.SpecialtyCode = '314'
and	(
		Ward.SourceWard like '%30%'
	or	Ward.SourceWard like '%32%'
	)


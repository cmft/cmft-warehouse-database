﻿create view DQ.ValidateReferralActivitySpecialtyMismatch as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.ReferralSpecialtyCode <> Encounter.SpecialtyCode



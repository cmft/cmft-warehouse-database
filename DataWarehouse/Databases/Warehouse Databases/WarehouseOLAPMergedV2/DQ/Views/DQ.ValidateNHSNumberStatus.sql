﻿


CREATE view [DQ].[ValidateNHSNumberStatus] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NHSNumber is not null
	and Encounter.NHSNumber <>''
	and Encounter.NHSNumberStatusCode not in
	(
	 '01'	-- Present and Verified
	 ,'07'	-- Not required
	 ,'08'	-- Postponed (Baby <6wks)
	)




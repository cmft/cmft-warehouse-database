﻿create view [DQ].[ValidateStaffTeam] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.StaffTeamCode is null



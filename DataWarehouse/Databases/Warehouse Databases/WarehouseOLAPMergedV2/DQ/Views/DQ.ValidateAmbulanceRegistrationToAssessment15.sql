﻿create view DQ.ValidateAmbulanceRegistrationToAssessment15 as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalArrivalModeCode = '1'
and	datediff(
		minute
		,Encounter.RegisteredTime
		,Encounter.InitialAssessmentTime
	) > 15


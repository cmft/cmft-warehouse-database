﻿create view DQ.ValidatePracticeDefault as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

left join [$(Organisation)].dbo.Gp
on	Gp.OrganisationCode = Encounter.GPCode

where
	Encounter.GPPracticeCode in
	(
	 'V81997'
	,'V81998'
	,'V81999'
	)

and	(
		Gp.ParentOrganisationCode is null
	or	Gp.ParentOrganisationCode = Encounter.GPPracticeCode
	)

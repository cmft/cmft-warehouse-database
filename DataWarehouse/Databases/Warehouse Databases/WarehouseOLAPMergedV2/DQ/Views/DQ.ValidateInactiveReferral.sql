﻿



CREATE view [DQ].[ValidateInactiveReferral] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	datediff(month, Encounter.DatasetStartDate, getdate()) > 13 -- only return referrals received 13 months ago or more
and
	Encounter.DatasetEndDate is null
and	not exists
	(
	select	
		1
	from
		COM.BaseEncounter Activity
	where
		Activity.ReferralID = Encounter.SourceEncounterRecno
	and	Activity.ContextCode = Encounter.ContextCode
	and EncounterOutcomeCode = 'ATTD'
	and	datediff(month, Activity.StartDate, GETDATE()) <= 13
	and RecordStatus = 'C'
	and JointActivityFlag = 0
	and Archived = 'N'
	)











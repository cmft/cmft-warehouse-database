﻿
CREATE view [DQ].[FactError] as

select
	 Base.DatasetRecno
	,Base.ContextID
	,Base.DatasetID
	,Base.EncounterDateID
	,Base.ServiceID
	,Base.DirectorateID
	,Base.SiteID
	,Base.ProfessionalCarerID

	,DatasetValidation.ErrorID
	,Cases = 1
from
	DQ.Base

inner join DQ.DatasetValidation
on	DatasetValidation.DatasetRecno = Base.DatasetRecno
and	DatasetValidation.DatasetID = Base.DatasetID


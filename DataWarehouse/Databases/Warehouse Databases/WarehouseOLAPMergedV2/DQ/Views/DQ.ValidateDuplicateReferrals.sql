﻿CREATE view [DQ].[ValidateDuplicateReferrals] as

select
	 DatasetID = 5
	,Encounter.DatasetRecno

from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		COM.BaseReferral Referral
	inner join COM.[BaseReferralReference] ReferralReference
	on ReferralReference.MergeEncounterRecno = Referral.MergeEncounterRecno

	where
		Referral.SourceUniqueID != Encounter.SourceEncounterRecno
	and	Referral.ContextCode = Encounter.ContextCode
	and	convert(varchar,ReferralReference.ReferredToSpecialtyID) = Encounter.SpecialtyCode
	and	convert(varchar,ReferralReference.ReferredToStaffTeamID) = Encounter.StaffTeamCode
	and	convert(varchar,Referral.PatientSourceID) = Encounter.PatientID
	and	Referral.ReferralClosedDate is null
	)

and	Encounter.DischargeDate is null







﻿create view DQ.DenominatorCommunityAppointmentCancelled as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.ScheduleTypeID = 1470 --Appointments
and	Encounter.OutcomeCode in
	(
	 'CHOS'
	,'CPAT'
	,'COTH'
	)

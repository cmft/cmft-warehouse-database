﻿CREATE view [DQ].[ValidateTreatmentFunctionInvalid] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.SpecialtyCode = Encounter.SpecialtyCode
	and	RuleBase.ErrorID = 5
	)

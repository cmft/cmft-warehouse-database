﻿create view DQ.ValidatePrimaryDiagnosisInvalid as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.Diagnosis is null

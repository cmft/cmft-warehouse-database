﻿



CREATE view [DQ].[ValidateSeenByClinician] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.ScheduleTypeID = 1470 --Appointments
and	Encounter.OutcomeCode = 'ATTD'
and	Encounter.SeenByClinicianCode is null
and Encounter.DatasetStartDate < getdate()





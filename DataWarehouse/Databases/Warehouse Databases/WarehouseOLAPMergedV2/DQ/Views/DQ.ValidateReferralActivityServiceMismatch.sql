﻿

CREATE view [DQ].[ValidateReferralActivityServiceMismatch] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
	
from
	DQ.WrkDataset Encounter

where 
	Encounter.DatasetID = 4
and	Encounter.ServiceAllocation <> Encounter.ReferToServiceAllocation

and not exists
	(
	Select 
		1
	from
		DQ.RuleBase
	where
		RuleBase.DatasetID = Encounter.DatasetID
	and	RuleBase.ErrorID = 118
	and Encounter.ServiceAllocation = RuleBase.ServiceAllocation
	and Encounter.ReferToServiceAllocation = RuleBase.ReferToServiceAllocation
	)



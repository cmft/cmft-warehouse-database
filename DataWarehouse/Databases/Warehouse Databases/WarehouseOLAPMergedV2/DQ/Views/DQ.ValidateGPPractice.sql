﻿

CREATE view [DQ].[ValidateGPPractice] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.GPPracticeCode is null
	or Encounter.GPPracticeCode = ''
	or Encounter.GPCode is null
	or Encounter.GPCode =''



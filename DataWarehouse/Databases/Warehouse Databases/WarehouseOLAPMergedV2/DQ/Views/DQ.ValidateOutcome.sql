﻿CREATE view [DQ].[ValidateOutcome] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.OutcomeCode = Encounter.OutcomeCode
	and	RuleBase.ErrorID = 74
	)

﻿



CREATE view [DQ].[Demographic] as

select distinct
	 Base.DatasetRecno
	,Base.DatasetID
	,Base.DistrictNo
	,Base.PatientForename
	,Base.PatientSurname
	,Base.DateOfBirth
	,Base.Postcode
	,Base.RegisteredGpCode
	,Base.RegisteredGpPracticeCode
	,Base.NHSNumber
from
	DQ.Base





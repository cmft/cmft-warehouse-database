﻿create view DQ.ValidateRTTPathwayID as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.RTTPathwayID is null

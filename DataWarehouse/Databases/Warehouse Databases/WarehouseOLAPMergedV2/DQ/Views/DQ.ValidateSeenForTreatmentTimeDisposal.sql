﻿create view DQ.ValidateSeenForTreatmentTimeDisposal as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalAttendanceDisposalCode not in
	(
	 '12' --exclude Did Not Wait, Self Discharge, Transfer
	,'13' --refused tresatment
	)

and	Encounter.SeenForTreatmentTime is null



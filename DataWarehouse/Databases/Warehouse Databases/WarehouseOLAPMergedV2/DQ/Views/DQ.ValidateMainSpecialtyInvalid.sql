﻿CREATE view [DQ].[ValidateMainSpecialtyInvalid] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.MainSpecialtyCode is null

or	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.SpecialtyCode = Encounter.MainSpecialtyCode
	and	RuleBase.ErrorID = 6
	)

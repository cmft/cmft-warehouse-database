﻿create view [DQ].[ValidateDisposalCode] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	(
		Encounter.SourceAttendanceDisposalCode = '-1'
	or	(
			Encounter.SourceAttendanceDisposalCode = '4888'
		and	Encounter.ContextCode = 'TRA||SYM'
		)
	)

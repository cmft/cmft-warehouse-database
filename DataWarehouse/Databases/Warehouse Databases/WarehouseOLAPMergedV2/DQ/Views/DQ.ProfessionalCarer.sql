﻿



create view DQ.[ProfessionalCarer] as


--ID are guaranteed unique as they all pass through the ReferenceMap database
select
	 SourceProfessionalCarerID = Consultant.SourceConsultantID
	,ProfessionalCarer = Consultant.SourceConsultant
from
	WH.Consultant

union all

select
	 SourceProfessionalCarerID = ProfessionalCarer.SourceValueID
	,ProfessionalCarer = ProfessionalCarer.ProfessionalCarer
from
	COM.ProfessionalCarer

union all

select
	 SourceProfessionalCarerID = StaffMember.SourceStaffMemberID
	,ProfessionalCarer = StaffMember.SourceStaffMember
from
	AE.StaffMember





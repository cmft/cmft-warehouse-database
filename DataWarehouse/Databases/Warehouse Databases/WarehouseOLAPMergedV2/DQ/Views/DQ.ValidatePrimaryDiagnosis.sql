﻿create view DQ.ValidatePrimaryDiagnosis as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.PrimaryDiagnosisCode is null
and	Encounter.DischargeDate is not null


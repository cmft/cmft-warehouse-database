﻿create view [DQ].[ValidateRTTStatus] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.RTTStatusCode is null



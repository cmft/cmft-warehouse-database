﻿create view DQ.ValidatePatientName as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	coalesce(upper(Encounter.PatientForename), 'UNKNOWN') = 'UNKNOWN'
or	coalesce(upper(Encounter.PatientSurname), 'UNKNOWN') = 'UNKNOWN'





﻿create view DQ.ValidateIntendedManagement as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.IntendedManagementCode = '-1'

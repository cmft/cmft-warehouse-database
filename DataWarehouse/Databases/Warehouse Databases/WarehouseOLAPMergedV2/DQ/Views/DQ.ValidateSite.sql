﻿create view DQ.ValidateSite as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.SiteCode = '-1'

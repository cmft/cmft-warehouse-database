﻿CREATE view [DQ].[ValidateEthnicCategory] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.NationalEthnicCategoryCode = Encounter.NationalEthnicCategoryCode
	and	RuleBase.ErrorID = 39
	)

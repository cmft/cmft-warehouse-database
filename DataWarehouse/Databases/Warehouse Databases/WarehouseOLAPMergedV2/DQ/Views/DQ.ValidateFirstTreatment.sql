﻿
CREATE view [DQ].[ValidateFirstTreatment] as

--First Treatment Code Missing
select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalAttendanceDisposalCode <> '12' --exclude Did Not Wait, Self Discharge, Transfer

and	not exists
	(
	select
		1
	from
		AE.BaseTreatment Treatment
	where
		Treatment.MergeEncounterRecno = Encounter.DatasetRecno
	and	Treatment.ContextCode = 
		(
		select
			ContextCode
		from
			WH.Context
		where
			Context.ContextID = Encounter.ContextID
		)
	and	Treatment.SequenceNo = 1
	)






﻿create view DQ.ValidateAEDepartmentTypeInvalid as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.DepartmentTypeCode not in
	(
	 '01'
	,'02'
	,'03'
	,'04'
	)


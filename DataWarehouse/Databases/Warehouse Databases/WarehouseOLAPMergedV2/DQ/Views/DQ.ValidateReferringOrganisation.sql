﻿
CREATE view [DQ].[ValidateReferringOrganisation] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

inner join [$(Organisation)].dbo.Gp
on	Gp.OrganisationCode = Encounter.ReferrerCode

inner join [$(Organisation)].dbo.Practice
on	Practice.OrganisationCode = Gp.ParentOrganisationCode

where
	Encounter.ReferredByCode = 'GP'
and	Encounter.ReferrerCode not in
	(
	 'G9999981'
	,'G9999998'
)
and	Practice.ParentOrganisationCode is null


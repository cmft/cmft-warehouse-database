﻿create view DQ.ValidateDepartureTime as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.DepartureTime is null


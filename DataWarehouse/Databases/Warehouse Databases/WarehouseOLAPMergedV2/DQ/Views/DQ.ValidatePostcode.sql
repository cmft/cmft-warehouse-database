﻿
CREATE view [DQ].[ValidatePostcode] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	(
		Encounter.Postcode is null
	or	Encounter.Postcode = ''
	or	Encounter.Postcode = 'ZZ99 3WZ'
	)




﻿

CREATE view [DQ].[ValidateGPPracticeMisMatch] as

select 
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

inner join [$(Organisation)].dbo.Gp GP
on	GP.OrganisationCode = Encounter.GPCode

where 
	Encounter.GPPracticeCode <> GP.ParentOrganisationCode 
and Exists -- ie a valid PracticeCode
		(select 1
		from [$(Organisation)].dbo.Gp Pra
		where 
			Pra.ParentOrganisationCode = Encounter.GPPracticeCode 
		)



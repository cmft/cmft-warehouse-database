﻿
CREATE view [DQ].[FactRowInError] as

select
	 Base.DatasetRecno
	,Base.ContextID
	,Base.DatasetID
	,Base.EncounterDateID
	,Base.ServiceID
	,Base.DirectorateID
	,Base.SiteID
	,Base.ProfessionalCarerID

	,Cases = 1
from
	DQ.Base
where
	exists
	(
	select
		1
	from
		DQ.DatasetValidation
	where
		DatasetValidation.DatasetRecno = Base.DatasetRecno
	and	DatasetValidation.DatasetID = Base.DatasetID
	)



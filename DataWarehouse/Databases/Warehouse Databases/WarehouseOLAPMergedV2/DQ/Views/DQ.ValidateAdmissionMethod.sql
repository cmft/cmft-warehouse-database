﻿CREATE view [DQ].[ValidateAdmissionMethod] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.AdmissionMethodCode = Encounter.AdmissionMethodCode
	and	RuleBase.ErrorID = 104
	)

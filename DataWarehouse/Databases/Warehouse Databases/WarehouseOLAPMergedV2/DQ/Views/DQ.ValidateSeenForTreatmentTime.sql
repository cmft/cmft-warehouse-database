﻿create view DQ.ValidateSeenForTreatmentTime as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalAttendanceDisposalCode <> '12' --exclude Did Not Wait, Self Discharge, Transfer
and	Encounter.SeenForTreatmentTime is null


﻿
CREATE view [DQ].[ValidateDaycaseNotDischarged] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

inner join APC.BaseEncounter Base
on	Base.MergeEncounterRecno = Encounter.DatasetRecno

inner join APC.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.DatasetRecno

inner join APC.PatientClassification
on	PatientClassification.SourcePatientClassificationID = Reference.PatientClassificationID

where
	Base.LOS > 0
and	Reference.StartWardID = Reference.EndWardID
and	PatientClassification.NationalPatientClassificationCode = '2'


﻿create view DQ.ValidateDateOfBirth as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.DateOfBirth is null
or	Encounter.Age > 120

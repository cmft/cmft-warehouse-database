﻿
CREATE view [DQ].[ValidateNeonatalLevelOfCareSpecialty] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

inner join APC.BaseEncounter Base
on	Base.MergeEncounterRecno = Encounter.DatasetRecno

inner join APC.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.DatasetRecno

inner join APC.NeonatalLevelOfCare
on	NeonatalLevelOfCare.SourceNeonatalLevelOfCareID = Reference.NeonatalLevelOfCareID

where
	Base.FirstEpisodeInSpellIndicator = 1
and	Encounter.SpecialtyCode in
	(
	 '810'
	,'990'
	,'422'
	)
and	NeonatalLevelOfCare.NationalNeonatalLevelOfCareCode in
	(
	 '1'
	,'2'
	,'3'
	)


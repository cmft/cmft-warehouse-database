﻿


CREATE view [DQ].[ValidateSeenTime] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.SeenTime is null
and	Encounter.ScheduleTypeID = 1470 --Appointments
and	Encounter.OutcomeCode = 'ATTD'





﻿






CREATE view [DQ].[COMReferral] as

select
	 Encounter.SourceUniqueID
	,Encounter.SourcePatientID
	,SourceReferredBySpecialtyID = Encounter.ReferredBySpecialtyID
	,SourceReferredToSpecialtyID = Encounter.ReferredToSpecialtyID
	,Encounter.SourceofReferralID
	,Encounter.ReferralReceivedTime
	,Encounter.ReferralReceivedDate
	,Encounter.ReasonForReferralID
	,Encounter.CancelledTime
	,Encounter.CancelledDate
	,Encounter.CancelledReasonID
	,Encounter.ReferralSentTime
	,Encounter.ReferralSentDate
	,Encounter.Urgency
	,Encounter.Priority
	,Encounter.AuthorisedTime
	,Encounter.AuthorisedDate
	,Encounter.ReferralClosedTime
	,Encounter.ReferralClosedDate
	,Encounter.ClosureReasonID
	,SourceReferredByProfessionalCarerID = Encounter.ReferredByProfessionalCarerID
	,SourceReferredByStaffTeamID = Encounter.ReferredByStaffTeamID
	,Encounter.ReferredByHealthOrgID
	,SourceReferredToProfessionalCarerID = Encounter.ReferredToProfessionalCarerID
	,Encounter.ReferredToHealthOrgID
	,SourceReferredToStaffTeamID = Encounter.ReferredToStaffTeamID
	,Encounter.OutcomeOfReferral
	,Encounter.ReferralStatus
	,Encounter.RejectionID
	,Encounter.TypeofReferralID
	,Encounter.PatientSourceID
	,Encounter.PatientSourceSystemUniqueID
	,Encounter.PatientNHSNumber
	,Encounter.PatientNHSNumberStatusIndicator
	,Encounter.PatientTitleID
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.PatientPostcode
	,Encounter.PatientDateOfBirth
	,Encounter.PatientDateOfDeath
	,Encounter.PatientSexID
	,Encounter.PatientEthnicGroupID
	,Encounter.RTTStatusDate
	,Encounter.RTTPatientPathwayID
	,Encounter.RTTStartTime
	,Encounter.RTTStartFlag
	,Encounter.RTTStatusID
	,Encounter.CreatedTime
	,Encounter.ModifiedTime
	,ModifiedDate = CAST(Encounter.ModifiedTime as date)
	,SourceCreatedByID = Encounter.CreatedByID
	,SourceModifiedByID = Encounter.ModifiedByID
	,Encounter.ArchiveFlag
	,Encounter.ParentID
	,Encounter.HealthOrgOwner
	,Encounter.RecordStatus
	,Encounter.RequestedServiceID
	,Encounter.ContextCode

	,Reference.MergeEncounterRecno
	,Reference.ReferredBySpecialtyID
	,Reference.ReferredToSpecialtyID
	,Reference.ReferredByProfessionalCarerID
	,Reference.ReferredToProfessionalCarerID
	,Reference.ReferredByStaffTeamID
	,Reference.ReferredToStaffTeamID
	,Reference.CreatedByID
	,Reference.ModifiedByID
from
	COM.BaseReferral Encounter

inner join COM.BaseReferralReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

--where
--	exists
--	(
--	select
--		1
--	from
--		DQ.DatasetValidation
--	where
--		DatasetValidation.DatasetRecno = Reference.EncounterRecno
--	and	DatasetValidation.DatasetID =
--		(
--		select
--			DatasetID
--		from
--			DQ.Dataset
--		where
--			Dataset.DatasetCode = 'COMR'
--		)
--	)








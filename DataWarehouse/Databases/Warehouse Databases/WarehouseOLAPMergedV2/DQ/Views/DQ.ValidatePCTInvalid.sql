﻿create view DQ.ValidatePCTInvalid as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.PCTCode = 'X98'

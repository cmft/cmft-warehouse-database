﻿

create view [DQ].[ValidateNHSNumber] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	(
		Encounter.NHSNumber is null
	or	Encounter.NHSNumber = ''
	)


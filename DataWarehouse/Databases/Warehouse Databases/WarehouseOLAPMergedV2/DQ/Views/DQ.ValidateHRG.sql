﻿CREATE view DQ.ValidateHRG as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.HRGCode is null
and	Encounter.DischargeDate is not null

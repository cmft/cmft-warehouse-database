﻿CREATE view [DQ].[ValidateCancelledReason] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.CancelledReasonCode is null
and	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.OutcomeCode = Encounter.OutcomeCode
	and	RuleBase.ErrorID = 81
	)

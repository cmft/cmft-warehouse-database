﻿
CREATE view [DQ].[FactErrorAPC] as

select
	 FactError.DatasetRecno
	,FactError.DatasetID
	,FactError.ContextID
	,FactError.EncounterDateID
	,FactError.ServiceID
	,FactError.DirectorateID
	,FactError.SiteID
	,FactError.ProfessionalCarerID
	,FactError.ErrorID
	,FactError.Cases
from
	DQ.FactError

inner join DQ.Dataset
on	Dataset.DatasetID = FactError.DatasetID
and	Dataset.DatasetCode = 'APC'


﻿CREATE view [DQ].[ValidateSex] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.SexCode = Encounter.SexCode
	and	RuleBase.ErrorID = 99
	)

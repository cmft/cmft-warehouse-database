﻿create view DQ.ValidateAttendanceConclusionTime as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.AttendanceConclusionTime is null


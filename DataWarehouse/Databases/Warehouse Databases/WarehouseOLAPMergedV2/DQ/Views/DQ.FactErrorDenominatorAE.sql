﻿

create view [DQ].[FactErrorDenominatorAE] as

select
	 Base.DatasetRecno
	,Base.ContextID
	,Base.DatasetID
	,Base.EncounterDateID
	,Base.ServiceID
	,Base.DirectorateID
	,Base.SiteID
	,Base.ProfessionalCarerID

	,DatasetDenominator.DenominatorID
	,DenominatorCases = 1
from
	DQ.Base

inner join DQ.Dataset
on	Dataset.DatasetID = Base.DatasetID
and	Dataset.DatasetCode = 'AE'

inner join DQ.DatasetDenominator
on	DatasetDenominator.DatasetRecno = Base.DatasetRecno
and	DatasetDenominator.DatasetID = Base.DatasetID





﻿

CREATE view [DQ].[ValidateOperationStatusInvalid] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

inner join APC.BaseEncounter Base
on	Base.MergeEncounterRecno = Encounter.DatasetRecno

where
	Encounter.OperationStatusCode = '9'
and	coalesce(
		 Base.PrimaryProcedureCode
		,Base.SecondaryProcedureCode1
		,Base.SecondaryProcedureCode2
		,Base.SecondaryProcedureCode3
		,Base.SecondaryProcedureCode4
		,Base.SecondaryProcedureCode5
		,Base.SecondaryProcedureCode6
		,Base.SecondaryProcedureCode7
		,Base.SecondaryProcedureCode8
		,Base.SecondaryProcedureCode9
		,Base.SecondaryProcedureCode10
		,Base.SecondaryProcedureCode11
	) is not null



﻿

CREATE view [DQ].[ValidateGPPracticeInvalid] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

left join [$(Organisation)].dbo.Gp
on	Gp.OrganisationCode = Encounter.GPCode

where
	Encounter.GPPracticeCode <> Gp.ParentOrganisationCode
and	(
		(
			Encounter.GPPracticeCode in
			(
			 'V81997'
			,'V81998'
			,'V81999'
			)
		and	Encounter.GPCode not in
			(
			 'G9999998'
			,'G9999981'
			)
		)
	or	(
			Encounter.GPPracticeCode not in
			(
			 'V81997'
			,'V81998'
			,'V81999'
			)
		and	Encounter.GPCode is not null
		)
	)
or
	Encounter.GPCode in
	(
	 'G9999998'
	,'G9999981'
	,'G0'
	)





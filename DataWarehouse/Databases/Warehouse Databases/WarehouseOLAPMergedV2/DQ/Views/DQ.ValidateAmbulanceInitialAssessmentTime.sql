﻿CREATE view DQ.ValidateAmbulanceInitialAssessmentTime as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalArrivalModeCode = '1'
and	Encounter.InitialAssessmentTime is null




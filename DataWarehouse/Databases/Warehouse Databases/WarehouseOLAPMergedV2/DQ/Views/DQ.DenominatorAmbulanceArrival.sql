﻿create view [DQ].[DenominatorAmbulanceArrival] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.NationalArrivalModeCode = '1'



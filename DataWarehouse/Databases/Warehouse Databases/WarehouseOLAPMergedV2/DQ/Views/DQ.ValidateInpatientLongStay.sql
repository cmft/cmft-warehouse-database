﻿create view DQ.ValidateInpatientLongStay as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	DATEDIFF(
		 month
		,Encounter.AdmissionDate
		,Encounter.DischargeDate
	) > 12

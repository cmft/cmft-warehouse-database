﻿create view DQ.ValidateCommissioner as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.CommissionerCode is null

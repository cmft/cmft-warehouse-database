﻿create view DQ.ValidatePurchaserInvalid as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

inner join DQ.RuleBase
on	Encounter.PurchaserCode = RuleBase.PurchaserCode
and	Encounter.ContextCode like RuleBase.SourceContextCode + '%'


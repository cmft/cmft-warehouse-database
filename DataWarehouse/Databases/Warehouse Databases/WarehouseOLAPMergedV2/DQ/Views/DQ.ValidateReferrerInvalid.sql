﻿


CREATE view [DQ].[ValidateReferrerInvalid] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter

left join WH.Consultant
on	cast(Consultant.SourceConsultantID as varchar) = Encounter.ReferrerCode
and	Encounter.ReferredByCode = 'CON'

where
	Encounter.ReferrerCode is not null

and	(
		(
			Encounter.ReferredByCode = 'GP'
		and	not exists
			(
			select
				1
			from
				[$(Organisation)].dbo.Gp
			where
				Gp.OrganisationCode = Encounter.ReferrerCode
			)
		)

		or

		(
			Encounter.ReferredByCode = 'GDP'
		and	not exists
			(
			select
				1
			from
				[$(Organisation)].dbo.DGp
			where
				DGp.OrganisationCode = Encounter.ReferrerCode
			)
		)

		or

		(
			Encounter.ReferredByCode = 'CON'
		and	not exists
			(
			select
				1
			from
				WH.Consultant
			where
				cast(Consultant.SourceConsultantID as varchar) = Encounter.ReferrerCode
			)

		)

		or

		(
			Encounter.ReferredByCode = 'CON'
		and	Consultant.NationalConsultantCode not like '%C_______'
		and	Consultant.NationalConsultantCode not like '%D_______'
		and	Consultant.NationalConsultantCode not like '%H_______'
		and	Consultant.NationalConsultantCode not like '%N_______'

		)
	)
 



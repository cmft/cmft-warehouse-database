﻿CREATE view [DQ].[ValidateAdmissionSource] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.AdmissionSourceCode = Encounter.AdmissionSourceCode
	and	RuleBase.ErrorID = 103
	)

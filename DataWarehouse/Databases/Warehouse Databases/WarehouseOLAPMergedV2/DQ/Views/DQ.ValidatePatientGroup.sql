﻿create view [DQ].[ValidatePatientGroup] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.SiteCode not in
	(
	 'RW3RE'
	,'RW3SM'
	)
and	Encounter.NationalAttendanceCategoryCode = '1' --First Accident And Emergency Attendance
and	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.PatientGroupCode = Encounter.PatientGroupCode
	and	Encounter.ContextCode like coalesce(RuleBase.SourceContextCode, Encounter.ContextCode) + '%'
	and	RuleBase.ErrorID = 37
	)


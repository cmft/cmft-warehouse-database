﻿CREATE view [DQ].[ValidateDischargeDestination] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	exists
	(
	select
		1
	from
		DQ.RuleBase
	where
		RuleBase.DischargeDestinationCode = Encounter.DischargeDestinationCode
	and	RuleBase.ErrorID = 106
	)

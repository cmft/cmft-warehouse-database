﻿create view [DQ].[ValidateGPInvalid] as

select
	 Encounter.DatasetID
	,Encounter.DatasetRecno
from
	DQ.WrkDataset Encounter
where
	Encounter.GPCode in
	(
	 'G9999998'
	,'G9999981'
	,'G0'
	)

or	Encounter.GPCode is null

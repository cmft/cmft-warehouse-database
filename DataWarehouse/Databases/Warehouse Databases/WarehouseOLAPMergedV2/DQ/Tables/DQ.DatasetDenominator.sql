﻿CREATE TABLE [DQ].[DatasetDenominator] (
    [DatasetID]     SMALLINT NOT NULL,
    [DatasetRecno]  BIGINT   NOT NULL,
    [DenominatorID] INT      NOT NULL,
    CONSTRAINT [PK_DatasetDenominator_1] PRIMARY KEY NONCLUSTERED ([DatasetID] ASC, [DatasetRecno] ASC, [DenominatorID] ASC),
    CONSTRAINT [FK_DatasetDenominator_Dataset] FOREIGN KEY ([DatasetID]) REFERENCES [DQ].[Dataset] ([DatasetID])
);


﻿CREATE TABLE [DQ].[WrkValidate] (
    [DatasetID]    SMALLINT NOT NULL,
    [DatasetRecno] BIGINT   NOT NULL,
    [ErrorID]      INT      NOT NULL,
    CONSTRAINT [PK_WrkValidate_1] PRIMARY KEY NONCLUSTERED ([DatasetID] ASC, [DatasetRecno] ASC, [ErrorID] ASC)
);


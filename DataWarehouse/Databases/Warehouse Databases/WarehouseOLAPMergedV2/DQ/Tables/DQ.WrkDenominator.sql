﻿CREATE TABLE [DQ].[WrkDenominator] (
    [DatasetID]     INT NOT NULL,
    [DatasetRecno]  INT NOT NULL,
    [DenominatorID] INT NOT NULL,
    CONSTRAINT [PK_WrkDenominator] PRIMARY KEY NONCLUSTERED ([DatasetID] ASC, [DatasetRecno] ASC, [DenominatorID] ASC)
);


﻿CREATE TABLE [DQ].[TargetFinancialMonth] (
    [FinancialMonthKey] INT        NOT NULL,
    [ErrorID]           INT        NOT NULL,
    [MonthlyTarget]     FLOAT (53) NOT NULL,
    CONSTRAINT [PK_TargetFinancialMonth] PRIMARY KEY CLUSTERED ([FinancialMonthKey] ASC, [ErrorID] ASC),
    CONSTRAINT [FK_TargetFinancialMonth_Error] FOREIGN KEY ([ErrorID]) REFERENCES [DQ].[Error] ([ErrorID])
);


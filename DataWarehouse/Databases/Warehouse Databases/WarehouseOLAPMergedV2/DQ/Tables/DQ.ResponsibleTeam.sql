﻿CREATE TABLE [DQ].[ResponsibleTeam] (
    [ResponsibleTeamID] INT           NOT NULL,
    [ResponsibleTeam]   VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_DQ_ResponsibleTeam] PRIMARY KEY CLUSTERED ([ResponsibleTeamID] ASC)
);


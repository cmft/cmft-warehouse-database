﻿CREATE TABLE [DQ].[RuleBase] (
    [RuleBaseRecno]              INT           IDENTITY (1, 1) NOT NULL,
    [DatasetID]                  SMALLINT      NULL,
    [ErrorID]                    INT           NULL,
    [SourceContextCode]          VARCHAR (50)  NULL,
    [NationalEthnicCategoryCode] VARCHAR (10)  NULL,
    [PurchaserCode]              VARCHAR (10)  NULL,
    [SexCode]                    VARCHAR (10)  NULL,
    [AdmissionSourceCode]        VARCHAR (10)  NULL,
    [AdmissionMethodCode]        VARCHAR (10)  NULL,
    [DischargeMethodCode]        VARCHAR (10)  NULL,
    [DischargeDestinationCode]   VARCHAR (10)  NULL,
    [SpecialtyCode]              VARCHAR (10)  NULL,
    [OutcomeCode]                VARCHAR (25)  NULL,
    [ScheduleTypeID]             INT           NULL,
    [PatientGroupCode]           VARCHAR (10)  NULL,
    [ServiceAllocation]          VARCHAR (100) NULL,
    [ReferToServiceAllocation]   VARCHAR (100) NULL,
    CONSTRAINT [PK_DQ_RuleBase] PRIMARY KEY CLUSTERED ([RuleBaseRecno] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_DQ_RuleBase_Error] FOREIGN KEY ([ErrorID]) REFERENCES [DQ].[Error] ([ErrorID])
);


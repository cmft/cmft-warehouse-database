﻿CREATE TABLE [DQ].[BaseError] (
    [EncounterRecno] BIGINT NOT NULL,
    [ContextID]      INT    NOT NULL,
    [ErrorID]        INT    NOT NULL,
    CONSTRAINT [PK_DQ_Error] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [ContextID] ASC, [ErrorID] ASC)
);


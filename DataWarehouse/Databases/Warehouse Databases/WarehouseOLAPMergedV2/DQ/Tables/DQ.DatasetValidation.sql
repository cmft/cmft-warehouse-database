﻿CREATE TABLE [DQ].[DatasetValidation] (
    [DatasetID]    SMALLINT NOT NULL,
    [DatasetRecno] BIGINT   NOT NULL,
    [ErrorID]      INT      NOT NULL,
    CONSTRAINT [PK_DatasetValidation_1] PRIMARY KEY NONCLUSTERED ([DatasetID] ASC, [DatasetRecno] ASC, [ErrorID] ASC),
    CONSTRAINT [FK_DatasetValidation_Dataset] FOREIGN KEY ([DatasetID]) REFERENCES [DQ].[Dataset] ([DatasetID])
);


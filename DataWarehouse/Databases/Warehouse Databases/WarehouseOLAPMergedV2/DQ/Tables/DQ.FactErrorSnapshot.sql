﻿CREATE TABLE [DQ].[FactErrorSnapshot] (
    [DateID]              INT      NOT NULL,
    [ContextID]           INT      NOT NULL,
    [DatasetID]           SMALLINT NOT NULL,
    [EncounterDateID]     INT      NOT NULL,
    [ServiceID]           INT      NOT NULL,
    [DirectorateID]       INT      NOT NULL,
    [SiteID]              INT      NOT NULL,
    [ProfessionalCarerID] INT      NOT NULL,
    [ErrorID]             INT      NOT NULL,
    [Cases]               INT      NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX-DQFES-DI]
    ON [DQ].[FactErrorSnapshot]([DatasetID] ASC)
    INCLUDE([DateID]);


GO
CREATE NONCLUSTERED INDEX [IX-DQFESDIDI]
    ON [DQ].[FactErrorSnapshot]([DateID] ASC, [DatasetID] ASC);


﻿CREATE TABLE [DQ].[Error] (
    [ErrorID]           INT           NOT NULL,
    [Error]             VARCHAR (255) NOT NULL,
    [ResponsibleTeamID] INT           NOT NULL,
    [ErrorLogic]        VARCHAR (255) NOT NULL,
    [DenominatorID]     INT           NOT NULL,
    CONSTRAINT [PK_DQ_Error_1] PRIMARY KEY CLUSTERED ([ErrorID] ASC),
    CONSTRAINT [FK_Error_Denominator] FOREIGN KEY ([DenominatorID]) REFERENCES [DQ].[Denominator] ([DenominatorID]),
    CONSTRAINT [FK_Error_ResponsibleTeam] FOREIGN KEY ([ResponsibleTeamID]) REFERENCES [DQ].[ResponsibleTeam] ([ResponsibleTeamID])
);


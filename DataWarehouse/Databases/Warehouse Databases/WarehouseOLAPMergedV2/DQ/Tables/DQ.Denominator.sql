﻿CREATE TABLE [DQ].[Denominator] (
    [DenominatorID]    INT           IDENTITY (1, 1) NOT NULL,
    [Denominator]      VARCHAR (255) NOT NULL,
    [DenominatorLogic] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_DQ_Denominator_1] PRIMARY KEY CLUSTERED ([DenominatorID] ASC)
);


﻿CREATE TABLE [DQ].[FactRowInErrorSnapshot] (
    [DateID]              INT      NOT NULL,
    [ContextID]           INT      NOT NULL,
    [DatasetID]           SMALLINT NOT NULL,
    [EncounterDateID]     INT      NOT NULL,
    [ServiceID]           INT      NOT NULL,
    [DirectorateID]       INT      NOT NULL,
    [SiteID]              INT      NOT NULL,
    [ProfessionalCarerID] INT      NOT NULL,
    [Cases]               INT      NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX-FRIESDI]
    ON [DQ].[FactRowInErrorSnapshot]([DatasetID] ASC)
    INCLUDE([DateID]);


GO
CREATE NONCLUSTERED INDEX [IX-FRIESDIDI]
    ON [DQ].[FactRowInErrorSnapshot]([DateID] ASC, [DatasetID] ASC);


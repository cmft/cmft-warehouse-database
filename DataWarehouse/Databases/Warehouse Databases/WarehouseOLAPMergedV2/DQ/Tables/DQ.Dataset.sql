﻿CREATE TABLE [DQ].[Dataset] (
    [DatasetID]   SMALLINT      IDENTITY (1, 1) NOT NULL,
    [DatasetCode] VARCHAR (10)  NOT NULL,
    [Dataset]     VARCHAR (255) NULL,
    CONSTRAINT [PK_DQ_Dataset] PRIMARY KEY CLUSTERED ([DatasetID] ASC) WITH (FILLFACTOR = 90)
);


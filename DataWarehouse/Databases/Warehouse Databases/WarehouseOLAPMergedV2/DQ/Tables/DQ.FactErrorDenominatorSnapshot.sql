﻿CREATE TABLE [DQ].[FactErrorDenominatorSnapshot] (
    [DateID]              INT      NOT NULL,
    [ContextID]           INT      NOT NULL,
    [DatasetID]           SMALLINT NOT NULL,
    [EncounterDateID]     INT      NOT NULL,
    [ServiceID]           INT      NOT NULL,
    [DirectorateID]       INT      NOT NULL,
    [SiteID]              INT      NOT NULL,
    [ProfessionalCarerID] INT      NOT NULL,
    [DenominatorID]       INT      NOT NULL,
    [DenominatorCases]    INT      NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX-DQFEDSDI]
    ON [DQ].[FactErrorDenominatorSnapshot]([DatasetID] ASC)
    INCLUDE([DateID]);


GO
CREATE NONCLUSTERED INDEX [IX-DQDFDSDIDI>]
    ON [DQ].[FactErrorDenominatorSnapshot]([DateID] ASC, [DatasetID] ASC);


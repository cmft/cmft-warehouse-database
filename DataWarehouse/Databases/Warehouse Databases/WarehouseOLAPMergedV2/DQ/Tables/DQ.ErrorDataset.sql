﻿CREATE TABLE [DQ].[ErrorDataset] (
    [ErrorID]   INT      NOT NULL,
    [DatasetID] SMALLINT NOT NULL,
    [Active]    BIT      NOT NULL,
    CONSTRAINT [PK_TemplateDataset] PRIMARY KEY CLUSTERED ([ErrorID] ASC, [DatasetID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ErrorDataset_Dataset] FOREIGN KEY ([DatasetID]) REFERENCES [DQ].[Dataset] ([DatasetID]),
    CONSTRAINT [FK_ErrorDataset_Error] FOREIGN KEY ([ErrorID]) REFERENCES [DQ].[Error] ([ErrorID]) ON UPDATE CASCADE
);


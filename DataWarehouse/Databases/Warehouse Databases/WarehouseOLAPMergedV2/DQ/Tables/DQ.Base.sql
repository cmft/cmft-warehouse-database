﻿CREATE TABLE [DQ].[Base] (
    [DatasetRecno]             INT           NOT NULL,
    [ContextID]                INT           NOT NULL,
    [DatasetID]                SMALLINT      NOT NULL,
    [EncounterDateID]          INT           NOT NULL,
    [ServiceID]                INT           NOT NULL,
    [DirectorateID]            INT           NOT NULL,
    [SiteID]                   INT           NOT NULL,
    [ProfessionalCarerID]      INT           NOT NULL,
    [Cases]                    INT           NOT NULL,
    [DistrictNo]               VARCHAR (50)  NULL,
    [PatientForename]          VARCHAR (100) NULL,
    [PatientSurname]           VARCHAR (100) NULL,
    [DateOfBirth]              DATE          NULL,
    [Postcode]                 VARCHAR (20)  NULL,
    [RegisteredGpCode]         VARCHAR (20)  NULL,
    [RegisteredGpPracticeCode] VARCHAR (20)  NULL,
    [NHSNumber]                VARCHAR (20)  NULL,
    CONSTRAINT [PK_DatasetError] PRIMARY KEY CLUSTERED ([DatasetRecno] ASC, [ContextID] ASC, [DatasetID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DQ_Base_1]
    ON [DQ].[Base]([DatasetID] ASC)
    INCLUDE([DatasetRecno], [ContextID]);


GO
CREATE NONCLUSTERED INDEX [IX-DQBDI]
    ON [DQ].[Base]([DatasetID] ASC)
    INCLUDE([DatasetRecno], [ContextID], [EncounterDateID], [ServiceID], [DirectorateID], [SiteID], [ProfessionalCarerID]);


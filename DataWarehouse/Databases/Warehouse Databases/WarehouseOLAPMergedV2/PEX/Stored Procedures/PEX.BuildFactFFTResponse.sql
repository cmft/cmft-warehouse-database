﻿
CREATE procedure [PEX].[BuildFactFFTResponse] as


declare
       @StartTime datetime = getdate()
       ,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
       ,@Elapsed int
       ,@Stats varchar(max)

truncate table PEX.FactFFTResponse

insert into PEX.FactFFTResponse
(
	MergeResponseRecno
	,AnswerID
	,IsDischargeID
	,SurveyID
	,LocationID
	,SurveyDateID
	,DirectorateID
	,Responses 
)
select 
	BaseFFTResponseReference.MergeResponseRecno
	,BaseFFTResponseReference.AnswerID
	,IsDischargeID =
		case
		when Survey.Channel in ('Online','Postcard','SMS Text') then 1
		when (Survey.Channel in ('Kiosk') and DischargeAnswer.LocalAnswer in ('Not Applicable')) then 1
		when (Survey.Channel in ('Bed Unit') and Survey.SourceSurvey  in ('Discharge Survey')) then 1
		when Survey.SourceSurveyCode in ('422','423','456','492','505','522') and ResponseTime >= '1 apr 2015' then 1 --Daycase Tablet Survey dont have a discharge question
		when (BaseFFTResponse.ContextCode in ('CMFT||CRT') and BaseFFTResponse.LocationID in ('105255')) then 1
		when DischargeAnswer.LocalAnswer = 'Yes' then 1
		when DischargeAnswer.LocalAnswer = 'No' then 2
		when DischargeAnswer.LocalAnswer = 'Don''t Know' then 3
		else 4 --Not Applicable
		end
		
	,BaseFFTResponseReference.SurveyID
	,BaseFFTResponseReference.LocationID
	,BaseFFTResponseReference.SurveyDateID
	,DirectorateID = Directorate.DirectorateID
	,Responses = 1
from
	PEX.BaseFFTResponseReference

inner join PEX.BaseFFTResponse
on	BaseFFTResponseReference.MergeResponseRecno = BaseFFTResponse.MergeResponseRecno

inner join WH.Directorate Directorate
on	Directorate.DirectorateCode = BaseFFTResponse.DirectorateCode

inner join WH.Answer DischargeAnswer
on	DischargeAnswer.SourceAnswerID = BaseFFTResponseReference.IsDischargeID

inner join WH.Survey 
on	Survey.SourceSurveyID = BaseFFTResponseReference.SurveyID
and Survey.SourceContextCode = BaseFFTResponse.ContextCode


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats = 'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
       @ProcedureName
       ,@Stats


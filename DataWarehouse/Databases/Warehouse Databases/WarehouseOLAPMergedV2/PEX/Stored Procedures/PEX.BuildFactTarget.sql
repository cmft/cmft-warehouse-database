﻿
CREATE procedure [PEX].[BuildFactTarget] as

truncate table PEX.FactTarget;

insert into PEX.FactTarget
(
LocationTypeID
,DateID
,Value
)

select 
	LocationTypeID
	,DateID
	,Value = case	when Calendar.FirstDayOfMonth = '1 mar 2015' and LocationTypeCode = '2' --IP
					then 0.4
					when Calendar.FirstDayOfQuarter = '1 jan 2015' and LocationTypeCode = '2' --IP
					then 0.3
					when Calendar.FirstDayOfQuarter = '1 jan 2015' and LocationTypeCode = '1' --AE
					then 0.2
					when Calendar.FinancialYear = '2014/2015' and LocationTypeCode = '2' --IP
					then 0.25
					when Calendar.FinancialYear = '2014/2015' and LocationTypeCode = '1' --AE
					then 0.15	
					when Calendar.FinancialYear = '2015/2016' and LocationTypeCode = '2' --IP
					then 0.2
					when Calendar.FinancialYear = '2015/2016' and LocationTypeCode = '1' --AE
					then 0.1
					else 0.2 --current agreed target for all
					end			
from
	WH.LocationTypeBase
  
inner join WH.Calendar
on Calendar.TheDate > '1 apr 2009'



﻿
CREATE proc [PEX].[BuildFactSample] 

as

declare @FFTExclusionReasonNotApplicableID int =
	(
	select
		AllocationID
	from
		Allocation.Allocation
	where
		Allocation.SourceAllocationID = '-1'
	and	Allocation.AllocationTypeID = 15
	)

truncate table PEX.FactSample;

with DischargeMetric
	(
	 MetricID
	)
as
	(
	select
		 MetricID
	from
		APC.Metric
	where
		MetricCode in ('DIS')

	union all

	select
		 Metric.MetricID
	from
		APC.Metric

	inner join DischargeMetric
	on	DischargeMetric.MetricID = Metric.MetricParentID

	)

insert into PEX.FactSample
(
MergeEncounterRecno 
,LocationID
,ActivityDateID
,IsEligible
,FFTExclusionReasonID
,DirectorateID
,Cases
)

select --top 100
	FactEncounter.MergeEncounterRecno 
	,LocationID = BaseEncounterReference.EndLocationID 
	,FactEncounter.EncounterDateID
	,IsEligible = 
				case
				when FFTExclusionReason.AllocationID = @FFTExclusionReasonNotApplicableID
				then 1
				else 0
				end
	,FFTExclusionReasonID = FFTExclusionReason.AllocationID
	--,DirectorateID = APC.FactEncounter.EndDirectorateID
	,DirectorateID = Directorate.DirectorateID -- DG 9.1.15 - Now points at Directorate associated with ward rather than patient
	,FactEncounter.Cases
from
	DischargeMetric

inner join APC.FactEncounter
on	FactEncounter.MetricID = DischargeMetric.MetricID

inner join APC.BaseEncounterReference -- just until the location is in the fact table 
on	FactEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join APC.BaseEncounter
on	FactEncounter.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join APC.BaseWardStay
on	BaseWardStay.ProviderSpellNo = BaseEncounter.ProviderSpellNo
and	BaseWardStay.ContextCode = BaseEncounter.ContextCode
and not exists
			(
			select
				1
			from
				APC.BaseWardStay Later
			where
				Later.ProviderSpellNo = BaseWardStay.ProviderSpellNo
			and	Later.ContextCode = BaseWardStay.ContextCode
			and	Later.StartTime > BaseWardStay.StartTime
			)

left join WH.Directorate -- DG 9.1.15 - Should be inner but changed for performance reasons (again...)
on	BaseWardStay.DirectorateCode = Directorate.DirectorateCode
				
inner join Allocation.DatasetAllocation FFTExclusionReason
on	FFTExclusionReason.AllocationTypeID = 15
and	FFTExclusionReason.DatasetCode = 'APC'
and	FFTExclusionReason.DatasetRecno = FactEncounter.MergeEncounterRecno


where
	not exists
	(
	select
		1
	from
		APC.Metric Parent
	where
		Parent.MetricParentID = DischargeMetric.MetricID
	)

union all

select
	FactEncounter.MergeEncounterRecno 
	,LocationID = BaseEncounterReference.LocationID 
	,FactEncounter.EncounterStartDateID
	,IsEligible = 
				case
				when FFTExclusionReason.AllocationID = @FFTExclusionReasonNotApplicableID
				then 1
				else 0
				end
	,FFTExclusionReasonID = FFTExclusionReason.AllocationID
	,DirectorateID = WHDirectorate.DirectorateID
	,FactEncounter.Cases
from
	AE.FactEncounter

inner join AE.BaseEncounterReference -- just until the location is in the fact table 
on	FactEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join AE.Stage
on	Stage.StageID = FactEncounter.StageID
and	Stage.StageCode = 'INDEPARTMENTADJUSTED'

inner join Allocation.DatasetAllocation FFTExclusionReason
on	FFTExclusionReason.AllocationTypeID = 15
and	FFTExclusionReason.DatasetCode = 'AE'
and	FFTExclusionReason.DatasetRecno = FactEncounter.MergeEncounterRecno

inner join Allocation.DatasetAllocation Directorate
on	Directorate.AllocationTypeID = 10
and	Directorate.DatasetCode = 'AE'
and	Directorate.DatasetRecno = FactEncounter.MergeEncounterRecno

inner join
	Allocation.Allocation
on	Directorate.AllocationID = Allocation.AllocationID
and Allocation.AllocationTypeID = 10

inner join
	WH.Directorate WHDirectorate
on	WHDirectorate.DirectorateCode = Allocation.SourceAllocationID


where
	FactEncounter.Reportable = 1


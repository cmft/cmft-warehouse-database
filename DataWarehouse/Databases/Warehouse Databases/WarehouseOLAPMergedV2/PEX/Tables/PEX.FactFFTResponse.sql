﻿CREATE TABLE [PEX].[FactFFTResponse] (
    [MergeResponseRecno] INT NOT NULL,
    [AnswerID]           INT NOT NULL,
    [IsDischargeID]      INT NOT NULL,
    [SurveyID]           INT NOT NULL,
    [LocationID]         INT NOT NULL,
    [SurveyDateID]       INT NOT NULL,
    [DirectorateID]      INT NOT NULL,
    [Responses]          INT NOT NULL
);


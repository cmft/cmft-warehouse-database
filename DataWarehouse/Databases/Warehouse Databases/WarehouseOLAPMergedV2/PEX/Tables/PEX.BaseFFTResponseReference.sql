﻿CREATE TABLE [PEX].[BaseFFTResponseReference] (
    [MergeResponseRecno] INT          NOT NULL,
    [AnswerID]           INT          NOT NULL,
    [IsDischargeID]      INT          NOT NULL,
    [SurveyID]           INT          NOT NULL,
    [LocationID]         INT          NOT NULL,
    [SurveyDateID]       INT          NOT NULL,
    [ResponseDateID]     INT          NOT NULL,
    [ContextID]          INT          NOT NULL,
    [Created]            DATETIME     NULL,
    [Updated]            DATETIME     NULL,
    [ByWhom]             VARCHAR (50) NULL
);


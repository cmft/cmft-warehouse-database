﻿CREATE TABLE [PEX].[BaseResponseReference] (
    [MergeResponseRecno] INT          NOT NULL,
    [ContextID]          INT          NOT NULL,
    [SiteID]             INT          NOT NULL,
    [WardID]             INT          NOT NULL,
    [SurveyID]           INT          NULL,
    [SurveyDateID]       INT          NULL,
    [LocationID]         BIGINT       NULL,
    [QuestionID]         BIGINT       NOT NULL,
    [AnswerID]           BIGINT       NULL,
    [ChannelID]          INT          NULL,
    [Created]            DATETIME     NULL,
    [Updated]            DATETIME     NULL,
    [ByWhom]             VARCHAR (50) NULL
);


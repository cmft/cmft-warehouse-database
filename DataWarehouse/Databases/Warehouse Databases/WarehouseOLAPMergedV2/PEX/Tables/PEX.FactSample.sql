﻿CREATE TABLE [PEX].[FactSample] (
    [MergeEncounterRecno]  INT NOT NULL,
    [LocationID]           INT NOT NULL,
    [ActivityDateID]       INT NOT NULL,
    [IsEligible]           BIT NULL,
    [FFTExclusionReasonID] INT NOT NULL,
    [DirectorateID]        INT NULL,
    [Cases]                INT NOT NULL
);


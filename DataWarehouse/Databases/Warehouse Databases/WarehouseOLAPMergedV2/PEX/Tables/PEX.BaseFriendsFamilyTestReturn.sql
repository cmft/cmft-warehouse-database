﻿CREATE TABLE [PEX].[BaseFriendsFamilyTestReturn] (
    [MergeFFTRecno]             INT           IDENTITY (1, 1) NOT NULL,
    [FFTRecno]                  INT           NOT NULL,
    [Return]                    VARCHAR (2)   NULL,
    [CensusDate]                DATE          NULL,
    [HospitalSiteCode]          NVARCHAR (15) NULL,
    [WardCode]                  NVARCHAR (15) NULL,
    [DirectorateCode]           VARCHAR (5)   NULL,
    [1ExtremelyLikely]          INT           NULL,
    [2Likely]                   INT           NULL,
    [3NeitherLikelyNorUnlikely] INT           NULL,
    [4Unlikely]                 INT           NULL,
    [5ExtremelyUnlikely]        INT           NULL,
    [6DontKnow]                 INT           NULL,
    [Responses]                 INT           NULL,
    [EligibleResponders]        INT           NULL,
    [InterfaceCode]             VARCHAR (10)  NULL,
    [Created]                   DATETIME      NULL,
    [Updated]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (50)  NULL,
    [ContextCode]               VARCHAR (15)  NULL,
    PRIMARY KEY CLUSTERED ([MergeFFTRecno] ASC)
);


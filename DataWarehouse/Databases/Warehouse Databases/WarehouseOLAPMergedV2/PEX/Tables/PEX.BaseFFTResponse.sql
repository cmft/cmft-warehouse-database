﻿CREATE TABLE [PEX].[BaseFFTResponse] (
    [MergeResponseRecno] INT           NOT NULL,
    [Question]           VARCHAR (MAX) NULL,
    [AnswerID]           BIGINT        NOT NULL,
    [FollowupAnswer]     VARCHAR (MAX) NULL,
    [IsDischargeID]      BIGINT        NULL,
    [SurveyID]           BIGINT        NULL,
    [LocationID]         BIGINT        NOT NULL,
    [DirectorateCode]    VARCHAR (10)  NULL,
    [SurveyTime]         DATETIME      NOT NULL,
    [ResponseTime]       DATETIME      NULL,
    [ContextCode]        VARCHAR (20)  NOT NULL,
    [Created]            DATETIME      NULL,
    [Updated]            DATETIME      NULL,
    [ByWhom]             VARCHAR (50)  NULL
);


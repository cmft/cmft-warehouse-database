﻿CREATE TABLE [PEX].[BaseFriendsFamilyTestReturnReference] (
    [MergeFFTRecno] INT          NOT NULL,
    [ContextID]     INT          NOT NULL,
    [SiteID]        INT          NOT NULL,
    [WardID]        INT          NOT NULL,
    [CensusDateID]  INT          NULL,
    [Created]       DATETIME     NULL,
    [Updated]       DATETIME     NULL,
    [ByWhom]        VARCHAR (50) NULL
);


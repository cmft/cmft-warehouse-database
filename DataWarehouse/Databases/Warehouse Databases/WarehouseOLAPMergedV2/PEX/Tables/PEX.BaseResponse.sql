﻿CREATE TABLE [PEX].[BaseResponse] (
    [MergeResponseRecno] INT           IDENTITY (1, 1) NOT NULL,
    [ResponseRecno]      INT           NULL,
    [SurveyTakenID]      BIGINT        NULL,
    [SurveyID]           INT           NULL,
    [QuestionID]         BIGINT        NOT NULL,
    [AnswerID]           BIGINT        NULL,
    [Answer]             VARCHAR (MAX) NULL,
    [SurveyDate]         DATE          NOT NULL,
    [SurveyTime]         DATETIME      NOT NULL,
    [DirectorateCode]    VARCHAR (10)  NULL,
    [LocationID]         BIGINT        NULL,
    [WardID]             BIGINT        NULL,
    [ResponseDate]       DATE          NULL,
    [ResponseTime]       DATETIME      NULL,
    [ChannelID]          INT           NULL,
    [QuestionTypeID]     INT           NULL,
    [LocationTypeID]     INT           NULL,
    [ContextCode]        VARCHAR (20)  NOT NULL,
    [Created]            DATETIME      NULL,
    [Updated]            DATETIME      NULL,
    [ByWhom]             VARCHAR (50)  NULL
);


﻿create view PEX.IsDischarge as

select
	 IsDischargeID = 1
	,IsDischarge = 'Yes'

union 

select
	 IsDischargeID = 2
	,IsDischarge = 'No'

union 

select
	 IsDischargeID = 3
	,IsDischarge = 'Not Known'

union 

select
	 IsDischargeID = 4
	,IsDischarge = 'Not Applicable'


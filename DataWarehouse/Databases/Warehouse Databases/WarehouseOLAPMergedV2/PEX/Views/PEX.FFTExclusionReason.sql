﻿
create view [PEX].[FFTExclusionReason] as

select
	 FFTExclusionReasonID = Allocation.AllocationID
	,SourceFFTExclusionReasonID = Allocation.SourceAllocationID
	,FFTExclusionReason = Allocation.Allocation
from
	Allocation.Allocation

where
	Allocation.AllocationTypeID = 15





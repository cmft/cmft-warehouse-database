﻿



CREATE view [PEX].[Answer] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAnswerID = SourceValueID
	,SourceAnswerCode = SourceValueCode
	,SourceAnswer = SourceValue
	,LocalAnswerID = LocalValueID
	,LocalAnswerCode = LocalValueCode
	,LocalAnswer = LocalValue
	,NationalAnswerID = NationalValueID
	,NationalAnswerCode = NationalValueCode
	,NationalAnswer = NationalValue
	,NetPromoterAnswerID = OtherValueID
	,NetPromoterAnswerCode = OtherValueCode
	,NetPromoterAnswer = OtherValue		
FROM
	WH.Member
where
	AttributeCode = 'ANSWER'





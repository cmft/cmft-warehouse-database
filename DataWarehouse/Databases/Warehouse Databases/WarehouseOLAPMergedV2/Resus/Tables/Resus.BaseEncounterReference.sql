﻿CREATE TABLE [Resus].[BaseEncounterReference] (
    [MergeRecno]         INT          NOT NULL,
    [ContextID]          INT          NULL,
    [AdmissionReasonID]  INT          NULL,
    [AdmissionDateID]    INT          NULL,
    [PresentingECGID]    INT          NULL,
    [CallOutReasonID]    INT          NULL,
    [CallOutDateID]      INT          NULL,
    [LocationID]         INT          NULL,
    [RespondingTeamID]   INT          NULL,
    [OutcomeID]          INT          NULL,
    [OutcomeDateID]      INT          NULL,
    [FinalOutcomeID]     INT          NULL,
    [FinalOutcomeDateID] INT          NULL,
    [AuditTypeID]        INT          NULL,
    [Created]            DATETIME     NULL,
    [Updated]            DATETIME     NULL,
    [ByWhom]             VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseEnco__A2E2104462E70C41] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


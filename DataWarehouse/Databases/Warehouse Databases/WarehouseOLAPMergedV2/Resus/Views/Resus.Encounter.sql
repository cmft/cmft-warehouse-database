﻿
create view Resus.Encounter as 

select 
	MergeRecno
	,SourcePatientNo
	,DistrictNo
	,NHSNumber
	,DateOfBirth
	,AdmissionDate
	,Comments
	,CallOutTime
	,OutcomeTime
	,FinalOutcomeTime
from
	Resus.BaseEncounter
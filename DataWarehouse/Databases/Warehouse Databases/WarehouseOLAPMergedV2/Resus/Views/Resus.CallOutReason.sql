﻿



CREATE view Resus.CallOutReason as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceCallOutReasonID = SourceValueID
	,SourceCallOutReasonCode = SourceValueCode
	,SourceCallOutReason = SourceValue
	,LocalCallOutReasonID = LocalValueID
	,LocalCallOutReasonCode = LocalValueCode
	,LocalCallOutReason = LocalValue
	,NationalCallOutReasonID = NationalValueID
	,NationalCallOutReasonCode = NationalValueCode
	,NationalCallOutReason = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'CALLOUTREASON'





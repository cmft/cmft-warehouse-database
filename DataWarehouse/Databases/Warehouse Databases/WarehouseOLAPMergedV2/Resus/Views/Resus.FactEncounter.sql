﻿






CREATE view [Resus].[FactEncounter] as 

select
	BaseEncounterReference.MergeRecno
	,BaseEncounterReference.AdmissionReasonID
	,AdmissionDateID
	--,PresentingECGID
	,BaseEncounterReference.CallOutReasonID
	,CallOutDateID
	,CallOutTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BaseEncounter.CallOutTime), 0)
							,BaseEncounter.CallOutTime
						)
						,-1
					)
	,BaseEncounterReference.LocationID
	--,RespondingTeamID
	--,OutcomeID
	--,OutcomeTimeID
	--,FinalOutcomeID
	--,FinalOutcomeTimeID
	,BaseEncounterReference.AuditTypeID
	,OutOfHours = 
				cast(
					case
					when datename(dw, BaseEncounter.CallOutTime) in ('Saturday', 'Sunday') then 1
					when cast(BaseEncounter.CallOutTime as time) < '8:00' then 1
					when cast(BaseEncounter.CallOutTime as time) > '17:00' then 1
					when exists
						(
						select
							1
						from
							[$(Warehouse)].WH.Holiday
						where
							cast(BaseEncounter.CallOutTime as date) = Holiday.HolidayDate
						)
					then 1
					else 0
					end
				as bit)
	,AgeID = 
				case
				when BaseEncounter.DateOfBirth > BaseEncounter.CallOutTime
				then 0
				when (month(BaseEncounter.CallOutTime) * 100) + day(BaseEncounter.CallOutTime) >= (month(BaseEncounter.DateOfBirth) * 100) + day(BaseEncounter.DateOfBirth)
				then datediff(year, BaseEncounter.DateOfBirth, BaseEncounter.CallOutTime)
				else datediff(year, BaseEncounter.DateOfBirth, BaseEncounter.CallOutTime) - 1
				end	
	,ContextID
	,Cases = 1
from
	Resus.BaseEncounterReference
inner join Resus.BaseEncounter
on	BaseEncounterReference.MergeRecno = BaseEncounter.MergeRecno







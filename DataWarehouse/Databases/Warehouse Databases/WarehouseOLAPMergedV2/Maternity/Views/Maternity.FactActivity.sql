﻿














CREATE view [Maternity].[FactActivity]

as

select
	 MergeEncounterRecno = Base.MergeAntenatalCareRecno
	,EncounterDateID = BaseReference.VisitDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'ANTE'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,DirectorateBase.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,Service.ServiceID
	,HRGID = HRGBase.HRGID
	,BaseReference.ContextID
	,Cases = 1
	,Value = null

from
	Maternity.BaseAntenatalCare Base

inner join Maternity.BaseAntenatalCareReference BaseReference
on	BaseReference.MergeAntenatalCareRecno = Base.MergeAntenatalCareRecno

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = '501'
and	Specialty.SourceContextCode = Base.ContextCode

inner join WH.Service
on	Service.SourceServiceID = 104 --Unassigned, needs allocating

inner join WH.DirectorateBase
on	DirectorateBase.DirectorateCode = '34' --needs allocating

inner join Allocation.Allocation PointOfDelivery
on	PointOfDelivery.SourceAllocationID = 'MAT-Antenatal'
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.Allocation ContractFlag
on	ContractFlag.SourceAllocationID = 'NCBPS00z'
and	ContractFlag.AllocationTypeID = 9


inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 'ANTE'

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	BaseReference.VisitDateID = Calendar.DateID

inner join WH.HRGBase
on	HRGBase.HRGCode = Base.HRGCode


where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 Base.MergePostnatalCareRecno
	,BaseReference.DischargedToHealthVisitorDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'POST'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable = cast(1 as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,DirectorateBase.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,Service.ServiceID
	,HRGID = HRGBase.HRGID
	,BaseReference.ContextID
	,Cases = 1
	,Value = null

from
	Maternity.BasePostnatalCare Base

inner join Maternity.BasePostnatalCareReference BaseReference
on	BaseReference.MergePostnatalCareRecno = Base.MergePostnatalCareRecno

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = '501'
and	Specialty.SourceContextCode = Base.ContextCode

inner join WH.Service
on	Service.SourceServiceID = 104 --Unassigned, needs allocating

inner join WH.DirectorateBase
on	DirectorateBase.DirectorateCode = '34' --needs allocating

inner join Allocation.Allocation PointOfDelivery
on	PointOfDelivery.SourceAllocationID = 'MAT-Postnatal'
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = 'NCBPS00z'
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 'POST'

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	BaseReference.DischargedToHealthVisitorDateID = Calendar.DateID

inner join WH.HRGBase
on	HRGBase.HRGCode = Base.HRGCode

where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 MergeEncounterRecno = Appointment.Contracting_ID
	,EncounterDateID = Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'MAT'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end
					as bit)
	,Reportable = 
			cast(
				case 
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				when PoD_Code in ('NELXBD', 'NELNEXBD', 'ELXBD', 'BPT', 'BPT-UC', 'RT-UB')
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,Specialty.SourceSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Context.ContextID
	,Cases = Activity_Actual
	,Value = Price_Actual
from
	SLAM.Encounter Appointment

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 
		case Appointment.PoD_Code
		when 'Mat-Antenatal' then 'ANTE'
		when 'Mat-Postnatal' then 'POST'
		end

left join 
		(
		select
			AllocationID
			,SourceAllocationID
			,Allocation
			,AllocationTypeID
		from
			Allocation.Allocation
		where
			not exists
					(
					select
						1
					from
						Allocation.Allocation Later
					where
						Later.SourceAllocationID = Allocation.SourceAllocationID
					and	Later.AllocationTypeID = Allocation.AllocationTypeID
					and	Later.AllocationID > Allocation.AllocationID
					)
		) PointOfDelivery
on	PointOfDelivery.SourceAllocationID = Appointment.PoD_Code
and	PointOfDelivery.AllocationTypeID = 5

left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = Appointment.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	Appointment.Spell_Admission_Date = Calendar.TheDate

inner join WH.Directorate
on	Directorate.DirectorateCode = Appointment.Directorate_Code

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = Appointment.Treatment_Function_Code
and	Specialty.SourceContextCode = Appointment.ContextCode

inner join WH.Context
on	Appointment.ContextCode = Context.ContextCode

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(Appointment.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = Appointment.Feed_Name
--and LatestExtractMonth.Extract_Type = Appointment.Extract_Type
--and LatestExtractMonth.Extract_Month = Appointment.Extract_Month
--and LatestExtractMonth.Extract_Year = Appointment.Extract_Year

where
	Appointment.PoD_Code in ('Mat-antenatal', 'Mat-Postnatal')
and	Appointment.Extract_Type = 'Freeze'
--and	Appointment.Is_Current = 1
and	Appointment.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')




















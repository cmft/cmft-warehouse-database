﻿CREATE TABLE [Maternity].[BaseAntenatalCareReference] (
    [MergeAntenatalCareRecno] INT          NOT NULL,
    [ContextID]               INT          NOT NULL,
    [VisitDateID]             INT          NOT NULL,
    [Created]                 DATETIME     NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) NULL,
    CONSTRAINT [PK_Maternity_BaseAntenatalCareReference] PRIMARY KEY CLUSTERED ([MergeAntenatalCareRecno] ASC)
);


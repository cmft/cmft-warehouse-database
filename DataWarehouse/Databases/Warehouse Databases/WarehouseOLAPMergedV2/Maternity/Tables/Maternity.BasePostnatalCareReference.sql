﻿CREATE TABLE [Maternity].[BasePostnatalCareReference] (
    [MergePostnatalCareRecno]         INT          NOT NULL,
    [ContextID]                       INT          NOT NULL,
    [DischargedToHealthVisitorDateID] INT          NOT NULL,
    [Created]                         DATETIME     NULL,
    [Updated]                         DATETIME     NULL,
    [ByWhom]                          VARCHAR (50) NULL,
    CONSTRAINT [PK_Maternity_BasePostnatalCareReference] PRIMARY KEY CLUSTERED ([MergePostnatalCareRecno] ASC)
);


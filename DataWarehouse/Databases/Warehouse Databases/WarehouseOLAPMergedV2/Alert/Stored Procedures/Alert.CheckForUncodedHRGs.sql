﻿ 
CREATE proc [Alert].[CheckForUncodedHRGs]

as

declare @APCUncoded varchar(100),@OPuncoded varchar(100),@AEuncoded varchar(100), @APC_CC_uncoded varchar(100)


if (
select
	distinct 1
from
	APC.Encounter with (nolock)
where
	not exists
		(
		select
			1
		from
			APC.HRG4Encounter  with (nolock)
		where
			Encounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
		)
and DischargeDate >= '1 apr 2012'

) = 1
set @APCUncoded = 'uncoded HRGs for APC'
--	else
--set @APCuncoded = 0
 



if (

 select
     distinct 1
from
      OP.BaseEncounter with(nolock)
where
      not exists
            (
            select
                  1
            from
                  OP.HRG4Encounter with (nolock)
            where
                  BaseEncounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
            )
and AppointmentDate >= '1 apr 2012'

) = 1
set @OPuncoded = 'uncoded HRGs for OP'
--	else
--set @OPuncoded = 0
 












if (
select
	distinct 1
from
	AE.BaseEncounter with (nolock)
where
	not exists
		(
		select
			1
		from
			AE.HRG4Encounter with (nolock)
		where
			BaseEncounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
		)
and ArrivalDate >= '1 apr 2012'
and Reportable = 1

) = 1
set @AEuncoded = 'uncoded HRGs for A&E'
--	else
--set @AEuncoded = 0
 



if (

select
	distinct 1
from
	APC.BaseCriticalCarePeriod with (nolock)
where
	not exists
		(
		select
			1
		from
			APC.HRG4CriticalCareEncounter with (nolock)
		where
			BaseCriticalCarePeriod.MergeEncounterRecno = HRG4CriticalCareEncounter.MergeEncounterRecno
		)
and StartDate >= '1 apr 2012'
and EndDate is not null --CH 31072015 discussion with AR that some patients without a discharge are coming through but need to not go into the grouper

) = 1
set @APC_CC_uncoded = 'Uncoded HRGs for APC Critical Care'
--	else
--set @APC_CC_uncoded = 0
 
  

declare  @codedList table (status varchar(100))

 insert into @codedList  
 values
 ( @APCUncoded),
 ( @OPuncoded ),
 ( @AEuncoded ),
 ( @APC_CC_uncoded )


delete from @codedList where status is null

--select *  from @codedList 




IF EXISTS (select * from @codedList)

BEGIN

DECLARE @HTML NVARCHAR(MAX)

SET	@HTML =
	'<html><head><style type="text/css">
	table { border: 0px; border-spacing: 0px; border-collapse: collapse;}
	th {color:#FFFFFF; font-size:12px; font-family:arial; background-color:black; font-weight:bold;border: 0;}
	th.header {color:#FFFFFF; font-size:13px; font-family:arial; background-color:red; font-weight:bold;border: 0;}
	td {font-size:11px; font-family:arial;border-right: 0;border-bottom: 1px solid #C1DAD7;padding: 5px 5px 5px 8px;}
	</style></head><body>
	<table width="605"> <tr><th class="header" width="405">Uncoded HRGs Identified</th></tr></table>
	<table width="605">
	<tr>  
	<th width="5">Dataset</th>
	</tr>'
SELECT @HTML =  @HTML +   
	'<tr>
	<td bgcolor="#E0E0E0" width="5">' + status  +'</td>	
	 	
	</tr>'
FROM @codedList -- where status != 0 

SELECT @HTML =  @HTML + '</table></body></html>'

print @HTML

--SELECT @EmailSubject = 'Not successful'

EXEC msdb.dbo.sp_send_dbmail
@recipients= 'jabran.rafiq@cmft.nhs.uk;darren.griffiths@cmft.nhs.uk;gareth.cunnah@cmft.nhs.uk;rachel.royston@cmft.nhs.uk;colin.hunter@cmft.nhs.uk;paul.egan@cmft.nhs.uk;',
@subject = 'Uncoded HRGs Identified',
@body = @HTML,
@body_format = 'HTML',
@importance = 'HIGH'

end


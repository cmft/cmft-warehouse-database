﻿ 
CREATE proc [Alert].[CheckForMissingRecordsInMergedOP]

as

declare 
	 
	 @OPMissingTrafford varchar(100)
	,@OPMissingCentral varchar(100)
	,@MissingCount varchar(100)
	
	

--if (
--select
--	distinct 1
--from
--	APC.BaseEncounter with(nolock)
--where
--	not exists
--		(
--		select
--			1
--		from
--			APC.BaseEncounterReference  
--		where
--			BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
--		)
--and DischargeDate >= '1 apr 2012'

--) = 1
--set @APCMissing = 'Missing BaseEncounterReference Records for APC'
--	else
--set @APCMissing = 0
 
 
 -- Trafford Check
set @MissingCount = 
(
	select count (*)


	from [$(TraffordWarehouse)].dbo.OPEncounter t --Check against Staging

	where 

		AppointmentDate >= '01 april 2009'
		and
			not exists

		(
		select 
		1 
		from OP.BaseEncounter c
		where c.SourceUniqueID = t.SourceUniqueID

		)
)

if (

@MissingCount

) > 0


set @OPMissingTrafford =  @MissingCount + ' OP Records Missing - Trafford data'


--	else
--set @OPMissing = 0
 


--if (
--select
--	distinct 1
--from
--	AE.BaseEncounter with (nolock)
--where
--	not exists
--		(
--		select
--			1
--		from
--			AE.BaseEncounterReference with (nolock)
--		where
--			BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
--		)
--and ArrivalDate >= '1 apr 2012'
--and Reportable = 1

--) = 1
--set @AEMissing = 'Missing BaseEncounterReference Records for AE'
--	else
--set @AEMissing = 0
 



  

declare  @MissingList table (status varchar(100))

 insert into @MissingList  
 values

	 ( @OPMissingTrafford) ,
	 ( @OPMissingCentral)


delete from @MissingList where status is null

--select *  from @codedList 




IF EXISTS (select * from @MissingList)

BEGIN

DECLARE @HTML NVARCHAR(MAX)
DECLARE @HTMLFooter NVARCHAR(MAX)

SET	@HTML =
	'<html><head><style type="text/css">
	table { border: 0px; border-spacing: 0px; border-collapse: collapse;}
	th {color:#FFFFFF; font-size:12px; font-family:arial; background-color:black; font-weight:bold;border: 0;}
	th.header {color:#FFFFFF; font-size:13px; font-family:arial; background-color:red; font-weight:bold;border: 0;}
	td {font-size:11px; font-family:arial;border-right: 0;border-bottom: 1px solid #C1DAD7;padding: 5px 5px 5px 8px;}
	</style></head><body>
	<table width="605"> 
	<tr><th class="header" width="405">Missing Source Records in Merge</th></tr>
	</table>
	<table width="605">
	<tr>  
	<th width="5">Dataset</th>
	</tr>
	'
SELECT @HTML =  @HTML +   
	'<tr>
	<td bgcolor="#E0E0E0" width="5">' + status  +'</td>	
	 	
	</tr>'
FROM @MissingList -- where status != 0 

select @HTMLFooter =

	'<html><head><style type="text/css">
	table { border: 0px; border-spacing: 0px; border-collapse: collapse;}
	th {color:#FFFFFF; font-size:12px; font-family:arial; background-color:black; font-weight:bold;border: 0;}
	th.header {color:#FFFFFF; font-size:13px; font-family:arial; background-color:black; font-weight:bold;border: 0;}
	td {font-size:11px; font-family:arial;border-right: 0;border-bottom: 1px solid #C1DAD7;padding: 5px 5px 5px 8px;}
	</style></head><body>
	<table width="605"> 
	<tr><th class="header" width="405">See http://redmine/dokuwiki/doku.php?id=source_data_missing_from_merged for more information</th></tr>
	</table>
	<table width="605">
	<tr>  
	<th width="5"></th>
	</tr>
	'
SELECT @HTML =  @HTML + '</table></body></html>' + @HTMLFooter

print @HTML

--SELECT @EmailSubject = 'Not successful'

EXEC msdb.dbo.sp_send_dbmail
@recipients= 'jabran.rafiq@cmft.nhs.uk;darren.griffiths@cmft.nhs.uk;gareth.cunnah@cmft.nhs.uk;rachel.royston@cmft.nhs.uk;colin.hunter@cmft.nhs.uk;paul.egan@cmft.nhs.uk;',
@subject = 'Missing Source Records in Merge',
@body = @HTML,
@body_format = 'HTML',
@importance = 'HIGH'

end


﻿

CREATE view [Mortality].[Fact] as


/* 
20150813	RR	Some deaths have multiple forms completed, one using the old, one the new.
				This is distorting the report.  The CTE has been added so the duplicates are removed.
				If this impacts on Performance, may have to change to a Stored procedure, or add a reportable flag??
*/

With MortalityCTE
	(
	MergeEncounterRecno
	,MergeReviewRecno
	,SourcePatientNo
	,SourceUniqueID
	)
as
	(
	Select 
		MergeEncounterRecno
		,MergeReviewRecno
		,SourcePatientNo
		,SourceUniqueID
	from 
		APC.BaseEncounterBaseReview Bridge

	inner join Mortality.BaseReview
	on MergeReviewRecno = BaseReview.MergeRecno

	where 
		QuestionNo in (0,1)
	)

select
	BaseReview.MergeRecno
	,BaseReview.SourceUniqueID
	,ContextID = Reference.ContextID
	
	,DirectorateID = coalesce(Directorate.DirectorateID,15)
	,Encounter.MergeEncounterRecno
	,Encounter.SpecialtyID
	,Encounter.ConsultantID
	,MortalityReviewAvailableDateID = coalesce
								(AvailableDate.DateID
								,NullDate.DateID
								)
	,MortalityReviewAddedDateID = coalesce
								(ReviewAddedDate.DateID
								,NullDate.DateID
								)
										
	,PrimaryReviewerID = Reference.SystemUserID
	,Age.AgeID
	,DateOfDeathID = coalesce(
						DeathDate.DateID
						,NullDate.DateID
						)
			
	,ClassificationGradeID = Reference.ClassificationGradeID

	,RequiresFurtherInvestigation =
		CAST(
		Case
			when BaseReview.RequiresFurtherInvestigation = 1 then 1
			when [$(CMFT)].Dates.GetAge(DateOfBirth, DateOfDeath) < 55 then 1
			else 0
		end
		as bit
		)
	
	,QuestionID = Reference.QuestionID
	,ResponseID = Reference.ResponseID
	--,Question.Question
	
	,PrimaryReviewCompletedDateID = coalesce
										(PrimaryCompletedDate.DateID
										,NullDate.DateID
										)
	--,PrimaryReviewCompleted = 
	--	CAST(
	--	Case
	--		when BaseReview.PrimaryReviewCompletedTime is not null then 1
	--		else 0
	--	end
	--	as bit
	--	)
	
	,PrimaryReviewOutstanding = 
		CAST(
			case 
				when LocalReviewStatus in ('Completed','NotRequired') then 0
				when PrimaryReviewCompletedTime is not null then 0
				else 1
			end								
		as bit)
	,Reference.ReviewStatusID
		
	--Measure
	,Cases = 1
	,Reviews = 
		case 
			when QuestionNo in (0,1) then 1 
			else 0 
		end
from
	Mortality.BaseReview

inner join Mortality.BaseReviewReference Reference
on BaseReview.MergeRecno = Reference.MergeRecno

inner join 
	(
	select 
		MergeEncounterRecno
		,SourcePatientNo
		,SourceUniqueID
	from 
		MortalityCTE
	where 
		not exists
		(
		Select
			1
		from MortalityCTE Latest
		where
			Latest.SourcePatientNo = MortalityCTE.SourcePatientNo
		and Latest.SourceUniqueID > MortalityCTE.SourceUniqueID
		)
	)Bridge
on BaseReview.SourceUniqueID = Bridge.SourceUniqueID
and BaseReview.SourcePatientNo = Bridge.SourcePatientNo
	
left join APC.Encounter
on Bridge.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WH.Calendar DeathDate
on Encounter.DateOfDeath = DeathDate.TheDate

left join WH.Calendar PrimaryCompletedDate
on	PrimaryCompletedDate.TheDate = cast(BaseReview.PrimaryReviewCompletedTime as date)

left join WH.Calendar ReviewAddedDate
on	ReviewAddedDate.TheDate = BaseReview.MortalityReviewAdded

left join WH.Calendar AvailableDate
on	AvailableDate.TheDate = coalesce(
								BaseReview.MortalityReviewAdded
								,cast(
									BaseReview.PrimaryReviewAssignedTime 
									as date)
									)
left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'

left join WH.Directorate
on Encounter.EndDirectorateCode = Directorate.DirectorateCode

left join WH.Age
on Age.AgeCode = Utility.fn_CalcAgeCode(Encounter.DateOfBirth, Encounter.DateOfDeath)

left join Mortality.ReviewStatus
on Reference.ReviewStatusID = ReviewStatus.SourceReviewStatusID






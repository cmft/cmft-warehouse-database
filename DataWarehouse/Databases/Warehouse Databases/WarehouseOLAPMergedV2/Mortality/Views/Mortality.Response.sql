﻿


CREATE view [Mortality].[Response] as

select 
	 ResponseID 
	 ,Response 
	,YesNoResponse
from
	Mortality.ResponseBase
union
select
	0
	,'Review Not Started'
	,0



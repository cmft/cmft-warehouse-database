﻿

CREATE view [Mortality].[ReviewMetric]

as

select
	ReviewMetricBase.MetricID
	,ReviewMetricBase.Metric
	,ReviewMetricBase.FormTypeID
	,ReviewFormTypeBase.FormType
from
	Mortality.ReviewMetricBase

inner join
	Mortality.ReviewFormTypeBase
on	ReviewFormTypeBase.FormTypeID = ReviewMetricBase.FormTypeID

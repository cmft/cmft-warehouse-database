﻿




-- drop view Mortality.ReviewStatus
CREATE view [Mortality].[ReviewStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceReviewStatusID = SourceValueID
	,SourceReviewStatusCode = SourceValueCode
	,SourceReviewStatus = SourceValue
	,LocalReviewStatusID = LocalValueID
	,LocalReviewStatusCode = LocalValueCode
	,LocalReviewStatus = LocalValue
	,NationalReviewStatusID = NationalValueID
	,NationalReviewStatusCode = NationalValueCode
	,NationalReviewStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ReviewStatus'

union all

select
	SourceContextCode = 'CMFT||MORT'
	,SourceContext = 'Mortality (CMFT)'
	,SourceReviewStatusID = -1
	,SourceReviewStatusCode = '-99'
	,SourceReviewStatus = 'Not Available'
	,LocalReviewStatusID = null
	,LocalReviewStatusCode = null
	,LocalReviewStatus = null
	,NationalReviewStatusID = null
	,NationalReviewStatusCode = null
	,NationalReviewStatus = null

union all

select
	SourceContextCode = 'CMFT||MORTARCHV'
	,SourceContext = 'Mortality Archive (CMFT)'
	,SourceReviewStatusID = -2
	,SourceReviewStatusCode = '-99'
	,SourceReviewStatus = 'Not Available' 
	,LocalReviewStatusID = null
	,LocalReviewStatusCode = null
	,LocalReviewStatus = null
	,NationalReviewStatusID = null
	,NationalReviewStatusCode = null
	,NationalReviewStatus = null
	




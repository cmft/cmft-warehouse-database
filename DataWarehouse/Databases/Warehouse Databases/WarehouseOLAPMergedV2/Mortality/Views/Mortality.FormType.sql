﻿
-- drop view Mortality.ReviewStatus
CREATE view Mortality.FormType as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceFormTypeID = SourceValueID
	,SourceFormTypeCode = SourceValueCode
	,SourceFormType = SourceValue
	,LocalFormTypeID = LocalValueID
	,LocalFormTypeCode = LocalValueCode
	,LocalFormType = LocalValue
	,NationalFormTypeID = NationalValueID
	,NationalFormTypeCode = NationalValueCode
	,NationalFormType = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'FormType'


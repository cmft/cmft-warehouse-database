﻿
CREATE view [Mortality].[ClassificationGrade]
as
select
	ClassificationGradeID 
	,ClassificationGradeCode 
	,ClassificationGrade 
	,ClassificationGradeLabel 
from
	Mortality.ClassificationGradeBase

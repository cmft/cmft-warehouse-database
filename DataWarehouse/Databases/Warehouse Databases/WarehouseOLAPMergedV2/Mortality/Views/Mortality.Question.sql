﻿




CREATE View [Mortality].[Question] 
as

Select
	QuestionID
	,Question
	,QuestionText = dbo.SpaceBeforeCap(Question)
	,QuestionNo
	,YesNoResponse
	,FormTypeID
from
	Mortality.QuestionBase

union
Select
	-SourceFormTypeID
	,'ReviewNotStarted'
	,'Review Not Started'
	,0
	,0
	,SourceFormTypeID
from 
	Mortality.FormType




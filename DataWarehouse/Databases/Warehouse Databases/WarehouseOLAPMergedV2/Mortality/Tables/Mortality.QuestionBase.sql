﻿CREATE TABLE [Mortality].[QuestionBase] (
    [QuestionID]    INT           IDENTITY (1, 1) NOT NULL,
    [Question]      VARCHAR (MAX) NULL,
    [QuestionNo]    INT           NULL,
    [YesNoResponse] INT           NULL,
    [FormTypeID]    INT           NOT NULL,
    [Created]       DATETIME      CONSTRAINT [DF_Question_Created] DEFAULT (getdate()) NULL,
    [Updated]       DATETIME      CONSTRAINT [DF_Question_Updated] DEFAULT (getdate()) NULL,
    [ByWhom]        VARCHAR (50)  CONSTRAINT [DF_Question_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK__Question__0DC06F8C77AC53A6] PRIMARY KEY CLUSTERED ([QuestionID] ASC)
);


﻿CREATE TABLE [Mortality].[ReviewFormTypeBase] (
    [FormTypeID] INT           NOT NULL,
    [FormType]   VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ReviewFormTypeBase] PRIMARY KEY CLUSTERED ([FormTypeID] ASC)
);


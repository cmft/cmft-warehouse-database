﻿CREATE TABLE [Mortality].[FactReview] (
    [MergeMortalityReviewRecno]    INT NOT NULL,
    [DirectorateID]                INT NULL,
    [SpecialtyID]                  INT NULL,
    [ConsultantID]                 INT NULL,
    [ReviewedDateID]               INT NULL,
    [Cases]                        INT NOT NULL,
    [ClassificationGradeID]        INT NULL,
    [FormTypeID]                   INT NULL,
    [AgeAtDeathID]                 INT NULL,
    [RequiresFurtherInvestigation] BIT NULL
);


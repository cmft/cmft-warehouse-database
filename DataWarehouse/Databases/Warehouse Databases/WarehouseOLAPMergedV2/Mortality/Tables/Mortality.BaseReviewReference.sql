﻿CREATE TABLE [Mortality].[BaseReviewReference] (
	[MergeRecno] [int] NOT NULL,
	[ContextID] [int] NULL,
	[FormTypeID] [int] NULL,
	[ReviewStatusID] [int] NULL,
	[SystemUserID] [int] NULL,
	[QuestionID] [int] NULL,
	[ResponseID] [int] NULL,
	[ClassificationGradeID] [int] NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NULL,
	[SpecialtyID] [varchar](100) NULL,
	[ConsultantID] [varchar](100) NULL,
    CONSTRAINT [PK_BaseReviewReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


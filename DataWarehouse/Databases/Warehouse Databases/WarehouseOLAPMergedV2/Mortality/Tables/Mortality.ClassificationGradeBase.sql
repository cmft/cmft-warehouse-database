﻿CREATE TABLE [Mortality].[ClassificationGradeBase] (
    [ClassificationGradeID]    INT           IDENTITY (1, 1) NOT NULL,
    [ClassificationGradeCode]  VARCHAR (10)  NULL,
    [ClassificationGrade]      VARCHAR (10)  NULL,
    [ClassificationGradeLabel] VARCHAR (200) NULL,
    CONSTRAINT [PK__Classifi__71E6117545EA0F89] PRIMARY KEY CLUSTERED ([ClassificationGradeID] ASC)
);


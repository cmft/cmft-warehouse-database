﻿CREATE TABLE [Mortality].[FactReviewMetric] (
    [MergeMortalityReviewRecno]    INT NOT NULL,
    [MetricID]                     INT NOT NULL,
    [DirectorateID]                INT NULL,
    [SpecialtyID]                  INT NULL,
    [ConsultantID]                 INT NULL,
    [ReviewedDateID]               INT NULL,
    [ClassificationGradeID]        INT NULL,
    [ResponseID]                   BIT NULL,
    [AgeAtDeathID]                 INT NULL,
    [RequiresFurtherInvestigation] BIT NULL,
    [Cases]                        INT NOT NULL
);


﻿CREATE TABLE [Mortality].[ReviewMetricBase] (
    [MetricID]   INT           IDENTITY (1, 1) NOT NULL,
    [Metric]     VARCHAR (100) NULL,
    [FormTypeID] INT           NULL,
    CONSTRAINT [PK_ReviewMetricBase] PRIMARY KEY CLUSTERED ([MetricID] ASC)
);


﻿CREATE TABLE [Mortality].[TimeBand] (
    [TimeBandID]            INT          NOT NULL,
    [TimeBandCode]          VARCHAR (10) NULL,
    [TimeBand]              VARCHAR (50) NULL,
    [ReportingTimeBandCode] VARCHAR (10) NULL,
    [ReportingTimeBand]     VARCHAR (50) NULL,
    CONSTRAINT [PK_MortalityTimeBand] PRIMARY KEY CLUSTERED ([TimeBandID] ASC)
);


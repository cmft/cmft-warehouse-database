﻿CREATE TABLE [Mortality].[ReviewStatusBase] (
    [StatusID]   INT          NOT NULL,
    [StatusCode] INT          NOT NULL,
    [Status]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ReviewStatus] PRIMARY KEY CLUSTERED ([StatusID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_rsb_SCode]
    ON [Mortality].[ReviewStatusBase]([StatusCode] ASC);


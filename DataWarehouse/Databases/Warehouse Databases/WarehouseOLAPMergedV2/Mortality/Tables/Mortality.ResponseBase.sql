﻿CREATE TABLE [Mortality].[ResponseBase] (
    [ResponseID]       INT           IDENTITY (1, 1) NOT NULL,
    [Response]         VARCHAR (MAX) NULL,
    [OriginalResponse] VARCHAR (MAX) NULL,
    [YesNoResponse]    INT           NULL,
    [Created]          DATETIME      CONSTRAINT [DF_Response_Created] DEFAULT (getdate()) NULL,
    [Updated]          DATETIME      CONSTRAINT [DF_Response_Updated] DEFAULT (getdate()) NULL,
    [ByWhom]           VARCHAR (50)  CONSTRAINT [DF_Response_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK__Response__0DC06F8C77AC53A6] PRIMARY KEY CLUSTERED ([ResponseID] ASC)
);


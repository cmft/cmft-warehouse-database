﻿
CREATE FUNCTION [Utility].[fn_CalcAgeCode]
(
	 @DateOfBirth	DATETIME
	,@ReferenceDate	DATETIME
)

RETURNS VARCHAR(50)
AS
BEGIN	
	/******************************************************************************
	**  Name: Utility.fn_CalcAgeCode
	**  Purpose: Returns an age code based on the date of birth and a refernce the age is determined at
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------  
	** 26.10.12      MH       Created  
	******************************************************************************/
	
	RETURN
		CASE
			WHEN	DATEDIFF(day, @DateOfBirth, @ReferenceDate) is null			THEN 'Age Unknown'
			WHEN	DATEDIFF(day, @DateOfBirth, @ReferenceDate) <= 0			THEN '00 Days'
			WHEN	DATEDIFF(day, @DateOfBirth, @ReferenceDate) = 1				THEN '01 Day'

			WHEN	DATEDIFF(day, @DateOfBirth, @ReferenceDate) > 1 
				AND DATEDIFF(day, @DateOfBirth, @ReferenceDate) <= 28 
			  THEN	RIGHT('0' + Convert(Varchar,DATEDIFF(day, @DateOfBirth, @ReferenceDate)), 2) + ' Days'
		
			WHEN	DATEDIFF(day, @DateOfBirth, @ReferenceDate) > 28 
				AND		DATEDIFF(month, @DateOfBirth, @ReferenceDate) -
						CASE  WHEN DATEPART(day, @DateOfBirth) > DATEPART(day, @ReferenceDate) THEN 1 ELSE 0 END
						< 1 
			  THEN	'Between 28 Days and 1 Month'

			WHEN	DATEDIFF(month, @DateOfBirth, @ReferenceDate) -
					CASE  WHEN DATEPART(day, @DateOfBirth) > DATEPART(day, @ReferenceDate) THEN 1 ELSE 0 END 
					= 1
			  THEN	'01 Month'

			WHEN	DATEDIFF(month, @DateOfBirth, @ReferenceDate) -
					CASE  WHEN DATEPART(day, @DateOfBirth) > DATEPART(day, @ReferenceDate) THEN 1 ELSE 0 END 
					> 1 
				AND
					DATEDIFF(month, @DateOfBirth, @ReferenceDate) -
					CASE  WHEN DATEPART(day, @DateOfBirth) > DATEPART(day, @ReferenceDate) THEN 1 ELSE 0 END 
					<= 23
			  THEN	RIGHT('0' + Convert(varchar,DATEDIFF(month, @DateOfBirth,@ReferenceDate) -
						CASE  WHEN DATEPART(day, @DateOfBirth) > DATEPART(day, @ReferenceDate) THEN 1 ELSE 0 END), 2) + ' Months'

			WHEN	DATEDIFF(yy, @DateOfBirth, @ReferenceDate) - 
				(
				CASE 
					WHEN	(DATEPART(m, @DateOfBirth) > DATEPART(m, @ReferenceDate)) 
						OR
							(
								DATEPART(m, @DateOfBirth) = DATEPART(m, @ReferenceDate) 
							AND	DATEPART(d, @DateOfBirth) > DATEPART(d, @ReferenceDate)
							)
				  THEN 1 
				  ELSE 0 
				END
				) > 99 
			  THEN	'99+'

			ELSE	RIGHT('0' + convert(varchar, DATEDIFF(yy, @DateOfBirth, @ReferenceDate) - 
			(
				CASE 
					WHEN	(DATEPART(m, @DateOfBirth) > DATEPART(m, @ReferenceDate)) 
						OR
							(
								DATEPART(m, @DateOfBirth) = DATEPART(m, @ReferenceDate) 
							AND	DATEPART(d, @DateOfBirth) > DATEPART(d, @ReferenceDate)
							) 
				  THEN 1 
				  ELSE 0 
				END
			)), 2) + ' Years'

		END			
END

﻿CREATE TABLE [Utility].[EntityLookup] (
    [EntityTypeCode] VARCHAR (50)  NOT NULL,
    [EntityCode]     VARCHAR (50)  NOT NULL,
    [Description]    VARCHAR (255) NOT NULL,
    [Created]        DATETIME      CONSTRAINT [df_EntityLookupCreated] DEFAULT (getdate()) NULL,
    [CreatedBy]      VARCHAR (50)  CONSTRAINT [df_EntityLookupCreatedBy] DEFAULT (suser_name()) NULL,
    [Updated]        DATETIME      CONSTRAINT [df_EntityLookupUpdated] DEFAULT (getdate()) NULL,
    [UpdatedBy]      VARCHAR (50)  CONSTRAINT [df_EntityLookupUpdatedBy] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_EntityLookup] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC, [EntityCode] ASC)
);


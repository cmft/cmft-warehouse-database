﻿CREATE TABLE [Utility].[EntityXref] (
    [EntityXrefRecno]    INT          IDENTITY (1, 1) NOT NULL,
    [EntityTypeCode]     VARCHAR (50) NOT NULL,
    [EntityCode]         VARCHAR (50) NOT NULL,
    [XrefEntityTypeCode] VARCHAR (50) NOT NULL,
    [XrefEntityCode]     VARCHAR (50) NOT NULL,
    [Created]            DATETIME     CONSTRAINT [df_EntityXrefCreated] DEFAULT (getdate()) NULL,
    [CreatedBy]          VARCHAR (50) CONSTRAINT [df_EntityXrefCreatedBy] DEFAULT (suser_name()) NULL,
    [Updated]            DATETIME     CONSTRAINT [df_EntityXrefUpdated] DEFAULT (getdate()) NULL,
    [UpdatedBy]          VARCHAR (50) CONSTRAINT [df_EntityXrefUpdatedBy] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_EntityXref] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC, [EntityCode] ASC, [XrefEntityTypeCode] ASC, [XrefEntityCode] ASC)
);


﻿CREATE TABLE [Utility].[AuditProcessSource] (
    [ProcessSourceCode] VARCHAR (50)  NOT NULL,
    [ProcessSource]     VARCHAR (255) NULL,
    CONSTRAINT [PK_ProcessSource] PRIMARY KEY CLUSTERED ([ProcessSourceCode] ASC)
);


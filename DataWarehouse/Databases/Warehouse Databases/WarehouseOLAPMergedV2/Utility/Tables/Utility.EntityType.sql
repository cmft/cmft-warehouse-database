﻿CREATE TABLE [Utility].[EntityType] (
    [EntityTypeCode] VARCHAR (50) NOT NULL,
    [EntityType]     VARCHAR (50) NULL,
    [Created]        DATETIME     CONSTRAINT [df_EntityTypeCreated] DEFAULT (getdate()) NULL,
    [CreatedBy]      VARCHAR (50) CONSTRAINT [df_EntityTypeCreatedBy] DEFAULT (suser_name()) NULL,
    [Updated]        DATETIME     CONSTRAINT [df_EntityTypeUpdated] DEFAULT (getdate()) NULL,
    [UpdatedBy]      VARCHAR (50) CONSTRAINT [df_EntityTypeUpdatedBy] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_EntityType] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC)
);


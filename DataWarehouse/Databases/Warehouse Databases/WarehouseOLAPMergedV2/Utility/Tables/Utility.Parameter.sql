﻿CREATE TABLE [Utility].[Parameter] (
    [Parameter]    VARCHAR (128)   NOT NULL,
    [TextValue]    VARCHAR (255)   NULL,
    [NumericValue] DECIMAL (18, 5) NULL,
    [DateValue]    DATETIME        NULL,
    [BooleanValue] BIT             NULL,
    [Created]      DATETIME        CONSTRAINT [DF__Parameter__Creat__2005291A] DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME        NULL,
    [ByWhom]       VARCHAR (255)   CONSTRAINT [DF__Parameter__ByWho__20F94D53] DEFAULT (suser_name()) NOT NULL,
    CONSTRAINT [PK_Parameter] PRIMARY KEY CLUSTERED ([Parameter] ASC)
);


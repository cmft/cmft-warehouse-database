﻿
CREATE proc [Utility].[BuildAPCProcessList] as

-- to be run manually in the advent of any problems with HRG grouping or losing records in the Reference table

insert  APC.ProcessList
(
	MergeRecno
)

select
	MergeEncounterRecno

from
	APC.Encounter
where
	not exists
		(
		select
			1
		from
			APC.HRG4Encounter
		where
			Encounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
		)
and not exists
		(
		select
			1
		from
			APC.ProcessList
		where
			Encounter.MergeEncounterRecno = ProcessList.MergeRecno
		)
and DischargeDate >= '1 apr 2012'


union

select
	MergeEncounterRecno
from
	APC.BaseEncounter
where
	not exists
		(
		select
			1
		from
			APC.BaseEncounterReference
		where
			BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
		)
and not exists
		(
		select
			1
		from
			APC.ProcessList
		where
			BaseEncounter.MergeEncounterRecno = ProcessList.MergeRecno
		)
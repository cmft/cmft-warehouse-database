﻿
create proc Utility.BuildAEProcessList as

insert  AE.ProcessList
(
	MergeRecno
	,Action
	,Dataset
)

select
	MergeEncounterRecno
	,'HRG'
	,'Encounter'
from
	AE.BaseEncounter
where
	not exists
		(
		select
			1
		from
			AE.HRG4Encounter
		where
			BaseEncounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
		)
and not exists
		(
		select
			1
		from
			AE.ProcessList
		where
			BaseEncounter.MergeEncounterRecno = ProcessList.MergeRecno
		and	ProcessList.Dataset = 'Encounter'
		)
and ArrivalDate >= '1 apr 2012'
and Reportable = 1
﻿
CREATE proc [Utility].[BuildOPProcessList] as

insert  OP.ProcessList
(
	MergeRecno
	,Action
)

select
	MergeEncounterRecno
	,'HRG'
from
	OP.BaseEncounter
where
	not exists
		(
		select
			1
		from
			OP.HRG4Encounter
		where
			BaseEncounter.MergeEncounterRecno = HRG4Encounter.MergeEncounterRecno
		)
and not exists
		(
		select
			1
		from
			OP.ProcessList
		where
			BaseEncounter.MergeEncounterRecno = ProcessList.MergeRecno
		)
and AppointmentDate >= '1 apr 2012' 





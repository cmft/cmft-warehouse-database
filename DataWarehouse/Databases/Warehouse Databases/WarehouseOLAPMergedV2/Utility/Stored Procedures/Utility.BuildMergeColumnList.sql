﻿CREATE procedure [Utility].[BuildMergeColumnList] as

select
	case
	when columns.column_id = 1
	then ' ' + columns.name
	else ',' + columns.name + ' = cast(' + columns.name + ' as ' + systypes.name + 
		case
		when columns.collation_name is null
		then ''
		else '(' + cast(columns.max_length as varchar) + ')'
		end + ')'
	end

	,case
	when columns.column_id = 1
	then ' '
	else ','
	end + columns.name

	,case
	when columns.column_id = 1
	then ' '
	else 'and'
	end +
	' isnull(target.' + columns.name + ', ' +
	case

	when columns.system_type_id in (40, 58, 61) --date
	then 'getdate()'

	when columns.system_type_id in (35, 99, 167, 175, 231, 239) --string
	then ''''''

	else '0'
	end +
	') = isnull(source.' + columns.name + ', ' +
	case

	when columns.system_type_id in (40, 58, 61) --date
	then 'getdate()'

	when columns.system_type_id in (35, 99, 167, 175, 231, 239) --string
	then ''''''

	else '0'
	end +
	')'
	

	,case
	when columns.column_id = 1
	then ' '
	else ','
	end +
	'target.' + columns.name + ' = ' +
	'source.' + columns.name


	--,case
	--when columns.column_id = 1
	--then ' ' + columns.name
	--else ',' + columns.name + ' = isnull(cast(' + columns.name + ' as ' + systypes.name + 
	--	case
	--	when columns.collation_name is null
	--	then ''
	--	else '(' + cast(columns.max_length as varchar) + ')'
	--	end + ')'
	--+ ', ' +
	--case

	--when columns.system_type_id in (40, 58, 61) --date
	--then 'getdate()'

	--when columns.system_type_id in (35, 99, 167, 175, 231, 239) --string
	--then ''''''

	--else '0'
	--end +
	--')'
	--end


	--,case
	--when columns.column_id = 1
	--then ' '
	--else 'and'
	--end +
	--' isnull(target.' + columns.name + ', ' +
	--case

	--when columns.system_type_id in (40, 58, 61) --date
	--then 'getdate()'

	--when columns.system_type_id in (35, 99, 167, 175, 231, 239) --string
	--then ''''''

	--else '0'
	--end +
	--') = source.' + columns.name

	,*
from
	sys.columns

inner join sys.tables
--inner join sys.views tables
on	tables.object_id = columns.object_id

inner join sys.systypes
on	systypes.xtype = columns.system_type_id

inner join sys.schemas
on	schemas.schema_id = tables.schema_id

where
	systypes.name <> 'sysname'
and	tables.name = 'BaseDailyActivity'
and	schemas.name = 'Neonatal'

order by
	columns.column_id

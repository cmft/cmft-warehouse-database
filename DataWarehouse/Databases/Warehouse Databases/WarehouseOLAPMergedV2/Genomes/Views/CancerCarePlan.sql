﻿CREATE VIEW [Genomes].[CancerCarePlan] as

select
	ReferralID = Wrk.ReferralID

	,EventReference = CarePlan.UniqueRecordId
	,OrganisationID = Imaging.OrganisationCode
	,EventDate = CarePlan.CarePlanAgreedDate

	,PrimaryDiagnosis =
		coalesce(
			Pathology.PrimaryDiagnosisCode
			,Referral.PrimaryDiagnosisCode1
			,Referral.HistologyCode
		)
	
	,StartDate = null
	,TreatmentIntent = CarePlan.CancerCarePlanIntentCode
	,PerformanceStatus = CarePlan.PerformanceStatusCode
	,Ace27Code = CarePlan.CoMorbidityIndexCode
	,NoCancerTreatmentReason = CarePlan.NoTreatmentReasonCode
	,OutcomeOfMDT = CarePlan.MDTComments
from
	Genomes.Wrk

inner join [$(Warehouse)].SCR.Referral
on	Referral.UniqueRecordId = Wrk.ReferralID

left join [$(Warehouse)].SCR.CarePlan
on	CarePlan.ReferralUniqueRecordId = Referral.UniqueRecordId

left join [$(Warehouse)].SCR.Pathology
on	Pathology.ReferralUniqueRecordId = Referral.UniqueRecordId

left join [$(Warehouse)].SCR.Imaging
on	Imaging.ReferralUniqueRecordId = Referral.UniqueRecordId

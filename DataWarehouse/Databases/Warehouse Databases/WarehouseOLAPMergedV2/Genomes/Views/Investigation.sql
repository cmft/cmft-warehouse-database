﻿CREATE VIEW [Genomes].[Investigation] as

select
	ReferralID = Wrk.ReferralID
	,EventDate = Imaging.EventDate
	,OrganisationID = 'NA'
	,EventReference = Imaging.UniqueRecordId

	,PrimaryDiagnosis =
		coalesce(
			Pathology.PrimaryDiagnosisCode
			,Referral.PrimaryDiagnosisCode1
			,Referral.HistologyCode
		)
from
	Genomes.Wrk

inner join [$(Warehouse)].SCR.Referral
on	Referral.UniqueRecordId = Wrk.ReferralID

left join [$(Warehouse)].SCR.Pathology
on	Pathology.ReferralUniqueRecordId = Referral.UniqueRecordId

left join [$(Warehouse)].SCR.Imaging
on	Imaging.ReferralUniqueRecordId = Wrk.ReferralID

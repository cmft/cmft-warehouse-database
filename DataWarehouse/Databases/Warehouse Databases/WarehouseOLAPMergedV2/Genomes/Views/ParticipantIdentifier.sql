﻿CREATE VIEW [Genomes].[ParticipantIdentifier] as

select
	ParticipantID = Wrk.ParticipantID
	,DateOfBirth = Demographic.DateOfBirth
	,NHSNumber = Demographic.NHSNumber
	,CHINumber = null
	,HospitalNumber = Demographic.HospitalNumber
	,Surname = Demographic.Surname
	,Forename = Demographic.Forename
from
	Genomes.Wrk

inner join [$(Warehouse)].SCR.Referral
on	Referral.UniqueRecordId = Wrk.ReferralID

inner join [$(Warehouse)].SCR.Demographic
on	Demographic.UniqueRecordId = Referral.DemographicUniqueRecordId

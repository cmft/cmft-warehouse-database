﻿CREATE VIEW [Genomes].[RiskFactor] as

select
	ReferralID = Wrk.ReferralID
	,PredominantEthnicity = Demographic.EthnicityCode
	,Smoking = null
	,AlcoholConsumption = null
	,Obesity = null
from
	Genomes.Wrk

inner join [$(Warehouse)].SCR.Referral
on	Referral.UniqueRecordId = Wrk.ReferralID

inner join [$(Warehouse)].SCR.Demographic
on	Demographic.UniqueRecordId = Referral.DemographicUniqueRecordId

﻿CREATE VIEW [Genomes].[Morphology] as

select
	EventReference = Referral.UniqueRecordId
	,Morphology =
		coalesce(
			Pathology.MorphologyCode
			,Referral.PrimaryDiagnosisCode1
			,Referral.HistologyCode
		)

	,MorphologyICD = Referral.HistologyCode
	,MorphologySnomedCT = null
	,MorphologySnomedRT = null
from
	Genomes.Wrk

inner join [$(Warehouse)].SCR.Referral
on	Referral.UniqueRecordId = Wrk.ReferralID

left join [$(Warehouse)].SCR.Pathology
on	Pathology.ReferralUniqueRecordId = Referral.UniqueRecordId


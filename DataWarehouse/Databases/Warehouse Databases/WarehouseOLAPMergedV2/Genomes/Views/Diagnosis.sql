﻿CREATE VIEW [Genomes].[Diagnosis] as

with

MetastaticSite as
	(
	select
		ReferralUniqueRecordId

		,MetastaticSiteCode = 
			case MetastaticSite
			when 'Brain' then '02'
			when 'Liver' then '03'
			when 'Lung' then '04'
			when 'Unknown' then '07'
			when 'Skin' then '08'
			when 'Lymph' then '09'
			when 'BoneExMarrow' then '10'
			when 'BoneMarrow' then '11'
			when 'Bone' then '11'
			when 'Other' then '99'
			end
	from
		(
		select
			ReferralUniqueRecordId
			,Bone
			,Brain
			,Liver
			,Lung
			,Other
			,Lymph
			--,Adrenal
			,Skin
			,Unknown
			,BoneMarrow
			,BoneExMarrow
		from
			[$(Warehouse)].SCR.Metastases
		) p
	unpivot
		(
		MetastaticStatus for MetastaticSite
		in
			(
			Bone
			,Brain
			,Liver
			,Lung
			,Other
			,Lymph
			--,Adrenal
			,Skin
			,Unknown
			,BoneMarrow
			,BoneExMarrow
			)
		) Unpvt
	where
		MetastaticStatus = 'Certain'
	)

select
	ReferralID = Wrk.ReferralID
	,EventReference = Imaging.UniqueRecordId
	,OrganisationID = Imaging.OrganisationCode
	,EventDate = Referral.DiagnosisDate
	,PrimaryDiagnosis = Pathology.PrimaryDiagnosisCode
	,RecurrenceIndicator = CarePlan.Recurrence

	,MetastaticSite =
		case
		when
			(
			select
				count(*)
			from
				MetastaticSite
			where
				MetastaticSite.ReferralUniqueRecordId = Referral.UniqueRecordId
			) > 1
		then '06' --Multiple metastatic sites

		else
			(
			select
				MetastaticSiteCode
			from
				MetastaticSite
			where
				MetastaticSite.ReferralUniqueRecordId = Referral.UniqueRecordId
			)

		end

	,Topography = Pathology.Topography
	,TopographyICD = Referral.TopographyCode
	,TopographySnomedCT = null
	,TopographySnomedRT = null
	,IntegratedT = Referral.FinalTStageCode
	,IntegratedN = Referral.FinalNStageCode
	,IntegratedM = Referral.FinalMStageCode
	,IntegratedTNM = Referral.IntegratedTNM
	,IntegratedTNMVersion = TNMVersionHistory.VersionNumber
	,BasisOfDiagnosis = Referral.BasisOfDiagnosisCode

	,Stage =
		isnull(
			ReferralGynaecology.Figo0Code
			,''
		) +
		isnull(
			ReferralGynaecology.Figo2Code
			,''
		) +
		isnull(
			ReferralGynaecology.Figo3Code
			,''
		)

	,StageType = null
	,GradeOfDifferentiation = Pathology.GradeOfDifferentiationCode

	,FinalFigoStage =
		isnull(
			ReferralGynaecology.Figo0Code
			,''
		) +
		isnull(
			ReferralGynaecology.Figo2Code
			,''
		) +
		isnull(
			ReferralGynaecology.Figo3Code
			,''
		)

	,FinalFigoStageVersion = null
	,ModifiedDukesStageVersion = null
	,SpecificGrade = GynaecologyMDT.GradeCode
	,SpecificGradingScheme = null
	,TumourLaterality = Pathology.TumourLateralityCode
	,Context = null
from
	Genomes.Wrk

inner join [$(Warehouse)].SCR.Referral
on	Referral.UniqueRecordId = Wrk.ReferralID

left join [$(Warehouse)].SCR.Imaging
on	Imaging.ReferralUniqueRecordId = Referral.UniqueRecordId

left join [$(Warehouse)].SCR.Pathology
on	Pathology.ReferralUniqueRecordId = Referral.UniqueRecordId

left join [$(Warehouse)].SCR.CarePlan
on	CarePlan.ReferralUniqueRecordId = Referral.UniqueRecordId

left join [$(Warehouse)].SCR.TNMVersionHistory --latest vesion
on	TNMVersionHistory.ReferralUniqueRecordId = Referral.UniqueRecordId
and	TNMVersionHistory.Active = 1
and	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.TNMVersionHistory Previous
	where
		Previous.ReferralUniqueRecordId = Referral.UniqueRecordId
	and	Previous.Active = 1
	and	Previous.ChangedDateTime > TNMVersionHistory.ChangedDateTime
	)

left join [$(Warehouse)].SCR.GynaecologyMDT
on	GynaecologyMDT.ReferralUniqueRecordId = Referral.UniqueRecordId

left join [$(Warehouse)].SCR.ReferralGynaecology
on	ReferralGynaecology.ReferralUniqueRecordId = Referral.UniqueRecordId

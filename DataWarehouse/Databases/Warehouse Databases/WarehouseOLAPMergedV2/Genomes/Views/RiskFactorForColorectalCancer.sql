﻿CREATE VIEW [Genomes].[RiskFactorForColorectalCancer] as

select
	ReferralID = Wrk.ReferralID
	,HistoryOfCrohns = null
	,HistoryOfUlcerativeColitis = null
	,HistoryOfCoeliacDisease = null
from
	Genomes.Wrk
where
--not collected at CMFT so return nothing
	1 = 0

﻿CREATE PROCEDURE Genomes.[BuildReturn] as

truncate table Genomes.Wrk

insert
into
	Genomes.Wrk
	(
	ReferralID
	,ParticipantID
	)
select
	DemographicID = Demographic.UniqueRecordId
	,ParticipantID = GenomicsParticipant.GenomicsParticipantID
from
	[$(Warehouse)].Themis.GenomicsParticipant

inner join [$(Warehouse)].SCR.Demographic
on	Demographic.NHSNumber = GenomicsParticipant.NHSNumber

inner join [$(Warehouse)].SCR.Referral
on	Referral.DemographicUniqueRecordId = Demographic.UniqueRecordId

where
	Demographic.NHSNumberStatus = '1' --validated
and	datediff(day, Referral.FirstAppointmentDate, getdate()) < 43

﻿CREATE PROCEDURE [Bedman].[BuildPatientSourceSystemFlag] AS


-- 20150723	RR	Using WOMV2 Bedman.BaseEvent however i can't see what is populating this, no proc, however there does appear to be an ETL process on Live which updates [$(Warehouse)].Bedman.Event, so using this.


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


--remove flags that no longer apply
delete
from
	Flag.PatientSourceSystemFlag
where
	PatientSourceSystemFlag.SourceSystemID = 2
and	PatientSourceSystemFlag.FlagID = 1
and	not exists
	(
	select
		1
	from
		APC.BaseLearningDisability
	where
		PatientSourceSystemFlag.ContextCode = 'CEN||BEDMAN'
	and	BaseLearningDisability.SourcePatientNo = PatientSourceSystemFlag.PatientID
	and KnownLearningDisability = 1
	and not exists
		(
		Select
			1
		from
			APC.BaseLearningDisability Latest
		where
			Latest.SourcePatientNo = BaseLearningDisability.SourcePatientNo
		and Latest.AssessmentTime > BaseLearningDisability.AssessmentTime
		)
	and	cast(BaseLearningDisability.AssessmentTime as date) = PatientSourceSystemFlag.StartDate
	)

select
	 @deleted = @@rowcount


--this inserts every new occurance of this flag - is that correct or do we just want the 1st (or last)?
insert
into
	Flag.PatientSourceSystemFlag
	(
	SourceSystemID
	,ContextCode
	,PatientID
	,StartDate
	,FlagID
	,EndDate
	)
select
	SourceSystemID
	,ContextCode
	,PatientID
	,StartDate
	,FlagID
	,EndDate
from
	(
	select distinct
		SourceSystemID = 2 --bedman
		,ContextCode = 'CEN||BEDMAN'
		,PatientID = SourcePatientNo
		,StartDate = cast(AssessmentTime as date)
		,FlagID = 1 --learning disability
		,EndDate = null
	from
		APC.BaseLearningDisability
	where
		KnownLearningDisability = 1
	and not exists
		(
		Select
			1
		from
			APC.BaseLearningDisability Latest
		where
			Latest.SourcePatientNo = BaseLearningDisability.SourcePatientNo
		and Latest.AssessmentTime > BaseLearningDisability.AssessmentTime
		)
	) Flags
where
	not exists
		(
		select
			1
		from
			Flag.PatientSourceSystemFlag
		where
			PatientSourceSystemFlag.SourceSystemID = Flags.SourceSystemID
		and	PatientSourceSystemFlag.ContextCode = Flags.ContextCode
		and	PatientSourceSystemFlag.PatientID = Flags.PatientID
		and	PatientSourceSystemFlag.StartDate = Flags.StartDate
		and	PatientSourceSystemFlag.FlagID = Flags.FlagID
		)

select
	 @inserted = @@rowcount
	 
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent @ProcedureName,@Stats

RETURN 0


﻿CREATE TABLE [Diagnostic].[ServiceRequestType] (
    [ServiceRequestTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [ServiceRequestTypeCode] VARCHAR (10)  NOT NULL,
    [ServiceRequestType]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ServiceRequestType] PRIMARY KEY CLUSTERED ([ServiceRequestTypeID] ASC)
);


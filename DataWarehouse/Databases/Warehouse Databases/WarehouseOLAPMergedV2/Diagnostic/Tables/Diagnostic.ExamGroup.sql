﻿CREATE TABLE [Diagnostic].[ExamGroup] (
    [ExamGroupCode] VARCHAR (10)  NOT NULL,
    [ExamGroup]     VARCHAR (100) NULL,
    [ExamAreaCode]  NCHAR (10)    NULL,
    [Created]       DATETIME2 (0) CONSTRAINT [df_Diagnostic_ExamGroup_Created] DEFAULT (getdate()) NULL,
    [Updated]       DATETIME2 (0) CONSTRAINT [df_Diagnostic_ExamGroup_Updated] DEFAULT (getdate()) NULL,
    [ByWhom]        VARCHAR (50)  CONSTRAINT [df_Diagnostic_ExamGroup_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_ExamGroup_1] PRIMARY KEY CLUSTERED ([ExamGroupCode] ASC),
    CONSTRAINT [FK_ExamGroup_ExamArea] FOREIGN KEY ([ExamAreaCode]) REFERENCES [Diagnostic].[ExamArea] ([ExamAreaCode])
);


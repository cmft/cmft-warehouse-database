﻿CREATE TABLE [Diagnostic].[BaseReference] (
    [MergeRecno]          INT NOT NULL,
    [ContextID]           INT NOT NULL,
    [ExamID]              INT NOT NULL,
    [EventDateID]         INT NOT NULL,
    [SpecialtyID]         INT NOT NULL,
    [ProfessionalCarerID] INT NOT NULL,
    [SiteID]              INT NOT NULL,
    [SexID]               INT NOT NULL
);


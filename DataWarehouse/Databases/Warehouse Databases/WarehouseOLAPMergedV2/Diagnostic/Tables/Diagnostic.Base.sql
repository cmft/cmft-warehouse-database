﻿CREATE TABLE [Diagnostic].[Base] (
    [MergeRecno]             INT          IDENTITY (1, 1) NOT NULL,
    [SourceMergeRecno]       INT          NOT NULL,
    [ContextCode]            VARCHAR (20) NOT NULL,
    [EventDate]              DATE         NULL,
    [DirectorateCode]        VARCHAR (5)  NULL,
    [NationalExamCode]       VARCHAR (10) NULL,
    [CCGCode]                VARCHAR (10) NULL,
    [ExamCode]               VARCHAR (10) NULL,
    [DataSourceID]           INT          NOT NULL,
    [SpecialtyCode]          VARCHAR (10) NULL,
    [ProfessionalCarerCode]  VARCHAR (20) NULL,
    [SiteCode]               VARCHAR (10) NULL,
    [SourceOfReferralCode]   VARCHAR (10) NULL,
    [SexCode]                VARCHAR (10) NULL,
    [AgeCode]                VARCHAR (50) NULL,
    [WeeksWaitingAtTCI]      INT          NULL,
    [ServiceRequestTypeCode] VARCHAR (10) NULL,
    [IsCancelled]            BIT          NULL
);


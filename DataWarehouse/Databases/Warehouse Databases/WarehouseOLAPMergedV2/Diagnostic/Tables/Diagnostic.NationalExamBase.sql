﻿CREATE TABLE [Diagnostic].[NationalExamBase] (
    [NationalExamID]         INT           IDENTITY (1, 1) NOT NULL,
    [NationalExamCode]       VARCHAR (3)   NOT NULL,
    [NationalExam]           VARCHAR (200) NOT NULL,
    [NationalExamParentCode] VARCHAR (3)   NULL,
    [DM01DiagnosticTestCode] VARCHAR (3)   NULL,
    [ExamGroupCode]          VARCHAR (10)  NULL,
    [Created]                DATETIME2 (0) CONSTRAINT [df_Diagnostic_NationalExamBase_Created] DEFAULT (getdate()) NULL,
    [Updated]                DATETIME2 (0) CONSTRAINT [df_Diagnostic_NationalExamBase_Updated] DEFAULT (NULL) NULL,
    [ByWhom]                 VARCHAR (50)  CONSTRAINT [df_Diagnostic_NationalExamBase_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_NationalExam] PRIMARY KEY CLUSTERED ([NationalExamID] ASC),
    CONSTRAINT [FK_NationalExam_DM01DiagnosticTest] FOREIGN KEY ([DM01DiagnosticTestCode]) REFERENCES [Diagnostic].[DM01DiagnosticTest] ([DM01DiagnosticTestCode]),
    CONSTRAINT [FK_NationalExam_ExamGroup] FOREIGN KEY ([ExamGroupCode]) REFERENCES [Diagnostic].[ExamGroup] ([ExamGroupCode]),
    CONSTRAINT [FK_NationalExamBase_NationalExamBase] FOREIGN KEY ([NationalExamParentCode]) REFERENCES [Diagnostic].[NationalExamBase] ([NationalExamCode])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_NationalExam]
    ON [Diagnostic].[NationalExamBase]([NationalExamCode] ASC);


﻿CREATE TABLE [Diagnostic].[TimeBand] (
    [TimeBandID]            INT          NOT NULL,
    [TimeBandCode]          VARCHAR (10) NOT NULL,
    [TimeBand]              VARCHAR (50) NOT NULL,
    [CensusTimeBandCode]    VARCHAR (10) NOT NULL,
    [CensusTimeBand]        VARCHAR (50) NOT NULL,
    [ReportingTimeBandCode] VARCHAR (10) NOT NULL,
    [ReportingTimeBand]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TimeBand] PRIMARY KEY CLUSTERED ([TimeBandID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TimeBand]
    ON [Diagnostic].[TimeBand]([TimeBandCode] ASC);


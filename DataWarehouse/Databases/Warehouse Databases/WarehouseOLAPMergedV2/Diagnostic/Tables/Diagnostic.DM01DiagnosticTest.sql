﻿CREATE TABLE [Diagnostic].[DM01DiagnosticTest] (
    [DM01DiagnosticTestCode]     VARCHAR (3)   NOT NULL,
    [DM01DiagnosticTest]         VARCHAR (100) NOT NULL,
    [DM01DiagnosticTestCategory] VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_DM01DiagnosticTest] PRIMARY KEY CLUSTERED ([DM01DiagnosticTestCode] ASC)
);


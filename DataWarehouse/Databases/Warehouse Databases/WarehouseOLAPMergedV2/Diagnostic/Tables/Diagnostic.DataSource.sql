﻿CREATE TABLE [Diagnostic].[DataSource] (
    [DataSourceID]   INT          IDENTITY (1, 1) NOT NULL,
    [DataSourceCode] VARCHAR (10) NOT NULL,
    [DataSource]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_DataSource] PRIMARY KEY CLUSTERED ([DataSourceID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataSource]
    ON [Diagnostic].[DataSource]([DataSourceCode] ASC);


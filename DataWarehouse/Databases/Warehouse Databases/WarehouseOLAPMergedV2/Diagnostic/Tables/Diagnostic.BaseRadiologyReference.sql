﻿CREATE TABLE [Diagnostic].[BaseRadiologyReference] (
    [MergeRecno]  INT NOT NULL,
    [ContextID]   INT NOT NULL,
    [ExamID]      INT NOT NULL,
    [EventDateID] INT NOT NULL,
    [SpecialtyID] INT NOT NULL,
    [SiteID]      INT NOT NULL,
    [SexID]       INT NOT NULL
);


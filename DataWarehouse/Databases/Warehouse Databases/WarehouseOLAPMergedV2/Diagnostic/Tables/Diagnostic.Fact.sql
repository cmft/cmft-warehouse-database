﻿CREATE TABLE [Diagnostic].[Fact] (
    [MergeRecno]           INT NOT NULL,
    [ContextID]            INT NOT NULL,
    [EventDateID]          INT NOT NULL,
    [DirectorateID]        INT NOT NULL,
    [CCGID]                INT NOT NULL,
    [NationalExamID]       INT NOT NULL,
    [ExamID]               INT NOT NULL,
    [DataSourceID]         INT NOT NULL,
    [SpecialtyID]          INT NOT NULL,
    [ProfessionalCarerID]  INT NOT NULL,
    [SiteID]               INT NOT NULL,
    [SexID]                INT NOT NULL,
    [AgeID]                INT NOT NULL,
    [WeeksWaitingAtTCIID]  INT NOT NULL,
    [ServiceRequestTypeID] INT NULL,
    [IsCancelled]          BIT NULL,
    [IsReportable]         BIT NULL
);


﻿CREATE TABLE [Diagnostic].[ExamArea] (
    [ExamAreaCode] NCHAR (10)    NOT NULL,
    [ExamArea]     VARCHAR (100) NULL,
    CONSTRAINT [PK_ExamArea_1] PRIMARY KEY CLUSTERED ([ExamAreaCode] ASC)
);


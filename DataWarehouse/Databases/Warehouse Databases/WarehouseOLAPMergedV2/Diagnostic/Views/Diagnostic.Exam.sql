﻿

CREATE view [Diagnostic].[Exam] as

select
	 SourceContextCode
	,SourceContextID
	,SourceContext
	,SourceExamID = SourceValueID
	,SourceExamCode = SourceValueCode
	,SourceExam = SourceValue
	,LocalExamID = LocalValueID
	,LocalExamCode = LocalValueCode
	,LocalExam = LocalValue
	,NationalExamID = NationalValueID
	,NationalExamCode = NationalValueCode
	,NationalExam = NationalValue
from
	WH.Member
where
	AttributeCode = 'DIAGNOSTICEXAM'

union all

select
	 SourceContextCode = 'N/A'
	,SourceContextID = null
	,SourceContext = 'N/A'
	,SourceExamID = -1
	,SourceExamCode = '-1'
	,SourceExam = 'Not Specified'
	,LocalExamID = null
	,LocalExamCode = null
	,LocalExam = null
	,NationalExamID = null
	,NationalExamCode = null
	,NationalExam = null




﻿create view Diagnostic.NationalExam as

select
	 NationalExamID
	,NationalExamCode
	,NationalExam

	,NationalExamParentID =
		(
		select
			Parent.NationalExamID
		from
			Diagnostic.NationalExamBase Parent
		where
			Parent.NationalExamCode = NationalExamBase.NationalExamParentCode
		)

	,DM01DiagnosticTestCode
	,ExamGroupCode
from
	Diagnostic.NationalExamBase

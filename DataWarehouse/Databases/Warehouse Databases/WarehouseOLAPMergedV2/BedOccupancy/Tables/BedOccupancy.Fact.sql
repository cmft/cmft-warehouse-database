﻿CREATE TABLE [BedOccupancy].[Fact] (
    [MetricID]           INT    NOT NULL,
    [WardStayRecno]      INT    NOT NULL,
    [BaseEncounterRecno] BIGINT NOT NULL,
    [WardID]             INT    NOT NULL,
    [ConsultantID]       INT    NOT NULL,
    [SpecialtyID]        INT    NOT NULL,
    [SiteID]             INT    NOT NULL,
    [DirectorateID]      INT    NOT NULL,
    [BedOccupancyDateID] INT    NOT NULL,
    [Cases]              INT    NULL,
    [Duration]           INT    NULL
);


﻿CREATE TABLE [BedOccupancy].[Metric] (
    [MetricID]   INT          IDENTITY (1, 1) NOT NULL,
    [MetricCode] VARCHAR (8)  NOT NULL,
    [Metric]     VARCHAR (18) NOT NULL,
    CONSTRAINT [PK_Metric] PRIMARY KEY CLUSTERED ([MetricID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Metric]
    ON [BedOccupancy].[Metric]([MetricCode] ASC);


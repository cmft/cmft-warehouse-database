﻿CREATE TABLE [BedOccupancy].[SpecialtySector] (
    [SpecialtySectorID] INT           NOT NULL,
    [SpecialtySector]   VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_SpecialtySector] PRIMARY KEY CLUSTERED ([SpecialtySectorID] ASC)
);


﻿CREATE TABLE [BedOccupancy].[WardBedConfiguration] (
    [WardCode]          VARCHAR (20) NOT NULL,
    [ContextCode]       VARCHAR (10) NOT NULL,
    [StartDate]         DATE         NOT NULL,
    [EndDate]           DATE         NULL,
    [InpatientBeds]     SMALLINT     NOT NULL,
    [DaycaseBeds]       SMALLINT     NOT NULL,
    [SingleRoomBeds]    SMALLINT     NOT NULL,
    [SpecialtySectorID] INT          NULL,
    [Created]           DATETIME     CONSTRAINT [DF_WardBedConfiguration_Created] DEFAULT (getdate()) NULL,
    [Updated]           DATETIME     NULL,
    [ByWhom]            VARCHAR (50) CONSTRAINT [DF_WardBedConfiguration_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_WardBedConfiguration] PRIMARY KEY CLUSTERED ([WardCode] ASC, [ContextCode] ASC, [StartDate] ASC),
    CONSTRAINT [FK_WardBedConfiguration_SpecialtySector] FOREIGN KEY ([SpecialtySectorID]) REFERENCES [BedOccupancy].[SpecialtySector] ([SpecialtySectorID])
);


﻿CREATE TABLE [BedOccupancy].[SpecialtySpecialtySectorMap] (
    [SpecialtyCode]     VARCHAR (50) NOT NULL,
    [SpecialtySectorID] INT          NOT NULL,
    [Created]           DATETIME     CONSTRAINT [DF_SpecialtySpecialtySectorMap_Created] DEFAULT (getdate()) NULL,
    [Updated]           DATETIME     NULL,
    [ByWhom]            VARCHAR (50) CONSTRAINT [DF_SpecialtySpecialtySectorMap_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_SpecialtySpecialtySectorMap] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC, [SpecialtySectorID] ASC)
);


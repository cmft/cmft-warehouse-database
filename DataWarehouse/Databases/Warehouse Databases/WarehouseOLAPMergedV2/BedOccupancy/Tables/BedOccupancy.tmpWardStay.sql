﻿CREATE TABLE [BedOccupancy].[tmpWardStay] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ProviderSpellNo]     VARCHAR (50)  NOT NULL,
    [ContextCode]         VARCHAR (10)  NOT NULL,
    [StartDate]           DATE          NULL,
    [StartTime]           SMALLDATETIME NULL,
    [EndDate]             DATE          NULL,
    [EndTime]             SMALLDATETIME NULL,
    [DerivedEndDate]      DATE          NULL,
    PRIMARY KEY NONCLUSTERED ([MergeEncounterRecno] ASC)
);


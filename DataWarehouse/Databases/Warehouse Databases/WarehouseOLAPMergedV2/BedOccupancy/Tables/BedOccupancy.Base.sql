﻿CREATE TABLE [BedOccupancy].[Base] (
    [MergeBedOccupancyRecno]          BIGINT       IDENTITY (1, 1) NOT NULL,
    [ProviderSpellNo]                 VARCHAR (50) NULL,
    [WardStayMergeEncounterRecno]     INT          NOT NULL,
    [APCEncounterMergeEncounterRecno] INT          NOT NULL,
    [BedOccupancyDate]                DATE         NOT NULL,
    [Nights]                          INT          NULL,
    [ZeroLengthStays]                 INT          NOT NULL,
    CONSTRAINT [PK_Base_4] PRIMARY KEY CLUSTERED ([WardStayMergeEncounterRecno] ASC, [APCEncounterMergeEncounterRecno] ASC, [BedOccupancyDate] ASC)
);


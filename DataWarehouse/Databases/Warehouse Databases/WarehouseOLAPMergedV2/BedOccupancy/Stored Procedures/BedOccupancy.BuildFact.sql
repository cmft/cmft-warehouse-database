﻿
				
CREATE procedure [BedOccupancy].[BuildFact]
as

declare @to date = getdate()

declare @from date= '31 Mar 2013'


truncate table BedOccupancy.Fact


/******************************************************************************
** Feed OBD Counts for overlapping Ward Stay/Episode/Day combinations
** 20140729 DF	changed from union to insert, as job query was taking 5hrs+ to run
** 20140730	RR	added in reportable = 1 criteria
** 20140908 GC changed ProviderSpellNo to use GlobalProviderSpellNo
******************************************************************************/

insert
into
	BedOccupancy.Fact
(
	 MetricID
	,WardStayRecno
	,BaseEncounterRecno
	,WardID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,DirectorateID
	,BedOccupancyDateID
	,Cases
	,Duration
)
select
	 Metric.MetricID

	,WardStayRecno = WardStay.MergeEncounterRecno
	,BaseEncounterRecno = BaseEncounter.EncounterRecno

	/* Ward Stay Fields */
	,BaseWardStayReference.WardID

	/* Episode Fields */
	,BaseEncounterReference.ConsultantID
	,BaseEncounterReference.SpecialtyID
	,BaseWardStayReference.SiteID
	,StartDirectorate.DirectorateID

	/* Month Fields */
	,BedOccupancyDateID = BedOccupancyDate.DateID

	/* Indicators */
	,Cases =
		case

		when
			Admission.ManagementIntentionCode = 'D' --day case
		and	datediff(day, Admission.AdmissionDate, Discharge.DischargeDate) = 0
		then BedOccupancy.ZeroLengthStays

		when
			datediff(day, Admission.AdmissionDate, Discharge.DischargeDate) = 0
		then BedOccupancy.ZeroLengthStays

		else BedOccupancy.Nights

		end

	,Duration =
		datediff(
			 minute
			,case
			when WardStay.StartDate < BedOccupancy.BedOccupancyDate
			then BedOccupancy.BedOccupancyDate
			else WardStay.StartTime
			end

			,case
			when WardStay.EndDate > BedOccupancy.BedOccupancyDate
			or	WardStay.EndDate is null
			then 
				case when WardStay.EndDate = dateadd(day, 1, BedOccupancy.BedOccupancyDate)
				then WardStay.EndTime
				else dateadd(day, 1, BedOccupancy.BedOccupancyDate)
				end
			else 
				case
				when WardStay.EndTime is null
				then dateadd(day, 1, WardStay.EndDate)
				else WardStay.EndTime
				end
			end
		)

from
	BedOccupancy.Base BedOccupancy

inner join APC.BaseWardStay WardStay
on	WardStay.MergeEncounterRecno = BedOccupancy.WardStayMergeEncounterRecno

inner join APC.BaseEncounter BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BedOccupancy.APCEncounterMergeEncounterRecno
and BaseEncounter.Reportable = 1

inner join APC.BaseEncounter Admission 
on	Admission.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

inner join APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno
 
inner join APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodCode = Admission.AdmissionMethodCode
and	AdmissionMethod.SourceContextCode = Admission.ContextCode

inner join APC.BaseWardStayReference
on	BaseWardStayReference.MergeEncounterRecno = WardStay.MergeEncounterRecno

inner join APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join WH.Directorate StartDirectorate
on	StartDirectorate.DirectorateCode = Admission.StartDirectorateCode

inner join WH.Calendar BedOccupancyDate
on	BedOccupancyDate.TheDate = BedOccupancy.BedOccupancyDate

inner join BedOccupancy.Metric
on	Metric.MetricCode = 
		case
		when
			Admission.ManagementIntentionCode = 'D' --day case
		and	datediff(day, Admission.AdmissionDate, Discharge.DischargeDate) = 0
		then 'DAYCASE'

		when
			datediff(day, Admission.AdmissionDate, Discharge.DischargeDate) = 0
		then 'ZEROLOS'

		else 'NIGHT'

		end

where
	BedOccupancy.BedOccupancyDate > @from


/******************************************************************************
** Feed missing Episode nights and days to OBD Counts
******************************************************************************/

insert
into
	BedOccupancy.Fact
(
	 MetricID
	,WardStayRecno
	,BaseEncounterRecno
	,WardID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,DirectorateID
	,BedOccupancyDateID
	,Cases
	,Duration
)

select
	 MetricID =
		(
		select
			MetricID
		from
			BedOccupancy.Metric
		where
			MetricCode = 'MISSEPI' --Unmatched Episodes
		)

	,WardStayRecno = 1
	,BaseEncounterRecno = Episode.MergeEncounterRecno

	,BaseEncounterReference.StartWardID

	,ConsultantID = BaseEncounterReference.ConsultantID

	,SpecialtyID = BaseEncounterReference.SpecialtyID

	,BaseEncounterReference.StartSiteID

	,StartDirectorate.DirectorateID

	,Episode.DateID

	,Cases =
		Episode.Nights + Episode.ZeroLengthStays

	,Duration = null

from
	(   /* This finds combinations of Ward Stay and Day that overlap each other. */
	select
		 BaseEncounter.ProviderSpellNo
		,BaseEncounter.MergeEncounterRecno
		,Calendar.DateID
		,BaseEncounter.StartDirectorateCode

		/* Nights = lowest Stay/Month end date - highest Stay/Month start date */
		,Nights =
			datediff(
				day

				,case
				when BaseEncounter.EpisodeStartDate >= Calendar.TheDate
				then BaseEncounter.EpisodeStartDate
				else Calendar.TheDate
				end

				,case
				when coalesce(BaseEncounter.EpisodeEndDate, '6/6/2079') <= Calendar.TheDate
				then BaseEncounter.EpisodeEndDate
				else dateadd(day, 1, Calendar.TheDate)
				end
			)

		/* ZeroLengthStays = 1 or 0

		**  0 when
		**      ward stay is not zero length (end date = start date)
		**  or  ward stay is not part of a zero length spell (max end date = min start date)
		*/
		,ZeroLengthStays =
			case
			when coalesce(BaseEncounter.EpisodeEndDate, '6/6/2079') <> BaseEncounter.EpisodeStartDate
			then 0
			when
				(
				select
					max(coalesce(A.EpisodeEndDate,'6/6/2079'))
				from
					APC.BaseEncounter A
				where
					A.ProviderSpellNo = BaseEncounter.ProviderSpellNo
				) <>
				(
				select
					min(A.EpisodeStartDate)
				from
					APC.BaseEncounter A
				where
					A.ProviderSpellNo = BaseEncounter.ProviderSpellNo
				)
			then 0
			when
				(
				select
					max(coalesce(A.EndDate,'6/6/2079'))
				from
					APC.BaseWardStay A
				where
					A.ProviderSpellNo = BaseEncounter.ProviderSpellNo
				) <>
				(
				select
					min(A.StartDate)
				from
					APC.BaseWardStay A
				where
					A.ProviderSpellNo = BaseEncounter.ProviderSpellNo
				)
			then 0

			else 1
			end
	from
		APC.BaseEncounter BaseEncounter

	inner join WH.Calendar
	on	Calendar.TheDate <= coalesce(BaseEncounter.EpisodeEndDate, @to)
	and Calendar.TheDate >= BaseEncounter.EpisodeStartDate
	and BaseEncounter.Reportable = 1

	where
		Calendar.TheDate between dateadd(day, 1, @from) and @to
	and	not exists
		(
		select
			1
		from
			BedOccupancy.Base BedOccupancy
		where
			(
				BedOccupancy.APCEncounterMergeEncounterRecno = BaseEncounter.MergeEncounterRecno
			and	BedOccupancy.BedOccupancyDate = Calendar.TheDate
			and	BedOccupancy.Nights = 1
			)
		or
			(
				BedOccupancy.ProviderSpellNo = BaseEncounter.ProviderSpellNo
			and	BedOccupancy.ZeroLengthStays = 1
			)
		)

	) Episode

inner join  APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = Episode.MergeEncounterRecno

inner join WH.Directorate StartDirectorate
on	StartDirectorate.DirectorateCode = Episode.StartDirectorateCode

where
	Episode.Nights + Episode.ZeroLengthStays = 1



/******************************************************************************
** Feed Finished Ward Stays Counts
******************************************************************************/

insert
into
	BedOccupancy.Fact
(
	 MetricID
	,WardStayRecno
	,BaseEncounterRecno
	,WardID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,DirectorateID
	,BedOccupancyDateID
	,Cases
	,Duration
)

select
	 MetricID =
		(
		select
			MetricID
		from
			BedOccupancy.Metric
		where
			MetricCode = 'WARDSTAY'
		)

	,WardStayRecno = WardStay.MergeEncounterRecno
	,BaseEncounterRecno = BaseEncounter.MergeEncounterRecno

	/* Ward Stay Fields */
	,BaseWardStayReference.WardID

	/* Episode Fields */
	,ConsultantID = BaseEncounterReference.ConsultantID

	,SpecialtyID = BaseEncounterReference.SpecialtyID

	,SiteID = BaseWardStayReference.SiteID

	,StartDirectorate.DirectorateID

	/* Month Fields */
	,WardStayEndDate.DateID

	/* Indicators */
	,Cases = 1

	,Duration =
		datediff(
			 minute
			,WardStay.StartTime

			,case
			when WardStay.EndTime = WardStay.EndDate
			then WardStay.EndTime + 1
			else WardStay.EndTime
			end
		)

from
	APC.BaseWardStay WardStay

inner join BedOccupancy.Base BedOccupancy
on	BedOccupancy.WardStayMergeEncounterRecno = WardStay.MergeEncounterRecno
and	not exists
	(
	select
		1
	from
		BedOccupancy.Base Previous
	where
		Previous.WardStayMergeEncounterRecno = BedOccupancy.WardStayMergeEncounterRecno
	and	Previous.BedOccupancyDate > BedOccupancy.BedOccupancyDate
	)

inner join APC.BaseEncounter BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BedOccupancy.APCEncounterMergeEncounterRecno
and BaseEncounter.Reportable = 1

inner join APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join APC.BaseWardStayReference
on	BaseWardStayReference.MergeEncounterRecno = WardStay.MergeEncounterRecno

inner join WH.Directorate StartDirectorate
on	StartDirectorate.DirectorateCode = BaseEncounter.StartDirectorateCode

inner join WH.Calendar WardStayEndDate
on	WardStayEndDate.TheDate = WardStay.EndDate

where
	WardStay.EndDate is not null
and	WardStay.EndDate > @from


/******************************************************************************
** Feed Admissions Counts
******************************************************************************/

insert
into
	BedOccupancy.Fact
(
	 MetricID
	,WardStayRecno
	,BaseEncounterRecno
	,WardID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,DirectorateID
	,BedOccupancyDateID
	,Cases
	,Duration
)

select
	 MetricID =
		(
		select
			MetricID
		from
			BedOccupancy.Metric
		where
			MetricCode = 'ADMIT'
		)

	,WardStayRecno = WardStay.MergeEncounterRecno
	,BaseEncounterRecno = BaseEncounter.MergeEncounterRecno

	/* Ward Stay Fields */
	,BaseWardStayReference.WardID

	/* Episode Fields */
	,ConsultantID = BaseEncounterReference.ConsultantID

	,SpecialtyID = BaseEncounterReference.SpecialtyID

	,SiteID = BaseWardStayReference.SiteID

	,StartDirectorate.DirectorateID

	/* Month Fields */
	,AdmissionDate.DateID

	/* Indicators */
	,Cases = 1

	,Duration = null
from
	APC.BaseEncounter

inner join APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = BaseEncounter.MergeEncounterRecno
and BaseEncounter.Reportable = 1

inner join APC.BaseWardStay WardStay
on	WardStay.ProviderSpellNo = BaseEncounter.ProviderSpellNo
and	not exists
	(
	select
		1
	from
		APC.BaseWardStay Previous
	where
		Previous.ProviderSpellNo = BaseEncounter.ProviderSpellNo
	and	(
			Previous.StartTime < WardStay.StartTime
		or	(
				Previous.StartTime = WardStay.StartTime
			and	Previous.MergeEncounterRecno < WardStay.MergeEncounterRecno
			)
		)
	)

inner join APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join APC.BaseWardStayReference
on	BaseWardStayReference.MergeEncounterRecno = WardStay.MergeEncounterRecno

inner join WH.Directorate StartDirectorate
on	StartDirectorate.DirectorateCode = BaseEncounter.StartDirectorateCode

inner join WH.Calendar AdmissionDate
on	AdmissionDate.TheDate = BaseEncounter.AdmissionDate

where
	WardStay.EndDate > @from


/******************************************************************************
** Feed Finished Spells Counts
******************************************************************************/

insert
into
	BedOccupancy.Fact
(
	 MetricID
	,WardStayRecno
	,BaseEncounterRecno
	,WardID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,DirectorateID
	,BedOccupancyDateID
	,Cases
	,Duration
)

select
	 MetricID =
		(
		select
			MetricID
		from
			BedOccupancy.Metric
		where
			MetricCode = 'DISCH'
		)

	,WardStayRecno = WardStay.MergeEncounterRecno
	,BaseEncounterRecno = BaseEncounter.MergeEncounterRecno

	/* Ward Stay Fields */
	,BaseWardStayReference.WardID

	/* Episode Fields */
	,ConsultantID = BaseEncounterReference.ConsultantID

	,SpecialtyID = BaseEncounterReference.SpecialtyID

	,SiteID = BaseWardStayReference.SiteID

	,StartDirectorate.DirectorateID

	/* Month Fields */
	,DischargeDate.DateID

	/* Indicators */
	,Cases = 1

	,Duration = null

from
	APC.BaseEncounter BaseEncounter

inner join APC.BaseWardStay WardStay
on	WardStay.ProviderSpellNo = BaseEncounter.ProviderSpellNo
and BaseEncounter.Reportable = 1

and	not exists
	(
	select
		1
	from
		APC.BaseWardStay Previous
	where
		Previous.ProviderSpellNo = BaseEncounter.ProviderSpellNo
	and	(
			Previous.StartTime > WardStay.StartTime
		or	(
				Previous.StartTime = WardStay.StartTime
			and	Previous.MergeEncounterRecno > WardStay.MergeEncounterRecno
			)
		)
	)

inner join APC.Spell
on	Spell.DischargeEpisodeMergeRecno = BaseEncounter.MergeEncounterRecno

inner join APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join APC.BaseWardStayReference
on	BaseWardStayReference.MergeEncounterRecno = WardStay.MergeEncounterRecno

inner join WH.Directorate StartDirectorate
on	StartDirectorate.DirectorateCode = BaseEncounter.StartDirectorateCode

inner join WH.Calendar DischargeDate
on	DischargeDate.TheDate = BaseEncounter.DischargeDate

where
	BaseEncounter.DischargeDate is not null
and	BaseEncounter.DischargeDate > @from




--this is resource hungry so it has beed split out from above

truncate table BedOccupancy.tmpWardStay
insert into
	BedOccupancy.tmpWardStay
(
MergeEncounterRecno,ProviderSpellNo,ContextCode,StartDate ,StartTime ,EndDate,EndTime,DerivedEndDate
)
select
	 WardStay.MergeEncounterRecno
	,WardStay.ProviderSpellNo
	,WardStay.ContextCode
	,StartDate = cast(WardStay.StartDate as date)
	,WardStay.StartTime
	,EndDate = cast(WardStay.EndDate as date)
	,WardStay.EndTime

	,DerivedEndDate =
		CAST(
			coalesce(
				(
				select
					BaseEncounter.DischargeDate
				from
					APC.BaseEncounter BaseEncounter
				where
					BaseEncounter.ProviderSpellNo = WardStay.ProviderSpellNo
				and	BaseEncounter.EpisodeStartTime = BaseEncounter.AdmissionTime
				and	BaseEncounter.DischargeDate > @from
				)
				,@to
			)
			as date
		)

from
	APC.BaseWardStay WardStay
where
	WardStay.EndDate is null

insert into BedOccupancy.tmpWardStay 
		(
		MergeEncounterRecno
			,ProviderSpellNo
			,ContextCode
			,StartDate
			,StartTime
			,EndDate
			,EndTime
			,DerivedEndDate
		)
		select
			 MergeEncounterRecno
			,ProviderSpellNo
			,ContextCode
			,StartDate
			,StartTime
			,EndDate
			,EndTime
			,DerivedEndDate = EndDate
		from
			APC.BaseWardStay
		where
			EndDate is not null


insert
into
	BedOccupancy.Fact
(
	 MetricID
	,WardStayRecno
	,BaseEncounterRecno
	,WardID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,DirectorateID
	,BedOccupancyDateID
	,Cases
	,Duration
)
select
	 MetricID =
		(
		select
			MetricID
		from
			BedOccupancy.Metric
		where
			MetricCode = 'MISSSTAY' --Unmatched Stays
		)

	,ws2.WardStayRecno
	,BaseEncounterRecno = -1
	,BaseWardStayReference.WardID

	,ConsultantID =
		(
		select
			SourceConsultantID
		from
			WH.Consultant
		where
			Consultant.SourceConsultantCode = '-1'
		and	Consultant.SourceContextCode = ws2.ContextCode
		)

	,SpecialtyID =
		(
		select
			SourceSpecialtyID
		from
			WH.Specialty
		where
			Specialty.SourceSpecialtyCode = '-1'
		and	Specialty.SourceContextCode = ws2.ContextCode
		)

	,BaseWardStayReference.SiteID

	,DirectorateID =
		(
		select
			DirectorateID
		from
			WH.Directorate
		where
			Directorate.DirectorateCode = 'N/A'
		)

	,DateID = ws2.BedOccupancyDateID

	,Cases =
		ws2.Nights + ws2.ZeroLengthStays

	,Duration = null

from
	(   /* This finds combinations of Ward Stay and Day that overlap each other. */
	select
		 ws.ProviderSpellNo
		,ws.ContextCode
		,WardStayRecno = ws.MergeEncounterRecno
		,BedOccupancyDateID = Calendar.DateID

		/* Nights = lowest Stay/Month end date - highest Stay/Month start date */
		,Nights =
			datediff(
				day

				,case
				when ws.StartDate >= Calendar.TheDate
				then ws.StartDate
				else Calendar.TheDate
				end

				,case
				when coalesce(ws.EndDate, '6/6/2079') <= Calendar.TheDate
				then ws.EndDate
				else dateadd(day, 1, Calendar.TheDate)
				end
			)

		/* ZeroLengthStays = 1 or 0

		**  0 when
		**      ward stay is not zero length (end date = start date)
		**  or  ward stay is not part of a zero length spell (max end date = min start date)
		*/
		,ZeroLengthStays =
			case
			when coalesce(ws.EndDate, '6/6/2079') != ws.StartDate
			then 0
			when
				(
				select
					max(coalesce(A.EpisodeEndDate,'6/6/2079'))
				from
					APC.BaseEncounter A
				where
					A.ProviderSpellNo = ws.ProviderSpellNo
				) <>
				(
				select
					min(A.EpisodeStartDate)
				from
					APC.BaseEncounter A
				where
					A.ProviderSpellNo = ws.ProviderSpellNo
				)
			then 0
			when
				(
				select
					max(coalesce(A.EndDate,'6/6/2079'))
				from
					APC.BaseWardStay A
				where
					A.ProviderSpellNo = ws.ProviderSpellNo
				) <>
				(
				select
					min(A.StartDate)
				from
					APC.BaseWardStay A
				where
					A.ProviderSpellNo = ws.ProviderSpellNo
				)
			then 0

			else 1
			end
	from
		BedOccupancy.tmpWardStay ws

	inner join WH.Calendar
	on	Calendar.TheDate between ws.StartDate
	and	ws.DerivedEndDate
	
	where
		Calendar.TheDate between dateadd(day, 1, @from) and @to
	and	not exists
		(
		select
			1
		from
			BedOccupancy.Base BedOccupancy
		where
			BedOccupancy.WardStayMergeEncounterRecno = ws.MergeEncounterRecno
		and	BedOccupancy.BedOccupancyDate = Calendar.TheDate
		)
	) ws2

inner join APC.BaseWardStayReference
on	BaseWardStayReference.MergeEncounterRecno = ws2.WardStayRecno

where
	Nights + ZeroLengthStays = 1


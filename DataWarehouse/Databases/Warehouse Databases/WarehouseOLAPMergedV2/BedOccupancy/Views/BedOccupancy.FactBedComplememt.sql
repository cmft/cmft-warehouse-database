﻿
CREATE view [BedOccupancy].[FactBedComplememt] as

with

WardBedConfigurationByDay as
	(
	select
		 WardBedConfigurationDateID = Calendar.DateID
		,WardID = Ward.SourceWardID
		,WardBedConfiguration.SpecialtySectorID

		,WardBedConfiguration.InpatientBeds
		,WardBedConfiguration.DaycaseBeds
		,WardBedConfiguration.SingleRoomBeds
	from
		BedOccupancy.WardBedConfiguration

	inner join WH.Calendar Calendar
	on	Calendar.TheDate between WardBedConfiguration.StartDate
	and	
		coalesce(
			WardBedConfiguration.EndDate
			,getdate()
		)

	inner join APC.Ward
	on	Ward.SourceWardCode = WardBedConfiguration.WardCode
	and	Ward.SourceContextCode = WardBedConfiguration.ContextCode

	--where
	--	exists
	--	(
	--	select
	--		1
	--	from
	--		BedOccupancy.Fact
	--	where
	--		Fact.BedOccupancyDateID = Calendar.DateID
	--	)
	)


select
	 WardBedConfigurationByDay.WardBedConfigurationDateID
	,WardBedConfigurationByDay.WardID
	,WardBedConfigurationByDay.SpecialtySectorID
	,Metric.MetricID

	,AvailableBeds = WardBedConfigurationByDay.InpatientBeds

from
	WardBedConfigurationByDay

inner join BedOccupancy.Metric
on	Metric.MetricCode = 'NIGHT'

where
	WardBedConfigurationByDay.InpatientBeds > 0

union all

select
	 WardBedConfigurationByDay.WardBedConfigurationDateID
	,WardBedConfigurationByDay.WardID
	,WardBedConfigurationByDay.SpecialtySectorID
	,Metric.MetricID

	,AvailableBeds = WardBedConfigurationByDay.DaycaseBeds

from
	WardBedConfigurationByDay

inner join BedOccupancy.Metric
on	Metric.MetricCode = 'DAYCASE'

where
	WardBedConfigurationByDay.DaycaseBeds > 0


﻿CREATE TABLE [OP].[BaseDiary] (
    [MergeRecno]               INT           IDENTITY (1, 1) NOT NULL,
    [EncounterChecksum]        INT           NOT NULL,
    [CensusDate]               DATE          NOT NULL,
    [ContextCode]              VARCHAR (10)  NOT NULL,
    [SourceUniqueID]           VARCHAR (254) NOT NULL,
    [ClinicCode]               VARCHAR (8)   NULL,
    [SessionCode]              VARCHAR (8)   NULL,
    [SessionDescription]       VARCHAR (255) NULL,
    [SessionStartTime]         SMALLDATETIME NULL,
    [SessionEndTime]           SMALLDATETIME NULL,
    [ReasonForCancellation]    VARCHAR (30)  NULL,
    [SessionPeriod]            VARCHAR (3)   NULL,
    [DoctorCode]               VARCHAR (10)  NULL,
    [Units]                    INT           NULL,
    [UsedUnits]                INT           NULL,
    [FreeUnits]                INT           NULL,
    [ValidAppointmentTypeCode] VARCHAR (23)  NULL,
    CONSTRAINT [PK_BaseDiary] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [SourceUniqueID] ASC, [CensusDate] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BaseDiary]
    ON [OP].[BaseDiary]([MergeRecno] ASC);


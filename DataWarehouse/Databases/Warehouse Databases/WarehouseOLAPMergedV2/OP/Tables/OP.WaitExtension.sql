﻿CREATE TABLE [OP].[WaitExtension] (
    [EncounterRecNo]                INT           NOT NULL,
    [ActivitySpecialty]             VARCHAR (100) NULL,
    [ActivityNationalSpecialtyCode] VARCHAR (20)  NULL,
    [ActivityFirstAttendanceCode]   VARCHAR (20)  NULL,
    [DiagnosticReportLineCode]      VARCHAR (3)   NULL,
    [DiagnosticNationalExamCode]    VARCHAR (3)   NULL
);


﻿CREATE TABLE [OP].[BaseDiaryReference] (
    [MergeRecno]         INT NOT NULL,
    [ContextID]          INT NOT NULL,
    [CensusDateID]       INT NOT NULL,
    [ClinicID]           INT NOT NULL,
    [DoctorID]           INT NOT NULL,
    [SessionStartDateID] INT NOT NULL,
    [SessionStartTimeID] INT NOT NULL,
    [SessionEndDateID]   INT NOT NULL,
    [SessionEndTimeID]   INT NOT NULL,
    CONSTRAINT [PK_OP_Base_Diary_Reference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


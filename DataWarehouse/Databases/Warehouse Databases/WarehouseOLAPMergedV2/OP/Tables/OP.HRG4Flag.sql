﻿CREATE TABLE [OP].[HRG4Flag] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [FlagCode]            VARCHAR (50)  NULL,
    [Created]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_HRG4Flag_1] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [OP].[BaseOperation] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [OperationCode]       VARCHAR (10)  NOT NULL,
    [OperationDate]       SMALLDATETIME NULL,
    [Created]             DATETIME      NOT NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseOperation] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


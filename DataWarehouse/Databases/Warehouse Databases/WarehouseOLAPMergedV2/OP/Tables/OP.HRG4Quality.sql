﻿CREATE TABLE [OP].[HRG4Quality] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [QualityTypeCode]     VARCHAR (10)  NULL,
    [QualityCode]         VARCHAR (10)  NULL,
    [QualityMessage]      VARCHAR (255) NULL,
    [Created]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_HRGQuality_1] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


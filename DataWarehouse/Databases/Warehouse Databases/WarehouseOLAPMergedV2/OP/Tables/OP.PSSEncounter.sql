﻿CREATE TABLE [OP].[PSSEncounter] (
    [MergeEncounterRecno]                INT           NOT NULL,
    [NationalProgrammeOfCareServiceCode] VARCHAR (50)  NULL,
    [ServiceLine]                        VARCHAR (50)  NULL,
    [Created]                            DATETIME      NULL,
    [ByWhom]                             VARCHAR (255) NULL,
    CONSTRAINT [PK_PSSEncounter_1] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


﻿CREATE TABLE [OP].[HRG4Encounter] (
    [MergeEncounterRecno]   INT           NOT NULL,
    [HRGCode]               VARCHAR (10)  NULL,
    [GroupingMethodFlag]    VARCHAR (10)  NULL,
    [DominantOperationCode] VARCHAR (10)  NULL,
    [Created]               DATETIME      NULL,
    [ByWhom]                VARCHAR (255) NULL,
    CONSTRAINT [PK_HRGEncounter_1] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


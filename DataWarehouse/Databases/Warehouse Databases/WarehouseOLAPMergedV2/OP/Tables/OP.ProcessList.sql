﻿CREATE TABLE [OP].[ProcessList] (
    [MergeRecno] INT           NOT NULL,
    [Action]     NVARCHAR (10) NULL,
    CONSTRAINT [PK_ProcessList] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


﻿CREATE TABLE [OP].[BaseFertilityReference] (
    [MergeRecno]      INT          NOT NULL,
    [ContextID]       INT          NOT NULL,
    [EncounterDateID] INT          NULL,
    [TreatmentID]     INT          NULL,
    [Created]         DATETIME     NULL,
    [Updated]         DATETIME     NULL,
    [ByWhom]          VARCHAR (50) NULL
);


﻿CREATE TABLE [OP].[HRG4Unbundled] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [HRGCode]             VARCHAR (10)  NOT NULL,
    [Created]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_HRGUnbundled] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [OP].[BaseEncounterReference] (
    [MergeEncounterRecno]       INT           NOT NULL,
    [ContextID]                 INT           NOT NULL,
    [SexID]                     INT           NULL,
    [AgeID]                     INT           NULL,
    [NHSNumberStatusID]         INT           NULL,
    [EthnicOriginID]            INT           NULL,
    [MaritalStatusID]           INT           NULL,
    [ReligionID]                INT           NULL,
    [SiteID]                    INT           NULL,
    [AppointmentDateID]         INT           NULL,
    [ClinicID]                  INT           NULL,
    [AdminCategoryID]           INT           NULL,
    [SourceOfReferralID]        INT           NULL,
    [ReasonForReferralID]       INT           NULL,
    [PriorityID]                INT           NULL,
    [FirstAttendanceID]         INT           NULL,
    [AttendanceStatusID]        INT           NULL,
    [AttendanceOutcomeID]       INT           NULL,
    [AppointmentTypeID]         INT           NULL,
    [ConsultantID]              INT           NULL,
    [SpecialtyID]               INT           NULL,
    [ReferralConsultantID]      INT           NULL,
    [ReferralSpecialtyID]       INT           NULL,
    [RTTSpecialtyID]            INT           NULL,
    [RTTCurrentStatusID]        INT           NULL,
    [ReferringConsultantID]     INT           NULL,
    [TreatmentFunctionID]       INT           NULL,
    [DerivedFirstAttendanceID]  INT           NULL,
    [Created]                   DATETIME      NULL,
    [Updated]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (255) NULL,
    [AppointmentResidenceCCGID] INT           NULL,
    [DoctorID]                  INT           NULL,
    CONSTRAINT [PK_OP_Base_Encounter_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_OP_BaseEncounterReference_1]
    ON [OP].[BaseEncounterReference]([ContextID] ASC)
    INCLUDE([Updated]);


GO
CREATE NONCLUSTERED INDEX [IX_BaseEncounterReference_AppointmentDateID]
    ON [OP].[BaseEncounterReference]([AppointmentDateID] ASC)
    INCLUDE([MergeEncounterRecno], [SexID], [SourceOfReferralID], [AttendanceStatusID], [ConsultantID], [SpecialtyID]);


GO
CREATE NONCLUSTERED INDEX [IX_AppointmentDateID]
    ON [OP].[BaseEncounterReference]([AppointmentDateID] ASC)
    INCLUDE([MergeEncounterRecno], [ReferralSpecialtyID], [DerivedFirstAttendanceID]);


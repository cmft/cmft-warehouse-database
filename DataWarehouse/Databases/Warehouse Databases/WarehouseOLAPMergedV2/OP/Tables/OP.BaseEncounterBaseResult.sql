﻿CREATE TABLE [OP].[BaseEncounterBaseResult] (
    [MergeEncounterRecno] INT NOT NULL,
    [MergeResultRecno]    INT NOT NULL,
    CONSTRAINT [PK_BaseEncounterBasePositiveTestResult] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeResultRecno] ASC)
);


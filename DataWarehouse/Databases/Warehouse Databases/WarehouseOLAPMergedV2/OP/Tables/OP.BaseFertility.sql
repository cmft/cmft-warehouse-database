﻿CREATE TABLE [OP].[BaseFertility] (
    [MergeRecno]      INT           IDENTITY (1, 1) NOT NULL,
    [FertilityRecno]  INT           NOT NULL,
    [SourceID]        VARCHAR (150) NULL,
    [HospitalNumber]  VARCHAR (15)  NULL,
    [PatientForename] VARCHAR (100) NULL,
    [PatientSurname]  VARCHAR (100) NULL,
    [EncounterDate]   DATE          NULL,
    [TreatmentType]   VARCHAR (20)  NULL,
    [DateOfBirth]     DATE          NULL,
    [Age]             SMALLINT      NULL,
    [ReferralSource]  VARCHAR (20)  NULL,
    [InterfaceCode]   VARCHAR (3)   NOT NULL,
    [ContextCode]     VARCHAR (20)  NOT NULL,
    [Created]         DATETIME      NULL,
    [Updated]         DATETIME      NULL,
    [ByWhom]          VARCHAR (50)  NULL
);


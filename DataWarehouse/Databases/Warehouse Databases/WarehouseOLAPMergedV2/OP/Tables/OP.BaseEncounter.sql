﻿
CREATE TABLE [OP].[BaseEncounter](
	[MergeEncounterRecno] [int] IDENTITY(1,1) NOT NULL,
	[EncounterRecno] [int] NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[SourcePatientNo] [varchar](20) NULL,
	[SourceEncounterNo] [varchar](20) NULL,
	[PatientTitle] [varchar](10) NULL,
	[PatientForename] [varchar](100) NULL,
	[PatientSurname] [varchar](100) NULL,
	[DateOfBirth] [datetime] NULL,
	[DateOfDeath] [smalldatetime] NULL,
	[SexCode] [varchar](10) NULL,
	[NHSNumber] [varchar](17) NULL,
	[NHSNumberStatusCode] [varchar](10) NULL,
	[DistrictNo] [varchar](12) NULL,
	[Postcode] [varchar](8) NULL,
	[PatientAddress1] [varchar](100) NULL,
	[PatientAddress2] [varchar](100) NULL,
	[PatientAddress3] [varchar](100) NULL,
	[PatientAddress4] [varchar](100) NULL,
	[DHACode] [varchar](3) NULL,
	[EthnicOriginCode] [varchar](10) NULL,
	[MaritalStatusCode] [varchar](10) NULL,
	[ReligionCode] [varchar](10) NULL,
	[RegisteredGpCode] [varchar](8) NULL,
	[RegisteredGpPracticeCode] [varchar](8) NULL,
	[SiteCode] [varchar](10) NULL,
	[AppointmentDate] [smalldatetime] NULL,
	[AppointmentTime] [smalldatetime] NULL,
	[ClinicCode] [varchar](10) NULL,
	[AdminCategoryCode] [varchar](10) NULL,
	[SourceOfReferralCode] [varchar](10) NULL,
	[ReasonForReferralCode] [varchar](10) NULL,
	[PriorityCode] [varchar](10) NULL,
	[FirstAttendanceFlag] [varchar](10) NULL,
	[DNACode] [varchar](10) NULL,
	[AppointmentStatusCode] [varchar](10) NULL,
	[CancelledByCode] [varchar](10) NULL,
	[TransportRequiredFlag] [varchar](10) NULL,
	[AttendanceOutcomeCode] [varchar](10) NULL,
	[AppointmentTypeCode] [varchar](10) NULL,
	[DisposalCode] [varchar](10) NULL,
	[ConsultantCode] [varchar](20) NULL,
	[SpecialtyCode] [varchar](10) NULL,
	[ReferralConsultantCode] [varchar](20) NULL,
	[ReferralSpecialtyCode] [varchar](10) NULL,
	[BookingTypeCode] [varchar](10) NULL,
	[CasenoteNo] [varchar](16) NULL,
	[AppointmentCreateDate] [smalldatetime] NULL,
	[EpisodicGpCode] [varchar](8) NULL,
	[EpisodicGpPracticeCode] [varchar](6) NULL,
	[DoctorCode] [varchar](20) NULL,
	[PrimaryDiagnosisCode] [varchar](10) NULL,
	[SubsidiaryDiagnosisCode] [varchar](10) NULL,
	[SecondaryDiagnosisCode1] [varchar](10) NULL,
	[SecondaryDiagnosisCode2] [varchar](10) NULL,
	[SecondaryDiagnosisCode3] [varchar](10) NULL,
	[SecondaryDiagnosisCode4] [varchar](10) NULL,
	[SecondaryDiagnosisCode5] [varchar](10) NULL,
	[SecondaryDiagnosisCode6] [varchar](10) NULL,
	[SecondaryDiagnosisCode7] [varchar](10) NULL,
	[SecondaryDiagnosisCode8] [varchar](10) NULL,
	[SecondaryDiagnosisCode9] [varchar](10) NULL,
	[SecondaryDiagnosisCode10] [varchar](10) NULL,
	[SecondaryDiagnosisCode11] [varchar](10) NULL,
	[SecondaryDiagnosisCode12] [varchar](10) NULL,
	[PrimaryOperationCode] [varchar](10) NULL,
	[PrimaryProcedureDate] [smalldatetime] NULL,
	[SecondaryProcedureCode1] [varchar](10) NULL,
	[SecondaryProcedureDate1] [smalldatetime] NULL,
	[SecondaryProcedureCode2] [varchar](10) NULL,
	[SecondaryProcedureDate2] [smalldatetime] NULL,
	[SecondaryProcedureCode3] [varchar](10) NULL,
	[SecondaryProcedureDate3] [smalldatetime] NULL,
	[SecondaryProcedureCode4] [varchar](10) NULL,
	[SecondaryProcedureDate4] [smalldatetime] NULL,
	[SecondaryProcedureCode5] [varchar](10) NULL,
	[SecondaryProcedureDate5] [smalldatetime] NULL,
	[SecondaryProcedureCode6] [varchar](10) NULL,
	[SecondaryProcedureDate6] [smalldatetime] NULL,
	[SecondaryProcedureCode7] [varchar](10) NULL,
	[SecondaryProcedureDate7] [smalldatetime] NULL,
	[SecondaryProcedureCode8] [varchar](10) NULL,
	[SecondaryProcedureDate8] [smalldatetime] NULL,
	[SecondaryProcedureCode9] [varchar](10) NULL,
	[SecondaryProcedureDate9] [smalldatetime] NULL,
	[SecondaryProcedureCode10] [varchar](10) NULL,
	[SecondaryProcedureDate10] [smalldatetime] NULL,
	[SecondaryProcedureCode11] [varchar](10) NULL,
	[SecondaryProcedureDate11] [smalldatetime] NULL,
	[PurchaserCode] [varchar](10) NULL,
	[ProviderCode] [varchar](10) NULL,
	[ContractSerialNo] [varchar](6) NULL,
	[ReferralDate] [smalldatetime] NULL,
	[RTTPathwayID] [varchar](25) NULL,
	[RTTPathwayCondition] [varchar](20) NULL,
	[RTTStartDate] [smalldatetime] NULL,
	[RTTEndDate] [smalldatetime] NULL,
	[RTTSpecialtyCode] [varchar](10) NULL,
	[RTTCurrentProviderCode] [varchar](10) NULL,
	[RTTCurrentStatusCode] [varchar](10) NULL,
	[RTTCurrentStatusDate] [smalldatetime] NULL,
	[RTTCurrentPrivatePatientFlag] [bit] NULL,
	[RTTOverseasStatusFlag] [bit] NULL,
	[RTTPeriodStatusCode] [varchar](10) NULL,
	[AppointmentCategoryCode] [varchar](10) NULL,
	[AppointmentCreatedBy] [varchar](10) NULL,
	[AppointmentCancelDate] [smalldatetime] NULL,
	[LastRevisedDate] [smalldatetime] NULL,
	[LastRevisedBy] [varchar](10) NULL,
	[OverseasStatusFlag] [varchar](10) NULL,
	[PatientChoiceCode] [varchar](10) NULL,
	[ScheduledCancelReasonCode] [varchar](10) NULL,
	[PatientCancelReason] [varchar](50) NULL,
	[DischargeDate] [smalldatetime] NULL,
	[QM08StartWaitDate] [smalldatetime] NULL,
	[QM08EndWaitDate] [smalldatetime] NULL,
	[DestinationSiteCode] [varchar](10) NULL,
	[EBookingReferenceNo] [varchar](50) NULL,
	[InterfaceCode] [varchar](5) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NULL,
	[LocalAdminCategoryCode] [varchar](10) NULL,
	[PCTCode] [varchar](10) NULL,
	[LocalityCode] [varchar](10) NULL,
	[AgeCode] [varchar](27) NULL,
	[HRGCode] [varchar](10) NULL,
	[Cases] [int] NULL,
	[LengthOfWait] [int] NULL,
	[IsWardAttender] [bit] NULL,
	[RTTActivity] [int] NULL,
	[RTTBreachStatusCode] [varchar](10) NULL,
	[ClockStartDate] [smalldatetime] NULL,
	[RTTBreachDate] [smalldatetime] NULL,
	[RTTTreated] [int] NULL,
	[DirectorateCode] [varchar](5) NULL,
	[ReferralSpecialtyTypeCode] [varchar](10) NULL,
	[LastDNAorPatientCancelledDate] [smalldatetime] NULL,
	[ReferredByCode] [varchar](10) NULL,
	[ReferrerCode] [varchar](10) NULL,
	[AppointmentOutcomeCode] [varchar](10) NULL,
	[MedicalStaffTypeCode] [varchar](10) NULL,
	[ContextCode] [varchar](8) NOT NULL,
	[TreatmentFunctionCode] [varchar](10) NULL,
	[CommissioningSerialNo] [varchar](10) NULL,
	[DerivedFirstAttendanceFlag] [varchar](1) NULL,
	[AttendanceIdentifier] [varchar](40) NULL,
	[RegisteredGpAtAppointmentCode] [varchar](8) NULL,
	[RegisteredGpPracticeAtAppointmentCode] [varchar](6) NULL,
	[ReferredByConsultantCode] [varchar](10) NULL,
	[ReferredByGpCode] [varchar](10) NULL,
	[ReferredByGdpCode] [varchar](10) NULL,
	[PseudoPostcode] [varchar](10) NULL,
	[CCGCode] [varchar](10) NULL,
	[PostcodeAtAppointment] [varchar](8) NULL,
	[EpisodicPostcode] [varchar](8) NULL,
	[GpCodeAtAppointment] [varchar](8) NULL,
	[GpPracticeCodeAtAppointment] [varchar](8) NULL,
	[EpisodicSiteCode] [varchar](5) NULL,
	[AppointmentResidenceCCGCode] [varchar](10) NULL,
	[EncounterChecksum] [int] NULL,
 CONSTRAINT [PK_BaseEncounter_1] PRIMARY KEY NONCLUSTERED 
(
	[MergeEncounterRecno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



GO
CREATE UNIQUE CLUSTERED INDEX [IX_OP_BaseEncounter]
    ON [OP].[BaseEncounter]([EncounterRecno] ASC, [ContextCode] ASC);


GO
CREATE NONCLUSTERED INDEX [OP_BaseEncounter_1]
    ON [OP].[BaseEncounter]([ContextCode] ASC, [Updated] ASC)
    INCLUDE([MergeEncounterRecno], [SexCode], [NHSNumberStatusCode], [EthnicOriginCode], [MaritalStatusCode], [ReligionCode], [SiteCode], [AppointmentDate], [ClinicCode], [AdminCategoryCode], [SourceOfReferralCode], [ReasonForReferralCode], [PriorityCode], [FirstAttendanceFlag], [DNACode], [AttendanceOutcomeCode], [AppointmentTypeCode], [ConsultantCode], [SpecialtyCode], [ReferralConsultantCode], [ReferralSpecialtyCode], [RTTSpecialtyCode], [RTTCurrentStatusCode], [Created], [ByWhom], [TreatmentFunctionCode], [DerivedFirstAttendanceFlag], [ReferredByConsultantCode]);


GO
CREATE NONCLUSTERED INDEX [[IX_OP_DistrictNo_AppointmentTime]
    ON [OP].[BaseEncounter]([DistrictNo] ASC, [AppointmentTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_opBE-AD]
    ON [OP].[BaseEncounter]([AppointmentDate] ASC)
    INCLUDE([MergeEncounterRecno], [EncounterRecno], [PatientTitle], [PatientForename], [PatientSurname], [DateOfBirth], [NHSNumber], [Postcode], [RegisteredGpCode], [RegisteredGpPracticeCode], [AppointmentTime], [PrimaryDiagnosisCode], [PrimaryOperationCode], [PurchaserCode], [RTTPathwayID], [PCTCode], [HRGCode], [ReferredByCode], [ContextCode], [SourcePatientNo], [SourceEncounterNo], [SiteCode], [ConsultantCode], [SpecialtyCode], [PrimaryProcedureDate], [DirectorateCode], [AppointmentOutcomeCode]);


GO
CREATE NONCLUSTERED INDEX [IX_SourcePatientNo_SourceEncounterNo_ContextCode_ClinicCode_ReferralSpecialtyCode]
    ON [OP].[BaseEncounter]([SourcePatientNo] ASC, [SourceEncounterNo] ASC, [ContextCode] ASC, [ClinicCode] ASC, [ReferralSpecialtyCode] ASC)
    INCLUDE([MergeEncounterRecno], [AppointmentTime]);


GO
CREATE NONCLUSTERED INDEX [IX_OP_DistrictNo_AppointmentTime]
    ON [OP].[BaseEncounter]([DNACode] ASC, [PrimaryDiagnosisCode] ASC)
    INCLUDE([MergeEncounterRecno], [SourcePatientNo], [AppointmentDate], [CasenoteNo], [SecondaryDiagnosisCode1], [SecondaryDiagnosisCode2], [SecondaryDiagnosisCode3], [SecondaryDiagnosisCode4], [SecondaryDiagnosisCode5], [SecondaryDiagnosisCode6], [SecondaryDiagnosisCode7], [SecondaryDiagnosisCode8], [SecondaryDiagnosisCode9], [SecondaryDiagnosisCode10], [SecondaryDiagnosisCode11], [SecondaryDiagnosisCode12], [InterfaceCode]);





﻿CREATE TABLE [OP].[PSSQuality] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [QualityTypeCode]     VARCHAR (50)  NULL,
    [QualityCode]         VARCHAR (50)  NULL,
    [QualityMessage]      VARCHAR (50)  NULL,
    [Created]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_PSSQuality_1] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


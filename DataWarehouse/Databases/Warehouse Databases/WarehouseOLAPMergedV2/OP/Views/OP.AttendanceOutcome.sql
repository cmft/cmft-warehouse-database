﻿create view [OP].[AttendanceOutcome] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAttendanceOutcomeID = SourceValueID
	,SourceAttendanceOutcomeCode = SourceValueCode
	,SourceAttendanceOutcome = SourceValue
	,LocalAttendanceOutcomeID = LocalValueID
	,LocalAttendanceOutcomeCode = LocalValueCode
	,LocalAttendanceOutcome = LocalValue
	,NationalAttendanceOutcomeID = NationalValueID
	,NationalAttendanceOutcomeCode = NationalValueCode
	,NationalAttendanceOutcome = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'OUTATT'








﻿






create view [OP].[Doctor] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceDoctorID = SourceValueID
	,SourceDoctorCode = SourceValueCode
	,SourceDoctor = SourceValue
	,LocalDoctorID = LocalValueID
	,LocalDoctorCode = LocalValueCode
	,LocalDoctor = LocalValue
	,NationalDoctorID = NationalValueID
	,NationalDoctorCode = NationalValueCode
	,NationalDoctor = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'OPDOCTOR'









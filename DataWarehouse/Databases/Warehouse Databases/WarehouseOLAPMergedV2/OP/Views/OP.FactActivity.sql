﻿







CREATE view [OP].[FactActivity] as

--20150519	RR	PH advised to change TEL Clinic to OPFUP Tel clinic for the Anticoag POD/HRG
--20150703	RR	PH has requested 360 (GUM) activity be excluded from the OP Activity Report, This comes from Freeze at present SLAM.Encounter, added to the Reportable case statement in 3 queries below.
--20150914	RR	brought through Stimulations				
				
select
	 Appointment.MergeEncounterRecno
	,Appointment.EncounterDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'OP'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlagAllocation.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,Appointment.DirectorateID
	--,SpecialtyID = Appointment.SpecialtyID
	,SpecialtyID = Appointment.ReferralSpecialtyID -- changed to referral specialty 15.5.15 - DG
	,Appointment.ServiceID
	,HRGID = HRGBase.HRGID
	,Appointment.ContextID
	,Appointment.Cases
	,Value = null
from
	OP.FactEncounter Appointment

inner join OP.BaseEncounter Base
on	Base.MergeEncounterRecno = Appointment.MergeEncounterRecno

inner join OP.BaseEncounterReference BaseReference
on	BaseReference.MergeEncounterRecno = Appointment.MergeEncounterRecno

inner join WH.Specialty
on	Specialty.SourceSpecialtyID = Appointment.SpecialtyID

inner join OP.AppointmentType
on	AppointmentType.SourceAppointmentTypeID = BaseReference.AppointmentTypeID

inner join OP.FirstAttendance
on	FirstAttendance.SourceFirstAttendanceID = BaseReference.FirstAttendanceID

inner join OP.Metric OPMetric
on	OPMetric.MetricID = Appointment.MetricID
--appointments
and	OPMetric.MetricCode in
	(
	 'FATT'
	,'RATT'
	)

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = OPMetric.MetricCode

inner join Allocation.DatasetAllocation PointOfDelivery
on	PointOfDelivery.DatasetRecno = Appointment.MergeEncounterRecno
and	PointOfDelivery.DatasetCode = 'OP'
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.DatasetAllocation ContractFlag
on	ContractFlag.DatasetRecno = Appointment.MergeEncounterRecno
and	ContractFlag.DatasetCode = 'OP'
and	ContractFlag.AllocationTypeID = 9

inner join Allocation.Allocation ContractFlagAllocation
on	ContractFlag.AllocationID = ContractFlagAllocation.AllocationID

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	Appointment.EncounterDateID = Calendar.DateID

inner join OP.HRG4Encounter -- need hrgs in fact main tables!
on HRG4Encounter.MergeEncounterRecno = Appointment.MergeEncounterRecno

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(HRG4Encounter.HRGCode, 'N/A')

where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 Base.MergeEncounterRecno
	,BaseReference.AppointmentDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'GUM'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable = cast(1 as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,DirectorateBase.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,Service.AllocationID
	,HRGID = HRGBase.HRGID
	,BaseReference.ContextID
	,Cases = 1
	,Value = null

from
	GUM.BaseEncounter Base

inner join GUM.BaseEncounterReference BaseReference
on	BaseReference.MergeEncounterRecno = Base.MergeEncounterRecno

inner join OP.AppointmentType
on	AppointmentType.SourceAppointmentTypeID = BaseReference.AppointmentTypeID

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = '360'
and	Specialty.SourceContextCode = Base.ContextCode

inner join Allocation.DatasetAllocation Service
on	Service.DatasetRecno = Base.MergeEncounterRecno
and	Service.DatasetCode = 'GUM'
and	Service.AllocationTypeID = 2

inner join Allocation.DatasetAllocation DirectorateAllocation
on	DirectorateAllocation.DatasetRecno = Base.MergeEncounterRecno
and	DirectorateAllocation.DatasetCode = 'GUM'
and	DirectorateAllocation.AllocationTypeID = 10

inner join Allocation.Allocation Directorate
on	Directorate.AllocationID = DirectorateAllocation.AllocationID

inner join WH.DirectorateBase
on	DirectorateBase.DirectorateCode = Directorate.SourceAllocationID

inner join Allocation.DatasetAllocation PointOfDelivery
on	PointOfDelivery.DatasetRecno = Base.MergeEncounterRecno
and	PointOfDelivery.DatasetCode = 'GUM'
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.DatasetAllocation ContractFlag
on	ContractFlag.DatasetRecno = Base.MergeEncounterRecno
and	ContractFlag.DatasetCode = 'GUM'
and	ContractFlag.AllocationTypeID = 9

inner join OP.Metric OPMetric
on	case AppointmentType.SourceAppointmentTypeCode
	when '1' then 'FATT'
	when '2' then 'RATT'
	when '3' then 'RATT'
	end = OPMetric.MetricCode

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = OPMetric.MetricCode

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	BaseReference.AppointmentDateID = Calendar.DateID

inner join Allocation.DatasetAllocation HRGAllocation
on	HRGAllocation.DatasetRecno = Base.MergeEncounterRecno
and	HRGAllocation.DatasetCode = 'GUM'
and	HRGAllocation.AllocationTypeID = 6

inner join Allocation.Allocation HRG
on	HRG.AllocationID = HRGAllocation.AllocationID

inner join WH.HRGBase
on	HRGBase.HRGCode = HRG.SourceAllocationID


where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all


select
	 Base.MergeEncounterRecno
	,BaseReference.TestDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'ANTI'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable = cast(1 as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,DirectorateBase.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,Service.ServiceID
	,HRGID = HRGBase.HRGID
	,BaseReference.ContextID
	,Cases = 1
	,Value = null

from
	Anticoagulation.BaseEncounter Base

inner join Anticoagulation.BaseEncounterReference BaseReference
on	BaseReference.MergeEncounterRecno = Base.MergeEncounterRecno

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = '324'
and	Specialty.SourceContextCode = Base.ContextCode

inner join WH.Service
on	Service.SourceServiceID = 104 --Unassigned, needs allocating

inner join WH.DirectorateBase
on	DirectorateBase.DirectorateCode = '81' --needs allocating

inner join OP.Clinic
on	Clinic.SourceClinicID = BaseReference.ClinicID

inner join 
		(
		select
			AllocationID
			,SourceAllocationID
			,Allocation
			,AllocationTypeID
		from
			Allocation.Allocation
		where
			not exists
					(
					select
						1
					from
						Allocation.Allocation Later
					where
						Later.SourceAllocationID = Allocation.SourceAllocationID
					and	Later.AllocationTypeID = Allocation.AllocationTypeID
					and	Later.AllocationID > Allocation.AllocationID
					)
		) PointOfDelivery
on	case
	when Clinic.SourceClinic = 'Path lab w.in'
	then 'OPFUPSPCL'
	else coalesce(
				case Clinic.PODCode
				when 'NPOPFA' then 'OPFASPCL'
				when 'NPOPFUP' then 'OPFUPSPCL'
				--20150519 RR PH advised to change TEL Clinic to OPFUP Tel clinic
				--when 'NPOPNFF' then 'TELCLIN'
				when 'NPOPNFF' then 'OPFUPTEL' -- 20150804 This was resulting in nulls, PH advised WF01C
				end
			,'-1')
	end	= SourceAllocationID 
and AllocationTypeID = 5

inner join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = 'NCBPS00z'
and	ContractFlag.AllocationTypeID = 9

inner join OP.Metric OPMetric
on	case
	when Clinic.SourceClinic = 'Path lab w.in'
	then 'RATT'
	else
		case Clinic.PODCode
		when 'NPOPFUP' then 'RATT' 
		else 'FATT'
		end
	end	= OPMetric.MetricCode

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = OPMetric.MetricCode

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	BaseReference.TestDateID = Calendar.DateID

left join WH.HRGBase --should be an inner but left for performance reasons!!!
on	case
	when Clinic.SourceClinic = 'Path lab w.in'
	then 'WF01A'
	else
		case Clinic.PODCode
		when 'NPOPFA' then 'WF01B' 
		when 'NPOPFUP' then 'WF01A' 
		--20150519 RR PH advised to change TEL Clinic to OPFUP Tel clinic
		--when 'NPOPNFF' then 'TELCLIN'
		-- when 'NPOPNFF' then 'OPFUPTEL' -- 20150804 This was resulting in nulls, PH advised WF01C
		when 'NPOPNFF' then 'WF01C' 
		else 'N/A'
		end
	end	= HRGBase.HRGCode

where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all


select
	 Fertility.MergeRecno
	,FertilityReference.EncounterDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'Fertility'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable = cast(1 as bit)
	,Metric.MetricID
	
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = ContractFlag.AllocationID 
	,DirectorateBase.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,Service.AllocationID
	,HRGID = HRGBase.HRGID
	
	,FertilityReference.ContextID
	,Cases = 1
	,Value = null
from
	OP.BaseFertility Fertility
	
inner join OP.BaseFertilityReference FertilityReference
on	Fertility.MergeRecno = FertilityReference.MergeRecno

inner join WH.Specialty
on Specialty.SourceSpecialtyCode = 'GYN1'
and Specialty.SourceContextCode = 'CEN||ACU'--Base.ContextCode

inner join OP.Metric OPMetric
on MetricCode = 'RATT'
	
inner join WH.ActivityMetric Metric
on	Metric.MetricCode = OPMetric.MetricCode

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	FertilityReference.EncounterDateID = Calendar.DateID


inner join Allocation.DatasetAllocation Service
on	Service.DatasetRecno = Fertility.MergeRecno
and	Service.DatasetCode = 'Fertility'
and	Service.AllocationTypeID = 2

inner join Allocation.DatasetAllocation DirectorateAllocation
on	DirectorateAllocation.DatasetRecno = Fertility.MergeRecno
and	DirectorateAllocation.DatasetCode = 'Fertility'
and	DirectorateAllocation.AllocationTypeID = 10

inner join Allocation.Allocation Directorate
on	Directorate.AllocationID = DirectorateAllocation.AllocationID

inner join WH.DirectorateBase
on	DirectorateBase.DirectorateCode = Directorate.SourceAllocationID

inner join Allocation.DatasetAllocation PointOfDelivery
on	PointOfDelivery.DatasetRecno = Fertility.MergeRecno
and	PointOfDelivery.DatasetCode = 'Fertility'
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.DatasetAllocation ContractFlag
on	ContractFlag.DatasetRecno = Fertility.MergeRecno
and	ContractFlag.DatasetCode = 'Fertility'
and	ContractFlag.AllocationTypeID = 9

inner join Allocation.DatasetAllocation HRGAllocation
on	HRGAllocation.DatasetRecno = Fertility.MergeRecno
and	HRGAllocation.DatasetCode = 'Fertility'
and	HRGAllocation.AllocationTypeID = 6

inner join Allocation.Allocation HRG
on	HRG.AllocationID = HRGAllocation.AllocationID

inner join WH.HRGBase
on	HRGBase.HRGCode = HRG.SourceAllocationID


where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')
and EncounterDate is not null
and coalesce(ReferralSource,'-1') not like 'Self%'
and coalesce(TreatmentType,'-1') <> 'FET'


union all


select
	 MergeEncounterRecno = Appointment.Contracting_ID
	,EncounterDateID = Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'OP'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				when Appointment.PoD_Code = 'RT-UB'
				then 0
				when Treatment_Function_Code = '360'
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,Specialty.SourceSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Context.ContextID
	,Cases = Activity_Actual
	,Value = Price_Actual
from
	SLAM.Encounter Appointment

inner join OP.Metric OPMetric
on	OPMetric.MetricCode =
		case left(Appointment.PoD_Code, 4)
		when 'OPFA' then 'FATT'
		else 'RATT' -- Phil needs to include derived first attendance flag as this is not robust
		end

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = OPMetric.MetricCode

left join 
		(
		select
			AllocationID
			,SourceAllocationID
			,Allocation
			,AllocationTypeID
		from
			Allocation.Allocation
		where
			not exists
					(
					select
						1
					from
						Allocation.Allocation Later
					where
						Later.SourceAllocationID = Allocation.SourceAllocationID
					and	Later.AllocationTypeID = Allocation.AllocationTypeID
					and	Later.AllocationID > Allocation.AllocationID
					)
		) PointOfDelivery
on	PointOfDelivery.SourceAllocationID = Appointment.PoD_Code
and	PointOfDelivery.AllocationTypeID = 5


left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = Appointment.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	Appointment.Spell_Admission_Date = Calendar.TheDate

inner join WH.Directorate
on	Directorate.DirectorateCode = Appointment.Directorate_Code

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = Appointment.Internal_Specialty_Code
and	Specialty.SourceContextCode = Appointment.ContextCode

inner join WH.Context
on	Appointment.ContextCode = Context.ContextCode

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(Appointment.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = Appointment.Feed_Name
--and LatestExtractMonth.Extract_Type = Appointment.Extract_Type
--and LatestExtractMonth.Extract_Month = Appointment.Extract_Month
--and LatestExtractMonth.Extract_Year = Appointment.Extract_Year

where
	Appointment.Feed_Name = 'OP'
and	Appointment.Extract_Type = 'Freeze'
--and	Appointment.Is_Current = 1
and	Appointment.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')
and Appointment.PoD_Code not in ('Mat-antenatal', 'Mat-Postnatal')


union all

select
	 MergeEncounterRecno = Appointment.Contracting_ID
	,EncounterDateID = Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'OP'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				when Appointment.PoD_Code = 'RT-UB'
				then 0
				when Treatment_Function_Code = '360'
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,Specialty.SourceSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Context.ContextID
	,Cases = Activity_Actual
	,Value = Price_Actual
from
	SLAM.Encounter Appointment

inner join OP.Metric OPMetric
on	OPMetric.MetricCode =
		case left(Appointment.PoD_Code, 4)
		when 'OPFA' then 'FATT'
		else 'RATT' -- Phil needs to include derived first attendance flag as this is not robust
		end

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = OPMetric.MetricCode

left join 
		(
		select
			AllocationID
			,SourceAllocationID
			,Allocation
			,AllocationTypeID
		from
			Allocation.Allocation
		where
			not exists
					(
					select
						1
					from
						Allocation.Allocation Later
					where
						Later.SourceAllocationID = Allocation.SourceAllocationID
					and	Later.AllocationTypeID = Allocation.AllocationTypeID
					and	Later.AllocationID > Allocation.AllocationID
					)
		) PointOfDelivery
on	PointOfDelivery.SourceAllocationID = Appointment.PoD_Code
and	PointOfDelivery.AllocationTypeID = 5


left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = Appointment.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	Appointment.Spell_Admission_Date = Calendar.TheDate

inner join WH.Directorate
on	Directorate.DirectorateCode = Appointment.Directorate_Code

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = Appointment.Treatment_Function_Code
and	Specialty.SourceContextCode = Appointment.ContextCode

inner join WH.Context
on	Appointment.ContextCode = Context.ContextCode

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(Appointment.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = Appointment.Feed_Name
--and LatestExtractMonth.Extract_Type = Appointment.Extract_Type
--and LatestExtractMonth.Extract_Month = Appointment.Extract_Month
--and LatestExtractMonth.Extract_Year = Appointment.Extract_Year

where
	Appointment.Feed_Name = 'Anticoagulant'
and	Appointment.Extract_Type = 'Freeze'
--and	Appointment.Is_Current = 1
and	Appointment.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')


union all

select
	 MergeEncounterRecno = Appointment.Contracting_ID
	,EncounterDateID = Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'OP'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end
					as bit)
	,Reportable = 
			cast(
				case 
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				when PoD_Code in ('NELXBD', 'NELNEXBD', 'ELXBD', 'BPT', 'BPT-UC', 'RT-UB')
				then 0
				when Treatment_Function_Code = '360'
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,Specialty.SourceSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Context.ContextID
	,Cases = Activity_Actual
	,Value = Price_Actual
from
	SLAM.Encounter Appointment

inner join OP.Metric OPMetric
on	OPMetric.MetricCode =
		case left(Appointment.PoD_Code, 4)
		when 'OPFA' then 'FATT'
		else 'RATT' -- Phil needs to include derived first attendance flag as this is not robust
		end

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = OPMetric.MetricCode

left join 
		(
		select
			AllocationID
			,SourceAllocationID
			,Allocation
			,AllocationTypeID
		from
			Allocation.Allocation
		where
			not exists
					(
					select
						1
					from
						Allocation.Allocation Later
					where
						Later.SourceAllocationID = Allocation.SourceAllocationID
					and	Later.AllocationTypeID = Allocation.AllocationTypeID
					and	Later.AllocationID > Allocation.AllocationID
					)
		) PointOfDelivery
on	PointOfDelivery.SourceAllocationID = Appointment.PoD_Code
and	PointOfDelivery.AllocationTypeID = 5


left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = Appointment.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	Appointment.Spell_Admission_Date = Calendar.TheDate

inner join WH.Directorate
on	Directorate.DirectorateCode = Appointment.Directorate_Code

inner join WH.Specialty
on	Specialty.SourceSpecialtyCode = Appointment.Treatment_Function_Code
and	Specialty.SourceContextCode = Appointment.ContextCode

inner join WH.Context
on	Appointment.ContextCode = Context.ContextCode

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(Appointment.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = Appointment.Feed_Name
--and LatestExtractMonth.Extract_Type = Appointment.Extract_Type
--and LatestExtractMonth.Extract_Month = Appointment.Extract_Month
--and LatestExtractMonth.Extract_Year = Appointment.Extract_Year

where
	Appointment.Feed_Name = 'GUM'
and	Appointment.Extract_Type = 'Freeze'
--and	Appointment.Is_Current = 1
and	Appointment.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')


union all

select
	 MergeEncounterRecno = Appointment.Contracting_ID
	,EncounterDateID = Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'OP'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlag.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				when Appointment.PoD_Code = 'RT-UB'
				then 0
				when Treatment_Function_Code = '360'
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,Specialty.SourceSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Context.ContextID
	,Cases = Activity_Actual
	,Value = Price_Actual
from
	SLAM.Encounter Appointment

inner join OP.Metric OPMetric
on	OPMetric.MetricCode =
		case left(Appointment.PoD_Code, 4)
		when 'OPFA' then 'FATT'
		else 'RATT' -- Phil needs to include derived first attendance flag as this is not robust
		end

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = OPMetric.MetricCode

left join 
		(
		select
			AllocationID
			,SourceAllocationID
			,Allocation
			,AllocationTypeID
		from
			Allocation.Allocation
		where
			not exists
					(
					select
						1
					from
						Allocation.Allocation Later
					where
						Later.SourceAllocationID = Allocation.SourceAllocationID
					and	Later.AllocationTypeID = Allocation.AllocationTypeID
					and	Later.AllocationID > Allocation.AllocationID
					)
		) PointOfDelivery
on	PointOfDelivery.SourceAllocationID = Appointment.PoD_Code
and	PointOfDelivery.AllocationTypeID = 5


left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = Appointment.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	Appointment.Spell_Admission_Date = Calendar.TheDate

inner join WH.Directorate
on	Directorate.DirectorateCode = Appointment.Directorate_Code

left join WH.Specialty
on	Specialty.SourceSpecialtyCode = Appointment.Treatment_Function_Code
and	Specialty.SourceContextCode = 'CEN||ACU' -- 17/09/2015 Context is null in SLAM data, discussed with PH, who advised to hard code for now.

inner join WH.Context
on	Context.ContextCode = 'CEN||ACU' -- 17/09/2015 Context is null in SLAM data, discussed with PH, who advised to hard code for now.

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(Appointment.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = Appointment.Feed_Name
--and LatestExtractMonth.Extract_Type = Appointment.Extract_Type
--and LatestExtractMonth.Extract_Month = Appointment.Extract_Month
--and LatestExtractMonth.Extract_Year = Appointment.Extract_Year

where
	Appointment.Feed_Name = 'StimStart'
and	Appointment.Extract_Type = 'Freeze'
--and	Appointment.Is_Current = 1
and	Appointment.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')




























































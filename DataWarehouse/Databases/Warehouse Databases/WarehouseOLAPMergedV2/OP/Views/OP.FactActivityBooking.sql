﻿








--/****** Object:  View [OP].[FactActivityBooking]    Script Date: 08/12/2015 16:27:15 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


CREATE view [OP].[FactActivityBooking] as


--select
--	 WaitingList.MergeEncounterRecno
--	,TCIDateID = WaitingList.AppointmentDateID
--	,SourceID = ActivitySource.SourceID
--	,ConsolidatedView = 
--					cast(
--						case
--						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
--						then 1
--						else 0
--						end
--					as bit)
--	,Metric.MetricID
--	,PointOfDeliveryID = 
--				(
--				select
--					AllocationID
--				from
--					Allocation.Allocation 
--				where
--					SourceAllocationID = '-1'
--				and	AllocationTypeID = 5
--				)
--	,WaitingList.DirectorateID
--	,WaitingList.SpecialtyID
--	,WaitingList.ServiceID
--	,WaitingList.Cases
--	,WaitingList.CensusDateID
--from
--	OPWL.FactEncounter WaitingList

--inner join OPWL.BaseEncounter Base
--on	Base.MergeEncounterRecno = WaitingList.MergeEncounterRecno

--inner join OP.FirstAttendance
--on	FirstAttendance.SourceFirstAttendanceID = WaitingList.FirstAttendanceID

--inner join WH.ActivityMetric Metric
--on	Metric.MetricCode =
--		case
--		when FirstAttendance.NationalFirstAttendanceCode = '2'
--		then 'RATT'
--		else 'FATT'
--		end

--inner join WH.Specialty
--on	Specialty.SourceSpecialtyID = WaitingList.SpecialtyID

--inner join WH.ActivitySource
--on ActivitySource.Source = 'Warehouse'

--inner join WH.Calendar
--on	WaitingList.AppointmentDateID = Calendar.DateID

--where
--	WaitingList.WaitTypeID = 50 --Active
--and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

----include ... what is ReferralSpecialtyType?
----and	coalesce(WaitingList.ReferralSpecialtyTypeCode, 'I') = 'I'


----exclude specialties
--and	Specialty.NationalSpecialtyCode not in
--	(
--	 '990' --"Other" specialties
--	,'192' --Critical care
--	)

----exclude SCBU
--and	not
--	(
--		Specialty.NationalSpecialtyCode in
--			(
--			 '422' --Neonatology
--			,'424' --Well Babies
--			)
--	and	WaitingList.DirectorateID in
--		(
--		select
--			 Directorate.DirectorateID
--		from
--			WH.Directorate
--		where
--			Directorate.DirectorateCode =
--				'30' --Women and Children's
--		)
--	)

----exclude private patients
--and	not exists
--	(
--	select
--		 1
--	from
--		WH.AdministrativeCategory
--	where
--		AdministrativeCategory.NationalAdministrativeCategoryCode = '02'
--	and	AdministrativeCategory.SourceAdministrativeCategoryCode = Base.AdminCategoryCode
--	and	AdministrativeCategory.SourceContextCode = Base.ContextCode
--	)


----Trafford exclusions
--and	not
--	(
--		Base.ContextCode = 'TRA||UG'
--	and	(
--			Specialty.NationalSpecialtyCode in
--			(
--			 '141' --Restorative Dentistry
--			,'143' --Orthodontics
--			,'711' --Child and Adolescent Psychiatry
--			,'840' --Audiology
--			,'960' --Allied Health Professional Episode
--			)

--		or	exists
--			(
--			select
--				1
--			from
--				OP.AppointmentType
--			where
--				AppointmentType.SourceContextCode = 'TRA||UG'
--			and
--				(
--					AppointmentType.LocalAppointmentType like '%Green Card%'
--				or	AppointmentType.LocalAppointmentType like '%Glucose Tolerance Test%'
--				or	AppointmentType.LocalAppointmentType like 'Fol Up Ward Visit (SpN)-TRAFF%'
--				or	AppointmentType.LocalAppointmentType like 'New Ward Visit (SpN)-TRAFF%'
--				or	AppointmentType.LocalAppointmentType like '%Tinnitus Review - Adult FU%'
--				or	AppointmentType.LocalAppointmentType like '%Tinnitus Assessment - Adult New%'
--				)

--			and	AppointmentType.SourceAppointmentTypeCode = Base.AppointmentTypeCode
--			and	AppointmentType.SourceContextCode = Base.ContextCode

--			)

--		or	Base.AppointmentStatusCode <> 'V'

--		or	FirstAttendance.SourceFirstAttendanceCode in
--			(
--			 '1'	--First Attendance Face To Face (TRA||UG:1)
--			,'2'	--Follow-Up Attendance Face To Face (TRA||UG:2)
--			,'11'	--Cmft First Attendance Face To Face (TRA||UG:11)
--			,'12'	--Cmft Follow-Up Attendance Face To Face (TRA||UG:12)
--			)

----exclude non-numeric national specialties (???)
--		or	isnumeric(Specialty.NationalSpecialtyCode) <> 1
--		)
--	)

--and	WaitingList.AppointmentDateID !=
--		(
--		select
--			DateID
--		from
--			WH.Calendar
--		where
--			TheDate = '1 Jan 1900' --load valid dates only
--		)

--and	WaitingList.WaitTargetID = 5


				
select
	 Appointment.MergeEncounterRecno
	,TCIDateID = Appointment.EncounterDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'OP'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end
					as bit)
	,Reportable =	
			cast(
				case
				when ContractFlagAllocation.SourceAllocationID in ('DIAGNOSTIC', 'DUPLICATE', 'PRIVATE', 'CRI',' DA', 'CAMHS_CONS', 'HEARAID', 'GREENCARD','TGH-OP-EXCL-WARDV','TGH-OP-EXCL-AUDTIN','TGH-OP-EXCL-CARDIO','TGH-OP-EXCL-MSURG','TGH-OP-EXCL-NCWC','TGH-OP-EXCL-RTT','TGH-OP-EXCL-TEL','PENDINGCV') 
				then 0
				else 1
				end
			as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID
	,ContractFlagID = ContractFlag.AllocationID
	,Appointment.DirectorateID
	,SpecialtyID = Appointment.ReferralSpecialtyID
	,Appointment.ServiceID
	,HRGID = 
			(
			select
				HRGID
			from
				 WH.HRGBase
			where
				HRGBase.HRGCode = 'N/A'
			)
	,Appointment.ContextID
	,Appointment.Cases
	,Value = null
	,CensusDateID 
			
from
	OP.FactEncounter Appointment

inner join OP.BaseEncounter Base
on	Base.MergeEncounterRecno = Appointment.MergeEncounterRecno

inner join OP.BaseEncounterReference BaseReference
on	BaseReference.MergeEncounterRecno = Appointment.MergeEncounterRecno

inner join WH.Specialty
on	Specialty.SourceSpecialtyID = Appointment.SpecialtyID

inner join OP.AppointmentType
on	AppointmentType.SourceAppointmentTypeID = BaseReference.AppointmentTypeID

inner join OP.FirstAttendance
on	FirstAttendance.SourceFirstAttendanceID = BaseReference.FirstAttendanceID

inner join OP.Metric OPMetric
on	OPMetric.MetricID = Appointment.MetricID
--appointments
and	OPMetric.MetricCode in
	(
	 'FAPP'
	,'RAPP'
	)

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 
	case OPMetric.MetricCode
	when 'FAPP' then 'FATT'
	when 'RAPP' then 'RATT'
	end 

inner join Allocation.DatasetAllocation PointOfDelivery
on	PointOfDelivery.DatasetRecno = Appointment.MergeEncounterRecno
and	PointOfDelivery.DatasetCode = 'OP'
and	PointOfDelivery.AllocationTypeID = 5

inner join Allocation.DatasetAllocation ContractFlag
on	ContractFlag.DatasetRecno = Appointment.MergeEncounterRecno
and	ContractFlag.DatasetCode = 'OP'
and	ContractFlag.AllocationTypeID = 9

inner join Allocation.Allocation ContractFlagAllocation
on	ContractFlag.AllocationID = ContractFlagAllocation.AllocationID

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	Appointment.EncounterDateID = Calendar.DateID

inner join WH.Census
on	CensusDate = 
	(
	select
		max(CensusDate)
	from
		OPWL.FactEncounter WaitingList

	inner join WH.Census
	on	WaitingList.CensusDateID = Census.CensusDateID
	)

where
	Calendar.TheDate > cast(getdate() as date)










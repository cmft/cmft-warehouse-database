﻿CREATE view [OP].[ReasonForReferral] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceReasonForReferralID = SourceValueID
	,SourceReasonForReferralCode = SourceValueCode
	,SourceReasonForReferral = SourceValue
	,LocalReasonForReferralID = LocalValueID
	,LocalReasonForReferralCode = LocalValueCode
	,LocalReasonForReferral = LocalValue
	,NationalReasonForReferralID = NationalValueID
	,NationalReasonForReferralCode = NationalValueCode
	,NationalReasonForReferral = NationalValue
FROM
	WH.Member
where 
	AttributeCode = 'REAREF'





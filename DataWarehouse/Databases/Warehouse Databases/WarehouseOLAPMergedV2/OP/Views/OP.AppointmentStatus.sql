﻿




CREATE view [OP].[AppointmentStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAppointmentStatusID = SourceValueID
	,SourceAppointmentStatusCode = SourceValueCode
	,SourceAppointmentStatus = SourceValue
	,LocalAppointmentStatusID = LocalValueID
	,LocalAppointmentStatusCode = LocalValueCode
	,LocalAppointmentStatus = LocalValue
	,NationalAppointmentStatusID = NationalValueID
	,NationalAppointmentStatusCode = NationalValueCode
	,NationalAppointmentStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'APPSTA'







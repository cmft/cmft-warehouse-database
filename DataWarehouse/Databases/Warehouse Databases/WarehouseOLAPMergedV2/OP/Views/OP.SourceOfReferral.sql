﻿CREATE view [OP].[SourceOfReferral] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceSourceOfReferralID = SourceValueID
	,SourceSourceOfReferralCode = SourceValueCode
	,SourceSourceOfReferral = SourceValue
	,LocalSourceOfReferralID = LocalValueID
	,LocalSourceOfReferralCode = LocalValueCode
	,LocalSourceOfReferral = LocalValue
	,NationalSourceOfReferralID = NationalValueID
	,NationalSourceOfReferralCode = NationalValueCode
	,NationalSourceOfReferral = NationalValue
	,ReferralType =
			case 
			when NationalValueCode in('03','92') then 'GP Referral'
			else 'Other Referral'
			end 
FROM
	WH.Member
where 
	AttributeCode = 'SCRFOP'






﻿

create view OP.[Operation]

as

SELECT [MergeEncounterRecno]
      ,[SequenceNo]
      ,[OperationCode]
      ,[OperationDate]
      ,[Created]
      ,[Updated]
      ,[ByWhom]
  FROM OP.[BaseOperation]


﻿CREATE view [OP].[AppointmentType] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAppointmentTypeID = SourceValueID
	,SourceAppointmentTypeCode = SourceValueCode
	,SourceAppointmentType = SourceValue
	,LocalAppointmentTypeID = LocalValueID
	,LocalAppointmentTypeCode = LocalValueCode
	,LocalAppointmentType = LocalValue
	,NationalAppointmentTypeID = NationalValueID
	,NationalAppointmentTypeCode = NationalValueCode
	,NationalAppointmentType = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'APPTYP'







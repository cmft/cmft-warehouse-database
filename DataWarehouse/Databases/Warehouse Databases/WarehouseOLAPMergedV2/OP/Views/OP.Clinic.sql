﻿

CREATE view [OP].[Clinic] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceClinicID = SourceValueID
	,SourceClinicCode = SourceValueCode
	,SourceClinic = SourceValue
	,LocalClinicID = LocalValueID
	,LocalClinicCode = LocalValueCode
	,LocalClinic = LocalValue
	,NationalClinicID = NationalValueID
	,NationalClinicCode = NationalValueCode
	,NationalClinic = NationalValue
	,ReportToLocationCode = Clinic.ReportToLocationCode
	,PODCode = ClinicPODSite.PODCode 
FROM
	WH.Member

left outer join [$(Warehouse)].PAS.Clinic
on	Member.SourceValueCode = Clinic.ClinicCode
and	Member.SourceContextCode = 'CEN||PAS'

left outer join [$(Warehouse)].Anticoagulation.ClinicPODSite
on	Member.SourceValue = ClinicPODSite.ClinicCode
and	Member.SourceContextCode = 'TRA||DAWN'

where
	AttributeCode = 'CLINIC'










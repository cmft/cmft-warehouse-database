﻿


CREATE view [OP].[FactDiary] as

select
	 Base.MergeRecno
	,CensusDateID = CensusDate.DateID
	,BaseReference.ContextID
	 
	,EncounterDateID = BaseReference.SessionStartDateID
	,EncounterTimeID = BaseReference.SessionStartTimeID
	
	,BaseReference.SessionEndDateID
	,BaseReference.SessionEndTimeID
	
	,BaseReference.ClinicID
	,BaseReference.DoctorID

	,Base.Units
	,Base.FreeUnits
	,Base.UsedUnits	
from
	OP.BaseDiary Base

inner join OP.BaseDiaryReference BaseReference
on	BaseReference.MergeRecno = Base.MergeRecno
	
cross join 
	(
	select
		Calendar.DateID
		,Calendar.TheDate
	from
		WH.Calendar
	where
		Calendar.TheDate <= cast(GETDATE() as date)
	and	(
			datepart(day, dateadd(day, 1, Calendar.TheDate)) = 1
		or	datename(dw, Calendar.TheDate) = 'Sunday'
		or	Calendar.TheDate between dateadd(day, -14, cast(GETDATE() as date)) and cast(GETDATE() as date)
		)
	) CensusDate

where
	Base.CensusDate <= CensusDate.TheDate
and	not exists
	(
	select
		1
	from
		OP.BaseDiary Previous
	where
		Previous.SourceUniqueID = Base.SourceUniqueID
	and	Previous.ContextCode = Base.ContextCode
	and	Previous.CensusDate <= CensusDate.TheDate
	and	Previous.CensusDate > Base.CensusDate
	)


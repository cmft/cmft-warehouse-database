﻿CREATE view [OP].[FirstAttendance] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceFirstAttendanceID = SourceValueID
	,SourceFirstAttendanceCode = SourceValueCode
	,SourceFirstAttendance = SourceValue
	,LocalFirstAttendanceID = LocalValueID
	,LocalFirstAttendanceCode = LocalValueCode
	,LocalFirstAttendance = LocalValue
	,NationalFirstAttendanceID = NationalValueID
	,NationalFirstAttendanceCode = NationalValueCode
	,NationalFirstAttendance = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'FSTATT'







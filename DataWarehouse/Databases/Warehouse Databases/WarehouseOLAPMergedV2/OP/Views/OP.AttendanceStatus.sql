﻿CREATE view [OP].[AttendanceStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAttendanceStatusID = SourceValueID
	,SourceAttendanceStatusCode = SourceValueCode
	,SourceAttendanceStatus = SourceValue
	,LocalAttendanceStatusID = LocalValueID
	,LocalAttendanceStatusCode = LocalValueCode
	,LocalAttendanceStatus = LocalValue
	,NationalAttendanceStatusID = NationalValueID
	,NationalAttendanceStatusCode = NationalValueCode
	,NationalAttendanceStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ATTDNA'







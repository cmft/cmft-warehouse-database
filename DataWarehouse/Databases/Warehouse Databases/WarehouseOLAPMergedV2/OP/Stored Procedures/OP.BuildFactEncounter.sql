﻿CREATE procedure [OP].[BuildFactEncounter] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare @MissingSubSpecialtyID int =
	(
	select
		SubSpecialty.SubSpecialtyID
	from
		WH.SubSpecialty
	where
		SubSpecialty.SubSpecialty = 'Unassigned'
	)


--generate a work table to speed up subsequent processing

--Checksum
select
	 Reference.MergeEncounterRecno
	,Reference.ContextID
	,Reference.AppointmentDateID
	,Metric.MetricID
	,Reference.ConsultantID
	,Reference.SpecialtyID
	,Directorate.DirectorateID
	,Reference.SiteID
	,Reference.AgeID
	,Reference.SexID
	,Reference.AttendanceStatusID
	,Reference.AttendanceOutcomeID
	,Reference.ClinicID
	,IsWardAttender = coalesce(Encounter.IsWardAttender, 0)
	,Reference.SourceOfReferralID
	,Reference.ReasonForReferralID
	,Reference.ReferralSpecialtyID
	,RTTActivity = coalesce(Encounter.RTTActivity, 0)
	,RTTTreated = coalesce(Encounter.RTTTreated, 0)
	,Encounter.SourcePatientNo
	,Encounter.CasenoteNo
	,Encounter.ContextCode
	,SubSpecialtyID = @MissingSubSpecialtyID
	,NationalExamID = NationalExam.AllocationID
	,ServiceID = Service.AllocationID

	,CasenoteMergeRecno =
		coalesce(
			(
			select
				Base.MergeRecno
			from
				Casenote.Base
			where
				Base.SourcePatientNo = Encounter.SourcePatientNo
			and	Base.CasenoteNumber = Encounter.CasenoteNo
			and	Base.ContextCode = Encounter.ContextCode
			)
			,-1
		)

	,Reference.AppointmentResidenceCCGID
	,Reference.DoctorID
	,Reference.ReligionID
	,EthnicCategoryID = Reference.EthnicOriginID

	,Encounter.Cases
	,Encounter.LengthOfWait

--work variables
	,AttendanceStatus.NationalAttendanceStatusCode
	,FirstAttendance.NationalFirstAttendanceCode
into
	#ChecksumEncounter
from
	OP.BaseEncounter Encounter

inner join OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join Allocation.DatasetAllocation NationalExam
on	NationalExam.AllocationTypeID = 1
and	NationalExam.DatasetCode = 'OP'
and	NationalExam.DatasetRecno = Encounter.MergeEncounterRecno

inner join Allocation.DatasetAllocation Service
on	Service.AllocationTypeID = 2
and	Service.DatasetCode = 'OP'
and	Service.DatasetRecno = Encounter.MergeEncounterRecno

inner join WH.Directorate
on	Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')

inner join OP.Metric
on	Metric.MetricCode = 'CHKAPP'

inner join OP.AttendanceStatus
on	AttendanceStatus.SourceAttendanceStatusID = Reference.AttendanceStatusID

inner join OP.FirstAttendance
on	FirstAttendance.SourceFirstAttendanceID = Reference.DerivedFirstAttendanceID


truncate table OP.FactEncounter

insert into OP.FactEncounter
(
	 MergeEncounterRecno
	,ContextID
	,EncounterDateID
	,MetricID
	,ConsultantID
	,SpecialtyID
	,DirectorateID
	,SiteID
	,AgeID
	,SexID
	,AttendanceStatusID
	,AttendanceOutcomeID
	,ClinicID
	,IsWardAttender
	,SourceOfReferralID
	,ReasonForReferralID
	,ReferralSpecialtyID
	,RTTActivity
	,RTTTreated
	,SubSpecialtyID
	,NationalExamID
	,ServiceID
	,CasenoteMergeRecno
	,AppointmentResidenceCCGID
	,DoctorID
	,ReligionID
	,EthnicCategoryID

	,Encounter.Cases
	,LengthOfWait
)

select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FATT'

where
	Encounter.NationalAttendanceStatusCode in ('5' , '6')
and	Encounter.NationalFirstAttendanceCode = '1'

union all

--Review Attendance
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RATT'

where
	Encounter.NationalAttendanceStatusCode in ('5' , '6')
and	Encounter.NationalFirstAttendanceCode = '2'

union all

--Referral To Treatment Clock Stop Administrative Event
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RTTATT'

where
	Encounter.NationalAttendanceStatusCode in ('5' , '6')
and	Encounter.NationalFirstAttendanceCode = '5'

union all

--First Telephone Attendance
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FTATT'

where
	Encounter.NationalAttendanceStatusCode in ('5' , '6')
and	Encounter.NationalFirstAttendanceCode = '3'

union all

--Unknown Attendance
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'UNKATT'

where
	Encounter.NationalAttendanceStatusCode in ('5' , '6')
and	Encounter.NationalFirstAttendanceCode = '-1'

union all

--First DNA
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FDNA'

where
	Encounter.NationalAttendanceStatusCode in ('3' , '7')
and	Encounter.NationalFirstAttendanceCode = '1'

union all

--Review DNA
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RDNA'

where
	Encounter.NationalAttendanceStatusCode in ('3' , '7')
and	Encounter.NationalFirstAttendanceCode = '2'

union all

--RTT DNA
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RTTDNA'

where
	Encounter.NationalAttendanceStatusCode in ('3' , '7')
and	Encounter.NationalFirstAttendanceCode = '5'

union all

--First Telephone DNA
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FTDNA'

where
	Encounter.NationalAttendanceStatusCode in ('3' , '7')
and	Encounter.NationalFirstAttendanceCode = '3'

union all

--Unknown DNA
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'UNKDNA'

where
	Encounter.NationalAttendanceStatusCode in ('3' , '7')
and	Encounter.NationalFirstAttendanceCode = '-1'

union all

--First cancellation by patient
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FCANPAT'

where
	Encounter.NationalAttendanceStatusCode = '2'
and	Encounter.NationalFirstAttendanceCode = '1'

union all

--Review Cancellation by Patient
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RCANPAT'

where
	Encounter.NationalAttendanceStatusCode = '2'
and	Encounter.NationalFirstAttendanceCode = '2'

union all

--Review Cancellation by Patient
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RTTCANPAT'

where
	Encounter.NationalAttendanceStatusCode = '2'
and	Encounter.NationalFirstAttendanceCode = '5'

union all

--First Telephone cancellation by Patient
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FTCANPAT'

where
	Encounter.NationalAttendanceStatusCode = '2'
and	Encounter.NationalFirstAttendanceCode = '3'

union all

--Unknown Cancellation by Patient
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'UNKCANPAT'

where
	Encounter.NationalAttendanceStatusCode = '2'
and	Encounter.NationalFirstAttendanceCode = '-1'

union all

--First Cancellation by provider
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FCANPROV'

where
	Encounter.NationalAttendanceStatusCode = '4'
and	Encounter.NationalFirstAttendanceCode = '1'

union all

--Review Cancellation by provider
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RCANPROV'

where
	Encounter.NationalAttendanceStatusCode = '4'
and	Encounter.NationalFirstAttendanceCode = '2'

union all

--RTT Cancellation by provider
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RTTCANPROV'

where
	Encounter.NationalAttendanceStatusCode = '4'
and	Encounter.NationalFirstAttendanceCode = '5'

union all

--First Telephone Cancellation by provider
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FTCANPROV'

where
	Encounter.NationalAttendanceStatusCode = '4'
and	Encounter.NationalFirstAttendanceCode = '3'

union all

--Unknown Cancellation by provider
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'UNKCANPROV'

where
	Encounter.NationalAttendanceStatusCode = '4'
and	Encounter.NationalFirstAttendanceCode = '-1'

union all

--First Cancellations
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FCANCEL'

where
	Encounter.NationalAttendanceStatusCode in ( '2' , '4' )
and	Encounter.NationalFirstAttendanceCode = '1'

union all

--Review Cancellations
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RCANCEL'

where
	Encounter.NationalAttendanceStatusCode in ( '2' , '4' )
and	Encounter.NationalFirstAttendanceCode = '2'

union all

--RTT Cancellations
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RTTCANCEL'

where
	Encounter.NationalAttendanceStatusCode in ( '2' , '4' )
and	Encounter.NationalFirstAttendanceCode = '5'

union all

--First Telephone Cancellations
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FTCANCEL'

where
	Encounter.NationalAttendanceStatusCode in ( '2' , '4' )
and	Encounter.NationalFirstAttendanceCode = '3'

union all

--Unknown Cancellations
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'UNKCANCEL'

where
	Encounter.NationalAttendanceStatusCode in ( '2' , '4' )
and	Encounter.NationalFirstAttendanceCode = '-1'

union all

--First unknown attendances
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FUNKATT'

where
	Encounter.NationalAttendanceStatusCode = '-1'
and	Encounter.NationalFirstAttendanceCode = '1'

union all

--Review Unknown Attendances
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RUNKATT'

where
	Encounter.NationalAttendanceStatusCode = '-1'
and	Encounter.NationalFirstAttendanceCode = '2'

union all

--RTT Unknown attendances
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RTTFUNKATT'

where
	Encounter.NationalAttendanceStatusCode = '-1'
and	Encounter.NationalFirstAttendanceCode = '5'

union all

--Appointment checksum
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'CHKAPP'

union all

--First Appointments
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FAPP'

where
	Encounter.NationalFirstAttendanceCode = '1'

union all

--Review Appointments
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RAPP'

where
	Encounter.NationalFirstAttendanceCode = '2'

union all

--First RTT Appointments
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'RTTAPP'

where
	Encounter.NationalFirstAttendanceCode = '5'

union all

--First Telephone Appointments
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'FTAPP'

where
	Encounter.NationalFirstAttendanceCode = '3'

union all

--Unknown Appointments
select
	 Encounter.MergeEncounterRecno
	,Encounter.ContextID
	,Encounter.AppointmentDateID
	,Metric.MetricID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.DirectorateID
	,Encounter.SiteID
	,Encounter.AgeID
	,Encounter.SexID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.ClinicID
	,Encounter.IsWardAttender
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTActivity
	,Encounter.RTTTreated
	,Encounter.SubSpecialtyID
	,Encounter.NationalExamID
	,Encounter.ServiceID
	,Encounter.CasenoteMergeRecno
	,Encounter.AppointmentResidenceCCGID
	,Encounter.DoctorID
	,Encounter.ReligionID
	,Encounter.EthnicCategoryID
	,Encounter.Cases
	,Encounter.LengthOfWait
from
	#ChecksumEncounter Encounter

inner join OP.Metric
on	Metric.MetricCode = 'UNKAPP'

where
	Encounter.NationalFirstAttendanceCode = '-1'


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




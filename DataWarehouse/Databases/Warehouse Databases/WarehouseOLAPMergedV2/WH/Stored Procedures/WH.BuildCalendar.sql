﻿





CREATE procedure [WH].[BuildCalendar] as

/* 
==============================================================================================
Description: 

When		Who			What
?			?			Initial coding.
?			?			Added Sitrep.
13/02/2015	Paul Egan	Added AcademicYear for CHAMP reports.
01/05/2015	RR			Added FinancialWeek information (note Standard Monday to Sunday, but there will be additional wks for end of year if this occurs midweek
===============================================================================================
*/

set datefirst 1 --monday

--SET TRANSACTION ISOLATION LEVEL SNAPSHOT; -- DG commented out following restore of ReferenceMap from Prod to Dev

--truncate table WH.Calendar  -- Please note, do not truncate, this will impact on all historic data in eg APC.BaseEncounter where DateID has been brought through.  
							-- If this table is truncated and reinserted all the DateIDs will change which will mean all the historic fact tables will need to be rebuilt.
BEGIN TRANSACTION;

delete
from 
	WH.Calendar
where
	not exists
		(
		select
		1
		from
			WH.CalendarBase
		where
			CalendarBase.TheDate = WH.Calendar.TheDate
		);

insert into WH.Calendar
	(
	 TheDate
	,DayOfWeek
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,MonthName
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth
	,WeekNoKey
	,CalendarWeekNo
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	,WorkingDay
	,SitrepQuarter
	,SitrepMonth
	,SitrepWeekNo
	,SitrepFromDate
	,SitrepToDate
	,SitrepFinancialYear
	,AcademicYear
	,FinancialWeekStart
	,FinancialWeekEnd 
	,FinancialWeekNo 
	,FinancialWeekNoKey 
	,FinancialWeek
	)
Select
	TheDate
	,TheDay 
	,DayOfWeekKey 
	,LongDate 
	,TheMonth 
	,[MonthName]
	,FinancialQuarter 
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth 
	,WeekNoKey
	,CalendarWeekNo 
	,WeekNo 
	,FirstDayOfWeek
	,LastDayOfWeek
	,WorkingDay 
	,SitrepQuarter
	,SitrepMonth 
	,SitrepWeekNo 
	,SitrepFromDate
	,SitrepToDate 
	,SitrepFinancialYear 
	,AcademicYear
	,FinancialWeekStart
	,FinancialWeekEnd 
	,FinancialWeekNo 
	,FinancialWeekNoKey = FinancialWeekNoKey + FinancialWeek
	,FinancialWeek
from
	(
	select
		Calendar.TheDate

		,TheDay =
			datename(weekday, Calendar.TheDate)

		,DayOfWeekKey =
			datepart(weekday, Calendar.TheDate)
			
		,LongDate =
			left(convert(varchar, Calendar.TheDate, 113), 11)

		,TheMonth =
			left(datename(month, Calendar.TheDate), 3) + ' ' + datename(year, Calendar.TheDate)

		,MonthName =
			left(datename(month, Calendar.TheDate), 3)

		,FinancialQuarter =
			'Qtr ' + 
			case datepart(quarter, Calendar.TheDate)
			when 1 then '4'
			else convert(varchar, datepart(quarter, Calendar.TheDate) - 1)
			end + ' ' + Calendar.FinancialYear
		

		,Calendar.FinancialYear
		,Calendar.FinancialMonthKey
		,Calendar.FinancialQuarterKey

		,Calendar.CalendarQuarter
		,Calendar.CalendarSemester
		,Calendar.CalendarYear

	--derived as 1 if either last day of month or 
		,LastCompleteMonth =
			case
			
			when
				datepart(day, dateadd(day, 1, getdate())) = 1 
			and	Calendar.TheDate = dateadd(day, -1, datepart(day, dateadd(day, 1, getdate())))
			then 1 --it is the last day of month
			when
				Calendar.TheDate = dateadd(month, -1, dateadd(day, datepart(day, getdate()) * -1 + 1, dateadd(day, datediff(day, 0, getdate()), 0))) --mid month so go back to start of last month
			then 1
			else 0
			end

		,WeekNoKey = 
			Case
				when TheDate in ('1 Apr 2000','2 Apr 2000') then '200014'
				else datename(year, Calendar.FirstDayOfWeek) + right('00' + datename(week, Calendar.FirstDayOfWeek), 2) 
			end

		,CalendarWeekNo =
			Case
				when TheDate in ('1 Apr 2000','2 Apr 2000') then '14'
				else right('00' + datename(week, Calendar.FirstDayOfWeek), 2) 
			end
			
		,WeekNo =
			Case
				when TheDate in ('1 Apr 2000','2 Apr 2000') then 'Week Ending 02 Apr 2000'
				else 'Week Ending ' + left(convert(varchar, dateadd(day, 6, Calendar.FirstDayOfWeek), 113), 11)
			end
		,Calendar.FirstDayOfWeek

		,LastDayOfWeek =
			dateadd(day, 6, Calendar.FirstDayOfWeek)
			
		,WorkingDay =
			case when datepart(weekday, Calendar.TheDate) in (6,7) then null else 1 end

		,SitrepQuarter = coalesce(CalendarSitrep.SitrepQuarter, 'N/A')
		,SitrepMonth = coalesce(CalendarSitrep.SitrepMonth, 'N/A')
		,SitrepWeekNo = coalesce(CalendarSitrep.SitrepWeekNo, 'NA')
		,SitrepFromDate = CalendarSitrep.SitrepFromDate
		,SitrepToDate = CalendarSitrep.SitrepToDate
		,SitrepFinancialYear = coalesce(CalendarSitrep.SitrepFinancialYear
										,case
											when TheDate >= getdate() then 'Future Year'
											else 'Previous Year'
										end
										) 
		,Calendar.AcademicYear
		
		,FinancialFirstDayOfWeek
		,FinancialYearStart
		,FinancialYearEnd
		,FinancialWeekStart = 
				Case
					when MONTH(TheDate) = 4 and MONTH(FinancialFirstDayOfWeek) = 3 
					then cast(cast(year(TheDate)as varchar) + '-' + cast(MONTH(TheDate)as varchar) + '-' + '01' as date)
					else FinancialFirstDayOfWeek
				end
		,FinancialWeekEnd = 
				Case
					when MONTH(TheDate) = 3 and MONTH(dateadd(day,6,FinancialFirstDayOfWeek)) = 4
					then cast(cast(year(TheDate)as varchar) + '-' + cast(MONTH(TheDate)as varchar) + '-' + '31' as date)
					else dateadd(day,6,FinancialFirstDayOfWeek)
				end	
		,FinancialWeekNo =
			Case
				when TheDate in ('1 Apr 2000','2 Apr 2000') then 'Week Ending 02 Apr 2000'
				else 'Week Ending ' +
					Case
						when MONTH(TheDate) = 3 and MONTH(dateadd(day,6,FinancialFirstDayOfWeek)) = 4
						then '31 Mar ' + cast(year(FinancialFirstDayOfWeek)as varchar)
						else left(convert(varchar, dateadd(day, 6, FinancialFirstDayOfWeek), 113), 11)
						--cast(dateadd(day,6,FinancialFirstDayOfWeek) as varchar)
					end	
			end
		,FinancialWeekNoKey = 
			case
				when datepart(quarter, TheDate) = 1
				then DATEPART(year, DATEADD(Year,-1,TheDate))*1000000
					+ DATEPART(year, TheDate)*100
				else DATEPART(year, TheDate) *1000000
					+ DATEPART(year, DATEADD(Year,1,TheDate))*100
			end
		,FinancialWeek = 
			case 
				when datename(weekday,FinancialYearStart) = 'Monday' 
					then 
						case
							when dbo.FiscalWeek('04',TheDate) = 1 
								and month(TheDate) = 3
							then dbo.FiscalWeek('04',dateadd(day,-6,TheDate))+ 1
							else dbo.FiscalWeek('04',TheDate)
						end
				else
					case 
						when dbo.FiscalWeek('04',TheDate) in (52,53,54) 
							and month(TheDate) = 4  
						then 1
						when dbo.FiscalWeek('04',TheDate) = 1 
							and month(TheDate) = 3
						then 
							case
								when dbo.FiscalWeek('04',FinancialYearStart) in (52,53,54) 
								then dbo.FiscalWeek('04',dateadd(day,-6,TheDate))+ 2
								else dbo.FiscalWeek('04',dateadd(day,-6,TheDate))+ 1
							end
						when dbo.FiscalWeek('04',FinancialYearStart) = 1
						then dbo.FiscalWeek('04',TheDate)
						else dbo.FiscalWeek('04',TheDate) + 1
				end
			end
	from
		(
		select
			TheDate

			,FinancialYear =
				case

				when datepart(quarter, TheDate) = 1
				then convert(char(4),datepart(year,dateadd(year,-1, convert(date, TheDate)))) + '/' + datename(year, TheDate)

				else datename(year, TheDate) + '/' + convert(char(4),datepart(year,dateadd(year,1, TheDate)))

				end

			,FinancialMonthKey =
				case datepart(quarter, TheDate)
				when 1 then datepart(year, TheDate) - 1
				else datepart(year, TheDate)
				end * 100 + 
				case datepart(quarter, TheDate)
				when 1 then 9 else -3 
				end + datepart(month, TheDate)

			,FinancialQuarterKey =
				case datepart(quarter, TheDate)
				when 1 then datepart(year, TheDate) - 1
				else datepart(year, TheDate)
				end * 100 + 
				case datepart(quarter, TheDate)
				when 1 then 4
				else datepart(quarter, TheDate) - 1
				end

			,FirstDayOfMonth =
				convert(date, '01 ' + datename(month, TheDate) + ' ' + datename(year, TheDate)) 

			,FirstDayOfWeek =
				(
				select
					max(StartOfWeek.TheDate)
				from
					WH.CalendarBase StartOfWeek
				where
					datepart(dw, StartOfWeek.TheDate) = 1
				and	StartOfWeek.TheDate <= CalendarBase.TheDate
				)

		--calendar hierarchy
			,CalendarQuarter =
				'Qtr ' + convert(varchar,datepart(quarter,TheDate))
	 
			,CalendarSemester =
				case when datepart(quarter, TheDate) < 3 then '1st Semester' else '2nd Semester' end

			,CalendarYear =
				datename(year,TheDate) 
				
			/* Added 13/02/2015 Paul Egan */
			,AcademicYear = 
				case
					when datepart(month, TheDate) <= 8
						then cast(year(TheDate) -1 as char(4)) + '/' + cast(year(TheDate) as char(4))
					else cast(year(TheDate) as char(4)) + '/' + cast(year(TheDate) + 1 as char(4))
				end
			,FinancialFirstDayOfWeek =
					case
						when TheDate in ('1 Apr 2000','2 Apr 2000') then '26 Mar 2000'
						else
							(
							select
								max(StartOfWeek.TheDate)
							from
								WH.CalendarBase StartOfWeek
							where
								datepart(dw, StartOfWeek.TheDate) = 1
							and	StartOfWeek.TheDate <= CalendarBase.TheDate
							)
						end
				,FinancialYearStart = 
					Case
						when datepart(quarter,TheDate) = 1
						then cast(cast(datepart(year,TheDate)-1 as varchar) + '-04-01' as date)
						else cast(cast(datepart(year,TheDate)as varchar) + '-04-01' as date)
					end
				,FinancialYearEnd = 
					Case
						when datepart(quarter,TheDate) = 1
						then cast(cast(datepart(year,TheDate)as varchar) + '-03-31' as date)
						else cast(cast(datepart(year,TheDate)+1 as varchar) + '-03-31' as date)
					end
		from
				WH.CalendarBase 
			where
				not exists
					(
					select
						1
					from
						WH.Calendar CurrentCalendar
					where
						CurrentCalendar.TheDate = WH.CalendarBase.TheDate
					)

			and	CalendarBase.TheDate not in
				(
				 '1 Jan 1900'
				,'2 Jan 1900'
				,'31 Dec 9999'
				)

			) Calendar

		left join WH.CalendarSitrep
		on	Calendar.TheDate between CalendarSitrep.SitrepFromDate and CalendarSitrep.SitrepToDate
	) Cal


insert into WH.Calendar
	(
	 TheDate
	,DayOfWeek
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,MonthName
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth
	,WeekNoKey
	,CalendarWeekNo
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	,WorkingDay
	,SitrepQuarter
	,SitrepMonth
	,SitrepWeekNo
	,SitrepFromDate
	,SitrepToDate
	,SitrepFinancialYear
	,AcademicYear
	,FinancialWeekStart
	,FinancialWeekEnd
	,FinancialWeekNo
	,FinancialWeekNoKey
	,FinancialWeek
	)
select
	*
from
	(
	select
		 TheDate = '31 dec 9999'
		,DayOfWeek = 'N/A'
		,DayOfWeekKey = 9
		,LongDate = 'Future Date'
		,TheMonth = 'N/A'
		,MonthName = 'N/A'
		,FinancialQuarter = 'N/A'
		,FinancialYear = 'Future Year'
		,FinancialMonthKey = 999909
		,FinancialQuarterKey = 999903
		,CalendarQuarter = 'N/A'
		,CalendarSemester = 'N/A'
		,CalendarYear = 'Future Year'
		,LastCompleteMonth = 0
		,WeekNoKey = '999953'
		,CalendarWeekNo = 'N/A'
		,WeekNo = 'N/A'
		,FirstDayOfWeek = '31 dec 9999'
		,LastDayOfWeek = '31 dec 9999'
		,WorkingDay = null
		,SitrepQuarter = 'N/A'
		,SitrepMonth = 'N/A'
		,SitrepWeekNo = 'NA'
		,SitrepFromDate = null
		,SitrepToDate = null
		,SitrepFinancialYear = 'N/A'
		,AcademicYear = 'Future Year'
		,FinancialWeekStart = cast('99991231' as date)
		,FinancialWeekEnd = cast('99991231' as date)
		,FinancialWeekNo = 'N/A'
		,FinancialWeekNoKey = '2998299953'
		,FinancialWeek = '99'

	union

	select
		 TheDate = '1 Jan 1900'
		,DayOfWeek = 'N/A'
		,DayOfWeekKey = 9
		,LongDate = 'N/A'
		,TheMonth = 'N/A'
		,MonthName = 'N/A'
		,FinancialQuarter = 'N/A'
		,FinancialYear = 'N/A'
		,FinancialMonthKey = 190001
		,FinancialQuarterKey = 190001
		,CalendarQuarter = 'N/A'
		,CalendarSemester = 'N/A'
		,CalendarYear = 'N/A'
		,LastCompleteMonth = 0
		,WeekNoKey = '190001'
		,CalendarWeekNo = 'N/A'
		,WeekNo = 'N/A'
		,FirstDayOfWeek = '1 Jan 1900'
		,LastDayOfWeek = '7 Jan 1900'
		,WorkingDay = null
		,SitrepQuarter = 'N/A'
		,SitrepMonth = 'N/A'
		,SitrepWeekNo = 'NA'
		,SitrepFromDate = null
		,SitrepToDate = null
		,SitrepFinancialYear = 'N/A'
		,AcademicYear = 'N/A'
		,FinancialWeekStart = cast('19000101' as date)
		,FinancialWeekEnd = cast('19000101' as date)
		,FinancialWeekNo = 'N/A'
		,FinancialWeekNoKey = '1900190101'
		,FinancialWeek = '99'
		
	union

	select
		 TheDate = '2 Jan 1900'
		,DayOfWeek = 'N/A'
		,DayOfWeekKey = 9
		,LongDate = 'N/A'
		,TheMonth = 'N/A'
		,MonthName = 'N/A'
		,FinancialQuarter = 'N/A'
		,FinancialYear = 'Previous Year'
		,FinancialMonthKey = 190001
		,FinancialQuarterKey = 190001
		,CalendarQuarter = 'N/A'
		,CalendarSemester = 'N/A'
		,CalendarYear = 'Previous Year'
		,LastCompleteMonth = 0
		,WeekNoKey = '190001'
		,CalendarWeekNo = 'N/A'
		,WeekNo = 'N/A'
		,FirstDayOfWeek = '2 Jan 1900'
		,LastDayOfWeek = '2 Jan 1900'
		,WorkingDay = null
		,SitrepQuarter = 'N/A'
		,SitrepMonth = 'N/A'
		,SitrepWeekNo = 'NA'
		,SitrepFromDate = null
		,SitrepToDate = null
		,SitrepFinancialYear = 'N/A'
		,AcademicYear = 'Previous Year'
		,FinancialWeekStart = cast('19000101' as date)
		,FinancialWeekEnd = cast('19000101' as date)
		,FinancialWeekNo = 'N/A'
		,FinancialWeekNoKey = '1900190101'
		,FinancialWeek = '99'
		
	) SystemDate
where
	not exists
	(
	select
		1
	from
		WH.Calendar
	where
		Calendar.TheDate = SystemDate.TheDate
	)


COMMIT TRANSACTION;









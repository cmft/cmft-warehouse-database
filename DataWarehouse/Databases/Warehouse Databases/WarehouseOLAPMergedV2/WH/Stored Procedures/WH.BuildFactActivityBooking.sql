﻿

CREATE procedure [WH].[BuildFactActivityBooking] as

truncate table WH.FactActivityBooking

insert into WH.FactActivityBooking
(
	 MergeEncounterRecno
	,TCIDateID
	,SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,DirectorateID
	,SpecialtyID
	,ServiceID
	,EncounterRecno
	,Cases
	,CensusDateID
)

select
	 FactActivity.MergeEncounterRecno
	,FactActivity.TCIDateID
	,FactActivity.SourceID
	,FactActivity.ConsolidatedView
	,FactActivity.Reportable
	,FactActivity.MetricID
	,FactActivity.PointOfDeliveryID
	,FactActivity.DirectorateID
	,FactActivity.SpecialtyID
	,FactActivity.ServiceID	
	,ActivityEncounter.EncounterRecno
	,FactActivity.Cases
	,FactActivity.CensusDateID
from
	(
	select
		 MergeEncounterRecno
		,TCIDateID
		,SourceID
		,ConsolidatedView
		,Reportable
		,MetricID
		,PointOfDeliveryID
		,DirectorateID
		,SpecialtyID
		,ServiceID
		,Cases
		,CensusDateID

		,SourceDatasetCode = 'APCWL'
	from
		APC.FactActivityBooking
	
	) FactActivity

	inner join WH.ActivitySource
	on	ActivitySource.SourceID = FactActivity.SourceID

	inner join Activity.Encounter ActivityEncounter
	on	ActivityEncounter.SourceRecno = -1
	and	ActivityEncounter.SourceSystem = ActivitySource.Source
	and	ActivityEncounter.SourceDatasetCode = FactActivity.SourceDatasetCode


insert into WH.FactActivityBooking
(
	 MergeEncounterRecno
	,TCIDateID
	,SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,DirectorateID
	,SpecialtyID
	,ServiceID
	,EncounterRecno
	,Cases
	,CensusDateID
)

select
	 FactActivity.MergeEncounterRecno
	,FactActivity.TCIDateID
	,FactActivity.SourceID
	,FactActivity.ConsolidatedView
	,FactActivity.Reportable
	,FactActivity.MetricID
	,FactActivity.PointOfDeliveryID
	,FactActivity.DirectorateID
	,FactActivity.SpecialtyID
	,FactActivity.ServiceID	
	,ActivityEncounter.EncounterRecno
	,FactActivity.Cases
	,FactActivity.CensusDateID
from
	(
	select
		 MergeEncounterRecno
		,TCIDateID
		,SourceID
		,ConsolidatedView
		,Reportable
		,MetricID
		,PointOfDeliveryID
		,DirectorateID
		,SpecialtyID
		,ServiceID
		,Cases
		,CensusDateID

		,SourceDatasetCode = 'OPWL'
	from
		OP.FactActivityBooking

	) FactActivity


	inner join WH.ActivitySource
	on	ActivitySource.SourceID = FactActivity.SourceID

	inner join Activity.Encounter ActivityEncounter
	on	ActivityEncounter.SourceRecno = -1
	and	ActivityEncounter.SourceSystem = ActivitySource.Source
	and	ActivityEncounter.SourceDatasetCode = FactActivity.SourceDatasetCode



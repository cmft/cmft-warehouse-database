﻿

CREATE proc [WH].[BuildPointOfDeliveryBase]

as

--PointOfDeliveryGroupBase

merge
	WH.PointOfDeliverySubgroupBase target
using
	(
	select distinct
		PointOfDeliverySubgroupCode = Allocation.SourceAllocationID
		,PointOfDeliverySubgroup = Allocation.Allocation 
	from
		Allocation.Allocation
	where
		Allocation.AllocationTypeID = 5
	and	Allocation.Active = 1

	) source
	on	source.PointOfDeliverySubgroupCode = target.PointOfDeliverySubgroupCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 PointOfDeliverySubgroupCode
			,PointOfDeliverySubgroup
			,PointOfDeliveryGroupID
			)
		values
			(
			 source.PointOfDeliverySubgroupCode
			,source.PointOfDeliverySubgroup
			,'13' -- default to N/A
			)

	when matched
	and not
		(
			isnull(target.PointOfDeliverySubgroupCode, '') = isnull(source.PointOfDeliverySubgroupCode, '')
		and isnull(target.PointOfDeliverySubgroup, '') = isnull(source.PointOfDeliverySubgroup, '')
		)
	then
		update
		set
			 target.PointOfDeliverySubgroupCode = source.PointOfDeliverySubgroupCode
			,target.PointOfDeliverySubgroup = source.PointOfDeliverySubgroup
;

--PointOfDeliveryBase

merge
	WH.PointOfDeliveryBase target
using
	(
	select
		PointOfDeliveryID = Allocation.AllocationID
		,PointOfDeliveryCode = Allocation.SourceAllocationID
		,PointOfDelivery = Allocation.Allocation 
		,PointOfDeliverySubgroupID = PointOfDeliverySubgroupBase.PointOfDeliverySubgroupID
	from
		Allocation.Allocation

	inner join WH.PointOfDeliverySubgroupBase
	on	PointOfDeliverySubgroupCode = Allocation.SourceAllocationID

	where
		Allocation.AllocationTypeID = 5
	and	Allocation.Active = 1

	) source
	on	source.PointOfDeliveryID = target.PointOfDeliveryID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 PointOfDeliveryID
			,PointOfDeliveryCode
			,PointOfDelivery
			,PointOfDeliverySubGroupID
			)
		values
			(
			 source.PointOfDeliveryID
			,source.PointOfDeliveryCode
			,source.PointOfDelivery
			,source.PointOfDeliverySubgroupID
			)

	when matched
	and not
		(
			isnull(target.PointOfDeliveryID, 0) = isnull(source.PointOfDeliveryID, 0)
		and isnull(target.PointOfDeliveryCode,'') = isnull(source.PointOfDeliveryCode, '')
		and isnull(target.PointOfDelivery, '') = isnull(source.PointOfDelivery, '')
		and isnull(target.PointOfDeliverySubGroupID, 0) = isnull(source.PointOfDeliverySubgroupID, 0)
		)
	then
		update
		set
			 target.PointOfDeliveryID = source.PointOfDeliveryID
			,target.PointOfDeliveryCode = source.PointOfDeliveryCode
			,target.PointOfDelivery = source.PointOfDelivery
			,target.PointOfDeliverySubGroupID = source.PointOfDeliverySubgroupID
;

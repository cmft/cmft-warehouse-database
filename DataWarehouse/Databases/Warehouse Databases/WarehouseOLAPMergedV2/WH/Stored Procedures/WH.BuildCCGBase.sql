﻿CREATE procedure [WH].[BuildCCGBase] as

insert into WH.CCGBase
(
	 CCGCode
	,CCG
	,LocalAreaTeamCode
	,LocalAreaTeam
	,CommissioningRegionCode
	,CommissioningRegion
)

select
	 CCGCode = NewCCG.OrganisationCode
	,CCG = NewCCG.Organisation
	,LocalAreaTeamCode = HealthAuthority.OrganisationCode
	,LocalAreaTeam = HealthAuthority.Organisation
	,CommissioningRegionCode = RegionalOffice.OrganisationCode
	,CommissioningRegion = RegionalOffice.Organisation

from
	[$(Organisation)].ODS.CCG NewCCG

inner join [$(Organisation)].ODS.HealthAuthority
on	HealthAuthority.OrganisationCode = NewCCG.HACode

inner join [$(Organisation)].ODS.RegionalOffice
on	RegionalOffice.OrganisationCode = HealthAuthority.ROCode

where
	not exists
		(
		select
			1
		from
			WH.CCG
		where
			CCG.CCGCode = NewCCG.OrganisationCode
		)

﻿

CREATE Procedure [WH].[BuildFactActivityPlan] as

--	20150702	RR Changed IS_Current = 1 to pull the latest reporting freeze month for 2014/2015 as everything is now set to Is_Current = 0
--  20150703	RR added criteria to exclude Treatment Function Code 360 as requested by Phil H.
--	20150811	RR changed from view to Proc

Truncate table WH.FactActivityPlan

Insert into WH.FactActivityPlan
	(
	Contracting_ID
	,DateID
	,MetricID
	,PointOfDeliveryID 
	,ContractFlagID 
	,DirectorateID 
	,NationalSpecialtyCode 
	,ServiceID 
	,HRGID 
	,Cases 
	,Value 											
	,ToDateFlag
	,ToFreezeFlag	
	)

select
	PlanProfile.Contracting_ID
	,CalendarDay.DateID
	,Metric.MetricID
	,PointOfDeliveryID = coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = coalesce(
								ContractFlag.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 9
								)
							)
	,DirectorateID = Directorate.DirectorateID
	,NationalSpecialtyCode =  left(PlanProfile.Treatment_Function_Code, 3) -- to handle dodgy specialty codes (...END)
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	--,Context.ContextID
	,Cases = PlanProfile.Activity_Plan_Divisional / (
													select 
														count(*)
													from
														WH.Calendar CalendarDay
													where
														CalendarDay.TheDate between Calendar.FirstDayOfMonth and Calendar.LastDayOfMonth
													--and WorkingDay = 1							
													)
	,Value = PlanProfile.Price_Plan_Divisional /	(
													select 
														count(*)
													from
														WH.Calendar CalendarDay
													where
														CalendarDay.TheDate between Calendar.FirstDayOfMonth and Calendar.LastDayOfMonth
													--and WorkingDay = 1
													)												
	,ToDateFlag =
		cast(
			case
			when CalendarDay.TheDate < cast(getdate() as date)
			then 1
			else 0
			end
			as bit
		)
	,ToFreezeFlag =
		cast(
			case
			when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
			then 1
			else 0
			end
		as bit
		)
from
	SLAM.Encounter PlanProfile

inner join WH.ActivityMetric Metric
on	Metric.MetricCode =
	case PoD_Code 
	when 'AandEFA'	then 'FAE'
	when 'AandEFUP' then 'RAE'
	when 'BMT-A'	then 'IESPL'
	when 'BMT-P'	then 'IESPL'
	--when 'BPT'		then 'IESPL'
	--when 'BPT-UC'	then 'IESPL'
	when 'DC'		then 'DCSPL'
	when 'EL'		then 'IESPL'
	--when 'ELXBD'	then 'IESPL'
	when 'Mat-Antenatal' then 'ANTE'
	when 'Mat-Postnatal' then 'POST'
	when 'Fertility'	
	then
		case HRG_Code
		when 'MC06Z' then 'DCSPL'
		when 'MC08Z' then 'DCSPL'		
		when 'MC10Z' then 'DCSPL'		
		when 'MC11Z' then 'DCSPL'		
		when 'MC12Z' then 'DCSPL'		
		when 'MC14Z' then 'DCSPL'						
		when 'STIMULATIONS' then 'RATT'
		end
	when 'NEL' then 'EMSPL'
	when 'NELNE'	
	then
		case Treatment_Function_Code
		when '501' then 'MATSPL'
		else 'NEOTHSPL'
		end
	--when 'NELNEXBD'			then 'EMSPL'
	when 'NELSD'			then 'EMSPL'
	when 'NELST'			then 'EMSPL'
	--when 'NELXBD'			then 'EMSPL'
	when 'Pancreas'			then 'IESPL'
	when 'RT-A'				then 'IESPL'
	when 'RT-P'				then 'IESPL'
	when 'Stroke-NEL'		then 'EMSPL'
	when 'Stroke-NELNE'		then 'NEOTHSPL'
	when 'COMMID'			then 'RATT'
	when 'OPFASPCL'			then 'FATT'
	when 'OPFAMPCL'			then 'FATT'
	when 'OPFAPROC'			then 'FATT'
	when 'OPFUPSPCL'		then 'RATT'
	when 'OPFUPMPCL'		then 'RATT'
	when 'OPFUPPROC'		then 'RATT'
	when 'NonConsultant'	then 'RATT'
	when 'TELCLIN'			then 'RATT'
	when 'Hearing'
	then 
		case Profile_ID
		when 25	 then 'RATT'
		when 94	 then 'RATT'
		when 121 then 'FATT'
		end
	when 'AKU'				then 'AKU'
	when 'ConCare'			then 'ConCare'
	when 'CRITICALCARE'		then 'CRITICALCARE'
	when 'HDU-P'			then 'HDU-P'
	when 'HDU-P(Outlying)'	then 'HDU-P(Outlying)'	
	when 'NEONATAL'			then 'NEONATAL'	
	when 'PICU'				then 'PICU'	
	when 'REHAB'			then 'REHAB'	
	when 'CAMHS-T4'			then 'CAMHS-T4'	
	when 'INRU-A'			then 'INRU-A'	
	when 'LTTU'				then 'LTTU'
	when 'NRU-P'			then 'NRU-P'
	when 'RDHOSP-A'			then 'RDHOSP-A'
--20150514 RR added per instruction PH
	when 'HSCT'				then 'IESPL'
	when 'RT'				then 'IESPL'
	when 'RDHOSP'			then 'RDHOSP'
	when 'HDU'				then 'HDU'
	when 'OutlyingHDU'		then 'OutlyingHDU'	
	when 'INRU'				then 'INRU'	
	when 'PNRU'				then 'PNRU'	
	end


left join 
		(
		select
			AllocationID
			,SourceAllocationID
			,Allocation
			,AllocationTypeID
		from
			Allocation.Allocation
		where
			not exists
					(
					select
						1
					from
						Allocation.Allocation Later
					where
						Later.SourceAllocationID = Allocation.SourceAllocationID
					and	Later.AllocationTypeID = Allocation.AllocationTypeID
					and	Later.AllocationID > Allocation.AllocationID
					)
	) PointOfDelivery
on	PointOfDelivery.SourceAllocationID = 
										case
										when PlanProfile.PoD_Code = 'AandEFA'
										then 'AEFA'
										when PlanProfile.PoD_Code = 'AandEFUP'
										then 'AEFUP'
										else PlanProfile.PoD_Code
										end
and	PointOfDelivery.AllocationTypeID = 5

left join Allocation.Allocation ContractFlag 
on	ContractFlag.SourceAllocationID = PlanProfile.Contract_Flag
and	ContractFlag.AllocationTypeID = 9

inner join WH.Calendar
on	convert(
			datetime, '01/' + 
							case
							when Report_Month <= 9
							then cast(Report_Month + 3 as varchar) + '/' + 
																	cast(
																		year(Spell_Admission_Date)
																		as varchar)
							when Report_Month > 9  
							then cast(Report_Month -9 as varchar) + '/' + 
																	cast(
																		year(
																			dateadd(
																				year, 1, Spell_Admission_Date) 
																				)
																	as varchar) 
							end
		,103) = Calendar.TheDate

inner join WH.Calendar CalendarDay
on	CalendarDay.TheDate between Calendar.FirstDayOfMonth and Calendar.LastDayOfMonth

inner join WH.Directorate
on	Directorate.DirectorateCode = PlanProfile.Directorate_Code

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(PlanProfile.HRG_Code, 'N/A')

where
	PlanProfile.Extract_Type = 'Freeze'
--and	PlanProfile.Is_Current = 1
and	(
		PlanProfile.Activity_Plan_Divisional != 0
	or	PlanProfile.Price_Plan_Divisional != 0
	)
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')
and PlanProfile.Extract_Year = '2014/2015'
and PlanProfile.Extract_Month = '12' --not exists removed and replaced after confirmation from PH
--and not exists
--	(
--	Select
--		1
--	from
--		SLAM.Encounter Latest
--	where
--		Latest.Extract_Year = '2014/2015'
--	and Latest.Extract_Type = 'Freeze'
--	and	(
--			Latest.Activity_Plan_Divisional != 0
--		or	Latest.Price_Plan_Divisional != 0
--		)
--	and Latest.Report_Month > PlanProfile.Report_Month
--	)
and Treatment_Function_Code <> '360'
			


	--15/16 plan data. Seperate dataset now

Insert into WH.FactActivityPlan
	(
	Contracting_ID
	,DateID
	,MetricID
	,PointOfDeliveryID 
	,ContractFlagID 
	,DirectorateID 
	,NationalSpecialtyCode 
	,ServiceID 
	,HRGID 
	,Cases 
	,Value 											
	,ToDateFlag
	,ToFreezeFlag	
	)

select
	Contracting_ID = null
	,Calendar.DateID
	,Metric.MetricID
	,PointOfDeliveryID = 
						coalesce(
							PointOfDelivery.AllocationID 
							,
							(
							select
								AllocationID
							from
								Allocation.Allocation 
							where
								SourceAllocationID = '-1'
							and	AllocationTypeID = 5
							)
						)
	,ContractFlagID = 
					(
					select
						AllocationID
					from
						Allocation.Allocation 
					where
						SourceAllocationID = '-1'
					and	AllocationTypeID = 9
					)
	,DirectorateID = Directorate.DirectorateID
	,NationalSpecialtyCode =  left(PlanProfile.Treatment_Function_Code, 3) -- to handle dodgy specialty codes (...END)
	,ServiceID = coalesce(
					Service.AllocationID
					,
					(
					select
						ServiceID
					from
						WH.Service 
					where
						Service = 'Unassigned'
					)
				)
	,HRGID = HRGBase.HRGID
	,Cases = Activity 						
	,Value = Price
													
	,ToDateFlag =
		cast(
			case
			when PlanProfile.TheDate < cast(getdate() as date)
			then 1
			else 0
			end
			as bit
		)
	,ToFreezeFlag =
		cast(
			case
			when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
			then 1
			else 0
			end
		as bit
		)


from
	[$(Contract_Reporting)].STAGING.Divisional_Plan_1516_Profiles_by_Day PlanProfile

inner join WH.Calendar
on	PlanProfile.TheDate = Calendar.TheDate

inner join WH.ActivityMetric Metric
on	Metric.MetricCode =
	case POD_Code 
	when 'AandEFA'	then 'FAE'
	when 'AandEFUP' then 'RAE'
	when 'BMT-A'	then 'IESPL'
	when 'BMT-P'	then 'IESPL'
	--when 'BPT'		then 'IESPL'
	--when 'BPT-UC'	then 'IESPL'
	when 'DC'		then 'DCSPL'
	when 'EL'		then 'IESPL'
	--when 'ELXBD'	then 'IESPL'
	when 'Mat-Antenatal' then 'ANTE'
	when 'Mat-Postnatal' then 'POST'
	when 'Fertility'	
	then
		case HRG_Code
		when 'MC06Z' then 'DCSPL'
		when 'MC08Z' then 'DCSPL'		
		when 'MC10Z' then 'DCSPL'		
		when 'MC11Z' then 'DCSPL'		
		when 'MC12Z' then 'DCSPL'		
		when 'MC14Z' then 'DCSPL'						
		when 'STIMULATIONS' then 'RATT'
		end
	when 'NEL' then 'EMSPL'
	when 'NELNE'	
	then
		case Treatment_Function_Code
		when '501' then 'MATSPL'
		else 'NEOTHSPL'
		end
	--when 'NELNEXBD'			then 'EMSPL'
	when 'NELSD'			then 'EMSPL'
	when 'NELST'			then 'EMSPL'
	--when 'NELXBD'			then 'EMSPL'
	when 'Pancreas'			then 'IESPL'
	when 'RT-A'				then 'IESPL'
	when 'RT-P'				then 'IESPL'
	when 'Stroke-NEL'		then 'EMSPL'
	when 'Stroke-NELNE'		then 'NEOTHSPL'
	when 'COMMID'			then 'RATT'
	when 'OPFASPCL'			then 'FATT'
	when 'OPFAMPCL'			then 'FATT'
	when 'OPFAPROC'			then 'FATT'
	when 'OPFUPSPCL'		then 'RATT'
	when 'OPFUPMPCL'		then 'RATT'
	when 'OPFUPPROC'		then 'RATT'
	when 'NonConsultant'	then 'RATT'
	when 'TELCLIN'			then 'RATT'
	when 'Hearing'			then 'RATT' -- requested by Phil Huitson 10.4.15
		--case Profile_ID
		--when 25	 then 'RATT'
		--when 94	 then 'RATT'
		--when 121 then 'FATT'
		--end
	when 'AKU'				then 'AKU'
	when 'ConCare'			then 'ConCare'
	when 'CRITICALCARE'		then 'CRITICALCARE'
	when 'HDU-P'			then 'HDU-P'
	when 'HDU-P(Outlying)'	then 'HDU-P(Outlying)'	
	when 'NEONATAL'			then 'NEONATAL'	
	when 'PICU'				then 'PICU'	
	when 'REHAB'			then 'REHAB'	
	when 'CAMHS-T4'			then 'CAMHS-T4'	
	when 'INRU-A'			then 'INRU-A'	
	when 'LTTU'				then 'LTTU'
	when 'NRU-P'			then 'NRU-P'
	when 'RDHOSP-A'			then 'RDHOSP-A'

	when 'HSCT-A'      then 'IESPL'
	when 'HSCT-P'      then 'IESPL'
	when 'OPFATEL'     then 'FATT'
	when 'OPFUPTEL'    then 'RATT'
	when 'OPFASPNCL'   then 'FATT'
	when 'OPFUPSPNCL'  then 'RATT'
--20150514 RR added per instruction PH
	when 'HSCT'				then 'IESPL'
	when 'RT'				then 'IESPL'
	when 'RDHOSP'			then 'RDHOSP'
	when 'HDU'				then 'HDU'
	when 'OutlyingHDU'		then 'OutlyingHDU'	
	when 'INRU'				then 'INRU'	
	when 'PNRU'				then 'PNRU'
	end

left join 
		(
		select
			AllocationID
			,SourceAllocationID
			,Allocation
			,AllocationTypeID
		from
			Allocation.Allocation
		where
			not exists
					(
					select
						1
					from
						Allocation.Allocation Later
					where
						Later.SourceAllocationID = Allocation.SourceAllocationID
					and	Later.AllocationTypeID = Allocation.AllocationTypeID
					and	Later.AllocationID > Allocation.AllocationID
					)
	) PointOfDelivery
on	PointOfDelivery.SourceAllocationID = 
										case
										when PlanProfile.POD_Code = 'AandEFA'
										then 'AEFA'
										when PlanProfile.POD_Code = 'AandEFUP'
										then 'AEFUP'
										else PlanProfile.POD_Code
										end
and	PointOfDelivery.AllocationTypeID = 5

left join Allocation.Allocation Service 
on	Service.Allocation = PlanProfile.Service
and	Service.AllocationTypeID = 2

inner join WH.Directorate
on	Directorate.DirectorateCode = PlanProfile.Directorate_Code

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(PlanProfile.HRG_Code, 'N/A')

where
	PlanProfile.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')
and Treatment_Function_Code <> '360'














































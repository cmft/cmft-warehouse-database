﻿

CREATE procedure [WH].[BuildFactActivity] as


truncate table WH.FactActivity


insert into WH.FactActivity
	(
	 MergeEncounterRecno
	,EncounterDateID
	,SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID	
	,HRGID
	,ContextID
	,EncounterRecno
	,Cases
	,Value
	)
select
	 MergeEncounterRecno
	,EncounterDateID
	,FactActivity.SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID	
	,HRGID
	,ContextID
	,ActivityEncounter.EncounterRecno
	,Cases
	,Value
from
	APC.FactActivity

inner join WH.ActivitySource
on	ActivitySource.SourceID = FactActivity.SourceID

inner join Activity.Encounter ActivityEncounter
on	ActivityEncounter.SourceRecno = FactActivity.MergeEncounterRecno
and	ActivityEncounter.SourceSystem = ActivitySource.Source
and	ActivityEncounter.SourceDatasetCode = FactActivity.SourceDatasetCode


insert into WH.FactActivity
	(
	 MergeEncounterRecno
	,EncounterDateID
	,SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID	
	,HRGID
	,ContextID
	,EncounterRecno
	,Cases
	,Value
	)
select
	 MergeEncounterRecno
	,EncounterDateID
	,FactActivityBedday.SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID
	,HRGID
	,ContextID
	,ActivityEncounter.EncounterRecno
	,Cases
	,Value
from
	APC.FactActivityBedday

inner join WH.ActivitySource
on	ActivitySource.SourceID = FactActivityBedday.SourceID

inner join Activity.Encounter ActivityEncounter
on	ActivityEncounter.SourceRecno = FactActivityBedday.MergeEncounterRecno
and	ActivityEncounter.SourceSystem = ActivitySource.Source
and	ActivityEncounter.SourceDatasetCode = FactActivityBedday.SourceDatasetCode


insert into WH.FactActivity
	(
	 MergeEncounterRecno
	,EncounterDateID
	,SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID	
	,HRGID
	,ContextID
	,EncounterRecno
	,Cases
	,Value
	)
select
	 MergeEncounterRecno
	,EncounterDateID
	,FactActivityDialysis.SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID
	,HRGID
	,ContextID
	,ActivityEncounter.EncounterRecno
	,Cases
	,Value
from
	APC.FactActivityDialysis

inner join WH.ActivitySource
on	ActivitySource.SourceID = FactActivityDialysis.SourceID

inner join Activity.Encounter ActivityEncounter
on	ActivityEncounter.SourceRecno = FactActivityDialysis.MergeEncounterRecno
and	ActivityEncounter.SourceSystem = ActivitySource.Source
and	ActivityEncounter.SourceDatasetCode = FactActivityDialysis.SourceDatasetCode



insert into WH.FactActivity
	(
	 MergeEncounterRecno
	,EncounterDateID
	,SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID	
	,HRGID
	,ContextID
	,EncounterRecno
	,Cases
	,Value
	)
select
	 MergeEncounterRecno
	,EncounterDateID
	,FactActivity.SourceID
	,ConsolidatedView
	,Reportable 
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID
	,HRGID
	,ContextID
	,ActivityEncounter.EncounterRecno
	,Cases
	,Value
from
	OP.FactActivity

inner join WH.ActivitySource
on	ActivitySource.SourceID = FactActivity.SourceID

inner join Activity.Encounter ActivityEncounter
on	ActivityEncounter.SourceRecno = FactActivity.MergeEncounterRecno
and	ActivityEncounter.SourceSystem = ActivitySource.Source
and	ActivityEncounter.SourceDatasetCode = FactActivity.SourceDatasetCode



insert into WH.FactActivity
	(
	 MergeEncounterRecno
	,EncounterDateID
	,SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID	
	,HRGID
	,ContextID
	,EncounterRecno
	,Cases
	,Value
	)
select
	 MergeEncounterRecno
	,EncounterDateID
	,FactActivity.SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID
	,HRGID
	,ContextID
	,ActivityEncounter.EncounterRecno
	,Cases
	,Value
from
	AE.FactActivity

inner join WH.ActivitySource
on	ActivitySource.SourceID = FactActivity.SourceID

inner join Activity.Encounter ActivityEncounter
on	ActivityEncounter.SourceRecno = FactActivity.MergeEncounterRecno
and	ActivityEncounter.SourceSystem = ActivitySource.Source
and	ActivityEncounter.SourceDatasetCode = FactActivity.SourceDatasetCode


insert into WH.FactActivity
	(
	 MergeEncounterRecno
	,EncounterDateID
	,SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID	
	,HRGID
	,ContextID
	,EncounterRecno
	,Cases
	,Value
	)
select
	 MergeEncounterRecno
	,EncounterDateID
	,FactActivity.SourceID
	,ConsolidatedView
	,Reportable
	,MetricID
	,PointOfDeliveryID
	,ContractFlagID
	,DirectorateID
	,SpecialtyID
	,ServiceID
	,HRGID
	,ContextID
	,ActivityEncounter.EncounterRecno
	,Cases
	,Value
from
	Maternity.FactActivity

inner join WH.ActivitySource
on	ActivitySource.SourceID = FactActivity.SourceID

inner join Activity.Encounter ActivityEncounter
on	ActivityEncounter.SourceRecno = FactActivity.MergeEncounterRecno
and	ActivityEncounter.SourceSystem = ActivitySource.Source
and	ActivityEncounter.SourceDatasetCode = FactActivity.SourceDatasetCode




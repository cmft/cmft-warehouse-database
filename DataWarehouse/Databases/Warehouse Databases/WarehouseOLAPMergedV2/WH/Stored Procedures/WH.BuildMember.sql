﻿CREATE procedure [WH].[BuildMember] as

--SET TRANSACTION ISOLATION LEVEL SNAPSHOT; -- DG commented out following restore of ReferenceMap from Prod to Dev

--truncate table WH.Member

BEGIN TRANSACTION;

delete
from
 WH.Member;



INSERT INTO WH.Member
(
	 AttributeCode
	,Attribute
	,SourceContextID
	,SourceContextCode
	,SourceContext
	,SourceValueID
	,SourceValueCode
	,SourceValue
	,LocalValueID
	,LocalValueCode
	,LocalValue
	,NationalValueID
	,NationalValueCode
	,NationalValue
	,OtherValueID
	,OtherValueCode
	,OtherValue
)
select
	 AttributeCode = Attribute.AttributeCode
	,Attribute = Attribute.Attribute
	,Context.ContextID
	,SourceContextCode = Context.ContextCode
	,SourceContext = Context.Context
	,SourceValueID = Value.ValueID
	,SourceValueCode = Value.ValueCode

	,SourceValue = Value.Value +
		case
		when exists
			(
			select
				1
			from
				ReferenceMapV2.Map.Value SameValue
			where
				SameValue.Value = Value.Value
			and	SameValue.AttributeContextID = Value.AttributeContextID
			and	SameValue.ValueID <> Value.ValueID
			)
		or	Value.Value = 'No Description'
		then ' (' + Context.ContextCode + ':' + Value.ValueCode + ')'
		else ''
		end

	,LocalValueID = LocalValue.ValueID
	,LocalValueCode = LocalValue.ValueCode
	,LocalValue = LocalValue.Value
	,NationalValueID = NationalValue.ValueID
	,NationalValueCode = NationalValue.ValueCode
	,NationalValue = NationalValue.Value
	,OtherValueID = OtherValue.ValueID
	,OtherValueCode = OtherValue.ValueCode
	,OtherValue = OtherValue.Value

from
	ReferenceMapV2.Map.Value

inner join ReferenceMapV2.Map.AttributeContext
on	AttributeContext.AttributeContextID = Value.AttributeContextID

inner join ReferenceMapV2.Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

inner join ReferenceMapV2.Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.BaseContext = 'True'

left join ReferenceMapV2.Map.ValueXref LocalValueXref
on	LocalValueXref.ValueID = Value.ValueID
and	exists
	(
	select
		1
	from
		ReferenceMapV2.Map.Value LocalValue

	inner join ReferenceMapV2.Map.AttributeContext LocalAttributeContext
	on	LocalValue.ValueID = LocalValueXref.ValueXrefID
	and	LocalValue.AttributeContextID = LocalAttributeContext.AttributeContextID

	inner join ReferenceMapV2.Map.Context LocalContext
	on	LocalContext.ContextID = LocalAttributeContext.ContextID
	and	LocalContext.ContextCode = 'L'
	)

and	not exists
	(
	select
		1
	from
		ReferenceMapV2.Map.ValueXref Previous
	where
		Previous.ValueID = Value.ValueID
	and	exists
		(
		select
			1
		from
			ReferenceMapV2.Map.Value LocalValue

		inner join ReferenceMapV2.Map.AttributeContext LocalAttributeContext
		on	LocalValue.ValueID = Previous.ValueXrefID
		and	LocalValue.AttributeContextID = LocalAttributeContext.AttributeContextID

		inner join ReferenceMapV2.Map.Context LocalContext
		on	LocalContext.ContextID = LocalAttributeContext.ContextID
		and	LocalContext.ContextCode = 'L'
		)
	and	Previous.AttributeXrefID > LocalValueXref.AttributeXrefID
	)

left join ReferenceMapV2.Map.Value LocalValue
on	LocalValue.ValueID = LocalValueXref.ValueXrefID

left join ReferenceMapV2.Map.ValueXref NationalValueXref
on	NationalValueXref.ValueID = Value.ValueID
and	exists
	(
	select
		1
	from
		ReferenceMapV2.Map.Value NationalValue

	inner join ReferenceMapV2.Map.AttributeContext NationalAttributeContext
	on	NationalValue.ValueID = NationalValueXref.ValueXrefID
	and	NationalValue.AttributeContextID = NationalAttributeContext.AttributeContextID

	inner join ReferenceMapV2.Map.Context NationalContext
	on	NationalContext.ContextID = NationalAttributeContext.ContextID
	and	NationalContext.ContextCode = 'N'
	)

and	not exists
	(
	select
		1
	from
		ReferenceMapV2.Map.ValueXref Previous
	where
		Previous.ValueID = Value.ValueID
	and	exists
		(
		select
			1
		from
			ReferenceMapV2.Map.Value NationalValue

		inner join ReferenceMapV2.Map.AttributeContext NationalAttributeContext
		on	NationalValue.ValueID = Previous.ValueXrefID
		and	NationalValue.AttributeContextID = NationalAttributeContext.AttributeContextID

		inner join ReferenceMapV2.Map.Context NationalContext
		on	NationalContext.ContextID = NationalAttributeContext.ContextID
		and	NationalContext.ContextCode = 'N'
		)
	and	Previous.AttributeXrefID > NationalValueXref.AttributeXrefID
	)

left join ReferenceMapV2.Map.Value NationalValue
on	NationalValue.ValueID = NationalValueXref.ValueXrefID


left join ReferenceMapV2.Map.ValueXref OtherValueXref
on	OtherValueXref.ValueID = Value.ValueID
and	exists
	(
	select
		1
	from
		ReferenceMapV2.Map.Value OtherValue

	inner join ReferenceMapV2.Map.AttributeContext OtherAttributeContext
	on	OtherValue.ValueID = OtherValueXref.ValueXrefID
	and	OtherValue.AttributeContextID = OtherAttributeContext.AttributeContextID

	inner join ReferenceMapV2.Map.Context OtherContext
	on	OtherContext.ContextID = OtherAttributeContext.ContextID
	and	OtherContext.ContextCode = 'O'
	)

and	not exists
	(
	select
		1
	from
		ReferenceMapV2.Map.ValueXref Previous
	where
		Previous.ValueID = Value.ValueID
	and	exists
		(
		select
			1
		from
			ReferenceMapV2.Map.Value OtherValue

		inner join ReferenceMapV2.Map.AttributeContext OtherAttributeContext
		on	OtherValue.ValueID = Previous.ValueXrefID
		and	OtherValue.AttributeContextID = OtherAttributeContext.AttributeContextID

		inner join ReferenceMapV2.Map.Context OtherContext
		on	OtherContext.ContextID = OtherAttributeContext.ContextID
		and	OtherContext.ContextCode = 'O'
		)
	and	Previous.AttributeXrefID > OtherValueXref.AttributeXrefID
	)

left join ReferenceMapV2.Map.Value OtherValue
on	OtherValue.ValueID = OtherValueXref.ValueXrefID

COMMIT TRANSACTION;

﻿


CREATE procedure [WH].[BuildCensus] as

declare @snapshot table (CensusDate date)

insert into @snapshot
(
      CensusDate
)
select distinct
      Calendar.TheDate
from
      COM.BaseWaitReference Activity

inner join WH.Calendar
on Activity.CensusDateID = Calendar.DateID

where
      not exists
      (
      select
            1
      from
            @snapshot Snapshot
      where
            Snapshot.CensusDate = Calendar.TheDate
      )


insert into @snapshot
(
      CensusDate
)
select distinct
      Calendar.TheDate
from
      APCWL.BaseEncounterReference Activity

inner join WH.Calendar
on Activity.CensusDateID = Calendar.DateID

where
      not exists
      (
      select
            1
      from
            @snapshot Snapshot
      where
            Snapshot.CensusDate = Calendar.TheDate
      )


insert into @snapshot
(
      CensusDate
)
select distinct
      Calendar.TheDate
from
      OPWL.BaseEncounterReference Activity

inner join WH.Calendar
on Activity.CensusDateID = Calendar.DateID

where
      not exists
      (
      select
            1
      from
            @snapshot Snapshot
      where
            Snapshot.CensusDate = Calendar.TheDate
      )


insert into @snapshot
(
      CensusDate
)
select distinct
      Calendar.TheDate
from
      DiagnosticWL.Fact Activity

inner join WH.Calendar
on Activity.CensusDateID = Calendar.DateID

where
      not exists
      (
      select
            1
      from
            @snapshot Snapshot
      where
            Snapshot.CensusDate = Calendar.TheDate
      )

insert into @snapshot
(
      CensusDate
)
select
      TheDate =
            (
            select
                  Calendar.TheDate
            from
                  WH.Calendar
            where
                  Calendar.DateID = Activity.CensusDateID
            )
from
      (
      select distinct
            Activity.CensusDateID
      from
            OP.FactDiary Activity
      ) Activity

where
      not exists
      (
      select
            1
      from
            @snapshot Snapshot
      where
            Snapshot.CensusDate =
                  (
                  select
                        Calendar.TheDate
                  from
                        WH.Calendar
                  where
                        Calendar.DateID = Activity.CensusDateID
                  )           
      )



delete
from
      WH.Census

insert
into
      WH.Census
(
      CensusDateID
      ,CensusDate
      ,Census
      ,LastInMonth
      ,CurrentCensus
)

select
      CensusDateID = Calendar.DateID
      ,CensusDate
      ,Census = 
            left(convert(varchar, CensusDate, 113), 11) 

      ,CurrentCensus =
            case
            when CensusDate in 
                  (
                  select 
                        max(CensusDate) 
                  from 
                        @snapshot LatestSnapshot 
                  group by 
                         datepart(year, CensusDate)
                        ,datepart(month, CensusDate)
                  )
            then 1
            else 0
            end
      ,LatestCensus = 
            case 
            when CensusDate =
                  (
                  select 
                        max(CensusDate) 
                  from 
                        @snapshot LatestSnapshot 
                  )
            then 1
            else 0
            end
from
      @snapshot snapshotTable
inner join WH.Calendar
on Calendar.TheDate = snapshotTable.CensusDate
order by
      1




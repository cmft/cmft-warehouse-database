﻿CREATE TABLE [WH].[FGMRegionBase] (
    [FGMRegionID] INT           IDENTITY (1, 1) NOT NULL,
    [RegionCode]  VARCHAR (10)  NOT NULL,
    [Region]      VARCHAR (200) NOT NULL,
    [CountryID]   INT           NOT NULL,
    [Created]     DATETIME      NULL,
    [ByWhom]      VARCHAR (50)  NULL,
    CONSTRAINT [PK_FGMRegion] PRIMARY KEY CLUSTERED ([FGMRegionID] ASC),
    CONSTRAINT [FK_FGMCountry] FOREIGN KEY ([CountryID]) REFERENCES [WH].[CountryBase] ([CountryID])
);


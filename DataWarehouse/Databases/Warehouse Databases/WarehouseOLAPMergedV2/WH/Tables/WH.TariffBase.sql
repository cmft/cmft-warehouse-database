﻿CREATE TABLE [WH].[TariffBase] (
    [Tariff]                      DECIMAL (18, 10) NULL,
    [NationalSpecialtyCode]       VARCHAR (10)     NULL,
    [NationalSpecialty]           VARCHAR (50)     NULL,
    [Division]                    VARCHAR (100)    NULL,
    [NationalFirstAttendanceCode] VARCHAR (10)     NULL,
    [PatientCategoryCode]         VARCHAR (2)      NULL,
    [InOut]                       VARCHAR (10)     NULL,
    [CalculateTariff]             INT              NULL,
    [DNARate]                     DECIMAL (18, 10) NULL
);


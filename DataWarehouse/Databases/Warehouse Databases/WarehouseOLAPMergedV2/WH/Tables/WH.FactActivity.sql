﻿CREATE TABLE [WH].[FactActivity] (
    [MergeEncounterRecno] INT        NOT NULL,
    [EncounterDateID]     INT        NOT NULL,
    [SourceID]            INT        NOT NULL,
    [ConsolidatedView]    BIT        NOT NULL,
    [Reportable]          BIT        NOT NULL,
    [MetricID]            INT        NOT NULL,
    [PointOfDeliveryID]   INT        NOT NULL,
    [ContractFlagID]      INT        NOT NULL,
    [DirectorateID]       INT        NOT NULL,
    [SpecialtyID]         INT        NOT NULL,
    [ServiceID]           INT        NOT NULL,
    [HRGID]               INT        NOT NULL,
    [ContextID]           INT        NOT NULL,
    [EncounterRecno]      INT        NOT NULL,
    [Cases]               FLOAT (53) NULL,
    [Value]               FLOAT (53) NULL
);


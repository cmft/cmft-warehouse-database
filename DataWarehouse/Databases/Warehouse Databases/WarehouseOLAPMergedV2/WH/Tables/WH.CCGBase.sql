﻿CREATE TABLE [WH].[CCGBase] (
    [CCGID]                   INT            IDENTITY (1, 1) NOT NULL,
    [CCGCode]                 NVARCHAR (8)   NOT NULL,
    [CCG]                     NVARCHAR (100) NOT NULL,
    [LocalAreaTeamCode]       NVARCHAR (8)   NOT NULL,
    [LocalAreaTeam]           NVARCHAR (100) NOT NULL,
    [CommissioningRegionCode] NVARCHAR (8)   NOT NULL,
    [CommissioningRegion]     NVARCHAR (100) NOT NULL
);


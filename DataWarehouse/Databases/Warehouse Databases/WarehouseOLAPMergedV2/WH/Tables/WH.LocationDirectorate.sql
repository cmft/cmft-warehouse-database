﻿CREATE TABLE [WH].[LocationDirectorate] (
    [LocationID]    INT          NOT NULL,
    [DirectorateID] INT          NOT NULL,
    [StartDate]     DATE         NOT NULL,
    [EndDate]       DATE         NULL,
    [Created]       DATETIME     NOT NULL,
    [Updated]       DATETIME     NULL,
    [ByWhom]        VARCHAR (50) NULL,
    CONSTRAINT [PK_LocalLocationDirectorate] PRIMARY KEY CLUSTERED ([LocationID] ASC, [StartDate] ASC)
);


﻿CREATE TABLE [WH].[HRG4] (
    [HRGID]               INT           IDENTITY (1, 1) NOT NULL,
    [HRGCode]             VARCHAR (5)   NOT NULL,
    [HRG]                 VARCHAR (MAX) NOT NULL,
    [CoreOrUnbundledCode] VARCHAR (3)   NOT NULL,
    [HRGSubChapterCode]   CHAR (2)      NOT NULL,
    CONSTRAINT [PK_WH_HRG4] PRIMARY KEY CLUSTERED ([HRGCode] ASC),
    CONSTRAINT [FK_HRG4_HRG4SubChapter] FOREIGN KEY ([HRGSubChapterCode]) REFERENCES [WH].[HRG4SubChapter] ([HRGSubChapterCode])
);


﻿CREATE TABLE [WH].[HRG4Chapter] (
    [HRGChapterCode] CHAR (1)      NOT NULL,
    [HRGChapter]     VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_WH_HRG4Chapter] PRIMARY KEY CLUSTERED ([HRGChapterCode] ASC)
);


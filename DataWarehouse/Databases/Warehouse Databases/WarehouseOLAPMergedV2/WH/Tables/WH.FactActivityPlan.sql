﻿CREATE TABLE [WH].[FactActivityPlan] (
    [Contracting_ID]        INT          NULL,
    [DateID]                INT          NOT NULL,
    [MetricID]              INT          NOT NULL,
    [PointOfDeliveryID]     INT          NULL,
    [ContractFlagID]        INT          NULL,
    [DirectorateID]         INT          NOT NULL,
    [NationalSpecialtyCode] NVARCHAR (3) NULL,
    [ServiceID]             INT          NULL,
    [HRGID]                 INT          NOT NULL,
    [Cases]                 FLOAT (53)   NULL,
    [Value]                 FLOAT (53)   NULL,
    [ToDateFlag]            BIT          NULL,
    [ToFreezeFlag]          BIT          NULL
);


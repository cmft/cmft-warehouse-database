﻿CREATE TABLE [WH].[SubSpecialtyBase] (
    [SubSpecialtyID] INT          IDENTITY (1, 1) NOT NULL,
    [SubSpecialty]   VARCHAR (50) NULL,
    CONSTRAINT [PK_WH.SubSpecialtyBase] PRIMARY KEY CLUSTERED ([SubSpecialtyID] ASC)
);


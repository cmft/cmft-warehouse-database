﻿CREATE TABLE [WH].[WaitTypeBase] (
    [WaitTypeID]       INT           NOT NULL,
    [WaitType]         VARCHAR (255) NOT NULL,
    [ParentWaitTypeID] INT           NULL,
    CONSTRAINT [PK_WaitTypeBase] PRIMARY KEY CLUSTERED ([WaitTypeID] ASC),
    CONSTRAINT [FK_WaitTypeBase_WaitTypeBase] FOREIGN KEY ([ParentWaitTypeID]) REFERENCES [WH].[WaitTypeBase] ([WaitTypeID])
);


﻿CREATE TABLE [WH].[WaitStatusBase] (
    [WaitStatusID]         INT           IDENTITY (1, 1) NOT NULL,
    [WaitStatusCode]       VARCHAR (50)  NOT NULL,
    [WaitStatus]           VARCHAR (255) NOT NULL,
    [ParentWaitStatusCode] VARCHAR (50)  NULL,
    CONSTRAINT [PK_WaitStatusBase] PRIMARY KEY CLUSTERED ([WaitStatusID] ASC)
);


﻿CREATE TABLE [WH].[CategoryBase] (
    [CategoryCode]       VARCHAR (50)  NOT NULL,
    [Category]           VARCHAR (255) NOT NULL,
    [ParentCategoryCode] VARCHAR (50)  NULL
);


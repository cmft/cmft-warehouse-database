﻿CREATE TABLE [WH].[ServiceGroupBase] (
    [ServiceGroupID] INT          IDENTITY (1, 1) NOT NULL,
    [ServiceGroup]   VARCHAR (50) NULL,
    CONSTRAINT [PK_WH.ServiceGroup] PRIMARY KEY CLUSTERED ([ServiceGroupID] ASC)
);


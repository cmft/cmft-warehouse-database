﻿CREATE TABLE [WH].[TimeBandBase] (
    [TimeBandCode]      INT          NOT NULL,
    [MinuteBand]        VARCHAR (5)  NOT NULL,
    [FiveMinuteBand]    VARCHAR (13) NOT NULL,
    [FifteenMinuteBand] VARCHAR (13) NOT NULL,
    [ThirtyMinuteBand]  VARCHAR (13) NOT NULL,
    [HourBand]          VARCHAR (13) NOT NULL,
    CONSTRAINT [PK_TimeBandBase] PRIMARY KEY CLUSTERED ([TimeBandCode] ASC)
);


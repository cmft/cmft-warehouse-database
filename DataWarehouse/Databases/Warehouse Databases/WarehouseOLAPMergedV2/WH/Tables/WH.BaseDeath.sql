﻿CREATE TABLE [WH].[BaseDeath] (
    [NHSNumber]   VARCHAR (17) NOT NULL,
    [DateOfDeath] DATE         NULL,
    CONSTRAINT [PK_DeathBase] PRIMARY KEY CLUSTERED ([NHSNumber] ASC)
);


﻿CREATE TABLE [WH].[Member] (
    [AttributeCode]     VARCHAR (20)  NOT NULL,
    [Attribute]         VARCHAR (100) NOT NULL,
    [SourceContextID]   INT           NOT NULL,
    [SourceContextCode] VARCHAR (20)  NOT NULL,
    [SourceContext]     VARCHAR (100) NOT NULL,
    [SourceValueID]     INT           NOT NULL,
    [SourceValueCode]   VARCHAR (100) NOT NULL,
    [SourceValue]       VARCHAR (900) NOT NULL,
    [LocalValueID]      INT           NULL,
    [LocalValueCode]    VARCHAR (50)  NULL,
    [LocalValue]        VARCHAR (200) NULL,
    [NationalValueID]   INT           NULL,
    [NationalValueCode] VARCHAR (50)  NULL,
    [NationalValue]     VARCHAR (200) NULL,
    [OtherValueID]      INT           NULL,
    [OtherValueCode]    VARCHAR (200) NULL,
    [OtherValue]        VARCHAR (200) NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [IDX_SourceValueId]
    ON [WH].[Member]([SourceValueID] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_WHMember_ValueIDlkp]
    ON [WH].[Member]([AttributeCode] ASC, [SourceContextCode] ASC, [SourceValueCode] ASC)
    INCLUDE([SourceValueID]);


GO
CREATE NONCLUSTERED INDEX [IX_WH_Member_1]
    ON [WH].[Member]([AttributeCode] ASC)
    INCLUDE([LocalValueID], [NationalValueCode], [SourceValueCode], [SourceValueID]);


GO
CREATE NONCLUSTERED INDEX [IDX_WH_Member_2]
    ON [WH].[Member]([SourceValueCode] ASC, [SourceContextCode] ASC)
    INCLUDE([NationalValue], [NationalValueCode]);


GO
CREATE NONCLUSTERED INDEX [IX_WHMember_AttCode]
    ON [WH].[Member]([AttributeCode] ASC)
    INCLUDE([SourceContextCode], [SourceValueCode], [SourceValue], [NationalValueCode], [NationalValue]);


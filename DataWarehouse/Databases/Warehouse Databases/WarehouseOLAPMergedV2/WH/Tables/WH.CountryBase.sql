﻿CREATE TABLE [WH].[CountryBase] (
    [CountryID]   INT           IDENTITY (1, 1) NOT NULL,
    [CountryCode] VARCHAR (10)  NOT NULL,
    [Country]     VARCHAR (200) NOT NULL,
    [Created]     DATETIME      NULL,
    [ByWhom]      VARCHAR (50)  NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);


﻿CREATE TABLE [WH].[LocationTypeBase] (
    [LocationTypeID]   INT          IDENTITY (1, 1) NOT NULL,
    [LocationTypeCode] VARCHAR (10) NOT NULL,
    [LocationType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_LocationType] PRIMARY KEY CLUSTERED ([LocationTypeID] ASC)
);


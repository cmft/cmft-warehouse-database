﻿CREATE TABLE [WH].[OrganisationTypeBase] (
    [OrganisationTypeID]   INT           IDENTITY (1, 1) NOT NULL,
    [OrganisationTypeCode] VARCHAR (10)  NOT NULL,
    [OrganisationType]     VARCHAR (200) NOT NULL,
    [Created]              DATETIME      NULL,
    [ByWhom]               VARCHAR (50)  NULL,
    CONSTRAINT [PK_OrganisationType] PRIMARY KEY CLUSTERED ([OrganisationTypeID] ASC)
);


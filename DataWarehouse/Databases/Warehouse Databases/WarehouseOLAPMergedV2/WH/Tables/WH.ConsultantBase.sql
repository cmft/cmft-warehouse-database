﻿CREATE TABLE [WH].[ConsultantBase] (
    [ConsultantID]            INT           NOT NULL,
    [ConsultantCode]          VARCHAR (20)  NULL,
    [Consultant]              VARCHAR (100) NULL,
    [ProviderCode]            VARCHAR (20)  NULL,
    [MainSourceSpecialtyCode] VARCHAR (20)  NULL,
    [MainSourceSpecialty]     VARCHAR (100) NULL,
    [MainSpecialtyCode]       VARCHAR (20)  NULL,
    [MainSpecialty]           VARCHAR (100) NULL,
    [Title]                   VARCHAR (6)   NULL,
    [Initials]                VARCHAR (6)   NULL,
    [Surname]                 VARCHAR (50)  NULL,
    [ContextCode]             VARCHAR (20)  NOT NULL,
    CONSTRAINT [PK_ConsultantBase] PRIMARY KEY CLUSTERED ([ConsultantID] ASC)
);


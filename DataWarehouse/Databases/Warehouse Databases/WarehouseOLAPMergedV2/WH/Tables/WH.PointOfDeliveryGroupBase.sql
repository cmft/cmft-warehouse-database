﻿CREATE TABLE [WH].[PointOfDeliveryGroupBase] (
    [PointOfDeliveryGroupID] INT           IDENTITY (1, 1) NOT NULL,
    [PointOfDeliveryGroup]   VARCHAR (100) NULL,
    CONSTRAINT [PK__PointOfD__1EFD6DFB515C8D8C] PRIMARY KEY CLUSTERED ([PointOfDeliveryGroupID] ASC)
);


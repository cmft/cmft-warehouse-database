﻿CREATE TABLE [WH].[DiagnosisDuration] (
    [DiagnosisCode] VARCHAR (6) NOT NULL,
    [Days]          INT         NOT NULL,
    CONSTRAINT [PK__Diagnosi__A43A221932A2FC42] PRIMARY KEY CLUSTERED ([DiagnosisCode] ASC)
);


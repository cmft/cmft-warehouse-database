﻿CREATE TABLE [WH].[FGMTypeBase] (
    [FGMTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [FGMType]   VARCHAR (100) NOT NULL,
    [Created]   DATETIME      NULL,
    [ByWhom]    VARCHAR (50)  NULL,
    CONSTRAINT [PK_FGMType] PRIMARY KEY CLUSTERED ([FGMTypeID] ASC)
);


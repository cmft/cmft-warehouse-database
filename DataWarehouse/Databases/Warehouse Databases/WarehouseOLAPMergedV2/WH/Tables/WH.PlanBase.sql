﻿CREATE TABLE [WH].[PlanBase] (
    [DirectorateCode]             VARCHAR (5)      NULL,
    [Division]                    VARCHAR (50)     NULL,
    [SpecialtyCode]               VARCHAR (50)     NULL,
    [NationalSpecialty]           VARCHAR (50)     NULL,
    [ProfileName]                 VARCHAR (20)     NULL,
    [PeriodKey]                   VARCHAR (10)     NULL,
    [PlanAmount]                  DECIMAL (18, 12) NULL,
    [CumulativePlanAmount]        DECIMAL (18, 12) NULL,
    [Period]                      VARCHAR (1)      NULL,
    [NationalFirstAttendanceCode] VARCHAR (10)     NULL,
    [PatientCategoryCode]         VARCHAR (2)      NULL,
    [ActivityDate]                DATE             NULL,
    [InOut]                       VARCHAR (10)     NULL,
    [ActivitySpecialtyGroup]      VARCHAR (50)     NULL,
    [ActivityDivision]            VARCHAR (50)     NULL,
    [ActivityTotal]               INT              NULL,
    [ServiceID]                   INT              NULL
);


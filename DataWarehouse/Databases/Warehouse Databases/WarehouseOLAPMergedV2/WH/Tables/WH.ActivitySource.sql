﻿CREATE TABLE [WH].[ActivitySource] (
    [SourceID] INT          IDENTITY (1, 1) NOT NULL,
    [Source]   VARCHAR (50) NULL,
    CONSTRAINT [PK_ActivitySource] PRIMARY KEY CLUSTERED ([SourceID] ASC)
);


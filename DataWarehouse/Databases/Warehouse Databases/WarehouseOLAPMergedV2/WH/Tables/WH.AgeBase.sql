﻿CREATE TABLE [WH].[AgeBase] (
    [AgeID]         INT          IDENTITY (1, 1) NOT NULL,
    [AgeCode]       VARCHAR (50) NOT NULL,
    [Age]           VARCHAR (50) NULL,
    [AgeParentCode] VARCHAR (50) NULL,
    [AgeInMonths]   INT          NULL,
    CONSTRAINT [PK_AgeBase] PRIMARY KEY CLUSTERED ([AgeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AgeBase]
    ON [WH].[AgeBase]([AgeCode] ASC);


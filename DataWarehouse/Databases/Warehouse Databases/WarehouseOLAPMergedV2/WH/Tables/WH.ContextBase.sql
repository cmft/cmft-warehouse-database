﻿CREATE TABLE [WH].[ContextBase] (
    [ContextID]   INT           IDENTITY (1, 1) NOT NULL,
    [ContextCode] VARCHAR (MAX) NOT NULL,
    [Context]     VARCHAR (MAX) NOT NULL,
    [Location]    VARCHAR (8)   NOT NULL,
    CONSTRAINT [PK_ContextBase] PRIMARY KEY CLUSTERED ([ContextID] ASC)
);


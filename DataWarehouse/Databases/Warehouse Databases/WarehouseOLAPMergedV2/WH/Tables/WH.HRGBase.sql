﻿CREATE TABLE [WH].[HRGBase] (
    [HRGID]         INT           IDENTITY (1, 1) NOT NULL,
    [HRGCode]       VARCHAR (50)  NULL,
    [HRG]           VARCHAR (100) NOT NULL,
    [HRGChapter]    VARCHAR (3)   NULL,
    [HRGSubChapter] VARCHAR (3)   NULL,
    [HighCost]      BIT           NULL,
    CONSTRAINT [PK__HRGBase__5AACB96003FD1BF2] PRIMARY KEY CLUSTERED ([HRGID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_HRGBase_HRGCode]
    ON [WH].[HRGBase]([HRGCode] ASC);


﻿CREATE TABLE [WH].[ServiceBase] (
    [ServiceID]             INT          IDENTITY (1, 1) NOT NULL,
    [Service]               VARCHAR (50) NOT NULL,
    [ServiceGroupID]        INT          NOT NULL,
    [NationalSpecialtyCode] VARCHAR (3)  NULL,
    [Priority]              INT          NOT NULL,
    [Active]                BIT          NOT NULL,
    [ContextCode]           VARCHAR (20) CONSTRAINT [DF_ServiceBase_ContextCode] DEFAULT ('SERVICE') NOT NULL,
    CONSTRAINT [PK_WH.ServiceBase] PRIMARY KEY CLUSTERED ([ServiceID] ASC),
    CONSTRAINT [FK_ServiceBase_ServiceGroup] FOREIGN KEY ([ServiceGroupID]) REFERENCES [WH].[ServiceGroupBase] ([ServiceGroupID])
);


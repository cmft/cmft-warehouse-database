﻿CREATE TABLE [WH].[DurationBase] (
    [DurationID]            INT          IDENTITY (1, 1) NOT NULL,
    [DurationCode]          VARCHAR (10) NOT NULL,
    [Duration]              VARCHAR (50) NULL,
    [DurationBandCode]      VARCHAR (10) NULL,
    [APCWLDurationBandCode] VARCHAR (10) NULL,
    [APCWLDurationBand]     VARCHAR (50) NULL,
    [APCWLBreach26BandCode] VARCHAR (10) NULL,
    [APCWLBreach26Band]     VARCHAR (50) NULL,
    [APCWLBreach18BandCode] VARCHAR (10) NULL,
    [APCWLBreach18Band]     VARCHAR (50) NULL,
    [OPWLDurationBandCode]  VARCHAR (10) NULL,
    [OPWLDurationBand]      VARCHAR (50) NULL,
    [OPWLBreachBandCode]    VARCHAR (10) NULL,
    [OPWLBreachBand]        VARCHAR (50) NULL,
    CONSTRAINT [PK_DurationBase_1] PRIMARY KEY CLUSTERED ([DurationID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DurationBase]
    ON [WH].[DurationBase]([DurationCode] ASC);


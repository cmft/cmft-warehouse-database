﻿CREATE TABLE [WH].[WaitTargetBase] (
    [WaitTargetTypeCode] VARCHAR (10) NOT NULL,
    [WaitTargetID]       INT          NOT NULL,
    [WaitTarget]         VARCHAR (10) NULL,
    CONSTRAINT [PK_WaitTargetBase] PRIMARY KEY CLUSTERED ([WaitTargetTypeCode] ASC, [WaitTargetID] ASC)
);


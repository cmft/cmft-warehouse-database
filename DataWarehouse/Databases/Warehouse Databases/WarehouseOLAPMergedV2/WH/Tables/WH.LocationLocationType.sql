﻿CREATE TABLE [WH].[LocationLocationType] (
    [LocationID]     INT NOT NULL,
    [LocationTypeID] INT NULL,
    CONSTRAINT [PK_LocationLocationType] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);


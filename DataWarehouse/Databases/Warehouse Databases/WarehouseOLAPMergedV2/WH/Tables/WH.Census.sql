﻿CREATE TABLE [WH].[Census] (
    [CensusDateID]  INT           NOT NULL,
    [CensusDate]    SMALLDATETIME NOT NULL,
    [Census]        VARCHAR (11)  NULL,
    [LastInMonth]   INT           NOT NULL,
    [CurrentCensus] INT           NOT NULL,
    CONSTRAINT [PK_Census_1] PRIMARY KEY CLUSTERED ([CensusDateID] ASC)
);


﻿CREATE TABLE [WH].[HRG4SubChapter] (
    [HRGSubChapterCode] CHAR (2)      NOT NULL,
    [HRGSubChapter]     VARCHAR (MAX) NOT NULL,
    [HRGChapterCode]    CHAR (1)      NOT NULL,
    CONSTRAINT [PK_WH_HRG4SubChapter] PRIMARY KEY CLUSTERED ([HRGSubChapterCode] ASC),
    CONSTRAINT [FK_HRG4SubChapter_HRG4Chapter] FOREIGN KEY ([HRGChapterCode]) REFERENCES [WH].[HRG4Chapter] ([HRGChapterCode])
);


﻿CREATE TABLE [WH].[DiagnosisBase] (
    [DiagnosisID]              INT           IDENTITY (1, 1) NOT NULL,
    [DiagnosisCode]            VARCHAR (6)   NOT NULL,
    [AlternativeDiagnosisCode] VARCHAR (5)   NULL,
    [UsageCode]                INT           NULL,
    [Diagnosis]                VARCHAR (300) NULL,
    [Release]                  VARCHAR (5)   NOT NULL,
    CONSTRAINT [PK_DiagnosisBase] PRIMARY KEY CLUSTERED ([DiagnosisID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DiagnosisCode_DiagnosisBase]
    ON [WH].[DiagnosisBase]([DiagnosisCode] ASC);


﻿CREATE TABLE [WH].[ConsultantBase20130516] (
    [ConsultantID]      INT           NULL,
    [ConsultantCode]    VARCHAR (MAX) NULL,
    [Consultant]        VARCHAR (MAX) NULL,
    [ProviderCode]      VARCHAR (MAX) NULL,
    [MainSpecialtyCode] VARCHAR (MAX) NULL,
    [MainSpecialty]     VARCHAR (MAX) NULL,
    [Title]             VARCHAR (6)   NULL,
    [Initials]          VARCHAR (6)   NULL,
    [Surname]           VARCHAR (50)  NULL,
    [ContextCode]       VARCHAR (8)   NOT NULL
);


﻿CREATE TABLE [WH].[GPPracticeBase] (
    [GpPracticeID]                            INT            IDENTITY (1, 1) NOT NULL,
    [GpPracticeCode]                          NVARCHAR (8)   NOT NULL,
    [GpPracticeName]                          NVARCHAR (100) NULL,
    [ROCode]                                  NVARCHAR (3)   NULL,
    [HACode]                                  NVARCHAR (3)   NULL,
    [AddressLine1]                            NVARCHAR (35)  NULL,
    [AddressLine2]                            NVARCHAR (35)  NULL,
    [AddressLine3]                            NVARCHAR (35)  NULL,
    [AddressLine4]                            NVARCHAR (35)  NULL,
    [AddressLine5]                            NVARCHAR (35)  NULL,
    [Postcode]                                NVARCHAR (8)   NULL,
    [PracticeOpenDate]                        DATE           NULL,
    [PracticeCloseDate]                       DATE           NULL,
    [StatusCode]                              NVARCHAR (1)   NULL,
    [OrganisationSubTypeCode]                 NVARCHAR (1)   NULL,
    [ParentOrganisationCode]                  NVARCHAR (6)   NULL,
    [JoinParentDate]                          DATE           NULL,
    [LeftParentDate]                          DATE           NULL,
    [ContactTelephone]                        NVARCHAR (12)  NULL,
    [ContactName]                             NVARCHAR (50)  NULL,
    [AmendedRecordIndicator]                  NVARCHAR (1)   NULL,
    [ElectronicCorrespondenceActivatedTime]   SMALLDATETIME  NULL,
    [ElectronicCorrespondenceDeactivatedTime] SMALLDATETIME  NULL,
    [Created]                                 DATETIME       NULL,
    [Updated]                                 DATETIME       NULL,
    [ByWhom]                                  VARCHAR (50)   NULL,
    CONSTRAINT [PK_OrganisationID] PRIMARY KEY CLUSTERED ([GpPracticeID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_GPB_GPC-temp]
    ON [WH].[GPPracticeBase]([GpPracticeCode] ASC, [GpPracticeID] ASC, [ElectronicCorrespondenceActivatedTime] ASC);


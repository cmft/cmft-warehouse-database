﻿CREATE TABLE [WH].[DirectorateBase] (
    [DirectorateID]   INT           IDENTITY (1, 1) NOT NULL,
    [DirectorateCode] VARCHAR (5)   NOT NULL,
    [Directorate]     VARCHAR (50)  NOT NULL,
    [DivisionCode]    VARCHAR (10)  NULL,
    [Division]        VARCHAR (50)  NOT NULL,
    [DivisionLabel]   VARCHAR (100) NULL,
    CONSTRAINT [PK_DirectorateBase] PRIMARY KEY CLUSTERED ([DirectorateID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_DB_DCode]
    ON [WH].[DirectorateBase]([DirectorateCode] ASC);


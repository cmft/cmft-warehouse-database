﻿CREATE TABLE [WH].[FactActivityBooking] (
    [MergeEncounterRecno] INT NOT NULL,
    [TCIDateID]           INT NOT NULL,
    [SourceID]            INT NOT NULL,
    [ConsolidatedView]    BIT NULL,
    [MetricID]            INT NOT NULL,
    [PointOfDeliveryID]   INT NULL,
    [DirectorateID]       INT NOT NULL,
    [SpecialtyID]         INT NOT NULL,
    [ServiceID]           INT NOT NULL,
    [EncounterRecno]      INT NOT NULL,
    [Cases]               INT NOT NULL,
    [CensusDateID]        INT NOT NULL,
    [Reportable]          BIT NULL
);


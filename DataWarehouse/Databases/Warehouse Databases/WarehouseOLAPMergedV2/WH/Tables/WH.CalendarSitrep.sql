﻿CREATE TABLE [WH].[CalendarSitrep] (
    [SitrepQuarter]       CHAR (5) NULL,
    [SitrepMonth]         CHAR (3) NULL,
    [SitrepWeekNo]        CHAR (2) NULL,
    [SitrepFromDate]      DATE     NOT NULL,
    [SitrepToDate]        DATE     NOT NULL,
    [SitrepFinancialYear] CHAR (9) NULL
);


﻿CREATE TABLE [WH].[Operation] (
    [OperationID]        INT           IDENTITY (1, 1) NOT NULL,
    [OperationCode]      VARCHAR (5)   NOT NULL,
    [Operation]          VARCHAR (68)  NULL,
    [OperationGroupCode] VARCHAR (3)   NULL,
    [OperationGroup]     VARCHAR (61)  NULL,
    [OperationChapter]   VARCHAR (113) NULL,
    CONSTRAINT [PK_BaseOperation_1] PRIMARY KEY CLUSTERED ([OperationID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_op_oc_temp]
    ON [WH].[Operation]([OperationCode] ASC);


﻿CREATE TABLE [WH].[ChameleonPreference] (
    [AttributeCode] VARCHAR (20) NOT NULL,
    [ContextCode]   VARCHAR (20) NULL,
    [Created]       DATETIME     NULL,
    [Updated]       DATETIME     NULL,
    [ByWhom]        VARCHAR (50) NULL,
    CONSTRAINT [PK_ChameleonPreference] PRIMARY KEY CLUSTERED ([AttributeCode] ASC)
);


﻿CREATE TABLE [WH].[ActivityMetric] (
    [MetricCode]       VARCHAR (20) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentCode] VARCHAR (20) NULL,
    [MetricID]         INT          IDENTITY (1, 1) NOT NULL,
    [ParentMetricID]   INT          NULL,
    [ReportOrder]      INT          NULL,
    CONSTRAINT [PK_ActivityMetric] PRIMARY KEY CLUSTERED ([MetricCode] ASC),
    CONSTRAINT [FK_ActivityMetric_ActivityMetric] FOREIGN KEY ([MetricParentCode]) REFERENCES [WH].[ActivityMetric] ([MetricCode])
);


﻿CREATE TABLE [WH].[PointOfDeliverySubgroupBase] (
    [PointOfDeliverySubgroupID]   INT           IDENTITY (1, 1) NOT NULL,
    [PointOfDeliverySubgroupCode] VARCHAR (100) NULL,
    [PointOfDeliverySubgroup]     VARCHAR (100) NULL,
    [PointOfDeliveryGroupID]      INT           NULL,
    CONSTRAINT [PK__PointOfD__1EFD6DFB43788289] PRIMARY KEY CLUSTERED ([PointOfDeliverySubgroupID] ASC),
    CONSTRAINT [FK_PointOfDeliveryGroupID] FOREIGN KEY ([PointOfDeliveryGroupID]) REFERENCES [WH].[PointOfDeliveryGroupBase] ([PointOfDeliveryGroupID])
);


﻿CREATE TABLE [WH].[PointOfDeliveryBase] (
    [PointOfDeliveryID]         INT           NOT NULL,
    [PointOfDeliveryCode]       VARCHAR (50)  NULL,
    [PointOfDelivery]           VARCHAR (100) NULL,
    [PointOfDeliverySubGroupID] INT           NULL,
    CONSTRAINT [PK__PointOfD__3DAC9F013FA7F1A5] PRIMARY KEY CLUSTERED ([PointOfDeliveryID] ASC),
    CONSTRAINT [FK_PointOfDeliverySubGroupID] FOREIGN KEY ([PointOfDeliverySubGroupID]) REFERENCES [WH].[PointOfDeliverySubgroupBase] ([PointOfDeliverySubgroupID])
);


﻿CREATE TABLE [WH].[SystemUserBase] (
    [SystemUserID]    INT           IDENTITY (1, 1) NOT NULL,
    [UserID]          VARCHAR (20)  NOT NULL,
    [ContextCode]     VARCHAR (10)  NOT NULL,
    [Username]        VARCHAR (255) NULL,
    [AlternateUserId] VARCHAR (20)  NULL,
    [HospitalCode]    VARCHAR (10)  NULL,
    CONSTRAINT [PK_SystemUser] PRIMARY KEY CLUSTERED ([UserID] ASC, [ContextCode] ASC)
);


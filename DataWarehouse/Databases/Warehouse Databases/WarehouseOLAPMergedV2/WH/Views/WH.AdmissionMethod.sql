﻿create view [WH].[AdmissionMethod] as

select
	 Member.SourceValueID
	,AdmissionMethodID = Member.SourceValueCode
	,AdmissionMethod = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member
where
	Member.AttributeCode = 'ADMMTH'



﻿





CREATE view [WH].[Sex] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceSexID = SourceValueID
	,SourceSexCode = SourceValueCode
	,SourceSex = SourceValue
	,LocalSexID = LocalValueID
	,LocalSexCode = LocalValueCode
	,LocalSex = LocalValue
	,NationalSexID = NationalValueID
	,NationalSexCode = NationalValueCode
	,NationalSex = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'SEX'








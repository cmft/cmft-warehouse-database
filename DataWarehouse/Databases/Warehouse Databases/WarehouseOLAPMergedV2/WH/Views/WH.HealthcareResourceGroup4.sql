﻿CREATE view WH.HealthcareResourceGroup4 as

select
	 HRG4.HRGID
	,HRG4.HRGCode
	,HRG = HRG4.HRGCode + ' - ' + HRG4.HRG
	,HRG4SubChapter.HRGSubChapter
	,HRG4Chapter.HRGChapter
from
	WH.HRG4

inner join WH.HRG4SubChapter
on	HRG4SubChapter.HRGSubChapterCode = HRG4.HRGSubChapterCode

inner join WH.HRG4Chapter
on	HRG4Chapter.HRGChapterCode = HRG4SubChapter.HRGChapterCode

union all

select
	 HRGID = -1
	,HRGCode = 'N/A'
	,HRG = 'N/A'
	,HRGSubChapter = 'N/A'
	,HRGChapter = 'N/A'

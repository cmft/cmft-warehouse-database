﻿



CREATE view [WH].[MaritalStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceMaritalStatusID = SourceValueID
	,SourceMaritalStatusCode = SourceValueCode
	,SourceMaritalStatus = SourceValue
	,LocalMaritalStatusID = LocalValueID
	,LocalMaritalStatusCode = LocalValueCode
	,LocalMaritalStatus = LocalValue
	,NationalMaritalStatusID = NationalValueID
	,NationalMaritalStatusCode = NationalValueCode
	,NationalMaritalStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'MARSTA'






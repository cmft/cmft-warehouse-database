﻿


CREATE view WH.[ProfessionalCarer] as

select
	 SourceContextCode
	,SourceContextID
	,SourceContext
	,SourceProfessionalCarerID = SourceValueID
	,SourceProfessionalCarerCode = SourceValueCode
	,SourceProfessionalCarer = SourceValue
	,LocalProfessionalCarerID = LocalValueID
	,LocalProfessionalCarerCode = LocalValueCode
	,LocalProfessionalCarer = LocalValue
	,NationalProfessionalCarerID = NationalValueID
	,NationalProfessionalCarerCode = NationalValueCode
	,NationalProfessionalCarer = NationalValue

	,ProfessionalCarer = SourceValue
from
	WH.Member
where
	AttributeCode = 'PROFCARER'

union all

select
	 SourceContextCode = 'N/A'
	,SourceContextID = null
	,SourceContext = 'N/A'
	,SourceProfessionalCarerID = -1
	,SourceProfessionalCarerCode = '-1'
	,SourceProfessionalCarer = 'Not Specified'
	,LocalProfessionalCarerID = null
	,LocalProfessionalCarerCode = null
	,LocalProfessionalCarer = null
	,NationalProfessionalCarerID = null
	,NationalProfessionalCarerCode = null
	,NationalProfessionalCarer = null

	,ProfessionalCarer = 'Not Specified'


----ID are guaranteed unique as they all pass through the ReferenceMap database
--select
--	 SourceProfessionalCarerID = Consultant.SourceConsultantID
--	,ProfessionalCarer = Consultant.SourceConsultant
--from
--	WH.Consultant

--union all

--select
--	 SourceProfessionalCarerID = ProfessionalCarer.SourceValueID
--	,ProfessionalCarer = ProfessionalCarer.ProfessionalCarer
--from
--	COM.ProfessionalCarer

--union all

--select
--	 SourceProfessionalCarerID = StaffMember.SourceStaffMemberID
--	,ProfessionalCarer = StaffMember.SourceStaffMember
--from
--	AE.StaffMember




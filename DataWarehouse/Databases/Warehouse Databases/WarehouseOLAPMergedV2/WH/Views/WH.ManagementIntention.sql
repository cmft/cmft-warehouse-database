﻿create view [WH].[ManagementIntention] as

select
	 Member.SourceValueID
	,ManagementIntentionID = Member.SourceValueCode
	,ManagementIntention = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member
where
	Member.AttributeCode = 'MGTINT'



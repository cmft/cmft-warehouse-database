﻿

CREATE view [WH].[Service] as

select
	 ServiceID = Allocation.AllocationID
	,SourceServiceID = Allocation.SourceAllocationID
	,Service = Allocation.Allocation

	,ServiceBase.ServiceGroupID
	,ServiceGroupBase.ServiceGroup
from
	Allocation.Allocation

inner join WH.ServiceBase
on	ServiceBase.ServiceID = Allocation.SourceAllocationID

inner join WH.ServiceGroupBase
on	ServiceGroupBase.ServiceGroupID = ServiceBase.ServiceGroupID

where
	AllocationTypeID = 2




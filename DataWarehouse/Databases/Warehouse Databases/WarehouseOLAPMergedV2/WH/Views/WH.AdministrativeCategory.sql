﻿







CREATE view [WH].[AdministrativeCategory] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAdministrativeCategoryID = SourceValueID
	,SourceAdministrativeCategoryCode = SourceValueCode
	,SourceAdministrativeCategory = SourceValue
	,LocalAdministrativeCategoryID = LocalValueID
	,LocalAdministrativeCategoryCode = LocalValueCode
	,LocalAdministrativeCategory = LocalValue
	,NationalAdministrativeCategoryID = NationalValueID
	,NationalAdministrativeCategoryCode = NationalValueCode
	,NationalAdministrativeCategory = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ADMCAT'










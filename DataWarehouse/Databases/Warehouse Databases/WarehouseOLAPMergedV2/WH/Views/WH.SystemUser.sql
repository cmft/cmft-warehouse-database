﻿




CREATE view [WH].[SystemUser] as

select
	 SystemUserID = SystemUserMap.SourceSystemUserID
	,UserID = SystemUserMap.SourceSystemUserCode
	,ContextCode = SystemUserMap.SourceContextCode
	,ContextID = SystemUserMap.SourceContextID
	,Username = SystemUserMap.SourceSystemUser
	,SystemUserBase.AlternateUserId
	,SiteID =
		coalesce(
			 Site.SourceSiteID
			,NullSite.SourceSiteID
		)
from
	(
	select
		 SourceContextCode
		,SourceContext
		,SourceContextID
		,SourceSystemUserID = SourceValueID
		,SourceSystemUserCode = SourceValueCode
		,SourceSystemUser = SourceValue
		,LocalSystemUserID = LocalValueID
		,LocalSystemUserCode = LocalValueCode
		,LocalSystemUser = LocalValue
		,NationalSystemUserID = NationalValueID
		,NationalSystemUserCode = NationalValueCode
		,NationalSystemUser = NationalValue
	from
		WH.Member
	where
		AttributeCode = 'SYSUSER'
	) SystemUserMap

left join WH.SystemUserBase
on	SystemUserBase.UserID = SystemUserMap.SourceSystemUserCode
and	SystemUserBase.ContextCode = SystemUserMap.SourceContextCode

left join WH.Site
on	Site.SourceSiteCode = SystemUserBase.HospitalCode
and	Site.SourceContextCode = SystemUserBase.ContextCode

left join WH.Site NullSite
on	NullSite.SourceSiteCode = '-1'
and	NullSite.SourceContextCode = SystemUserBase.ContextCode

--where
--	SourceSystemUserID not in ( 3393596)
	
union all

select
	 SystemUserID = -1
	,UserID = 'Not Specified'
	,ContextCode = null
	,ContextID = null
	,Username = 'Not Specified'
	,AlternateUserId = null
	,SiteID = null
	



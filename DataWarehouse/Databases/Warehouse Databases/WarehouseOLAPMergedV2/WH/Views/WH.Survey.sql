﻿





CREATE view [WH].[Survey]

as

select
	SourceContextCode = Member.SourceContextCode
	,SourceContext = Member.SourceContext
	,SourceSurveyID = Member.SourceValueID 
	,SourceSurveyCode = Member.SourceValueCode
	,SourceSurvey = Member.SourceValue
	,LocalSurveyCode = Member.LocalValueCode
	,LocalSurvey = Member.LocalValue
	,NationalSurveyID = Member.NationalValueID
	,NationalSurveyCode = Member.NationalValueCode
	,NationalSurvey = Member.NationalValue
	--,SurveyChannelCode = Channel.ChannelID
	,Channel = Channel.Channel
	
from
	(	
	select --'CMFT||CRT'
		SurveyID
		,Survey
		,ChannelID = coalesce(ChannelID,6)
	from
		[$(Warehouse)].PEX.KioskOnlinePostcardTabletSurvey

	union

	select --'CMFT||HOSP'
		SurveyID
		,Survey
		,ChannelID
	from
		[$(Warehouse)].PEX.BedUnitSurvey	

	union

	select --'CMFT||ENVOY'
		SurveyID
		,Survey
		,ChannelID
	from
		[$(Warehouse)].PEX.SMSSurvey
		
	) Survey	

inner join
	[$(Warehouse)].PEX.Channel
on	Channel.ChannelID = Survey.ChannelID

inner join
	WH.Member
on	Member.SourceValueCode = Survey.SurveyID
and AttributeCode = 'SURVEY'	







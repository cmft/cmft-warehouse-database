﻿

CREATE view [WH].[Age] as

select
	 AgeBase.AgeCode
	,AgeBase.Age
	,AgeBase.AgeParentCode

	,AgeBase.AgeID

	,AgeParentID =
		ParentAgeBase.AgeID

	,AgeInYears =
		case
		when AgeBase.AgeInMonths is null
		then 'N/A'

		when AgeBase.AgeInMonths < 12
		then '0'

		when AgeBase.AgeInMonths = 9999
		then '99+'

		else right('00' + cast(FLOOR(AgeBase.AgeInMonths / 12) as varchar), 2)

		end

from
	WH.AgeBase

left join WH.AgeBase ParentAgeBase
on	ParentAgeBase.AgeCode = AgeBase.AgeParentCode




﻿







CREATE view [WH].[LocationType] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceLocationTypeID = SourceValueID
	,SourceLocationTypeCode = SourceValueCode
	,SourceLocationType = SourceValue
	,LocalLocationTypeID = LocalValueID
	,LocalLocationTypeCode = LocalValueCode
	,LocalLocationType = LocalValue
	,NationalLocationTypeID = NationalValueID
	,NationalLocationTypeCode = NationalValueCode
	,NationalLocationType = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'LOCATIONTYPE'











﻿

CREATE view [WH].[Specialty] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceSpecialtyID = SourceValueID
	,SourceSpecialtyCode = SourceValueCode
	,SourceSpecialty = SourceValue
	,LocalSpecialtyID = LocalValueID
	,LocalSpecialtyCode = LocalValueCode
	,LocalSpecialty = LocalValue
	,NationalSpecialtyID = NationalValueID
	,NationalSpecialtyCode = NationalValueCode
	,NationalSpecialty = NationalValue
	,NationalSpecialtyLabel = NationalValueCode + ' : ' + NationalValue
FROM
	WH.Member
where
	AttributeCode = 'SPEC'


﻿create view [WH].[AdmissionReason] as

select
	 Member.SourceValueID
	,AdmissionReasonID = Member.SourceValueCode
	,AdmissionReason = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member
where
	Member.AttributeCode = 'ADMREASON'



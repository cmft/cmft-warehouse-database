﻿
create view WH.[Outcome] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceOutcomeID = SourceValueID
	,SourceOutcomeCode = SourceValueCode
	,SourceOutcome = SourceValue
	,LocalOutcomeID = LocalValueID
	,LocalOutcomeCode = LocalValueCode
	,LocalOutcome = LocalValue
	,NationalOutcomeID = NationalValueID
	,NationalOutcomeCode = NationalValueCode
	,NationalOutcome = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'OUTCOME'









﻿







CREATE view [WH].[QuestionType] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceQuestionTypeID = SourceValueID
	,SourceQuestionTypeCode = SourceValueCode
	,SourceQuestionType = SourceValue
	,LocalQuestionTypeID = LocalValueID
	,LocalQuestionTypeCode = LocalValueCode
	,LocalQuestionType = LocalValue
	,NationalQuestionTypeID = NationalValueID
	,NationalQuestionTypeCode = NationalValueCode
	,NationalQuestionType = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'QUESTIONTYPE'











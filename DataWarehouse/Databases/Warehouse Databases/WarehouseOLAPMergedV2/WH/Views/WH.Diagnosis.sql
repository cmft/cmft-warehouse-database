﻿











CREATE view [WH].[Diagnosis]

as

select
	DiagnosisID
	,DiagnosisCode = Diagnosis.DiagnosisCode
	,Diagnosis = Diagnosis.DiagnosisCode + ' - ' + Diagnosis.Diagnosis
	,DiagnosisChapterCode = coalesce(DiagnosisChapterCode, 'N/A')
	,DiagnosisChapter = coalesce(DiagnosisChapterCode, 'N/A')  + ' - ' + coalesce(DiagnosisChapter, 'No Description')
	,DiagnosisGroupCode = 
				case
				when len(coalesce(CCSXref.XrefEntityCode, 'N/A')) = 1
				then '00' + CCSXref.XrefEntityCode
				when len(coalesce(CCSXref.XrefEntityCode, 'N/A')) = 2
				then '0' + CCSXref.XrefEntityCode
				else coalesce(CCSXref.XrefEntityCode, 'N/A')
				end
	,DiagnosisGroup = 
				case
				when len(coalesce(CCSXref.XrefEntityCode, 'N/A')) = 1
				then '00' + CCSXref.XrefEntityCode
				when len(coalesce(CCSXref.XrefEntityCode, 'N/A')) = 2
				then '0' + CCSXref.XrefEntityCode
				else coalesce(CCSXref.XrefEntityCode, 'N/A')
				end
				+ ' - ' + coalesce(CCSLookUp.Description, 'No Description')
	,DiagnosisGroupSHMICode = 
				case
				when len(coalesce(SHMIXref.XrefEntityCode, 'N/A')) = 1
				then '00' + SHMIXref.XrefEntityCode
				when len(coalesce(SHMIXref.XrefEntityCode, 'N/A')) = 2
				then '0' + SHMIXref.XrefEntityCode
				else coalesce(SHMIXref.XrefEntityCode, 'N/A')
				end
	,CharlsonConditionCode = 
				case
				when len(coalesce(CharlsonXref.XrefEntityCode, 'N/A')) = 1
				then '0' + CharlsonXref.XrefEntityCode
				else coalesce(CharlsonXref.XrefEntityCode, 'N/A')
				end
	,CharlsonCondition = 
				case
				when len(coalesce(CharlsonXref.XrefEntityCode, 'N/A')) = 1
				then '0' + CharlsonXref.XrefEntityCode
				else coalesce(CharlsonXref.XrefEntityCode, 'N/A')
				end
				+ ' - ' + coalesce(CharlsonLookUp.Description, 'No Description')
	,IsCharlson =
				cast(
				case
				when CharlsonXref.EntityCode is not null
				then 1
				else 0
				end
				as bit)
	,IsConnectingForHealthMandatory = 
				cast(
				case
				when ConnectingForHealth.EntityCode is not null
				then 1
				else 0
				end
				as bit)
	,IsAlwaysPresent = 
				cast(
				case
				when CMFTAlways.EntityCode is not null
				then 1
				else 0
				end
				as bit)
	,IsLongTerm = 
				cast(
				case
				when CMFTLongTerm.EntityCode is not null
				then 1
				else 0
				end
				as bit)
	,IsRCode = 
				cast(
				case
				when left(Diagnosis.DiagnosisCode, 1) = 'R'
				then 1
				else 0
				end
				as bit)
	,IsUnspecified = 
				cast(
				case
				when right(Diagnosis.DiagnosisCode, 2) = ('.9')
				then 1
				else 0
				end
				as bit)
	,IsRCodeCodingOpportunity = 
				case  
				when 
					Diagnosis.DiagnosisCode like ('R07.[1-4]')				
				or	Diagnosis.DiagnosisCode like ('R10.[0-4]')				
				or	Diagnosis.DiagnosisCode like ('R50.[019]')				
				or	Diagnosis.DiagnosisCode in ('R11.X','R53.X','R55.X')	
				then 1
				else 0
				end
	,Lifespan = DiagnosisDuration.Days


from 
	WH.DiagnosisBase Diagnosis

inner join WH.DiagnosisDuration
on	Diagnosis.DiagnosisCode = DiagnosisDuration.DiagnosisCode

left outer join [$(Warehouse)].dbo.EntityLookup ConnectingForHealth
on	ConnectingForHealth.EntityCode  = Diagnosis.DiagnosisCode
and ConnectingForHealth.EntityTypeCode = 'CFHDIAGNOSISCODE'

left outer join [$(Warehouse)].dbo.EntityLookup CMFTAlways
on	CMFTAlways.EntityCode  = Diagnosis.DiagnosisCode
and CMFTAlways.EntityTypeCode = 'CMFTALWAYSDIAGNOSISCODE'

left outer join [$(Warehouse)].dbo.EntityLookup CMFTLongTerm
on	CMFTLongTerm.EntityCode  = Diagnosis.DiagnosisCode
and CMFTLongTerm.EntityTypeCode = 'CMFTLONGTERMDIAGNOSISCODE'

left outer join [$(Warehouse)].dbo.EntityXref CharlsonXref
on	CharlsonXref.EntityCode = Diagnosis.DiagnosisCode
and CharlsonXref.EntityTypeCode = 'DIAGNOSISCODE'
and CharlsonXref.XrefEntityTypeCode = 'CHARLSONINDEXCODE'

left outer join [$(Warehouse)].dbo.EntityLookup CharlsonLookUp
on	CharlsonLookUp.EntityCode  = CharlsonXref.XrefEntityCode
and CharlsonLookUp.EntityTypeCode = 'CHARLSONINDEX'

left outer join [$(Warehouse)].dbo.EntityXref CCSXref
on	CCSXref.EntityCode = Diagnosis.DiagnosisCode
and CCSXref.EntityTypeCode = 'DIAGNOSISCODE'
and CCSXref.XrefEntityTypeCode = 'CCSCATEGORYCODE'

left outer join [$(Warehouse)].dbo.EntityLookup CCSLookUp
on	CCSLookUp.EntityCode  = CCSXref.XrefEntityCode
and CCSLookUp.EntityTypeCode = 'CCSCATEGORYCODE'

left join [$(Warehouse)].dbo.EntityXref SHMIXref
on	CCSXref.XrefEntityCode = SHMIXref.EntityCode
and SHMIXref.EntityTypeCode = 'CCSCATEGORYCODE' 
and SHMIXref.XrefEntityTypeCode = 'SHMICCSCATEGORYCODE'

left outer join 
(
select distinct
	DiagnosisChapterCode
	,DiagnosisChapter
from
	[$(Warehouse)].PAS.Diagnosis
) DiagnosisChapter

on	DiagnosisChapter.DiagnosisChapterCode = left(Diagnosis.DiagnosisCode, 3)














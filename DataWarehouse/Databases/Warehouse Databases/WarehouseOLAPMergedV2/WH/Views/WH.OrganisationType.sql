﻿
Create View WH.OrganisationType

as

select 
	OrganisationTypeID
	,OrganisationTypeCode
	,OrganisationType
from 
	WH.OrganisationTypeBase

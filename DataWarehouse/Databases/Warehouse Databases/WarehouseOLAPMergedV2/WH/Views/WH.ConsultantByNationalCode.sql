﻿
CREATE VIEW [WH].[ConsultantByNationalCode] 
AS

	/******************************************************************************
	**  Name: WH.ConsultantByNationalCode
	**  Purpose: 
	**
	**  View to return a single for each National Consultant Code 
	**
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 31.01.13    MH         Created
	******************************************************************************/

	SELECT
		 NationalConsultantCode	= CA.NationalConsultantCode
		,ConsultantName			= LTRIM(COALESCE(CA.Title,'') + ' ' + COALESCE(CA.Initials,'') + ' ' + COALESCE(CA.Surname,''))
		,NationalSpecialtyCode	= CA.MainSpecialtyCode
		,Title					= CA.Title
		,Initials				= CA.Initials
		,Surname				= CA.Surname

	FROM
		WH.Consultant CA
		LEFT JOIN WH.Consultant CB
			ON		CA.NationalConsultantCode = CB.NationalConsultantCode 
				AND CB.Surname NOT LIKE '%DO NOT USE%'
				AND CB.SourceConsultantID > CA.SourceConsultantID
	WHERE
			CB.SourceConsultantID IS NULL
		AND CA.Surname NOT LIKE '%DO NOT USE%'




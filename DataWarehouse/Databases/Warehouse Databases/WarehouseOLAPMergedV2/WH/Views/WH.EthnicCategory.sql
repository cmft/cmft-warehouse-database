﻿





CREATE view [WH].[EthnicCategory] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceEthnicCategoryID = SourceValueID
	,SourceEthnicCategoryCode = SourceValueCode
	,SourceEthnicCategory = SourceValue
	,LocalEthnicCategoryID = LocalValueID
	,LocalEthnicCategoryCode = LocalValueCode
	,LocalEthnicCategory = LocalValue
	,NationalEthnicCategoryID = NationalValueID
	,NationalEthnicCategoryCode = NationalValueCode
	,NationalEthnicCategory = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ETHCAT'








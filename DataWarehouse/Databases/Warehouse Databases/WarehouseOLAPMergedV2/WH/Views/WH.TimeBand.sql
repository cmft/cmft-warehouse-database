﻿



CREATE view [WH].[TimeBand] as

select
	 TimeBandCode
	,MinuteBand
	,FiveMinuteBand
	,FifteenMinuteBand
	,ThirtyMinuteBand
	,HourBand
from
	WH.TimeBandBase





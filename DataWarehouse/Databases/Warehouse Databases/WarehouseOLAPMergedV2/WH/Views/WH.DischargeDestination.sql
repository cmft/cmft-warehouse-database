﻿create view [WH].[DischargeDestination] as

select
	 Member.SourceValueID
	,DischargeDestinationID = Member.SourceValueCode
	,DischargeDestination = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member
where
	Member.AttributeCode = 'DISDES'



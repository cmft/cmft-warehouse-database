﻿


--/****** Object:  View [WH].[PointOfDelivery]    Script Date: 01/05/2015 14:50:55 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO





CREATE view [WH].[PointOfDelivery]

as

select 
	PointOfDeliveryID 
	,PointOfDeliveryCode
	,PointOfDelivery
	,PointOfDeliverySubgroup = PointOfDeliverySubgroupBase.PointOfDeliverySubgroup
	,PointOfDeliveryGroup = PointOfDeliveryGroupBase.PointOfDeliveryGroup
from
	WH.PointOfDeliveryBase

inner join WH.PointOfDeliverySubgroupBase
on	PointOfDeliveryBase.PointOfDeliverySubGroupID = PointOfDeliverySubgroupBase.PointOfDeliverySubgroupID

inner join WH.PointOfDeliveryGroupBase
on	PointOfDeliverySubgroupBase.PointOfDeliveryGroupID = PointOfDeliveryGroupBase.PointOfDeliveryGroupID







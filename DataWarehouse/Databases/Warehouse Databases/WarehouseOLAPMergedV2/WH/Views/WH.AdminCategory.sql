﻿create view [WH].[AdminCategory] as

select
	 Member.SourceValueID
	,AdminCategoryID = Member.SourceValueCode
	,AdminCategory = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member
where
	Member.AttributeCode = 'ADMCAT'



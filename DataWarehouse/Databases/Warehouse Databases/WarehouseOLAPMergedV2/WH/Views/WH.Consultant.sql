﻿














CREATE view [WH].[Consultant] as

SELECT
	SourceContextID = Member.SourceContextID
	,SourceContextCode = Member.SourceContextCode
	,SourceContext = Member.SourceContext
	,SourceConsultantID = Member.SourceValueID
	,SourceConsultantCode = Member.SourceValueCode
	,SourceConsultant= Member.SourceValue
	,LocalConsultantID = Member.LocalValueID
	,LocalConsultantCode = Member.LocalValueCode
	,LocalConsultant = Member.LocalValue
	,NationalConsultantID = Member.NationalValueID
	,NationalConsultantCode = Member.NationalValueCode
	,NationalConsultant = Member.NationalValue
	,Base.ProviderCode
	,Base.MainSpecialtyCode
	,Base.MainSpecialty
	,Base.Title
	,Base.Initials
	,Base.Surname
	,Consultant = 
		case left(SourceContextCode,3)
		when 'TRA' then 
			case 
			when NationalValueCode = 'N||CONSUL' Then SourceValueCode
			else NationalValueCode
			end
		else SourceValueCode
		end + ' - ' + 
		coalesce (Base.Consultant
				,Member.SourceValue
		)

	,SpecialtySectorID =
		coalesce(
			 SpecialtySpecialtySectorMap.SpecialtySectorID
			,99
		)
	
FROM
	WH.Member Member

LEFT OUTER JOIN	 WH.ConsultantBase Base
ON	Base.ConsultantID = Member.SourceValueID

left join BedOccupancy.SpecialtySpecialtySectorMap
on	SpecialtySpecialtySectorMap.SpecialtyCode = Base.MainSpecialtyCode

where
	Member.AttributeCode = 'CONSUL'





















﻿






CREATE view [WH].[ServicePoint] as


select
	 Member.SourceContextCode
	,Member.SourceContext
	,SourceServicePointID = SourceValueID
	,SourceServicePointCode = SourceValueCode
	,SourceServicePoint = SourceValue
	,LocalServicePointID = LocalValueID
	,LocalServicePointCode = LocalValueCode
	,LocalServicePoint = LocalValue
	,NationalServicePointID = NationalValueID
	,NationalServicePointCode = NationalValueCode
	,NationalServicePoint = NationalValue
	,ServicePointType = Member.Attribute
	--,ServicePointType2 = LocationTypeBase.LocationType

from
	WH.Member

left join WH.LocationLocationType
on	Member.LocalValueID = LocationLocationType.LocationID

left join WH.LocationTypeBase
on	LocationTypeBase.LocationTypeID = LocationLocationType.LocationTypeID

where
	(
		AttributeCode in
		(
		 'CLINIC'
		,'COMLOC'
		,'THEATRE'
		,'WARD'
		)
	and Member.SourceContextCode != 'CEN||PTRACK'
	)	
or (
		AttributeCode = 'LOCATION' -- DG 11.11.2015. Observation no longer has wards, just locations. Holding off flowing locations into service point as they are not the same thing potentially.
	and Member.SourceContextCode = 'CEN||PTRACK'
	)

union all

select
	 SourceContextCode = null
	,SourceContext = null
	,SourceServicePointID = -1
	,SourceServicePointCode  = null
	,SourceServicePoint = 'N/A'
	,LocalServicePointID = null
	,LocalServicePointCode = null
	,LocalServicePoint = null
	,NationalServicePointID = null
	,NationalServicePointCode = null
	,NationalServicePoint = null
	,ServicePointType = 'N/A'
	--,ServicePointType2 = 'N/A'











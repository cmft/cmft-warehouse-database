﻿
CREATE view [WH].[Religion] as

select
	SourceContextCode
	,SourceContext
	,SourceReligionID
	,SourceReligionCode
	,SourceReligion
	,LocalReligionID
	,LocalReligionCode
	,LocalReligion
	,NationalReligionID
	,NationalReligionCode
	,NationalReligion
	,NationalReligionGroupCode
	,NationalReligionGroup = coalesce(ReligionGroup.ReligionGroup,'Unassigned')
from
	(
	SELECT
		 SourceContextCode
		,SourceContext
		,SourceReligionID = SourceValueID
		,SourceReligionCode = SourceValueCode
		,SourceReligion = SourceValue
		,LocalReligionID = LocalValueID
		,LocalReligionCode = LocalValueCode
		,LocalReligion = LocalValue
		,NationalReligionID = NationalValueID
		,NationalReligionCode = NationalValueCode
		,NationalReligion = NationalValue
		,NationalReligionGroupCode = case 
										when NationalValueCode = 'N||RELIGION' 
										then 'N||RELIGION' 
										else left(NationalValueCode,1) 
										end
	FROM
		WH.Member
	where
		Member.AttributeCode = 'RELIGION'
			
	) Relgion

left join [$(Warehouse)].WH.ReligionGroup
on 	Relgion.NationalReligionGroupCode = ReligionGroup.ReligionGroupCode


-- 20150623 Discussed with DG, removed the below as we shouldn't be joining to reference map, used the above table from warehouse instead
-- this can go through reference map as other

--left join ReferenceMapv2.Map.Value
--on	Relgion.NationalReligionGroupCode = Value.ValueCode
--and Value.AttributeContextID = (
--								select
--									AttributeContextID
--								from 
--									ReferenceMapv2.Map.ContextAttribute
--								where
--									ContextCode = 'N'
--								and AttributeCode = 'RELIGION'
--								)









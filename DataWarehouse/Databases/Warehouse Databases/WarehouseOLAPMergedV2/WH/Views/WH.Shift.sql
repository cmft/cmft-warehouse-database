﻿
CREATE view WH.Shift as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceShiftID = SourceValueID
	,SourceShiftCode = SourceValueCode
	,SourceShift = SourceValue
	,LocalShiftID = LocalValueID
	,LocalShiftCode = LocalValueCode
	,LocalShift = LocalValue
	,NationalShiftID = NationalValueID
	,NationalShiftCode = NationalValueCode
	,NationalShift = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'SHIFT'









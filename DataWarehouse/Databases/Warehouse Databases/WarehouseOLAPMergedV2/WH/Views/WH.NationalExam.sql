﻿CREATE view WH.NationalExam as

select
	 NationalExamID = Allocation.AllocationID
	,NationalExam.NationalExamCode
	,NationalExam.NationalExam
	,ExamGroup.ExamGroup
	,ExamArea.ExamArea
from
	Allocation.Allocation

inner join Diagnostic.NationalExam
on	NationalExam.NationalExamCode = Allocation.SourceAllocationID

inner join Diagnostic.ExamGroup
on	ExamGroup.ExamGroupCode = NationalExam.ExamGroupCode

inner join Diagnostic.ExamArea
on	ExamArea.ExamAreaCode = ExamGroup.ExamAreaCode

where
	Allocation.AllocationTypeID = 1


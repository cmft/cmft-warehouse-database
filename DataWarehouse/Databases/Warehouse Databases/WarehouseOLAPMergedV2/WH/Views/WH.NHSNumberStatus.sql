﻿



CREATE view [WH].[NHSNumberStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceNHSNumberStatusID = SourceValueID
	,SourceNHSNumberStatusCode = SourceValueCode
	,SourceNHSNumberStatus = SourceValue
	,LocalNHSNumberStatusID = LocalValueID
	,LocalNHSNumberStatusCode = LocalValueCode
	,LocalNHSNumberStatus = LocalValue
	,NationalNHSNumberStatusID = NationalValueID
	,NationalNHSNumberStatusCode = NationalValueCode
	,NationalNHSNumberStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'NHSNST'






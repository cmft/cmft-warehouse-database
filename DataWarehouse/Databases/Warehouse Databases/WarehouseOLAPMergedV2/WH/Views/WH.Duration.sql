﻿





CREATE view [WH].[Duration] as

select
	 DurationCode
	,Duration
	,APCWLDurationBandCode
	,APCWLDurationBand
	,APCWLBreach18BandCode
	,APCWLBreach18Band
	,APCWLBreach26BandCode
	,APCWLBreach26Band
	,OPWLDurationBandCode
	,OPWLDurationBand
	,OPWLBreachBandCode
	,OPWLBreachBand
	,DurationID
from
	WH.DurationBase





﻿




CREATE view [WH].[Site] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceSiteID = SourceValueID
	,SourceSiteCode = SourceValueCode
	,SourceSite = SourceValue
	,LocalSiteID = LocalValueID
	,LocalSiteCode = LocalValueCode
	,LocalSite = LocalValue
	,NationalSiteID = NationalValueID
	,NationalSiteCode = NationalValueCode
	,NationalSite = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'SITE'







﻿



--USE WarehouseOLAPMergedV2

CREATE View [WH].[GPPractice] as

		select
			 GpPracticeID
			,GpPracticeCode 
			,GpPracticeName = dbo.f_ProperCase(GpPracticeName)
			,Postcode
			,PracticeOpenDate
			,PracticeCloseDate
			,ParentOrganisationCode 
			,JoinParentDate 
			,LeftParentDate 
			,ElectronicCorrespondenceActivatedTime 
			,ElectronicCorrespondenceDeactivatedTime

		from
			WH.GPPracticeBase
union
		Select
			 GpPracticeID = -1
			,GpPracticeCode = 'N/A'
			,GpPracticeName  = 'N/A'
			,Postcode = null
			,PracticeOpenDate = null
			,PracticeCloseDate = null
			,ParentOrganisationCode = null
			,JoinParentDate = null
			,LeftParentDate = null
			,ElectronicCorrespondenceActivatedTime = null
			,ElectronicCorrespondenceDeactivatedTime = null






﻿









CREATE view [WH].[Department] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceDepartmentID = SourceValueID
	,SourceDepartmentCode = SourceValueCode
	,SourceDepartment = SourceValue
	,OtherValueID
	,OtherValueCode
	,OtherValue
FROM
	WH.Member
where
	AttributeCode = 'DEPT'












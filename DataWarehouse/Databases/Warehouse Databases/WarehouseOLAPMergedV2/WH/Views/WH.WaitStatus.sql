﻿



CREATE view [WH].[WaitStatus] as

select
	 WaitStatusBase.WaitStatusID
	,WaitStatusBase.WaitStatus
	,WaitStatusParentID =
		ParentWaitStatusBase.WaitStatusID

	,WaitStatusBase.WaitStatusCode
from
	WH.WaitStatusBase

left join WH.WaitStatusBase ParentWaitStatusBase
on	ParentWaitStatusBase.WaitStatusCode = WaitStatusBase.ParentWaitStatusCode




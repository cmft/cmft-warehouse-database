﻿create view [WH].[AdmissionSource] as

select
	 Member.SourceValueID
	,AdmissionSourceID = Member.SourceValueCode
	,AdmissionSource = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member
where
	Member.AttributeCode = 'ADMSRC'



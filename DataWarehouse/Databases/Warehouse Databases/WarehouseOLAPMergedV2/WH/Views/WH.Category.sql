﻿
create view [WH].[Category] as

select
	 Category.CategoryCode
	,Category.Category
	,Category.ParentCategoryCode
from
	WH.CategoryBase Category


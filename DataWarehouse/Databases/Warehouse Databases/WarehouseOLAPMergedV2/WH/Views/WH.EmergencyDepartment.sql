﻿







create view [WH].[EmergencyDepartment] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceEmergencyDepartmentID = SourceValueID
	,SourceEmergencyDepartmentCode = SourceValueCode
	,SourceEmergencyDepartment = SourceValue
	,LocalEmergencyDepartmentID = LocalValueID
	,LocalEmergencyDepartmentCode = LocalValueCode
	,LocalEmergencyDepartment = LocalValue
	,NationalEmergencyDepartmentID = NationalValueID
	,NationalEmergencyDepartmentCode = NationalValueCode
	,NationalEmergencyDepartment = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'EMERGENCYDEPT'
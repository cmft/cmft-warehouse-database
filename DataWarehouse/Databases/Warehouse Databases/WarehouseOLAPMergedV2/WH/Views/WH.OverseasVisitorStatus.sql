﻿



CREATE view [WH].[OverseasVisitorStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceOverseasVisitorStatusID = SourceValueID
	,SourceOverseasVisitorStatusCode = SourceValueCode
	,SourceOverseasVisitorStatus = SourceValue
	,LocalOverseasVisitorStatusID = LocalValueID
	,LocalOverseasVisitorStatusCode = LocalValueCode
	,LocalOverseasVisitorStatus = LocalValue
	,NationalOverseasVisitorStatusID = NationalValueID
	,NationalOverseasVisitorStatusCode = NationalValueCode
	,NationalOverseasVisitorStatus = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'OVEVIS'






﻿


CREATE view [WH].[DocumentType] as

SELECT
	 SourceDocumentTypeID = Member.SourceValueID
	,SourceDocumentTypeCode = Member.SourceValueCode
	,SourceDocumentType = Member.SourceValue
	,SourceContextID = Member.SourceContextID
	,SourceContext = Member.SourceContext
	,SourceContextCode = Member.SourceContextCode
FROM
	WH.Member Member
	
where
	Member.AttributeCode = 'DOCTYPE'























﻿create view WH.Month as

select distinct
	 FinancialMonthKey
	,TheMonth
from
	WH.Calendar

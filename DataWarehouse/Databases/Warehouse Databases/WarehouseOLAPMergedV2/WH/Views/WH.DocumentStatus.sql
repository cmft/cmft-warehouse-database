﻿


CREATE view [WH].[DocumentStatus] as

SELECT
	 SourceDocumentStatusID = Member.SourceValueID
	,SourceDocumentStatusCode = Member.SourceValueCode
	,SourceDocumentStatus = Member.SourceValue
	,SourceContextID = Member.SourceContextID
	,SourceContext = Member.SourceContext
	,SourceContextCode = Member.SourceContextCode
FROM
	WH.Member Member

where
	Member.AttributeCode = 'DOCSTATUS'






















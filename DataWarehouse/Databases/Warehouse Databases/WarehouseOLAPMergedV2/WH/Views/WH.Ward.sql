﻿






CREATE view [WH].[Ward] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceWardID = SourceValueID
	,SourceWardCode = SourceValueCode
	,SourceWard = SourceValue
	,LocalWardID = LocalValueID
	,LocalWardCode = LocalValueCode
	,LocalWard = LocalValue
	,NationalWardID = NationalValueID
	,NationalWardCode = NationalValueCode
	,NationalWard = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'WARD'









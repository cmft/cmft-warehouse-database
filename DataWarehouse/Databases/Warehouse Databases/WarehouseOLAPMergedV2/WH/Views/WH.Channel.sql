﻿







CREATE view [WH].[Channel] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceChannelID = SourceValueID
	,SourceChannelCode = SourceValueCode
	,SourceChannel = SourceValue
	,LocalChannelID = LocalValueID
	,LocalChannelCode = LocalValueCode
	,LocalChannel = LocalValue
	,NationalChannelID = NationalValueID
	,NationalChannelCode = NationalValueCode
	,NationalChannel = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'CHANNEL'











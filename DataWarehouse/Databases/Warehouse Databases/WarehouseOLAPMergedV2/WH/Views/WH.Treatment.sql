﻿




create view WH.[Treatment] as

select
	 Member.SourceValueID
	,TreatmentCode = Member.SourceValueCode
	,Treatment = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join [$(Warehouse)].SCR.Treatment
on	Treatment.TreatmentCode = Member.SourceValueCode

where
	Member.AttributeCode = 'TREATMENT'









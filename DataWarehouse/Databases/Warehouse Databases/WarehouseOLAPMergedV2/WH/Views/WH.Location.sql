﻿

CREATE view [WH].[Location] as

select
	 SourceContextCode
	,SourceContext
	,SourceLocationID = SourceValueID
	,SourceLocationCode = SourceValueCode
	,SourceLocation = SourceValue
	,LocalLocationID = LocalValueID
	,LocalLocationCode = LocalValueCode
	,LocalLocation = LocalValue
	,NationalLocationID = NationalValueID
	,NationalLocationCode = NationalValueCode
	,NationalLocation = NationalValue
	--,LocationTypeID = OtherValueID
	--,LocationTypeCode = OtherValueCode
	--,LocationType = OtherValue
	,LocationTypeID = LocationTypeBase.LocationTypeID
	,LocationTypeCode = LocationTypeBase.LocationTypeCode
	,LocationType = LocationTypeBase.LocationType
from
	WH.Member

left join WH.LocationLocationType
on LocationLocationType.LocationID = Member.LocalValueID

left join WH.LocationTypeBase --Where LocalLocationID not in LocationLocationType
on coalesce(LocationLocationType.LocationTypeID,6) = LocationTypeBase.LocationTypeID

where
	AttributeCode = 'LOCATION' 



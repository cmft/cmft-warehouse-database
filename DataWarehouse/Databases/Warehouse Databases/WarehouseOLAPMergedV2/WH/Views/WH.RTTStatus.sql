﻿



CREATE view [WH].[RTTStatus] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceRTTStatusID = SourceValueID
	,SourceRTTStatusCode = SourceValueCode
	,SourceRTTStatus = SourceValue
	,LocalRTTStatusID = LocalValueID
	,LocalRTTStatusCode = LocalValueCode
	,LocalRTTStatus = LocalValue
	,NationalRTTStatusID = NationalValueID
	,NationalRTTStatusCode = NationalValueCode
	,NationalRTTStatus = NationalValue
FROM
	WH.Member
where 
	AttributeCode = 'RTTSTA'




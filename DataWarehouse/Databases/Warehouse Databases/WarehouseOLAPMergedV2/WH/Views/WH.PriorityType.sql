﻿



CREATE view [WH].[PriorityType] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourcePriorityTypeID = SourceValueID
	,SourcePriorityTypeCode = SourceValueCode
	,SourcePriorityType = SourceValue
	,LocalPriorityTypeID = LocalValueID
	,LocalPriorityTypeCode = LocalValueCode
	,LocalPriorityType = LocalValue
	,NationalPriorityTypeID = NationalValueID
	,NationalPriorityTypeCode = NationalValueCode
	,NationalPriorityType = NationalValue
FROM
	WH.Member
where 
	AttributeCode = 'PRITYP'




﻿create view [WH].[DischargeMethod] as

select
	 Member.SourceValueID
	,DischargeMethodID = Member.SourceValueCode
	,DischargeMethod = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member
where
	Member.AttributeCode = 'DISMTH'



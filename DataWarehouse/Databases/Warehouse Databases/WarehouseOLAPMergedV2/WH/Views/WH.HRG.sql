﻿

CREATE view [WH].[HRG]

as

select
	HRGID
	,HRGCode
	,HRG = HRGCode + ' - ' + HRG
	,HRGChapter
	,HRGSubChapter
	,HighCost
from
	WH.HRGBase


﻿



CREATE view [WH].[Directorate] as

select
	 DirectorateID
	,DirectorateCode
	,Directorate
	,DivisionCode
	,Division
	,DivisionLabel
	,DivisionGroup = -- Added for Observation reporting
				case coalesce(
					DivisionCode
					,'N/A'
					)
				when 'CHILD' then 'Childrens'
				when 'CORPORATE' then 'N/A'
				when '9' then 'N/A'
				else 'Adults'
				end
					
from
	WH.DirectorateBase








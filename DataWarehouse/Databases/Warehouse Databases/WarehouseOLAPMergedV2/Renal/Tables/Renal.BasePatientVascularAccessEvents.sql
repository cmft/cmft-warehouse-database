﻿CREATE TABLE [Renal].[BasePatientVascularAccessEvents] (
    [SourceUniqueID]                        VARCHAR (100)  NULL,
    [PatientObjectID]                       INT            NULL,
    [PatientFullName]                       NVARCHAR (40)  NULL,
    [PatientMedicalRecordNo]                NVARCHAR (12)  NULL,
    [StartDate]                             DATETIME       NULL,
    [StopDate]                              DATETIME       NULL,
    [DaysElapsed]                           INT            NULL,
    [AgeAtEvent]                            NUMERIC (18)   NULL,
    [TimelineEvent]                         NVARCHAR (256) NULL,
    [TimelineEventDetail]                   NVARCHAR (256) NULL,
    [InformationSource]                     NVARCHAR (256) NULL,
    [Notes]                                 NVARCHAR (MAX) NULL,
    [Etiology]                              NVARCHAR (256) NULL,
    [AccessType]                            NVARCHAR (256) NULL,
    [AccessLocation]                        NVARCHAR (256) NULL,
    [PermanentStatus]                       NVARCHAR (256) NULL,
    [Brand]                                 NVARCHAR (256) NULL,
    [CatheterType]                          NVARCHAR (256) NULL,
    [Configuration]                         NVARCHAR (256) NULL,
    [Elasticity]                            NVARCHAR (256) NULL,
    [ExternalStatus]                        NVARCHAR (256) NULL,
    [NeedleGuage]                           NVARCHAR (256) NULL,
    [NeedleLength]                          NVARCHAR (256) NULL,
    [NeedleType]                            NVARCHAR (256) NULL,
    [NeedleGuage2]                          NVARCHAR (256) NULL,
    [NeedleLength2]                         NVARCHAR (256) NULL,
    [NeedleType2]                           NVARCHAR (256) NULL,
    [VenousLumen]                           NVARCHAR (50)  NULL,
    [ArterialLumen]                         NVARCHAR (50)  NULL,
    [Preparation]                           NVARCHAR (255) NULL,
    [PrimingProcedures]                     NVARCHAR (255) NULL,
    [Surgeon]                               NVARCHAR (40)  NULL,
    [VascularAccessObjectID]                INT            NULL,
    [Created]                               DATETIME       NULL,
    [Updated]                               DATETIME       NULL,
    [ByWhom]                                VARCHAR (100)  NULL,
    [PatientVascularAccessEventsRecno]      INT            NULL,
    [PatientVascularAccessEventsMergeRecno] INT            IDENTITY (1, 1) NOT NULL,
    [ContextCode]                           VARCHAR (100)  NULL,
    CONSTRAINT [PK__BasePati__C2381B235A5B1C8A] PRIMARY KEY CLUSTERED ([PatientVascularAccessEventsMergeRecno] ASC)
);


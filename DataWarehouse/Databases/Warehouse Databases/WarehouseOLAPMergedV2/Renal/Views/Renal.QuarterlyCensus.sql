﻿






CREATE view [Renal].[QuarterlyCensus] as

/*
Creates a record for each PatientRenalModality record active over a quarter end
Record contains the latest values in the quarter for a range of test values
*/

with Hb as -- Take the latest Hb measurement carried out within each quarter
(
	select
		PatientObjectID
		,LabTestDate
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,LabTestDate),-1)
		,HgbNum
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,LabTestDate),-1) order by LabTestDate desc)

	from Renal.BasePatientLabPanelIronStudies
	where HgbNum is not null
)

,DialysisAccess as
(
	select
		PatientObjectID
		,StartDate
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,StartDate),-1)
		,TimelineEventDetailCode
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,StartDate),-1) order by StartDate desc)

	from Renal.BasePatientVascularAccess
	where TimelineEventDetailCode is not null
)

,Phosphate as 
(
	select
		PatientObjectID
		,LabTestDate
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,LabTestDate),-1)
		,PhosNum
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,LabTestDate),-1) order by LabTestDate desc)

	from Renal.BasePatientLabPanelBoneAndMinerals
	where PhosNum is not null
)

,Calcium as 
(
	select
		PatientObjectID
		,LabTestDate
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,LabTestDate),-1)
		,CAadjNum
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,LabTestDate),-1) order by LabTestDate desc)

	from Renal.BasePatientLabPanelBoneAndMinerals
	where CAadjNum is not null
)

,Pth as 
(
	select
		PatientObjectID
		,LabTestDate
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,LabTestDate),-1)
		,PTHIntactNum
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,LabTestDate),-1) order by LabTestDate desc)

	from Renal.BasePatientLabPanelBoneAndMinerals
	where PTHIntactNum is not null
)

,Ferritin as 
(
	select
		PatientObjectID
		,LabTestDate
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,LabTestDate),-1)
		,FerritinNum
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,LabTestDate),-1) order by LabTestDate desc)

	from Renal.BasePatientLabPanelIronStudies
	where FerritinNum is not null
)

,Bicarbonate as 
(
	select
		PatientObjectID
		,LabTestDate
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,LabTestDate),-1)
		,HCO3Num
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,LabTestDate),-1) order by LabTestDate desc)

	from Renal.BasePatientLabPanelElectrolytes
	where HCO3Num is not null
)

,BloodPressure as
(
	select
		PatientObjectID
		,ServiceStartTime
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,ServiceStartTime),-1)
		,PostVitalsSittingBloodPressureSystolic
		,PostVitalsSittingBloodPressureDiastolic
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,ServiceStartTime),-1) order by ServiceStartTime desc)

from Renal.BasePatientHemodialysisServices
where PostVitalsSittingBloodPressureSystolic is not null
and PostVitalsSittingBloodPressureDiastolic is not null
)

,ActualRunTime as
(
	select
		PatientObjectID
		,ServiceStartTime
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,ServiceStartTime),-1)
		,ActualRunTime
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,ServiceStartTime),-1) order by ServiceStartTime desc)

from Renal.BasePatientHemodialysisServices
where ActualRunTime is not null
)

,Weight as
(
	select
		PatientObjectID
		,ServiceStartTime
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,ServiceStartTime),-1)
		,PreVitalsWeightInKg
		,PostVitalsWeightInKg
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,ServiceStartTime),-1) order by ServiceStartTime desc)

	from Renal.BasePatientHemodialysisServices
	where PreVitalsWeightInKg is not null
	and PostVitalsWeightInKg is not null
)

,UF as
(
	select
		PatientObjectID
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,ServiceStartTime),-1)
		,UltraFiltrationVolume = avg(UltraFiltrationVolume)

	from Renal.BasePatientHemodialysisServices
	where UltraFiltrationVolume is not null
	group by 
		PatientObjectID
		,dateadd(quarter,datediff(quarter,-1,ServiceStartTime),-1)
)

,URR as
(
	select
		PatientObjectID
		,LabTestDate
		,QtrEnd = dateadd(quarter,datediff(quarter,-1,LabTestDate),-1)
		,URRNum
		,RowNum = row_number() over (partition by PatientObjectID, dateadd(quarter,datediff(quarter,-1,LabTestDate),-1) order by LabTestDate desc)

	from Renal.BasePatientLabPanelKidneyFunction
	where URRNum is not null
)

,EPO as -- Take latest EPO record regardless of the quarter in which the regimen started
(
	select
		E.PatientObjectID
		,QtrEnd = Calendar.LastDayOfQuarter
		,E.StartDate
		,E.StopDate
		,RowNum = row_number() over (partition by E.PatientObjectID, Calendar.LastDayOfQuarter order by E.StartDate desc)

	from Renal.BasePatientMedicationOrders E
	join 
	(
		select distinct 
		FirstDayOfQuarter
		,LastDayOfQuarter
		from WH.Calendar
	) Calendar  on E.StartDate <= Calendar.LastDayOfQuarter and coalesce(E.StopDate, getdate()) >= Calendar.FirstDayOfQuarter

	where DrugType = 'EPO'
)

,TransplantListStatus as -- Take latest Transplant Status regardless of the quarter in which the status was updated
(
	select
		E.PatientObjectID
		,QtrEnd = Calendar.LastDayOfQuarter
		,E.StartDate
		,E.StopDate
		,E.TimelineEventDetailCode
		,RowNum = row_number() over (partition by E.PatientObjectID order by E.StartDate desc)

	from Renal.BaseRecipientTransplantStatusEvents E
	join 
	(
		select distinct 
		FirstDayOfQuarter
		,LastDayOfQuarter
		from WH.Calendar
	) Calendar  on E.StartDate <= Calendar.LastDayOfQuarter and coalesce(E.StopDate, getdate()) >= Calendar.FirstDayOfQuarter
)

select
	M.PatientRenalModalityRecno -- Logical key
	,Calendar.LastDayOfQuarter -- Logical key
	,M.ContextCode
	,P.DateOfBirth
	,P.DateOfDeath
	,M.ModalityCode
	,M.ModalitySettingCode
	,M.IsPrimary
	,M.DialysisProviderCode
	,M.EventDetailCode
	,M.StartDate
	,M.StopDate
	,M.ReasonForChangeCode
	,VascularAccessCode = DialysisAccess.TimelineEventDetailCode
	,HCO3Num = Bicarbonate.HCO3Num
	,PostVitalsSittingBloodPressureSystolic = BloodPressure.PostVitalsSittingBloodPressureSystolic
	,PostVitalsSittingBloodPressureDiastolic = BloodPressure.PostVitalsSittingBloodPressureDiastolic
	,ActualRunTime = ActualRunTime.ActualRunTime
	,PreVitalsWeightInKg = Weight.PreVitalsWeightInKg
	,PostVitalsWeightInKg = Weight.PostVitalsWeightInKg
	,URRNum = URR.URRNum
	,UltrafiltrationVolume = UF.UltraFiltrationVolume
	,HgBNum = Hb.HgbNum
	,FerritinNum = Ferritin.FerritinNum
	,TimingQualifier = 0 --.TimingQualifier
	,PhosNum = Phosphate.PhosNum
	,CaAdjNum = Calcium.CAadjNum
	,PTHIntactNum = Pth.PTHIntactNum
	,TransplantListStatusCode = TransplantListStatus.TimelineEventDetailCode
	,OnEPO = -- Patient started EPO during this episode, and was on EPO at quarter end
		case 
			when EPO.StartDate between M.StartDate and (coalesce(M.StopDate, getdate()))
			and (EPO.StopDate is null or EPO.StopDate > Calendar.LastDayOfQuarter) then 1
			else 0
		end
	,PreviousHDModalityExists = -- Flag if a previous PatientRenalModality rec exists for same patient, with Modality = HD
	(
		select 
			top 1 1
		
		from Renal.BasePatientRenalModality M1
		
		where M1.ModalityCode = '90:55425' -- HD
		and M1.PatientObjectID =  M.PatientObjectID 
		and (M1.StartDate < M.StartDate or (M1.StartDate = M.StartDate and M1.StopDate < M.StopDate))
		
		order by StartDate, StopDate
	)

from Renal.BaseRenalPatient P

join Renal.BasePatientRenalModality M on M.PatientObjectID = P.SourceUniqueID

join 
(
	select distinct 
	FirstDayOfQuarter
	,LastDayOfQuarter
	from WH.Calendar
) Calendar  on cast(M.StartDate as date) <= Calendar.LastDayOfQuarter and coalesce(M.StopDate, getdate()) >= Calendar.LastDayOfQuarter

left join Hb on Hb.PatientObjectID = M.PatientObjectID
and Hb.QtrEnd = Calendar.LastDayOfQuarter
and Hb.RowNum = 1

left join DialysisAccess on DialysisAccess.PatientObjectID = M.PatientObjectID
and DialysisAccess.QtrEnd = Calendar.LastDayOfQuarter
and DialysisAccess.RowNum = 1

left join Phosphate on Phosphate.PatientObjectID = M.PatientObjectID
and Phosphate.QtrEnd = Calendar.LastDayOfQuarter
and Phosphate.RowNum = 1

left join Calcium on Calcium.PatientObjectID = M.PatientObjectID
and Calcium.QtrEnd = Calendar.LastDayOfQuarter
and Calcium.RowNum = 1

left join Pth on Pth.PatientObjectID = M.PatientObjectID
and Pth.QtrEnd = Calendar.LastDayOfQuarter
and Pth.RowNum = 1

left join Ferritin on Ferritin.PatientObjectID = M.PatientObjectID
and Ferritin.QtrEnd = Calendar.LastDayOfQuarter
and Ferritin.RowNum = 1

left join Bicarbonate on Bicarbonate.PatientObjectID = M.PatientObjectID
and Bicarbonate.QtrEnd = Calendar.LastDayOfQuarter
and Bicarbonate.RowNum = 1

left join BloodPressure on BloodPressure.PatientObjectID = M.PatientObjectID
and BloodPressure.QtrEnd = Calendar.LastDayOfQuarter
and BloodPressure.RowNum = 1

left join ActualRunTime on ActualRunTime.PatientObjectID = M.PatientObjectID
and ActualRunTime.QtrEnd = Calendar.LastDayOfQuarter
and ActualRunTime.RowNum = 1

left join Weight on Weight.PatientObjectID = M.PatientObjectID
and Weight.QtrEnd = Calendar.LastDayOfQuarter
and Weight.RowNum = 1

left join URR on URR.PatientObjectID = M.PatientObjectID
and URR.QtrEnd = Calendar.LastDayOfQuarter
and URR.RowNum = 1

left join UF on UF.PatientObjectID = M.PatientObjectID
and UF.QtrEnd = Calendar.LastDayOfQuarter

left join EPO on EPO.PatientObjectID = M.PatientObjectID
and EPO.QtrEnd = Calendar.LastDayOfQuarter
and EPO.RowNum = 1

left join TransplantListStatus on TransplantListStatus.PatientObjectID = M.PatientObjectID
and TransplantListStatus.QtrEnd = Calendar.LastDayOfQuarter
and TransplantListStatus.RowNum = 1






﻿CREATE TABLE [Finance].[BaseExpenditureReference] (
    [MergeExpenditureRecno] INT          NOT NULL,
    [ContextID]             INT          NULL,
    [CensusDateID]          INT          NULL,
    [Created]               DATETIME     NULL,
    [Updated]               DATETIME     NULL,
    [ByWhom]                VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseExpe__624520D44B2EB2EA] PRIMARY KEY CLUSTERED ([MergeExpenditureRecno] ASC)
);


﻿CREATE TABLE [Finance].[BaseExpenditure] (
    [MergeExpenditureRecno] INT           IDENTITY (1, 1) NOT NULL,
    [ExpenditureRecno]      INT           NOT NULL,
    [CensusDate]            DATE          NOT NULL,
    [Division]              VARCHAR (100) NOT NULL,
    [Budget]                FLOAT (53)    NULL,
    [AnnualBudget]          FLOAT (53)    NULL,
    [Actual]                FLOAT (53)    NULL,
    [InterfaceCode]         VARCHAR (50)  NULL,
    [ContextCode]           VARCHAR (12)  NOT NULL,
    [Created]               DATETIME      NULL,
    [Updated]               DATETIME      NULL,
    [ByWhom]                VARCHAR (50)  NULL
);


﻿
Create View PCC.Activity
as

Select 
	ActivityRecno	
	,PeriodID	
	,ActivityDate	
	,ActivityCode	
	,ActivitySequence	
	,Created	
	,Updated	
	,ByWhom
from 
	[$(Warehouse)].PCC.Activity


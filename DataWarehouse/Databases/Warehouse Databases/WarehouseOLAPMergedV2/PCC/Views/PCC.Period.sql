﻿
Create View PCC.Period as

Select 
	PeriodRecno	
	,PeriodID	
	,NHSNumber	
	,CasenoteNo	
	,DateOfBirth	
	,AdmissionTime	
	,DischargeTime	
	,UnitFunctionCode	
	,IsolationReasonCode	
	,PrimaryDiagnosisReadCode	
	,SecondaryDiagnosisReadCode1	
	,SecondaryDiagnosisReadCode2	
	,SecondaryDiagnosisReadCode3	
	,Created	
	,Updated	
	,ByWhom
from 
	[$(Warehouse)].PCC.Period
	

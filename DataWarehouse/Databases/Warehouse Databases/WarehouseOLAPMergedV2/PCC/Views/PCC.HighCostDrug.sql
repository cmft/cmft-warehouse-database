﻿

Create View PCC.HighCostDrug
as

Select 
	HighCostDrugRecno	
	,PeriodID	
	,HighCostDrugDate	
	,HighCostDrugCode	
	,HighCostDrugSequence
	,Created	
	,Updated	
	,ByWhom
from 
	[$(Warehouse)].PCC.HighCostDrug


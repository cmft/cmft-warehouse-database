﻿CREATE procedure [APCWL].[BuildFactEncounter] as


declare
       @StartTime datetime = getdate()
       ,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
       ,@Elapsed int
       ,@Stats varchar(max)


declare @MissingSubSpecialtyID int =
       (
       select
              SubSpecialty.SubSpecialtyID
       from
              WH.SubSpecialty
       where
              SubSpecialty.SubSpecialty = 'Unassigned'
       )

truncate table APCWL.FactEncounter

insert into APCWL.FactEncounter
(
	 MergeEncounterRecno
	,WaitTargetID
	,BreachDateID
	,AgeID
	,SexID
	,CensusDateID
	,ConsultantID
	,SpecialtyID
	,SiteID
	,WaitTypeID
	,TCIDateID
	,DirectorateID
	,ContextID
	,WaitingListID
	,IntendedManagementID
	,DurationID
	,WaitStatusID
	,TCIDateDurationID
	,ServiceID
	,SubSpecialtyID
	,AddedLastWeek
	,IsDiagnosticProcedure
	,WithExpectedAdmissionDate
	,WithTCIDate
	,WithGuaranteedAdmissionDate
	,WithRTTOpenPathway
	,TCIBeyondBreach
	,Cases
	,LengthOfWait
	,WaitingListAddMinutes
	,OPCSCoded
	,WithRTTStartDate
	,IsBrokerDuplicate
	,PriorityID
	,IsShortNotice
	,EthnicCategoryID
	,ReligionID
)
select
   Encounter.MergeEncounterRecno

   ,WaitTargetID =
          WaitTarget.WaitTargetID

   ,BreachDateID =
          case
          when Encounter.WaitTypeCode in ('20' , '40') -- no breach date for planned or private patients
          then NullDate.DateID
          else
                 coalesce(
                       WaitTarget.BreachDateID
                       ,NullDate.DateID
                 )
          end

   ,Reference.AgeID
   ,Reference.SexID
   ,Reference.CensusDateID
   ,Reference.ConsultantID
   ,Reference.SpecialtyID
   ,Reference.SiteID
   ,Reference.WaitTypeID
   ,Reference.TCIDateID
   ,Directorate.DirectorateID
   ,Reference.ContextID
   ,Reference.WaitingListID
   ,Reference.IntendedManagementID

   ,Reference.DurationID
   ,Reference.WaitStatusID
   ,Reference.TCIDateDurationID

   ,ServiceID = Service.AllocationID

   ,SubSpecialtyID = 
          @MissingSubSpecialtyID

   ,AddedLastWeek = cast(Encounter.AddedLastWeek as bit)
   ,IsDiagnosticProcedure = cast(Encounter.DiagnosticProcedure as bit)
   ,WithExpectedAdmissionDate = cast(Encounter.WithExpectedAdmissionDate as bit)
   ,WithTCIDate = cast(Encounter.WithTCIDate as bit)
   ,WithGuaranteedAdmissionDate = cast(Encounter.WithGuaranteedAdmissionDate as bit)
   ,WithRTTOpenPathway = cast(Encounter.WithRTTOpenPathway as bit)

   ,TCIBeyondBreach =
          cast(
          case
          when
                 Encounter.TCIDate >= BreachDate.TheDate
          and    WaitTarget.BreachDateID != NullDate.DateID
          and    Encounter.TCIDate is not null
          then 1
          else 0
          end
          as bit)

   ,Encounter.Cases
   ,Encounter.LengthOfWait
   ,Encounter.WaitingListAddMinutes
   ,Encounter.OPCSCoded
   ,Encounter.WithRTTStartDate

	,IsBrokerDuplicate =
		CAST(
			case
			when
			--not on transfer list
				WaitingList.IsTransferWaitingList = 0

			--on transfer list at primary scheduler
			or	(
					WaitingList.IsTransferWaitingList = 1
				and	WaitingList.IsPrimaryScheduler = 1
				)

			or	(
					WaitingList.IsTransferWaitingList = 1
				and	WaitingList.IsPrimaryScheduler = 0

			--remove waiters who are on same Transfer Waiting Lists at > 1 site retaining the one at the primary scheduler site
				and	not exists
					(
					select
						1
					from
						APCWL.BaseEncounter TransferListWaiter

					inner join APCWL.WaitingList TransferList
					on	TransferList.SourceWaitingListCode = TransferListWaiter.WaitingListCode
					and	TransferList.SourceContextCode = TransferListWaiter.ContextCode

					left join [$(Chameleon)].Patient.PatientApplicationIdentity CentralPatientTransfer
					on	CentralPatientTransfer.PatientApplicationIdentityValue = TransferListWaiter.DistrictNo
					and	CentralPatientTransfer.SourceApplicationIdentifierId = 1 --DistrictNo
					and	TransferListWaiter.ContextCode = 'CEN||PAS'

					left join [$(Chameleon)].Patient.PatientApplicationIdentity TraffordPatientTransfer
					on	TraffordPatientTransfer.PatientApplicationIdentityValue = TransferListWaiter.SourcePatientNo
					and	TraffordPatientTransfer.SourceApplicationIdentifierId = 2 --EPMINumber
					and	TransferListWaiter.ContextCode = 'TRA||UG'

					where
						TransferList.IsTransferWaitingList = 1
					and	TransferList.IsPrimaryScheduler = 1

					and	TransferListWaiter.CensusDate = Encounter.CensusDate
					and	TransferListWaiter.WaitingListCode = Encounter.WaitingListCode
					and	TransferListWaiter.ContextCode != Encounter.ContextCode
					and	(
							CentralPatientTransfer.PatientId = TraffordPatient.PatientId
						or	TraffordPatientTransfer.PatientId = CentralPatient.PatientId
						)
					)
				)
			then 0
			else 1
			end
			as bit
		)


	,Reference.PriorityID
	,Encounter.IsShortNotice
	,EthnicCategoryID = Reference.EthnicOriginID
	,ReligionID = Reference.ReligionID
from
	APCWL.BaseEncounter Encounter

inner join APCWL.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join Allocation.DatasetAllocation NationalExam
on	NationalExam.AllocationTypeID = 1
and	NationalExam.DatasetCode = 'APCWAIT'
and	NationalExam.DatasetRecno = Encounter.MergeEncounterRecno

inner join Allocation.DatasetAllocation Service
on	Service.AllocationTypeID = 2
and	Service.DatasetCode = 'APCWAIT'
and	Service.DatasetRecno = Encounter.MergeEncounterRecno

inner join WH.Directorate
on	Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')

inner join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'


--different versions of records based on various breach targets
inner join
   (
   select
          Encounter.MergeEncounterRecno
          ,WaitTargetID = 7
          ,BreachDateID = 
                 (
                 select
                       DateID
                 from
                       WH.Calendar
                 where
                       TheDate = 
                              dateadd(
                                     day
                                     ,7 * 7
                                     ,dateadd(
                                            day
                                            ,-1 * Encounter.LengthOfWait
                                            --,Census.CensusDate
                                            ,Census.TheDate
                                     )
                              )
                 )
   from
          APCWL.BaseEncounter Encounter

   inner join APCWL.BaseEncounterReference Reference
   on     Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

   --inner join WH.Census
   --on   Census.CensusDateID = Reference.CensusDateID

   inner join WH.Calendar Census
   on     Census.DateID = Reference.CensusDateID

   union

   select
          Encounter.MergeEncounterRecno
          ,WaitTargetID = 11
          ,BreachDateID = 
                 (
                 select
                       DateID
                 from
                       WH.Calendar
                 where
                       TheDate = 
                              dateadd(
                                     day
                                     ,7 * 11
                                     ,dateadd(
                                            day
                                            ,-1 * Encounter.LengthOfWait
                                            --,Census.CensusDate
                                            ,Census.TheDate
                                     )
                              )
                 )
   from
          APCWL.BaseEncounter Encounter

   inner join APCWL.BaseEncounterReference Reference
   on     Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

   --inner join WH.Census
   --on   Census.CensusDateID = Reference.CensusDateID

   inner join WH.Calendar Census
   on     Census.DateID = Reference.CensusDateID

   union

   select
          Encounter.MergeEncounterRecno
          ,WaitTargetID = 20
          ,BreachDateID = 
                 (
                 select
                       DateID
                 from
                       WH.Calendar
                 where
                       TheDate = 
                              dateadd(
                                     day
                                     ,7 * 20
                                     ,dateadd(
                                            day
                                            ,-1 * Encounter.LengthOfWait
                                            --,Census.CensusDate
                                            ,Census.TheDate
                                     )
                              )
                 )
   from
          APCWL.BaseEncounter Encounter

   inner join APCWL.BaseEncounterReference Reference
   on     Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

   --inner join WH.Census
   --on   Census.CensusDateID = Reference.CensusDateID

   inner join WH.Calendar Census
   on     Census.DateID = Reference.CensusDateID

   union

   select
          Encounter.MergeEncounterRecno
          ,WaitTargetID = 26
          ,BreachDateID = 
                 (
                 select
                       DateID
                 from
                       WH.Calendar
                 where
                       TheDate = 
                              dateadd(
                                     day
                                     ,7 * 26
                                     ,dateadd(
                                            day
                                            ,-1 * Encounter.LengthOfWait
                                            --,Census.CensusDate
                                            ,Census.TheDate
                                     )
                              )
                 )
   from
          APCWL.BaseEncounter Encounter

   inner join APCWL.BaseEncounterReference Reference
   on     Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

   --inner join WH.Census
   --on   Census.CensusDateID = Reference.CensusDateID

   inner join WH.Calendar Census
   on     Census.DateID = Reference.CensusDateID

   ) WaitTarget
on	WaitTarget.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WH.Calendar BreachDate
on	BreachDate.DateID = WaitTarget.BreachDateID

inner join APCWL.WaitingList
on	WaitingList.SourceWaitingListID = Reference.WaitingListID

left join [$(Chameleon)].Patient.PatientApplicationIdentity CentralPatient
on	CentralPatient.PatientApplicationIdentityValue = Encounter.DistrictNo
and	CentralPatient.SourceApplicationIdentifierId = 1 --DistrictNo
and	Encounter.ContextCode = 'CEN||PAS'

left join [$(Chameleon)].Patient.PatientApplicationIdentity TraffordPatient
on	TraffordPatient.PatientApplicationIdentityValue = Encounter.SourcePatientNo
and	TraffordPatient.SourceApplicationIdentifierId = 2 --EPMINumber
and	Encounter.ContextCode = 'TRA||UG'


select
       @Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
       @Stats =
              'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
       @ProcedureName
       ,@Stats


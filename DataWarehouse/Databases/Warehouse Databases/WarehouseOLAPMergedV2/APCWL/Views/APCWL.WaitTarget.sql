﻿


CREATE view [APCWL].[WaitTarget] as

select
	 WaitTargetID
	,WaitTarget
from
	WH.WaitTargetBase

where
	WaitTargetTypeCode = 'APC'



﻿
CREATE view [APCWL].[Priority] as

SELECT
	 SourceContextCode
	,SourceContextID
	,SourceContext
	,SourcePriorityID = SourceValueID
	,SourcePriorityCode = SourceValueCode
	,SourcePriority = SourceValue
	,LocalPriorityID = LocalValueID
	,LocalPriorityCode = LocalValueCode
	,LocalPriority = LocalValue
	,NationalPriorityID = NationalValueID
	,NationalPriorityCode = NationalValueCode
	,NationalPriority = NationalValue
FROM
	WH.Member
where 
	AttributeCode = 'WLPRIORITY'

union all

select
	 SourceContextCode = 'N/A'
	,SourceContextID = null
	,SourceContext = 'N/A'
	,SourcePriorityID = -1
	,SourcePriorityCode = '-1'
	,SourcePriority = 'Not Specified'
	,LocalPriorityID = null
	,LocalPriorityCode = null
	,LocalPriority = null
	,NationalPriorityID = null
	,NationalPriorityCode = null
	,NationalPriority = null





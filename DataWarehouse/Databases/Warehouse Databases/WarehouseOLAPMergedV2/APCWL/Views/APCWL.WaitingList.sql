﻿


CREATE view [APCWL].[WaitingList] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceWaitingListID = SourceValueID
	,SourceWaitingListCode = SourceValueCode
	,SourceWaitingList = SourceValue
	,LocalWaitingListID = LocalValueID
	,LocalWaitingListCode = LocalValueCode
	,LocalWaitingList = LocalValue
	,NationalWaitingListID = NationalValueID
	,NationalWaitingListCode = NationalValueCode
	,NationalWaitingList = NationalValue

	,IsOriginator =
		CAST(
			case
			when TransferWaitingList.ScheduleAtOrigin is null then 1
			when
				Member.SourceContextCode =
					case
					when TransferWaitingList.OriginEPRApplicationContextID = 1 then 'CEN||PAS'
					when TransferWaitingList.OriginEPRApplicationContextID = 2 then 'TRA||UG'
					end

			then 1
			else 0
			end
		as bit
		)

	,IsPrimaryScheduler =
		CAST(
			case
			when TransferWaitingList.ScheduleAtOrigin is null then 1
			when
				Member.SourceContextCode =
					case
					when TransferWaitingList.OriginEPRApplicationContextID = 1 then 'CEN||PAS'
					when TransferWaitingList.OriginEPRApplicationContextID = 2 then 'TRA||UG'
					end
			and	TransferWaitingList.ScheduleAtOrigin = 1

			then 1
			else 0
			end
		as bit
		)

	,IsTransferWaitingList =
		CAST(
			case
			when TransferWaitingList.WaitingListCode is null then 0
			else 1
			end
		as bit
		)

FROM
	WH.Member

left join [$(ActivityBroker)].AB.WaitingList TransferWaitingList
on	TransferWaitingList.WaitingListCode = Member.SourceValueCode
and	TransferWaitingList.PathwayTypeID = 1


where
	AttributeCode = 'IPWLIST'












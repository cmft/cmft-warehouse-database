﻿
CREATE Procedure [AE].[BuildBaseEncounterBaseResult]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @inserted int
	 ,@deleted int

declare
	@LastUpdated datetime = 
		(
		select 
			--dateadd(day,-2,(MAX(EventTime)))
			MAX(EventTime)
		from 
			[$(Warehouse)].Utility.AuditLog
		where 
			ProcessCode = 'AE.BuildBaseEncounterBaseResult'
		)
		
 
-- Insert ResultRecnos which are in the latest Result archive
Select
	MergeRecno
into 
	#ResultProcessList
from 
	Result.ProcessListArchive
where
	ArchiveTime = (Select MAX(ArchiveTime) from Result.ProcessListArchive)

-- Insert ResultRecno which are linked to an AE encounter which are in the latest AE archive
Insert into #ResultProcessList
Select
	MergeResultRecno
from
	AE.BaseEncounterBaseResult

inner join AE.BaseEncounter
on BaseEncounterBaseResult.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

where
	Updated >= @LastUpdated
or
	(
		Created >= @LastUpdated 
	and Updated is null
	)

and not exists
	(
	Select 
		1	
	from
		#ResultProcessList
	where
		#ResultProcessList.MergeRecno = BaseEncounterBaseResult.MergeResultRecno
	)

-- Insert ResultRecno of any records which are linked to AE but now the AE encounter recno no longer exists
Insert into #ResultProcessList
Select
	MergeResultRecno
from
	AE.BaseEncounterBaseResult
where 
	not exists
		(
		Select
			1
		from
			AE.BaseEncounter Encounter
		where 
			BaseEncounterBaseResult.MergeEncounterRecno = Encounter.MergeEncounterRecno
		and Encounter.Reportable = 1
		)
and not exists
	(
	Select
		1
	from
		#ResultProcessList Present
	where
		Present.MergeRecno = BaseEncounterBaseResult.MergeResultRecno
	)



-- Delete where the MergeResultRecno no longer exists in the Result table 
-- or 
-- the MergeResultRecno is in the temporary table created above as we'll re insert
-- (was one query with an or but was taking a few minutes so changed it to two queries went to 30secs)

delete
from
	AE.BaseEncounterBaseResult
where 
	not exists
		(
		Select
			1
		from
			Result.BaseResult
		where 
			BaseEncounterBaseResult.MergeResultRecno = BaseResult.MergeResultRecno
		)

Select
	@deleted = @@ROWCOUNT
	
delete
from
	AE.BaseEncounterBaseResult
where 
	exists
	(
	Select
		1
	from
		#ResultProcessList
	where
		#ResultProcessList.MergeRecno = BaseEncounterBaseResult.MergeResultRecno
	)	

Select
	@deleted = @deleted + @@ROWCOUNT

--Truncate table AE.BaseEncounterBaseResult

Select 
	MergeEncounterRecno
	,MergeResultRecno
	,ArrivalTime
into 
	#AEResult
from
	(
	Select
		Encounter.MergeEncounterRecno
		,BaseResult.MergeResultRecno
		,Encounter.ArrivalTime
	from 
		Result.BaseResult

	inner join #ResultProcessList
	on BaseResult.MergeResultRecno = #ResultProcessList.MergeRecno
	
	inner join AE.BaseEncounter Encounter
	on BaseResult.DistrictNo = Encounter.DistrictNo
	and Encounter.Reportable = 1
	and left(Encounter.ContextCode,3) = 'CEN'
	and BaseResult.EffectiveTime between Encounter.ArrivalTime and coalesce(Encounter.DepartureTime,Encounter.AttendanceConclusionTime,getdate())
	
	where
		BaseResult.DistrictNo is not null

	union

	Select
		Encounter.MergeEncounterRecno
		,BaseResult.MergeResultRecno
		,Encounter.ArrivalTime
	from 
		Result.BaseResult

	inner join #ResultProcessList
	on BaseResult.MergeResultRecno = #ResultProcessList.MergeRecno
	
	inner join AE.BaseEncounter Encounter
	on BaseResult.NHSNumber = Encounter.NHSNumber
	and Encounter.Reportable = 1
	and BaseResult.EffectiveTime between Encounter.ArrivalTime and coalesce(Encounter.DepartureTime,Encounter.AttendanceConclusionTime,getdate())

	where
		BaseResult.NHSNumber is not null
			
	union

	Select
		Encounter.MergeEncounterRecno
		,BaseResult.MergeResultRecno
		,Encounter.ArrivalTime
	from 
		Result.BaseResult

	inner join #ResultProcessList
	on BaseResult.MergeResultRecno = #ResultProcessList.MergeRecno
	
	inner join AE.BaseEncounter Encounter
	on BaseResult.CasenoteNumber = Encounter.CasenoteNo
	and Encounter.Reportable = 1
	and left(Encounter.ContextCode,3) = 'CEN'
	and BaseResult.EffectiveTime between Encounter.ArrivalTime and coalesce(Encounter.DepartureTime,Encounter.AttendanceConclusionTime,getdate())

	where
		BaseResult.CasenoteNumber is not null
	) 
	AEResult


CREATE NONCLUSTERED INDEX [IX_AETempTable_MergeResultRecno_ArrivalTime_MergeEncounterRecno]
ON [dbo].[#AEResult] ([MergeResultRecno],[ArrivalTime],[MergeEncounterRecno])	

Insert Into AE.BaseEncounterBaseResult
	(
	MergeEncounterRecno
	,MergeResultRecno
	)
Select 
	MergeEncounterRecno
	,MergeResultRecno
from 
	#AEResult
where
	not exists
		(
		Select	
			1
		from 
			#AEResult Latest
		where
			Latest.MergeResultRecno = #AEResult.MergeResultRecno
		and Latest.ArrivalTime > #AEResult.ArrivalTime
		)
and not exists	
		(
		Select
			1
		from 
			#AEResult Latest
		where
			Latest.MergeResultRecno = #AEResult.MergeResultRecno
			and Latest.ArrivalTime = #AEResult.ArrivalTime
			and Latest.MergeEncounterRecno > #AEResult.MergeEncounterRecno
		)

select
	@inserted = @@ROWCOUNT
	
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		' Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


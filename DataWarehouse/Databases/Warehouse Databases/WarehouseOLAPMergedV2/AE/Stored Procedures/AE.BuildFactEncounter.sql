﻿CREATE procedure [AE].[BuildFactEncounter] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @inserted int
	,@deleted int


declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


declare @LoadAEDate date =
	(select DateValue from [$(Warehouse)].dbo.Parameter where Parameter = 'LOADAEDATE')


select
	 BaseEncounter.MergeEncounterRecno
	,BaseEncounter.EncounterStartTimeOfDay
	,BaseEncounter.EncounterEndTimeOfDay
	,BaseEncounter.EncounterDurationMinutes
	,BaseEncounter.Reportable
	,BaseEncounter.UnplannedReattend7Day
	,BaseEncounter.LeftWithoutBeingSeenCases
	,BaseEncounter.ServiceID
	,BaseEncounter.LevelOfCareCode
	,BaseEncounter.DepartmentTypeCode
	,BaseEncounter.EncounterBreachStatusCode
	,BaseEncounter.ContextCode
	,BaseEncounter.ArrivalTimeAdjusted
	,BaseEncounter.PCDULeftDepartmentTime
	,BaseEncounter.CDULeftDepartmentTime
	,BaseEncounter.DepartureTimeAdjusted
	,BaseEncounter.DepartureTime
	,BaseEncounter.ArrivalDate

	,EncounterDepartureTime =
		coalesce(
			 BaseEncounter.PCDULeftDepartmentTime
			,BaseEncounter.CDULeftDepartmentTime
			,BaseEncounter.DepartureTimeAdjusted
		)

	,EncounterWaitTime =
		datediff(
			 minute
			,BaseEncounter.ArrivalTimeAdjusted
			,coalesce(
				 BaseEncounter.PCDULeftDepartmentTime
				,BaseEncounter.CDULeftDepartmentTime
				,BaseEncounter.DepartureTimeAdjusted
			)
		)

	,BaseEncounterReference.ArrivalModeID
	,BaseEncounterReference.AttendanceDisposalID
	,BaseEncounterReference.SiteID
	,BaseEncounterReference.AttendanceCategoryID
	,BaseEncounterReference.ReferredToSpecialtyID
	,BaseEncounterReference.EncounterStartDateID
	,BaseEncounterReference.EncounterEndDateID
	,BaseEncounterReference.AttendanceResidenceCCGID
	,BaseEncounterReference.AgeID
	,BaseEncounterReference.SexID
	,BaseEncounterReference.EthnicOriginID
	,BaseEncounterReference.ContextID
	,BaseEncounterReference.IncidentLocationTypeID
	,BaseEncounterReference.PatientGroupID
	,BaseEncounterReference.SourceOfReferralID
	,BaseEncounterReference.TriageCategoryID
	,BaseEncounterReference.CareGroupID
	,BaseEncounterReference.ReligionID

	,BaseEncounterStage.StageCode
	,BaseEncounterStage.StageStartTime
	,BaseEncounterStage.StageEndTime

	,StageWaitTime =
		datediff(
			 minute
			,BaseEncounterStage.StageStartTime
			,BaseEncounterStage.StageEndTime
		)

into
	#BaseEncounterStage
from
	AE.BaseEncounter

inner join AE.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join AE.ProcessList
on	ProcessList.MergeRecno = BaseEncounter.MergeEncounterRecno
and	ProcessList.Dataset = 'Encounter'

inner join
	(
	--in department cases
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'INDEPARTMENT'
		,StageStartTime = BaseEncounter.ArrivalTime
		--,StageEndTime = BaseEncounter.AttendanceConclusionTime
		,StageEndTime = 
			coalesce(
				 BaseEncounter.PCDULeftDepartmentTime
				,BaseEncounter.CDULeftDepartmentTime
				,BaseEncounter.DepartureTime
		)
	from
		AE.BaseEncounter

	union all

	--Seen For Treatment cases
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'SEENFORTREATMENT'
		,StageStartTime = BaseEncounter.ArrivalTime
		,StageEndTime = coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime)
	from
		AE.BaseEncounter
	where
		BaseEncounter.SeenForTreatmentTime is not null
	or	BaseEncounter.ClinicalAssessmentTime is not null


	union all

	--Awaiting Diagnostic cases
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'AWAITINGDIAGNOSTIC'
		,StageStartTime = AEInvestigation.InvestigationDate
		,StageEndTime = AEInvestigation.ResultDate
	from
		AE.BaseEncounter

	inner join [$(Warehouse)].AE.Investigation AEInvestigation
	on	AEInvestigation.AESourceUniqueID = BaseEncounter.SourceUniqueID
	and	AEInvestigation.SequenceNo = 1

	union all

	-- Arrival to X-ray Available
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'ARRIVALTOXRAYAVAILABLE'
		,StageStartTime = BaseEncounter.ArrivalTime
		,StageEndTime = BaseEncounter.FromXrayTime
	from
		AE.BaseEncounter
	where
		BaseEncounter.ToXrayTime is not null

	union all

	-- Awaiting X-ray
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'AWAITINGXRAY'
		,StageStartTime = BaseEncounter.ToXrayTime
		,StageEndTime = BaseEncounter.FromXrayTime
	from
		AE.BaseEncounter
	where
		BaseEncounter.ToXrayTime is not null

	union all

	--Awaiting Specialty cases
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'AWAITINGSPECIALTY'
		,StageStartTime = BaseEncounter.ToSpecialtyTime
		,StageEndTime = BaseEncounter.SeenBySpecialtyTime
	from
		AE.BaseEncounter
	where
		BaseEncounter.ToSpecialtyTime is not null

	union all

	--Awaiting Specialty Conclusion cases
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'AWAITINGSPECIALTYCONCLUSION'
		,StageStartTime = BaseEncounter.SeenBySpecialtyTime
		,StageEndTime = BaseEncounter.AttendanceConclusionTime
	from
		AE.BaseEncounter
	where
		BaseEncounter.SeenBySpecialtyTime is not null

	union all

	--Awaiting Bed
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'AWAITINGBED'
		,StageStartTime = BaseEncounter.DecisionToAdmitTime
		,StageEndTime = BaseEncounter.DepartureTime
	from
		AE.BaseEncounter

	inner join AE.BaseEncounterReference
	on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	
	left join AE.AttendanceDisposal Disposal
	on	Disposal.SourceAttendanceDisposalID = BaseEncounterReference.AttendanceDisposalID
	
	where
		BaseEncounter.DecisionToAdmitTime is not null
	and	Disposal.NationalAttendanceDisposalCode = '01' --admitted

	union all

	--Awaiting Bed (Adjusted)
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'AWAITINGBEDADJUSTED'
		,StageStartTime = 
			coalesce(
				 BaseEncounter.DecisionToAdmitTime
				,BaseEncounter.AttendanceConclusionTime
				,BaseEncounter.DepartureTime
			)
		,StageEndTime = BaseEncounter.DepartureTime
	from
		AE.BaseEncounter
	
	inner join AE.BaseEncounterReference
	on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	
	left join AE.AttendanceDisposal Disposal
	on	Disposal.SourceAttendanceDisposalID = BaseEncounterReference.AttendanceDisposalID
	
	where
		Disposal.NationalAttendanceDisposalCode = '01' --admitted

	union all

	--Triage cases
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'TRIAGE'
		,StageStartTime = BaseEncounter.ArrivalTime
		,StageEndTime = BaseEncounter.InitialAssessmentTime
	from
		AE.BaseEncounter
	where
		BaseEncounter.InitialAssessmentTime is not null

	union all

	--registration to departure
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'REGISTRATIONTODEPARTURE'
		,StageStartTime =
			coalesce(
				 BaseEncounter.RegisteredTime
				,BaseEncounter.ArrivalTime
			)
		,StageEndTime = BaseEncounter.DepartureTime
	from
		AE.BaseEncounter

	union all

	-- Awaiting Transport cases
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'AWAITINGTRANSPORT'
		,StageStartTime = BaseEncounter.TransportRequestTime
		,StageEndTime = BaseEncounter.DepartureTime
	from
		AE.BaseEncounter
	where
		BaseEncounter.TransportRequestTime is not null

	union all

	-- Ascribe In Department cases
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'INDEPARTMENTASCRIBE'
		,StageStartTime = BaseEncounter.ArrivalTime
		,StageEndTime = BaseEncounter.AscribeLeftDeptTime
	from
		AE.BaseEncounter

-- CCB 2011-04-07
-- new stages added

	union all

	-- Arrival to Triage, adjusted for the influence of Ambulance Arrival Time, if exists
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'ARRIVALTOTRIAGEADJUSTED'
		,StageStartTime = 
			coalesce(
				 BaseEncounter.ArrivalTimeAdjusted
				,BaseEncounter.ArrivalTime
			)

		-- CCB 2011-06-06 in some cases triage may occur after being seen by a doctor, so use the earliest time
		,StageEndTime =
			case
			when coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime) is null then BaseEncounter.InitialAssessmentTime
			when coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime) < BaseEncounter.InitialAssessmentTime then coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime)
			else BaseEncounter.InitialAssessmentTime
			end
	from
		AE.BaseEncounter

	union all

	-- Ambulance Arrival to Arrival in A&E
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'INAMBULANCE'
		,StageStartTime = BaseEncounter.AmbulanceArrivalTime
		,StageEndTime = BaseEncounter.ArrivalTime
	from
		AE.BaseEncounter
	where
		BaseEncounter.AmbulanceArrivalTime is not null

	union all

	-- Arrival to Departure, Adjusted for the influence of Ambulance Arrival Time, if exists
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'INDEPARTMENTADJUSTED'
		,StageStartTime = 
			coalesce(
				 BaseEncounter.ArrivalTimeAdjusted
				,BaseEncounter.ArrivalTime
			)
		--,StageEndTime = BaseEncounter.AttendanceConclusionTime
		,StageEndTime = 
			coalesce(
				 BaseEncounter.PCDULeftDepartmentTime
				,BaseEncounter.CDULeftDepartmentTime
				,BaseEncounter.DepartureTime
			)
	from
		AE.BaseEncounter

	union all


	-- Arrival to Departure, Adjusted for the influence of Ambulance Arrival Time, if exists. Conclusiontime used for Trafford and Altrincham sites
	select
		 BaseEncounter.MergeEncounterRecno
		 					
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'INDEPARTMENTADJUSTEDNATIONAL'
		,StageStartTime = 
			coalesce(
				 BaseEncounter.ArrivalTimeAdjusted
				,BaseEncounter.ArrivalTime
			)
		--,StageEndTime = BaseEncounter.AttendanceConclusionTime
		,StageEndTime = 
			coalesce(
				 BaseEncounter.PCDULeftDepartmentTime
				,BaseEncounter.CDULeftDepartmentTime
				,
				--case 
				--	when wh.[Site].NationalSiteCode IN ('RW3TR','RW3T1','RW3T2','RW3T3','RW3T4')
					--	then BaseEncounter.AttendanceConclusionTime 
					--else 
					BaseEncounter.DepartureTimeAdjusted --adjusted for Trafford conclusion time and CMFT where disposal code is 2 and 3
		--		end
			)
	from
		AE.BaseEncounter

	union all
	
	--Seen For Treatment cases adjusted
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'SEENFORTREATMENTADJUSTED'
		,StageStartTime = 
			coalesce(
				 BaseEncounter.ArrivalTimeAdjusted
				,BaseEncounter.ArrivalTime
			)
		,StageEndTime = coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime)
	from
		AE.BaseEncounter
	where
		BaseEncounter.SeenForTreatmentTime is not null
	or	BaseEncounter.ClinicalAssessmentTime is not null
	
	union all

-- GS 2011-08-01
-- new stages added

	--Seen For Treatment cases adjusted to include cases where a seen time was expected but has not been recorded
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'SEENFORTREATMENTADJUSTEDINC'
		,StageStartTime = 
			coalesce(
				 BaseEncounter.ArrivalTimeAdjusted
				,BaseEncounter.ArrivalTime
			)
		,StageEndTime = coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime)
	from
		AE.BaseEncounter

	inner join AE.BaseEncounterReference
	on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	
	left join AE.AttendanceDisposal Disposal
	on	Disposal.SourceAttendanceDisposalID = BaseEncounterReference.AttendanceDisposalID

	WHERE 
		(		
			BaseEncounter.SeenForTreatmentTime is not null
		OR	BaseEncounter.ClinicalAssessmentTime is not null
		)
	OR
		(		
			BaseEncounter.SeenForTreatmentTime is null
		and	BaseEncounter.ClinicalAssessmentTime is null
		AND Disposal.NationalAttendanceDisposalCode not in ('12','13')
		)



	union all

	-- Triage to Treatment
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'TRIAGETOTREATMENT'

		,StageStartTime = 
			case
			when coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime) is null then BaseEncounter.InitialAssessmentTime
			when coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime) < BaseEncounter.InitialAssessmentTime
				then coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime)
			else BaseEncounter.InitialAssessmentTime
			end

		,StageEndTime = coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime)
	from
		AE.BaseEncounter

	inner join AE.BaseEncounterReference
	on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	
	left join AE.AttendanceDisposal Disposal
	on	Disposal.SourceAttendanceDisposalID = BaseEncounterReference.AttendanceDisposalID

	WHERE 
		BaseEncounter.InitialAssessmentTime is not null
	and (
			(		
				BaseEncounter.SeenForTreatmentTime is not null
			OR	BaseEncounter.ClinicalAssessmentTime is not null
			)
		OR
			(		
				BaseEncounter.SeenForTreatmentTime is null
			and	BaseEncounter.ClinicalAssessmentTime is null
			AND Disposal.NationalAttendanceDisposalCode not in ('12','13')
			)
		)



	union all

	-- Treatment to Departure
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'TREATMENTTODEPARTUREADJUSTED'

		,StageStartTime = coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime)
		,StageEndTime = 
			coalesce(
				 BaseEncounter.PCDULeftDepartmentTime
				,BaseEncounter.CDULeftDepartmentTime
				,BaseEncounter.DepartureTimeAdjusted --adjusted for Trafford conclusion time and CMFT where disposal code is 2 and 3
			)
	from
		AE.BaseEncounter

	WHERE 
		not
			(		
				BaseEncounter.SeenForTreatmentTime is  null
			and	BaseEncounter.ClinicalAssessmentTime is  null
			)



	union all

	-- Treatment to DTA
	select
		 BaseEncounter.MergeEncounterRecno
		--,SourceUniqueID = BaseEncounter.SourceUniqueID
		,StageCode = 'TREATMENTTODTA'

		,StageStartTime = coalesce(BaseEncounter.ClinicalAssessmentTime, BaseEncounter.SeenForTreatmentTime)
		,StageEndTime = BaseEncounter.DecisionToAdmitTime
	from
		AE.BaseEncounter
	
	inner join AE.BaseEncounterReference
	on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	
	left join AE.AttendanceDisposal Disposal
	on	Disposal.SourceAttendanceDisposalID = BaseEncounterReference.AttendanceDisposalID
	
	where
		Disposal.NationalAttendanceDisposalCode = '01' --admitted

	and	not
		(		
			BaseEncounter.SeenForTreatmentTime is null
		and	BaseEncounter.ClinicalAssessmentTime is  null
		)

	) BaseEncounterStage
on	BaseEncounterStage.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

where
	coalesce(
		 BaseEncounter.PCDULeftDepartmentTime
		,BaseEncounter.CDULeftDepartmentTime
		,BaseEncounter.DepartureTimeAdjusted
		,dateadd(day , -1 , @LoadAEDate)
	) < @LoadAEDate
and	BaseEncounter.ArrivalDate < @LoadAEDate


delete
from
	AE.FactEncounter
where
	exists
	(
	select
		1
	from
		AE.ProcessList
	where
		ProcessList.MergeRecno = FactEncounter.MergeEncounterRecno
	and	ProcessList.Dataset = 'Encounter'
	)

select
	@deleted = @@rowcount


insert into AE.FactEncounter
(
	 MergeEncounterRecno
	,StageID
	,ArrivalModeID
	,AttendanceDisposalID
	,SiteID
	,AttendanceCategoryID
	,ReferredToSpecialtyID
	,LevelOfCareID
	,EncounterBreachStatusID
	,EncounterStartTimeOfDay
	,EncounterEndTimeOfDay
	,EncounterStartDateID
	,EncounterEndDateID
	,StageBreachStatusID
	,StageDurationMinutes
	,StageBreachMinutes
	,StageStartTimeOfDay
	,StageEndTimeOfDay
	,StageStartDateID
	,StageEndDateID
	,BreachValue
	,EncounterDurationMinutes
	,AgeID
	,StageDurationBandID
	,EncounterDurationBandID
	,Reportable
	,StageEndTimeIsWorkingHours
	,StageStartTimeIsWorkingHours
	,SexID
	,EthnicCategoryID
	,ContextID
	,IncidentLocationTypeID
	,PatientGroupID
	,SourceOfReferralID
	,TriageCategoryID
	,DepartmentTypeID
	,DiagnosisFirstID
	,AttendanceResidenceCCGID
	,Cases
	,UnplannedReattend7DayCases
	,LeftWithoutBeingSeenCases

	,CareGroupID
	,CareGroupTypeEncounterBreachStatusID
	,CareGroupTypeStageBreachStatusID
	,ServiceID
	,ReligionID
)

select
	 MergeEncounterRecno
	,StageID
	,ArrivalModeID
	,AttendanceDisposalID
	,SiteID
	,AttendanceCategoryID
	,ReferredToSpecialtyID
	,LevelOfCareID
	,EncounterBreachStatusID
	,EncounterStartTimeOfDay
	,EncounterEndTimeOfDay
	,EncounterStartDateID
	,EncounterEndDateID
	,StageBreachStatusID
	,StageDurationMinutes
	,StageBreachMinutes
	,StageStartTimeOfDay
	,StageEndTimeOfDay

	,StageStartDateID =
		coalesce(StageStartDate.DateID , StageEndDate.DateID)

	,StageEndDateID =
		coalesce(StageEndDate.DateID , StageStartDate.DateID)

	,BreachValue
	,EncounterDurationMinutes
	,AgeID
	,StageDurationBandID
	,EncounterDurationBandID
	,Reportable
	,StageEndTimeIsWorkingHours
	,StageStartTimeIsWorkingHours
	,SexID
	,EthnicCategoryID
	,ContextID
	,IncidentLocationTypeID
	,PatientGroupID
	,SourceOfReferralID
	,TriageCategoryID
	,DepartmentTypeID
	,DiagnosisFirstID
	,AttendanceResidenceCCGID
	,Cases
	,UnplannedReattend7DayCases = coalesce(UnplannedReattend7DayCases, 0)
	,LeftWithoutBeingSeenCases = coalesce(LeftWithoutBeingSeenCases, 0)

	,CareGroupID
	,CareGroupTypeEncounterBreachStatusID
	,CareGroupTypeStageBreachStatusID
	,ServiceID
	,ReligionID

from
	(
	select
		 BaseEncounterStage.MergeEncounterRecno
		,Stage.StageID
		,BaseEncounterStage.ArrivalModeID
		,BaseEncounterStage.AttendanceDisposalID
		,BaseEncounterStage.SiteID
		,BaseEncounterStage.AttendanceCategoryID
		,BaseEncounterStage.ReferredToSpecialtyID
		,LevelOfCare.LevelOfCareID
		,EncounterBreachStatusID = EncounterBreachStatus.BreachStatusID
		,BaseEncounterStage.EncounterStartTimeOfDay
		,BaseEncounterStage.EncounterEndTimeOfDay
		,BaseEncounterStage.EncounterStartDateID
		,BaseEncounterStage.EncounterEndDateID
		,StageBreachStatusID = StageBreachStatus.BreachStatusID

		,StageDurationMinutes =
			BaseEncounterStage.StageWaitTime

		,StageBreachMinutes =
			BaseEncounterStage.StageWaitTime - Stage.BreachValue

		,StageStartTimeOfDay =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, BaseEncounterStage.StageStartTime), 0)
					,BaseEncounterStage.StageStartTime
				)
				,-1
			)

		,StageEndTimeOfDay =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, BaseEncounterStage.StageEndTime), 0)
					,BaseEncounterStage.StageEndTime
				)
				,-1
			)

		,StageStartDate =
			coalesce(
				 dateadd(day, datediff(day, 0, BaseEncounterStage.StageStartTime), 0)
				,dateadd(day, datediff(day, 0, BaseEncounterStage.StageEndTime), 0)
			)

		,StageEndDate =
			coalesce(
				 dateadd(day, datediff(day, 0, BaseEncounterStage.StageEndTime), 0)
				,dateadd(day, datediff(day, 0, BaseEncounterStage.StageStartTime), 0)
			)

		,BaseEncounterStage.AttendanceResidenceCCGID
		,Cases = 1

		,BreachValue =
			case
			when BaseEncounterStage.StageEndTime is null 
			then null
			else Stage.BreachValue
			end


		,BaseEncounterStage.EncounterDurationMinutes
		,BaseEncounterStage.AgeID
		,UnplannedReattend7DayCases = BaseEncounterStage.UnplannedReattend7Day
		,BaseEncounterStage.LeftWithoutBeingSeenCases

		,StageDurationBandID =
			coalesce(
				case 
				when BaseEncounterStage.StageWaitTime < 0 then -1
				when BaseEncounterStage.StageWaitTime > 1440 then 1440
				else BaseEncounterStage.StageWaitTime
				end
				, -2
			)

		,EncounterDurationBandID =
			coalesce(
				case 
				when BaseEncounterStage.EncounterDurationMinutes < 0 then -1
				when BaseEncounterStage.EncounterDurationMinutes > 1440 then 1440
				else BaseEncounterStage.EncounterDurationMinutes
				end
				, -2
			)

		,BaseEncounterStage.Reportable
		
		,StageEndTimeIsWorkingHours =	
				CASE
					WHEN		
							datediff(minute,dateadd(day, datediff(day, 0, BaseEncounterStage.StageEndTime), 0),BaseEncounterStage.StageEndTime) BETWEEN 480 AND 1109 
						AND 
							dbo.udfIsWeekDay(BaseEncounterStage.StageEndTime) = 1
						THEN 1
					ELSE 0
				END

		,StageStartTimeIsWorkingHours =	
				CASE
					WHEN		
							datediff(minute,dateadd(day, datediff(day, 0, BaseEncounterStage.StageStartTime), 0),BaseEncounterStage.StageStartTime) BETWEEN 480 AND 1109 
						AND 
							dbo.udfIsWeekDay(BaseEncounterStage.StageStartTime) = 1
						THEN 1
					ELSE 0
				END
			
		,BaseEncounterStage.SexID
		,EthnicCategoryID = BaseEncounterStage.EthnicOriginID
		,BaseEncounterStage.ContextID
		,BaseEncounterStage.IncidentLocationTypeID
		,BaseEncounterStage.PatientGroupID
		,BaseEncounterStage.SourceOfReferralID
		,BaseEncounterStage.TriageCategoryID
		,DepartmentType.DepartmentTypeID

		,DiagnosisFirstID =
			coalesce(
				 DiagnosisFirstReference.DiagnosisID
				,DiagnosisNotSpecified.SourceDiagnosisID
			)

		,BaseEncounterStage.CareGroupID
		,CareGroupTypeEncounterBreachStatusID = CareGroupTypeEncounterBreachStatus.BreachStatusID
		,CareGroupTypeStageBreachStatusID = CareGroupTypeStageBreachStatus.BreachStatusID
		,BaseEncounterStage.ServiceID
		,BaseEncounterStage.ReligionID

	from
		#BaseEncounterStage BaseEncounterStage

	inner join AE.Stage
	on	Stage.StageCode = BaseEncounterStage.StageCode

	inner join AE.LevelOfCare
	on	LevelOfCare.LevelOfCareCode = BaseEncounterStage.LevelOfCareCode

	left join AE.BaseDiagnosis DiagnosisFirst
	on	DiagnosisFirst.MergeEncounterRecno = BaseEncounterStage.MergeEncounterRecno
	and DiagnosisFirst.SequenceNo = 1

	left join AE.BaseDiagnosisReference DiagnosisFirstReference
	on DiagnosisFirstReference.MergeRecno = DiagnosisFirst.MergeRecno

	left join AE.Diagnosis DiagnosisNotSpecified
	on	DiagnosisNotSpecified.SourceDiagnosisCode = '-1'
	and DiagnosisNotSpecified.SourceContextCode = BaseEncounterStage.ContextCode

	inner join AE.DepartmentType
	on	DepartmentType.DepartmentTypeCode = BaseEncounterStage.DepartmentTypeCode

	inner join AE.BreachStatus EncounterBreachStatus
	on	EncounterBreachStatus.BreachStatusCode = BaseEncounterStage.EncounterBreachStatusCode

	inner join AE.BreachStatus StageBreachStatus
	on	StageBreachStatus.BreachStatusCode =
			case
			when BaseEncounterStage.StageEndTime is null then 'X'
			when
				BaseEncounterStage.StageWaitTime > Stage.BreachValue
			then 'B'
			else 'N'
			end


	inner join AE.CareGroup
	on	CareGroup.SourceCareGroupID = BaseEncounterStage.CareGroupID

	inner join WH.Religion
	on	Religion.SourceReligionID = BaseEncounterStage.ReligionID

	left join [$(Warehouse)].AE.CareGroupTypeStageBreach CareGroupTypeEncounterBreach
	on	CareGroupTypeEncounterBreach.StageCode = 'INDEPARTMENTADJUSTEDNATIONAL'
	and	CareGroup.CareGroupTypeCode = CareGroupTypeEncounterBreach.CareGroupTypeCode
	and	BaseEncounterStage.EncounterWaitTime > CareGroupTypeEncounterBreach.BreachValue

	inner join AE.BreachStatus CareGroupTypeEncounterBreachStatus
	on	CareGroupTypeEncounterBreachStatus.BreachStatusCode =
			case
			when 
				BaseEncounterStage.EncounterDepartureTime is null then 'X'
			when
				CareGroupTypeEncounterBreach.BreachValue is not null
			then 'B'
			else 'N'
			end

	inner join [$(Warehouse)].AE.CareGroupTypeStageBreach CareGroupTypeStageBreach
	on	CareGroupTypeStageBreach.StageCode = BaseEncounterStage.StageCode
	and	CareGroupTypeStageBreach.CareGroupTypeCode = CareGroup.CareGroupTypeCode

	inner join AE.BreachStatus CareGroupTypeStageBreachStatus
	on	CareGroupTypeStageBreachStatus.BreachStatusCode =
			case
			when BaseEncounterStage.StageEndTime is null then 'X'
			when
				datediff(
					 minute
					,BaseEncounterStage.StageStartTime
					,BaseEncounterStage.StageEndTime
				)
				 > CareGroupTypeStageBreach.BreachValue
			then 'B'
			else 'N'
			end

	) AEEncounter

left join WH.Calendar StageStartDate
on	StageStartDate.TheDate = AEEncounter.StageStartDate

left join WH.Calendar StageEndDate
on	StageEndDate.TheDate = AEEncounter.StageEndDate

select @inserted = @@rowcount


update
	AE.FactEncounter
set
	DelayID = StageDelay.DelayID
from
	AE.FactEncounter

inner join AE.ProcessList
on	ProcessList.MergeRecno = FactEncounter.MergeEncounterRecno
and	ProcessList.Dataset = 'Encounter'

inner join
	(
	select
		 FactEncounter.MergeEncounterRecno
		,DelayID =
			row_number() over(
			partition by
				 FactEncounter.MergeEncounterRecno
			order by
				coalesce(
					FactEncounter.StageDurationMinutes * 1.0
					 /
					Stage.BreachValue * 1.0
					,1.0
				) desc
			)
	from
		AE.FactEncounter

	inner join AE.ProcessList
	on	ProcessList.MergeRecno = FactEncounter.MergeEncounterRecno
	and	ProcessList.Dataset = 'Encounter'

	inner join AE.Stage
	on	Stage.StageID = FactEncounter.StageID

	) StageDelay
on	StageDelay.MergeEncounterRecno = FactEncounter.MergeEncounterRecno


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


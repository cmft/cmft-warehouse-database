﻿
CREATE proc [AE].[BuildBaseInvestigationConsolidated] as 

truncate table AE.BaseInvestigationConsolidated

insert AE.BaseInvestigationConsolidated
(
MergeInvestigationRecno
,MergeEncounterRecno
,SequenceNo
,InvestigationCode
,InvestigationTime
,ContextCode
)

select
	MergeInvestigationRecno = MergeRecno
	,MergeEncounterRecno
	,SequenceNo = row_number() over (partition by MergeEncounterRecno order by HRGInvestigationSequenceNo)
	,InvestigationCode
	,InvestigationTime
	,ContextCode
from
	(
	
	select
		BaseInvestigation.MergeRecno
		,BaseInvestigation.MergeEncounterRecno
		,InvestigationCode = InvestigationTest.NationalInvestigationCode
		,BaseInvestigation.InvestigationTime
		,BaseInvestigation.ContextCode
		,HRGInvestigationSequenceNo = BaseInvestigation.SequenceNo
	from
		AE.BaseInvestigation

	inner join AE.Investigation InvestigationTest
	on	BaseInvestigation.InvestigationCode = InvestigationTest.SourceInvestigationCode
	and BaseInvestigation.ContextCode = InvestigationTest.SourceContextCode
	
	where
		not exists (
					select
						1
					from	
						AE.BaseInvestigation BaseInvestigationLater

					inner join AE.Investigation  InvestigationTestLater
					on	BaseInvestigationLater.InvestigationCode = InvestigationTestLater.SourceInvestigationCode
					and BaseInvestigationLater.ContextCode = InvestigationTestLater.SourceContextCode
					
					where
						BaseInvestigation.MergeEncounterRecno = BaseInvestigationLater.MergeEncounterRecno
					and BaseInvestigation.InvestigationTime = BaseInvestigationLater.InvestigationTime
					and BaseInvestigation.ContextCode = BaseInvestigationLater.ContextCode
					and InvestigationTest.NationalInvestigationCode = InvestigationTestLater.NationalInvestigationCode
					and BaseInvestigation.SequenceNo > BaseInvestigationLater.SequenceNo
					) 
				
	) Investigation

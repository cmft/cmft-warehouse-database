﻿CREATE TABLE [AE].[Service] (
    [ServiceID]   INT          NOT NULL,
    [ServiceCode] VARCHAR (2)  NOT NULL,
    [Service]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ServiceBase] PRIMARY KEY CLUSTERED ([ServiceID] ASC)
);


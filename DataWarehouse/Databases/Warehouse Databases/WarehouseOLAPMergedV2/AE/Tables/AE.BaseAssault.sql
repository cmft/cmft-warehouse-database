﻿CREATE TABLE [AE].[BaseAssault] (
    [MergeEncounterRecno]        INT            NOT NULL,
    [AssaultDate]                DATE           NULL,
    [AssaultTime]                SMALLDATETIME  NULL,
    [AssaultWeapon]              VARCHAR (80)   NULL,
    [AssaultWeaponDetails]       VARCHAR (4000) NULL,
    [AssaultLocation]            VARCHAR (80)   NULL,
    [AssaultLocationDetails]     VARCHAR (4000) NULL,
    [AlcoholConsumed3Hour]       VARCHAR (80)   NULL,
    [AssaultRelationship]        VARCHAR (80)   NULL,
    [AssaultRelationshipDetails] VARCHAR (4000) NULL,
    [ContextCode]                VARCHAR (10)   NULL,
    [Created]                    DATETIME       NOT NULL,
    [Updated]                    DATETIME       NULL,
    [ByWhom]                     VARCHAR (50)   NOT NULL,
    CONSTRAINT [PK_BaseAssault] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


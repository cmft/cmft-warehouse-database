﻿CREATE TABLE [AE].[BaseEncounterBaseResult] (
    [MergeEncounterRecno] INT NOT NULL,
    [MergeResultRecno]    INT NOT NULL,
    CONSTRAINT [PK_AEBaseEncounterBaseResult] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [MergeResultRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AEBaseEncounterBaseResultMergeResultRecn]
    ON [AE].[BaseEncounterBaseResult]([MergeResultRecno] ASC);


﻿CREATE TABLE [AE].[DepartmentTypeBase] (
    [DepartmentTypeID]   INT            NOT NULL,
    [DepartmentTypeCode] VARCHAR (2)    NOT NULL,
    [DepartmentType]     VARCHAR (50)   NULL,
    [DepartmentTypeFull] VARCHAR (4000) NULL,
    CONSTRAINT [PK_DepartmentTypeBase] PRIMARY KEY CLUSTERED ([DepartmentTypeID] ASC)
);


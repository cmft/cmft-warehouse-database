﻿CREATE TABLE [AE].[BaseEncounter] (
    [MergeEncounterRecno]                 INT            IDENTITY (1, 1) NOT NULL,
    [EncounterRecno]                      INT            NOT NULL,
    [SourceUniqueID]                      VARCHAR (50)   NOT NULL,
    [UniqueBookingReferenceNo]            VARCHAR (50)   NULL,
    [PathwayId]                           VARCHAR (50)   NULL,
    [PathwayIdIssuerCode]                 VARCHAR (10)   NULL,
    [RTTStatusCode]                       VARCHAR (10)   NULL,
    [RTTStartDate]                        SMALLDATETIME  NULL,
    [RTTEndDate]                          SMALLDATETIME  NULL,
    [DistrictNo]                          VARCHAR (50)   NULL,
    [TrustNo]                             VARCHAR (50)   NULL,
    [CasenoteNo]                          VARCHAR (50)   NULL,
    [DistrictNoOrganisationCode]          VARCHAR (10)   NULL,
    [NHSNumber]                           VARCHAR (17)   NULL,
    [NHSNumberStatusCode]                 VARCHAR (10)   NULL,
    [PatientTitle]                        VARCHAR (50)   NULL,
    [PatientForename]                     VARCHAR (100)  NULL,
    [PatientSurname]                      VARCHAR (100)  NULL,
    [PatientAddress1]                     VARCHAR (50)   NULL,
    [PatientAddress2]                     VARCHAR (50)   NULL,
    [PatientAddress3]                     VARCHAR (50)   NULL,
    [PatientAddress4]                     VARCHAR (50)   NULL,
    [Postcode]                            VARCHAR (10)   NULL,
    [DateOfBirth]                         DATETIME       NULL,
    [DateOfDeath]                         SMALLDATETIME  NULL,
    [SexCode]                             VARCHAR (10)   NULL,
    [CarerSupportIndicator]               BIT            NULL,
    [RegisteredGpCode]                    VARCHAR (10)   NULL,
    [RegisteredPracticeCode]              VARCHAR (10)   NULL,
    [AttendanceNumber]                    VARCHAR (50)   NULL,
    [ArrivalModeCode]                     VARCHAR (10)   NULL,
    [AttendanceCategoryCode]              VARCHAR (10)   NULL,
    [AttendanceDisposalCode]              VARCHAR (10)   NULL,
    [IncidentLocationTypeCode]            VARCHAR (10)   NULL,
    [PatientGroupCode]                    VARCHAR (10)   NULL,
    [SourceOfReferralCode]                VARCHAR (10)   NULL,
    [ArrivalDate]                         SMALLDATETIME  NULL,
    [ArrivalTime]                         SMALLDATETIME  NULL,
    [AgeOnArrival]                        TINYINT        NULL,
    [InitialAssessmentTime]               SMALLDATETIME  NULL,
    [SeenForTreatmentTime]                SMALLDATETIME  NULL,
    [AttendanceConclusionTime]            SMALLDATETIME  NULL,
    [DepartureTime]                       SMALLDATETIME  NULL,
    [CommissioningSerialNo]               VARCHAR (50)   NULL,
    [NHSServiceAgreementLineNo]           VARCHAR (50)   NULL,
    [ProviderReferenceNo]                 VARCHAR (50)   NULL,
    [CommissionerReferenceNo]             VARCHAR (50)   NULL,
    [ProviderCode]                        VARCHAR (10)   NULL,
    [CommissionerCode]                    VARCHAR (10)   NULL,
    [StaffMemberCode]                     VARCHAR (10)   NULL,
    [InvestigationCodeFirst]              VARCHAR (10)   NULL,
    [InvestigationCodeSecond]             VARCHAR (10)   NULL,
    [DiagnosisCodeFirst]                  VARCHAR (10)   NULL,
    [DiagnosisCodeSecond]                 VARCHAR (10)   NULL,
    [TreatmentCodeFirst]                  VARCHAR (10)   NULL,
    [TreatmentCodeSecond]                 VARCHAR (10)   NULL,
    [PASHRGCode]                          VARCHAR (10)   NULL,
    [HRGVersionCode]                      VARCHAR (10)   NULL,
    [PASDGVPCode]                         VARCHAR (10)   NULL,
    [SiteCode]                            VARCHAR (10)   NULL,
    [Created]                             DATETIME       CONSTRAINT [DF_BaseEncounter_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]                             DATETIME       NULL,
    [ByWhom]                              VARCHAR (50)   CONSTRAINT [DF_BaseEncounter_ByWhom] DEFAULT (suser_name()) NOT NULL,
    [InterfaceCode]                       VARCHAR (5)    NULL,
    [PCTCode]                             VARCHAR (8)    NULL,
    [ResidencePCTCode]                    VARCHAR (10)   NULL,
    [InvestigationCodeList]               VARCHAR (16)   NULL,
    [TriageCategoryCode]                  VARCHAR (10)   NULL,
    [PresentingProblem]                   VARCHAR (4000) NULL,
    [PresentingProblemCode]               VARCHAR (10)   NULL,
    [ToXrayTime]                          SMALLDATETIME  NULL,
    [FromXrayTime]                        SMALLDATETIME  NULL,
    [ToSpecialtyTime]                     SMALLDATETIME  NULL,
    [SeenBySpecialtyTime]                 SMALLDATETIME  NULL,
    [EthnicCategoryCode]                  VARCHAR (10)   NULL,
    [ReferredToSpecialtyCode]             VARCHAR (10)   NULL,
    [DecisionToAdmitTime]                 SMALLDATETIME  NULL,
    [DischargeDestinationCode]            VARCHAR (10)   NULL,
    [RegisteredTime]                      SMALLDATETIME  NULL,
    [TransportRequestTime]                SMALLDATETIME  NULL,
    [TransportAvailableTime]              SMALLDATETIME  NULL,
    [AscribeLeftDeptTime]                 SMALLDATETIME  NULL,
    [CDULeftDepartmentTime]               SMALLDATETIME  NULL,
    [PCDULeftDepartmentTime]              SMALLDATETIME  NULL,
    [TreatmentDateFirst]                  SMALLDATETIME  NULL,
    [TreatmentDateSecond]                 SMALLDATETIME  NULL,
    [SourceAttendanceDisposalCode]        INT            NULL,
    [AmbulanceArrivalTime]                SMALLDATETIME  NULL,
    [AmbulanceCrewPRF]                    VARCHAR (30)   NULL,
    [UnplannedReattend7Day]               INT            NULL,
    [ArrivalTimeAdjusted]                 SMALLDATETIME  NULL,
    [ClinicalAssessmentTime]              SMALLDATETIME  NULL,
    [LevelOfCareCode]                     VARCHAR (10)   NOT NULL,
    [EncounterBreachStatusCode]           VARCHAR (1)    NOT NULL,
    [EncounterStartTimeOfDay]             INT            NULL,
    [EncounterEndTimeOfDay]               INT            NULL,
    [EncounterStartDate]                  DATE           NULL,
    [EncounterEndDate]                    DATE           NULL,
    [EncounterDurationMinutes]            INT            NULL,
    [AgeCode]                             VARCHAR (27)   NULL,
    [LeftWithoutBeingSeenCases]           INT            NOT NULL,
    [HRGCode]                             VARCHAR (10)   NULL,
    [Reportable]                          BIT            NULL,
    [CareGroup]                           VARCHAR (20)   NULL,
    [EPMINo]                              VARCHAR (50)   NULL,
    [DepartmentTypeCode]                  VARCHAR (2)    NULL,
    [DepartureTimeAdjusted]               SMALLDATETIME  NULL,
    [CarePathwayAttendanceConclusionTime] SMALLDATETIME  NULL,
    [CarePathwayDepartureTime]            SMALLDATETIME  NULL,
    [ContextCode]                         VARCHAR (10)   NOT NULL,
    [GpCodeAtAttendance]                  VARCHAR (8)    NULL,
    [GpPracticeCodeAtAttendance]          VARCHAR (8)    NULL,
    [PostcodeAtAttendance]                VARCHAR (8)    NULL,
    [CCGCode]                             VARCHAR (10)   NULL,
    [AlcoholLocation]                     VARCHAR (80)   NULL,
    [AlcoholInPast12Hours]                VARCHAR (10)   NULL,
    [BaseUpdated]                         DATETIME       NULL,
    [ReligionCode]                        VARCHAR (10)   NULL,
    [AttendanceResidenceCCGCode]          VARCHAR (10)   NULL,
    [ServiceID]                           INT            NULL,
    CONSTRAINT [PK_BaseEncounter_5] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BE-SUICC]
    ON [AE].[BaseEncounter]([ContextCode] ASC, [EncounterRecno] ASC)
    INCLUDE([MergeEncounterRecno]);


GO
CREATE NONCLUSTERED INDEX [IX_BE-DoDCCDT]
    ON [AE].[BaseEncounter]([DateOfDeath] ASC, [ContextCode] ASC, [DepartureTime] ASC)
    INCLUDE([MergeEncounterRecno], [SourceUniqueID], [DistrictNo], [DateOfBirth]);


GO
CREATE NONCLUSTERED INDEX [IX_BE-RpAD]
    ON [AE].[BaseEncounter]([Reportable] ASC, [ArrivalDate] ASC)
    INCLUDE([MergeEncounterRecno], [EncounterRecno], [NHSNumber], [Postcode], [InitialAssessmentTime], [SeenForTreatmentTime], [AttendanceConclusionTime], [DepartureTime], [RegisteredTime], [AmbulanceArrivalTime], [ClinicalAssessmentTime], [DepartmentTypeCode], [ContextCode]);


GO
CREATE NONCLUSTERED INDEX [IX_BE-MERRAD]
    ON [AE].[BaseEncounter]([MergeEncounterRecno] ASC, [Reportable] ASC, [ArrivalDate] ASC)
    INCLUDE([EncounterRecno], [NHSNumber], [Postcode], [InitialAssessmentTime], [SeenForTreatmentTime], [AttendanceConclusionTime], [DepartureTime], [RegisteredTime], [AmbulanceArrivalTime], [ClinicalAssessmentTime], [DepartmentTypeCode], [ContextCode]);


GO
CREATE NONCLUSTERED INDEX [IX_BE-CC]
    ON [AE].[BaseEncounter]([ContextCode] ASC)
    INCLUDE([MergeEncounterRecno], [SourceUniqueID]);


GO
CREATE NONCLUSTERED INDEX [IX_BE-AD]
    ON [AE].[BaseEncounter]([ArrivalDate] ASC)
    INCLUDE([MergeEncounterRecno], [DistrictNo], [ArrivalTime], [InitialAssessmentTime], [DepartureTime], [ContextCode]);


GO
CREATE NONCLUSTERED INDEX [IX_BaseEncounter_AttendanceDisposalCode]
    ON [AE].[BaseEncounter]([AttendanceDisposalCode] ASC, [DistrictNo] ASC)
    INCLUDE([DepartureTime], [ContextCode]);


GO
CREATE NONCLUSTERED INDEX [IX_BaseEncounter_AttendanceDisposalCode_Reportable]
    ON [AE].[BaseEncounter]([AttendanceDisposalCode] ASC, [Reportable] ASC, [DistrictNo] ASC)
    INCLUDE([MergeEncounterRecno], [DepartureTime], [ContextCode]);


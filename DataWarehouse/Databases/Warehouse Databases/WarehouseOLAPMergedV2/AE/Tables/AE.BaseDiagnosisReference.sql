﻿CREATE TABLE [AE].[BaseDiagnosisReference] (
    [MergeRecno]  INT NOT NULL,
    [ContextID]   INT NOT NULL,
    [DiagnosisID] INT NULL,
    CONSTRAINT [PK_AE_Base_Diagnosis_Reference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


﻿CREATE TABLE [AE].[BaseInvestigationOriginal] (
    [MergeEncounterRecno]     INT           NOT NULL,
    [SequenceNo]              SMALLINT      NOT NULL,
    [InvestigationSchemeCode] VARCHAR (10)  NULL,
    [InvestigationCode]       VARCHAR (50)  NULL,
    [InvestigationTime]       SMALLDATETIME NULL,
    [ResultTime]              SMALLDATETIME NULL,
    [ContextCode]             VARCHAR (10)  NULL,
    [Created]                 DATETIME      NULL,
    [Updated]                 DATETIME      NULL,
    [ByWhom]                  VARCHAR (50)  NULL
);


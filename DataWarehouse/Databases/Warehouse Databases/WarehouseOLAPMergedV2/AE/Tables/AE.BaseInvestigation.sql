﻿CREATE TABLE [AE].[BaseInvestigation] (
    [MergeRecno]              INT          IDENTITY (1, 1) NOT NULL,
    [MergeEncounterRecno]     INT          NOT NULL,
    [SequenceNo]              SMALLINT     NULL,
    [InvestigationSchemeCode] VARCHAR (10) NULL,
    [InvestigationCode]       VARCHAR (50) NULL,
    [InvestigationTime]       DATETIME     NULL,
    [ResultTime]              DATETIME     NULL,
    [ContextCode]             VARCHAR (10) NOT NULL,
    [Created]                 DATETIME     CONSTRAINT [DF_BaseInvestigation_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) CONSTRAINT [DF_BaseInvestigation_ByWhom] DEFAULT (suser_name()) NOT NULL,
    [InvestigationRecno]      INT          NOT NULL,
    CONSTRAINT [PK_tmpBaseInvestigation_2] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


﻿CREATE TABLE [AE].[BaseTreatment] (
    [MergeRecno]          INT           IDENTITY (1, 1) NOT NULL,
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [TreatmentSchemeCode] VARCHAR (10)  NULL,
    [TreatmentCode]       VARCHAR (50)  NULL,
    [TreatmentTime]       SMALLDATETIME NULL,
    [ContextCode]         VARCHAR (10)  NOT NULL,
    [Created]             DATETIME      CONSTRAINT [DF_BaseTreatment_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (50)  CONSTRAINT [DF_BaseTreatment_ByWhom] DEFAULT (suser_name()) NOT NULL,
    [TreatmentRecno]      INT           NOT NULL,
    CONSTRAINT [PK_BaseTreatment_1] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BaseTreatment]
    ON [AE].[BaseTreatment]([ContextCode] ASC, [TreatmentRecno] ASC);


﻿CREATE TABLE [AE].[HRG4Encounter] (
    [MergeEncounterRecno] INT           NOT NULL,
    [HRGCode]             VARCHAR (50)  NULL,
    [Created]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_HRG4AEEncounter] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


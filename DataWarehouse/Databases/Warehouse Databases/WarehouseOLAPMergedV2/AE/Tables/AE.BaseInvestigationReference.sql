﻿CREATE TABLE [AE].[BaseInvestigationReference] (
    [MergeRecno]      INT NOT NULL,
    [ContextID]       INT NOT NULL,
    [InvestigationID] INT NULL,
    CONSTRAINT [PK_BaseInvestigationReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


﻿CREATE TABLE [AE].[BaseDiagnosisOriginal] (
    [MergeEncounterRecno]  INT          NOT NULL,
    [SequenceNo]           SMALLINT     NOT NULL,
    [DiagnosticSchemeCode] VARCHAR (10) NULL,
    [DiagnosisCode]        VARCHAR (50) NULL,
    [DiagnosisSiteCode]    VARCHAR (10) NULL,
    [DiagnosisSideCode]    VARCHAR (10) NULL,
    [ContextCode]          VARCHAR (10) NULL,
    [Created]              DATETIME     NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NULL
);


﻿CREATE TABLE [AE].[ProcessList] (
    [MergeRecno] INT           NOT NULL,
    [Action]     NVARCHAR (10) NOT NULL,
    [Dataset]    VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ProcessList] PRIMARY KEY CLUSTERED ([Dataset] ASC, [MergeRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_AE_ProcessList_01]
    ON [AE].[ProcessList]([Dataset] ASC)
    INCLUDE([MergeRecno]);


﻿CREATE TABLE [AE].[BaseDiagnosis] (
    [MergeRecno]           INT          IDENTITY (1, 1) NOT NULL,
    [MergeEncounterRecno]  INT          NOT NULL,
    [SequenceNo]           SMALLINT     NOT NULL,
    [DiagnosticSchemeCode] VARCHAR (10) NULL,
    [DiagnosisCode]        VARCHAR (50) NULL,
    [DiagnosisSiteCode]    VARCHAR (10) NULL,
    [DiagnosisSideCode]    VARCHAR (10) NULL,
    [ContextCode]          VARCHAR (10) NOT NULL,
    [Created]              DATETIME     CONSTRAINT [DF_BaseDiagnosis_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) CONSTRAINT [DF_BaseDiagnosis_ByWhom] DEFAULT (suser_name()) NOT NULL,
    [DiagnosisRecno]       INT          NOT NULL,
    CONSTRAINT [PK_BaseDiagnosis_1] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BaseDiagnosis]
    ON [AE].[BaseDiagnosis]([MergeEncounterRecno] ASC, [SequenceNo] ASC);


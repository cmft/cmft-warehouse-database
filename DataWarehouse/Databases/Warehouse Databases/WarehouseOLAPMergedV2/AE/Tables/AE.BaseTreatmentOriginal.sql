﻿CREATE TABLE [AE].[BaseTreatmentOriginal] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [TreatmentSchemeCode] VARCHAR (10)  NULL,
    [TreatmentCode]       VARCHAR (50)  NULL,
    [TreatmentTime]       SMALLDATETIME NULL,
    [ContextCode]         VARCHAR (10)  NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (50)  NULL
);


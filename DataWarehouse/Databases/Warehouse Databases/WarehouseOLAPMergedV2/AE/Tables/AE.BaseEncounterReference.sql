﻿CREATE TABLE [AE].[BaseEncounterReference] (
    [MergeEncounterRecno]      INT           NOT NULL,
    [ContextID]                INT           NOT NULL,
    [AgeID]                    INT           NULL,
    [ArrivalModeID]            INT           NULL,
    [AttendanceCategoryID]     INT           NULL,
    [AttendanceDisposalID]     INT           NULL,
    [DiagnosisFirstID]         INT           NULL,
    [DiagnosisSecondID]        INT           NULL,
    [EncounterStartDateID]     INT           NULL,
    [EncounterEndDateID]       INT           NULL,
    [EthnicOriginID]           INT           NULL,
    [IncidentLocationTypeID]   INT           NULL,
    [InvestigationFirstID]     INT           NULL,
    [InvestigationSecondID]    INT           NULL,
    [NHSNumberStatusID]        INT           NULL,
    [PatientGroupID]           INT           NULL,
    [ReferredToSpecialtyID]    INT           NULL,
    [SexID]                    INT           NULL,
    [SiteID]                   INT           NULL,
    [SourceOfReferralID]       INT           NULL,
    [StaffMemberID]            INT           NULL,
    [TreatmentFirstID]         INT           NULL,
    [TreatmentSecondID]        INT           NULL,
    [TriageCategoryID]         INT           NULL,
    [Created]                  DATETIME      NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (255) NULL,
    [AttendanceResidenceCCGID] INT           NULL,
    [CareGroupID]              INT           NULL,
    [LocationID]               INT           NULL,
    [ReligionID]               INT           NULL,
    CONSTRAINT [PK_AE_Base_Encounter_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BaseEncounterReference_ContextID]
    ON [AE].[BaseEncounterReference]([ContextID] ASC)
    INCLUDE([Updated]);


GO
CREATE NONCLUSTERED INDEX [IX_BER-AMI]
    ON [AE].[BaseEncounterReference]([ArrivalModeID] ASC)
    INCLUDE([MergeEncounterRecno], [ContextID], [AttendanceCategoryID], [AttendanceDisposalID], [EthnicOriginID], [PatientGroupID], [SiteID]);


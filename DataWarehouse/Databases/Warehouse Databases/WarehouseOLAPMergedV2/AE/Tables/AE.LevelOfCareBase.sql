﻿CREATE TABLE [AE].[LevelOfCareBase] (
    [LevelOfCareID]   INT         IDENTITY (1, 1) NOT NULL,
    [LevelOfCareCode] INT         NOT NULL,
    [LevelOfCare]     VARCHAR (6) NOT NULL,
    CONSTRAINT [PK_LevelOfCareBase] PRIMARY KEY CLUSTERED ([LevelOfCareCode] ASC)
);


﻿CREATE TABLE [AE].[BaseInvestigationConsolidated] (
    [MergeInvestigationRecno] INT           NOT NULL,
    [MergeEncounterRecno]     INT           NOT NULL,
    [SequenceNo]              SMALLINT      NOT NULL,
    [InvestigationCode]       VARCHAR (50)  NULL,
    [InvestigationTime]       SMALLDATETIME NULL,
    [ContextCode]             VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK_BaseInvestigationConsolidated] PRIMARY KEY NONCLUSTERED ([MergeInvestigationRecno] ASC, [MergeEncounterRecno] ASC, [ContextCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BaseInvestigation_SequenceNo_MergeEncounterRecno]
    ON [AE].[BaseInvestigationConsolidated]([SequenceNo] ASC, [MergeEncounterRecno] ASC)
    INCLUDE([InvestigationCode]);


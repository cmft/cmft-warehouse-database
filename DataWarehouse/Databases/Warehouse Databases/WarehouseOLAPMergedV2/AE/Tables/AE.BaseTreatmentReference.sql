﻿CREATE TABLE [AE].[BaseTreatmentReference] (
    [MergeRecno]  INT NOT NULL,
    [ContextID]   INT NOT NULL,
    [TreatmentID] INT NULL,
    CONSTRAINT [PK_BaseTreatmentReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


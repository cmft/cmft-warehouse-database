﻿CREATE view [AE].[PatientGroup] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourcePatientGroupID = SourceValueID
	,SourcePatientGroupCode = SourceValueCode
	,SourcePatientGroup = SourceValue
	,LocalPatientGroupID = LocalValueID
	,LocalPatientGroupCode = LocalValueCode
	,LocalPatientGroup = LocalValue
	,NationalPatientGroupID = NationalValueID
	,NationalPatientGroupCode = NationalValueCode
	,NationalPatientGroup = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'PATGRP'

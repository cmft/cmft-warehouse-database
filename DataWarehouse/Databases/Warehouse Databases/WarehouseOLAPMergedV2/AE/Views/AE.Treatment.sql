﻿CREATE view [AE].[Treatment] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceTreatmentID = SourceValueID
	,SourceTreatmentCode = SourceValueCode
	,SourceTreatment = SourceValue
	,LocalTreatmentID = LocalValueID
	,LocalTreatmentCode = LocalValueCode
	,LocalTreatment = LocalValue
	,NationalTreatmentID = NationalValueID
	,NationalTreatmentCode = NationalValueCode
	,NationalTreatment = NationalValue
FROM
	WH.Member
WHERE
	AttributeCode = 'AETREA'

﻿CREATE view [AE].[Diagnosis] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceDiagnosisID = SourceValueID
	,SourceDiagnosisCode = SourceValueCode
	,SourceDiagnosis = SourceValue
	,LocalDiagnosisID = LocalValueID
	,LocalDiagnosisCode = LocalValueCode
	,LocalDiagnosis = LocalValue
	,NationalDiagnosisID = NationalValueID
	,NationalDiagnosisCode = NationalValueCode
	,NationalDiagnosis = NationalValue
FROM
	WH.Member
WHERE
	AttributeCode = 'AEDIAG'

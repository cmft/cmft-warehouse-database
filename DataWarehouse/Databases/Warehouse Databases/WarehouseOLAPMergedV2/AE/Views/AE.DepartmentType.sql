﻿
CREATE view [AE].[DepartmentType]
as
select
	 DepartmentTypeID
	,DepartmentTypeCode
	,DepartmentType
	,DepartmentTypeFull
from
	AE.DepartmentTypeBase


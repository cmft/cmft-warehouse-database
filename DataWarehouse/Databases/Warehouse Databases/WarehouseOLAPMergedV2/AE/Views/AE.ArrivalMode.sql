﻿CREATE view [AE].[ArrivalMode] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceArrivalModeID = SourceValueID
	,SourceArrivalModeCode = SourceValueCode
	,SourceArrivalMode = SourceValue
	,LocalArrivalModeID = LocalValueID
	,LocalArrivalModeCode = LocalValueCode
	,LocalArrivalMode = LocalValue
	,NationalArrivalModeID = NationalValueID
	,NationalArrivalModeCode = NationalValueCode
	,NationalArrivalMode = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ARRMOD'

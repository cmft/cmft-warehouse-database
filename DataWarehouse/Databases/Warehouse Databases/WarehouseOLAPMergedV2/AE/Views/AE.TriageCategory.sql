﻿CREATE view [AE].[TriageCategory] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceTriageCategoryID = SourceValueID
	,SourceTriageCategoryCode = SourceValueCode
	,SourceTriageCategory = SourceValue
	,LocalTriageCategoryID = LocalValueID
	,LocalTriageCategoryCode = LocalValueCode
	,LocalTriageCategory = LocalValue
	,NationalTriageCategoryID = NationalValueID
	,NationalTriageCategoryCode = NationalValueCode
	,NationalTriageCategory = NationalValue
FROM
	WH.Member
where 
	AttributeCode = 'TRICAT'

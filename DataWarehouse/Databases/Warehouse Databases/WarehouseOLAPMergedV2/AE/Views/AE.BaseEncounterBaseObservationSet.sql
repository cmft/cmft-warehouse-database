﻿

CREATE view [AE].[BaseEncounterBaseObservationSet] as 

--Uses the link table already in classic and translates the recnos to merge recnos

select
	BaseEncounter.MergeEncounterRecno
	,BaseObservationSet.MergeObservationSetRecno
from
	[$(Warehouse)].AE.EncounterObservationSet

inner join AE.BaseEncounter
on	EncounterObservationSet.EncounterRecno = BaseEncounter.EncounterRecno
and	BaseEncounter.ContextCode = 'CEN||SYM'

inner join Observation.BaseObservationSet
on	EncounterObservationSet.ObservationSetRecno = BaseObservationSet.ObservationSetRecno


﻿CREATE view [AE].[AttendanceCategory] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAttendanceCategoryID = SourceValueID
	,SourceAttendanceCategoryCode = SourceValueCode
	,SourceAttendanceCategory = SourceValue
	,LocalAttendanceCategoryID = LocalValueID
	,LocalAttendanceCategoryCode = LocalValueCode
	,LocalAttendanceCategory = LocalValue
	,NationalAttendanceCategoryID = NationalValueID
	,NationalAttendanceCategoryCode = NationalValueCode
	,NationalAttendanceCategory = NationalValue

	,AttendanceCategoryGroup =
		case NationalValueCode
		when '2' then 'Clinic Attender'
		else 'Attender'
		end
FROM
	WH.Member
where
	AttributeCode = 'ATTCAT'

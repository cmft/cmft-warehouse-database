﻿

CREATE view [AE].[BreachStatus] as

select
	 BreachStatusID
	,BreachStatusCode
	,BreachStatus
	,DischargeStatus
from
	[$(Warehouse)].AE.BreachStatus



﻿






CREATE VIEW [AE].[DischargeSummary] as

select
	DischargeSummaryRecno
	,AttendanceNumber
	,PatientName
	,DistrictNo
	,NHSNumber
	,DateOfBirth
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ArrivalDate
	,ArrivalTime
	,DepartureTime
	,ClinicianSeen
	,InformationForGP
	,TargetTime = dateadd(hour, 24, DepartureTime)
	,ActualTime = dateadd(hour, datediff(hour, DepartureTime, getdate()), DepartureTime)
	,Breach = 
			cast(
				case
				when dateadd(hour, datediff(hour, DepartureTime, getdate()), DepartureTime) > 	dateadd(hour, 24, DepartureTime)
				and EDTSendTime is null 
				then 1
				else 0
				end
				as bit)
	,DischargeSummaryXML = 
				cast(
					(
					select
						(
						select
							DischargedByStaffMember
							,DepartureTime 
							,AttendanceNumber
						for xml path('Header'), type
						)
						,
						(
						select
							PatientName
							,PatientAddressLine1
							,PatientAddressLine2
							,PatientAddressLine3
							,PatientAddressLine4
							,PatientPostcode
							,DateOfBirth
							,NHSNumber
							,HospitalNumber
							,ArrivalDate
							,ArrivalTime
							,ArrivalMode
							,SourceOfArrival
							,AccompaniedBy
							,PreviousAttendances
							,ReferredBy
							,SchoolAttended
						for xml path('PatientDetails'), type
						)
						,
						(
						select
							RegisteredGpCode
							,RegisteredGp
							,RegisteredGpPracticeCode
							,RegisteredGpAddressLine1
							,RegisteredGpAddressLine2
							,RegisteredGpAddressLine3
							,RegisteredGpAddressLine4
							,RegisteredGpAddressPostcode
						for xml path('GPDetails'), type
						)
						,
						(
						select
							PresentingCondition
							,TriagedBy
							,BriefHistory
							,TriageComments
							,AllergyOrSensitivity
						for xml path('TriageInformation'), type
						)
						,
						(
						select
							InitialObservationsPulse
							,InitialObservationsSystolicBP
							,InitialObservationsDiastolicBP
							,InitialObservationsO2Saturation
							,InitialObservationsRespiratoryRate
							,InitialObservationsGCS
							,InitialObservationsEWS
							,InitialObservationsTemperature
							,InitialObservationsPainScoreAtRest
							,InitialObservationsPainScoreOnMovement
							,InitialObservationsBloodSugar
							,InitialObservationsFractionOfInspiredOxygen
							,InitialObservationsPerformPeakFlow
							,InitialObservationsCapRefill
						for xml path('InitialObservation'), type
						)
						,
						(
						select
						  FinalObservationsPulse
						  ,FinalObservationsSystolicBP
						  ,FinalObservationsDiastolicBP
						  ,FinalObservationsO2Saturation
						  ,FinalObservationsRespiratoryRate
						  ,FinalObservationsGCS
						  ,FinalObservationsEWS
						for xml path('FinalObservation'), type
						)
						,
						(
						select
							DutyConsultant
							,ClinicianSeen
							,Diagnosis01
							,Diagnosis02
							,DepartmentalInvestigation
							,DepartmentalInvestigationDetails
							,Investigation01
							,Investigation02
							,
							(  
							select  
								ResultType = 
										case	
										when charindex('(CEN||ICE', SourceResult) = 0 
										then SourceResult
										else left(SourceResult, charindex('(CEN||ICE', SourceResult))
										end
								,Result
							FROM
								Result.ReportableResult
								
							inner join Result.BaseResultReference
							on	BaseResultReference.MergeResultRecno = ReportableResult.MergeResultRecno

							inner join Result.Result
							on  Result.SourceResultID = ReportableResult.ResultID

							where
								DischargeSummary.DistrictNo = ReportableResult.DistrictNo	
							and	ReportableResult.EffectiveTime between DischargeSummary.ArrivalDate and DischargeSummary.DepartureTime 			
							and ReportableResult.ContextCode = 'CEN||ICE'

							for xml path('Results'), type
							)
							,Treatment
							,TreatmentDetails
							,MedicationGiven
						for xml path('ClinicalData'), type
						)
						,
						(
						select
						  InformationForGP
						for xml path('InformationForGp'), type
						)
						,
						(
						select
						  AuditCScore
						for xml path('AuditC'), type
						)
						,
						(
						select
						  FollowUpAdvice
						  ,FollowUpArrangement
						  ,FollowUpTime
						for xml path('FollowUpInformation'), type
						)
						,
						(
						select
						  DischargeOutcome
						  ,DischargeOutcomeDetails
						  ,DischargeDestination

						,(
						select
							(
							select
								Drug = ReferenceLookup.ReferenceLookup
							from
								[$(Warehouse)].SymphonyStaging.AttendanceExtraField

							cross apply [$(Warehouse)].dbo.Split(AttendanceExtraField.FieldValue, '||') Medications

							left join [$(Warehouse)].SymphonyStaging.ReferenceLookup
							on	ReferenceLookup.ReferenceLookupID = Medications.Value

							where
								AttendanceExtraField.AttendanceID = DischargeSummary.AESourceUniqueID
							and	AttendanceExtraField.FieldID in 
								(
								 1472 --Adult Drugs
								,1604 --EEC Drugs
								,1213 --Paediatric Drugs
								)

							for xml path(''), type
							)
						for xml path('DischargeMedicationGiven'), type
						)

						for xml path('DischargeDetails'), type
						)
					from
						[$(Warehouse)].AE.DischargeSummary DischargeSummaryXML
					where
						DischargeSummaryXML.DischargeSummaryRecno = DischargeSummary.DischargeSummaryRecno
					for xml path ('EmergencyDepartmentDischarge')
						,root ('EmergencyDepartmentDischargeSummary')
						--,elements xsinil
					) 
					as xml)
	,Updated
from
	[$(Warehouse)].AE.DischargeSummary






﻿create view AE.APCFactEncounter as

with AdmissionDischargeMetric
	(
	 MetricID
	)
as
	(
	select
		 MetricID
	from
		APC.Metric
	where
		MetricCode in ('ADM', 'DIS')

	union all

	select
		 Metric.MetricID
	from
		APC.Metric

	inner join AdmissionDischargeMetric
	on	AdmissionDischargeMetric.MetricID = Metric.MetricParentID

	)

select
	 FactEncounter.MetricID
	,DirectorateID = FactEncounter.DirectorateID
	,FactEncounter.SiteID
	,FactEncounter.EncounterDateID
	,FactEncounter.Cases
from
	AdmissionDischargeMetric

inner join APC.FactEncounter
on	FactEncounter.MetricID = AdmissionDischargeMetric.MetricID

where
	not exists
	(
	select
		1
	from
		APC.Metric Parent
	where
		Parent.MetricParentID = AdmissionDischargeMetric.MetricID
	)


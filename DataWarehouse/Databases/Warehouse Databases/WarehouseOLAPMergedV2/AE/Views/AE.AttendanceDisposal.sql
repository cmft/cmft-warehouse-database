﻿CREATE view [AE].[AttendanceDisposal] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceAttendanceDisposalID = SourceValueID
	,SourceAttendanceDisposalCode = SourceValueCode
	,SourceAttendanceDisposal = SourceValue
	,LocalAttendanceDisposalID = LocalValueID
	,LocalAttendanceDisposalCode = LocalValueCode
	,LocalAttendanceDisposal = LocalValue
	,NationalAttendanceDisposalID = NationalValueID
	,NationalAttendanceDisposalCode = NationalValueCode
	,NationalAttendanceDisposal = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'ATTDIS'
and not
	(
		SourceValueID in (130572 , 130577)
	and	LocalValueID in (130329 , 130330)
	)

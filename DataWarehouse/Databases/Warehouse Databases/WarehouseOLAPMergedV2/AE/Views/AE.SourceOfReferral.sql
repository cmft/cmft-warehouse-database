﻿CREATE view [AE].[SourceOfReferral] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceSourceOfReferralID = SourceValueID
	,SourceSourceOfReferralCode = SourceValueCode
	,SourceSourceOfReferral = SourceValue
	,LocalSourceOfReferralID = LocalValueID
	,LocalSourceOfReferralCode = LocalValueCode
	,LocalSourceOfReferral = LocalValue
	,NationalSourceOfReferralID = NationalValueID
	,NationalSourceOfReferralCode = NationalValueCode
	,NationalSourceOfReferral = NationalValue
FROM
	WH.Member
where 
	AttributeCode = 'SCRFAE'

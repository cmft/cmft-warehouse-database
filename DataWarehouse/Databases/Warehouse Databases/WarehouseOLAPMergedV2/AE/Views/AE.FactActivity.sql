﻿


CREATE view [AE].[FactActivity] as

--	20150519	RR	added HRG allocation for PCEC and WIC, brought this through below
--	20150722	RR	HRG has not been running for the past week, therefore inner join to grouper has no activity since 12/07/2015.  Discussed with GS and PH, changed to left join and added coalesce UZ01Z for HRG.  This may need to come out once HRG grouper issue is resolved.
--	20151104	CH	added Directorate allocation for Directorate for Warehouse Activity. For teh SLAM side it appears the code has changed form 'ADAS' to 'PCEC' Site_Code for PCEC data, this is now again 90 Emergency Services (Same as MRI)

select
	Attendance.MergeEncounterRecno
	,EncounterDateID = Attendance.EncounterStartDateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'A&E'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) <= 2
						then 1
						else 0
						end 
						as bit)
	,Metric.MetricID
	,PointOfDeliveryID = PointOfDelivery.AllocationID 
	,ContractFlagID = 
				(
				select
					AllocationID
				from
					Allocation.Allocation 
				where
					SourceAllocationID = '-1'
				and	AllocationTypeID = 9
				)
	,Directorate.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Reportable = cast(Attendance.Reportable as bit)
	,Attendance.ContextID
	,Cases = 1
	,Value = null
from
	AE.FactEncounter Attendance

inner join AE.Stage
on	Stage.StageID = Attendance.StageID
and	Stage.StageCode = 'INDEPARTMENTADJUSTEDNATIONAL' --In Department (Adjusted for national submissions)

inner join WH.Site
on	Site.SourceSiteID = Attendance.SiteID

inner join Allocation.DatasetAllocation DirectorateDatasetAllocation
on	Attendance.MergeEncounterRecno = DirectorateDatasetAllocation.DatasetRecno
and DirectorateDatasetAllocation.AllocationTypeID = 10
and DirectorateDatasetAllocation.DatasetCode = 'AE'

inner join Allocation.Allocation DirectorateAllocation
on	DirectorateDatasetAllocation.AllocationID = DirectorateAllocation.AllocationID

inner join WH.Directorate
on	DirectorateAllocation.SourceAllocationID = Directorate.DirectorateCode

--old logic replaced with the above as we allocate division CH 20151104
--inner join WH.Directorate
--on	Directorate.DirectorateCode =
--		case Site.NationalSiteCode
--		when 'RW3TR' then '80' --Trafford
--		when 'RM401' then '80' --Trafford
--		when 'RM403' then '80' --Trafford		
--		when 'RW3MR' then '90' --MRI
--		when 'RW3SM' then '30' --St Marys
--		when 'RW3RC' then  '0' --Childrens
--		when 'RW3DH' then '50' --Dental
--		when 'RW3RE' then '40' --Eye
--		when 'ADAS'  then 'N/A' --N/A
--		else 'N/A'
--		end

inner join AE.BaseEncounter Base
on	Base.MergeEncounterRecno = Attendance.MergeEncounterRecno

inner join WH.Specialty
on	Specialty.SourceContextCode = Base.ContextCode
and	Specialty.SourceSpecialtyCode = '180'

inner join AE.AttendanceCategory
on	AttendanceCategory.SourceAttendanceCategoryID = Attendance.AttendanceCategoryID

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 
		case
		when AttendanceCategory.AttendanceCategoryGroup = 'Attender'
		then 'FAE'
		else 'RAE'
		end

inner join Allocation.DatasetAllocation PointOfDelivery
on	PointOfDelivery.DatasetRecno = Attendance.MergeEncounterRecno
and	PointOfDelivery.DatasetCode = 'AE'
and	PointOfDelivery.AllocationTypeID = 5

inner join WH.ActivitySource
on ActivitySource.Source = 'Warehouse'

inner join WH.Calendar
on	Attendance.EncounterStartDateID = Calendar.DateID

--	20150519	RR

--inner join WH.HRGBase 
--on HRGBase.HRGCode = coalesce(HRG4Encounter.HRGCode, 'N/A')

inner join Allocation.DatasetAllocation HRGAllocation
on	HRGAllocation.DatasetRecno = Attendance.MergeEncounterRecno
and	HRGAllocation.DatasetCode = 'AE'
and	HRGAllocation.AllocationTypeID = 6

inner join Allocation.Allocation HRG
on	HRG.AllocationID = HRGAllocation.AllocationID
and HRG.AllocationTypeID = HRGAllocation.AllocationTypeID

left join AE.HRG4Encounter -- need hrgs in fact main tables!
on HRG4Encounter.MergeEncounterRecno = Attendance.MergeEncounterRecno

inner join WH.HRGBase
on	HRGBase.HRGCode = 
	case 
	when HRG.SourceAllocationID in ('WIC','PCEC') then HRG.SourceAllocationID
	else coalesce(HRG4Encounter.HRGCode, 'N/A')
	end

where
	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')
and Base.Reportable = 1 -- to be consistent with the APC method, so the reportable flag is set to 0 if duplicate that should not be present in model

union all

select
	Attendance.Contracting_ID
	,EncounterDateID = Calendar.DateID
	,SourceID = ActivitySource.SourceID
	,SourceDatasetCode = 'A&E'
	,ConsolidatedView = 
					cast(
						case
						when datediff(month, Calendar.FirstDayOfMonth, getdate()) > 2
						then 1
						else 0
						end 
						as bit)
	,Metric.MetricID
	,PointOfDeliveryID = 
						coalesce(
								PointOfDelivery.AllocationID 
								,
								(
								select
									AllocationID
								from
									Allocation.Allocation 
								where
									SourceAllocationID = '-1'
								and	AllocationTypeID = 5
								)
							)
	,ContractFlagID = 
				(
				select
					AllocationID
				from
					Allocation.Allocation 
				where
					SourceAllocationID = '-1'
				and	AllocationTypeID = 9
				)
	,Directorate.DirectorateID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,ServiceID = 
				(
				select
					ServiceID
				from
					WH.Service 
				where
					Service = 'Unassigned'
				)
	,HRGID = HRGBase.HRGID
	,Reportable = cast(1 as bit)
	,Context.ContextID
	,Cases = Attendance.Activity_Actual
	,Value = Attendance.Price_Actual
from
	SLAM.Encounter Attendance

inner join WH.Site
on	Site.SourceContextCode = Attendance.ContextCode
and	Site.SourceSiteCode = '-1'

inner join WH.Directorate
on	Directorate.DirectorateCode =
		case Attendance.Site_Code
		when 'RW3TR' then '80' --Trafford
		when 'RW3MR' then '90' --MRI
		when 'RW3SM' then '30' --St Marys
		when 'RW3RC' then  '0' --Childrens
		when 'RW3DH' then '50' --Dental
		when 'RW3RE' then '40' --Eye
		--when 'ADAS'  then 'N/A' --N/A
		when 'PCEC'  then '90' --PCEC
		else 'N/A'
		end

inner join WH.Specialty
on	Specialty.SourceContextCode = Attendance.ContextCode
and	Specialty.SourceSpecialtyCode = '180'

inner join WH.ActivityMetric Metric
on	Metric.MetricCode = 
		case
		when Attendance.PoD_Code = 'AandEFA'
		then 'FAE'
		else 'RAE'
		end

inner join Allocation.Allocation PointOfDelivery 
on	PointOfDelivery.SourceAllocationID = 
										case
										when Attendance.PoD_Code = 'AandEFA'
										then 'AEFA'
										else 'AEFUP'
										end
and	PointOfDelivery.AllocationTypeID = 5

inner join WH.ActivitySource
on ActivitySource.Source = 'SLAM'

inner join WH.Calendar
on	Attendance.Spell_Admission_Date = Calendar.TheDate

inner join WH.Context
on	Attendance.ContextCode = Context.ContextCode

inner join WH.HRGBase
on	HRGBase.HRGCode = coalesce(Attendance.HRG_Code, 'N/A')

--inner join 
--	(
--	select
--		Extract_Month = max(Extract_Month)
--		,Feed_Name
--		,Extract_Type
--		,Extract_Year
--	from
--		SLAM.Encounter
--	group by
--		Feed_Name
--		,Extract_Type
--		,Extract_Year
--	) LatestExtractMonth
--on  LatestExtractMonth.Feed_Name = Attendance.Feed_Name
--and LatestExtractMonth.Extract_Type = Attendance.Extract_Type
--and LatestExtractMonth.Extract_Month = Attendance.Extract_Month
--and LatestExtractMonth.Extract_Year = Attendance.Extract_Year

where
	Attendance.Feed_Name = 'AAndE'
and	Attendance.Extract_Type = 'Freeze'
--and	Attendance.Is_Current = 1
and	Attendance.Latest_In_Year = 1
and	Calendar.TheDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')













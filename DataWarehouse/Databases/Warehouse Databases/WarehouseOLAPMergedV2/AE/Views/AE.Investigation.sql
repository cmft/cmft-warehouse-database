﻿


CREATE view [AE].[Investigation] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceInvestigationID = SourceValueID
	,SourceInvestigationCode = SourceValueCode
	,SourceInvestigation = SourceValue
	,LocalInvestigationID = LocalValueID
	,LocalInvestigationCode = LocalValueCode
	,LocalInvestigation = LocalValue
	,NationalInvestigationID = NationalValueID
	,NationalInvestigationCode = NationalValueCode
	,NationalInvestigation = NationalValue
FROM
	WH.Member
WHERE
	AttributeCode = 'AEINVE'
	
union all

SELECT
	 SourceContextCode
	,SourceContext
	,SourceInvestigationID = SourceValueID
	,SourceInvestigationCode = SourceValueCode
	,SourceInvestigation = SourceValue
	,LocalInvestigationID = LocalValueID
	,LocalInvestigationCode = LocalValueCode
	,LocalInvestigation = LocalValue
	,NationalInvestigationID = NationalValueID
	,NationalInvestigationCode = NationalValueCode
	,NationalInvestigation = NationalValue
FROM
	WH.Member
	
WHERE
	AttributeCode = 'TEST'
and	SourceContextCode = 'CEN||ICE'




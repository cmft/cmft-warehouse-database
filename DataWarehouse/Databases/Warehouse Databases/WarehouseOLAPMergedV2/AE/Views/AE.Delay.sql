﻿

CREATE view [AE].[Delay] as

select
	 DelayCode = Row
	,Delay =
		case Row
		when 1 then 'Primary Delay'
		else 'Secondary Delay ' + convert(varchar, Row-1)
		end
from
	(
	select
		Row =
			convert(
				 int
				,ROW_NUMBER() OVER(ORDER BY StageCode)
			)

	from
		AE.Stage
	) Stage



﻿CREATE view [AE].[IncidentLocationType] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceIncidentLocationTypeID = SourceValueID
	,SourceIncidentLocationTypeCode = SourceValueCode
	,SourceIncidentLocationType = SourceValue
	,LocalIncidentLocationTypeID = LocalValueID
	,LocalIncidentLocationTypeCode = LocalValueCode
	,LocalIncidentLocationType = LocalValue
	,NationalIncidentLocationTypeID = NationalValueID
	,NationalIncidentLocationTypeCode = NationalValueCode
	,NationalIncidentLocationType = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'INCLOC'

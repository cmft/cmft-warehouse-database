﻿CREATE view [AE].[StaffMember] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceStaffMemberID = SourceValueID
	,SourceStaffMemberCode = SourceValueCode
	,SourceStaffMember = SourceValue
	,LocalStaffMemberID = LocalValueID
	,LocalStaffMemberCode = LocalValueCode
	,LocalStaffMember = LocalValue
	,NationalStaffMemberID = NationalValueID
	,NationalStaffMemberCode = NationalValueCode
	,NationalStaffMember = NationalValue
FROM
	WH.Member
where 
	AttributeCode = 'STAFFM'

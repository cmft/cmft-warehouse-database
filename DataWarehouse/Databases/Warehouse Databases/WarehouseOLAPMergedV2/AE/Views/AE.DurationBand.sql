﻿
CREATE view [AE].[DurationBand] AS

SELECT
	 DurationBandCode
	,MinuteBand
	,FifteenMinuteBandCode
	,FifteenMinuteBand
	,ThirtyMinuteBandCode
	,ThirtyMinuteBand
	,HourBandCode
	,HourBand
	,BreachFifteenMinuteBandCode
	,BreachFifteenMinuteBand
	,BreachThirtyMinuteBandCode
	,BreachThirtyMinuteBand
	,BreachHourBandCode
	,BreachHourBand
	,BreachHourBandGroupCode
	,BreachHourBandGroup
from
	AE.DurationBandBase

union

select
	 -2
	,'N/A'
	,-2
	,'N/A'
	,-2
	,'N/A'
	,-2
   	,'N/A'
	,-2
	,'N/A'
	,-2
	,'N/A'
	,-2
   	,'N/A'
   	,-2
   	,'N/A'


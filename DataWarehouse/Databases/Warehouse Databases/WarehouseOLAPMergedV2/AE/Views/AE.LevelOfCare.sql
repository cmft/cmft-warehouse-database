﻿
CREATE view [AE].[LevelOfCare] as

select
	 LevelOfCareID
	,LevelOfCareCode
	,LevelOfCare
from
	AE.LevelOfCareBase


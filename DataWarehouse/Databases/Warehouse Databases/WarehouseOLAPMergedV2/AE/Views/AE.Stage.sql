﻿
CREATE view [AE].[Stage] as

select
	 StageID
	,StageCode
	,Stage
	,BreachValue
from
	[$(Warehouse)].AE.StageBase


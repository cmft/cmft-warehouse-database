﻿



CREATE view [AE].[CareGroup] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceCareGroupID = SourceValueID
	,SourceCareGroupCode = SourceValueCode
	,SourceCareGroup = SourceValue
	,LocalCareGroupID = LocalValueID
	,LocalCareGroupCode = LocalValueCode
	,LocalCareGroup = CareGroup.CareGroup
	,NationalCareGroupID = NationalValueID
	,NationalCareGroupCode = NationalValueCode
	,NationalCareGroup = NationalValue

	,CareGroup.CareGroupTypeCode
	,CareGroupType.CareGroupType
FROM
	WH.Member

left join [$(Warehouse)].AE.CareGroup
on	cast(CareGroup.CareGroupCode as varchar) = Member.LocalValueCode

left join [$(Warehouse)].AE.CareGroupType
on	cast(CareGroupType.CareGroupTypeCode as varchar) = CareGroup.CareGroupTypeCode

where 
	AttributeCode = 'CAREGROUP'






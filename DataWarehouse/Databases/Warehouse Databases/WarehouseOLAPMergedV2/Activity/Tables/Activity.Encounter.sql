﻿CREATE TABLE [Activity].[Encounter] (
    [EncounterRecno]              INT           IDENTITY (1, 1) NOT NULL,
    [SourceRecno]                 INT           NOT NULL,
    [SourceSystem]                VARCHAR (10)  NOT NULL,
    [SourceDatasetCode]           VARCHAR (10)  NOT NULL,
    [Site]                        VARCHAR (255) NULL,
    [DepartmentType]              VARCHAR (255) NULL,
    [AttendanceCategory]          VARCHAR (255) NULL,
    [AttendanceNumber]            VARCHAR (255) NULL,
    [PatientForename]             VARCHAR (255) NULL,
    [PatientSurname]              VARCHAR (255) NULL,
    [ArrivalDate]                 DATE          NULL,
    [ArrivalTime]                 SMALLDATETIME NULL,
    [LeftDepartmentDate]          DATE          NULL,
    [LeftDepartmentTime]          SMALLDATETIME NULL,
    [LOS]                         INT           NULL,
    [StaffMember]                 VARCHAR (255) NULL,
    [AppointmentType]             VARCHAR (255) NULL,
    [AppointmentDate]             DATE          NULL,
    [AppointmentTime]             SMALLDATETIME NULL,
    [CasenoteNumber]              VARCHAR (255) NULL,
    [SourceConsultant]            VARCHAR (255) NULL,
    [Clinic]                      VARCHAR (255) NULL,
    [ProcedureCode1]              VARCHAR (255) NULL,
    [ProcedureCode2]              VARCHAR (255) NULL,
    [ProcedureCode3]              VARCHAR (255) NULL,
    [ProcedureCode4]              VARCHAR (255) NULL,
    [ProcedureCode5]              VARCHAR (255) NULL,
    [Outcome]                     VARCHAR (255) NULL,
    [AdmissionDate]               DATE          NULL,
    [DischargeDate]               DATE          NULL,
    [SourcePatientClassification] VARCHAR (255) NULL,
    [StartWardTypeCode]           VARCHAR (255) NULL,
    [PrimaryProcedureCode]        VARCHAR (255) NULL,
    [PrimaryDiagnosis]            VARCHAR (255) NULL,
    [PathwayType]                 VARCHAR (255) NULL,
    [PathwayStartDate]            DATE          NULL,
    [PathwayStartTime]            SMALLDATETIME NULL,
    [NHSNumber]                   VARCHAR (20)  NULL,
    [DistrictNo]                  VARCHAR (20)  NULL,
    CONSTRAINT [PK_Encounter] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Encounter_MergeEncounterRecno_SourceSystem_SourceDatasetCode]
    ON [Activity].[Encounter]([SourceRecno] ASC, [SourceSystem] ASC, [SourceDatasetCode] ASC)
    INCLUDE([EncounterRecno]);


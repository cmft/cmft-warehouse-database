﻿
CREATE procedure [Activity].[BuildEncounter] as

declare @EarliestSLAMAdmissionDate date
select	@EarliestSLAMAdmissionDate = 
									(
									select
										min(Spell_Admission_Date)
									from
										SLAM.Encounter
									)
									
truncate table Activity.Encounter

--A&E
insert
into
	Activity.Encounter
(
	 SourceRecno
	,SourceSystem
	--,SourceMetric
	,SourceDatasetCode
	,Site
	,DepartmentType
	,AttendanceCategory
	,AttendanceNumber
	,PatientForename
	,PatientSurname
	,ArrivalDate
	,ArrivalTime
	,LeftDepartmentDate
	,LeftDepartmentTime
	,LOS
	,StaffMember
	,NHSNumber
	,DistrictNo
)
select
	 SourceRecno = Attendance.MergeEncounterRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'A&E'
	,SourceDatasetCode = 'A&E'

	,Site = Site.NationalSite
	,DepartmentType = DepartmentType.DepartmentType
	,AttendanceCategory = AttendanceCategory.SourceAttendanceCategory
	,Attendance.AttendanceNumber
	,Attendance.PatientForename
	,Attendance.PatientSurname
	,ArrivalDate = cast(Attendance.ArrivalDate as date)
	,Attendance.ArrivalTime

	,LeftDepartmentDate =
		cast(
			coalesce(
				 Attendance.PCDULeftDepartmentTime
				,Attendance.CDULeftDepartmentTime
				,Attendance.DepartureTimeAdjusted --adjusted for Trafford conclusion time and CMFT where disposal code is 2 and 3
			)
			as date
		)

	,LeftDepartmentTime =
		coalesce(
			 Attendance.PCDULeftDepartmentTime
			,Attendance.CDULeftDepartmentTime
			,Attendance.DepartureTimeAdjusted --adjusted for Trafford conclusion time and CMFT where disposal code is 2 and 3
		)

	,LOS = FactEncounter.StageDurationMinutes
	,StaffMember = StaffMember.SourceStaffMember
	,NHSNumber = Attendance.NHSNumber
	,DistrictNo = Attendance.DistrictNo

from
	AE.BaseEncounter Attendance

inner join AE.FactEncounter
on	FactEncounter.MergeEncounterRecno = Attendance.MergeEncounterRecno

inner join AE.Stage
on	Stage.StageID = FactEncounter.StageID
and	Stage.StageCode = 'INDEPARTMENTADJUSTEDNATIONAL' --In Department (Adjusted for national submissions)

inner join WH.Site
on	Site.SourceSiteID = FactEncounter.SiteID

inner join AE.DepartmentType
on	DepartmentType.DepartmentTypeID = FactEncounter.DepartmentTypeID

inner join AE.AttendanceCategory
on	AttendanceCategory.SourceAttendanceCategoryID = FactEncounter.AttendanceCategoryID

left join AE.StaffMember
on	StaffMember.SourceStaffMemberCode = Attendance.StaffMemberCode
and	StaffMember.SourceContextCode = Attendance.ContextCode

where
	ArrivalDate >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 SourceRecno = Attendance.Contracting_ID
	,SourceSystem = 'SLAM'
	--,SourceMetric = 'A&E'
	,SourceDatasetCode = 'A&E'
	,Site = Site.Site
	,DepartmentType = null
	,AttendanceCategory = null
	,AttendanceNumber = null
	,PatientForename = Attendance.Forename
	,PatientSurname = Attendance.Surname
	,ArrivalDate = cast(Attendance.Spell_Admission_Date as date)
	,ArrivalTime = null
	,LeftDepartmentDate = null
	,LeftDepartmentTime = null
	,LOS = Attendance.LoS
	,StaffMember = null
	,NHSNumber = NHS_Number
	,DistrictNo = District_Number

from
	SLAM.Encounter Attendance

left join 
	(
	select distinct
		 SiteCode = Site.NationalSiteCode
		,Site = Site.NationalSite
	from
		WH.Site
	) Site
	on	Site.SiteCode = Attendance.Site_Code

where
	Attendance.Feed_Name = 'AAndE'
and	Attendance.Extract_Type = 'Freeze'
--and	Attendance.Is_Current = 1
and	Attendance.Latest_In_Year = 1
and cast(Attendance.Spell_Admission_Date as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')


--Outpatient
insert
into
	Activity.Encounter
(
	 SourceRecno
	,SourceSystem
	--,SourceMetric
	,SourceDatasetCode
	,Site
	,AppointmentType
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,AppointmentDate
	,AppointmentTime
	,SourceConsultant
	,Clinic
	,PrimaryProcedureCode
	,ProcedureCode1
	,ProcedureCode2
	,ProcedureCode3
	,ProcedureCode4
	,ProcedureCode5
	,Outcome
	,NHSNumber
	,DistrictNo
)
select
	 SourceRecno = Appointment.MergeEncounterRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'OP'
	,SourceDatasetCode = 'OP'

	,Site = Site.NationalSite
	,AppointmentType = AppointmentType.SourceAppointmentType
	,Appointment.CasenoteNo
	,Appointment.PatientForename
	,Appointment.PatientSurname
	,AppointmentDate = cast(Appointment.AppointmentDate as date)
	,Appointment.AppointmentTime
	,Consultant = Consultant.SourceConsultant
	,Clinic = Clinic.SourceClinic
	,PrimaryProcedureCode = Appointment.PrimaryOperationCode
	,ProcedureCode1 = Appointment.SecondaryProcedureCode1
	,ProcedureCode2 = Appointment.SecondaryProcedureCode2
	,ProcedureCode3 = Appointment.SecondaryProcedureCode3
	,ProcedureCode4 = Appointment.SecondaryProcedureCode4
	,ProcedureCode5 = Appointment.SecondaryProcedureCode5
	,AttendanceOutcome = AttendanceOutcome.SourceAttendanceOutcome
	,NHSNumber = Appointment.NHSNumber
	,DistrictNo = Appointment.DistrictNo

from
	OP.BaseEncounter Appointment

inner join OP.FactEncounter
on	FactEncounter.MergeEncounterRecno = Appointment.MergeEncounterRecno


inner join OP.Metric Metric
on	Metric.MetricID = FactEncounter.MetricID
--appointments
and	Metric.MetricCode in
	(
	 'FATT'
	,'RATT'
	)

inner join WH.Site
on	Site.SourceSiteID = FactEncounter.SiteID

left join OP.AppointmentType
on	AppointmentType.SourceAppointmentTypeCode = Appointment.AppointmentTypeCode
and	AppointmentType.SourceContextCode = Appointment.ContextCode

inner join WH.Consultant
on	Consultant.SourceConsultantID = FactEncounter.ConsultantID

inner join OP.Clinic
on	Clinic.SourceClinicID = FactEncounter.ClinicID

inner join OP.AttendanceOutcome
on	AttendanceOutcome.SourceAttendanceOutcomeID = FactEncounter.AttendanceOutcomeID

where
	cast(Appointment.AppointmentDate as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 SourceRecno = BaseEncounter.MergeEncounterRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'OP'
	,SourceDatasetCode = 'GUM'

	,Site = null
	,AppointmentType = AppointmentType.SourceAppointmentType
	,CasenoteNo = null
	,PatientForename = null
	,PatientSurname = null
	,AppointmentDate = cast(BaseEncounter.AppointmentDate as date)
	,AppointmentTime = null
	,Consultant = null
	,Clinic = Clinic.SourceClinic
	,PrimaryProcedureCode = null
	,ProcedureCode1 = null
	,ProcedureCode2 = null
	,ProcedureCode3 = null
	,ProcedureCode4 = null
	,ProcedureCode5 = null
	,AttendanceOutcome = null
	,NHSNumber = null
	,DistrictNo = null


from
	GUM.BaseEncounter

inner join GUM.BaseEncounterReference
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join OP.AppointmentType
on	AppointmentType.SourceAppointmentTypeID = BaseEncounterReference.AppointmentTypeID

inner join OP.Clinic
on	Clinic.SourceClinicID = BaseEncounterReference.ClinicID

where
	cast(BaseEncounter.AppointmentDate as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 SourceRecno = BaseEncounter.MergeEncounterRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'OP'
	,SourceDatasetCode = 'ANTI'

	,Site = null
	,AppointmentType = null
	,CasenoteNo = null
	,PatientForename = PatientForename
	,PatientSurname = PatientSurname
	,AppointmentDate = cast(BaseEncounter.TestDate as date)
	,AppointmentTime = null
	,Consultant = null
	,Clinic = Clinic.SourceClinic
	,PrimaryProcedureCode = null
	,ProcedureCode1 = null
	,ProcedureCode2 = null
	,ProcedureCode3 = null
	,ProcedureCode4 = null
	,ProcedureCode5 = null
	,AttendanceOutcome = null
	,NHSNumber = null
	,DistrictNo = null
from
	Anticoagulation.BaseEncounter

inner join Anticoagulation.BaseEncounterReference
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join OP.Clinic
on	Clinic.SourceClinicID = BaseEncounterReference.ClinicID

where
	cast(BaseEncounter.TestDate as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 SourceRecno = BaseFertility.MergeRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'OP'
	,SourceDatasetCode = 'Fertility'

	,Site = null
	,AppointmentType = null
	,CasenoteNo = null
	,PatientForename 
	,PatientSurname 
	,AppointmentDate = cast(BaseFertility.EncounterDate as date)
	,AppointmentTime = null
	,Consultant = null
	,Clinic = TreatmentType
	,PrimaryProcedureCode = null
	,ProcedureCode1 = null
	,ProcedureCode2 = null
	,ProcedureCode3 = null
	,ProcedureCode4 = null
	,ProcedureCode5 = null
	,AttendanceOutcome = null
	,NHSNumber = null
	,DistrictNo = null
from
	OP.BaseFertility

inner join OP.BaseFertilityReference
on	BaseFertility.MergeRecno = BaseFertilityReference.MergeRecno

where
	cast(BaseFertility.EncounterDate as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')
and BaseFertility.EncounterDate is not null
and coalesce(BaseFertility.ReferralSource,'-1') not like 'Self%'
and coalesce(BaseFertility.TreatmentType,'-1') <> 'FET'

union all

select
	 SourceRecno = Appointment.Contracting_ID
	,SourceSystem = 'SLAM'
	--,SourceMetric = 'OP'
	,SourceDatasetCode = 'OP'

	,Site = Site.Site
	,AppointmentType = null
	,Appointment.Casenote_Number
	,PatientForename = Appointment.Forename
	,PatientSurname = Appointment.Surname
	,AppointmentDate = cast(Appointment.Spell_Admission_Date as date)
	,AppointmentTime = null
	,Consultant = Consultant.Consultant
	,Clinic = Appointment.Clinic_Code
	,PrimaryProcedureCode = Appointment.Procedure_Code
	,ProcedureCode1 = null
	,ProcedureCode2 = null
	,ProcedureCode3 = null
	,ProcedureCode4 = null
	,ProcedureCode5 = null
	,AttendanceOutcome = null
	,NHSNumber = NHS_Number
	,DistrictNo = District_Number

from
	SLAM.Encounter Appointment

left join 
	(
	select distinct
		 SiteCode = Site.NationalSiteCode
		,Site = Site.NationalSite
	from
		WH.Site
	) Site
on	Site.SiteCode = Appointment.Site_Code

left join 
	(
	select distinct
		 ConsultantCode = Consultant.NationalConsultantCode
		,Consultant = Consultant.NationalConsultant
	from
		WH.Consultant
	) Consultant
on	Consultant.ConsultantCode = Appointment.Consultant_Code

where
	(
		Appointment.Feed_Name = 'OP'
	or	Appointment.Feed_Name = 'Anticoagulant'
	or	Appointment.Feed_Name = 'GUM'
	or	Appointment.Feed_Name = 'StimStart'
	)
and	Appointment.Extract_Type = 'Freeze'
and	Appointment.Latest_In_Year = 1
--and	Appointment.Is_Current = 1
and Appointment.PoD_Code not in ('Mat-antenatal', 'Mat-Postnatal')
and cast(Appointment.Spell_Admission_Date as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')


--APC / Beddays / Dialysis
insert
into
	Activity.Encounter
(
	 SourceRecno
	,SourceSystem
	--,SourceMetric
	,SourceDatasetCode
	,Site
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,SourcePatientClassification
	,StartWardTypeCode
	,AdmissionDate
	,DischargeDate
	,SourceConsultant
	,PrimaryProcedureCode
	,PrimaryDiagnosis
	,NHSNumber
	,DistrictNo
)
select
	 SourceRecno = Encounter.MergeEncounterRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'APC'
	,SourceDatasetCode = 'APC'

	,Site = Site.NationalSite
	,Encounter.CasenoteNumber
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,PatientClassification = PatientClassification.SourcePatientClassification
	,StartWardTypeCode = Ward.SourceWardCode
	,AdmissionDate = cast(Encounter.AdmissionDate as date)
	,DischargeDate = cast(Encounter.DischargeDate as date)
	,Consultant = Consultant.SourceConsultant
	,PrimaryProcedureCode = Encounter.PrimaryProcedureCode
	,PrimaryDiagnosis = Diagnosis.Diagnosis
	,NHSNumber = Encounter.NHSNumber
	,DistrictNo = Encounter.DistrictNo

from
	APC.Encounter Encounter

inner join APC.FactEncounter
on	FactEncounter.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join APC.Metric
on	Metric.MetricID = FactEncounter.MetricID
and	Metric.MetricCode in
	(
	-- 'DCADM'
	--,'EMADM'
	--,'IEADM'
	--,'MATADM'
	--,'NEOTHADM'
	----,'RNADM'
	----,'RDADM'
	'CHK' -- need all FCEs in this dimension (for bed days)
	)

inner join WH.Site
on	Site.SourceSiteID = FactEncounter.SiteID

inner join WH.Consultant
on	Consultant.SourceConsultantID = FactEncounter.ConsultantID

inner join APC.Ward
on	Ward.SourceWardID = FactEncounter.StartWardID

inner join APC.PatientClassification
on	PatientClassification.SourcePatientClassificationID = FactEncounter.PatientClassificationID

inner join WH.Diagnosis
on	Diagnosis.DiagnosisID = FactEncounter.PrimaryDiagnosisID

where
	cast(Encounter.AdmissionDate as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 SourceRecno = BaseEncounter.MergeEncounterRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'APC'
	,SourceDatasetCode = 'IUI'

	,Site = null

	,BaseEncounter.CasenoteNumber
	,PatientForename = substring(BaseEncounter.PatientName, charindex(',', BaseEncounter.PatientName) + 2, len(BaseEncounter.PatientName))
	,PatientSurname = left(BaseEncounter.PatientName, charindex(',', BaseEncounter.PatientName) -1)
	,PatientClassification = null
	,StartWardTypeCode = null
	,AdmissionDate = null
	,DischargeDate = null
	,Consultant = null
	,PrimaryProcedureCode = null
	,PrimaryDiagnosis = null
	,NHSNumber = NHSNumber
	,DistrictNo = null
from
	IUI.BaseEncounter

inner join IUI.BaseEncounterReference
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

where
	cast(BaseEncounter.TreatmentDate as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 SourceRecno = Encounter.Contracting_ID
	,SourceSystem = 'SLAM'
	--,SourceMetric = 'APC'
	,SourceDatasetCode = 'APC'

	,Site = Site.Site
	,Encounter.Casenote_Number
	,PatientForename = Encounter.Forename
	,PatientSurname = Encounter.Surname
	,PatientClassification = PatientClassification.PatientClassification
	,StartWardTypeCode = null
	,AdmissionDate = cast(Encounter.Spell_Admission_Date as date)
	,DischargeDate = cast(Encounter.Spell_Discharge_Date as date)
	,Consultant = Consultant.Consultant
	,PrimaryProcedureCode = Encounter.Procedure_Code
	,PrimaryDiagnosis = Diagnosis.Diagnosis
	,NHSNumber = NHS_Number
	,DistrictNo = District_Number

from
	SLAM.Encounter Encounter

left join 
	(
	select distinct
		 SiteCode = Site.NationalSiteCode
		,Site = Site.NationalSite
	from
		WH.Site
	) Site
on	Site.SiteCode = Encounter.Site_Code

left join 
	(
	select distinct
		 ConsultantCode = Consultant.NationalConsultantCode
		,Consultant = Consultant.NationalConsultant
	from
		WH.Consultant
	) Consultant
on	Consultant.ConsultantCode = Encounter.Consultant_Code

left join 
	(
	select PatientClassificationCode = -1, PatientClassification = 'Not Specified' union all
	select PatientClassificationCode = 1, PatientClassification = 'Ordinary Admission' union all
	select PatientClassificationCode = 2, PatientClassification = 'Day Case Admission' union all
	select PatientClassificationCode = 3, PatientClassification = 'Regular Day Admission' union all
	select PatientClassificationCode = 4, PatientClassification = 'Regular Night Admission' union all
	select PatientClassificationCode = 5, PatientClassification = 'Mother And Baby Using Delivery Facilities Only' union all
	select PatientClassificationCode = 8, PatientClassification = 'Not Applicable'
	) PatientClassification
on	PatientClassification.PatientClassificationCode = Encounter.Patient_Classification

left join WH.Diagnosis
on	Diagnosis.DiagnosisCode = Encounter.Diagnosis_Code

where
	--Encounter.Feed_Name = 'APC'
	Feed_Name in 
			(
			'APC'
			,'Beddays_Non_Realtime'
			,'Beddays_Realtime'
			,'Dialysis'
			)
and	Encounter.Extract_Type = 'Freeze'
and	Encounter.Latest_In_Year = 1
--and	Encounter.Is_Current = 1
and	cast(Encounter.Spell_Admission_Date as date) >= @EarliestSLAMAdmissionDate -- to handle LTTU --(select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')




--Maternity
insert
into
	Activity.Encounter
(
	 SourceRecno
	,SourceSystem
	--,SourceMetric
	,SourceDatasetCode 
	,Site
	,AppointmentType
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,AppointmentDate
	,AppointmentTime
	,SourceConsultant
	,Clinic
	,PrimaryProcedureCode
	,ProcedureCode1
	,ProcedureCode2
	,ProcedureCode3
	,ProcedureCode4
	,ProcedureCode5
	,Outcome
	,NHSNumber
	,DistrictNo 
)

select
	 SourceRecno = BaseAntenatalCare.MergeAntenatalCareRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'MAT'
	,SourceDatasetCode = 'ANTE'

	,Site = null
	,AppointmentType = null
	,CasenoteNo = CasenoteNumber
	,PatientForename = null
	,PatientSurname = null
	,AppointmentDate = cast(BaseAntenatalCare.VisitDate as date)
	,AppointmentTime = null
	,Consultant = null
	,Clinic = null 
	,PrimaryProcedureCode = null
	,ProcedureCode1 = null
	,ProcedureCode2 = null
	,ProcedureCode3 = null
	,ProcedureCode4 = null
	,ProcedureCode5 = null
	,AttendanceOutcome = null
	,NHSNumber = NHSNumber
	,DistrictNo = DistrictNo
from
	Maternity.BaseAntenatalCare

inner join Maternity.BaseAntenatalCareReference
on	BaseAntenatalCare.MergeAntenatalCareRecno = BaseAntenatalCareReference.MergeAntenatalCareRecno

where
	cast(BaseAntenatalCare.VisitDate as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 SourceRecno = BasePostnatalCare.MergePostnatalCareRecno
	,SourceSystem = 'Warehouse'
	--,SourceMetric = 'MAT'
	,SourceDatasetCode = 'POST'

	,Site = null
	,AppointmentType = null
	,CasenoteNo = CasenoteNumber
	,PatientForename = null
	,PatientSurname = null
	,AppointmentDate = cast(BasePostnatalCare.DischargedToHealthVisitorDate as date)
	,AppointmentTime = null
	,Consultant = null
	,Clinic = null 
	,PrimaryProcedureCode = null
	,ProcedureCode1 = null
	,ProcedureCode2 = null
	,ProcedureCode3 = null
	,ProcedureCode4 = null
	,ProcedureCode5 = null
	,AttendanceOutcome = null
	,NHSNumber = NHSNumber
	,DistrictNo = null
from
	Maternity.BasePostnatalCare

inner join Maternity.BasePostnatalCareReference
on	BasePostnatalCare.MergePostnatalCareRecno = BasePostnatalCareReference.MergePostnatalCareRecno

where
	cast(BasePostnatalCare.DischargedToHealthVisitorDate as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')

union all

select
	 SourceRecno = Appointment.Contracting_ID
	,SourceSystem = 'SLAM'
	--,SourceMetric = 'MAT'
	,SourceDatasetCode = 'MAT'

	,Site = Site.Site
	,AppointmentType = null
	,Appointment.Casenote_Number
	,PatientForename = Appointment.Forename
	,PatientSurname = Appointment.Surname
	,AppointmentDate = cast(Appointment.Spell_Admission_Date as date)
	,AppointmentTime = null
	,Consultant = Consultant.Consultant
	,Clinic = Appointment.Clinic_Code
	,PrimaryProcedureCode = Appointment.Procedure_Code
	,ProcedureCode1 = null
	,ProcedureCode2 = null
	,ProcedureCode3 = null
	,ProcedureCode4 = null
	,ProcedureCode5 = null
	,AttendanceOutcome = null
	,NHSNumber = NHS_Number
	,DistrictNo = District_Number

from
	SLAM.Encounter Appointment

left join 
	(
	select distinct
		 SiteCode = Site.NationalSiteCode
		,Site = Site.NationalSite
	from
		WH.Site
	) Site
on	Site.SiteCode = Appointment.Site_Code

left join 
	(
	select distinct
		 ConsultantCode = Consultant.NationalConsultantCode
		,Consultant = Consultant.NationalConsultant
	from
		WH.Consultant
	) Consultant
on	Consultant.ConsultantCode = Appointment.Consultant_Code

where
	Appointment.PoD_Code in ('Mat-antenatal', 'Mat-Postnatal')
and	Appointment.Extract_Type = 'Freeze'
and Appointment.Latest_In_Year = 1
--and	Appointment.Is_Current = 1
and cast(Appointment.Spell_Admission_Date as date) >= (select DateValue from Utility.Parameter where Parameter = 'ACTIVITYSTARTDATE')


--Other
insert
into
	Activity.Encounter
(
	 SourceRecno
	,SourceSystem
	,SourceDatasetCode
)
select
	 SourceRecno = -1
	,SourceSystem = 'Warehouse'
	,SourceDatasetCode = 'APCWL'

union all

select
	 SourceRecno = -1
	,SourceSystem = 'Warehouse'
	,SourceDatasetCode = 'OPWL'


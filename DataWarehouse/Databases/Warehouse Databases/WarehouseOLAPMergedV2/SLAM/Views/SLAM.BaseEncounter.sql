﻿


CREATE view [SLAM].[BaseEncounter]

as

/* 
==============================================================================================
Description:

When		Who			What
20150608	Paul Egan	Replaced SELECT * with column names
===============================================================================================
*/
 
select
	 Contracting_ID
	,Feed_key
	,Report_Master_ID
	,Period_ID
	,Spell_Admission_Date
	,Spell_Discharge_Date
	,NHS_Number
	,DoB
	,District_Number
	,Forename
	,Surname
	,Clinic_Code
	,Casenote_Number
	,Internal_Specialty_Code
	,LoS
	,Activity_Actual
	,Price_Actual
	,Price_Actual_Adjusted
	,Activity_Plan_Commissioner
	,Activity_Plan_Divisional
	,Price_Plan_Commissioner
	,Price_Plan_Divisional
	,Price_Forecast
	,PoD_Code
	,Summary_PoD
	,HRG_Code
	,HRG_Description
	,Feed_Name
	,Treatment_Function_Code
	,Treatment_Function_Description
	,Main_Specialty_Code
	,Main_Specialty_Description
	,Extract_Month
	,Extract_Type
	,Extract_Year
	,Report_Month
	,Commissioner_Code
	,Organisation_Type
	,Commissioner_Description
	,Contract_Type
	,NPoC
	,Contract_Stream
	,Area
	,Spell_ID
	,Site_Code
	,Contract_Flag
	,Core_Commissioner_Code
	,Core_Commissioner_Description
	,AdmMethod
	,Patient_Classification
	,Directorate_Code
	,Directorate_Name
	,Division
	,Consultant_Code
	,Exam_Code
	,Age_Category
	,Procedure_Code
	,Practice_Code
	,Postcode
	,SLAM_Part
	,Income_Type		-- New Column 20150608
	,Profile_ID
	,Diagnosis_Code
	,SUS_ID
	,Is_Current
	,SLAM_Import_Type
	,GP_Code
	,Referral_Source_Code
	,Latest_In_Year
from 
	[$(Contract_Reporting)].SLAM.ContractingInformationFull
;




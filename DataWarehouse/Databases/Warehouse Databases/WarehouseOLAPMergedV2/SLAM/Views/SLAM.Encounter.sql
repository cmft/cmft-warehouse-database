﻿








CREATE view [SLAM].[Encounter]

as

select --top 1000
	Contracting_ID
	,Feed_key
	,Report_Master_ID
	,Spell_Admission_Date
	,Spell_Discharge_Date
	,NHS_Number
	,DoB
	,District_Number
	,Forename
	,Surname
	,Clinic_Code
	,Casenote_Number
	,Internal_Specialty_Code
	,LoS
	,Activity_Actual
	,Price_Actual
	,Price_Actual_Adjusted
	,Activity_Plan_Commissioner
	,Activity_Plan_Divisional
	,Price_Plan_Commissioner
	,Price_Plan_Divisional
	,Price_Forecast
	,PoD_Code
	,Summary_PoD
	,HRG_Code
	,HRG_Description
	,Feed_Name
	,Treatment_Function_Code
	,Treatment_Function_Description
	,Extract_Month
	,Extract_Year
	,Extract_Type
	,Report_Month
	,Commissioner_Code
	,Organisation_Type
	,Commissioner_Description
	,Contract_Type
	,NPoC
	,Spell_ID
	,Site_Code
	,Contract_Flag
	,Core_Commissioner_Code
	,Core_Commissioner_Description
	,AdmMethod
	,Patient_Classification
	,Directorate_Code
	,Directorate_Name
	,Division
	,Consultant_Code
	,Exam_Code
	,Age_Category
	,Procedure_Code
	,Practice_Code
	,Postcode
	,SLAM_Part
	,Profile_ID
	,Diagnosis_Code
	,SUS_ID
	,Is_Current
	,Latest_In_Year
	,ContextCode = 
				case
				when Feed_Name = 'AandE'
				then
					case
					when Site_Code = 'RW3TR' and HRG_Code = 'WIC'
					then 'TRA||ADAS'
					when Site_Code = 'RW3TR' and HRG_Code != 'WIC'
					then 'TRA||SYM' 
					when Site_Code != 'RW3TR' and HRG_Code = 'PCEC'
					then 'CEN||ADAS'
					when Site_Code != 'RW3TR' and HRG_Code != 'PCEC'
					then 'CEN||SYM'
					end
				when Feed_Name = 'APC'
				then
					case
					when left(Spell_ID, 1) = 'A'
					then 'TRA||UG'
					else 'CEN||PAS'
					end
				when Feed_Name = 'Beddays_Non_Realtime'
				then
					case
					when left(Spell_ID, 1) = 'A'
					then 'TRA||UG'
					else 'CEN||PAS'
					end
				when Feed_Name = 'Beddays_Realtime'
				then
					case
					when left(Spell_ID, 1) = 'A'
					then 'TRA||UG'
					else 'CEN||PAS'
					end
				when Feed_Name = 'Chemotherapy_Delivery'
				then
					case
					when left(Spell_ID, 1) = 'A'
					then 'TRA||UG'
					else 'CEN||PAS'
					end
				when Feed_Name = 'Diabetes_Paediatric'
				then
					case
					when left(Spell_ID, 1) = 'A'
					then 'TRA||UG'
					else 'CEN||PAS'
					end
				when Feed_Name = 'Dialysis'
				then
					case
					when left(Spell_ID, 1) = 'A'
					then 'TRA||UG'
					else 'CEN||PAS'
					end
				when Feed_Name = 'OP'
				then
					case
					when SUS_ID like '%*%'
					then 'CEN||PAS'
					else 'TRA||UG'
					end								
				when Feed_Name = 'Anticoagulant'
				then 'TRA||DAWN'
				when PoD_Code in ('Mat-antenatal', 'Mat-Postnatal')
				then 'CEN||SMMIS'
				when Feed_Name = 'GUM'
				then 'CEN||MCSH'
				end
								
from
	SLAM.BaseEncounter








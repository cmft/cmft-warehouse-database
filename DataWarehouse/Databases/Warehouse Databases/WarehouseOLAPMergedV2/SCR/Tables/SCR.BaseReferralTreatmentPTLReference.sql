﻿CREATE TABLE [SCR].[BaseReferralTreatmentPTLReference] (
    [MergeRecno]                     INT          NOT NULL,
    [ContextID]                      INT          NOT NULL,
    [CensusDateID]                   INT          NULL,
    [AtDiagnosisAgeID]               INT          NULL,
    [SexID]                          INT          NULL,
    [PracticeID]                     INT          NULL,
    [PriorityTypeID]                 INT          NULL,
    [CancerTypeID]                   INT          NULL,
    [CancerStatusID]                 INT          NULL,
    [SourceForOutpatientID]          INT          NULL,
    [InitialTreatmentID]             INT          NULL,
    [TreatmentSettingID]             INT          NULL,
    [ConsultantID]                   INT          NULL,
    [SpecialtyID]                    INT          NULL,
    [PrimaryDiagnosisID]             INT          NULL,
    [FirstAppointmentConsultantID]   INT          NULL,
    [ReferringOrganisationID]        INT          NULL,
    [TreatmentOrganisationID]        INT          NULL,
    [FirstAppointmentOrganisationID] INT          NULL,
    [Created]                        DATETIME     NULL,
    [ByWhom]                         VARCHAR (50) NULL,
    CONSTRAINT [PK_BaseReferralTreatmentPTLReference] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


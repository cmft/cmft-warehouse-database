﻿CREATE TABLE [SCR].[SubTumourSite] (
    [SubTumourSiteID] INT           NOT NULL,
    [SubTumourSite]   VARCHAR (255) NULL,
    CONSTRAINT [PK_SubTumourSite] PRIMARY KEY CLUSTERED ([SubTumourSiteID] ASC)
);


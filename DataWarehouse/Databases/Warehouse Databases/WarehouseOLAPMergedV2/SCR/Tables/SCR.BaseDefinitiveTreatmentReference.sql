﻿CREATE TABLE [SCR].[BaseDefinitiveTreatmentReference] (
    [UniqueRecordID]                 INT           NOT NULL,
    [ContextID]                      INT           NOT NULL,
    [FirstDefinitiveTreatmentDateID] INT           NULL,
    [Created]                        DATETIME      NULL,
    [Updated]                        DATETIME      NULL,
    [ByWhom]                         VARCHAR (255) NULL,
    CONSTRAINT [PK__BaseDefi__281341816F763501] PRIMARY KEY CLUSTERED ([UniqueRecordID] ASC)
);


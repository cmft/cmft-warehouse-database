﻿CREATE TABLE [SCR].[BaseReferralReference] (
    [MergeRecno]              INT           NOT NULL,
    [ContextID]               INT           NOT NULL,
    [SpecialtyID]             INT           NULL,
    [ReceiptOfReferralDateID] INT           NULL,
    [FirstAppointmentDateID]  INT           NULL,
    [DiagnosisDateID]         INT           NULL,
    [Created]                 DATETIME      NULL,
    [Updated]                 DATETIME      NULL,
    [ByWhom]                  VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseReferralReference_1] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


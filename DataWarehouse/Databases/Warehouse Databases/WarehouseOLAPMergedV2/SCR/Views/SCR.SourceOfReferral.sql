﻿CREATE view SCR.SourceOfReferral as

select
	 SourceOfReferralCode
	,SourceOfReferral
	,SourceOfReferralShort
	,SourceOfReferralMini
	,OrderBy
from
	[$(Warehouse)].SCR.SourceOfReferral

union all

select distinct
	 SourceOfReferralCode = BaseReferralTreatmentPTL.SourceOfReferralCode
	,SourceOfReferral = BaseReferralTreatmentPTL.SourceOfReferralCode + ' - No Description'
	,SourceOfReferralShort = BaseReferralTreatmentPTL.SourceOfReferralCode + ' - No Description'
	,SourceOfReferralMini = BaseReferralTreatmentPTL.SourceOfReferralCode + ' - No Description'
	,OrderBy = 999
from
	SCR.BaseReferralTreatmentPTL
where
	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.SourceOfReferral
	where
		SourceOfReferral.SourceOfReferralCode = BaseReferralTreatmentPTL.SourceOfReferralCode
	)
and	BaseReferralTreatmentPTL.SourceOfReferralCode is not null

union all

select
	 'N/A'
	,'N/A'
	,'N/A'
	,'N/A'
	,99

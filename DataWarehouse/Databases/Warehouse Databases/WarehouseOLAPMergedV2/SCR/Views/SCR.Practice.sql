﻿
CREATE view [SCR].[Practice] as

select

	 ActivePractice.SourceValueID
	,PracticeCode = ActivePractice.SourceValueCode
	,Practice = ActivePractice.SourceValueCode + ' - ' + coalesce(Practice.Organisation, 'No Description')
	,ContextCode = ActivePractice.SourceContextCode

	,CCG =
		convert(
			 varchar(255)
			,coalesce(rtrim(Practice.ParentOrganisationCode) + ' - ', '') + 
			 coalesce(CCG.Organisation, 'Unknown')
		)

	,HealthAuthority =
		convert(varchar(255),
			coalesce(
				HealthAuthority.Organisation
				,case
				when CCG.HACode is null
				then 'Unknown'
				else rtrim(CCG.HACode) + ' - No Description'
				end
			)
		)

	,LocalCCG =
		convert(varchar(255), 
			case
			when exists
				(
				select
					1
				from
					[$(Warehouse)].dbo.EntityLookup LocalCCG
				where
					LocalCCG.EntityCode = Practice.ParentOrganisationCode
				and	LocalCCG.EntityTypeCode = 'LOCALCCG'
				)
			then coalesce(rtrim(CCG.OrganisationCode), 'N/A') + ' - ' + coalesce(CCG.Organisation, 'Unknown')
			else 'Other'
			end
		)

from
	WH.Member ActivePractice

left join [$(Organisation)].ODS.Practice Practice
on	ActivePractice.SourceValueCode = Practice.OrganisationCode

left join [$(Organisation)].ODS.CCG CCG
on	CCG.OrganisationCode = Practice.ParentOrganisationCode

left join [$(Organisation)].ODS.HealthAuthority HealthAuthority
on	HealthAuthority.OrganisationCode = CCG.HACode

where
	ActivePractice.AttributeCode = 'PRACTICE'
and	ActivePractice.SourceContextCode = 'CEN||SCR'

﻿
CREATE view SCR.Age as

select distinct
	 AgeCode = AgeAtDiagnosis
	,AgeAtDiagnosis = cast(AgeAtDiagnosis as varchar)
from
	SCR.BaseReferralTreatmentPTL
where
	AgeAtDiagnosis is not null

union all

select
	-1
	,'N/A'

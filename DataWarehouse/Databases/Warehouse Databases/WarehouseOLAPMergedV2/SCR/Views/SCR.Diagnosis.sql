﻿CREATE view [SCR].[Diagnosis] as

select
	 Member.SourceValueID
	,DiagnosisCode = Member.SourceValueCode
	,Diagnosis = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join [$(Warehouse)].SCR.Diagnosis
on	Diagnosis.DiagnosisCode = Member.SourceValueCode

where
	Member.AttributeCode = 'DIAGNOSIS'
and	Member.SourceContextCode = 'CEN||SCR'









﻿CREATE view SCR.PCT as

select
	 PCTCode = rtrim(PCT.OrganisationCode)
	,PCT = rtrim(PCT.OrganisationCode) + ' - ' + PCT.Organisation
from
	[$(Organisation)].dbo.PCT

union all

select
	'N/A'
	,'Unknown'

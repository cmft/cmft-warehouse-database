﻿create view SCR.Gender as

select
	 GenderCode
	,Gender
from
	[$(Warehouse)].SCR.Gender

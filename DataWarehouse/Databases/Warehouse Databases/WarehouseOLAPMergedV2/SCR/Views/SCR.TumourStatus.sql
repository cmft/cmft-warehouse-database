﻿
CREATE view [SCR].[TumourStatus] as

select
	 TumourStatus.TumourStatusCode
	,TumourStatus.TumourStatus
from
	[$(Warehouse)].SCR.TumourStatus

union

select
	-1
	,'N/A'


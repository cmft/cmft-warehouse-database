﻿create view SCR.CancerSite as

select
	 CancerSite.CancerSiteId
	,CancerSite.CancerSite
from
	[$(Warehouse)].SCR.CancerSite

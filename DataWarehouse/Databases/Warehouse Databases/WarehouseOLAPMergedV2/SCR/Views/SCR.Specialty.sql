﻿CREATE view SCR.Specialty as

select
	 SpecialtyCode = Specialty.SpecialtyCode
	,Specialty = Specialty.Specialty
	,Division
from
	[$(Warehouse)].SCR.Specialty

union all

select
	 999
	,'N/A'
	,'N/A'

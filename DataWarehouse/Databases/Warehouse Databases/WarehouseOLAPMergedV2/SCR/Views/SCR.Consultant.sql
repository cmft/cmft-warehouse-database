﻿CREATE view SCR.Consultant as

select
	 ConsultantCode = Consultant.NationalConsultantCode
	,Consultant = Consultant.Consultant + ' (' + Consultant.NationalConsultantCode + ')'
from
	[$(Warehouse)].SCR.Consultant

union all

select
	 'N/A'
	,'N/A'

union all

select distinct
	 BaseReferralTreatmentPTL.ConsultantCode
	,BaseReferralTreatmentPTL.ConsultantCode + ' - No Description'
from
	SCR.BaseReferralTreatmentPTL
where
	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.Consultant
	where
		Consultant.NationalConsultantCode = BaseReferralTreatmentPTL.ConsultantCode
	)
and	BaseReferralTreatmentPTL.ConsultantCode is not null

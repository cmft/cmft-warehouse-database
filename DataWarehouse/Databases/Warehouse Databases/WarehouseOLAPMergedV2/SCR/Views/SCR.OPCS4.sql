﻿CREATE view SCR.OPCS4 as

select
	 OPCS4Code
	,OPCS4 = OPCS4Code + ' - ' + OPCS4
from
	[$(Warehouse)].SCR.OPCS4
where
	exists
		(
		select
			1
		from
			SCR.BaseReferralTreatmentPTL
		where
			BaseReferralTreatmentPTL.IntendedProcedureCode = OPCS4.OPCS4Code
		)

union all

select distinct

	 IntendedProcedureCode = coalesce(BaseReferralTreatmentPTL.IntendedProcedureCode, 'N/A')
 	,coalesce(BaseReferralTreatmentPTL.IntendedProcedureCode + ' - Unknown', 'N/A')

from
	SCR.BaseReferralTreatmentPTL

where
	not exists
		(
		select
			1
		from
			[$(Warehouse)].SCR.OPCS4
		where
			OPCS4.OPCS4Code = BaseReferralTreatmentPTL.IntendedProcedureCode
		)

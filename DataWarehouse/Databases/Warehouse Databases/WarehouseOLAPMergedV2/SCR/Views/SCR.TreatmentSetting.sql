﻿




CREATE view [SCR].[TreatmentSetting] as

select
	 Member.SourceValueID
	,TreatmentSettingCode = Member.SourceValueCode
	,TreatmentSetting = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join [$(Warehouse)].SCR.TreatmentSetting
on	TreatmentSetting.TreatmentSettingCode = Member.SourceValueCode

where
	Member.AttributeCode = 'TREATMENTSETTING'









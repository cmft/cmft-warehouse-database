﻿CREATE view SCR.SourceForOutpatient as

select
	 SourceForOutpatientCode = SourceForOutpatient.SourceForOutpatientCode
	,SourceForOutpatient = SourceForOutpatient.SourceForOutpatient
	,OrderBy
from
	[$(Warehouse)].SCR.SourceForOutpatient

union all

select distinct
	 SourceForOutpatientCode = BaseReferralTreatmentPTL.SourceForOutpatientCode
	,SourceForOutpatient = BaseReferralTreatmentPTL.SourceForOutpatientCode + ' - No Description'
	,OrderBy = 999
from
	SCR.BaseReferralTreatmentPTL
where
	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.SourceForOutpatient
	where
		SourceForOutpatient.SourceForOutpatientCode = BaseReferralTreatmentPTL.SourceForOutpatientCode
	)
and	BaseReferralTreatmentPTL.SourceForOutpatientCode is not null

union all

select
	 'N/A'
	,'N/A'
	,999

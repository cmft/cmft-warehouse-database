﻿




CREATE view [SCR].[Organisation] as

select
	 Member.SourceValueID
	,OrganisationCode = Member.SourceValueCode
	,Organisation = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join [$(Warehouse)].SCR.Organisation
on	Organisation.OrganisationCode = Member.SourceValueCode

where
	Member.AttributeCode = 'ORG'









﻿


CREATE view [SCR].[FactReferralTreatmentPTL] as


select
	 PTL.PTLID
	,PTL.MergeRecno
	,PTL.ContextID
	,PTL.CensusDateID
	,PTL.AtDiagnosisAgeID
	,PTL.SexID
	,PTL.PracticeID
	,PTL.PriorityTypeID
	,PTL.CancerTypeID
	,PTL.CancerStatusID
	,PTL.SourceForOutpatientID
	,PTL.InitialTreatmentID
	,PTL.TreatmentSettingID
	,PTL.ConsultantID
	,PTL.SpecialtyID
	,PTL.PrimaryDiagnosisID
	,PTL.FirstAppointmentConsultantID
	,PTL.FirstAppointmentOrganisationID
	,PTL.ReferringOrganisationID
	,PTL.TreatmentOrganisationID
	,PTL.TumourStatusID
	,CancerSite.CancerSiteId

	,AdmittedPathway =
		cast(
			case
			when PTL.PathwayTypeCode = 'A'
			then 1
			else 0
			end
			as bit
		)

	,PTL.DTTAdjustmentDays
	,PTL.CurrentWait31Days
	,PTL.CurrentWait62Days
	,PTL.SevenDayWait
	,PTL.AgeAtDiagnosis

	,SubTumourSiteID =
		coalesce(
			 PTL.SubTumourSiteID
			,-1
		)

from
	(
	select
		 PTL.PTLID
		,BaseReferralTreatmentPTL.MergeRecno
		,Reference.ContextID
		,Reference.CensusDateID
		,Reference.AtDiagnosisAgeID
		,Reference.SexID
		,Reference.PracticeID
		,Reference.PriorityTypeID
		,Reference.CancerTypeID
		,Reference.CancerStatusID
		,Reference.SourceForOutpatientID
		,Reference.InitialTreatmentID
		,Reference.TreatmentSettingID
		,Reference.ConsultantID
		,Reference.SpecialtyID
		,Reference.PrimaryDiagnosisID
		,Reference.FirstAppointmentConsultantID
		,Reference.FirstAppointmentOrganisationID
		,Reference.ReferringOrganisationID
		,Reference.TreatmentOrganisationID
		,TumourStatusID = coalesce(BaseReferralTreatmentPTL.TumourStatusCode, -1)
		,BaseReferralTreatmentPTL.CancerSite
		,BaseReferralTreatmentPTL.PathwayTypeCode

		,BaseReferralTreatmentPTL.DTTAdjustmentDays
		,BaseReferralTreatmentPTL.CurrentWait31Days
		,BaseReferralTreatmentPTL.CurrentWait62Days
		,BaseReferralTreatmentPTL.SevenDayWait
		,BaseReferralTreatmentPTL.AgeAtDiagnosis
		,BaseReferralTreatmentPTL.SubTumourSiteID
	from
		SCR.BaseReferralTreatmentPTL

	inner join SCR.BaseReferralTreatmentPTLReference Reference
	on	Reference.MergeRecno = BaseReferralTreatmentPTL.MergeRecno

	inner join SCR.PTL
	on	PTL.PTL = 'Full'

	where
		(
			BaseReferralTreatmentPTL.DTT = 1
		or	BaseReferralTreatmentPTL.RTT = 1
		or	BaseReferralTreatmentPTL.Upgrade = 1
		or	BaseReferralTreatmentPTL.Subsequent = 1
		or	BaseReferralTreatmentPTL.Screening = 1
		)

	union all

	select
		 PTL.PTLID
		,BaseReferralTreatmentPTL.MergeRecno
		,Reference.ContextID
		,Reference.CensusDateID
		,Reference.AtDiagnosisAgeID
		,Reference.SexID
		,Reference.PracticeID
		,Reference.PriorityTypeID
		,Reference.CancerTypeID
		,Reference.CancerStatusID
		,Reference.SourceForOutpatientID
		,Reference.InitialTreatmentID
		,Reference.TreatmentSettingID
		,Reference.ConsultantID
		,Reference.SpecialtyID
		,Reference.PrimaryDiagnosisID
		,Reference.FirstAppointmentConsultantID
		,Reference.FirstAppointmentOrganisationID
		,Reference.ReferringOrganisationID
		,Reference.TreatmentOrganisationID
		,TumourStatusID = coalesce(BaseReferralTreatmentPTL.TumourStatusCode, -1)
		,BaseReferralTreatmentPTL.CancerSite
		,BaseReferralTreatmentPTL.PathwayTypeCode

		,BaseReferralTreatmentPTL.DTTAdjustmentDays
		,BaseReferralTreatmentPTL.CurrentWait31Days
		,BaseReferralTreatmentPTL.CurrentWait62Days
		,BaseReferralTreatmentPTL.SevenDayWait
		,BaseReferralTreatmentPTL.AgeAtDiagnosis
		,BaseReferralTreatmentPTL.SubTumourSiteID
	from
		SCR.BaseReferralTreatmentPTL

	inner join SCR.BaseReferralTreatmentPTLReference Reference
	on	Reference.MergeRecno = BaseReferralTreatmentPTL.MergeRecno

	inner join SCR.PTL
	on	PTL.PTL = '62 Day'

	where
		BaseReferralTreatmentPTL.RTT = 1

	union all

	select
		 PTL.PTLID
		,BaseReferralTreatmentPTL.MergeRecno
		,Reference.ContextID
		,Reference.CensusDateID
		,Reference.AtDiagnosisAgeID
		,Reference.SexID
		,Reference.PracticeID
		,Reference.PriorityTypeID
		,Reference.CancerTypeID
		,Reference.CancerStatusID
		,Reference.SourceForOutpatientID
		,Reference.InitialTreatmentID
		,Reference.TreatmentSettingID
		,Reference.ConsultantID
		,Reference.SpecialtyID
		,Reference.PrimaryDiagnosisID
		,Reference.FirstAppointmentConsultantID
		,Reference.FirstAppointmentOrganisationID
		,Reference.ReferringOrganisationID
		,Reference.TreatmentOrganisationID
		,TumourStatusID = coalesce(BaseReferralTreatmentPTL.TumourStatusCode, -1)
		,BaseReferralTreatmentPTL.CancerSite
		,BaseReferralTreatmentPTL.PathwayTypeCode

		,BaseReferralTreatmentPTL.DTTAdjustmentDays
		,BaseReferralTreatmentPTL.CurrentWait31Days
		,BaseReferralTreatmentPTL.CurrentWait62Days
		,BaseReferralTreatmentPTL.SevenDayWait
		,BaseReferralTreatmentPTL.AgeAtDiagnosis
		,BaseReferralTreatmentPTL.SubTumourSiteID
	from
		SCR.BaseReferralTreatmentPTL

	inner join SCR.BaseReferralTreatmentPTLReference Reference
	on	Reference.MergeRecno = BaseReferralTreatmentPTL.MergeRecno

	inner join SCR.PTL
	on	PTL.PTL = 'Screening'

	where
		BaseReferralTreatmentPTL.Screening = 1

	union all

	select
		 PTL.PTLID
		,BaseReferralTreatmentPTL.MergeRecno
		,Reference.ContextID
		,Reference.CensusDateID
		,Reference.AtDiagnosisAgeID
		,Reference.SexID
		,Reference.PracticeID
		,Reference.PriorityTypeID
		,Reference.CancerTypeID
		,Reference.CancerStatusID
		,Reference.SourceForOutpatientID
		,Reference.InitialTreatmentID
		,Reference.TreatmentSettingID
		,Reference.ConsultantID
		,Reference.SpecialtyID
		,Reference.PrimaryDiagnosisID
		,Reference.FirstAppointmentConsultantID
		,Reference.FirstAppointmentOrganisationID
		,Reference.ReferringOrganisationID
		,Reference.TreatmentOrganisationID
		,TumourStatusID = coalesce(BaseReferralTreatmentPTL.TumourStatusCode, -1)
		,BaseReferralTreatmentPTL.CancerSite
		,BaseReferralTreatmentPTL.PathwayTypeCode

		,BaseReferralTreatmentPTL.DTTAdjustmentDays
		,BaseReferralTreatmentPTL.CurrentWait31Days
		,BaseReferralTreatmentPTL.CurrentWait62Days
		,BaseReferralTreatmentPTL.SevenDayWait
		,BaseReferralTreatmentPTL.AgeAtDiagnosis
		,BaseReferralTreatmentPTL.SubTumourSiteID
	from
		SCR.BaseReferralTreatmentPTL

	inner join SCR.BaseReferralTreatmentPTLReference Reference
	on	Reference.MergeRecno = BaseReferralTreatmentPTL.MergeRecno

	inner join SCR.PTL
	on	PTL.PTL = 'Upgrade'

	where
		BaseReferralTreatmentPTL.Upgrade = 1



	--this is the equivalent of the Full PTL
	union all

	select
		 PTL.PTLID
		,BaseReferralTreatmentPTL.MergeRecno
		,Reference.ContextID
		,Reference.CensusDateID
		,Reference.AtDiagnosisAgeID
		,Reference.SexID
		,Reference.PracticeID
		,Reference.PriorityTypeID
		,Reference.CancerTypeID
		,Reference.CancerStatusID
		,Reference.SourceForOutpatientID
		,Reference.InitialTreatmentID
		,Reference.TreatmentSettingID
		,Reference.ConsultantID
		,Reference.SpecialtyID
		,Reference.PrimaryDiagnosisID
		,Reference.FirstAppointmentConsultantID
		,Reference.FirstAppointmentOrganisationID
		,Reference.ReferringOrganisationID
		,Reference.TreatmentOrganisationID
		,TumourStatusID = coalesce(BaseReferralTreatmentPTL.TumourStatusCode, -1)
		,BaseReferralTreatmentPTL.CancerSite
		,BaseReferralTreatmentPTL.PathwayTypeCode

		,BaseReferralTreatmentPTL.DTTAdjustmentDays
		,BaseReferralTreatmentPTL.CurrentWait31Days
		,BaseReferralTreatmentPTL.CurrentWait62Days
		,BaseReferralTreatmentPTL.SevenDayWait
		,BaseReferralTreatmentPTL.AgeAtDiagnosis
		,BaseReferralTreatmentPTL.SubTumourSiteID
	from
		SCR.BaseReferralTreatmentPTL

	inner join SCR.BaseReferralTreatmentPTLReference Reference
	on	Reference.MergeRecno = BaseReferralTreatmentPTL.MergeRecno

	inner join SCR.PTL
	on	PTL.PTL = '31 Day'

	where
		BaseReferralTreatmentPTL.DTT = 1


	union all

	select
		 PTL.PTLID
		,BaseReferralTreatmentPTL.MergeRecno
		,Reference.ContextID
		,Reference.CensusDateID
		,Reference.AtDiagnosisAgeID
		,Reference.SexID
		,Reference.PracticeID
		,Reference.PriorityTypeID
		,Reference.CancerTypeID
		,Reference.CancerStatusID
		,Reference.SourceForOutpatientID
		,Reference.InitialTreatmentID
		,Reference.TreatmentSettingID
		,Reference.ConsultantID
		,Reference.SpecialtyID
		,Reference.PrimaryDiagnosisID
		,Reference.FirstAppointmentConsultantID
		,Reference.FirstAppointmentOrganisationID
		,Reference.ReferringOrganisationID
		,Reference.TreatmentOrganisationID
		,TumourStatusID = coalesce(BaseReferralTreatmentPTL.TumourStatusCode, -1)
		,BaseReferralTreatmentPTL.CancerSite
		,BaseReferralTreatmentPTL.PathwayTypeCode

		,BaseReferralTreatmentPTL.DTTAdjustmentDays
		,BaseReferralTreatmentPTL.CurrentWait31Days
		,BaseReferralTreatmentPTL.CurrentWait62Days
		,BaseReferralTreatmentPTL.SevenDayWait
		,BaseReferralTreatmentPTL.AgeAtDiagnosis
		,BaseReferralTreatmentPTL.SubTumourSiteID
	from
		SCR.BaseReferralTreatmentPTL

	inner join SCR.BaseReferralTreatmentPTLReference Reference
	on	Reference.MergeRecno = BaseReferralTreatmentPTL.MergeRecno

	inner join SCR.PTL
	on	PTL.PTL = 'Subsequent'

	where
		BaseReferralTreatmentPTL.Subsequent = 1

	) PTL

inner join SCR.CancerSite
on	CancerSite.CancerSite = PTL.CancerSite




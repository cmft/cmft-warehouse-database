﻿create view SCR.CancerStatus as

select
	 Member.SourceValueID
	,CancerStatusCode = Member.SourceValueCode
	,CancerStatus = Member.SourceValue
	,ContextCode = Member.SourceContextCode
	,CancerStatus.CancerStatusShort
	,CancerStatus.OrderBy
from
	WH.Member

left join [$(Warehouse)].SCR.CancerStatus
on	CancerStatus.CancerStatusCode = Member.SourceValueCode

where
	Member.AttributeCode = 'CANCERSTATUS'







﻿
CREATE view [SCR].[CancerType] as

select
	 Member.SourceValueID
	,CancerTypeCode = Member.SourceValueCode
	,CancerType = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join [$(Warehouse)].SCR.CancerType
on	CancerType.CancerTypeCode = Member.SourceValueCode

where
	Member.AttributeCode = 'CANCERTYPE'

union

select
	-1
	,'N/A'
	,'N/A'
	,'N/A'






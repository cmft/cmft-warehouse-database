﻿
create view SCR.PathwayType as

select
	 PathwayTypeCode = 'A'
	,PathwayType = 'Admitted'

union all

select
	 PathwayTypeCode = 'N'
	,PathwayType = 'Non-Admitted'

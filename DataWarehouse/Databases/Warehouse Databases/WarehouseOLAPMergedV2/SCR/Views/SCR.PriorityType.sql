﻿

CREATE view [SCR].[PriorityType] as

select
	 Member.SourceValueID
	,PriorityTypeCode = Member.SourceValueCode
	,PriorityType = Member.SourceValue
	,ContextCode = Member.SourceContextCode
from
	WH.Member

left join [$(Warehouse)].SCR.PriorityType
on	PriorityType.PriorityTypeCode = Member.SourceValueCode

where
	Member.AttributeCode = 'CANCERPRIORITY'


union

select
	-1
	,'N/A'
	,'N/A'
	,'N/A'








﻿CREATE TABLE [GUM].[BaseEncounter] (
    [MergeEncounterRecno] INT           IDENTITY (1, 1) NOT NULL,
    [EncounterRecno]      INT           NOT NULL,
    [AppointmentDate]     DATE          NOT NULL,
    [AppointmentTime]     DATETIME      NOT NULL,
    [PatientIdentifier]   NVARCHAR (50) NULL,
    [Postcode]            NVARCHAR (50) NULL,
    [ClinicID]            INT           NULL,
    [AppointmentTypeID]   INT           NULL,
    [AttendanceStatusID]  INT           NULL,
    [InterfaceCode]       VARCHAR (10)  NULL,
    [ContextCode]         VARCHAR (9)   NOT NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (50)  NULL,
    CONSTRAINT [PK__BaseGUME__4C9E43CC292EA0A1] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


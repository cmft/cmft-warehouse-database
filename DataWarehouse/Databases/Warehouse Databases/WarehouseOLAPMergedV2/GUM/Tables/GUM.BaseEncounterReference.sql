﻿CREATE TABLE [GUM].[BaseEncounterReference] (
    [MergeEncounterRecno] INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [AppointmentDateID]   INT           NULL,
    [ClinicID]            INT           NOT NULL,
    [AppointmentTypeID]   INT           NOT NULL,
    [AttendanceStatusID]  INT           NOT NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (255) NULL,
    CONSTRAINT [PK_GUM_Base_Encounter_Reference] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC)
);


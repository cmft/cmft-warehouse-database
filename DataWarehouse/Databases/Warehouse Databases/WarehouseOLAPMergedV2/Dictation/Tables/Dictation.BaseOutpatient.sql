﻿CREATE TABLE [Dictation].[BaseOutpatient] (
    [MergeOutpatientRecno] INT           IDENTITY (1, 1) NOT NULL,
    [OutpatientRecno]      INT           NULL,
    [SourcePatientNo]      VARCHAR (20)  NULL,
    [AppointmentTime]      DATETIME      NULL,
    [DoctorCode]           VARCHAR (50)  NULL,
    [InterfaceCode]        VARCHAR (10)  NULL,
    [ContextCode]          VARCHAR (10)  NULL,
    [Created]              SMALLDATETIME NOT NULL,
    [Updated]              SMALLDATETIME NULL,
    [ByWhom]               VARCHAR (50)  NULL
);


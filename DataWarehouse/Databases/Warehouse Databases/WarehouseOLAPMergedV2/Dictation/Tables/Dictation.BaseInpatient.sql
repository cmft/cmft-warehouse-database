﻿CREATE TABLE [Dictation].[BaseInpatient] (
    [MergeInpatientRecno] INT           IDENTITY (1, 1) NOT NULL,
    [InpatientRecno]      INT           NOT NULL,
    [SourcePatientNo]     VARCHAR (20)  NULL,
    [AdmissionTime]       DATETIME      NULL,
    [SpellSequenceNo]     INT           NULL,
    [RequestTypeCode]     VARCHAR (2)   NULL,
    [DocumentID]          VARCHAR (20)  NULL,
    [InterfaceCode]       VARCHAR (20)  NULL,
    [ContextCode]         VARCHAR (10)  NOT NULL,
    [Created]             SMALLDATETIME NULL,
    [Updated]             SMALLDATETIME NULL,
    [ByWhom]              VARCHAR (50)  NULL,
    CONSTRAINT [PK__BaseInpa__5F9FAF650E1B8309] PRIMARY KEY CLUSTERED ([MergeInpatientRecno] ASC)
);


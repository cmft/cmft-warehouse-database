﻿CREATE TABLE [Dictation].[BaseDocumentReference] (
    [MergeDocumentRecno] INT          NOT NULL,
    [ContextID]          INT          NULL,
    [DocumentTypeID]     INT          NULL,
    [SignedByID]         INT          NULL,
    [SignedStatusID]     INT          NULL,
    [DepartmentID]       INT          NULL,
    [Created]            DATETIME     NULL,
    [Updated]            DATETIME     NULL,
    [ByWhom]             VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseDocu__7BB4BD0311EC13ED] PRIMARY KEY CLUSTERED ([MergeDocumentRecno] ASC)
);


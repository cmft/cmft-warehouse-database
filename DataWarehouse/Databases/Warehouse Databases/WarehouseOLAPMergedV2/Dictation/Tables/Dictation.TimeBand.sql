﻿CREATE TABLE [Dictation].[TimeBand] (
    [TimeBandID]         INT          NOT NULL,
    [MinuteBand]         VARCHAR (20) NULL,
    [HourBand]           VARCHAR (20) NULL,
    [TwelveHourBand]     VARCHAR (20) NULL,
    [TwentyFourHourBand] VARCHAR (20) NULL,
    [FortyEightHourBand] VARCHAR (20) NULL,
    CONSTRAINT [PK_TimeBand] PRIMARY KEY CLUSTERED ([TimeBandID] ASC)
);


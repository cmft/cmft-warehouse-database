﻿



create view Dictation.[DocumentType] as

SELECT
	 Member.SourceContextCode
	,Member.SourceContext
	,SourceDocumentTypeID = Member.SourceValueID
	,SourceDocumentTypeCode = Member.SourceValueCode
	,SourceDocumentType = Member.SourceValue
	,LocalDocumentTypeID = Member.LocalValueID
	,LocalDocumentTypeCode = Member.LocalValueCode
	,LocalDocumentType = Member.LocalValue
	,NationalDocumentTypeID = Member.NationalValueID
	,NationalDocumentTypeCode = Member.NationalValueCode
	,NationalDocumentType = Member.NationalValue

FROM
	WH.Member

where 
	Member.AttributeCode = 'DOCTYPE'




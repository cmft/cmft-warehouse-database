﻿CREATE view Dictation.Document

as

select
	MergeDocumentRecno
	,DocumentRecno
	,SourceUniqueID
	,SourcePatientNo
	,DocumentID
	,DocumentDescription
	,DocumentTypeCode
	,AuthorCode
	,TypedTime
	,DictatedTime
	,DictatedByCode
	,IssuedTime
	,IssuedByCode
	,SignedTime
	,SignedByCode
	,SignedStatusCode
	,DocumentXMLTemplateCode
	--,DocumentXML
	,TransmissionTime
	,TransmissionDestinationCode
	,ContextCode
	,Created
	,Updated
	,ByWhom
from
	Dictation.BaseDocument

union

select
	MergeDocumentRecno = -1
	,DocumentRecno = -1
	,SourceUniqueID = -1
	,SourcePatientNo = -1
	,DocumentID = 'N/A'
	,DocumentDescription = 'N/A'
	,DocumentTypeCode  = 'N/A'
	,AuthorCode  = 'N/A'
	,TypedTime = '1 jan 1900'
	,DictatedTime = '1 jan 1900'
	,DictatedByCode  = 'N/A'
	,IssuedTime  = '1 jan 1900'
	,IssuedByCode  = 'N/A'
	,SignedTime  = '1 jan 1900'
	,SignedByCode = 'N/A'
	,SignedStatusCode = 'N/A'
	,DocumentXMLTemplateCode = 'N/A'
	--,DocumentXML = 'N/A'
	,TransmissionTime = '1 jan 1900'
	,TransmissionDestinationCode = 'N/A'
	,ContextCode = 'N/A'
	,Created = '1 jan 1900'
	,Updated = '1 jan 1900'
	,ByWhom = 'N/A'
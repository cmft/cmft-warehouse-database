﻿CREATE TABLE [HR].[BaseSummaryReference] (
    [MergeSummaryRecno] INT          NOT NULL,
    [ContextID]         INT          NULL,
    [CensusDateID]      INT          NULL,
    [WardID]            INT          NULL,
    [Created]           DATETIME     NULL,
    [Updated]           DATETIME     NULL,
    [ByWhom]            VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseSumm__F8D6199920E37363] PRIMARY KEY CLUSTERED ([MergeSummaryRecno] ASC)
);


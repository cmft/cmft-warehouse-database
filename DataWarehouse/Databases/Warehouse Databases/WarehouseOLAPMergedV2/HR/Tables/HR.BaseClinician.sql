﻿CREATE TABLE [HR].[BaseClinician] (
    [ClinicianCode]  VARCHAR (8)   NOT NULL,
    [MeasureCode]    VARCHAR (20)  NOT NULL,
    [CompletionDate] DATE          NULL,
    [ExpiryDate]     DATE          NULL,
    [Status]         VARCHAR (255) NULL,
    [Created]        DATETIME      NULL,
    [Updated]        DATETIME      NULL,
    [ByWhom]         VARCHAR (50)  NULL,
    CONSTRAINT [PK__BaseCons__D3B9944747E831EB] PRIMARY KEY CLUSTERED ([ClinicianCode] ASC, [MeasureCode] ASC)
);


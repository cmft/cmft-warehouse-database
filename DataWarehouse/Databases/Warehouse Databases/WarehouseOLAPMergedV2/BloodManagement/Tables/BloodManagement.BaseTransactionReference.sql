﻿CREATE TABLE [BloodManagement].[BaseTransactionReference] (
    [MergeRecno]        INT          NOT NULL,
    [ContextID]         INT          NULL,
    [SiteID]            INT          NULL,
    [LocationID]        INT          NULL,
    [SystemUserID]      INT          NULL,
    [TransactionTypeID] INT          NULL,
    [ConditionID]       INT          NULL,
    [FateID]            INT          NULL,
    [BloodGroupID]      INT          NULL,
    [BloodProductID]    INT          NULL,
    [Created]           DATETIME     NULL,
    [Updated]           DATETIME     NULL,
    [ByWhom]            VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseTran__A2E210447D7019F4] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


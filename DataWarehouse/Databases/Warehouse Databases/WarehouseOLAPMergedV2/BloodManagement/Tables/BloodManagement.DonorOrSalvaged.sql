﻿CREATE TABLE [BloodManagement].[DonorOrSalvaged] (
    [DonorOrSalvagedID] INT          NOT NULL,
    [DonorOrSalvaged]   VARCHAR (50) NULL,
    CONSTRAINT [PK_TransfusedOrSalvaged] PRIMARY KEY CLUSTERED ([DonorOrSalvagedID] ASC)
);


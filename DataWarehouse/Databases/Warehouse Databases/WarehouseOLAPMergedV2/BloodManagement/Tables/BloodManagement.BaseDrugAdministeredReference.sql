﻿CREATE TABLE [BloodManagement].[BaseDrugAdministeredReference] (
    [MergeRecno]   INT          NOT NULL,
    [ContextID]    INT          NULL,
    [LocationID]   INT          NULL,
    [SpecialtyID]  INT          NULL,
    [ConsultantID] INT          NULL,
    [DrugID]       INT          NULL,
    [UnitID]       INT          NULL,
    [Created]      DATETIME     NULL,
    [Updated]      DATETIME     NULL,
    [ByWhom]       VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseDrug__A2E210441547A385] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


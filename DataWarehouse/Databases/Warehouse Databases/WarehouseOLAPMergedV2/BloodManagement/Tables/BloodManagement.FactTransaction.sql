﻿CREATE TABLE [BloodManagement].[FactTransaction] (
    [MergeRecno]         INT NULL,
    [APCMergeRecno]      INT NULL,
    [TransactionDateID]  INT NULL,
    [TransactionTimeID]  INT NULL,
    [TransactionTypeID]  INT NULL,
    [ConditionID]        INT NULL,
    [BloodProductID]     INT NULL,
    [BloodGroupID]       INT NULL,
    [ReservationDateID]  INT NULL,
    [ReservationTimeID]  INT NULL,
    [ExpiryDateID]       INT NULL,
    [FateID]             INT NULL,
    [SystemUserID]       INT NULL,
    [BloodProductVolume] INT NULL,
    [Cases]              INT NULL
);


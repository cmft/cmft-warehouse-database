﻿CREATE TABLE [BloodManagement].[BaseCellSalvageReference] (
    [MergeRecno]   INT          NOT NULL,
    [ContextID]    INT          NULL,
    [LocationID]   INT          NULL,
    [SpecialtyID]  INT          NULL,
    [ConsultantID] INT          NULL,
    [Created]      DATETIME     NULL,
    [Updated]      DATETIME     NULL,
    [ByWhom]       VARCHAR (50) NULL,
    CONSTRAINT [PK__BaseCell__A2E210446C458DF2] PRIMARY KEY CLUSTERED ([MergeRecno] ASC)
);


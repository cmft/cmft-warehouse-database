﻿CREATE TABLE [BloodManagement].[FactDrug] (
    [MergeRecno]             INT NULL,
    [APCMergeRecno]          INT NULL,
    [LocationID]             INT NULL,
    [SpecialtyID]            INT NULL,
    [ConsultantID]           INT NULL,
    [DrugID]                 INT NULL,
    [UnitID]                 INT NULL,
    [DrugAdministeredDateID] INT NULL,
    [DrugAdministeredTimeID] INT NULL,
    [Dose]                   INT NULL,
    [Cases]                  INT NULL
);


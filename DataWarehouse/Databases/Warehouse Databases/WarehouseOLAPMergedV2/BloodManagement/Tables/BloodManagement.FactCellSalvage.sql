﻿CREATE TABLE [BloodManagement].[FactCellSalvage] (
    [MergeRecno]    INT NULL,
    [APCMergeRecno] INT NULL,
    [LocationID]    INT NULL,
    [SpecialtyID]   INT NULL,
    [ConsultantID]  INT NULL,
    [StartDateID]   INT NULL,
    [StartTimeID]   INT NULL,
    [EndDateID]     INT NULL,
    [EndTimeID]     INT NULL,
    [Duration]      INT NULL,
    [Dose]          INT NULL,
    [Cases]         INT NULL
);


﻿CREATE TABLE [BloodManagement].[BaseTranexamicAcidReference] (
    [MergeTranexamicAcidRecno] INT          NOT NULL,
    [ContextID]                INT          NULL,
    [StartDateID]              INT          NULL,
    [LocationID]               INT          NULL,
    [SpecialtyID]              INT          NULL,
    [ConsultantID]             INT          NULL,
    [Created]                  DATETIME     NULL,
    [Updated]                  DATETIME     NULL,
    [ByWhom]                   VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([MergeTranexamicAcidRecno] ASC)
);


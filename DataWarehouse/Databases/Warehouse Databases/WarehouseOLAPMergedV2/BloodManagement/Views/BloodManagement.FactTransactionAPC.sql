﻿

CREATE view [BloodManagement].[FactTransactionAPC] as

-- Spells
select
	FirstInMetric
	, DonorOrSalvagedID
	, MergeRecno
	, APCMergeRecno
	, TransactionDateID
	, TransactionTimeID
	, TransactionTypeID
	, ConditionID
	, BloodProductID
	, BloodGroupID
	, ReservationDateID
	, ReservationTimeID
	, ExpiryDateID
	, FateID
	, SystemUserID
	, BloodProductVolume
	, Transactions
	, EncounterDateID
	, MetricID
	, ConsultantID
	, SpecialtyID
	, SiteID
	, AgeID
	, AdmissionMethodID
	, RTTActivity
	, RTTTreated
	, EncounterTimeOfDay
	, EndDirectorateID
	, SexID
	, ISTAdmissionSpecialtyID
	, ISTAdmissionDemandTimeOfDay
	, ISTDischargeTimeOfDay
	, ContextID
	, AdmissionSourceID
	, DischargeDestinationID
	, DischargeMethodID
	, IntendedManagementID
	, NeonatalLevelOfCareID
	, PatientClassificationID
	, SubSpecialtyID
	, CodingComplete
	, VTE
	, VTECategoryID
	, VTEExclusionReasonID
	, VTECompleteAndOrExclusion
	, StartWardID
	, EndWardID
	, NationalExamID
	, ServiceID
	, CCGID
	, PrimaryDiagnosisID
	, PrimaryProcedureID
	, Cases
	, LengthOfEncounter
	, CasenoteMergeRecno
	, ResidenceCCGID
	, MergeDocumentRecno
	, DischargeSummaryStatusID
	, DischargeSummarySignedByID
	, DischargeSummaryProductionTimeID
	, DischargeSummaryExclusionReasonID
	, DischargeSummaryRequired
	, DischargeSummarySignedTimeID
	, GPPracticeID
	, ElectronicCorrespondenceActivated
	, HRGID
from
	BloodManagement.FactTransactionAPCSpell

union all -- Episodes

select
	FirstInMetric
	, DonorOrSalvagedID
	, MergeRecno
	, APCMergeRecno
	, TransactionDateID
	, TransactionTimeID
	, TransactionTypeID
	, ConditionID
	, BloodProductID
	, BloodGroupID
	, ReservationDateID
	, ReservationTimeID
	, ExpiryDateID
	, FateID
	, SystemUserID
	, BloodProductVolume
	, Transactions
	, EncounterDateID
	, MetricID
	, ConsultantID
	, SpecialtyID
	, SiteID
	, AgeID
	, AdmissionMethodID
	, RTTActivity
	, RTTTreated
	, EncounterTimeOfDay
	, EndDirectorateID
	, SexID
	, ISTAdmissionSpecialtyID
	, ISTAdmissionDemandTimeOfDay
	, ISTDischargeTimeOfDay
	, ContextID
	, AdmissionSourceID
	, DischargeDestinationID
	, DischargeMethodID
	, IntendedManagementID
	, NeonatalLevelOfCareID
	, PatientClassificationID
	, SubSpecialtyID
	, CodingComplete
	, VTE
	, VTECategoryID
	, VTEExclusionReasonID
	, VTECompleteAndOrExclusion
	, StartWardID
	, EndWardID
	, NationalExamID
	, ServiceID
	, CCGID
	, PrimaryDiagnosisID
	, PrimaryProcedureID
	, Cases
	, LengthOfEncounter
	, CasenoteMergeRecno
	, ResidenceCCGID
	, MergeDocumentRecno
	, DischargeSummaryStatusID
	, DischargeSummarySignedByID
	, DischargeSummaryProductionTimeID
	, DischargeSummaryExclusionReasonID
	, DischargeSummaryRequired
	, DischargeSummarySignedTimeID
	, GPPracticeID
	, ElectronicCorrespondenceActivated
	, HRGID
from
	BloodManagement.FactTransactionAPCEpisode



﻿

CREATE view [BloodManagement].[Product] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceBloodProductID = SourceValueID
	,SourceBloodProductCode = SourceValueCode
	,SourceBloodProduct = SourceValue
	,LocalBloodProductID = LocalValueID
	,LocalBloodProductCode = LocalValueCode
	,LocalBloodProduct = LocalValue
	,NationalBloodProductID = NationalValueID
	,NationalBloodProductCode = NationalValueCode
	,NationalBloodProduct = NationalValue
	,ProductTypeID = OtherValueID
	,ProductTypeCode = OtherValueCode
	,ProductType = OtherValue
	
FROM
	WH.Member
where
	AttributeCode = 'BLOODPRODUCT'
	

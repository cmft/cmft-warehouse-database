﻿



CREATE view [BloodManagement].[FactCellSalvageAPC] as

select
	FirstInMetric =
		cast(
			case
			when not exists
				(
				select
					1
				from
					BloodManagement.FactCellSalvage PreviousCellSalvage

				inner join APC.FactEncounter PreviousEncounter
				on	PreviousEncounter.MergeEncounterRecno = PreviousCellSalvage.APCMergeRecno
				and	PreviousEncounter.MetricID = FactEncounter.MetricID

				where
					PreviousCellSalvage.APCMergeRecno = FactCellSalvage.APCMergeRecno
				and	(
						PreviousCellSalvage.StartDateID < FactCellSalvage.StartDateID
					or	(
							PreviousCellSalvage.StartDateID = FactCellSalvage.StartDateID
						and	PreviousCellSalvage.StartTimeID < FactCellSalvage.StartTimeID
						)
					or	(
							PreviousCellSalvage.StartDateID = FactCellSalvage.StartDateID
						and	PreviousCellSalvage.StartTimeID = FactCellSalvage.StartTimeID
						and	PreviousCellSalvage.MergeRecno < FactCellSalvage.MergeRecno
						)
					)
				)

			then 1
			else 0
			end

			as bit
		)

	,FactCellSalvage.MergeRecno
	,FactCellSalvage.APCMergeRecno
	,CellSalvageLocationID = FactCellSalvage.LocationID
	,CellSalvageSpecialtyID = FactCellSalvage.SpecialtyID
	,CellSalvageConsultantID = FactCellSalvage.ConsultantID
	,CellSalvageStartDateID = FactCellSalvage.StartDateID
	,CellSalvageStartTimeID = FactCellSalvage.StartTimeID
	,CellSalvageEndDateID = FactCellSalvage.EndDateID
	,CellSalvageEndTimeID = FactCellSalvage.EndTimeID
	,CellSalvageDuration = FactCellSalvage.Duration
	,CellSalvageVolume = FactCellSalvage.Dose
	,CellSalvage = FactCellSalvage.Cases

	,FactEncounter.EncounterDateID
	,FactEncounter.MetricID
	,FactEncounter.ConsultantID
	,FactEncounter.SpecialtyID
	,FactEncounter.SiteID
	,FactEncounter.AgeID
	,FactEncounter.AdmissionMethodID
	,FactEncounter.RTTActivity
	,FactEncounter.RTTTreated
	,FactEncounter.EncounterTimeOfDay
	,FactEncounter.DirectorateID
	,FactEncounter.SexID
	,FactEncounter.ISTAdmissionSpecialtyID
	,FactEncounter.ISTAdmissionDemandTimeOfDay
	,FactEncounter.ISTDischargeTimeOfDay
	,FactEncounter.ContextID
	,FactEncounter.AdmissionSourceID
	,FactEncounter.DischargeDestinationID
	,FactEncounter.DischargeMethodID
	,FactEncounter.IntendedManagementID
	,FactEncounter.NeonatalLevelOfCareID
	,FactEncounter.PatientClassificationID
	,FactEncounter.SubSpecialtyID
	,FactEncounter.CodingComplete
	,FactEncounter.VTE
	,FactEncounter.VTECategoryID
	,FactEncounter.VTEExclusionReasonID
	,FactEncounter.VTECompleteAndOrExclusion
	,FactEncounter.StartWardID
	,FactEncounter.EndWardID
	,FactEncounter.NationalExamID
	,FactEncounter.ServiceID
	,FactEncounter.CCGID
	,FactEncounter.PrimaryDiagnosisID
	,FactEncounter.PrimaryProcedureID
	,FactEncounter.CasenoteMergeRecno
	,FactEncounter.ResidenceCCGID
	,FactEncounter.MergeDocumentRecno
	,FactEncounter.DischargeSummaryStatusID
	,FactEncounter.DischargeSummarySignedByID
	,FactEncounter.DischargeSummaryProductionTimeID
	,FactEncounter.DischargeSummaryExclusionReasonID
	,FactEncounter.DischargeSummaryRequired
	,FactEncounter.DischargeSummarySignedTimeID
	,FactEncounter.GPPracticeID
	,FactEncounter.ElectronicCorrespondenceActivated
	,FactEncounter.HRGID
from
	BloodManagement.FactCellSalvage

inner join APC.FactEncounter
on	FactEncounter.MergeEncounterRecno = FactCellSalvage.APCMergeRecno







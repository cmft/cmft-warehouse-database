﻿




create view [BloodManagement].[Unit] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceUnitID = SourceValueID
	,SourceUnitCode = SourceValueCode
	,SourceUnit = SourceValue
	,LocalUnitID = LocalValueID
	,LocalUnitCode = LocalValueCode
	,LocalUnit = LocalValue
	,NationalUnitID = NationalValueID
	,NationalUnitCode = NationalValueCode
	,NationalUnit = NationalValue
	,UnitTypeID = OtherValueID
	,UnitTypeCode = OtherValueCode
	,UnitType = OtherValue	
FROM
	WH.Member
where
	AttributeCode = 'UNIT'





﻿
create view [BloodManagement].[UnitFate] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceBloodUnitFateID = SourceValueID
	,SourceBloodUnitFateCode = SourceValueCode
	,SourceBloodUnitFate = SourceValue
	,LocalBloodUnitFateID = LocalValueID
	,LocalBloodUnitFateCode = LocalValueCode
	,LocalBloodUnitFate = LocalValue
	,NationalBloodUnitFateID = NationalValueID
	,NationalBloodUnitFateCode = NationalValueCode
	,NationalBloodUnitFate = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'BLOODUNITFATE'

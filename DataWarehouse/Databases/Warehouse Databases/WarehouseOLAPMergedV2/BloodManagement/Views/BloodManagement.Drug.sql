﻿



create view [BloodManagement].[Drug] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceDrugID = SourceValueID
	,SourceDrugCode = SourceValueCode
	,SourceDrug = SourceValue
	,LocalDrugID = LocalValueID
	,LocalDrugCode = LocalValueCode
	,LocalDrug = LocalValue
	,NationalDrugID = NationalValueID
	,NationalDrugCode = NationalValueCode
	,NationalDrug = NationalValue
	,DrugTypeID = OtherValueID
	,DrugTypeCode = OtherValueCode
	,DrugType = OtherValue	
FROM
	WH.Member
where
	AttributeCode = 'DRUG'




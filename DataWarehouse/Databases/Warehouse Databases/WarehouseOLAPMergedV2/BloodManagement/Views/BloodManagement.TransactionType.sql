﻿





CREATE view [BloodManagement].[TransactionType] as

SELECT
	 SourceContextCode
	,SourceContext
	,SourceTransactionTypeID = SourceValueID
	,SourceTransactionTypeCode = SourceValueCode
	,SourceTransactionType = SourceValue
	,LocalTransactionTypeID = LocalValueID
	,LocalTransactionTypeCode = LocalValueCode
	,LocalTransactionType = LocalValue
	,NationalTransactionTypeID = NationalValueID
	,NationalTransactionTypeCode = NationalValueCode
	,NationalTransactionType = NationalValue
FROM
	WH.Member
where
	AttributeCode = 'TRANSACTIONTYPE'

--SELECT
--	 SourceContextCode
--	,SourceContext
--	,SourceLocationID = SourceValueID
--	,SourceLocationCode = SourceValueCode
--	,SourceLocation = SourceValue
--	,LocalLocationID = LocalValueID
--	,LocalLocationCode = LocalValueCode
--	,LocalLocation = LocalValue
--	,NationalLocationID = NationalValueID
--	,NationalLocationCode = NationalValueCode
--	,NationalLocation = NationalValue
--	--,LocationTypeCode = Locations.LocationTypeID
--	,LocationType = coalesce(Locations.LocationType,
--							case	
--							when SourceValueCode = '-1' then 'Not Specified'
--							when SourceContextCode = 'CEN||ADAS' then 'Accident and Emergency'
--							when SourceContextCode = 'CEN||PAS' then 'Inpatient Ward'
--							when SourceContextCode = 'CEN||SYM' then 'Accident and Emergency'
--							when SourceContextCode = 'CMFT||CRIS' then 'Accident and Emergency'
--							when SourceContextCode = 'CMFT||ENVOY' then 'Accident and Emergency'
--							when SourceContextCode = 'CMFT||HOSP' then 'Inpatient Ward'
--							when SourceContextCode = 'TRA||PAS' then 'Accident and Emergency'
--							when SourceContextCode = 'TRA||ADAS' then 'Accident and Emergency'
--							when SourceContextCode = 'TRA||SYM' then 'Accident and Emergency'
--							when SourceContextCode = 'TRA||UG' then 'Inpatient Ward'
--							else 'Not Specified'
--							end
--							)	
--from
--	WH.Member

--left join
--	(

--	select
--			Location.LocationID
--			,Location.Location
--			,Location.LocationTypeID
--			,LocationType.LocationType							
								
--	from
--		(	
--		select --'CMFT||CRT'
--			LocationID
--			,Location
--			,LocationTypeID
--		from
--			[$(Warehouse)].PEX.KioskOnlinePostcardTabletLocation

--		union

--		select --'CMFT||HOSP'
--			LocationID
--			,Location
--			,LocationTypeID
--		from
--			[$(Warehouse)].PEX.BedUnitLocation	

--		union

--		select --'CMFT||ENVOY'
--			LocationID
--			,Location
--			,LocationTypeID
--		from
--			[$(Warehouse)].PEX.SMSLocation
			
--		) Location

--	inner join
--		[$(Warehouse)].PEX.LocationType
--	on	LocationType.LocationTypeID = Location.LocationTypeID

--	) Locations

--on	Member.SourceValueCode = cast(Locations.LocationID as varchar)


--where
--	AttributeCode = 'LOCATION'






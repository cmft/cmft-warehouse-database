﻿



CREATE view [BloodManagement].[FactDrugAPC] as

select
	 FactDrug.MergeRecno
	,FactDrug.APCMergeRecno
	,DrugLocationID = FactDrug.LocationID
	,DrugSpecialtyID = FactDrug.SpecialtyID
	,DrugConsultantID = FactDrug.ConsultantID
	,FactDrug.DrugID
	,FactDrug.UnitID
	,FactDrug.DrugAdministeredDateID
	,FactDrug.DrugAdministeredTimeID
	,FactDrug.Dose
	,Drugs = FactDrug.Cases

	,FactEncounter.EncounterDateID
	,FactEncounter.MetricID
	,FactEncounter.ConsultantID
	,FactEncounter.SpecialtyID
	,FactEncounter.SiteID
	,FactEncounter.AgeID
	,FactEncounter.AdmissionMethodID
	,FactEncounter.RTTActivity
	,FactEncounter.RTTTreated
	,FactEncounter.EncounterTimeOfDay
	,FactEncounter.DirectorateID
	,FactEncounter.SexID
	,FactEncounter.ISTAdmissionSpecialtyID
	,FactEncounter.ISTAdmissionDemandTimeOfDay
	,FactEncounter.ISTDischargeTimeOfDay
	,FactEncounter.ContextID
	,FactEncounter.AdmissionSourceID
	,FactEncounter.DischargeDestinationID
	,FactEncounter.DischargeMethodID
	,FactEncounter.IntendedManagementID
	,FactEncounter.NeonatalLevelOfCareID
	,FactEncounter.PatientClassificationID
	,FactEncounter.SubSpecialtyID
	,FactEncounter.CodingComplete
	,FactEncounter.VTE
	,FactEncounter.VTECategoryID
	,FactEncounter.VTEExclusionReasonID
	,FactEncounter.VTECompleteAndOrExclusion
	,FactEncounter.StartWardID
	,FactEncounter.EndWardID
	,FactEncounter.NationalExamID
	,FactEncounter.ServiceID
	,FactEncounter.CCGID
	,FactEncounter.PrimaryDiagnosisID
	,FactEncounter.PrimaryProcedureID
	,FactEncounter.CasenoteMergeRecno
	,FactEncounter.ResidenceCCGID
	,FactEncounter.MergeDocumentRecno
	,FactEncounter.DischargeSummaryStatusID
	,FactEncounter.DischargeSummarySignedByID
	,FactEncounter.DischargeSummaryProductionTimeID
	,FactEncounter.DischargeSummaryExclusionReasonID
	,FactEncounter.DischargeSummaryRequired
	,FactEncounter.DischargeSummarySignedTimeID
	,FactEncounter.GPPracticeID
	,FactEncounter.ElectronicCorrespondenceActivated
	,FactEncounter.HRGID
from
	BloodManagement.FactDrug

inner join APC.FactEncounter
on	FactEncounter.MergeEncounterRecno = FactDrug.APCMergeRecno







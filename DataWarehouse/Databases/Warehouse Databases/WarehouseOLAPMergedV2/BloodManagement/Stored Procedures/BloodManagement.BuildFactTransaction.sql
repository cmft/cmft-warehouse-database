﻿

CREATE procedure BloodManagement.BuildFactTransaction 
as

	
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare @MissingTimeID int =
	(
	select
		TimeBand.TimeBandCode
	from
		WH.TimeBand
	where
		TimeBand.MinuteBand = 'N/A'
	)

Truncate table BloodManagement.FactTransaction

insert into BloodManagement.FactTransaction
	(
	MergeRecno 
	,APCMergeRecno 
	,TransactionDateID 
	,TransactionTimeID 
	,TransactionTypeID 
	,ConditionID 
	,BloodProductID 
	,BloodGroupID 
	,ReservationDateID
	,ReservationTimeID 
	,ExpiryDateID 
	,FateID 
	,SystemUserID 

	,BloodProductVolume 
	,Cases 
	)

Select
	BaseTransaction.MergeRecno 
	,APCMergeRecno = Bridge.MergeEncounterRecno
	,TransactionDateID = coalesce
								(
								TransactionDate.DateID
								,NullDate.DateID
								)
	,TransactionTimeID = coalesce
								(
								TransactionTimeBand.TimeBandCode
								,@MissingTimeID
								)
	,Reference.TransactionTypeID 
	,Reference.ConditionID 
	,Reference.BloodProductID 
	,Reference.BloodGroupID 
	,ReservationDateID = coalesce(
								ReservationDate.DateID
								,NullDate.DateID
								)
	,ReservationTimeID = coalesce
								(
								ReservationTimeBand.TimeBandCode
								,@MissingTimeID
								)
	,ExpiryDateID = coalesce(
							ExpiryDate.DateID
							,NullDate.DateID
							)
	,Reference.FateID 
	,Reference.SystemUserID 

	,BaseTransaction.BloodProductVolume 
	,Cases = 1 
	
from
	BloodManagement.BaseTransaction

inner join BloodManagement.BaseTransactionReference Reference
on BaseTransaction.MergeRecno = Reference.MergeRecno

left join APC.BaseEncounterBaseTransaction Bridge
on BaseTransaction.MergeRecno = Bridge.MergeTransactionRecno

left join WH.Calendar TransactionDate
on	TransactionDate.TheDate = cast(BaseTransaction.TransactionTime as date)

left join WH.Calendar ReservationDate
on	ReservationDate.TheDate = cast(BaseTransaction.ReservationTime as date)

left join WH.Calendar ExpiryDate
on	ExpiryDate.TheDate = cast (BaseTransaction.ExpiryTime as date)

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'

left join WH.TimeBand TransactionTimeBand
on TransactionTimeBand.MinuteBand = left(cast(BaseTransaction.TransactionTime as time),5)

left join WH.TimeBand ReservationTimeBand
on ReservationTimeBand.MinuteBand = left(cast(BaseTransaction.ReservationTime as time),5)

	


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






﻿

Create procedure BloodManagement.BuildFactCellSalvage
as

	
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare @MissingTimeID int =
	(
	select
		TimeBand.TimeBandCode
	from
		WH.TimeBand
	where
		TimeBand.MinuteBand = 'N/A'
	)

Truncate table BloodManagement.FactCellSalvage

insert into BloodManagement.FactCellSalvage
	(
	MergeRecno 
	,APCMergeRecno 
	,LocationID 
	,SpecialtyID 
	,ConsultantID 
	,StartDateID 
	,StartTimeID 
	,EndDateID 
	,EndTimeID 
	
	,Duration
	,Dose 
	,Cases 
	
	)

Select
	BaseCellSalvage.MergeRecno 
	,APCMergeRecno = Bridge.MergeEncounterRecno
	,Reference.LocationID 
	,Reference.SpecialtyID 
	,Reference.ConsultantID 
	,StartDateID = coalesce
						(
						CellSalvageStartDate.DateID
						,NullDate.DateID
						)
	,StartTimeID = coalesce
						(
						StartTimeBand.TimeBandCode
						,@MissingTimeID
						) 
	,EndDateID = coalesce
						(
						CellSalvageEndDate.DateID
						,NullDate.DateID
						) 
	,EndTimeID = coalesce
						(
						EndTimeBand.TimeBandCode
						,@MissingTimeID
						)  
	
	,Duration
	,Dose 
	,Cases = 1
	
from
	BloodManagement.BaseCellSalvage

inner join BloodManagement.BaseCellSalvageReference Reference
on BaseCellSalvage.MergeRecno = Reference.MergeRecno

left join APC.BaseEncounterBaseCellSalvage Bridge
on BaseCellSalvage.MergeRecno = Bridge.MergeCellSalvageRecno

left join WH.Calendar CellSalvageStartDate
on	CellSalvageStartDate.TheDate = cast(BaseCellSalvage.StartTime as date)

left join WH.Calendar CellSalvageEndDate
on	CellSalvageEndDate.TheDate = cast(BaseCellSalvage.EndTime as date)

left join WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'

left join WH.TimeBand StartTimeBand
on StartTimeBand.MinuteBand = left(cast(BaseCellSalvage.StartTime as time),5)

left join WH.TimeBand EndTimeBand
on EndTimeBand.MinuteBand = left(cast(BaseCellSalvage.EndTime as time),5)

	


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






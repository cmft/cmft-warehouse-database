﻿
Create Procedure BloodManagement.BuildFactTransactionAPCEpisode
as

--	20150814	RR	Issue with spells and identifying first transfusion in spell in order to calculate transfusion rate correctly.
--					Seperated procs, 1 specifically for spells, 1 for episodes

Truncate table BloodManagement.FactTransactionAPCEpisode
;


With EpisodeMetricCTE
	(
	MetricID
	)
as
	(
	Select
		MetricID
	from
		APC.Metric
	where
		MetricCode not in 
		(
		-- Checks
		'CHK'
		,'CHKADM'
		,'CHKDIS'
		,'CHKFCE'
		,'CHKFFCE'
		,'CHKSPL'
		-- Spell metrics
		,'DCSPL'
		,'ELSPL'
		,'EMSPL'
		,'IESPL'
		,'MATSPL'
		,'NEOTHSPL'
		,'NESPL'
		,'RDSPL'
		,'RNSPL'
		,'SPL'
		)
	)		

Insert into BloodManagement.FactTransactionAPCEpisode
	(
	FirstInMetric
	, DonorOrSalvagedID
	, MergeRecno
	, APCMergeRecno
	, TransactionDateID
	, TransactionTimeID
	, TransactionTypeID
	, ConditionID
	, BloodProductID
	, BloodGroupID
	, ReservationDateID
	, ReservationTimeID
	, ExpiryDateID
	, FateID
	, SystemUserID
	, BloodProductVolume
	, Transactions
	, EncounterDateID
	, MetricID
	, ConsultantID
	, SpecialtyID
	, SiteID
	, AgeID
	, AdmissionMethodID
	, RTTActivity
	, RTTTreated
	, EncounterTimeOfDay
	, EndDirectorateID
	, SexID
	, ISTAdmissionSpecialtyID
	, ISTAdmissionDemandTimeOfDay
	, ISTDischargeTimeOfDay
	, ContextID
	, AdmissionSourceID
	, DischargeDestinationID
	, DischargeMethodID
	, IntendedManagementID
	, NeonatalLevelOfCareID
	, PatientClassificationID
	, SubSpecialtyID
	, CodingComplete
	, VTE
	, VTECategoryID
	, VTEExclusionReasonID
	, VTECompleteAndOrExclusion
	, StartWardID
	, EndWardID
	, NationalExamID
	, ServiceID
	, CCGID
	, PrimaryDiagnosisID
	, PrimaryProcedureID
	, Cases
	, LengthOfEncounter
	, CasenoteMergeRecno
	, ResidenceCCGID
	, MergeDocumentRecno
	, DischargeSummaryStatusID
	, DischargeSummarySignedByID
	, DischargeSummaryProductionTimeID
	, DischargeSummaryExclusionReasonID
	, DischargeSummaryRequired
	, DischargeSummarySignedTimeID
	, GPPracticeID
	, ElectronicCorrespondenceActivated
	, HRGID
	)
Select
	FirstInMetric
	, DonorOrSalvagedID
	, MergeRecno
	, APCMergeRecno
	, TransactionDateID
	, TransactionTimeID
	, TransactionTypeID
	, ConditionID
	, BloodProductID
	, BloodGroupID
	, ReservationDateID
	, ReservationTimeID
	, ExpiryDateID
	, FateID
	, SystemUserID
	, BloodProductVolume
	, Transactions
	, EncounterDateID
	, MetricID
	, ConsultantID
	, SpecialtyID
	, SiteID
	, AgeID
	, AdmissionMethodID
	, RTTActivity
	, RTTTreated
	, EncounterTimeOfDay
	, DirectorateID
	, SexID
	, ISTAdmissionSpecialtyID
	, ISTAdmissionDemandTimeOfDay
	, ISTDischargeTimeOfDay
	, ContextID
	, AdmissionSourceID
	, DischargeDestinationID
	, DischargeMethodID
	, IntendedManagementID
	, NeonatalLevelOfCareID
	, PatientClassificationID
	, SubSpecialtyID
	, CodingComplete
	, VTE
	, VTECategoryID
	, VTEExclusionReasonID
	, VTECompleteAndOrExclusion
	, StartWardID
	, EndWardID
	, NationalExamID
	, ServiceID
	, CCGID
	, PrimaryDiagnosisID
	, PrimaryProcedureID
	, Cases
	, LengthOfEncounter
	, CasenoteMergeRecno
	, ResidenceCCGID
	, MergeDocumentRecno
	, DischargeSummaryStatusID
	, DischargeSummarySignedByID
	, DischargeSummaryProductionTimeID
	, DischargeSummaryExclusionReasonID
	, DischargeSummaryRequired
	, DischargeSummarySignedTimeID
	, GPPracticeID
	, ElectronicCorrespondenceActivated
	, HRGID
from
	(
	select
		FirstInMetric =
			cast(
				case
					when not exists
						(
						Select
							1
						from
							BloodManagement.UnitFate
						where
							FactTransaction.FateID = UnitFate.SourceBloodUnitFateID
						and UnitFate.SourceBloodUnitFate = 'Transfused'
						)
					then 0
					when not exists
					(
					select
						1
					from
						BloodManagement.FactTransaction PreviousTransaction

					inner join APC.FactEncounter PreviousEncounter
					on	PreviousEncounter.MergeEncounterRecno = PreviousTransaction.APCMergeRecno
					and	PreviousEncounter.MetricID = FactEncounter.MetricID
					
					where
						PreviousTransaction.APCMergeRecno = FactTransaction.APCMergeRecno
					and	PreviousTransaction.FateID = FactTransaction.FateID
					and	UnitFate.SourceBloodUnitFate = 'Transfused'
					and	PreviousTransaction.MergeRecno < FactTransaction.MergeRecno
					)
				
				then 1
				else 0
				end

				as bit
			)

		,DonorOrSalvagedID =
			case
			when
				UnitFate.SourceBloodUnitFate = 'Transfused'
			and	exists
				(
				select
					1
				from
					BloodManagement.FactCellSalvage
				where
					FactCellSalvage.APCMergeRecno = FactTransaction.APCMergeRecno
				)
			then 3 --Both
			
			when
				UnitFate.SourceBloodUnitFate = 'Transfused'
			then 1 --Blood Transfusion
			
			--when
			--	exists
			--	(
			--	select
			--		1
			--	from
			--		BloodManagement.FactCellSalvage
			--	where
			--		FactCellSalvage.APCMergeRecno = FactTransaction.APCMergeRecno
			--	)
			--then 2 --Cell Salvage

			else 4 --N/A
			
			end

		,FactTransaction.MergeRecno
		,FactTransaction.APCMergeRecno
		,FactTransaction.TransactionDateID
		,FactTransaction.TransactionTimeID
		,FactTransaction.TransactionTypeID
		,FactTransaction.ConditionID
		,FactTransaction.BloodProductID
		,FactTransaction.BloodGroupID
		,FactTransaction.ReservationDateID
		,FactTransaction.ReservationTimeID
		,FactTransaction.ExpiryDateID
		,FactTransaction.FateID
		,FactTransaction.SystemUserID
		,FactTransaction.BloodProductVolume
		,Transactions = FactTransaction.Cases

		,FactEncounter.EncounterDateID
		,FactEncounter.MetricID
		,FactEncounter.ConsultantID
		,FactEncounter.SpecialtyID
		,FactEncounter.SiteID
		,FactEncounter.AgeID
		,FactEncounter.AdmissionMethodID
		,FactEncounter.RTTActivity
		,FactEncounter.RTTTreated
		,FactEncounter.EncounterTimeOfDay
		,FactEncounter.DirectorateID
		,FactEncounter.SexID
		,FactEncounter.ISTAdmissionSpecialtyID
		,FactEncounter.ISTAdmissionDemandTimeOfDay
		,FactEncounter.ISTDischargeTimeOfDay
		,FactEncounter.ContextID
		,FactEncounter.AdmissionSourceID
		,FactEncounter.DischargeDestinationID
		,FactEncounter.DischargeMethodID
		,FactEncounter.IntendedManagementID
		,FactEncounter.NeonatalLevelOfCareID
		,FactEncounter.PatientClassificationID
		,FactEncounter.SubSpecialtyID
		,FactEncounter.CodingComplete
		,FactEncounter.VTE
		,FactEncounter.VTECategoryID
		,FactEncounter.VTEExclusionReasonID
		,FactEncounter.VTECompleteAndOrExclusion
		,FactEncounter.StartWardID
		,FactEncounter.EndWardID
		,FactEncounter.NationalExamID
		,FactEncounter.ServiceID
		,FactEncounter.CCGID
		,FactEncounter.PrimaryDiagnosisID
		,FactEncounter.PrimaryProcedureID
		,FactEncounter.Cases
		,FactEncounter.LengthOfEncounter
		,FactEncounter.CasenoteMergeRecno
		,FactEncounter.ResidenceCCGID
		,FactEncounter.MergeDocumentRecno
		,FactEncounter.DischargeSummaryStatusID
		,FactEncounter.DischargeSummarySignedByID
		,FactEncounter.DischargeSummaryProductionTimeID
		,FactEncounter.DischargeSummaryExclusionReasonID
		,FactEncounter.DischargeSummaryRequired
		,FactEncounter.DischargeSummarySignedTimeID
		,FactEncounter.GPPracticeID
		,FactEncounter.ElectronicCorrespondenceActivated
		,FactEncounter.HRGID
	from
		EpisodeMetricCTE

	inner join APC.FactEncounter
	on FactEncounter.MetricID = EpisodeMetricCTE.MetricID

	inner join BloodManagement.FactTransaction
	on	FactEncounter.MergeEncounterRecno = FactTransaction.APCMergeRecno

	inner join BloodManagement.UnitFate
	on	UnitFate.SourceBloodUnitFateID = FactTransaction.FateID
	
	union all --Cell Salvage only

	select
		FirstInMetric =
			cast(
				case
					when not exists
						(
						select
							1
						from
							BloodManagement.FactCellSalvage PreviousTransaction
						where
							PreviousTransaction.APCMergeRecno = FactCellSalvage.APCMergeRecno
						and	PreviousTransaction.MergeRecno < FactCellSalvage.MergeRecno
						)
					then 1
					else 0
				end
			as bit
			)

		,DonorOrSalvagedID = 2 --Cell Salvage

		,FactCellSalvage.MergeRecno
		,FactCellSalvage.APCMergeRecno
		,FactCellSalvage.StartDateID
		,FactCellSalvage.StartTimeID
		,TransactionTypeID = (select top 1 TransactionType.SourceTransactionTypeID from BloodManagement.TransactionType where TransactionType.SourceTransactionTypeCode = '-1' order by TransactionType.SourceTransactionTypeID)
		,ConditionID = (select top 1 TransactionCondition.SourceTransactionConditionID from BloodManagement.TransactionCondition where TransactionCondition.SourceTransactionConditionCode = '-1' order by TransactionCondition.SourceTransactionConditionID)
		,BloodProductID = (select top 1 Product.SourceBloodProductID from BloodManagement.Product where Product.SourceBloodProductCode = '-1' order by Product.SourceBloodProductID)
		,BloodGroupID = (select top 1 UnitGroup.SourceBloodGroupID from BloodManagement.UnitGroup where UnitGroup.SourceBloodGroupCode = '-1' order by UnitGroup.SourceBloodGroupID)
		,ReservationDateID = (select DateID from WH.Calendar where Calendar.TheDate = '01 Jan 1900')
		,ReservationTimeID = -1
		,ExpiryDateID = (select DateID from WH.Calendar where Calendar.TheDate = '01 Jan 1900')
		,FateID = (select top 1 UnitFate.SourceBloodUnitFateID from BloodManagement.UnitFate where UnitFate.SourceBloodUnitFateCode = '-1' order by UnitFate.SourceBloodUnitFateID)
		,SystemUserID = (select top 1 SystemUser.SystemUserID from WH.SystemUser where SystemUser.UserID = '-1' order by SystemUser.SystemUserID)
		,FactCellSalvage.Dose
		,FactCellSalvage.Cases

		,FactEncounter.EncounterDateID
		,FactEncounter.MetricID
		,FactEncounter.ConsultantID
		,FactEncounter.SpecialtyID
		,FactEncounter.SiteID
		,FactEncounter.AgeID
		,FactEncounter.AdmissionMethodID
		,FactEncounter.RTTActivity
		,FactEncounter.RTTTreated
		,FactEncounter.EncounterTimeOfDay
		,FactEncounter.DirectorateID
		,FactEncounter.SexID
		,FactEncounter.ISTAdmissionSpecialtyID
		,FactEncounter.ISTAdmissionDemandTimeOfDay
		,FactEncounter.ISTDischargeTimeOfDay
		,FactEncounter.ContextID
		,FactEncounter.AdmissionSourceID
		,FactEncounter.DischargeDestinationID
		,FactEncounter.DischargeMethodID
		,FactEncounter.IntendedManagementID
		,FactEncounter.NeonatalLevelOfCareID
		,FactEncounter.PatientClassificationID
		,FactEncounter.SubSpecialtyID
		,FactEncounter.CodingComplete
		,FactEncounter.VTE
		,FactEncounter.VTECategoryID
		,FactEncounter.VTEExclusionReasonID
		,FactEncounter.VTECompleteAndOrExclusion
		,FactEncounter.StartWardID
		,FactEncounter.EndWardID
		,FactEncounter.NationalExamID
		,FactEncounter.ServiceID
		,FactEncounter.CCGID
		,FactEncounter.PrimaryDiagnosisID
		,FactEncounter.PrimaryProcedureID
		,FactEncounter.Cases
		,FactEncounter.LengthOfEncounter
		,FactEncounter.CasenoteMergeRecno
		,FactEncounter.ResidenceCCGID
		,FactEncounter.MergeDocumentRecno
		,FactEncounter.DischargeSummaryStatusID
		,FactEncounter.DischargeSummarySignedByID
		,FactEncounter.DischargeSummaryProductionTimeID
		,FactEncounter.DischargeSummaryExclusionReasonID
		,FactEncounter.DischargeSummaryRequired
		,FactEncounter.DischargeSummarySignedTimeID
		,FactEncounter.GPPracticeID
		,FactEncounter.ElectronicCorrespondenceActivated
		,FactEncounter.HRGID
	from
		EpisodeMetricCTE

	inner join APC.FactEncounter
	on FactEncounter.MetricID = EpisodeMetricCTE.MetricID
	
	inner join BloodManagement.FactCellSalvage
	on FactEncounter.MergeEncounterRecno = FactCellSalvage.APCMergeRecno

	where
		not exists
			(
			select
				1
			from
				BloodManagement.FactTransaction
			
			inner join BloodManagement.UnitFate
			on	UnitFate.SourceBloodUnitFateID = FactTransaction.FateID
			
			where
				FactTransaction.APCMergeRecno = FactCellSalvage.APCMergeRecno
			and	UnitFate.SourceBloodUnitFate = 'Transfused'
			)
	) B



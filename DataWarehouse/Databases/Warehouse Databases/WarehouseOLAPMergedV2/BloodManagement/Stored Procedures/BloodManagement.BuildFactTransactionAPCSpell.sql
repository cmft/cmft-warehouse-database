﻿
Create Procedure BloodManagement.BuildFactTransactionAPCSpell
as

--	20150814	RR	Issue with spells and identifying first transfusion in spell in order to calculate transfusion rate correctly.
--					the spell episode uses the admitting episode, therefore the first is only pulling if this occurs in the admitting episode, the first transfusion could be in the 4th or 5th episode
--					in order to pull the first in spell and not admitting episode, the query worked quicker when creating temp tables.		(when using CTE stopped after 13mins)



-- Create temp tables to use in the build fact

Select
	MetricID 
	,AdmittingMergeEncounterRecno 
	,APCMergeEncounterRecno 
	,GlobalProviderSpellNo 
	,GlobalEpisodeNo 
	,TransactionMergeRecno 
	,FateID 
into
	#SpellMetricTransactionCTE
from
	(
	Select distinct
		MetricID = FactEncounter.MetricID
		,AdmittingMergeEncounterRecno = FactEncounter.MergeEncounterRecno
		,APCMergeEncounterRecno = AllEpisodesInSpell.MergeEncounterRecno
		,GlobalProviderSpellNo = AllEpisodesInSpell.GlobalProviderSpellNo
		,GlobalEpisodeNo = AllEpisodesInSpell.GlobalEpisodeNo
		,TransactionMergeRecno = FactTransaction.MergeRecno
		,FateID = UnitFate.SourceBloodUnitFateID
	from
		APC.Metric

	inner join APC.FactEncounter -- Spell metrics relate to admitting episode and the admitting episode attributes, plus the discharge episode discharge date.
	on FactEncounter.MetricID = Metric.MetricID

	inner join APC.BaseEncounter AdmittingEpisode
	on FactEncounter.MergeEncounterRecno = AdmittingEpisode.MergeEncounterRecno

	inner join APC.BaseEncounter AllEpisodesInSpell
	on AdmittingEpisode.GlobalProviderSpellNo = AllEpisodesInSpell.GlobalProviderSpellNo

	inner join BloodManagement.FactTransaction
	on AllEpisodesInSpell.MergeEncounterRecno = FactTransaction.APCMergeRecno

	inner join BloodManagement.UnitFate
	on	UnitFate.SourceBloodUnitFateID = FactTransaction.FateID

	where
		MetricCode in 
		(
		'DCSPL'
		,'ELSPL'  -- ELSpl, NESPL and SPL are not in FactEncounter, this must be part of the hierarchy. We can pull the lower level, eg DC and El and the cube will aggregate them up to spell
		,'EMSPL'
		,'IESPL'
		,'MATSPL'
		,'NEOTHSPL'
		,'NESPL'
		,'RDSPL'
		,'RNSPL'
		,'SPL'
		)
	)FactTransaction



Select
	MetricID 
	,AdmittingMergeEncounterRecno 
	,APCMergeEncounterRecno
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,CellSalvageMergeRecno 
into
	#SpellMetricCellSalvageCTE
from
	(
	Select distinct
		MetricID = FactEncounter.MetricID
		,AdmittingMergeEncounterRecno = FactEncounter.MergeEncounterRecno
		,APCMergeEncounterRecno = AllEpisodesInSpell.MergeEncounterRecno
		,GlobalProviderSpellNo = AllEpisodesInSpell.GlobalProviderSpellNo
		,GlobalEpisodeNo = AllEpisodesInSpell.GlobalEpisodeNo
		,CellSalvageMergeRecno = FactCellSalvage.MergeRecno

	from
		APC.Metric

	inner join APC.FactEncounter -- Spell metrics relate to admitting episode and the admitting episode attributes, plus the discharge episode discharge date.
	on FactEncounter.MetricID = Metric.MetricID

	inner join APC.BaseEncounter AdmittingEpisode
	on FactEncounter.MergeEncounterRecno = AdmittingEpisode.MergeEncounterRecno

	inner join APC.BaseEncounter AllEpisodesInSpell
	on AdmittingEpisode.GlobalProviderSpellNo = AllEpisodesInSpell.GlobalProviderSpellNo

	inner join BloodManagement.FactCellSalvage
	on AllEpisodesInSpell.MergeEncounterRecno = FactCellSalvage.APCMergeRecno

	where
		MetricCode in 
		(
		'DCSPL'
		,'ELSPL'  -- ELSpl, NESPL and SPL are not in FactEncounter, this must be part of the hierarchy. We can pull the lower level, eg DC and El and the cube will aggregate them up to spell
		,'EMSPL'
		,'IESPL'
		,'MATSPL'
		,'NEOTHSPL'
		,'NESPL'
		,'RDSPL'
		,'RNSPL'
		,'SPL'
		)
	)CellSalvage

where
	not exists
	(
	select
		1
	from
		#SpellMetricTransactionCTE CTE
	
	inner join BloodManagement.UnitFate
	on CTE.FateID = UnitFate.SourceBloodUnitFateID
	
	where
		CTE.AdmittingMergeEncounterRecno = CellSalvage.AdmittingMergeEncounterRecno
	and	UnitFate.SourceBloodUnitFate = 'Transfused'
	)




-- Build FactTransactionAPCSpell

Truncate table BloodManagement.FactTransactionAPCSpell

Insert Into BloodManagement.FactTransactionAPCSpell
	(
	FirstInMetric
	, DonorOrSalvagedID
	, MergeRecno
	, APCMergeRecno
	, TransactionDateID
	, TransactionTimeID
	, TransactionTypeID
	, ConditionID
	, BloodProductID
	, BloodGroupID
	, ReservationDateID
	, ReservationTimeID
	, ExpiryDateID
	, FateID
	, SystemUserID
	, BloodProductVolume
	, Transactions
	, EncounterDateID
	, MetricID
	, ConsultantID
	, SpecialtyID
	, SiteID
	, AgeID
	, AdmissionMethodID
	, RTTActivity
	, RTTTreated
	, EncounterTimeOfDay
	, EndDirectorateID
	, SexID
	, ISTAdmissionSpecialtyID
	, ISTAdmissionDemandTimeOfDay
	, ISTDischargeTimeOfDay
	, ContextID
	, AdmissionSourceID
	, DischargeDestinationID
	, DischargeMethodID
	, IntendedManagementID
	, NeonatalLevelOfCareID
	, PatientClassificationID
	, SubSpecialtyID
	, CodingComplete
	, VTE
	, VTECategoryID
	, VTEExclusionReasonID
	, VTECompleteAndOrExclusion
	, StartWardID
	, EndWardID
	, NationalExamID
	, ServiceID
	, CCGID
	, PrimaryDiagnosisID
	, PrimaryProcedureID
	, Cases
	, LengthOfEncounter
	, CasenoteMergeRecno
	, ResidenceCCGID
	, MergeDocumentRecno
	, DischargeSummaryStatusID
	, DischargeSummarySignedByID
	, DischargeSummaryProductionTimeID
	, DischargeSummaryExclusionReasonID
	, DischargeSummaryRequired
	, DischargeSummarySignedTimeID
	, GPPracticeID
	, ElectronicCorrespondenceActivated
	, HRGID
	)
Select
	FirstInMetric
	, DonorOrSalvagedID
	, MergeRecno
	, APCMergeRecno
	, TransactionDateID
	, TransactionTimeID
	, TransactionTypeID
	, ConditionID
	, BloodProductID
	, BloodGroupID
	, ReservationDateID
	, ReservationTimeID
	, ExpiryDateID
	, FateID
	, SystemUserID
	, BloodProductVolume
	, Transactions
	, EncounterDateID
	, MetricID
	, ConsultantID
	, SpecialtyID
	, SiteID
	, AgeID
	, AdmissionMethodID
	, RTTActivity
	, RTTTreated
	, EncounterTimeOfDay
	, EndDirectorateID
	, SexID
	, ISTAdmissionSpecialtyID
	, ISTAdmissionDemandTimeOfDay
	, ISTDischargeTimeOfDay
	, ContextID
	, AdmissionSourceID
	, DischargeDestinationID
	, DischargeMethodID
	, IntendedManagementID
	, NeonatalLevelOfCareID
	, PatientClassificationID
	, SubSpecialtyID
	, CodingComplete
	, VTE
	, VTECategoryID
	, VTEExclusionReasonID
	, VTECompleteAndOrExclusion
	, StartWardID
	, EndWardID
	, NationalExamID
	, ServiceID
	, CCGID
	, PrimaryDiagnosisID
	, PrimaryProcedureID
	, Cases
	, LengthOfEncounter
	, CasenoteMergeRecno
	, ResidenceCCGID
	, MergeDocumentRecno
	, DischargeSummaryStatusID
	, DischargeSummarySignedByID
	, DischargeSummaryProductionTimeID
	, DischargeSummaryExclusionReasonID
	, DischargeSummaryRequired
	, DischargeSummarySignedTimeID
	, GPPracticeID
	, ElectronicCorrespondenceActivated
	, HRGID
from
	(
	select 
		FirstInMetric = 
			case 
				when not exists
					(
					Select
						1
					from
						BloodManagement.UnitFate
					where
						SpellMetricTransactionCTE.FateID = UnitFate.SourceBloodUnitFateID
					and UnitFate.SourceBloodUnitFate = 'Transfused'
					)
				then 0
				when not exists
					(
					Select
						1
					from
						#SpellMetricTransactionCTE Previous
					where
						Previous.AdmittingMergeEncounterRecno = SpellMetricTransactionCTE.AdmittingMergeEncounterRecno
					and Previous.MetricID = SpellMetricTransactionCTE.MetricID
					and Previous.FateID = SpellMetricTransactionCTE.FateID
					and Previous.FateID = UnitFate.SourceBloodUnitFateID
					and UnitFate.SourceBloodUnitFate = 'Transfused'
					and Previous.TransactionMergeRecno < SpellMetricTransactionCTE.TransactionMergeRecno
					)
				then 1
				else 0
			end

		,DonorOrSalvagedID =
			case
				when
					UnitFate.SourceBloodUnitFate = 'Transfused'
				and	exists
					(
					select
						1
					from
						BloodManagement.FactCellSalvage
					where
						FactCellSalvage.APCMergeRecno = FactTransaction.APCMergeRecno
					)
				then 3 --Both
				
				when
					UnitFate.SourceBloodUnitFate = 'Transfused'
				then 1 --Blood Transfusion
				
				--when
				--	exists
				--	(
				--	select
				--		1
				--	from
				--		BloodManagement.FactCellSalvage
				--	where
				--		FactCellSalvage.APCMergeRecno = FactTransaction.APCMergeRecno
				--	)
				--then 2 --Cell Salvage picked up in union

				else 4 --N/A
			
			end

		,FactTransaction.MergeRecno
		,APCMergeRecno = SpellMetricTransactionCTE.AdmittingMergeEncounterRecno
		,FactTransaction.TransactionDateID
		,FactTransaction.TransactionTimeID
		,FactTransaction.TransactionTypeID
		,FactTransaction.ConditionID
		,FactTransaction.BloodProductID
		,FactTransaction.BloodGroupID
		,FactTransaction.ReservationDateID
		,FactTransaction.ReservationTimeID
		,FactTransaction.ExpiryDateID
		,FactTransaction.FateID
		,FactTransaction.SystemUserID
		,FactTransaction.BloodProductVolume
		,Transactions = FactTransaction.Cases

		,FactEncounter.EncounterDateID
		,FactEncounter.MetricID
		,FactEncounter.ConsultantID
		,FactEncounter.SpecialtyID
		,FactEncounter.SiteID
		,FactEncounter.AgeID
		,FactEncounter.AdmissionMethodID
		,FactEncounter.RTTActivity
		,FactEncounter.RTTTreated
		,FactEncounter.EncounterTimeOfDay
		,FactEncounter.EndDirectorateID
		,FactEncounter.SexID
		,FactEncounter.ISTAdmissionSpecialtyID
		,FactEncounter.ISTAdmissionDemandTimeOfDay
		,FactEncounter.ISTDischargeTimeOfDay
		,FactEncounter.ContextID
		,FactEncounter.AdmissionSourceID
		,FactEncounter.DischargeDestinationID
		,FactEncounter.DischargeMethodID
		,FactEncounter.IntendedManagementID
		,FactEncounter.NeonatalLevelOfCareID
		,FactEncounter.PatientClassificationID
		,FactEncounter.SubSpecialtyID
		,FactEncounter.CodingComplete
		,FactEncounter.VTE
		,FactEncounter.VTECategoryID
		,FactEncounter.VTEExclusionReasonID
		,FactEncounter.VTECompleteAndOrExclusion
		,FactEncounter.StartWardID
		,FactEncounter.EndWardID
		,FactEncounter.NationalExamID
		,FactEncounter.ServiceID
		,FactEncounter.CCGID
		,FactEncounter.PrimaryDiagnosisID
		,FactEncounter.PrimaryProcedureID
		,FactEncounter.Cases
		,FactEncounter.LengthOfEncounter
		,FactEncounter.CasenoteMergeRecno
		,FactEncounter.ResidenceCCGID
		,FactEncounter.MergeDocumentRecno
		,FactEncounter.DischargeSummaryStatusID
		,FactEncounter.DischargeSummarySignedByID
		,FactEncounter.DischargeSummaryProductionTimeID
		,FactEncounter.DischargeSummaryExclusionReasonID
		,FactEncounter.DischargeSummaryRequired
		,FactEncounter.DischargeSummarySignedTimeID
		,FactEncounter.GPPracticeID
		,FactEncounter.ElectronicCorrespondenceActivated
		,FactEncounter.HRGID

	from
		#SpellMetricTransactionCTE SpellMetricTransactionCTE

	inner join APC.FactEncounter -- Spell metrics relate to admitting episode and the admitting episode attributes, plus the discharge episode discharge date.
	on FactEncounter.MergeEncounterRecno = SpellMetricTransactionCTE.AdmittingMergeEncounterRecno
	and FactEncounter.MetricID = SpellMetricTransactionCTE.MetricID

	inner join BloodManagement.FactTransaction
	on	SpellMetricTransactionCTE.TransactionMergeRecno = FactTransaction.MergeRecno

	inner join BloodManagement.UnitFate
	on	UnitFate.SourceBloodUnitFateID = FactTransaction.FateID



	union all --Cell Salvage only

	select
		FirstInMetric =
			cast(
				case
					when not exists
						(
						select
							1
						from
							#SpellMetricCellSalvageCTE Previous
						where
							Previous.AdmittingMergeEncounterRecno = CellSalvageCTE.AdmittingMergeEncounterRecno
						and Previous.CellSalvageMergeRecno < CellSalvageCTE.CellSalvageMergeRecno
						)
					then 1
					else 0
				end

				as bit
				)
		,DonorOrSalvagedID = 2 --Cell Salvage
		,FactCellSalvage.MergeRecno
		,FactCellSalvage.APCMergeRecno
		,FactCellSalvage.StartDateID
		,FactCellSalvage.StartTimeID
		,TransactionTypeID = (select top 1 TransactionType.SourceTransactionTypeID from BloodManagement.TransactionType where TransactionType.SourceTransactionTypeCode = '-1' order by TransactionType.SourceTransactionTypeID)
		,ConditionID = (select top 1 TransactionCondition.SourceTransactionConditionID from BloodManagement.TransactionCondition where TransactionCondition.SourceTransactionConditionCode = '-1' order by TransactionCondition.SourceTransactionConditionID)
		,BloodProductID = (select top 1 Product.SourceBloodProductID from BloodManagement.Product where Product.SourceBloodProductCode = '-1' order by Product.SourceBloodProductID)
		,BloodGroupID = (select top 1 UnitGroup.SourceBloodGroupID from BloodManagement.UnitGroup where UnitGroup.SourceBloodGroupCode = '-1' order by UnitGroup.SourceBloodGroupID)
		,ReservationDateID = (select DateID from WH.Calendar where Calendar.TheDate = '01 Jan 1900')
		,ReservationTimeID = -1
		,ExpiryDateID = (select DateID from WH.Calendar where Calendar.TheDate = '01 Jan 1900')
		,FateID = (select top 1 UnitFate.SourceBloodUnitFateID from BloodManagement.UnitFate where UnitFate.SourceBloodUnitFateCode = '-1' order by UnitFate.SourceBloodUnitFateID)
		,SystemUserID = (select top 1 SystemUser.SystemUserID from WH.SystemUser where SystemUser.UserID = '-1' order by SystemUser.SystemUserID)
		,FactCellSalvage.Dose
		,FactCellSalvage.Cases

		,FactEncounter.EncounterDateID
		,FactEncounter.MetricID
		,FactEncounter.ConsultantID
		,FactEncounter.SpecialtyID
		,FactEncounter.SiteID
		,FactEncounter.AgeID
		,FactEncounter.AdmissionMethodID
		,FactEncounter.RTTActivity
		,FactEncounter.RTTTreated
		,FactEncounter.EncounterTimeOfDay
		,FactEncounter.EndDirectorateID
		,FactEncounter.SexID
		,FactEncounter.ISTAdmissionSpecialtyID
		,FactEncounter.ISTAdmissionDemandTimeOfDay
		,FactEncounter.ISTDischargeTimeOfDay
		,FactEncounter.ContextID
		,FactEncounter.AdmissionSourceID
		,FactEncounter.DischargeDestinationID
		,FactEncounter.DischargeMethodID
		,FactEncounter.IntendedManagementID
		,FactEncounter.NeonatalLevelOfCareID
		,FactEncounter.PatientClassificationID
		,FactEncounter.SubSpecialtyID
		,FactEncounter.CodingComplete
		,FactEncounter.VTE
		,FactEncounter.VTECategoryID
		,FactEncounter.VTEExclusionReasonID
		,FactEncounter.VTECompleteAndOrExclusion
		,FactEncounter.StartWardID
		,FactEncounter.EndWardID
		,FactEncounter.NationalExamID
		,FactEncounter.ServiceID
		,FactEncounter.CCGID
		,FactEncounter.PrimaryDiagnosisID
		,FactEncounter.PrimaryProcedureID
		,FactEncounter.Cases
		,FactEncounter.LengthOfEncounter
		,FactEncounter.CasenoteMergeRecno
		,FactEncounter.ResidenceCCGID
		,FactEncounter.MergeDocumentRecno
		,FactEncounter.DischargeSummaryStatusID
		,FactEncounter.DischargeSummarySignedByID
		,FactEncounter.DischargeSummaryProductionTimeID
		,FactEncounter.DischargeSummaryExclusionReasonID
		,FactEncounter.DischargeSummaryRequired
		,FactEncounter.DischargeSummarySignedTimeID
		,FactEncounter.GPPracticeID
		,FactEncounter.ElectronicCorrespondenceActivated
		,FactEncounter.HRGID
	from
		#SpellMetricCellSalvageCTE CellSalvageCTE
		
	inner join APC.FactEncounter
	on CellSalvageCTE.AdmittingMergeEncounterRecno = FactEncounter.MergeEncounterRecno
	and CellSalvageCTE.MetricID = FactEncounter.MetricID

	inner join BloodManagement.FactCellSalvage
	on CellSalvageCTE.CellSalvageMergeRecno = FactCellSalvage.MergeRecno
	
	)A





﻿

CREATE procedure [BloodManagement].[BuildFactDrug]
as

      
declare
      @StartTime datetime = getdate()
      ,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
      ,@Elapsed int
      ,@Stats varchar(max)


declare @MissingTimeID int =
      (
      select
            TimeBand.TimeBandCode
      from
            WH.TimeBand
      where
            TimeBand.MinuteBand = 'N/A'
      )

Truncate table BloodManagement.FactDrug

insert into BloodManagement.FactDrug
      (
      MergeRecno 
      ,APCMergeRecno 
      ,LocationID 
      ,SpecialtyID 
      ,ConsultantID 
      ,DrugID 
      ,UnitID 
      ,DrugAdministeredDateID 
      ,DrugAdministeredTimeID 
      
      ,Dose 
      ,Cases 
      )

Select
      BaseDrug.MergeRecno 
      ,APCMergeRecno = Bridge.MergeEncounterRecno
      ,Reference.LocationID 
      ,Reference.SpecialtyID 
      ,Reference.ConsultantID 
      ,Reference.DrugID 
      ,Reference.UnitID 
      ,DrugAdministeredDateID = coalesce
                                    (
                                    DrugAdministeredDate.DateID
                                    ,NullDate.DateID
                                    )
      ,DrugAdministeredTimeID = coalesce
                                    (
                                    DrugTimeBand.TimeBandCode
                                    ,@MissingTimeID
                                    ) 
      
      ,Dose 
      ,Cases = 1
      
from
      BloodManagement.BaseDrugAdministered BaseDrug

inner join BloodManagement.BaseDrugAdministeredReference Reference
on BaseDrug.MergeRecno = Reference.MergeRecno

left join APC.BaseEncounterBaseDrugAdministered Bridge
on BaseDrug.MergeRecno = Bridge.MergeDrugRecno

left join WH.Calendar DrugAdministeredDate
on    DrugAdministeredDate.TheDate = cast(BaseDrug.DrugAdministeredTime as date)

left join WH.Calendar NullDate
on    NullDate.TheDate = '1 jan 1900'

left join WH.TimeBand DrugTimeBand
on DrugTimeBand.MinuteBand = left(cast(BaseDrug.DrugAdministeredTime as time),5)


select
      @Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
      @Stats =
            'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
      @ProcedureName
      ,@Stats




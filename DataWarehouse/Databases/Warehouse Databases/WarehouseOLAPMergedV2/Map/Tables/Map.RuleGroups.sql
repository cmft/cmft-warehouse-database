﻿CREATE TABLE [Map].[RuleGroups] (
    [RuleGroupId] INT            IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (200) NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_RuleGroups] PRIMARY KEY CLUSTERED ([RuleGroupId] ASC)
);


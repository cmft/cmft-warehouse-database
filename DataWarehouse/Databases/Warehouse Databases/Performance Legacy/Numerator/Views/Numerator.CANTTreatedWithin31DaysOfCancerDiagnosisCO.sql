﻿
CREATE view Numerator.CANTTreatedWithin31DaysOfCancerDiagnosisCO as

-- Based on SP CancerRegister.dbo.RPT_DEC_TREAT

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.TreatmentDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CANTTreatedWithin31DaysOfCancerDiagnosisCO'
	
where DatasetCode = 'CANT'
and TreatmentEventTypeCode in ('01','07')
and TumourStatusCode not in ('3', '6', '7', '8', '9')
and (InappropriateReferralCode <> 1 or InappropriateReferralCode is null) 

and 
(
	coalesce(HistologyCode,'') not in ('M80903','M80913','M80923','M80933','M80943','M80953','M81103') --Exclude basal cell carcinoma
	and -- Exclude Carcinoma in Situ if not breast DO5
		case 
			when PrimaryDiagnosisCode = 'D05' and HistologyCode = 'M80102' 
			then 1
			when coalesce(HistologyCode,'') = 'M80102'
			then 0
			else 1
		end = 1	
)

and	CancerSite <> 'Spinal Cord Compression'

and 
(
	PrimaryDiagnosisCode = 'D05' or coalesce(PrimaryDiagnosisCode,'') not like 'D%'
)

and
(
	-- Calculate adjusted days between decision to treat and treatment
	-- If this is less than 0, use 0
	case 
		when datediff(d,DecisionToTreatDate,TreatmentDate) - 
			(
				case 
					when TreatmentSettingCode in ('01','02') and DecisionToTreatAdjustmentReasonCode = 8 
					then coalesce(DecisionToTreatAdjustment,0) 
					else 0 
				end
			) > 0
		then datediff(d,DecisionToTreatDate,TreatmentDate) -  
			(
				case 
					when TreatmentSettingCode in ('01','02') and DecisionToTreatAdjustmentReasonCode = 8 
					then coalesce(DecisionToTreatAdjustment,0) 
					else 0 
				end
			) 
		else 0
	end
) <= 31 -- Must be less than or equal to 31

and CancerSite = 'Colorectal'
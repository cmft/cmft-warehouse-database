﻿CREATE procedure [dbo].[BuildCANTSubsequentDrugTreatmentWithin31Days](@type char(2), @site varchar(100)) as

-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CANT Patient receiving subsequent anti cancer drug regimen treatment < 31 days - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CANT Patient receiving subsequent anti cancer drug regimen treatment < 31 days - ' + @type, 'Numerator.CANTSubsequentDrugTreatmentWithin31Days' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Denominator record, if it does not exist
declare @DenominatorID int = (select DenominatorID from Denominator where Denominator = 'CANT Patient receiving subsequent anti cancer drug regimen treatment - ' + @type)

if @DenominatorID is null
begin
	insert into Denominator (Denominator, DenominatorLogic)
	values ('CANT Patient receiving subsequent anti cancer drug regimen treatment - ' + @type, 'Denominator.CANTSubsequentDrugTreatment' + @type)
	select @DenominatorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = 
(select ItemID from Item 
	where Item = 'Patients receiving subsequent treatment for cancer within 31 days where that treatment is an Anti Cancer Drug Regimen - ' + @site)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Patients receiving subsequent treatment for cancer within 31 days where that treatment is an Anti Cancer Drug Regimen - ' + @site
		,2, @NumeratorID, @DenominatorID, 1, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = @DenominatorID where ItemID = @ItemID
end


-- Create view DenominatorCANTSubsequentDrugTreatment with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Denominator.CANTSubsequentDrugTreatment' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)
	
declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_SUB_TREAT_MODALITY

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = ''' + @view + '''
	
where Dataset.DatasetCode = ''CANT''
and D.TreatmentEventTypeCode not in (''01'',''07'') -- Exclude first treatment
and (D.InappropriateReferralCode <> 1 or D.InappropriateReferralCode is null) 
and D.InitialTreatmentCode in (''04'',''05'',''06'',''13'') -- Drug regimen
and not
(
	coalesce(D.PrimaryDiagnosisCode,'''') = ''C44''
	and left(coalesce(D.HistologyCode,''''),6) in (''M80903'',''M80913'',''M80923'',''M80933'',''M80943'',''M80953'',''M81103'')
)
and not
(
	coalesce(D.PrimaryDiagnosisCode,'''') <> ''D05''
	and left(coalesce(D.PrimaryDiagnosisCode,''''),1) = ''D''	
)

and CancerSite = ''' + @site + ''''

exec(@sql)


-- Create view NumeratorCANTSubsequentDrugTreatmentWithin31Days with a suffix for the given CancerTypeCode

set @view = 'Numerator.CANTSubsequentDrugTreatmentWithin31Days' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)
	
set @sql = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_SUB_TREAT_MODALITY

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where Dataset.DatasetCode = ''CANT''
and D.TreatmentEventTypeCode not in (''01'',''07'') -- Exclude first treatment
and (D.InappropriateReferralCode <> 1 or D.InappropriateReferralCode is null) 
and D.InitialTreatmentCode in (''04'',''05'',''06'',''13'') -- Drug regimen
and not
(
	coalesce(D.PrimaryDiagnosisCode,'''') = ''C44''
	and left(coalesce(D.HistologyCode,''''),6) in (''M80903'',''M80913'',''M80923'',''M80933'',''M80943'',''M80953'',''M81103'')
)
and not
(
	coalesce(D.PrimaryDiagnosisCode,'''') <> ''D05''
	and left(coalesce(D.PrimaryDiagnosisCode,''''),1) = ''D''	
)

and
(
	datediff(d,D.DecisionToTreatDate,D.TreatmentDate) - 
	(
		case 
			when D.AdjustmentReasonCode = ''8'' and D.TreatmentSettingCode in (''01'',''02'') 
			then coalesce(D.WaitingTimeAdjustment,0) 
			else 0 
		end
	) <= 31
)

and CancerSite = ''' + @site + ''''

exec(@sql)




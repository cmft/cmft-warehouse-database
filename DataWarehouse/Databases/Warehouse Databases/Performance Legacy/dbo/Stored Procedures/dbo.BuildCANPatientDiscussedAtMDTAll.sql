﻿create procedure [dbo].[BuildCANPatientDiscussedAtMDTAll] as

exec BuildCANPatientDiscussedAtMDT 'CO', 'Colorectal'
exec BuildCANPatientDiscussedAtMDT 'GY', 'Gynaecology'
exec BuildCANPatientDiscussedAtMDT 'HA', 'Haematology'
exec BuildCANPatientDiscussedAtMDT 'HN', 'Head and Neck'
exec BuildCANPatientDiscussedAtMDT 'LU', 'Lung'
exec BuildCANPatientDiscussedAtMDT 'OT', 'Other'
exec BuildCANPatientDiscussedAtMDT 'PA', 'Paediatric'
exec BuildCANPatientDiscussedAtMDT 'SA', 'Sarcoma'
exec BuildCANPatientDiscussedAtMDT 'SK', 'Skin'
exec BuildCANPatientDiscussedAtMDT 'UG', 'Upper GI'
exec BuildCANPatientDiscussedAtMDT 'UR', 'Urology'

﻿CREATE ROLE [CMFT Analyst]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\james.watson';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\ian.howarth';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Ian.Connolly';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\joanna.ashberry';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\samaira.akram';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\sec - sp information';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'THT\KBurns';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\gareth.jones';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\adam.black';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'adamblack';


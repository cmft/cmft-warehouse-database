﻿CREATE ROLE [Community]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Community', @membername = N'CMMC\sec - Sharepoint Central Intelligence - Community PID';


GO
EXECUTE sp_addrolemember @rolename = N'Community', @membername = N'CMMC\Infounit members';


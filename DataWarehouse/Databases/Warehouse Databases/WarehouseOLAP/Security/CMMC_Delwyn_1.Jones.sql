﻿CREATE LOGIN [CMMC\Delwyn.Jones]
    FROM WINDOWS WITH DEFAULT_DATABASE = [WarehouseReportingMerged], DEFAULT_LANGUAGE = [us_english];


GO
ALTER LOGIN [CMMC\Delwyn.Jones] DISABLE;


﻿


CREATE procedure [HAN].[BuildFactServiceRequest] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


truncate table HAN.FactServiceRequest


insert into	HAN.FactServiceRequest
(
	 EncounterRecno
	,SourceUniqueID
	,StatusID
	,CallTypeID
	,SituationID
	,RaisedTimeOfDay
	,NightShiftDate
	,ClinicalRoleRequestedID
	,ClinicalRoleAllocatedID
	,ClinicalRoleAllocatedDate
	,ClinicalRoleAllocatedTimeOfDay
	,OutcomeID
	,ClosedByClinicalRoleID
	,WardID
	,SexCode
	,Cases
)


SELECT
	 EncounterRecno
	,SourceUniqueID
	,StatusID
	,CallTypeID
	,SituationID
	,RaisedTimeOfDay
	,NightShiftDate
	,ClinicalRoleRequestedID
	,ClinicalRoleAllocatedID
	,ClinicalRoleAllocatedDate
	,ClinicalRoleAllocatedTimeOfDay
	,OutcomeID
	,ClosedByClinicalRoleID
	,WardID
	,SexCode
	,Cases
FROM
	HAN.BaseServiceRequest



select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		--', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		--', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





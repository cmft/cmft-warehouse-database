﻿


CREATE procedure [HAN].[BuildBaseServiceRequest] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


truncate table HAN.BaseServiceRequest

INSERT INTO HAN.BaseServiceRequest
(
	 EncounterRecno
	,SourceUniqueID
	,RaisedBy
	,ContactTelephone
	,ContactBleep
	,EmailAddress
	,StatusID
	,CallTypeID
	,SituationID
	,SituationNotes
	,SituationHistory
	,WorkGroupID
	,PersonID
	,ActionRequestID
	,UrgencyID
	,SpecialistTypeID
	,RaisedTime
	,RaisedDate
	,RaisedTimeOfDay
	,NightShiftDate
	,ClinicalRoleRequestedID
	,ClinicalRoleAllocatedID
	,ClinicalRoleAllocatedTime
	,ClinicalRoleAllocatedDate
	,ClinicalRoleAllocatedTimeOfDay 
	,OutcomeID
	,ClosedByClinicalRoleID
	,ClosedNotes
	,CasenoteNumber
	,NHSNumber
	,DistrictNo
	,InpatientEpisode
	,PatientName
	,DateOfBirth
	,SexCode
	,WardID
	,BedManSourceUniqueID
	,Cases
)



SELECT
	 EncounterRecno
	,SourceUniqueID
	,RaisedBy
	,ContactTelephone
	,ContactBleep
	,EmailAddress
	,StatusID = coalesce(StatusID , -1)
	,CallTypeID = coalesce(CallTypeID , -1)
	,SituationID = coalesce(SituationID , -1)
	,SituationNotes
	,SituationHistory
	,WorkGroupID
	,PersonID
	,ActionRequestID
	,UrgencyID
	,SpecialistTypeID
	,RaisedTime = coalesce(RaisedTime , '1 Jan 1900')
	,RaisedDate = coalesce(RaisedDate , '1 Jan 1900')

	,RaisedTimeOfDay =
		coalesce(
			datediff(minute , RaisedDate , RaisedTime)
				,-1
		)

	,NightShiftDate =
		case
		when datediff(minute , RaisedDate , RaisedTime) between 0 and ((14 * 60) + 14) then dateadd(day , -1 , RaisedDate)
		else RaisedDate
		end

	,ClinicalRoleRequestedID = coalesce(ClinicalRoleRequestedID , -1)
	,ClinicalRoleAllocatedID = coalesce(ClinicalRoleAllocatedID , -1)
	,ClinicalRoleAllocatedTime = coalesce(ClinicalRoleAllocatedTime , '1 Jan 1900')
	,ClinicalRoleAllocatedDate = coalesce(ClinicalRoleAllocatedDate , '1 Jan 1900')

	,ClinicalRoleAllocatedTimeOfDay =
		coalesce(
			datediff(minute , ClinicalRoleAllocatedDate , ClinicalRoleAllocatedTime)
			,-1
		)

	,OutcomeID = coalesce(OutcomeID , -1)
	,ClosedByClinicalRoleID = coalesce(ClosedByClinicalRoleID , -1)
	,ClosedNotes
	,CasenoteNumber
	,NHSNumber
	,DistrictNo
	,InpatientEpisode
	,PatientName
	,DateOfBirth

	,SexCode =
		case SexCode
		when 'M' then '1'
		when 'F' then '2'
		when 'U' then '0'
		when 'I' then '9'
		else '#'
		end
		
	,WardID = coalesce(Ward.WardID , -1)
	,BedManSourceUniqueID
	,Cases = 1
FROM
	Warehouse.HAN.ServiceRequest

left join Warehouse.HAN.Ward
on	Ward.PASWardCode = ServiceRequest.PASWardCode

select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		--', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		--', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





﻿CREATE TABLE [HAN].[FactServiceRequest] (
    [EncounterRecno]                 INT         NOT NULL,
    [SourceUniqueID]                 INT         NOT NULL,
    [StatusID]                       INT         NOT NULL,
    [CallTypeID]                     INT         NOT NULL,
    [SituationID]                    INT         NOT NULL,
    [RaisedTimeOfDay]                INT         NOT NULL,
    [NightShiftDate]                 DATE        NOT NULL,
    [ClinicalRoleRequestedID]        INT         NOT NULL,
    [ClinicalRoleAllocatedID]        INT         NOT NULL,
    [ClinicalRoleAllocatedDate]      DATE        NOT NULL,
    [ClinicalRoleAllocatedTimeOfDay] INT         NOT NULL,
    [OutcomeID]                      INT         NOT NULL,
    [ClosedByClinicalRoleID]         INT         NOT NULL,
    [WardID]                         INT         NOT NULL,
    [SexCode]                        VARCHAR (1) NOT NULL,
    [Cases]                          TINYINT     NULL
);


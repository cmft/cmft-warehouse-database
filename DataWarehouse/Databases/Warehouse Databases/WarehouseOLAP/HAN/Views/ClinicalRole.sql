﻿


CREATE view [HAN].[ClinicalRole] as
select
	 ClinicalRole.ClinicalRoleID
	,ClinicalRole.ClinicalRole
	,ClinicalRole.ClinicalCategoryID
	,ClinicalCategory.ClinicalCategory
	,ClinicalRole.Active
from
	Warehouse.HAN.ClinicalRole

left join Warehouse.HAN.ClinicalCategory
on	ClinicalCategory.ClinicalCategoryID = ClinicalRole.ClinicalCategoryID

union

select
	 -1
	,'N/A'
	,-1
	,'N/A'
	,1



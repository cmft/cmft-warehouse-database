﻿


CREATE view [HAN].[Outcome] as
select
	 OutcomeID
	,Outcome
	,DisplayOrder = coalesce(DisplayOrder , 999)
	,Active
from
	Warehouse.HAN.Outcome

union

select
	-1
	,'N/A'
	,999
	,1


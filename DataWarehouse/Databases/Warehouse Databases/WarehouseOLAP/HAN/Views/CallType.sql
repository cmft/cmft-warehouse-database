﻿


CREATE view [HAN].[CallType] as
select
	 CallTypeID
	,CallType
	,Active
from
	Warehouse.HAN.CallType

union

select
	 -1
	,'N/A'
	,1



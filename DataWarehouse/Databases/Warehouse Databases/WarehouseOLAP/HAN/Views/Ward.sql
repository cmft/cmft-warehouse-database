﻿



CREATE view [HAN].[Ward] as
select
	 Ward.WardID
	,Ward.PASWardCode
	,Ward.Ward
	,Directorate = coalesce(Directorate.Directorate , 'N/A')
	,Division = coalesce(Division.Division , 'N/A')
from
	Warehouse.HAN.Ward

left join Warehouse.HAN.PASWardDirectorate
on	PASWardDirectorate.PASWardCode = Ward.PASWardCode

left join Warehouse.WH.Directorate
on	Directorate.DirectorateCode = PASWardDirectorate.DirectorateCode

left join Warehouse.WH.Division
on	Division.DivisionCode = Directorate.DivisionCode

union

select
	 -1
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'



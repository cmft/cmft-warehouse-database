﻿



CREATE view [HAN].[Situation] as
select
	 Situation.SituationID
	,Situation.Situation
	--,SituationGroupID = Situation.SituationParentID
	,Situation.SituationParentID
	--,SituationGroup = ParentSituation.Situation
	,Situation.Active
from
	Warehouse.HAN.Situation

--left join Warehouse.HAN.Situation ParentSituation
--on	ParentSituation.SituationID = Situation.SituationParentID

--where
--	Situation.SituationParentID is not null

--union

--select
--	 -1
--	,'N/A'
--	,-1
--	,'N/A'
--	,1




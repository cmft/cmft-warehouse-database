﻿CREATE TABLE [OCMTest].[CreatinineNationalAlgorithm] (
    [ResultRecNo]      INT              NULL,
    [ResultTime]       SMALLDATETIME    NULL,
    [SourcePatientNo]  INT              NOT NULL,
    [MergeEpisodeNo]   INT              NULL,
    [Age]              INT              NULL,
    [AgeCode]          VARCHAR (10)     NULL,
    [C1Result]         INT              NULL,
    [RatioDescription] VARCHAR (15)     NOT NULL,
    [ResultForRatio]   INT              NULL,
    [RVRatio]          DECIMAL (31, 19) NULL,
    [Alert]            VARCHAR (5)      NULL,
    [DateOfBirth]      DATE             NULL,
    [DistrictNo]       NVARCHAR (14)    NULL,
    [Surname]          NVARCHAR (24)    NULL,
    [Forenames]        NVARCHAR (20)    NULL,
    [Sexcode]          NVARCHAR (1)     NULL,
    [EthnicOriginCode] NVARCHAR (4)     NULL,
    [DeathIndicator]   VARCHAR (3)      NULL,
    [DateOfDeath]      DATE             NULL
);


﻿CREATE TABLE [OCMTest].[Result] (
    [ResultRecno]                     INT           NULL,
    [SourceUniqueID]                  VARCHAR (50)  NULL,
    [SourcePatientNo]                 INT           NOT NULL,
    [SequenceNo]                      BIGINT        NULL,
    [SourceEpisodeNo]                 INT           NULL,
    [EpisodeNoType]                   VARCHAR (14)  NOT NULL,
    [MergeEpisodeNo]                  INT           NULL,
    [APCAdmittingMergeEncounterRecno] INT           NULL,
    [Result]                          INT           NULL,
    [ResultDate]                      DATE          NULL,
    [ResultTime]                      SMALLDATETIME NULL,
    [DateOfBirth]                     DATE          NULL,
    [DistrictNo]                      NVARCHAR (14) NULL,
    [Surname]                         NVARCHAR (24) NULL,
    [Forenames]                       NVARCHAR (20) NULL,
    [Sexcode]                         NVARCHAR (1)  NULL,
    [EthnicOriginCode]                NVARCHAR (4)  NULL,
    [DeathIndicator]                  VARCHAR (3)   NULL,
    [DateOfDeath]                     DATE          NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CodedResult_1]
    ON [OCMTest].[Result]([SourcePatientNo] ASC, [ResultTime] ASC, [SequenceNo] ASC);


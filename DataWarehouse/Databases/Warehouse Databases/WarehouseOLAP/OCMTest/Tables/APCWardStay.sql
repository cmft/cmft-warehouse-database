﻿CREATE TABLE [OCMTest].[APCWardStay] (
    [SourcePatientNo] INT           NULL,
    [WardCode]        VARCHAR (10)  NULL,
    [StartDate]       SMALLDATETIME NOT NULL,
    [StartTime]       SMALLDATETIME NULL,
    [EndDate]         SMALLDATETIME NULL,
    [EndTime]         SMALLDATETIME NULL,
    [RenalWardStay]   INT           NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_APCWardStay_1]
    ON [OCMTest].[APCWardStay]([SourcePatientNo] ASC, [EndTime] ASC);


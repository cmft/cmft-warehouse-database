﻿CREATE TABLE [OCMTest].[APCEncounter] (
    [MergeEncounterRecno]         BIGINT        NOT NULL,
    [SourcePatientNo]             INT           NULL,
    [SourceSpellNo]               VARCHAR (20)  NULL,
    [SourceUniqueID]              VARCHAR (50)  NULL,
    [AdmissionDate]               SMALLDATETIME NULL,
    [PatientForename]             VARCHAR (80)  NULL,
    [PatientSurname]              VARCHAR (80)  NULL,
    [DistrictNo]                  VARCHAR (50)  NULL,
    [CasenoteNumber]              VARCHAR (20)  NULL,
    [DateOfBirth]                 DATETIME      NULL,
    [EpisodeStartDate]            SMALLDATETIME NULL,
    [EpisodeStartTime]            SMALLDATETIME NULL,
    [EthnicOriginCode]            VARCHAR (10)  NULL,
    [SexCode]                     VARCHAR (1)   NULL,
    [ConsultantCode]              VARCHAR (50)  NULL,
    [SpecialtyCode]               VARCHAR (10)  NULL,
    [DischargeDate]               SMALLDATETIME NULL,
    [DischargeMethodCode]         VARCHAR (10)  NULL,
    [NationalDischargeMethodCode] VARCHAR (1)   NULL,
    [StartWardTypeCode]           VARCHAR (10)  NULL,
    [PatientCategoryCode]         VARCHAR (2)   NULL,
    [RenalPatient]                BIT           NULL
);


GO
CREATE CLUSTERED INDEX [IX_APCEncounter_1]
    ON [OCMTest].[APCEncounter]([SourcePatientNo] ASC, [EpisodeStartTime] ASC);


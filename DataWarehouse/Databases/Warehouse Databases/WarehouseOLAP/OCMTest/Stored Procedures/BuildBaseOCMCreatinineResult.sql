﻿--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE procedure OCMTest.[BuildBaseOCMCreatinineResult] as

declare @cutoffDate smalldatetime = '30 Jul 2012'

select
	 EncounterRecno = max(CodedResult.EncounterRecno) 
	,SourceUniqueID = max(CodedResult.SourceUniqueID)
	,CodedResult.SourcePatientNo
	,SequenceNo =
		row_number() over(partition by CodedResult.SourcePatientNo order by CodedResult.ResultTime)
	,Result = cast(CodedResult.Result as int)
	,CodedResult.ResultDate
	,CodedResult.ResultTime
into
	OCMTest.Result
from
	Warehouse.OCM.CodedResult
where
	--CodedResult.ResultCode = 'CR' and	
	--ResultTime<= @cutoffDate --and	
	isnumeric(CodedResult.Result) = 1

group by
	 CodedResult.SourcePatientNo
	,CodedResult.Result
	,CodedResult.ResultDate
	,CodedResult.ResultTime

CREATE INDEX [IX_CodedResult_1] ON OCMTest.Result
(
	 SourcePatientNo ASC
	,ResultTime ASC
	,SequenceNo asc
);


select
	 Encounter.EncounterRecno
	,convert(int,Encounter.SourcePatientNo) SourcePatientNo
	,Encounter.AdmissionDate
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DistrictNo
	,Encounter.CasenoteNumber
	,Encounter.DateOfBirth
	,Encounter.EpisodeStartDate
	,Encounter.EpisodeStartTime
	,Encounter.EthnicOriginCode
	,Encounter.SexCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.DischargeDate
	,Encounter.StartWardTypeCode
into
	OCMTest.APCEncounter
from
	Warehouse.APC.Encounter
where
	(
		Encounter.DischargeDate > @cutoffDate
	or	Encounter.DischargeDate is null
	)
and	Encounter.SpecialtyCode not in ('NEIT' , 'PNEP' , 'NEPH' , 'RT' , 'NPCH')
and	left(Encounter.SpecialtyCode , 2) != 'IH'
and	Encounter.StartWardTypeCode not in ('TDU' , 'OMU')
and	left(Encounter.StartWardTypeCode , 2) != 'IH'

CREATE CLUSTERED INDEX [IX_APCEncounter_1] ON OCMTest.APCEncounter
(
	 SourcePatientNo ASC
	,EpisodeStartTime ASC
);
 
select
	 convert(int,SourcePatientNo) SourcePatientNo
	,WardCode
	,EndDate
	,EndTime
into
	OCMTest.APCWardStay
from
	Warehouse.APC.WardStay
where
	(
		WardStay.EndDate > @cutoffDate
	or	WardStay.EndDate is null
	)
and	WardStay.WardCode not in ('TDU' , 'OMU')
and	left(WardStay.WardCode , 2) != 'IH'

CREATE INDEX [IX_APCWardStay_1] ON OCMTest.APCWardStay
(
	 SourcePatientNo ASC
	,EndTime ASC
)


select
	 Result.EncounterRecno
	,Result.SourceUniqueID
	,Result.APCEncounterRecno
	,Result.AdmissionDate
	,Result.PatientForename
	,Result.PatientSurname
	,Result.DistrictNo
	,Result.CasenoteNumber
	,Result.DateOfBirth
	,Result.AgeCode
	,Result.EthnicOriginCode 
	,Result.SexCode
	,Result.ConsultantCode
	,Result.SpecialtyCode
	,Result.WardCode
	,Result.Result
	,Result.ResultDate
	,Result.ResultTime
	,Result.SequenceNo
	,Result.PreviousResult
	,Result.PreviousResultTime
	,Result.ResultIntervalDays
	,Result.ResultChange
	,Result.CurrentToPreviousResultRatio
	,Result.ResultRateOfChangePer24Hr
	,Result.CurrentInpatient
	,Result.BaselineResultRecno

	,BaselineResult =
		coalesce(
			 cast(BaselineResult.Result as int)
			,case
			when Result.SexCode = 'M' then 80
			else 60
			end
		)

	,BaselineResultTime =
		coalesce(
			 BaselineResult.ResultTime
			,dateadd(m , -6 , Result.ResultTime)
		)

	,BaselineToCurrentDays =
		datediff(
			 second
			,coalesce(
				 BaselineResult.ResultTime
				,dateadd(m , -6 , Result.ResultTime)
			)
			,Result.ResultTime
		) / (24.0 * 60 * 60)

	,BaselineToCurrentChange =
		cast(Result.Result as int)
		-
		coalesce(
			 cast(BaselineResult.Result as int)
			,case
			when Result.SexCode = 'M' then 80
			else 60
			end
		)

	,CurrentToBaselineResultRatio =
		cast(Result.Result as float)
		/
		cast(
			coalesce(
				 cast(BaselineResult.Result as int)
				,case
				when Result.SexCode = 'M' then 80
				else 60
				end
			) as float
		)
into
	OCMTest.TLoad

from
	(
	SELECT
		 Result.EncounterRecno
		,Result.SourceUniqueID

		,APCEncounterRecno =
			Encounter.EncounterRecno

		,Encounter.AdmissionDate
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DistrictNo
		,Encounter.CasenoteNumber
		,Encounter.DateOfBirth
	
		,AgeCode = --DATEDIFF(day,DateOfBirth, Encounter.EpisodeStartDate)
			 dbo.f_AgeCode(DateOfBirth, Encounter.EpisodeStartDate)
	
		,Encounter.EthnicOriginCode 
		,Encounter.SexCode

		,ConsultantCode =
			coalesce(Encounter.ConsultantCode , 'N/A')

		,SpecialtyCode =
			coalesce(Encounter.SpecialtyCode , 'N/A')

		,WardCode = coalesce(WardStay.WardCode , 'N/A')

		,Result =
			cast(Result.Result as int)

		,Result.ResultDate
		,Result.ResultTime

		,SequenceNo =
			row_number() over(partition by Result.SourcePatientNo order by Result.ResultTime)

		,PreviousResult = cast(PreviousResult.Result as int)
		,PreviousResultTime = PreviousResult.ResultTime

		,ResultIntervalDays =
			datediff(second , PreviousResult.ResultTime , Result.ResultTime ) / (24.0 * 60 * 60)

		,ResultChange =
			cast(Result.Result as int) - cast(PreviousResult.Result as int)

		,CurrentToPreviousResultRatio =
			cast(Result.Result as float) / cast(PreviousResult.Result as float)

		,ResultRateOfChangePer24Hr = 
			case 
			when Result.ResultTime = PreviousResult.ResultTime then 0.0
			else
				(cast(Result.Result as int) - cast(PreviousResult.Result as int))
				/
				(datediff(second , PreviousResult.ResultTime , Result.ResultTime ) / ( 24.0 * 60 * 60 ))
			end

		,CurrentInpatient =
			case
			when Encounter.DischargeDate is null then 1
			else 0
			end

		,BaselineResultRecno = 
			(
			select
				BaselineResultRecno =
					case
					when
							BaselineResult.EncounterRecno = Result.EncounterRecno
						and	(
							select
								count(*)
							from
								OCMTest.Result PreviousResult
							where
								PreviousResult.SourcePatientNo = Result.SourcePatientNo
							and	PreviousResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
							and	PreviousResult.EncounterRecno != Result.EncounterRecno
							) = 0
						then null
					else BaselineResult.EncounterRecno
					end
			from
				OCMTest.Result BaselineResult
			where
				BaselineResult.SourcePatientNo = Result.SourcePatientNo
			and	BaselineResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
			and	not exists
					(
					select
						1
					from
						OCMTest.Result LowerBaselineResult
					where
						LowerBaselineResult.SourcePatientNo = Result.SourcePatientNo
					and	LowerBaselineResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
					and	LowerBaselineResult.Result < BaselineResult.Result		
					)
			and	not exists
					(
					select
						1
					from
						OCMTest.Result LaterBaselineResult
					where
						LaterBaselineResult.SourcePatientNo = Result.SourcePatientNo
					and	LaterBaselineResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
					and	LaterBaselineResult.Result = BaselineResult.Result
					and	LaterBaselineResult.SequenceNo > BaselineResult.SequenceNo
					)
			)

	from
		OCMTest.Result Result

	left join
		(
		select
			 SourcePatientNo

			,SequenceNo =
				PreviousResult.SequenceNo + 1

			,PreviousResult.Result
			,PreviousResult.ResultTime
		from
			OCMTest.Result PreviousResult
		) PreviousResult
	on	PreviousResult.SourcePatientNo = Result.SourcePatientNo
	and	PreviousResult.SequenceNo = Result.SequenceNo
		
	--latest consultant episode
	inner join OCMTest.APCEncounter Encounter
	on	Encounter.SourcePatientNo = Result.SourcePatientNo
	and not exists
			(
			select
				1
			from
				OCMTest.APCEncounter LaterFCE
			where
				LaterFCE.SourcePatientNo = Encounter.SourcePatientNo
			and	LaterFCE.EpisodeStartTime > Encounter.EpisodeStartTime
			)

	--latest ward stay
	left join OCMTest.APCWardStay WardStay
	on	WardStay.SourcePatientNo = Result.SourcePatientNo
	and not exists
			(
			select
				1
			from
				OCMTest.APCWardStay LaterWardStay
			where
				LaterWardStay.SourcePatientNo = WardStay.SourcePatientNo
			and	LaterWardStay.EndTime > WardStay.EndTime
			)

	) Result

left join Warehouse.OCM.CodedResult BaselineResult
on	BaselineResult.EncounterRecno = Result.BaselineResultRecno



truncate table WarehouseOLAP.dbo.BaseOCMCreatinineResult


INSERT INTO dbo.BaseOCMCreatinineResult
(
	 EncounterRecno
	,SourceUniqueID
	,APCEncounterRecno
	,AdmissionDate
	,PatientForename
	,PatientSurname
	,DistrictNo
	,CasenoteNumber
	,DateOfBirth
	,AgeCode
	,EthnicOriginCode
	,SexCode
	,ConsultantCode
	,SpecialtyCode
	,WardCode
	,Result
	,ResultDate
	,ResultTime
	,SequenceNo
	,PreviousResult
	,PreviousResultTime
	,ResultIntervalDays
	,ResultChange
	,CurrentToPreviousResultRatio
	,ResultRateOfChangePer24Hr
	,CurrentInpatient
	,BaselineResultRecno
	,BaselineResult
	,BaselineResultTime
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio
)
select
	 EncounterRecno
	,SourceUniqueID
	,APCEncounterRecno
	,AdmissionDate
	,PatientForename
	,PatientSurname
	,DistrictNo
	,CasenoteNumber
	,DateOfBirth
	,AgeCode
	,EthnicOriginCode
	,SexCode
	,ConsultantCode
	,SpecialtyCode
	,WardCode
	,Result
	,ResultDate
	,ResultTime
	,SequenceNo
	,PreviousResult
	,PreviousResultTime
	,ResultIntervalDays
	,ResultChange
	,CurrentToPreviousResultRatio
	,ResultRateOfChangePer24Hr
	,CurrentInpatient
	,BaselineResultRecno
	,BaselineResult
	,BaselineResultTime
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio
from
	OCMTest.TLoad

drop table OCMTest.Result
drop table OCMTest.APCEncounter
drop table OCMTest.APCWardStay
drop table OCMTest.TLoad

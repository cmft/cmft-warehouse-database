﻿
CREATE view [Pharmacy].[OlapSystemUser] as

select
	 SystemUser.SystemUserRecno
	,SystemUser.SystemUserCode

	,SystemUser =
		SystemUser.Initials + ' (' +
		SystemUser.Forename + ' ' +
		SystemUser.Surname + ')'

	,SystemUser.Username
	
	,SystemUserGroup.SystemUserGroup
from
	Warehouse.Pharmacy.SystemUser

inner join Warehouse.Pharmacy.SystemUserGroup
on	SystemUserGroup.SystemUserGroupCode = SystemUser.SystemUserGroupCode

union all

select
	 SystemUserRecno = -1
	,SystemUserCode = null
	,SystemUser = 'N/A'
	,Username = 'N/A'
	,SystemUserGroup = 'Unassigned'


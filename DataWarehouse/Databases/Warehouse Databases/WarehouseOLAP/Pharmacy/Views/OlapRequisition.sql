﻿create view [Pharmacy].[OlapRequisition]
 as
select
	 InterfaceCode
	,RequisitionID
	,SiteCode
	,Items
	,PickNo
	,InDisputeUser
	,ShelfPrinted
	,RequisitionNo
	,TaxRatePct
	,TaxInclusive
	,InDispute
	,CustomerOrderNo
	,TaxAmount
	,TaxRateCode
	,PFlag
	,CreatedUser
	,ConversionFactor
	,IssueForm
	,Stocked
	,InternalSiteNo
	,InternalMethodCode
	,SupplierTypeCode
	,OrderedPacks
	,Urgency
	,ToFollow
	,InvoiceNumber
	,PayDate
	,Cost
	,ReceivedPacks
	,ReceivedDate
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderTime
	,LocationCode
	,NSVCode
	,OutstandingPacks
	,Created
	,ByWhom
	,RequisitionRecno
from
	Pharmacy.BaseRequisition


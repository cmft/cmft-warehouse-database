﻿






CREATE view [Pharmacy].[OlapSpecialty] as

select
	 Specialty.SpecialtyRecno
	,Specialty.Specialty
	,Specialty.LocalSpecialtyCode

	,DivisionCode =
		coalesce(
			 Specialty.DivisionCode
			,-1
		)

	,DirectorateCode =
		coalesce(
			 Specialty.DirectorateCode
			,-1
		)

	,TrustCode =
		coalesce(
			 Specialty.TrustCode
			,-1
		)

	,ChemoSpecialty =
		CAST(
			case
			when
				Specialty.LocalSpecialtyCode in
					(
					 'ONCO'
					,'HAEM'
					,'HAEP'
					)
			then 1
			else 0
			end

			as bit
		)

	,Specialty.CostCentreCode

from
	Warehouse.Pharmacy.Specialty

union all

select
	 SpecialtyRecno = -1
	,Specialty = 'N/A'
	,LocalSpecialtyCode = 'N/A'
	,DivisionCode = -1
	,DirectorateCode = -1
	,TrustCode = -1
	,ChemoSpecialty = cast(0 as bit)
	,CostCentreCode = null





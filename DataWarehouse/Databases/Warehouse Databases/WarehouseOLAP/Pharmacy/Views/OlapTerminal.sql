﻿create view [Pharmacy].[OlapTerminal] as

select
	 Terminal.TerminalRecno
	,Terminal.TerminalCode
	,Terminal = coalesce(Terminal.Terminal, cast(Terminal.TerminalCode as varchar) + ' - No Description')
	,Terminal.TerminalTypeCode
from
	Warehouse.Pharmacy.Terminal

union all

select
	 TerminalRecno = -1
	,TerminalCode = null
	,Terminal = 'N/A'
	,TerminalTypeCode = null



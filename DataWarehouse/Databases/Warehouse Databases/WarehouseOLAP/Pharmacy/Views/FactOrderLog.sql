﻿


CREATE view [Pharmacy].[FactOrderLog] as

/*
 User		Date		Change Log.
 S.SIDDA	30/01/2015	Added OrderKindCode to the View
*/

select
	 OrderLog.OrderLogRecno
	,OrderLog.InterfaceCode
	,OrderKindCode
	,OrderDate =
		coalesce(
			 OrderDate.TheDate

			,case
			when OrderLog.OrderTime is null
			then NullDate.TheDate

			when cast(OrderLog.OrderTime as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)


	,OrderTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					OrderLog.OrderTime
					as date
				)
				,OrderLog.OrderTime
			)
			,-1
		)

	,ReceivedDate =
		coalesce(
			 ReceivedDate.TheDate

			,case
			when OrderLog.ReceivedDate is null
			then NullDate.TheDate

			when cast(OrderLog.ReceivedDate as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,ReceivedTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					OrderLog.ReceivedDate
					as date
				)
				,OrderLog.ReceivedDate
			)
			,-1
		)

	,SupplierRecno = coalesce(Supplier.SupplierRecno, -1)

	,CreatedByRecno = SystemUser.SystemUserRecno

	,SiteRecno = Site.SiteRecno


	,OrderLogDate =
		coalesce(
			 OrderLogDate.TheDate

			,case
			when OrderLog.OrderLogTime is null
			then NullDate.TheDate

			when cast(OrderLog.OrderLogTime as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)


	,OrderLogTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					OrderLog.OrderLogTime
					as date
				)
				,OrderLog.OrderLogTime
			)
			,-1
		)

	,ProductStock.ProductStockRecno

	,DateInvoiced =
		coalesce(
			 DateInvoiced.TheDate

			,case
			when OrderLog.DateInvoiced is null
			then NullDate.TheDate

			when OrderLog.DateInvoiced < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,OrderLog.OrderedPacks
	,OrderLog.ReceivedPacks
	,OrderLog.StockLevel
	,OrderLog.Cost
	,OrderLog.CostExTax
	,OrderLog.TaxCost
	,OrderLog.StockValue
from
	Pharmacy.BaseOrderLog OrderLog

left join Warehouse.Pharmacy.Site
on	Site.InterfaceCode = OrderLog.InterfaceCode
and	Site.SiteID = OrderLog.SiteID

inner join Warehouse.Pharmacy.Product
on	Product.InterfaceCode = OrderLog.InterfaceCode
and	Product.NSVCode = OrderLog.NSVCode

left join Warehouse.Pharmacy.ProductStock
on	ProductStock.NSVCode = Product.NSVCode
and	ProductStock.InterfaceCode = Product.InterfaceCode
and	ProductStock.SiteCode = Site.SiteCode

left join Warehouse.Pharmacy.Supplier
on	Supplier.InterfaceCode = OrderLog.InterfaceCode
and	Supplier.LocalSupplierCode = OrderLog.SupplierCode
and	Supplier.SiteID = Site.SiteID

left join Warehouse.Pharmacy.SystemUser
on	SystemUser.SystemUserCode = OrderLog.CreatedByCode
and	SystemUser.InterfaceCode = OrderLog.InterfaceCode


left join WarehouseOLAPMergedV2.WH.Calendar OrderDate
on	OrderDate.TheDate = cast(OrderLog.OrderTime as date)

left join WarehouseOLAPMergedV2.WH.Calendar ReceivedDate
on	ReceivedDate.TheDate = cast(OrderLog.ReceivedDate as date)

left join WarehouseOLAPMergedV2.WH.Calendar OrderLogDate
on	OrderLogDate.TheDate = cast(OrderLog.OrderLogTime as date)

left join WarehouseOLAPMergedV2.WH.Calendar DateInvoiced
on	DateInvoiced.TheDate = OrderLog.DateInvoiced

left join WarehouseOLAPMergedV2.WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WarehouseOLAPMergedV2.WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join WarehouseOLAPMergedV2.Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WarehouseOLAPMergedV2.WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'



﻿
CREATE view [Pharmacy].[OlapIssueTransactionType] as

select
	 IssueTransactionType.IssueTransactionTypeCode
	,IssueTransactionType.IssueTransactionType
from
	Warehouse.Pharmacy.IssueTransactionType

union all

select
	'#'
	,'N/A'



﻿

CREATE view [Pharmacy].[FactReconciliationCensus] as

select
	 ReconciliationCensusRecno
	,ReconciliationCensus.InterfaceCode
	,ReconciliationCensus.CensusDate

	,SiteRecno = Site.SiteRecno
	,SupplierRecno = coalesce(Supplier.SupplierRecno, -1)
	,ProductStock.ProductStockRecno

	,OrderDate =
		coalesce(
			 OrderDate.TheDate

			,case
			when ReconciliationCensus.OrderTime is null
			then NullDate.TheDate

			when cast(ReconciliationCensus.OrderTime as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,OrderTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					ReconciliationCensus.OrderTime
					as date
				)
				,ReconciliationCensus.OrderTime
			)
			,-1
		)


	,ReceivedDate =
		coalesce(
			 ReceivedDate.TheDate

			,case
			when ReconciliationCensus.ReceivedDate is null
			then NullDate.TheDate

			when cast(ReconciliationCensus.ReceivedDate as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,ReceivedTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					ReconciliationCensus.ReceivedDate
					as date
				)
				,ReconciliationCensus.ReceivedDate
			)
			,-1
		)


	,ReconciliationCensus.OrderedPacks
	,ReconciliationCensus.PackCost
	,ReconciliationCensus.ReceivedPacks
	,ReconciliationCensus.NetValue
	,ReconciliationCensus.GrossValue

from
	Pharmacy.BaseReconciliationCensus ReconciliationCensus

left join Warehouse.Pharmacy.Site
on	Site.InterfaceCode = ReconciliationCensus.InterfaceCode
and	Site.SiteID = ReconciliationCensus.SiteID

inner join Warehouse.Pharmacy.Product
on	Product.InterfaceCode = ReconciliationCensus.InterfaceCode
and	Product.NSVCode = ReconciliationCensus.NSVCode

left join Warehouse.Pharmacy.ProductStock
on	ProductStock.NSVCode = Product.NSVCode
and	ProductStock.InterfaceCode = Product.InterfaceCode
and	ProductStock.SiteCode = Site.SiteCode

left join Warehouse.Pharmacy.Supplier
on	Supplier.InterfaceCode = ReconciliationCensus.InterfaceCode
and	Supplier.LocalSupplierCode = ReconciliationCensus.SupplierCode
and	Supplier.SiteID = Site.SiteID


left join WarehouseOLAPMergedV2.WH.Calendar OrderDate
on	OrderDate.TheDate = cast(ReconciliationCensus.OrderTime as date)

left join WarehouseOLAPMergedV2.WH.Calendar ReceivedDate
on	ReceivedDate.TheDate = cast(ReconciliationCensus.ReceivedDate as date)

left join WarehouseOLAPMergedV2.WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WarehouseOLAPMergedV2.WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join WarehouseOLAPMergedV2.Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WarehouseOLAPMergedV2.WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'


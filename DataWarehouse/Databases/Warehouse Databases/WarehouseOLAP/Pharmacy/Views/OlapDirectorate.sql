﻿

CREATE view [Pharmacy].[OlapDirectorate] as

select
	 Directorate.DirectorateCode
	,Directorate.Directorate
	,Directorate.SourceDirectorateCode
	
from
	Warehouse.Pharmacy.Directorate

union all

select
	-1
	,'N/A'
	,'N/A'




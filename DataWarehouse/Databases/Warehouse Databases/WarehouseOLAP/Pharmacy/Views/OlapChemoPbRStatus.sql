﻿

CREATE view [Pharmacy].[OlapChemoPbRStatus] as

select
	 ChemoPbRStatusCode = cast(1 as tinyint)
	,ChemoPbRStatus = 'Chemo PbR Exclusion'

union all

select
	 ChemoPbRStatusCode = cast(2 as tinyint)
	,ChemoPbRStatus = 'PbR Exclusion'

union all

select
	 ChemoPbRStatusCode = cast(3 as tinyint)
	,ChemoPbRStatus = 'Not PbR'

union all

select
	 ChemoPbRStatusCode = cast(4 as tinyint)
	,ChemoPbRStatus = 'Chemo PbR'




﻿



CREATE view [Pharmacy].[OlapDrugSubGroup] as

select
	 DrugSubGroup.DrugSubGroupCode
	,DrugSubGroup.DrugSubGroup
	,CostType.CostType
from
	Warehouse.Pharmacy.DrugSubGroup

left join Warehouse.Pharmacy.CostType
on	CostType.CostTypeCode = DrugSubGroup.CostTypeCode

union all

select distinct
	 ProductStock.DrugSubGroupCode
	,rtrim(ProductStock.DrugSubGroupCode) + ' - No Description'
	,'N/A'
from
	Warehouse.Pharmacy.ProductStock
where
	not exists
	(
	select
		1
	from
		Warehouse.Pharmacy.DrugSubGroup
	where
		DrugSubGroup.DrugSubGroupCode = ProductStock.DrugSubGroupCode
	)
and	ProductStock.DrugSubGroupCode is not null

union all

select distinct
	 DrugSubGroupCode = '-1'
	,'N/A'
	,'N/A'







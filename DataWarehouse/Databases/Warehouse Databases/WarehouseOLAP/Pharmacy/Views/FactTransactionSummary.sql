﻿
CREATE view [Pharmacy].[FactTransactionSummary] as

select
	 CostPerTransaction.InterfaceCode
	,CostPerTransaction.BudgetDate
	,FactTransactionLog.SpecialtyRecno
	,FactTransactionLog.Transactions
	,SLRCost = CostPerTransaction.CostPerTransaction
from
	Pharmacy.FactTransactionLog

inner join
	(
	select
		 Budget.InterfaceCode
		,Budget.BudgetDate
		,CostPerTransaction = Budget.BudgetAmount / TransactionLog.TotalTransactions
	from
		Warehouse.Pharmacy.Budget

	inner join
		(
		select
			 FactTransactionLog.InterfaceCode

			,TransactionLogDate =
				cast(
					'1 ' +
					datename(month, FactTransactionLog.TransactionLogDate) + ' ' +
					datename(year, FactTransactionLog.TransactionLogDate)

					as date
				)

			,TotalTransactions = COUNT(*) * 1.000
		from
			Pharmacy.FactTransactionLog
		group by
			 FactTransactionLog.InterfaceCode
			,cast(
				'1 ' +
				datename(month, FactTransactionLog.TransactionLogDate) + ' ' +
				datename(year, FactTransactionLog.TransactionLogDate)

				as date
			)
		) TransactionLog
	on	TransactionLog.InterfaceCode = Budget.InterfaceCode
	and	TransactionLog.TransactionLogDate = Budget.BudgetDate
	) CostPerTransaction
on	CostPerTransaction.InterfaceCode = FactTransactionLog.InterfaceCode
and	CostPerTransaction.BudgetDate = 
		cast(
			'1 ' +
			datename(month, FactTransactionLog.TransactionLogDate) + ' ' +
			datename(year, FactTransactionLog.TransactionLogDate)

			as date
		)






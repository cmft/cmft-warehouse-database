﻿CREATE view [Pharmacy].[OlapDivision] as

select
	 Division.DivisionCode
	,Division.Division
from
	Warehouse.Pharmacy.Division

union all

select
	-1
	,'N/A'



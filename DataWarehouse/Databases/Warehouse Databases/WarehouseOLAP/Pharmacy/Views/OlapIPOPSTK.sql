﻿
CREATE view [Pharmacy].[OlapIPOPSTK] as

select
	 IPOPSTK.IssueLabelTypeCode
	,IPOPSTK.IssueKindCode
	,IPOPSTK.IPOPSTK
from
	Warehouse.Pharmacy.IPOPSTK

union all

select distinct
	 TransactionLog.IssueLabelTypeCode
	,TransactionLog.IssueKindCode
	,'Undefined (IssueLabelTypeCode: ' + 
		TransactionLog.IssueLabelTypeCode +
		' IssueKindCode: ' + 
		TransactionLog.IssueKindCode +
		')'
from
	Pharmacy.FactTransactionLog TransactionLog
where
	not exists
	(
	select
		1
	from
		Warehouse.Pharmacy.IPOPSTK
	where
		IPOPSTK.IssueLabelTypeCode = TransactionLog.IssueLabelTypeCode
	and	IPOPSTK.IssueKindCode = TransactionLog.IssueKindCode
	)





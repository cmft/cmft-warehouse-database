﻿




CREATE view [Pharmacy].[OlapConsultant] as

select
	 ConsultantRecno
	,ConsultantCode

	,Consultant =
		coalesce(Consultant.Forename + ' ', '') +
		coalesce(Consultant.Surname + ' ', 'No Surname ') + '(' +
		coalesce(Consultant.Initials + ')', '')

	,CostCentreCode =
		coalesce(
			 CostCentreCode
			,'N/A'
		)
		
	,Consultant.Initials
from
	Warehouse.Pharmacy.Consultant

union all

select
	 ConsultantRecno = -1
	,ConsultantCode = null
	,Consultant = 'N/A'
	,CostCentreCode = 'N/A'
	,Initials = null






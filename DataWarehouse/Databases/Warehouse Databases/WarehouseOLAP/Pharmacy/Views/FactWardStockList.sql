﻿
CREATE view [Pharmacy].[FactWardStockList] as

select
	 Supplier.SupplierRecno
	,Product.ProductRecno
	,TopUpLevel
	,WardStockList.InterfaceCode
from
	Warehouse.Pharmacy.WardStockList

inner join Warehouse.Pharmacy.Supplier
on	Supplier.SupplierCode = WardStockList.SupplierCode
and	Supplier.InterfaceCode = WardStockList.InterfaceCode

inner join Warehouse.Pharmacy.Product
on	Product.NSVCode = WardStockList.NSVCode
and	Product.InterfaceCode = WardStockList.InterfaceCode

where
	WardStockList.NSVCode <> '&'




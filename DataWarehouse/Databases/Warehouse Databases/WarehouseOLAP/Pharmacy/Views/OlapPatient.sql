﻿


CREATE view [Pharmacy].[OlapPatient] as

select
	 PatientRecno
	,Title
	,Initials
	,Forename
	,Surname
	,DateOfBirth
	,NHSNumber
	,NHSNumberValidationStatusCode
	,Sex = Sex.Sex
	,CasenoteNo
	,Address.Postcode
from
	Warehouse.Pharmacy.Patient
	
left join Warehouse.Pharmacy.Sex
on	Sex.InterfaceCode = Patient.InterfaceCode
and	Sex.SexCode = Patient.SexCode

left join Warehouse.Pharmacy.PatientAddressMap
on	PatientAddressMap.InterfaceCode = Patient.InterfaceCode
and	PatientAddressMap.SourcePatientNo = Patient.SourcePatientNo

left join Warehouse.Pharmacy.AddressType
on	AddressType.InterfaceCode = Patient.InterfaceCode
and	AddressType.AddressTypeCode = PatientAddressMap.AddressTypeCode
and	AddressType.AddressType = 'Home'

left join Warehouse.Pharmacy.Address
on	Address.InterfaceCode = Patient.InterfaceCode
and	Address.AddressCode = PatientAddressMap.AddressCode

union all

select
	 PatientRecno = -1
	,Title = null
	,Initials = null
	,Forename = null
	,Surname = 'N/A'
	,DateOfBirth = null
	,NHSNumber = null
	,NHSNumberValidationStatusCode = null
	,Sex = null
	,CasenoteNo = null
	,Postcode = null




﻿
CREATE view [Pharmacy].[Calendar] as

select
	 DateID
	,TheDate
	,DayOfWeek
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,MonthName
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth
	,WeekNoKey
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	,FirstDayOfMonth
	,LastDayOfMonth
	,FirstDayOfQuarter
	,LastDayOfQuarter

	,MonthNameKey =
		case
		when MonthName = 'N/A' then 99
		when datepart(month, TheDate)<4 then datepart(month, TheDate) + 9 
		else datepart(month, TheDate) - 3
		end

from
	WarehouseOLAPMergedV2.WH.Calendar


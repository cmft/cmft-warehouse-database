﻿









CREATE view [Pharmacy].[OlapProduct] as

select
	 Product.ProductRecno
	,Product.NSVCode
	,Product.InterfaceCode
	,Product.Product
	,Product.TradeName
	,Product.ReorderPackSize
	,Product.MlsPerPack
	,Product.Cytotoxic
	,Product.BNFCode
	,Product.CatalogueDescription
	,Product.ReportGroup

	,IssueForm =
		coalesce(
			 Antibiotic.Form
			,Product.IssueForm
		)

	,ChildChemoFlag =
		case
		when left(Product.BNFCode, 5) in
			(
			 '08.01'
			,'08.02'
			,'04.06'
			,'09.01'
			)
		then 'Yes'

		else 'No'
		end

	,ChemoDrugFlag =
		case
		when upper(Product.Product) like upper('Alemtuzumab%') then 'Yes'
		when upper(Product.Product) like upper('Ciclosporin%') then 'Yes'
		when upper(Product.Product) like upper('Filgrastim%') then 'Yes'
		when upper(Product.Product) like upper('MyCophenolate Mofetil%') then 'Yes'
		when upper(Product.Product) like upper('Pegfilgrastim%') then 'Yes'
		when upper(Product.Product) like upper('Posaconazole%') then 'Yes'
		when upper(Product.Product) like upper('Rituximab%') then 'Yes'
		when upper(Product.Product) like upper('Tacrolimus%') then 'Yes'
		when upper(Product.Product) like upper('Thalidomide%') then 'Yes'
		when upper(Product.Product) like upper('Valganciclovir%') then 'Yes'
		when upper(Product.Product) like upper('Voriconazole%') then 'Yes'

		else 'No'
		end

	,GenericName =
		coalesce(
			 Antibiotic.GenericName
			,'N/A'
		)

	,DefinedDailyDose =
		coalesce(
			 Antibiotic.DefinedDailyDose
			,0.0
		)

	,AntibioticFlag =
		case
		when Antibiotic.NSVCode is not null
		then 'Yes'

		else 'No'
		end
		
	,Product.DosesPerIssueUnit
	,Product.DosingUnits

	,TaxRate = Product.TaxRate - 1
from
	Warehouse.Pharmacy.Product

left join Warehouse.Pharmacy.Antibiotic
on	Antibiotic.InterfaceCode = Product.InterfaceCode
and	Antibiotic.NSVCode = Product.NSVCode

union all

select
	 ProductRecno = -1
	,NSVCode = 'N/A'
	,InterfaceCode = 'N/A'
	,Product = 'N/A'
	,TradeName = null
	,ReorderPackSize = null
	,MlsPerPack = null
	,Cytotoxic = null
	,BNFCode = null
	,CatalogueDescription = null
	,ReportGroup = null
	,IssueForm = null
	,ChildChemoFlag = 'No'
	,ChemoDrugFlag = 'No'
	,GenericName = 'N/A'
	,DefinedDailyDose = 0.0
	,AntibioticFlag = 'No'
	,DosesPerIssueUnit = null
	,DosingUnits = null
	,TaxRate = null




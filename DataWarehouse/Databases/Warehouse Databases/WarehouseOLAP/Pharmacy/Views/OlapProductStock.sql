﻿
CREATE view [Pharmacy].[OlapProductStock] as

select
	 ProductStock.InterfaceCode
	,ProductStock.ProductStockCode
	,ProductStock.DrugID
	,ProductStock.InternalSiteCode
	,ProductStock.SiteCode
	,ProductStock.AnnualUse
	,ProductStock.ReorderLevelUnits
	,ProductStock.ReorderQuantity
	,ProductStock.StockLevel
	,ProductStock.ArchiveFlag
	,ProductStock.Formulary
	,ProductStock.OrderCycle
	,ProductStock.SupplierCode
	,ProductStock.LastOrderedDateText
	,ProductStock.StockTakeStatus
	,ProductStock.IssuePrice
	,ProductStock.LossesGains
	,ProductStock.LocalCode
	,ProductStock.BatchTracking
	,ProductStock.LastIssuedDate
	,ProductStock.LastStocktakeDate
	,DrugSubGroupCode = coalesce(ProductStock.DrugSubGroupCode, '-1')
	,DrugGroupCode = coalesce(ProductStock.DrugGroupCode, '-1')
	,ProductStock.UseThisPeriodUnits
	,ProductStock.LastPeriodEndDate
	,ProductStock.PrimaryLocationCode
	,ProductStock.SecondaryLocationCode
	,ProductStock.NSVCode
	,ProductStock.ExtraMessage
	,ProductStock.Stocked
	,ProductStock.DefinedDailyDoseValue
	,ProductStock.DefinedDailyDoseUnits
	,ProductStock.UserField1
	,ProductStock.UserField2
	,ProductStock.UserField3
	,ProductStock.PIPCode
	,ProductStock.MasterPIPCode
	,ProductStock.HighCostProduct
	,ProductStock.EDILinkCode
	,ProductStock.OuterPackSize
	,ProductStock.SiteProductCode
	,ProductStock.ProductStockRecno
	,Product.ProductRecno

	,SupplierRecno =
		coalesce(
			 Supplier.SupplierRecno
			,-1
		)
from
	Pharmacy.BaseProductStock ProductStock

inner join Warehouse.Pharmacy.Product
on	Product.SiteProductCode = ProductStock.SiteProductCode
and	Product.InterfaceCode = ProductStock.InterfaceCode

left join Warehouse.Pharmacy.Site
on	Site.InterfaceCode = ProductStock.InterfaceCode
and	Site.SiteCode = ProductStock.SiteCode

left join Warehouse.Pharmacy.Supplier
on	Supplier.LocalSupplierCode = ProductStock.SupplierCode
and	Supplier.InterfaceCode = ProductStock.InterfaceCode
and	Supplier.SiteID = Site.SiteID



﻿create view [Pharmacy].[OlapOrderLog] as

select
	 InterfaceCode
	,OrderLogID
	,OrderNumber
	,ConversionFactor
	,IssueForm
	,DispId
	,Terminal
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,ReceivedPacks
	,SupplierCode
	,BatchNumber
	,ExpiryDate
	,InvoiceNumber
	,LinkedNumber
	,ReasonCode
	,StockLevel
	,CreatedByCode
	,DateOrdered
	,DateReceived
	,SiteID
	,NSVCode
	,OrderLogTime
	,OrderKindCode
	,Cost
	,CostExTax
	,TaxCost
	,TaxCode
	,TaxRate
	,StockValue
	,DateInvoiced
	,Created
	,ByWhom
	,OrderLogRecno
from
	Pharmacy.BaseOrderLog



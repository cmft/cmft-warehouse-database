﻿create view [Pharmacy].[FactBudget] as

select
	 Budget.InterfaceCode
	,Budget.BudgetDate
	,Budget.BudgetAmount
from
	Warehouse.Pharmacy.Budget



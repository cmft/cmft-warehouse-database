﻿

CREATE view [Pharmacy].[FactRequisition] as

select
	 RequisitionRecno
	,Requisition.InterfaceCode

	,SiteRecno = Site.SiteRecno
	,SupplierRecno = coalesce(Supplier.SupplierRecno, -1)
	,ProductStock.ProductStockRecno


	,OrderDate =
		coalesce(
			 OrderDate.TheDate

			,case
			when Requisition.OrderTime is null
			then NullDate.TheDate

			when cast(Requisition.OrderTime as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,OrderTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					Requisition.OrderTime
					as date
				)
				,Requisition.OrderTime
			)
			,-1
		)

	,ReceivedDate =
		coalesce(
			 ReceivedDate.TheDate

			,case
			when Requisition.ReceivedDate is null
			then NullDate.TheDate

			when cast(Requisition.ReceivedDate as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,ReceivedTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					Requisition.ReceivedDate
					as date
				)
				,Requisition.ReceivedDate
			)
			,-1
		)


	,Requisition.Cost
	,Requisition.ReceivedPacks
	,Requisition.OrderedPacks
	,Requisition.OutstandingPacks

from
	Pharmacy.BaseRequisition Requisition

left join Warehouse.Pharmacy.Site
on	Site.InterfaceCode = Requisition.InterfaceCode
and	Site.SiteCode = Requisition.SiteCode

inner join Warehouse.Pharmacy.Product
on	Product.InterfaceCode = Requisition.InterfaceCode
and	Product.NSVCode = Requisition.NSVCode

left join Warehouse.Pharmacy.ProductStock
on	ProductStock.NSVCode = Product.NSVCode
and	ProductStock.InterfaceCode = Product.InterfaceCode
and	ProductStock.SiteCode = Site.SiteCode

left join Warehouse.Pharmacy.Supplier
on	Supplier.InterfaceCode = Requisition.InterfaceCode
and	Supplier.LocalSupplierCode = Requisition.SupplierCode
and	Supplier.SiteID = Site.SiteID


left join WarehouseOLAPMergedV2.WH.Calendar OrderDate
on	OrderDate.TheDate = cast(Requisition.OrderTime as date)

left join WarehouseOLAPMergedV2.WH.Calendar ReceivedDate
on	ReceivedDate.TheDate = cast(Requisition.ReceivedDate as date)

left join WarehouseOLAPMergedV2.WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WarehouseOLAPMergedV2.WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join WarehouseOLAPMergedV2.Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WarehouseOLAPMergedV2.WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'


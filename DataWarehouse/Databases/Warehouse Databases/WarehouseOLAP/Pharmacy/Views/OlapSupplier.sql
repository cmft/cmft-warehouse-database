﻿




CREATE view [Pharmacy].[OlapSupplier] as

select
	 Supplier.SupplierRecno
	,Supplier.Supplier
	,Supplier.LocalSupplierCode
	,Supplier.SupplierTypeCode
	,Supplier.SupplierFullName

	,CostCentreCode =
		coalesce(
			 Supplier.CostCentreCode
			,'N/A'
		)

	,Division = coalesce(Division.Division, 'EXTERNAL SUPPLIER')
from
	Warehouse.Pharmacy.Supplier

left join Warehouse.Pharmacy.SupplierMap
on	SupplierMap.SupplierRecno = Supplier.SupplierRecno

left join Warehouse.Pharmacy.Specialty
on	Specialty.SpecialtyRecno = SupplierMap.SpecialtyRecno

left join Warehouse.Pharmacy.Division
on	Division.DivisionCode = Specialty.DivisionCode

union all

select
	 SupplierRecno = -1
	,Supplier = 'N/A'
	,LocalSupplierCode = 'N/A'
	,SupplierTypeCode = 'N/A'
	,SupplierFullName = 'N/A'
	,CostCentreCode = 'N/A'
	,Division = 'N/A'







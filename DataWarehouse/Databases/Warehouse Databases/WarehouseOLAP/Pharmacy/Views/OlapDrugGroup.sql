﻿

CREATE view [Pharmacy].[OlapDrugGroup] as

select
	 DrugGroup.DrugGroupCode
	,DrugGroup.DrugGroup
from
	Warehouse.Pharmacy.DrugGroup

union all

select distinct
	 ProductStock.DrugGroupCode
	,rtrim(ProductStock.DrugGroupCode) + ' - No Description'
from
	Warehouse.Pharmacy.ProductStock
where
	not exists
	(
	select
		1
	from
		Warehouse.Pharmacy.DrugGroup
	where
		DrugGroup.DrugGroupCode = ProductStock.DrugGroupCode
	)
and	ProductStock.DrugGroupCode is not null

union all

select distinct
	 DrugGroupCode = '-1'
	,'N/A'





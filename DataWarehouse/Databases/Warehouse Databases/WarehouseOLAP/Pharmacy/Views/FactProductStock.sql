﻿



CREATE view [Pharmacy].[FactProductStock] as

select
	 ProductStock.ProductStockRecno

	,ProductStock.InterfaceCode

	,SiteRecno = Site.SiteRecno

	,ProductStock.ArchiveFlag

	,SupplierRecno = coalesce(Supplier.SupplierRecno, -1)

	,PbRFlag =
		CAST(
			case
			when upper(Product.Product) like '%EPOETIN%'
			then 1
			when ProductStock.DrugGroupCode = 'PB'
			then 1
			else 0
			end
			as bit
		)

	,Stocked =
		CAST(
			case ProductStock.Stocked
			when 'Y' then 1
			else 0
			end
			as bit
		)


	,ProductStock.AnnualUse
	,ProductStock.StockLevel
	,NetLossesGains = ProductStock.LossesGains
	,ProductStock.UseThisPeriodUnits

	,NetValue =
		case
		when Product.ReorderPackSize <> 0
		then (ProductStock.IssuePrice / Product.ReorderPackSize * 1.00) * ProductStock.StockLevel
		else ProductStock.IssuePrice
		end / 100.00

	,GrossStockValue =
		(
		case
		when Product.ReorderPackSize <> 0
		then (ProductStock.IssuePrice / Product.ReorderPackSize * 1.00) * ProductStock.StockLevel
		else ProductStock.IssuePrice
		end / 100.00
		) * Product.TaxRate

	,GrossLossesGains =
		ProductStock.LossesGains * Product.TaxRate
		
from
	Pharmacy.BaseProductStock ProductStock

inner join Warehouse.Pharmacy.Product
on	Product.InterfaceCode = ProductStock.InterfaceCode
and	Product.SiteProductCode = ProductStock.SiteProductCode

left join Warehouse.Pharmacy.Site
on	Site.InterfaceCode = ProductStock.InterfaceCode
and	Site.SiteCode = ProductStock.SiteCode

left join Warehouse.Pharmacy.Supplier
on	Supplier.InterfaceCode = ProductStock.InterfaceCode
and	Supplier.LocalSupplierCode = ProductStock.SupplierCode
and	Supplier.SiteID = Site.SiteID






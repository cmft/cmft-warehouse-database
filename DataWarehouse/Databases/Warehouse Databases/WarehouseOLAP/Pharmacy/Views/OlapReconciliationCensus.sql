﻿create view [Pharmacy].[OlapReconciliationCensus]
 as
select
	 InterfaceCode
	,CensusDate
	,ReconciliationCode
	,SiteID
	,SupplierCode
	,OrderNumber
	,NSVCode
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,PackCost
	,ReceivedPacks
	,NetValue
	,GrossValue
	,LocationCode
	,InDispute
	,InDisputeUser
	,Created
	,ByWhom
	,ReconciliationCensusRecno
from
	Pharmacy.BaseReconciliationCensus


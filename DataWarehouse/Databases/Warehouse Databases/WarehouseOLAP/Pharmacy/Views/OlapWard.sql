﻿

CREATE view [Pharmacy].[OlapWard] as

select
	 Ward.WardRecno
	,Ward.SpecialtyRecno
	,Supplier.WardCode
	,Ward = Supplier.SupplierFullName
	,Divided = case when Ward.Divided = 1 then 'Yes' else 'No' end
	,InUse = case when Ward.InUse = 1 then 'Yes' else 'No' end
	,OneStopService = case when Ward.OneStopService = 1 then 'Yes' else 'No' end
	,StockAvailableOutOfHours = case when Ward.StockAvailableOutOfHours = 1 then 'Yes' else 'No' end
	,Ward.OutOfHoursLocationPriority
	,StockReplenishmentService.StockReplenishmentService
	,WardType.WardType
from
	Warehouse.Pharmacy.Ward

inner join Warehouse.Pharmacy.Supplier
on	Supplier.SupplierRecno = Ward.WardRecno

inner join Warehouse.Pharmacy.StockReplenishmentService
on	StockReplenishmentService.StockReplenishmentServiceCode = Ward.StockReplenishmentServiceCode

inner join Warehouse.Pharmacy.WardType
on	WardType.WardTypeCode = Ward.WardTypeCode

--Unassigned Wards
union all

select
	 WardRecno = Supplier.SupplierRecno
	,SpecialtyRecno = -1
	,Supplier.LocalSupplierCode
	,Ward = Supplier.SupplierFullName
	,Divided = 'N/A'
	,InUse = case when Supplier.InUse = 1 then 'Yes' else 'No' end
	,OneStopService = 'N/A'
	,StockAvailableOutOfHours = 'N/A'
	,OutOfHoursLocationPriority = 0
	,StockReplenishmentService = 'N/A'
	,WardType.WardType
from
	Warehouse.Pharmacy.Supplier

inner join Warehouse.Pharmacy.WardType
on	WardType.WardTypeCode = '09'

where
	exists
	(
	select
		1
	from
		Warehouse.Pharmacy.TransactionLog
	where
		TransactionLog.WardCode = Supplier.SupplierCode
	and	TransactionLog.InterfaceCode = Supplier.InterfaceCode
	and	not exists
		(
		select
			1
		from
			Warehouse.Pharmacy.Ward
		where
			Ward.WardRecno = Supplier.SupplierRecno
		)
	)


union all

select
	-1
	,-1
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'
	,0
	,'N/A'
	,'N/A'





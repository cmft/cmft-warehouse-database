﻿

CREATE view [Pharmacy].[FactOrderCensus] as

select
	 OrderCensusRecno
	,OrderCensus.InterfaceCode
	,OrderCensus.CensusDate

	,SiteRecno = Site.SiteRecno
	,ProductStock.ProductStockRecno

	,OrderDate =
		coalesce(
			 OrderDate.TheDate

			,case
			when OrderCensus.OrderTime is null
			then NullDate.TheDate

			when cast(OrderCensus.OrderTime as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,OrderTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					OrderCensus.OrderTime
					as date
				)
				,OrderCensus.OrderTime
			)
			,-1
		)

	,SupplierRecno = coalesce(Supplier.SupplierRecno, -1)

	,ReceivedDate =
		coalesce(
			 ReceivedDate.TheDate

			,case
			when OrderCensus.ReceivedDate is null
			then NullDate.TheDate

			when cast(OrderCensus.ReceivedDate as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,ReceivedTimeBand =
		coalesce(
			DATEDIFF(
				 minute
				,
				cast(
					OrderCensus.ReceivedDate
					as date
				)
				,OrderCensus.ReceivedDate
			)
			,-1
		)


	,OrderCensus.OutstandingOrder
	,OrderCensus.OutstandingOrderValueNet
	,OrderCensus.OutstandingOrderValueGross
	,OrderCensus.PackCostNet
	,OrderCensus.PackCostGross
	,OrderCensus.ReceivedPacks
	,OrderCensus.OrderedPacks

from
	Pharmacy.BaseOrderCensus OrderCensus

left join Warehouse.Pharmacy.Site
on	Site.InterfaceCode = OrderCensus.InterfaceCode
and	Site.SiteCode = OrderCensus.SiteCode

inner join Warehouse.Pharmacy.Product
on	Product.InterfaceCode = OrderCensus.InterfaceCode
and	Product.NSVCode = OrderCensus.NSVCode

left join Warehouse.Pharmacy.ProductStock
on	ProductStock.NSVCode = Product.NSVCode
and	ProductStock.InterfaceCode = Product.InterfaceCode
and	ProductStock.SiteCode = Site.SiteCode

left join Warehouse.Pharmacy.Supplier
on	Supplier.InterfaceCode = OrderCensus.InterfaceCode
and	Supplier.LocalSupplierCode = OrderCensus.SupplierCode
and	Supplier.SiteID = Site.SiteID


left join WarehouseOLAPMergedV2.WH.Calendar OrderDate
on	OrderDate.TheDate = cast(OrderCensus.OrderTime as date)

left join WarehouseOLAPMergedV2.WH.Calendar ReceivedDate
on	ReceivedDate.TheDate = cast(OrderCensus.ReceivedDate as date)

left join WarehouseOLAPMergedV2.WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WarehouseOLAPMergedV2.WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join WarehouseOLAPMergedV2.Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WarehouseOLAPMergedV2.WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'


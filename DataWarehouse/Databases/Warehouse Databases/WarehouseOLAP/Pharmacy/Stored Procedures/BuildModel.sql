﻿create procedure [Pharmacy].[BuildModel] as

exec Pharmacy.BuildBaseProductStock
exec Pharmacy.BuildBaseTransactionLog
exec Pharmacy.BuildFactTransactionLog
exec Pharmacy.BuildBaseOrderCensus
exec Pharmacy.BuildBaseOrderLog
exec Pharmacy.BuildBaseReconciliationCensus
exec Pharmacy.BuildBaseRequisition


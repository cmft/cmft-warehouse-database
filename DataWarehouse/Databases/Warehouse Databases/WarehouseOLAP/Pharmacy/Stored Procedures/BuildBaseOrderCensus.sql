﻿CREATE procedure [Pharmacy].[BuildBaseOrderCensus] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table Pharmacy.BaseOrderCensus

insert into Pharmacy.BaseOrderCensus
	(
	 InterfaceCode
	,CensusDate
	,OrderID
	,SiteCode
	,NSVCode
	,OutstandingOrder
	,OutstandingOrderValueNet
	,OutstandingOrderValueGross
	,OrderTime
	,LocationCode
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderNumber
	,PackCostNet
	,PackCostGross
	,PickNo
	,ReceivedPacks
	,ReceivedDate
	,OrderedPacks
	,UrgencyCode
	,ToFollow
	,InternalSiteNo
	,InternalMethodCode
	,PFlag
	,CreatedUser
	,CustomerOrderNo
	,InDispute
	,InDisputeUser
	,ShelfPrinted
	,TaxRate
	,Created
	,ByWhom
	,OrderCensusRecno
	)
select
	 InterfaceCode
	,CensusDate
	,OrderID
	,SiteCode
	,NSVCode
	,OutstandingOrder
	,OutstandingOrderValueNet
	,OutstandingOrderValueGross
	,OrderTime
	,LocationCode
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderNumber
	,PackCostNet
	,PackCostGross
	,PickNo
	,ReceivedPacks
	,ReceivedDate
	,OrderedPacks
	,UrgencyCode
	,ToFollow
	,InternalSiteNo
	,InternalMethodCode
	,PFlag
	,CreatedUser
	,CustomerOrderNo
	,InDispute
	,InDisputeUser
	,ShelfPrinted
	,TaxRate
	,Created
	,ByWhom
	,OrderCensusRecno
from
	Warehouse.Pharmacy.OrderCensus


select @RowsInserted = @@rowcount

exec BuildOlapCensus


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.BuildBaseOrderCensus', @Stats, @StartTime


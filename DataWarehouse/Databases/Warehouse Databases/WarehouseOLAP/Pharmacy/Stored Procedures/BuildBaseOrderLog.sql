﻿create procedure [Pharmacy].[BuildBaseOrderLog] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table Pharmacy.BaseOrderLog

insert into Pharmacy.BaseOrderLog
	(
	 InterfaceCode
	,OrderLogID
	,OrderNumber
	,ConversionFactor
	,IssueForm
	,DispId
	,Terminal
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,ReceivedPacks
	,SupplierCode
	,BatchNumber
	,ExpiryDate
	,InvoiceNumber
	,LinkedNumber
	,ReasonCode
	,StockLevel
	,CreatedByCode
	,DateOrdered
	,DateReceived
	,SiteID
	,NSVCode
	,OrderLogTime
	,OrderKindCode
	,Cost
	,CostExTax
	,TaxCost
	,TaxCode
	,TaxRate
	,StockValue
	,DateInvoiced
	,Created
	,ByWhom
	,OrderLogRecno
	)
select
	 InterfaceCode
	,OrderLogID
	,OrderNumber
	,ConversionFactor
	,IssueForm
	,DispId
	,Terminal
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,ReceivedPacks
	,SupplierCode
	,BatchNumber
	,ExpiryDate
	,InvoiceNumber
	,LinkedNumber
	,ReasonCode
	,StockLevel
	,CreatedByCode
	,DateOrdered
	,DateReceived
	,SiteID
	,NSVCode
	,OrderLogTime
	,OrderKindCode
	,Cost
	,CostExTax
	,TaxCost
	,TaxCode
	,TaxRate
	,StockValue
	,DateInvoiced
	,Created
	,ByWhom
	,OrderLogRecno
from
	Warehouse.Pharmacy.OrderLog


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.BuildBaseOrderLog', @Stats, @StartTime


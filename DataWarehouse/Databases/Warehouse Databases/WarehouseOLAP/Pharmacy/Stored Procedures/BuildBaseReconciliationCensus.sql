﻿create procedure [Pharmacy].[BuildBaseReconciliationCensus] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table Pharmacy.BaseReconciliationCensus

insert into Pharmacy.BaseReconciliationCensus
	(
	 InterfaceCode
	,CensusDate
	,ReconciliationCode
	,SiteID
	,SupplierCode
	,OrderNumber
	,NSVCode
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,PackCost
	,ReceivedPacks
	,NetValue
	,GrossValue
	,LocationCode
	,InDispute
	,InDisputeUser
	,Created
	,ByWhom
	,ReconciliationCensusRecno
	)
select
	 InterfaceCode
	,CensusDate
	,ReconciliationCode
	,SiteID
	,SupplierCode
	,OrderNumber
	,NSVCode
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,PackCost
	,ReceivedPacks
	,NetValue
	,GrossValue
	,LocationCode
	,InDispute
	,InDisputeUser
	,Created
	,ByWhom
	,ReconciliationCensusRecno
from
	Warehouse.Pharmacy.ReconciliationCensus


select @RowsInserted = @@rowcount

exec BuildOlapCensus


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.BuildBaseReconciliationCensus', @Stats, @StartTime


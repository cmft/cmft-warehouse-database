﻿
CREATE procedure [Pharmacy].[BuildFactTransactionLog] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table Pharmacy.FactTransactionLog

insert into Pharmacy.FactTransactionLog
(
	 PatientRecno
	,ConsultantRecno
	,SpecialtyRecno
	,InterfaceCode
	,SiteRecno
	,WardRecno
	,ProductStockRecno
	,CreatedByRecno
	,TerminalRecno
	,IssueKindCode
	,IssueLabelTypeCode
	,TransactionLogDate
	,TimeBandCode
	,Transactions
	,Quantity
	,Cost
	,TaxCost
	,CostExcludingTax
	,DefinedDailyDoseUnits
	,DefinedDailyDoseValue
	,ChemoPbRStatusCode
	,ServiceCost
	,BasicCost
	,BasicCostTax
	,PbRFlag
	,DefinedDailyDoses
)
select
	 PatientRecno = coalesce(Patient.PatientRecno, -1)
	,ConsultantRecno = coalesce(Consultant.ConsultantRecno, -1)
	,SpecialtyRecno = coalesce( Specialty.SpecialtyRecno,-1)/*Updated to get all the Specialties for Trafford*/
	
	--,SpecialtyRecno =
	--	coalesce(
	--		case
	--		when IPOPSTK.IPOPSTK = 'STK'
	--		then Ward.SpecialtyRecno
	--		end
	--		,Specialty.SpecialtyRecno
	--		,-1
	--	)

	,BaseTransactionLog.InterfaceCode
	,SiteRecno = Site.SiteRecno
	,WardRecno = coalesce(Supplier.SupplierRecno, -1)
	,ProductStock.ProductStockRecno
	,CreatedByRecno = SystemUser.SystemUserRecno
	,TerminalRecno = coalesce(Terminal.TerminalRecno, -1)
	,BaseTransactionLog.IssueKindCode
	,BaseTransactionLog.IssueLabelTypeCode

	,TransactionLogDate =
		coalesce(
			 TransactionLogDate.TheDate

			,case
			when BaseTransactionLog.TransactionLogTime is null
			then NullDate.TheDate

			when cast(BaseTransactionLog.TransactionLogTime as date) < CalendarStartDate.DateValue
			then PreviousDate.TheDate

			else FutureDate.TheDate
			end
		)

	,TimeBandCode =
		DATEDIFF(
			 minute
			,cast(BaseTransactionLog.TransactionLogTime as date)
			,BaseTransactionLog.TransactionLogTime
		)

	,Transactions = 1
	,BaseTransactionLog.Quantity
	,cast(BaseTransactionLog.Cost as float) / 100.00
	,cast(BaseTransactionLog.TaxCost as float) / 100.00
	,cast(BaseTransactionLog.CostExcludingTax as float) / 100.00
	,cast(ProductStock.DefinedDailyDoseUnits as float)
	,cast(ProductStock.DefinedDailyDoseValue as float)

	,ChemoPbRStatusCode =
		CAST(
			case
			when ProductStock.DrugGroupCode = 'PB'
			then
				case
				when Product.BNFCode like '08.01%'
				then
					case
					when Specialty.LocalSpecialtyCode in ('ONCO', 'HAEM', 'HAEP')
					then 1 --chemo pbr exclusion
					else 4 --chemo pbr
					end
				else 2 --pbr exclusion
				end

			else 3 --not pbr
			end

			as tinyint
		)

	,ServiceCost =
		case
		when DrugSubGroup.CostTypeCode = 'S'
		then cast(BaseTransactionLog.Cost as float) / 100.00
		end

	,BasicCost =
		case
		when DrugSubGroup.CostTypeCode = 'B'
		then cast(BaseTransactionLog.CostExcludingTax as float) / 100.00
		end

	,BasicCostTax =
		case
		when DrugSubGroup.CostTypeCode = 'B'
		then cast(BaseTransactionLog.TaxCost as float) / 100.00
		end

	,PbRFlag =
		CAST(
			case
			when upper(Product.Product) like '%EPOETIN%'
			then 1
			when ProductStock.DrugGroupCode = 'PB'
			then 1
			-- [EW] Added 04 April 2013 14:30
			when ProductStock.DrugGroupCode = 'L'
			then 1
			-- [EW]
			else 0
			end
			as bit
		)

	,DefinedDailyDoses =
		case
		when
			BaseTransactionLog.InterfaceCode = 'PHAC'
		and	BaseTransactionLog.NSVCode = 'DUF838A'
		then 0.625

		when
			BaseTransactionLog.InterfaceCode = 'PHAC'
		and BaseTransactionLog.NSVCode = 'DUF859A'
		then 0.375

		when
			BaseTransactionLog.InterfaceCode = 'PHAT'
		and	BaseTransactionLog.NSVCode = 'DUF838A'
		then 0.625

		when
			BaseTransactionLog.InterfaceCode = 'PHAT'
		and BaseTransactionLog.NSVCode = 'DUF859A'
		then 0.375

		when Product.DosingUnits = 'mg'
		then Product.DosesPerIssueUnit / 1000.0

		when Product.DosingUnits = 'g'
		then Product.DosesPerIssueUnit

		end *

		BaseTransactionLog.Quantity /

		Antibiotic.DefinedDailyDose

from
	Pharmacy.BaseTransactionLog

inner join Warehouse.Pharmacy.Product
on	Product.NSVCode = BaseTransactionLog.NSVCode
and	Product.InterfaceCode = BaseTransactionLog.InterfaceCode

left join Warehouse.Pharmacy.Antibiotic
on	Antibiotic.InterfaceCode = Product.InterfaceCode
and	Antibiotic.NSVCode = Product.NSVCode

left join Warehouse.Pharmacy.ProductStock
on	ProductStock.NSVCode = Product.NSVCode
and	ProductStock.InterfaceCode = Product.InterfaceCode
and	ProductStock.SiteCode = BaseTransactionLog.SiteCode

left join Warehouse.Pharmacy.Patient
on	Patient.SourcePatientNo = BaseTransactionLog.SourcePatientNo
and	Patient.InterfaceCode = BaseTransactionLog.InterfaceCode

left join Warehouse.Pharmacy.Consultant
on	Consultant.ConsultantCode = BaseTransactionLog.ConsultantCode
and	Consultant.InterfaceCode = BaseTransactionLog.InterfaceCode

left join Warehouse.Pharmacy.Specialty
on	Specialty.SpecialtyCode = BaseTransactionLog.SpecialtyCode
and	Specialty.InterfaceCode = BaseTransactionLog.InterfaceCode

left join Warehouse.Pharmacy.Supplier
on	Supplier.SupplierCode = BaseTransactionLog.WardCode
and	Supplier.InterfaceCode = BaseTransactionLog.InterfaceCode

left join Warehouse.Pharmacy.SystemUser
on	SystemUser.SystemUserCode = BaseTransactionLog.CreatedByCode
and	SystemUser.InterfaceCode = BaseTransactionLog.InterfaceCode

left join Warehouse.Pharmacy.Terminal
on	Terminal.TerminalCode = BaseTransactionLog.TerminalCode
and	Terminal.InterfaceCode = BaseTransactionLog.InterfaceCode

left join Warehouse.Pharmacy.Site
on	Site.SiteCode = BaseTransactionLog.SiteCode
and	Site.InterfaceCode = BaseTransactionLog.InterfaceCode

left join Warehouse.Pharmacy.DrugSubGroup
on	DrugSubGroup.DrugSubGroupCode = ProductStock.DrugSubGroupCode

left join Warehouse.Pharmacy.CostTypeTaxRateMap
on	CostTypeTaxRateMap.CostTypeCode = DrugSubGroup.CostTypeCode
and	CostTypeTaxRateMap.InterfaceCode = ProductStock.InterfaceCode

left join Warehouse.Pharmacy.TaxRate
on	TaxRate.InterfaceCode = CostTypeTaxRateMap.InterfaceCode
and	TaxRate.TaxCode = CostTypeTaxRateMap.TaxCode

left join Warehouse.Pharmacy.Ward
on	Ward.WardRecno = Supplier.SupplierRecno

left join Warehouse.Pharmacy.IPOPSTK
on	IPOPSTK.IssueLabelTypeCode = BaseTransactionLog.IssueLabelTypeCode
and	IPOPSTK.IssueKindCode = BaseTransactionLog.IssueKindCode

left join WarehouseOLAPMergedV2.WH.Calendar TransactionLogDate
on	TransactionLogDate.TheDate = cast(BaseTransactionLog.TransactionLogTime as date)

left join WarehouseOLAPMergedV2.WH.Calendar FutureDate
on	FutureDate.TheDate = '31 dec 9999'

left join WarehouseOLAPMergedV2.WH.Calendar PreviousDate
on	PreviousDate.TheDate = '2 Jan 1900'

left join WarehouseOLAPMergedV2.Utility.Parameter CalendarStartDate
on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

left join WarehouseOLAPMergedV2.WH.Calendar NullDate
on	NullDate.TheDate = '1 jan 1900'


select @RowsInserted = @@rowcount


select
	 @from = min(FactTransactionLog.TransactionLogDate)
	,@to = max(FactTransactionLog.TransactionLogDate)
from
	Pharmacy.FactTransactionLog



select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'Pharmacy.BuildFactTransactionLog', @Stats, @StartTime


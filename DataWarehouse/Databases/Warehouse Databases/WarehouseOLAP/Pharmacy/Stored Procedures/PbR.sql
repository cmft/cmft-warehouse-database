﻿CREATE procedure [Pharmacy].[PbR]
	 @FromDate date
	,@ToDate date
as

--complete list
select
	 [Month] = Activity.Period
	,Activity.Area
	,Activity.SubArea
	,Activity.Forename
	,Activity.Surname
	,Activity.NHSNumber
	,Activity.CasenoteNo
	,Activity.NSVCode
	,Activity.BNFCode
	,Activity.Product
	,Activity.Directorate
	,Activity.Consultant
	,Activity.SupplierFullName
	,Activity.IPOPSTK
	,Activity.DrugGroup
	,Activity.DrugSubGroup
	,Activity.ProductFlag
	,Activity.PbRChemo
	,Activity.PbRFlag
	,Quantity = sum(Activity.Quantity)
	,Cost = sum(Activity.Cost)

from
	(
	select
		 Period =
			LEFT(
				DATENAME(
					 month
					,TransactionLog.TransactionLogDate
				)
				,3
			) + '_' +
			RIGHT(
				DATENAME(
					 year
					,TransactionLog.TransactionLogDate
				)
				,2
			)


		,Area = Division.Division
		,SubArea = Specialty.Specialty

		,Patient.Forename
		,Patient.Surname
		,Patient.NHSNumber
		,Patient.CasenoteNo
		,ProductStock.NSVCode
		,Product.BNFCode
		,Product.Product
		,TransactionLog.Quantity
		,TransactionLog.Cost
		,Directorate.Directorate
		,Consultant.Consultant
		,Supplier.SupplierFullName
		,IPOPSTK.IPOPSTK
		,DrugGroup.DrugGroup
		,DrugSubGroup.DrugSubGroup

		,ProductFlag =
			DrugGroup.DrugGroup + ' & ' + DrugSubGroup.DrugSubGroup

		,PbRChemo =
			case
			when
				ProductStock.DrugGroupCode = 'PB'
			and	left(Product.BNFCode, 5) = '08.01'
			and	Specialty.LocalSpecialtyCode in ('HAEM', 'ONCO', 'HAEP')
			then 'Exclude: Chemo PBR'

			when
				ProductStock.DrugGroupCode = 'PB'
			and	left(Product.BNFCode, 5) = '08.01'
			then 'Chemo PBR'

			when
				ProductStock.DrugGroupCode = 'PB'
			then 'PBR Exclusion'
			else 'NOT PBR'
			end

		,PbRFlag =
			case
			when TransactionLog.PbRFlag = 1
			then 'True'
			else 'False'
			end

	from
		Pharmacy.FactTransactionLog TransactionLog

	inner join Pharmacy.OlapPatient Patient
	on	Patient.PatientRecno = TransactionLog.PatientRecno

	inner join Pharmacy.OlapProductStock ProductStock
	on	ProductStock.ProductStockRecno = TransactionLog.ProductStockRecno

	inner join Pharmacy.OlapProduct Product
	on	Product.ProductRecno = ProductStock.ProductRecno

	inner join Pharmacy.OlapSpecialty Specialty
	on	Specialty.SpecialtyRecno = TransactionLog.SpecialtyRecno

	inner join Pharmacy.OlapDivision Division
	on	Division.DivisionCode = Specialty.DivisionCode

	inner join Pharmacy.OlapDirectorate Directorate
	on	Directorate.DirectorateCode = Specialty.DirectorateCode

	inner join Pharmacy.OlapIPOPSTK IPOPSTK
	on	IPOPSTK.IssueKindCode = TransactionLog.IssueKindCode
	and	IPOPSTK.IssueLabelTypeCode = TransactionLog.IssueLabelTypeCode

	inner join Pharmacy.OlapSupplier Supplier
	on	Supplier.SupplierRecno = TransactionLog.WardRecno

	inner join Pharmacy.OlapConsultant Consultant
	on	Consultant.ConsultantRecno = TransactionLog.ConsultantRecno

	inner join Pharmacy.OlapDrugGroup DrugGroup
	on	DrugGroup.DrugGroupCode = ProductStock.DrugGroupCode

	inner join Pharmacy.OlapDrugSubGroup DrugSubGroup
	on	DrugSubGroup.DrugSubGroupCode = ProductStock.DrugSubGroupCode

	where
		TransactionLog.TransactionLogDate between @FromDate and @ToDate
	and	TransactionLog.PbRFlag = 1
	) Activity
group by
	 Activity.Period
	,Activity.Area
	,Activity.SubArea
	,Activity.Forename
	,Activity.Surname
	,Activity.NHSNumber
	,Activity.CasenoteNo
	,Activity.NSVCode
	,Activity.BNFCode
	,Activity.Product
	,Activity.Directorate
	,Activity.Consultant
	,Activity.SupplierFullName
	,Activity.IPOPSTK
	,Activity.DrugGroup
	,Activity.DrugSubGroup
	,Activity.ProductFlag
	,Activity.PbRChemo
	,Activity.PbRFlag


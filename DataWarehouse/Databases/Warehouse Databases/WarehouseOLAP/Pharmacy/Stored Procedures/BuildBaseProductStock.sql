﻿CREATE procedure [Pharmacy].[BuildBaseProductStock] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table Pharmacy.BaseProductStock

insert into Pharmacy.BaseProductStock
	(
	 InterfaceCode
	,ProductStockCode
	,DrugID
	,InternalSiteCode
	,SiteCode
	,AnnualUse
	,ReorderLevelUnits
	,ReorderQuantity
	,StockLevel
	,ArchiveFlag
	,Formulary
	,OrderCycle
	,SupplierCode
	,LastOrderedDateText
	,StockTakeStatus
	,IssuePrice
	,LossesGains
	,LocalCode
	,BatchTracking
	,LastIssuedDate
	,LastStocktakeDate
	,DrugSubGroupCode
	,DrugGroupCode
	,UseThisPeriodUnits
	,LastPeriodEndDate
	,PrimaryLocationCode
	,SecondaryLocationCode
	,NSVCode
	,ExtraMessage
	,Stocked
	,DefinedDailyDoseValue
	,DefinedDailyDoseUnits
	,UserField1
	,UserField2
	,UserField3
	,PIPCode
	,MasterPIPCode
	,HighCostProduct
	,EDILinkCode
	,OuterPackSize
	,SiteProductCode
	,ProductStockRecno
	)
select
	 InterfaceCode
	,ProductStockCode
	,DrugID
	,InternalSiteCode
	,SiteCode
	,AnnualUse
	,ReorderLevelUnits
	,ReorderQuantity
	,StockLevel
	,ArchiveFlag
	,Formulary
	,OrderCycle
	,SupplierCode
	,LastOrderedDateText
	,StockTakeStatus
	,IssuePrice
	,LossesGains
	,LocalCode
	,BatchTracking
	,LastIssuedDate
	,LastStocktakeDate
	,DrugSubGroupCode
	,DrugGroupCode
	,UseThisPeriodUnits
	,LastPeriodEndDate
	,PrimaryLocationCode
	,SecondaryLocationCode
	,NSVCode
	,ExtraMessage
	,Stocked
	,DefinedDailyDoseValue
	,DefinedDailyDoseUnits
	,UserField1
	,UserField2
	,UserField3
	,PIPCode
	,MasterPIPCode
	,HighCostProduct
	,EDILinkCode
	,OuterPackSize
	,SiteProductCode
	,ProductStockRecno
from
	Warehouse.Pharmacy.ProductStock


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.BuildBaseProductStock', @Stats, @StartTime


﻿CREATE procedure [Pharmacy].[FinancePassThrough]
	 @FromDate date
	,@ToDate date
as

--complete list
select
	 [Month] = Activity.Period
	,Activity.Area
	,Activity.SubArea
	,Activity.Location
	,Inpatient = sum(Activity.Inpatient)
	,Outpatient = sum(Activity.Outpatient)
	,Consultant = sum(Activity.Consultant)
	,[Ward Stock] = sum(Activity.WardStock)
	,[Total Cost] = sum(Activity.TotalCost)
	,[Cost Centre] = Activity.CostCentre
from
	(
	select
		 Period =
			LEFT(
				DATENAME(
					 month
					,TransactionLog.TransactionLogDate
				)
				,3
			) + '_' +
			RIGHT(
				DATENAME(
					 year
					,TransactionLog.TransactionLogDate
				)
				,2
			)


		,Area = Division.Division
		,SubArea = Specialty.Specialty

		,Location =
			case
			when IPOPSTK.IPOPSTK = 'STK'
			then Supplier.SupplierFullName
			else Consultant.Consultant
			end

		,Inpatient =
			case
			when IPOPSTK.IPOPSTK = 'IP'
			then TransactionLog.Cost
			else 0
			end

		,Outpatient =
			case
			when IPOPSTK.IPOPSTK = 'OP'
			then TransactionLog.Cost
			else 0
			end

		,Consultant =
			case
			when IPOPSTK.IPOPSTK <> 'STK'
			then TransactionLog.Cost
			else 0
			end

		,WardStock =
			case
			when IPOPSTK.IPOPSTK = 'STK'
			then TransactionLog.Cost
			else 0
			end

		,TotalCost =
			TransactionLog.Cost

		,CostCentre =
			coalesce(
				 Consultant.CostCentreCode
				,Supplier.CostCentreCode
			)

	from
		Pharmacy.FactTransactionLog TransactionLog

	inner join Pharmacy.OlapSpecialty Specialty
	on	Specialty.SpecialtyRecno = TransactionLog.SpecialtyRecno

	inner join Pharmacy.OlapDivision Division
	on	Division.DivisionCode = Specialty.DivisionCode

	inner join Pharmacy.OlapIPOPSTK IPOPSTK
	on	IPOPSTK.IssueKindCode = TransactionLog.IssueKindCode
	and	IPOPSTK.IssueLabelTypeCode = TransactionLog.IssueLabelTypeCode

	inner join Pharmacy.OlapSupplier Supplier
	on	Supplier.SupplierRecno = TransactionLog.WardRecno

	inner join Pharmacy.OlapConsultant Consultant
	on	Consultant.ConsultantRecno = TransactionLog.ConsultantRecno

	where
		cast(TransactionLog.TransactionLogDate as date) between @FromDate and @ToDate
	) Activity
group by
	 Activity.Period
	,Activity.Area
	,Activity.SubArea
	,Activity.Location
	,Activity.CostCentre


--PbR only
select
	 [Month] = Activity.Period
	,Activity.Area
	,Activity.SubArea
	,Activity.Location
	,Inpatient = sum(Activity.Inpatient)
	,Outpatient = sum(Activity.Outpatient)
	,Consultant = sum(Activity.Consultant)
	,[Ward Stock] = sum(Activity.WardStock)
	,[Total Cost] = sum(Activity.TotalCost)
	,[Cost Centre] = Activity.CostCentre
from
	(
	select
		 Period =
			LEFT(
				DATENAME(
					 month
					,TransactionLog.TransactionLogDate
				)
				,3
			) + '_' +
			RIGHT(
				DATENAME(
					 year
					,TransactionLog.TransactionLogDate
				)
				,2
			)


		,Area = Division.Division
		,SubArea = Specialty.Specialty

		,Location =
			case
			when IPOPSTK.IPOPSTK = 'STK'
			then Supplier.SupplierFullName
			else Consultant.Consultant
			end

		,Inpatient =
			case
			when IPOPSTK.IPOPSTK = 'IP'
			then TransactionLog.Cost
			else 0
			end

		,Outpatient =
			case
			when IPOPSTK.IPOPSTK = 'OP'
			then TransactionLog.Cost
			else 0
			end

		,Consultant =
			case
			when IPOPSTK.IPOPSTK <> 'STK'
			then TransactionLog.Cost
			else 0
			end

		,WardStock =
			case
			when IPOPSTK.IPOPSTK = 'STK'
			then TransactionLog.Cost
			else 0
			end

		,TotalCost =
			TransactionLog.Cost

		,CostCentre =
			coalesce(
				 Consultant.CostCentreCode
				,Supplier.CostCentreCode
			)

	from
		Pharmacy.FactTransactionLog TransactionLog

	inner join Pharmacy.OlapSpecialty Specialty
	on	Specialty.SpecialtyRecno = TransactionLog.SpecialtyRecno

	inner join Pharmacy.OlapDivision Division
	on	Division.DivisionCode = Specialty.DivisionCode

	inner join Pharmacy.OlapIPOPSTK IPOPSTK
	on	IPOPSTK.IssueKindCode = TransactionLog.IssueKindCode
	and	IPOPSTK.IssueLabelTypeCode = TransactionLog.IssueLabelTypeCode

	inner join Pharmacy.OlapSupplier Supplier
	on	Supplier.SupplierRecno = TransactionLog.WardRecno

	inner join Pharmacy.OlapConsultant Consultant
	on	Consultant.ConsultantRecno = TransactionLog.ConsultantRecno

	where
		cast(TransactionLog.TransactionLogDate as date) between @FromDate and @ToDate
	and	TransactionLog.PbRFlag = 1

	) Activity
group by
	 Activity.Period
	,Activity.Area
	,Activity.SubArea
	,Activity.Location
	,Activity.CostCentre


--Non-PbR only
select
	 [Month] = Activity.Period
	,Activity.Area
	,Activity.SubArea
	,Activity.Location
	,Inpatient = sum(Activity.Inpatient)
	,Outpatient = sum(Activity.Outpatient)
	,Consultant = sum(Activity.Consultant)
	,[Ward Stock] = sum(Activity.WardStock)
	,[Total Cost] = sum(Activity.TotalCost)
	,[Cost Centre] = Activity.CostCentre
from
	(
	select
		 Period =
			LEFT(
				DATENAME(
					 month
					,TransactionLog.TransactionLogDate
				)
				,3
			) + '_' +
			RIGHT(
				DATENAME(
					 year
					,TransactionLog.TransactionLogDate
				)
				,2
			)


		,Area = Division.Division
		,SubArea = Specialty.Specialty

		,Location =
			case
			when IPOPSTK.IPOPSTK = 'STK'
			then Supplier.SupplierFullName
			else Consultant.Consultant
			end

		,Inpatient =
			case
			when IPOPSTK.IPOPSTK = 'IP'
			then TransactionLog.Cost
			else 0
			end

		,Outpatient =
			case
			when IPOPSTK.IPOPSTK = 'OP'
			then TransactionLog.Cost
			else 0
			end

		,Consultant =
			case
			when IPOPSTK.IPOPSTK <> 'STK'
			then TransactionLog.Cost
			else 0
			end

		,WardStock =
			case
			when IPOPSTK.IPOPSTK = 'STK'
			then TransactionLog.Cost
			else 0
			end

		,TotalCost =
			TransactionLog.Cost

		,CostCentre =
			coalesce(
				 Consultant.CostCentreCode
				,Supplier.CostCentreCode
			)

	from
		Pharmacy.FactTransactionLog TransactionLog

	inner join Pharmacy.OlapSpecialty Specialty
	on	Specialty.SpecialtyRecno = TransactionLog.SpecialtyRecno

	inner join Pharmacy.OlapDivision Division
	on	Division.DivisionCode = Specialty.DivisionCode

	inner join Pharmacy.OlapIPOPSTK IPOPSTK
	on	IPOPSTK.IssueKindCode = TransactionLog.IssueKindCode
	and	IPOPSTK.IssueLabelTypeCode = TransactionLog.IssueLabelTypeCode

	inner join Pharmacy.OlapSupplier Supplier
	on	Supplier.SupplierRecno = TransactionLog.WardRecno

	inner join Pharmacy.OlapConsultant Consultant
	on	Consultant.ConsultantRecno = TransactionLog.ConsultantRecno

	where
		cast(TransactionLog.TransactionLogDate as date) between @FromDate and @ToDate
	and	TransactionLog.PbRFlag = 0

	) Activity
group by
	 Activity.Period
	,Activity.Area
	,Activity.SubArea
	,Activity.Location
	,Activity.CostCentre

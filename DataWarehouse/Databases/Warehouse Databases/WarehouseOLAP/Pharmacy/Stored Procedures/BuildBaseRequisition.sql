﻿create procedure [Pharmacy].[BuildBaseRequisition] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table Pharmacy.BaseRequisition

insert into Pharmacy.BaseRequisition
	(
	 InterfaceCode
	,RequisitionID
	,SiteCode
	,Items
	,PickNo
	,InDisputeUser
	,ShelfPrinted
	,RequisitionNo
	,TaxRatePct
	,TaxInclusive
	,InDispute
	,CustomerOrderNo
	,TaxAmount
	,TaxRateCode
	,PFlag
	,CreatedUser
	,ConversionFactor
	,IssueForm
	,Stocked
	,InternalSiteNo
	,InternalMethodCode
	,SupplierTypeCode
	,OrderedPacks
	,Urgency
	,ToFollow
	,InvoiceNumber
	,PayDate
	,Cost
	,ReceivedPacks
	,ReceivedDate
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderTime
	,LocationCode
	,NSVCode
	,OutstandingPacks
	,Created
	,ByWhom
	,RequisitionRecno
	)
select
	 InterfaceCode
	,RequisitionID
	,SiteCode
	,Items
	,PickNo
	,InDisputeUser
	,ShelfPrinted
	,RequisitionNo
	,TaxRatePct
	,TaxInclusive
	,InDispute
	,CustomerOrderNo
	,TaxAmount
	,TaxRateCode
	,PFlag
	,CreatedUser
	,ConversionFactor
	,IssueForm
	,Stocked
	,InternalSiteNo
	,InternalMethodCode
	,SupplierTypeCode
	,OrderedPacks
	,Urgency
	,ToFollow
	,InvoiceNumber
	,PayDate
	,Cost
	,ReceivedPacks
	,ReceivedDate
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderTime
	,LocationCode
	,NSVCode
	,OutstandingPacks
	,Created
	,ByWhom
	,RequisitionRecno
from
	Warehouse.Pharmacy.Requisition


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.BuildBaseRequisition', @Stats, @StartTime


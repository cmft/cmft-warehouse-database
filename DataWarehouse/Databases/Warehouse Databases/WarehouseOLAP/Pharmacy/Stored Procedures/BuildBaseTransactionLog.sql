﻿create procedure [Pharmacy].[BuildBaseTransactionLog] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table Pharmacy.BaseTransactionLog

insert into Pharmacy.BaseTransactionLog
	(
	 InterfaceCode
	,TransactionLogID
	,RevisionLevel
	,SourcePatientNo
	,CasenoteNo
	,NSVCode
	,PackSize
	,IssueForm
	,Terminal
	,TerminalCode
	,Quantity
	,Cost
	,CostExcludingTax
	,TaxCost
	,TaxCode
	,TaxRate
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,IssueDirections
	,IssueKindCode
	,SiteCode
	,IssueLabelTypeCode
	,EpisodeID
	,PrescriptionNo
	,BatchNo
	,BatchExpiry
	,StockLevel
	,CustomerOrderNo
	,CIVASAmount
	,SiteID
	,CreatedByCode
	,ProductID
	,TransactionLogTime
	,PrescriptionRequestID
	,PrescriberEntityID
	,DispensingRequestID
	,StockValue
	,GPCode
	,Created
	,ByWhom
	)
select
	 InterfaceCode
	,TransactionLogID
	,RevisionLevel
	,SourcePatientNo
	,CasenoteNo
	,NSVCode
	,PackSize
	,IssueForm
	,Terminal
	,TerminalCode
	,Quantity
	,Cost
	,CostExcludingTax
	,TaxCost
	,TaxCode
	,TaxRate
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,IssueDirections
	,IssueKindCode
	,SiteCode
	,IssueLabelTypeCode
	,EpisodeID
	,PrescriptionNo
	,BatchNo
	,BatchExpiry
	,StockLevel
	,CustomerOrderNo
	,CIVASAmount
	,SiteID
	,CreatedByCode
	,ProductID
	,TransactionLogTime
	,PrescriptionRequestID
	,PrescriberEntityID
	,DispensingRequestID
	,StockValue
	,GPCode
	,Created
	,ByWhom
from
	Warehouse.Pharmacy.TransactionLog


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.BuildBaseTransactionLog', @Stats, @StartTime


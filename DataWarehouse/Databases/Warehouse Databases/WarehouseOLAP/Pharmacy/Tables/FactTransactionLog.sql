﻿CREATE TABLE [Pharmacy].[FactTransactionLog] (
    [PatientRecno]          INT          NULL,
    [ConsultantRecno]       INT          NULL,
    [SpecialtyRecno]        INT          NULL,
    [InterfaceCode]         VARCHAR (10) NOT NULL,
    [SiteRecno]             INT          NULL,
    [WardRecno]             INT          NULL,
    [CreatedByRecno]        INT          NULL,
    [TerminalRecno]         INT          NULL,
    [IssueKindCode]         VARCHAR (1)  NULL,
    [IssueLabelTypeCode]    VARCHAR (1)  NULL,
    [TransactionLogDate]    DATE         NULL,
    [TimeBandCode]          INT          NULL,
    [Transactions]          INT          NULL,
    [Quantity]              FLOAT (53)   NULL,
    [Cost]                  FLOAT (53)   NULL,
    [TaxCost]               FLOAT (53)   NULL,
    [CostExcludingTax]      FLOAT (53)   NULL,
    [DefinedDailyDoseUnits] FLOAT (53)   NULL,
    [DefinedDailyDoseValue] FLOAT (53)   NULL,
    [ChemoPbRStatusCode]    TINYINT      NULL,
    [ServiceCost]           FLOAT (53)   NULL,
    [BasicCost]             FLOAT (53)   NULL,
    [ProductStockRecno]     INT          NULL,
    [PbRFlag]               BIT          NULL,
    [BasicCostTax]          FLOAT (53)   NULL,
    [DefinedDailyDoses]     FLOAT (53)   NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_TransactionLog_1]
    ON [Pharmacy].[FactTransactionLog]([PbRFlag] ASC, [TransactionLogDate] ASC)
    INCLUDE([ConsultantRecno], [Cost], [IssueKindCode], [IssueLabelTypeCode], [PatientRecno], [ProductStockRecno], [Quantity], [SpecialtyRecno], [WardRecno]);


GO
CREATE NONCLUSTERED INDEX [IX_Pharmacy_FactTransactionLog_2]
    ON [Pharmacy].[FactTransactionLog]([InterfaceCode] ASC)
    INCLUDE([TransactionLogDate]);


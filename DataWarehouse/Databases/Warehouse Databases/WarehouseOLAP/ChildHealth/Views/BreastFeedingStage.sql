﻿create view [ChildHealth].[BreastFeedingStage] as

select
	 BreastFeedingStage.BreastFeedingStageNo
	,BreastFeedingStage.BreastFeedingStage
from
	Warehouse.ChildHealth.BreastFeedingStage

union all

select
	-1
	,'N/A'

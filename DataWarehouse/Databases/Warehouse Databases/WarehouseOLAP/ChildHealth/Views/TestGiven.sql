﻿create view [ChildHealth].[TestGiven] as

select distinct
	 TestGivenCode =
		coalesce(
			 TestGiven.TestGivenCode
			,'#'
		)

	,TestGiven =
		coalesce(
			 TestGiven.TestGivenCode
			,'N/A'
		)

from
	Warehouse.ChildHealth.Bloodspot TestGiven

﻿
CREATE view [ChildHealth].[FactEventDetail] as

select
	 FactEvent.EntitySourceUniqueID
	,FactEvent.EventDate
	,FactEvent.ModeOfEntryCode
	,FactEvent.ReasonNoVaccineGivenCode
	,FactEvent.ScheduledAntiGensCode
	,FactEvent.VenueTypeCode
	,FactEvent.VenueCode
	,FactEvent.BCGTypeCode

	,DoseCode =
		coalesce(
			AntigenDetail.DoseIdentifier +
			case
			when
				AntigenDetail.SupplementaryDose is null
			or	AntigenDetail.SupplementaryDose = 0
			then ''
			else 'S'
			end
			,'#'
		)

	,AntigenCode = AntigenDetail.AntigenValue

	,ConsentCode =
		case
		when EventConsent.ConsentCode = 'Y'
		then 1 --given
		when EventConsent.ConsentCode is null
		then 3 --pending
		else 2 --not given
		end

	,ConsentDate =
		case
		when ConsentDate.TheDate is null
		then '1 Jan 1900'
		else EventConsent.ConsentDate
		end

	,ReasonForNegativeConsentCode =
		coalesce(
			EventConsent.ReasonForNegativeConsentCode
			,'#'
		)

	,EventDetailCases = 1

from
	ChildHealth.FactEvent

inner join Warehouse.ChildHealth.AntigenDetail
on	AntigenDetail.EntitySourceUniqueID = FactEvent.EntitySourceUniqueID
and	AntigenDetail.EventDate = FactEvent.EventDate
and	AntigenDetail.SequenceNo = FactEvent.SequenceNo

left join Warehouse.ChildHealth.EventConsent
on	EventConsent.EntitySourceUniqueID = AntigenDetail.EntitySourceUniqueID
and	EventConsent.AntigenCode = AntigenDetail.AntigenValue

left join ChildHealth.Calendar ConsentDate
on	ConsentDate.TheDate = EventConsent.ConsentDate


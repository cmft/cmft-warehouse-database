﻿create view [ChildHealth].[FollowUpByWhomType] as

select
	 FollowUpByWhomType.FollowUpByWhomTypeCode
	,FollowUpByWhomType.FollowUpByWhomType
from
	Warehouse.ChildHealth.FollowUpByWhomType

union all

select
	'#'
	,'N/A'

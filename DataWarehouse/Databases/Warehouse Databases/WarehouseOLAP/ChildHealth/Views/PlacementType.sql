﻿create view [ChildHealth].[PlacementType] as

select
	 PlacementType.PlacementTypeCode

	,PlacementType =
		coalesce(
			 PlacementType.PlacementType
			,PlacementType.PlacementTypeCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.PlacementType

union all

select
	 '#'
	,'N/A'

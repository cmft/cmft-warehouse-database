﻿create view [ChildHealth].[ReferredTo] as

select
	 ReferredTo.ReferredToCode
	,ReferredTo.ReferredTo
from
	Warehouse.ChildHealth.ReferredTo

union all

select
	 -1
	,'N/A'

﻿create view [ChildHealth].[RegistrationStatus] as

select
	 RegistrationStatus.RegistrationStatusCode
	,RegistrationStatus.RegistrationStatus
from
	Warehouse.ChildHealth.RegistrationStatus

union all

select
	'#'
	,'N/A'

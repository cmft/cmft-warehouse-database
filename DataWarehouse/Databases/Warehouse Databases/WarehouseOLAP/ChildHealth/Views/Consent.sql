﻿create view [ChildHealth].[Consent] as

select
	 ConsentCode = 1
	,Consent = 'Given'

union all

select
	 ConsentCode = 2
	,Consent = 'Not Given'

union all

select
	 ConsentCode = 3
	,Consent = 'Pending'

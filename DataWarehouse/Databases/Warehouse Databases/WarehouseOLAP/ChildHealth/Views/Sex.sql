﻿create view [ChildHealth].[Sex] as

select
	 SexCode = Sex.ReferenceCode
	,Sex = Sex.Reference
from
	Warehouse.ChildHealth.Reference Sex
where
	Sex.ReferenceDomainCode = 'SEX'

union all

select
	 '#'
	,'N/A'

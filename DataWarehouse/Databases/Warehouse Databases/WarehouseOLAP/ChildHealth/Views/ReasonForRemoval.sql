﻿CREATE view [ChildHealth].[ReasonForRemoval] as

select
	 ReasonForRemovalCode
	,ReasonForRemoval
from
	Warehouse.ChildHealth.ReasonForRemoval


union all

select
	 '#'
	,'N/A'

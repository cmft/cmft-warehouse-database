﻿create view [ChildHealth].[LeadClinicianType] as

select
	 LeadClinicianType.LeadClinicianTypeCode
	,LeadClinicianType.LeadClinicianType
from
	Warehouse.ChildHealth.LeadClinicianType

union all

select
	'#'
	,'N/A'

﻿CREATE view [ChildHealth].[Antigen] as

select
	 Antigen.AntigenCode

	,Antigen =
		coalesce(
			 Antigen.Antigen
			,Antigen.AntigenCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.Antigen

union all

select
	 '#'
	,'N/A'

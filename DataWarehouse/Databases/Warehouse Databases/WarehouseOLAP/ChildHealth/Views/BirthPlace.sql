﻿CREATE view [ChildHealth].[BirthPlace] as

select
	 BirthPlaceCode

	,BirthPlace =
		case
		when BirthPlace.BirthPlace = 'DELETED'
		then BirthPlace.BirthPlaceCode + ' - Deleted'
		else BirthPlace.BirthPlace
		end

from
	Warehouse.ChildHealth.BirthPlace

union all

select
	'#'
	,'N/A'

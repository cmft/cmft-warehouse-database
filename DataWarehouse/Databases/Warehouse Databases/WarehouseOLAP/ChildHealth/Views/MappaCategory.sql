﻿create view [ChildHealth].[MappaCategory] as

select
	 MappaCategory.MappaCategoryCode

	,MappaCategory =
		coalesce(
			 MappaCategory.MappaCategory
			,MappaCategory.MappaCategoryCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.MappaCategory

union all

select
	 '#'
	,'N/A'

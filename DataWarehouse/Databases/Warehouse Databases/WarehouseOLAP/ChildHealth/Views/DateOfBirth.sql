﻿CREATE view [ChildHealth].[DateOfBirth] as

select
	 DateOfBirth = OlapCalendar.TheDate
	,LongDateOfBirth = OlapCalendar.LongDate
	,MonthOfBirth = OlapCalendar.TheMonth
	,QuarterOfBirth = rtrim(OlapCalendar.CalendarQuarter) + ' ' + OlapCalendar.CalendarYear
	,YearOfBirth = OlapCalendar.CalendarYear
	,OlapCalendar.FinancialMonthKey
	,OlapCalendar.FinancialQuarterKey
from
	dbo.OlapCalendar

where
	exists
	(
	select
		1
	from
		ChildHealth.Child
	where
		Child.DateOfBirth = OlapCalendar.TheDate
	)

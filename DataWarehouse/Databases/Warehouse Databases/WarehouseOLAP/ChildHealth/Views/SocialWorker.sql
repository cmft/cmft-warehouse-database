﻿
create view [ChildHealth].[SocialWorker] as

select distinct
	 SocialWorkerCode =
		coalesce(
			 SocialWorker.SocialWorker
			,'#'
		)

	,SocialWorker =
		coalesce(
			 SocialWorker.SocialWorker
			,'N/A'
		)

from
	Warehouse.ChildHealth.LookedAfter SocialWorker


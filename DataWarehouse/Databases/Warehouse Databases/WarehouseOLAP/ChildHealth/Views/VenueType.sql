﻿create view [ChildHealth].[VenueType] as

select
	 VenueType.VenueTypeCode
	,VenueType.VenueType
from
	Warehouse.ChildHealth.VenueType

union all

select
	'#'
	,'N/A'

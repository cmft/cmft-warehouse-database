﻿CREATE view [ChildHealth].[HEPBStatus] as

select
	 HEPBStatus.HEPBStatusCode
	,HEPBStatus.HEPBStatus
from
	Warehouse.ChildHealth.HEPBStatus

union all

select
	'#'
	,'N/A'

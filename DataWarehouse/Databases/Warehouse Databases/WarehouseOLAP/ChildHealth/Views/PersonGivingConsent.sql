﻿create view [ChildHealth].[PersonGivingConsent] as

select
	 PersonGivingConsent.PersonGivingConsentCode
	,PersonGivingConsent.PersonGivingConsent
from
	Warehouse.ChildHealth.PersonGivingConsent

union all

select
	 '#'
	,'N/A'

﻿CREATE view [ChildHealth].[Exam] as

select
	 ExamCode
	,Exam = ExamCode + ' - ' + Exam
from
	Warehouse.ChildHealth.Exam

union all

select
	 '#'
	,'N/A'

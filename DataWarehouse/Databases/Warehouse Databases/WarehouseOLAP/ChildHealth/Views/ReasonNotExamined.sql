﻿create view [ChildHealth].[ReasonNotExamined] as

select
	 ReasonNotExamined.ReasonNotExaminedCode
	,ReasonNotExamined.ReasonNotExamined
from
	Warehouse.ChildHealth.ReasonNotExamined

union all

select
	'#'
	,'N/A'

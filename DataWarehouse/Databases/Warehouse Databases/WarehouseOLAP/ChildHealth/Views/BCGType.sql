﻿create view [ChildHealth].[BCGType] as

select
	 BCGType.BCGTypeCode

	,BCGType =
		coalesce(
			 BCGType.BCGType
			,BCGType.BCGTypeCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.BCGType

union all

select
	 '#'
	,'N/A'

﻿create view [ChildHealth].[BreastFeedingResult] as

select
	 BreastFeedingResult.BreastFeedingResultCode
	,BreastFeedingResult.BreastFeedingResult
from
	Warehouse.ChildHealth.BreastFeedingResult

union all

select
	 '#'
	,'N/A'

﻿CREATE view [ChildHealth].[FlagReasonForRemoval] as

select
	 FlagReasonForRemovalCode
	,FlagReasonForRemoval
from
	Warehouse.ChildHealth.FlagReasonForRemoval

union all

select
	'#'
	,'Currently Active'

﻿


CREATE view [ChildHealth].[FactLookedAfterChild] as

select
	 LookedAfter.EntitySourceUniqueID

	,LookedAfterDate =
		case
		when LookedAfter.LookedAfterDate = '31 Dec 1899'
		then '1 Jan 1900'
		else LookedAfter.LookedAfterDate
		end

	,HealthNotifiedDate =
		coalesce(
			 LookedAfter.HealthNotifiedDate
			,'1 Jan 1900'
		)

	,RemovedDate =
		coalesce(
			 LookedAfter.RemovedDate
			,'1 Jan 1900'
		)

	,LastDentalExamDate =
		coalesce(
			 LookedAfter.LastDentalExamDate
			,'1 Jan 1900'
		)

	,LookedAfterRemovalReasonCode =
		coalesce(
			 LookedAfter.LookedAfterRemovalReasonCode
			,'#'
		)

	,LookedAfterStatusCode =
		coalesce(
			 LookedAfter.LookedAfterStatusCode
			,'#'
		)

	,LeadClinicianTypeCode =
		coalesce(
			 LookedAfter.LeadClinicianTypeCode
			,'#'
		)

	,LeadClinicianCode =
		coalesce(
			 LookedAfter.LeadClinicianCode
			,'#'
		)

	,SocialServicesDepartmentCode =
		coalesce(
			 LookedAfter.SocialServicesDepartmentCode
			,'#'
		)

	,SocialWorker =
		coalesce(
			 LookedAfter.SocialWorker
			,'#'
		)

	,Supervision =
		cast(
			case
			when LookedAfter.Supervision = 'Yes'
			then 1
			else 0
			end
			as bit
		)

	,SupervisionStartDate =
		coalesce(
			 LookedAfter.SupervisionStartDate
			,'1 Jan 1900'
		)

	,SupervisionEndDate =
		coalesce(
			 LookedAfter.SupervisionEndDate
			,'1 Jan 1900'
		)

	,PlacementTypeCode =
		coalesce(
			 LookedAfter.PlacementTypeCode
			,'#'
		)

	,OriginatingPCTCode =
		coalesce(
			 LookedAfter.OriginatingPCTCode
			,'#'
		)

	,ResidingPCTCode =
		coalesce(
			 LookedAfter.ResidingPCTCode
			,'#'
		)

	,PayingPCTCode =
		coalesce(
			 LookedAfter.PayingPCTCode
			,'#'
		)

	,UnaccompaniedAsylumSeeker =
		cast(
			case
			when LookedAfter.UnaccompaniedAsylumSeeker = 'N'
			then 0
			else 1
			end
			as bit
		)


	,ImmsUptoDateFlag =
		cast(
			case
			when LookedAfter.ImmsUptoDateFlag = 'N'
			then 0
			else 1
			end
			as bit
		)

	,ImmsUptoDateDecisionDate =
		coalesce(
			 LookedAfter.ImmsUptoDateDecisionDate
			,'1 Jan 1900'
		)

	,ImmsUptoDateClinician =
		coalesce(
			 LookedAfter.ImmsUptoDateClinician
			,'#'
		)

	,CarerDetail =
		coalesce(
			 LookedAfterCarerDetail.CarerDetail
			,'#'
		)

	,CarerStartDate =
		coalesce(
	 		case
			when LookedAfterCarerDetail.StartDate = '31 Dec 1899'
			then '1 Jan 1900'
			else LookedAfterCarerDetail.StartDate
			end

			,'1 Jan 1900'
		)

	,CarerEndDate =
		coalesce(
	 		case
			when LookedAfterCarerDetail.EndDate = '31 Dec 1899'
			then '1 Jan 1900'
			else LookedAfterCarerDetail.EndDate
			end

			,'1 Jan 1900'
		)

	,LookedAfterCases = 1

from
	Warehouse.ChildHealth.LookedAfter

left join Warehouse.ChildHealth.LookedAfterCarerDetail
on	LookedAfterCarerDetail.EntitySourceUniqueID = LookedAfter.EntitySourceUniqueID
and	LookedAfterCarerDetail.LookedAfterDate = LookedAfter.LookedAfterDate
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.LookedAfterCarerDetail Previous
	where
		Previous.EntitySourceUniqueID = LookedAfter.EntitySourceUniqueID
	and	Previous.LookedAfterDate = LookedAfter.LookedAfterDate
	and	(
			Previous.StartDate > LookedAfterCarerDetail.StartDate
		or	(
				Previous.StartDate = LookedAfterCarerDetail.StartDate
			and	Previous.EndDate is null
			)
		)
	)




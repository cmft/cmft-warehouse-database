﻿create view [ChildHealth].[FactAEAttendance] as

select
	 AEAttendance.EntitySourceUniqueID

	,AEAttendance.SequenceNo

	,AEAttendance.AttendanceDate

	,VenueTypeCode =
		coalesce(
			 AEAttendance.VenueTypeCode
			,'#'
		)

	,VenueCode =
		coalesce(
			 AEAttendance.VenueCode
			,'#'
		)

	,AEDiagnosisCode =
		coalesce(
			 AEAttendance.AEDiagnosisCode
			,'#'
		)

	,ReasonForDischargeCode =
		coalesce(
			 AEAttendance.ReasonForDischargeCode
			,'#'
		)

	,FollowUpByWhomCode =
		coalesce(
			 AEAttendance.FollowUpByWhomCode
			,'#'
		)

	,FollowUpByWhomTypeCode =
		coalesce(
			 AEAttendance.FollowUpByWhomTypeCode
			,'#'
		)

	,FollowUp =
		cast(
			case
			when AEAttendance.FollowUp = 'Y'
			then 1
			else 0
			end
			as bit
		)

	,AEAttendanceCases = 1

from
	Warehouse.ChildHealth.AEAttendance

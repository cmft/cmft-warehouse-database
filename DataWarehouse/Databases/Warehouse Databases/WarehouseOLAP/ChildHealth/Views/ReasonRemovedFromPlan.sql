﻿create view [ChildHealth].[ReasonRemovedFromPlan] as

select
	 ReasonRemovedFromPlan.ReasonRemovedFromPlanCode
	,ReasonRemovedFromPlan.ReasonRemovedFromPlan
from
	Warehouse.ChildHealth.ReasonRemovedFromPlan

union all

select
	'#'
	,'N/A'

﻿CREATE view [ChildHealth].[FactChild] as

select
	 Entity.SourceUniqueID
	
	,Cases = 1

	,Birth.BirthWeight

	,PreviousLiveBirths = 
		case
		when isnumeric(Birth.PreviousLiveBirths) = 1
		then cast(Birth.PreviousLiveBirths as int)
		else null
		end

	,PreviousMiscarriages = 
		case
		when isnumeric(Birth.PreviousMiscarriages) = 1
		then cast(Birth.PreviousMiscarriages as int)
		else null
		end

	,PreviousAbortions = 
		case
		when isnumeric(Birth.PreviousAbortions) = 1
		then cast(Birth.PreviousAbortions as int)
		else null
		end

	,PreviousStillBirths = 
		case
		when isnumeric(Birth.PreviousStillBirths) = 1
		then cast(Birth.PreviousStillBirths as int)
		else null
		end

	,YellowBanding =
		case
		when exists
			(
			select
				1
			from
				Warehouse.ChildHealth.ChildFlag Banding
			where
				Banding.EntitySourceUniqueID = Entity.SourceUniqueID
			and	Banding.RemovedDate is null
			and	Banding.FlagCode = 'Y'
			)
		then 1
		else 0
		end

	,GreenBanding =
		case
		when exists
			(
			select
				1
			from
				Warehouse.ChildHealth.ChildFlag Banding
			where
				Banding.EntitySourceUniqueID = Entity.SourceUniqueID
			and	Banding.RemovedDate is null
			and	Banding.FlagCode = 'G'
			)
		then 1
		else 0
		end

	,BlueBanding =
		case
		when exists
			(
			select
				1
			from
				Warehouse.ChildHealth.ChildFlag Banding
			where
				Banding.EntitySourceUniqueID = Entity.SourceUniqueID
			and	Banding.RemovedDate is null
			and	Banding.FlagCode = 'B'
			)
		then 1
		else 0
		end

from
	Warehouse.ChildHealth.Entity

inner join Warehouse.ChildHealth.Birth
on	Birth.EntitySourceUniqueID = Entity.SourceUniqueID

where
	Entity.TypeCode = 'P' --Person? Patient?

﻿CREATE view [ChildHealth].[FactChildConsent] as

select
	 ChildConsent.EntitySourceUniqueID
	,ChildConsent.SystemOrDepartmentCode
	,ChildConsent.ConsentDate

	,ConsentCode =
		case
		when ConsentCode is null then 3 --Pending
		when ConsentCode = 'Y' then 1 --Given
		when ConsentCode = 'N' then 2 --Not Given
		else 3 --Pending
		end

	,PersonGivingConsentCode =
		coalesce(
			 ChildConsent.PersonGivingConsentCode
			,'#'
		)

	,PersonGivingConsentTypeCode =
		coalesce(
			 ChildConsent.PersonGivingConsentTypeCode
			,'#'
		)

	,ConsentCases = 1
from
	Warehouse.ChildHealth.ChildConsent

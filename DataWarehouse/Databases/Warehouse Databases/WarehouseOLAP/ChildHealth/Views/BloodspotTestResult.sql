﻿create view [ChildHealth].[BloodspotTestResult] as

select
	 BloodspotTestResult.BloodspotTestResultCode

	,BloodspotTestResult =
		coalesce(
			 BloodspotTestResult.BloodspotTestResult
			,BloodspotTestResult.BloodspotTestResultCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.BloodspotTestResult

union all

select
	 '#'
	,'N/A'

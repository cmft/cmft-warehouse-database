﻿create view [ChildHealth].[ModeOfEntry] as

select
	 ModeOfEntry.ModeOfEntryCode
	,ModeOfEntry.ModeOfEntry
from
	Warehouse.ChildHealth.ModeOfEntry

union all

select
	'#'
	,'N/A'

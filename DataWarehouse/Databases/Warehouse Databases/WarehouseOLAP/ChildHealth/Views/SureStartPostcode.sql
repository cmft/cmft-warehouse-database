﻿CREATE view [ChildHealth].[SureStartPostcode] as

select
	 SureStartPostcode.Postcode

	,ChildrensCentre = 
		coalesce(
			SureStartPostcode.ChildrensCentre
			,'N/A'
		)

	,Ward2004 = 
		coalesce(
			SureStartPostcode.Ward2004
			,'N/A'
		)

	,LowerSuperOutputArea =
		coalesce(
			SureStartPostcode.LowerSuperOutputArea
			,'N/A'
		)
from
	Warehouse.ChildHealth.SureStartPostcode

union all

select distinct
	 Child.Postcode
	,'N/A'
	,'N/A'
	,'N/A'
from
	ChildHealth.Child
where
	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.SureStartPostcode
	where
		SureStartPostcode.Postcode = Child.Postcode
	)


union all

select
	'#'
	,'N/A'
	,'N/A'
	,'N/A'

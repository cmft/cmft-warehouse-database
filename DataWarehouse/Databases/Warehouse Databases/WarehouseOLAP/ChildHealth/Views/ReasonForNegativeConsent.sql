﻿create view [ChildHealth].[ReasonForNegativeConsent] as

select
	 ReasonForNegativeConsent.ReasonForNegativeConsentCode
	,ReasonForNegativeConsent.ReasonForNegativeConsent
from
	Warehouse.ChildHealth.ReasonForNegativeConsent

union all

select
	 '#'
	,'N/A'

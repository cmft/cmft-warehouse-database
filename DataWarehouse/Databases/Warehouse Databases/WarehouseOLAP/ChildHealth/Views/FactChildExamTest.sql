﻿CREATE view [ChildHealth].[FactChildExamTest] as

select
	 FactChildExam.*

	,ExamHistoryTestResult.TestCode

	,TestResult =
		coalesce(
			 ExamHistoryTestResult.Result
			,'N/A'
		)

	,ReferredToCode =
		coalesce(
			 ExamHistoryTestResult.ReferredToCode
			,-1
		)

	,TestCases = 1

from
	ChildHealth.FactChildExam

inner join Warehouse.ChildHealth.ExamHistoryTestResult
on	ExamHistoryTestResult.EntitySourceUniqueID = FactChildExam.EntitySourceUniqueID
and	ExamHistoryTestResult.ExamDate = FactChildExam.ExamDate
and	ExamHistoryTestResult.ExamCode = FactChildExam.ExamCode

﻿CREATE view [ChildHealth].[EthnicOrigin] as

select
	 EthnicOriginCode = EthnicOrigin.ReferenceCode
	,EthnicOrigin = EthnicOrigin.Reference
from
	Warehouse.ChildHealth.Reference EthnicOrigin
where
	EthnicOrigin.ReferenceDomainCode = 'ETH'

union all

select
	 '#'
	,'N/A'

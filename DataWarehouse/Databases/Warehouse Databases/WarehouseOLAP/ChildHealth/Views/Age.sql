﻿create view ChildHealth.Age as

select
	 AgeBase.AgeCode
	,AgeBase.Age
	,AgeBase.AgeParentCode


	,AgeInMonthsKey =
			coalesce(
				 AgeBase.AgeInMonths
				,99999
			)

	,AgeInMonths =
			case
			when AgeBase.AgeInMonths is null
			then 'N/A'

			when AgeBase.AgeInMonths = 1200
			then '1200+ Months'

			else 
				CAST(
					AgeBase.AgeInMonths
					as varchar
				) + ' < ' +
				cast(
					(
					select
						NextAgeInMonths =
							min(NextAge.AgeInMonths)
					from
						ChildHealth.AgeBase NextAge
					where
						NextAge.AgeInMonths > AgeBase.AgeInMonths
					)
					as varchar
				) + ' Months'

			end

	,AgeUpto4HalfYears =
		CAST(
			case
			when AgeBase.AgeInMonths is null
			then 0
			when AgeBase.AgeInMonths < 54
			then 1
			else 0
			end
			as bit
		)

	,AgeUpto5Years =
		CAST(
			case
			when AgeBase.AgeInMonths is null
			then 0
			when AgeBase.AgeInMonths < 60
			then 1
			else 0
			end
			as bit
		)

from
	ChildHealth.AgeBase


﻿CREATE view [ChildHealth].[FactChildExam] as

select
	 ExamHistory.EntitySourceUniqueID

	,VenueCode =
		case
		when ExamHistory.ClinicCode is null
		then '#'
		else ExamHistory.ClinicCode
		end

	,VenueTypeCode =
		case
		when ExamHistory.VenueTypeCode is null
		then '#'
		else ExamHistory.VenueTypeCode
		end

	,Examined =
		CAST(
			case
			when ExamHistory.Examined = 'N'
			then 0
			else 1
			end
			as bit
		)

	,ReasonForDNACode =
		case
		when ExamHistory.ReasonForDNACode is null
		then '#'
		else ExamHistory.ReasonForDNACode
		end

	,ReasonNotExaminedCode =
		case
		when ExamHistory.ReasonNotExaminedCode is null
		then '#'
		else ExamHistory.ReasonNotExaminedCode
		end

	,ExamDate =
		case
		when ExamHistory.ExamDate > GETDATE()
		then '1 Jan 1901'
		else ExamHistory.ExamDate
		end

	,ExamHistory.ExamCode

	,ConsentCode =
		case
		when ExamConsent.ConsentCode = 'Y'
		then 1 --given
		when ExamConsent.ConsentCode is null
		then 3 --pending
		else 2 --not given
		end

	,ConsentDate =
		coalesce(
			 ExamConsent.ConsentDate
			,'1 Jan 1900'
		)

	,ReasonForNegativeConsentCode =
		coalesce(
			ExamConsent.ReasonForNegativeConsentCode
			,'#'
		)

	,RecallDate =
		coalesce(
			 ExamHistory.RecallDate
			,'1 Jan 1900'
		)

	,RecallExamCode =
		coalesce(
			ExamHistory.RecallExamCode
			,'#'
		)

	,RecallVenueCode =
		coalesce(
			ExamHistory.RecallVenueCode
			,'#'
		)

	,RecallVenueTypeCode =
		coalesce(
			ExamHistory.RecallVenueTypeCode
			,'#'
		)


	,ExamCases = 1

	,ExamRecall =
		case
		when ExamHistory.RecallDate is null
		then 0
		else 1
		end
from
	Warehouse.ChildHealth.ExamHistory

left join Warehouse.ChildHealth.ExamConsent
on	ExamConsent.EntitySourceUniqueID = ExamHistory.EntitySourceUniqueID
and	ExamConsent.ExamCode = ExamHistory.ExamCode

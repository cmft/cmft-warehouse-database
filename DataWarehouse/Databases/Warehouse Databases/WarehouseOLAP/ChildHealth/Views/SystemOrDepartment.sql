﻿create view [ChildHealth].[SystemOrDepartment] as

select
	 SystemOrDepartment.SystemOrDepartmentCode
	,SystemOrDepartment.SystemOrDepartment
from
	Warehouse.ChildHealth.SystemOrDepartment

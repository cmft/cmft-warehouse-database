﻿create view [ChildHealth].[Dose] as

select
	 DoseCode
	,Dose = DoseCode
from
	(
	select distinct
		DoseCode =
			AntigenDetail.DoseIdentifier +
			case
			when
				AntigenDetail.SupplementaryDose is null
			or	AntigenDetail.SupplementaryDose = 0
			then ''
			else 'S'
			end
	from
		Warehouse.ChildHealth.AntigenDetail
	where
		AntigenDetail.DoseIdentifier is not null
	) Dose

union all

select
	'#'
	,'N/A'

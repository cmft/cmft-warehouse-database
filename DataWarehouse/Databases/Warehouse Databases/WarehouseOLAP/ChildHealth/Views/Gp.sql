﻿create view [ChildHealth].[Gp] as

select
	 Gp.GpCode
	,Gp =
		coalesce(
			 Gp.Surname + coalesce(', ' + Gp.Initials, '')
			,Gp.Address1
			,'No Description'
		)
	,NationalGpCode = NationalCode
from
	Warehouse.ChildHealth.Gp

union all

select distinct
	 coalesce(GpCode, -1)
	,coalesce(GpCode + ' - No Description', 'N/A')
	,'N/A'
from
	Warehouse.ChildHealth.Entity
where
	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.Gp
	where
		Gp.GPCode = Entity.GPCode
	)

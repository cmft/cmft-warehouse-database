﻿create view [ChildHealth].[BloodspotTest] as

select
	 BloodspotTest.BloodspotTestCode

	,BloodspotTest =
		coalesce(
			 BloodspotTest.BloodspotTest
			,BloodspotTest.BloodspotTestCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.BloodspotTest

union all

select
	 '#'
	,'N/A'

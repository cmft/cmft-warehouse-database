﻿create view [ChildHealth].[ReasonNoVaccineGiven] as

select
	 ReasonNoVaccineGiven.ReasonNoVaccineGivenCode
	,ReasonNoVaccineGiven.ReasonNoVaccineGiven
from
	Warehouse.ChildHealth.ReasonNoVaccineGiven

union all

select
	 '#'
	,'N/A'

﻿create view [ChildHealth].[ReasonForDNA] as

select
	 ReasonForDNA.ReasonForDNACode
	,ReasonForDNA.ReasonForDNA
from
	Warehouse.ChildHealth.ReasonForDNA

union all

select
	'#'
	,'N/A'

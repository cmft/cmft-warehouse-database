﻿create view [ChildHealth].[StaffType] as

select
	 StaffTypeCode = StaffType.ReferenceCode
	,StaffType = StaffType.Reference
from
	Warehouse.ChildHealth.Reference StaffType
where
	StaffType.ReferenceDomainCode = 'STC'

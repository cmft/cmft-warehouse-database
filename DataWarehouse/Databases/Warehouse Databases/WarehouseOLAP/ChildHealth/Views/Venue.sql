﻿create view [ChildHealth].[Venue] as

select
	 Venue.VenueCode
	,Venue.Venue
from
	Warehouse.ChildHealth.Venue

union all

select
	'#'
	,'N/A'

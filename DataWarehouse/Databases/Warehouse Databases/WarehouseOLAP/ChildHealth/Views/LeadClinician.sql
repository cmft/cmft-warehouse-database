﻿create view [ChildHealth].[LeadClinician] as

select
	 LeadClinician.LeadClinicianCode
	,LeadClinician.LeadClinician
from
	Warehouse.ChildHealth.LeadClinician

union all

select
	'#'
	,'N/A'

﻿create view [ChildHealth].[PersonGivingConsentType] as

select
	 PersonGivingConsentType.PersonGivingConsentTypeCode
	,PersonGivingConsentType.PersonGivingConsentType
from
	Warehouse.ChildHealth.PersonGivingConsentType

union all

select
	 '#'
	,'N/A'

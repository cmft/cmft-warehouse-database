﻿CREATE view [ChildHealth].[Outcome] as

select
	 OutcomeCode
	,Outcome
from
	Warehouse.ChildHealth.Outcome

union all

select
	 '#'
	,'N/A'

﻿CREATE view [ChildHealth].[Staff] as

select distinct
	 Staff.StaffCode

	,StaffName =
		coalesce(
			 Entity.Forename + ' '
			,''
		) +
		coalesce(
			 Entity.Surname
			,cast(Staff.StaffCode as varchar) + ' - No Description'
		) +
		case
		when
			Entity.Forename = 'Not'
		and Entity.Surname = 'Known'
		then ' (' + cast(Staff.StaffCode as varchar) + ')'
		else ''
		end

	--,PersonType =
	--	coalesce(
	--		 PersonType.Reference
	--		,'N/A'
	--	)

from
	Warehouse.ChildHealth.ChildHealthEncounter

inner join Warehouse.ChildHealth.Staff
on	Staff.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID

left join Warehouse.ChildHealth.Entity
on	Entity.SourceUniqueID = Staff.StaffCode
and	Entity.TypeCode = 'E'

--left join Warehouse.ChildHealth.Reference PersonType
--on	PersonType.ReferenceDomainCode = 'APR'
--and	PersonType.ReferenceCode = Staff.PersonTypeCode

where
	exists
	(
	select
		1
	from
		ChildHealth.FactChild
	where
		FactChild.SourceUniqueID = ChildHealthEncounter.EntitySourceUniqueID
	)

union all

select
	-1
	,'N/A'
	--,'N/A'

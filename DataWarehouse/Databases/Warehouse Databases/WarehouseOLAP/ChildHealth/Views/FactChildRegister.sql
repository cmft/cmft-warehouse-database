﻿create view [ChildHealth].[FactChildRegister] as

select
	 ChildRegister.EntitySourceUniqueID
	,ChildRegister.RegisterCode
	,ChildRegister.OnRegisterDate

	,OffRegisterDate =
		coalesce(
			 ChildRegister.OffRegisterDate
			,'1 Jan 1900'
		)

	,OnRegister =
		cast(
			case
			when ChildRegister.OffRegisterDate is null
			then 1
			else 0
			end
			as bit
		)

	,ReasonForRemovalCode =
		coalesce(
			 ChildRegister.ReasonForRemovalCode
			,'#'
		)

	,RegisterStaffCode =
		coalesce(
			 ChildRegister.RegisterStaffCode
			,'#'
		)

	,RegisterStaffTypeCode =
		coalesce(
			 ChildRegister.RegisterStaffTypeCode
			,'#'
		)

	,ChildRegisterCases = 1

from
	Warehouse.ChildHealth.ChildRegister

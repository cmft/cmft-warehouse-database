﻿create view [ChildHealth].[BirthState] as

select
	 BirthState.BirthStateCode
	,BirthState.BirthState
from
	Warehouse.ChildHealth.BirthState

union all

select
	'#'
	,'N/A'

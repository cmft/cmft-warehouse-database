﻿create view [ChildHealth].[BirthWeightBand] as

select
	 BirthWeightBandCode = 1
	,BirthWeightBand = 'Low'

union all

select
	 BirthWeightBandCode = 2
	,BirthWeightBand = 'High'

union all

select
	 BirthWeightBandCode = 99
	,BirthWeightBand = 'N/A'

﻿CREATE view [ChildHealth].[CarerDetail] as

select distinct
	 CarerDetailCode =
		coalesce(
			 CarerDetail.CarerDetail
			,'#'
		)

	,CarerDetail =
		coalesce(
			 CarerDetail.CarerDetail
			,'N/A'
		)
from
	Warehouse.ChildHealth.LookedAfterCarerDetail CarerDetail

union

select
	 '#'
	,'N/A'

﻿CREATE view [ChildHealth].[FactEvent] as

select
	 Event.EntitySourceUniqueID

	,Event.SequenceNo

	,Event.EventDate

	,ModeOfEntryCode =
		coalesce(
			 Event.ModeOfEntryCode
			,'#'
		)

	,ReasonNoVaccineGivenCode =
		coalesce(
			 Event.ReasonNoVaccineGivenCode
			,'#'
		)

	,ScheduledAntiGensCode =
		coalesce(
			 Event.ScheduledAntiGensCode
			,'#'
		)

	,VenueTypeCode =
		coalesce(
			 Event.VenueTypeCode
			,'#'
		)

	,VenueCode =
		coalesce(
			 Event.ClinicCode
			,'#'
		)

	,BCGTypeCode =
		coalesce(
			 Event.BCGTypeCode
			,'#'
		)

	,PlannedSessionCode =
		coalesce(
			 BCG.PlannedSessionCode
			,'#'
		)

	,EventCases = 1

from
	Warehouse.ChildHealth.Event

left join Warehouse.ChildHealth.BCG
on	BCG.EntitySourceUniqueID = Event.EntitySourceUniqueID
and	BCG.EventDate = Event.EventDate

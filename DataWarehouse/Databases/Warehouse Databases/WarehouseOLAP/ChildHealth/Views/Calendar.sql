﻿CREATE view [ChildHealth].[Calendar] as

select
	 TheDate
	,DayOfWeek = case when TheDate = '1 Jan 1900' then 'N/A' else DayOfWeek end
	,DayOfWeekKey = case when TheDate = '1 Jan 1900' then 0 else DayOfWeekKey end
	,LongDate = case when TheDate = '1 Jan 1900' then 'N/A' else LongDate end
	,TheMonth = case when TheDate = '1 Jan 1900' then 'N/A' else TheMonth end
	,FinancialQuarter = case when TheDate = '1 Jan 1900' then 'N/A' else FinancialQuarter end
	,FinancialYear = case when TheDate = '1 Jan 1900' then 'N/A' else FinancialYear end
	,FinancialMonthKey = case when TheDate = '1 Jan 1900' then 0 else FinancialMonthKey end
	,FinancialQuarterKey = case when TheDate = '1 Jan 1900' then 0 else FinancialQuarterKey end
	,CalendarQuarter = case when TheDate = '1 Jan 1900' then 'N/A' else CalendarQuarter end
	,CalendarSemester = case when TheDate = '1 Jan 1900' then 'N/A' else CalendarSemester end
	,CalendarYear = case when TheDate = '1 Jan 1900' then 'N/A' else CalendarYear end
	,LastCompleteMonth
	,WeekNoKey = case when TheDate = '1 Jan 1900' then 'N/A' else WeekNoKey end
	,WeekNo = case when TheDate = '1 Jan 1900' then 'N/A' else WeekNo end
	,FirstDayOfWeek
	,LastDayOfWeek
from
	dbo.OlapCalendar

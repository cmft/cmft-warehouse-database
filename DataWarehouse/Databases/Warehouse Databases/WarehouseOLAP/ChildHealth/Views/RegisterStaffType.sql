﻿CREATE view [ChildHealth].[RegisterStaffType] as

select
	 RegisterStaffTypeCode
	,RegisterStaffType
from
	Warehouse.ChildHealth.RegisterStaffType


union all

select
	 '#'
	,'N/A'

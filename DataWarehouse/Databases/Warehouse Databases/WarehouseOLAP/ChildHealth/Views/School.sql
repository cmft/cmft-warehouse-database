﻿CREATE view [ChildHealth].[School] as

select
	 School.SchoolCode

	,School =
		case
		when School.School = 'Not Known'
		then School.School + ' (' + School.SchoolCode + ')'
		else School.School
		end

	,SchoolCategory =
		coalesce(
			 SchoolCategory.SchoolCategory
			,'N/A'
		)

	,DepartmentOfEducationCode =
		coalesce(
			 School.DepartmentOfEducationCode
			,'N/A'
		)

	,NationalSchoolCode =
		coalesce(
			 School.NationalSchoolCode
			,'N/A'
		)

from
	Warehouse.ChildHealth.School

left join Warehouse.ChildHealth.SchoolCategory
on	SchoolCategory.SchoolCategoryCode = School.SchoolCategoryCode

union all

select
	 '#'
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'

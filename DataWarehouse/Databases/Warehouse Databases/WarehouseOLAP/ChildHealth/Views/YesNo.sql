﻿create view [ChildHealth].[YesNo] as

select
	 OlapYesNo.YesNoFlag
	,OlapYesNo.YesNo
from
	OlapYesNo

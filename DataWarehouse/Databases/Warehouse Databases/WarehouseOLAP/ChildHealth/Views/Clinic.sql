﻿CREATE view [ChildHealth].[Clinic] as

select
	 ClinicCode
	,Clinic =
		coalesce(
			 Clinic + ' (' + ClinicCode + ')'
			,ClinicCode + ' - No Description'
		)

from
	Warehouse.ChildHealth.Clinic

union all

select
	'#'
	,'N/A'

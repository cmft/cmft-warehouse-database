﻿create view [ChildHealth].[LookedAfterStatus] as

select
	 LookedAfterStatus.LookedAfterStatusCode

	,LookedAfterStatus =
		coalesce(
			 LookedAfterStatus.LookedAfterStatus
			,LookedAfterStatus.LookedAfterStatusCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.LookedAfterStatus

union all

select
	 '#'
	,'N/A'

﻿CREATE view [ChildHealth].[FactSafeguarding] as

select
	 ChildProtectionPlan.EntitySourceUniqueID

	,ChildProtectionPlan.PlacedOnPlanDate

	,RemovedFromPlanDate =
		coalesce(
			 ChildProtectionPlan.RemovedFromPlanDate
			,'1 Jan 1900'
		)

	,ReasonRemovedFromPlanCode =
		coalesce(
			 ChildProtectionPlan.ReasonRemovedFromPlanCode
			,'#'
		)

	,LeadClinicianTypeCode =
		coalesce(
			 ChildProtectionPlan.LeadClinicianTypeCode
			,'#'
		)

	,LeadClinicianCode =
		coalesce(
			 ChildProtectionPlan.LeadClinicianCode
			,'#'
		)

	,SocialServicesDepartmentCode =
		coalesce(
			 ChildProtectionPlan.SocialServicesDepartmentCode
			,'#'
		)

	,VulnerabilityFactorCode =
		coalesce(
			 ChildProtectionPlan.VulnerabilityFactorCode
			,'#'
		)

	,Supervision =
		cast(
			case
			when ChildProtectionPlan.Supervision = 'Y'
			then 1
			else 0
			end
			as bit
		)

	,SupervisionStartDate =
		coalesce(
			 ChildProtectionPlan.SupervisionStartDate
			,'1 Jan 1900'
		)

	,SupervisionEndDate =
		coalesce(
			 ChildProtectionPlan.SupervisionEndDate
			,'1 Jan 1900'
		)

	,MappaCategoryCode =
		coalesce(
			 ChildProtectionMappa.MappaCategoryCode
			,'#'
		)

	,MaracReasonForReferralCode =
		coalesce(
			 ChildProtectionMarac.MaracReasonForReferralCode
			,'#'
		)

	,SafeguardingCases = 1

from
	Warehouse.ChildHealth.ChildProtectionPlan

left join Warehouse.ChildHealth.ChildProtectionMappa
on	ChildProtectionMappa.EntitySourceUniqueID = ChildProtectionPlan.EntitySourceUniqueID
and	ChildProtectionMappa.IncidentDate >= ChildProtectionPlan.PlacedOnPlanDate
and	ChildProtectionMappa.EndDate is null
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.ChildProtectionMappa Previous
	where
		Previous.EntitySourceUniqueID = ChildProtectionPlan.EntitySourceUniqueID
	and	Previous.IncidentDate >= ChildProtectionPlan.PlacedOnPlanDate
	and	Previous.EndDate is null
	and	Previous.SequenceNo > ChildProtectionMappa.SequenceNo
	)

left join Warehouse.ChildHealth.ChildProtectionMarac
on	ChildProtectionMarac.EntitySourceUniqueID = ChildProtectionPlan.EntitySourceUniqueID
and	ChildProtectionMarac.ReferredDate >= ChildProtectionPlan.RemovedFromPlanDate
and	ChildProtectionMarac.EndDate is null
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.ChildProtectionMarac Previous
	where
		Previous.EntitySourceUniqueID = ChildProtectionPlan.EntitySourceUniqueID
	and	Previous.ReferredDate >= ChildProtectionPlan.PlacedOnPlanDate
	and	Previous.EndDate is null
	and	Previous.SequenceNo > ChildProtectionMarac.SequenceNo
	)

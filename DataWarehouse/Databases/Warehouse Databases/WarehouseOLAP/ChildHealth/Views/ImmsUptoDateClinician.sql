﻿create view [ChildHealth].[ImmsUptoDateClinician] as

select distinct
	 ImmsUptoDateClinicianCode =
		coalesce(
			 ImmsUptoDateClinician.ImmsUptoDateClinician
			,'#'
		)

	,ImmsUptoDateClinician =
		coalesce(
			 ImmsUptoDateClinician.ImmsUptoDateClinician
			,'N/A'
		)

from
	Warehouse.ChildHealth.LookedAfter ImmsUptoDateClinician

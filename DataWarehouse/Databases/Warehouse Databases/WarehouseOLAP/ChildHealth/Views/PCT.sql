﻿


CREATE view [ChildHealth].[PCT] as

select distinct
	 PCTCode =
		coalesce(
			GpPCT.PCTCode
			,'#'
		)

	,PCT =
		coalesce(
			GpPCT.PCTCode + ' - ' +
			coalesce(
				 PCT.Organisation
				,'No Description'
			)
			,'N/A'
		)
from
	(
	select distinct
		GpPCT.PCTCode
	from
		Warehouse.ChildHealth.EntityGP GpPCT
		
	union all
	
	select distinct
		LookedAfter.ResidingPCTCode
	from
		Warehouse.ChildHealth.LookedAfter
		
	union all
	
	select distinct
		LookedAfter.OriginatingPCTCode
	from
		Warehouse.ChildHealth.LookedAfter
		
	union all
	
	select distinct
		Child.PCTCode
	from
		ChildHealth.Child
	where
		Child.PCTCode <> '#'
	
	) GpPCT

left join Organisation.dbo.PCT
on	PCT.OrganisationCode = GpPCT.PCTCode




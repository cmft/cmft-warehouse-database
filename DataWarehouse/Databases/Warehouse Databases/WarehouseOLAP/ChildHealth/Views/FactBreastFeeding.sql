﻿CREATE view [ChildHealth].[FactBreastFeeding] as

select
	 BreastFeeding.EntitySourceUniqueID

	,BreastFeeding.SourceUniqueID
	,BreastFeeding.StageNo

	,BreastFeedingResultCode =
		coalesce(
			 BreastFeeding.BreastFeedingResultCode
			,'#'
		)

	,StageDate =
		coalesce(
			 BreastFeeding.StageDate
			,'1 Jan 1901'
		)

	,BreastFeedingCases = 1

from
	Warehouse.ChildHealth.BreastFeeding

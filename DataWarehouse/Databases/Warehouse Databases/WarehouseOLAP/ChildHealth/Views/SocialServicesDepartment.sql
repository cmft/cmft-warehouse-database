﻿create view [ChildHealth].[SocialServicesDepartment] as

select
	 SocialServicesDepartment.SocialServicesDepartmentCode
	,SocialServicesDepartment.SocialServicesDepartment
from
	Warehouse.ChildHealth.SocialServicesDepartment

union all

select
	'#'
	,'N/A'

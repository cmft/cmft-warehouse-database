﻿create view [ChildHealth].[ReasonForDischarge] as

select
	 ReasonForDischarge.ReasonForDischargeCode
	,ReasonForDischarge.ReasonForDischarge
from
	Warehouse.ChildHealth.ReasonForDischarge

union all

select
	'#'
	,'N/A'

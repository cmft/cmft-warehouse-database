﻿create view [ChildHealth].[CauseForConcern] as

select
	 CauseForConcern.CauseForConcernCode

	,CauseForConcern =
		coalesce(
			 CauseForConcern.CauseForConcern
			,CauseForConcern.CauseForConcernCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.CauseForConcern

union all

select
	 '#'
	,'N/A'

﻿










CREATE view [ChildHealth].[ChildBase] as

select
	 Entity.SourceUniqueID
	,Entity.FullName
	,Entity.Surname
	,Entity.Forename
	,Entity.NHSNumber
	,Entity.PseudonomisedPerson
	,Birth.CongenitalMalformation
	,Birth.MotherAgeAtConception
	,Birth.MotherDateOfBirth
	,Birth.MotherNHSNumber
	,Birth.MotherName
	,Birth.OneParentFamily
	,Birth.PreviousAbortions
	,Birth.PreviousChildDeaths
	,Birth.PreviousLiveBirths
	,Birth.PreviousMiscarriages
	,Birth.PreviousStillBirths
	,Birth.BirthOrder

	,HasSiblings =
		case
		when exists
		(
		select
			1
		from
			Warehouse.ChildHealth.Relationship
		where
			Relationship.EntitySourceUniqueID = Entity.SourceUniqueID
		and	Relationship.RelationshipCode = 'S' --Sibling
		)
		then 'Y'
		else 'N'
		end

	,EntityAddress.Address1
	,EntityAddress.Address2
	,EntityAddress.City
	,EntityAddress.Street

	,Postcode =
		coalesce(
			EntityAddress.Postcode
			,'#'
		)

	,MiCareNumber = 
		(
		select
			XRefValue
		from
			Warehouse.ChildHealth.EntityXRef
		where
			EntityXRef.EntitySourceUniqueID = Entity.SourceUniqueID
		and	EntityXRef.XrefTypeCode = 'MI'
		and	not exists
			(
			select
				1
			from
				Warehouse.ChildHealth.EntityXRef Previous
			where
				Previous.EntitySourceUniqueID = Entity.SourceUniqueID
			and	Previous.XrefTypeCode = EntityXRef.XrefTypeCode
			and	Previous.SequenceNo > EntityXRef.SequenceNo
			)
		)

	,PupilNumber = 
		(
		select
			XRefValue
		from
			Warehouse.ChildHealth.EntityXRef
		where
			EntityXRef.EntitySourceUniqueID = Entity.SourceUniqueID
		and	EntityXRef.XrefTypeCode = 'PIN'
		and	not exists
			(
			select
				1
			from
				Warehouse.ChildHealth.EntityXRef Previous
			where
				Previous.EntitySourceUniqueID = Entity.SourceUniqueID
			and	Previous.XrefTypeCode = EntityXRef.XrefTypeCode
			and	Previous.SequenceNo > EntityXRef.SequenceNo
			)
		)

	,ChildHealthNumber = 
		(
		select
			XRefValue
		from
			Warehouse.ChildHealth.EntityXRef
		where
			EntityXRef.EntitySourceUniqueID = Entity.SourceUniqueID
		and	EntityXRef.XrefTypeCode = 'CH'
		and	not exists
			(
			select
				1
			from
				Warehouse.ChildHealth.EntityXRef Previous
			where
				Previous.EntitySourceUniqueID = Entity.SourceUniqueID
			and	Previous.XrefTypeCode = EntityXRef.XrefTypeCode
			and	Previous.SequenceNo > EntityXRef.SequenceNo
			)
		)

	,UniquePupilNumber = 
		(
		select
			XRefValue
		from
			Warehouse.ChildHealth.EntityXRef
		where
			EntityXRef.EntitySourceUniqueID = Entity.SourceUniqueID
		and	EntityXRef.XrefTypeCode = 'UPID'
		and	not exists
			(
			select
				1
			from
				Warehouse.ChildHealth.EntityXRef Previous
			where
				Previous.EntitySourceUniqueID = Entity.SourceUniqueID
			and	Previous.XrefTypeCode = EntityXRef.XrefTypeCode
			and	Previous.SequenceNo > EntityXRef.SequenceNo
			)
		)

	,ScheduleCode =
		coalesce(
			 ClientSummary.ScheduleCode
			,'1'
		)

	,SchoolCode =
		coalesce(
			 Birth.SchoolCode
			,'#'
		)

	,Entity.DateOfBirth

	,InitialRegistrationStatusCode =
		coalesce(
			(
			select
				ChildStatusHistory.RegistrationStatusCode
			from
				Warehouse.ChildHealth.ChildStatusHistory
			where
				ChildStatusHistory.EntitySourceUniqueID = Entity.SourceUniqueID
			and	not exists
				(
				select
					1
				from
					Warehouse.ChildHealth.ChildStatusHistory Previous
				where
					Previous.EntitySourceUniqueID = Entity.SourceUniqueID
				and	Previous.ChangedDate < ChildStatusHistory.ChangedDate
				)
			)
			,'#'
		)


	,RegistrationStatusCode =
		coalesce(
			(
			select
				ChildStatusHistory.RegistrationStatusCode
			from
				Warehouse.ChildHealth.ChildStatusHistory
			where
				ChildStatusHistory.EntitySourceUniqueID = Entity.SourceUniqueID
			and	not exists
				(
				select
					1
				from
					Warehouse.ChildHealth.ChildStatusHistory Previous
				where
					Previous.EntitySourceUniqueID = Entity.SourceUniqueID
				and	Previous.ChangedDate > ChildStatusHistory.ChangedDate
				)
			)
			,Birth.RegistrationStatusCode
			,'#'
		)

	,LiveBirth =
		CAST(
			case
			when Birth.BirthStateCode = 'L'
			then 1
			else 0
			end

			as bit
		)

	,BirthWeightBand =
		case
		when Birth.BirthWeight is null
		then 99	--'N/A'

		when Birth.BirthWeight <= 2500
		then 1	--'Low'

		else 2	--'High'
		end

	,TeenageMother =
		CAST(
			case
			when
				DATEDIFF(
					year
					,Birth.MotherDateOfBirth
					,Entity.DateOfBirth
				) < 18
			then 1
			else 0
			end

			as bit
		)

	,FirstTimeMother =
		CAST(
			case
			when Birth.PreviousLiveBirths is null
			then 1

			when Birth.PreviousLiveBirths = 0
			then 1

			else 0
			end

			as bit
		)

	,HealthVisitorCode =
		coalesce(
			 HealthVisitor.StaffCode
			,-1
		)

	,BirthPlaceCode =
		coalesce(
			Birth.BirthPlaceCode
			,'#'
		)

	,MotherHEPBStatusCode =
		coalesce(
			Birth.MotherHEPBStatusCode
			,'#'
		)

	,BirthStateCode =
		coalesce(
			Birth.BirthStateCode
			,'#'
		)

	,AgeCode =
		case
		when datediff(day, Entity.DateOfBirth, cast(getdate() as date)) is null then 'Age Unknown'
		when datediff(day, Entity.DateOfBirth, cast(getdate() as date)) <= 0 then '00 Days'
		when datediff(day, Entity.DateOfBirth, cast(getdate() as date)) = 1 then '01 Day'
		when datediff(day, Entity.DateOfBirth, cast(getdate() as date)) > 1 and
		datediff(day, Entity.DateOfBirth, cast(getdate() as date)) <= 28 then
			right('0' + Convert(Varchar,datediff(day, Entity.DateOfBirth, cast(getdate() as date))), 2) + ' Days'
		
		when datediff(day, Entity.DateOfBirth, cast(getdate() as date)) > 28 and
		datediff(month, Entity.DateOfBirth, cast(getdate() as date)) -
		case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'

		when datediff(month, Entity.DateOfBirth, cast(getdate() as date)) -
		case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end = 1
			then '01 Month'

		--when datediff(month, Entity.DateOfBirth, cast(getdate() as date)) -
		--case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end > 1 and
		-- datediff(month, Entity.DateOfBirth, cast(getdate() as date)) -
		--case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end between 48 and 53
		--then
		--'041Years'

		--when datediff(month, Entity.DateOfBirth, cast(getdate() as date)) -
		--case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end > 1 and
		-- datediff(month, Entity.DateOfBirth, cast(getdate() as date)) -
		--case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end between 54 and 59
		--then
		--'049Years'

		when datediff(month, Entity.DateOfBirth, cast(getdate() as date)) -
		case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end > 1 and
		 datediff(month, Entity.DateOfBirth, cast(getdate() as date)) -
		case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end <= 59
		then
		right('0' + Convert(varchar,datediff(month, Entity.DateOfBirth,cast(getdate() as date)) -
		case  when datepart(day, Entity.DateOfBirth) > datepart(day, cast(getdate() as date)) then 1 else 0 end), 2) + ' Months'

		when datediff(yy, Entity.DateOfBirth, cast(getdate() as date)) - 
		(
		case 
		when	(datepart(m, Entity.DateOfBirth) > datepart(m, cast(getdate() as date))) 
		or
			(
				datepart(m, Entity.DateOfBirth) = datepart(m, cast(getdate() as date)) 
			And	datepart(d, Entity.DateOfBirth) > datepart(d, cast(getdate() as date))
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, Entity.DateOfBirth, cast(getdate() as date)) - 
		(
		case 
		when	(datepart(m, Entity.DateOfBirth) > datepart(m, cast(getdate() as date))) 
		or
			(
				datepart(m, Entity.DateOfBirth) = datepart(m, cast(getdate() as date)) 
			And	datepart(d, Entity.DateOfBirth) > datepart(d, cast(getdate() as date))
			) then 1 else 0 end
		)), 2) + ' Years'

		end

	,EthnicOriginCode =
		coalesce(
			Entity.EthnicOriginCode
			,'#'
		)

	,SexCode =
		coalesce(
			Entity.SexCode
			,'#'
		)

	,GPCode =
		coalesce(
			 Entity.GPCode
			,-1
		)

	,PracticeCode =
		coalesce(
			 EntityGp.PracticeCode
			,'#'
		)

	,PCTCode =
		coalesce(
			 EntityGp.PCTCode
			,'#'
		)

	,LACNurseCode =
		coalesce(
			 LACNurse.StaffCode
			,-1
		)

	,SchoolNurseCode =
		coalesce(
			 SchoolNurse.StaffCode
			,-1
		)

	,NamedHealthVisitorCode =
		coalesce(
			 NamedHealthVisitor.StaffCode
			,-1
		)

	,SchoolClerkCode =
		coalesce(
			 SchoolClerk.StaffCode
			,-1
		)

	,LinkedSchoolNurseCode =
		coalesce(
			 LinkedSchoolNurse.StaffCode
			,-1
		)

	,NeonatalHearingTestOutcomeCode =
		coalesce(
			 NeonatalInitialExam.OutcomeCode
			,'#'
		)

	,NeonatalHearingTestDate =
		coalesce(
			 NeonatalInitialExam.ExamDate
			,'1 Jan 1900'
		)

--death details
	,DateOfDeath =
		coalesce(
			 Death.DateOfDeath
			,'1 Jan 1900'
		)

	,DeathPlaceCode =
		coalesce(
			 Death.DeathPlaceCode
			,'#'
		)

	,CauseOfDeathCode =
		coalesce(
			 Death.CauseOfDeathCode1
			,'#'
		)

	,DeathNotifiedByCode =
		coalesce(
			 Death.DeathNotifiedByCode
			,'#'
		)
	,DeathNotifiedDate =
		coalesce(
			 Death.DeathNotifiedDate
			,'1 Jan 1900'
		)

	,Death.CauseOfDeath

from
	Warehouse.ChildHealth.Entity

inner join Warehouse.ChildHealth.Birth
on	Birth.EntitySourceUniqueID = Entity.SourceUniqueID

left join Warehouse.ChildHealth.Death
on	Death.EntitySourceUniqueID = Entity.SourceUniqueID

left join Warehouse.ChildHealth.EntityAddress
on	EntityAddress.EntitySourceUniqueID = Entity.SourceUniqueID
and	EntityAddress.AddressEndDate is null
and	EntityAddress.AddressTypeCode = 'HOME'
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.EntityAddress Previous
	where
		Previous.EntitySourceUniqueID = Entity.SourceUniqueID
	and	Previous.AddressEndDate is null
	and	Previous.AddressTypeCode = 'HOME'
	and	Previous.AddressSeqno > EntityAddress.AddressSeqno
	)

left join Warehouse.ChildHealth.ClientSummary
on	ClientSummary.EntitySourceUniqueID = Entity.SourceUniqueID

left join Warehouse.ChildHealth.EntityGp
on	EntityGp.EntitySourceUniqueID = Entity.SourceUniqueID
and	EntityGp.GpCode = Entity.GPCode

left join Warehouse.ChildHealth.ChildHealthEncounter
on	ChildHealthEncounter.EntitySourceUniqueID = Entity.SourceUniqueID
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.ChildHealthEncounter Previous
	where
		Previous.EntitySourceUniqueID = Entity.SourceUniqueID
	and	(
			Previous.StartDate > ChildHealthEncounter.StartDate
		or	(
				Previous.StartDate = ChildHealthEncounter.StartDate
			and	Previous.SourceUniqueID > ChildHealthEncounter.SourceUniqueID
			)
		)
	)


left join Warehouse.ChildHealth.Staff LACNurse
on	LACNurse.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
and	LACNurse.EndDate is null
and	LACNurse.StaffTypeCode = 'LN'	--Looked After Nurse
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.Staff Previous
	where
		Previous.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
	and	Previous.EndDate is null
	and	Previous.StaffTypeCode = LACNurse.StaffTypeCode
	and	Previous.SequenceNo > LACNurse.SequenceNo
	)

left join Warehouse.ChildHealth.Staff SchoolNurse
on	SchoolNurse.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
and	SchoolNurse.EndDate is null
and	SchoolNurse.StaffTypeCode = 'SN'	--School Nurse
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.Staff Previous
	where
		Previous.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
	and	Previous.EndDate is null
	and	Previous.StaffTypeCode = SchoolNurse.StaffTypeCode
	and	Previous.SequenceNo > SchoolNurse.SequenceNo
	)

left join Warehouse.ChildHealth.Staff NamedHealthVisitor
on	NamedHealthVisitor.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
and	NamedHealthVisitor.EndDate is null
and	NamedHealthVisitor.StaffTypeCode = 'NHV'	--Named Health Visitor
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.Staff Previous
	where
		Previous.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
	and	Previous.EndDate is null
	and	Previous.StaffTypeCode = NamedHealthVisitor.StaffTypeCode
	and	Previous.SequenceNo > NamedHealthVisitor.SequenceNo
	)

left join Warehouse.ChildHealth.Staff HealthVisitor
on	HealthVisitor.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
and	HealthVisitor.EndDate is null
and	HealthVisitor.StaffTypeCode = 'HV'	--Health Visitor
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.Staff Previous
	where
		Previous.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
	and	Previous.EndDate is null
	and	Previous.StaffTypeCode = HealthVisitor.StaffTypeCode
	and	Previous.SequenceNo > HealthVisitor.SequenceNo
	)

left join Warehouse.ChildHealth.Staff SchoolClerk
on	SchoolClerk.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
and	SchoolClerk.EndDate is null
and	SchoolClerk.StaffTypeCode = 'SC'	--School Health Clerk
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.Staff Previous
	where
		Previous.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
	and	Previous.EndDate is null
	and	Previous.StaffTypeCode = SchoolClerk.StaffTypeCode
	and	Previous.SequenceNo > SchoolClerk.SequenceNo
	)

left join Warehouse.ChildHealth.Staff LinkedSchoolNurse
on	LinkedSchoolNurse.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
and	LinkedSchoolNurse.EndDate is null
and	LinkedSchoolNurse.StaffTypeCode = 'LSN'	--Linked School Nurse
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.Staff Previous
	where
		Previous.ChildHealthEncounterSourceUniqueID = ChildHealthEncounter.SourceUniqueID
	and	Previous.EndDate is null
	and	Previous.StaffTypeCode = LinkedSchoolNurse.StaffTypeCode
	and	Previous.SequenceNo > LinkedSchoolNurse.SequenceNo
	)

left join Warehouse.ChildHealth.NeonatalInitialExam
on	NeonatalInitialExam.EntitySourceUniqueID = Entity.SourceUniqueID
and	not exists
	(
	select
		1
	from
		Warehouse.ChildHealth.NeonatalInitialExam Previous
	where
		Previous.EntitySourceUniqueID = Entity.SourceUniqueID
	and	(
			Previous.ExamDate > NeonatalInitialExam.ExamDate
		or	(
				Previous.ExamDate = NeonatalInitialExam.ExamDate
			and	Previous.SequenceNo > NeonatalInitialExam.SequenceNo
			)
		)
	)

where
	Entity.TypeCode = 'P' --Person? Patient?













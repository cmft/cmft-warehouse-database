﻿CREATE view [ChildHealth].[Practice] as

select
	 Practice.PracticeCode
	,Practice =
		Practice.PracticeCode + ' - ' +
		coalesce(
			 Practice.Practice
			,'No Description'
		)
from
	Warehouse.ChildHealth.Practice

union all

select
	'#'
	,'N/A'

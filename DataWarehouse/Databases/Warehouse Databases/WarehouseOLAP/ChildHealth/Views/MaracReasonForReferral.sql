﻿create view [ChildHealth].[MaracReasonForReferral] as

select
	 MaracReasonForReferral.MaracReasonForReferralCode

	,MaracReasonForReferral =
		coalesce(
			 MaracReasonForReferral.MaracReasonForReferral
			,MaracReasonForReferral.MaracReasonForReferralCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.MaracReasonForReferral

union all

select
	 '#'
	,'N/A'

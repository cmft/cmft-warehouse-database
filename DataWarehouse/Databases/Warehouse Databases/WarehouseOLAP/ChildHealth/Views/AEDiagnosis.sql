﻿create view [ChildHealth].[AEDiagnosis] as

select
	 AEDiagnosis.AEDiagnosisCode
	,AEDiagnosis.AEDiagnosis
from
	Warehouse.ChildHealth.AEDiagnosis

union all

select
	'#'
	,'N/A'

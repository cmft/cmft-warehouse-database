﻿create view [ChildHealth].[PlannedSession] as

select
	 PlannedSession.PlannedSessionCode

	,PlannedSession =
		coalesce(
			 PlannedSession.PlannedSession
			,PlannedSession.PlannedSessionCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.PlannedSession

union all

select
	 '#'
	,'N/A'

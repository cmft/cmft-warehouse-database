﻿CREATE view [ChildHealth].[RegisterStaff] as

select
	 RegisterStaffCode
	,RegisterStaff
from
	Warehouse.ChildHealth.RegisterStaff

union all

select
	 '#'
	,'N/A'

﻿create view [ChildHealth].[DeathNotifiedBy] as

select
	 DeathNotifiedByCode

	,DeathNotifiedBy

from
	Warehouse.ChildHealth.DeathNotifiedBy

union all

select
	'#'
	,'N/A'

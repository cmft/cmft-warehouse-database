﻿CREATE view [ChildHealth].[FactChildFlag] as

select
	 ChildFlag.EntitySourceUniqueID
	,ChildFlag.FlagCode

	,FlagAddedDate =
		ChildFlag.AddedDate

	,FlagRemovedDate =
		coalesce(
			 ChildFlag.RemovedDate
			,'1 Jan 1900'
		)

	,FlagReasonForRemovalCode =
		coalesce(
			 ChildFlag.FlagReasonForRemovalCode
			,'#'
		)

	,FlagCases = 1
from
	Warehouse.ChildHealth.ChildFlag

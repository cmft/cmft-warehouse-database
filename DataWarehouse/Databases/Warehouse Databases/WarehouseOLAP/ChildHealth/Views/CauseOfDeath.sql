﻿
CREATE view [ChildHealth].[CauseOfDeath] as

select
	 CauseOfDeathCode

	,CauseOfDeath = coalesce(CauseOfDeath, CauseOfDeathCode + ' - No Description')

from
	Warehouse.ChildHealth.CauseOfDeath

union all

select
	'#'
	,'N/A'


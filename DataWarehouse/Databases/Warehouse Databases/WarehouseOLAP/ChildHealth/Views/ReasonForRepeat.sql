﻿create view [ChildHealth].[ReasonForRepeat] as

select
	 ReasonForRepeat.ReasonForRepeatCode

	,ReasonForRepeat =
		coalesce(
			 ReasonForRepeat.ReasonForRepeat
			,ReasonForRepeat.ReasonForRepeatCode + ' - No Description'
		)
from
	Warehouse.ChildHealth.ReasonForRepeat

union all

select
	 '#'
	,'N/A'

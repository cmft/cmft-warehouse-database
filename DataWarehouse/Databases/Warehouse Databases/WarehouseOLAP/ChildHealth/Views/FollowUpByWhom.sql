﻿create view [ChildHealth].[FollowUpByWhom] as

select
	 FollowUpByWhom.FollowUpByWhomCode
	,FollowUpByWhom.FollowUpByWhom
from
	Warehouse.ChildHealth.FollowUpByWhom

union all

select
	'#'
	,'N/A'

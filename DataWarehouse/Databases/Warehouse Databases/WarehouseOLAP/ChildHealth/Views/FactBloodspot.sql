﻿CREATE view [ChildHealth].[FactBloodspot] as

select
	 Bloodspot.EntitySourceUniqueID

	,Bloodspot.SpecimenDate

	,Bloodspot.TestCode

	,Bloodspot.SequenceNo

	,TestResultCode =
		coalesce(
			case
			when Bloodspot.TestResultCode = ''
			then null
			else Bloodspot.TestResultCode
			end
			,'#'
		)

	,TestGivenCode =
		coalesce(
			 Bloodspot.TestGivenCode
			,'#'
		)

	,ReasonForRepeatCode =
		coalesce(
			 Bloodspot.ReasonForRepeatCode
			,'#'
		)

	,SpecimenResultDate =
		coalesce(
			 Bloodspot.SpecimenResultDate
			,'1 Jan 1900'
		)

	,SentDate =
		coalesce(
			 Bloodspot.SentDate
			,'1 Jan 1900'
		)

	,AcknowledgedDate =
		coalesce(
			 Bloodspot.AcknowledgedDate
			,'1 Jan 1900'
		)

	,BloodspotCases = 1

from
	Warehouse.ChildHealth.Bloodspot

﻿create view [ChildHealth].[FactSafeguardingCauseForConcern] as

select
	 FactSafeguarding.EntitySourceUniqueID
	,FactSafeguarding.PlacedOnPlanDate
	,FactSafeguarding.RemovedFromPlanDate
	,FactSafeguarding.ReasonRemovedFromPlanCode
	,FactSafeguarding.LeadClinicianTypeCode
	,FactSafeguarding.LeadClinicianCode
	,FactSafeguarding.SocialServicesDepartmentCode
	,FactSafeguarding.VulnerabilityFactorCode
	,FactSafeguarding.Supervision
	,FactSafeguarding.SupervisionStartDate
	,FactSafeguarding.SupervisionEndDate
	,FactSafeguarding.MappaCategoryCode
	,FactSafeguarding.MaracReasonForReferralCode

	,ChildProtectionCauseForConcern.IdentifiedDate

	,CauseForConcernCode =
		coalesce(
			 ChildProtectionCauseForConcern.CauseForConcernCode
			,'#'
		)

	,EndDate =
		coalesce(
			 ChildProtectionCauseForConcern.EndDate
			,'1 Jan 1900'
		)

	,SafeguardingCauseForConcernCases = 1

from
	ChildHealth.FactSafeguarding

inner join Warehouse.ChildHealth.ChildProtectionCauseForConcern
on	ChildProtectionCauseForConcern.EntitySourceUniqueID = FactSafeguarding.EntitySourceUniqueID

﻿create view [ChildHealth].[DeathPlace] as

select
	 DeathPlaceCode

	,DeathPlace =
		case
		when DeathPlace.DeathPlace = 'DELETED'
		then DeathPlace.DeathPlaceCode + ' - Deleted'
		else DeathPlace.DeathPlace
		end

from
	Warehouse.ChildHealth.DeathPlace

union all

select
	'#'
	,'N/A'

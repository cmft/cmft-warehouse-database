﻿CREATE TABLE [ChildHealth].[AgeBase] (
    [AgeCode]       VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Age]           VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AgeParentCode] VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AgeInMonths]   INT          NULL,
    CONSTRAINT [PK_Age] PRIMARY KEY CLUSTERED ([AgeCode] ASC),
    CONSTRAINT [FK_Age_Age] FOREIGN KEY ([AgeParentCode]) REFERENCES [ChildHealth].[AgeBase] ([AgeCode])
);


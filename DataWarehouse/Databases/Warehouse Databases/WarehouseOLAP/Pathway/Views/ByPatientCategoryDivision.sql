﻿



CREATE view [Pathway].[ByPatientCategoryDivision] as

select
	 WrkEncounterProcedure.ProviderSpellNo
	,WrkEncounterProcedure.AdmissionTime
	,WrkEncounterProcedure.DischargeTime
	,WrkEncounterProcedure.OperationStartTime
	,WrkEncounterProcedure.OperationEndTime
	,WrkEncounterProcedure.AnaestheticStartTime
	,WrkEncounterProcedure.AnaestheticEndTime
	,WrkEncounterProcedure.PathwayRecno
from
	Pathway.WrkEncounterProcedure WrkEncounterProcedure
where
	WrkEncounterProcedure.TemplateCode = 'PATCATDIVISION'
and	WrkEncounterProcedure.PatientCategoryCode = WrkEncounterProcedure.RuleBasePatientCategoryCode
and	WrkEncounterProcedure.AdmissionDivisionCode = WrkEncounterProcedure.RuleBaseDivisionCode

--get distinct spell pathways
and	not exists
	(
	select
		1
	from
		Pathway.WrkEncounterProcedure Previous
	where
		Previous.TemplateCode = WrkEncounterProcedure.TemplateCode
	and	Previous.PatientCategoryCode = Previous.RuleBasePatientCategoryCode
	and	Previous.AdmissionDivisionCode = Previous.RuleBaseDivisionCode

	and	Previous.ProviderSpellNo = WrkEncounterProcedure.ProviderSpellNo
	and	Previous.PathwayRecno = WrkEncounterProcedure.PathwayRecno
	and	Previous.RuleBaseRecno > WrkEncounterProcedure.RuleBaseRecno
	)



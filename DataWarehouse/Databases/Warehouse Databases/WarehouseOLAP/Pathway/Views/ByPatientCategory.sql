﻿

CREATE view [Pathway].[ByPatientCategory] as

select
	 WrkEncounterProcedure.ProviderSpellNo
	,WrkEncounterProcedure.AdmissionTime
	,WrkEncounterProcedure.DischargeTime
	,WrkEncounterProcedure.OperationStartTime
	,WrkEncounterProcedure.OperationEndTime
	,WrkEncounterProcedure.AnaestheticStartTime
	,WrkEncounterProcedure.AnaestheticEndTime
	,WrkEncounterProcedure.PathwayRecno
from
	Pathway.WrkEncounterProcedure WrkEncounterProcedure
where
	WrkEncounterProcedure.TemplateCode = 'PATCAT'
and	WrkEncounterProcedure.PatientCategoryCode = WrkEncounterProcedure.RuleBasePatientCategoryCode

--get distinct spell pathways
and	not exists
	(
	select
		1
	from
		Pathway.WrkEncounterProcedure Previous
	where
		Previous.TemplateCode = WrkEncounterProcedure.TemplateCode
	and	Previous.PatientCategoryCode = Previous.RuleBasePatientCategoryCode

	and	Previous.ProviderSpellNo = WrkEncounterProcedure.ProviderSpellNo
	and	Previous.PathwayRecno = WrkEncounterProcedure.PathwayRecno
	and	Previous.RuleBaseRecno > WrkEncounterProcedure.RuleBaseRecno
	)



﻿


CREATE view [Pathway].[ByMethodOfDelivery] as

select
	 WrkEncounterNonProcedure.ProviderSpellNo
	,WrkEncounterNonProcedure.AdmissionTime
	,WrkEncounterNonProcedure.DischargeTime
	,WrkEncounterNonProcedure.OperationStartTime
	,WrkEncounterNonProcedure.OperationEndTime
	,WrkEncounterNonProcedure.AnaestheticStartTime
	,WrkEncounterNonProcedure.AnaestheticEndTime
	,WrkEncounterNonProcedure.PathwayRecno
from
	Pathway.WrkEncounterNonProcedure WrkEncounterNonProcedure
where
	WrkEncounterNonProcedure.TemplateCode = 'METHODOFDELIVERY'
and	WrkEncounterNonProcedure.MethodOfDeliveryCode = WrkEncounterNonProcedure.RuleBaseMethodOfDeliveryCode

--get distinct spell pathways
and	not exists
	(
	select
		1
	from
		Pathway.WrkEncounterNonProcedure Previous
	where
		Previous.TemplateCode = WrkEncounterNonProcedure.TemplateCode
	and	Previous.MethodOfDeliveryCode = Previous.RuleBaseMethodOfDeliveryCode

	and	Previous.ProviderSpellNo = WrkEncounterNonProcedure.ProviderSpellNo
	and	Previous.PathwayRecno = WrkEncounterNonProcedure.PathwayRecno
	and	Previous.RuleBaseRecno > WrkEncounterNonProcedure.RuleBaseRecno
	)




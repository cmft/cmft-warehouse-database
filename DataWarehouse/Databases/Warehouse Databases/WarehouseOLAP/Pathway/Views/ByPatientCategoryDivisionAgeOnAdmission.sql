﻿create view [Pathway].[ByPatientCategoryDivisionAgeOnAdmission] as

select
	 WrkEncounterProcedure.ProviderSpellNo
	,WrkEncounterProcedure.AdmissionTime
	,WrkEncounterProcedure.DischargeTime
	,WrkEncounterProcedure.OperationStartTime
	,WrkEncounterProcedure.OperationEndTime
	,WrkEncounterProcedure.AnaestheticStartTime
	,WrkEncounterProcedure.AnaestheticEndTime
	,WrkEncounterProcedure.PathwayRecno
from
	Pathway.WrkEncounterProcedure WrkEncounterProcedure
where
	WrkEncounterProcedure.TemplateCode = 'PATCATDIVISIONAGE'
and	WrkEncounterProcedure.PatientCategoryCode = WrkEncounterProcedure.RuleBasePatientCategoryCode
and	WrkEncounterProcedure.AdmissionDivisionCode = WrkEncounterProcedure.RuleBaseDivisionCode
and	WrkEncounterProcedure.AgeOnAdmission between coalesce(WrkEncounterProcedure.LowerLimit, WrkEncounterProcedure.AgeOnAdmission) and coalesce(WrkEncounterProcedure.UpperLimit, WrkEncounterProcedure.AgeOnAdmission)

--get distinct spell pathways
and	not exists
	(
	select
		1
	from
		Pathway.WrkEncounterProcedure Previous
	where
		Previous.TemplateCode = WrkEncounterProcedure.TemplateCode
	and	Previous.PatientCategoryCode = Previous.RuleBasePatientCategoryCode
	and	Previous.AdmissionDivisionCode = Previous.RuleBaseDivisionCode
	and	Previous.AgeOnAdmission between coalesce(Previous.LowerLimit, Previous.AgeOnAdmission) and coalesce(Previous.UpperLimit, Previous.AgeOnAdmission)

	and	Previous.ProviderSpellNo = WrkEncounterProcedure.ProviderSpellNo
	and	Previous.PathwayRecno = WrkEncounterProcedure.PathwayRecno
	and	Previous.RuleBaseRecno > WrkEncounterProcedure.RuleBaseRecno
	)



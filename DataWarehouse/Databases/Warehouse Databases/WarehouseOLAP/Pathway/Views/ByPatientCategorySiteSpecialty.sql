﻿



CREATE view [Pathway].[ByPatientCategorySiteSpecialty] as

select
	 WrkEncounterProcedure.ProviderSpellNo
	,WrkEncounterProcedure.AdmissionTime
	,WrkEncounterProcedure.DischargeTime
	,WrkEncounterProcedure.OperationStartTime
	,WrkEncounterProcedure.OperationEndTime
	,WrkEncounterProcedure.AnaestheticStartTime
	,WrkEncounterProcedure.AnaestheticEndTime
	,WrkEncounterProcedure.PathwayRecno
from
	Pathway.WrkEncounterProcedure WrkEncounterProcedure
where
	WrkEncounterProcedure.TemplateCode = 'PATCATSITESPECIALTY'
and	WrkEncounterProcedure.PatientCategoryCode = WrkEncounterProcedure.RuleBasePatientCategoryCode
and	WrkEncounterProcedure.SiteCode = WrkEncounterProcedure.RuleBaseSiteCode
and	WrkEncounterProcedure.SpecialtyCode = WrkEncounterProcedure.RuleBaseSpecialtyCode

--get distinct spell pathways
and	not exists
	(
	select
		1
	from
		Pathway.WrkEncounterProcedure Previous
	where
		Previous.TemplateCode = WrkEncounterProcedure.TemplateCode
	and	Previous.PatientCategoryCode = Previous.RuleBasePatientCategoryCode
	and	Previous.SiteCode = Previous.RuleBaseSiteCode
	and	Previous.SpecialtyCode = Previous.RuleBaseSpecialtyCode

	and	Previous.ProviderSpellNo = WrkEncounterProcedure.ProviderSpellNo
	and	Previous.PathwayRecno = WrkEncounterProcedure.PathwayRecno
	and	Previous.RuleBaseRecno > WrkEncounterProcedure.RuleBaseRecno
	)





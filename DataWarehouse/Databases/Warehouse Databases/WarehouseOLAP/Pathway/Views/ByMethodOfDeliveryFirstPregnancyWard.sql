﻿




CREATE view [Pathway].[ByMethodOfDeliveryFirstPregnancyWard] as

select
	 WrkEncounterNonProcedure.ProviderSpellNo
	,WrkEncounterNonProcedure.AdmissionTime
	,WrkEncounterNonProcedure.DischargeTime
	,WrkEncounterNonProcedure.OperationStartTime
	,WrkEncounterNonProcedure.OperationEndTime
	,WrkEncounterNonProcedure.AnaestheticStartTime
	,WrkEncounterNonProcedure.AnaestheticEndTime
	,WrkEncounterNonProcedure.PathwayRecno
from
	Pathway.WrkEncounterNonProcedure WrkEncounterNonProcedure
where
	WrkEncounterNonProcedure.TemplateCode = 'METHODOFDELIVERYFIRSTPREGNANCYWARD'
and	WrkEncounterNonProcedure.MethodOfDeliveryCode = WrkEncounterNonProcedure.RuleBaseMethodOfDeliveryCode
and	WrkEncounterNonProcedure.FirstPregnancy = WrkEncounterNonProcedure.RuleBaseFirstPregnancy
--and	WrkEncounterNonProcedure.WardCode = WrkEncounterNonProcedure.RuleBaseWardCode
and	exists
	(
	select
		1
	from
		APC.WardStay
	where
		WardStay.ProviderSpellNo = WrkEncounterNonProcedure.ProviderSpellNo
	and	WardStay.WardCode = WrkEncounterNonProcedure.RuleBaseWardCode
	)

--get distinct spell pathways
and	not exists
	(
	select
		1
	from
		Pathway.WrkEncounterNonProcedure Previous
	where
		Previous.TemplateCode = WrkEncounterNonProcedure.TemplateCode
	and	Previous.MethodOfDeliveryCode = Previous.RuleBaseMethodOfDeliveryCode
	and	Previous.FirstPregnancy = Previous.RuleBaseFirstPregnancy
--	and	Previous.WardCode = Previous.RuleBaseWardCode
	and	exists
		(
		select
			1
		from
			APC.WardStay
		where
			WardStay.ProviderSpellNo = Previous.ProviderSpellNo
		and	WardStay.WardCode = Previous.RuleBaseWardCode
		)

	and	Previous.ProviderSpellNo = WrkEncounterNonProcedure.ProviderSpellNo
	and	Previous.PathwayRecno = WrkEncounterNonProcedure.PathwayRecno
	and	Previous.RuleBaseRecno > WrkEncounterNonProcedure.RuleBaseRecno
	)




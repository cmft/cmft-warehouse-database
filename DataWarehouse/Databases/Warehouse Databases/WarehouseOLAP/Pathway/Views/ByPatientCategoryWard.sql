﻿


CREATE view [Pathway].[ByPatientCategoryWard] as

select
	 WrkEncounterProcedure.ProviderSpellNo
	,WrkEncounterProcedure.AdmissionTime
	,WrkEncounterProcedure.DischargeTime
	,WrkEncounterProcedure.OperationStartTime
	,WrkEncounterProcedure.OperationEndTime
	,WrkEncounterProcedure.AnaestheticStartTime
	,WrkEncounterProcedure.AnaestheticEndTime
	,WrkEncounterProcedure.PathwayRecno
from
	Pathway.WrkEncounterProcedure WrkEncounterProcedure
where
	WrkEncounterProcedure.TemplateCode = 'PATCATWARD'
and	WrkEncounterProcedure.PatientCategoryCode = WrkEncounterProcedure.RuleBasePatientCategoryCode
--and	WrkEncounterProcedure.WardCode = WrkEncounterProcedure.RuleBaseWardCode
and	exists
	(
	select
		1
	from
		APC.WardStay
	where
		WardStay.ProviderSpellNo = WrkEncounterProcedure.ProviderSpellNo
	and	WardStay.WardCode = WrkEncounterProcedure.RuleBaseWardCode
	)

--get distinct spell pathways
and	not exists
	(
	select
		1
	from
		Pathway.WrkEncounterProcedure Previous
	where
		Previous.TemplateCode = WrkEncounterProcedure.TemplateCode
	and	Previous.PatientCategoryCode = Previous.RuleBasePatientCategoryCode
--	and	Previous.WardCode = Previous.RuleBaseWardCode
	and	exists
		(
		select
			1
		from
			APC.WardStay
		where
			WardStay.ProviderSpellNo = Previous.ProviderSpellNo
		and	WardStay.WardCode = Previous.RuleBaseWardCode
		)

	and	Previous.ProviderSpellNo = WrkEncounterProcedure.ProviderSpellNo
	and	Previous.PathwayRecno = WrkEncounterProcedure.PathwayRecno
	and	Previous.RuleBaseRecno > WrkEncounterProcedure.RuleBaseRecno
	)




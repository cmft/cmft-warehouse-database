﻿





CREATE view [Pathway].[WrkEncounterNonProcedure] as

select
	 WrkEncounter.ProviderSpellNo
	,WrkEncounter.AdmissionTime
	,WrkEncounter.DischargeTime

	,OperationStartTime =
		coalesce(
			WrkProcedure.OperationStartTime
			,WrkEncounter.DateOfBirth
		)

	,OperationEndTime =
		coalesce(
			WrkProcedure.OperationEndTime
			,WrkEncounter.DateOfBirth
		)

	,WrkProcedure.AnaestheticStartTime
	,WrkProcedure.AnaestheticEndTime
	,Template.TemplateCode
	,WrkEncounter.PatientCategoryCode
	,WrkEncounter.SiteCode
	,WrkEncounter.WardCode
	,WrkEncounter.AgeOnAdmission
	,WrkEncounter.SpecialtyCode
	,WrkEncounter.MethodOfDeliveryCode
	,WrkEncounter.FirstPregnancy
	,WrkEncounter.AdmissionDivisionCode

	,RuleBase.RuleBaseRecno
	,RuleBase.PathwayRecno
	,RuleBase.ProcedureCode
	,RuleBasePatientCategoryCode = RuleBase.PatientCategoryCode
	,RuleBaseSiteCode = RuleBase.SiteCode
	,RuleBaseWardCode = RuleBase.WardCode
	,RuleBase.LowerLimit
	,RuleBase.UpperLimit
	,RuleBaseSpecialtyCode = RuleBase.SpecialtyCode
	,RuleBaseMethodOfDeliveryCode = RuleBase.MethodOfDeliveryCode
	,RuleBaseFirstPregnancy = RuleBase.FirstPregnancy
	,RuleBaseDivisionCode = RuleBase.DivisionCode
from
	Pathway.WrkEncounter WrkEncounter

cross join Pathway.Template

inner join Pathway.Pathway
on	Pathway.TemplateRecno = Template.TemplateRecno

inner join Pathway.RuleBase RuleBase
on	RuleBase.PathwayRecno = Pathway.PathwayRecno

left join Pathway.WrkProcedure WrkProcedure
on	WrkProcedure.ProviderSpellNo = WrkEncounter.ProviderSpellNo
and	WrkProcedure.ProcedureCode = RuleBase.ProcedureCode





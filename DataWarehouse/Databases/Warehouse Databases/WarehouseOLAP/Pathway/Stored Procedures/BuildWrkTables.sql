﻿CREATE procedure [Pathway].[BuildWrkTables] as


truncate table Pathway.WrkEncounter

insert
into
	Pathway.WrkEncounter
	(
	 ProviderSpellNo
	,PatientCategoryCode
	,SiteCode
	,WardCode
	,AdmissionTime
	,DischargeTime
	,AgeOnAdmission
	,MethodOfDeliveryCode
	,FirstPregnancy
	,SpecialtyCode
	,AdmissionDivisionCode
	,DateOfBirth
	)
select
	 Encounter.ProviderSpellNo

	,Encounter.PatientCategoryCode
	,Encounter.SiteCode
	,Encounter.StartWardTypeCode
	,Encounter.AdmissionTime
	,Encounter.DischargeTime

	,AgeOnAdmission =
		case
		when Encounter.DateOfBirth >= Encounter.AdmissionDate
		then 0
		else
			datediff(year, Encounter.DateOfBirth, Encounter.AdmissionDate) -
			case
			when Encounter.DateOfBirth >= Encounter.AdmissionDate
			then 0
			when datepart(day, Encounter.DateOfBirth) = datepart(day, Encounter.AdmissionDate)
			and	datepart(month, Encounter.DateOfBirth) = datepart(month,Encounter.AdmissionDate)
			and datepart(year, Encounter.DateOfBirth) <> datepart(year,Encounter.AdmissionDate)
			then 0 
			else 
				case
				when Encounter.DateOfBirth > Encounter.AdmissionDate then 0
				when datepart(dy, Encounter.DateOfBirth) > datepart(dy, Encounter.AdmissionDate) 
				then 1
				else 0 
				end 
			end
		end

	,Birth.MethodOfDeliveryCode
	,Birth.FirstPregnancy

	,Encounter.SpecialtyCode
	,Encounter.AdmissionDivisionCode
	,Birth.DateOfBirth

from
	APC.Encounter Encounter

inner join PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

inner join PAS.ManagementIntention ManagementIntention
on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode

left join
	(
	select
		 CasenoteNumber = Episode.HOSP_NUMBER

		,DateOfBirth =
			dateadd(
				minute
				,convert(int, left(BirthRegistration.TIME_OF_BIRTH, 2)) * 60 +
				 convert(int, right(BirthRegistration.TIME_OF_BIRTH, 2))
				,InfantRegistration.DATE_OF_BIRTH
			)

		,MethodOfDeliveryCode = BirthRegistration.METHOD_DELIVERY

		,FirstPregnancy =
			convert(
				bit
				,case
				when max(Visit.FIRST_PREG) is null
				then 1
				when max(Visit.FIRST_PREG) = 'Y'
				then 1
				else 0
				end
			)

	from
		[$(CMISStaging)].SMMIS.CON_EPISODE Episode

	inner join [$(CMISStaging)].SMMIS.BIRTH_REG BirthRegistration
	on	BirthRegistration.M_NUMBER = Episode.HOSP_NUMBER
	and	BirthRegistration.OCCURRENCE = Episode.OCCURRENCE

	inner join [$(CMISStaging)].SMMIS.INFANT_REG InfantRegistration
	on	InfantRegistration.I_NUMBER = BirthRegistration.I_NUMBER

	inner join [$(CMISStaging)].SMMIS.BK_VISIT_DATA Visit
	on	Visit.M_NUMBER = Episode.HOSP_NUMBER collate latin1_general_bin
	and	Visit.OCCURRENCE = Episode.OCCURRENCE

	and	not exists
		(
		select
			1
		from
			[$(CMISStaging)].SMMIS.CON_EPISODE PreviousEpisode

		inner join [$(CMISStaging)].SMMIS.BIRTH_REG PreviousBirthRegistration
		on	PreviousBirthRegistration.M_NUMBER = PreviousEpisode.HOSP_NUMBER
		and	PreviousBirthRegistration.OCCURRENCE = PreviousEpisode.OCCURRENCE

		inner join [$(CMISStaging)].SMMIS.INFANT_REG PreviousInfantRegistration
		on	PreviousInfantRegistration.I_NUMBER = PreviousBirthRegistration.I_NUMBER

		inner join [$(CMISStaging)].SMMIS.BK_VISIT_DATA PreviousVisit
		on	PreviousVisit.M_NUMBER = PreviousEpisode.HOSP_NUMBER collate latin1_general_bin
		and	PreviousVisit.OCCURRENCE = PreviousEpisode.OCCURRENCE

		where
			PreviousEpisode.HOSP_NUMBER	= Episode.HOSP_NUMBER
		and	PreviousEpisode.OCCURRENCE = Episode.OCCURRENCE
		and	PreviousBirthRegistration.METHOD_DELIVERY in ('S', 'C', 'D', 'E', 'U')
		and	PreviousBirthRegistration.BIRTH_ORDER = 1

		and	(
				coalesce(PreviousEpisode.EP_START_DATE, getdate()) > coalesce(Episode.EP_START_DATE, getdate())
			or	(
					coalesce(PreviousEpisode.EP_START_DATE, getdate()) = coalesce(Episode.EP_START_DATE, getdate())
				and	coalesce(PreviousEpisode.EP_START_TIME, '00:00') > coalesce(Episode.EP_START_TIME, '00:00')
				)
			or	(
					coalesce(PreviousEpisode.EP_START_DATE, getdate()) = coalesce(Episode.EP_START_DATE, getdate())
				and	coalesce(PreviousEpisode.EP_START_TIME, '00:00') = coalesce(Episode.EP_START_TIME, '00:00')
				and	PreviousEpisode.EPISODE > Episode.EPISODE
				)
			or	(
					coalesce(PreviousEpisode.EP_START_DATE, getdate()) = coalesce(Episode.EP_START_DATE, getdate())
				and	coalesce(PreviousEpisode.EP_START_TIME, '00:00') = coalesce(Episode.EP_START_TIME, '00:00')
				and	PreviousEpisode.EPISODE = Episode.EPISODE
				and	PreviousEpisode.SPELL > Episode.SPELL
				)
			)
		)
	and	BirthRegistration.METHOD_DELIVERY in ('S', 'C', 'D', 'E', 'U')
	and	BirthRegistration.BIRTH_ORDER = 1

	group by
		 Episode.HOSP_NUMBER
		
		,dateadd(
			minute
			,convert(int, left(BirthRegistration.TIME_OF_BIRTH, 2)) * 60 +
			 convert(int, right(BirthRegistration.TIME_OF_BIRTH, 2))
			,InfantRegistration.DATE_OF_BIRTH
		)

		,BirthRegistration.METHOD_DELIVERY

	) Birth
on	Birth.CasenoteNumber = Encounter.CasenoteNumber
and	Birth.DateOfBirth between Encounter.EpisodeStartTime and Encounter.EpisodeEndTime

where
	Encounter.AdmissionTime = Encounter.EpisodeStartTime
and	Encounter.DischargeDate is not null
and	Encounter.EpisodeEndTime is not null
and	not exists
	(
	select
		1
	from
		APC.Encounter Previous
	where
		Previous.ProviderSpellNo = Encounter.ProviderSpellNo
	and	Previous.AdmissionTime = Previous.EpisodeStartTime
	and	Previous.DischargeDate is not null
	and	Previous.EpisodeEndTime is not null
	and	(
			Previous.EpisodeEndTime > Encounter.EpisodeEndTime
		or	(
				Previous.EpisodeEndTime = Encounter.EpisodeEndTime
			and	Previous.SourceUniqueID > Encounter.SourceUniqueID
			)
		)
	)



--procedures

truncate table Pathway.WrkProcedure



--PAS procedures

--primary procedure
insert
into
	Pathway.WrkProcedure
(
	 ProviderSpellNo
	,ProcedureCode
	,OperationStartTime
	,OperationEndTime
	,AnaestheticStartTime
	,AnaestheticEndTime
	,Age
)
select distinct
	 Operation.ProviderSpellNo
	,ProcedureCode = coalesce(Operation.PrimaryOperationCode, 'DUMMY')
	,OperationStartTime = null --Operation.PrimaryOperationDate
	,OperationEndTime = null
	,AnaestheticStartTime = null
	,AnaestheticEndTime = null
	,Age = null
from
	APC.Encounter Operation
where
	Operation.AdmissionTime = Operation.EpisodeStartTime
and	(
		Operation.PrimaryOperationCode is not null
	or	(
			Operation.PrimaryOperationCode is null
		and	Operation.ClinicalCodingStatus = 'YES'
		)
	)
and	not exists
	(
	select
		1
	from
		APC.Encounter Previous
	where
		Previous.ProviderSpellNo = Operation.ProviderSpellNo
	and	Previous.AdmissionTime = Previous.EpisodeStartTime
	and	Previous.DischargeDate is not null
	and	Previous.EpisodeEndTime is not null
	and	(
			Previous.EpisodeEndTime > Operation.EpisodeEndTime
		or	(
				Previous.EpisodeEndTime = Operation.EpisodeEndTime
			and	Previous.SourceUniqueID > Operation.SourceUniqueID
			)
		)
	)


----other PAS procedures
--insert
--into
--	Pathway.WrkProcedure
--(
--	 ProviderSpellNo
--	,ProcedureCode
--	,OperationStartTime
--	,OperationEndTime
--	,AnaestheticStartTime
--	,AnaestheticEndTime
--	,Age
--)
--select
--	 Operation.ProviderSpellNo
--	,ProcedureCode = Operation.OperationCode
--	,OperationStartTime = null --Operation.OperationDate
--	,OperationEndTime = null
--	,AnaestheticStartTime = null
--	,AnaestheticEndTime = null
--	,Age = null
--from
--	APC.Operation Operation
--where
----earliest if more than one
--	not exists
--	(
--	select
--		1
--	from
--		APC.Operation PreviousOperation
--	where
--		PreviousOperation.ProviderSpellNo = Operation.ProviderSpellNo
--	and	PreviousOperation.OperationCode = Operation.OperationCode
--	and	(
--			PreviousOperation.OperationDate > Operation.OperationDate
--		or	(
--				PreviousOperation.OperationDate = Operation.OperationDate
--			and	PreviousOperation.SourceUniqueID > Operation.SourceUniqueID
--			)
--		)
--	)
----filter out if we already have this procedure
--and	not exists
--	(
--	select
--		1
--	from
--		Pathway.WrkProcedure
--	where
--		WrkProcedure.ProviderSpellNo = Operation.ProviderSpellNo
--	and	WrkProcedure.ProcedureCode = Operation.OperationCode
--	)


--ORMIS procedures
insert
into
	Pathway.WrkProcedure
(
	 ProviderSpellNo
	,ProcedureCode
	,OperationStartTime
	,OperationEndTime
	,AnaestheticStartTime
	,AnaestheticEndTime
	,Age
)
select
	 EncounterProcedureDetail.ProviderSpellNo
	,ProcedureCode = Operation.OperationCode1
	,OperationStartTime = OperationDetail.OperationStartDate
	,OperationEndTime = OperationDetail.OperationEndDate
	,AnaestheticStartTime = OperationDetail.InAnaestheticTime
	,AnaestheticEndTime = OperationDetail.OperationStartDate
--	,AnaestheticEndTime = OperationDetail.AnaestheticReadyTime
	,Age = null
from
	(
	select distinct
		 Encounter.ProviderSpellNo
		,EncounterProcedureDetail.ProcedureDetailSourceUniqueID
	from
		APC.EncounterProcedureDetail

	inner join APC.Encounter
	on	Encounter.EncounterRecno = EncounterProcedureDetail.EncounterRecno
	) EncounterProcedureDetail

inner join Theatre.ProcedureDetail ProcedureDetail
on	ProcedureDetail.SourceUniqueID = EncounterProcedureDetail.ProcedureDetailSourceUniqueID

inner join Theatre.Operation Operation
on	Operation.OperationCode = ProcedureDetail.ProcedureCode

inner join Theatre.OperationDetail OperationDetail
on	OperationDetail.SourceUniqueID = ProcedureDetail.OperationDetailSourceUniqueID

where
	ProcedureDetail.PrimaryProcedureFlag = 1
--get the 1st distinct procedure for this spell
and	not exists
	(
	select
		1
	from
		(
		select distinct
			 Encounter.ProviderSpellNo
			,EncounterProcedureDetail.ProcedureDetailSourceUniqueID
		from
			APC.EncounterProcedureDetail

		inner join APC.Encounter
		on	Encounter.EncounterRecno = EncounterProcedureDetail.EncounterRecno
		) PreviousEncounterProcedureDetail

	inner join Theatre.ProcedureDetail PreviousProcedureDetail
	on	PreviousProcedureDetail.SourceUniqueID = PreviousEncounterProcedureDetail.ProcedureDetailSourceUniqueID

	inner join Theatre.Operation PreviousOperation
	on	PreviousOperation.OperationCode = PreviousProcedureDetail.ProcedureCode

	where
		PreviousProcedureDetail.PrimaryProcedureFlag = 1
	and	PreviousEncounterProcedureDetail.ProviderSpellNo = EncounterProcedureDetail.ProviderSpellNo
--get first operation only
--	and	PreviousOperation.OperationCode1 = Operation.OperationCode1
	and	(
		coalesce(PreviousProcedureDetail.ProcedureStartTime, getdate()) < coalesce(ProcedureDetail.ProcedureStartTime, getdate())
		or	(
				coalesce(PreviousProcedureDetail.ProcedureStartTime, getdate()) = coalesce(ProcedureDetail.ProcedureStartTime, getdate())
			and	PreviousProcedureDetail.SourceUniqueID > ProcedureDetail.SourceUniqueID
			)
		)
	)
--filter out if we already have this spell
and	not exists
	(
	select
		1
	from
		Pathway.WrkProcedure
	where
		WrkProcedure.ProviderSpellNo = EncounterProcedureDetail.ProviderSpellNo
--	and	WrkProcedure.ProcedureCode = Operation.OperationCode1
	)



--PAS intended procedure
insert
into
	Pathway.WrkProcedure
(
	 ProviderSpellNo
	,ProcedureCode
	,OperationStartTime
	,OperationEndTime
	,AnaestheticStartTime
	,AnaestheticEndTime
	,Age
)
select distinct
	 Encounter.ProviderSpellNo
	,ProcedureCode = Encounter.IntendedPrimaryOperationCode
	,OperationStartTime = null
	,OperationEndTime = null
	,AnaestheticStartTime = null
	,AnaestheticEndTime = null
	,Age = null
from
	APC.Encounter Encounter
where
	Encounter.IntendedPrimaryOperationCode is not null

--filter out if we already have this spell
and	not exists
	(
	select
		1
	from
		Pathway.WrkProcedure
	where
		WrkProcedure.ProviderSpellNo = Encounter.ProviderSpellNo
	)
--and not already coded
--and	coalesce(Encounter.ClinicalCodingStatus, 'NO') = 'NO'

--is not coded with a "Procedure not carried..." diagnosis code
and	not exists
	(
	select
		1
	from
		APC.Diagnosis
	where
		Diagnosis.ProviderSpellNo = Encounter.ProviderSpellNo
	and	Diagnosis.DiagnosisCode like 'Z53%'
	)


--clear null procedures
delete
from
	Pathway.WrkProcedure
where
	ProcedureCode = 'DUMMY'
	
	
----Dusia bit start	


Delete 
from dbo.PathwayDiagnosisData


INSERT into dbo.PathwayDiagnosisData (ProviderSpell,PathwayNo,PathwayDesc)
SELECT
DISTINCT ProviderSpellNo, 
	PathwayDiagnosis.PathwayRecNo,
	PathwayDiagnosis.PathwayDesc
FROM  APC.Encounter INNER JOIN
      PathwayDiagnosis ON PrimaryDiagnosisCode = PathwayDiagnosis.Diagnsosis
  
  

Update Pathway.WrkProcedure

Set ProcedureCode =

               case 
               When PathwayDiagnosisData.PathwayDesc  = 'TOP' Then
					case 
						when Pathway.WrkProcedure.ProcedureCode = 'Q14.5' then 'TOPM'
						when Pathway.WrkProcedure.ProcedureCode like 'Q11%' then 'TOPS'
						when Pathway.WrkProcedure.ProcedureCode in ('Q10.1','Q12.2') then 'TOPS'
					else PathwayDiagnosisData.PathwayDesc
					end
				else 		
				PathwayDiagnosisData.PathwayDesc
				end  
      
               
FROM  Pathway.WrkProcedure INNER JOIN
               PathwayDiagnosisData ON Pathway.WrkProcedure.ProviderSpellNo = PathwayDiagnosisData.ProviderSpell

--------Dusia bit End


--update all dates and times where record can be found in ORMIS
update
	Pathway.WrkProcedure
set
	 OperationStartTime = OperationDetail.OperationStartDate
	,OperationEndTime = OperationDetail.OperationEndDate
	,AnaestheticStartTime = OperationDetail.InAnaestheticTime
	,AnaestheticEndTime = OperationDetail.OperationStartDate
--	,AnaestheticEndTime = OperationDetail.AnaestheticReadyTime
from
	Pathway.WrkProcedure Operation

--link to ORMIS for operation start time, operation end time, anaesthetic start time and anaesthetic end time
inner join 
	(
	select distinct
		 Encounter.ProviderSpellNo
		,EncounterProcedureDetail.ProcedureDetailSourceUniqueID
	from
		APC.EncounterProcedureDetail

	inner join APC.Encounter
	on	Encounter.EncounterRecno = EncounterProcedureDetail.EncounterRecno
	) EncounterProcedureDetail
on	EncounterProcedureDetail.ProviderSpellNo = Operation.ProviderSpellNo

inner join Theatre.ProcedureDetail ProcedureDetail
on	ProcedureDetail.SourceUniqueID = EncounterProcedureDetail.ProcedureDetailSourceUniqueID

-- we don't care if the procedure codes match, just that an operation was recorded
--inner join Theatre.Operation TheatreOperation
--on	TheatreOperation.OperationCode = ProcedureDetail.ProcedureCode
--and	TheatreOperation.OperationCode1 = Operation.ProcedureCode

inner join Theatre.OperationDetail OperationDetail
on	OperationDetail.SourceUniqueID = ProcedureDetail.OperationDetailSourceUniqueID

where
--get the 1st distinct procedure for this spell from ORMIS
	not exists
	(
	select
		1
	from
		APC.Operation PreviousOperation

	--link to ORMIS for operation start time, operation end time, anaesthetic start time and anaesthetic end time
	inner join 
		(
		select distinct
			 Encounter.ProviderSpellNo
			,EncounterProcedureDetail.ProcedureDetailSourceUniqueID
		from
			APC.EncounterProcedureDetail

		inner join APC.Encounter
		on	Encounter.EncounterRecno = EncounterProcedureDetail.EncounterRecno
		) PreviousEncounterProcedureDetail
	on	PreviousEncounterProcedureDetail.ProviderSpellNo = PreviousOperation.ProviderSpellNo

	inner join Theatre.ProcedureDetail PreviousProcedureDetail
	on	PreviousProcedureDetail.SourceUniqueID = PreviousEncounterProcedureDetail.ProcedureDetailSourceUniqueID

-- we don't care if the procedure codes match, just that an operation was recorded
--	inner join Theatre.Operation PreviousTheatreOperation
--	on	PreviousTheatreOperation.OperationCode = PreviousProcedureDetail.ProcedureCode
--	and	PreviousTheatreOperation.OperationCode1 = Operation.ProcedureCode

	where
		PreviousEncounterProcedureDetail.ProviderSpellNo = EncounterProcedureDetail.ProviderSpellNo
--	and	PreviousTheatreOperation.OperationCode1 = TheatreOperation.OperationCode1

	and	(
		coalesce(PreviousProcedureDetail.ProcedureStartTime, getdate()) < coalesce(ProcedureDetail.ProcedureStartTime, getdate())
		or	(
				coalesce(PreviousProcedureDetail.ProcedureStartTime, getdate()) = coalesce(ProcedureDetail.ProcedureStartTime, getdate())
			and	PreviousProcedureDetail.SourceUniqueID < ProcedureDetail.SourceUniqueID
			)
		)
	)

and	Operation.OperationEndTime is null


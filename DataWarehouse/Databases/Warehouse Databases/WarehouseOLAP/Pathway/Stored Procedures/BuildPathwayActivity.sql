﻿CREATE procedure [Pathway].[BuildPathwayActivity] as


exec Pathway.BuildWrkTables

truncate table Pathway.WrkPathwayActivity


BEGIN

	/**************************************************************************/
	/* General Declarations                                                   */
	/**************************************************************************/
	declare @audit varchar(255)
	declare @sqlSelect nvarchar(4000);

	select @sqlSelect = [$(DBA)].dbo.EmailConcat(stmt) from 
		(select 
				('insert into Pathway.WrkPathwayActivity(PathwayTypeCode,ProviderSpellNo,PathwayRecno,AdmissionTime,DischargeTime,OperationStartTime,OperationEndTime,AnaestheticStartTime,AnaestheticEndTime) select PathwayTypeCode = ''IP'',ProviderSpellNo,PathwayRecno,AdmissionTime,DischargeTime,OperationStartTime,OperationEndTime,AnaestheticStartTime,AnaestheticEndTime from ' + Template.SQLObject) stmt from
			Pathway.Template	) stmts 				
					
	exec (@sqlSelect);	

END



truncate table Pathway.PathwayActivity

--insert priority pathways i.e. those that should be used in preference to others

insert
into
	Pathway.PathwayActivity
	(
	 PathwayTypeCode
	,SourceUniqueID
	,PathwayRecno
	,AdmissionTime
	,DischargeTime
	,OperationStartTime
	,OperationEndTime
	,AnaestheticStartTime
	,AnaestheticEndTime
	,Cases
	)
select
	 PathwayActivity.PathwayTypeCode
	,PathwayActivity.ProviderSpellNo
	,PathwayActivity.PathwayRecno
	,PathwayActivity.AdmissionTime
	,PathwayActivity.DischargeTime
	,PathwayActivity.OperationStartTime
	,PathwayActivity.OperationEndTime
	,PathwayActivity.AnaestheticStartTime
	,PathwayActivity.AnaestheticEndTime
	,Cases = 1
from
	Pathway.WrkPathwayActivity PathwayActivity

inner join Pathway.Pathway Pathway
on	Pathway.PathwayRecno = PathwayActivity.PathwayRecno
and	Pathway.Preferred = 'true'

--now insert the rest
insert
into
	Pathway.PathwayActivity
	(
	 PathwayTypeCode
	,SourceUniqueID
	,PathwayRecno
	,AdmissionTime
	,DischargeTime
	,OperationStartTime
	,OperationEndTime
	,AnaestheticStartTime
	,AnaestheticEndTime
	,Cases
	)
select
	 PathwayTypeCode
	,ProviderSpellNo
	,PathwayRecno
	,AdmissionTime
	,DischargeTime
	,OperationStartTime
	,OperationEndTime
	,AnaestheticStartTime
	,AnaestheticEndTime
	,Cases = 1
from
	Pathway.WrkPathwayActivity PathwayActivity
where
	not exists
	(
	select
		1
	from
		Pathway.PathwayActivity OtherPathwayActivity
	where
		OtherPathwayActivity.PathwayTypeCode = PathwayActivity.PathwayTypeCode
	and	OtherPathwayActivity.SourceUniqueID = PathwayActivity.ProviderSpellNo
	)


--build missing dates into calendar
insert into WarehouseOLAP.dbo.CalendarBase
(
	TheDate
)
select distinct
	dateadd(day, datediff(day, 0, PathwayActivity.DischargeTime), 0)
from
	Pathway.PathwayActivity
where
	not exists
	(
	select
		1
	from	
		WarehouseOLAP.dbo.CalendarBase Calendar
	where
		Calendar.TheDate = dateadd(day, datediff(day, 0, PathwayActivity.DischargeTime), 0)
	)

--re-build Calendar
if @@rowcount > 0
	exec WarehouseOLAP.dbo.BuildCalendar


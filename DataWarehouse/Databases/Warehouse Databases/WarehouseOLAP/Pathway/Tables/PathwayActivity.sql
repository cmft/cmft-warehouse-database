﻿CREATE TABLE [Pathway].[PathwayActivity] (
    [PathwayTypeCode]      VARCHAR (50)  NOT NULL,
    [SourceUniqueID]       VARCHAR (50)  NOT NULL,
    [PathwayRecno]         INT           NOT NULL,
    [AdmissionTime]        SMALLDATETIME NULL,
    [DischargeTime]        SMALLDATETIME NULL,
    [OperationStartTime]   DATETIME      NULL,
    [OperationEndTime]     DATETIME      NULL,
    [AnaestheticStartTime] DATETIME      NULL,
    [AnaestheticEndTime]   DATETIME      NULL,
    [Cases]                INT           NULL,
    CONSTRAINT [PK_PathwayActivity] PRIMARY KEY CLUSTERED ([PathwayTypeCode] ASC, [SourceUniqueID] ASC, [PathwayRecno] ASC)
);


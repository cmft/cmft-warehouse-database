﻿CREATE TABLE [Pathway].[WrkPathwayActivity] (
    [PathwayTypeCode]      VARCHAR (2)   NOT NULL,
    [ProviderSpellNo]      VARCHAR (50)  NOT NULL,
    [PathwayRecno]         INT           NULL,
    [AdmissionTime]        SMALLDATETIME NULL,
    [DischargeTime]        SMALLDATETIME NULL,
    [OperationStartTime]   DATETIME      NULL,
    [OperationEndTime]     DATETIME      NULL,
    [AnaestheticStartTime] DATETIME      NULL,
    [AnaestheticEndTime]   DATETIME      NULL
);


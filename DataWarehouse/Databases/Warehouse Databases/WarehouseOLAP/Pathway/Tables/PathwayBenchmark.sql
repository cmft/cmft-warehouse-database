﻿CREATE TABLE [Pathway].[PathwayBenchmark] (
    [PathwayRecno]       INT NOT NULL,
    [BenchmarkTypeRecno] INT NOT NULL,
    [BenchmarkMinutes]   INT NOT NULL,
    CONSTRAINT [PK_PathwayBenchmark] PRIMARY KEY CLUSTERED ([PathwayRecno] ASC, [BenchmarkTypeRecno] ASC)
);


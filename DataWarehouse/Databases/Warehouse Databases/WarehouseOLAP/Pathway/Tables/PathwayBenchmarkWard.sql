﻿CREATE TABLE [Pathway].[PathwayBenchmarkWard] (
    [PathwayRecno]       INT          NOT NULL,
    [BenchmarkTypeRecno] INT          NOT NULL,
    [WardCode]           VARCHAR (10) NOT NULL,
    [BenchmarkMinutes]   INT          NOT NULL,
    CONSTRAINT [PK_PathwayBenchmarkWard] PRIMARY KEY CLUSTERED ([PathwayRecno] ASC, [BenchmarkTypeRecno] ASC, [WardCode] ASC)
);


﻿CREATE TABLE [Pathway].[WrkEncounter] (
    [ProviderSpellNo]       VARCHAR (50)  NOT NULL,
    [PatientCategoryCode]   VARCHAR (2)   NOT NULL,
    [SiteCode]              VARCHAR (10)  NULL,
    [WardCode]              VARCHAR (10)  NULL,
    [AdmissionTime]         SMALLDATETIME NULL,
    [DischargeTime]         SMALLDATETIME NULL,
    [AgeOnAdmission]        INT           NULL,
    [MethodOfDeliveryCode]  CHAR (1)      NULL,
    [FirstPregnancy]        BIT           NULL,
    [SpecialtyCode]         VARCHAR (10)  NULL,
    [AdmissionDivisionCode] VARCHAR (5)   NULL,
    [DateOfBirth]           DATETIME      NULL,
    CONSTRAINT [PK_WrkEncounter] PRIMARY KEY CLUSTERED ([ProviderSpellNo] ASC)
);


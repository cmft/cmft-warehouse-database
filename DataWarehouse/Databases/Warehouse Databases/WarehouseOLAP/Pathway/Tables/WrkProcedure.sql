﻿CREATE TABLE [Pathway].[WrkProcedure] (
    [ProviderSpellNo]      VARCHAR (50) NOT NULL,
    [ProcedureCode]        VARCHAR (8)  NOT NULL,
    [OperationStartTime]   DATETIME     NULL,
    [OperationEndTime]     DATETIME     NULL,
    [AnaestheticStartTime] DATETIME     NULL,
    [AnaestheticEndTime]   DATETIME     NULL,
    [Age]                  INT          NULL,
    CONSTRAINT [PK_WrkProcedure] PRIMARY KEY CLUSTERED ([ProviderSpellNo] ASC, [ProcedureCode] ASC)
);


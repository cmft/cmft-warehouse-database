﻿CREATE TABLE [Pathway].[Pathway] (
    [PathwayRecno]  INT           IDENTITY (1, 1) NOT NULL,
    [Pathway]       VARCHAR (255) NOT NULL,
    [Preferred]     BIT           CONSTRAINT [DF_Pathway_Preferred] DEFAULT ((0)) NULL,
    [TemplateRecno] INT           NOT NULL,
    CONSTRAINT [PK_Pathway] PRIMARY KEY CLUSTERED ([PathwayRecno] ASC),
    CONSTRAINT [FK_Pathway_Template] FOREIGN KEY ([TemplateRecno]) REFERENCES [Pathway].[Template] ([TemplateRecno])
);


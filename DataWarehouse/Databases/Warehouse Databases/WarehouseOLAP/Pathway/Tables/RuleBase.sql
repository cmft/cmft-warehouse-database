﻿CREATE TABLE [Pathway].[RuleBase] (
    [RuleBaseRecno]        INT          IDENTITY (1, 1) NOT NULL,
    [PathwayRecno]         INT          NULL,
    [ProcedureCode]        VARCHAR (10) NULL,
    [PatientCategoryCode]  VARCHAR (2)  NULL,
    [SiteCode]             VARCHAR (10) NULL,
    [WardCode]             VARCHAR (10) NULL,
    [LowerLimit]           FLOAT (53)   NULL,
    [UpperLimit]           FLOAT (53)   NULL,
    [MethodOfDeliveryCode] CHAR (1)     NULL,
    [FirstPregnancy]       BIT          NULL,
    [SpecialtyCode]        VARCHAR (10) NULL,
    [DivisionCode]         VARCHAR (5)  NULL,
    CONSTRAINT [PK_RuleBase] PRIMARY KEY NONCLUSTERED ([RuleBaseRecno] ASC)
);


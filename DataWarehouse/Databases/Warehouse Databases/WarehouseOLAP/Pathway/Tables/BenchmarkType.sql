﻿CREATE TABLE [Pathway].[BenchmarkType] (
    [BenchmarkTypeRecno] INT           IDENTITY (1, 1) NOT NULL,
    [BenchmarkTypeCode]  VARCHAR (10)  NULL,
    [BenchmarkType]      VARCHAR (255) NULL,
    CONSTRAINT [PK_BenchmarkType] PRIMARY KEY CLUSTERED ([BenchmarkTypeRecno] ASC)
);


﻿CREATE TABLE [Pathway].[Template] (
    [TemplateRecno] INT           IDENTITY (1, 1) NOT NULL,
    [TemplateCode]  VARCHAR (50)  NOT NULL,
    [Template]      VARCHAR (255) NOT NULL,
    [SQLObject]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Template_1] PRIMARY KEY CLUSTERED ([TemplateRecno] ASC)
);


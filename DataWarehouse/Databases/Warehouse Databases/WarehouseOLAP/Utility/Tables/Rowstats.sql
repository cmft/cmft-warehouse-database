﻿CREATE TABLE [Utility].[Rowstats] (
    [fulltable_name] NVARCHAR (261) NULL,
    [schema_name]    NVARCHAR (128) NULL,
    [table_name]     [sysname]      NOT NULL,
    [rows]           INT            NULL,
    [sampleTime]     DATETIME2 (7)  NOT NULL,
    [idx]            INT            IDENTITY (1, 1) NOT NULL,
    PRIMARY KEY CLUSTERED ([idx] ASC)
);


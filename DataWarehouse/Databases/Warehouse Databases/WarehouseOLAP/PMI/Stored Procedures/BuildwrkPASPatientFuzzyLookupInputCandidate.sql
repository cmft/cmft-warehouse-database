﻿
CREATE procedure [PMI].[BuildwrkPASPatientFuzzyLookupInputCandidate]
(
	 @BatchId		INT
	,@IsNewBatch	BIT = 0
	,@RowsToProcess INT OUTPUT
)
AS

/******************************************************************************
	**  Name: PMI.BuildwrkPASPatientFuzzyLookupInputCandidate
	**  Purpose: 
	**
	**  Populates the wrkPASPatientFuzzyLookupInputCandidate table which is used
	**	as a filter for populating the wrkPASPatientFuzzyLookupInput table 
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	**             CB         Initial Coding
	** 15.03.13    MH         Brought into source control
	** 15.03.13    MH         Added batch processing functionality
	******************************************************************************/
	
	TRUNCATE TABLE wrkPASPatientFuzzyLookupInputCandidate

	
	INSERT INTO wrkPASPatientFuzzyLookupInputCandidate
	(
		InternalPatientNumber
	)
	SELECT DISTINCT 
		InternalPatientNumber  = CN.InternalPatientNumber
	FROM
	(
	/*
		This rule won't necessarily return 1000 patients as there may be many inactive case notes
		for the same patient within the 1000 sorted case notes.
		However it doesn't strictly matter as whatever patient is selected they are assigned to a batch
		which will prohibit them being selected again
	*/
		SELECT TOP 1000
				InternalPatientNumber = CAST(Casenote.SourcePatientNo AS VARCHAR)
		FROM
			BasePASCasenoteInactive Casenote
			INNER JOIN PMI.BasePASPatientIndex PAT
				ON Casenote.SourcePatientNo = PAT.InternalPatientNumber
		WHERE
		(
				PAT.BatchId	IS NULL
			AND @IsNewBatch = 1
		)
		OR
		(
				PAT.BatchId = @BatchId
			AND @IsNewBatch = 0
		)
		ORDER BY
			Casenote.CasenoteNo ASC
	) CN

	-- Set output parameter
	SET @RowsToProcess = @@ROWCOUNT	

	-- Set the batch id in the BasePASPatientIndex table 
	UPDATE
		BasePASPatientIndex
	SET 
		 BatchId		= @BatchId
		,HasDuplicates	= NULL
		,DateProcessed	= NULL
	FROM
		PMI.BasePASPatientIndex wrkPAT
		INNER JOIN wrkPASPatientFuzzyLookupInputCandidate wrkCAND
			ON wrkPAT.InternalPatientNumber = wrkCAND.InternalPatientNumber


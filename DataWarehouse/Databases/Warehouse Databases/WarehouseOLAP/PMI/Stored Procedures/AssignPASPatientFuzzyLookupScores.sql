﻿
CREATE procedure [PMI].[AssignPASPatientFuzzyLookupScores] 
(
	 @BatchId INT
	,@RowsMatched INT OUTPUT
)
AS

	/******************************************************************************
	**  Name: PMI.AssignPASPatientFuzzyLookupScores
	**  Purpose: 
	**
	**  Insert BasePASPatientIndexMatched with matching patients from the BasePASPatientFuzzyLookup table
	**  and update the BasePASPatientIndex with the duplicates flag is applicable
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 18.03.13    MH         Initial coding
	******************************************************************************/

	DECLARE @DateProcessed DATETIME = GETDATE()

	-- Delete any previously matched rows
	DELETE FROM PMI.BasePASPatientIndexMatched 
	WHERE
		InternalPatientNumber IN (SELECT DISTINCT InternalPatientNumber FROM PMI.BasePASPatientFuzzyLookup)

	-- Insert matched
	INSERT INTO PMI.BasePASPatientIndexMatched 
	(
		 [InternalPatientNumber]
		,[MatchedInternalPatientNumber]
		,[SimilarityScore]
		,[ConfidenceScore]
	)
	SELECT
		 [InternalPatientNumber]			= InternalPatientNumber
		,[MatchedInternalPatientNumber]		= [InternalPatientNumber (1)]
		,[SimilarityScore]					= _Similarity
		,[ConfidenceScore]					= _Confidence

	FROM
		PMI.BasePASPatientFuzzyLookup 

	-- Set output parameter of the number of duplicate patients
	SET @RowsMatched = @@ROWCOUNT

	-- Set duplicates flag in PMI.BasePASPatientIndex
	UPDATE PMI.BasePASPatientIndex
		SET 
			 HasDuplicates = CASE 
								WHEN L.InternalPatientNumber IS NULL THEN 0
								ELSE 1
							 END
			,DateProcessed = @DateProcessed
	FROM
		PMI.BasePASPatientIndex I
		LEFT JOIN
		(
			SELECT DISTINCT
				InternalPatientNumber
			FROM 
				PMI.BasePASPatientFuzzyLookup
		) L
			ON I.InternalPatientNumber = L.InternalPatientNumber
	WHERE
			BatchId = @BatchId
		AND DateProcessed IS NULL
			

﻿
CREATE procedure [PMI].[BuildwrkPASPatientFuzzyLookupInput] as

/******************************************************************************
	**  Name: PMI.BuildwrkPASPatientFuzzyLookupInput
	**  Purpose: 
	**
	**  Populates the wrkPASPatientFuzzyLookupInput table which is the data source
	**  for the PMI Fuzzy Lookup SSIS package
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 15.03.13    CB         Initial coding
	******************************************************************************/

truncate table wrkPASPatientFuzzyLookupInput

insert into wrkPASPatientFuzzyLookupInput
(
	 DistrictNumber
	,Forenames
	,GpCode
	,InternalDateOfBirth
	,InternalPatientNumber
	,PtAddrLine1
	,PtAddrPostCode
	,PtDobDayInt
	,PtDobMonthInt
	,PtDobYearInt
	,Sex
	,Surname

)
select 
	 Patient.DistrictNumber
	,Patient.Forenames
	,Patient.GpCode
	,Patient.InternalDateOfBirth
	,Patient.InternalPatientNumber
	,Patient.PtAddrLine1
	,Patient.PtAddrPostCode
	,Patient.PtDobDayInt
	,Patient.PtDobMonthInt
	,Patient.PtDobYearInt
	,Patient.Sex
	,Patient.Surname
from
	PMI.BasePASPatientIndex Patient

inner join wrkPASPatientFuzzyLookupInputCandidate Filter
on	Filter.InternalPatientNumber = Patient.InternalPatientNumber






﻿
CREATE procedure [PMI].[BuildBasePASPatientIndex] as

/******************************************************************************
	**  Name: PMI.BuildBasePASPatientIndex
	**  Purpose: 
	**
	**  Populates the BasePASPatientIndex table which is a synchronised version
	**  of the PAS.Inquire.PATDATA table
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 15.03.13    CB         Initial coding
	******************************************************************************/

merge PMI.BasePASPatientIndex target
using
	(
	select
		 DistrictNumber
		,Forenames
		,GpCode
		,InternalPatientNumber
		,InternalDateOfBirth
		,NHSNumber
		,PtAddrLine1
		,PtAddrPostCode
		,PtDobDayInt
		,PtDobMonthInt
		,PtDobYearInt
		,Sex
		,Surname
	from
		PAS.Inquire.PATDATA
	) source
	on source.InternalPatientNumber = target.InternalPatientNumber

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 DistrictNumber
			,Forenames
			,GpCode
			,InternalPatientNumber
			,InternalDateOfBirth
			,NHSNumber
			,PtAddrLine1
			,PtAddrPostCode
			,PtDobDayInt
			,PtDobMonthInt
			,PtDobYearInt
			,Sex
			,Surname
			)
		values
			(
			 DistrictNumber
			,Forenames
			,GpCode
			,InternalPatientNumber
			,InternalDateOfBirth
			,NHSNumber
			,PtAddrLine1
			,PtAddrPostCode
			,PtDobDayInt
			,PtDobMonthInt
			,PtDobYearInt
			,Sex
			,Surname
			)

	when matched
	and not
		(
			target.DistrictNumber = source.DistrictNumber
		and target.Forenames = source.Forenames
		and target.GpCode = source.GpCode
		and target.InternalDateOfBirth = source.InternalDateOfBirth
		and target.NHSNumber = source.NHSNumber
		and target.PtAddrLine1 = source.PtAddrLine1
		and target.PtAddrPostCode = source.PtAddrPostCode
		and target.PtDobDayInt = source.PtDobDayInt
		and target.PtDobMonthInt = source.PtDobMonthInt
		and target.PtDobYearInt = source.PtDobYearInt
		and target.Sex = source.Sex
		and target.Surname = source.Surname
		)
	then
		update
		set
			 target.DistrictNumber = source.DistrictNumber
			,target.Forenames = source.Forenames
			,target.GpCode = source.GpCode
			,target.InternalDateOfBirth = source.InternalDateOfBirth
			,target.NHSNumber = source.NHSNumber
			,target.PtAddrLine1 = source.PtAddrLine1
			,target.PtAddrPostCode = source.PtAddrPostCode
			,target.PtDobDayInt = source.PtDobDayInt
			,target.PtDobMonthInt = source.PtDobMonthInt
			,target.PtDobYearInt = source.PtDobYearInt
			,target.Sex = source.Sex
			,target.Surname = source.Surname
;

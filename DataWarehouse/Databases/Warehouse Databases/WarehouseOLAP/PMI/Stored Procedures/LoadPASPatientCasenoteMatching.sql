﻿
CREATE procedure [PMI].[LoadPASPatientCasenoteMatching] 
(
	 @InBatchName	VARCHAR(100) = NULL
	,@InBatchId		INT = NULL
	,@NewBatchFlag	BIT = 0
)
AS

	/******************************************************************************
	**  Name: PMI.LoadPASPatientCasenoteMatching
	**  Purpose: 
	**
	**  Controller script to match casenotes for the same patient
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 15.03.13    MH         Initial coding
	******************************************************************************/

	-- Validation of Input Parameters
	DECLARE @BatchId			INT				= NULL
	DECLARE @BatchName			VARCHAR(200)	= ''
	DECLARE @StartTime			DATETIME		= GETDATE()
	DECLARE @EndTime			DATETIME
	DECLARE @StartFLTime		DATETIME
	DECLARE @EndFLTime			DATETIME
	DECLARE @RowsToProcess		INT				= 0
	DECLARE @RowsMatched		INT				= 0

	DECLARE @MailFrom			VARCHAR(200)
	DECLARE @MailTo				VARCHAR(200)
	DECLARE @Subject			VARCHAR(200)
	DECLARE @Body				VARCHAR(MAX) = ''
	DECLARE @AuditRowId			INT
	DECLARE @BatchCaseNote		VARCHAR(3) = 'CN'
	DECLARE @SSISPackagePath	VARCHAR(300) = '\\warehousefs\PublicInformation\Users\Malcolm Hodson\SSIS_Repository\'
	DECLARE @SSISPackageName	VARCHAR(100) = 'PMI Fuzzy Lookup First Run.dtsx'
	DECLARE @SSISConfigDev		VARCHAR(100) = 'PMI FuzzyLookupDevelopment.dtsConfig'
	DECLARE @SSISConfigProd		VARCHAR(100) = 'PMI FuzzyLookupProduction.dtsConfig'
	DECLARE @SSISCommand		VARCHAR(4000)
	DECLARE @CommandResult		INT
	
	SET NOCOUNT ON

	-- Insert an audit row with the start time, so at least we know it started
	INSERT INTO PMI.Audit
	(
		StartTime
	)
	SELECT
		@StartTime

	SELECT @AuditRowId = SCOPE_IDENTITY()


	SELECT @MailFrom = TextValue FROM Warehouse.Utility.Parameter WHERE Parameter = 'MAILFROM'
	SELECT @MailTo = TextValue FROM Warehouse.Utility.Parameter WHERE Parameter = 'SSISNOTIFICATIONS'
	
	BEGIN TRY
		IF (@InBatchName IS NULL AND @InBatchId IS NULL)
		  BEGIN
			RAISERROR('A Batch name or Batch Id must be passed as a parameter', 11	,1)
		  END

		IF (@NewBatchFlag = 1 AND @InBatchId IS NOT NULL)
		  BEGIN
			RAISERROR('The NewBatchFlag and BatchId cannot both have parameter values', 11 ,1)
		  END

		-- Lookup the batch from the batch id or batch name
		SELECT 
			 @BatchId = BatchId
			,@BatchName = BatchName
		FROM
			PMI.BasePASPatientBatch
		WHERE
				BatchName	= ISNULL(@InBatchName, BatchName)
			AND	BatchId		= ISNULL(@InBatchId, BatchId)
			AND BatchType	= @BatchCaseNote
	
		IF (@BatchId IS NULL AND @NewBatchFlag IS NULL)
		  BEGIN
			RAISERROR('The Batch name %s or Batch Id %d does not exist', 11	,1, @BatchName, @InBatchId)
			RETURN 1
		  END
	
		IF (@BatchId IS NULL AND @NewBatchFlag = 1 AND COALESCE(@InBatchName,'') = '')
		  BEGIN
			RAISERROR('Cannot create a new Batch with no batch name', 11 ,1)
		  END

		-- Create new Batch if required and set the @BatchId variable
		IF (@BatchId IS NULL AND @NewBatchFlag = 1)
		  BEGIN
			INSERT INTO PMI.BasePASPatientBatch (BatchName, BatchType) VALUES (@InBatchName, @BatchCaseNote)
			SELECT @BatchId = SCOPE_IDENTITY()
			SET @BatchName = @InBatchName
		  END

		SET @Subject = 'Casenote Matching Batch - Run Successful'
		SET @Body = '<P>Running Batch ' + COALESCE(@BatchName, 'Unknown')  + ' : BatchId ' + CAST(@BatchId AS VARCHAR(20)) + '</P>'

		-- Synchronise the BasePASPatientIndex table with the PAS.PatData table
		EXEC PMI.BuildBasePASPatientIndex

		-- Create a table of patients to be processed by this batch
		EXEC PMI.BuildwrkPASPatientFuzzyLookupInputCandidate @BatchId, @NewBatchFlag, @RowsToProcess = @RowsToProcess OUTPUT

		-- Build the patient fuzzy lookup SSIS package data source table
		EXEC PMI.BuildwrkPASPatientFuzzyLookupInput

		-- Run the Fuzzy lookup SSIS package via a SQL job to match patients from the wrkPASPatientFuzzyLookupInput table
		SET @StartFLTime = GETDATE()

		SET @SSISCommand = 'dtexec /FILE "'
							+ @SSISPackagePath
							+ @SSISPackageName
							+ '" /X86 '
							+ '/CONF "'
							+ @SSISPackagePath
							+ CASE WHEN @@SERVERNAME = 'WAREHOUSESQL' THEN @SSISConfigProd ELSE @SSISConfigDev END
							+ '" /CHECKPOINTING OFF /REPORTING E'

		EXEC @CommandResult = xp_cmdshell @SSISCommand, NO_OUTPUT

		IF (@CommandResult = 1)
			RAISERROR('An error was returned from the SSIS package', 11 ,1)

		SET @EndFLTime = GETDATE()

		-- Update BasePASPatientIndexMatched with Similarity and Confidence scores for matched patients
		EXEC PMI.AssignPASPatientFuzzyLookupScores @BatchId, @RowsMatched = @RowsMatched OUTPUT

		-- Finished - wrap up
		SET @EndTime = GETDATE()
		SET @Body = @Body	+ '<P>Run Time ' 
							+ CAST(DATEDIFF(MINUTE, @StartTime, @EndTime) AS VARCHAR(5)) 
							+ ' Minutes</P>'
							+ '<P>Fuzzy Lookup Time '
							+ CAST(DATEDIFF(MINUTE, @StartFLTime, @EndFLTime) AS VARCHAR(5)) 
							+ ' Minutes</P>'
							+ '<P>Patients Processed '
							+ CAST(@RowsToProcess AS VARCHAR(5)) 
							+ ' Duplicates '
							+ CAST(@RowsMatched AS VARCHAR(5)) 
							+ '</P>'
	END TRY

	BEGIN CATCH
		SET @Subject = 'Casenote Matching Batch - Run FAILED'
		SET @Body = @Body	+ '<P>The following errors occurred in procedure ' 
							+  ERROR_PROCEDURE() 
							+ '</P>'
							+ '<P>Message : '
							+ ERROR_MESSAGE() 
							+ '</P>'
	END CATCH

	-- Update Audit Table Row
	UPDATE PMI.Audit
		SET	 BatchId				= @BatchId
			,EndTime				= @EndTime
			,StartFuzzyLookupTime	= @StartFLTime
			,EndFuzzyLookupTime		= @EndFLTime
			,RowsToProcess			= @RowsToProcess
			,RowsMatched			= @RowsMatched
	WHERE
		ID = @AuditRowId

	-- E-mail results	
	EXEC msdb.dbo.sp_send_dbmail 
			 @Subject		= @Subject
			,@From_Address	= @MailFrom
			,@Body			= @Body
			,@Recipients	= @MailTo
			,@Body_Format	= 'HTML' 


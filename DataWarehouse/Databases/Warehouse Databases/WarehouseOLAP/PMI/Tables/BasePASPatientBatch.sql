﻿CREATE TABLE [PMI].[BasePASPatientBatch] (
    [BatchId]     INT           IDENTITY (1, 1) NOT NULL,
    [BatchName]   VARCHAR (100) NULL,
    [DateCreated] DATETIME      CONSTRAINT [DateCreated_Constraint1] DEFAULT (getdate()) NULL,
    [BatchType]   VARCHAR (3)   NULL,
    [IsActive]    BIT           CONSTRAINT [IsActive_Constraint1] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_BasePASCasenoteBatch] PRIMARY KEY CLUSTERED ([BatchId] ASC)
);


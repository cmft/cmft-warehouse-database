﻿CREATE TABLE [PMI].[Audit] (
    [ID]                   INT      IDENTITY (1, 1) NOT NULL,
    [BatchId]              INT      NULL,
    [StartTime]            DATETIME NULL,
    [EndTime]              DATETIME NULL,
    [StartFuzzyLookupTime] DATETIME NULL,
    [EndFuzzyLookupTime]   DATETIME NULL,
    [RowsToProcess]        INT      NULL,
    [RowsMatched]          INT      NULL
);


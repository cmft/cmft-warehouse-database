﻿CREATE TABLE [PMI].[BasePASPatientIndexMatched] (
    [ID]                           INT          IDENTITY (1, 1) NOT NULL,
    [InternalPatientNumber]        NVARCHAR (9) NOT NULL,
    [MatchedInternalPatientNumber] NVARCHAR (9) NOT NULL,
    [SimilarityScore]              FLOAT (53)   NULL,
    [ConfidenceScore]              FLOAT (53)   NULL,
    CONSTRAINT [PK_BasePASPatientIndexMatched] PRIMARY KEY CLUSTERED ([ID] ASC)
);


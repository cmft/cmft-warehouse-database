﻿CREATE TABLE [dbo].[WaitTypeBase] (
    [WaitTypeCode]       VARCHAR (10)  NOT NULL,
    [WaitType]           VARCHAR (255) NOT NULL,
    [ParentWaitTypeCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_WaitTypeBase] PRIMARY KEY CLUSTERED ([WaitTypeCode] ASC),
    CONSTRAINT [FK_WaitTypeBase_WaitTypeBase] FOREIGN KEY ([ParentWaitTypeCode]) REFERENCES [dbo].[WaitTypeBase] ([WaitTypeCode])
);


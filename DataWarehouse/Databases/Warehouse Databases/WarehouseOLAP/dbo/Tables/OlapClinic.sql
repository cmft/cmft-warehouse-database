﻿CREATE TABLE [dbo].[OlapClinic] (
    [ClinicCode]        VARCHAR (8)  NOT NULL,
    [Comment]           VARCHAR (20) NULL,
    [ConsultantCode]    VARCHAR (6)  NULL,
    [SpecialtyCode]     VARCHAR (4)  NULL,
    [Clinic]            VARCHAR (50) NULL,
    [SiteCode]          VARCHAR (4)  NULL,
    [FunctionCode]      VARCHAR (4)  NULL,
    [ProviderCode]      VARCHAR (4)  NULL,
    [VirtualClinicFlag] INT          NULL,
    [POAClinicFlag]     INT          NULL,
    CONSTRAINT [PK_OlapClinic] PRIMARY KEY CLUSTERED ([ClinicCode] ASC)
);


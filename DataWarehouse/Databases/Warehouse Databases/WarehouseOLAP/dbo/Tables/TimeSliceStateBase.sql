﻿CREATE TABLE [dbo].[TimeSliceStateBase] (
    [TimeSliceStateCode]       CHAR (3)     NOT NULL,
    [TimeSliceState]           VARCHAR (50) NOT NULL,
    [TimeSliceStateParentCode] CHAR (3)     NOT NULL,
    [TimeSliceStateParent]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TimeSliceStateBase] PRIMARY KEY CLUSTERED ([TimeSliceStateCode] ASC)
);


﻿CREATE TABLE [dbo].[FactObservationSignificantEvent] (
    [SignificantEventRecno]  INT          NOT NULL,
    [SpecialtyID]            INT          NULL,
    [WardID]                 INT          NULL,
    [SignificantEventTypeID] INT          NULL,
    [StartDate]              DATE         NULL,
    [StartTimeOfDayCode]     INT          NOT NULL,
    [OutOfHours]             BIT          NULL,
    [AgeID]                  INT          NULL,
    [ContextCode]            VARCHAR (11) NOT NULL,
    [Cases]                  INT          NULL
);


﻿CREATE TABLE [dbo].[AgeBase] (
    [AgeCode]       VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Age]           VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AgeParentCode] VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    CONSTRAINT [PK_AgeBase] PRIMARY KEY CLUSTERED ([AgeCode] ASC),
    CONSTRAINT [FK_AgeBase_AgeBase] FOREIGN KEY ([AgeParentCode]) REFERENCES [dbo].[AgeBase] ([AgeCode])
);


﻿CREATE TABLE [dbo].[FactAPCWaitingList] (
    [EncounterRecno]              INT           NOT NULL,
    [Cases]                       INT           NOT NULL,
    [LengthOfWait]                INT           NULL,
    [AgeCode]                     VARCHAR (50)  NOT NULL,
    [CensusDate]                  SMALLDATETIME NOT NULL,
    [ConsultantCode]              VARCHAR (10)  NOT NULL,
    [SpecialtyCode]               VARCHAR (10)  NOT NULL,
    [DurationCode]                VARCHAR (10)  NOT NULL,
    [PracticeCode]                VARCHAR (8)   NOT NULL,
    [SiteCode]                    VARCHAR (50)  NOT NULL,
    [WaitTypeCode]                VARCHAR (10)  NOT NULL,
    [StatusCode]                  VARCHAR (10)  NOT NULL,
    [CategoryCode]                VARCHAR (10)  NOT NULL,
    [AddedLastWeek]               INT           NULL,
    [WaitingListAddMinutes]       INT           NULL,
    [OPCSCoded]                   INT           NULL,
    [WithRTTStartDate]            INT           NULL,
    [WithRTTStatusCode]           INT           NULL,
    [WithExpectedAdmissionDate]   INT           NULL,
    [RTTActivity]                 BIT           NULL,
    [RTTBreachStatusCode]         VARCHAR (10)  NULL,
    [DiagnosticProcedure]         BIT           NULL,
    [TCIDate]                     SMALLDATETIME NULL,
    [SexCode]                     VARCHAR (1)   NULL,
    [DirectorateCode]             VARCHAR (5)   NULL,
    [DurationAtTCIDateCode]       VARCHAR (10)  NULL,
    [WithTCIDate]                 INT           NULL,
    [WithGuaranteedAdmissionDate] BIT           NULL,
    [WithRTTOpenPathway]          BIT           NULL,
    CONSTRAINT [PK_APCWaitingListFact] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


﻿CREATE TABLE [dbo].[DurationBase2] (
    [DurationCode]          VARCHAR (10) NOT NULL,
    [Duration]              VARCHAR (50) NULL,
    [DurationBandCode]      VARCHAR (10) NULL,
    [APCWLDurationBandCode] VARCHAR (10) NULL,
    [APCWLDurationBand]     VARCHAR (50) NULL,
    [OPWLDurationBandCode]  VARCHAR (10) NULL,
    [OPWLDurationBand]      VARCHAR (50) NULL,
    CONSTRAINT [PK_DurationBase2] PRIMARY KEY CLUSTERED ([DurationCode] ASC)
);


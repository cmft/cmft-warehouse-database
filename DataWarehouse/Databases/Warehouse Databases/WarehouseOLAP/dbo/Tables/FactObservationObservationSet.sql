﻿CREATE TABLE [dbo].[FactObservationObservationSet] (
    [ObservationSetRecno]         INT          NOT NULL,
    [SpecialtyID]                 INT          NULL,
    [WardID]                      INT          NULL,
    [StartDate]                   DATE         NULL,
    [StartTimeOfDayCode]          INT          NULL,
    [DueTimeStatusID]             INT          NULL,
    [ObservationNotTakenReasonID] INT          NULL,
    [ClinicianPresentSeniorityID] INT          NULL,
    [OverallRiskIndexCode]        INT          NULL,
    [OverallAssessedStatusID]     INT          NULL,
    [OutOfHours]                  BIT          NULL,
    [AgeID]                       INT          NULL,
    [ContextCode]                 VARCHAR (20) NULL,
    [Cases]                       INT          NOT NULL,
    [OverallAssessedStatusScore]  INT          NULL
);


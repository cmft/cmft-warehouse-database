﻿CREATE TABLE [dbo].[FactObservationCombined] (
    [AlertRecno]            INT          NOT NULL,
    [ObservationSetRecno]   INT          NOT NULL,
    [MetricCode]            VARCHAR (5)  NOT NULL,
    [SpecialtyID]           INT          NULL,
    [WardID]                INT          NULL,
    [EventDate]             DATE         NULL,
    [EventTimeOfDayCode]    INT          NULL,
    [GenericDimensionCode1] VARCHAR (20) NULL,
    [GenericDimensionCode2] VARCHAR (20) NULL,
    [OutOfHours]            BIT          NULL,
    [Cases]                 INT          NOT NULL
);


﻿CREATE TABLE [dbo].[BaseBedComplement] (
    [TheDate]                    DATE          NOT NULL,
    [WardCode]                   VARCHAR (255) NULL,
    [DaycaseBeds]                INT           NULL,
    [InpatientBeds]              INT           NULL,
    [SingleRoomBeds]             INT           NULL,
    [WardBedComment]             VARCHAR (255) NULL,
    [WardBedStartDate]           DATE          NOT NULL,
    [DivisionCode]               VARCHAR (5)   NULL,
    [SpecialtyGroupID]           INT           NULL,
    [WardTypeID]                 INT           NULL,
    [Schedule]                   VARCHAR (255) NULL,
    [WardConfigurationStartDate] DATE          NOT NULL,
    [WardConfigurationEndDate]   DATE          NULL,
    [WardConfigurationComment]   VARCHAR (255) NULL,
    [BaseBedComplementRecno]     INT           IDENTITY (1, 1) NOT NULL
);


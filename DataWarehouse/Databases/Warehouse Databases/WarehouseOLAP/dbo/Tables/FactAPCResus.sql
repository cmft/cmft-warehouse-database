﻿CREATE TABLE [dbo].[FactAPCResus] (
    [EncounterRecno]             INT          NOT NULL,
    [AdmissionDate]              DATE         NULL,
    [EventDate]                  DATE         NULL,
    [LocationCode]               VARCHAR (50) NULL,
    [ROSCTimeOfDayCode]          INT          NULL,
    [DeathTimeOfDayCode]         INT          NULL,
    [CollapseTimeOfDayCode]      INT          NULL,
    [CPRCalloutTimeOfDayCode]    INT          NULL,
    [CPRArriveTimeOfDayCode]     INT          NULL,
    [ArrestConfirmTimeOfDayCode] INT          NULL,
    [CPRStartTimeOfDayCode]      INT          NULL,
    [AwakeningTimeOfDayCode]     INT          NULL,
    [AwakeningDate]              DATETIME     NULL,
    [DischargeDate]              DATE         NULL,
    [DateOfDeath]                DATE         NULL,
    [CallOutTypeID]              INT          NULL,
    [PotentiallyAvoidable]       BIT          NULL,
    [AgeID]                      INT          NULL,
    [ConsultantCode]             VARCHAR (10) NULL,
    [PrimaryDiagnosisCode]       VARCHAR (5)  NULL,
    [PrimaryOperationCode]       VARCHAR (5)  NULL,
    [AdmissionMethodCode]        VARCHAR (2)  NULL,
    [DischargeMethodCode]        VARCHAR (2)  NULL,
    [EthnicOriginCode]           VARCHAR (4)  NULL,
    [SexCode]                    VARCHAR (1)  NOT NULL,
    [InterfaceCode]              VARCHAR (10) NULL,
    [Cases]                      INT          NULL
);


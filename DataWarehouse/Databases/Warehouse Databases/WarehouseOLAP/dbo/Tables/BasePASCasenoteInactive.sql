﻿CREATE TABLE [dbo].[BasePASCasenoteInactive] (
    [SourcePatientNo]          INT           NULL,
    [Surname]                  NVARCHAR (24) NULL,
    [Forenames]                NVARCHAR (20) NULL,
    [DateOfBirth]              DATE          NULL,
    [DateOfDeath]              DATE          NULL,
    [DistrictNo]               NVARCHAR (14) NULL,
    [ModifiedDate]             DATE          NULL,
    [ModifiedTime]             SMALLDATETIME NULL,
    [CasenoteNo]               VARCHAR (14)  NOT NULL,
    [AllocatedDate]            DATE          NULL,
    [CasenoteLocationCode]     VARCHAR (4)   NULL,
    [CasenoteLocationDate]     DATE          NULL,
    [CasenoteStatus]           VARCHAR (10)  NULL,
    [WithdrawnDate]            DATE          NULL,
    [Comment]                  VARCHAR (50)  NULL,
    [CurrentBorrowerCode]      NVARCHAR (6)  NULL,
    [HasDuplicateRegistration] BIT           NULL,
    [Deceased]                 BIT           NULL,
    [CasenoteTypeCode]         VARCHAR (10)  NULL,
    [IsBorrowed]               BIT           NULL
);


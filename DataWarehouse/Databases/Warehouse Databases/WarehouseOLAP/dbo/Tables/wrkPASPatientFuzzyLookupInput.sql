﻿CREATE TABLE [dbo].[wrkPASPatientFuzzyLookupInput] (
    [DistrictNumber]        NVARCHAR (14) NULL,
    [Forenames]             NVARCHAR (20) NULL,
    [GpCode]                NVARCHAR (8)  NULL,
    [InternalDateOfBirth]   NVARCHAR (8)  NULL,
    [InternalPatientNumber] NVARCHAR (9)  NOT NULL,
    [PtAddrLine1]           NVARCHAR (20) NULL,
    [PtAddrPostCode]        NVARCHAR (10) NULL,
    [PtDobDayInt]           NVARCHAR (2)  NULL,
    [PtDobMonthInt]         NVARCHAR (2)  NULL,
    [PtDobYearInt]          NVARCHAR (4)  NULL,
    [Sex]                   NVARCHAR (1)  NULL,
    [Surname]               NVARCHAR (24) NULL
);


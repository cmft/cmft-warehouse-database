﻿CREATE TABLE [dbo].[WaitStatusBase] (
    [WaitStatusCode]       VARCHAR (50)  NOT NULL,
    [WaitStatus]           VARCHAR (255) NOT NULL,
    [ParentWaitStatusCode] VARCHAR (50)  NULL,
    CONSTRAINT [PK_WaitStatusBase] PRIMARY KEY CLUSTERED ([WaitStatusCode] ASC),
    CONSTRAINT [FK_WaitStatusBase_WaitStatusBase] FOREIGN KEY ([ParentWaitStatusCode]) REFERENCES [dbo].[WaitStatusBase] ([WaitStatusCode])
);


﻿CREATE TABLE [dbo].[MetricCOMEncounterBase] (
    [MetricCode]       VARCHAR (10) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentCode] VARCHAR (10) NULL
);


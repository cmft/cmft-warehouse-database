﻿CREATE TABLE [dbo].[ObservationAgeBase] (
    [AgeID]   INT          NOT NULL,
    [AgeCode] INT          NULL,
    [AgeBand] VARCHAR (20) NULL,
    CONSTRAINT [PK__Observat__875454C27544E847] PRIMARY KEY CLUSTERED ([AgeID] ASC)
);


﻿CREATE TABLE [dbo].[EntityLookup] (
    [EntityTypeCode] VARCHAR (50)  NOT NULL,
    [EntityCode]     VARCHAR (50)  NOT NULL,
    [Description]    VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_EntityLookup] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC, [EntityCode] ASC)
);


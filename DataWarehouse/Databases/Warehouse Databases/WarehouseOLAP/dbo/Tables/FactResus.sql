﻿CREATE TABLE [dbo].[FactResus] (
    [EncounterRecno]             INT          NOT NULL,
    [AdmissionDate]              DATE         NULL,
    [EventDate]                  DATE         NULL,
    [LocationCode]               VARCHAR (50) NULL,
    [ROSCTimeOfDayCode]          INT          NULL,
    [DeathTimeOfDayCode]         INT          NULL,
    [CollapseTimeOfDayCode]      INT          NULL,
    [CPRCalloutTimeOfDayCode]    INT          NULL,
    [CPRArriveTimeOfDayCode]     INT          NULL,
    [ArrestConfirmTimeOfDayCode] INT          NULL,
    [CPRStartTimeOfDayCode]      INT          NULL,
    [AwakeningTimeOfDayCode]     INT          NULL,
    [AwakeningDate]              DATE         NULL,
    [DischargeDate]              DATE         NULL,
    [DateOfDeath]                DATE         NULL,
    [CallOutTypeID]              INT          NULL,
    [PotentiallyAvoidable]       BIT          NULL,
    [AgeID]                      INT          NULL,
    [InterfaceCode]              VARCHAR (10) NULL,
    [Cases]                      INT          NULL
);


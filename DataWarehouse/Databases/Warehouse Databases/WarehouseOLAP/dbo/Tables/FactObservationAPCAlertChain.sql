﻿CREATE TABLE [dbo].[FactObservationAPCAlertChain] (
    [ObservationSetRecno]     INT          NOT NULL,
    [SpecialtyID]             INT          NULL,
    [WardID]                  INT          NULL,
    [StartDate]               DATE         NULL,
    [StartTimeOfDayCode]      INT          NULL,
    [OutOfHours]              BIT          NULL,
    [OverallRiskIndexCode]    INT          NULL,
    [OverallAssessedStatusID] INT          NULL,
    [DurationID]              INT          NULL,
    [AgeID]                   INT          NULL,
    [ConsultantCode]          VARCHAR (10) NULL,
    [PrimaryDiagnosisCode]    VARCHAR (10) NULL,
    [PrimaryOperationCode]    VARCHAR (10) NULL,
    [AdmissionMethodCode]     VARCHAR (2)  NULL,
    [DischargeMethodCode]     VARCHAR (2)  NULL,
    [EthnicOriginCode]        VARCHAR (2)  NULL,
    [SexCode]                 VARCHAR (2)  NULL,
    [ContextCode]             VARCHAR (20) NULL,
    [Cases]                   INT          NOT NULL,
    [Duration]                INT          NULL
);


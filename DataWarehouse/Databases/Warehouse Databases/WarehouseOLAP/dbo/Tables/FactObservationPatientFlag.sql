﻿CREATE TABLE [dbo].[FactObservationPatientFlag] (
    [PatientFlagRecno]        INT          NOT NULL,
    [SourceUniqueID]          INT          NOT NULL,
    [CasenoteNumber]          VARCHAR (20) NULL,
    [DateOfBirth]             DATE         NULL,
    [PatientFlagID]           INT          NULL,
    [StartDate]               DATE         NULL,
    [StartTime]               DATETIME     NOT NULL,
    [StartTimeOfDayCode]      INT          NULL,
    [EndDate]                 DATE         NULL,
    [EndTime]                 DATETIME     NULL,
    [EndTimeOfDayCode]        INT          NULL,
    [CreatedByUserID]         INT          NOT NULL,
    [EWSDisabled]             BIT          NOT NULL,
    [DurationID]              INT          NULL,
    [OutOfHours]              INT          NOT NULL,
    [AgeID]                   INT          NULL,
    [ContextCode]             VARCHAR (11) NOT NULL,
    [Cases]                   INT          NOT NULL,
    [PatientFlagDurationDays] INT          NULL
);


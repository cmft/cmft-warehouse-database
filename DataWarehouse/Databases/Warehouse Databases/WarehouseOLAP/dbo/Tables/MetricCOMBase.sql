﻿CREATE TABLE [dbo].[MetricCOMBase] (
    [MetricCode]       VARCHAR (10) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_MetricCOMEncounterBase] PRIMARY KEY CLUSTERED ([MetricCode] ASC),
    CONSTRAINT [FK_MetricCOMEncounterBase_MetricOPBase] FOREIGN KEY ([MetricParentCode]) REFERENCES [dbo].[MetricCOMBase] ([MetricCode])
);


﻿CREATE TABLE [dbo].[ObservationPatientFlagDurationBase] (
    [DurationID]   INT          NOT NULL,
    [DayBand]      INT          NULL,
    [WeekBand]     VARCHAR (69) NULL,
    [WeekBandKey]  INT          NULL,
    [MonthBand]    VARCHAR (70) NULL,
    [MonthBandKey] INT          NULL,
    [YearBand]     VARCHAR (69) NULL,
    [YearBandKey]  INT          NULL,
    CONSTRAINT [PK__Observat__AF77E8166D6EBC55] PRIMARY KEY CLUSTERED ([DurationID] ASC)
);


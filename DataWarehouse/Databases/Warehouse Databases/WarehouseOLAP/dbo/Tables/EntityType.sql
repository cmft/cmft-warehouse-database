﻿CREATE TABLE [dbo].[EntityType] (
    [EntityTypeCode] VARCHAR (50) NOT NULL,
    [EntityType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_EntityType] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC)
);


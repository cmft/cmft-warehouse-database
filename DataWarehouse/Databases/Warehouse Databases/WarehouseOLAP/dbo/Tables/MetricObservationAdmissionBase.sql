﻿CREATE TABLE [dbo].[MetricObservationAdmissionBase] (
    [MetricCode] VARCHAR (10) NOT NULL,
    [Metric]     VARCHAR (50) NULL,
    CONSTRAINT [PK__MetricOb__8F1381DD34363EF9] PRIMARY KEY CLUSTERED ([MetricCode] ASC)
);


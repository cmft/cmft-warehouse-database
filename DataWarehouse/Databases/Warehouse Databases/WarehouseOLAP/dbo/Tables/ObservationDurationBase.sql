﻿CREATE TABLE [dbo].[ObservationDurationBase] (
    [DurationID]             INT          NOT NULL,
    [MinuteBand]             INT          NULL,
    [AlertDurationBand]      VARCHAR (20) NULL,
    [AlertChainDurationBand] VARCHAR (20) NULL,
    CONSTRAINT [PK__APCObser__7F7A47822E7C9A4C] PRIMARY KEY CLUSTERED ([DurationID] ASC)
);


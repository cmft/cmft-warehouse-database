﻿CREATE TABLE [dbo].[FactRADWaitingList] (
    [EncounterRecno]         INT           NOT NULL,
    [CensusDate]             SMALLDATETIME NULL,
    [SiteCode]               VARCHAR (50)  NULL,
    [ExamCode]               VARCHAR (50)  NULL,
    [StatusCode]             VARCHAR (50)  NULL,
    [UrgencyCode]            VARCHAR (20)  NULL,
    [WaitCategoryCode]       VARCHAR (1)   NOT NULL,
    [ReferralSourceCode]     VARCHAR (20)  NULL,
    [ReferrerCode]           VARCHAR (20)  NULL,
    [ReferralLocationCode]   VARCHAR (20)  NULL,
    [SpecialtyCode]          VARCHAR (20)  NULL,
    [RegisteredPracticeCode] VARCHAR (20)  NULL,
    [RegisteredPCTCode]      VARCHAR (20)  NULL,
    [SexCode]                VARCHAR (5)   NULL,
    [AgeCode]                VARCHAR (27)  NULL,
    [DurationCode]           VARCHAR (2)   NULL,
    [AdjustedDurationCode]   VARCHAR (2)   NULL,
    [RADWLCases]             INT           NOT NULL,
    [WithAppointment]        INT           NOT NULL,
    [DaysWaiting]            INT           NULL,
    [DaysWaitingAdjusted]    INT           NULL
);


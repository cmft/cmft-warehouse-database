﻿CREATE TABLE [dbo].[FactObservationAlertChain] (
    [ObservationSetRecno]     INT          NOT NULL,
    [SpecialtyID]             INT          NULL,
    [WardID]                  INT          NULL,
    [StartDate]               DATE         NULL,
    [StartTimeOfDayCode]      INT          NULL,
    [OutOfHours]              BIT          NULL,
    [OverallRiskIndexCode]    INT          NULL,
    [OverallAssessedStatusID] INT          NULL,
    [DurationID]              INT          NULL,
    [AgeID]                   INT          NULL,
    [ContextCode]             VARCHAR (20) NULL,
    [Cases]                   INT          NOT NULL,
    [Duration]                INT          NULL
);


﻿CREATE TABLE [dbo].[FactRF] (
    [EncounterRecno]       INT           NOT NULL,
    [EncounterDate]        SMALLDATETIME NULL,
    [ConsultantCode]       VARCHAR (10)  NULL,
    [SpecialtyCode]        VARCHAR (10)  NULL,
    [PracticeCode]         VARCHAR (8)   NULL,
    [PCTCode]              VARCHAR (10)  NULL,
    [SiteCode]             VARCHAR (10)  NULL,
    [AgeCode]              VARCHAR (50)  NULL,
    [SourceOfReferralCode] VARCHAR (10)  NULL,
    [Cases]                INT           NOT NULL,
    [SexCode]              VARCHAR (1)   NULL,
    [DirectorateCode]      VARCHAR (5)   NULL,
    CONSTRAINT [PK_FactRF] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


﻿CREATE TABLE [dbo].[OlapCalendar] (
    [TheDate]             DATE          NOT NULL,
    [DayOfWeek]           NVARCHAR (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DayOfWeekKey]        INT           NULL,
    [LongDate]            VARCHAR (11)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [TheMonth]            NVARCHAR (34) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [FinancialQuarter]    NVARCHAR (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [FinancialYear]       NVARCHAR (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [FinancialMonthKey]   INT           NULL,
    [FinancialQuarterKey] INT           NULL,
    [CalendarQuarter]     VARCHAR (34)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CalendarSemester]    VARCHAR (12)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [CalendarYear]        NVARCHAR (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [LastCompleteMonth]   INT           NOT NULL,
    [WeekNoKey]           VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [WeekNo]              VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [FirstDayOfWeek]      SMALLDATETIME NULL,
    [LastDayOfWeek]       SMALLDATETIME NULL,
    CONSTRAINT [PK_OlapCalendar] PRIMARY KEY CLUSTERED ([TheDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_OlapCalendar_FinMonthKey]
    ON [dbo].[OlapCalendar]([FinancialMonthKey] ASC);


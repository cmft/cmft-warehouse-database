﻿CREATE TABLE [dbo].[BasePatientExperienceResponse] (
    [ResponseRecno]    INT          NOT NULL,
    [SurveyTakenID]    INT          NOT NULL,
    [SurveyID]         INT          NOT NULL,
    [QuestionPosition] INT          NULL,
    [QuestionID]       INT          NOT NULL,
    [AnswerID]         INT          NOT NULL,
    [CensusTime]       DATETIME     NOT NULL,
    [LocationID]       INT          NOT NULL,
    [DeviceNumber]     INT          NULL,
    [Cases]            INT          NULL,
    [Created]          DATETIME     NULL,
    [ByWhom]           VARCHAR (50) NULL
);


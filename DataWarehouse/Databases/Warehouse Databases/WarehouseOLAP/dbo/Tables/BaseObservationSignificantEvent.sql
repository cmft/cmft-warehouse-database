﻿CREATE TABLE [dbo].[BaseObservationSignificantEvent] (
    [SignificantEventRecno]   INT           NOT NULL,
    [SourceUniqueID]          INT           NOT NULL,
    [CasenoteNumber]          VARCHAR (20)  NULL,
    [DateOfBirth]             DATE          NULL,
    [SpecialtyID]             INT           NULL,
    [WardID]                  INT           NULL,
    [AdmissionSourceUniqueID] INT           NOT NULL,
    [SignificantEventTypeID]  INT           NOT NULL,
    [StartDate]               DATE          NULL,
    [StartTimeOfDayCode]      INT           NOT NULL,
    [EndDate]                 DATE          NULL,
    [EndTimeOfDayCode]        INT           NULL,
    [Comment]                 VARCHAR (MAX) NULL,
    [CreatedByUserID]         INT           NOT NULL,
    [CreatedTime]             DATETIME      NOT NULL,
    [LastModifiedByUserID]    INT           NOT NULL,
    [LastModifiedTime]        DATETIME      NOT NULL,
    [ContextCode]             VARCHAR (11)  NOT NULL,
    [OutOfHours]              BIT           NULL,
    [AgeID]                   INT           NULL,
    [Cases]                   INT           NULL
);


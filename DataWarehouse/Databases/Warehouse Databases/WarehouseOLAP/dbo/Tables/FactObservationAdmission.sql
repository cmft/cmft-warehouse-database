﻿CREATE TABLE [dbo].[FactObservationAdmission] (
    [MetricCode]             VARCHAR (3)  NOT NULL,
    [AdmissionRecno]         INT          NOT NULL,
    [SpecialtyID]            INT          NULL,
    [WardID]                 INT          NULL,
    [TreatmentOutcomeID]     INT          NOT NULL,
    [DischargeDestinationID] INT          NOT NULL,
    [Date]                   DATE         NULL,
    [TimeOfDayCode]          INT          NULL,
    [LengthOfStay]           INT          NULL,
    [OutOfHours]             BIT          NOT NULL,
    [AgeID]                  INT          NULL,
    [SexID]                  INT          NOT NULL,
    [ContextCode]            VARCHAR (11) NOT NULL,
    [Cases]                  INT          NOT NULL
);


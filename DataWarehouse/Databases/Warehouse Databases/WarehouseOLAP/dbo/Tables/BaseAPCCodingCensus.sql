﻿CREATE TABLE [dbo].[BaseAPCCodingCensus] (
    [CensusDate]                      DATE          NOT NULL,
    [EncounterRecNo]                  INT           NOT NULL,
    [SourceUniqueID]                  VARCHAR (50)  NOT NULL,
    [SourcePatientNo]                 VARCHAR (20)  NULL,
    [ProviderSpellNo]                 VARCHAR (50)  NULL,
    [SourceSpellNo]                   VARCHAR (20)  NOT NULL,
    [SourceEncounterNo]               VARCHAR (20)  NOT NULL,
    [AdmissionTime]                   SMALLDATETIME NULL,
    [EpisodeStartTime]                SMALLDATETIME NULL,
    [EpisodeEndTime]                  SMALLDATETIME NULL,
    [DischargeTime]                   SMALLDATETIME NULL,
    [ClinicalCodingCompleteTime]      SMALLDATETIME NULL,
    [CodingLagDays]                   INT           NULL,
    [SpecialtyCode]                   VARCHAR (5)   NULL,
    [EndDirectorateCode]              VARCHAR (5)   NULL,
    [EndWardTypeCode]                 VARCHAR (10)  NULL,
    [PatientCategoryCode]             VARCHAR (5)   NULL,
    [SpellClinicalCodingCompleteTime] SMALLDATETIME NULL
);


﻿CREATE TABLE [dbo].[MetricObservationBase] (
    [MetricCode]       VARCHAR (10) NOT NULL,
    [Metric]           VARCHAR (50) NULL,
    [MetricParentCode] VARCHAR (10) NULL,
    CONSTRAINT [PK__MetricPa__8F1381DD2977EE0D] PRIMARY KEY CLUSTERED ([MetricCode] ASC),
    CONSTRAINT [FK__MetricPat__Metri__2B60367F] FOREIGN KEY ([MetricParentCode]) REFERENCES [dbo].[MetricObservationBase] ([MetricCode])
);


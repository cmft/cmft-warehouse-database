﻿CREATE TABLE [dbo].[FactCOMCaseLoad] (
    [CaseLoadID]                    NUMERIC (10) NULL,
    [ReferralID]                    NUMERIC (10) NULL,
    [RoleTypeID]                    NUMERIC (10) NULL,
    [StatusID]                      NUMERIC (10) NULL,
    [AllocationTime]                DATETIME     NULL,
    [AllocationReasonID]            NUMERIC (10) NULL,
    [InterventionlevelID]           NUMERIC (10) NULL,
    [CaseloadDate]                  DATETIME     NULL,
    [MetricCode]                    VARCHAR (10) NULL,
    [Outcome]                       NUMERIC (10) NULL,
    [AllocationProfessionalCarerID] NUMERIC (10) NULL,
    [AllocationSpecialtyID]         NUMERIC (10) NULL,
    [AllocationStaffTeamID]         NUMERIC (10) NULL,
    [ResponsibleHealthOrganisation] NUMERIC (10) NULL,
    [DischargeReasonID]             NUMERIC (10) NULL,
    [PatientSourceID]               NUMERIC (10) NULL,
    [Cases]                         INT          NULL,
    [AgeCode]                       VARCHAR (50) NULL,
    [EthnicCategoryID]              NUMERIC (10) NULL,
    [PatientSexID]                  NUMERIC (10) NULL
);


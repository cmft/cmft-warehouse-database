﻿CREATE TABLE [dbo].[FactBedOccupancy] (
    [MetricCode]            VARCHAR (20)  NOT NULL,
    [WardStayRecno]         INT           NOT NULL,
    [APCEncounterRecno]     INT           NOT NULL,
    [WardCode]              VARCHAR (10)  NOT NULL,
    [ConsultantCode]        VARCHAR (10)  NOT NULL,
    [TreatmentFunctionCode] VARCHAR (5)   NOT NULL,
    [SiteCode]              VARCHAR (5)   NOT NULL,
    [DirectorateCode]       VARCHAR (5)   NOT NULL,
    [BedOccupancyDate]      SMALLDATETIME NOT NULL,
    [Cases]                 INT           NOT NULL,
    [Duration]              INT           NULL,
    [KH03SpecialtyCode]     VARCHAR (5)   NULL
);


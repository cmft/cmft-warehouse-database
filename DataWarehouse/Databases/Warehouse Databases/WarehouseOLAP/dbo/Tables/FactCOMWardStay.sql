﻿CREATE TABLE [dbo].[FactCOMWardStay] (
    [EncounterRecno]          INT          NOT NULL,
    [MetricCode]              VARCHAR (4)  NOT NULL,
    [EncounterDate]           DATE         NULL,
    [EncounterTimeOfDay]      INT          NULL,
    [EncounterLengthOfStay]   INT          NULL,
    [ProfessionalCarerID]     NUMERIC (10) NOT NULL,
    [SpecialtyID]             NUMERIC (10) NOT NULL,
    [PracticeCode]            VARCHAR (25) NULL,
    [PatientSexID]            NUMERIC (10) NOT NULL,
    [PatientEthnicGroupID]    NUMERIC (10) NOT NULL,
    [ServicePointID]          NUMERIC (10) NULL,
    [AdmissionMethodID]       NUMERIC (10) NULL,
    [AdmissionSourceID]       NUMERIC (10) NULL,
    [PatientClassificationID] NUMERIC (10) NULL,
    [AdminCategoryID]         NUMERIC (10) NULL,
    [IntendedManagementID]    NUMERIC (10) NULL,
    [ReasonForAdmissionID]    NUMERIC (10) NULL,
    [DischargeDestinationID]  NUMERIC (10) NULL,
    [DischargeMethodID]       NUMERIC (10) NULL,
    [AgeCode]                 VARCHAR (27) NULL,
    [Cases]                   INT          NOT NULL
);


﻿
create function [dbo].[f_AgeCode] (
	 @DateOfBirth datetime
	,@EncounterDate datetime
)
returns varchar(10)
as
begin
	declare @AgeCode varchar(10)

	set
		@AgeCode =
			case
			when datediff(day, @DateOfBirth, @EncounterDate) is null then 'Age Unknown'
			when datediff(day, @DateOfBirth, @EncounterDate) <= 0 then '00 Days'
			when datediff(day, @DateOfBirth, @EncounterDate) = 1 then '01 Day'
			when datediff(day, @DateOfBirth, @EncounterDate) > 1 and
			datediff(day, @DateOfBirth, @EncounterDate) <= 28 then
				right('0' + Convert(Varchar,datediff(day, @DateOfBirth, @EncounterDate)), 2) + ' Days'
		
			when datediff(day, @DateOfBirth, @EncounterDate) > 28 and
			datediff(month, @DateOfBirth, @EncounterDate) -
			case  when datepart(day, @DateOfBirth) > datepart(day, @EncounterDate) then 1 else 0 end
				 < 1 then 'Between 28 Days and 1 Month'
			when datediff(month, @DateOfBirth, @EncounterDate) -
			case  when datepart(day, @DateOfBirth) > datepart(day, @EncounterDate) then 1 else 0 end = 1
				then '01 Month'
			when datediff(month, @DateOfBirth, @EncounterDate) -
			case  when datepart(day, @DateOfBirth) > datepart(day, @EncounterDate) then 1 else 0 end > 1 and
			 datediff(month, @DateOfBirth, @EncounterDate) -
			case  when datepart(day, @DateOfBirth) > datepart(day, @EncounterDate) then 1 else 0 end <= 23
			then
			right('0' + Convert(varchar,datediff(month, @DateOfBirth,@EncounterDate) -
			case  when datepart(day, @DateOfBirth) > datepart(day, @EncounterDate) then 1 else 0 end), 2) + ' Months'
			when datediff(yy, @DateOfBirth, @EncounterDate) - 
			(
			case 
			when	(datepart(m, @DateOfBirth) > datepart(m, @EncounterDate)) 
			or
				(
					datepart(m, @DateOfBirth) = datepart(m, @EncounterDate) 
				And	datepart(d, @DateOfBirth) > datepart(d, @EncounterDate)
				) then 1 else 0 end
			) > 99 then '99+'
			else right('0' + convert(varchar, datediff(yy, @DateOfBirth, @EncounterDate) - 
			(
			case 
			when	(datepart(m, @DateOfBirth) > datepart(m, @EncounterDate)) 
			or
				(
					datepart(m, @DateOfBirth) = datepart(m, @EncounterDate) 
				And	datepart(d, @DateOfBirth) > datepart(d, @EncounterDate)
				) then 1 else 0 end
			)), 2) + ' Years'

			end

	return(@AgeCode)
end


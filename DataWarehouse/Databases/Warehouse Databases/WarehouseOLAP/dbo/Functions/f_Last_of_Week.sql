﻿

CREATE function [dbo].[f_Last_of_Week] (
	@date datetime
)
returns datetime
as
begin
	if @date is null return null;
	
	return(dbo.f_Next_Day_of_the_Week('Sunday', @date))
end


﻿
CREATE function [dbo].[f_get_patient_category] (
	@AdmissionMethodCode varchar(2)
	,@ManagementIntention varchar(2)
	,@AdmissionDate date
	,@DischargeDate date
)
returns varchar(2)
as
begin
	DECLARE @patcat varchar(2) =	case
			
			--Elective Inpatient
				when @AdmissionMethodCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and @ManagementIntention in 
					(
					 '1' --INPATIENT
					,'3' --INTERVAL ADMISSION
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective


			--Elective Inpatients where intended management was daycase but patient stayed overnight
				when @AdmissionMethodCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and @ManagementIntention IN ('2','4','5') -- Regular day and night '2' -- DAY CASE
				and	@AdmissionDate < @DischargeDate
				then 'EL'--Elective


			--Elective Daycase
				when @AdmissionMethodCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and @ManagementIntention= '2'-- DAY CASE
				and	@AdmissionDate = @DischargeDate
				then 'DC'--Daycase
				
			
			--Elective Inaptient where intended management was daycase or regular but not discharged
				when @AdmissionMethodCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and @ManagementIntention IN ('2','4','5')-- DAY CASE, Regular day and night
				and	@DischargeDate IS NULL
				then 'EL'--Elective
	
	
			--Regular Day Case
				when @AdmissionMethodCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and @ManagementIntention IN ('4','5')-- Regular day and night
				and	@AdmissionDate = @DischargeDate
				then 'RD'--Regular
				
			--Non Elective
				else 'NE' --Non Elective
			
			end

	return(@patcat)
end
﻿
create function dbo.f_Last_Day_of_the_Week (
	@day varchar(9),
	@date datetime
)
returns datetime
as
begin
	if @date is null return null;
	
	declare @day_of_the_week datetime
	set @day_of_the_week = dateadd(day, -7, common.f_Next_Day_of_the_Week(@day , @date))
	return(@day_of_the_week)
end

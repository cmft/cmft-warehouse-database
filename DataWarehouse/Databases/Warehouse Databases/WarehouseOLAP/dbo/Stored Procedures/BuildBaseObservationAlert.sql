﻿
--USE WarehouseOLAP
--GO

--/****** Object:  StoredProcedure dbo.BuildBaseObservationAlert    Script Date: 29/08/2012 12:46:46 ******/

CREATE proc [dbo].[BuildBaseObservationAlert] 

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

set dateformat dmy

truncate table dbo.BaseObservationAlert

insert into dbo.BaseObservationAlert

(		
	AlertRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,WardID
	,AdmissionSourceUniqueID
	,TypeID
	,ReasonID
	,ObservationSetSourceUniqueID
	,CreatedDate
	,CreatedTime
	,CreatedTimeOfDayCode
	,BedsideDueDate
	,BedsideDueTime
	,BedsideDueTimeOfDayCode
	,EscalationDate
	,EscalationTime
	,EscalationTimeOfDayCode
	,AcceptedDate
	,AcceptedTime
	,AcceptedTimeOfDayCode
	,AttendedByUserID
	,AttendanceTypeID
	,Comment
	,CurrentSeverityID
	,CurrentClinicianSeniorityID
	,AcceptanceRemindersRemaining
	,ChainSequenceNumber
	,NextReminderTime
	,ClosedDate
	,ClosedTime
	,ClosedTimeOfDayCode
	,ClosureReasonID
	,ClosedByUserID
	,CancelledByObservationSetSourceUniqueID
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,AlertDurationMinutes
)

select
	AlertRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID = 
			coalesce(
				SpecialtyID
				,-1
			)
	,WardID = 
			coalesce(
				WardID
				,-1
			)
	,AdmissionSourceUniqueID
	,TypeID	= 
			coalesce(
				TypeID
				,-1
			)
	,ReasonID = 
			coalesce(
				ReasonID
				, -1
			)
	,ObservationSetSourceUniqueID
	,CreatedDate
	,CreatedTime
	,CreatedTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, CreatedTime), 0)
							,CreatedTime
						)
						,-1
					)
	,BedsideDueDate
	,BedsideDueTime
	,BedsideDueTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, BedsideDueTime), 0)
							,BedsideDueTime
						)
						,-1
					)
	,EscalationDate
	,EscalationTime
	,EscalationTimeOfDayCode =
					coalesce(
						datediff(
							 minute
							,dateadd(day, datediff(day, 0, EscalationTime), 0)
							,EscalationTime
						)
						,-1
					)
	,AcceptedDate
	,AcceptedTime
	,AcceptedTimeOfDayCode =
					coalesce(
						datediff(
							 minute
							,dateadd(day, datediff(day, 0, AcceptedTime), 0)
							,AcceptedTime
						)
						,-1
					)
	,AttendedByUserID = 
				coalesce(
					AttendedByUserID
					,-1
				)
	,AttendanceTypeID = 
				coalesce(
					AttendanceTypeID
					,-1
				)
	,Comment
	,CurrentSeverityID = 
				coalesce(
					CurrentSeverityID
					,-1
				)
	,CurrentClinicianSeniorityID = 
				coalesce(
					CurrentClinicianSeniorityID
					,-1
				)
	,AcceptanceRemindersRemaining
	,ChainSequenceNumber
	,NextReminderTime
	,ClosedDate
	,ClosedTime
	,ClosedTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, ClosedTime), 0)
							,ClosedTime
						)
						,-1
					)
	,ClosureReasonID = 
				coalesce(
					ClosureReasonID
					,-1
				)
	,ClosedByUserID = 
				coalesce(
					ClosedByUserID
					,-1
				)
	,CancelledByObservationSetSourceUniqueID
	,OutOfHours = 
				case
				when datename(dw, CreatedTime) in ('Saturday', 'Sunday') then 1
				when cast(CreatedTime as time) < '8:00' then 1
				when cast(CreatedTime as time) > '17:00' then 1
				when exists
					(
					select
						1
					from
						Warehouse.WH.Holiday
					where
						CreatedDate = Holiday.HolidayDate
					)
				then 1
				else 0
				end
	--,AgeID = CMFT.Dates.GetAge(DateOfBirth, CreatedTime)
	,AgeID = 
			-- Ripped from the Dates.GetAge function in CMFT for speed
			case
			when DateOfBirth > CreatedTime
			then 0
			when (month(CreatedTime) * 100) + day(CreatedTime) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
			then datediff(year, DateOfBirth, CreatedTime)
			else datediff(year, DateOfBirth, CreatedTime) - 1
			end

	--case
	--when datediff(day, DateOfBirth, CreatedTime) is null then 'Age Unknown'
	--when datediff(day, DateOfBirth, CreatedTime) <= 0 then '00 Days'
	--when datediff(day, DateOfBirth, CreatedTime) = 1 then '01 Day'
	--when datediff(day, DateOfBirth, CreatedTime) > 1 and
	--datediff(day, DateOfBirth,CreatedTime) <= 28 then
	--    right('0' + Convert(Varchar,datediff(day, DateOfBirth, CreatedTime)), 2) + ' Days'
	
	--when datediff(day, DateOfBirth, CreatedTime) > 28 and
	--datediff(month, DateOfBirth,CreatedTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day,CreatedTime) then 1 else 0 end
	--     < 1 then 'Between 28 Days and 1 Month'
	--when datediff(month, DateOfBirth, CreatedTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day, CreatedTime) then 1 else 0 end = 1
	--    then '01 Month'
	--when datediff(month, DateOfBirth, CreatedTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day, CreatedTime) then 1 else 0 end > 1 and
	-- datediff(month, DateOfBirth, CreatedTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day, CreatedTime) then 1 else 0 end <= 23
	--then
	--right('0' + Convert(varchar,datediff(month, DateOfBirth,CreatedTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day, CreatedTime) then 1 else 0 end), 2) + ' Months'
	--when datediff(yy, DateOfBirth, CreatedTime) - 
	--(
	--case 
	--when	(datepart(m, DateOfBirth) > datepart(m, CreatedTime)) 
	--or
	--	(
	--		datepart(m, DateOfBirth) = datepart(m, CreatedTime) 
	--	And	datepart(d, DateOfBirth) > datepart(d, CreatedTime)
	--	) then 1 else 0 end
	--) > 99 then '99+'
	--else right('0' + convert(varchar, datediff(yy, DateOfBirth, CreatedTime) - 
	--(
	--case 
	--when	(datepart(m, DateOfBirth) > datepart(m, CreatedTime)) 
	--or
	--	(
	--		datepart(m, DateOfBirth) = datepart(m, CreatedTime) 
	--	And	datepart(d, DateOfBirth) > datepart(d, CreatedTime)
	--	) then 1 else 0 end
	--)), 2) + ' Years'

	--end
	,ContextCode
	,Cases = 1
	,AlertDurationMinutes = datediff(minute,CreatedTime, coalesce(ClosedTime, getdate()))
from
	Warehouse.Observation.Alert



select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	














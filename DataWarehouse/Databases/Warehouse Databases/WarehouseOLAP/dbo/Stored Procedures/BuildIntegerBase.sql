﻿
create procedure [dbo].[BuildIntegerBase]
(
     @FromInteger int
	,@ToInteger int
)
as

declare @CurrentInteger int

select @CurrentInteger = @FromInteger

while @CurrentInteger <= @ToInteger
   begin
        if not exists (select * from IntegerBase where TheInteger = @CurrentInteger)
            insert into IntegerBase
			(
			TheInteger
			)
			values
			(
			@CurrentInteger
			)

        select @CurrentInteger = @CurrentInteger + 1

    end



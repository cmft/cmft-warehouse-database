﻿


--/****** Object:  StoredProcedure [dbo].[BuildAPCObservationDurationBase]    Script Date: 06/08/2012 16:04:17 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


CREATE proc [dbo].[BuildObservationDurationBase]

as

--declare @minute int
--set @minute = 0


--truncate table WarehouseOLAP.dbo.ObservationDurationBase

----if object_id('WarehouseOLAP.dbo.ObservationDurationBase') is not null
----drop table WarehouseOLAP.dbo.ObservationDurationBase

----create table WarehouseOLAP.dbo.APCObservationDurationBase
----(
----DurationBandCode int primary key
----,MinuteBand int
----,AlertResponseBand varchar(20)
----,AlertChainBand varchar(20)
----)

--while @minute  <= 
--					(
--					select
--						max([DurationMinutes])
--					from
--						BaseObservationAlert
--					)

--begin

----if not exists (select 1 from ObservationDurationBase where DurationBandCode = @minute)
--insert into WarehouseOLAP.dbo.ObservationDurationBase

--	--(
--	--DurationBandCode
--	--,MinuteBand
--	--,AlertDurationBand
--	--,AlertChainDurationBand
--	--)

--	--values
--	--(
--	--@minute
--	--,@minute
--	--,case
--	--	when @minute >= 0 and @minute < 60 then '00:00<00:60'
--	--	when @minute >= 60 and @minute < 90 then '00:60<01:30'
--	--	when @minute >= 90 then '01:30+'
--	--end
--	--,case
--	--	when @minute >= 0 and @minute < 180 then '00:00<03:00'
--	--	when @minute >= 180 and @minute < 300 then '03:00<05:00'
--	--	when @minute >= 300 then '05:00+'
--	--end
--	--)

--select 
--	DurationCode = @minute
--	,MinuteBand = @minute
--	,AlertDurationBand = 
--						case
--							when @minute >= 0 and @minute < 60 then '00:00<00:60'
--							when @minute >= 60 and @minute < 90 then '00:60<01:30'
--							when @minute >= 90 then '01:30+'
--						end
--	,AlertChainDurationBand = 
--						case
--							when @minute >= 0 and @minute < 180 then '00:00<03:00'
--							when @minute >= 180 and @minute < 300 then '03:00<05:00'
--							when @minute >= 300 then '05:00+'
--						end
	
--set @minute = @minute + 1

--end

--insert into WarehouseOLAP.dbo.ObservationDurationBase
--select	
--	-1
--	,-1
--	,'<00:00'
--	,'<00:00'
	
--insert into WarehouseOLAP.dbo.ObservationDurationBase
--select	
--	-2
--	,-2
--	,'N/A'
--	,'N/A'


truncate table dbo.ObservationDurationBase

declare @Minute int
		,@Extent int

set @Minute = 0
set @Extent = 1440;

				--(
				--select
				--	max([DurationMinutes])
				--from
				--	BaseObservationAlert
				--);

with ObservationDuration
as
(
select 
	DurationID = @Minute
union all
select
	DurationID + 1
from
	ObservationDuration
where
	DurationID <= @Extent
)




insert into WarehouseOLAP.dbo.ObservationDurationBase

(
DurationID
,MinuteBand
,AlertDurationBand
,AlertChainDurationBand
)


select 
	DurationID = DurationID
	,MinuteBand = DurationID
	,AlertDurationBand = 
						case
							when DurationID >= 0 and DurationID < 60 then '00:00<01:00'
							when DurationID >= 60 and DurationID < 90 then '01:00<01:30'
							when DurationID >= 90 then '01:30+'
						end
	,AlertChainDurationBand = 
						case
							when DurationID >= 0 and DurationID < 180 then '00:00<03:00'
							when DurationID >= 180 and DurationID < 300 then '03:00<05:00'
							when DurationID >= 300 then '05:00+'
						end
from
	ObservationDuration

option (maxrecursion 0)


insert into WarehouseOLAP.dbo.ObservationDurationBase
(
DurationID
,MinuteBand
,AlertDurationBand
,AlertChainDurationBand
)

select	
	-1
	,-1
	,'<00:00'
	,'<00:00'
	
insert into WarehouseOLAP.dbo.ObservationDurationBase
(
DurationID
,MinuteBand
,AlertDurationBand
,AlertChainDurationBand
)
select	
	-2
	,-2
	,'N/A'
	,'N/A'










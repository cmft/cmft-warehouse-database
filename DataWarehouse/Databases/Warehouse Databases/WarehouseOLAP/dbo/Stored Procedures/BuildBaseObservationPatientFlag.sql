﻿


CREATE proc [dbo].[BuildBaseObservationPatientFlag] 

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

set dateformat dmy

truncate table dbo.BaseObservationPatientFlag

insert into dbo.BaseObservationPatientFlag
(
PatientFlagRecno
,SourceUniqueID
,CasenoteNumber
,DateOfBirth
,PatientFlagID
,StartDate
,StartTime
,StartTimeOfDayCode
,EndDate
,EndTime
,EndTimeOfDayCode
,CreatedByUserID
,EWSDisabled
,OutOfHours
,AgeID
,ContextCode
,Cases
,PatientFlagDurationDays
)

select --top 10 
	PatientFlagRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,PatientFlagID = 
			coalesce(
				PatientFlagID
				,-1
			)
	,StartDate = cast(StartTime as date)
	,StartTime
	,StartTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, StartTime), 0)
							,StartTime
						)
						,-1
					)
	,EndDate = cast(StartTime as date)
	,EndTime
	,EndTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, EndTime), 0)
							,EndTime
						)
						,-1
					)
	,CreatedByUserID = 
			coalesce(
				CreatedByUserID
				,-1
			)
	,EWSDisabled =
			coalesce(
				EWSDisabled
				,0
			) 
	,OutOfHours = 
				case
				when datename(dw, StartTime) in ('Saturday', 'Sunday') then 1
				when cast(StartTime as time) < '8:00' then 1
				when cast(StartTime as time) > '17:00' then 1
				when exists
					(
					select
						1
					from
						Warehouse.WH.Holiday
					where
						cast(StartTime as date) = Holiday.HolidayDate
					)
				then 1
				else 0
				end
	,AgeID = 
			-- Ripped from the Dates.GetAge function in CMFT for speed
			case
			when DateOfBirth > StartTime
			then 0
			when (month(StartTime) * 100) + day(StartTime) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
			then datediff(year, DateOfBirth, StartTime)
			else datediff(year, DateOfBirth, StartTime) - 1
			end
	,ContextCode	
	,Cases = 1
	,PatientFlagDurationDays = datediff(day, StartTime, coalesce(EndTime, getdate()))
from
	Warehouse.Observation.PatientFlag


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	















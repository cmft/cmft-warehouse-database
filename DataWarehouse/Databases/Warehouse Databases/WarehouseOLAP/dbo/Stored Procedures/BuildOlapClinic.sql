﻿
CREATE procedure [dbo].[BuildOlapClinic] as

truncate table dbo.OlapClinic

insert into dbo.OlapClinic
(
	 ClinicCode
	,Comment
	,ConsultantCode
	,SpecialtyCode
	,Clinic
	,SiteCode
	,FunctionCode
	,ProviderCode
	,VirtualClinicFlag
	,POAClinicFlag
)
select
	 ClinicCode
	,Comment
	,ConsultantCode
	,SpecialtyCode
	,Clinic = Clinic + ' (' + ClinicCode + ')'
	,SiteCode
	,FunctionCode
	,ProviderCode
	,VirtualClinicFlag
	,POAClinicFlag
from
	(
	select
		 Clinic.ClinicCode
		,Comment
		,ConsultantCode = coalesce(Clinic.ConsultantCode, 'N/A')
		,SpecialtyCode = coalesce(Clinic.SpecialtyCode, 'N/A')
		,Clinic.Clinic
		,Clinic.SiteCode
		,Clinic.FunctionCode
		,Clinic.ProviderCode
		,Clinic.VirtualClinicFlag
		,Clinic.POAClinicFlag
	from
		Warehouse.PAS.Clinic Clinic

	union all

	select
		 Ward.WardCode + '|W'
		,Comment = null
		,ConsultantCode = 'N/A'
		,SpecialtyCode = 'N/A'
		,Ward.Ward
		,Ward.SiteCode
		,FunctionCode = null
		,ProviderCode = null
		,VirtualWardFlag = 0
		,POAWardFlag = 0
	from
		Warehouse.PAS.Ward Ward
	where
	--	exists
	--	(
	--	select
	--		1
	--	from
	--		dbo.OlapDiaryFact
	--	where
	--		OlapDiaryFact.WardCode = Ward.WardCode
	--	)
	--
	--or
		exists
		(
		select
			1
		from
			dbo.FactOP
		where
			FactOP.ClinicCode = Ward.WardCode + '|W'
		)

	--union all

	--select distinct
	--	 'GUM|' + GUM.SiteCode
	--	,Comment = null
	--	,ConsultantCode = 'N/A'
	--	,SpecialtyCode = 'N/A'
	--	,'GUM ' + OlapPASSite.Site
	--	,GUM.SiteCode
	--	,FunctionCode = null
	--	,ProviderCode = null
	--	,VirtualWardFlag = 0
	--	,POAWardFlag = 0
	--from
	--	RTT.dbo.GUM

	--inner join dbo.OlapPASSite
	--on	OlapPASSite.SiteCode = GUM.SiteCode


	union all

	select distinct
		 ClinicCode = 'N/A'
		,Comment = null
		,ConsultantCode = 'N/A'
		,SpecialtyCode = 'N/A'
		,Clinic = 'N/A'
		,SiteCode = 'N/A'
		,FunctionCode = null
		,ProviderCode = null
		,VirtualWardFlag = 0
		,POAWardFlag = 0

	union all

	select distinct
		 ClinicCode = 'N/A|W'
		,Comment = null
		,ConsultantCode = 'N/A'
		,SpecialtyCode = 'N/A'
		,Clinic = 'N/A Ward'
		,SiteCode = 'N/A'
		,FunctionCode = null
		,ProviderCode = null
		,VirtualWardFlag = 0
		,POAWardFlag = 0


	) Clinic

where
--	exists
--	(
--	select
--		1
--	from
--		dbo.OlapDiaryFact
--	where
--		OlapDiaryFact.ClinicCode = Clinic.ClinicCode
--	)
--
--or
	exists
	(
	select
		1
	from
		dbo.FactOP
	where
		FactOP.ClinicCode = Clinic.ClinicCode
	)


insert into dbo.OlapClinic
(
	 ClinicCode
	,Comment
	,ConsultantCode
	,SpecialtyCode
	,Clinic
	,SiteCode
	,FunctionCode
	,ProviderCode
	,VirtualClinicFlag
	,POAClinicFlag
)
select distinct
	 ClinicCode
	,Comment = null
	,ConsultantCode = 'N/A'
	,SpecialtyCode = 'N/A'
	,Clinic = ClinicCode + ' - No Description'
	,SiteCode = 'N/A'
	,FunctionCode = null
	,ProviderCode = null
	,VirtualWardFlag = 0
	,POAWardFlag = 0
from
	FactOP
where
	not exists
	(
	select
		1
	from
		OlapClinic
	where
		OlapClinic.ClinicCode = FactOP.ClinicCode
	)
and	ClinicCode <> 'N/A'


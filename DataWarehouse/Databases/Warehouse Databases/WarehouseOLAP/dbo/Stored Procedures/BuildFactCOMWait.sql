﻿
CREATE procedure [dbo].[BuildFactCOMWait] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table dbo.FactCOMWait

insert into dbo.FactCOMWait
(
	 EncounterRecno
	,Cases
	,LengthOfWait
	,AgeCode
	,CensusDate
	,DurationCode
	,WithAppointment
	,BookedBeyondBreach
	,NearBreach
	,RTTActivity
	,RTTBreachStatusCode
	,DurationAtAppointmentDateCode
	,Encounter.WithRTTOpenPathway
	,ReferralReceivedDate
	,ReasonforReferralID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfessionalCarerID
	,SourceofReferralID
	,TypeofReferralID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientEthnicGroupID
	,patientsexid
	,DerivedFirstAttendanceFlag
	,DerivedWaitFlag
	,ScheduleOutcomeCode
	,ScheduleLocationTypeID
	,ScheduleTypeID
	,ScheduleCanceledReasonID
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,ScheduleMoveCount
	,ScheduleSpecialtyID
	,ScheduleStaffTeamID
	,ScheduleProfessionalCarerID
	,ProfessionalCarerCurrentID
	,ProfessionalCarerScheduleID
	,ProfessionalCarerReferralID
)
select
	 Encounter.EncounterRecno
	,Encounter.Cases
	,Encounter.LengthOfWait
	,Encounter.AgeCode
	,Encounter.CensusDate
	,Encounter.DurationCode
	,Encounter.WithAppointment
	,Encounter.BookedBeyondBreach
	,Encounter.NearBreach
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,DurationAtAppointmentDateCode
	,Encounter.WithRTTOpenPathway
	,ReferralReceivedDate
	,ReasonforReferralID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfessionalCarerID
	,SourceofReferralID
	,TypeofReferralID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientEthnicGroupID
	,patientsexid
	,DerivedFirstAttendanceFlag
	,DerivedWaitFlag
	,ScheduleOutcomeCode
	,Encounter.ScheduleLocationTypeID
	,encounter.ScheduleTypeID
	,encounter.ScheduleCanceledReasonID
	,encounter.CountOfDNAs
	,encounter.CountOfHospitalCancels
	,encounter.CountOfPatientCancels
	,encounter.ScheduleMoveCount
	,encounter.ScheduleSpecialtyID
	,encounter.ScheduleStaffTeamID
	,encounter.ScheduleProfessionalCarerID
	,[ProfessionalCarerCurrentID]
	,[ProfessionalCarerScheduleID]
	,[ProfessionalCarerReferralID]
FROM
	dbo.BaseCOMWait Encounter

	

--select @RowsInserted = @@rowcount

--select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

--select @Stats = 
--	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
--		CONVERT(varchar(6), @Elapsed) + ' Mins'

--exec WriteAuditLogEvent 'BuildFactCOMWait', @Stats, @StartTime


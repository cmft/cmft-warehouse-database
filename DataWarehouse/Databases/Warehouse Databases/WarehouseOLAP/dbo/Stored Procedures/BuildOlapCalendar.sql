﻿CREATE procedure [dbo].[BuildOlapCalendar] as

set datefirst 1 --monday


truncate table OlapCalendar

insert into OlapCalendar
	(
	 TheDate
	,DayOfWeek
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth
	,WeekNoKey
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	)
select
	 TheDate

	,TheDay =
		datename(weekday, TheDate)

	,DayOfWeekKey =
		datepart(weekday, TheDate)

	,LongDate =
		left(convert(varchar, TheDate, 113), 11)

	,TheMonth =
		left(datename(month, TheDate), 3) + ' ' + datename(year, TheDate) 

	,FinancialQuarter =
		'Qtr ' + 
		case datepart(quarter, TheDate)
		when 1 then '4'
		else convert(varchar, datepart(quarter, TheDate) - 1)
		end + ' ' + FinancialYear
	

	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey

	,CalendarQuarter
	,CalendarSemester
	,CalendarYear

--derived as 1 if either last day of month or 
	,LastCompleteMonth =
		case
		when
			datepart(day, dateadd(day, 1, getdate())) = 1 
		and	TheDate = dateadd(day, -1, datepart(day, dateadd(day, 1, getdate())))
		then 1 --it is the last day of month
		when
			TheDate = dateadd(month, -1, dateadd(day, datepart(day, getdate()) * -1 + 1, dateadd(day, datediff(day, 0, getdate()), 0))) --mid month so go back to start of last month
		then 1
		else 0
		end

	,WeekNoKey =
		datename(year, TheDate) + right('00' + datename(week, TheDate), 2) 

	,WeekNo =
		'Week Ending ' + left(convert(varchar, dateadd(day, 6, FirstDayOfWeek), 113), 11) 

	,FirstDayOfWeek

	,LastDayOfWeek =
		case
		when datepart(week , TheDate) != 1 then dateadd(day, 6, FirstDayOfWeek)
		else dateadd(day , 7 - (datepart(weekday, TheDate)) , TheDate)
		end

from
	(
	select
		TheDate

		,FinancialYear =
			case datepart(quarter, TheDate)
			when 1 then convert(char(4),datepart(year,dateadd(year,-1, convert(datetime, TheDate)))) + '/' + datename(year, TheDate)
			else datename(year, TheDate) + '/' + convert(char(4),datepart(year,dateadd(year,1, TheDate)))
			end

		,FinancialMonthKey =
			case datepart(quarter, TheDate)
			when 1 then datepart(year, TheDate) - 1
			else datepart(year, TheDate)
			end * 100 + 
			case datepart(quarter, TheDate)
			when 1 then 9 else -3 
			end + datepart(month, TheDate)

		,FinancialQuarterKey =
			case datepart(quarter, TheDate)
			when 1 then datepart(year, TheDate) - 1
			else datepart(year, TheDate)
			end * 100 + 
			case datepart(quarter, TheDate)
			when 1 then 4
			else datepart(quarter, TheDate) - 1
			end

		,FirstDayOfMonth =
			convert(smalldatetime, '01 ' + datename(month, TheDate) + ' ' + datename(year, TheDate)) 

		,FirstDayOfWeek =
			(
			select
				min(StartOfWeek.TheDate)
			from
				CalendarBase StartOfWeek
			where
				datepart(week, StartOfWeek.TheDate) = datepart(week, CalendarBase.TheDate)
			and	datepart(year, StartOfWeek.TheDate) = datepart(year, CalendarBase.TheDate)
			)


	--calendar hierarchy
		,CalendarQuarter =
			'Qtr ' + convert(char,datepart(quarter,TheDate))
 
		,CalendarSemester =
			case when datepart(quarter, TheDate) < 3 then '1st Semester' else '2nd Semester' end

		,CalendarYear =
			datename(year,TheDate) 

	from
		dbo.CalendarBase
	) Calendar

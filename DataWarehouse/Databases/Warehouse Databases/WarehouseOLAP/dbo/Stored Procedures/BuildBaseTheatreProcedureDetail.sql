﻿

CREATE procedure [dbo].[BuildBaseTheatreProcedureDetail] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseTheatreProcedureDetail

INSERT INTO WarehouseOLAP.dbo.BaseTheatreProcedureDetail
(
	 SequenceNo
	,SourceUniqueID
	,SupervisingSurgeon1Code
	,SupervisingSurgeon2Code
	,SupervisingSurgeon3Code
	,SupervisingAnaesthetistNurseCode
	,SupervisingScoutNurseCode
	,SupervisingInstrumentNurseCode
	,SupervisingTechnician1Code
	,SupervisingTechnician2Code
	,ProcedureConfirmedFlag
	,CMBSItemCode
	,Unused2
	,SupervisingAnaesthetist2Code
	,Unused3
	,SupervisingAnaesthetist3Code
	,ProcedureStartTime
	,ProcedureEndTime
	,ProcedureDescription
	,WoundType1Code
	,WoundType2Code
	,WoundType3Code
	,ProcedureCode
	,OperationDetailSourceUniqueID
	,SupervisingAnaesthetist1Code
	,Unused4
	,Unused5
	,Unused6
	,LastUpdated
	,LogDetail
	,Surgeon1Code
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist1Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaesthetistNurseCode
	,Technician1Code
	,Technician2Code
	,ChangeOverAnaesthetistNurseCode
	,ChangeOverScoutNurseCode
	,ChangeOverInstrumentNurseCode
	,InventoryRecordCreatedFlag
	,PrincipleDiagnosisFlag
	,WoundType4Code
	,PrimaryProcedureFlag
	,WoundType5Code
	,WoundType6Code
	,PANCode
	,PADCode
	,RecordTimesCode
	,RecoverySourceUniqueID
	,ProcedureItemSourceUniqueID
	,ProcedureStartTimeOfDay
	,ProcedureEndTimeOfDay
)


SELECT
	 SequenceNo
	,SourceUniqueID
	,SupervisingSurgeon1Code
	,SupervisingSurgeon2Code
	,SupervisingSurgeon3Code
	,SupervisingAnaesthetistNurseCode
	,SupervisingScoutNurseCode
	,SupervisingInstrumentNurseCode
	,SupervisingTechnician1Code
	,SupervisingTechnician2Code
	,ProcedureConfirmedFlag
	,CMBSItemCode
	,Unused2
	,SupervisingAnaesthetist2Code
	,Unused3
	,SupervisingAnaesthetist3Code
	,ProcedureStartTime
	,ProcedureEndTime
	,ProcedureDescription
	,WoundType1Code
	,WoundType2Code
	,WoundType3Code

	,ProcedureCode =
		Operation.OperationCode1

	,OperationDetailSourceUniqueID
	,SupervisingAnaesthetist1Code
	,Unused4
	,Unused5
	,Unused6
	,LastUpdated
	,LogDetail
	,Surgeon1Code
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist1Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaesthetistNurseCode
	,Technician1Code
	,Technician2Code
	,ChangeOverAnaesthetistNurseCode
	,ChangeOverScoutNurseCode
	,ChangeOverInstrumentNurseCode
	,InventoryRecordCreatedFlag
	,PrincipleDiagnosisFlag
	,WoundType4Code
	,PrimaryProcedureFlag
	,WoundType5Code
	,WoundType6Code
	,PANCode
	,PADCode
	,RecordTimesCode
	,RecoverySourceUniqueID
	,ProcedureItemSourceUniqueID

	,ProcedureStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, ProcedureDetail.ProcedureStartTime), 0)
				,ProcedureDetail.ProcedureStartTime
			)
			,-1
		)

	,ProcedureEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, ProcedureDetail.ProcedureEndTime), 0)
				,ProcedureDetail.ProcedureEndTime
			)
			,-1
		)
FROM
	Warehouse.Theatre.ProcedureDetail

left join Warehouse.Theatre.Operation
on	Operation.OperationCode = ProcedureDetail.ProcedureCode




select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseTheatreProcedureDetail', @Stats, @StartTime



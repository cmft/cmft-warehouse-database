﻿
CREATE proc [dbo].[BuildBaseObservationObservationSet]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

set dateformat dmy

truncate table dbo.BaseObservationObservationSet

insert into dbo.BaseObservationObservationSet
(
	ObservationSetRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,WardID
	,ReplacedSourceUniqueID
	,CurrentObservationSetFlag
	,AdditionalObservationSetFlag
	,AdmissionSourceUniqueID
	,EarlyWarningScoreRegimeApplicationID
	,ObservationProfileApplicationID
	,ObservationNotTakenReasonID
	,ClinicianPresentSeniorityID
	,StartDate
	,StartTime
	,StartTimeOfDayCode
	,TakenDate
	,TakenTime
	,TakenTimeOfDayCode
	,OverallRiskIndexCode
	,OverallAssessedStatusID
	,AlertSeverityID
	,DueTime
	,DueTimeStatusID
	,DueTimeCreatedBySourceUniqueID
	,AlertChainID
	,IsFirstInAlertChain
	,IsLastInAlertChain
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,AlertChainDurationMinutes
	,OverallAssessedStatusScore
)

select
	ObservationSetRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID = 
			coalesce(
				SpecialtyID
				,-1
			)
	,WardID =	
			coalesce(
				WardID
				,-1
			)
	,ReplacedSourceUniqueID
	,CurrentObservationSetFlag
	,AdditionalObservationSetFlag
	,AdmissionSourceUniqueID
	,EarlyWarningScoreRegimeApplicationID
	,ObservationProfileApplicationID
	,ObservationNotTakenReasonID = 
			coalesce(
				ObservationNotTakenReasonID
				,-1
			)
	,ClinicianPresentSeniorityID = 
  			coalesce(
				ClinicianPresentSeniorityID
				,-1
			)
	,StartDate
	,StartTime
	,StartTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, StartTime), 0)
							,StartTime
						)
						,-1
					)
	,TakenDate
	,TakenTime
	,TakenTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, TakenTime), 0)
							,TakenTime
						)
						,-1
					)
	,OverallRiskIndexCode = 
  			coalesce(
				OverallRiskIndexCode
				,-1
			)
	,OverallAssessedStatusID =
  			coalesce(
				ObservationSet.OverallAssessedStatusID
				,-1
			)
	,AlertSeverityID = 
  	  		coalesce(
				AlertSeverityID
				,-1
			)
	,DueTime
	,DueTimeStatusID = 
			coalesce(
				DueTimeStatusID
				,-1
			)
	,DueTimeCreatedBySourceUniqueID
	,AlertChainID
	,IsFirstInAlertChain
	,IsLastInAlertChain
	,OutOfHours = 
				case
				when datename(dw, StartTime) in ('Saturday', 'Sunday') then 1
				when cast(StartTime as time) < '8:00' then 1
				when cast(StartTime as time) > '17:00' then 1
				when exists
					(
					select
						1
					from
						Warehouse.WH.Holiday
					where
						StartDate = Holiday.HolidayDate
					)
				then 1
				else 0
				end
	--,AgeID = CMFT.Dates.GetAge(DateOfBirth, StartTime)
	,AgeID = 
			-- Ripped from the Dates.GetAge function in CMFT for speed
			case
			when DateOfBirth > StartTime
			then 0
			when (month(StartTime) * 100) + day(StartTime) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
			then datediff(year, DateOfBirth, StartTime)
			else datediff(year, DateOfBirth, StartTime) - 1
			end
	--case
	--when datediff(day, DateOfBirth, StartTime) is null then 'Age Unknown'
	--when datediff(day, DateOfBirth, StartTime) <= 0 then '00 Days'
	--when datediff(day, DateOfBirth, StartTime) = 1 then '01 Day'
	--when datediff(day, DateOfBirth, StartTime) > 1 and
	--datediff(day, DateOfBirth,StartTime) <= 28 then
	--    right('0' + Convert(Varchar,datediff(day, DateOfBirth, StartTime)), 2) + ' Days'
	
	--when datediff(day, DateOfBirth, StartTime) > 28 and
	--datediff(month, DateOfBirth,StartTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day,StartTime) then 1 else 0 end
	--     < 1 then 'Between 28 Days and 1 Month'
	--when datediff(month, DateOfBirth, StartTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day, StartTime) then 1 else 0 end = 1
	--    then '01 Month'
	--when datediff(month, DateOfBirth, StartTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day, StartTime) then 1 else 0 end > 1 and
	-- datediff(month, DateOfBirth, StartTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day, StartTime) then 1 else 0 end <= 23
	--then
	--right('0' + Convert(varchar,datediff(month, DateOfBirth,StartTime) -
	--case  when datepart(day, DateOfBirth) > datepart(day, StartTime) then 1 else 0 end), 2) + ' Months'
	--when datediff(yy, DateOfBirth, StartTime) - 
	--(
	--case 
	--when	(datepart(m, DateOfBirth) > datepart(m, StartTime)) 
	--or
	--	(
	--		datepart(m, DateOfBirth) = datepart(m, StartTime) 
	--	And	datepart(d, DateOfBirth) > datepart(d, StartTime)
	--	) then 1 else 0 end
	--) > 99 then '99+'
	--else right('0' + convert(varchar, datediff(yy, DateOfBirth, StartTime) - 
	--(
	--case 
	--when	(datepart(m, DateOfBirth) > datepart(m, StartTime)) 
	--or
	--	(
	--		datepart(m, DateOfBirth) = datepart(m, StartTime) 
	--	And	datepart(d, DateOfBirth) > datepart(d, StartTime)
	--	) then 1 else 0 end
	--)), 2) + ' Years'

	--end
	,ContextCode
	,Cases = 1
	,AlertChainDurationMinutes
	,OverallAssessedStatus.OverallAssessedStatusScore
from
	Warehouse.Observation.ObservationSet

left join dbo.OlapObservationOverallAssessedStatus OverallAssessedStatus
on ObservationSet.OverallAssessedStatusID = OverallAssessedStatus.OverallAssessedStatusID


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	













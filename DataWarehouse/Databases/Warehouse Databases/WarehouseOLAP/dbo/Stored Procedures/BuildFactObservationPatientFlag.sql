﻿


CREATE proc [dbo].[BuildFactObservationPatientFlag]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationPatientFlag

insert into dbo.FactObservationPatientFlag
(
PatientFlagRecno
,SourceUniqueID
,CasenoteNumber
,DateOfBirth
,PatientFlagID
,StartDate
,StartTime
,StartTimeOfDayCode
,EndDate
,EndTime
,EndTimeOfDayCode
,CreatedByUserID
,EWSDisabled
,DurationID
,OutOfHours
,AgeID
,ContextCode
,Cases
,PatientFlagDurationDays
)

select
	PatientFlagRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,PatientFlagID
	,StartDate
	,StartTime
	,StartTimeOfDayCode
	,EndDate
	,EndTime
	,EndTimeOfDayCode
	,CreatedByUserID
	,EWSDisabled
	,DurationID = 
				coalesce(
						case 
						when PatientFlagDurationDays < 0 then -1
						else PatientFlagDurationDays
						end
					, -2
					)
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,PatientFlagDurationDays
from
	dbo.BaseObservationPatientFlag



select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	







﻿
create proc dbo.BuildBaseMedisec 

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.BaseMedisec

insert into dbo.BaseMedisec

select
	EncounterRecno
	,SourcePatientNo
	,ProviderSpellNo
	,CaseNoteNumber
	,EpisodicGpPracticeCode
	,EpisodicGpPracticeIsMedisecUser
	,PCTCode
	,AdmissionDate
	,AdmissionTime
	,DischargeDate
	,DischargeTime 
	,EpisodeStartDate 
	,EpisodeEndDate
	,PatientCategoryCode = 
		coalesce(PatientCategoryCode, 'N/A')
	,EndDirectorateCode = 
		coalesce(EndDirectorateCode, 'N/A')
	,SiteCode = 
		coalesce(SiteCode, 'N/A')
	,ManagementIntentionCode = 
		coalesce(ManagementIntentionCode, 'N/A')
	,SpecialtyCode = 
		coalesce(SpecialtyCode, 'N/A')
	,NationalSpecialtyCode =
		coalesce(NationalSpecialtyCode, 'N/A')
	,ConsultantCode =
		coalesce(ConsultantCode, 'N/A')
	,NeonatalLevelOfCare = 
		coalesce(NeonatalLevelOfCare, 'N/A')
	,PrimaryOperationCode = 
		coalesce(rtrim(left(PrimaryOperationCode, 5)), '##')
	,PrimaryDiagnosisCode = 
		coalesce(rtrim(left(PrimaryDiagnosisCode, 5)), '##')
	,WardCode = 
		coalesce(WardCode, 'N/A')
	,DocumentTime = 
		coalesce(DocumentTime, '01/01/1900')
	,StatusCode = 
		coalesce(StatusCode, 'N/A')
	,Comment = 
		coalesce(Comment, 'N/A')
	,DocumentName = 
		coalesce(DocumentName, 'N/A')
	,[Description] = 
		coalesce([Description], 'N/A')
	,DocumentTypeCode = 
		coalesce(DocumentTypeCode, 'N/A')
	,AuthorCode = 
		coalesce(AuthorCode, 'N/A')
	,IssuedTime = 
		coalesce(IssuedTime, '01/01/1900')
	,IssuedByCode = 
		coalesce(IssuedByCode, 'N/A')			
	,SignedTime = 
		coalesce(SignedTime, '01/01/1900')
	,SignedByCode = 
		coalesce(SignedByCode, 'N/A')	
	,DocumentProgressCode = 
		coalesce(DocumentProgressCode, 'N/A')	
	,RequestOrderForDischarge
	,IsFirstDocumentForDischarge
	,DocumentSentGPForDischarge
	,TemplateCode =
		coalesce(TemplateCode, 'N/A')	
	,AdmissionReason =
		coalesce(AdmissionReason, 'N/A')	
	,Diagnosis = 
		coalesce(Diagnosis, 'N/A')	
	,Investigations = 
		coalesce(Investigations, 'N/A')
	,RecommendationsOnFutureManagement =
		coalesce(RecommendationsOnFutureManagement, 'N/A')
	,[InformationToPatientOrRelatives] =
		coalesce([InformationToPatientOrRelatives], 'N/A')
	,ChangesToMedication = 
		coalesce(ChangesToMedication, 'N/A')
	,Cases
from 
	Warehouse.Medisec.Encounter


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime


exec dbo.BuildBaseDischargeSummary
﻿
create proc dbo.DateRange_Get
(@daterange varchar (50))

as

declare @date datetime = getdate()

set dateformat dmy
set nocount on


if @daterange = 'Last Week'
	select [Start] = dateadd(day, 1, dbo.f_Last_of_Week(dateadd(day, -14, @date)))
			,[End] = dbo.f_Last_of_Week(dateadd(day, -7, @date))
			
else if @daterange = 'YTD'
	select [Start] = dbo.f_First_of_Financial_Year(getdate())
			,[End] = dbo.f_Last_of_Week(dateadd(day, -7, getdate()))
			
else if @daterange = 'Last 12 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -12, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
			
			
else if @daterange = 'Last 13 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -13, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
			
else if @daterange = 'Last 6 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -6, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))

else if @daterange = 'Last 4 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -4, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
			
else if @daterange = 'Last 3 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -3, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))

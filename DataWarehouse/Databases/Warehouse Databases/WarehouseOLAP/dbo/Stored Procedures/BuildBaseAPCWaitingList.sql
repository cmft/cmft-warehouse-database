﻿
CREATE procedure [dbo].[BuildBaseAPCWaitingList] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseAPCWaitingList

insert into dbo.BaseAPCWaitingList
	(
	 EncounterRecno
	,InterfaceCode
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,EpisodeSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,EpisodicGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,ExpectedLOS
	,ProcedureDate
	,FutureCancellationDate
	,TheatreKey
	,TheatreInterfaceCode
	,EpisodeNo
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,AgeCode
	,Cases
	,LengthOfWait
	,DurationCode
	,WaitTypeCode
	,StatusCode
	,CategoryCode
	,AddedToWaitingListTime
	,AddedLastWeek
	,WaitingListAddMinutes
	,OPCSCoded
	,WithRTTStartDate
	,WithRTTStatusCode
	,WithExpectedAdmissionDate
	,SourceUniqueID
	,AdminCategoryCode
	,RTTActivity
	,RTTBreachStatusCode
	,DiagnosticProcedure
	,DirectorateCode
	,GuaranteedAdmissionDate
	,DurationAtTCIDateCode
	,WithTCIDate
	,WithGuaranteedAdmissionDate
	,WithRTTOpenPathway
	)
SELECT --top 100
	 EncounterRecno
	,InterfaceCode
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle

	,SexCode =
		case Encounter.SexCode
		when '' then '0'
		when 'M' then '1'
		when 'F' then '2'
		when 'I' then '9'
		else '#'
		end

	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,coalesce(Encounter.ConsultantCode, 'N/A') ConsultantCode
	,coalesce(Encounter.SpecialtyCode, 'N/A') SpecialtyCode
	,coalesce(Encounter.PASSpecialtyCode, 'N/A') PASSpecialtyCode
	,coalesce(Encounter.EpisodeSpecialtyCode, 'N/A') EpisodeSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical

	,IntendedPrimaryOperationCode =
		case
		when len(IntendedPrimaryOperationCode) = 3 then IntendedPrimaryOperationCode + '.9'
		else IntendedPrimaryOperationCode
		end

	,Operation

	,SiteCode =
		coalesce(SiteCode , 'N/A')

	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList

	,TCIDate = 
		case
		when TCIDate is null
		then '1 Jan 1900'
		when TCIDate < (select min(TheDate) from dbo.CalendarBase where TheDate <> '1 Jan 1900')
		then '1 Jan 1900'
		when TCIDate < CensusDate
		then '1 Jan 1900'
		else cast(TCIDate as date)
		end

	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,EpisodicGpCode
	,coalesce(Encounter.RegisteredPracticeCode, Encounter.EpisodicGpPracticeCode, 'X99999') RegisteredGpPracticeCode
	,coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredPracticeCode, 'X99999') EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,ExpectedLOS
	,ProcedureDate
	,FutureCancellationDate
	,TheatreKey
	,TheatreInterfaceCode
	,EpisodeNo

	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays

	,case
	when datediff(day, DateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 1 and
	datediff(day, DateOfBirth, Encounter.CensusDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.CensusDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 28 and
	datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end AgeCode

	,1 Cases

	,convert(int, Encounter.KornerWait) LengthOfWait

	,DurationCode =
		coalesce(
			right('00' +
			convert(varchar, 
			case
			when convert(int, Encounter.KornerWait /7 ) > 207 then 208
			else convert(int, Encounter.KornerWait /7 )
			end), 3)
			,'NA'
		)

	--,WaitTypeCode =
	--case
	--when rtrim(Encounter.AdmissionMethodCode) = 'BA' then '20'
	--when rtrim(Encounter.AdmissionMethodCode) = 'BL' then '20'
	--when rtrim(Encounter.AdmissionMethodCode) = 'BP' then '20'
	--when rtrim(Encounter.AdmissionMethodCode) = 'PA' then '30'
	--when rtrim(Encounter.AdmissionMethodCode) = 'PL' then '30'
	--else '10'
	--end

	,WaitTypeCode =
	case
	when rtrim(Encounter.AdmissionMethodCode) = 'PA' then '20'
	when rtrim(Encounter.AdmissionMethodCode) = 'PL' then '20'
	when Encounter.WLStatus = 'WL Suspend' then '30'
	when Encounter.AdminCategoryCode = 'PAY' then '40' --CCB fixed 2012-05-31
	else '10'
	end

	,StatusCode =
	case
	when Encounter.WLStatus = 'WL Suspend' then '20'
	else '10'
	end

	,CategoryCode =
	case
	when rtrim(Encounter.ManagementIntentionCode) = 'I' then '10'
	else '20'
	end

	,AddedToWaitingListTime

	,AddedLastWeek =
	case
	when
		datediff(
			 day
			,dateadd(day, datediff(day, 0, Encounter.AddedToWaitingListTime), 0)
			,Encounter.CensusDate
		) < 8 then 1
	else 0
	end

	,WaitingListAddMinutes =
	case
	when
		datediff(
			 day
			,dateadd(day, datediff(day, 0, Encounter.AddedToWaitingListTime), 0)
			,Encounter.CensusDate
		) < 8
	then
		coalesce(
			 WaitingListAddMinutes.WaitingListAddMinutes
			,(
			select
				NumericValue
			from
				Warehouse.dbo.Parameter
			where
				Parameter = 'DEFAULTWLADDMINUTES'
			)
			,10
		)
	else null
	end

	,OPCSCoded =
	case
	when Encounter.IntendedPrimaryOperationCode is null
	then 0
	else 1
	end

	,WithRTTStartDate =
	case
	when Encounter.RTTStartDate is null
	then 0
	else 1
	end

	,WithRTTStatusCode =
	case
	when Encounter.RTTCurrentStatusCode in ('10', '20') then 1
	else 0
	end

	,WithExpectedAdmissionDate =
	case
	when Encounter.ExpectedAdmissionDate is null
	then 0
	else 1
	end



	,SourceUniqueID
	,AdminCategoryCode

	,RTTActivity =
		convert(
			bit
			,0
			--,case
			--when
			--	Encounter.AdmissionMethodCode in ('BA', 'BL', 'WL')
			--and	Encounter.SpecialtyCode not in 
			--	(
			--	select
			--		SpecialtyCode
			--	from
			--		RTT.dbo.ExcludedSpecialty
			--	)

			---- 'GENE' ?
			--and	Encounter.ConsultantCode <> 'FRAC'

			----remove ICAT
			--and	Encounter.SiteCode != 'FMSK'

			--and	not exists
			--	(
			--	select
			--		1
			--	from
			--		RTT.dbo.RTTClockStart ClockChangeReason
			--	where
			--		ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
			--	and	ClockChangeReason.SourceEntityRecno = Encounter.SourceEncounterNo
			--	and	coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
			--	)
			--then 1
			--else 0
			--end
		)


	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null
		then 'U'
		when Encounter.RTTBreachDate < Encounter.TCIDate
		then 'B'
		else 'N'
		end


	,DiagnosticProcedure =
		convert(
			bit
			,case
			when DiagnosticProcedure.ProcedureCode is null
			then 0
			else 1
			end
		)
	,DirectorateCode = coalesce(Encounter.DirectorateCode , 'N/A')
	,GuaranteedAdmissionDate

	,DurationAtTCIDateCode =
		case
		when Encounter.TCIDate is null then 'NA'
		when Encounter.TCIDate < (select min(TheDate) from dbo.CalendarBase where TheDate <> '1 Jan 1900') then 'NA'
		when Encounter.TCIDate < CensusDate then 'NA'
		else right('00' +
				convert(varchar, 
					case
					when convert(int, (Encounter.KornerWait + datediff(day , CensusDate , TCIDate)) /7 ) > 207 then 208
					else convert(int, (Encounter.KornerWait + datediff(day , CensusDate , TCIDate)) /7 )
					end
				)
				,3
			)
		end

	,WithTCIDate =
		case
		when TCIDate is null
		then 0
		when TCIDate < (select min(TheDate) from dbo.CalendarBase where TheDate <> '1 Jan 1900')
		then 0
		when TCIDate < CensusDate
		then 0
		else 1
		end

	,WithGuaranteedAdmissionDate =
		case
		when Encounter.GuaranteedAdmissionDate is null
		then 0
		else 1
		end

	,coalesce(Encounter.WithRTTOpenPathway , 0)

FROM
	Warehouse.APC.WaitingList Encounter

left join Warehouse.dbo.DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = Encounter.IntendedPrimaryOperationCode

left join 
	(
	select
		 SpecialtyCode = EntityCode
		,WaitingListAddMinutes = convert(int, Description)
	from
		Warehouse.dbo.EntityLookup
	where
		EntityTypeCode = 'WLADDMINUTES'
	) WaitingListAddMinutes
on	WaitingListAddMinutes.SpecialtyCode = Encounter.SpecialtyCode


select @RowsInserted = @@rowcount

exec BuildOlapCensus

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseAPCWaitingList', @Stats, @StartTime


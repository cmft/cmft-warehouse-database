﻿CREATE procedure [dbo].[BuildBaseBedComplement] as

truncate table dbo.BaseBedComplement

insert
into
	dbo.BaseBedComplement
(
	 TheDate
	,WardCode
	,DaycaseBeds
	,InpatientBeds
	,SingleRoomBeds
	,WardBedComment
	,WardBedStartDate
	,DivisionCode
	,SpecialtyGroupID
	,WardTypeID
	,Schedule
	,WardConfigurationStartDate
	,WardConfigurationEndDate
	,WardConfigurationComment
)
select
	 Calendar.TheDate

	,Ward.WardCode

	,WardBed.DaycaseBeds
	,WardBed.InpatientBeds
	,WardBed.SingleRoomBeds
	,WardBedComment = WardBed.Comment
	,WardBedStartDate = WardBed.StartDate

	,WardConfiguration.DivisionCode
	,WardConfiguration.SpecialtyGroupID
	,WardConfiguration.WardTypeID
	,WardConfiguration.Schedule
	,WardConfigurationStartDate = WardConfiguration.StartDate
	,WardConfigurationEndDate = WardConfiguration.EndDate
	,WardConfigurationComment = WardConfiguration.Comment
from
	Warehouse.BedComplement.Ward

cross join dbo.CalendarBase Calendar

inner join Warehouse.BedComplement.WardBed
on	WardBed.WardID = Ward.WardID
and Calendar.TheDate between WardBed.StartDate
and	
	coalesce(
		dateadd(
			 day
			,-1
			,(
			select
				min(NextWardBed.StartDate)
			from
				Warehouse.BedComplement.WardBed NextWardBed
			where
				NextWardBed.WardID = Ward.WardID
			and	NextWardBed.StartDate > WardBed.StartDate
			)
		)
		,Calendar.TheDate
	)	

inner join Warehouse.BedComplement.WardConfiguration
on	WardConfiguration.WardID = Ward.WardID
and Calendar.TheDate between WardConfiguration.StartDate and
	coalesce(
		 WardConfiguration.EndDate
		,Calendar.TheDate
	)

where
	exists
	(
	select
		1
	from
		dbo.FactBedOccupancy
	where
		FactBedOccupancy.BedOccupancyDate = Calendar.TheDate
	)

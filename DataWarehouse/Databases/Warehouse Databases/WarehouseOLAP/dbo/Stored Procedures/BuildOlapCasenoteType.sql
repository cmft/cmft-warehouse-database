﻿


create procedure [dbo].[BuildOlapCasenoteType] as

truncate table dbo.OlapCasenoteType

insert into dbo.OlapCasenoteType
(
	CasenoteTypeCode
)

select distinct
	CasenoteTypeCode =
		case
		when isnumeric(CasenoteNumber) = 1 and len(CasenoteNumber) = 10 then 'NHSNumber'
		when left(CasenoteNumber , 3) = 'PCT' then 'PCT'
		when charindex('/' , CasenoteNumber , 0) != 0 then
			case
			when left(CasenoteNumber , 1) = 'G' then 'G'
			when left(CasenoteNumber , 1) = 'T' then 'T'
			else left(CasenoteNumber , charindex('/' , CasenoteNumber , 0) - 1)
			end
		--when right(CasenoteNumber , 1) = 'D' then left(CasenoteNumber , 1) + '_D'
		when right(CasenoteNumber , 1) = 'D' then 'D'
		else left(CasenoteNumber , 1)
		end
from
	Warehouse.PAS.Casenote


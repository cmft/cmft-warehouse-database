﻿


CREATE proc [dbo].[BuildFactResus]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactResus

insert into dbo.FactResus

(
EncounterRecno
,AdmissionDate
,EventDate
,LocationCode
,ROSCTimeOfDayCode
,DeathTimeOfDayCode
,CollapseTimeOfDayCode
,CPRCalloutTimeOfDayCode
,CPRArriveTimeOfDayCode
,ArrestConfirmTimeOfDayCode
,CPRStartTimeOfDayCode
,AwakeningTimeOfDayCode
,AwakeningDate
,DischargeDate
,DateOfDeath
,CallOutTypeID
,PotentiallyAvoidable
,AgeID
,InterfaceCode
,Cases
)

select
	EncounterRecno
	,AdmissionDate
	,EventDate
	,LocationCode
	,ROSCTimeOfDayCode
	,DeathTimeOfDayCode
	,CollapseTimeOfDayCode
	,CPRCalloutTimeOfDayCode
	,CPRArriveTimeOfDayCode
	,ArrestConfirmTimeOfDayCode
	,CPRStartTimeOfDayCode
	,AwakeningTimeOfDayCode
	,AwakeningDate
	,DischargeDate
	,DateOfDeath
	,CallOutTypeID
	,ebmPAI
	,AgeID
	,InterfaceCode
	,Cases
from
	dbo.BaseResus



select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	







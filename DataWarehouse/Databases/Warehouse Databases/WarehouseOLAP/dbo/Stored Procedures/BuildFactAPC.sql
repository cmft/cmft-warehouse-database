﻿
CREATE procedure [dbo].[BuildFactAPC] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactAPC


insert into dbo.FactAPC
(
	 EncounterRecno
	,EncounterDate
	,MetricCode
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode
	,PCTCode
	,SiteCode
	,AgeCode
	,HRGCode
	,Cases
	,LengthOfEncounter
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,AdmissionMethodCode
	,RTTActivity
	,RTTBreachStatusCode
	,DiagnosticProcedure
	,RTTTreated
	,EncounterTimeOfDay
	,DirectorateCode
	,SexCode
	,ISTAdmissionSpecialtyCode
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,CodingComplete
)
--FCE Checksum
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'CHKFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	EpisodeEndDate is not null

union all

--Inpatient Elective FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'IEFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	PatientClassificationCode = '1'
--and	(
--		(
--			PatientClassificationCode = '1'
--		and	ManagementIntentionCode <> 'D'
--		)
--		or	(
--			ManagementIntentionCode in ('D', 'R', 'N')
----		and	datediff(day, AdmissionDate, DischargeDate) > 0
--		and	not datediff(day, AdmissionDate, DischargeDate) = 0 -- 2010-04-01 CCB: Include non-discharged patients
--		)
--	)
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL' --elective

union all

--Day Case FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'DCFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	PatientClassificationCode = '2'
--and	ManagementIntentionCode = 'D'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Regular Day FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'RDFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	PatientClassificationCode = '3'
--and	ManagementIntentionCode = 'R'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Regular Night FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'RNFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	PatientClassificationCode = '4'
--and	ManagementIntentionCode = 'N'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Other FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'NEOTHFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'NE'

union all

--Maternity FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'MATFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'MAT'

union all

--Emergency FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'EMFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'EM'

union all

--Admissions Checksum
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'CHKADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	FirstEpisodeInSpellIndicator = 'true'

union all

--Inpatient Elective Admissions
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'IEADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '1'

--and	(
--		(
--			PatientClassificationCode = '1'
--		and	ManagementIntentionCode <> 'D'
--		)
--	or	(
--			ManagementIntentionCode in ('D', 'R', 'N')
--		and	datediff(day, AdmissionDate, DischargeDate) > 0
--		)
--	)
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL' --elective

union all

--Day Case Admissions
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'DCADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '2'

--and	ManagementIntentionCode = 'D'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Regular Day Admissions
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'RDADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '3'
--and	ManagementIntentionCode = 'R'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Regular Night Admissions
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'RNADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '4'
--and	ManagementIntentionCode = 'N'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Other Admissions
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'NEOTHADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	AdmissionMethod.AdmissionMethodTypeCode = 'NE'

union all

--Maternity Admissions
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'MATADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	AdmissionMethod.AdmissionMethodTypeCode = 'MAT'

union all

--Emergency Admissions
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'EMADM' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	AdmissionMethod.AdmissionMethodTypeCode = 'EM'

union all

--Discharges Checksum
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'CHKDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	LastEpisodeInSpellIndicator = 'Y'
and	Encounter.DischargeDate is not null

union all

--Inpatient Elective Discharges
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'IEDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	PatientClassificationCode = '1'
--and	(
--		(
--			PatientClassificationCode = '1'
--		and	ManagementIntentionCode <> 'D'
--		)
--		or	(
--			ManagementIntentionCode in ('D', 'R', 'N')
--		and	datediff(day, AdmissionDate, DischargeDate) > 0
--		)
--	)
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL' --elective

union all

--Day Case Discharges
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'DCDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	PatientClassificationCode = '2'
--and	ManagementIntentionCode = 'D'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Regular Day Discharges
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'RDDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	PatientClassificationCode = '3'
--and	ManagementIntentionCode = 'R'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Regular Night Discharges
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'RNDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	PatientClassificationCode = '4'
--and	ManagementIntentionCode = 'N'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Other Discharges
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'NEOTHDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'NE'

union all

--Maternity Discharges
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'MATDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'MAT'

union all

--Emergency Discharges
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'EMDIS' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.EndDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'EM'

union all

--Well Babies
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'WELLBABY' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	coalesce(NeonatalLevelOfCare, 'N') = 'N'
and	(
		PrimaryDiagnosisCode like 'Z38%'
--	or	exists
--		(
--		select
--			1
--		from
--			Warehouse.APC.Diagnosis Diagnosis
--		where
--			Diagnosis.APCSourceUniqueID = Encounter.SourceUniqueID
--		and	Diagnosis.DiagnosisCode like 'Z38%'
--		)
	)

union all

--Unwell Babies
SELECT
	 Encounter.EncounterRecno
	,Encounter.AdmissionDate EncounterDate
	,'ILLBABY' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	coalesce(NeonatalLevelOfCare, 'N') <> 'N'
and	(
		PrimaryDiagnosisCode like 'Z38%'
--	or	exists
--		(
--		select
--			1
--		from
--			Warehouse.APC.Diagnosis Diagnosis
--		where
--			Diagnosis.APCSourceUniqueID = Encounter.SourceUniqueID
--		and	Diagnosis.DiagnosisCode like 'Z38%'
--		)
	)

union all

--Died
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'DIED' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.EndSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	DischargeMethodCode in ('DN', 'DP')

union all

--Stillbirths
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'STILL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.DischargeTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	LastEpisodeInSpellIndicator = 'Y'
and	DischargeDate is not null
and	DischargeMethodCode IN ('ST', 'SP')
--and	DischargeMethodCode = 'ST'   GS20110830  Replaced with codes ST and SP

union all

--First FCE Checksum
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'CHKFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	EpisodeEndDate is not null
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Inpatient Elective First FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'IEFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '1'
--and	(
--		(
--			PatientClassificationCode = '1'
--		and	ManagementIntentionCode <> 'D'
--		)
--		or	(
--			ManagementIntentionCode in ('D', 'R', 'N')
--		and	datediff(day, AdmissionDate, DischargeDate) > 0
--		)
--	)
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL' --elective

union all

--Day Case First FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'DCFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	PatientClassificationCode = '2'
and	ManagementIntentionCode = 'D'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
--and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Regular Day First FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'RDFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '3'
--and	ManagementIntentionCode = 'R'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Regular Night First FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'RNFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	Encounter.FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '4'
--and	ManagementIntentionCode = 'N'
--and	datediff(day, AdmissionDate, DischargeDate) = 0
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'

union all

--Other First FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'NEOTHFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'NE'
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Maternity First FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'MATFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'MAT'
and	Encounter.FirstEpisodeInSpellIndicator = 'true'

union all

--Emergency First FCEs
SELECT
	 Encounter.EncounterRecno
	,Encounter.EpisodeEndDate EncounterDate
	,'EMFFCE' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOE
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.EpisodeEndTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	EpisodeEndDate is not null
and	AdmissionMethod.AdmissionMethodTypeCode = 'EM'
and	Encounter.FirstEpisodeInSpellIndicator = 'true'



union all

--Spells Checksum
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'CHKSPL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter
where
	FirstEpisodeInSpellIndicator = 'true'
AND Encounter.DischargeDate IS NOT NULL

union all

--Inpatient Elective Spells
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'IESPL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '1'
AND Encounter.DischargeDate IS NOT NULL
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL' --elective

union all

--Day Case Spells
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'DCSPL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '2'
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'
AND Encounter.DischargeDate IS NOT NULL

union all

--Regular Day Spells
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'RDSPL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '3'
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'
AND Encounter.DischargeDate IS NOT NULL

union all

--Regular Night Spells
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'RNSPL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	PatientClassificationCode = '4'
and	AdmissionMethod.AdmissionMethodTypeCode = 'EL'
AND Encounter.DischargeDate IS NOT NULL

union all

--Other Spells
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'NEOTHSPL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	AdmissionMethod.AdmissionMethodTypeCode = 'NE'
AND Encounter.DischargeDate IS NOT NULL

union all

--Maternity Spells
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'MATSPL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	AdmissionMethod.AdmissionMethodTypeCode = 'MAT'
AND Encounter.DischargeDate IS NOT NULL

union all

--Emergency Spells
SELECT
	 Encounter.EncounterRecno
	,Encounter.DischargeDate EncounterDate
	,'EMSPL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.StartSiteCode
	,Encounter.AgeCode
	,Encounter.SpellHRGCode
	,Encounter.Cases
	,Encounter.LOS
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.AdmissionMethodCode
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.RTTTreated
	,Encounter.AdmissionTimeOfDay EncounterTimeOfDay
	,Encounter.StartDirectorateCode
	,Encounter.SexCode
	,Encounter.ISTAdmissionSpecialtyCode
	,Encounter.ISTAdmissionDemandTimeOfDay
	,Encounter.ISTDischargeTimeOfDay

	,CodingComplete =
		case
		when Encounter.ClinicalCodingCompleteDate is null
		then 0
		else 1
		end
from
	dbo.BaseAPC Encounter

inner join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

where
	FirstEpisodeInSpellIndicator = 'true'
and	AdmissionMethod.AdmissionMethodTypeCode = 'EM'
AND Encounter.DischargeDate IS NOT NULL




select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.EncounterDate)
	,@to = max(EncounterFact.EncounterDate)
from
	dbo.FactAPC EncounterFact
where
	EncounterFact.EncounterDate is not null


exec BuildOlapPractice 'APC'
exec BuildCalendarBasePair @from, @to
exec BuildAPCDurationBase


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactAPC', @Stats, @StartTime


﻿
CREATE procedure [dbo].[BuildFactEDD] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactEDD


-- Insert Current Spells
insert into dbo.FactEDD
(
	   SourceSpellNo
	  ,EddHistoryID
	  ,EddSequence
	  ,HasEddFlag
	  ,HasEddBedmanFlag
	  ,HasEddPASFlag
	  ,CurrentRecordFlag
      ,SourcePatientID
      ,SourceAEEncounterNo
      ,sourcepatientcategorycode
      ,SourceRTTPathwayCondition
      ,AdmitDate
      ,DischargeDate
      ,CurrentWard
      ,FacilityID
      ,AEDepartureTime
      ,CurrentSpecialty
      ,CurrentConsulant
      ,CurrentLocation
      ,SiteCode
      ,FirstEdd
      ,LastEdd
      ,PrevEdd
      ,Edd
      ,EddCreated
      ,EddReasonCode
      ,Actual_LOS
	  ,Exp_LOS
      ,varSpellFistLastEDD
	  ,varFromPrevEDD
	  ,varDischargeEDDCreated
	  ,EddEnteredWithin24hrs
	  ,DirectorateCode
	  ,SourceEncounterNo

)

SELECT SourceSpellNo
	  ,EddHistoryID
	  ,EddSequence
	  ,HasEddFlag
	  ,HasEddBedmanFlag
	  ,HasEddPASFlag
	  ,CurrentRecordFlag
      ,SourcePatientID
      ,SourceAEEncounterNo
      ,sourcepatientcategorycode
      ,SourceRTTPathwayCondition
      ,AdmitDate
      ,DischargeDate
      ,CurrentWard
      ,FacilityID
      ,AEDepartureTime
      ,CurrentSpecialty
      ,CurrentConsulant
      ,CurrentLocation
      ,SiteCode
      ,FirstEdd
      ,LastEdd
      ,prev_Edd AS PrevEdd
      ,dateadd(day, datediff(day, 0, Edd), 0) AS Edd
      ,EddCreated
      ,EddReasonCode
      ,coalesce(
			datediff(
				 d
				,dateadd(day, datediff(day, 0, [AdmitDate]), 0)
				,tbl_factEDD.DischargeDate
			)
			,-1
		) AS Actual_LOS
	  ,coalesce(
			datediff(
				 d
				,dateadd(day, datediff(day, 0, tbl_factEDD.[AdmitDate]), 0)
				,tbl_factEDD.[Edd]
			)
			,0
		) AS Exp_LOS
		  ,coalesce(
			datediff(
				 d
				,dateadd(day, datediff(day, 0, tbl_factEDD.firstEdd), 0)
				,tbl_factEDD.[lastEdd]
			)
			,0
			) AS varSpellFistLastEDD
		  ,coalesce(
			datediff(
				 d
				,dateadd(day, datediff(day, 0, tbl_factEDD.Edd), 0)
				,tbl_factEDD.[prev_Edd]
			)*-1
			,0
		) AS varFromPrevEDD
			  ,coalesce(
			datediff(
				 d
				,dateadd(day, datediff(day, 0, tbl_factEDD.[EddCreated]), 0)
				,tbl_factEDD.DischargeDate
			)
			,0
		) AS varDischargeEDDCreated
		,EddEnteredWithin24hrs
		,DirectorateCode
		,SourceEncounterNo

FROM
(

select DISTINCT tbl_BDspells.[SourceSpellNo]
	  ,tbl_BDhis.[PddHistoryID] AS [EddHistoryID]
	  ,coalesce(
				tbl_BDhis.[PddSequence]
				,0
				) AS EddSequence
	  ,CASE WHEN tbl_BDhis.[Pdd] IS NOT NULL THEN 1 
			WHEN SourceAPCEdd IS NOT NULL THEN 1
			ELSE 0 
		END  AS HasEddFlag
			
	  ,CASE 
			WHEN tbl_BDhis.[Pdd] IS NOT NULL THEN 1 
			ELSE 0 
		END  AS HasEddBedmanFlag
			
	  ,CASE 
			WHEN SourceAPCEdd IS NOT NULL THEN 1 
			ELSE 0 
		END  AS HasEddPASFlag
			
	  ,CASE 
			WHEN PddSequence = 1 THEN 1 
			WHEN tbl_BDhis.[Pdd] IS NULL THEN 1
			ELSE 0 
		END  AS CurrentRecordFlag
      ,tbl_BDspells.[SourcePatientID]
      ,tbl_BDspells.[SourceAEEncounterNo]
      ,sourcepatientcategorycode
      ,tbl_BDspells.[AdmitDate]
      ,tbl_BDspells.[DischargeDate]
      ,tbl_BDspells.[CurrentWard]
      ,tbl_BDspells.[FacilityID]
      ,tbl_BDspells.[AEDepartureTime]
      ,tbl_BDspells.[CurrentSpecialty]
      ,tbl_BDspells.[CurrentConsulant]
      ,tbl_BDspells.[CurrentLocation]
      ,tbl_BDspells.SourceAPCSiteCode AS Sitecode
       ,coalesce (
				tbl_BDspells.[FirstEdd]
				,SourceAPCEdd
				,NULL
				) AS [FirstEdd]
	   ,coalesce (
				tbl_BDspells.[LastEdd]
				,SourceAPCEdd
				,NULL
				) AS [LastEdd]
      ,coalesce (
				tbl_BDhis.[Pdd]
				,SourceAPCEdd
				,NULL
				) AS [Edd]
      ,dateadd(day, datediff(day, 0, tbl_BDhis.[PddCreated]), 0)AS EddCreated
      ,tbl_BDhis.[PddReasonCode] AS EDDReasoncode
      ,coalesce (CASE
					WHEN DATEDIFF(hour, AdmitDatetime,tbl_BDhis.[PddCreated]) <=24	THEN 1
					WHEN DATEDIFF(hour, AdmitDatetime,tbl_BDhis.[PddCreated]) >24	THEN 0
					END 
				,NULL
				)AS EddEnteredWithin24hrs
					
      ,tbl_BDhis.[prev_Pdd] AS prev_Edd
      ,tbl_BDspells.SourceRTTPathwayCondition
      ,DirectorateCode
      ,SourceEncounterNo
     

FROM (SELECT DISTINCT
	SourceSpellNo
	,SourcePatientID
	,SourceEncounterNo
	,SourceAEEncounterNo
	,AdmitDate
	,AdmitDatetime
	,DischargeDate
	,DischargeDatetime
	,CurrentWard
	,FacilityID
	,AEDepartureTime 
	,CurrentSpecialty
	,CurrentConsulant
	,CurrentLocation
	,FirstEdd
	,LastEdd
	,dateadd(day, datediff(day, 0, SourceAPCEdd), 0)AS SourceAPCEdd
	,SourceAPCSiteCode
	,sourcepatientcategorycode
	,SourceRTTPathwayCondition
	,DirectorateCode
	
	FROM warehouse.EDD.spells
	
	WHERE SourceAPCManagementIntentionCode <>'R'
		AND spells.CurrentWard NOT IN ('47','64','64A','64B','67','67A','HM')
		AND NOT (spells.CurrentWard = 'ESTU' AND DATEDIFF(HOUR,spells.AdmitDatetime,spells.DischargeDatetime) <6)
  ) tbl_BDspells
LEFT OUTER JOIN Warehouse.Bedman.PddHistory tbl_BDhis ON tbl_BDspells.SourceSpellNo = tbl_BDhis.SourceSpellNo
	
)tbl_factEDD


/*

	
select
	 @from = min(EncounterFact.EncounterStartDate)
	,@to = max(EncounterFact.EncounterEndDate)
from
	dbo.FactAE EncounterFact
where
	EncounterFact.EncounterEndDate is not null


exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactBedman', @Stats, @StartTime
*/

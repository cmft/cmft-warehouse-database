﻿
CREATE procedure [dbo].[BuildAPCDurationBase]
as

declare @Counter int 
declare @Extent int

select @Extent =
	(
	select
		max(LOS)
	from
		BaseAPC
	)
select @Counter = 0

while @Counter <= @Extent
   begin
        if not exists (select * from APCDurationBase where DurationCode = @Counter)
            insert into APCDurationBase
			(
			 DurationCode
			,OccupiedBedDaysCode
			)
			values
			(
			 @Counter
			,@Counter + 1
			)

        select @Counter = @Counter + 1

    end


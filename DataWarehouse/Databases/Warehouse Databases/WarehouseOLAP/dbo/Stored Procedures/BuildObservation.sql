﻿
CREATE proc [dbo].[BuildObservation] as 

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

exec dbo.BuildObservationAgeBase
exec dbo.BuildObservationDurationBase
--exec dbo.BuildObservationPatientFlagDurationBase


exec dbo.BuildBaseObservationAdmission
exec dbo.BuildBaseObservationAlert
exec dbo.BuildBaseObservationObservationSet
exec dbo.BuildBaseObservationSignificantEvent
exec dbo.BuildBaseObservationPatientFlag 

-- Resus dataset included as part of model
exec dbo.BuildBaseResus

exec dbo.BuildFactObservationAdmission
exec dbo.BuildFactObservationAlert
exec dbo.BuildFactObservationAlertChain
exec dbo.BuildFactObservationAPCAlert
exec dbo.BuildFactObservationAPCAlertChain
exec dbo.BuildFactObservationAPCObservationSet
exec dbo.BuildFactObservationCombined
exec dbo.BuildFactObservationObservationSet
exec dbo.BuildFactObservationSignificantEvent
exec dbo.BuildFactObservationPatientFlag

-- Resus included as part of model
exec dbo.BuildFactResus
exec dbo.BuildFactAPCResus


select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	
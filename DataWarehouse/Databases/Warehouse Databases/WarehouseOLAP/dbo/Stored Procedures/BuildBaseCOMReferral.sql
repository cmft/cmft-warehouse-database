﻿
CREATE PROCEDURE [dbo].[BuildBaseCOMReferral] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()


TRUNCATE TABLE dbo.BaseCOMReferral

-- Insert new rows into encounter destination table
INSERT INTO dbo.BaseCOMReferral
(
     SourceUniqueID --[REFRL_REFNO]
	,ReferredBySpecialtyID --[REFBY_SPECT_REFNO]
	,ReferredToSpecialtyID --[REFTO_SPECT_REFNO]
	,SourceofReferralID --[SORRF_REFNO]
	,ReferralReceivedDate --[RECVD_DTTM]
	,ReasonforReferralID --[REASN_REFNO]
	,CancelledDate --[CANCD_DTTM]
	,CancelledReasonID --[CANRS_REFNO]
	,ReferralSentDate --[SENT_DTTM]
	,Urgency --[URGNC_REFNO]
	,Priority --[PRITY_REFNO]
	,AuthoriedDate --[AUTHR_DTTM]
	,ReferralClosedDate --[CLOSR_DATE]
	,ClosureReasonID --CLORS_REFNO
	,ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]
	,ReferredByStaffTeamID --[REFBY_STEAM_REFNO]
	,ReferredByHealthOrgID --[REFBY_HEORG_REFNO]
	,ReferredToProfessionalCarerID --[REFTO_PROCA_REFNO]
	,ReferredToHealthOrgID --[REFTO_HEORG_REFNO]
	,ReferredToStaffTeamID --[REFTO_STEAM_REFNO]
	,OutcomeOfReferral --[RFOCM_REFNO]
	,ReferralStatus --[RSTAT_REFNO]
	,RejectionID --RejectionID--[RJECT_REFNO]
	,TypeofReferralID --[RETYP_REFNO]
	,PatientSourceID 
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientLocalIdentifier
	,PatientPASIdentifier
	,PatientIdentifier
	,PatientFacilityIdentifier
	,PatientTitleID 
	,PatientForename 
	,PatientSurname 
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,ProfessionalCarerCurrentID
	,ProfessionalCarerHealthOrgCurrentID
	,ProfessionalCarerParentHealthOrgCurrentID
	,ProfessionalCarerReferralID 
	,ProfessionalCarerHealthOrgReferralID 
	,ProfessionalCarerParentHealthOrgReferralID 
	,RTTStatusDate --[STATUS_DTTM]
	,RTTPatientPathwayID --[PATNT_PATHWAY_ID]
	,RTTStartTime --[RTT_START_DATE]
	,RTTStartFlag --[RTT_STARTED]
	,RTTStatusID --[RTTST_REFNO]
	,CreatedTime --[CREATE_DTTM]
	,ModifiedTime --[MODIF_DTTM]
	,CreatedByID --[USER_CREATE]
	,ModifiedByID --[USER_MODIF]
	,ArchiveFlag --[ARCHV_FLAG]
	,ParentID  --[PARNT_REFNO]
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
	,RecordStatus  --Status of record either C(current), H(historical increment), d(deleted), or t(test patient)
	,AgeCode
	,RequestedServiceID
)
SELECT
	 SourceUniqueID --[REFRL_REFNO]
	,ReferredBySpecialtyID --[REFBY_SPECT_REFNO]
	,ReferredToSpecialtyID --[REFTO_SPECT_REFNO]
	,SourceofReferralID --[SORRF_REFNO]
	,ReferralReceivedDate --[RECVD_DTTM]
	,ReasonforReferralID --[REASN_REFNO]
	,CancelledDate --[CANCD_DTTM]
	,CancelledReasonID --[CANRS_REFNO]
	,ReferralSentDate --[SENT_DTTM]
	,Urgency --[URGNC_REFNO]
	,Priority --[PRITY_REFNO]
	,AuthoriedDate --[AUTHR_DTTM]
	,ReferralClosedDate --[CLOSR_DATE]
	,ClosureReasonID --CLORS_REFNO
	,ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]
	,ReferredByStaffTeamID --[REFBY_STEAM_REFNO]
	,ReferredByHealthOrgID --[REFBY_HEORG_REFNO]
	,ReferredToProfessionalCarerID --[REFTO_PROCA_REFNO]
	,ReferredToHealthOrgID --[REFTO_HEORG_REFNO]
	,ReferredToStaffTeamID --[REFTO_STEAM_REFNO]
	,OutcomeOfReferral --[RFOCM_REFNO]
	,ReferralStatus --[RSTAT_REFNO]
	,RejectionID --RejectionID--[RJECT_REFNO]
	,TypeofReferralID --[RETYP_REFNO]
	,PatientSourceID 
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientLocalIdentifier
	,PatientPASIdentifier
	,PatientIdentifier
	,PatientFacilityIdentifier
	,PatientTitleID 
	,PatientForename 
	,PatientSurname 
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,ProfessionalCarerCurrentID
	,ProfessionalCarerHealthOrgCurrentID
	,ProfessionalCarerParentHealthOrgCurrentID
	,ProfessionalCarerReferralID 
	,ProfessionalCarerHealthOrgReferralID 
	,ProfessionalCarerParentHealthOrgReferralID 
	,RTTStatusDate --[STATUS_DTTM]
	,RTTPatientPathwayID --[PATNT_PATHWAY_ID]
	,RTTStartTime --[RTT_START_DATE]
	,RTTStartFlag --[RTT_STARTED]
	,RTTStatusID --[RTTST_REFNO]
	,CreatedTime --[CREATE_DTTM]
	,ModifiedTime --[MODIF_DTTM]
	,CreatedByID --[USER_CREATE]
	,ModifiedByID --[USER_MODIF]
	,ArchiveFlag --[ARCHV_FLAG]
	,ParentID  --[PARNT_REFNO]
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
	,RecordStatus  --Status of record either C(current), H(historical increment), d(deleted), or t(test patient)
	,AgeCode =
	case
	when datediff(day, PatientDateOfBirth, Referral.ReferralReceivedDate) is null then 'Age Unknown'
	when datediff(day, PatientDateOfBirth, Referral.ReferralReceivedDate) <= 0 then '00 Days'
	when datediff(day, PatientDateOfBirth, Referral.ReferralReceivedDate) = 1 then '01 Day'
	when datediff(day, PatientDateOfBirth, Referral.ReferralReceivedDate) > 1 and
	datediff(day, PatientDateOfBirth, Referral.ReferralReceivedDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, PatientDateOfBirth, Referral.ReferralReceivedDate)), 2) + ' Days'
	
	when datediff(day, PatientDateOfBirth, Referral.ReferralReceivedDate) > 28 and
	datediff(month, PatientDateOfBirth, Referral.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Referral.ReferralReceivedDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, PatientDateOfBirth, Referral.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Referral.ReferralReceivedDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, PatientDateOfBirth, Referral.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Referral.ReferralReceivedDate) then 1 else 0 end > 1 and
	 datediff(month, PatientDateOfBirth, Referral.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Referral.ReferralReceivedDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, PatientDateOfBirth,Referral.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Referral.ReferralReceivedDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, PatientDateOfBirth, Referral.ReferralReceivedDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, Referral.ReferralReceivedDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, Referral.ReferralReceivedDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, Referral.ReferralReceivedDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, PatientDateOfBirth, Referral.ReferralReceivedDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, Referral.ReferralReceivedDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, Referral.ReferralReceivedDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, Referral.ReferralReceivedDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end
	,RequestedServiceID
FROM
		warehouse.COM.Referral
WHERE
		ArchiveFlag = 'N'
    AND RecordStatus = 'C'
	AND 
		(
			ReferralReceivedDate >= '01 Apr 2008'
		OR
			ReferralClosedDate >= '01 Apr 2008'
		)

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec WriteAuditLogEvent 'BuildOLAPBaseCOMReferral', @Stats, @StartTime

﻿CREATE procedure [dbo].[BuildFactOP] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactOP

insert into dbo.FactOP
(
	 EncounterRecno
	,EncounterDate
	,MetricCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringSpecialtyCode
	,PracticeCode
	,PCTCode
	,AgeCode
	,ClinicCode
	,Cases
	,SiteCode
	,SourceOfReferralCode
	,BookingTypeCode
	,HRGCode
	,LengthOfWait
	,IsWardAttender
	,RTTActivity
	,RTTBreachStatusCode
	,RTTTreated
	,PrimaryOperationCode
	,DirectorateCode
	,SexCode
)
SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FATT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode in ('1', '5', '6')
and	FirstAttendanceFlag = '1'
--and	AppointmentTypeCode <> 'ZZZ'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RATT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode in ('1', '5', '6')
and	(
		FirstAttendanceFlag != '1'
--	or	AppointmentTypeCode = 'ZZZ'
	)

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FDNA' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode in ('3', '7')
and	FirstAttendanceFlag = '1'
--and	AppointmentTypeCode <> 'ZZZ'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RDNA' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode in ('3', '7')
and	(
		FirstAttendanceFlag != '1'
--	or	AppointmentTypeCode = 'ZZZ'
	)

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FCANPAT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode = '2'
and	FirstAttendanceFlag = '1'
--and	AppointmentTypeCode <> 'ZZZ'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RCANPAT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode = '2'
and	(
		FirstAttendanceFlag != '1'
--	or	AppointmentTypeCode = 'ZZZ'
	)

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FCANPROV' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode = '4'
and	FirstAttendanceFlag = '1'
--and	AppointmentTypeCode <> 'ZZZ'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RCANPROV' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode = '4'
and	(
		FirstAttendanceFlag != '1'
--	or	AppointmentTypeCode = 'ZZZ'
	)

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FCANCEL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode in ('2', '4')
and	FirstAttendanceFlag = '1'
--and	AppointmentTypeCode <> 'ZZZ'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RCANCEL' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	DNACode in ('2', '4')
and	(
		FirstAttendanceFlag != '1'
--	or	AppointmentTypeCode = 'ZZZ'
	)

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FUNKATT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	coalesce(DNACode, '9') not in ('1', '2', '3', '4', '5', '6', '7')
and	FirstAttendanceFlag = '1'
--and	AppointmentTypeCode <> 'ZZZ'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RUNKATT' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	coalesce(DNACode, '9') not in ('2', '3', '4', '5', '6', '7')
and	(
		FirstAttendanceFlag != '1'
--	or	AppointmentTypeCode = 'ZZZ'
	)

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'CHKAPP' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'FAPP' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	FirstAttendanceFlag = '1'
--and	AppointmentTypeCode <> 'ZZZ'

union all

SELECT
	 Encounter.EncounterRecno
	,AppointmentDate EncounterDate
	,'RAPP' MetricCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.ClinicCode + case when Encounter.IsWardAttender = 1 then '|W' else '' end
	,Encounter.Cases
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.BookingTypeCode
	,Encounter.HRGCode
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.RTTTreated
	,Encounter.PrimaryOperationCode
	,DirectorateCode
	,Encounter.SexCode
FROM
	dbo.BaseOP Encounter
where
	FirstAttendanceFlag != '1'
--or	AppointmentTypeCode = 'ZZZ'


select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.EncounterDate)
	,@to = max(EncounterFact.EncounterDate)
from
	dbo.FactOP EncounterFact
where
	EncounterFact.EncounterDate is not null


exec BuildOlapClinic
exec BuildOlapPractice 'OP'
exec BuildCalendarBasePair @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactOP', @Stats, @StartTime

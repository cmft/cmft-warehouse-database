﻿


CREATE proc [dbo].[BuildBaseResus]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

select @StartTime = getdate()


truncate table dbo.BaseResus

insert into dbo.BaseResus
 (
EncounterRecno
,SourceUniqueID
,NHSNumber
,CasenoteNumber
,PatientForename
,PatientSurname
,DateOfBirth
,Sex
,ConsultantCode
,Ethnicity
,PatientGroup
,AdmissionReason
,AdmissionDate
,EventDate
,LocationCode
,Witnessed
,Monitored
,ALSInterventionNone
,IVAccess
,IVMedication
,ECGMonitor
,TrachealTube
,OtherAirwayDevice
,MechanicalVentilation
,ImplantDefibCardioverter
,IntraArterialCatheter
,ImmediateCause
,ImmediateCauseOther
,ResuscitationAttempt
,Conscious
,Breathing
,Pulse
,PresentingECG
,CPRStoppedTime
,CPRStoppedReason
,SpontaneousCirculation
,ROSCTime
,ROSCTimeOfDayCode
,TimeOfDeath
,DeathTimeOfDayCode
,UnsustainedROSC
,CollapseTime
,CollapseTimeOfDayCode
,CPRCalloutTime
,CPRCalloutTimeOfDayCode
,CPRArriveTime
,CPRArriveTimeOfDayCode
,ArrestConfirmTime
,ArrestConfirmTimeOfDayCode
,CPRStartTime
,CPRStartTimeOfDayCode
,AwakeningTime
,AwakeningTimeOfDayCode
,AwakeningDate
,DischargeOrDeath
,DischargeDate
,DischargeDestination
,DateOfDeath
,CauseOfDeath
,CallOutType
,CallOutTypeID
,Comment
,ebmB
,ebmB2
,ebmB3
,ebmB4
,ebmB5
,ebmPAI
,AgeID
,InterfaceCode
,Cases
)

select
	EncounterRecno
	,SourceUniqueID
	,NHSNumber
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,Sex
	,ConsultantCode
	,Ethnicity
	,PatientGroup
	,AdmissionReason
	,AdmissionDate = 
		coalesce(
				AdmissionDate
				,'1 jan 1900'
			)
	,EventDate = 
		coalesce(
				EventDate
				,'1 jan 1900'
			)
	,LocationCode = 
			coalesce(
				LocationCode
				,'N/A'
			)
	,Witnessed
	,Monitored
	,ALSInterventionNone
	,IVAccess
	,IVMedication
	,ECGMonitor
	,TrachealTube
	,OtherAirwayDevice
	,MechanicalVentilation
	,ImplantDefibCardioverter
	,IntraArterialCatheter
	,ImmediateCause
	,ImmediateCauseOther
	,ResuscitationAttempt
	,Conscious
	,Breathing
	,Pulse
	,PresentingECG
	,CPRStoppedTime = 
		coalesce(
				CPRStoppedTime
				,'1 jan 1900'
			)
	,CPRStoppedReason
	,SpontaneousCirculation
	,ROSCTime = 
		coalesce(
				ROSCTime
				,'1 jan 1900'
			)
	,ROSCTimeOfDayCode =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, ROSCTime), 0)
					,ROSCTime
				)
				,-1
			)
	,TimeOfDeath = 
		coalesce(
				TimeOfDeath
				,'1 jan 1900'
			)
	,DeathTimeOfDayCode =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, TimeOfDeath), 0)
					,TimeOfDeath
				)
				,-1
			)
	,UnsustainedROSC
	,CollapseTime = 
		coalesce(
				CollapseTime
				,'1 jan 1900'
			)
	,CollapseTimeOfDayCode =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, CollapseTime), 0)
					,CollapseTime
				)
				,-1
			)
	,CPRCalloutTime = 
		coalesce(
				CPRCalloutTime
				,'1 jan 1900'
			)
	,CPRCalloutTimeOfDayCode =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, CPRCalloutTime), 0)
					,CPRCalloutTime
				)
				,-1
			)
	,CPRArriveTime = 
		coalesce(
				CPRArriveTime
				,'1 jan 1900'
			)
	,CPRArriveTimeOfDayCode =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, CPRArriveTime), 0)
					,CPRArriveTime
				)
				,-1
			)
	,ArrestConfirmTime = 
		coalesce(
				ArrestConfirmTime
				,'1 jan 1900'
			)
	,ArrestConfirmTimeOfDayCode =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, ArrestConfirmTime), 0)
					,ArrestConfirmTime
				)
				,-1
			)
	,CPRStartTime = 
		coalesce(
				CPRStartTime
				,'1 jan 1900'
			)
	,CPRStartTimeOfDayCode =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, CPRStartTime), 0)
					,CPRStartTime
				)
				,-1
			)
	,AwakeningTime = 
		coalesce(
				AwakeningTime
				,'1 jan 1900'
			)
	,AwakeningTimeOfDayCode =
			coalesce(
				datediff(
					 minute
					,dateadd(day, datediff(day, 0, AwakeningTime), 0)
					,AwakeningTime
				)
				,-1
			)
	,AwakeningDate = 
		coalesce(
				AwakeningDate
				,'1 jan 1900'
			)
	,DischargeOrDeath
	,DischargeDate = 
		coalesce(
				DischargeDate
				,'1 jan 1900'
			)
	,DischargeDestination
	,DateOfDeath = 
		coalesce(
				DateOfDeath
				,'1 jan 1900'
			)
	,CauseOfDeath
	,Encounter.CallOutType
	,CallOutTypeID = 
		coalesce(
				CallOutType.CallOutTypeID
				,-1
			)
	,Comment
	,ebmB
	,ebmB2
	,ebmB3
	,ebmB4
	,ebmB5
	,ebmPAI
	,AgeID = 
			case
			when DateOfBirth > EventDate
			then 0
			when (month(EventDate) * 100) + day(EventDate) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
			then datediff(year, DateOfBirth, EventDate)
			else datediff(year, DateOfBirth, EventDate) - 1
			end
	,InterfaceCode
	,Cases = 1
from
	Warehouse.Resus.Encounter
left join Warehouse.Resus.CallOutType
on	Encounter.CallOutType = CallOutType.CallOutType



select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime
	



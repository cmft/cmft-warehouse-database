﻿
CREATE procedure [dbo].[BuildBaseAE] as


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.BaseAE

insert into dbo.BaseAE
(
	 EncounterRecno
	,SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,CarerSupportIndicator
	,RegisteredGpCode
	,RegisteredPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,TreatmentCodeFirst
	,TreatmentCodeSecond
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,SiteCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode
	,PCTCode
	,ResidencePCTCode
	,InvestigationCodeList
	,TriageCategoryCode
	,PresentingProblem
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,EthnicCategoryCode
	,ReferredToSpecialtyCode
	,DecisionToAdmitTime
	,DischargeDestinationCode
	,RegisteredTime
	,TransportRequestTime
	,TransportAvailableTime
	,AscribeLeftDeptTime
	,CDULeftDepartmentTime
	,PCDULeftDepartmentTime
	,TreatmentDateFirst
	,TreatmentDateSecond
	,SourceAttendanceDisposalCode
	,AmbulanceArrivalTime
	,AmbulanceCrewPRF
	,UnplannedReattend7Day
	,ArrivalTimeAdjusted
	,ClinicalAssessmentTime
	,LevelOfCareCode
	,EncounterBreachStatusCode
	,EncounterStartTimeOfDay
	,EncounterEndTimeOfDay
	,EncounterStartDate
	,EncounterEndDate
	,EncounterDurationMinutes
	,AgeCode
	,LeftWithoutBeingSeenCases
	,HRGCode
	,Reportable
	,CareGroup
	,AlcoholLocation
	,AlcoholInPast12Hours
	)
select DISTINCT
	 AEEncounter.EncounterRecno
	,AEEncounter.SourceUniqueID
	,AEEncounter.UniqueBookingReferenceNo
	,AEEncounter.PathwayId
	,AEEncounter.PathwayIdIssuerCode
	,AEEncounter.RTTStatusCode
	,AEEncounter.RTTStartDate
	,AEEncounter.RTTEndDate
	,AEEncounter.DistrictNo
	,AEEncounter.TrustNo
	,AEEncounter.CasenoteNo
	,AEEncounter.DistrictNoOrganisationCode
	,AEEncounter.NHSNumber
	,AEEncounter.NHSNumberStatusId
	,AEEncounter.PatientTitle
	,AEEncounter.PatientForename
	,AEEncounter.PatientSurname
	,AEEncounter.PatientAddress1
	,AEEncounter.PatientAddress2
	,AEEncounter.PatientAddress3
	,AEEncounter.PatientAddress4
	,AEEncounter.Postcode
	,AEEncounter.DateOfBirth
	,AEEncounter.DateOfDeath
	,AEEncounter.SexCode
	,AEEncounter.CarerSupportIndicator
	,AEEncounter.RegisteredGpCode

	,RegisteredPracticeCode =
		coalesce(AEEncounter.RegisteredGpPracticeCode, 'X99999')

	,AEEncounter.AttendanceNumber
	,AEEncounter.ArrivalModeCode
	,AEEncounter.AttendanceCategoryCode

	,AttendanceDisposalCode =
		coalesce(
			AEEncounter.SourceAttendanceDisposalCode
			,-1
		)

	,AEEncounter.IncidentLocationTypeCode
	,AEEncounter.PatientGroupCode
	,AEEncounter.SourceOfReferralCode
	,AEEncounter.ArrivalDate
	,AEEncounter.ArrivalTime
	,AEEncounter.AgeOnArrival
	,AEEncounter.InitialAssessmentTime
	,AEEncounter.SeenForTreatmentTime
	,AEEncounter.AttendanceConclusionTime
	,AEEncounter.DepartureTime
	,AEEncounter.CommissioningSerialNo
	,AEEncounter.NHSServiceAgreementLineNo
	,AEEncounter.ProviderReferenceNo
	,AEEncounter.CommissionerReferenceNo
	,AEEncounter.ProviderCode
	,AEEncounter.CommissionerCode
	,AEEncounter.StaffMemberCode
	,AEEncounter.InvestigationCodeFirst
	,AEEncounter.InvestigationCodeSecond
	,AEEncounter.DiagnosisCodeFirst
	,AEEncounter.DiagnosisCodeSecond
	,AEEncounter.TreatmentCodeFirst
	,AEEncounter.TreatmentCodeSecond
	,AEEncounter.PASHRGCode
	,AEEncounter.HRGVersionCode
	,AEEncounter.PASDGVPCode
	,AEEncounter.SiteCode
	,AEEncounter.Created
	,AEEncounter.Updated
	,AEEncounter.ByWhom
	,AEEncounter.InterfaceCode
	,AEEncounter.PCTCode
	,AEEncounter.ResidencePCTCode
	,AEEncounter.InvestigationCodeList
	,AEEncounter.TriageCategoryCode
	,AEEncounter.PresentingProblem
	,AEEncounter.ToXrayTime
	,AEEncounter.FromXrayTime
	,AEEncounter.ToSpecialtyTime
	,AEEncounter.SeenBySpecialtyTime
	,AEEncounter.EthnicCategoryCode

	,ReferredToSpecialtyCode =
		coalesce(
			 AEEncounter.ReferredToSpecialtyCode
			,0
		)

	,AEEncounter.DecisionToAdmitTime
	,AEEncounter.DischargeDestinationCode
	,AEEncounter.RegisteredTime
	,AEEncounter.TransportRequestTime
	,AEEncounter.TransportAvailableTime
	,AEEncounter.AscribeLeftDeptTime
	,AEEncounter.CDULeftDepartmentTime
	,AEEncounter.PCDULeftDepartmentTime
	,AEEncounter.TreatmentDateFirst
	,AEEncounter.TreatmentDateSecond
	,AEEncounter.SourceAttendanceDisposalCode
	,AEEncounter.AmbulanceArrivalTime
	,AEEncounter.AmbulanceCrewPRF

	,UnplannedReattend7Day =
		UnplannedReattend.UnplannedReattend7Day

	,AEEncounter.ArrivalTimeAdjusted
	,AEEncounter.ClinicalAssessmentTime


	,LevelOfCareCode =
		case
		when
			AEEncounter.AttendanceDisposalCode = '01'
		then 2 --high
		when
			AEInvestigation.InvestigationDate is null
		and	AEEncounter.ToSpecialtyTime is null
		then 0 --low
		else 1 --medium
		end

	,EncounterBreachStatusCode = 
		case
		when AEEncounter.DepartureTime is null then 'X'
		when EncounterBreach.BreachValue is null
		then 'N'
		else 'B'
		end

	,EncounterStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
				,AEEncounter.ArrivalTime
			)
			,-1
		)

	,EncounterEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
				,AEEncounter.DepartureTime
			)
			,-1
		)

	,EncounterStartDate =
		dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)

	,EncounterEndDate = 
		coalesce(
			 dateadd(day, datediff(day, 0, AEEncounter.PCDULeftDepartmentTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounter.CDULeftDepartmentTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
		)

	,EncounterDurationMinutes =
		datediff(
			 minute
			,AEEncounter.ArrivalTime
			,AEEncounter.DepartureTime
		)

	,AgeCode =
		case
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) is null then 'Age Unknown'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 0 then '00 Days'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) = 1 then '01 Day'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 1 and
		datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate)), 2) + ' Days'
		
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 28 and
		datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end > 1 and
		 datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, AEEncounter.DateOfBirth,AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
		(
		case 
		when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
		or
			(
				datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
			And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
		(
		case 
		when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
		or
			(
				datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
			And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
			) then 1 else 0 end
		)), 2) + ' Years'

	end

	,LeftWithoutBeingSeenCases =
		case
		when
				(
					SourceAttendanceDisposalCode in ('10804' , '6193', '17369', '17368', '17759', '17901') -- GS 2011-06-02 change additional codes 17369 and 17368 added
																										   -- GS 2011-10-12 change additional codes '17759','17901'(both Left dept before being treated) added
				or	LookupBase.Lkp_Name in ( 'Did Not Wait' , 'Self Discharge') 
				)
			and	SeenForTreatmentTime is null
			and ClinicalAssessmentTime is null
		then 1
		else 0
		end

	,HRG4AEEncounter.HRGCode

	,Reportable = coalesce(AEEncounter.Reportable , 1)
	,CareGroup = CareGroup.Reference
	,AEEncounter.AlcoholLocation
	,AEEncounter.AlcoholInPast12Hours
from
	Warehouse.AE.Encounter AEEncounter

left join Warehouse.AE.StageBreach EncounterBreach
on	EncounterBreach.StageCode = 'INDEPARTMENT'
and
	datediff(
		 minute
		,AEEncounter.ArrivalTime
		--,AEEncounter.AttendanceConclusionTime
		,coalesce(
				 AEEncounter.PCDULeftDepartmentTime
				,AEEncounter.CDULeftDepartmentTime
				,AEEncounter.DepartureTime
		)
	)
	 > EncounterBreach.BreachValue


left join Warehouse.AE.Investigation AEInvestigation
on	AEInvestigation.AESourceUniqueID = AEEncounter.SourceUniqueID
and	AEInvestigation.SequenceNo = 1

left join Warehouse.AE.HRG4Encounter HRG4AEEncounter
on	HRG4AEEncounter.EncounterRecno = AEEncounter.EncounterRecno

-- determine which attendances follow a previous unplanned attendance within seven days
left join (
	select
		 SourceUniqueID
		,UnplannedReattend7Day = 1
	from
		Warehouse.AE.Encounter AEEncounter
	where
		exists
			(
			select
				1
			from
				Warehouse.AE.Encounter PreviousAEEncounter
			where
				PreviousAEEncounter.DistrictNo = AEEncounter.DistrictNo
			and	PreviousAEEncounter.SiteCode = AEEncounter.SiteCode
			and	coalesce(
					 dateadd(day, datediff(day, 0, PreviousAEEncounter.PCDULeftDepartmentTime), 0)
					,dateadd(day, datediff(day, 0, PreviousAEEncounter.CDULeftDepartmentTime), 0)
					,dateadd(day, datediff(day, 0, PreviousAEEncounter.DepartureTime), 0)
					,dateadd(day, datediff(day, 0, PreviousAEEncounter.ArrivalTime), 0)
				) between dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime) - 7 , 0) and AEEncounter.ArrivalTime
			and	coalesce(
					 PreviousAEEncounter.PCDULeftDepartmentTime
					,PreviousAEEncounter.CDULeftDepartmentTime
					,PreviousAEEncounter.DepartureTime
					,PreviousAEEncounter.ArrivalTime
				) < AEEncounter.ArrivalTime
			and	PreviousAEEncounter.AttendanceCategoryCode != 2
			-- exclude children's division from the measurement of reattendance rate
			and	PreviousAEEncounter.SiteCode != 'RW3RC'
			)
	) UnplannedReattend

on	UnplannedReattend.SourceUniqueID = AEEncounter.SourceUniqueID
and	AEEncounter.AttendanceCategoryCode != 2

left join Warehouse.AE.LookupBase
on	LookupBase.Lkp_ID = AEEncounter.SourceAttendanceDisposalCode

LEFT JOIN Warehouse.AE.Reference CareGroup
ON  CareGroup.ReferenceCode = AEEncounter.SourceCareGroupCode
AND CareGroup.ReferenceParentCode = '5713'

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildBaseAE', @Stats, @StartTime


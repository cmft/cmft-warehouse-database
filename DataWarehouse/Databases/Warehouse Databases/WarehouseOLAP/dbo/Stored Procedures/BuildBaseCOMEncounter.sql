﻿
CREATE PROCEDURE dbo.BuildBaseCOMEncounter as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()


TRUNCATE TABLE dbo.BaseCOMEncounter

-- Insert new rows into encounter destination table
INSERT INTO dbo.BaseCOMEncounter
(
	 EncounterRecNo
    ,SourceEncounterID
    ,SourceUniqueID --[SCHDL_REFNO]
	,SpecialtyID --[SPECT_REFNO]
	,StaffTeamID--[STEAM_REFNO]
	,ProfessionalCarerID--[PROCA_REFNO]
	,SeenByProfessionalCarerID --[SEENBY_PROCA_REFNO]
	,EncounterProfessionalCarerID
	,StartDate
	,StartTime
	,EndTime
	,ArrivedTime  --[ARRIVED_DTTM]
	,SeenTime  --[SEEN_DTTM]
	,DepartedTime --[DEPARTED_DTTM]
	,AttendedID --[ATTND_REFNO] for use in clinic activity
	,OutcomeID  --[SCOCM_REFNO] for use in contact activity
	,EncounterOutcomeCode
	,EncounterDuration
	,ReferralID  --[REFRL_REFNO]
	,ProfessionalCarerEpisodeID --[PRCAE_REFNO]
	,PatientSourceID --[PATNT_REFNO]
	,PatientSourceSystemUniqueID
	,PatientNHSNumber 
	,PatientNHSNumberStatusIndicator 
	,PatientIDLocalIdentifier 
	,PatientIDPASNumber 
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath 
	,PatientSexID 
	,PatientEthnicGroupID 
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,EncounterTypeID
	,ScheduleTypeID --[SCTYP_REFNO]  appointments 1470 Contacts 1468 
	,ScheduleReasonID --[REASN_REFNO]
	,CanceledReasonID --[CANCR_REFNO]
	,CanceledTime --[CANCR_DTTM]
	,CancelledByID --[CANCB_REFNO]
	,MoveReasonID  --[MOVRN_REFNO]
	,MoveTime --[MOVE_DTTM]
	,ServicePointID  --[SPONT_REFNO]
	,ServicePointSessionID --[SPSSN_REFNO]
	,MoveCount --[MOVE_COUNT]
	,LocationTypeID --[LOTYP_REFNO]
	,LocationDescription --[LOCATION]
	,LocationTypeHealthOrgID
	,EncounterLocationID
	,ServicePointStaysID --[SSTAY_REFNO]
	,WaitingListID --[WLIST_REFNO]
	,PlannedAttendees --[PLANNED_ATTENDEES]
	,ActualAttendees --[ACTUAL_ATTENDEES]
	,ProviderSpellID --[PRVSP_REFNO]
	,RTTStatusID --[RTTST_REFNO]
	,EarliestReasinableOfferTime --[ERO_DTTM]
	,EncounterInterventionsCount
	,CreatedTime --[CREATE_DTTM]
	,ModifiedTime --[MODIF_DTTM]
	,CreatedBy --[USER_CREATE]
	,ModifiedBy --[USER_MODIF]
	,Archived --[ARCHV_FLAG]
	,ParentID --[PARNT_REFNO]
	,HealthOrgOwnerID --[OWNER_HEORG_REFNO]
	,RecordStatus  --Status of record either C(current), H(historical increment), d(deleted), or t(test patient)
	,JointActivityFlag
	,StartTimeOfDay
	,AgeCode
	,SexCode
	,DerivedFirstAttendanceFlag
	,Comment
	,ReferralReceivedTime
	,ReferralReceivedDate
	,ReferralClosedTime
	,ReferralClosedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,CommissionerCode 
	,PctOfResidence  
	,LocalAuthorityCode 
	,DistrictOfResidence 
)
SELECT
	 EncounterRecNo
	,SourceEncounterID 
	,SourceUniqueID --[SCHDL_REFNO]
	,SpecialtyID --[SPECT_REFNO]
	,StaffTeamID--[STEAM_REFNO]
	,ProfessionalCarerID--[PROCA_REFNO]
	,SeenByProfessionalCarerID --[SEENBY_PROCA_REFNO]
	,EncounterProfessionalCarerID
	,StartDate
	,StartTime
	,EndTime
	,ArrivedTime  --[ARRIVED_DTTM]
	,SeenTime  --[SEEN_DTTM]
	,DepartedTime --[DEPARTED_DTTM]
	,AttendedID --[ATTND_REFNO] for use in clinic activity
	,OutcomeID  --[SCOCM_REFNO] for use in contact activity
	,EncounterOutcomeCode
	,EncounterDuration
	,ReferralID  --[REFRL_REFNO]
	,ProfessionalCarerEpisodeID --[PRCAE_REFNO]
	,PatientSourceID --[PATNT_REFNO]
	,PatientSourceSystemUniqueID
	,PatientNHSNumber 
	,PatientNHSNumberStatusIndicator 
	,PatientIDLocalIdentifier 
	,PatientIDPASNumber 
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath 
	,PatientSexID 
	,PatientEthnicGroupID 
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode = 
			case 
				when PatientEncounterRegisteredPracticeCode = '5NT' then 'V81999'
				else isnull(PatientEncounterRegisteredPracticeCode,'V81999')
			end
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,EncounterTypeID
	,ScheduleTypeID --[SCTYP_REFNO]  appointments 1470 Contacts 1468 
	,ScheduleReasonID --[REASN_REFNO]
	,CanceledReasonID --[CANCR_REFNO]
	,CanceledTime --[CANCR_DTTM]
	,CancelledByID --[CANCB_REFNO]
	,MoveReasonID  --[MOVRN_REFNO]
	,MoveTime --[MOVE_DTTM]
	,ServicePointID  --[SPONT_REFNO]
	,ServicePointSessionID --[SPSSN_REFNO]
	,MoveCount --[MOVE_COUNT]
	,LocationTypeID --[LOTYP_REFNO]
	,LocationDescription --[LOCATION]
	,LocationTypeHealthOrgID
	,EncounterLocationID
	,ServicePointStaysID --[SSTAY_REFNO]
	,WaitingListID --[WLIST_REFNO]
	,PlannedAttendees --[PLANNED_ATTENDEES]
	,ActualAttendees --[ACTUAL_ATTENDEES]
	,ProviderSpellID --[PRVSP_REFNO]
	,RTTStatusID --[RTTST_REFNO]
	,EarliestReasinableOfferTime --[ERO_DTTM]
	,EncounterInterventionsCount
	,CreatedTime --[CREATE_DTTM]
	,ModifiedTime --[MODIF_DTTM]
	,CreatedBy --[USER_CREATE]
	,ModifiedBy --[USER_MODIF]
	,Archived --[ARCHV_FLAG]
	,ParentID --[PARNT_REFNO]
	,HealthOrgOwnerID --[OWNER_HEORG_REFNO]
	,RecordStatus  --Status of record either C(current), H(historical increment), d(deleted), or t(test patient)
	,JointActivityFlag
	,StartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, StartTime), 0)
				,StartTime
			)
			,-1
		)
	,AgeCode =
	case
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) is null then 'Age Unknown'
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) <= 0 then '00 Days'
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) = 1 then '01 Day'
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) > 1 and
	datediff(day, PatientDateOfBirth, Encounter.StartDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, PatientDateOfBirth, Encounter.StartDate)), 2) + ' Days'
	
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) > 28 and
	datediff(month, PatientDateOfBirth, Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, PatientDateOfBirth, Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, PatientDateOfBirth, Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end > 1 and
	 datediff(month, PatientDateOfBirth, Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, PatientDateOfBirth,Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, PatientDateOfBirth, Encounter.StartDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, Encounter.StartDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, Encounter.StartDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, Encounter.StartDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, PatientDateOfBirth, Encounter.StartDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, Encounter.StartDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, Encounter.StartDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, Encounter.StartDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end
	,SexCode = NULL
	,DerivedFirstAttendanceFlag
	,Comment
	,ReferralReceivedTime
	,ReferralReceivedDate
	,ReferralClosedTime
	,ReferralClosedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,CommissionerCode 
	,PctOfResidence  
	,LocalAuthorityCode 
	,DistrictOfResidence 
FROM
		warehouse.COM.Encounter
WHERE
		Archived = 'N'
    AND RecordStatus = 'C'
    AND StartDate >= '01 Apr 2008'
    and ReferralID is not null

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec WriteAuditLogEvent 'LoadOLAPBaseCOMEncounter', @Stats, @StartTime;

﻿

CREATE PROCEDURE [dbo].[BuildFactCOMCaseLoad] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()


TRUNCATE TABLE dbo.FactCOMCaseLoad

-- Insert new rows into encounter destination table
INSERT INTO dbo.FactCOMCaseLoad
(
     [CaseLoadID]
	,[ReferralID]
	,[RoleTypeID]
	,[StatusID]
	,CaseloadDate
	,MetricCode
	,AllocationReasonID
	,InterventionlevelID
	,Outcome
	,AllocationProfessionalCarerID
	,AllocationSpecialtyID
	,AllocationStaffTeamID
	,ResponsibleHealthOrganisation
	,DischargeReasonID
	,PatientSourceID
	,Cases
	,AgeCode
	,EthnicCategoryID
	,PatientSexID
	
)
SELECT
	 Caseload.[CaseLoadID]
	,Caseload.[ReferralID]
	,Caseload.[RoleTypeID]
	,Caseload.[StatusID]
	,tblCaseloadMetrics.CaseloadDate
	,tblCaseloadMetrics.MetricCode
	,Caseload.AllocationReasonID
	,Caseload.InterventionlevelID
	,Caseload.Outcome
	,Caseload.AllocationProfessionalCarerID
	,AllocationSpecialtyID = ISNULL(Caseload.AllocationSpecialtyID,-1)
	,AllocationStaffTeamID = ISNULL(Caseload.AllocationStaffTeamID,-1)
	,Caseload.ResponsibleHealthOrganisation
	,Caseload.DischargeReasonID
	,Caseload.PatientSourceID
	,Cases = 1
	,AgeCode
	,EthnicCategoryID = ISNULL(PatientEthnicGroupID,-1)
	,PatientSexID = ISNULL(PatientSexID,-1)
FROM
	WarehouseOLAP.dbo.BaseCOMCaseload Caseload

INNER JOIN
	(
	SELECT 
		 CaseLoadID = CaseLoadID
		 
		,CaseLoadDate = AllocationDate --[RECVD_DTTM]

		,MetricCode = 'CLAL'
	
	FROM 
		WarehouseOLAP.dbo.BaseCOMCaseload
	
	WHERE
		AllocationDate IS NOT NULL
	
	UNION ALL
	
	--Referrals Closed
	SELECT 
		 CaseLoadID =CaseLoadID
		 
		,CaseLoadDate =  DischargeDate--[RECVD_DTTM]

		,MetricCode = 'CLDI'
	
	FROM 
		WarehouseOLAP.dbo.BaseCOMCaseload
	
	WHERE
		DischargeDate IS NOT NULL
	

	)tblCaseloadMetrics
	
	
ON Caseload.CaseLoadID = tblCaseloadMetrics.CaseLoadID
	
	
	
SELECT
	 @from = min(Caseload.CaseloadDate)
	,@to = max(Caseload.CaseloadDate)

FROM
	dbo.FactCOMCaseload Caseload
	
WHERE
	Caseload.CaseloadDate is not null

EXEC BuildCalendarBase @from, @to


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec WriteAuditLogEvent 'LoadOLAPFactCOMCaseload', @Stats, @StartTime


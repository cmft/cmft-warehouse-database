﻿
CREATE PROCEDURE dbo.BuildBaseCOMWardStay as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

TRUNCATE TABLE dbo.BaseCOMWardStay

-- Insert new rows into encounter destination table
INSERT INTO dbo.BaseCOMWardStay
(
     EncounterRecno
    ,SourceUniqueID
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,AdmissionTime
	,DischargeTime
	,AdmissionDate
	,DischargeDate
	,StartTime
	,EndTime
	,StartDate
	,EndDate
	,PatientSourceUniqueID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,SourceServicePointID
	,SourceAdmissionMethodID
	,SourceAdmissionSourceID
	,SourcePatientClassificationID
	,SourceAdminCategoryID
	,SourceIntendedManagementID
	,SourceReasonForAdmissionID
	,ExpectedDischargeDate
	,SourceDischargeDestinationID
	,SourceDischargeMethodID
	,SpellProfessionalCarer
	,SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode
	,WardStayPCTCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwner
	,InterfaceCode
	,AgeCode
	,LOWS
	,LOS
	,Cases
	,FirstStayInSpellIndicator
	,LastStayInSpellIndicator
	,WardStayStartTimeOfDay
	,WardStayEndTimeOfDay
	,AdmissionTimeOfDay
	,DischargeTimeOfDay
)
SELECT
	 EncounterRecno
	,SourceUniqueID
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,AdmissionTime
	,DischargeTime
	,AdmissionDate
	,DischargeDate
	,StartTime
	,EndTime
	,StartDate
	,EndDate
	,PatientSourceUniqueID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,SourceServicePointID
	,SourceAdmissionMethodID
	,SourceAdmissionSourceID
	,SourcePatientClassificationID
	,SourceAdminCategoryID
	,SourceIntendedManagementID
	,SourceReasonForAdmissionID
	,ExpectedDischargeDate
	,SourceDischargeDestinationID
	,SourceDischargeMethodID
	,SpellProfessionalCarer
	,SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode
	,WardStayPCTCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwner
	,InterfaceCode
	,AgeCode =
		case
		when datediff(day, PatientDateOfBirth, StartTime) is null then 'Age Unknown'
		when datediff(day, PatientDateOfBirth, StartTime) <= 0 then '00 Days'
		when datediff(day, PatientDateOfBirth, StartTime) = 1 then '01 Day'
		when datediff(day, PatientDateOfBirth, StartTime) > 1 and
		datediff(day, PatientDateOfBirth, StartTime) <= 28 then
			right('0' + Convert(Varchar,datediff(day, PatientDateOfBirth, StartTime)), 2) + ' Days'
		
		when datediff(day, PatientDateOfBirth, StartTime) > 28 and
		datediff(month, PatientDateOfBirth, StartTime) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, StartTime) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, PatientDateOfBirth, StartTime) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, StartTime) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, PatientDateOfBirth, StartTime) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, StartTime) then 1 else 0 end > 1 and
		 datediff(month, PatientDateOfBirth, StartTime) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, StartTime) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, PatientDateOfBirth,StartTime) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, StartTime) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, PatientDateOfBirth, StartTime) - 
		(
		case 
		when	(datepart(m, PatientDateOfBirth) > datepart(m, StartTime)) 
		or
			(
				datepart(m, PatientDateOfBirth) = datepart(m, StartTime) 
			And	datepart(d, PatientDateOfBirth) > datepart(d, StartTime)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, PatientDateOfBirth, StartTime) - 
		(
		case 
		when	(datepart(m, PatientDateOfBirth) > datepart(m, StartTime)) 
		or
			(
				datepart(m, PatientDateOfBirth) = datepart(m, StartTime) 
			And	datepart(d, PatientDateOfBirth) > datepart(d, StartTime)
			) then 1 else 0 end
		)), 2) + ' Years'

	end
	,LOWS =
		datediff(day, WardStay.StartDate, WardStay.EndDate)

	,LOS =
	datediff(
		 day
		,WardStay.AdmissionDate
		,coalesce(
			 WardStay.DischargeDate
			,(
				SELECT
					MAX(MaxDate)
				FROM
					(
						select
							MaxDate = max(DischargeDate)
						from
							Warehouse.COM.WardStay
						UNION ALL
						select
							MaxDate = max(AdmissionDate)
						from
							Warehouse.COM.WardStay
					)tblMaxDate
			)
		)
	)
	,Cases = 1
	,FirstStayInSpellIndicator =
		case
		when not exists
		(
		select
			1
		from
			Warehouse.COM.WardStay Previous
		where
			Previous.ProviderSpellNo = WardStay.ProviderSpellNo
		and	(
				Previous.StartTime < WardStay.StartTime
			or	(
					Previous.StartTime = WardStay.StartTime
				and	Previous.SourceEncounterNo < WardStay.SourceEncounterNo
				)
			)
		)
		then 1
		else 0
		end
	,LastStayInSpellIndicator =
			case
			when WardStay.EndDate is null then 1
			when WardStay.EndDate = WardStay.DischargeDate then 1
			else 0
			end
	,WardStayStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, StartTime), 0)
				,StartTime
			)
			,-1
		)
	,WardStayEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, EndTime), 0)
				,EndTime
			)
			,-1
		)
	,AdmissionTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AdmissionTime), 0)
				,AdmissionTime
			)
			,-1
		)
	,DischargeTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, DischargeTime), 0)
				,DischargeTime
			)
			,-1
		)
FROM 
	warehouse.COM.WardStay WardStay
WHERE
		ArchiveFlag = 'N'
	AND 
		(
			AdmissionDate >= '01 Apr 2008'
		OR
			(
					DischargeDate >= '01 Apr 2008'
				OR
					DischargeDate IS NULL
				OR 
					EndDate  IS NULL
			)
			
		)

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec WriteAuditLogEvent 'BuildOLAPBaseCOMWardStay', @Stats, @StartTime

﻿
CREATE procedure [dbo].[BuildBaseOPWaitingList] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()



truncate table BaseOPWaitingList

insert into
	dbo.BaseOPWaitingList
	(
	 EncounterRecno
	,InterfaceCode
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,KornerWait
	,EpisodicGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,PCTCode
	,BreachDate
	,ReferralDate
	,BookedDate
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,FuturePatientCancelDate
	,AdditionFlag
	,LastAppointmentFlag
	,FutureCancellationDate
	,EpisodeNo
	,AgeCode
	,Cases
	,LengthOfWait
	,WeeksWaiting
	,WithAppointment
	,BookedBeyondBreach
	,NearBreach
	,DurationCode
	,AdminCategoryCode
	,ClockStartDate
	,RTTBreachDate
	,RTTActivity
	,RTTBreachStatusCode
	,DirectorateCode
	,SpecialtyTypeCode
	,DurationAtAppointmentDateCode
	,WithRTTOpenPathway
	)
SELECT
	 EncounterRecno
	,InterfaceCode
	,SourcePatientNo
	,SourceEncounterNo
	,Encounter.CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle

	,SexCode =
		case Encounter.SexCode
		when '' then '0'
		when 'M' then '1'
		when 'F' then '2'
		when 'I' then '9'
		else '#'
		end

	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,coalesce(Encounter.ConsultantCode, 'N/A') ConsultantCode
	,coalesce(Encounter.SpecialtyCode, 'N/A') SpecialtyCode
	,Encounter.PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,coalesce(Encounter.BookingTypeCode, 'N/A') BookingTypeCode
	,CasenoteNumber
	,KornerWait
	,EpisodicGpCode
	,coalesce(Encounter.RegisteredPracticeCode, Encounter.EpisodicGpPracticeCode, 'X99999') RegisteredGpPracticeCode
	,coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredPracticeCode, 'X99999') EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,PCTCode
	,BreachDate
	,ReferralDate
	,BookedDate
	,AppointmentDate = coalesce(AppointmentDate, '1 jan 1900')
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode = coalesce(convert(varchar, Encounter.ClinicCode), 'N/A')
	,SourceOfReferralCode
	,FuturePatientCancelDate
	,AdditionFlag
	,LastAppointmentFlag
	,FutureCancellationDate
	,EpisodeNo

	,case
	when datediff(day, DateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 1 and
	datediff(day, DateOfBirth, Encounter.CensusDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.CensusDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 28 and
	datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end AgeCode

	,1 Cases

	,convert(int, Encounter.KornerWait) LengthOfWait

	,Encounter.WeeksWaiting

	,WithAppointment =
		case
		when Encounter.AppointmentDate is null then 0
		when Encounter.AppointmentDate < (select min(TheDate) from dbo.CalendarBase where TheDate <> '1 Jan 1900') then 0
		when Encounter.AppointmentDate < CensusDate then 0
		else 1
		end 

	,case when Encounter.BreachDate < Encounter.AppointmentDate then 1 else 0 end BookedBeyondBreach

	,case when datediff(month, Encounter.AppointmentDate, Encounter.BreachDate) < 2 then 1 else 0 end NearBreach

	,DurationCode =
	coalesce(
		right('00' +
		convert(varchar, 
		case
		when convert(int, Encounter.KornerWait /7 ) > 207 then 208
		else convert(int, Encounter.KornerWait /7 )
		end), 3)
		,'NA'
	)

	,AdminCategoryCode
--
--	,WaitTypeCode = '10' --waiter
--
--	,StatusCode = '40' --O/P Active
--
--	,CategoryCode = '30' --reportable outpatient
--

	,ClockStartDate
	,RTTBreachDate

	,RTTActivity = null
		--convert(
		--	bit
		--	,case
		--	when
		--		Encounter.SpecialtyCode not in 
		--		(
		--		select
		--			SpecialtyCode
		--		from
		--			RTT.dbo.ExcludedSpecialty
		--		)

		--	-- 'GENE' ?
		--	and	Encounter.ConsultantCode <> 'FRAC'

		--	--remove ICAT
		--	and	Encounter.SiteCode != 'FMSK'

		--	and	not exists
		--		(
		--		select
		--			1
		--		from
		--			RTT.dbo.RTTOPClockStop ClockChangeReason
		--		where
		--			ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
		--		and	ClockChangeReason.SourceEntityRecno = Encounter.SourceEncounterNo
		--		and	(
		--				coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
		--			or	coalesce(ClockChangeReason.ClockStopReasonCode, '') = 'NOP' --Not an 18 week pathway
		--			)
		--		)

		--	and	Encounter.RTTPathwayID is not null

		--	then 1
		--	else 0
		--	end
		--)

	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null
		then 'U'
		when Encounter.AppointmentDate is null
		then 'U'
		when Encounter.RTTBreachDate < Encounter.AppointmentDate
		then 'B'
		else 'N'
		end

		,Encounter.DirectorateCode
		,Encounter.SpecialtyTypeCode

	,DurationAtAppointmentDateCode =
		case
		when Encounter.AppointmentDate is null then 'NA'
		when Encounter.AppointmentDate < (select min(TheDate) from dbo.CalendarBase where TheDate <> '1 Jan 1900') then 'NA'
		when Encounter.AppointmentDate < CensusDate then 'NA'
		else right('00' +
				convert(varchar, 
					case
					when convert(int, (Encounter.KornerWait + datediff(day , CensusDate , AppointmentDate)) /7 ) > 207 then 208
					else convert(int, (Encounter.KornerWait + datediff(day , CensusDate , AppointmentDate)) /7 )
					end
				)
				,3
			)
		end

	,WithRTTOpenPathway = 0

FROM
	Warehouse.OP.PTL Encounter


select @RowsInserted = @@rowcount


exec BuildOlapCensus


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseOPWaitingList', @Stats, @StartTime


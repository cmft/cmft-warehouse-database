﻿
/*** Rewritten as a series of set based operations by CMFT DBA: DF ***/
CREATE procedure [dbo].[BuildCalendarBase]
(
    @FromDate datetime = null,
    @ToDate datetime = null
)
as

if (@FromDate is null)
	select @FromDate = MIN(dbo.CalendarBasePairs.TheStartDate)
	from dbo.CalendarBasePairs;

	
if (@ToDate is null)
	select @ToDate = MAX(dbo.CalendarBasePairs.TheEndDate)
	from dbo.CalendarBasePairs;
	
declare @NumberOfDayIntervals int = datediff(d,@FromDate,@ToDate);

/* upper limit as smalldatetime */

--if convert(smalldatetime,@FromDate)<'1 Jan 1950' select @FromDate='1 Jan 1950'
--if convert(smalldatetime,@ToDate)>'31 Dec 2020' select @ToDate='31 Dec 2020'
insert CalendarBase(TheDate)
select r.TheDate
from (select dateadd(d,i.TheInteger,@FromDate) TheDate
	  FROM dbo.IntegerBase i
		LEFT OUTER JOIN CalendarBase c 
		  ON dateadd(d,i.TheInteger,@FromDate) = c.TheDate 
	  WHERE i.TheInteger <= @NumberOfDayIntervals
	    AND c.TheDate IS NULL) r;

--while @CurrentDate <= @ToDate
--   begin
--        if not exists (select * from CalendarBase where TheDate = @CurrentDate)
--            insert into CalendarBase
--			(
--			[TheDate]
--			)
--			values
--			(
--			@CurrentDate
--			)

--        select @CurrentDate=dateadd(day, 1, @CurrentDate)

--    end

if not exists(select TheDate FROM dbo.CalendarBase where TheDate = '1 Jan 1900')
insert into dbo.CalendarBase
values('1 jan 1900');

﻿
CREATE procedure [dbo].[BuildBasePASCasenoteBooking] as

/******************************************************************************
	**  Name: dbo.BuildBasePASCasenoteBooking
	**  Purpose: 
	**
	**  Populates the BasePASCasenoteBooking table comprising of duplicate case notes
	**  for upcoming in-patient and out-patient attendances
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 15.03.13    CB         Initial coding
	** 18.04.13    CB		  Removal of certain Casenote types
	**						  Addition of counts of Other OP/APC Waits
	**						  Determination of Current Inpatient status
	** 22.08.13	   DG		  Added CurrentLoanComment, CurrentLoanReason, 
	**						  CurrentLoanBorrowerCode from CasenoteLoan table. 
	**						  Returns latest loan event for each casenote. 
	**						  At request of Melissa Taafe. Appended to existing 
	**						  structure as Melissa unsure if existing columns used.
	******************************************************************************/


--generate set of in-scope casenotes
select
	 SourcePatientNo
	,CasenoteNumber
	,AllocatedDate
	,CasenoteLocationCode
	,CasenoteLocationDate
	,CasenoteStatus
	,WithdrawnDate
	,Comment
	,CurrentBorrowerCode
	,CasenoteTypeCode
	,CurrentLoanComment
	,CurrentLoanReason
	,CurrentLoanBorrowerCode
into
	#Casenote
from
	(
	select
		Casenote.SourcePatientNo
		,Casenote.CasenoteNumber
		,AllocatedDate
		,CasenoteLocationCode
		,CasenoteLocationDate
		,CasenoteStatus
		,WithdrawnDate
		,Casenote.Comment
		,CurrentBorrowerCode

		,CasenoteTypeCode =
			case
			when isnumeric(Casenote.CasenoteNumber) = 1 and len(Casenote.CasenoteNumber) = 10 then 'NHSNumber'
			when left(Casenote.CasenoteNumber , 3) = 'PCT' then 'PCT'
			when charindex('/' , Casenote.CasenoteNumber , 0) != 0 then
				case
				when left(Casenote.CasenoteNumber , 1) = 'G' then 'G'
				when left(Casenote.CasenoteNumber , 1) = 'T' then 'T'
				else left(Casenote.CasenoteNumber , charindex('/' , Casenote.CasenoteNumber , 0) - 1)
				end
			--when right(CasenoteNumber , 1) = 'D' then left(CasenoteNumber , 1) + '_D'
			when right(Casenote.CasenoteNumber , 1) = 'D' then 'D'
			else left(Casenote.CasenoteNumber , 1)
			end
	,CurrentLoanComment = CurrentCasenoteLoan.Comment
	,CurrentLoanReason = CurrentCasenoteLoan.LoanReason
	,CurrentLoanBorrowerCode = CurrentCasenoteLoan.BorrowerCode
	from
		Warehouse.PAS.Casenote

	left outer join
			(
				select
					SourcePatientNo
					,CasenoteNumber
					,BorrowerCode
					,Comment
					,LoanReason

				from
					Warehouse.PAS.CasenoteLoan CasenoteLoan
				where
					not exists
								(
								select
									1
								from
									Warehouse.PAS.CasenoteLoan CasenoteLoanNext
								where
									CasenoteLoanNext.SourcePatientNo = CasenoteLoan.SourcePatientNo
								and
									CasenoteLoanNext.CasenoteNumber = CasenoteLoan.CasenoteNumber
								and
									CasenoteLoanNext.SequenceNo > CasenoteLoan.SequenceNo
								)

			) CurrentCasenoteLoan
			on	CurrentCasenoteLoan.SourcePatientNo = Casenote.SourcePatientNo
			and	CurrentCasenoteLoan.CasenoteNumber = Casenote.CasenoteNumber

	where
		coalesce(Casenote.CasenoteStatus , '') != 'WITHDRAWN'
	and	Casenote.CurrentBorrowerCode is null
	) Casenote
where
	left(Casenote.CasenoteTypeCode , 1) in ('M' , 'S' , 'B' , 'R' , 'E' , 'D')
and	left(Casenote.CasenoteTypeCode , 3) != 'BCG'



CREATE NONCLUSTERED INDEX [Idx_Casenote_CasenoteNumber] ON #Casenote
(
	[CasenoteNumber] ASC
)

CREATE NONCLUSTERED INDEX [INDEX_Casenote_AllocatedDateStatus] ON #Casenote
(
	[AllocatedDate] ASC,
	[CasenoteStatus] ASC
)
INCLUDE ( 	[SourcePatientNo])

CREATE NONCLUSTERED INDEX [IX_Casenote] ON #Casenote
(
	[SourcePatientNo] ASC,
	[CasenoteNumber] ASC
)



truncate table dbo.BasePASCasenoteBooking

insert into dbo.BasePASCasenoteBooking
(
	 Surname
	,Forenames
	,DateOfBirth
	,DistrictNo
	,EncounterTypeCode
	,BookingDate
	,BookingTime
	,SourcePatientNo
	,SourceEntityRecno
	,EncounterDate
	,EncounterTime
	,BookingCasenoteNo
	,WardCode
	,ClinicCode
	,CasenoteNo
	,AllocatedDate
	,CasenoteLocationCode
	,CasenoteLocationDate
	,CasenoteStatus
	,WithdrawnDate
	,Comment
	,CurrentBorrowerCode
	,CurrentLoanComment
	,CurrentLoanReason
	,CurrentLoanBorrowerCode
	,OtherOPWaits
	,OtherNonMNumberOPWaits
	,OtherAPCWaits
	,OtherNonMNumberAPCWaits
	,IsCurrentInpatient
	,SiteCode
	,ServicePointCode
)


select
	 Patient.Surname
	,Patient.Forenames
	,Patient.DateOfBirth
	,Patient.DistrictNo
	,Booking.EncounterTypeCode
	,Booking.BookingDate
	,Booking.BookingTime
	,Booking.SourcePatientNo
	,Booking.SourceEntityRecno
	,Booking.EncounterDate
	,Booking.EncounterTime
	,BookingCasenoteNo = Booking.CasenoteNo
	,Booking.WardCode
	,Booking.ClinicCode
	,CasenoteNo = Casenote.CasenoteNumber
	,Casenote.AllocatedDate
	,Casenote.CasenoteLocationCode
	,Casenote.CasenoteLocationDate
	,Casenote.CasenoteStatus
	,Casenote.WithdrawnDate
	,Casenote.Comment
	,Casenote.CurrentBorrowerCode
	,Casenote.CurrentLoanComment
	,Casenote.CurrentLoanReason
	,Casenote.CurrentLoanBorrowerCode

	,OtherOPWaits =
		(
		select
			count(SourceUniqueID)
		from
			Warehouse.RF.Encounter RFEncounter
		where
			RFEncounter.SourcePatientNo = Booking.SourcePatientNo
		and	RFEncounter.SourceEncounterNo != Booking.SourceEntityRecno

		and	RFEncounter.DischargeDate is null
		and	RFEncounter.SourceOfReferralCode is not null
		)

	,OtherNonMNumberOPWaits =
		(
		select
			count(SourceUniqueID)
		from
			Warehouse.RF.Encounter RFEncounter
		where
			RFEncounter.SourcePatientNo = Booking.SourcePatientNo
		and	RFEncounter.SourceEncounterNo != Booking.SourceEntityRecno

		and	RFEncounter.DischargeDate is null
		and	RFEncounter.SourceOfReferralCode is not null
		and	left(RFEncounter.CasenoteNo , 1) != 'M'
		)


	,OtherAPCWaits =
		(
		select
			count(SourcePatientNo)
		from
			Warehouse.APC.WaitingList
		where
			WaitingList.CensusDate =
				(
				select
					max(CensusDate)
				from
					Warehouse.APC.WaitingList
				)
		and	WaitingList.SourcePatientNo = Booking.SourcePatientNo
		and	WaitingList.SourceEncounterNo != Booking.SourceEntityRecno
		)


	,OtherNonMNumberAPCWaits =
		(
		select
			count(SourcePatientNo)
		from
			Warehouse.APC.WaitingList
		where
			WaitingList.CensusDate =
				(
				select
					max(CensusDate)
				from
					Warehouse.APC.WaitingList
				)
		and	WaitingList.SourcePatientNo = Booking.SourcePatientNo
		and	WaitingList.SourceEncounterNo != Booking.SourceEntityRecno
		and	left(WaitingList.CasenoteNumber , 1) != 'M'
		)

	,IsCurrentInpatient =
		coalesce(
			(
			select distinct
				1
			from
				Warehouse.APC.Encounter
			where
				Encounter.DischargeDate is null
			and	Encounter.SourcePatientNo = Booking.SourcePatientNo
			and	Encounter.SourceSpellNo != Booking.SourceEntityRecno
			)
			,0
		)

	,SiteCode =
		coalesce(Clinic.SiteCode , Ward.SiteCode)

	,ServicePointCode =
		coalesce(Booking.ClinicCode , Booking.WardCode)
		
from
	(
	select
		 Preadmission.EncounterTypeCode
		,Preadmission.BookingDate
		,Preadmission.BookingTime
		,Preadmission.SourcePatientNo
		,Preadmission.SourceEntityRecno
		,Preadmission.EncounterDate
		,Preadmission.EncounterTime
		,Preadmission.CasenoteNo
		,Preadmission.WardCode
		,Preadmission.ClinicCode
	from
		(
		select
			 EncounterTypeCode = 'TCI'
			,BookingDate = Encounter.ActivityDate
			,BookingTime = Encounter.ActivityTime
			,Encounter.SourcePatientNo
			,Encounter.SourceEntityRecno
			,EncounterDate = cast(Encounter.TCITime as date)
			,EncounterTime = Encounter.TCITime
			,CasenoteNo = null
			,Encounter.WardCode
			,ClinicCode = null
		from
			Warehouse.APC.WaitingListActivity Encounter
		where
			ActivityTypeCode = '5'
		and	cast(Encounter.TCITime as date) >= dateadd(month , -1 , getdate())
		) Preadmission
	where
		not exists
			(
			select
				1
			from
				Warehouse.APC.WaitingListActivity LaterActivity
			where
				--don't care which episode
				LaterActivity.SourcePatientNo = Preadmission.SourcePatientNo
			and	LaterActivity.ActivityTime > Preadmission.EncounterTime
			)

	union

	select
		 Appointment.EncounterTypeCode
		,Appointment.BookingDate
		,Appointment.BookingTime
		,Appointment.SourcePatientNo
		,Appointment.SourceEncounterNo
		,Appointment.EncounterDate
		,Appointment.EncounterTime
		,Appointment.CasenoteNo
		,Appointment.WardCode
		,Appointment.ClinicCode
	from
		(
		select
			 EncounterTypeCode =
				case Encounter.DerivedFirstAttendanceFlag
				when 1 then 'NEW'
				when 2 then 'FUP'
				end

			,BookingDate = Encounter.AppointmentCreateDate
			,BookingTime = null
			,Encounter.SourcePatientNo
			,Encounter.SourceEncounterNo
			,EncounterDate = Encounter.AppointmentDate
			,EncounterTime = Encounter.AppointmentTime
			,Encounter.CasenoteNo
			,Encounter.WardCode
			,Encounter.ClinicCode
			,Encounter.DoctorCode
		from
			Warehouse.OP.Encounter
		where
			Encounter.AppointmentDate > dateadd(month , -1 , getdate()) --and dateadd(month , 3 , getdate())
		and	coalesce(DNACode, '9') not in ('1', '2', '3', '4', '5', '6', '7')
		) Appointment
	where
		--return the first available appointment
		not exists
			(
			select
				 1
			from
				Warehouse.OP.Encounter EarlierEncounter
			where
				EarlierEncounter.AppointmentDate > dateadd(month , -1 , getdate()) --and dateadd(month , 3 , getdate())
			and	coalesce(DNACode, '9') not in ('1', '2', '3', '4', '5', '6', '7')
			and	EarlierEncounter.SourcePatientNo = Appointment.SourcePatientNo
			and	(
					EarlierEncounter.AppointmentTime < Appointment.EncounterTime
				or	(
						EarlierEncounter.AppointmentTime = Appointment.EncounterTime
					and	EarlierEncounter.DoctorCode < Appointment.DoctorCode
					)
				)
			)

	union 

	select
		 NonElectiveAdmission.EncounterTypeCode
		,NonElectiveAdmission.BookingDate
		,NonElectiveAdmission.BookingTime
		,NonElectiveAdmission.SourcePatientNo
		,NonElectiveAdmission.SourceEncounterNo
		,NonElectiveAdmission.EncounterDate
		,NonElectiveAdmission.EncounterTime
		,NonElectiveAdmission.CasenoteNo
		,NonElectiveAdmission.StartWardTypeCode
		,NonElectiveAdmission.ClinicCode
	from
		(
		select
			 EncounterTypeCode = 'NEL'
			,BookingDate = Encounter.AdmissionDate
			,BookingTime = Encounter.AdmissionTime
			,Encounter.SourcePatientNo
			,Encounter.SourceEncounterNo
			,EncounterDate = cast(Encounter.AdmissionDate as date)
			,EncounterTime = Encounter.AdmissionTime
			,CasenoteNo = CasenoteNumber
			,Encounter.StartWardTypeCode
			,ClinicCode = null
		from
			Warehouse.APC.Encounter Encounter
		where
			cast(Encounter.AdmissionDate as date) >= dateadd(day , -5 , getdate())
		and
			not exists
				(
				select
					1
				from
					Warehouse.APC.WaitingListActivity LaterActivity
				where
					--don't care which episode
					LaterActivity.SourcePatientNo = Encounter.SourcePatientNo
				and	LaterActivity.ActivityTime > Encounter.AdmissionTime
				)
		and
			Encounter.PatientCategoryCode = 'NE'
		and
			/* just get the latest FCE in spell */
			not exists
						(
						select
							1
						from
							Warehouse.APC.Encounter EncounterNext
						where
							EncounterNext.SourcePatientNo = Encounter.SourcePatientNo
						and
							EncounterNext.ProviderSpellNo = Encounter.ProviderSpellNo
						and
							EncounterNext.EpisodeStartTime > Encounter.EpisodeStartTime
						)

		) NonElectiveAdmission
	
	) Booking


left join Warehouse.PAS.Patient
on	Patient.SourcePatientNo = Booking.SourcePatientNo

--return only in-scope casenotes
left join #Casenote Casenote
on	Casenote.SourcePatientNo = Booking.SourcePatientNo
--and	Casenote.CasenoteNumber is not null

left join Warehouse.PAS.Clinic
on	Booking.ClinicCode = Clinic.ClinicCode

left join Warehouse.PAS.Ward
on	Ward.WardCode = Booking.WardCode

--exclude obstetrics
where
	not exists
		(
		select
			1
		from
			Warehouse.RF.Encounter RFEncounter
		where
			RFEncounter.SourcePatientNo = Booking.SourcePatientNo
		and	RFEncounter.DischargeDate is null
		and	RFEncounter.SpecialtyCode in
				(
				 'OBS'
				,'OBS1'
				,'OBS2'
				,'OBS3'
				,'FMU'
				,'MID'
				,'PCON'
				)
		)
--filter for patients with multiple in-scope casenotes
and exists
	(
	select
		1
	from
		(
		select
			SourcePatientNo
		from
			#Casenote Casenote
		group by
			SourcePatientNo
		having
			count(*) > 1
		) MultipleCasenote
	where
		MultipleCasenote.SourcePatientNo = Booking.SourcePatientNo
	)



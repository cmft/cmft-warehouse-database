﻿
CREATE procedure [dbo].[BuildBaseTheatrePatientBooking] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseTheatrePatientBooking

INSERT INTO WarehouseOLAP.dbo.BaseTheatrePatientBooking
(
	 SequenceNo
	,SourceUniqueID
	,DistrictNo
	,IntendedProcedureDate
	,IntendedTheatreCode
	,CustodianDetail
	,IntendedSessionTypeCode
	,Surname
	,Forename
	,DateOfBirth
	,PatientSourceUniqueID
	,TheatreCode
	,ConsultantCode
	,ConsultantCode2
	,BiohazardFlag
	,BloodProductsRequiredFlag
	,SpecialEquipmentFlag
	,PatientAgeInYears
	,PatientAgeInMonths
	,AdmissionWardCode
	,SessionStartTime
	,PatientEpisodeNumber
	,Operation
	,PreferredOrder
	,PatientBookingStatusCode
	,PatientStatusCode
	,PatientClassificationCode2
	,AnaestheticCode
	,AnaestheticTypeCode
	,SurgeonComment
	,SurgeonSpecialtyCode
	,BedNumber
	,WaitingListSourceUniqueID
	,SessionNo
	,OperationDuration
	,ChangeOverDuration
	,SexCode
	,SurgeonCode
	,BookingEventAdmittedTime
	,SurgeryRequestedTime
	,ReadyForSurgeryTime
	,VerifiedFlag
	,PlannedReturnFlag
	,EmergencyFlag
	,TheatreSessionSort
	,EstimatedStartTime
	,IntendedSurgeonCode
	,PatientClassificationCode
	,WardCode
	,Surgeon1SpecialtyCode
	,LogLastUpdated
	,RecordLogDetails
	,WaitingDetailsSourceUniqueID
	,AnaesthetistCode
	,Address1
	,Address3
	,Postcode
	,HomeTelephone
	,BusinessTelephone
	,AgeInDays
	,AdmissionTypeCode
	,ChangedSinceLastDataTransferFlag
	,ASAScoreCode
	,Unused4
	,Unused5
	,Unused6
	,Unused7
	,Unused8
	,PathologyRequiredFlag
	,XrayRequiredFlag
	,PathologyComment
	,XrayComment
	,BloodProductsRequiredComment
	,UnitsOfBloodProductRequired
	,WardAdvisedTime
	,WardAdvisedTime2
	,ModeOfTransportCode
	,NurseRequestingTransferCode
	,NurseReceivingNotificationCode
	,OxygenRequiredFlag
	,IVFlag
	,TransferredToWardCode
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,AnaesthetisNurseCode
	,InstrumentNurseCode
	,Technician1Code
	,Technician2Code
	,Unused1
	,InTheatreTime
	,CampusCode
	,ClinicalPriorityCode
	,StaffCode
	,AdditionalCaseFlag
	,UnplannedReturnCode
	,UnplannedReturnComment
	,AdmittingStaffCode
	,ReviewFlag
	,ExternalReferenceNo
	,UserFlag
	,AdmittedTime
	,ACCFlag
	,ACCNumber
	,ACCCode
	,MajorMinorCaseFlag
	,EstimatedStartTimeFixedFlag
	,PatientLanguageCode
	,LMOCode
	,ReferrerCode
	,MaritalStatusCode
	,PatientAddressStatus
	,LastUpdated
	,OldMRN
	,EstimatedArrivalTime
	,Address2
	,Address4
	,SentForTime
	,PorterLeftTime
	,PorterCode
	,CombinedCaseOrderNumberF
	,CombinedCaseConsultantCode
	,CombinedCaseTheatreCode
	,CombinedCaseSessionNo
	,CombinedCaseEstimateStartTime
	,CombinedCaseSpecialtyCode
	,CombinedCaseSurgeonCode
	,CombinedCaseAnaesthetistCode
	,CombinedCaseFlag
	,CombinedCaseFixedTimeFlag
	,PicklistPrintedFlag
	,PicklistMergedFlag
	,BookingInformationReceivedTime
	,InitiatedByStaffName
	,RequestingSurgerySpecialtyCode
	,HSCComment
	,SessionSourceUniqueID
	,CombinedCaseSessionSourceUniqueID
	,ReasonForOrderCode
	,PrintedFlag
	,PorterDelayCode
	,NHSNumber
	,CasenoteNumber
	,AgeCode
	,EstimatedEndTime
	,EstimatedOperationStartTimeOfDay
	,EstimatedOperationEndTimeOfDay
)

SELECT
	 PatientBooking.SequenceNo
	,PatientBooking.SourceUniqueID
	,PatientBooking.DistrictNo
	,PatientBooking.IntendedProcedureDate
	,PatientBooking.IntendedTheatreCode
	,PatientBooking.CustodianDetail
	,PatientBooking.IntendedSessionTypeCode
	,PatientBooking.Surname
	,PatientBooking.Forename
	,PatientBooking.DateOfBirth
	,PatientBooking.PatientSourceUniqueID
	,PatientBooking.TheatreCode
	,PatientBooking.ConsultantCode
	,PatientBooking.ConsultantCode2
	,PatientBooking.BiohazardFlag
	,PatientBooking.BloodProductsRequiredFlag
	,PatientBooking.SpecialEquipmentFlag
	,PatientBooking.PatientAgeInYears
	,PatientBooking.PatientAgeInMonths
	,PatientBooking.AdmissionWardCode
	,PatientBooking.SessionStartTime
	,PatientBooking.PatientEpisodeNumber

	,Operation =
		left(cast(PatientBooking.Operation as varchar(max)) , 1000)

	,PatientBooking.PreferredOrder
	,PatientBooking.PatientBookingStatusCode
	,PatientBooking.PatientStatusCode
	,PatientBooking.PatientClassificationCode2
	,PatientBooking.AnaestheticCode
	,PatientBooking.AnaestheticTypeCode
	,PatientBooking.SurgeonComment
	,PatientBooking.SurgeonSpecialtyCode
	,PatientBooking.BedNumber
	,PatientBooking.WaitingListSourceUniqueID
	,PatientBooking.SessionNo
	,PatientBooking.OperationDuration
	,PatientBooking.ChangeOverDuration
	,PatientBooking.SexCode
	,PatientBooking.SurgeonCode
	,PatientBooking.BookingEventAdmittedTime
	,PatientBooking.SurgeryRequestedTime
	,PatientBooking.ReadyForSurgeryTime
	,PatientBooking.VerifiedFlag
	,PatientBooking.PlannedReturnFlag
	,PatientBooking.EmergencyFlag
	,PatientBooking.TheatreSessionSort
	,PatientBooking.EstimatedStartTime
	,PatientBooking.IntendedSurgeonCode
	,PatientBooking.PatientClassificationCode
	,PatientBooking.WardCode
	,PatientBooking.Surgeon1SpecialtyCode
	,PatientBooking.LogLastUpdated
	,PatientBooking.RecordLogDetails
	,PatientBooking.WaitingDetailsSourceUniqueID
	,PatientBooking.AnaesthetistCode
	,PatientBooking.Address1
	,PatientBooking.Address3
	,PatientBooking.Postcode
	,PatientBooking.HomeTelephone
	,PatientBooking.BusinessTelephone
	,PatientBooking.AgeInDays
	,PatientBooking.AdmissionTypeCode
	,PatientBooking.ChangedSinceLastDataTransferFlag
	,PatientBooking.ASAScoreCode
	,PatientBooking.Unused4
	,PatientBooking.Unused5
	,PatientBooking.Unused6
	,PatientBooking.Unused7
	,PatientBooking.Unused8
	,PatientBooking.PathologyRequiredFlag
	,PatientBooking.XrayRequiredFlag
	,PatientBooking.PathologyComment
	,PatientBooking.XrayComment
	,PatientBooking.BloodProductsRequiredComment
	,PatientBooking.UnitsOfBloodProductRequired
	,PatientBooking.WardAdvisedTime
	,PatientBooking.WardAdvisedTime2
	,PatientBooking.ModeOfTransportCode
	,PatientBooking.NurseRequestingTransferCode
	,PatientBooking.NurseReceivingNotificationCode
	,PatientBooking.OxygenRequiredFlag
	,PatientBooking.IVFlag
	,PatientBooking.TransferredToWardCode
	,PatientBooking.Surgeon2Code
	,PatientBooking.Surgeon3Code
	,PatientBooking.Anaesthetist2Code
	,PatientBooking.Anaesthetist3Code
	,PatientBooking.ScoutNurseCode
	,PatientBooking.AnaesthetisNurseCode
	,PatientBooking.InstrumentNurseCode
	,PatientBooking.Technician1Code
	,PatientBooking.Technician2Code
	,PatientBooking.Unused1
	,PatientBooking.InTheatreTime
	,PatientBooking.CampusCode
	,PatientBooking.ClinicalPriorityCode
	,PatientBooking.StaffCode
	,PatientBooking.AdditionalCaseFlag
	,PatientBooking.UnplannedReturnCode
	,PatientBooking.UnplannedReturnComment
	,PatientBooking.AdmittingStaffCode
	,PatientBooking.ReviewFlag
	,PatientBooking.ExternalReferenceNo
	,PatientBooking.UserFlag
	,PatientBooking.AdmittedTime
	,PatientBooking.ACCFlag
	,PatientBooking.ACCNumber
	,PatientBooking.ACCCode
	,PatientBooking.MajorMinorCaseFlag
	,PatientBooking.EstimatedStartTimeFixedFlag
	,PatientBooking.PatientLanguageCode
	,PatientBooking.LMOCode
	,PatientBooking.ReferrerCode
	,PatientBooking.MaritalStatusCode
	,PatientBooking.PatientAddressStatus
	,PatientBooking.LastUpdated
	,PatientBooking.OldMRN
	,PatientBooking.EstimatedArrivalTime
	,PatientBooking.Address2
	,PatientBooking.Address4
	,PatientBooking.SentForTime
	,PatientBooking.PorterLeftTime
	,PatientBooking.PorterCode
	,PatientBooking.CombinedCaseOrderNumberF
	,PatientBooking.CombinedCaseConsultantCode
	,PatientBooking.CombinedCaseTheatreCode
	,PatientBooking.CombinedCaseSessionNo
	,PatientBooking.CombinedCaseEstimateStartTime
	,PatientBooking.CombinedCaseSpecialtyCode
	,PatientBooking.CombinedCaseSurgeonCode
	,PatientBooking.CombinedCaseAnaesthetistCode
	,PatientBooking.CombinedCaseFlag
	,PatientBooking.CombinedCaseFixedTimeFlag
	,PatientBooking.PicklistPrintedFlag
	,PatientBooking.PicklistMergedFlag
	,PatientBooking.BookingInformationReceivedTime
	,PatientBooking.InitiatedByStaffName
	,PatientBooking.RequestingSurgerySpecialtyCode
	,PatientBooking.HSCComment
	,PatientBooking.SessionSourceUniqueID
	,PatientBooking.CombinedCaseSessionSourceUniqueID
	,PatientBooking.ReasonForOrderCode
	,PatientBooking.PrintedFlag
	,PatientBooking.PorterDelayCode
	,PatientBooking.NHSNumber
	,PatientBooking.CasenoteNumber

	,AgeCode =
	case
	when datediff(day, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) is null then 'Age Unknown'
	when datediff(day, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) <= 0 then '00 Days'
	when datediff(day, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) = 1 then '01 Day'
	when datediff(day, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) > 1 and
	datediff(day, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate)), 2) + ' Days'
	
	when datediff(day, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) > 28 and
	datediff(month, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) -
	case  when datepart(day, PatientBooking.DateOfBirth) > datepart(day, PatientBooking.IntendedProcedureDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) -
	case  when datepart(day, PatientBooking.DateOfBirth) > datepart(day, PatientBooking.IntendedProcedureDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) -
	case  when datepart(day, PatientBooking.DateOfBirth) > datepart(day, PatientBooking.IntendedProcedureDate) then 1 else 0 end > 1 and
	 datediff(month, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) -
	case  when datepart(day, PatientBooking.DateOfBirth) > datepart(day, PatientBooking.IntendedProcedureDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, PatientBooking.DateOfBirth,PatientBooking.IntendedProcedureDate) -
	case  when datepart(day, PatientBooking.DateOfBirth) > datepart(day, PatientBooking.IntendedProcedureDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) - 
	(
	case 
	when	(datepart(m, PatientBooking.DateOfBirth) > datepart(m, PatientBooking.IntendedProcedureDate)) 
	or
		(
			datepart(m, PatientBooking.DateOfBirth) = datepart(m, PatientBooking.IntendedProcedureDate) 
		And	datepart(d, PatientBooking.DateOfBirth) > datepart(d, PatientBooking.IntendedProcedureDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, PatientBooking.DateOfBirth, PatientBooking.IntendedProcedureDate) - 
	(
	case 
	when	(datepart(m, PatientBooking.DateOfBirth) > datepart(m, PatientBooking.IntendedProcedureDate)) 
	or
		(
			datepart(m, PatientBooking.DateOfBirth) = datepart(m, PatientBooking.IntendedProcedureDate) 
		And	datepart(d, PatientBooking.DateOfBirth) > datepart(d, PatientBooking.IntendedProcedureDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,EstimatedEndTime =
		dateadd(minute , PatientBooking.OperationDuration , PatientBooking.EstimatedStartTime)

	,EstimatedOperationStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, PatientBooking.EstimatedStartTime), 0)
				,PatientBooking.EstimatedStartTime
			)
			,-1
		)

	,EstimatedOperationEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, PatientBooking.EstimatedStartTime), 0)
				,PatientBooking.EstimatedStartTime
			) + PatientBooking.OperationDuration
			,-1
		)
FROM
	Warehouse.Theatre.PatientBooking PatientBooking


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseTheatrePatientBooking', @Stats, @StartTime


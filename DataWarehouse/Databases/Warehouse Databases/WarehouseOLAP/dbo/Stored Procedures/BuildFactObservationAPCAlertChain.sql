﻿









CREATE proc [dbo].[BuildFactObservationAPCAlertChain]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationAPCAlertChain

insert into dbo.FactObservationAPCAlertChain

(
	ObservationSetRecno
	,SpecialtyID
	,WardID
	,StartDate
	,StartTimeOfDayCode
	,OverallRiskIndexCode
	,OverallAssessedStatusID
	,DurationID
	,OutOfHours
	,AgeID
	,ConsultantCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,AdmissionMethodCode
	,DischargeMethodCode
	,EthnicOriginCode
	,SexCode
	,ContextCode
	,Cases
	,Duration
)

select
	 BaseObservationObservationSet.ObservationSetRecno
	,SpecialtyID
	,WardID
	,StartDate
	,StartTimeOfDayCode
	,OverallRiskIndexCode
	,OverallAssessedStatusID
	,DurationID = 
				coalesce(
						case 
						when AlertChainDurationMinutes < 0 then -1
						when AlertChainDurationMinutes > 1440 then 1440
						else AlertChainDurationMinutes
						end
					, -2
					)
	,OutOfHours
	,AgeID
	,ConsultantCode = 
				coalesce(
					Encounter.ConsultantCode
				,'N/A'
				)
	,PrimaryDiagnosisCode = 
				coalesce(
					left(Encounter.PrimaryDiagnosisCode, 5)
				,'##'
				)
	,PrimaryOperationCode = 
				coalesce(
					left(Encounter.PrimaryOperationCode, 5)
				,'##'
				)
	,AdmissionMethodCode = 
				coalesce(
					Encounter.AdmissionMethodCode
				,'NA'
				)
	,DischargeMethodCode = 
				coalesce(
					Encounter.DischargeMethodCode
				,'##'
				)
	,EthnicOriginCode = 
				coalesce(
					Encounter.EthnicOriginCode
				,'X'
				)
	,SexCode =
		case Encounter.SexCode
		when '' then '0'
		when 'M' then '1'
		when 'F' then '2'
		when 'I' then '9'
		else '#'
		end
	,ContextCode
	,Cases
	,AlertChainDurationMinutes
from
	dbo.BaseObservationObservationSet

inner join Warehouse.APC.EncounterObservationSet
on	BaseObservationObservationSet.ObservationSetRecno = EncounterObservationSet.ObservationSetRecno

inner join Warehouse.APC.Encounter
on	EncounterObservationSet.EncounterRecno = Encounter.EncounterRecno

where
	IsLastInAlertChain = 1



select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	







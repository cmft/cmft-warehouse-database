﻿
CREATE procedure [dbo].[BuildOlapCensus] as

delete from 	dbo.OlapCensus

insert
into
	dbo.OlapCensus
(
	Snapshot.CensusDate
	,Census
	,LastInMonth
)
select
	Snapshot.CensusDate

	,Census = left(convert(varchar, Snapshot.CensusDate, 113), 11) 

	,LastInMonth =
	case
	when CensusDate in 
		(
		select 
			max(CensusDate) 
		from 
			Warehouse.APC.Snapshot LatestSnapshot 
		group by 
			 datepart(year, CensusDate)
			,datepart(month, CensusDate)
		)
	then 1
	else 0
	end
from
	(
	select
		CensusDate
	from
		Warehouse.APC.Snapshot

	union

	select
		CensusDate
	from
		Warehouse.OP.Snapshot

	union

	select distinct
		CensusDate
	from
		WarehouseOLAP.dbo.FactAPCOccupancy

	union

	select distinct
		CensusDate
	from
		WarehouseOLAP.Pharmacy.BaseOrderCensus

	union

	select distinct
		CensusDate
	from
		WarehouseOLAP.Pharmacy.BaseReconciliationCensus

	union

	select distinct
		CensusDate
	from
		Warehouse.COM.Snapshot

	) Snapshot


﻿
CREATE proc [dbo].[BuildBaseCancelledOperation]

as

set dateformat dmy


truncate table dbo.BaseCancelledOperation

insert into dbo.BaseCancelledOperation

(
[SourceUniqueID]
,[CasenoteNumber]
,[DirectorateCode]
,[NationalSpecialtyCode]
,[ConsultantCode]
,[AdmissionDate]
,[ProcedureDate]
,[WardCode]
,[Anaesthetist]
,[Surgeon]
,[ProcedureCode]
,[CancellationDate]
,[CancellationReason]
,[TCIDate]
,[Comments]
,[ProcedureCompleted]
,[BreachwatchComments]
,[Removed]
,[RemovedComments]
,[ReportableBreach]
,[ContextCode]
,[Created]
,[Updated]
,[ByWhom]
)


select 
	[SourceUniqueID]
	,[CasenoteNumber]
	,[DirectorateCode]
	,[NationalSpecialtyCode]
	,[ConsultantCode]
	,[AdmissionDate]
	,[ProcedureDate]
	,[WardCode]
	,[Anaesthetist]
	,[Surgeon]
	,[ProcedureCode]
	,[CancellationDate]
	,[CancellationReason]
	,[TCIDate]
	,[Comments]
	,[ProcedureCompleted]
	,[BreachwatchComments]
	,[Removed]
	,[RemovedComments]
	,[ReportableBreach]
	,[ContextCode]
	,[Created]
	,[Updated]
	,[ByWhom]
from
	Warehouse.Theatre.CancelledOperation
	

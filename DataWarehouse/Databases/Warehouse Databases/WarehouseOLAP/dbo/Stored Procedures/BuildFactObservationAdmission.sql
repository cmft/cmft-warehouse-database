﻿


CREATE proc [dbo].[BuildFactObservationAdmission]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationAdmission

insert into dbo.FactObservationAdmission

(
MetricCode
,AdmissionRecno
,SpecialtyID
,WardID
,TreatmentOutcomeID
,DischargeDestinationID
,Date
,TimeOfDayCode
,LengthOfStay
,OutOfHours
,AgeID
,SexID
,ContextCode
,Cases
)

select
	MetricCode 
	,AdmissionRecno
	,SpecialtyID 
	,WardID 
	,TreatmentOutcomeID
	,DischargeDestinationID
	,Date 
	,TimeOfDayCode 
	,LengthOfStay
	,OutOfHours
	,AgeID 
	,SexID
	,ContextCode
	,Cases
from
	(
	select
		MetricCode = 'ADM' -- Admission
		,AdmissionRecno
		,SpecialtyID = AdmissionSpecialtyID
		,WardID = AdmissionWardID
		,TreatmentOutcomeID
		,DischargeDestinationID
		,Date = AdmissionDate
		,TimeOfDayCode = AdmissionTimeOfDayCode
		,LengthOfStay
		,OutOfHours = AdmissionOutOfHours
		,AgeID = AdmissionAgeID
		,SexID
		,ContextCode
		,Cases
	from
		dbo.BaseObservationAdmission

	union all

	select
		MetricCode = 'DIS' -- Discharge
		,AdmissionRecno
		,SpecialtyID = DischargeSpecialtyID
		,WardID = DischargeWardID
		,TreatmentOutcomeID
		,DischargeDestinationID
		,Date = DischargeDate
		,TimeOfDayCode = DischargeTimeOfDayCode
		,LengthOfStay
		,OutOfHours = DischargeOutOfHours
		,AgeID = DischargeAgeID
		,SexID
		,ContextCode
		,Cases
	from
		dbo.BaseObservationAdmission
	where
		DischargeDate is not null

	) AdmissionFact




select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	







﻿
CREATE procedure [dbo].[BuildBaseOP] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseOP

insert into dbo.BaseOP
	(
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,DNACode
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AttendanceOutcomeCode
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
	,LocalAdminCategoryCode
	,PCTCode
	,LocalityCode
	,AgeCode
	,HRGCode
	,Cases
	,LengthOfWait
	,IsWardAttender
	,RTTActivity
	,RTTBreachStatusCode
	,ClockStartDate
	,RTTBreachDate
	,RTTTreated
	,DirectorateCode
	,ReferringSpecialtyTypeCode
	,InterpreterIndicator
	,DerivedFirstAttendanceFlag
	)
SELECT --top 100
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath

	--,SexCode =
	--	case
	--	when coalesce(Encounter.SexCode, '') = ''
	--	then null
	--	else Encounter.SexCode
	--	end

	,SexCode =
		case Encounter.SexCode
		when '' then '0'
		when 'M' then '1'
		when 'F' then '2'
		when 'I' then '9'
		else '#'
		end

	,Encounter.NHSNumber
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode, Encounter.EpisodicGpPracticeCode, 'X99999')

	,Encounter.SiteCode
	,Encounter.AppointmentDate
	,Encounter.AppointmentTime

	,ClinicCode =
		coalesce(
			Encounter.ClinicCode
			,'N/A'
		)

	,Encounter.AdminCategoryCode
	,Encounter.SourceOfReferralCode
	,Encounter.ReasonForReferralCode
	,Encounter.PriorityCode
	,Encounter.FirstAttendanceFlag
	,Encounter.DNACode
	,Encounter.AppointmentStatusCode
	,Encounter.CancelledByCode
	,Encounter.TransportRequiredFlag
	,Encounter.AttendanceOutcomeCode
	,Encounter.AppointmentTypeCode
	,Encounter.DisposalCode

	,ConsultantCode =
		coalesce(Encounter.ConsultantCode, 'N/A')

	,SpecialtyCode =
		coalesce(Encounter.SpecialtyCode, 'N/A')

	,ReferringConsultantCode =
		coalesce(Encounter.ReferringConsultantCode, 'N/A')

	,ReferringSpecialtyCode =
		coalesce(Encounter.ReferringSpecialtyCode , 'N/A')

	,BookingTypeCode =
		coalesce(Encounter.BookingTypeCode, 'N/A')

	,Encounter.CasenoteNo
	,Encounter.AppointmentCreateDate
	,Encounter.EpisodicGpCode

	,EpisodicGpPracticeCode =
		coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredGpPracticeCode, 'X99999')

	,Encounter.DoctorCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12

	,PrimaryOperationCode =
		coalesce(Encounter.PrimaryOperationCode , '##')

	,Encounter.PrimaryOperationDate
	,Encounter.SecondaryOperationCode1
	,Encounter.SecondaryOperationDate1
	,Encounter.SecondaryOperationCode2
	,Encounter.SecondaryOperationDate2
	,Encounter.SecondaryOperationCode3
	,Encounter.SecondaryOperationDate3
	,Encounter.SecondaryOperationCode4
	,Encounter.SecondaryOperationDate4
	,Encounter.SecondaryOperationCode5
	,Encounter.SecondaryOperationDate5
	,Encounter.SecondaryOperationCode6
	,Encounter.SecondaryOperationDate6
	,Encounter.SecondaryOperationCode7
	,Encounter.SecondaryOperationDate7
	,Encounter.SecondaryOperationCode8
	,Encounter.SecondaryOperationDate8
	,Encounter.SecondaryOperationCode9
	,Encounter.SecondaryOperationDate9
	,Encounter.SecondaryOperationCode10
	,Encounter.SecondaryOperationDate10
	,Encounter.SecondaryOperationCode11
	,Encounter.SecondaryOperationDate11
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.ReferralDate
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.AppointmentCategoryCode
	,Encounter.AppointmentCreatedBy
	,Encounter.AppointmentCancelDate
	,Encounter.LastRevisedDate
	,Encounter.LastRevisedBy
	,Encounter.OverseasStatusFlag
	,Encounter.PatientChoiceCode
	,Encounter.ScheduledCancelReasonCode
	,Encounter.PatientCancelReason
	,Encounter.DischargeDate
	,Encounter.QM08StartWaitDate
	,Encounter.QM08EndWaitDate
	,Encounter.DestinationSiteCode
	,Encounter.EBookingReferenceNo
	,Encounter.InterfaceCode
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.LocalAdminCategoryCode
	,Encounter.PCTCode
	,Encounter.LocalityCode

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) > 1 and
	datediff(day, DateOfBirth, Encounter.AppointmentDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.AppointmentDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) > 28 and
	datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.AppointmentDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.AppointmentDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.AppointmentDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.AppointmentDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.AppointmentDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.AppointmentDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.AppointmentDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.AppointmentDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,HRGCode =
		coalesce(HRG4Encounter.HRGCode, 'ZZZ')
	,Cases = 1
	,LengthOfWait =
		datediff(
			 day
			,Encounter.ReferralDate
			,Encounter.AppointmentDate
		)
	,Encounter.IsWardAttender
	,RTTActivity = null
		--convert(
		--	bit
		--	,case
		--	when
		--		Encounter.SpecialtyCode not in 
		--		(
		--		select
		--			SpecialtyCode
		--		from
		--			RTT.dbo.ExcludedSpecialty
		--		)

		--	-- 'GENE' ?
		--	and	Encounter.ConsultantCode <> 'FRAC'

		--	--ward attenders
		--	and	Encounter.IsWardAttender = 0

		--	and	Encounter.SiteCode != 'FMSK'

		--	--attended
		--	and	Encounter.DNACode in ('1', '5', '6')

		--	and	not exists
		--		(
		--		select
		--			1
		--		from
		--			RTT.dbo.RTTOPClockStop ClockChangeReason
		--		where
		--			ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
		--		and	ClockChangeReason.SourceEntityRecno = Encounter.SourceEncounterNo
		--		and	(
		--				coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
		--			or	coalesce(ClockChangeReason.ClockStopReasonCode, '') = 'NOP' --Not an 18 week pathway
		--			)
		--		)

		--	then 1
		--	else 0
		--	end
		--)

	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null
		then 'U'
		when
			(
				--RTTOPClockStop.ClockStopDate = Encounter.AppointmentDate
			--or	Encounter.RTTEndDate = Encounter.AppointmentDate
				Encounter.RTTEndDate = Encounter.AppointmentDate
			)
		and Encounter.RTTBreachDate < Encounter.AppointmentDate
		then 'B'
		else 'N'
		end
	,Encounter.ClockStartDate
	,RTTBreachDate
	,RTTTreated = null
		--case
		--when RTTStatus.ClockStopFlag = 1 then 1
		--when RTTOPClockStop.ClockStopDate = Encounter.AppointmentDate then 1
		--when Encounter.RTTEndDate = Encounter.AppointmentDate then 1
		--else 0
		--end

	,DirectorateCode = coalesce(Encounter.DirectorateCode , 'N/A')
	,ReferringSpecialtyTypeCode =  coalesce(Encounter.ReferringSpecialtyTypeCode , 'U')
	,[InterpreterIndicator]
	,DerivedFirstAttendanceFlag
from
	Warehouse.OP.Encounter Encounter

left join Warehouse.OP.HRG4Encounter HRG4Encounter
on	HRG4Encounter.EncounterRecno = Encounter.EncounterRecno

left join Warehouse.WH.RTTStatus RTTStatus
on	RTTStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode

--left join RTT.dbo.RTTOPClockStop RTTOPClockStop
--on	RTTOPClockStop.SourcePatientNo = Encounter.SourcePatientNo
--and	RTTOPClockStop.SourceEntityRecno = Encounter.SourceEncounterNo
--and	RTTOPClockStop.ClockStopDate is not null
--and	not exists
--	(
--	select
--		1
--	from
--		RTT.dbo.RTTOPClockStop Previous
--	where
--		Previous.SourcePatientNo = Encounter.SourcePatientNo
--	and Previous.SourceEntityRecno = Encounter.SourceEncounterNo
--	and	Previous.ClockStopDate is not null
--	and	(
--			coalesce(Previous.Updated, Previous.Created) > coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
--		or	(
--				coalesce(Previous.Updated, Previous.Created) = coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
--			and	Previous.RTTClockStopRecno > RTTOPClockStop.RTTClockStopRecno
--			)
--		)
--	)

where
	Encounter.AppointmentDate >= '1 Apr 2009'


select @RowsInserted = @@rowcount


----GUM
--insert into dbo.BaseOP
--	(
--	 EncounterRecno
--	,SourceUniqueID
--	,AppointmentDate
--	,ConsultantCode
--	,SpecialtyCode
--	,EpisodicGpPracticeCode
--	,PCTCode
--	,AgeCode
--	,ClinicCode
--	,Cases
--	,SiteCode
--	,SourceOfReferralCode
--	,BookingTypeCode
--	,HRGCode
--	,LengthOfWait
--	,IsWardAttender
--	,RTTActivity
--	,RTTBreachStatusCode
--	,DNACode
--	,FirstAttendanceFlag
--	,AppointmentTypeCode
--	,RTTTreated
--)
--select
--	 Encounter.EncounterRecno * -1 --to make unique
--	,Encounter.EncounterRecno
--	,AppointmentDate = Encounter.TreatmentDate
--	,ConsultantCode = 'N/A'
--	,SpecialtyCode = 'GUM'
--	,EpisodicGpPracticeCode = 'X99999'
--	,Encounter.PCTCode
--	,AgeCode = 'Age Unknown'
--	,ClinicCode =  'GUM|' + Encounter.SiteCode
--	,Cases = Encounter.Patients
--	,Encounter.SiteCode
--	,SourceOfReferralCode = 'SR' --self referral
--	,BookingTypeCode = 'N/A'
--	,HRGCode = 'ZZZ'
--	,LengthOfWait = 0
--	,IsWardAttender = 0
--	,RTTActivity = 1
--	,RTTBreachStatusCode = 'N'
--	,DNACode = '1'
--	,FirstAttendanceFlag = '1'
--	,AppointmentTypeCode = 'N/A'
--	,RTTTreated = 1
--from
--	RTT.dbo.GUM Encounter


select @RowsInserted = @RowsInserted + @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseOP', @Stats, @StartTime


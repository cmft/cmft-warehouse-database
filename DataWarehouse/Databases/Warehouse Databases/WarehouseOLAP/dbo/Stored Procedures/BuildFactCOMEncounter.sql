﻿
CREATE procedure dbo.BuildFactCOMEncounter

as

begin
	set nocount on;
	
set dateformat dmy

declare @starttime datetime
declare @elapsed int
declare @rowsdeleted int
declare @rowsinserted int
declare @stats varchar(255)
declare @from smalldatetime
declare @to smalldatetime

select @starttime = getdate()



truncate table dbo.FactCOMEncounter

insert into dbo.FactCOMEncounter
(
	 EncounterRecNo
	,sourceencounterid
	,sourceuniqueid
	,specialtyid
	,staffteamid
	,encounterprofessionalcarerid
	,startdate
	,encounteroutcomeCode
	,referralid
	,patientsourceid
	,encounterregisteredpracticecode
	,encountertypeid
	,scheduletypeid
	,canceledreasonid
	,encounterlocationid
	,countcases
	,countduration
	,countinterventions
	,jointactivityflag
	,starttimeofday
	,agecode
	,ethniccategoryid
	,patientsexid
	,metriccode
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

)
--first attendances
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=EncounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fatt'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'attd'

union all

--follow-up attendances
select
	  EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'ratt'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'attd'

union all

--first dna
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fdna'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'dnat'

union all

--follow-up dna
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'rdna'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'dnat'


union all

--first cancelled by hospital
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fchos'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'chos'

union all

--follow-up cancelled by hospital
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'rchos'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'chos'

union all

--first cancelled by patient
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fcpat'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'cpat'

union all

--follow-up cancelled by patient
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'rcpat'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'cpat'

union all

--first cancelled by other
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fcoth'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'coth'

union all

--follow-up cancelled by other
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'rcoth'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'coth'

union all

--first not specified
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fnspd'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'nspd'

union all

--follow-up not specified
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'rnspd'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'nspd'

union all

--first planned
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fplnd'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'plnd'

union all

--follow-up planned
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'rplnd'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'plnd'

union all

--first un-actualised
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fuact'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'uact'

union all

--follow-up un-actualised
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'ruact'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'uact'

union all

--first patient deceased
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fpdcd'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode = 'pdcd'

union all

--follow-up patient deceased
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'rpdcd'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode = 'pdcd'

union all

--activity checksum
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'chkact'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter

union all

--First activity
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'FACT'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where
	encounter.derivedfirstattendanceflag = 1

union all

--Follow-up activity
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'RACT'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where
	encounter.derivedfirstattendanceflag = 2

union all

--first cancelled
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'fcancel'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 1
and EncounterOutcomeCode IN ('coth','cpat','chos')

union all

--follow-up cancelled
select
	 EncounterRecNo
	,sourceencounterid = sourceencounterid
	,sourceuniqueid=sourceuniqueid
	,specialtyid=isnull(specialtyid,-1)
	,staffteamid=isnull(staffteamid,-1)
	,encounterprofessionalcarerid=isnull(encounterprofessionalcarerid,-1)
	,startdate=startdate
	,encounterOutcomeCode=encounterOutcomeCode
	,referralid=referralid
	,patientsourceid=patientsourceid
	,patientencounterregisteredpracticecode=patientencounterregisteredpracticecode
	,encountertypeid=encountertypeid
	,scheduletypeid=scheduletypeid
	,canceledreasonid=canceledreasonid
	,encounterlocationid=encounterlocationid
	,countcases = 1
	,countduration = encounterduration
	,countinterventions = encounterinterventionscount
	,jointactivityflag = jointactivityflag
	,starttimeofday = starttimeofday
	,agecode = agecode
	,ethniccategoryid = isnull(patientethnicgroupid,-1)
	,patientsexid = isnull(patientsexid,-1)
	,metriccode = 'rcancel'
	,ReferralReceivedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID

from
	dbo.basecomencounter encounter
where 
	encounter.derivedfirstattendanceflag = 2
and EncounterOutcomeCode IN ('coth','cpat','chos')

select @rowsinserted = @@rowcount

select
	 @from = min(EncounterFact.StartDate)
	,@to = max(EncounterFact.StartDate)

from
	dbo.FactCOMEncounter EncounterFact
	
where
	EncounterFact.StartDate is not null

exec buildcalendarbase @from, @to


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec WriteAuditLogEvent 'LoadOLAPFactCOMEncounter', @Stats, @StartTime
END;


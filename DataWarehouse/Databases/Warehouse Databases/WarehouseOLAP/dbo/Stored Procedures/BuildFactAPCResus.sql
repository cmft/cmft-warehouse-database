﻿


CREATE proc [dbo].[BuildFactAPCResus]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactAPCResus

insert into dbo.FactAPCResus

(
EncounterRecno
,AdmissionDate
,EventDate
,LocationCode
,ROSCTimeOfDayCode
,DeathTimeOfDayCode
,CollapseTimeOfDayCode
,CPRCalloutTimeOfDayCode
,CPRArriveTimeOfDayCode
,ArrestConfirmTimeOfDayCode
,CPRStartTimeOfDayCode
,AwakeningTimeOfDayCode
,AwakeningDate
,DischargeDate
,DateOfDeath
,CallOutTypeID
,PotentiallyAvoidable
,AgeID
,ConsultantCode
,PrimaryDiagnosisCode
,PrimaryOperationCode
,AdmissionMethodCode
,DischargeMethodCode
,EthnicOriginCode
,SexCode
,InterfaceCode
,Cases
)

select
	BaseResus.EncounterRecno
	,BaseResus.AdmissionDate
	,EventDate
	,LocationCode
	,ROSCTimeOfDayCode
	,DeathTimeOfDayCode
	,CollapseTimeOfDayCode
	,CPRCalloutTimeOfDayCode
	,CPRArriveTimeOfDayCode
	,ArrestConfirmTimeOfDayCode
	,CPRStartTimeOfDayCode
	,AwakeningTimeOfDayCode
	,AwakeningDate
	,BaseResus.DischargeDate
	,BaseResus.DateOfDeath
	,CallOutTypeID
	,PotentiallyAvoidable = ebmPAI
	,AgeID
	,ConsultantCode = 
				coalesce(
					Encounter.ConsultantCode
				,'N/A'
				)
	,PrimaryDiagnosisCode = 
				coalesce(
					left(Encounter.PrimaryDiagnosisCode, 5)
				,'##'
				)
	,PrimaryOperationCode = 
				coalesce(
					left(Encounter.PrimaryOperationCode, 5)
				,'##'
				)
	,AdmissionMethodCode = 
				coalesce(
					Encounter.AdmissionMethodCode
				,'NA'
				)
	,DischargeMethodCode = 
				coalesce(
					Encounter.DischargeMethodCode
				,'##'
				)
	,EthnicOriginCode = 
				coalesce(
					Encounter.EthnicOriginCode
				,'X'
				)
	,SexCode =
		case Encounter.SexCode
		when '' then '0'
		when 'M' then '1'
		when 'F' then '2'
		when 'I' then '9'
		else '#'
		end
	,BaseResus.InterfaceCode
	,Cases
from
	dbo.BaseResus

inner join Warehouse.APC.EncounterResus
on	BaseResus.EncounterRecno = EncounterResus.ResusEncounterRecno

inner join Warehouse.APC.Encounter
on	EncounterResus.APCEncounterRecno = Encounter.EncounterRecno



select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	







﻿

CREATE procedure [dbo].[BuildPASwrkInactivePatient] as

/******************************************************************************
	**  Name: dbo.BuildPASwrkInactivePatient
	**  Purpose: 
	**
	**  Populates the PAS.wrkInactivePatient table of patients identified as "inactive"
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 15.03.13		CB         Initial coding
	** 03.06.13		GC			Added Rule for Obstetrics
	******************************************************************************/

set dateformat dmy

select
	SourcePatientNo
into
	#ActivePatient
from
	(
	--patients with orders in the last eight years (actually more like ten)
	select distinct
		SourcePatientNo = InternalPatientNumber
	from
		PAS.Inquire.OCMPTLINK

	union

	--patients with OP activity in the last eight years
	select distinct
		SourcePatientNo = InternalPatientNumber
	from
		(
		select
				InternalPatientNumber
			,AppointmentDate = cast(ApptDate as date)
		from
			PAS.Inquire.OPA
		) Encounter
	where
		AppointmentDate > dateadd(year , -8 , getdate())

	union

	--	SMH Obstetrics patients 
	select distinct
		SourcePatientNo = InternalPatientNumber
	from
		(
		select
			InternalPatientNumber
			,AppointmentDate = cast(ApptDate as date)
			,RefSpecialty
			,ClinicSpecialty
		from
			PAS.Inquire.OPA
		) Encounter
	where
		(
			RefSpecialty in ('OBS','PCON','OBS1','OBS2','OBS3','OBN') or ClinicSpecialty in ('OBS','PCON','OBS1','OBS2','OBS3','OBN')
		)
		and AppointmentDate > dateadd(year , -25 , getdate())

	union

	--patients with consultant episodes in the last eight years
	select distinct
		SourcePatientNo = InternalPatientNumber
	from
		PAS.Inquire.CONSEPISODE
	where
		left(EpsActvDtimeInt , 8) > convert(varchar , dateadd(year , -8 , getdate()) , 112)

	union
	--	SMH Obstetrics patients 	
	select distinct
		SourcePatientNo = InternalPatientNumber
	from
		PAS.Inquire.CONSEPISODE
	where
		SpecialtyCode in ('OBS','PCON','OBS1','OBS2','OBS3','OBN')
		and
		left(EpsActvDtimeInt , 8) > convert(varchar , dateadd(year , -25 , getdate()) , 112)
			

	union

	--patients with ward attendances in the last eight years
	select distinct
		SourcePatientNo = InternalPatientNumber
	from
		PAS.Inquire.WARDATTENDER
	where
		AttendanceDateInternal > convert(varchar , dateadd(year , -8 , getdate()) , 112)
	) ActivePatient

select
	SourcePatientNo
into
	#YoungPatient
from
	Warehouse.PAS.Patient
where
	datediff(year , DateOfBirth , getdate()) < 26



--build work table

truncate table PAS.wrkInactivePatient

insert into PAS.wrkInactivePatient
(
	SourcePatientNo
)


select
	 SourcePatientNo
from
	Warehouse.PAS.Patient
where
	not exists
		(
		select
			1
		from
			#ActivePatient
		where
			#ActivePatient.SourcePatientNo = Patient.SourcePatientNo
		)
and	not exists
		(
		select
			1
		from
			#YoungPatient
		where
			#YoungPatient.SourcePatientNo = Patient.SourcePatientNo
		)
		


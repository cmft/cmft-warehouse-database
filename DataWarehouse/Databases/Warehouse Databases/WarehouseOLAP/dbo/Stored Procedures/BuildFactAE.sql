﻿

CREATE procedure [dbo].[BuildFactAE] as


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactAE

insert into dbo.FactAE
(
	 SourceUniqueID
	,StageCode
	,ArrivalModeCode
	,AttendanceDisposalCode
	,SiteCode
	,RegisteredPracticeCode
	,AttendanceCategoryCode
	,ReferredToSpecialtyCode
	,LevelOfCareCode
	,EncounterBreachStatusCode
	,EncounterStartTimeOfDay
	,EncounterEndTimeOfDay
	,EncounterStartDate
	,EncounterEndDate
	,StageBreachStatusCode
	,StageDurationMinutes
	,StageBreachMinutes
	,StageStartTimeOfDay
	,StageEndTimeOfDay
	,StageStartDate
	,StageEndDate
	,Cases
	,BreachValue
	,DelayCode
	,EncounterDurationMinutes
	,AgeCode
	,UnplannedReattend7DayCases
	,LeftWithoutBeingSeenCases
	,StageStartTime
	,StageEndTime
	,StageDurationBandCode
	,EncounterDurationBandCode
	,Reportable
	,StageEndTimeIsWorkingHours
	,StageStartTimeIsWorkingHours
)
select DISTINCT
	 AEEncounter.SourceUniqueID

	,AEEncounterStage.StageCode

	,AEEncounter.ArrivalModeCode

	,AEEncounter.AttendanceDisposalCode

	,AEEncounter.SiteCode

	,AEEncounter.RegisteredPracticeCode

	,AEEncounter.AttendanceCategoryCode

	,AEEncounter.ReferredToSpecialtyCode

	,AEEncounter.LevelOfCareCode

	,AEEncounter.EncounterBreachStatusCode

	,AEEncounter.EncounterStartTimeOfDay

	,AEEncounter.EncounterEndTimeOfDay

	,AEEncounter.EncounterStartDate

	,AEEncounter.EncounterEndDate

	,StageBreachStatusCode = 
		case
		when AEEncounterStage.StageEndTime is null then 'X'
		when
			datediff(
				 minute
				,AEEncounterStage.StageStartTime
				,AEEncounterStage.StageEndTime
			)
			 > StageBreach.BreachValue
		then 'B'
		else 'N'
		end

	,StageDurationMinutes =
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)

	,StageBreachMinutes =
		datediff(
			 minute
			,AEEncounterStage.StageStartTime
			,AEEncounterStage.StageEndTime
		)
		 - StageBreach.BreachValue


	,StageStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounterStage.StageStartTime), 0)
				,AEEncounterStage.StageStartTime
			)
			,-1
		)

	,StageEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounterStage.StageEndTime), 0)
				,AEEncounterStage.StageEndTime
			)
			,-1
		)

	,StageStartDate =
		coalesce(
			 dateadd(day, datediff(day, 0, AEEncounterStage.StageStartTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounterStage.StageEndTime), 0)
		)

	,StageEndDate =
		coalesce(
			 dateadd(day, datediff(day, 0, AEEncounterStage.StageEndTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounterStage.StageStartTime), 0)
		)

	,Cases = 1

	,BreachValue =
		case
		when AEEncounterStage.StageEndTime is null 
		then null
		else StageBreach.BreachValue
		end

	,DelayCode =
		row_number() over(
		partition by AEEncounter.SourceUniqueID
		order by
			coalesce(
				datediff(
					 minute
					,AEEncounterStage.StageStartTime
					,AEEncounterStage.StageEndTime
				) * 1.0
				 /
				StageBreach.BreachValue * 1.0
				,1.0
			) desc
		)

	,AEEncounter.EncounterDurationMinutes

	,AEEncounter.AgeCode

	,AEEncounter.UnplannedReattend7Day

	,AEEncounter.LeftWithoutBeingSeenCases

	,AEEncounterStage.StageStartTime

	,AEEncounterStage.StageEndTime

	,StageDurationBandCode =
		coalesce(
			case 
			when datediff(minute , AEEncounterStage.StageStartTime , AEEncounterStage.StageEndTime) < 0 then -1
			when datediff(minute , AEEncounterStage.StageStartTime , AEEncounterStage.StageEndTime) > 1440 then 1440
			else datediff(minute , AEEncounterStage.StageStartTime , AEEncounterStage.StageEndTime)
			end
			, -2
		)

	,EncounterDurationBandCode =
		coalesce(
			case 
			when AEEncounter.EncounterDurationMinutes < 0 then -1
			when AEEncounter.EncounterDurationMinutes > 1440 then 1440
			else AEEncounter.EncounterDurationMinutes
			end
			, -2
		)

	,Reportable
	
	,StageEndTimeIsWorkingHours =	
			CASE
				WHEN		
						datediff(minute,dateadd(day, datediff(day, 0, AEEncounterStage.StageEndTime), 0),AEEncounterStage.StageEndTime) BETWEEN 480 AND 1109 
					AND 
						dbo.udfIsWeekDay(AEEncounterStage.StageEndTime) = 1
					THEN 1
				ELSE 0
			END
	,StageStartTimeIsWorkingHours =	
			CASE
				WHEN		
						datediff(minute,dateadd(day, datediff(day, 0, AEEncounterStage.StageStartTime), 0),AEEncounterStage.StageStartTime) BETWEEN 480 AND 1109 
					AND 
						dbo.udfIsWeekDay(AEEncounterStage.StageStartTime) = 1
					THEN 1
				ELSE 0
			END
		

from
	dbo.BaseAE AEEncounter

inner join
	(
	--in department cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'INDEPARTMENT'
		,StageStartTime = AEEncounter.ArrivalTime
		--,StageEndTime = AEEncounter.AttendanceConclusionTime
		,StageEndTime = 
			coalesce(
				 AEEncounter.PCDULeftDepartmentTime
				,AEEncounter.CDULeftDepartmentTime
				,AEEncounter.DepartureTime
		)
	from
		dbo.BaseAE AEEncounter

	union all

	--Seen For Treatment cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'SEENFORTREATMENT'
		,StageStartTime = AEEncounter.ArrivalTime
		,StageEndTime = coalesce(AEEncounter.ClinicalAssessmentTime, AEEncounter.SeenForTreatmentTime)
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.SeenForTreatmentTime is not null
	or	AEEncounter.ClinicalAssessmentTime is not null


	union all

	--Awaiting Diagnostic cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'AWAITINGDIAGNOSTIC'
		,StageStartTime = AEInvestigation.InvestigationDate
		,StageEndTime = AEInvestigation.ResultDate
	from
		dbo.BaseAE AEEncounter

	inner join Warehouse.AE.Investigation AEInvestigation
	on	AEInvestigation.AESourceUniqueID = AEEncounter.SourceUniqueID
	and	AEInvestigation.SequenceNo = 1

	union all

	-- Arrival to X-ray Available
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'ARRIVALTOXRAYAVAILABLE'
		,StageStartTime = AEEncounter.ArrivalTime
		,StageEndTime = AEEncounter.FromXrayTime
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.ToXrayTime is not null

	union all

	-- Awaiting X-ray
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'AWAITINGXRAY'
		,StageStartTime = AEEncounter.ToXrayTime
		,StageEndTime = AEEncounter.FromXrayTime
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.ToXrayTime is not null

	union all

	--Awaiting Specialty cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'AWAITINGSPECIALTY'
		,StageStartTime = AEEncounter.ToSpecialtyTime
		,StageEndTime = AEEncounter.SeenBySpecialtyTime
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.ToSpecialtyTime is not null

	union all

	--Awaiting Specialty Conclusion cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'AWAITINGSPECIALTYCONCLUSION'
		,StageStartTime = AEEncounter.SeenBySpecialtyTime
		,StageEndTime = AEEncounter.AttendanceConclusionTime
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.SeenBySpecialtyTime is not null

	union all

	--Awaiting Bed
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'AWAITINGBED'
		,StageStartTime = AEEncounter.DecisionToAdmitTime
		,StageEndTime = AEEncounter.DepartureTime
	from
		dbo.BaseAE AEEncounter
	
	left join OlapAEAttendanceDisposal Disposal
	on	Disposal.AttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
	
	where
		AEEncounter.DecisionToAdmitTime is not null
	and	
	Disposal.AttendanceDisposalNationalCode = '01' --admitted

	union all

	--Awaiting Bed (Adjusted)
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'AWAITINGBEDADJUSTED'
		,StageStartTime = 
			coalesce(
				 AEEncounter.DecisionToAdmitTime
				,AEEncounter.AttendanceConclusionTime
				,AEEncounter.DepartureTime
			)
		,StageEndTime = AEEncounter.DepartureTime
	from
		dbo.BaseAE AEEncounter
	
	left join OlapAEAttendanceDisposal Disposal
	on	Disposal.AttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
	
	where
		Disposal.AttendanceDisposalNationalCode = '01' --admitted

	union all

	--Triage cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'TRIAGE'
		,StageStartTime = AEEncounter.ArrivalTime
		,StageEndTime = AEEncounter.InitialAssessmentTime
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.InitialAssessmentTime is not null

	union all

	--registration to departure
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'REGISTRATIONTODEPARTURE'
		,StageStartTime =
			coalesce(
				 AEEncounter.RegisteredTime
				,AEEncounter.ArrivalTime
			)
		,StageEndTime = AEEncounter.DepartureTime
	from
		dbo.BaseAE AEEncounter

	union all

	-- Awaiting Transport cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'AWAITINGTRANSPORT'
		,StageStartTime = AEEncounter.TransportRequestTime
		,StageEndTime = AEEncounter.DepartureTime
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.TransportRequestTime is not null

	union all

	-- Ascribe In Department cases
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'INDEPARTMENTASCRIBE'
		,StageStartTime = AEEncounter.ArrivalTime
		,StageEndTime = AEEncounter.AscribeLeftDeptTime
	from
		dbo.BaseAE AEEncounter

-- CCB 2011-04-07
-- new stages added

	union all

	-- Arrival to Triage, adjusted for the influence of Ambulance Arrival Time, if exists
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'ARRIVALTOTRIAGEADJUSTED'
		,StageStartTime = AEEncounter.ArrivalTimeAdjusted

		-- CCB 2011-06-06 in some cases triage may occur after being seen by a doctor, so use the earliest time
		,StageEndTime =
			case
			when coalesce(AEEncounter.ClinicalAssessmentTime, AEEncounter.SeenForTreatmentTime) is null then AEEncounter.InitialAssessmentTime
			when coalesce(AEEncounter.ClinicalAssessmentTime, AEEncounter.SeenForTreatmentTime) < AEEncounter.InitialAssessmentTime then coalesce(AEEncounter.ClinicalAssessmentTime, AEEncounter.SeenForTreatmentTime)
			else AEEncounter.InitialAssessmentTime
			end
	from
		dbo.BaseAE AEEncounter

	union all

	-- Ambulance Arrival to Arrival in A&E
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'INAMBULANCE'
		,StageStartTime = AEEncounter.AmbulanceArrivalTime
		,StageEndTime = AEEncounter.ArrivalTime
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.AmbulanceArrivalTime is not null

	union all

	-- Arrival to Departure, Adjusted for the influence of Ambulance Arrival Time, if exists
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'INDEPARTMENTADJUSTED'
		,StageStartTime = AEEncounter.ArrivalTimeAdjusted
		--,StageEndTime = AEEncounter.AttendanceConclusionTime
		,StageEndTime = 
			coalesce(
				 AEEncounter.PCDULeftDepartmentTime
				,AEEncounter.CDULeftDepartmentTime
				,AEEncounter.DepartureTime
			)
	from
		dbo.BaseAE AEEncounter

	union all

	--Seen For Treatment cases adjusted
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'SEENFORTREATMENTADJUSTED'
		,StageStartTime = AEEncounter.ArrivalTimeAdjusted
		,StageEndTime = coalesce(AEEncounter.ClinicalAssessmentTime, AEEncounter.SeenForTreatmentTime)
	from
		dbo.BaseAE AEEncounter
	where
		AEEncounter.SeenForTreatmentTime is not null
	or	AEEncounter.ClinicalAssessmentTime is not null
	
	union all

-- GS 2011-08-01
-- new stages added

	--Seen For Treatment cases adjusted include cases where an expected time seen has not been recorded
	select
		 SourceUniqueID = AEEncounter.SourceUniqueID
		,StageCode = 'SEENFORTREATMENTADJUSTEDINC'
		,StageStartTime = AEEncounter.ArrivalTimeAdjusted
		,StageEndTime = coalesce(AEEncounter.ClinicalAssessmentTime, AEEncounter.SeenForTreatmentTime)
	from
		dbo.BaseAE AEEncounter

	left join dbo.OlapAEAttendanceDisposal
	on	AEEncounter.AttendanceDisposalCode = OlapAEAttendanceDisposal.AttendanceDisposalCode

	WHERE 
		(		
			AEEncounter.SeenForTreatmentTime is not null
		OR	AEEncounter.ClinicalAssessmentTime is not null
		)
	OR
		(		
			AEEncounter.SeenForTreatmentTime is null
		and	AEEncounter.ClinicalAssessmentTime is null
		AND OlapAEAttendanceDisposal.AttendanceDisposalNationalCode not in ('12','13')
		)
	) AEEncounterStage
on	AEEncounterStage.SourceUniqueID = AEEncounter.SourceUniqueID

left join Warehouse.AE.StageBreach EncounterBreach
on	EncounterBreach.StageCode = 'INDEPARTMENT'
and
	datediff(
		 minute
		,AEEncounter.ArrivalTime
		--,AEEncounter.AttendanceConclusionTime
		,coalesce(
				 AEEncounter.PCDULeftDepartmentTime
				,AEEncounter.CDULeftDepartmentTime
				,AEEncounter.DepartureTime
		)
	)
	 > EncounterBreach.BreachValue


inner join Warehouse.AE.StageBreach StageBreach
on	StageBreach.StageCode = AEEncounterStage.StageCode

where
	coalesce(
		 AEEncounter.PCDULeftDepartmentTime
		,AEEncounter.CDULeftDepartmentTime
		,AEEncounter.DepartureTime
		,dateadd(day , -1 , datediff(day , 0 , (select DateValue from Warehouse.dbo.Parameter where Parameter = 'LOADAEDATE')))
	) < dateadd(day , 0 , datediff(day , 0 , (select DateValue from Warehouse.dbo.Parameter where Parameter = 'LOADAEDATE')))
and	AEEncounter.ArrivalDate < dateadd(day , 0 , datediff(day , 0 , (select DateValue from Warehouse.dbo.Parameter where Parameter = 'LOADAEDATE')))

select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.EncounterStartDate)
	,@to = max(EncounterFact.EncounterEndDate)
from
	dbo.FactAE EncounterFact
where
	EncounterFact.EncounterEndDate is not null


exec BuildOlapPractice 'AE'
exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactAE', @Stats, @StartTime


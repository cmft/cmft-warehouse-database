﻿

CREATE procedure [dbo].[BuildFactTheatreOperation] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactTheatreOperation

insert into dbo.FactTheatreOperation
	(
	 SessionSourceUniqueID
	,SourceUniqueID
	,TheatreCode
	,CancelReasonCode
	,ConsultantCode
	,EpisodicConsultantCode
	,SurgeonCode
	,AnaesthetistCode
	,PrimaryProcedureCode
	,OperationGroupCode
	,AgeCode
	,SexCode
	,AdmissionTypeCode
	,OperationDate
	,OperationTypeCode
	,TheatreSessionPeriodCode
	,SessionConsultantCode
	,TimetableConsultantCode
	,TemplateConsultantCode
	,SessionAnaesthetistCode
	,TimetableAnaesthetistCode
	,TemplateAnaesthetistCode
	,SessionSpecialtyCode
	,TimetableSpecialtyCode
	,TemplateSpecialtyCode
	,Biohazard
	,ASAScoreCode
	,ReceptionTime
	,ReceptionWaitTime
	,AnaestheticWaitTime
	,AnaestheticTime
	,OperationTime
	,RecoveryTime
	,ReturnTime
	,PatientTime
	,Operation
	,TurnaroundTime
	,PatientTimeWithinPlan
	,OperationStartTimeOfDay
	,OperationEndTimeOfDay
	)
select
	 SessionSourceUniqueID = convert(int, TheatreOperation.SessionSourceUniqueID)
	,TheatreOperation.SourceUniqueID
	,TheatreCode = coalesce(TheatreOperation.TheatreCode, 0)
	,CancelReasonCode = coalesce(TheatreOperation.CancelReasonCode, 0)
	,ConsultantCode = coalesce(TheatreOperation.ConsultantCode, 0)
	,EpisodicConsultantCode = coalesce(TheatreOperation.EpisodicConsultantCode, -1)
	,SurgeonCode = TheatreOperation.Surgeon1Code
	,AnaesthetistCode = TheatreOperation.Anaesthetist1Code
	,PrimaryProcedureCode = coalesce(TheatreOperation.PrimaryProcedureCode, '##')
	,OperationGroupCode =  coalesce(left(TheatreOperation.PrimaryProcedureCode , 3) , '##')
	,TheatreOperation.AgeCode

	,SexCode =
		case
		when TheatreOperation.SexCode = ''
		then 'N' --N/A
		else TheatreOperation.SexCode
		end

	,TheatreOperation.AdmissionTypeCode

	,OperationDate =
		case
		when TheatreOperation.OperationDate > '6 Jun 2079' -- beyond smalldatetime
		then '1 Jan 1900' -- smallest smalldatetime
		else TheatreOperation.OperationDate
		end

	,OperationTypeCode = 
		case coalesce(TheatreOperation.OperationTypeCode, 'N/A')
		when 'URGENT' then 'ELECTIVE'
		when ''	then 'N/A'
		else TheatreOperation.OperationTypeCode
		end

	,TheatreSessionPeriodCode =
		case
		when
			left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, Session.EndTime, 8), 5) > '16:29' then 'AD' --All Day

		when left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29' then 'AM' --Morning
		when left(convert(varchar, Session.StartTime, 8), 5) between '12:30' and '16:29' then 'PM' --Afternoon
		when left(convert(varchar, Session.StartTime, 8), 5) between '16:30' and '23:59' then 'VN' --Evening
		else 'NA'
		end

	,SessionConsultantCode = coalesce(Session.ConsultantCode, 0)

	,TimetableConsultantCode = coalesce(Session.TimetableConsultantCode, 0)

	,TemplateConsultantCode = coalesce(Session.TemplateConsultantCode, 0)

	,SessionAnaesthetistCode = coalesce(Session.AnaesthetistCode1, 0)

	,TimetableAnaesthetistCode = coalesce(Session.TimetableAnaesthetistCode, 0)

	,TemplateAnaesthetistCode = coalesce(Session.TemplateAnaesthetistCode, 0)

	,SessionSpecialtyCode = coalesce(Session.SpecialtyCode, 0)

	,TimetableSpecialtyCode = coalesce(Session.TimetableSpecialtyCode, 0)

	,TemplateSpecialtyCode = coalesce(Session.TemplateSpecialtyCode, 0)

	,Biohazard = convert(bit, TheatreOperation.BiohazardFlag)

	,ASAScoreCode = coalesce(TheatreOperation.ASAScoreCode, 0)

	,ReceptionTime =
		datediff(minute, TheatreOperation.SentForTime, TheatreOperation.InSuiteTime)

	,ReceptionWaitTime =
		datediff(minute, TheatreOperation.InSuiteTime, TheatreOperation.InAnaestheticTime)

	,AnaestheticWaitTime =
		datediff(minute, TheatreOperation.InAnaestheticTime, TheatreOperation.AnaestheticInductionTime)

	,AnaestheticTime =
		datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.AnaestheticReadyTime)

	,OperationTime =
		datediff(minute, TheatreOperation.ProcedureStartTime, TheatreOperation.ProcedureEndTime)

	,RecoveryTime =
		datediff(minute, TheatreOperation.InRecoveryTime, TheatreOperation.ReadyToDepartTime)

	,ReturnTime =
		datediff(minute, TheatreOperation.ReadyToDepartTime, TheatreOperation.DischargeTime)

	,PatientTime =
		datediff(minute, TheatreOperation.AnaestheticInductionTime, TheatreOperation.ProcedureEndTime)

	,Operation = 1

	,TurnaroundTime =
		case
		when TheatreOperation.OperationEndDate > TheatreOperation.NextOperationAnaestheticInductionTime
		then null
		else datediff(minute, TheatreOperation.OperationEndDate, TheatreOperation.NextOperationAnaestheticInductionTime)
		end

	,PatientTimeWithinPlan =
		case
		when TheatreOperation.AnaestheticInductionTime > Session.EndTime or TheatreOperation.ProcedureEndTime < Session.StartTime then 0
		else
			datediff(
				 minute
				,(
					select top 1
						StartTime
					from
						(
						select
							StartTime = AnaestheticInductionTime
						from
							BaseTheatreOperation OperationStart
						where
							OperationStart.SourceUniqueID = TheatreOperation.SourceUniqueID

						union

						select
							StartTime = StartTime
						from
							BaseTheatreSession SessionStart
						where
							SessionStart.SourceUniqueID = TheatreOperation.SessionSourceUniqueID
						) StartTime
					order by
						StartTime desc
				)
				,(
					select top 1
						EndTime
					from
						(
						select
							EndTime = ProcedureEndTime
						from
							BaseTheatreOperation OperationEnd
						where
							OperationEnd.SourceUniqueID = TheatreOperation.SourceUniqueID

						union

						select
							EndTime = EndTime
						from
							BaseTheatreSession SessionEnd
						where
							SessionEnd.SourceUniqueID = TheatreOperation.SessionSourceUniqueID
						) EndTime
					order by
						EndTime
				)
			)
		end

		,OperationStartTimeOfDay =
			coalesce(
				datediff(
					 minute
					,cast(OperationStartDate as date)
					,OperationStartDate
				)
				,-1
			)

		,OperationEndTimeOfDay =
			coalesce(
				datediff(
					 minute
					,cast(TheatreOperation.OperationEndDate as date)
					,TheatreOperation.OperationEndDate
				)
				,-1
			)




from
	dbo.BaseTheatreOperation TheatreOperation

left join dbo.BaseTheatreSession Session
on	Session.SourceUniqueID = TheatreOperation.SessionSourceUniqueID


select @RowsInserted = @@rowcount


select
	 @from = min(cast(EncounterFact.OperationDate as date))
	,@to = max(cast(EncounterFact.OperationDate as date))
from
	dbo.FactTheatreOperation EncounterFact
where
	EncounterFact.OperationDate > '1 Jan 1900'


exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactTheatreOperation', @Stats, @StartTime



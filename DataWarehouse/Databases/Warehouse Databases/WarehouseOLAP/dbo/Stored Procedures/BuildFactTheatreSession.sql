﻿
CREATE procedure [dbo].[BuildFactTheatreSession] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactTheatreSession

insert into dbo.FactTheatreSession
(
	 SourceUniqueID
	,TheatreCode
	,SessionDate
	,ConsultantCode
	,TimetableConsultantCode
	,TemplateConsultantCode
	,AnaesthetistCode
	,TimetableAnaesthetistCode
	,TemplateAnaesthetistCode
	,SpecialtyCode
	,TimetableSpecialtyCode
	,TemplateSpecialtyCode
	,SessionAllocationCode
	,TheatreSessionPeriodCode
	,CancelledSession
	,Session
	,SessionDuration
	,PlannedSessionDuration
	,TheoreticalSessionDuration
	,ActualDuration
	,OverUtilisation
	,UnderUtilisation
	,PlannedStartTimeOfDay
	,PlannedEndTimeOfDay
	,EarlyStart
	,LateStart
	,EarlyFinish
	,LateFinish
	,EarlyStartMinutes
	,LateStartMinutes
	,EarlyFinishMinutes
	,LateFinishMinutes
	,AllTurnaroundsWithin15Minutes
	,OperationTypeCode

)
select
	 Session.SourceUniqueID

	,TheatreCode = Session.TheatreCode

	,SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0)

	,ConsultantCode = coalesce(Session.ConsultantCode, 0)

	,TimetableConsultantCode = coalesce(Session.TimetableConsultantCode, 0)

	,TemplateConsultantCode = coalesce(Session.TemplateConsultantCode, 0)

	,AnaesthetistCode = coalesce(Session.AnaesthetistCode1, 0)

	,TimetableAnaesthetistCode = coalesce(Session.TimetableAnaesthetistCode, 0)

	,TemplateAnaesthetistCode = coalesce(Session.TemplateAnaesthetistCode, 0)

	,SpecialtyCode = coalesce(Session.SpecialtyCode, 0)

	,TimetableSpecialtyCode = coalesce(Session.TimetableSpecialtyCode, 0)

	,TemplateSpecialtyCode = coalesce(Session.TemplateSpecialtyCode, 0)

	,SessionAllocationCode =
		case	
		when 
			coalesce(
				 Session.TimetableConsultantCode
				,Session.ConsultantCode
				,'##'
			) = 
			coalesce(
				 Session.ConsultantCode
				,'##'
			)
		then 'SC' --Same Consultant

		when 
			coalesce(
				 Session.TimetableSpecialtyCode
				,Session.SpecialtyCode
				,'##'
			) = 
			coalesce(
				 Session.SpecialtyCode
				,'##'
			)
		then 'SS' --Same Specialty

		else 'DS' --Different Specialty
		end

	,TheatreSessionPeriodCode =
		case
		when
			left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, Session.EndTime, 8), 5) > '16:29' then 'AD' --All Day

		when left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29' then 'AM' --Morning
		when left(convert(varchar, Session.StartTime, 8), 5) between '12:30' and '16:29' then 'PM' --Afternoon
		when left(convert(varchar, Session.StartTime, 8), 5) between '16:30' and '23:59' then 'VN' --Evening
		else 'NA'
		end

	,CancelledSession = Session.CancelledFlag

	,Session = 1

	,SessionDuration =
		datediff(minute, Session.StartTime, Session.EndTime) -
		case
		when
			left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
		and left(convert(varchar, Session.EndTime, 8), 5) > '16:29' 
		then 60
		else 0
		end

	,PlannedSessionDuration = Session.PlannedSessionMinutes

	,TheoreticalSessionDuration =
		case
		when
			Session.ActualSessionStartTime is null 
		or	Session.ActualSessionEndTime is null
		then null
		else
			Session.PlannedSessionMinutes -
			case
			when
				left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
			and left(convert(varchar, Session.EndTime, 8), 5) > '16:29' 
			then 60
			else 0
			end
		end

	,ActualDuration =
		datediff(
			 minute
			,Session.ActualSessionStartTime
			,Session.ActualSessionEndTime
		) -
--discrepency here however, although all day session duration loses 60 minutes, actual duration does not
		case
		when
			left(convert(varchar, Session.ActualSessionStartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, Session.ActualSessionEndTime, 8), 5) > '16:29'
		then 0
		else 0
		end

	,OverUtilisation =
		case
		when datediff(minute, Session.StartTime, Session.EndTime) > datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)
		then null
		else datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime) - datediff(minute, Session.StartTime, Session.EndTime)
		end

	,UnderUtilisation =
		case
		when datediff(minute, Session.StartTime, Session.EndTime) < datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)
		then null
		else  datediff(minute, Session.StartTime, Session.EndTime) - datediff(minute, Session.ActualSessionStartTime, Session.ActualSessionEndTime)
		end

	,PlannedStartTimeOfDay = left(convert(varchar, Session.StartTime, 8), 5)

	,PlannedEndTimeOfDay = left(convert(varchar, Session.EndTime, 8), 5)

	,EarlyStart =
		case
		when Session.ActualSessionStartTime < Session.StartTime
		then 1
		else 0
		end

	,LateStart =
		case
		when Session.ActualSessionStartTime > Session.StartTime
		then 1
		else 0
		end

	,EarlyFinish =
		case
		when Session.ActualSessionEndTime < Session.EndTime
		then 1
		else 0
		end

	,LateFinish =
		case
		when Session.ActualSessionEndTime > Session.EndTime
		then 1
		else 0
		end

	,EarlyStartMinutes =
		case
		when Session.ActualSessionStartTime < Session.StartTime
		then datediff(minute , Session.ActualSessionStartTime , Session.StartTime)
		else 0
		end

	,LateStartMinutes =
		case
		when Session.ActualSessionStartTime > Session.StartTime
		then datediff(minute , Session.StartTime , Session.ActualSessionStartTime)
		else 0
		end

	,EarlyFinishMinutes =
		case
		when Session.ActualSessionEndTime < Session.EndTime
		then datediff(minute , Session.ActualSessionEndTime , Session.EndTime)
		else 0
		end

	,LateFinishMinutes =
		case
		when Session.ActualSessionEndTime > Session.EndTime
		then datediff(minute , Session.EndTime , Session.ActualSessionEndTime)
		else 0
		end

	,AllTurnaroundsWithin15Minutes =
		case
		when not exists
			(
			select
				1
			from
				BaseTheatreOperation Operation
			where
				Operation.SessionSourceUniqueID = Session.SourceUniqueID
			and datediff(minute, Operation.OperationEndDate, Operation.NextOperationAnaestheticInductionTime) > 15
			)
		then 1
		else 0
		end

	,OperationTypeCode =
		case
		when exists
			(
			select
				1
			from
				BaseTheatreOperation Operation
			where
				Operation.SessionSourceUniqueID = Session.SourceUniqueID
			and Operation.OperationTypeCode in ( 'ELECTIVE' , 'URGENT' )
			) then 'ELECTIVE'
		when exists
			(
			select
				1
			from
				BaseTheatreOperation Operation
			where
				Operation.SessionSourceUniqueID = Session.SourceUniqueID
			and Operation.OperationTypeCode = 'EMERGENCY'
			) then 'EMERGENCY'
		else 'N/A'
		end
from
	dbo.BaseTheatreSession Session


select @RowsInserted = @@rowcount

select
	 @from = min(cast(FactTheatreSession.SessionDate as date))
	,@to = max(cast(FactTheatreSession.SessionDate as date))
from
	dbo.FactTheatreSession
where
	FactTheatreSession.SessionDate is not null


exec BuildCalendarBase @from, @to


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildFactTheatreSession', @Stats, @StartTime

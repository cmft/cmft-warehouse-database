﻿
create proc dbo.BuildFactKPIActual

as

set dateformat dmy


/* Build FactKPIActual */

/* ======== */
/* Activity */
/* ======== */

/* Referrals */

print '/* Referrals */'

truncate table dbo.FactKPIActual

insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'Referrals')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
from 
	dbo.FactRF r
	inner join dbo.OLAPCalendar c 
	on r.EncounterDate = c.TheDate

group by
	c.TheMonth
	
/* GP Referrals */

print '/* GP Referrals */'
	
insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'GP Referrals')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
from 
	dbo.FactRF r
	inner join dbo.OLAPCalendar c 
	on r.EncounterDate = c.TheDate
	inner join [Warehouse].[PAS].[SourceOfReferral] s
	on s.SourceOfReferralCode = r.SourceOfReferralCode

where 
	NationalSourceOfReferralCode = '03'
	

group by
	c.TheMonth
	
/* Non GP Referrals */

print '/* Non GP Referrals */'
	
insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'Non GP Referrals')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
from 
	dbo.FactRF r
	inner join dbo.OLAPCalendar c 
	on r.EncounterDate = c.TheDate
	inner join [Warehouse].[PAS].[SourceOfReferral] s
	on s.SourceOfReferralCode = r.SourceOfReferralCode

where 
	NationalSourceOfReferralCode <> '03'
	

group by
	c.TheMonth
	
	
/* Outpatient Waiting List */

print '/* Outpatient Waiting List */'


insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'Outpatient Waiting List')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
	
from 
	dbo.FactOPWaitingList o
	inner join dbo.OLAPCalendar c 
	on o.CensusDate = c.TheDate
	
group by
	c.TheMonth
	
	
/* New OP Attendances */

print '/* New OP Attendances */'

insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'New Outpatient Attendances')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
from 
	dbo.FactOP o
	inner join dbo.OLAPCalendar c 
	on o.EncounterDate = c.TheDate
	

where 
	MetricCode = 'FATT'

group by
	c.TheMonth
	
	
/* FUP OP Attendances */

print '/* FUP OP Attendances */'

insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'Follow-Up Outpatient Attendances')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
	
from 
	dbo.FactOP o
	inner join dbo.OLAPCalendar c 
	on o.EncounterDate = c.TheDate
	
where 
	MetricCode = 'RATT' -- check
	
group by
	c.TheMonth
	
	
/* Elective Admissions */

print '/* Elective Admissions */'

insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'Elective Admissions')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
	
from 
	dbo.FactAPC a
	inner join dbo.OLAPCalendar c 
	on a.EncounterDate = c.TheDate
	
where 
	MetricCode = 'IEADM'
	
group by
	c.TheMonth
	
	
	
/* Inpatient Waiting List  */

print '/* Inpatient Waiting List  */'

insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'Inpatient Waiting List')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
	
from 
	dbo.FactAPCWaitingList a
	inner join dbo.OLAPCalendar c 
	on a.CensusDate = c.TheDate
	
group by
	c.TheMonth
	
	
	
/* Urgent Admissions */

print '/* Urgent Admissions */'

insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'Urgent Admissions')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
	
from 
	dbo.FactAPC a
	inner join dbo.OLAPCalendar c 
	on a.EncounterDate = c.TheDate
	
where 
	MetricCode = 'EMADM'
	
group by
	c.TheMonth
	

/* A&E Attendances */

print '/* A&E Attendances */'

insert into dbo.FactKPIActual
(
 [MetricCode]
,[Date]
,[Numerator]
,[Denominator]
)

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'A&E Attendances')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Cases)
	,[Denominator] = null
	
from 
	dbo.FactAE a
	inner join dbo.OLAPCalendar c 
	on a.EncounterStartDate = c.TheDate
	
group by
	c.TheMonth

	
/* EDD Completeness */

print '/* EDD Completeness */'

insert into dbo.FactKPIActual

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'EDD Data Completeness')
	,[Date] = c.TheMonth
	,[Numerator] = sum(WithExpectedLOSInPAS) + sum(HasEddBedmanFlag)
	,[Denominator] = sum(Discharges)
		
from 
	dbo.rptEDD_vw e
	inner join dbo.OLAPCalendar c 
	on e.AdmitDate = c.TheDate
where e.CurrentRecordFlag = 1

group by
	c.TheMonth
	

/* EDD Performance */	

print '/* EDD Performance */'

insert into dbo.FactKPIActual

select 
	 --[MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where Metric = 'EDD Performance')
	[Date] = c.TheMonth
	,[Numerator] = sum(
						case
							when Var_ActualExp <= 0 
							then 1 
							else 0
						end
					)
	,[Denominator] = count(SourceSpellNo) 
		
from
	dbo.rptEDD_vw e
	
inner join dbo.OLAPCalendar c 
	on e.AdmitDate = c.TheDate

where
  [CurrentRecordFlag] = 1
  AND HasEddFlag = 'Yes'
  and DischargeDate between '01 may 2011' and '31 may 2011'

group by
	c.TheMonth
		

--declare @start datetime = '01/04/2011'
--,@end datetime = getdate()

--set dateformat dmy

--declare @edddc table
--(
--Division varchar(100)
--,Specialty varchar(100)
--,Ward varchar(100)
--,Consultant varchar(100)
--,POD varchar(100)
--,Discharges int
--,WithExpectedLOSInPAS int
--,HasEddBedmanFlag int
--)

--insert into @edddc
--exec EDD.rptCompletness_get_sp @start,@end

--select * from @edddc
	
	
/* ======= */	
/* Finance */
/* ======= */	
	
/* EBIDTA Margin */

print '/* EBIDTA Margin */'

insert into dbo.FactKPIActual

select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where MetricCode1 = 'EBIDTA')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Result)
	,[Denominator] = null
	
from 
	Warehouse.KPI.SharePointInput s
	inner join dbo.OLAPCalendar c 
	on s.Date = c.TheDate
	
where 
	MetricCode = 'EBIDTA'

group by
	c.TheMonth


/* Rate of Return On Assets */

print '/* Rate of Return On Assets */'

insert into dbo.FactKPIActual

	select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where MetricCode1 = 'RRETAS')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Result)
	,[Denominator] = null
	
from 
	Warehouse.KPI.SharePointInput s
	inner join dbo.OLAPCalendar c 
	on s.Date = c.TheDate
	
where 
	MetricCode = 'RRETAS'

group by
	c.TheMonth
	
	
/* I&E Surplus/Deficit Margin */

print '/* I&E Surplus/Deficit Margin */'

insert into dbo.FactKPIActual

	select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where MetricCode1 = 'IESUDEF')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Result)
	,[Denominator] = null
	
from 
	Warehouse.KPI.SharePointInput s
	inner join dbo.OLAPCalendar c 
	on s.Date = c.TheDate
	
where 
	MetricCode = 'IESUDEF'

group by
	c.TheMonth
	
	
/* Liquidity */

print '/* Liquidity */'

insert into dbo.FactKPIActual

	select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where MetricCode1 = 'LIQUID')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Result)
	,[Denominator] = null
	
from 
	Warehouse.KPI.SharePointInput s
	inner join dbo.OLAPCalendar c 
	on s.Date = c.TheDate
	
where 
	MetricCode = 'LIQUID'

group by
	c.TheMonth
	

/* Monitor Risk Rating */

print '/* Monitor Risk Rating */'

insert into dbo.FactKPIActual

	select 
	 [MetricCode] = (select top 1 MetricCode from dbo.OLAPKPIMetric where MetricCode1 = 'MONRR')
	,[Date] = c.TheMonth
	,[Numerator] = sum(Result)
	,[Denominator] = null
	
from 
	Warehouse.KPI.SharePointInput s
	inner join dbo.OLAPCalendar c 
	on s.Date = c.TheDate
	
where 
	MetricCode = 'MONRR'

group by
	c.TheMonth
	
		
--select distinct MetricCode
--from warehouse.KPI.SharePointInput


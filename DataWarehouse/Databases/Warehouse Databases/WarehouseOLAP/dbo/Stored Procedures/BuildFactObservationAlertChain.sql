﻿








CREATE proc [dbo].[BuildFactObservationAlertChain]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationAlertChain

insert into dbo.FactObservationAlertChain

(
	ObservationSetRecno
	,SpecialtyID
	,WardID
	,StartDate
	,StartTimeOfDayCode
	,OverallRiskIndexCode
	,OverallAssessedStatusID
	,DurationID
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,Duration
)

select
	 ObservationSetRecno
	,SpecialtyID
	,WardID
	,StartDate
	,StartTimeOfDayCode
	,OverallRiskIndexCode
	,OverallAssessedStatusID
	,DurationID = 
				coalesce(
						case 
						when AlertChainDurationMinutes < 0 then -1
						when AlertChainDurationMinutes > 1440 then 1440
						else AlertChainDurationMinutes
						end
					, -2
					)
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,AlertChainDurationMinutes
from
	dbo.BaseObservationObservationSet
where
	IsLastInAlertChain = 1



select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	






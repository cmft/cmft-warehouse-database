﻿








CREATE proc [dbo].[BuildFactObservationSignificantEvent]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationSignificantEvent

insert into dbo.FactObservationSignificantEvent

(
	SignificantEventRecno
	,SpecialtyID
	,WardID
	,SignificantEventTypeID
	,StartDate
	,StartTimeOfDayCode
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
)

select
	SignificantEventRecno
	,SpecialtyID
	,WardID
	,SignificantEventTypeID
	,StartDate
	,StartTimeOfDayCode
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
from
	dbo.BaseObservationSignificantEvent


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	






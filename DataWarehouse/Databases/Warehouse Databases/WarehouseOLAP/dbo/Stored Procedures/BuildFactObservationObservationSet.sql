﻿








CREATE proc [dbo].[BuildFactObservationObservationSet]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationObservationSet

insert into dbo.FactObservationObservationSet

(
	ObservationSetRecno
	,SpecialtyID
	,WardID
	,StartDate
	,StartTimeOfDayCode
	,DueTimeStatusID
	,ObservationNotTakenReasonID
	,ClinicianPresentSeniorityID
	,OverallRiskIndexCode
	,OverallAssessedStatusID
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,OverallAssessedStatusScore
)

select
	 ObservationSetRecno
	,SpecialtyID
	,WardID
	,StartDate
	,StartTimeOfDayCode
	,DueTimeStatusID
	,ObservationNotTakenReasonID
	,ClinicianPresentSeniorityID
	,OverallRiskIndexCode
	,BaseObservationObservationSet.OverallAssessedStatusID
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,OverallAssessedStatus.OverallAssessedStatusScore
from
	dbo.BaseObservationObservationSet

inner join dbo.OlapObservationOverallAssessedStatus OverallAssessedStatus
on	BaseObservationObservationSet.OverallAssessedStatusID = OverallAssessedStatus.OverallAssessedStatusID
select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	







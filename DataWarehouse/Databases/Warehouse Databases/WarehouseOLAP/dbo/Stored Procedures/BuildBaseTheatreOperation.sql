﻿
CREATE procedure [dbo].[BuildBaseTheatreOperation] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseTheatreOperation

insert into dbo.BaseTheatreOperation
	(
	 DistrictNo
	,SourceUniqueID
	,PatientAgeInYears
	,OperationDay
	,OperationStartDate
	,TheatreCode
	,OperationDate
	,OperationEndDate
	,OperationEndDate2
	,DateOfBirth
	,OperationTypeCode
	,OperationOrder
	,Forename
	,InSuiteTime
	,Surname
	,InAnaestheticTime
	,InRecoveryTime
	,BiohazardFlag
	,ASAScoreCode
	,UnplannedReturnReasonCode
	,ReviewFlag
	,OperationDetailSourceUniqueID
	,Operation
	,PatientDiedFlag
	,CauseOfDeath
	,DiagnosisCode
	,PatientAgeInYearsMonths
	,PatientAgeInYearsDays
	,SexCode
	,TransferredToWardCode
	,PatientSourceUniqueNo
	,PatientClassificationCode
	,PatientBookingSourceUniqueID
	,SpecialtyCode
	,FromWardCode
	,ToWardCode
	,ConsultantCode
	,Address1
	,Address3
	,Postcode
	,HomeTelephone
	,BusinessTelephone
	,Surgeon1Code
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist1Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,AnaesthetistNurseCode
	,InstrumentNurseCode
	,Technician1Code
	,Technician2Code
	,Technician3Code
	,SessionNumber
	,CampusCode
	,SpecimenCode
	,DischargeTime
	,StaffInChargeCode
	,EscortReturnedTime
	,OperationCancelledFlag
	,DelayCode
	,AnaestheticInductionTime
	,ReadyToDepartTime
	,OtherCommentCode
	,PACUClassificationCode
	,PostOperationInstructionCode
	,ClinicalPriorityCode
	,EmergencyOperationSurgeonCode
	,SecondaryStaffCode
	,HospitalDischargeDate
	,HospitalEpisodeNo
	,UnplannedReturnToTheatreFlag
	,AdmissionTime
	,WaitingListSourceUniqueNo
	,AdmissionTypeCode
	,CalfCompressorUsedFlag
	,OperatingRoomReadyTime
	,DressingTime
	,NursingSetupCompleteTime
	,LastUpdated
	,ProceduralStatusCode
	,ACCNumber
	,ACCIndicatorCode
	,ACCCode
	,UpdatedFlag
	,PatientAddressStatus
	,OperationKey
	,ExtubationTime
	,Address2
	,Address4
	,SentForTime
	,PorterLeftTime
	,PorterCode
	,SecondaryTheatreCode
	,SecondaryConsultantCode
	,SecondaryOrderNumberCode
	,SecondarySessionNumber
	,AdditionalFlag
	,DictatedSurgeonNoteCode
	,AnaestheticReadyTime
	,PreparationReadyTime
	,SessionSourceUniqueID
	,CombinedCaseSessionSourceUniqueID
	,IncisionDetailCode
	,CancelReasonCode
	,CancellationDate
	,APCEncounterSourceUniqueID
	,EpisodicConsultantCode
	,PrimaryProcedureCode
	,AgeCode
	,InTheatreTime
	,ProcedureStartTime
	,ProcedureEndTime
	,NextOperationAnaestheticInductionTime
	,OperationStartTimeOfDay
	,OperationEndTimeOfDay
	)
select
	 Operation.DistrictNo
	,Operation.SourceUniqueID
	,Operation.PatientAgeInYears
	,Operation.OperationDay
	,Operation.OperationStartDate
	,Operation.TheatreCode
	,Operation.OperationDate
	,Operation.OperationEndDate

	,OperationEndDate2 = dateadd(day , 0 , datediff(day , 0 , Operation.OperationEndDate))

	,Operation.DateOfBirth
	,Operation.OperationTypeCode
	,Operation.OperationOrder
	,Operation.Forename
	,Operation.InSuiteTime
	,Operation.Surname
	,Operation.InAnaestheticTime
	,Operation.InRecoveryTime
	,Operation.BiohazardFlag
	,Operation.ASAScoreCode
	,Operation.UnplannedReturnReasonCode
	,Operation.ReviewFlag
	,Operation.OperationDetailSourceUniqueID
	,Operation.Operation
	,Operation.PatientDiedFlag
	,Operation.CauseOfDeath
	,Operation.DiagnosisCode
	,Operation.PatientAgeInYearsMonths
	,Operation.PatientAgeInYearsDays
	,Operation.SexCode
	,Operation.TransferredToWardCode
	,Operation.PatientSourceUniqueNo
	,Operation.PatientClassificationCode
	,Operation.PatientBookingSourceUniqueID
	,Operation.SpecialtyCode
	,Operation.FromWardCode
	,Operation.ToWardCode
	,Operation.ConsultantCode
	,Operation.Address1
	,Operation.Address3
	,Operation.Postcode
	,Operation.HomeTelephone
	,Operation.BusinessTelephone
	,Operation.Surgeon1Code
	,Operation.Surgeon2Code
	,Operation.Surgeon3Code
	,Operation.Anaesthetist1Code
	,Operation.Anaesthetist2Code
	,Operation.Anaesthetist3Code
	,Operation.ScoutNurseCode
	,Operation.AnaesthetistNurseCode
	,Operation.InstrumentNurseCode
	,Operation.Technician1Code
	,Operation.Technician2Code
	,Operation.Technician3Code
	,Operation.SessionNumber
	,Operation.CampusCode
	,Operation.SpecimenCode
	,Operation.DischargeTime
	,Operation.StaffInChargeCode
	,Operation.EscortReturnedTime
	,Operation.OperationCancelledFlag
	,Operation.DelayCode
	,Operation.AnaestheticInductionTime
	,Operation.ReadyToDepartTime
	,Operation.OtherCommentCode
	,Operation.PACUClassificationCode
	,Operation.PostOperationInstructionCode
	,Operation.ClinicalPriorityCode
	,Operation.EmergencyOperationSurgeonCode
	,Operation.SecondaryStaffCode
	,Operation.HospitalDischargeDate
	,Operation.HospitalEpisodeNo
	,Operation.UnplannedReturnToTheatreFlag
	,Operation.AdmissionTime
	,Operation.WaitingListSourceUniqueNo
	,Operation.AdmissionTypeCode
	,Operation.CalfCompressorUsedFlag
	,Operation.OperatingRoomReadyTime
	,Operation.DressingTime
	,Operation.NursingSetupCompleteTime
	,Operation.LastUpdated
	,Operation.ProceduralStatusCode
	,Operation.ACCNumber
	,Operation.ACCIndicatorCode
	,Operation.ACCCode
	,Operation.UpdatedFlag
	,Operation.PatientAddressStatus
	,Operation.OperationKey
	,Operation.ExtubationTime
	,Operation.Address2
	,Operation.Address4
	,Operation.SentForTime
	,Operation.PorterLeftTime
	,Operation.PorterCode
	,Operation.SecondaryTheatreCode
	,Operation.SecondaryConsultantCode
	,Operation.SecondaryOrderNumberCode
	,Operation.SecondarySessionNumber
	,Operation.AdditionalFlag
	,Operation.DictatedSurgeonNoteCode
	,Operation.AnaestheticReadyTime
	,Operation.PreparationReadyTime
	,Operation.SessionSourceUniqueID
	,Operation.CombinedCaseSessionSourceUniqueID
	,Operation.IncisionDetailCode

	,Cancellation.CancelReasonCode
	,Cancellation.CancellationDate

	,APCEncounterSourceUniqueID = Encounter.SourceUniqueID
	,EpisodicConsultantCode = Encounter.ConsultantCode

	,PrimaryProcedureCode = 
		case
		when len(OperationBase.OperationCode1) = 3 then OperationBase.OperationCode1 + '.9'
		else coalesce(OperationBase.OperationCode1 , '##')
		end

	,AgeCode =
	case
	when datediff(day, Operation.DateOfBirth, Operation.OperationDate) is null then 'Age Unknown'
	when datediff(day, Operation.DateOfBirth, Operation.OperationDate) <= 0 then '00 Days'
	when datediff(day, Operation.DateOfBirth, Operation.OperationDate) = 1 then '01 Day'
	when datediff(day, Operation.DateOfBirth, Operation.OperationDate) > 1 and
	datediff(day, Operation.DateOfBirth, Operation.OperationDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, Operation.DateOfBirth, Operation.OperationDate)), 2) + ' Days'
	
	when datediff(day, Operation.DateOfBirth, Operation.OperationDate) > 28 and
	datediff(month, Operation.DateOfBirth, Operation.OperationDate) -
	case  when datepart(day, Operation.DateOfBirth) > datepart(day, Operation.OperationDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, Operation.DateOfBirth, Operation.OperationDate) -
	case  when datepart(day, Operation.DateOfBirth) > datepart(day, Operation.OperationDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, Operation.DateOfBirth, Operation.OperationDate) -
	case  when datepart(day, Operation.DateOfBirth) > datepart(day, Operation.OperationDate) then 1 else 0 end > 1 and
	 datediff(month, Operation.DateOfBirth, Operation.OperationDate) -
	case  when datepart(day, Operation.DateOfBirth) > datepart(day, Operation.OperationDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, Operation.DateOfBirth,Operation.OperationDate) -
	case  when datepart(day, Operation.DateOfBirth) > datepart(day, Operation.OperationDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, Operation.DateOfBirth, Operation.OperationDate) - 
	(
	case 
	when	(datepart(m, Operation.DateOfBirth) > datepart(m, Operation.OperationDate)) 
	or
		(
			datepart(m, Operation.DateOfBirth) = datepart(m, Operation.OperationDate) 
		And	datepart(d, Operation.DateOfBirth) > datepart(d, Operation.OperationDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, Operation.DateOfBirth, Operation.OperationDate) - 
	(
	case 
	when	(datepart(m, Operation.DateOfBirth) > datepart(m, Operation.OperationDate)) 
	or
		(
			datepart(m, Operation.DateOfBirth) = datepart(m, Operation.OperationDate) 
		And	datepart(d, Operation.DateOfBirth) > datepart(d, Operation.OperationDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,PatientBooking.InTheatreTime

	,ProcedureStartTime =
		(
		select
			min(ProcedureStartTime)
		from
			Warehouse.Theatre.ProcedureDetail ProcedureDetail
		where
			ProcedureDetail.OperationDetailSourceUniqueID = Operation.SourceUniqueID
		)

	,ProcedureEndTime =
		(
		select
			max(ProcedureEndTime)
		from
			Warehouse.Theatre.ProcedureDetail ProcedureDetail
		where
			ProcedureDetail.OperationDetailSourceUniqueID = Operation.SourceUniqueID
		)

	,NextOperationAnaestheticInductionTime =
		(
		select
			AnaestheticInductionTime
		from
			Warehouse.Theatre.OperationDetail NextOperation
		where
			Operation.SessionSourceUniqueID = NextOperation.SessionSourceUniqueID
		and	Operation.AnaestheticInductionTime < NextOperation.AnaestheticInductionTime
		and not exists
			(
			select
				1
			from
				Warehouse.Theatre.OperationDetail LaterOperation
			where
				Operation.SessionSourceUniqueID = LaterOperation.SessionSourceUniqueID
			and	Operation.AnaestheticInductionTime < LaterOperation.AnaestheticInductionTime
			and	(
					LaterOperation.AnaestheticInductionTime < NextOperation.AnaestheticInductionTime
				or	(
						LaterOperation.AnaestheticInductionTime = NextOperation.AnaestheticInductionTime
					and	LaterOperation.SourceUniqueID < NextOperation.SourceUniqueID
					)
				)
			)
		)

	,OperationStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, Operation.AnaestheticInductionTime), 0)
				,Operation.AnaestheticInductionTime
			)
			,-1
		)

	,OperationEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, Operation.OperationEndDate), 0)
				,Operation.OperationEndDate
			)
			,-1
		)

from
	Warehouse.Theatre.OperationDetail Operation

left join Warehouse.Theatre.Cancellation Cancellation
on	Cancellation.OperationDetailSourceUniqueID = Operation.SourceUniqueID
and	not exists
	(
	select
		1
	from
		Warehouse.Theatre.Cancellation Previous
	where
		Previous.OperationDetailSourceUniqueID = Operation.SourceUniqueID
	and	(
			Previous.CancellationDate > Cancellation.CancellationDate
		or	(
				Previous.CancellationDate = Cancellation.CancellationDate
			and	Previous.SourceUniqueID > Cancellation.SourceUniqueID
			)
		)
	)

left join Warehouse.Theatre.PatientBooking PatientBooking
on	PatientBooking.SourceUniqueID = Operation.PatientBookingSourceUniqueID

left join Warehouse.APC.Encounter Encounter
on	Encounter.TheatrePatientBookingKey = PatientBooking.SourceUniqueID

left join Warehouse.Theatre.ProcedureDetail
on	Operation.SourceUniqueID = ProcedureDetail.OperationDetailSourceUniqueID
and	ProcedureDetail.PrimaryProcedureFlag = 1
and	not exists
		(
		select
			1
		from
			Warehouse.Theatre.ProcedureDetail EarlierProcedure
		where
			EarlierProcedure.OperationDetailSourceUniqueID = ProcedureDetail.OperationDetailSourceUniqueID
		and	EarlierProcedure.PrimaryProcedureFlag = 1
		and	EarlierProcedure.SourceUniqueID < ProcedureDetail.SourceUniqueID -- some ops have more than one Primary Procedure!
		)

left join Warehouse.Theatre.Operation OperationBase
on	OperationBase.OperationCode = convert(varchar , ProcedureDetail.ProcedureCode)

option  (maxdop 4)


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseTheatreOperation', @Stats, @StartTime


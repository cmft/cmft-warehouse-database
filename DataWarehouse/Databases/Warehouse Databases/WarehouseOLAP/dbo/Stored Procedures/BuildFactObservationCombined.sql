﻿

CREATE proc [dbo].[BuildFactObservationCombined]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationCombined

insert into	dbo.FactObservationCombined

(
	AlertRecno
	,ObservationSetRecno
	,MetricCode
	,SpecialtyID
	,WardID
	,EventDate
	,EventTimeOfDayCode
    ,GenericDimensionCode1
    ,GenericDimensionCode2
	,OutOfHours
	,Cases
)

select
	 -1
	 ,-1
	,MetricCode = 'ADMSN'
	,AdmissionSpecialtyID
	,AdmissionWardID
	,AdmissionDate
	,AdmissionTimeOfDayCode
	,-1
	,-1
	,AdmissionOutOfHours
	,Cases
from
	dbo.BaseObservationAdmission

insert into	dbo.FactObservationCombined

(
	AlertRecno
	,ObservationSetRecno
	,MetricCode
	,SpecialtyID
	,WardID
	,EventDate
	,EventTimeOfDayCode
    ,GenericDimensionCode1
    ,GenericDimensionCode2
	,OutOfHours
	,Cases
)

select
	 AlertRecno
	 ,-1
	,MetricCode = 'ALERT'
	,SpecialtyID
	,WardID
	,CreatedDate
	,CreatedTimeOfDayCode
	,'ALSEV||' + cast(OlapObservationAlertSeverity.AlertSeverityCode as varchar)
	,'ALDUR||' + cast(
					coalesce(
						case 
						when AlertDurationMinutes < 0 then -1
						when AlertDurationMinutes > 1440 then 1440
						else AlertDurationMinutes
						end
						,-2
					)
				as varchar)
	,OutOfHours
	,Cases
from
	dbo.BaseObservationAlert

inner join dbo.OlapObservationAlertSeverity
on	OlapObservationAlertSeverity.AlertSeverityID = BaseObservationAlert.CurrentSeverityID


set @RowsInserted = @@rowcount

insert into dbo.FactObservationCombined
(
	AlertRecno
	,ObservationSetRecno
	,MetricCode
	,SpecialtyID
	,WardID
	,EventDate
	,EventTimeOfDayCode
	,GenericDimensionCode1
	,GenericDimensionCode2
	,OutOfHours
	,Cases
)

select --top 10
	-1 
	,ObservationSetRecno
	,MetricCode = 'OBSET'
	,SpecialtyID
	,WardID
	,StartDate
	,StartTimeOfDayCode
	,'ODTST||' + cast(OlapObservationObservationStatus.ObservationStatusCode as varchar)
	,'ASDST||' + cast(OverallAssessedStatusCode as varchar)
	,OutOfHours
	,Cases
from
	dbo.BaseObservationObservationSet

inner join dbo.OlapObservationObservationStatus
on	OlapObservationObservationStatus.ObservationStatusID = BaseObservationObservationSet.DueTimeStatusID

inner join dbo.OlapObservationOverallAssessedStatus
on	OlapObservationOverallAssessedStatus.OverallAssessedStatusID = BaseObservationObservationSet.OverallAssessedStatusID

where
	ObservationNotTakenReasonID = -1 --only include observation sets which were carried out


set @RowsInserted = @@rowcount + @RowsInserted

insert into dbo.FactObservationCombined
(
	AlertRecno
	,ObservationSetRecno
	,MetricCode
	,SpecialtyID
	,WardID
	,EventDate
	,EventTimeOfDayCode
	,GenericDimensionCode1
	,GenericDimensionCode2
	,OutOfHours
	,Cases
)

select
	-1
	,ObservationSetRecno
	,MetricCode = 'CHAIN' 
	,SpecialtyID
	,WardID
	,StartDate
	,StartTimeOfDayCode
	,'ASDST||' + cast(OverallAssessedStatusCode as varchar)
	,'CHDUR||' + cast(
					coalesce(
						case 
						when AlertChainDurationMinutes < 0 then -1
						when AlertChainDurationMinutes > 1440 then 1440
						else AlertChainDurationMinutes
						end
						,-2
					)
				as varchar) 
	,OutOfHours
	,Cases
from
	dbo.BaseObservationObservationSet

left join dbo.OlapObservationOverallAssessedStatus
on	OlapObservationOverallAssessedStatus.OverallAssessedStatusID = BaseObservationObservationSet.OverallAssessedStatusID

where
	IsLastInAlertChain = 1

set @RowsInserted = @@rowcount + @RowsInserted

insert into dbo.FactObservationCombined

(
	AlertRecno
	,ObservationSetRecno
	,MetricCode
	,SpecialtyID
	,WardID
	,EventDate
	,EventTimeOfDayCode
	,GenericDimensionCode1
	,GenericDimensionCode2
	,OutOfHours
	,Cases
)

select
	-1
	,-1
	,MetricCode = 'RESUS' 
	,-1
	,OlapObservationWard.WardID
	,EventDate = 
			case
			when EventDate > getdate()
			then '1 jan 1900'
			else EventDate
			end
	,-1
	,CallOutTypeCode = 'RESUS||' + cast(CallOutTypeID as varchar)
	,-1
	,OutOfHours = 0
	,Cases = 1
from
	dbo.BaseResus

inner join dbo.OlapObservationWard OlapObservationWard
on	BaseResus.LocationCode = WardCode

set @RowsInserted = @@rowcount + @RowsInserted

insert into	dbo.FactObservationCombined

(
	AlertRecno
	,ObservationSetRecno
	,MetricCode
	,SpecialtyID
	,WardID
	,EventDate
	,EventTimeOfDayCode
	,GenericDimensionCode1
	,GenericDimensionCode2
	,OutOfHours
	,Cases
)

select
	-1
	,-1
	,MetricCode = 'PAI' 
	,-1
	,OlapObservationWard.WardID
	,EventDate = 
			case
			when EventDate > getdate()
			then '1 jan 1900'
			else EventDate
			end
	,-1
	,CallOutTypeCode = 'RESUS||' + cast(CallOutTypeID as varchar)
	,-1
	,OutOfHours = 0
	,Cases = 1
from
	dbo.BaseResus

inner join 	dbo.OlapObservationWard OlapObservationWard
on	BaseResus.LocationCode = WardCode

where
	ebmPAI = 1


select @RowsInserted = @@rowcount + @RowsInserted

select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	








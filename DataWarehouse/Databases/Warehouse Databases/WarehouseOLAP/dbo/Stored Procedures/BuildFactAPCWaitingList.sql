﻿
CREATE procedure [dbo].[BuildFactAPCWaitingList] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactAPCWaitingList

insert into dbo.FactAPCWaitingList
(
	 EncounterRecno
	,Cases
	,LengthOfWait
	,AgeCode
	,CensusDate
	,ConsultantCode
	,SpecialtyCode
	,DurationCode
	,PracticeCode
	,SiteCode
	,WaitTypeCode
	,StatusCode
	,CategoryCode
	,AddedLastWeek
	,WaitingListAddMinutes
	,OPCSCoded
	,WithRTTStartDate
	,WithRTTStatusCode
	,WithExpectedAdmissionDate
	,RTTActivity
	,RTTBreachStatusCode
	,DiagnosticProcedure
	,TCIDate
	,SexCode
	,DirectorateCode
	,DurationAtTCIDateCode
	,WithTCIDate
	,WithGuaranteedAdmissionDate
	,WithRTTOpenPathway
)
SELECT
	 Encounter.EncounterRecno
	,Encounter.Cases
	,Encounter.LengthOfWait
	,Encounter.AgeCode
	,Encounter.CensusDate
	,Encounter.ConsultantCode

	,SpecialtyCode = 
		case
		when InterfaceCode = 'INQ' then Encounter.PASSpecialtyCode
		else Encounter.EpisodeSpecialtyCode
		end

	,Encounter.DurationCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SiteCode
	,Encounter.WaitTypeCode
	,Encounter.StatusCode
	,Encounter.CategoryCode
	,Encounter.AddedLastWeek
	,Encounter.WaitingListAddMinutes
	,Encounter.OPCSCoded
	,Encounter.WithRTTStartDate
	,Encounter.WithRTTStatusCode
	,Encounter.WithExpectedAdmissionDate
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.TCIDate
	,Encounter.SexCode
	,Encounter.DirectorateCode
	,Encounter.DurationAtTCIDateCode
	,Encounter.WithTCIDate
	,Encounter.WithGuaranteedAdmissionDate
	,Encounter.WithRTTOpenPathway
FROM
	dbo.OlapAPCWaitingList Encounter


exec BuildOlapPractice 'APCWL'

select
	 @from = min(EncounterFact.TCIDate)
	,@to = max(EncounterFact.TCIDate)
from
	dbo.FactAPCWaitingList EncounterFact
where
	EncounterFact.TCIDate <> '1 Jan 1900'

exec BuildCalendarBasePair @from, @to


select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildFactAPCWaitingList', @Stats, @StartTime


﻿
CREATE procedure [dbo].[BuildFactOPWaitingList] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table dbo.FactOPWaitingList

insert into dbo.FactOPWaitingList
(
	 EncounterRecno
	,Cases
	,LengthOfWait
	,AgeCode
	,CensusDate
	,ConsultantCode
	,SpecialtyCode
	,DurationCode
	,PracticeCode
	,SiteCode
	,SourceOfReferralCode
	,WithAppointment
	,BookedBeyondBreach
	,NearBreach
	,RTTActivity
	,RTTBreachStatusCode
	,AppointmentDate
	,SexCode
	,DirectorateCode
	,DurationAtAppointmentDateCode
	,WithRTTOpenPathway
)
SELECT
	 Encounter.EncounterRecno
	,Encounter.Cases
	,Encounter.LengthOfWait
	,Encounter.AgeCode
	,Encounter.CensusDate
	,Encounter.ConsultantCode
	,Encounter.PASSpecialtyCode
	,Encounter.DurationCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.WithAppointment
	,Encounter.BookedBeyondBreach
	,Encounter.NearBreach
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.AppointmentDate
	,Encounter.SexCode
	,Encounter.DirectorateCode
	,DurationAtAppointmentDateCode
	,Encounter.WithRTTOpenPathway
FROM
	dbo.BaseOPWaitingList Encounter
where
	coalesce(Encounter.AppointmentTypeCode , '') != '5'
and	Encounter.PASSpecialtyCode not in ( 'PSCH' , 'OPFI' , 'OPOD' , 'OPRE' , 'CADI' , 'RDCS' , 'RPES' , 'COPD' )


exec BuildOlapPractice 'OPWL'


select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildFactOPWaitingList', @Stats, @StartTime


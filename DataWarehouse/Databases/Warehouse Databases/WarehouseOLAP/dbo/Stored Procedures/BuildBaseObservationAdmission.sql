﻿


CREATE proc [dbo].[BuildBaseObservationAdmission] 

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

set dateformat dmy

truncate table dbo.BaseObservationAdmission

insert into dbo.BaseObservationAdmission

(
AdmissionRecno
,SourceUniqueID
,CasenoteNumber
,DateOfBirth
,SexID
,Surname
,Forename
,AdmissionDate
,AdmissionTime
,AdmissionTimeOfDayCode
,AdmissionSpecialtyID
,AdmissionWardID
,DischargeDate
,DischargeTime
,DischargeTimeOfDayCode
,DischargeSpecialtyID
,DischargeWardID
,TreatmentOutcomeID
,DischargeDestinationID
,Comment
,AdmissionOutOfHours
,DischargeOutOfHours
,AdmissionAgeID
,DischargeAgeID
,ContextCode
,Cases
,LengthOfStay
)

select --top 10 
	AdmissionRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SexID = 
			coalesce(
				SexID
				,-1
			)
	,Surname
	,Forename
	,AdmissionDate = cast(AdmissionTime as date)
	,AdmissionTime
	,AdmissionTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, AdmissionTime), 0)
							,AdmissionTime
						)
						,-1
					)
	,AdmissionSpecialtyID = 
			coalesce(
				AdmissionSpecialtyID
				,-1
			)
	,AdmissionWardID = 
			coalesce(
				AdmissionWardID
				,-1
			)
	,DischargeDate = cast(DischargeTime as date)
	,DischargeTime
	,DischargeTimeOfDayCode =
					coalesce(
						datediff(
								minute
							,dateadd(day, datediff(day, 0, DischargeTime), 0)
							,DischargeTime
						)
						,-1
					)
	,DischargeSpecialtyID = 
			coalesce(
				DischargeSpecialtyID
				,-1
			)
	,DischargeWardID = 
			coalesce(
				DischargeWardID
				,-1
			)
	,TreatmentOutcomeID = 
			coalesce(
				TreatmentOutcomeID
				,-1
			)
	,DischargeDestinationID = 
			coalesce(
				DischargeDestinationID
				,-1
			)
	,Comment
	,AdmissionOutOfHours = 
				case
				when datename(dw, AdmissionTime) in ('Saturday', 'Sunday') then 1
				when cast(AdmissionTime as time) < '8:00' then 1
				when cast(AdmissionTime as time) > '17:00' then 1
				when exists
					(
					select
						1
					from
						Warehouse.WH.Holiday
					where
						cast(AdmissionTime as date) = Holiday.HolidayDate
					)
				then 1
				else 0
				end
	,DischargeOutOfHours = 
				case
				when datename(dw, DischargeTime) in ('Saturday', 'Sunday') then 1
				when cast(DischargeTime as time) < '8:00' then 1
				when cast(DischargeTime as time) > '17:00' then 1
				when exists
					(
					select
						1
					from
						Warehouse.WH.Holiday
					where
						cast(DischargeTime as date) = Holiday.HolidayDate
					)
				then 1
				else 0
				end
	,AdmissionAgeID = 
			-- Ripped from the Dates.GetAge function in CMFT for speed
			case
			when DateOfBirth > AdmissionTime
			then 0
			when (month(AdmissionTime) * 100) + day(AdmissionTime) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
			then datediff(year, DateOfBirth, AdmissionTime)
			else datediff(year, DateOfBirth, AdmissionTime) - 1
			end
	,DischargeAgeID = 
			case
			when DateOfBirth > DischargeTime
			then 0
			when (month(DischargeTime) * 100) + day(DischargeTime) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
			then datediff(year, DateOfBirth, DischargeTime)
			else datediff(year, DateOfBirth, DischargeTime) - 1
			end
	,ContextCode	
	,Cases = 1
	,LengthOfStay = datediff(hour, AdmissionTime, DischargeTime) / cast(24 as float)
from
	Warehouse.Observation.Admission


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	















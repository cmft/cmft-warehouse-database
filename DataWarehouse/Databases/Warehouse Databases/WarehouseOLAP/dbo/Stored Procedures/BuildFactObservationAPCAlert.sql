﻿


CREATE proc [dbo].[BuildFactObservationAPCAlert]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationAPCAlert

insert into dbo.FactObservationAPCAlert
(
	AlertRecno
	,SpecialtyID
	,WardID
	,TypeID
	,ReasonID
	,CreatedDate
	,CreatedTimeOfDayCode
	,BedsideDueDate
	,BedsideDueTimeOfDayCode
	,EscalationDate
	,EscalationTimeOfDayCode
	,AcceptedDate
	,AcceptedTimeOfDayCode
	,AttendedByUserID
	,AttendanceTypeID
	,CurrentSeverityID
	,CurrentClinicianSeniorityID
	,ClosedDate
	,ClosedTimeOfDayCode
	,ClosedByUserID
	,ClosureReasonID
	,DurationID
	,OutOfHours
	,AgeID
	,ConsultantCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,AdmissionMethodCode
	,DischargeMethodCode
	,EthnicOriginCode
	,SexCode
	,ContextCode
	,Cases
	,Duration
)


select
	 BaseObservationAlert.AlertRecno
	,SpecialtyID
	,WardID
	,TypeID
	,ReasonID
	,CreatedDate
	,CreatedTimeOfDayCode
	,BedsideDueDate
	,BedsideDueTimeOfDayCode
	,EscalationDate
	,EscalationTimeOfDayCode
	,AcceptedDate
	,AcceptedTimeOfDayCode
	,AttendedByUserID
	,AttendanceTypeID
	,CurrentSeverityID
	,CurrentClinicianSeniorityID
	,ClosedDate
	,ClosedTimeOfDayCode
	,ClosedByUserID
	,ClosureReasonID
	,DurationID =
				coalesce(
					case 
						when AlertDurationMinutes < 0 then -1
						when AlertDurationMinutes > 1440 then 1440
						else AlertDurationMinutes
					end
					, -2
					)
	,OutOfHours
	,AgeID
	,ConsultantCode = 
				coalesce(
					Encounter.ConsultantCode
				,'N/A'
				)
	,PrimaryDiagnosisCode = 
				coalesce(
					left(Encounter.PrimaryDiagnosisCode, 5)
				,'##'
				)
	,PrimaryOperationCode = 
				coalesce(
					left(Encounter.PrimaryOperationCode, 5)
				,'##'
				)
	,AdmissionMethodCode = 
				coalesce(
					Encounter.AdmissionMethodCode
				,'NA'
				)
	,DischargeMethodCode = 
				coalesce(
					Encounter.DischargeMethodCode
				,'##'
				)
	,EthnicOriginCode = 
				coalesce(
					Encounter.EthnicOriginCode
				,'X'
				)
	,SexCode =
		case Encounter.SexCode
		when '' then '0'
		when 'M' then '1'
		when 'F' then '2'
		when 'I' then '9'
		else '#'
		end
	,ContextCode
	,Cases
	,AlertDurationMinutes
from
	dbo.BaseObservationAlert

inner join Warehouse.APC.EncounterAlert
on	BaseObservationAlert.AlertRecno = EncounterAlert.AlertRecno

inner join Warehouse.APC.Encounter
on	EncounterAlert.EncounterRecno = Encounter.EncounterRecno


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	

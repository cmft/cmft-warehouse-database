﻿
CREATE proc [dbo].[BuildBaseObservationSignificantEvent] as 

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

set dateformat dmy

truncate table dbo.BaseObservationSignificantEvent

insert into dbo.BaseObservationSignificantEvent

(
SignificantEventRecno
,SourceUniqueID
,CasenoteNumber
,DateOfBirth
,SpecialtyID
,WardID
,AdmissionSourceUniqueID
,SignificantEventTypeID
,StartDate
,StartTimeOfDayCode
,EndDate
,EndTimeOfDayCode
,Comment
,CreatedByUserID
,CreatedTime
,LastModifiedByUserID
,LastModifiedTime
,OutOfHours
,AgeID
,ContextCode
,Cases
)

select
	SignificantEventRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID = 
			coalesce(
				SpecialtyID
				,-1
			)
	,WardID = 
			coalesce(
				WardID
				,-1
			)
	,AdmissionSourceUniqueID
	,SignificantEventTypeID = 
			coalesce(
				SignificantEventTypeID
				,-1
			)

	,StartDate = cast(StartTime as date)
	,StartTimeTimeOfDayCode =
					coalesce(
						datediff(
							 minute
							,dateadd(day, datediff(day, 0, StartTime), 0)
							,StartTime
						)
						,-1
					)
	,EndDate = cast(EndTime as date)
	,EndTimeTimeOfDayCode = 
					coalesce(
						datediff(
							 minute
							,dateadd(day, datediff(day, 0, StartTime), 0)
							,StartTime
						)
						,-1
					)
	,Comment
	,CreatedByUserID = 
			coalesce(
				CreatedByUserID
				,-1
			)
	,CreatedTime
	,LastModifiedByUserID = 
			coalesce(
				LastModifiedByUserID
				,-1
			)
	,LastModifiedTime
	,OutOfHours = 
				case
				when datename(dw, StartTime) in ('Saturday', 'Sunday') then 1
				when cast(StartTime as time) < '8:00' then 1
				when cast(StartTime as time) > '17:00' then 1
				when exists
					(
					select
						1
					from
						Warehouse.WH.Holiday
					where
						cast(StartTime as date) = Holiday.HolidayDate
					)
				then 1
				else 0
				end
	--,AgeID = CMFT.Dates.GetAge(DateOfBirth, CreatedTime)
	,AgeID = 
			-- Ripped from the Dates.GetAge function in CMFT for speed
			case
			when DateOfBirth > StartTime
			then 0
			when (month(StartTime) * 100) + day(StartTime) >= (month(DateOfBirth) * 100) + day(DateOfBirth)
			then datediff(year, DateOfBirth, StartTime)
			else datediff(year, DateOfBirth, StartTime) - 1
			end
	,ContextCode
	,Cases = 1
from
	Warehouse.Observation.SignificantEvent


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	

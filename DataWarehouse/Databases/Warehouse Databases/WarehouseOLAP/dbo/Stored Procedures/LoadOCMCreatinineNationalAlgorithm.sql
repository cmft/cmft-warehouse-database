﻿-- All results within period specified

-- select * from [dbo].[TLoadOCMCreatinineNationalAlgorithm] -- 598462

CREATE Procedure [dbo].[LoadOCMCreatinineNationalAlgorithm] As
-- took 40 secs returned 2,129,046 rows as expected
-- 1,146,402 within 7 days; 592,981 8 to 365 days; 389,663 default
-- Previous Result 8-365 days find median of all results in the 8-365 day period (advised by Leonard Ebah 25/09/2014)

Truncate table OCMTest.CreatinineNationalAlgorithm;

With ResultsCTE
	(ResultRecNo
	,SourcePatientNo
	,MergeEpisodeNo
	,ResultTime
	,Result
	,PreviousResult
	,PreviousResultTime
	,OrderNo
	,DateOfBirth
	,DistrictNo
	,Surname
	,Forenames
	,Sexcode
	,EthnicOriginCode
	,DeathIndicator
	,DateOfDeath
	)
As
	(select
		Result.ResultRecNo
		,Result.SourcePatientNo
		,Result.MergeEpisodeNo
		,Result.ResultTime
		,C1Result = Result.Result
		,PreviousResult = PreviousResult.Result	
		,PreviousResultTime = PreviousResult.ResultTime
		,OrderNo = row_number() over (partition by Result.ResultRecNo order by PreviousResult.Result)
		,Result.DateOfBirth
		,Result.DistrictNo
		,Result.Surname
		,Result.Forenames
		,Result.Sexcode
		,Result.EthnicOriginCode
		,Result.DeathIndicator
		,Result.DateOfDeath
	from
		OCMTest.Result Result

	inner join OCMTest.Result PreviousResult
	on Result.SourcePatientNo = PreviousResult.SourcePatientNo
	where 
		Result.ResultRecNo not in 
				(select
					Latest.ResultRecNo
				from 
					OCMTest.Result Latest
				inner join OCMTest.Result Previous
				on Latest.SourcePatientNo = Previous.SourcePatientNo
				and Latest.ResultRecNo <> Previous.ResultRecNo
				and datediff(day,Previous.ResultTime, Latest.ResultTime) between 0 and 7
				)
	and datediff(day,PreviousResult.ResultTime,Result.ResultTime) between 8 and 365
	)


Insert into OCMTest.CreatinineNationalAlgorithm
	(ResultRecNo
	,ResultTime
	,SourcePatientNo
	,MergeEpisodeNo
	,Age
	,AgeCode 
	,C1Result
	,RatioDescription
	,ResultForRatio
	,RVRatio
	,Alert
	,DateOfBirth
	,DistrictNo
	,Surname
	,Forenames
	,Sexcode
	,EthnicOriginCode
	,DeathIndicator
	,DateOfDeath
	)
Select
	Results.ResultRecNo
	,ResultTime
	,Results.SourcePatientNo
	,Results.MergeEpisodeNo
	,Age = cast((datediff(day,DateOfBirth, Results.ResultTime)/365.25) as int)
	,AgeCode = dbo.f_AgeCode(DateOfBirth, Results.ResultTime)
	,C1Result
	,RatioDescription
	,ResultForRatio = RV2MedianResult
	,RVRatio
	,Alert
	,Results.DateOfBirth
	,Results.DistrictNo
	,Results.Surname
	,Results.Forenames
	,Results.Sexcode
	,Results.EthnicOriginCode
	,Results.DeathIndicator
	,Results.DateOfDeath

from
	(
	Select
		CTE.ResultRecNo
		,CTE.ResultTime
		,SourcePatientNo
		,MergeEpisodeNo
		,DateOfBirth
		,DistrictNo
		,Surname
		,Forenames
		,Sexcode
		,EthnicOriginCode
		,DeathIndicator
		,DateOfDeath
		,C1Result = Result
		,RatioDescription = 'Median8-365days'
		,RV2MedianResult = PreviousResult
		,RVRatio = cast(Result as decimal (12,6))
					/
					cast(PreviousResult as decimal (12,6))
	from 
		ResultsCTE CTE
	inner join
		(select 
			ResultRecNo
			,MedianValue = cast(round(((max(OrderNo)+0.5)/2),0) as int)
		from 
			ResultsCTE
		group by 
			ResultRecNo
		)Median
	on CTE.ResultRecNo = Median.ResultRecNo
	and CTE.OrderNo = Median.MedianValue
	
	
	union

	-- Previous Result 0-7 days find lowest of all results within the 7 day period (advised by Leonard Ebah 25/09/2014)
	
	select
		Result.ResultRecNo
		,Result.ResultTime
		,Result.SourcePatientNo
		,Result.MergeEpisodeNo
		,Result.DateOfBirth
		,Result.DistrictNo
		,Result.Surname
		,Result.Forenames
		,Result.Sexcode
		,Result.EthnicOriginCode
		,Result.DeathIndicator
		,Result.DateOfDeath
		,C1Result = Result.Result
		,RatioDescription = 'Lowest0-7days'
		,RV1LowestValue = case
							when Result.Result < min(PreviousResult.Result)
							then Result.Result
							else min(PreviousResult.Result)
						end
		,RVRatio = cast(Result.Result as decimal (12,6))
					/
					cast(
						case
							when Result.Result < min(PreviousResult.Result)
							then Result.Result
							else min(PreviousResult.Result)
						end
					as decimal(12,6))
	from
		OCMTest.Result Result

	inner join OCMTest.Result PreviousResult
	on Result.SourcePatientNo = PreviousResult.SourcePatientNo
	and PreviousResult.ResultTime<Result.ResultTime
	and datediff(day,PreviousResult.ResultTime,Result.ResultTime) between 0 and 7
	and Result.ResultRecNo <> PreviousResult.ResultRecNo
	
	group by
		Result.ResultRecNo
		,Result.SourcePatientNo
		,Result.MergeEpisodeNo
		,Result.ResultTime
		,Result.Result
		,Result.DateOfBirth
		,Result.DistrictNo
		,Result.Surname
		,Result.Forenames
		,Result.Sexcode
		,Result.EthnicOriginCode
		,Result.DeathIndicator
		,Result.DateOfDeath
	
	union
	-- If there is no previous Result within the last 365 days use the default value
	
	select 
		Result.ResultRecNo
		,Result.ResultTime
		,Result.SourcePatientNo
		,Result.MergeEpisodeNo
		,Result.DateOfBirth
		,Result.DistrictNo
		,Result.Surname
		,Result.Forenames
		,Result.Sexcode
		,Result.EthnicOriginCode
		,Result.DeathIndicator
		,Result.DateOfDeath
		,C1Result = Result.Result
		,RatioDescription = 'Baseline'
		,BaselineResult = case 
							when Encounter.SexCode = 'M' then 80
							when Encounter.SexCode = 'F' then 60
							else null
						end
		,BaselineRatio = 
					case 
						when Encounter.SexCode is null then null
						when Encounter.SexCode = 'M' then cast(Result.Result as decimal (12,6))/cast(80 as decimal(12,6))
						else cast(Result.Result as decimal (12,6))/cast(60 as decimal(12,6))
					end
	from
		OCMTest.Result Result

	left join OCMTest.APCEncounter Encounter
	on	Encounter.SourcePatientNo = Result.SourcePatientNo
	and not exists
			(select 
				1
			from
				OCMTest.APCEncounter Latest
			where 
				Latest.SourcePatientNo = Encounter.SourcePatientNo
			and Latest.MergeEncounterRecno<Encounter.MergeEncounterRecno
			)
	where 
		Result.ResultRecNo not in 
				(select 
					Latest.ResultRecNo
				from 
					OCMTest.Result Latest
				inner join OCMTest.Result Previous
				on Latest.SourcePatientNo = Previous.SourcePatientNo
				and Latest.ResultRecNo <> Previous.ResultRecNo
				and Previous.ResultTime < Latest.ResultTime
				and datediff(day,Previous.ResultTime, Latest.ResultTime) between 0 and 365
				)
	) Results
	
left join	
	(select
		Result.ResultRecNo
		,Alert = case
					when Result.Result - min(PreviousResult.Result) > 26 then 'AKI 1'
					else Null
				end
					
	from
		OCMTest.Result Result

	inner join OCMTest.Result PreviousResult
	on Result.SourcePatientNo = PreviousResult.SourcePatientNo
	and datediff(mi,PreviousResult.ResultTime,Result.ResultTime) between 0 and 2880
	and Result.ResultRecNo <> PreviousResult.ResultRecNo
	group by
		Result.ResultRecNo
		,Result.Result
	) Result48hrs
	
on Results.ResultRecNo = Result48hrs.ResultRecNo
		

--598462
-- District No


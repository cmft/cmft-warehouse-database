﻿
create proc [dbo].[BuildResus] as 

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()


exec dbo.BuildBaseResus
exec dbo.BuildFactResus
exec dbo.BuildFactAPCResus


select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	
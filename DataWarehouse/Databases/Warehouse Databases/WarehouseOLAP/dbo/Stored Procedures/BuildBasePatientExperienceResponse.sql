﻿





					CREATE proc [dbo].[BuildBasePatientExperienceResponse]

					as

					truncate table [dbo].[BasePatientExperienceResponse]

					INSERT INTO [dbo].[BasePatientExperienceResponse]
							   (
								[ResponseRecno]
							   ,[SurveyTakenID]
							   ,SurveyID
							   ,[QuestionPosition]
							   ,[QuestionID]
							   ,[AnswerID]
							   ,[CensusTime]
							   ,[LocationID]
							   ,[DeviceNumber]
							   ,Cases
							   ,[Created]
							   ,[ByWhom]
							   )

					select
						[ResponseRecno]
						,[SurveyTakenID]
						,SurveyID
						,[QuestionPosition]
						,[QuestionID]
						,[AnswerID]
						,[CensusTime]
						,[LocationID]
						,[DeviceNumber]
						,Cases = 1
						,[Created] = getdate()
						,[ByWhom] = system_user
					from
						[Warehouse].PEX.Response





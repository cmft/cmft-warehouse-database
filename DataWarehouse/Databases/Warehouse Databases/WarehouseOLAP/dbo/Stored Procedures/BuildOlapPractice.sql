﻿
CREATE      procedure [dbo].[BuildOlapPractice]
	@dataset varchar(50) = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

----------------------------------------------------
-- Practice Reference Data Build
----------------------------------------------------

create table #tPractice 
	(
	PracticeCode varchar(10) not null
	)

create clustered index #tPracticeIX on #tPractice
	(
	PracticeCode
	)

-- prepare distinct list

if @dataset = 'AE' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.RegisteredPracticeCode, 'N/A')
	from
		dbo.FactAE Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.RegisteredPracticeCode, 'N/A')
		)


--if @dataset = 'ALS' or @dataset is null

--	insert into #tPractice
--	select distinct
--		 coalesce(Encounter.RegisteredPracticeCode, 'N/A')
--	from
--		dbo.FactALS Encounter
--	where
--		not exists
--		(
--		select
--			1
--		from
--			#tPractice
--		where
--			#tPractice.PracticeCode = coalesce(Encounter.RegisteredPracticeCode, 'N/A')
--		)

if @dataset = 'APC' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, 'N/A')
	from
		dbo.FactAPC Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, 'N/A')
		)

if @dataset = 'OP' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, 'N/A')
	from
		dbo.FactOP Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, 'N/A')
		)

if @dataset = 'RF' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, 'N/A')
	from
		dbo.FactRF Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, 'N/A')
		)


if @dataset = 'APCWL' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, 'N/A')
	from
		dbo.FactAPCWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, 'N/A')
		)

if @dataset = 'OPWL' or @dataset is null

	insert into #tPractice
	select distinct
		 coalesce(Encounter.PracticeCode, 'N/A')
	from
		dbo.FactOPWaitingList Encounter
	where
		not exists
		(
		select
			1
		from
			#tPractice
		where
			#tPractice.PracticeCode = coalesce(Encounter.PracticeCode, 'N/A')
		)


--delete all entries if this is ALL datasets
if @dataset is null

	delete from dbo.EntityXref
	where
		EntityTypeCode = 'OLAPPRACTICE'
	and	XrefEntityTypeCode = 'OLAPPRACTICE'
	and	not exists
		(
		select
			1
		from
			#tPractice
		where
			EntityCode = PracticeCode
		)


insert into dbo.EntityXref
(
	 EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
)
select distinct
	 EntityTypeCode = 'OLAPPRACTICE'
	,PracticeCode
	,XrefEntityTypeCode = 'OLAPPRACTICE'
	,PracticeCode
from
	#tPractice
where
	not exists
	(
	select
		1
	from
		dbo.EntityXref
	where
		EntityTypeCode = 'OLAPPRACTICE'
	and	XrefEntityTypeCode = 'OLAPPRACTICE'
	and	EntityCode = PracticeCode
	and	XrefEntityCode = PracticeCode
	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins, ' +
	'Dataset ' + coalesce(@dataset, 'All')

exec WriteAuditLogEvent 'BuildOlapPractice', @Stats, @StartTime


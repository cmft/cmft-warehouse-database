﻿
CREATE procedure [dbo].[BuildBaseTheatreSession] as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseTheatreSession

insert into dbo.BaseTheatreSession
(
	 SourceUniqueID
	,ActualSessionMinutes
	,PlannedSessionMinutes
	,StartTime
	,EndTime
	,SessionNumber
	,TheatreCode
	,ConsultantCode
	,SpecialtyCode
	,SurgeonCode1
	,SurgeonCode2
	,AnaesthetistCode1
	,AnaesthetistCode2
	,AnaesthetistCode3
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaestheticNurseCode
	,LastUpdated
	,TimetableDetailCode
	,Comment
	,SessionOrder
	,CancelledFlag
	,CancelledMinutes
	,OverrunReason
	,OverrunReasonDate
	,OverrunReasonStaffCode
	,TimetableConsultantCode
	,TemplateConsultantCode
	,TimetableAnaesthetistCode
	,TemplateAnaesthetistCode
	,TimetableSpecialtyCode
	,TemplateSpecialtyCode
	,ActualSessionStartTime
	,ActualSessionEndTime
	,SessionStartTimeOfDay
	,SessionEndTimeOfDay
)
select
	 Session.SourceUniqueID
	,Session.ActualSessionMinutes
	,Session.PlannedSessionMinutes
	,Session.StartTime
	,Session.EndTime
	,Session.SessionNumber
	,Session.TheatreCode
	,Session.ConsultantCode
	,Session.SpecialtyCode
	,Session.SurgeonCode1
	,Session.SurgeonCode2
	,Session.AnaesthetistCode1
	,Session.AnaesthetistCode2
	,Session.AnaesthetistCode3
	,Session.ScoutNurseCode
	,Session.InstrumentNurseCode
	,Session.AnaestheticNurseCode
	,Session.LastUpdated
	,Session.TimetableDetailCode
	,Session.Comment
	,Session.SessionOrder
	,Session.CancelledFlag
	,Session.CancelledMinutes
	,Session.OverrunReason
	,Session.OverrunReasonDate
	,Session.OverrunReasonStaffCode

	,TimetableConsultantCode = TimetableConsultant.StaffCode

	,TemplateConsultantCode = TimetableTemplateConsultant.StaffCode

	,TimetableAnaesthetistCode = TimetableAnaesthetist.StaffCode

	,TemplateAnaesthetistCode = TimetableTemplateAnaesthetist.StaffCode

	,TimetableSpecialtyCode = TimetableSpecialty.SpecialtyCode

	,TemplateSpecialtyCode = TimetableTemplateSpecialty.SpecialtyCode

	,ActualSessionStartTime = ActualSession.StartTime
	,ActualSessionEndTime = ActualSession.EndTime

	,SessionStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, Session.StartTime), 0)
				,Session.StartTime
			)
			,-1
		)

	,SessionEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, Session.EndTime), 0)
				,Session.EndTime
			)
			,-1
		)

from
	Warehouse.Theatre.Session Session

left join Warehouse.Theatre.TimetableDetail TimetableDetail
on	TimetableDetail.TimetableDetailCode = Session.TimetableDetailCode
and	TimetableDetail.SessionNumber = Session.SessionNumber
and	TimetableDetail.DayNumber = datepart(dw, Session.StartTime)

left join Warehouse.Theatre.TimetableTemplate TimetableTemplate
on	TimetableTemplate.TimetableTemplateCode = TimetableDetail.TimetableTemplateCode
and	TimetableTemplate.SessionNumber = Session.SessionNumber
and	TimetableTemplate.DayNumber = datepart(dw, Session.StartTime)

left join Warehouse.Theatre.Staff TimetableConsultant
on	TimetableConsultant.StaffCode1 = TimetableDetail.ConsultantCode

left join Warehouse.Theatre.Staff TimetableTemplateConsultant
on	TimetableTemplateConsultant.StaffCode1 = TimetableTemplate.ConsultantCode

left join Warehouse.Theatre.Staff TimetableAnaesthetist
on	TimetableAnaesthetist.StaffCode1 = TimetableDetail.AnaesthetistCode

left join Warehouse.Theatre.Staff TimetableTemplateAnaesthetist
on	TimetableTemplateAnaesthetist.StaffCode1 = TimetableTemplate.AnaesthetistCode

left join Warehouse.Theatre.Specialty TimetableSpecialty
on	TimetableSpecialty.SpecialtyCode1 = TimetableDetail.SpecialtyCode

left join Warehouse.Theatre.Specialty TimetableTemplateSpecialty
on	TimetableTemplateSpecialty.SpecialtyCode1 = TimetableTemplate.SpecialtyCode

left join 
	(
	select
		 SessionSourceUniqueID
		,StartTime = min(AnaestheticInductionTime)
		,EndTime = max(OperationEndDate)
	from
		Warehouse.Theatre.OperationDetail
	group by
		SessionSourceUniqueID
	) ActualSession
on	ActualSession.SessionSourceUniqueID = Session.SourceUniqueID


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseTheatreSession', @Stats, @StartTime


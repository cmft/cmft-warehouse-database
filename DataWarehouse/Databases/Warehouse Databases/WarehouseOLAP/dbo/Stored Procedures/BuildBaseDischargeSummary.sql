﻿
create proc dbo.BuildBaseDischargeSummary 

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.BaseDischargeSummary

insert into dbo.BaseDischargeSummary

select
	EncounterRecno
	,SourcePatientNo
	,ProviderSpellNo
	,CaseNoteNumber
	,EpisodicGpPracticeCode
	,EpisodicGpPracticeIsMedisecUser
	,PCTCode
	,AdmissionDate
	,AdmissionTime
	,DischargeDate
	,DischargeTime 
	,EpisodeStartDate 
	,EpisodeEndDate
	,PatientCategoryCode
	,EndDirectorateCode
	,SiteCode
	,ManagementIntentionCode
	,SpecialtyCode
	,NationalSpecialtyCode
	,ConsultantCode
	,NeonatalLevelOfCare
	,PrimaryOperationCode
	,PrimaryDiagnosisCode
	,WardCode
	,DocumentTime
	,StatusCode
	,Comment
	,DocumentName
	,[Description]
	,DocumentTypeCode
	,AuthorCode
	,IssuedTime
	,IssuedByCode
	,SignedTime
	,SignedByCode
	,DocumentProgressCode
	,RequestOrderForDischarge
	,IsFirstDocumentForDischarge
	,DocumentSentGPForDischarge
	,TemplateCode
	,AdmissionReason
	,Diagnosis
	,Investigations
	,RecommendationsOnFutureManagement
	,[InformationToPatientOrRelatives]
	,ChangesToMedication
	,Cases
	,DischargeDivisionCode = coalesce(DivisionCode, 'N/A')
from 
	WarehouseOLAP.dbo.BaseMedisec
left outer join 
	Warehouse.WH.Directorate directorate
	on DirectorateCode = EndDirectorateCode
	
	
--insert into dbo.BaseDischargeSummary	
	
--select top 1
--	EncounterRecno
--	,SourcePatientNo
--	,SourceSpellNo 
--	,CaseNoteNumber
--	,RegisteredGpPracticeCode 
--	,GpPracticeIsMedisecUser
--	,AdmissionDate
--	,AdmissionTime
--	,DischargeDate 
--	,DischargeTime 
--	,EpisodeStartDate 
--	,EpisodeEndDate 
--	,PatientCategoryCode = null
--	,EndDirectorateCode 
--	,EndSiteCode
--	,ManagementIntentionCode = null
--	,SpecialtyCode = SpecialtyCode
--	,NationalSpecialtyCode = null
--	,ConsultantCode = ConsultantCode
--	,NeonatalLevelOfCare = null
--	,PrimaryOperationCode = null
--	,PrimaryDiagnosisCode = null
--	,EndWardTypeCode = null
--	,DocumentTime
--	,StatusCode = case
--					when DocumentName is not null
--					then '02'
--					else '00'
--				end
--	,Comment = null
--	,DocumentName
--	,[Description]
--	,DocumentTypeCode = case
--							when DocumentName is not null
--							then 'I'
--							else null
--						end
--	,AuthorCode 
--	,IssuedTime
--	,IssuedByCode
--	,SignedTime 
--	,SignedByCode
--	,DocumentProgressCode  = case
--								when DocumentName is not null
--								then 'G'
--								else null
--							end
--	,RequestOrderForDischarge = row_number() over (partition by SourcePatientNo, AdmissionTime order by SignedTime) 
--	,IsFirstDocumentForDischarge = case
--										when DocumentName is not null
--										then 1
--										else 0
--									end
--	,DocumentSentGPForDischarge = case
--										when DocumentName is not null
--										then 1
--										else 0
--									end 
--	,[TemplateCode]	 = null					
--	,[AdmissionReason] = null
--	,[Diagnosis] = null
--	,[Investigations] = null
--	,[RecommendationsOnFutureManagement] = null
--	,InformationToPatientOrRelatives = null
--	,[ChangesToMedication] = null
--	,[Cases] = 1
--from
--	Warehouse.SMMIS.Encounter


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime


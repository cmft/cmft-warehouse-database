﻿CREATE procedure [dbo].[BuildFactRF] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()

truncate table dbo.FactRF

insert into dbo.FactRF
(
	 EncounterRecno
	,EncounterDate
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode
--	,PCTCode
	,AgeCode
	,SiteCode
	,SourceOfReferralCode
	,Cases
	,SexCode
	,DirectorateCode
)
SELECT
	 Encounter.EncounterRecno

	,EncounterDate =
		case
		when ReferralDate < '1 Jan 1980'
		then '1 Jan 1900'
		else ReferralDate
		end

	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.EpisodicGpPracticeCode
--	,Encounter.PCTCode
	,Encounter.AgeCode
	,Encounter.SiteCode
	,Encounter.SourceOfReferralCode
	,Encounter.Cases
	,Encounter.SexCode
	,DirectorateCode
FROM
	BaseRF Encounter


select @RowsInserted = @@rowcount


select
	 @from = min(EncounterFact.EncounterDate)
	,@to = max(EncounterFact.EncounterDate)
from
	dbo.FactRF EncounterFact
where
	EncounterFact.EncounterDate is not null
and	EncounterFact.EncounterDate > '31 Dec 1979'

exec BuildOlapPractice 'RF'
exec BuildCalendarBasePair @from, @to


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'BuildFactRF', @Stats, @StartTime

﻿
CREATE PROCEDURE [dbo].[BuildBaseAPCCodingCensus] AS

set transaction isolation level read uncommitted;

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

declare @Calendar TABLE (TheDate date not null primary key);
declare @FromDate date, @ToDate date

select @FromDate = '1 Apr 2012'
select @ToDate = dateadd(day, datediff(day, 1, getdate()), 0)
	
declare @NumberOfDayIntervals int = datediff(d,@FromDate,@ToDate);

insert @Calendar(TheDate)
SELECT TheDate
FROM 
	(
	select 
		dateadd(d,i.TheInteger,@FromDate) TheDate
	FROM 
		WarehouseOLAP.dbo.IntegerBase i
	WHERE 
		i.TheInteger <= @NumberOfDayIntervals
	) tblDates



TRUNCATE TABLE BaseAPCCodingCensus;

INSERT INTO BaseAPCCodingCensus
(
	 CensusDate
	,EncounterRecNo
	,SourceUniqueID
	,SourcePatientNo
	,ProviderSpellNo
	,SourceSpellNo
	,SourceEncounterNo
	,AdmissionTime
	,EpisodeStartTime
	,EpisodeEndTime
	,DischargeTime
	,ClinicalCodingCompleteTime
	,CodingLagDays
	,SpecialtyCode
	,EndDirectorateCode
	,EndWardTypeCode
	,PatientCategoryCode
	,SpellClinicalCodingCompleteTime
)

SELECT
	  CensusDate = Calendar.TheDate
	 ,Encounter.EncounterRecNo
	 ,Encounter.SourceUniqueID
	 ,Encounter.SourcePatientNo
	 ,Encounter.ProviderSpellNo
	 ,Encounter.SourceSpellNo
	 ,Encounter.SourceEncounterNo
	 ,Encounter.AdmissionTime
	 ,Encounter.EpisodeStartTime
	 ,Encounter.EpisodeEndTime
	 ,Encounter.DischargeTime
	 ,Encounter.ClinicalCodingCompleteTime
	 ,CodingLagDays =  DATEDIFF(D,Encounter.DischargeTime, Calendar.TheDate)
	 ,Encounter.SpecialtyCode
	 ,Encounter.EndDirectorateCode
	 ,Encounter.EndWardTypeCode
	 ,Encounter.PatientCategoryCode
	 ,SpellClinicalCodingCompleteTime = LatestCodingCompleteTime.SpellClinicalCodingCompleteTime

FROM
(
SELECT
	  EncounterRecNo
	 ,SourceUniqueID
	 ,SourcePatientNo
	 ,ProviderSpellNo
	 ,SourceSpellNo
	 ,SourceEncounterNo
	 ,AdmissionTime
	 ,EpisodeStartTime
	 ,EpisodeEndTime
	 ,DischargeTime
	 ,ClinicalCodingCompleteTime
	 ,SpecialtyCode
	 ,EndDirectorateCode
	 ,ConsultantCode
	 ,EndWardTypeCode
	 ,PatientCategoryCode
FROM
	Warehouse.APC.Encounter Encounter WITH (NOLOCK)
WHERE 
	DischargeDate >= '1 Apr 2012'
)Encounter
	
INNER JOIN 
(
	SELECT
		TheDate
	FROM @Calendar Calendar
	WHERE 
		(
			TheDate IN(SELECT DISTINCT
				dateadd(mm,datediff(mm,-1,TheDate),-1)
			FROM 
				@Calendar Calendar
			WHERE 
				TheDate <=dateadd(day, datediff(day, 1, getdate()), 0))
			
		OR
			TheDate between dateadd(day, -14, dateadd(day, datediff(day, 1, getdate()), 0)) and  dateadd(day, datediff(day, 1, getdate()), 0)
		)
) Calendar
on	(
		(
			Calendar.TheDate >= CONVERT(Date,Encounter.DischargeTime)
		and	Calendar.TheDate < CONVERT(Date,Encounter.ClinicalCodingCompleteTime)
		)
	or
		(
			Calendar.TheDate >= CONVERT(Date,Encounter.DischargeTime)
		and	Encounter.ClinicalCodingCompleteTime is null
		)
	)
INNER JOIN 
(
SELECT 
	 ProviderSpellNo
	,SpellClinicalCodingCompleteTime = Max(ClinicalCodingCompleteTime)

FROM
	Warehouse.APC.Encounter WITH (NOLOCK)
GROUP BY 	 
	ProviderSpellNo
)LatestCodingCompleteTime
ON Encounter.ProviderSpellNo = LatestCodingCompleteTime.ProviderSpellNo

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec warehouse.dbo.WriteAuditLogEvent 'BuildBaseAPCCodingCensus', @Stats, @StartTime


﻿

CREATE PROCEDURE [dbo].[BuildFactCOMReferral] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()


TRUNCATE TABLE dbo.FactCOMReferral

-- Insert new rows into encounter destination table
INSERT INTO dbo.FactCOMReferral
(
     SourceUniqueID --[REFRL_REFNO]
	,ReferredBySpecialtyID --[REFBY_SPECT_REFNO]
	,ReferredToSpecialtyID --[REFTO_SPECT_REFNO]
	,SourceofReferralID --[SORRF_REFNO]
	,ReferralDate --[RECVD_DTTM]
	,MetricCode
	,ReasonforReferralID --[REASN_REFNO]
	,CancelledReasonID --[CANRS_REFNO]
	,Urgency --[URGNC_REFNO]
	,Priority --[PRITY_REFNO]
	,ClosureReasonID --CLORS_REFNO
	,ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]
	,ReferredByStaffTeamID --[REFBY_STEAM_REFNO]
	,ReferredByHealthOrgID --[REFBY_HEORG_REFNO]
	,ReferredToProfessionalCarerID --[REFTO_PROCA_REFNO]
	,ReferredToHealthOrgID --[REFTO_HEORG_REFNO]
	,ReferredToStaffTeamID --[REFTO_STEAM_REFNO]
	,OutcomeOfReferral --[RFOCM_REFNO]
	,ReferralStatus --[RSTAT_REFNO]
	,RejectionID --RejectionID--[RJECT_REFNO]
	,TypeofReferralID --[RETYP_REFNO]
	,PatientSourceID 
--	,PatientCurrentRegisteredPracticeCode
	,RTTStatusID --[RTTST_REFNO]
	,Cases
	,AgeCode
	,EthnicCategoryID
	,PatientSexID
	,RequestedServiceID
)
SELECT
	 BaseCOMReferral.SourceUniqueID --[REFRL_REFNO]
	,ReferredBySpecialtyID = ISNULL(BaseCOMReferral.ReferredBySpecialtyID,-1) --[REFBY_SPECT_REFNO]
	,ReferredToSpecialtyID = ISNULL(BaseCOMReferral.ReferredToSpecialtyID,-1) --[REFTO_SPECT_REFNO]
	,BaseCOMReferral.SourceofReferralID --[SORRF_REFNO]
	,tblReferralsMetrics.ReferralDate
	,tblReferralsMetrics.MetricCode
	,BaseCOMReferral.ReasonforReferralID --[REASN_REFNO]
	,BaseCOMReferral.CancelledReasonID --[CANRS_REFNO]
	,BaseCOMReferral.Urgency --[URGNC_REFNO]
	,BaseCOMReferral.Priority --[PRITY_REFNO]
	,BaseCOMReferral.ClosureReasonID --CLORS_REFNO
	,BaseCOMReferral.ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]
	,ReferredByStaffTeamID = ISNULL(BaseCOMReferral.ReferredByStaffTeamID,-1) --[REFBY_STEAM_REFNO]
	,BaseCOMReferral.ReferredByHealthOrgID --[REFBY_HEORG_REFNO]
	,ReferredToProfessionalCarerID = ISNULL(BaseCOMReferral.ReferredToProfessionalCarerID,-1) --[REFTO_PROCA_REFNO]
	,BaseCOMReferral.ReferredToHealthOrgID --[REFTO_HEORG_REFNO]
	,ReferredToStaffTeamID = ISNULL(BaseCOMReferral.ReferredToStaffTeamID,-1) --[REFTO_STEAM_REFNO]
	,BaseCOMReferral.OutcomeOfReferral --[RFOCM_REFNO]
	,BaseCOMReferral.ReferralStatus --[RSTAT_REFNO]
	,BaseCOMReferral.RejectionID --RejectionID--[RJECT_REFNO]
	,BaseCOMReferral.TypeofReferralID --[RETYP_REFNO]
	,BaseCOMReferral.PatientSourceID 
	--,BaseCOMReferral.PatientCurrentRegisteredPracticeCode
	,BaseCOMReferral.RTTStatusID --[RTTST_REFNO]
	,Cases = 1
	,AgeCode = AgeCode
	,EthnicCategoryID = ISNULL(PatientEthnicGroupID,-1)
	,PatientSexID = ISNULL(PatientSexID,-1)
	,RequestedServiceID
FROM
	WarehouseOLAP.dbo.BaseCOMReferral

INNER JOIN

	-- Referral Metrics
	(
	--Referrals Received
	SELECT 
		 SourceUniqueID = SourceUniqueID
		 
		,ReferralDate = ReferralReceivedDate --[RECVD_DTTM]

		,MetricCode = 'RREC'
	
	FROM 
		WarehouseOLAP.dbo.BaseCOMReferral
	
	WHERE
		ReferralReceivedDate IS NOT NULL
	
	UNION ALL
	
	--Referrals Closed
	SELECT 
		 SourceUniqueID = SourceUniqueID
		 
		,ReferralDate = ReferralClosedDate --[RECVD_DTTM]

		,MetricCode = 'RCLO'
	
	FROM 
		WarehouseOLAP.dbo.BaseCOMReferral
	
	WHERE
		ReferralClosedDate IS NOT NULL
	
	UNION ALL
	
	--Referrals Cancelled
	SELECT 
		 SourceUniqueID = SourceUniqueID
		 
		,ReferralDate = CancelledDate --[RECVD_DTTM]

		,MetricCode = 'RCAN'
	
	FROM 
		WarehouseOLAP.dbo.BaseCOMReferral
	
	WHERE
		CancelledDate IS NOT NULL
	
	UNION ALL
	
	--Referrals Sent
	SELECT 
		 SourceUniqueID = SourceUniqueID
		 
		,ReferralDate = ReferralSentDate --[RECVD_DTTM]

		,MetricCode = 'RSEN'
	
	FROM 
		WarehouseOLAP.dbo.BaseCOMReferral
	
	WHERE
		ReferralSentDate IS NOT NULL
	
	UNION ALL
	
	--Referrals Sent
	SELECT 
		 SourceUniqueID = SourceUniqueID
		 
		,ReferralDate = AuthoriedDate --[RECVD_DTTM]

		,MetricCode = 'RAUT'
	
	FROM 
		WarehouseOLAP.dbo.BaseCOMReferral
	
	WHERE
		AuthoriedDate IS NOT NULL
	
	
	)tblReferralsMetrics
	
	
ON BaseCOMReferral.SourceUniqueID = tblReferralsMetrics.SourceUniqueID
	
	
	
SELECT
	 @from = min(ReferralFact.ReferralDate)
	,@to = max(ReferralFact.ReferralDate)

FROM
	dbo.FactCOMReferral ReferralFact
	
WHERE
	ReferralFact.ReferralDate is not null

EXEC BuildCalendarBase @from, @to


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' 

exec WriteAuditLogEvent 'LoadOlapCOMFactReferrals', @Stats, @StartTime


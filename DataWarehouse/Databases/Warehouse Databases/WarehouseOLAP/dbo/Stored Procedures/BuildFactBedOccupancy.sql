﻿CREATE procedure [dbo].[BuildFactBedOccupancy]
as

/******************************************************************************
** Make sure Calendar table is set up for the date range to be extracted
** Make sure BedOccupancy table is set up for the date range to be extracted
**
******************************************************************************/
exec Warehouse.APC.BuildBedOccupancy

declare @from smalldatetime
declare @to smalldatetime

select
	 @from = min(BedOccupancyDate)
	,@to = max(BedOccupancyDate)
from
	Warehouse.APC.BedOccupancy

exec dbo.BuildCalendarBase @from, @to


truncate table dbo.FactBedOccupancy


/******************************************************************************
** Feed OBD Counts for overlapping Ward Stay/Episode/Day combinations
******************************************************************************/
insert into dbo.FactBedOccupancy
(
	 MetricCode
	,WardStayRecno
	,APCEncounterRecno
	,WardCode
	,ConsultantCode
	,TreatmentFunctionCode
	,SiteCode
	,DirectorateCode
	,BedOccupancyDate
	,Cases
	,Duration
	,KH03SpecialtyCode
)
select
	 Activity.MetricCode
	,Activity.WardStayRecno
	,Activity.APCEncounterRecno
	,Activity.WardCode
	,Activity.ConsultantCode
	,Activity.TreatmentFunctionCode
	,Activity.SiteCode
	,Activity.DirectorateCode
	,Activity.BedOccupancyDate
	,Activity.Cases
	,Activity.Duration

	,KH03SpecialtyCode =
		case
		when Specialty.TreatmentFunctionFlag = 1
		then ConsultantSpecialtyMap.NationalSpecialtyCode
		else SpecialtyMap.NationalSpecialtyCode
		end
from
	(
	select
		 MetricCode =
			case
			when
				Spell.ManagementIntentionCode = 'D' --day case
			and	datediff(day, Spell.AdmissionDate, Spell.DischargeDate) = 0
			then 'DAYCASE'

			when
				datediff(day, Spell.AdmissionDate, Spell.DischargeDate) = 0
			then 'ZEROLOS'

			else 'NIGHT'

			end

		,WardStayRecno = WardStay.EncounterRecno
		,APCEncounterRecno = APCEncounter.EncounterRecno

		/* Ward Stay Fields */
		,WardStay.WardCode

		/* Episode Fields */
		,APCEncounter.ConsultantCode
		,TreatmentFunctionCode = APCEncounter.SpecialtyCode
		,SiteCode = coalesce(WardStay.SiteCode, 'N/A')
		,DirectorateCode = coalesce(APCEncounter.StartDirectorateCode, 'N/A')

		/* Month Fields */
		,BedOccupancy.BedOccupancyDate

		/* Indicators */
		,Cases =
			case

			when
				Spell.ManagementIntentionCode = 'D' --day case
			and	datediff(day, Spell.AdmissionDate, Spell.DischargeDate) = 0
			then BedOccupancy.ZeroLengthStays

			when
				datediff(day, Spell.AdmissionDate, Spell.DischargeDate) = 0
			then BedOccupancy.ZeroLengthStays

			else BedOccupancy.Nights

			end

		,Duration =
			datediff(
				 minute
				,case
				when dateadd(dd, datediff(dd, 0, WardStay.StartTime), 0) < BedOccupancy.BedOccupancyDate
				then BedOccupancy.BedOccupancyDate
				else WardStay.StartTime
				end

				,case
				when dateadd(dd, datediff(dd, 0, WardStay.EndTime), 0)  > BedOccupancy.BedOccupancyDate
				or	WardStay.EndDate is null
				then 
					case when dateadd(dd, datediff(dd, 0, WardStay.EndTime), 0) = BedOccupancy.BedOccupancyDate + 1
					then WardStay.EndTime
					else BedOccupancy.BedOccupancyDate + 1
					end
				else 
					case
					when WardStay.EndTime is null
					then WardStay.EndDate + 1
					else WardStay.EndTime
					end
				end
			)

	from
		Warehouse.APC.BedOccupancy BedOccupancy

	inner join Warehouse.APC.WardStay WardStay
	on	WardStay.EncounterRecno = BedOccupancy.WardStayRecno

	inner join Warehouse.APC.Encounter APCEncounter
	on	APCEncounter.EncounterRecno = BedOccupancy.EncounterRecno

	inner join Warehouse.APC.Encounter Spell 
	on	Spell.ProviderSpellNo = BedOccupancy.ProviderSpellNo
	and	Spell.FirstEpisodeInSpellIndicator = 'Y'
	 
	inner join Warehouse.PAS.AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = Spell.AdmissionMethodCode

	where
		BedOccupancy.BedOccupancyDate > '31 Mar 2009'

	/******************************************************************************
	** Feed missing Ward Stay nights and days to OBD Counts
	******************************************************************************/

	--union all

	--select
	--	 MetricCode = 'MISSSTAY' --Unmatched Stays

	--	,WardStayRecno = WardStay.EncounterRecno
	--	,APCEncounterRecno = -1

	--	/* Ward Stay Fields */
	--	,WardStay.WardCode

	--	/* Episode Fields */
	--	,ConsultantCode = 'N/A'
	--	,TreatmentFunctionCode = 'N/A'
	--	,SiteCode = coalesce(WardStay.SiteCode, 'N/A')
	--	,DirectorateCode = 'N/A'

	--	/* Month Fields */
	--	,BedOccupancy.BedOccupancyDate

	--	/* Indicators */
	--	,Cases =
	--		BedOccupancy.Nights + BedOccupancy.ZeroLengthStays

	--	,Duration = null

	--from
	--	(
	--	select
	--		 ProviderSpellNo
	--		,WardStayRecno
	--		,BedOccupancyDate
	--		,Nights
	--		,ZeroLengthStays
	--	from
	--		(   /* This finds combinations of Ward Stay and Day that overlap each other. */
	--		select
	--			 WardStay.ProviderSpellNo
	--			,WardStayRecno = WardStay.EncounterRecno
	--			,BedOccupancyDate = Calendar.TheDate

	--			/* Nights = lowest Stay/Month end date - highest Stay/Month start date */
	--			,Nights =
	--				datediff(
	--					day

	--					,case
	--					when WardStay.StartDate >= Calendar.TheDate
	--					then WardStay.StartDate
	--					else Calendar.TheDate
	--					end

	--					,case
	--					when coalesce(WardStay.EndDate, '6/6/2079') <= Calendar.TheDate
	--					then WardStay.EndDate
	--					else dateadd(day, 1, Calendar.TheDate)
	--					end
	--				)

	--			/* ZeroLengthStays = 1 or 0

	--			**  0 when
	--			**      ward stay is not zero length (end date = start date)
	--			**  or  ward stay is not part of a zero length spell (max end date = min start date)
	--			*/
	--			,ZeroLengthStays =
	--				case
	--				when coalesce(WardStay.EndDate, '6/6/2079') <> WardStay.StartDate
	--				then 0
	--				when
	--					(
	--					select
	--						max(coalesce(A.EpisodeEndDate,'6/6/2079'))
	--					from
	--						Warehouse.APC.Encounter A
	--					where
	--						A.ProviderSpellNo = WardStay.ProviderSpellNo
	--					) <>
	--					(
	--					select
	--						min(A.EpisodeStartDate)
	--					from
	--						Warehouse.APC.Encounter A
	--					where
	--						A.ProviderSpellNo = WardStay.ProviderSpellNo
	--					)
	--				then 0
	--				when
	--					(
	--					select
	--						max(coalesce(A.EndDate,'6/6/2079'))
	--					from
	--						Warehouse.APC.WardStay A
	--					where
	--						A.ProviderSpellNo = WardStay.ProviderSpellNo
	--					) <>
	--					(
	--					select
	--						min(A.StartDate)
	--					from
	--						Warehouse.APC.WardStay A
	--					where
	--						A.ProviderSpellNo = WardStay.ProviderSpellNo
	--					)
	--				then 0

	--				else 1
	--				end
	--		from
	--			Warehouse.APC.WardStay WardStay

	--		left join Warehouse.APC.Encounter APCEncounter
	--		on	APCEncounter.ProviderSpellNo = WardStay.ProviderSpellNo
	--		and	APCEncounter.FirstEpisodeInSpellIndicator = 'Y'

	--		inner join dbo.CalendarBase Calendar
	--		on	Calendar.TheDate <= coalesce(WardStay.EndDate, APCEncounter.DischargeDate, getdate())
	--		and Calendar.TheDate >= WardStay.StartDate

	--		where
	--			Calendar.TheDate between '1 Apr 2009' and @to

	--		) WardStay
	--	where
	--		Nights + ZeroLengthStays = 1
	--	and	not exists
	--		(
	--		select
	--			1
	--		from
	--			Warehouse.APC.BedOccupancy
	--		where
	--			BedOccupancy.WardStayRecno = WardStay.WardStayRecno
	--		and	BedOccupancy.BedOccupancyDate = WardStay.BedOccupancyDate
	--		)
	--	) BedOccupancy

	--inner join Warehouse.APC.WardStay WardStay
	--on	WardStay.EncounterRecno = BedOccupancy.WardStayRecno


	/******************************************************************************
	** Feed missing Episode nights and days to OBD Counts
	******************************************************************************/

	union all

	select
		 MetricCode = 'MISSEPI' --Unmatched Episodes

		,WardStayRecno = -1
		,APCEncounterRecno = APCEncounter.EncounterRecno

		/* Ward Stay Fields */
		,WardCode = 'N/A'

		/* Episode Fields */
		,APCEncounter.ConsultantCode
		,TreatmentFunctionCode = APCEncounter.SpecialtyCode
		,SiteCode = 'N/A'
		,DirectorateCode = coalesce(APCEncounter.StartDirectorateCode, 'N/A')

		/* Month Fields */
		,BedOccupancy.BedOccupancyDate

		/* Indicators */
		,Cases =
			BedOccupancy.Nights + BedOccupancy.ZeroLengthStays

		,Duration = null

	from
	(
		select
			 ProviderSpellNo
			,EncounterRecno
			,BedOccupancyDate
			,Nights
			,ZeroLengthStays
		from
			(   /* This finds combinations of Ward Stay and Day that overlap each other. */
			select
				 APCEncounter.ProviderSpellNo
				,APCEncounter.EncounterRecno
				,BedOccupancyDate = Calendar.TheDate

				/* Nights = lowest Stay/Month end date - highest Stay/Month start date */
				,Nights =
					datediff(
						day

						,case
						when APCEncounter.EpisodeStartDate >= Calendar.TheDate
						then APCEncounter.EpisodeStartDate
						else Calendar.TheDate
						end

						,case
						when coalesce(APCEncounter.EpisodeEndDate, '6/6/2079') <= Calendar.TheDate
						then APCEncounter.EpisodeEndDate
						else dateadd(day, 1, Calendar.TheDate)
						end
					)

				/* ZeroLengthStays = 1 or 0

				**  0 when
				**      ward stay is not zero length (end date = start date)
				**  or  ward stay is not part of a zero length spell (max end date = min start date)
				*/
				,ZeroLengthStays =
					case
					when coalesce(APCEncounter.EpisodeEndDate, '6/6/2079') <> APCEncounter.EpisodeStartDate
					then 0
					when
						(
						select
							max(coalesce(A.EpisodeEndDate,'6/6/2079'))
						from
							Warehouse.APC.Encounter A
						where
							A.ProviderSpellNo = APCEncounter.ProviderSpellNo
						) <>
						(
						select
							min(A.EpisodeStartDate)
						from
							Warehouse.APC.Encounter A
						where
							A.ProviderSpellNo = APCEncounter.ProviderSpellNo
						)
					then 0
					when
						(
						select
							max(coalesce(A.EndDate,'6/6/2079'))
						from
							Warehouse.APC.WardStay A
						where
							A.ProviderSpellNo = APCEncounter.ProviderSpellNo
						) <>
						(
						select
							min(A.StartDate)
						from
							Warehouse.APC.WardStay A
						where
							A.ProviderSpellNo = APCEncounter.ProviderSpellNo
						)
					then 0

					else 1
					end
			from
				Warehouse.APC.Encounter APCEncounter

			inner join dbo.CalendarBase Calendar
			on	Calendar.TheDate <= coalesce(APCEncounter.EpisodeEndDate, getdate())
			and Calendar.TheDate >= APCEncounter.EpisodeStartDate

			where
				Calendar.TheDate between '1 Apr 2009' and @to

			) Episode
		where
			Nights + ZeroLengthStays = 1
		and	not exists
			(
			select
				1
			from
				Warehouse.APC.BedOccupancy
			where
				(
					BedOccupancy.EncounterRecno = Episode.EncounterRecno
				and	BedOccupancy.BedOccupancyDate = Episode.BedOccupancyDate
				and	BedOccupancy.Nights = 1
				)
			or
				(
					BedOccupancy.ProviderSpellNo = Episode.ProviderSpellNo
				and	BedOccupancy.ZeroLengthStays = 1
				)
			)
		) BedOccupancy

	inner join  Warehouse.APC.Encounter APCEncounter
	on	APCEncounter.EncounterRecno = BedOccupancy.EncounterRecno

	inner join Warehouse.APC.Encounter Spell 
	on	Spell.ProviderSpellNo = BedOccupancy.ProviderSpellNo
	and	Spell.FirstEpisodeInSpellIndicator = 'Y'
	 
	inner join Warehouse.PAS.AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = Spell.AdmissionMethodCode

	/******************************************************************************
	** Feed Finished Ward Stays Counts
	******************************************************************************/

	union all

	select
		MetricCode = 'WARDSTAY'

		,WardStayRecno = WardStay.EncounterRecno
		,APCEncounterRecno = APCEncounter.EncounterRecno

		/* Ward Stay Fields */
		,WardStay.WardCode

		/* Episode Fields */
		,ConsultantCode = coalesce(APCEncounter.ConsultantCode, 'N/A')
		,TreatmentFunctionCode = coalesce(APCEncounter.SpecialtyCode, 'N/A')
		,SiteCode = coalesce(WardStay.SiteCode, 'N/A')
		,DirectorateCode = coalesce(APCEncounter.StartDirectorateCode, 'N/A')

		/* Month Fields */
		,WardStay.EndDate

		/* Indicators */
		,Cases = 1

		,Duration =
			datediff(
				 minute
				,WardStay.StartTime

				,case
				when WardStay.EndTime = WardStay.EndDate
				then WardStay.EndTime + 1
				else WardStay.EndTime
				end
			)

	from
		Warehouse.APC.WardStay WardStay

	inner join Warehouse.APC.BedOccupancy BedOccupancy
	on	BedOccupancy.WardStayRecno = WardStay.EncounterRecno
	and	not exists
		(
		select
			1
		from
			Warehouse.APC.BedOccupancy Previous
		where
			Previous.WardStayRecno = BedOccupancy.WardStayRecno
		and	Previous.BedOccupancyDate > BedOccupancy.BedOccupancyDate
		)

	inner join Warehouse.APC.Encounter APCEncounter
	on	APCEncounter.EncounterRecno = BedOccupancy.EncounterRecno

	where
		WardStay.EndDate is not null
	and	WardStay.EndDate > '31 Mar 2009'

	/******************************************************************************
	** Feed Admissions Counts
	******************************************************************************/

	union all

	select
		MetricCode = 'ADMIT'

		,WardStayRecno = WardStay.EncounterRecno
		,APCEncounterRecno = APCEncounter.EncounterRecno

		/* Ward Stay Fields */
		,WardCode = WardStay.WardCode

		/* Episode Fields */
		,APCEncounter.ConsultantCode
		,TreatmentFunctionCode = APCEncounter.SpecialtyCode
		,SiteCode = coalesce(WardStay.SiteCode, 'N/A')
		,DirectorateCode = coalesce(APCEncounter.StartDirectorateCode, 'N/A')

		/* Month Fields */
		,APCEncounter.AdmissionDate

		/* Indicators */
		,Cases = 1

		,Duration = null
	from
		Warehouse.APC.Encounter APCEncounter

	inner join Warehouse.APC.WardStay WardStay
	on	WardStay.ProviderSpellNo = APCEncounter.ProviderSpellNo
	and	not exists
		(
		select
			1
		from
			Warehouse.APC.WardStay Previous
		where
			Previous.ProviderSpellNo = APCEncounter.ProviderSpellNo
		and	(
				Previous.StartTime < WardStay.StartTime
			or	(
					Previous.StartTime = WardStay.StartTime
				and	Previous.EncounterRecno < WardStay.EncounterRecno
				)
			)
		)
	where
		APCEncounter.FirstEpisodeInSpellIndicator = 'Y'
	and	WardStay.EndDate > '31 Mar 2009'


	/******************************************************************************
	** Feed Finished Spells Counts
	******************************************************************************/

	union all

	select
		 MetricCode = 'DISCH'

		,WardStayRecno = WardStay.EncounterRecno
		,APCEncounterRecno = APCEncounter.EncounterRecno

		/* Ward Stay Fields */
		,WardCode = WardStay.WardCode

		/* Episode Fields */
		,APCEncounter.ConsultantCode
		,TreatmentFunctionCode = APCEncounter.SpecialtyCode
		,SiteCode = coalesce(WardStay.SiteCode, 'N/A')
		,DirectorateCode = coalesce(APCEncounter.StartDirectorateCode, 'N/A')

		/* Month Fields */
		,APCEncounter.DischargeDate

		/* Indicators */
		,Cases = 1

		,Duration = null

	from
		Warehouse.APC.Encounter APCEncounter

	inner join Warehouse.APC.WardStay WardStay
	on	WardStay.ProviderSpellNo = APCEncounter.ProviderSpellNo
	and	not exists
		(
		select
			1
		from
			Warehouse.APC.WardStay Previous
		where
			Previous.ProviderSpellNo = APCEncounter.ProviderSpellNo
		and	(
				Previous.StartTime > WardStay.StartTime
			or	(
					Previous.StartTime = WardStay.StartTime
				and	Previous.EncounterRecno > WardStay.EncounterRecno
				)
			)
		)

	where
		APCEncounter.DischargeDate is not null
	and	APCEncounter.LastEpisodeInSpellIndicator = 'Y'
	and	APCEncounter.DischargeDate > '31 Mar 2009'

	) Activity

left join Warehouse.PAS.Specialty
on	Specialty.SpecialtyCode = Activity.TreatmentFunctionCode

left join Warehouse.PAS.SpecialtyMap
on	SpecialtyMap.SpecialtyCode = Specialty.SpecialtyCode

left join Warehouse.PAS.Consultant
on	Consultant.ConsultantCode = Activity.ConsultantCode

left join Warehouse.PAS.SpecialtyMap ConsultantSpecialtyMap
on	ConsultantSpecialtyMap.SpecialtyCode = Consultant.MainSpecialtyCode

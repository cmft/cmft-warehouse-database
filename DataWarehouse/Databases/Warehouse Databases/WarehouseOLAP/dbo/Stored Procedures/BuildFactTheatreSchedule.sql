﻿
CREATE procedure [dbo].[BuildFactTheatreSchedule] as

declare @today date

set @today = cast(getdate() as date)

/*********************************
** build time slice work tables **
*********************************/


-- operation
select

	 PatientBookingSourceUniqueID =
		case
		when Operation.PatientBookingSourceUniqueID = 0 then -1
		else Operation.PatientBookingSourceUniqueID
		end

	,OperationSourceUniqueID = Operation.SourceUniqueID

	,SessionSourceUniqueID =
		cast(coalesce(Session.SourceUniqueID , -1) as int)

	,Operation.OperationDate
	,Operation.TheatreCode
	,OlapTimeBand.TimeBandCode
	,TimeSliceStateCode = 2 --utilised
into
	#wkOperationTimeSlice
from
	BaseTheatreOperation Operation

inner join OlapTimeBand
on	OlapTimeBand.TimeBandCode between Operation.OperationStartTimeOfDay and (Operation.OperationEndTimeOfDay - 1)

left join OlapTheatre
on OlapTheatre.TheatreCode = Operation.TheatreCode
	
left join BaseTheatreSession Session
on	Session.SourceUniqueID = Operation.SessionSourceUniqueID

where
	Operation.OperationDate between '28 Mar 2011' and dateadd(day , -1 , @today)



-- patient booking
select
	 PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
	,OperationSourceUniqueID = -1

	,SessionSourceUniqueID =
		coalesce(Session.SourceUniqueID , -1)

	,PatientBooking.IntendedProcedureDate
	,PatientBooking.TheatreCode
	,OlapTimeBand.TimeBandCode
	,TimeSliceStateCode = 1 --booked
into
	#wkFutureOperationTimeSlice
from
	BaseTheatrePatientBooking PatientBooking

inner join OlapTimeBand
on	OlapTimeBand.TimeBandCode between PatientBooking.EstimatedOperationStartTimeOfDay and (PatientBooking.EstimatedOperationEndTimeOfDay - 1)

left join OlapTheatre
on OlapTheatre.TheatreCode = PatientBooking.TheatreCode
	
left join BaseTheatreSession Session
on	Session.SourceUniqueID = PatientBooking.SessionSourceUniqueID

where
	not exists
		(
		select
			1
		from
			BaseTheatreOperation Operation
		where
			Operation.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
		)
and PatientBooking.IntendedProcedureDate between @today and dateadd(year , 1 , @today)



-- session
select
	 PatientBookingSourceUniqueID = -1
	,OperationSourceUniqueID = -1
	,SessionSourceUniqueID = Session.SourceUniqueID
	,SessionDate = dateadd(day, datediff(day, 0, Session.StartTime), 0)
	,Session.TheatreCode
	,OlapTimeBand.TimeBandCode
	,TimeSliceStateCode = 0 --unutilised
into
	#wkSessionTimeSlice
from
	BaseTheatreSession Session

inner join OlapTimeBand
on	OlapTimeBand.TimeBandCode between Session.SessionStartTimeOfDay and (Session.SessionEndTimeOfDay - 1)

left join OlapTheatre
on OlapTheatre.TheatreCode = Session.TheatreCode

where
	dateadd(day, datediff(day, 0, Session.StartTime), 0) between '28 Mar 2011'
		and (select max(IntendedProcedureDate) from BaseTheatrePatientBooking where IntendedProcedureDate < dateadd(year , 1 , getdate()))




/*********************
** build fact table **
*********************/



truncate table FactTheatreSchedule

INSERT INTO WarehouseOLAP.dbo.FactTheatreSchedule
(
	 PatientBookingSourceUniqueID
	,OperationSourceUniqueID
	,ProcedureSourceUniqueID
	,SessionSourceUniqueID
	,ScheduleDate
	,TimeBandCode
	,TheatreCode
	,TimeSliceStateCode
	,ConsultantCode
	,EpisodicConsultantCode
	,SurgeonCode
	,AnaesthetistCode
	,PrimaryProcedureCode
	,AgeCode
	,SexCode
	,AdmissionTypeCode
	,OperationTypeCode
	,TheatreSessionPeriodCode
	,SessionConsultantCode
	,TimetableConsultantCode
	,TemplateConsultantCode
	,SessionAnaesthetistCode
	,TimetableAnaesthetistCode
	,TemplateAnaesthetistCode
	,SessionSpecialtyCode
	,TimetableSpecialtyCode
	,TemplateSpecialtyCode
	,Biohazard
	,ASAScoreCode
	,IsSessionBoundary
	,Cases
)


--populate only where operations, bookings or sessions exist

select
	 TimeSlice.PatientBookingSourceUniqueID
	,TimeSlice.OperationSourceUniqueID

	,ProcedureSourceUniqueID =
		coalesce(ProcedureDetail.SourceUniqueID , -1)

	,TimeSlice.SessionSourceUniqueID
	,TimeSlice.ScheduleDate
	,TimeSlice.TimeBandCode

	,TheatreCode =
		cast(TimeSlice.TheatreCode as int)

	,TimeSlice.TimeSliceStateCode

	,ConsultantCode = coalesce(TheatreOperation.ConsultantCode, 0)
	,EpisodicConsultantCode = coalesce(TheatreOperation.EpisodicConsultantCode, -1)
	,SurgeonCode = coalesce(TheatreOperation.Surgeon1Code, 0)
	,AnaesthetistCode = coalesce(TheatreOperation.Anaesthetist1Code, 0)

	,PrimaryProcedureCode = 
		case
		when len(TheatreOperation.PrimaryProcedureCode) = 3 then TheatreOperation.PrimaryProcedureCode + '.9'
		else coalesce(TheatreOperation.PrimaryProcedureCode , '##')
		end

	,AgeCode = coalesce(TheatreOperation.AgeCode , TheatrePatientBooking.AgeCode, 'Age Unknown')

	,SexCode =
		case
		when coalesce(TheatreOperation.SexCode , TheatrePatientBooking.SexCode) = ''
		then 'N' --N/A
		else coalesce(TheatreOperation.SexCode , TheatrePatientBooking.SexCode)
		end

	,AdmissionTypeCode = coalesce(TheatreOperation.AdmissionTypeCode, 0)

	,OperationTypeCode = 
		case coalesce(TheatreOperation.OperationTypeCode, 'N/A')
		when ''
		then 'N/A'
		else TheatreOperation.OperationTypeCode
		end

	,TheatreSessionPeriodCode =
		case
		when
			left(convert(varchar, TheatreSession.StartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, TheatreSession.EndTime, 8), 5) > '16:29' then 'AD' --All Day

		when left(convert(varchar, TheatreSession.StartTime, 8), 5) between '00:00' and '12:29' then 'AM' --Morning
		when left(convert(varchar, TheatreSession.StartTime, 8), 5) between '12:30' and '16:29' then 'PM' --Afternoon
		when left(convert(varchar, TheatreSession.StartTime, 8), 5) between '16:30' and '23:59' then 'VN' --Evening
		else 'NA'
		end

	,SessionConsultantCode = coalesce(TheatreSession.ConsultantCode, 0)

	,TimetableConsultantCode = coalesce(TheatreSession.TimetableConsultantCode, 0)

	,TemplateConsultantCode = coalesce(TheatreSession.TemplateConsultantCode, 0)

	,SessionAnaesthetistCode = coalesce(TheatreSession.AnaesthetistCode1, 0)

	,TimetableAnaesthetistCode = coalesce(TheatreSession.TimetableAnaesthetistCode, 0)

	,TemplateAnaesthetistCode = coalesce(TheatreSession.TemplateAnaesthetistCode, 0)

	,SessionSpecialtyCode = coalesce(TheatreSession.SpecialtyCode, 0)

	,TimetableSpecialtyCode = coalesce(TheatreSession.TimetableSpecialtyCode, 0)

	,TemplateSpecialtyCode = coalesce(TheatreSession.TemplateSpecialtyCode, 0)

	,Biohazard = convert(bit, TheatreOperation.BiohazardFlag)

	,ASAScoreCode = coalesce(TheatreOperation.ASAScoreCode, 0)




	,IsSessionBoundary =
		case
		when SessionStart.SessionStartTimeOfDay is not null or SessionEnd.SessionEndTimeOfDay is not null
		then 1
		else 0
		end
	,Cases = 1
from
	(
	-- operation slices
	select
		 #wkOperationTimeSlice.PatientBookingSourceUniqueID
		,#wkOperationTimeSlice.OperationSourceUniqueID
		,#wkOperationTimeSlice.SessionSourceUniqueID
		,ScheduleDate = #wkOperationTimeSlice.OperationDate
		,#wkOperationTimeSlice.TheatreCode
		,#wkOperationTimeSlice.TimeBandCode
		,#wkOperationTimeSlice.TimeSliceStateCode
	from
		#wkOperationTimeSlice

	union all

	-- booked slices
	select
		 #wkFutureOperationTimeSlice.PatientBookingSourceUniqueID
		,#wkFutureOperationTimeSlice.OperationSourceUniqueID
		,#wkFutureOperationTimeSlice.SessionSourceUniqueID
		,ScheduleDate = #wkFutureOperationTimeSlice.IntendedProcedureDate
		,#wkFutureOperationTimeSlice.TheatreCode
		,#wkFutureOperationTimeSlice.TimeBandCode
		,#wkFutureOperationTimeSlice.TimeSliceStateCode
	from
		#wkFutureOperationTimeSlice

	union all

	-- unutilised slices
	select
		 PatientBookingSourceUniqueID
		,OperationSourceUniqueID
		,SessionSourceUniqueID
		,ScheduleDate = SessionDate
		,TheatreCode
		,TimeBandCode
		,TimeSliceStateCode
	from
		#wkSessionTimeSlice
	where
		not exists
			(
			select	
				1
			from
				#wkOperationTimeSlice
			where
				#wkOperationTimeSlice.TheatreCode = #wkSessionTimeSlice.TheatreCode
			and	#wkOperationTimeSlice.TimeBandCode = #wkSessionTimeSlice.TimeBandCode
			and	#wkOperationTimeSlice.OperationDate = #wkSessionTimeSlice.SessionDate
			)
	and	not exists
			(
			select	
				1
			from
				#wkFutureOperationTimeSlice
			where
				#wkFutureOperationTimeSlice.TheatreCode = #wkSessionTimeSlice.TheatreCode
			and	#wkFutureOperationTimeSlice.TimeBandCode = #wkSessionTimeSlice.TimeBandCode
			and	#wkFutureOperationTimeSlice.IntendedProcedureDate = #wkSessionTimeSlice.SessionDate
			)
	) TimeSlice

left join BaseTheatreSession SessionStart
on	dateadd(day, datediff(day, 0, SessionStart.StartTime), 0) = TimeSlice.ScheduleDate
and	SessionStart.SessionStartTimeOfDay = TimeSlice.TimeBandCode
and	SessionStart.TheatreCode = TimeSlice.TheatreCode

left join BaseTheatreSession SessionEnd
on	dateadd(day, datediff(day, 0, SessionEnd.EndTime), 0) = TimeSlice.ScheduleDate
and	SessionEnd.SessionEndTimeOfDay = TimeSlice.TimeBandCode
and	SessionEnd.TheatreCode = TimeSlice.TheatreCode

left join BaseTheatreOperation TheatreOperation
on	TheatreOperation.SourceUniqueID = TimeSlice.OperationSourceUniqueID

left join BaseTheatreSession TheatreSession
on	TheatreSession.SourceUniqueID = TimeSlice.SessionSourceUniqueID

left join BaseTheatrePatientBooking TheatrePatientBooking
on	TheatrePatientBooking.SourceUniqueID = TimeSlice.PatientBookingSourceUniqueID

left join BaseTheatreProcedureDetail ProcedureDetail
on	ProcedureDetail.OperationDetailSourceUniqueID = TimeSlice.OperationSourceUniqueID
and	TimeSlice.TimeBandCode between ProcedureDetail.ProcedureStartTimeOfDay and (ProcedureDetail.ProcedureEndTimeOfDay - 1)


where
	TimeSlice.TimeBandCode != -1
and	TimeSlice.ScheduleDate is not null

--GC Added 16/04/2015
--Op without link to bookinng found Application sent to CSC for fix
and TimeSlice.PatientBookingSourceUniqueID is not null

order by
	 TimeSlice.ScheduleDate
	,TimeSlice.TheatreCode
	,TimeSlice.TimeBandCode

drop table #wkOperationTimeSlice
drop table #wkSessionTimeSlice
drop table #wkFutureOperationTimeSlice

﻿


CREATE procedure [dbo].[BuildBasePASCasenoteInactive] as

/******************************************************************************
	**  Name: dbo.BuildBasePASCasenoteInactive
	**  Purpose: 
	**
	**  Populates the BasePASCasenoteInactive table comprising of case notes
	**  relating to "inactive" patients
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 15.03.13    CB         Initial coding
	******************************************************************************/


--build work table of inactive patients
exec dbo.BuildPASwrkInactivePatient


truncate table dbo.BasePASCasenoteInactive

insert into dbo.BasePASCasenoteInactive
(
	 SourcePatientNo
	,Surname
	,Forenames
	,DateOfBirth
	,DateOfDeath
	,DistrictNo
	,ModifiedDate
	,ModifiedTime
	,CasenoteNo
	,AllocatedDate
	,CasenoteLocationCode
	,CasenoteLocationDate
	,CasenoteStatus
	,WithdrawnDate
	,Comment
	,CurrentBorrowerCode
	,HasDuplicateRegistration
	,Deceased
	,CasenoteTypeCode
	,IsBorrowed
)

select
	 Patient.SourcePatientNo
	,Patient.Surname
	,Patient.Forenames
	,Patient.DateOfBirth
	,Patient.DateOfDeath
	,Patient.DistrictNo
	,ModifiedDate = cast(Patient.Modified as date)
	,ModifiedTime = Patient.Modified
	,CasenoteNo = Casenote.CasenoteNumber
	,Casenote.AllocatedDate
	,Casenote.CasenoteLocationCode
	,Casenote.CasenoteLocationDate
	,Casenote.CasenoteStatus
	,Casenote.WithdrawnDate
	,Casenote.Comment
	,Casenote.CurrentBorrowerCode
	,HasDuplicateRegistration = null

	,Deceased =
		case
		when Patient.DateOfDeath is not null then 1
		else 0
		end

	,CasenoteTypeCode =
		case
		when isnumeric(CasenoteNumber) = 1 and len(CasenoteNumber) = 10 then 'NHSNumber'
		when left(CasenoteNumber , 3) = 'PCT' then 'PCT'
		when charindex('/' , CasenoteNumber , 0) != 0 then
			case
			when left(CasenoteNumber , 1) = 'G' then 'G'
			when left(CasenoteNumber , 1) = 'T' then 'T'
			else left(CasenoteNumber , charindex('/' , CasenoteNumber , 0) - 1)
			end
		--when right(CasenoteNumber , 1) = 'D' then left(CasenoteNumber , 1) + '_D'
		when right(CasenoteNumber , 1) = 'D' then 'D'
		else left(CasenoteNumber , 1)
		end

	,IsBorrowed =
		case
		when Casenote.CurrentBorrowerCode is null then 0
		else 1
		end
from
	Warehouse.PAS.Patient

inner join PAS.wrkInactivePatient Inactive
on	Inactive.SourcePatientNo = Patient.SourcePatientNo

inner join Warehouse.PAS.Casenote
on	Casenote.SourcePatientNo = Patient.SourcePatientNo


where
	coalesce(Casenote.CasenoteStatus , '') != 'WITHDRAWN'
--limit casenotes to those located at Gorton Library , short for offsite storage and st marys library
and	Casenote.CasenoteLocationCode in (
		 'GOR'
		,'MML'
		,'OSS'
		,'SML'
		)
--and	Casenote.CurrentBorrowerCode is null


exec dbo.BuildOlapCasenoteType


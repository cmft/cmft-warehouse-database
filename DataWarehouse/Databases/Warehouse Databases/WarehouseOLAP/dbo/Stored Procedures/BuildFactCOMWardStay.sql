﻿

CREATE PROCEDURE [dbo].[BuildFactCOMWardStay] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()


TRUNCATE TABLE dbo.FactCOMWardStay

-- Insert new rows into encounter destination table
INSERT INTO dbo.FactCOMWardStay
(
	 EncounterRecno
	,MetricCode
	,EncounterDate
	,EncounterTimeOfDay
	,EncounterLengthOfStay
	,ProfessionalCarerID
	,SpecialtyID
	,PracticeCode
	,PatientSexID
	,PatientEthnicGroupID
	,ServicePointID
	,AdmissionMethodID
	,AdmissionSourceID
	,PatientClassificationID
	,AdminCategoryID
	,IntendedManagementID
	,ReasonForAdmissionID
	,DischargeDestinationID
	,DischargeMethodID
	,AgeCode
	,Cases
)
--Admissions
SELECT
	 EncounterRecno
	,MetricCode = 'ADMI'
	,EncounterDate = AdmissionDate
	,EncounterTimeOfDay = AdmissionTimeOfDay
	,EncounterLengthOfStay = LOS
	,ProfessionalCarerID = ISNULL(SpellProfessionalCarer,-1)
	,SpecialtyID = ISNULL(SpellSpecialty,-1)
	,PracticeCode = WardStayPracticeCode
	,PatientSexID = ISNULL(PatientSexID,-1)
	,PatientEthnicGroupID = ISNULL(PatientEthnicGroupID,-1)
	,ServicePointID = SourceServicePointID
	,AdmissionMethodID = SourceAdmissionMethodID
	,AdmissionSourceID = SourceAdmissionSourceID
	,PatientClassificationID = SourcePatientClassificationID
	,AdminCategoryID = SourceAdminCategoryID
	,IntendedManagementID = SourceIntendedManagementID
	,ReasonForAdmissionID = SourceReasonForAdmissionID
	,DischargeDestinationID = SourceDischargeDestinationID
	,DischargeMethodID =SourceDischargeMethodID
	,AgeCode = AgeCode
	,Cases = Cases
FROM
	dbo.BaseCOMWardStay WardStay
WHERE 
	WardStay.FirstStayInSpellIndicator = 1

UNION ALL

--Discharges
SELECT
	 EncounterRecno
	,MetricCode = 'DISC'
	,EncounterDate = DischargeDate
	,EncounterTimeOfDay = DischargeTimeOfDay
	,EncounterLengthOfStay = LOS
	,ProfessionalCarerID = ISNULL(SpellProfessionalCarer,-1)
	,SpecialtyID = ISNULL(SpellSpecialty,-1)
	,PracticeCode = WardStayPracticeCode
	,PatientSexID = ISNULL(PatientSexID,-1)
	,PatientEthnicGroupID = ISNULL(PatientEthnicGroupID,-1)
	,ServicePointID = SourceServicePointID
	,AdmissionMethodID = SourceAdmissionMethodID
	,AdmissionSourceID = SourceAdmissionSourceID
	,PatientClassificationID = SourcePatientClassificationID
	,AdminCategoryID = SourceAdminCategoryID
	,IntendedManagementID = SourceIntendedManagementID
	,ReasonForAdmissionID = SourceReasonForAdmissionID
	,DischargeDestinationID = SourceDischargeDestinationID
	,DischargeMethodID =SourceDischargeMethodID
	,AgeCode = AgeCode
	,Cases = Cases
FROM
	dbo.BaseCOMWardStay WardStay
WHERE 
	WardStay.LastStayInSpellIndicator = 1
	AND WardStay.DischargeDate IS NOT NULL

UNION ALL

--Ward Stays
SELECT
	 EncounterRecno
	,MetricCode = 'STAY'
	,EncounterDate = EndDate
	,EncounterTimeOfDay = WardStay.WardStayEndTimeOfDay
	,EncounterLengthOfStay = LOWS
	,ProfessionalCarerID = ISNULL(SpellProfessionalCarer,-1)
	,SpecialtyID = ISNULL(SpellSpecialty,-1)
	,PracticeCode = WardStayPracticeCode
	,PatientSexID = ISNULL(PatientSexID,-1)
	,PatientEthnicGroupID = ISNULL(PatientEthnicGroupID,-1)
	,ServicePointID = SourceServicePointID
	,AdmissionMethodID = SourceAdmissionMethodID
	,AdmissionSourceID = SourceAdmissionSourceID
	,PatientClassificationID = SourcePatientClassificationID
	,AdminCategoryID = SourceAdminCategoryID
	,IntendedManagementID = SourceIntendedManagementID
	,ReasonForAdmissionID = SourceReasonForAdmissionID
	,DischargeDestinationID = SourceDischargeDestinationID
	,DischargeMethodID =SourceDischargeMethodID
	,AgeCode = AgeCode
	,Cases = Cases
FROM
	dbo.BaseCOMWardStay WardStay
WHERE 
	WardStay.EndDate IS NOT NULL


UNION ALL

--Spells
SELECT
	 EncounterRecno
	,MetricCode = 'SPEL'
	,EncounterDate = DischargeDate
	,EncounterTimeOfDay = DischargeTimeOfDay
	,EncounterLengthOfStay = LOS
	,ProfessionalCarerID = ISNULL(SpellProfessionalCarer,-1)
	,SpecialtyID = ISNULL(SpellSpecialty,-1)
	,PracticeCode = WardStayPracticeCode
	,PatientSexID = ISNULL(PatientSexID,-1)
	,PatientEthnicGroupID = ISNULL(PatientEthnicGroupID,-1)
	,ServicePointID = SourceServicePointID
	,AdmissionMethodID = SourceAdmissionMethodID
	,AdmissionSourceID = SourceAdmissionSourceID
	,PatientClassificationID = SourcePatientClassificationID
	,AdminCategoryID = SourceAdminCategoryID
	,IntendedManagementID = SourceIntendedManagementID
	,ReasonForAdmissionID = SourceReasonForAdmissionID
	,DischargeDestinationID = SourceDischargeDestinationID
	,DischargeMethodID =SourceDischargeMethodID
	,AgeCode = AgeCode
	,Cases = Cases
FROM
	dbo.BaseCOMWardStay WardStay
WHERE 
	WardStay.FirstStayInSpellIndicator = 1
	AND WardStay.DischargeDate IS NOT NULL

select @RowsInserted = @@rowcount

SELECT
	 @from = min(WardStay.EncounterDate)
	,@to = max(WardStay.EncounterDate)

FROM
	dbo.FactCOMWardStay WardStay
	
WHERE
	WardStay.EncounterDate is not null

EXEC BuildCalendarBase @from, @to




select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec WriteAuditLogEvent 'LoadOLAPFactCOMWardStay', @Stats, @StartTime


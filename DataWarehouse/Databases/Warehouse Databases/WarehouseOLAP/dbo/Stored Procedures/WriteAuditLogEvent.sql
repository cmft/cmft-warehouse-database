﻿CREATE   PROCEDURE [dbo].[WriteAuditLogEvent] 
	 @ProcessCode varchar(255)
	,@Event varchar(255)
	,@StartTime datetime = null
as

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcessCode
	,@Event
	,@StartTime

﻿create proc [dbo].[BuildBaseDictationDocument]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.BaseDictationDocument

insert into dbo.BaseDictationDocument
(
	DocumentRecNo
	,SourceUniqueID
	,[Document]
	,[BatchNumber]
	,[Length]
	,[DocumentTypeCode]
	,[Priority]
	,[StatusCode]
	,[WorktypeCode]
	,[WorkStationCode]
	,[InterfaceCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[AuthoriserCode]
	,[CreationDate]
	,[CreationTime]
	,[RecognitionRate]
	,[WaitingTimeCorrection]
	,[CorrectionDate]
	,[CorrectionTime]
	,[CorrectionTimeTaken]
	,[WaitingTimeAuthorization]
	,[AuthorisationDate]
	,[AuthorisationTime]
	,[PassThroughTimeTaken]
	,[ContextCode]
	,[DictationTimeTaken]
	,[CorrectionRate]
	,WarehouseInterfaceCode
)

select
	DocumentRecNo
	,SourceUniqueID
	,[Document]
	,[BatchNumber]
	,[Length]
	,[DocumentTypeCode]
	,[Priority]
	,[StatusCode]
	,[WorktypeCode]
	,[WorkStationCode]
	,[InterfaceCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[AuthoriserCode]
	,[CreationDate]
	,[CreationTime]
	,[RecognitionRate]
	,[WaitingTimeCorrection]
	,[CorrectionDate]
	,[CorrectionTime]
	,[CorrectionTimeTaken]
	,[WaitingTimeAuthorization]
	,[AuthorisationDate]
	,[AuthorisationTime]
	,[PassThroughTimeTaken]
	,[ContextCode]
	,[DictationTimeTaken]
	,[CorrectionRate]
	,WarehouseInterfaceCode
from
	[warehouse].Dictation.Document
	
	
select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime

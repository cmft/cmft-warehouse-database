﻿--use WarehouseOLAP

--alter table [dbo].[BaseAPC]

--add PseudoPostCode varchar(10)




CREATE procedure [dbo].[BuildBaseAPC] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseAPC

insert into dbo.BaseAPC
	(
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,CancelledElectiveAdmissionFlag
	,LocalAdminCategoryCode
	,EpisodicGpPracticeCode
	,PCTCode
	,LocalityCode
	,AgeCode
	,HRGCode
	,SpellHRGCode
	,CategoryCode
	,LOE
	,LOS
	,Cases
	,FirstEpisodeInSpellIndicator
	,RTTActivity
	,RTTBreachStatusCode
	,DiagnosticProcedure
	,RTTTreated

	,EpisodeStartTimeOfDay
	,EpisodeEndTimeOfDay
	,AdmissionTimeOfDay
	,DischargeTimeOfDay
	,StartDirectorateCode
	,EndDirectorateCode
	,ResidencePCTCode
	,ISTAdmissionSpecialtyCode
	,ISTAdmissionDemandTime
	,ISTDischargeTime
	,ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay
	,PatientCategoryCode
	,Research2
	
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate
	,ClinicalCodingCompleteTime
	,PseudoPostcode
	)
SELECT --top 100
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceSpellNo
	,Encounter.SourceEncounterNo
	,Encounter.ProviderSpellNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath

	,SexCode =
		case Encounter.SexCode
		when '' then '0'
		when 'M' then '1'
		when 'F' then '2'
		when 'I' then '9'
		else '#'
		end

	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.DateOnWaitingList
	,Encounter.AdmissionDate
	,Encounter.DischargeDate
	,Encounter.EpisodeStartDate
	,Encounter.EpisodeEndDate

	,StartSiteCode =
		coalesce(
			 Encounter.StartSiteCode
			,'N/A'
		)

	,Encounter.StartWardTypeCode

	,EndSiteCode =
		coalesce(
			 Encounter.EndSiteCode
			,'N/A'
		)

	,Encounter.EndWardTypeCode
	,Encounter.RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode, Encounter.EpisodicGpPracticeCode, 'X99999')

	,Encounter.SiteCode
	,Encounter.AdmissionMethodCode
	,Encounter.AdmissionSourceCode
	,Encounter.PatientClassificationCode
	,Encounter.ManagementIntentionCode
	,Encounter.DischargeMethodCode
	,Encounter.DischargeDestinationCode
	,Encounter.AdminCategoryCode

	,ConsultantCode =
		coalesce(Encounter.ConsultantCode, 'N/A')

	,SpecialtyCode =
		coalesce(Encounter.SpecialtyCode, 'N/A')

	,Encounter.LastEpisodeInSpellIndicator
	,Encounter.FirstRegDayOrNightAdmit
	,Encounter.NeonatalLevelOfCare
	,Encounter.PASHRGCode

	,PrimaryDiagnosisCode =
		coalesce(rtrim(left(Encounter.PrimaryDiagnosisCode, 5)), '##')

	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12

	,PrimaryOperationCode =
		coalesce(rtrim(left(Encounter.PrimaryOperationCode, 5)), '##')
	
	,Encounter.PrimaryOperationDate
	,Encounter.SecondaryOperationCode1
	,Encounter.SecondaryOperationDate1
	,Encounter.SecondaryOperationCode2
	,Encounter.SecondaryOperationDate2
	,Encounter.SecondaryOperationCode3
	,Encounter.SecondaryOperationDate3
	,Encounter.SecondaryOperationCode4
	,Encounter.SecondaryOperationDate4
	,Encounter.SecondaryOperationCode5
	,Encounter.SecondaryOperationDate5
	,Encounter.SecondaryOperationCode6
	,Encounter.SecondaryOperationDate6
	,Encounter.SecondaryOperationCode7
	,Encounter.SecondaryOperationDate7
	,Encounter.SecondaryOperationCode8
	,Encounter.SecondaryOperationDate8
	,Encounter.SecondaryOperationCode9
	,Encounter.SecondaryOperationDate9
	,Encounter.SecondaryOperationCode10
	,Encounter.SecondaryOperationDate10
	,Encounter.SecondaryOperationCode11
	,Encounter.SecondaryOperationDate11
	,Encounter.OperationStatusCode
	,Encounter.ContractSerialNo
	,Encounter.CodingCompleteDate
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.EpisodeStartTime
	,Encounter.EpisodeEndTime
	,Encounter.RegisteredGdpCode
	,Encounter.EpisodicGpCode
	,Encounter.InterfaceCode
	,Encounter.CasenoteNumber
	,Encounter.NHSNumberStatusCode
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.TransferFrom
	,Encounter.DistrictNo
	,Encounter.ExpectedLOS
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.Operation
	,Encounter.Research1
	,Encounter.CancelledElectiveAdmissionFlag
	,Encounter.LocalAdminCategoryCode

	,EpisodicGpPracticeCode =
		coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredGpPracticeCode, 'X99999')

	,Encounter.PCTCode
	,Encounter.LocalityCode

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 1 and
	datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.EpisodeStartDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 28 and
	datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,HRGCode =
		coalesce(HRG4Encounter.HRGCode, 'ZZZ')

	,SpellHRGCode =
		coalesce(HRG4Spell.HRGCode, 'ZZZ')

	,CategoryCode =
		case
		when Encounter.ManagementIntentionCode = 'I' then '10'
		when Encounter.ManagementIntentionCode = 'D' then '20'
		when Encounter.ManagementIntentionCode = 'R' then '24'
		else '28'
		end

	,LOE =
		datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)

	,LOS =
		case
		when DischargeDate is null and AdmissionDate > (select max(DischargeDate) from Warehouse.APC.Encounter)
		then 0
		else
			datediff(
				 day
				,Encounter.AdmissionDate
				,coalesce(
					 Encounter.DischargeDate
					,(
					select
						max(DischargeDate)
					from
						Warehouse.APC.Encounter
					)
				)
			)
		end

	,Cases = 1

	,FirstEpisodeInSpellIndicator =
		case
		when not exists
		(
		select
			1
		from
			Warehouse.APC.Encounter Previous
		where
			Previous.ProviderSpellNo = Encounter.ProviderSpellNo
		and	(
				Previous.EpisodeStartTime < Encounter.EpisodeStartTime
			or	(
					Previous.EpisodeStartTime = Encounter.EpisodeStartTime
				and	Previous.SourceEncounterNo < Encounter.SourceEncounterNo
				)
			)
		)
		then 1
		else 0
		end

	,RTTActivity = null
	--	convert(
	--		bit
	--		,case
	--		when
	--			Encounter.AdmissionTime = Encounter.EpisodeStartTime
	--		and	Encounter.AdmissionMethodCode in ('BA', 'BL', 'WL')
	--		and	Encounter.SpecialtyCode not in 
	--			(
	--			select
	--				SpecialtyCode
	--			from
	--				RTT.dbo.ExcludedSpecialty
	--			)

	--		-- 'GENE' ?
	--		and	Encounter.ConsultantCode <> 'FRAC'

	--		--remove CEA admissions
	--		and	Encounter.CancelledElectiveAdmissionFlag <> 1

	--		--remove ICAT
	--		and	Encounter.SiteCode != 'FMSK'

	--		and	not exists
	--			(
	--			select
	--				1
	--			from
	--				RTT.dbo.RTTClockStart ClockChangeReason
	--			where
	--				ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
	--			and	ClockChangeReason.SourceEntityRecno = Encounter.SourceSpellNo
	--			and	coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
	--			)

	----check for NOPs on previous referral
	--		and	not exists
	--			(
	--			select
	--				1
	--			from
	--				WH.RF.Encounter Referral

	--			inner join RTT.dbo.RTTOPClockStop ClockChangeReason
	--			on	ClockChangeReason.SourcePatientNo = Referral.SourcePatientNo
	--			and	ClockChangeReason.SourceEntityRecno = Referral.SourceEncounterNo
	--			and	(
	--					ClockChangeReason.ClockStopReasonCode = 'NOP' --Not an 18 week pathway
	--				or	ClockChangeReason.ClockStartReasonCode = 'NOP' --Not an 18 week pathway
	--				)

	--			where
	--				Referral.EncounterRecno = Encounter.ReferralEncounterRecno	
	--			)
	--		then 1
	--		else 0
	--		end
	--	)


	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null
		then 'U'
		when Encounter.RTTBreachDate < Encounter.AdmissionDate
		then 'B'
		else 'N'
		end

	,DiagnosticProcedure =
		convert(
			bit
			,case
			when RTTStatus.ClockStopFlag = 1 then 0
			when Encounter.RTTBreachDate is null then 0 -- unknown clock starts are counted
			when DiagnosticProcedure.ProcedureCode is null
			then 0
			else 1
			end
		)

	,RTTTreated =
		case
		when RTTStatus.ClockStopFlag = 1 then 1
		when Encounter.RTTBreachDate is null then 1 -- unknown clock starts are counted
		else 0
		end

	,EpisodeStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, EpisodeStartTime), 0)
				,EpisodeStartTime
			)
			,-1
		)

	,EpisodeEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, EpisodeEndTime), 0)
				,EpisodeEndTime
			)
			,-1
		)

	,AdmissionTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AdmissionTime), 0)
				,AdmissionTime
			)
			,-1
		)

	,DischargeTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, DischargeTime), 0)
				,DischargeTime
			)
			,-1
		)

	
	,StartDirectorateCode = coalesce(Encounter.StartDirectorateCode , 'N/A')
	,EndDirectorateCode = coalesce(Encounter.EndDirectorateCode , 'N/A')
	,ResidencePCTCode = ResidencePCTCode

	,ISTAdmissionSpecialtyCode
	,ISTAdmissionDemandTime
	,ISTDischargeTime

	,ISTAdmissionDemandTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, ISTAdmissionDemandTime), 0)
				,ISTAdmissionDemandTime
			)
			,-1
		)

	,ISTDischargeTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, ISTDischargeTime), 0)
				,ISTDischargeTime
			)
			,-1
		)
	,Encounter.PatientCategoryCode
	,Encounter.Research2
	,Encounter.ClinicalCodingStatus
	,Encounter.ClinicalCodingCompleteDate
	,Encounter.ClinicalCodingCompleteTime
	,PseudoPostcode
from
	Warehouse.APC.Encounter Encounter

left join Warehouse.APC.HRG4Encounter HRG4Encounter
on	HRG4Encounter.EncounterRecno = Encounter.EncounterRecno

left join Warehouse.APC.HRG4Spell HRG4Spell
on	HRG4Spell.EncounterRecno = Encounter.EncounterRecno

left join Warehouse.dbo.DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = Encounter.PrimaryOperationCode

left join Warehouse.PAS.RTTStatus RTTStatus
on    RTTStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode

where
	Encounter.EpisodeEndDate >= '1 Apr 2009'
or	Encounter.EpisodeEndDate is null


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseAPC', @Stats, @StartTime




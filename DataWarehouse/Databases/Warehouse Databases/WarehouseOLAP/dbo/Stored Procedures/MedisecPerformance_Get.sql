﻿

CREATE proc [dbo].[MedisecPerformance_Get]
(
	
	@start datetime = null
	,@end datetime = null
	,@division varchar(50) = null
	,@specialty varchar(50) = null
	,@proc varchar(50) = null
	,@pct varchar(100) = null
	,@medisecuser varchar(50) = null
)

as


-- ===============================================
-- Modified:	12 Aug 2011
-- By:			Cassian Butterworth
-- Description:	Refactored code style
--				Removed abbreviated table aliases
--				Replaced isnull with coalesce
-- ===============================================

set dateformat dmy

--set @end = getdate()
--set @start = coalesce(@start, dbo.f_First_of_Month(dateadd(year, -1, @end)))
set @specialty = coalesce(@specialty, '(Select All)')
set @division = coalesce(@division, '(Select All)')
set @pct = coalesce(@pct, '(Select All)')
set @medisecuser = coalesce(@medisecuser, '(Select All)')


begin



select 
	calendar.FirstDayOfWeek
	,DivisionCode = DischargeDivisionCode
	,Division = upper(division.Division)
	,SpecialtyCode = Specialty.SpecialtyCode
	,Specialty = coalesce(specialty.Specialty, '') + ' (' + Specialty.SpecialtyCode + ')'
	,ConsultantCode = Consultant.ConsultantCode
	,Consultant = Consultant.Consultant + ' (' + Consultant.ConsultantCode + ')'
	,SignedBy = coalesce(MedisecUser.Username + ' (' + MedisecUser.SourceUserCode + ')','N/A')

	,LetterRequired =
		sum(
			case
			when
					StatusCode not in ('09') 
				and coalesce(DocumentProgressCode, '') not in ('N','X') 
				and RequestOrderForDischarge = 1 
				then Cases
			else 0
			end
		)

	,DNFRequired =
		sum(
			case
			when 
					StatusCode not in ('09') 
				and coalesce(DocumentProgressCode, '') not in ('N','X')	
				--and DocumentTypeCode in ('F', 'I')
				and DocumentTypeCode = ('F')
				and RequestOrderForDischarge = 1
				then Cases
			else 0
			end
		)

	,LetterProduced =
		sum(
			case
			when
					StatusCode not in ('09') 
				and IsFirstDocumentForDischarge = 1
				and DocumentProgressCode in ('G','P')
				then Cases
			else 0
			end
		)

	,DNFProduced = 
		sum(
			case
			when 
					StatusCode not in ('09') 
				and IsFirstDocumentForDischarge = 1
				and DocumentProgressCode in ('G','P')
				--and DocumentTypeCode in ('F', 'I')
				and DocumentTypeCode in ('F')
				then Cases
			else 0
			end
		)

	,LetterPriorToDischarge =
		sum(
			case
			when
					SignedTime < DischargeTime 
				and StatusCode not in ('09')
				and DocumentProgressCode in ('G','P')
				and IsFirstDocumentForDischarge = 1
				then Cases
			else 0
			end
		)

	,[Within24Hours] = 
		sum(
			case
			when 
					datediff(minute, DischargeTime, SignedTime) / 60 < 24
				and StatusCode not in ('09')
				and DocumentProgressCode in ('G','P')
				and IsFirstDocumentForDischarge = 1
				then Cases
			else 0
			end
		)

	,Between24And48Hours =
		sum(
			case
			when 
					datediff(minute, DischargeTime, SignedTime) / 60 between 24 and 48
				and StatusCode not in ('09')
				and DocumentProgressCode in ('G','P')
				and IsFirstDocumentForDischarge = 1
				then Cases
			else 0
			end
		)

	,GreaterThan48Hours = 
		sum(
			case
			when 
					datediff(minute, DischargeTime, SignedTime) / 60 > 48
				and StatusCode not in ('09') 
				and DocumentProgressCode in ('G','P')
				and IsFirstDocumentForDischarge = 1
			then Cases
			else 0
			end
		)
		,Breach = 
		sum(
			case
			when 
				(
				datediff(minute, DischargeTime, SignedTime) / 60 >= 24
				and StatusCode not in ('09') 
				and DocumentProgressCode in ('G','P')
				and IsFirstDocumentForDischarge = 1
				)
				or
				(
				StatusCode not in ('09') 
				and coalesce(DocumentProgressCode, '') not in ('N','X')
				and RequestOrderForDischarge = 1
				and DocumentSentGPForDischarge = 0
				)
				then Cases	
			else 0
			end
		)
	,[AdmissionReason] = 
		sum(
			case
			when 
					[AdmissionReason] <> ''
				and StatusCode not in ('09')
				and IsFirstDocumentForDischarge = 1
				and DocumentProgressCode in ('G','P')
				--and DocumentTypeCode in ('F', 'I')
				and DocumentTypeCode in ('F')
				then Cases
			else 0
			end
		)

	,[Diagnosis] = 
		sum(
			case
			when 
					[Diagnosis] <> '' 
				and StatusCode not in ('09')
				and IsFirstDocumentForDischarge = 1
				and DocumentProgressCode in ('G','P')
				--and DocumentTypeCode in ('F', 'I')
				and DocumentTypeCode in ('F')
				then Cases
			else 0
			end
		)

	,[Investigations] = 
		sum(
			case
			when 
					[Investigations] <> ''
				and StatusCode not in ('09')
				and IsFirstDocumentForDischarge = 1
				and DocumentProgressCode in ('G','P')
				--and DocumentTypeCode in ('F', 'I')
				and DocumentTypeCode in ('F')
				then Cases
			else 0
			end
		)

	,[RecommendationsOnFutureManagement] = 
		sum(
			case
			when 
					[RecommendationsOnFutureManagement] <> ''
				and StatusCode not in ('09')
				and IsFirstDocumentForDischarge = 1
				and DocumentProgressCode in ('G','P')
				--and DocumentTypeCode in ('F', 'I')
				and DocumentTypeCode in ('F')
				then Cases
			else 0
			end
		)

	,InformationToPatientOrRelatives = 
		sum(
			case
			when 
					InformationToPatientOrRelatives <> ''
				and StatusCode not in ('09')
				and IsFirstDocumentForDischarge = 1
				and DocumentProgressCode in ('G','P')
				--and DocumentTypeCode in ('F', 'I')
				and DocumentTypeCode in ('F')
				then Cases
			else 0
			end
		)

	,[ChangesToMedication] = 
		sum(
			case
			when 
					[ChangesToMedication] <> ''
				and StatusCode not in ('09')
				and IsFirstDocumentForDischarge = 1
				and DocumentProgressCode in ('G','P')
				--and DocumentTypeCode in ('F', 'I')
				and DocumentTypeCode in ('F')
				then Cases
			else 0
			end
		) 													

from
	WarehouseOLAP.dbo.BaseDischargeSummary DischargeSummary

inner join 
	Warehouse.WH.TreatmentFunction specialty
on 
	specialty.SpecialtyCode = DischargeSummary.NationalSpecialtyCode
	
inner join 
	Warehouse.WH.Division division
on 
	DischargeSummary.DischargeDivisionCode = division.DivisionCode

inner join 
	Warehouse.PAS.Consultant Consultant
on 
	Consultant.ConsultantCode = DischargeSummary.ConsultantCode
	
inner join
	WarehouseOLAP.dbo.OlapCalendar calendar
on 
	DischargeSummary.DischargeDate = calendar.TheDate

left outer join
	[warehouse].[Medisec].[UserMaster] MedisecUser
on
	SignedByCode = MedisecUser.SourceUserCode
	

where
	(DischargeSummary.DischargeDate between @start and @end)
	
	and (DischargeSummary.NationalSpecialtyCode = @specialty or @specialty = '(Select All)')
	and (DischargeSummary.DischargeDivisionCode = @division or @division = '(Select All)')
	
	and (cast(DischargeSummary.EpisodicGpPracticeIsMedisecUser as varchar(10)) = @medisecuser or @medisecuser = '(Select All)')
	
	and (DischargeSummary.PCTCode = @pct or @pct = '(Select All)')
	
	--and DischargeSummary.SpecialtyCode in (@specialty)-- (select Value from warehouse.dbo.SSRS_MultiValueParamSplit(@specialty,','))
	--and DischargeSummary.DischargeDivisionCode in (@division) -- (select Value from  warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))

group by
	Calendar.FirstDayOfWeek
	,DischargeSummary.DischargeDivisionCode
	,Division.Division
	,Specialty.SpecialtyCode
	,Specialty.Specialty
	,Consultant.ConsultantCode
	,Consultant.Consultant
	,MedisecUser.SourceUserCode
	,MedisecUser.UserName

--select 
--	calendar.FirstDayOfWeek
--	,DivisionCode = DischargeDivisionCode
--	,Division = upper(division.Division)
--	,SpecialtyCode = dischargesummary.NationalSpecialtyCode
--	,Specialty = coalesce(specialty.Specialty, '') + ' (' + dischargesummary.NationalSpecialtyCode + ')'
--	,dischargesummary.ConsultantCode

--	,LetterRequired =
--		sum(
--			case
--			when
--					StatusCode not in ('09') 
--				and coalesce(DocumentProgressCode, '') not in ('N','X') 
--				and RequestOrderForDischarge = 1 
--				then Cases
--			else 0
--			end
--		)

--	,DNFRequired =
--		sum(
--			case
--			when 
--					StatusCode not in ('09') 
--				and coalesce(DocumentProgressCode, '') not in ('N','X')	
--				--and DocumentTypeCode in ('F', 'I')
--				and DocumentTypeCode = ('F')
--				and RequestOrderForDischarge = 1
--				then Cases
--			else 0
--			end
--		)

--	,LetterProduced =
--		sum(
--			case
--			when
--					StatusCode not in ('09') 
--				and IsFirstDocumentForDischarge = 1
--				and DocumentProgressCode in ('G','P')
--				then Cases
--			else 0
--			end
--		)

--	,DNFProduced = 
--		sum(
--			case
--			when 
--					StatusCode not in ('09') 
--				and IsFirstDocumentForDischarge = 1
--				and DocumentProgressCode in ('G','P')
--				--and DocumentTypeCode in ('F', 'I')
--				and DocumentTypeCode in ('F')
--				then Cases
--			else 0
--			end
--		)

--	,LetterPriorToDischarge =
--		sum(
--			case
--			when
--					SignedTime < DischargeTime 
--				and StatusCode not in ('09')
--				and DocumentProgressCode in ('G','P')
--				and IsFirstDocumentForDischarge = 1
--				then Cases
--			else 0
--			end
--		)

--	,[Within24Hours] = 
--		sum(
--			case
--			when 
--					datediff(minute, DischargeTime, SignedTime) / 60 < 24
--				and StatusCode not in ('09')
--				and DocumentProgressCode in ('G','P')
--				and IsFirstDocumentForDischarge = 1
--				then Cases
--			else 0
--			end
--		)

--	,Between24And48Hours =
--		sum(
--			case
--			when 
--					datediff(minute, DischargeTime, SignedTime) / 60 between 24 and 48
--				and StatusCode not in ('09')
--				and DocumentProgressCode in ('G','P')
--				and IsFirstDocumentForDischarge = 1
--				then Cases
--			else 0
--			end
--		)

--	,GreaterThan48Hours = 
--		sum(
--			case
--			when 
--					datediff(minute, DischargeTime, SignedTime) / 60 > 48
--				and StatusCode not in ('09') 
--				and DocumentProgressCode in ('G','P')
--				and IsFirstDocumentForDischarge = 1
--			then Cases
--			else 0
--			end
--		)
--		,Breach = 
--		sum(
--			case
--			when 
--				(
--				datediff(minute, DischargeTime, SignedTime) / 60 >= 24
--				and StatusCode not in ('09') 
--				and DocumentProgressCode in ('G','P')
--				and IsFirstDocumentForDischarge = 1
--				)
--				or
--				(
--				StatusCode not in ('09') 
--				and coalesce(DocumentProgressCode, '') not in ('N','X')
--				and RequestOrderForDischarge = 1
--				and DocumentSentGPForDischarge = 0
--				)
--				then Cases	
--			else 0
--			end
--		)
--	,[AdmissionReason] = 
--		sum(
--			case
--			when 
--					[AdmissionReason] <> ''
--				and StatusCode not in ('09')
--				and IsFirstDocumentForDischarge = 1
--				and DocumentProgressCode in ('G','P')
--				--and DocumentTypeCode in ('F', 'I')
--				and DocumentTypeCode in ('F')
--				then Cases
--			else 0
--			end
--		)

--	,[Diagnosis] = 
--		sum(
--			case
--			when 
--					[Diagnosis] <> '' 
--				and StatusCode not in ('09')
--				and IsFirstDocumentForDischarge = 1
--				and DocumentProgressCode in ('G','P')
--				--and DocumentTypeCode in ('F', 'I')
--				and DocumentTypeCode in ('F')
--				then Cases
--			else 0
--			end
--		)

--	,[Investigations] = 
--		sum(
--			case
--			when 
--					[Investigations] <> ''
--				and StatusCode not in ('09')
--				and IsFirstDocumentForDischarge = 1
--				and DocumentProgressCode in ('G','P')
--				--and DocumentTypeCode in ('F', 'I')
--				and DocumentTypeCode in ('F')
--				then Cases
--			else 0
--			end
--		)

--	,[RecommendationsOnFutureManagement] = 
--		sum(
--			case
--			when 
--					[RecommendationsOnFutureManagement] <> ''
--				and StatusCode not in ('09')
--				and IsFirstDocumentForDischarge = 1
--				and DocumentProgressCode in ('G','P')
--				--and DocumentTypeCode in ('F', 'I')
--				and DocumentTypeCode in ('F')
--				then Cases
--			else 0
--			end
--		)

--	,InformationToPatientOrRelatives = 
--		sum(
--			case
--			when 
--					InformationToPatientOrRelatives <> ''
--				and StatusCode not in ('09')
--				and IsFirstDocumentForDischarge = 1
--				and DocumentProgressCode in ('G','P')
--				--and DocumentTypeCode in ('F', 'I')
--				and DocumentTypeCode in ('F')
--				then Cases
--			else 0
--			end
--		)

--	,[ChangesToMedication] = 
--		sum(
--			case
--			when 
--					[ChangesToMedication] <> ''
--				and StatusCode not in ('09')
--				and IsFirstDocumentForDischarge = 1
--				and DocumentProgressCode in ('G','P')
--				--and DocumentTypeCode in ('F', 'I')
--				and DocumentTypeCode in ('F')
--				then Cases
--			else 0
--			end
--		) 													

--from
--	WarehouseOLAP.dbo.BaseDischargeSummary dischargesummary

--left outer join 
--	Warehouse.WH.TreatmentFunction specialty
--on 
--	specialty.SpecialtyCode = dischargesummary.NationalSpecialtyCode
	
----inner join 
----	Warehouse.WH.Directorate directorate
----on 
----	directorate.DirectorateCode = dischargesummary.EndDirectorateCode
	
--inner join 
--	Warehouse.WH.Division division
--on 
--	dischargesummary.DischargeDivisionCode = division.DivisionCode
	
--inner join
--	WarehouseOLAP.dbo.OlapCalendar calendar
--on 
--	dischargesummary.DischargeDate = calendar.TheDate
	

--where
--	(dischargesummary.DischargeDate between @start and @end)
	
--	and (dischargesummary.NationalSpecialtyCode = @specialty or @specialty = '(Select All)')
--	and (dischargesummary.DischargeDivisionCode = @division or @division = '(Select All)')
	
--	and (cast(dischargesummary.EpisodicGpPracticeIsMedisecUser as varchar(10)) = @medisecuser or @medisecuser = '(Select All)')
	
--	and (dischargesummary.PCTCode = @pct or @pct = '(Select All)')
	
--	--and dischargesummary.SpecialtyCode in (@specialty)-- (select Value from warehouse.dbo.SSRS_MultiValueParamSplit(@specialty,','))
--	--and dischargesummary.DischargeDivisionCode in (@division) -- (select Value from  warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))

--group by
--	calendar.FirstDayOfWeek
--	,dischargesummary.DischargeDivisionCode
--	,division.Division
--	,dischargesummary.NationalSpecialtyCode
--	,specialty.Specialty
--	,dischargesummary.ConsultantCode


option(recompile)

end



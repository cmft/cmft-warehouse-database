﻿


CREATE proc [dbo].[BuildFactObservationAlert]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table dbo.FactObservationAlert


insert into dbo.FactObservationAlert

(
	AlertRecno
	,SpecialtyID
	,WardID
	,TypeID
	,ReasonID
	,CreatedDate
	,CreatedTimeOfDayCode
	,BedsideDueDate
	,BedsideDueTimeOfDayCode
	,EscalationDate
	,EscalationTimeOfDayCode
	,AcceptedDate
	,AcceptedTimeOfDayCode
	,AttendedByUserID
	,AttendanceTypeID
	,CurrentSeverityID
	,CurrentClinicianSeniorityID
	,ClosedDate
	,ClosedTimeOfDayCode
	,ClosedByUserID
	,ClosureReasonID
	,DurationID
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,Duration
)

select
	 AlertRecno
	,SpecialtyID
	,WardID
	,TypeID
	,ReasonID
	,CreatedDate
	,CreatedTimeOfDayCode
	,BedsideDueDate
	,BedsideDueTimeOfDayCode
	,EscalationDate
	,EscalationTimeOfDayCode
	,AcceptedDate
	,AcceptedTimeOfDayCode
	,AttendedByUserID
	,AttendanceTypeID
	,CurrentSeverityID
	,CurrentClinicianSeniorityID
	,ClosedDate
	,ClosedTimeOfDayCode
	,ClosedByUserID
	,ClosureReasonID
	,DurationID =
					coalesce(
						case 
							when AlertDurationMinutes < 0 then -1
							when AlertDurationMinutes > 1440 then 1440
							else AlertDurationMinutes
						end
						, -2
						)
	,OutOfHours
	,AgeID
	,ContextCode
	,Cases
	,AlertDurationMinutes
from
	dbo.BaseObservationAlert


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @sproc, @Stats, @StartTime	







﻿CREATE procedure [dbo].[BuildBaseRF] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()


truncate table dbo.BaseRF

insert into dbo.BaseRF
	(
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
	,Created
	,Updated
	,ByWhom

	,AgeCode
	,Cases
	,DirectorateCode
	,PCTCode
	,ReasonForReferralCode
	)
select --top 100
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	
	,SexCode =
		case Encounter.SexCode
		when '' then '0'
		when 'M' then '1'
		when 'F' then '2'
		when 'I' then '9'
		else '#'
		end

	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode, Encounter.EpisodicGpPracticeCode, 'X99999')

	,EpisodicGpCode

	,EpisodicGpPracticeCode =
		coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredGpPracticeCode, 'X99999')

	,EpisodicGdpCode
	,SiteCode

	,ConsultantCode =
		coalesce(Encounter.ConsultantCode, 'N/A')

	,SpecialtyCode =
		coalesce(Encounter.SpecialtyCode, 'N/A')

	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
	,Created
	,Updated
	,ByWhom

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.ReferralDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.ReferralDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.ReferralDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.ReferralDate) > 1 and
	datediff(day, DateOfBirth, Encounter.ReferralDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.ReferralDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.ReferralDate) > 28 and
	datediff(month, DateOfBirth, Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.ReferralDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.ReferralDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.ReferralDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.ReferralDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.ReferralDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.ReferralDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.ReferralDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.ReferralDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.ReferralDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.ReferralDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,Cases = 1

	,DirectorateCode = coalesce(Encounter.DirectorateCode , 'N/A')
	,PCTCode
	,ReasonForReferralCode
from
	Warehouse.RF.Encounter Encounter
where
	Encounter.ReferralDate >= '1 Apr 2005'


select @RowsInserted = @@rowcount



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'BuildBaseRF', @Stats, @StartTime

﻿
CREATE PROCEDURE [dbo].[BuildBaseCOMCaseLoad] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()


TRUNCATE TABLE dbo.BaseCOMCaseLoad

-- Insert new rows into encounter destination table
INSERT INTO dbo.BaseCOMCaseLoad
(
     CaseLoadID
	,ReferralID
	,RoleTypeID
	,StatusID
	,AllocationTime
	,AllocationDate
	,AllocationReasonID
	,InterventionlevelID
	,DischargeTime
	,DischargeDate
	,Outcome
	,AllocationProfessionalCarerID
	,AllocationSpecialtyID
	,AllocationStaffTeamID
	,ResponsibleHealthOrganisation
	,DischargeReasonID
	,PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwnerID
	,AgeCode
)
SELECT
	 CaseLoadID
	,ReferralID
	,RoleTypeID
	,StatusID
	,AllocationTime
	,AllocationDate
	,AllocationReasonID
	,InterventionlevelID
	,DischargeTime
	,DischargeDate
	,Outcome
	,AllocationProfessionalCarerID
	,AllocationSpecialtyID
	,AllocationStaffTeamID
	,ResponsibleHealthOrganisation
	,DischargeReasonID
	,PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwnerID
	,AgeCode =
	case
	when datediff(day, PatientDateOfBirth, CaseLoad.AllocationDate) is null then 'Age Unknown'
	when datediff(day, PatientDateOfBirth, CaseLoad.AllocationDate) <= 0 then '00 Days'
	when datediff(day, PatientDateOfBirth, CaseLoad.AllocationDate) = 1 then '01 Day'
	when datediff(day, PatientDateOfBirth, CaseLoad.AllocationDate) > 1 and
	datediff(day, PatientDateOfBirth, CaseLoad.AllocationDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, PatientDateOfBirth, CaseLoad.AllocationDate)), 2) + ' Days'
	
	when datediff(day, PatientDateOfBirth, CaseLoad.AllocationDate) > 28 and
	datediff(month, PatientDateOfBirth, CaseLoad.AllocationDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, CaseLoad.AllocationDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, PatientDateOfBirth, CaseLoad.AllocationDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, CaseLoad.AllocationDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, PatientDateOfBirth, CaseLoad.AllocationDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, CaseLoad.AllocationDate) then 1 else 0 end > 1 and
	 datediff(month, PatientDateOfBirth, CaseLoad.AllocationDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, CaseLoad.AllocationDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, PatientDateOfBirth,CaseLoad.AllocationDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, CaseLoad.AllocationDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, PatientDateOfBirth, CaseLoad.AllocationDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, CaseLoad.AllocationDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, CaseLoad.AllocationDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, CaseLoad.AllocationDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, PatientDateOfBirth, CaseLoad.AllocationDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, CaseLoad.AllocationDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, CaseLoad.AllocationDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, CaseLoad.AllocationDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end
FROM
		warehouse.COM.CaseLoad
WHERE
		ArchiveFlag = 'N'
	AND	AllocationDate >= '01 Apr 2008'
	AND	AllocationDate < getdate()

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec WriteAuditLogEvent 'BuildOLAPBaseCOMCaseload', @Stats, @StartTime


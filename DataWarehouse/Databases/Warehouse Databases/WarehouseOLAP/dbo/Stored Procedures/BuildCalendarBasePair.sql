﻿CREATE procedure [dbo].[BuildCalendarBasePair]
(
    @FromDate varchar (20),
    @ToDate varchar (20)
)
as

insert dbo.CalendarBasePairs
(TheStartDate, TheEndDate)
values (@FromDate, @ToDate);

﻿

CREATE Procedure [dbo].[TLoadOCMCreatinineResult] as


-- 20141023	RR	Had originally done the following with a CTE, but each time the query ran the time taken to execute kept increasing, so changed to a temp table
-- May need to change to an actual table and put indexes on as volume is quite big??  Need advice from JR
-- drop table #OCMTestResult
SELECT
	 Result.ResultRecno
	,Result.SourceUniqueID
	,Result.SourcePatientNo
	,Result.SourceEpisodeNo
	,Result.MergeEpisodeNo
	,ResultAPCLinked = 
		case 
			when EpisodeNoType = 'NoAPCLinkFound' then 0
			else 1
		end
	,LatestAPCEncounterRecno = Encounter.MergeEncounterRecno
	,LatestAPCSpellNo = Encounter.SourceSpellNo
	,LatestPatientCategory = Encounter.PatientCategoryCode
	,LatestAdmissionDate = Encounter.AdmissionDate
	,LatestDischargeDate = Encounter.DischargeDate
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DistrictNo
	,Encounter.CasenoteNumber
	,Encounter.DateOfBirth
	,Encounter.RenalPatient
	,WardStay.RenalWardStay
	,Age = 
		case 
			when EpisodeNoType <> 'NoAPCLinkFound' then cast((datediff(day,Encounter.DateOfBirth,Encounter.AdmissionDate)/365.25) as int)
			else cast((datediff(day,Encounter.DateOfBirth,ResultDate)/365.25) as int)
		end
		--cast((datediff(day,DateOfBirth,EpisodeStartDate)/365.25) as int)
	,AgeCode = 
		case 
			when EpisodeNoType <> 'NoAPCLinkFound' then dbo.f_AgeCode(Encounter.DateOfBirth, Encounter.AdmissionDate)
			else dbo.f_AgeCode(Encounter.DateOfBirth, ResultDate)
		end
		--DATEDIFF(day,DateOfBirth, Encounter.EpisodeStartDate)
		 --dbo.f_AgeCode(DateOfBirth, Encounter.EpisodeStartDate)
	,Encounter.EthnicOriginCode 
	,Encounter.SexCode
	,ConsultantCode = 
		coalesce(Encounter.ConsultantCode , 'N/A')
	,SpecialtyCode = 
		coalesce(Encounter.SpecialtyCode , 'N/A')
	,WardCode = 
		coalesce(WardStay.WardCode , 'N/A')
	,ResultWardCode = 
		coalesce(ResultWardStay.WardCode , 'N/A')
	,CurrentInpatient =
		case
			when Encounter.DischargeDate is null then 1
			else 0
		end

	,LengthOfStay =
		case
			when Encounter.DischargeDate is null then datediff(day,Encounter.AdmissionDate,getdate())
			else datediff(day,Encounter.AdmissionDate,Encounter.DischargeDate)
		end
	
	,DiedInHospital = 
		case 
			when Encounter.NationalDischargeMethodCode = 4 then 'Y' 
			else null 
		end
	,Result =
		cast(Result.Result as int)

	,Result.ResultDate
	,Result.ResultTime

	,PreviousResult = cast(PreviousResult.Result as int)
	,PreviousResultTime = PreviousResult.ResultTime

	,ResultIntervalDays =
		datediff(minute , PreviousResult.ResultTime , Result.ResultTime ) / 1440.0

	,ResultChange =
		cast(Result.Result as int) - cast(PreviousResult.Result as int)

	,CurrentToPreviousResultRatio =
		cast(Result.Result as float) / cast(PreviousResult.Result as float)

	,ResultRateOfChangePer24Hr = 
		case 
		when Result.ResultTime = PreviousResult.ResultTime then 0.0
		else
			(cast(Result.Result as int) - cast(PreviousResult.Result as int))
			/
			(datediff(minute , PreviousResult.ResultTime , Result.ResultTime ) / 1440.0)
		end
	,BaselineResultRecno = 
		(
		select 
			Baseline.ResultRecno
		from
			OCMTest.Result Baseline
		where 
			Baseline.SourcePatientNo = Result.SourcePatientNo
		and Baseline.ResultRecno <> Result.ResultRecno
		and	Baseline.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
		and not exists -- lowest result in last 6 months
				(
				select
					1
				from
					OCMTest.Result LowerBaselineResult
				where
					LowerBaselineResult.SourcePatientNo = Result.SourcePatientNo
				and LowerBaselineResult.ResultRecno <> Result.ResultRecno
				and	LowerBaselineResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
				and	LowerBaselineResult.Result < Baseline.Result		
				)
		and	not exists -- latest lowest result in last 6 months
				(
				select
					1
				from
					OCMTest.Result LaterBaselineResult
				where
					LaterBaselineResult.SourcePatientNo = Result.SourcePatientNo
				and LaterBaselineResult.ResultRecno <> Result.ResultRecno
				and	LaterBaselineResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
				and	LaterBaselineResult.Result = Baseline.Result
				and	LaterBaselineResult.SequenceNo > Baseline.SequenceNo
				)
		)
	,SequenceNoPatient =
		row_number() over(partition by Result.SourcePatientNo order by Result.ResultTime)

	,SequenceNoSpell =
		case 
			when EpisodeNoType <> 'NoAPCLinkFound' 
			then
				row_number() over(partition by Result.SourcePatientNo, Result.MergeEpisodeNo order by Result.ResultTime)
			else 1
		end
	,EpisodeNoType
	
into
	#OCMTestResult
from
	OCMTest.Result Result

--latest consultant episode
inner join OCMTest.APCEncounter Encounter
on	Encounter.SourcePatientNo = Result.SourcePatientNo
and not exists
	(
	select
		1
	from
		OCMTest.APCEncounter LaterFCE
	where
		LaterFCE.SourcePatientNo = Encounter.SourcePatientNo
	and	LaterFCE.EpisodeStartTime > Encounter.EpisodeStartTime
	)

--latest ward stay
inner join OCMTest.APCWardStay WardStay
on	WardStay.SourcePatientNo = Result.SourcePatientNo
and not exists
		(
		select
			1
		from
			OCMTest.APCWardStay LaterWardStay
		where
			LaterWardStay.SourcePatientNo = WardStay.SourcePatientNo
		and	LaterWardStay.StartTime > WardStay.StartTime
		)
--Result ward stay
left join OCMTest.APCWardStay ResultWardStay
on	ResultWardStay.SourcePatientNo = Result.SourcePatientNo
and Result.ResultTime between ResultWardStay.StartTime and coalesce(ResultWardStay.EndTime,getdate())

left join
	(
	select
		 SourcePatientNo
		,SequenceNo = PreviousResult.SequenceNo + 1
		,PreviousResult.Result
		,PreviousResult.ResultTime
	from
		OCMTest.Result PreviousResult
	) PreviousResult
on	PreviousResult.SourcePatientNo = Result.SourcePatientNo
and	PreviousResult.SequenceNo = Result.SequenceNo

	


-- select ResultRecno,count(*) from #OCMTestResult group by ResultRecno having count(*)>1 -- 237 with dup records (0.04%, this is due to having 2 episodes with same start time in APCEncounter, think a dev issue
-- select * from OCMTest.APCEncounter where SourcePatientNo = 2485480 order by EpisodeStartTime desc
-- select * from #OCMTestResult where ResultRecno = 29542898

Truncate table OCMTest.TLoad
Insert into OCMTest.TLoad
	(
	ResultRecno
	,Sequence
	,SourceUniqueID
	,SourcePatientNo
	,SourceEpisodeNo
	,MergeEpisodeno
	,ResultAPCLinked
	,SequenceNoPatient
	,SequenceNoSpell
	,RenalPatient
	,RenalWardStay
	
	,LatestAPCEncounterRecno
	,LatestAPCSpellNo
	,LatestPatientCategory
	,LatestAdmissionDate
	,PatientForename
	,PatientSurname
	,DistrictNo
	,CasenoteNumber
	,AgeCode
	,EthnicOriginCode 
	,SexCode
	,ConsultantCode
	,SpecialtyCode
	,WardCode
	,CurrentInpatient
	,LengthOfStay
	,DiedInHospital
	
	,Result
	,ResultDate
	,ResultTime
	,PreviousResult
	,PreviousResultTime
	,ResultIntervalDays
	,ResultChange
	,CurrentToPreviousResultRatio
	,ResultRateOfChangePer24Hr
	,Result.BaselineResultRecno
	,BaselineResult 
	,BaselineResultTime 
	,BaselineToCurrentDays
	,BaselineToCurrentChange 
	,CurrentToBaselineResultRatio 
	,C1Result
	,RatioDescription
	,ResultForRatio
	,RVRatio	
	,NationalAlert 
	
	,DateOfBirth
	,ResultWardCode
	)
select
	Result.ResultRecno
	,Sequence = row_number() over (partition by Result.ResultRecno -- due to duplicates, think it may just be a DEV issue, but added the following just in case
									order by Result.ResultTime
									)
	,Result.SourceUniqueID
	,Result.SourcePatientNo
	,Result.SourceEpisodeNo
	,Result.MergeEpisodeNo
	,Result.ResultAPCLinked
	,Result.SequenceNoPatient
	,Result.SequenceNoSpell
	,Result.RenalPatient
	,Result.RenalWardStay
	,Result.LatestAPCEncounterRecno
	,Result.LatestAPCSpellNo
	,Result.LatestPatientCategory
	,Result.LatestAdmissionDate
	,Result.PatientForename
	,Result.PatientSurname
	,Result.DistrictNo
	,Result.CasenoteNumber

	--,Result.Age
	,Result.AgeCode
	,Result.EthnicOriginCode 
	,Result.SexCode
	,Result.ConsultantCode
	,Result.SpecialtyCode
	,Result.WardCode
	,Result.CurrentInpatient
	,Result.LengthOfStay
	,Result.DiedInHospital
	
	,Result.Result
	,Result.ResultDate
	,Result.ResultTime
	,Result.PreviousResult
	,Result.PreviousResultTime
	,Result.ResultIntervalDays
	,Result.ResultChange
	,Result.CurrentToPreviousResultRatio
	,Result.ResultRateOfChangePer24Hr
	
	,Result.BaselineResultRecno
	
	,BaselineResult = 
		case
			when Result.SequenceNoSpell = 1 then 
				case
					when Result.Result < coalesce(BaselineResult.Result,DefaultBaseline.DefaultBaselineResult) then Result.Result
					else coalesce(BaselineResult.Result,DefaultBaseline.DefaultBaselineResult,Result.Result) 
				end
			else
				case
					when DefaultBaselineResult < Result.Result 
						and
							(DefaultBaselineResult < BaselineResult.Result 
							or 
							BaselineResult.Result is null
							)
						then DefaultBaselineResult
					when BaselineResult.Result < Result.Result 
						and
							(BaselineResult.Result < DefaultBaselineResult
							or
							DefaultBaselineResult is null
							)
						then BaselineResult.Result
					else Result.Result
				end
		end
	
	,BaselineResultTime = 
		case
			when Result.SequenceNoSpell = 1 then 
				case
					when Result.Result < coalesce(BaselineResult.Result,DefaultBaseline.DefaultBaselineResult) then Result.ResultTime
					else coalesce(BaselineResult.ResultTime,DefaultBaseline.DefaultBaselineResultTime,Result.ResultTime) 
				end
			else
				case
					when DefaultBaselineResult < Result.Result 
						and
							(DefaultBaselineResult < BaselineResult.Result 
							or 
							BaselineResult.Result is null
							)
						then DefaultBaselineResultTime
					when BaselineResult.Result < Result.Result 
						and
							(BaselineResult.Result < DefaultBaselineResult
							or
							DefaultBaselineResult is null
							)
						then BaselineResult.ResultTime
					else Result.ResultTime
				end
		end
		
	,BaselineToCurrentDays =	datediff(second,
		(
		case
			when Result.SequenceNoSpell = 1 then 
				case
					when Result.Result < coalesce(BaselineResult.Result,DefaultBaseline.DefaultBaselineResult) then Result.ResultTime
					else coalesce(BaselineResult.ResultTime,DefaultBaseline.DefaultBaselineResultTime,Result.ResultTime) 
				end
			else
				case
					when DefaultBaselineResult < Result.Result 
						and
							(DefaultBaselineResult < BaselineResult.Result 
							or 
							BaselineResult.Result is null
							)
						then DefaultBaselineResultTime
					when BaselineResult.Result < Result.Result 
						and
							(BaselineResult.Result < DefaultBaselineResult
							or
							DefaultBaselineResult is null
							)
						then BaselineResult.ResultTime
					else Result.ResultTime
				end
		end
		)
		,Result.ResultTime		
		) / (24.0 * 60 * 60)	
				
	,BaselineToCurrentChange =			
		cast(Result.Result as int)		
		-		
		(
		case
			when Result.SequenceNoSpell = 1 then 
				case
					when Result.Result < coalesce(BaselineResult.Result,DefaultBaseline.DefaultBaselineResult) then Result.Result
					else coalesce(BaselineResult.Result,DefaultBaseline.DefaultBaselineResult,Result.Result) 
				end
			else
				case
					when DefaultBaselineResult < Result.Result 
						and
							(DefaultBaselineResult < BaselineResult.Result 
							or 
							BaselineResult.Result is null
							)
						then DefaultBaselineResult
					when BaselineResult.Result < Result.Result 
						and
							(BaselineResult.Result < DefaultBaselineResult
							or
							DefaultBaselineResult is null
							)
						then BaselineResult.Result
					else Result.Result
				end
		end
		)
	,CurrentToBaselineResultRatio =			
	cast(Result.Result as float)		
	/		
	cast(		
		(	
		case
			when Result.SequenceNoSpell = 1 then 
				case
					when Result.Result < coalesce(BaselineResult.Result,DefaultBaseline.DefaultBaselineResult) then Result.Result
					else coalesce(BaselineResult.Result,DefaultBaseline.DefaultBaselineResult,Result.Result) 
				end
			else
				case
					when DefaultBaselineResult < Result.Result 
						and
							(DefaultBaselineResult < BaselineResult.Result 
							or 
							BaselineResult.Result is null
							)
						then DefaultBaselineResult
					when BaselineResult.Result < Result.Result 
						and
							(BaselineResult.Result < DefaultBaselineResult
							or
							DefaultBaselineResult is null
							)
						then BaselineResult.Result
					else Result.Result
				end
		end
		) as float	
	)	
	
	,NationalRatio.C1Result
	,NationalRatio.RatioDescription
	,NationalRatio.ResultForRatio
	,NationalRatio.RVRatio	
	,NationalAlert = case
						when RatioDescription = 'Baseline' then
							case
								when C1Result < RVRatio then 'Low'
								when C1Result = RVRatio then 'NoFlag'
								else 'Suggest'
							end
						when RatioDescription in ('Lowest0-7days','Median8-365days') then
							case
								when RVRatio >=3.0 then 'AKI 3'
								when RVRatio >=1.5 then
									case
										when NationalRatio.Age <18 and NationalRatio.SexCode = 'M' and C1Result > (80*3) then 'AKI 3'
										when NationalRatio.Age <18 and NationalRatio.SexCode = 'F' and C1Result > (60*3) then 'AKI 3'
										when NationalRatio.Age >=18 and C1Result > 354 then 'AKI 3'
										when RVRatio >=2.0 then 'AKI 2'
										else 'AKI 1'
									end
								when Alert is not null then Alert
								when RatioDescription = 'Lowest0-7days' and C1Result-ResultForRatio >26 then 'No Alert Repeat'
								else 'No Alert'
							end
						else null
					end
	
	,Result.DateOfBirth
	,Result.ResultWardCode
--into OCMTest.TLoad
from 
	#OCMTestResult Result

inner join OCMTest.CreatinineNationalAlgorithm NationalRatio
on Result.ResultRecno = NationalRatio.ResultRecno

left join OCMTest.Result BaselineResult
on	BaselineResult.ResultRecno = Result.BaselineResultRecno

left join
	(Select 
		ResultRecno
		,SourcePatientNo
		,MergeEpisodeNo
		,SequenceNoSpell
		,DefaultBaselineResult = 
			case 
				when SexCode = 'M' then 80
				when SexCode = 'F' then 60
				else null
			end
		,DefaultBaselineResultTime = dateadd(month,-6,ResultTime)
	from
		#OCMTestResult
	where
		SequenceNoSpell = 1 
	and BaselineResultRecno is null
	)DefaultBaseline
on 
--Result.ResultRecno = DefaultBaseline.ResultRecno
Result.SourcePatientNo = DefaultBaseline.SourcePatientNo
and Result.MergeEpisodeNo = DefaultBaseline.MergeEpisodeNo

--Result.SourcePatientNo = DefaultBaseline.SourcePatientNo
--and Result.MergeEpisodeNo = DefaultBaseline.MergeEpisodeNo
-- order by Result.sourcePatientNo, Result.ResultTime desc



﻿


CREATE procedure [dbo].[BuildBaseOCMCreatinineResult] as

declare @cutoffDate smalldatetime = cast(dateadd(day, -731,getdate()) as date) -- 2 yrs worth, to be able to do 12 mths worth of calculations
--'30 Jul 2012'

-- 20140930 RR added criteria to pull Creatinine results only from Warehouse.OCM.CodedResult following DG update
-- 20141023 RR Brought in additional fields and updated criteria based on advice by Prasanna Hanumapura


-- Pull every Creatinine result, left join to APC to identify if the Episode relates to APC or OP
Truncate table OCMTest.Result
Insert into OCMTest.Result
	(ResultRecno
	,SourceUniqueID
	,SourcePatientNo
	,SequenceNo
	,SourceEpisodeNo
	,EpisodeNoType
	,MergeEpisodeNo 
	,APCAdmittingMergeEncounterRecno
	,Result
	,ResultDate
	,ResultTime
	,DateOfBirth
	,DistrictNo
	,Surname
	,Forenames
	,Sexcode
	,EthnicOriginCode
	,DeathIndicator
	,DateOfDeath
	)
--drop table OCMTest.Result
select
	 ResultRecno = max(CodedResult.ResultRecno)
	,SourceUniqueID = max(CodedResult.SourceUniqueID)
	,CodedResult.SourcePatientNo
	,SequenceNo = row_number() over(partition by CodedResult.SourcePatientNo order by CodedResult.ResultTime)
	,SourceEpisodeNo = min(CodedResult.SourceEpisodeNo)
	
	,EpisodeNoType =
		case
			when AEMerge.APCMergeEncounterRecno is not null 
			then 'AEMergeAEAPC'
			when APCMerge.APCMergeEncounterRecno is not null 
			then 'APCMergeAEAPC'
			when APCEncounter.MergeEncounterRecno is not null 
			then 'APC'
			else 'NoAPCLinkFound'
		end
		
	,MergeEpisodeNo =
		case
			when AEMerge.APCMergeEncounterRecno is not null 
			then AEMerge.APCEpisodeNo
			else min(CodedResult.SourceEpisodeNo) 
		end
	,APCAdmittingMergeEncounterRecno =
		case
			when AEMerge.APCMergeEncounterRecno is not null 
			then AEMerge.APCMergeEncounterRecno
			when APCMerge.APCMergeEncounterRecno is not null 
			then APCMerge.APCMergeEncounterRecno
			when min(CodedResult.SourceEpisodeNo) = APCEncounter.SourceSpellNo
			then APCEncounter.MergeEncounterRecno
			else null
		end
	,Result = cast(CodedResult.Result as int)
	,CodedResult.ResultDate
	,CodedResult.ResultTime
	,Patient.DateOfBirth
	,Patient.DistrictNo
	,Patient.Surname
	,Patient.Forenames
	,Patient.Sexcode
	,Patient.EthnicOriginCode
	,Patient.DeathIndicator
	,Patient.DateOfDeath
--into OCMTest.Result
from
	Warehouse.OCM.CodedResult

left join Warehouse.PAS.Patient
on CodedResult.SourcePatientNo = Patient.SourcePatientNo

left join WarehouseOLAPMergedV2.APC.BaseEncounterAEBaseEncounter APCMerge -- If the view is changed to pull all for spell, this will need to be updated to pull through first encounterrecno
on CodedResult.SourcePatientNo = APCMerge.APCSourcePatientNo
and CodedResult.SourceEpisodeNo = APCMerge.APCEpisodeNo

left join WarehouseOLAPMergedV2.APC.BaseEncounterAEBaseEncounter AEMerge -- If the view is changed to pull all for spell, this will need to be updated to pull through first encounterrecno
on CodedResult.SourcePatientNo = AEMerge.APCSourcePatientNo
and CodedResult.SourceEpisodeNo = (AEMerge.APCEpisodeNo - 1)

left join WarehouseOLAPMergedV2.APC.BaseEncounter APCEncounter
on CodedResult.SourcePatientNo = APCEncounter.SourcePatientNo
and CodedResult.SourceEpisodeNo = APCEncounter.SourceSpellNo
and APCEncounter.FirstEpisodeInSpellIndicator = 1
and APCEncounter.Reportable = 1

where
	CodedResult.ResultCode = 'CR' 
and	isnumeric(CodedResult.Result) = 1
and ResultDate >= @cutoffdate

group by
	CodedResult.SourcePatientNo
	,AEMerge.APCEpisodeNo 
	,APCMerge.APCEpisodeNo
	,AEMerge.APCMergeEncounterRecno 
	,APCMerge.APCMergeEncounterRecno	
	,APCEncounter.SourceSpellNo
	,APCEncounter.MergeEncounterRecno	
	,CodedResult.Result 
	,CodedResult.ResultDate
	,CodedResult.ResultTime
	,Patient.DateOfBirth
	,Patient.DistrictNo
	,Patient.Surname
	,Patient.Forenames
	,Patient.Sexcode
	,Patient.EthnicOriginCode
	,Patient.DeathIndicator
	,Patient.DateOfDeath

--CREATE INDEX [IX_CodedResult_1] ON OCMTest.Result
--(
--	 SourcePatientNo ASC
--	,ResultTime ASC
--	,SequenceNo asc
--);



--set identity_insert OCMTest.APCEncounter on

--declare @cutoffDate smalldatetime = '30 Jul 2012'
-- Pull APC data from WOMV2 where Reportable, where Central, excluding specific Specialty criteria
-- 20141107 RR taken out exclusion criteria and added a field As Exclusion criteria not applicable using national algorithm

Truncate table OCMTest.APCEncounter
Insert into OCMTest.APCEncounter
	(MergeEncounterRecno
	,SourcePatientNo
	,SourceSpellNo
	,SourceUniqueID
	,AdmissionDate
	,PatientForename
	,PatientSurname
	,DistrictNo
	,CasenoteNumber
	,DateOfBirth
	,EpisodeStartDate
	,EpisodeStartTime
	,EthnicOriginCode
	,SexCode
	,ConsultantCode
	,SpecialtyCode
	,DischargeDate
	,DischargeMethodCode
	,NationalDischargeMethodCode
	,StartWardTypeCode
	,PatientCategoryCode
	,RenalPatient
	)
select
	 Encounter.MergeEncounterRecno
	,SourcePatientNo = convert(int,Encounter.SourcePatientNo) 
	
	,Encounter.SourceSpellNo
	,Encounter.SourceUniqueID
	
	,Encounter.AdmissionDate
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DistrictNo
	,Encounter.CasenoteNumber
	,Encounter.DateOfBirth
	,Encounter.EpisodeStartDate
	,Encounter.EpisodeStartTime
	,Encounter.EthnicOriginCode
	,Encounter.SexCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.DischargeDate
	
	,Encounter.DischargeMethodCode
	,DischargeMethod.NationalDischargeMethodCode
	
	,Encounter.StartWardTypeCode
	,Encounter.PatientCategoryCode
	
	,RenalPatient = 
		case
			when exists -- any episode within the previous 365 of current episode contains 1 of the 13 specs, exclude whole spell
				(select
					1
				from 
					WarehouseOLAPMergedV2.APC.BaseEncounter Renal
				where
					Reportable = 1
				and	Encounter.SourcePatientNo = Renal.SourcePatientNo
				and datediff(day,Renal.EpisodeEndDate,Encounter.EpisodeStartDate) between 0 and 365
				and 
					Renal.SpecialtyCode in ('NEIT' , 'PNEP' , 'RT' , 'NPCH' , 'NED' , 'RTC' , 'RTL' , 'RTR','IH','IH2','IH4','IHD','IHP')
				)
			then 1
			when exists -- exclude spells where the admitting episode = 23 Renal Spec Codes
				(select
					1
				from 
					WarehouseOLAPMergedV2.APC.BaseEncounter Renal
				where
					Reportable = 1
				and	Encounter.SourcePatientNo = Renal.SourcePatientNo
				and Encounter.SourceSpellNo = Renal.SourceSpellNo
				and Renal.FirstEpisodeInSpellIndicator = 1
				and
					(
					Renal.SpecialtyCode in ('NEIT' , 'PNEP' , 'NEPH' , 'RT' , 'NPCH' , 'NED' , 'NEPD' , 'RENS' , 'RTC' , 'RTL' , 'RTR')
				or left(Renal.SpecialtyCode , 2) = 'IH'
					)
				)
			then 1
			else 0
		end

--into
--	OCMTest.APCEncounter
from
	--Warehouse.APC.Encounter
	WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

left join Warehouse.PAS.DischargeMethod  -- should we change this
on Encounter.DischargeMethodCode = DischargeMethod.DischargeMethodCode

where
	ContextCode = 'CEN||PAS'
and Reportable = 1
and	(
		Encounter.DischargeDate > @cutoffDate
	or	Encounter.DischargeDate is null
	)
-- 20140811 originally excluding Renal episodes but PH asked to exclude Spells where 
--		a) admitting episode within the spell related to Renal (23 Specialties provided)
--		b) any episode within the Spell or any spell in the last 12 months related to specific renal codes  (13 Specialty Codes provided)
--and	Encounter.SpecialtyCode not in ('NEIT' , 'PNEP' , 'NEPH' , 'RT' , 'NPCH' , 'NED' , 'NEPD' , 'RENS' , 'RTC' , 'RTL' , 'RTR')
--and	left(Encounter.SpecialtyCode , 2) != 'IH'
--and	Encounter.StartWardTypeCode not in ('TDU' , 'OMU' , 'RDU' , 'CAPD' )
--and	left(Encounter.StartWardTypeCode , 2) != 'IH'
	
	--CREATE CLUSTERED INDEX [IX_APCEncounter_1] ON OCMTest.APCEncounter
--(
--	 SourcePatientNo ASC
--	,EpisodeStartTime ASC
--);

--declare @cutoffDate smalldatetime = '30 Jul 2012'

Truncate table OCMTest.APCWardStay 
Insert into OCMTest.APCWardStay 
	(SourcePatientNo
	,WardCode
	,StartDate
	,StartTime
	,EndDate
	,EndTime
	,RenalWardStay
	)
select
	 SourcePatientNo = convert(int,SourcePatientNo)
	,WardCode
	,StartDate
	,StartTime
	,EndDate
	,EndTime
	,RenalWardStay =
		case
			when WardStay.WardCode in ('TDU' , 'OMU') then 1
			when left(WardStay.WardCode , 2) = 'IH' then 1
			else 0
		end
--into
--	OCMTest.APCWardStay
from
	WarehouseReportingMerged.APC.WardStay
	--Warehouse.APC.WardStay
where
	ContextCode = 'CEN||PAS' 
and
	(
		WardStay.EndDate > @cutoffDate
	or	WardStay.EndDate is null
	)

	
--CREATE INDEX [IX_APCWardStay_1] ON OCMTest.APCWardStay
--(
--	 SourcePatientNo ASC
--	,EndTime ASC
--)

-- 20141111 RR added following proc for national algorithm
execute [dbo].[LoadOCMCreatinineNationalAlgorithm]

--20141023 RR added in the following step to calculate the national and local Ratios
-- The original is below
execute [dbo].[TLoadOCMCreatinineResult]

Truncate table dbo.BaseOCMCreatinineResult
Insert into dbo.BaseOCMCreatinineResult
(
	 ResultRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEpisodeNo
	,MergeEpisodeNo
	,ResultAPCLinked
	,SequenceNoPatient 
	,SequenceNoSpell
	,RenalPatient
	,RenalWardStay
	
	,LatestAPCEncounterRecno
	,LatestAPCSpellNo
	,LatestPatientCategory
	,LatestAdmissionDate
	,PatientForename
	,PatientSurname
	,DistrictNo
	,CasenoteNumber
	,AgeCode
	,EthnicOriginCode
	,SexCode
	,ConsultantCode
	,SpecialtyCode
	,WardCode
	,CurrentInpatient
	,LengthOfStay
	,DiedInHospital
	
	,Result
	,ResultDate
	,ResultTime
	,PreviousResult
	,PreviousResultTime
	,ResultIntervalDays
	,ResultChange
	,CurrentToPreviousResultRatio
	,ResultRateOfChangePer24Hr
	
	,BaselineResultRecno
	,BaselineResult
	,BaselineResultTime
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio
	
	,C1Result
	,RatioDescription
	,ResultForRatio
	,RVRatio	
	,NationalAlert 
	,InHospitalAKI
	,DateOfBirth
	,ResultWardCode
)
select
	 ResultRecno
	,SourceUniqueID
	,TLoad.SourcePatientNo
	,SourceEpisodeNo
	,TLoad.MergeEpisodeNo
	,ResultAPCLinked
	,SequenceNoPatient 
	,SequenceNoSpell
	,RenalPatient
	,RenalWardStay
	
	,LatestAPCEncounterRecno
	,LatestAPCSpellNo
	,LatestPatientCategory
	,LatestAdmissionDate
	,PatientForename
	,PatientSurname
	,DistrictNo
	,CasenoteNumber
	,AgeCode
	,EthnicOriginCode
	,SexCode
	,ConsultantCode
	,SpecialtyCode
	,WardCode
	,CurrentInpatient
	,LengthOfStay
	,DiedInHospital
	
	,Result
	,ResultDate
	,ResultTime
	,PreviousResult
	,PreviousResultTime
	,ResultIntervalDays
	,ResultChange
	,CurrentToPreviousResultRatio
	,ResultRateOfChangePer24Hr
	
	,BaselineResultRecno
	,BaselineResult
	,BaselineResultTime
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio
	
	,C1Result
	,RatioDescription
	,ResultForRatio
	,RVRatio	
	,NationalAlert 
	,InHospitalAKI = case
						when InHospitalAKI.SourcePatientNo is not null then 'Yes'
						else 'No'
					end
	,DateOfBirth
	,ResultWardCode
	--341866
from
	OCMTest.TLoad

left join
	(
	select 
		Admission.SourcePatientNo
		,Admission.MergeEpisodeNo
	from
		(
		select 
			SourcePatientNo
			,MergeEpisodeNo
		from 
			OCMTest.TLoad
		where
			ResultAPCLinked = 1
		and SequenceNoSpell = 1
		and CurrentToBaselineResultRatio <1.5
		) Admission
	inner join
		(
		select 
			SourcePatientNo
			,MergeEpisodeNo
			,SequenceNoSpell
		from 
			OCMTest.TLoad
		where
			ResultAPCLinked = 1
		and CurrentToBaselineResultRatio >=1.5
		) AKI	
	on Admission.SourcePatientNo = AKI.SourcePatientNo
	and Admission.MergeEpisodeNo = AKI.MergeEpisodeNo
	and not exists
		(select
			1
		from 
			OCMTest.TLoad Earliest
		where
			ResultAPCLinked = 1
		and CurrentToBaselineResultRatio >=1.5
		and Earliest.SourcePatientNo = AKI.SourcePatientNo
		and Earliest.MergeEpisodeNo = AKI.MergeEpisodeNo
		and Earliest.SequenceNoSpell < AKI.SequenceNoSpell
		)
	) InHospitalAKI
on TLoad.SourcePatientNo = InHospitalAKI.SourcePatientNo
and TLoad.MergeEpisodeNo = InHospitalAKI.MergeEpisodeNo
where
	(TLoad.AgeCode = '99+'
	or
	(TLoad.AgeCode like '%Years%'
	and	left(TLoad.AgeCode , 2) between '18' and '99'
	)
	)
and RenalPatient = 0
and Sequence = 1

--341,866


--	(select
--		1
--	from
--		OCMTest.TLoad RenalWard
--	where
--		RenalWard.SourcePatientNo = TLoad.SourcePatientNo
--	and RenalWard.RenalWardStay = 1
--	)	

-- 423,543
-- 342,829	
-- 325,546
	
--drop table OCMTest.Result
--drop table OCMTest.APCEncounter
--drop table OCMTest.APCWardStay
--drop table OCMTest.TLoad



/* 20141023 RR took the following out
It was in the original version, but changed it to a separate proc

Truncate table OCMTest.TLoad
Insert into OCMTest.TLoad
select
	 Result.ResultRecno
	,Result.SourceUniqueID
	,Result.APCEncounterRecno
	,Result.PatientCategoryCode
	,Result.AdmissionDate
	,Result.PatientForename
	,Result.PatientSurname
	,Result.DistrictNo
	,Result.CasenoteNumber
	,Result.DateOfBirth
	,Result.AgeCode
	,Result.EthnicOriginCode 
	,Result.SexCode
	,Result.ConsultantCode
	,Result.SpecialtyCode
	,Result.WardCode
	,Result.Result
	,Result.ResultDate
	,Result.ResultTime
	,Result.SequenceNo
	,Result.PreviousResult
	,Result.PreviousResultTime
	,Result.ResultIntervalDays
	,Result.ResultChange
	,Result.CurrentToPreviousResultRatio
	,Result.ResultRateOfChangePer24Hr
	,Result.CurrentInpatient
	,LengthOfStay = cast(null as int)
	,DiedInHospital = cast(null as char)
	,[PreviousSpell(12mths)WithRenalEpisode] = cast(null as char)

	,Result.BaselineResultRecno

	,BaselineResult =
		coalesce(
			 cast(BaselineResult.Result as int)
			,case
			when Result.SexCode = 'M' then 80
			else 60
			end
		)

	,BaselineResultTime =
		coalesce(
			 BaselineResult.ResultTime
			,dateadd(m , -6 , Result.ResultTime)
		)

	,BaselineToCurrentDays =
		datediff(
			 second
			,coalesce(
				 BaselineResult.ResultTime
				,dateadd(m , -6 , Result.ResultTime)
			)
			,Result.ResultTime
		) / (24.0 * 60 * 60)

	,BaselineToCurrentChange =
		cast(Result.Result as int)
		-
		coalesce(
			 cast(BaselineResult.Result as int)
			,case
			when Result.SexCode = 'M' then 80
			else 60
			end
		)

	,CurrentToBaselineResultRatio =
		cast(Result.Result as float)
		/
		cast(
			coalesce(
				 cast(BaselineResult.Result as int)
				,case
				when Result.SexCode = 'M' then 80
				else 60
				end
			) as float
		)
--into
--	Sandbox.RachelRoyston.AKIOriginalRefresh

from
	(
	SELECT
		 Result.ResultRecno
		,Result.SourceUniqueID

		,APCEncounterRecno =
			Encounter.EncounterRecno

		,Encounter.PatientCategoryCode

		,Encounter.AdmissionDate
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DistrictNo
		,Encounter.CasenoteNumber
		,Encounter.DateOfBirth
	
		,AgeCode = --DATEDIFF(day,DateOfBirth, Encounter.EpisodeStartDate)
			 dbo.f_AgeCode(DateOfBirth, Encounter.EpisodeStartDate)
	
		,Encounter.EthnicOriginCode 
		,Encounter.SexCode

		,ConsultantCode =
			coalesce(Encounter.ConsultantCode , 'N/A')

		,SpecialtyCode =
			coalesce(Encounter.SpecialtyCode , 'N/A')

		,WardCode = coalesce(WardStay.WardCode , 'N/A')

		,Result =
			cast(Result.Result as int)

		,Result.ResultDate
		,Result.ResultTime

		,SequenceNo =
			row_number() over(partition by Result.SourcePatientNo order by Result.ResultTime)

		,PreviousResult = cast(PreviousResult.Result as int)
		,PreviousResultTime = PreviousResult.ResultTime

		,ResultIntervalDays =
			datediff(minute , PreviousResult.ResultTime , Result.ResultTime ) / 1440.0

		,ResultChange =
			cast(Result.Result as int) - cast(PreviousResult.Result as int)

		,CurrentToPreviousResultRatio =
			cast(Result.Result as float) / cast(PreviousResult.Result as float)

		,ResultRateOfChangePer24Hr = 
			case 
			when Result.ResultTime = PreviousResult.ResultTime then 0.0
			else
				(cast(Result.Result as int) - cast(PreviousResult.Result as int))
				/
				(datediff(minute , PreviousResult.ResultTime , Result.ResultTime ) / 1440.0)
			end

		,CurrentInpatient =
			case
			when Encounter.DischargeDate is null then 1
			else 0
			end

		,BaselineResultRecno = 
			(
			select
				BaselineResultRecno =
					case
					when
							BaselineResult.ResultRecno = Result.ResultRecno
						and	(
							select
								count(*)
							from
								OCMTest.Result PreviousResult
							where
								PreviousResult.SourcePatientNo = Result.SourcePatientNo
							and	PreviousResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
							and	PreviousResult.ResultRecno != Result.ResultRecno
							) = 0
						then null
					else BaselineResult.ResultRecno
					end
			from
				OCMTest.Result BaselineResult
			where
				BaselineResult.SourcePatientNo = Result.SourcePatientNo
			and	BaselineResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
			and	not exists
					(
					select
						1
					from
						OCMTest.Result LowerBaselineResult
					where
						LowerBaselineResult.SourcePatientNo = Result.SourcePatientNo
					and	LowerBaselineResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
					and	LowerBaselineResult.Result < BaselineResult.Result		
					)
			and	not exists
					(
					select
						1
					from
						OCMTest.Result LaterBaselineResult
					where
						LaterBaselineResult.SourcePatientNo = Result.SourcePatientNo
					and	LaterBaselineResult.ResultTime between dateadd(month , -6 , Result.ResultTime) and Result.ResultTime
					and	LaterBaselineResult.Result = BaselineResult.Result
					and	LaterBaselineResult.SequenceNo > BaselineResult.SequenceNo
					)
			)

	from
		OCMTest.Result Result

	left join
		(
		select
			 SourcePatientNo

			,SequenceNo =
				PreviousResult.SequenceNo + 1

			,PreviousResult.Result
			,PreviousResult.ResultTime
		from
			OCMTest.Result PreviousResult
		) PreviousResult
	on	PreviousResult.SourcePatientNo = Result.SourcePatientNo
	and	PreviousResult.SequenceNo = Result.SequenceNo
		
	--latest consultant episode
	inner join OCMTest.APCEncounter Encounter
	on	Encounter.SourcePatientNo = Result.SourcePatientNo
	and not exists
			(
			select
				1
			from
				OCMTest.APCEncounter LaterFCE
			where
				LaterFCE.SourcePatientNo = Encounter.SourcePatientNo
			and	LaterFCE.EpisodeStartTime > Encounter.EpisodeStartTime
			)

	--latest ward stay
	left join OCMTest.APCWardStay WardStay
	on	WardStay.SourcePatientNo = Result.SourcePatientNo
	and not exists
			(
			select
				1
			from
				OCMTest.APCWardStay LaterWardStay
			where
				LaterWardStay.SourcePatientNo = WardStay.SourcePatientNo
			and	LaterWardStay.StartTime > WardStay.StartTime
			)

	) Result

left join OCMTest.Result BaselineResult
on	BaselineResult.ResultRecno = Result.BaselineResultRecno 

*/

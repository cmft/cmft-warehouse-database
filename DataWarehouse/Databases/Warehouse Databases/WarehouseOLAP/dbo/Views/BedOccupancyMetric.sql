﻿create view [dbo].[BedOccupancyMetric] as

select
	 MetricCode = 'DAYCASE'
	,Metric = 'Daycase'

union all

select
	 MetricCode = 'ZEROLOS'
	,Metric = 'Zero LOS'

union all

select
	 MetricCode = 'NIGHT'
	,Metric = 'Stayed Overnight'

union all

select
	 MetricCode = 'MISSSTAY'
	,Metric = 'Unmatched Stays'

union all

select
	 MetricCode = 'MISSEPI'
	,Metric = 'Unmatched Episodes'

union all

select
	 MetricCode = 'WARDSTAY'
	,Metric = 'Ward Stay'

union all

select
	 MetricCode = 'ADMIT'
	,Metric = 'Admission'

union all

select
	 MetricCode = 'DISCH'
	,Metric = 'Discharge'

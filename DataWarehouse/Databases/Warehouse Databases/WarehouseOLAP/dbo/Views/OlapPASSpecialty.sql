﻿

CREATE VIEW dbo.OlapPASSpecialty as

select
	 SpecialtyCode = left(PASSpecialty.SpecialtyCode , 10)
	,Specialty = PASSpecialty.Specialty + ' (' + PASSpecialty.SpecialtyCode + ')'
	,NationalSpecialtyCode = COALESCE (
										 PASSpecialty.NationalSpecialtyCode
										,'N/A'
									  )
	,NationalSpecialty = COALESCE (
									WHSpecialty.SpecialtyCode + ' - ' + WHSpecialty.Specialty
									,'N/A'
								  )
	,PASSpecialty.TreatmentFunctionFlag
from
	Warehouse.PAS.Specialty PASSpecialty

left join Warehouse.WH.TreatmentFunction WHSpecialty
on	WHSpecialty.SpecialtyCode = PASSpecialty.NationalSpecialtyCode


union all

select distinct
	 Fact.SpecialtyCode
	,Consultant =
		Fact.SpecialtyCode + ' - No Description'

	,NationalSpecialtyCode =
		'N/A'

	,NationalSpecialty =
		'N/A'

	,TreatmentFunctionFlag =
		'False'

from
	dbo.FactPathway Fact
where
	not exists
	(
	select
		1
	from
		Warehouse.PAS.Specialty PASSpecialty
	where
		PASSpecialty.SpecialtyCode = Fact.SpecialtyCode
	)

union all

select
	 'N/A'
	,'N/A'
	,'N/A'
	,'N/A'
	,'False'








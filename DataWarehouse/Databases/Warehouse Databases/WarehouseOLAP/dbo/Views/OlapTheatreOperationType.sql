﻿create view [dbo].[OlapTheatreOperationType] as

select
	 TheatreOperationTypeCode = EntityCode
	,TheatreOperationType = Description
from
	dbo.EntityLookup
where
	EntityTypeCode = 'THEATREOPERATIONTYPE'

﻿


CREATE VIEW [dbo].[OlapCOMProfessionalCarer] 

AS

SELECT
		 ProfessionalCarerID
		,ProfessionalCarerCode
		,FullName
FROM 
	(
		SELECT 
			 ProfessionalCarerID
			,ProfessionalCarerCode
			,FullName = LTRIM(FullName)
		FROM 
			Warehouse.COM.ProfessionalCarer
	) ProfessionalCarer


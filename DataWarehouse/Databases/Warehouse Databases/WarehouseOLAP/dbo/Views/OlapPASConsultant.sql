﻿
CREATE view [dbo].[OlapPASConsultant] as

select
	 PASConsultant.ConsultantCode
	,Consultant =
		PASConsultant.Surname + 
		coalesce(' ' + PASConsultant.Initials, '') +
		coalesce(' ' + PASConsultant.Title, '')
		+
		case
		when exists
			(
			select
				1
			from
				Warehouse.PAS.Consultant PC
			where
				PC.Consultant = PASConsultant.Consultant
			and	PC.ConsultantCode != PASConsultant.ConsultantCode
			)
		then ' (' + PASConsultant.ConsultantCode + ')'
		else ''
		end

	,NationalConsultantCode =
		coalesce(
			 PASConsultant.NationalConsultantCode
			,'N/A'
		)

	,SpecialtyCode =
		coalesce(
			Specialty.SpecialtyCode
			,InvalidSpecialty.SpecialtyCode
		)

	,Specialty =
		coalesce(
			 Specialty.Specialty
			,InvalidSpecialty.Specialty
		)
	,DomainLogin	

	,NationalSpecialtyCode =
		COALESCE(
			 NationalSpecialty.NationalSpecialtyCode
			,'N/A'
		)

	,NationalSpecialty =
		COALESCE (
			 NationalSpecialty.NationalSpecialtyCode + ' - ' + NationalSpecialty.NationalSpecialty
			,'N/A'
		)

	,SpecialtySectorCode =
		COALESCE (
			 SpecialtySectorMap.XrefEntityCode
			,'99'
		)

from
	Warehouse.PAS.Consultant PASConsultant

left join dbo.PASSpecialty Specialty
on	Specialty.SpecialtyCode = coalesce(PASConsultant.MainSpecialtyCode, 'N/A')

inner join dbo.PASSpecialty InvalidSpecialty
on	InvalidSpecialty.SpecialtyCode = 'N/A'

left join Warehouse.WH.TreatmentFunction WHSpecialty
on	WHSpecialty.SpecialtyCode = Specialty.NationalSpecialtyCode

left join Warehouse.PAS.SpecialtyMap NationalSpecialty
on	NationalSpecialty.SpecialtyCode = PASConsultant.MainSpecialtyCode

left join dbo.EntityXref SpecialtySectorMap
on	SpecialtySectorMap.EntityTypeCode = 'NATIONALSPECIALTY'
and	SpecialtySectorMap.XrefEntityTypeCode = 'KH03SECTOR'
and	SpecialtySectorMap.EntityCode = NationalSpecialty.NationalSpecialtyCode


--turn on when FactPathway is a table
--where
--	exists
--	(
--	select
--		1
--	from
--		dbo.FactPathway
--	where
--		FactPathway.ConsultantCode = PASConsultant.ConsultantCode
--	)

union all

select distinct
	 Fact.ConsultantCode
	,Consultant =
		Fact.ConsultantCode + ' - No Description'

	,NationalConsultantCode =
		'N/A'

	,SpecialtyCode =
		'N/A'

	,Specialty =
		'N/A'
	,DomainLogin = 
		'N/A'

	,NationalSpecialtyCode =
		'N/A'

	,NationalSpecialty =
		'N/A'

	,SpecialtySectorCode =
		'99'
from
	dbo.FactPathway Fact
where
	not exists
	(
	select
		1
	from
		Warehouse.PAS.Consultant PASConsultant
	where
		PASConsultant.ConsultantCode = Fact.ConsultantCode
	)
and	Fact.ConsultantCode <> 'N/A'


union all

select distinct
	 ConsultantCode = 'N/A'
	,Consultant = 'N/A'
	,NationalConsultantCode = 'N/A'
	,SpecialtyCode = 'N/A'
	,Specialty = 'N/A'
	,DomainLogin = 'N/A'

	,NationalSpecialtyCode =
		'N/A'

	,NationalSpecialty =
		'N/A'

	,SpecialtySectorCode =
		'99'







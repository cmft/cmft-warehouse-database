﻿
CREATE VIEW [dbo].[OlapCOMSex]

AS

SELECT 
	 SexSourceUniqueID = LookupID
	,SexCode = CASE
					WHEN LookupID = '3678' THEN 'NOTSPEC'
					ELSE
							LookupCode
				END
	,Sex = CASE
					WHEN LookupID = '3678' THEN 'Not Specified'
					ELSE
							LookupDescription
				END
	,NHSCode = LookupNHSCode
	,NationalCode = ISNULL (lookupNatCode,'99')
	,CDSCode = ISNULL (LookupNHSCode,LookupCDSCode)
	,NationalSexCode = Null
	,NationalSex = NULL
FROM 
	warehouse.COM.LookupBase
WHERE 
	LookupTypeID= 'SEXXX'
AND ArchiveFlag = 'N'

UNION ALL

SELECT
	 -1
	,'NOTSPEC'
	,'Not Specified'
	,9
	,'99'
	,9
	,Null
	,NULL

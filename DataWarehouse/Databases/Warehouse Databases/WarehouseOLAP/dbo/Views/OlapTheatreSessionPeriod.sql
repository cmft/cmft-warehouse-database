﻿CREATE view [dbo].[OlapTheatreSessionPeriod] as

select
	 TheatreSessionPeriodCode = EntityCode
	,TheatreSessionPeriod = Description
from
	dbo.EntityLookup
where
	EntityTypeCode = 'THEATRESESSIONPERIOD'

﻿create view [dbo].[OlapCategory] as

select
	 Category.CategoryCode
	,Category.Category
	,Category.ParentCategoryCode
from
	dbo.CategoryBase Category

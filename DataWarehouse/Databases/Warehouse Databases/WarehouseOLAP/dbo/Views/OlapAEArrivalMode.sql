﻿

-- Find 'Warehouse.' and replace with 'Warehouse.'

CREATE view [dbo].[OlapAEArrivalMode] as

select
	 ArrivalMode.ArrivalModeCode
	,ArrivalMode =
		ArrivalMode.ArrivalMode +
			case
			when exists
				(
				select
					1
				from
					Warehouse.AE.ArrivalMode DuplicateArrivalMode
				where
					DuplicateArrivalMode.ArrivalMode in
					(
					select
						ArrivalMode.ArrivalMode
					from
						Warehouse.AE.ArrivalMode ArrivalMode
					where
						exists
						(
						select
							1
						from
							dbo.FactAE FactAE
						where
							FactAE.ArrivalModeCode = ArrivalMode.ArrivalModeCode
						)

					group by
						ArrivalMode.ArrivalMode
					having
						count(*) > 1
					)
				and	DuplicateArrivalMode.ArrivalModeCode = ArrivalMode.ArrivalModeCode
				)
			then ' (' + convert(varchar, ArrivalMode.ArrivalModeCode) + ')'
			else ''
			end
from
	Warehouse.AE.ArrivalMode ArrivalMode
where
	exists
	(
	select
		1
	from
		dbo.FactAE FactAE
	where
		FactAE.ArrivalModeCode = ArrivalMode.ArrivalModeCode
	)

union all

select distinct
	 ArrivalModeCode
	,ArrivalMode = 
		case
		when FactAE.ArrivalModeCode = 0
		then 'N/A'
		else convert(varchar, FactAE.ArrivalModeCode) + ' - No Description'
		end
from
	dbo.FactAE
where
	not exists
	(
	select
		1
	from
		Warehouse.AE.ArrivalMode ArrivalMode
	where
		ArrivalMode.ArrivalModeCode = FactAE.ArrivalModeCode
	)


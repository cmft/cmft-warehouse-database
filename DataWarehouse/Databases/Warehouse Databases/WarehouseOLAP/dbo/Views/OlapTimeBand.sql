﻿

CREATE view [dbo].[OlapTimeBand] as

select
	 TimeBandCode
	,MinuteBand
	,FiveMinuteBand
	,FifteenMinuteBand
	,ThirtyMinuteBand
	,HourBand
from
	Warehouse.WH.TimeBand



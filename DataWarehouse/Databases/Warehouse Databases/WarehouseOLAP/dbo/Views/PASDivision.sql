﻿

CREATE view [dbo].[PASDivision] as

select
	 Division.DivisionCode
	,Division.Division
from
	Warehouse.WH.Division Division


union all

select distinct
	 DivisionCode = Fact.AdmissionDivisionCode
	,Division =
		Fact.AdmissionDivisionCode + ' - No Description'
from
	dbo.FactPathway Fact
where
	not exists
	(
	select
		1
	from
		Warehouse.WH.Division Division
	where
		Division.DivisionCode = Fact.AdmissionDivisionCode
	)



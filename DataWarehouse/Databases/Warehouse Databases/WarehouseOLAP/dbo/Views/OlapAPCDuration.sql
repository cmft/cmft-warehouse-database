﻿
create view [dbo].[OlapAPCDuration] as

SELECT
	 DurationCode
	,OccupiedBedDaysCode
FROM
	WarehouseOLAP.dbo.APCDurationBase

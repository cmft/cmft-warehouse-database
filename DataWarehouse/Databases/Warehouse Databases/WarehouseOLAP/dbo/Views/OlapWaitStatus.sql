﻿create view [dbo].[OlapWaitStatus] as

select
	 WaitStatus.WaitStatusCode
	,WaitStatus.WaitStatus
	,WaitStatus.ParentWaitStatusCode
from
	dbo.WaitStatusBase WaitStatus

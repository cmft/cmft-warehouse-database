﻿

CREATE VIEW dbo.OlapCOMDischargeDestination

AS

SELECT 
	 SourceUniqueID = LookupID
	,DischargeDestinationCode = 	LookupCode
	,DischargeDestination = 	LookupDescription
	,DischargeDestinationNHSCode = LookupNHSCode
	,DischargeDestinationNationalCode = lookupNatCode
	,DischargeDestinationCDSCode = LookupCDSCode
	,NationalDischargeDestinationCode = Null
	,NationalDischargeDestination = NULL
--	,lookuptypeid
FROM 
	warehouse.COM.LookupBase
WHERE 
	LookupTypeID = 'DISDE'
AND ArchiveFlag = 'N'

UNION ALL

SELECT
	 -1
	,'NOTSPEC'
	,'Not Specified'
	,9
	,'99'
	,9
	,Null
	,NULL


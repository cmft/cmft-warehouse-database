﻿


CREATE view [dbo].[OlapObservationObservationSet]
as
SELECT [ObservationSetRecno]
      ,[SourceUniqueID]
      ,[CasenoteNumber]
      ,[DateOfBirth]
      --,[SpecialtyCode]
      --,[WardCode]
      --,[ReplacedSourceUniqueID]
      --,[CurrentObservationSetFlag]
      --,[AdditionalObservationSetFlag]
      --,[AdmissionSourceUniqueID]
      --,[EarlyWarningScoreRegimeApplicationID]
      --,[ObservationProfileApplicationID]
      --,[ClinicianPresentSeniorityCode]
      ,[StartDate]
      ,[StartTime]
      --,[StartTimeOfDay]
      ,[TakenDate]
      ,[TakenTime]
      --,[TakenTimeOfDay]
      ,[OverallRiskIndexCode]
      --,[OverallAssessedStatusCode]
      --,[AlertSeverityCode]
      ,[DueTime]
      --,[DueTimeStatusCode]
      --,[DueTimeCreatedBySourceUniqueID]
      --,[OutOfHoursCode]
      --,[AgeCode]
      --,[AlertChainID]
      --,[IsFirstInAlertChain]
      --,[IsLastInAlertChain]
      --,[AlertChainDurationMinutes]
      --,[ContextCode]
      --,[Cases]
  FROM [dbo].[BaseObservationObservationSet]

union all
	
SELECT 
	[ObservationSetRecno] = -1
      ,[SourceUniqueID] = -1
      ,[CasenoteNumber] = 'N/A'
      ,[DateOfBirth] = '1 jan 1900'
      --,[SpecialtyCode]
      --,[WardCode]
      --,[ReplacedSourceUniqueID]
      --,[CurrentObservationSetFlag]
      --,[AdditionalObservationSetFlag]
      --,[AdmissionSourceUniqueID]
      --,[EarlyWarningScoreRegimeApplicationID]
      --,[ObservationProfileApplicationID]
      --,[ClinicianPresentSeniorityCode]
      ,[StartDate] = '1 jan 1900'
      ,[StartTime] = '1 jan 1900'
      --,[StartTimeOfDay]
      ,[TakenDate] = '1 jan 1900'
      ,[TakenTime] = '1 jan 1900'
      --,[TakenTimeOfDay]
      ,[OverallRiskIndexCode] = -1
      --,[OverallAssessedStatusCode]
      --,[AlertSeverityCode]
      ,[DueTime] = '1 jan 1900'
      --,[DueTimeStatusCode]
      --,[DueTimeCreatedBySourceUniqueID]
      --,[OutOfHoursCode]
      --,[AgeCode]
      --,[AlertChainID]
      --,[IsFirstInAlertChain]
      --,[IsLastInAlertChain]
      --,[AlertChainDurationMinutes]
      --,[ContextCode]
      --,[Cases]





















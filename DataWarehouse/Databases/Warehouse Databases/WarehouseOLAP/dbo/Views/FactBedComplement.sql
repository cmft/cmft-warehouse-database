﻿

CREATE view [dbo].[FactBedComplement] as


select
	 BaseBedComplement.BaseBedComplementRecno
	,TheDate
	,WardCode
	,DivisionCode
	,SpecialtyGroupID
	,WardTypeID
	,SpecialtySectorCode = coalesce(SpecialtySectorMap.XrefEntityCode, '99')
	,MetricCode
	,AvailableBeds
from
(
	select
		 BaseBedComplement.BaseBedComplementRecno
		,TheDate
		,WardCode
		,DivisionCode
		,SpecialtyGroupID
		,WardTypeID

		,MetricCode = 'DAYCASE'

		,AvailableBeds = DaycaseBeds
	from
		dbo.BaseBedComplement
	where
		DaycaseBeds > 0

	union all

	select
		 BaseBedComplement.BaseBedComplementRecno
		,TheDate
		,WardCode
		,DivisionCode
		,SpecialtyGroupID
		,WardTypeID

		,MetricCode = 'NIGHT'

		,AvailableBeds = InpatientBeds
	from
		dbo.BaseBedComplement
	where
		InpatientBeds > 0

	) BaseBedComplement

left join dbo.EntityXref SpecialtySectorMap
on	SpecialtySectorMap.EntityTypeCode = 'SPECIALTYGROUP'
and	SpecialtySectorMap.XrefEntityTypeCode = 'KH03SECTOR'
and	SpecialtySectorMap.EntityCode = BaseBedComplement.SpecialtyGroupID


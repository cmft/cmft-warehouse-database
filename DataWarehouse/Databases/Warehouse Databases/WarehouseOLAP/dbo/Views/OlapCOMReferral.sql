﻿

CREATE VIEW [dbo].[OlapCOMReferral]

AS

SELECT DISTINCT
	 SourceUniqueID
	--,ReferredBySpecialtyID
	--,ReferredToSpecialtyID
	--,SourceofReferralID
	--,ReferralReceivedDate
	--,ReasonforReferralID
	--,CancelledDate
	--,CancelledReasonID
	--,ReferralSentDate
	,Urgency
	,Priority
	--,AuthoriedDate
	--,ReferralClosedDate
	--,ClosureReasonID
	--,ReferredByProfessionalCarerID
	--,ReferredByStaffTeamID
	--,ReferredByHealthOrgID
	--,ReferredToProfessionalCarerID
	--,ReferredToHealthOrgID
	--,ReferredToStaffTeamID
	--,OutcomeOfReferral
	--,ReferralStatus
	--,RejectionID
	--,TypeofReferralID
	,PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	--,PatientTitleID
	,PatientForename
	,PatientSurname
	--,PatientAddress1
	--,PatientAddress2
	--,PatientAddress3
	--,PatientAddress4
	--,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	--,PatientSexID
	--,PatientEthnicGroupID
	--,PatientCurrentRegisteredPracticeCode
	--,PatientEncounterRegisteredPracticeCode
	--,PatientCurrentRegisteredGPCode
	--,PatientEncounterRegisteredGPCode
	,RTTStatusDate
	,RTTPatientPathwayID
	,RTTStartTime
	,RTTStartFlag
	--,RTTStatusID
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	--,ArchiveFlag
	--,ParentID
	--,HealthOrgOwner
	--,RecordStatus
FROM WarehouseOLAP.dbo.BaseCOMReferral

﻿



CREATE VIEW [dbo].[OlapCOMLocation] 

AS

SELECT

		 LocationCode
		 
		,LocationClinic
		
		,Location
		
		,LocationType

FROM (

	SELECT 
		 
		 LocationCode
		 
		,LocationClinic
		
		,Location
		
		,LocationType

	FROM Warehouse.COM.Location

	) Location




﻿

CREATE VIEW [dbo].[OlapCOMReligion] 

AS

select 
	 ReligionStatusID = Religion.LookupID
	,ReligionStatusCode = Religion.LookupCode
	,ReligionStatus  = Religion.LookupDescription

from
	warehouse.com.LookupBase Religion

where 
	LookupTypeID = 'RELIG'
and 
(
	exists (
		select
			1
		from warehouse.com.encounter	
		where PatientReligionid = Religion.Lookupid
	)
or
	exists (
		select
			1
		from warehouse.com.Wait	
		where PatientReligionid = Religion.Lookupid
	)
)



--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
--SELECT DISTINCT
--	 ReligionStatusID = Religion.LookupID
--	,ReligionStatusCode = Religion.LookupCode
--	,ReligionStatus  = Religion.LookupDescription
--	,Religion.

--FROM
--	warehouse.COM.Encounter
--INNER JOIN warehouse.com.LookupBase Religion
--	ON Encounter.PatientReligionID = Religion.LookupID

--union

--SELECT DISTINCT
--	 ReligionStatusID = Religion.LookupID
--	,ReligionStatusCode = Religion.LookupCode
--	,ReligionStatus  = Religion.LookupDescription

--FROM
--	warehouse.COM.Wait
--INNER JOIN warehouse.com.LookupBase Religion
--	ON Wait.PatientReligionID = Religion.LookupID




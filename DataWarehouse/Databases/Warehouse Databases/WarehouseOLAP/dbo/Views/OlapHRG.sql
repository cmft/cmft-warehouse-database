﻿

CREATE view [dbo].[OlapHRG] as

select
	 HRG.HRGCode
	,HRG =
		HRG.HRGCode + ' - ' + HRG.HRG

	,HRGChapterCode =
		left(HRG.HRGCode, 1)

	,HRGChapter =
		left(HRG.HRGCode, 1)

from
	Warehouse.WH.HRG HRG

union all

select
	'ZZZ',
	'N/A',
	'#',
	'N/A'



﻿


CREATE view [dbo].[FactObservationObservation] as

select --top 100
	FactObservationObservationSet.ObservationSetRecno
	,Observation.ObservationRecno
	,FactObservationObservationSet.SpecialtyID
	,FactObservationObservationSet.WardID
	,FactObservationObservationSet.StartDate
	,FactObservationObservationSet.StartTimeOfDayCode
	,FactObservationObservationSet.DueTimeStatusID
	,FactObservationObservationSet.ClinicianPresentSeniorityID
	,FactObservationObservationSet.OverallRiskIndexCode
	,FactObservationObservationSet.OverallAssessedStatusID
	,FactObservationObservationSet.OutOfHours
	,FactObservationObservationSet.AgeID
	,ObservationMeasureID = 
				coalesce(
					Observation.ObservationMeasureID
					,-1
				)
	,FactObservationObservationSet.ContextCode
	,Observation.Result
	,Observations = 1
from
	 WarehouseOLAP.dbo.FactObservationObservationSet

inner join dbo.BaseObservationObservationSet
on	FactObservationObservationSet.ObservationSetRecno = BaseObservationObservationSet.ObservationSetRecno

inner join Warehouse.Observation.Observation -- too many records to keep rebuilding in WHOLAP. Will address this when moving to WOMv2
on Observation.ObservationSetSourceUniqueID = BaseObservationObservationSet.SourceUniqueID


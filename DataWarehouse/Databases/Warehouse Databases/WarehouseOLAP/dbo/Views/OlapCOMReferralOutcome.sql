﻿

CREATE VIEW [dbo].OlapCOMReferralOutcome 

AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 ReferralOutcomeID = ReferralOutcome.LookupID
	,ReferralOutcomeCode = ReferralOutcome.LookupCode
	,ReferralOutcome  = ReferralOutcome.LookupDescription


FROM
	warehouse.COM.Referral
	INNER JOIN warehouse.com.LookupBase ReferralOutcome
		ON Referral.OutcomeOfReferral = ReferralOutcome.LookupID


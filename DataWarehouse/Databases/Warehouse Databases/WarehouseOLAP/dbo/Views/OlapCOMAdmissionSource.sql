﻿

CREATE VIEW dbo.OlapCOMAdmissionSource

AS

SELECT 
	 SourceUniqueID = LookupID
	,AdmissionSourceCode = 	LookupCode
	,AdmissionSource = 	LookupDescription
	,AdmissionSourceNHSCode = LookupNHSCode
	,AdmissionSourceNationalCode = lookupNatCode
	,AdmissionSourceCDSCode = LookupCDSCode
	,NationalAdmissionSourceCode = Null
	,NationalAdmissionSource = NULL
FROM 
	warehouse.COM.LookupBase
WHERE 
	LookupTypeID = 'ADSOR'
AND ArchiveFlag = 'N'

UNION ALL

SELECT
	 -1
	,'NOTSPEC'
	,'Not Specified'
	,9
	,'99'
	,9
	,Null
	,NULL


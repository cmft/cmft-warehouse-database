﻿








CREATE view [dbo].[OlapAEDurationBand] AS

SELECT
	 DurationBandCode
	,MinuteBand
	,FifteenMinuteBandCode
	,FifteenMinuteBand
	,ThirtyMinuteBandCode
	,ThirtyMinuteBand
	,HourBandCode
	,HourBand
	,BreachFifteenMinuteBandCode
	,BreachFifteenMinuteBand
	,BreachThirtyMinuteBandCode
	,BreachThirtyMinuteBand
	,BreachHourBandCode
	,BreachHourBand
	,BreachHourBandGroupCode
	,BreachHourBandGroup
FROM
	Warehouse.AE.DurationBand

union

select
	 -2
	,'N/A'
	,-2
	,'N/A'
	,-2
	,'N/A'
	,-2
   	,'N/A'
	,-2
	,'N/A'
	,-2
	,'N/A'
	,-2
   	,'N/A'
   	,-2
   	,'N/A'




﻿



CREATE view dbo.OlapObservationSpecialty

as

select
	SpecialtyID
    ,SpecialtyCode
	,Specialty
	,Division = 
				coalesce(
					Division
					,'N/A'
					)
	,DivisionGroup = 
				case coalesce(
					Division
					,'N/A'
					)
				when 'Childrens' then 'Childrens'
				when 'N/A' then 'N/A'
				else 'Adults'
				end
					

from
	warehouse.Observation.Specialty

--union all

--select distinct
--	 SpecialtyCode = Fact.SpecialtyCode
--	,SpecialtyCode1 =
--				cast(Fact.SpecialtyCode as varchar) + ' - N/A'
--	,Specialty =
--				cast(Fact.SpecialtyCode as varchar) + ' - No Description'
--	,Division = 
--				cast(Fact.SpecialtyCode as varchar) + ' - No Description'
--from
--	dbo.FactObservationSet Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		warehouse.Observation.Specialty specialty
--	where
--		specialty.SpecialtyCode = Fact.SpecialtyCode
--	)

--union all

--select distinct
--	 SpecialtyCode = Fact.SpecialtyCode
--	,SpecialtyCode1 =
--				cast(Fact.SpecialtyCode as varchar) + ' - N/A'
--	,Specialty =
--				cast(Fact.SpecialtyCode as varchar) + ' - No Description'
--	,Division = 'N/A'
				
--from
--	dbo.FactObservationAlert Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		warehouse.Observation.Specialty specialty
--	where
--		specialty.SpecialtyCode = Fact.SpecialtyCode
--	)

union all

select
	'-1'
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'





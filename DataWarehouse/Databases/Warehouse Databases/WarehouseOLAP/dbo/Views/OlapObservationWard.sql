﻿


CREATE view dbo.OlapObservationWard

as

select
	WardID
    ,WardCode
	,Ward = Ward + ' (' + WardCode + ')'
	,Division = 
				coalesce(
					Division
					,'N/A'
					)
	,DivisionGroup = 
				case coalesce(
					Division
					,'N/A'
					)
				when 'Childrens' then 'Childrens'
				when 'N/A' then 'N/A'
				else 'Adults'
				end

from
	warehouse.Observation.Ward

--union all

--select distinct
--	 WardCode = Fact.WardCode
--	,WardCode1 =
--				cast(Fact.WardCode as varchar) + ' - N/A'
--	,WardCode =
--				cast(Fact.WardCode as varchar) + ' - No Description'
--	,Division = 
--				'N/A'
--from
--	dbo.FactObservationSet Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		warehouse.Observation.Ward ward
--	where
--		ward.WardCode = Fact.WardCode
--	)

--union all

--select distinct
--	 WardCode = Fact.WardCode
--	,WardCode1 =
--				cast(Fact.WardCode as varchar) + ' - N/A'
--	,WardCode =
--				cast(Fact.WardCode as varchar) + ' - No Description'
--	,Division = 
--			'N/A'
--from
--	dbo.FactObservationAlert Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		warehouse.Observation.Ward ward
--	where
--		ward.WardCode = Fact.WardCode
	--)

union all

select
	'-1'
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'




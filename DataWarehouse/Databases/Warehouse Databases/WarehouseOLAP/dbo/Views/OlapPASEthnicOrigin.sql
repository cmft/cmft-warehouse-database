﻿create view [dbo].[OlapPASEthnicOrigin] as
SELECT [EthnicOriginCode]
      ,[EthnicOrigin]
      ,[NationalEthnicOriginCode]
      ,[NationalEthnicOrigin]
      ,[DeletedFlag]
  FROM [warehouse].[PAS].[EthnicOrigin]

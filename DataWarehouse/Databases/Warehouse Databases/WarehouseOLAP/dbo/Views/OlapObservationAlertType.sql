﻿

CREATE view [dbo].[OlapObservationAlertType]

as

select
	[AlertTypeID]
	,[AlertTypeCode]
    ,AlertType

from
	[warehouse].[Observation].[AlertType]

--union all

--select distinct
--	 [AlertTypeCode] = Fact.[TypeCode]
--	,AlertType =
--				cast(Fact.[TypeCode] as varchar) + ' - No Description'
--from
--	[dbo].[FactObservationAlert] Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		[warehouse].[Observation].[AlertType] alerttype
--	where
--		alerttype.[AlertTypeCode] = Fact.[TypeCode]
--	)

union all

select
	'-1'
	,'N/A'
	,'N/A'




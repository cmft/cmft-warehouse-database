﻿


CREATE VIEW [dbo].[OlapCOMMaritalStatus] 

AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 MaritalStatusID = MaritalStatus.LookupID
	,MaritalStatusCode = MaritalStatus.LookupCode
	,MaritalStatus  = MaritalStatus.LookupDescription

FROM
	warehouse.COM.Encounter
	INNER JOIN warehouse.com.LookupBase MaritalStatus
		ON Encounter.PatientMaritalStatusID = MaritalStatus.LookupID



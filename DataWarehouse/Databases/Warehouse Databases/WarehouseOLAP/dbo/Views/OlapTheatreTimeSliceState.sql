﻿
create view dbo.OlapTheatreTimeSliceState as

--SELECT
--	 TimeSliceStateCode
--	,TimeSliceState
--	,TimeSliceStateParentCode
--	,TimeSliceStateParent
--FROM
--	WarehouseOLAP.dbo.TimeSliceStateBase

select
	TimeSliceStateCode = 0 , TimeSliceState = 'Not Utilised'
union
select
	TimeSliceStateCode = 1 , TimeSliceState = 'Booked'
union
select
	TimeSliceStateCode = 2 , TimeSliceState = 'Utilised'


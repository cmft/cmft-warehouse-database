﻿




CREATE view [dbo].[FactPathway] as

select
	 PathwayActivity.PathwayTypeCode
	,PathwayActivity.SourceUniqueID
	,PathwayActivity.PathwayRecno
	,PathwayActivity.BenchmarkTypeRecno
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.SiteCode

	,AdmissionDivisionCode =
		coalesce(
			Encounter.AdmissionDivisionCode
			,'N/A'
		)

	,WardCode = 'N/A'

	,ActivityDate =
		Encounter.DischargeDate

	,PathwayActivity.Duration

	,Benchmark =
		case
		when PathwayBenchmark.BenchmarkMinutes is null then null
		when PathwayActivity.Duration > PathwayBenchmark.BenchmarkMinutes
		then 0
		else 1
		end

	,Cases = 1
from
	(
	select
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno

		,Duration =
			DATEDIFF(
				minute
				,PathwayActivity.AdmissionTime
				,case
				when PathwayActivity.OperationEndTime is null
				then PathwayActivity.DischargeTime
				when PathwayActivity.DischargeTime > PathwayActivity.OperationEndTime
				then PathwayActivity.DischargeTime
				else PathwayActivity.OperationEndTime
				end
			)
	from
		Warehouse.Pathway.PathwayActivity PathwayActivity

	inner join Warehouse.Pathway.BenchmarkType
	on	BenchmarkType.BenchmarkTypeCode = 'LOS'

	union all

	select
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno

		,Duration =
			DATEDIFF(
				minute
				,PathwayActivity.AdmissionTime
				,coalesce(
					 PathwayActivity.AnaestheticStartTime
					,PathwayActivity.OperationStartTime
				)
			)
	from
		Warehouse.Pathway.PathwayActivity PathwayActivity

	inner join Warehouse.Pathway.BenchmarkType
	on	BenchmarkType.BenchmarkTypeCode = 'PREOPLOS'

	where
		PathwayActivity.OperationStartTime is not null

	union all

	select
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno

		,Duration =
			DATEDIFF(
				minute
				,PathwayActivity.OperationStartTime
				,PathwayActivity.OperationEndTime
			)
	from
		Warehouse.Pathway.PathwayActivity PathwayActivity

	inner join Warehouse.Pathway.BenchmarkType
	on	BenchmarkType.BenchmarkTypeCode = 'SURG'

	where
		PathwayActivity.OperationStartTime is not null
	and	PathwayActivity.OperationEndTime is not null

	union all

	select
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno

		,Duration =
			DATEDIFF(
				minute
				,PathwayActivity.AnaestheticStartTime
				,PathwayActivity.AnaestheticEndTime
			)
	from
		Warehouse.Pathway.PathwayActivity PathwayActivity

	inner join Warehouse.Pathway.BenchmarkType
	on	BenchmarkType.BenchmarkTypeCode = 'ANAES'

	where
		PathwayActivity.AnaestheticStartTime is not null
	and	PathwayActivity.AnaestheticEndTime is not null

	union all

	select
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno

		,Duration =
			DATEDIFF(
				minute
				,PathwayActivity.OperationEndTime
				,case
				when PathwayActivity.OperationEndTime is null
				then PathwayActivity.DischargeTime
				when PathwayActivity.DischargeTime > PathwayActivity.OperationEndTime
				then PathwayActivity.DischargeTime
				else PathwayActivity.OperationEndTime
				end
			)
	from
		Warehouse.Pathway.PathwayActivity PathwayActivity

	inner join Warehouse.Pathway.BenchmarkType
	on	BenchmarkType.BenchmarkTypeCode = 'POSTOPLOS'

	where
		PathwayActivity.OperationEndTime is not null

	union all

	select
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno

		,Duration =
			sum(
				DATEDIFF(
					minute
					,case
					when WardStay.StartTime > PathwayActivity.OperationEndTime
					then WardStay.StartTime
					else PathwayActivity.OperationEndTime
					end
					,WardStay.EndTime
				)
			)
	from
		Warehouse.Pathway.PathwayActivity PathwayActivity

	inner join Warehouse.Pathway.BenchmarkType
	on	BenchmarkType.BenchmarkTypeCode = 'WARDOTHER'

	inner join Warehouse.APC.WardStay
	on	WardStay.ProviderSpellNo = PathwayActivity.SourceUniqueID
	and	WardStay.EndTime >= PathwayActivity.OperationEndTime

	where
		not exists
		(
		select
			1
		from
			Warehouse.Pathway.PathwayBenchmarkWard

		inner join Warehouse.Pathway.BenchmarkType PathwayBenchmarkType
		on	PathwayBenchmarkType.BenchmarkTypeCode = 'WARD'

		where
			PathwayBenchmarkWard.WardCode = WardStay.WardCode
		and	PathwayBenchmarkWard.PathwayRecno = PathwayActivity.PathwayRecno
		and	PathwayBenchmarkWard.BenchmarkTypeRecno = PathwayBenchmarkType.BenchmarkTypeRecno
		)

	group by
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno

	) PathwayActivity

inner join Warehouse.APC.Encounter Encounter
on	Encounter.ProviderSpellNo = PathwayActivity.SourceUniqueID
and	Encounter.AdmissionTime = Encounter.EpisodeStartTime
and	Encounter.EpisodeEndDate is not null

left join Warehouse.Pathway.PathwayBenchmark
on	PathwayBenchmark.PathwayRecno = PathwayActivity.PathwayRecno
and	PathwayBenchmark.BenchmarkTypeRecno = PathwayActivity.BenchmarkTypeRecno

union all

--ward level activity
select
	 PathwayActivity.PathwayTypeCode
	,PathwayActivity.SourceUniqueID
	,PathwayActivity.PathwayRecno
	,PathwayActivity.BenchmarkTypeRecno
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.SiteCode

	,AdmissionDivisionCode =
		coalesce(
			Encounter.AdmissionDivisionCode
			,'N/A'
		)

	,PathwayActivity.WardCode
	,PathwayActivity.ActivityDate
	,PathwayActivity.Duration

	,Benchmark =
		case
		when PathwayBenchmarkWard.BenchmarkMinutes is null then null
		when PathwayActivity.Duration > PathwayBenchmarkWard.BenchmarkMinutes
		then 0
		else 1
		end

	,Cases = 1
from
	(
	select
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno
		,PathwayBenchmarkWard.WardCode

		,ActivityDate =
			dateadd(day, datediff(day, 0, PathwayActivity.DischargeTime), 0)

		,Duration =
			sum(
				DATEDIFF(
					minute
					,case
					when WardStay.StartTime > PathwayActivity.OperationEndTime
					then WardStay.StartTime
					else PathwayActivity.OperationEndTime
					end
					,WardStay.EndTime
				)
			)

	from
		Warehouse.Pathway.PathwayActivity PathwayActivity

	inner join Warehouse.Pathway.BenchmarkType
	on	BenchmarkType.BenchmarkTypeCode = 'WARD'

	inner join Warehouse.APC.WardStay
	on	WardStay.ProviderSpellNo = PathwayActivity.SourceUniqueID
	and	WardStay.EndTime >= PathwayActivity.OperationEndTime

	inner join Warehouse.Pathway.PathwayBenchmarkWard
	on	PathwayBenchmarkWard.WardCode = WardStay.WardCode
	and	PathwayBenchmarkWard.PathwayRecno = PathwayActivity.PathwayRecno
	and	PathwayBenchmarkWard.BenchmarkTypeRecno = BenchmarkType.BenchmarkTypeRecno

	group by
		 PathwayActivity.PathwayTypeCode
		,PathwayActivity.SourceUniqueID
		,PathwayActivity.PathwayRecno
		,BenchmarkType.BenchmarkTypeRecno
		,PathwayBenchmarkWard.WardCode
		,PathwayActivity.DischargeTime
	) PathwayActivity

inner join Warehouse.Pathway.PathwayBenchmarkWard
on	PathwayBenchmarkWard.WardCode = PathwayActivity.WardCode
and	PathwayBenchmarkWard.PathwayRecno = PathwayActivity.PathwayRecno
and	PathwayBenchmarkWard.BenchmarkTypeRecno = PathwayActivity.BenchmarkTypeRecno

inner join Warehouse.APC.Encounter Encounter
on	Encounter.ProviderSpellNo = PathwayActivity.SourceUniqueID
and	Encounter.AdmissionTime = Encounter.EpisodeStartTime
and	Encounter.EpisodeEndDate is not null






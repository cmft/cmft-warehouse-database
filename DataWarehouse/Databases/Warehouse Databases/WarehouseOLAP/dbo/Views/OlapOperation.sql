﻿





CREATE view [dbo].[OlapOperation] as

select distinct
	 Operation.OperationCode

	,Operation = 
		Operation.OperationCode + ' - ' + Operation.Operation

	,OperationGroupCode = 
		left(Operation.OperationCode , 3)

	,OperationGroup = 
		left(Operation.OperationCode , 3) + ' - ' + Operation.OperationChapter3

	,OperationChapter = 
		OperationChapter.OperationChapterCode + ' - ' + OperationChapter.OperationChapter
from
	Warehouse.PAS.Operation Operation

left join Warehouse.WH.OperationChapter
on	OperationChapter.OperationChapterCode = left(Operation.OperationCode , 1)

union all

select distinct
	 OperationCode = FactAPC.PrimaryOperationCode

	,Operation = 
		FactAPC.PrimaryOperationCode + ' - No Description'

	,OperationGroupCode = '##'

	,OperationGroup = 'N/A'
	,OperationChapter = 'N/A'

from
	dbo.FactAPC
where
	not exists
	(
	select
		1
	from
		Warehouse.PAS.Operation Operation
	where
		Operation.OperationCode = FactAPC.PrimaryOperationCode
	)
and not FactAPC.PrimaryOperationCode = '##'

--and	not exists
--	(
--	select
--		1
--	from
--		WH.PAS.Operation Operation
--	where
--		Operation.OperationCode = FactTheatreOperation.PrimaryProcedureCode
--	)

union all

select
	 '##'
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'







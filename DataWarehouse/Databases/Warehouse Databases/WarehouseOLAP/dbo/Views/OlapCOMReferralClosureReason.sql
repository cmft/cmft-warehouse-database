﻿

CREATE VIEW [dbo].[OlapCOMReferralClosureReason] 

AS
--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 ClosureReasonID = ClosureReason.LookupID
	,ClosureReasonCode = ClosureReason.LookupCode
	,ClosureReason = ClosureReason.LookupDescription


FROM
	warehouse.COM.Referral
	INNER JOIN warehouse.com.LookupBase ClosureReason
		ON Referral.ClosureReasonID = ClosureReason.LookupID





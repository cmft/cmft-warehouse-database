﻿


CREATE VIEW [dbo].[OlapMetricCOMWardStay] as

SELECT
	 MetricBase.MetricCode
	,MetricBase.Metric
	,MetricBase.MetricParentCode

FROM
	dbo.MetricCOMBase MetricBase

WHERE 
	MetricParentCode = 'WSTA'




﻿



CREATE view [dbo].[OlapPASSite]
as

select
	 PASSite.SiteCode
	,PASSite.Site
	,WHSite.Colour
	,WHSite.Sequence
	,PASSite.MappedSiteCode
from
	(
	select
		 PASSite.SiteCode
		,Site = coalesce(PASSite.Site, 'No Description') + ' (' + PASSite.SiteCode + ')'
		,PASSite.MappedSiteCode
	from
		Warehouse.PAS.Site PASSite

	union all

	select DISTINCT
		SiteCode
		,SiteCode + ' - No Description'
		,'#'
	from
		FactAPCWaitingList
	where
		NOT EXISTS
		(
		SELECT
			1
		from
			Warehouse.PAS.Site PASSite
		where
			PASSite.SiteCode = FactAPCWaitingList.SiteCode
		)
	) PASSite

inner join Warehouse.WH.Site WHSite
on	WHSite.SiteCode = PASSite.MappedSiteCode

union all

select
	 SiteCode = 'N/A'
	,Site = 'N/A'
	,Colour = 'pink'
	,Sequence = 999
	,MappedSiteCode = '#'





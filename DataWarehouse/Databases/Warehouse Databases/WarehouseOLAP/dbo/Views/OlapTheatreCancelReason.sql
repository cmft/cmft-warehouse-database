﻿
CREATE view [dbo].[OlapTheatreCancelReason] as

select
	 CancelReason.CancelReasonCode
	,CancelReason.CancelReason
	,CancelReason.CancelReasonGroupCode
	,CancelReasonGroup.CancelReasonGroup
from
	Warehouse.Theatre.CancelReason CancelReason

inner join Warehouse.Theatre.CancelReasonGroup CancelReasonGroup
on	CancelReasonGroup.CancelReasonGroupCode = CancelReason.CancelReasonGroupCode

union all

select
	 CancelReasonCode = 0
	,CancelReason = 'Not Cancelled'
	,CancelReasonGroupCoe = 0
	,CancelReasonGroup = 'Not Cancelled'

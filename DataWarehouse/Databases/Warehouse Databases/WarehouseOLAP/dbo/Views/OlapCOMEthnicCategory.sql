﻿

CREATE VIEW dbo.OlapCOMEthnicCategory

AS

SELECT 
	 EthnicCategorySourceUniqueID = LookupID
	,EthnicCategoryCode = LookupCode
	,EthnicCategory = LookupCode + ' - ' +LookupDescription
	,NHSCode = LookupNHSCode
	,NationalCode = ISNULL (lookupNatCode,'99')
	,CDSCode = ISNULL (LookupNHSCode,LookupCDSCode)
FROM 
	warehouse.com.lookupbase
WHERE
	LookupTypeID like 'ETHGR%'

UNION ALL

SELECT
	-1
	,'NSP'
	,'NSP - Not Specified'
	,'99'
	,'99'
	,'99'
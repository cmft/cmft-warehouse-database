﻿


CREATE view [dbo].[OlapObservationAttendanceType]

as

select
	[AttendanceTypeID]
	,[AttendanceTypeCode]
	,[AttendanceType]
from
	[Warehouse].[Observation].[AttendanceType]

union all

select
	'-1'
	,'N/A'
	,'N/A'





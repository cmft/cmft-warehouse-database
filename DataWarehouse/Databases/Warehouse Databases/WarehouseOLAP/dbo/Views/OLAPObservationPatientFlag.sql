﻿

create view dbo.OLAPObservationPatientFlag as 


select
	PatientFlagID
	,PatientFlagCode
	,PatientFlag
from
	Warehouse.Observation.PatientFlagBase

union 

select
	-1
	,'N/A'
	,'N/A'
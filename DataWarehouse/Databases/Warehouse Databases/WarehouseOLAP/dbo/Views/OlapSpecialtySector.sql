﻿create view OlapSpecialtySector as

select
	SpecialtySectorCode =
		SpecialtySector.EntityCode

	,SpecialtySector =
		SpecialtySector.Description
from
	dbo.EntityLookup SpecialtySector
where
	SpecialtySector.EntityTypeCode = 'KH03SECTOR'

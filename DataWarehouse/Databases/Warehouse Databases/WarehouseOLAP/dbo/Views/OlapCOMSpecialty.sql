﻿


CREATE VIEW [dbo].[OlapCOMSpecialty] 

AS

SELECT
	 Specialty.SpecialtyID  --[SPECT_REFNO]
	 
	,Specialty.SpecialtyCode --[MAIN_IDENT]
	
	,Specialty.Specialty  --[DESCRIPTION]
	
	,Specialty.SpecialtyParentID --[PARNT_REFNO]
	
	,Specialty.SpecialtyHealthOrgOwner --[OWNER_HEORG_REFNO]
	
	,District = ISNULL([Service].District,'Not Recorded')
	
	,Directorate = ISNULL([Service].Directorate,'NotRecorded')

FROM (

	SELECT 
		 SpecialtyID  --[SPECT_REFNO]
		 
		,SpecialtyCode --[MAIN_IDENT]
		
		,Specialty  --[DESCRIPTION]
		
		,SpecialtyParentID --[PARNT_REFNO]
		
		,SpecialtyHealthOrgOwner --[OWNER_HEORG_REFNO]

	FROM warehouse.com.Specialty

	) Specialty
	
	LEFT OUTER JOIN
	
	(
	SELECT 
		SpecialtyID = SPECT_REFNO
		
		,ServiceName = Name
		
		,District
		
		,Directorate
	
	FROM warehouse.COM.[Service]
	
	)[Service]
	
	ON Specialty.SpecialtyID = [Service].SpecialtyID



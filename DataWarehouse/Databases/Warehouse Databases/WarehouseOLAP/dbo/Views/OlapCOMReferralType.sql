﻿

CREATE VIEW [dbo].OlapCOMReferralType 

AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 ReferralTypeID = ReferralType.LookupID
	,ReferralTypeCode = ReferralType.LookupCode
	,ReferralType  = ReferralType.LookupDescription


FROM
	warehouse.COM.Referral
	INNER JOIN warehouse.com.LookupBase ReferralType
		ON Referral.TypeofReferralID = ReferralType.LookupID


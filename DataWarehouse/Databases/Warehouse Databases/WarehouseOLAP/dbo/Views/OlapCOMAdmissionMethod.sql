﻿


CREATE VIEW [dbo].[OlapCOMAdmissionMethod]

AS

SELECT 
	 SourceUniqueID = LookupID
	,AdmissionMethodCode = 	LookupCode
	,AdmissionMethod = 	LookupDescription
	,AdmissionMethodNHSCode = LookupNHSCode
	,AdmissionMethodNationalCode = lookupNatCode
	,AdmissionMethodCDSCode = LookupCDSCode
	,NationalAdmissionMethodCode = Null
	,NationalAdmissionMethod = NULL

FROM 
	warehouse.COM.LookupBase
WHERE 
	LookupTypeID like 'ADMET'
AND ArchiveFlag = 'N'

UNION ALL

SELECT
	 -1
	,'NOTSPEC'
	,'Not Specified'
	,9
	,'99'
	,9
	,Null
	,NULL



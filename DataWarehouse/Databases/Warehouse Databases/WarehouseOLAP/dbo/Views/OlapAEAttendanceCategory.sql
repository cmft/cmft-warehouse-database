﻿
CREATE view [dbo].[OlapAEAttendanceCategory] as

select
	 AttendanceCategoryCode =
		convert(int, AttendanceCategoryCode)

	,AttendanceCategory = AttendanceCategory

	,AttendanceCategoryGroup =
	case AttendanceCategoryCode
	when 2 then 'Clinic Attender'
	else 'Attender'
	end
from
	Warehouse.AE.AttendanceCategory


﻿create  view [dbo].[OlapTheatreSex] as

select
	 SexCode = EntityCode
	,Sex = Description
from
	dbo.EntityLookup
where
	EntityTypeCode = 'THEATRESEX'

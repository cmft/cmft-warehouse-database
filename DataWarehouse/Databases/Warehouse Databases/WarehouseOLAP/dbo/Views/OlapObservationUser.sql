﻿


CREATE view [dbo].[OlapObservationUser] as 

select
	UserBase.UserID
    ,UserCode
	,Username =
				Surname + 
						case
						when Forename is null 
						then ''
						else ', ' + Forename
						end
	,UserGroup = coalesce(
					UserGroup
					,'N/A'
				)
from
	Warehouse.Observation.UserBase

left join Warehouse.Observation.UserGroupBase
on	UserGroupBase.UserID = UserBase.UserID
and not exists
		(
		select
			1
		from
			Warehouse.Observation.UserGroupBase LaterUserGroup
		where
			LaterUserGroup.UserID = UserGroupBase.UserID
		and	LaterUserGroup.SourceUniqueID > UserGroupBase.SourceUniqueID
		)

left join Warehouse.Observation.UserGroup
on	UserGroup.UserGroupID = UserGroupBase.UserGroupID


union all

select
	'-1'
	,'N/A'
	,'N/A'
	,'N/A'







﻿


CREATE VIEW [dbo].[OlapInterface] AS

SELECT
	 InterfaceCode = InterfaceCode
	,Interface = Interface
	,InterfaceGroupCode =	CASE 
								WHEN Interface.InterfaceCode IN('INFO','INQ') THEN 'PAS'
								ELSE Interface.InterfaceCode
							END
	,InterfaceGroup =	CASE 
							WHEN Interface.InterfaceCode IN('INFO','INQ') THEN 'PAS'
							ELSE Interface.Interface
						END
FROM
	Warehouse.WH.Interface




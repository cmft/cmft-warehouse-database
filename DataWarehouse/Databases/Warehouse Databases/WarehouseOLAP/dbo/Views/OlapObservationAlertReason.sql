﻿

CREATE view [dbo].[OlapObservationAlertReason]

as

select
	[AlertReasonID]
	,[AlertReasonCode]
    ,AlertReason

from
	[warehouse].[Observation].[AlertReason]

--union all

--select distinct
--	 [AlertReasonCode] = Fact.[ReasonCode]
--	,AlertReason =
--				cast(Fact.[ReasonCode] as varchar) + ' - No Description'
--from
--	[dbo].[FactObservationAlert] Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		[warehouse].[Observation].[AlertReason] alertreason
--	where
--		alertreason.[AlertReasonCode] = Fact.[ReasonCode]
--	)


union all

select
	'-1'
	,'N/A'
	,'N/A'



﻿



CREATE view [dbo].[OlapTheatre] as

select
	 Theatre.TheatreCode
	,Theatre.Theatre
	,Theatre.OperatingSuiteCode

	,OperatingSuite = 
		coalesce(
			 OperatingSuite.OperatingSuite
			,convert(varchar, Theatre.OperatingSuiteCode) + ' - No Description'
		)

	,Theatre.TheatreCode1
	,Colour = EntityXref.XrefEntityCode
from
	Warehouse.Theatre.Theatre Theatre

left join Warehouse.Theatre.OperatingSuite
on	OperatingSuite.OperatingSuiteCode = Theatre.OperatingSuiteCode

left join Warehouse.dbo.EntityXref
on	EntityXref.EntityTypeCode = 'THEATRE'
and	EntityXref.EntityCode = Theatre.TheatreCode
and	EntityXref.XrefEntityTypeCode = 'COLOUR'

union all

select distinct
	 FactTheatreSession.TheatreCode
	,cast(FactTheatreSession.TheatreCode as varchar) + ' - No Description'
	,0
	,'N/A'
	,'N/A'
	,'DarkGray'
from
	FactTheatreSession
where
	not exists
	(
	select
		1
	from
		Warehouse.Theatre.Theatre Theatre
	where
		Theatre.TheatreCode = FactTheatreSession.TheatreCode
	)

union all

select
	0
	,'N/A'
	,0
	,'N/A'
	,'N/A'
	,'N/A'





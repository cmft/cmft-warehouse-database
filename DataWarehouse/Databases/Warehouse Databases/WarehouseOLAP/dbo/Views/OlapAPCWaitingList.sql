﻿











CREATE view [dbo].[OlapAPCWaitingList] as

select
	 EncounterRecno
	,InterfaceCode
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,WaitingListSpecialty = Encounter.SpecialtyCode
	,Encounter.PASSpecialtyCode
	,Encounter.EpisodeSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,EpisodicGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,ExpectedLOS
	,ProcedureDate
	,FutureCancellationDate
	,TheatreKey
	,TheatreInterfaceCode
	,EpisodeNo
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,AgeCode
	,Cases
	,LengthOfWait
	,DurationCode
	,WaitTypeCode
	,StatusCode
	,CategoryCode
	,AddedToWaitingListTime

	,AddedLastWeek
	,WaitingListAddMinutes
	,OPCSCoded
	,WithRTTStartDate
	,WithRTTStatusCode
	,WithExpectedAdmissionDate


	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.DiagnosticProcedure
	,Encounter.DirectorateCode
	,Encounter.AdminCategoryCode
	,Encounter.DurationAtTCIDateCode

	,WithTCIDate
	,GuaranteedAdmissionDate
	,WithGuaranteedAdmissionDate
	,WithRTTOpenPathway

from
	dbo.BaseAPCWaitingList Encounter








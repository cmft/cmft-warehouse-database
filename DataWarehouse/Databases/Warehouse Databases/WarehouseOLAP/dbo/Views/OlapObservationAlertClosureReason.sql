﻿

CREATE view [dbo].[OlapObservationAlertClosureReason]
as

select
	[AlertClosureReasonID]
	,[AlertClosureReasonCode]
	,[AlertClosureReason]
from
	Warehouse.[Observation].[AlertClosureReason]

union all

select
	'-1'
	,'N/A'
	,'N/A'




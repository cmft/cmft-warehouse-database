﻿





/* for combined cube for reporting */
CREATE view [dbo].[OlapObservationReference]

as

select 
	 ReferenceCode
	,Reference
	,ContextCode
from
	(
	select
		ReferenceCode = 'ALSEV||' + AlertSeverityCode
		,Reference = AlertSeverityGroup
		,ContextCode
	from
		Warehouse.Observation.AlertSeverity

	union

	select
		ReferenceCode = 'ALSEV||N/A' 
		,Reference = 'N/A'
		,ContextCode = 'PTRACK'
			

	union 

	select
		ReferenceCode = 'ODTST||' + ObservationStatusCode
		,Reference = ObservationShortStatus
		,ContextCode
	from
		Warehouse.Observation.ObservationStatus		

	union

	select
		ReferenceCode = 'ODTST||N/A' 
		,Reference = 'N/A'
		,ContextCode = 'PTRACK'

	union 

	select
		ReferenceCode = 'ASDST||' + OverallAssessedStatusCode
		,Reference = OverallAssessedStatusGroup
		,ContextCode
	from
		Warehouse.Observation.OverallAssessedStatus

	union

	select
		ReferenceCode = 'ASDST||N/A' 
		,Reference = 'N/A'
		,ContextCode = 'PTRACK'

	union 

	select
		'ALDUR||' + cast(DurationID as varchar)
		,AlertDurationBand
		,ContextCode = 'PTRACK' 
	from
		dbo.ObservationDurationBase

	union 

	select
		'CHDUR||' + cast(DurationID as varchar)
		,AlertChainDurationBand
		,ContextCode = 'PTRACK' 
	from
		dbo.ObservationDurationBase

	union 

	select
		'RESUS||' + cast(CallOutTypeID as varchar)
		,CallOutType
		,ContextCode = 'CEN||RESUS' 
	from
		Warehouse.Resus.CallOutType

	union 

	select
		ReferenceCode = 'RESUS||' + '-1'
		,Reference = 'N/A'
		,ContextCode = 'CEN||RESUS' 

	) Reference
		
union all

select
	'-1'
	,'N/A'
	,'N/A'








﻿
CREATE VIEW dbo.OlapCOMCaseload

AS

SELECT DISTINCT
	 CaseLoadID
	--,ReferralID
	--,RoleTypeID
	--,StatusID
	--,AllocationTime
	--,AllocationDate
	--,AllocationReasonID
	--,InterventionlevelID
	--,DischargeTime
	--,DischargeDate
	--,Outcome
	--,AllocationProfessionalCarerID
	--,AllocationSpecialtyID
	--,AllocationStaffTeamID
	--,ResponsibleHealthOrganisation
	--,DischargeReasonID
	--,PatientSourceID
	--,PatientSourceSystemUniqueID
	,PatientNHSNumber
	--,PatientNHSNumberStatusIndicator
	--,PatientTitleID
	--,PatientForename
	--,PatientSurname
	--,PatientAddress1
	--,PatientAddress2
	--,PatientAddress3
	--,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	--,PatientDateOfDeath
	--,PatientSexID
	--,PatientEthnicGroupID
	--,PatientCurrentRegisteredPracticeCode
	--,PatientEncounterRegisteredPracticeCode
	--,PatientCurrentRegisteredGPCode
	--,PatientEncounterRegisteredGPCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	--,ArchiveFlag
	--,HealthOrgOwnerID
FROM 
	WarehouseOLAP.dbo.BaseCOMCaseLoad

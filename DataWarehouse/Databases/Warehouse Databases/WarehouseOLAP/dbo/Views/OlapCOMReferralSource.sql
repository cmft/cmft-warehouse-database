﻿

CREATE VIEW [dbo].OlapCOMReferralSource 

AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 ReferralSourceID = ReferralSource.LookupID
	,ReferralSourceCode = ReferralSource.LookupCode
	,ReferralSource  = ReferralSource.LookupDescription


FROM
	warehouse.COM.Referral
	INNER JOIN warehouse.com.LookupBase ReferralSource
		ON Referral.SourceofReferralID = ReferralSource.LookupID


﻿


CREATE view [dbo].[OlapDirectorate] as

select
	 Directorate.DirectorateCode
	,Directorate.Directorate
	,Directorate.DivisionCode
from
	Warehouse.WH.Directorate Directorate

union

select distinct
	 DirectorateCode = isnull(Fact.DirectorateCode, 'N/A')
	,Directorate =
		isnull(Fact.DirectorateCode,'N/A') + ' - No Description'
	,DivisionCode = 'N/A'
from
	dbo.FactAPC Fact
where
	not exists
	(
	select
		1
	from
		Warehouse.WH.Directorate Directorate
	where
		Directorate.DirectorateCode = Fact.DirectorateCode
	)

union

select distinct
	 DirectorateCode = isnull(Fact.DirectorateCode, 'N/A')
	,Directorate =
		isnull(Fact.DirectorateCode,'N/A') + ' - No Description'
	,DivisionCode = 'N/A'
from
	dbo.FactOP Fact
where
	not exists
	(
	select
		1
	from
		Warehouse.WH.Directorate Directorate
	where
		Directorate.DirectorateCode = Fact.DirectorateCode
	)


union

select distinct
	 DirectorateCode = isnull(Fact.DirectorateCode, 'N/A')
	,Directorate =
		isnull(Fact.DirectorateCode,'N/A') + ' - No Description'
	,DivisionCode = 'N/A'
from
	dbo.FactOPWaitingList Fact
where
	not exists
	(
	select
		1
	from
		Warehouse.WH.Directorate Directorate
	where
		Directorate.DirectorateCode = Fact.DirectorateCode
	)

union

select distinct
	 DirectorateCode = isnull(Fact.DirectorateCode, 'N/A')
	,Directorate =
		isnull(Fact.DirectorateCode,'N/A') + ' - No Description'
	,DivisionCode = 'N/A'
from
	dbo.FactAPCWaitingList Fact
where
	not exists
	(
	select
		1
	from
		Warehouse.WH.Directorate Directorate
	where
		Directorate.DirectorateCode = Fact.DirectorateCode
	)

union

select
	 'N/A'
	,'N/A'
	,'N/A'







﻿

CREATE VIEW [dbo].OlapCOMReferralReason 

AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 ReferralReasonID = ReasonforReferral.LookupID
	,ReferralReasonCode = ReasonforReferral.LookupCode
	,ReferralReason  = ReasonforReferral.LookupDescription


FROM
	warehouse.COM.Referral
	INNER JOIN warehouse.com.LookupBase ReasonforReferral
		ON Referral.ReasonforReferralID = ReasonforReferral.LookupID


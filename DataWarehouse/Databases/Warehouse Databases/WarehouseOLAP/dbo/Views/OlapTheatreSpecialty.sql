﻿CREATE view [dbo].[OlapTheatreSpecialty] as

select
	SpecialtyCode
	,Specialty
	,NationalSpecialtyCode = SpecialtyCode1
from
	Warehouse.Theatre.Specialty

union all

select
	 0
	,'N/A'
	,null

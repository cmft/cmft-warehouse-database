﻿

CREATE VIEW [dbo].OlapCOMReferralStatus 

AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 ReferralStatusID = ReferralStatus.LookupID
	,ReferralStatusCode = ReferralStatus.LookupCode
	,ReferralStatus  = ReferralStatus.LookupDescription


FROM
	warehouse.COM.Referral
	INNER JOIN warehouse.com.LookupBase ReferralStatus
		ON Referral.ReferralStatus = ReferralStatus.LookupID


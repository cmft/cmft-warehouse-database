﻿



CREATE view [dbo].[OlapObservationAlertSeverity]

as

select
	AlertSeverityID
	,AlertSeverityCode
    ,AlertSeverity
    ,AlertSeverityLabel = AlertSeverityCode + ' - ' + AlertSeverity
	,AlertSeverityGroup

from
	Warehouse.Observation.AlertSeverity

union all

select
	'-1'
	,'N/A'
	,'N/A'
	,'N/A'
	,'N/A'






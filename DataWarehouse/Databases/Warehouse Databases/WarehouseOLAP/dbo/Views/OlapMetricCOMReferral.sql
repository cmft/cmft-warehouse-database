﻿

CREATE VIEW [dbo].[OlapMetricCOMReferral] as

SELECT
	 MetricBase.MetricCode
	,MetricBase.Metric
	,MetricBase.MetricParentCode

FROM
	dbo.MetricCOMBase MetricBase

WHERE 
	MetricParentCode = 'REFL';




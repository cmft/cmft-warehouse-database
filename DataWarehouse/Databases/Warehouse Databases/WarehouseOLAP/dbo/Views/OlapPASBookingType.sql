﻿

CREATE view [dbo].[OlapPASBookingType] as

select
	 BookingTypeCode
	,BookingType
	,BookingCode
	,Booking
from
	Warehouse.PAS.BookingType

union all


select distinct
	 BookingTypeCode
	,BookingTypeCode + case when BookingTypeCode = 'N/A' then '' else ' - No Description' end
	,'#'
	,'Unknown'
from
	dbo.FactOP Encounter
where
	not exists
	(
	select
		1
	from
		Warehouse.PAS.BookingType
	where
		Encounter.BookingTypeCode = BookingType.BookingTypeCode
	)



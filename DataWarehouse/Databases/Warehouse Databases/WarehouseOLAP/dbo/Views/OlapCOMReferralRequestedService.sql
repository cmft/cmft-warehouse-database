﻿
CREATE VIEW dbo.OlapCOMReferralRequestedService AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 RequestedServiceID = cast(RequestedService.LookupID as int)
	,RequestedServiceCode = RequestedService.LookupCode
	,RequestedService  = RequestedService.LookupDescription
FROM
	Warehouse.COM.Referral
INNER JOIN warehouse.com.LookupBase RequestedService
ON Referral.RequestedServiceID = RequestedService.LookupID


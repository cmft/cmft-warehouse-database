﻿

CREATE view [dbo].[OlapObservationAlertDuration]

as

select	
	DurationID
	,[MinuteBand]
	,[AlertDurationBand]
from
	[dbo].[ObservationDurationBase]



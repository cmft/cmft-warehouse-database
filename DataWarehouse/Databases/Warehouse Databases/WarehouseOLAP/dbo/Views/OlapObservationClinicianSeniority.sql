﻿

create view [dbo].[OlapObservationClinicianSeniority] as 

select
	ClinicianSeniorityID
	,ClinicianSeniorityCode
	,ClinicianSeniority
from
	Warehouse.Observation.ClinicianSeniority

union all

select
	'-1'
	,'N/A'
	,'N/A'



﻿

create view [dbo].[OlapAEEncounter_Old] as

select
	 AEEncounter.EncounterRecno
	,AEEncounter.SourceUniqueID
	,AEEncounter.UniqueBookingReferenceNo
	,AEEncounter.PathwayId
	,AEEncounter.PathwayIdIssuerCode
	,AEEncounter.RTTStatusCode
	,AEEncounter.RTTStartDate
	,AEEncounter.RTTEndDate
	,AEEncounter.DistrictNo
	,AEEncounter.TrustNo
	,AEEncounter.CasenoteNo
	,AEEncounter.DistrictNoOrganisationCode
	,AEEncounter.NHSNumber
	,AEEncounter.NHSNumberStatusId
	,AEEncounter.PatientTitle
	,AEEncounter.PatientForename
	,AEEncounter.PatientSurname
	,AEEncounter.PatientAddress1
	,AEEncounter.PatientAddress2
	,AEEncounter.PatientAddress3
	,AEEncounter.PatientAddress4
	,AEEncounter.Postcode
	,AEEncounter.DateOfBirth
	,AEEncounter.DateOfDeath
	,AEEncounter.SexCode
	,AEEncounter.CarerSupportIndicator
	,AEEncounter.RegisteredGpCode
	,AEEncounter.RegisteredGpPracticeCode
	,AEEncounter.AttendanceNumber
	,AEEncounter.ArrivalModeCode
	,AEEncounter.AttendanceCategoryCode
	,AEEncounter.AttendanceDisposalCode
	,AEEncounter.IncidentLocationTypeCode
	,AEEncounter.PatientGroupCode
	,AEEncounter.SourceOfReferralCode
	,AEEncounter.ArrivalDate
	,AEEncounter.ArrivalTime
	,AEEncounter.AgeOnArrival
	,AEEncounter.InitialAssessmentTime
	,AEEncounter.SeenForTreatmentTime
	,AEEncounter.AttendanceConclusionTime
	,AEEncounter.DepartureTime
	,AEEncounter.CommissioningSerialNo
	,AEEncounter.NHSServiceAgreementLineNo
	,AEEncounter.ProviderReferenceNo
	,AEEncounter.CommissionerReferenceNo
	,AEEncounter.ProviderCode
	,AEEncounter.CommissionerCode
	,AEEncounter.StaffMemberCode
	,AEEncounter.InvestigationCodeFirst
	,AEEncounter.InvestigationCodeSecond
	,AEEncounter.DiagnosisCodeFirst
	,AEEncounter.DiagnosisCodeSecond
	,AEEncounter.TreatmentCodeFirst
	,AEEncounter.TreatmentCodeSecond
	,AEEncounter.PASHRGCode
	,AEEncounter.HRGVersionCode
	,AEEncounter.PASDGVPCode
	,AEEncounter.SiteCode
	,AEEncounter.Created
	,AEEncounter.Updated
	,AEEncounter.ByWhom
	,AEEncounter.InterfaceCode
	,AEEncounter.PCTCode
	,AEEncounter.ResidencePCTCode
	,AEEncounter.InvestigationCodeList
	,AEEncounter.TriageCategoryCode
	,AEEncounter.PresentingProblem
	,AEEncounter.ToXrayTime
	,AEEncounter.FromXrayTime
	,AEEncounter.ToSpecialtyTime
	,AEEncounter.SeenBySpecialtyTime
	,AEEncounter.EthnicCategoryCode
	,AEEncounter.ReferredToSpecialtyCode
	,AEEncounter.DecisionToAdmitTime
	,HRG4AEEncounter.HRGCode
	,AEEncounter.AscribeLeftDeptTime
	,AEEncounter.AmbulanceArrivalTime
from
	Warehouse.AE.Encounter AEEncounter

left join Warehouse.AE.HRG4Encounter HRG4AEEncounter
on	HRG4AEEncounter.EncounterRecno = AEEncounter.EncounterRecno



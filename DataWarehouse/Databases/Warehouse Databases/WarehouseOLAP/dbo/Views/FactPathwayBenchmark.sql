﻿create view FactPathwayBenchmark as

select
	 PathwayBenchmarkWard.PathwayRecno
	,PathwayBenchmarkWard.BenchmarkTypeRecno
	,PathwayBenchmarkWard.WardCode
	,PathwayBenchmarkWard.BenchmarkMinutes
from
	Warehouse.Pathway.PathwayBenchmarkWard

inner join Warehouse.Pathway.BenchmarkType PathwayBenchmarkType
on	PathwayBenchmarkType.BenchmarkTypeCode = 'WARD'

union all

select
	 PathwayBenchmark.PathwayRecno
	,PathwayBenchmark.BenchmarkTypeRecno
	,WardCode = 'N/A'
	,PathwayBenchmark.BenchmarkMinutes
from
	Warehouse.Pathway.PathwayBenchmark



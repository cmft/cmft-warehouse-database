﻿

CREATE view [dbo].[OlapPASSourceOfReferral] as

select
	 SourceOfReferral.SourceOfReferralCode
	,SourceOfReferral.SourceOfReferral
	,SourceOfReferral.NationalSourceOfReferralCode

	,ReferralType =
	case SourceOfReferral.NationalSourceOfReferralCode
	when '03'	then 'Gp Referral'
	when '92'	then 'Gp Referral' --GS20110830  Added GP referral should include both GMP's and GDP's
	else 'Other Referral'
	end 

	,ReportableFlag = coalesce(SourceOfReferral.ReportableFlag, 0)

	,Reportable =
	case coalesce(SourceOfReferral.ReportableFlag, 0)
	when 0
	then 'Non-reportable'
	else 'Reportable'
	end 

	,NewFlag = coalesce(SourceOfReferral.NewFlag, 0)

	,New =
	case coalesce(SourceOfReferral.NewFlag, 0)
	when 0
	then 'Review'
	else 'New'
	end 
from
	Warehouse.PAS.SourceOfReferral SourceOfReferral



﻿
CREATE view [dbo].[OlapTheatreAdmissionType] as

select
	 TheatreAdmissionType.AdmissionTypeCode
	,TheatreAdmissionType.AdmissionType
from
	Warehouse.Theatre.AdmissionType TheatreAdmissionType

union all

select
	0
	,'N/A'

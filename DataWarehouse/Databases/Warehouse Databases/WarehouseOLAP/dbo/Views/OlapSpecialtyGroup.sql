﻿create view [dbo].[OlapSpecialtyGroup] as

select
	 SpecialtyGroupID
	,SpecialtyGroup
from
	Warehouse.BedComplement.SpecialtyGroup

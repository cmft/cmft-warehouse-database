﻿
CREATE view [dbo].[OlapAESite]
as

select
	 AESite.SiteCode
	,AESite.Site
	,WHSite.Colour
	,WHSite.Sequence
	,AESite.MappedSiteCode
from
	Warehouse.AE.Site AESite

inner join Warehouse.WH.Site WHSite
on	WHSite.SiteCode = AESite.MappedSiteCode


﻿create view WrkEncounterProcedure as

select
	 WrkEncounter.ProviderSpellNo
	,WrkEncounter.AdmissionTime
	,WrkEncounter.DischargeTime
	,WrkProcedure.OperationStartTime
	,WrkProcedure.OperationEndTime
	,WrkProcedure.AnaestheticStartTime
	,WrkProcedure.AnaestheticEndTime
	,Template.TemplateCode
	,RuleBase.*
from
	Pathway.WrkEncounter WrkEncounter

cross join Pathway.Template
--on	Template.TemplateCode = 'PATCAT'

inner join Pathway.Pathway
on	Pathway.TemplateRecno = Template.TemplateRecno

inner join Pathway.RuleBase RuleBase
on	RuleBase.PathwayRecno = Pathway.PathwayRecno

inner join Pathway.WrkProcedure WrkProcedure
on	WrkProcedure.ProviderSpellNo = WrkEncounter.ProviderSpellNo
and	WrkProcedure.ProcedureCode = RuleBase.ProcedureCode

where
	WrkEncounter.PatientCategoryCode = RuleBase.PatientCategoryCode

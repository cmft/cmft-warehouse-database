﻿

CREATE view [dbo].[PathwayDetail] as

select
	 PathwayActivity.PathwayTypeCode
	,PathwayActivity.SourceUniqueID
	,Encounter.PrimaryOperationCode
	,ORMISPrimaryOperationCode = ORMISDetail.ProcedureCode
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.CasenoteNumber
	,Encounter.NHSNumber

	,AgeOnAdmission =
		convert(
			varchar
			,case
			when Encounter.DateOfBirth >= Encounter.AdmissionDate
			then 0
			else
				datediff(year, Encounter.DateOfBirth, Encounter.AdmissionDate) -
				case
				when Encounter.DateOfBirth >= Encounter.AdmissionDate
				then 0
				when datepart(day, Encounter.DateOfBirth) = datepart(day, Encounter.AdmissionDate)
				and	datepart(month, Encounter.DateOfBirth) = datepart(month,Encounter.AdmissionDate)
				and datepart(year, Encounter.DateOfBirth) <> datepart(year,Encounter.AdmissionDate)
				then 0 
				else 
					case
					when Encounter.DateOfBirth > Encounter.AdmissionDate then 0
					when datepart(dy, Encounter.DateOfBirth) > datepart(dy, Encounter.AdmissionDate) 
					then 1
					else 0 
					end 
				end
			end
		)

	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.AdmissionDate
	,Encounter.DischargeDate
	,Encounter.PrimaryOperationDate
	,Encounter.ManagementIntentionCode

	,ORMISDetail.Surgeon1
	,ORMISDetail.Surgeon2
	,ORMISDetail.Surgeon3
	,ORMISDetail.Anaesthetist1
	,ORMISDetail.Anaesthetist2
	,ORMISDetail.Anaesthetist3

	,OperationTime =
		convert(
			 varchar
			,DATEDIFF(
				minute
				,PathwayActivity.OperationStartTime
				,PathwayActivity.OperationEndTime
			)
		)

	,AnaestheticTime =
		convert(
			varchar
			,DATEDIFF(
				minute
				,PathwayActivity.AnaestheticStartTime
				,PathwayActivity.AnaestheticEndTime
			)
		)

	,LOS =
		convert(
			 varchar
			,DATEDIFF(
				minute
				,PathwayActivity.AdmissionTime
				,case
				when PathwayActivity.OperationEndTime is null
				then PathwayActivity.DischargeTime
				when PathwayActivity.DischargeTime > PathwayActivity.OperationEndTime
				then PathwayActivity.DischargeTime
				else PathwayActivity.OperationEndTime
				end
			)
		)

	,PreOpLOS =
		convert(
			 varchar
			,DATEDIFF(
				minute
				,PathwayActivity.AdmissionTime
				,coalesce(
					 PathwayActivity.AnaestheticStartTime
					,PathwayActivity.OperationStartTime
				)
			)
		)

	,PostOpLOS =
		convert(
			 varchar
			,DATEDIFF(
				minute
				,PathwayActivity.OperationEndTime
				,case
				when PathwayActivity.OperationEndTime is null
				then PathwayActivity.DischargeTime
				when PathwayActivity.DischargeTime > PathwayActivity.OperationEndTime
				then PathwayActivity.DischargeTime
				else PathwayActivity.OperationEndTime
				end
			)
		)

	,Encounter.PatientCategoryCode

from
	Warehouse.Pathway.PathwayActivity PathwayActivity

inner join Warehouse.APC.Encounter Encounter
on	Encounter.ProviderSpellNo = PathwayActivity.SourceUniqueID
and	Encounter.EpisodeStartTime = Encounter.AdmissionTime

left join 
	(
	select
		 EncounterProcedureDetail.ProviderSpellNo
		,ProcedureCode = Operation.OperationCode1
		,OperationStartTime = OperationDetail.OperationStartDate
		,OperationEndTime = OperationDetail.OperationEndDate
		,AnaestheticStartTime = OperationDetail.InAnaestheticTime
		,AnaestheticEndTime = OperationDetail.AnaestheticReadyTime
		,OperationDetail.Surgeon1Code
		,OperationDetail.Surgeon2Code
		,OperationDetail.Surgeon3Code
		,OperationDetail.Anaesthetist1Code
		,OperationDetail.Anaesthetist2Code
		,OperationDetail.Anaesthetist3Code

		,Surgeon1 = Surgeon1.Forename + ' ' + Surgeon1.Surname
		,Surgeon2 = Surgeon2.Forename + ' ' + Surgeon2.Surname
		,Surgeon3 = Surgeon3.Forename + ' ' + Surgeon3.Surname

		,Anaesthetist1 = Anaesthetist1.Forename + ' ' + Anaesthetist1.Surname
		,Anaesthetist2 = Anaesthetist2.Forename + ' ' + Anaesthetist2.Surname
		,Anaesthetist3 = Anaesthetist3.Forename + ' ' + Anaesthetist3.Surname

	from
		Warehouse.APC.EncounterProcedureDetail EncounterProcedureDetail

	inner join Warehouse.Theatre.ProcedureDetail ProcedureDetail
	on	ProcedureDetail.SourceUniqueID = EncounterProcedureDetail.ProcedureDetailSourceUniqueID

	inner join Warehouse.Theatre.Operation Operation
	on	Operation.OperationCode = ProcedureDetail.ProcedureCode

	inner join Warehouse.Theatre.OperationDetail OperationDetail
	on	OperationDetail.SourceUniqueID = ProcedureDetail.OperationDetailSourceUniqueID

	left join Warehouse.Theatre.Staff Surgeon1
	on	Surgeon1.StaffCode = OperationDetail.Surgeon1Code

	left join Warehouse.Theatre.Staff Surgeon2
	on	Surgeon2.StaffCode = OperationDetail.Surgeon2Code

	left join Warehouse.Theatre.Staff Surgeon3
	on	Surgeon3.StaffCode = OperationDetail.Surgeon3Code

	left join Warehouse.Theatre.Staff Anaesthetist1
	on	Anaesthetist1.StaffCode = OperationDetail.Anaesthetist1Code

	left join Warehouse.Theatre.Staff Anaesthetist2
	on	Anaesthetist2.StaffCode = OperationDetail.Anaesthetist2Code

	left join Warehouse.Theatre.Staff Anaesthetist3
	on	Anaesthetist3.StaffCode = OperationDetail.Anaesthetist3Code

	where
		ProcedureDetail.PrimaryProcedureFlag = 1
	--get the 1st distinct procedure for this spell
	and	not exists
		(
		select
			1
		from
			Warehouse.APC.EncounterProcedureDetail PreviousEncounterProcedureDetail

		inner join Warehouse.Theatre.ProcedureDetail PreviousProcedureDetail
		on	PreviousProcedureDetail.SourceUniqueID = PreviousEncounterProcedureDetail.ProcedureDetailSourceUniqueID

		inner join Warehouse.Theatre.Operation PreviousOperation
		on	PreviousOperation.OperationCode = PreviousProcedureDetail.ProcedureCode

		where
			PreviousProcedureDetail.PrimaryProcedureFlag = 1
		and	PreviousEncounterProcedureDetail.ProviderSpellNo = EncounterProcedureDetail.ProviderSpellNo
	--get first operation only
	--	and	PreviousOperation.OperationCode1 = Operation.OperationCode1
		and	(
			coalesce(PreviousProcedureDetail.ProcedureStartTime, getdate()) < coalesce(ProcedureDetail.ProcedureStartTime, getdate())
			or	(
					coalesce(PreviousProcedureDetail.ProcedureStartTime, getdate()) = coalesce(ProcedureDetail.ProcedureStartTime, getdate())
				and	PreviousProcedureDetail.SourceUniqueID < ProcedureDetail.SourceUniqueID
				)
			)
		)
	) ORMISDetail
on	ORMISDetail.ProviderSpellNo = PathwayActivity.SourceUniqueID

where
	PathwayActivity.PathwayTypeCode = 'IP'
--and	PathwayActivity.SourceUniqueID = '3075884/3'





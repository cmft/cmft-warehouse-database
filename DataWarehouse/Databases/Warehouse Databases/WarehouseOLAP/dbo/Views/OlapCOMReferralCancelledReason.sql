﻿

CREATE VIEW [dbo].[OlapCOMReferralCancelledReason] 

AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 CancelledReasonID = CancelledReason.LookupID
	,CancelledReasonCode = CancelledReason.LookupCode
	,CancelledReason = CancelledReason.LookupDescription


FROM
	warehouse.COM.Referral
	INNER JOIN warehouse.com.LookupBase CancelledReason
		ON Referral.CancelledReasonID = CancelledReason.LookupID




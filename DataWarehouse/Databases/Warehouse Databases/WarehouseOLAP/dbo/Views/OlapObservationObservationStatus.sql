﻿

CREATE view [dbo].[OlapObservationObservationStatus]
as
select
	[ObservationStatusID]
	,[ObservationStatusCode]
	,[ObservationStatus]
	,[ObservationShortStatus]
	--,[ObservationStatusOrder]
	--,[ContextCode]
from
	Warehouse.[Observation].[ObservationStatus]

--union all

--select distinct
--	 [ObservationStatusCode] = Fact.[DueTimeStatusCode]
--	,[ObservationStatus] =
--				cast(Fact.[DueTimeStatusCode] as varchar) + ' - No Description'
--	,[ObservationShortStatus] = 'N/A'

--from
--	[dbo].[FactObservationSet] Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		Warehouse.[Observation].[ObservationStatus] ObservationStatus
--	where
--		ObservationStatus.[ObservationStatusCode] = Fact.[DueTimeStatusCode]
--	)

union all

select
	'-1'
	,'N/A'
	,'N/A'
	,'N/A'




﻿


CREATE view [dbo].[OlapObservationSignificantEventType]

as

select
	[SignificantEventTypeID]
	,[SignificantEventTypeCode]
	,[SignificantEventType]
from
	[Warehouse].[Observation].[SignificantEventType]

union all

select
	'-1'
	,'N/A'
	,'N/A'





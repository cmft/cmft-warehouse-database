﻿
CREATE view [dbo].[OlapAEDiagnosis] as

select distinct
	 Diagnosis.ReferenceCode
	,Diagnosis.Reference
	,Diagnosis.ReferenceParentCode
	,Diagnosis.DisplayOrder
from
	(
	select
		 Diagnosis.ReferenceCode
		,Diagnosis.Reference
		,Diagnosis.ReferenceParentCode
		,Diagnosis.DisplayOrder
	from
		Warehouse.AE.Reference Diagnosis
	where
		exists
		(
		select
			1
		from
			Warehouse.AE.Diagnosis FactAE
		where
			FactAE.SourceDiagnosisCode = Diagnosis.ReferenceCode
		)

	union all

	select
		 Diagnosis.ReferenceCode
		,Diagnosis.Reference
		,Diagnosis.ReferenceParentCode
		,Diagnosis.DisplayOrder
	from
		Warehouse.AE.Reference Diagnosis
	where
		Diagnosis.ReferenceCode in
		(
		select distinct
			 Diagnosis.ReferenceParentCode
		from
			Warehouse.AE.Reference Diagnosis
		where
			exists
			(
			select
				1
			from
				Warehouse.AE.Diagnosis FactAE
			where
				FactAE.SourceDiagnosisCode = Diagnosis.ReferenceCode
			)
		)
	) Diagnosis


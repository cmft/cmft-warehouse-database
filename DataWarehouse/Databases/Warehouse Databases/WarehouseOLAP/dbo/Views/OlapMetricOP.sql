﻿
create view [dbo].[OlapMetricOP] as

select
	 MetricBase.MetricCode
	,MetricBase.Metric
	,MetricBase.MetricParentCode
from
	dbo.MetricOPBase MetricBase


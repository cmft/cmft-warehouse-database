﻿


CREATE VIEW [dbo].[OlapCOMSpokenLanguage] 

AS

--Grab all codes used within referrals table.
--Reference type code not used as not one distinct set of code types are used - ones used are unknown and random..
SELECT DISTINCT
	 SpokenLanguageID = SpokenLanguage.LookupID
	,SpokenLanguageCode = SpokenLanguage.LookupCode
	,SpokenLanguage  = SpokenLanguage.LookupDescription

FROM
	warehouse.COM.Encounter
	INNER JOIN warehouse.com.LookupBase SpokenLanguage
		ON Encounter.PatientSpokenLanguageID = SpokenLanguage.LookupID



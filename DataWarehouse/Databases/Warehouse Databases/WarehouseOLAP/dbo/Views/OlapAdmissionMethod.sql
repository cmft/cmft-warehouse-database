﻿

CREATE view [dbo].[OlapAdmissionMethod] as

SELECT
	 AdmissionMethod.AdmissionMethodCode
	,AdmissionMethod.AdmissionMethod AdmissionMethod
	,AdmissionMethod.NationalAdmissionMethodCode
	,coalesce(AdmissionMethodNational.AdmissionMethod, 'Unassigned') NationalAdmissionMethod
	,AdmissionMethod.AdmissionMethodTypeCode
	,AdmissionMethodType.AdmissionMethodType
FROM 
	Warehouse.PAS.AdmissionMethod AdmissionMethod

left join Warehouse.WH.AdmissionMethod AdmissionMethodNational
on	AdmissionMethodNational.AdmissionMethodCode = AdmissionMethod.NationalAdmissionMethodCode

inner join Warehouse.PAS.AdmissionMethodType AdmissionMethodType
on	AdmissionMethodType.AdmissionMethodTypeCode = AdmissionMethod.AdmissionMethodTypeCode

union all

SELECT
	 'NA' AdmissionMethodCode
	,'N/A' AdmissionMethod
	,'NA' NationalAdmissionMethodCode
	,'N/A' NationalAdmissionMethod
	,'N' AdmissionMethodTypeCode
	,'N/A' AdmissionMethodType



﻿


/* alert  */


create view [dbo].[OlapObservationAlert]
as
SELECT
	[AlertRecno]
      ,[SourceUniqueID]
      ,[CasenoteNumber]
      ,[DateOfBirth]
      ----,[SpecialtyCode]
      ----,[WardCode]
      ----,[AdmissionSourceUniqueID]
      ----,[TypeCode]
      ----,[ReasonCode]
      ----,[ObservationSetSourceUniqueID]
      ,[CreatedDate]
      ,[CreatedTime]
      ----,[CreatedTimeOfDay]
      ,[BedsideDueDate]
      ,[BedsideDueTime]
      ----,[BedsideDueTimeOfDay]
      ,[EscalationDate]
      ,[EscalationTime]
      ----,[EscalationTimeOfDay]
      ,[AcceptedDate]
      ,[AcceptedTime]
      ----,[AcceptedTimeOfDay]
      ----,[AttendedByCode]
      ----,[AttendanceTypeCode]
      --,[Note]
      ----,[CurrentSeverityCode]
      ----,[CurrentClinicianSeniorityCode]
      ----,[AcceptanceRemindersRemaining]
      ----,[ChainSequenceNumber]
      --,[NextReminderTime]
      ,[ClosedDate]
      ,[ClosedTime]
      ----,[ClosedTimeOfDay]
      ----,[ClosureReasonCode]
      ----,[ClosedByCode]
      ----,[CancelledByObservationSetSourceUniqueID]
      --,[DurationMinutes]
      ----,[OutOfHoursCode]
      --,[AgeCode]
      ----,[ContextCode]
      --,[Cases]
  FROM [dbo].[BaseObservationAlert]

  union all


  SELECT
	[AlertRecno] = -1
      ,[SourceUniqueID] = -1
      ,[CasenoteNumber] = 'N/A'
      ,[DateOfBirth] = '1 jan 1900'
      ----,[SpecialtyCode]
      ----,[WardCode]
      ----,[AdmissionSourceUniqueID]
      ----,[TypeCode]
      ----,[ReasonCode]
      ----,[ObservationSetSourceUniqueID]
      ,[CreatedDate] = '1 jan 1900'
      ,[CreatedTime] = '1 jan 1900'
      ----,[CreatedTimeOfDay]
      ,[BedsideDueDate] = '1 jan 1900'
      ,[BedsideDueTime] = '1 jan 1900'
      ----,[BedsideDueTimeOfDay]
      ,[EscalationDate] = '1 jan 1900'
      ,[EscalationTime] = '1 jan 1900'
      ----,[EscalationTimeOfDay]
      ,[AcceptedDate] = '1 jan 1900'
      ,[AcceptedTime] = '1 jan 1900'
      ----,[AcceptedTimeOfDay]
      ----,[AttendedByCode]
      ----,[AttendanceTypeCode]
      --,[Note]
      ----,[CurrentSeverityCode]
      ----,[CurrentClinicianSeniorityCode]
      ----,[AcceptanceRemindersRemaining]
      ----,[ChainSequenceNumber]
      --,[NextReminderTime]
      ,[ClosedDate] = '1 jan 1900'
      ,[ClosedTime] = '1 jan 1900'
      ----,[ClosedTimeOfDay]
      ----,[ClosureReasonCode]
      ----,[ClosedByCode]
      ----,[CancelledByObservationSetSourceUniqueID]
      --,[DurationMinutes]
      ----,[OutOfHoursCode]
      --,[AgeCode]
      ----,[ContextCode]
      --,[Cases]




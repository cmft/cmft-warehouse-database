﻿


CREATE view [dbo].[OlapObservationOverallAssessedStatus] as 

select
	OverallAssessedStatusID
	,OverallAssessedStatusCode
	,OverallAssessedStatus
	,OverallAssessedStatusGroup
	,OverallAssessedStatusScore
from
	Warehouse.Observation.OverallAssessedStatus

union all

select
	'-1'
	,'N/A'
	,'N/A'
	,'N/A'
	,0




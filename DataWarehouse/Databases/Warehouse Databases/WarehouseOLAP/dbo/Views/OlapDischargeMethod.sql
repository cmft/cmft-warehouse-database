﻿



CREATE view [dbo].[OlapDischargeMethod] as 

select
	PASDischargeMethod.DischargeMethodCode
	,PASDischargeMethod.DischargeMethod
	,NationalDischargeMethodCode = coalesce(DischargeMethod.DischargeMethodCode, 'N/A')
	,NationalDischargeMethod = coalesce(DischargeMethod.DischargeMethod, 'N/A')
	
from
	Warehouse.PAS.DischargeMethod PASDischargeMethod

left join Warehouse.WH.DischargeMethod
on	PASDischargeMethod.NationalDischargeMethodCode = DischargeMethod.DischargeMethodCode


union 

select
	'##'
	,'N/A'
	,'N/A'
	,'N/A'



﻿



CREATE view [dbo].[OlapDiagnosis] as
select
	 Diagnosis.DiagnosisCode

	,Diagnosis = 
		Diagnosis.DiagnosisCode + ' - ' + Diagnosis.Diagnosis

	,DiagnosisChapterCode

	,DiagnosisChapter =
		Diagnosis.DiagnosisChapterCode + ' - ' + Diagnosis.DiagnosisChapter
	,[IsCharlsonFlag] = 
				coalesce([IsCharlsonFlag], 0)
	,[IsConnectingForHealthMandatoryFlag] = 
				coalesce([IsConnectingForHealthMandatoryFlag], 0)
	,[IsAlwaysPresentFlag] = 
				coalesce([IsAlwaysPresentFlag], 0)
	,[IsLongTermFlag] = 
				coalesce([IsLongTermFlag], 0)

from
	Warehouse.PAS.Diagnosis Diagnosis


union all

select distinct
	 DiagnosisCode = FactAPC.PrimaryDiagnosisCode

	,Diagnosis = 
		FactAPC.PrimaryDiagnosisCode + ' - No Description'

	,'##'

	,'N/A'
	,0
	,0
	,0
	,0

from
	dbo.FactAPC
where
	not exists
	(
	select
		1
	from
		Warehouse.PAS.Diagnosis Diagnosis
	where
		Diagnosis.DiagnosisCode = FactAPC.PrimaryDiagnosisCode
	)
and	not FactAPC.PrimaryDiagnosisCode = '##'

union all

select
	 '##'
	,'N/A'
	,'##'
	,'N/A'
	,0
	,0
	,0
	,0




﻿

create view [dbo].[OlapWLDuration] as

select
	 DurationCode
	,Duration
	,APCWLDurationBandCode
	,APCWLDurationBand
	,OPWLDurationBandCode
	,OPWLDurationBand
from
	dbo.DurationBase2



﻿
create view [dbo].[OlapMetricIP] as

select
	 MetricBase.MetricCode
	,MetricBase.Metric
	,MetricBase.MetricParentCode
from
	dbo.MetricIPBase MetricBase


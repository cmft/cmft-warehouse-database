﻿

CREATE VIEW [dbo].[OlapCOMStaffTeam] 

AS

SELECT
	 StaffTeamID  
	,StaffTeamCode 
	,StaffTeam 
	,StaffTeamHealthOrgOwner 

FROM (

	SELECT 
		 StaffTeamID  
		,StaffTeamCode 
		,StaffTeam = StaffTeam + ' (' + StaffTeamCode + ')'
		,StaffTeamHealthOrgOwner 
	FROM warehouse.com.StaffTeam

	) StaffTeam


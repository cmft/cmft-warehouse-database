﻿



CREATE VIEW [dbo].[OlapAEAttendanceDisposal]

AS


select
	 AttendanceDisposalCode
	,AttendanceDisposal
	,AttendanceDisposalNationalCode = coalesce(
										 AttendanceDisposalNationalCode
										,'14'
									  )
from
	Warehouse.AE.AttendanceDisposal

union all

select distinct
	 AttendanceDisposalCode =
		coalesce(
			SourceAttendanceDisposalCode
			,-1
		)
	,AttendanceDisposal =
		coalesce(
			convert(varchar, SourceAttendanceDisposalCode) + ' - No Description'
			,'N/A'
		)
	,AttendanceDisposalNationalCode = CASE 
										WHEN SourceAttendanceDisposalCode = -2 THEN '02' --Adastra defualt code
										WHEN SourceAttendanceDisposalCode IS NULL  THEN 'N/A' 
										ELSE  '14' 
									  END

from
	Warehouse.AE.Encounter AEEncounter

where
	not exists
	(
	select
		1
	from
		Warehouse.AE.AttendanceDisposal AttendanceDisposal
	where
		AttendanceDisposal.AttendanceDisposalCode = AEEncounter.SourceAttendanceDisposalCode
	)





﻿

CREATE VIEW [dbo].[OlapCOMCaseType] 

AS

SELECT
	 Code  
	 
	,[Type] 
	
	,TypeGroup 
	
	,TypeGroupChannel 

FROM 
	(
	SELECT 
		 Code  
		 
		,[Type] 
		
		,TypeGroup 
		
		,TypeGroupChannel 
		
	FROM 
		warehouse.com.CaseType 
	
	)CaseType



﻿

CREATE VIEW [dbo].[OlapCOMEncounter]

AS

SELECT DISTINCT
	 EncounterRecNo
	,SourceEncounterID
--	,SourceUniqueID
----	,SpecialtyID
----	,StaffTeamID
----	,ProfessionalCarerID
----	,SeenByProfessionalCarerID
----	,EncounterProfessionalCarerID
	,StartDate
	,StartTime
	,EndTime
	,ArrivedTime
	,SeenTime
	,DepartedTime
----	,AttendedID
----	,OutcomeID
	,EncounterOutcomeCode
--	,EncounterDuration
----	,ReferralID
----	,ProfessionalCarerEpisodeID
	,PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
--	,PatientNHSNumberStatusIndicator
	,PatientIDLocalIdentifier 
	,PatientIDPASNumber 
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID
----	,PatientTitleID
	,PatientForename
	,PatientSurname
----	,PatientAddress1
----	,PatientAddress2
----	,PatientAddress3
----	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
--	,PatientSexID
--	,PatientEthnicGroupID
--	,PatientCurrentRegisteredPracticeCode
--	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
--	,PatientEncounterRegisteredGPCode
----	,EncounterTypeID
----	,ScheduleTypeID
----	,ScheduleReasonID
----	,CanceledReasonID
--	,CanceledTime
----	,CancelledByID
----	,MoveReasonID
----	,MoveTime
----	,ServicePointID
----	,ServicePointSessionID
--	,MoveCount
----	,LocationTypeID
----	,LocationDescription
----	,LocationTypeHealthOrgID
----	,EncounterLocationID
----	,ServicePointStaysID
----	,WaitingListID
--	,PlannedAttendees
--	,ActualAttendees
----	,ProviderSpellID
----	,RTTStatusID
--	,EarliestReasinableOfferTime
--	,EncounterInterventionsCount
	,CreatedTime
	,ModifiedTime
	,CreatedBy
	,ModifiedBy
----	,Archived
----	,ParentID
----	,HealthOrgOwnerID
----	,RecordStatus
	,DerivedFirstAttendanceFlag
	,Comment
	,CommissionerCode  
	,PctOfResidence 
	,LocalAuthorityCode  
	,DistrictOfResidence 
	,ReferralReceivedDate

FROM 
	WarehouseOLAP.dbo.BaseCOMEncounter

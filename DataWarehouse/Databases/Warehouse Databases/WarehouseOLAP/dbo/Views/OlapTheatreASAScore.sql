﻿CREATE view [dbo].[OlapTheatreASAScore] as

select
	 TheatreASAScore.ASAScoreCode
	,TheatreASAScore.ASAScore
from
	Warehouse.Theatre.ASAScore TheatreASAScore

union all

select
	0
	,'N/A'

﻿


CREATE view [dbo].[OlapWard] as

select
	 WardCode
	,Ward = Ward + ' (' + WardCode + ')'
from
(
select
	 PASWard.WardCode
	,Ward =
		coalesce(
			 Ward.Ward
			,PASWard.Ward
		)
from
	Warehouse.PAS.Ward PASWard

left join Warehouse.BedComplement.Ward
on	Ward.WardCode = PASWard.WardCode

union all

select
	 Ward.WardCode
	,Ward.Ward
from
	Warehouse.BedComplement.Ward

where
	not exists
	(
	select
		1
	from
		Warehouse.PAS.Ward PASWard
	where
		PASWard.WardCode = Ward.WardCode
	)
) Ward

union all

select
	 'N/A'
	,'N/A'




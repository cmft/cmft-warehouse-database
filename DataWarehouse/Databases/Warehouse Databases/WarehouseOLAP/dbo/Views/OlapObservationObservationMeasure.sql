﻿





CREATE view [dbo].[OlapObservationObservationMeasure]

as

select
	[ObservationMeasureID]
	,[ParentObservationMeasureID]
	,[ObservationMeasureCode]
	,[ObservationMeasure]
	,ObservationMeasureLabel = [ObservationMeasureCode] + ' - ' + [ObservationMeasure]
	,[Complex]
	,[Unit]
	,[Format]
	,[MinimumValue]
	,[MaximumValue]
	,[StartTime]
	,[EndTime]
	,[Deleted]
	--,[CreatedByUserID]
	--,[CreatedTime]
	--,[LastModifiedByUserID]
	--,[LastModifiedTime]
	--,[InterfaceCode]

from
	Warehouse.Observation.ObservationMeasureBase


union all

select
	'-1'
	,'-1'
	,'N/A'
	,'N/A'
	,'N/A'
	,'0'
	,'N/A'
	,'N/A'
	,null
	,null
	,null
	,null
	,'0'













﻿
CREATE VIEW [dbo].[OlapMetricCOMOutcomes] as

SELECT
	 MetricBase.MetricCode
	,MetricBase.Metric
	,MetricBase.MetricParentCode

FROM
	dbo.MetricCOMBase MetricBase

WHERE 
	MetricParentCode = 'OCOM'


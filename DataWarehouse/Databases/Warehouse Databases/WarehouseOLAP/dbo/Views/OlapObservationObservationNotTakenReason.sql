﻿


CREATE view [dbo].[OlapObservationObservationNotTakenReason]

as

select
	ObservationNotTakenReasonID
	,ObservationNotTakenReasonCode
    ,ObservationNotTakenReason

from
	[Warehouse].Observation.ObservationNotTakenReason

--union all

--select distinct
--	 [AlertTypeCode] = Fact.[TypeCode]
--	,AlertType =
--				cast(Fact.[TypeCode] as varchar) + ' - No Description'
--from
--	[dbo].[FactObservationAlert] Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		[warehouse].[Observation].[AlertType] alerttype
--	where
--		alerttype.[AlertTypeCode] = Fact.[TypeCode]
--	)

union all

select
	'-1'
	,'N/A'
	,'N/A'





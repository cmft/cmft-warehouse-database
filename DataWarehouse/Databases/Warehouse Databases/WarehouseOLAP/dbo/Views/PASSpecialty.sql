﻿
CREATE view [dbo].[PASSpecialty] as

select
	 PASSpecialty.SpecialtyCode
	,PASSpecialty.Specialty
	,PASSpecialty.NationalSpecialtyCode
	,PASSpecialty.TreatmentFunctionFlag
from
	Warehouse.PAS.Specialty PASSpecialty


union all

select distinct
	 Fact.SpecialtyCode
	,Consultant =
		Fact.SpecialtyCode + ' - No Description'

	,NationalSpecialtyCode =
		'N/A'

	,TreatmentFunctionFlag =
		'False'

from
	dbo.FactPathway Fact
where
	not exists
	(
	select
		1
	from
		Warehouse.PAS.Specialty PASSpecialty
	where
		PASSpecialty.SpecialtyCode = Fact.SpecialtyCode
	)

union all

select
	'N/A'
	,'N/A'
	,'N/A'
	,'False'


﻿create view [dbo].[OlapDuration] as

select
	 DurationCode
	,Duration
	,DurationParentCode
from
	dbo.DurationBase

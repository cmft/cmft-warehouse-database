﻿
CREATE view [dbo].[OlapPractice] as

select
--	 PracticeRecno = ActivePractice.EntityXrefRecno

	 PracticeCode = convert(varchar(10), ActivePractice.EntityCode)
 
	,PCTCode = convert(varchar(10), coalesce(PCT.OrganisationCode, 'N/A'))

	,HealthAuthorityCode = convert(varchar(10), coalesce(PCT.HACode, 'N/A'))
 
	,Practice =
		convert(varchar(255), rtrim(ActivePractice.EntityCode) + ' - ' + coalesce(Practice.Organisation, 'Unknown'))

	,PCT =
		convert(varchar(255), coalesce(rtrim(PCT.OrganisationCode), 'N/A') + ' - ' + coalesce(PCT.Organisation, 'Unknown'))

	,HealthAuthority =
		convert(varchar(255),
			coalesce(
				HealthAuthority.Organisation
				,case
				when PCT.HACode is null
				then 'Unknown'
				else rtrim(PCT.HACode) + ' - Unknown'
				end
			)
		)

	,LocalPCT =
		convert(varchar(255), 
			case
			when exists
				(
				select
					1
				from
					Warehouse.dbo.EntityLookup LocalPCT
				where
					LocalPCT.EntityCode = Practice.ParentOrganisationCode
				and	LocalPCT.EntityTypeCode = 'LOCALPCT'
				)
			then coalesce(rtrim(PCT.OrganisationCode), 'N/A') + ' - ' + coalesce(PCT.Organisation, 'Unknown')
			else 'Other'
			end
		)

from
	dbo.EntityXref ActivePractice

left join Organisation.dbo.Practice Practice
on	Practice.OrganisationCode = ActivePractice.EntityCode

left join Organisation.dbo.PCT PCT
on	PCT.OrganisationCode = Practice.ParentOrganisationCode

left join Organisation.dbo.HealthAuthority HealthAuthority
on	HealthAuthority.OrganisationCode = PCT.HACode

where
	ActivePractice.EntityTypeCode = 'OLAPPRACTICE'
and	ActivePractice.XrefEntityTypeCode = 'OLAPPRACTICE'


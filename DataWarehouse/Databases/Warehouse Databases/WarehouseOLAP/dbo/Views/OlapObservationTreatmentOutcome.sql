﻿



CREATE view dbo.OlapObservationTreatmentOutcome

as


select
	TreatmentOutcomeID
	,TreatmentOutcomeCode
	,TreatmentOutcome

from
	Warehouse.Observation.TreatmentOutcome

union all

select
	'-1'
	,'N/A'
	,'N/A'






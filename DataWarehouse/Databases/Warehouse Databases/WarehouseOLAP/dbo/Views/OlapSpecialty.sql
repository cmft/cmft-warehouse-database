﻿
CREATE view [dbo].[OlapSpecialty] as

select distinct
	 SpecialtyCode =
		NationalSpecialty.NationalSpecialtyCode

	,Specialty =
		NationalSpecialty.NationalSpecialtyCode + ' - ' + NationalSpecialty.NationalSpecialty

from
	Warehouse.PAS.SpecialtyMap NationalSpecialty

union all

select
	SpecialtyCode =
		'N/A'

	,Specialty =
		'N/A'


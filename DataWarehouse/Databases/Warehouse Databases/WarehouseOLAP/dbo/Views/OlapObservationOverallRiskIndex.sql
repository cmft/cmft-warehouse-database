﻿
create view [dbo].[OlapObservationOverallRiskIndex]

as

select
	[OverallRiskIndexCode]
    ,[OverallRiskIndex]

from
	[warehouse].[Observation].[OverallRiskIndex]
	

--union all

--select distinct
--	 [OverallRiskIndexCode] = Fact.[OverallRiskIndexCode]
--	,[OverallRiskIndex] =
--				cast(Fact.[OverallRiskIndexCode] as varchar) + ' - No Description'
--from
--	[dbo].[FactObservationSet] Fact
--where
--	not exists
--	(
--	select
--		1
--	from
--		[warehouse].[Observation].[OverallRiskIndex] overallriskindex
--	where
--		overallriskindex.[OverallRiskIndexCode] = Fact.[OverallRiskIndexCode]
--	)

union

select
	'-1'
	,'N/A'



﻿
CREATE VIEW [dbo].[OlapMetricCOMCaseload] as

SELECT
	 MetricBase.MetricCode
	,MetricBase.Metric
	,MetricBase.MetricParentCode

FROM
	dbo.MetricCOMBase MetricBase

WHERE 
	MetricParentCode = 'CSLD'



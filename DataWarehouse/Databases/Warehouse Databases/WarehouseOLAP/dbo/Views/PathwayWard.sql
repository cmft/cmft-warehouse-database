﻿create view PathwayWard as

select
	PathwayBenchmarkWard.WardCode

	,Ward =
		coalesce(
			Ward.Ward
			,PathwayBenchmarkWard.WardCode + ' - No Description'
		)

	,PathwayBenchmarkWard.BenchmarkMinutes
from
	Warehouse.Pathway.PathwayBenchmarkWard

left join Warehouse.PAS.Ward
on	Ward.WardCode = PathwayBenchmarkWard.WardCode

union all

select
	'N/A'
	,'N/A'
	,null

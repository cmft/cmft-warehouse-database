﻿

CREATE VIEW dbo.OlapCOMHealthOrg

AS

SELECT 
	 HealthOrgID
	,HealthOrgTypeID
	,HealthOrgCode
	,HealthOrg = HealthOrg + ' (' + HealthOrgCode + ')'
	,HealthOrgParentID
	,StartTime
	,EndTime
	,ArchiveFlag
	,HealthOrgOwnerID
FROM 
	warehouse.COM.HealthOrg
﻿

CREATE view [dbo].[OlapObservationOutOfHours]

as

select
	OutOfHoursCode = 1
    ,OutOfHours = 'Out Of Hours'

union

select
	OutOfHoursCode = 0
    ,OutOfHours = 'Office Hours'

union

select
	OutOfHoursCode = -1
    ,OutOfHours = 'N/A'




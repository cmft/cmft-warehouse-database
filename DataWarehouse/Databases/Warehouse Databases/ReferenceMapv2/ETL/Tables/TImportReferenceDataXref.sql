﻿CREATE TABLE [ETL].[TImportReferenceDataXref](
	[AttributeCode] [varchar](20) NOT NULL,
	[ContextCode] [varchar](20) NOT NULL,
	[ValueCode] [varchar](255) NOT NULL,
	[ContextXrefCode] [varchar](20) NOT NULL,
	[ValueXrefCode] [varchar](255) NOT NULL,
 CONSTRAINT [PK_TImportReferenceDataXref] PRIMARY KEY CLUSTERED 
(
	[AttributeCode] ASC,
	[ContextCode] ASC,
	[ValueCode] ASC,
	[ContextXrefCode] ASC,
	[ValueXrefCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
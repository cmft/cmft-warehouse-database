﻿CREATE TABLE [ETL].[TImportReferenceData](
	[AttributeCode] [varchar](20) NOT NULL,
	[ContextCode] [varchar](20) NOT NULL,
	[ValueCode] [varchar](255) NOT NULL,
	[Value] [varchar](512) NULL,
 CONSTRAINT [PK_TImportReferenceData] PRIMARY KEY CLUSTERED 
(
	[AttributeCode] ASC,
	[ContextCode] ASC,
	[ValueCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
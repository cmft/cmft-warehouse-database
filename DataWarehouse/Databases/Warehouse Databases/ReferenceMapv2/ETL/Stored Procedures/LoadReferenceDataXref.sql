﻿CREATE procedure [ETL].[LoadReferenceDataXref]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@inserted int


--populate ETL.TImportReferenceData with distinct source and xref values
insert
into
	ETL.TImportReferenceData
(
	AttributeCode
	,ContextCode
	,ValueCode
	,Value
)
select
	AttributeCode
	,ContextCode
	,ValueCode
	,Value = 'Not Specified'
from
	(
	select
		TImportReferenceDataXref.AttributeCode
		,TImportReferenceDataXref.ContextCode
		,TImportReferenceDataXref.ValueCode
	from
		ETL.TImportReferenceDataXref

	union all

	select
		TImportReferenceDataXref.AttributeCode
		,TImportReferenceDataXref.ContextXrefCode
		,TImportReferenceDataXref.ValueXrefCode
	from
		ETL.TImportReferenceDataXref
	) TImportReferenceDataXref
where
	not exists --not already in Map.Value
	(
	select
		1
	from
		Map.Value

	inner join Map.AttributeContext
	on	AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID

	inner join Map.Context
	on	Context.ContextID = AttributeContext.ContextID

	where
		Attribute.AttributeCode = TImportReferenceDataXref.AttributeCode
	and	Context.ContextCode = TImportReferenceDataXref.ContextCode
	and	Value.ValueCode = TImportReferenceDataXref.ValueCode
	)

and	not exists --not already in ETL.TImportReferenceData
	(
	select
		1
	from
		ETL.TImportReferenceData
	where
		TImportReferenceData.AttributeCode = TImportReferenceDataXref.AttributeCode
	and	TImportReferenceData.ContextCode = TImportReferenceDataXref.ContextCode
	and	TImportReferenceData.ValueCode = TImportReferenceDataXref.ValueCode
	)

if @@rowcount > 0
	exec ETL.LoadReferenceData


insert
into
	Map.ValueXref
(
	 ValueID
	,ValueXrefID
	,Created
	,ByWhom
)
select
	 Value.ValueID
	 ,ValueXref.ValueID
	 ,getdate()
	 ,suser_name()
from
	ETL.TImportReferenceDataXref

inner join Map.Attribute
on	Attribute.AttributeCode = TImportReferenceDataXref.AttributeCode

inner join Map.Context
on	Context.ContextCode = TImportReferenceDataXref.ContextCode

inner join Map.AttributeContext
on	AttributeContext.AttributeID = Attribute.AttributeID
and	AttributeContext.ContextID = Context.ContextID

inner join Map.Value
on	Value.AttributeContextID = AttributeContext.AttributeContextID
and	Value.ValueCode = TImportReferenceDataXref.ValueCode

inner join Map.Context ContextXref
on	ContextXref.ContextCode = TImportReferenceDataXref.ContextXrefCode

inner join Map.AttributeContext AttributeContextXref
on	AttributeContextXref.AttributeID = Attribute.AttributeID
and	AttributeContextXref.ContextID = ContextXref.ContextID

inner join Map.Value ValueXref
on	ValueXref.AttributeContextID = AttributeContextXref.AttributeContextID
and	ValueXref.ValueCode = TImportReferenceDataXref.ValueXrefCode

where
	not exists --this Value already has a mapping to the Xref Context
	(
	select
		1
	from
		Map.ValueXref ExistingValueXref
	where
		ExistingValueXref.ValueID = Value.ValueID
	and	exists
		(
		select
			1
		from
			 Map.Value
		where
			Value.ValueID = ExistingValueXref.ValueXrefID
		and	Value.AttributeContextID = AttributeContextXref.AttributeContextID		
		)	
	)



select
	 @inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + cast(@Elapsed as varchar) + ' minutes'

print @Stats

exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿CREATE view [ETL].[TLoadLocalResultSetResult] as

--prepares reference data from ICE to seed the Local Result and Result Set values
--only presents unique codes, codes with multiple descriptions need to be added manually

select
	AttributeCode
	,ContextCode
	,ValueCode
	,Value
from
	(
	select 
		AttributeCode = 'RESULTSET'
		,ContextCode = 'L'
		,ValueCode = CENICEInvestigationReference.InvestigationCode
		,Value = CENICEInvestigationReference.InvestigationName
	from
		[$(Warehouse)].Result.CENICEInvestigationReference
	where
		not exists
			(
			select
				1
			from
				[$(Warehouse)].Result.CENICEInvestigationReference SameCodeDifferentDescription
			where
				SameCodeDifferentDescription.InvestigationCode = CENICEInvestigationReference.InvestigationCode
			and SameCodeDifferentDescription.InvestigationName != CENICEInvestigationReference.InvestigationName
			)

	union

	select 
		AttributeCode = 'RESULT'
		,ContextCode = 'L'
		,ValueCode = CENICEResultReference.ResultCode
		,Value = CENICEResultReference.ResultName
	from
		[$(Warehouse)].Result.CENICEResultReference

	where
		not exists
			(
			select
				1
			from
				[$(Warehouse)].Result.CENICEResultReference SameCodeDifferentDescription
			where
				SameCodeDifferentDescription.ResultCode = CENICEResultReference.ResultCode
			and SameCodeDifferentDescription.ResultName != CENICEResultReference.ResultName
			)	

	) LocalResultSetResult

where
	not exists
		(
		select
			1
		from
			ETL.TImportReferenceData
		where
			TImportReferenceData.AttributeCode = LocalResultSetResult.AttributeCode
		and	TImportReferenceData.ContextCode = LocalResultSetResult.ContextCode
		and	TImportReferenceData.ValueCode = LocalResultSetResult.ValueCode
		)
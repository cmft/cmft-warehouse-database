﻿
USE [master]
GO

/****** Object:  Database [ReferenceMapv2]    Script Date: 25/08/2015 16:36:17 ******/
CREATE DATABASE [ReferenceMapv2] ON  PRIMARY 
( NAME = N'ReferenceMap', FILENAME = N'F:\Data\ReferenceMapV2.mdf' , SIZE = 217792KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ReferenceMap_log', FILENAME = N'G:\Log\ReferenceMapV2_log.ldf' , SIZE = 757248KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [ReferenceMapv2] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ReferenceMapv2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [ReferenceMapv2] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [ReferenceMapv2] SET ANSI_NULLS OFF
GO

ALTER DATABASE [ReferenceMapv2] SET ANSI_PADDING OFF
GO

ALTER DATABASE [ReferenceMapv2] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [ReferenceMapv2] SET ARITHABORT OFF
GO

ALTER DATABASE [ReferenceMapv2] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [ReferenceMapv2] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [ReferenceMapv2] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [ReferenceMapv2] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [ReferenceMapv2] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [ReferenceMapv2] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [ReferenceMapv2] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [ReferenceMapv2] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [ReferenceMapv2] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [ReferenceMapv2] SET  DISABLE_BROKER
GO

ALTER DATABASE [ReferenceMapv2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [ReferenceMapv2] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [ReferenceMapv2] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [ReferenceMapv2] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [ReferenceMapv2] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [ReferenceMapv2] SET READ_COMMITTED_SNAPSHOT ON
GO

ALTER DATABASE [ReferenceMapv2] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [ReferenceMapv2] SET RECOVERY FULL
GO

ALTER DATABASE [ReferenceMapv2] SET  MULTI_USER
GO

ALTER DATABASE [ReferenceMapv2] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [ReferenceMapv2] SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'ReferenceMapv2', N'ON'
GO

USE [ReferenceMapv2]
GO

/****** Object:  Table [Audit].[Log]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SchemaChangeLog]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ETL].[TImportReferenceData]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ETL].[TImportReferenceDataXref]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[Attribute]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[AttributeContext]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Map].[ColumnBase]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[ColumnType]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[Context]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[TableBase]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[TableType]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[Value]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[ValueXref]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  UserDefinedFunction [Map].[GetAttributeValue]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ETL].[TLoadLocalResultSetResult]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[ContextAttribute]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[MappedAttribute]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[TableTree]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  StoredProcedure [Audit].[WriteLogEvent]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETL].[LoadReferenceData]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ETL].[LoadReferenceDataXref]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildDefaultXrefs]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildLocationValues]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildMissingNonBaseContextValues]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildMissingValues]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildMissingXrefs]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[DeleteDuplicateXrefs]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  DdlTrigger [tr_DDL_SchemaChangeLog]    Script Date: 25/08/2015 16:36:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [ReferenceMapv2] SET  READ_WRITE
GO

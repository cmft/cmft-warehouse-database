﻿ALTER ROLE [db_datareader] ADD MEMBER [THT\WCollier]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Wendy.Collier]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Sec - Warehouse BI Developers]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [CMMC\Sec - Warehouse BI Developers]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\richard.adams]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Rachel.Reed]
GO
ALTER ROLE [db_owner] ADD MEMBER [CMMC\Phil.Orrell]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Phil.Orrell]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [CMMC\Phil.Orrell]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\paul.westhead]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Mohamed.Athman]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\gordon.fenton]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Gareth.Cunnah]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Colin.Hunter]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [CMMC\Colin.Hunter]
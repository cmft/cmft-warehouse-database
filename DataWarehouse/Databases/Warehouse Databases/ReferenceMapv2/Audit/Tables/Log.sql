﻿CREATE TABLE [Audit].[Log](
	[LogRecno] [int] IDENTITY(1,1) NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[UserId] [varchar](30) NOT NULL,
	[ProcessCode] [varchar](255) NOT NULL,
	[Event] [varchar](255) NOT NULL,
	[StartTime] [datetime] NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[LogRecno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
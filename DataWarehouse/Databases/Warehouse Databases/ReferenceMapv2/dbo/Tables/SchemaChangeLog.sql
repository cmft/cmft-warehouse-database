﻿CREATE TABLE [dbo].[SchemaChangeLog](
	[SchemaChangeLogID] [int] IDENTITY(1,1) NOT NULL,
	[CreateDate] [datetime] NULL,
	[LoginName] [sysname] NULL,
	[ComputerName] [sysname] NULL,
	[DBName] [sysname] NOT NULL,
	[SQLEvent] [sysname] NOT NULL,
	[Schema] [sysname] NULL,
	[ObjectName] [sysname] NULL,
	[SQLCmd] [nvarchar](max) NOT NULL,
	[XmlEvent] [xml] NOT NULL,
 CONSTRAINT [PK_SchemaChangeLog] PRIMARY KEY CLUSTERED 
(
	[SchemaChangeLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
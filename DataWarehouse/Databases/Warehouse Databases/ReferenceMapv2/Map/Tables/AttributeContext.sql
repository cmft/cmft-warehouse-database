﻿CREATE TABLE [Map].[AttributeContext](
	[AttributeContextID] [int] IDENTITY(1,1) NOT NULL,
	[AttributeID] [int] NOT NULL,
	[ContextID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AttributeContextID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Map].[AttributeContext]  WITH CHECK ADD  CONSTRAINT [FK_AttributeContext_Attribute] FOREIGN KEY([AttributeID])
REFERENCES [Map].[Attribute] ([AttributeID])
GO

ALTER TABLE [Map].[AttributeContext] CHECK CONSTRAINT [FK_AttributeContext_Attribute]
GO
ALTER TABLE [Map].[AttributeContext]  WITH CHECK ADD  CONSTRAINT [FK_AttributeContext_Context] FOREIGN KEY([ContextID])
REFERENCES [Map].[Context] ([ContextID])
GO

ALTER TABLE [Map].[AttributeContext] CHECK CONSTRAINT [FK_AttributeContext_Context]
GO
/****** Object:  Index [IX_AttributeContext]    Script Date: 25/08/2015 16:36:17 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AttributeContext] ON [Map].[AttributeContext]
(
	[AttributeID] ASC,
	[ContextID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
﻿CREATE procedure [Map].[BuildMissingValues]
	 @activityTableID int
	,@NewRows int = 0 output
	,@debug bit = 0
	,@ActivityTableOverride varchar(max) = null
as

--prevent parallel running and consequent duplication of values
set transaction isolation level serializable

begin transaction

	declare @activityTable varchar(max)
	declare @activityCodeColumn varchar(max)
	declare @referenceTable varchar(max)
	declare @referenceCodeColumn varchar(max)
	declare @referenceColumn varchar(max)
	declare @attributeContextID int
	declare @contextCode varchar(max)
	declare @sql varchar(max)

	declare @sqlTemplate varchar(max) = 
		'
if object_id(''tempdb..#DistinctValues'') is not null
begin
	drop table #DistinctValues
end


select distinct
	ValueCode = cast(ActivityTable.<activityCodeColumn> as varchar(MAX))
into
	#DistinctValues
from
	<activityTable> ActivityTable
where
	ActivityTable.ContextCode = ''<contextCode>''
and	ActivityTable.<activityCodeColumn> is not null
;

insert
into
	Map.Value
	(
	AttributeContextID
	,ValueCode
	,Value
	,Created
	,ByWhom
	)
select
	AttributeContextID
	,ValueCode
	,Value
	,Created = GETDATE()
	,ByWhom = SUSER_NAME()
from
	(
	select
		AttributeContextID = <attributeContextID>
		,DistinctValues.ValueCode

		,Value =
			isnull(
				cast(ReferenceTable.<referenceColumn> as varchar(MAX))
				,''No Description''
			)
	from
		#DistinctValues DistinctValues

	left join <referenceTable> ReferenceTable
	on	cast(ReferenceTable.<referenceCodeColumn> as varchar(max)) = DistinctValues.ValueCode

	union

	select
		 AttributeContextID = <attributeContextID>
		,ValueCode = ''-1''
		,Value = ''Not Specified''

	) ActivityTable
where
	not exists
	(
	select
		1
	from
		Map.Value
	where
		Value.ValueCode = ActivityTable.ValueCode
	and	Value.AttributeContextID = <attributeContextID>
	)

;

		'

	declare @sqlTemplateNoReferenceTable varchar(max) = 
		'
if object_id(''tempdb..#DistinctValues'') is not null
begin
	drop table #DistinctValues
end


select distinct
	ValueCode = cast(ActivityTable.<activityCodeColumn> as varchar(MAX))
into
	#DistinctValues
from
	<activityTable> ActivityTable
where
	ActivityTable.ContextCode = ''<contextCode>''
and	ActivityTable.<activityCodeColumn> is not null
;

insert
into
	Map.Value
	(
	AttributeContextID
	,ValueCode
	,Value
	,Created
	,ByWhom
	)
select
	AttributeContextID
	,ValueCode
	,Value
	,Created = GETDATE()
	,ByWhom = SUSER_NAME()
from
	(
	select
		AttributeContextID = <attributeContextID>
		,ValueCode
		,Value = ''No Description''
	from
		#DistinctValues ActivityTable

	union

	select
		AttributeContextID = <attributeContextID>
		,ValueCode = ''-1''
		,Value = ''Not Specified''

	) ActivityTable

where
	not exists
	(
	select
		1
	from
		Map.Value
	where
		Value.ValueCode = ActivityTable.ValueCode
	and	Value.AttributeContextID = <attributeContextID>
	)
		'

	select
		@activityTable =
			coalesce(
				 @ActivityTableOverride
				,TableBase.[Table]
			)
	from
		Map.TableBase
	where
		TableBase.TableID = @activityTableID

	create table #AttributeContext
		(
		AttributeContextID int
		,ContextCode varchar(max)
		)

	--get distinct Attribute/Context from the activity table

	--due to performace issues (specifically with Trafford APC WL data) a left join is used below and
	--any null records are removed in the cursor select statement
	select
		@sql =
			REPLACE(
				'
				insert into #AttributeContext
				select distinct
					 AttributeContext.AttributeContextID
					,Context.ContextCode
				from 
					Map.Attribute

				inner join Map.Context
				on	Context.BaseContext = 1
				and	exists
					(
					select
						1
					from
						<activityTable> ActivityTable
					where
						ActivityTable.ContextCode = Context.ContextCode
					)

				left join Map.AttributeContext
				on	AttributeContext.ContextID = Context.ContextID
				and	AttributeContext.AttributeID = Attribute.AttributeID

				'
				,'<activityTable>'
				,@activityTable
			)

	if @debug = 1
		print @sql


	execute(@sql)

	set @NewRows = 0

	--loop through each context

	declare contextCursor cursor for
	select
		 AttributeContextID
		,ContextCode
	from
		#AttributeContext
	where
		AttributeContextID is not null

	OPEN contextCursor
  
	FETCH NEXT FROM contextCursor
	INTO @attributeContextID, @contextCode

	WHILE @@FETCH_STATUS = 0

	BEGIN

		select
			 @activityCodeColumn = null
			,@referenceTable = null
			,@referenceCodeColumn = null
			,@referenceColumn = null


		--get the reference table code column name and reference table name
		select
			@referenceTable = 
				TableBase.[Table]

			,@referenceCodeColumn =
				'[' + ColumnBase.[Column] + ']'

		from
			Map.ColumnBase

		inner join Map.TableBase
		on	TableBase.TableID = ColumnBase.TableID

		where
			ColumnBase.ColumnTypeID = 2	--Reference Code
		and	ColumnBase.AttributeContextID = @attributeContextID

		--get the reference value (description) column name
		select
			@referenceColumn =
				'[' + ColumnBase.[Column] + ']'
		from
			Map.ColumnBase
		where
			ColumnBase.AttributeContextID = @attributeContextID
		and	ColumnBase.ColumnTypeID = 3	--Reference


	--loop through each activity column code

		declare activityCodeColumnCursor cursor for

		--get the activity table column name
		select
			ActivityCodeColumn = '[' + ColumnBase.[Column] + ']'
		from
			Map.ColumnBase
		where
			ColumnBase.TableID = @activityTableID
		and	ColumnBase.AttributeContextID = @attributeContextID
		and	ColumnBase.ColumnTypeID = 1	--Activity Code

		OPEN activityCodeColumnCursor
	  
		FETCH NEXT FROM activityCodeColumnCursor
		INTO @activityCodeColumn

		WHILE @@FETCH_STATUS = 0

		BEGIN


			select @sql = 
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
					 @sqlTemplate
					,'<activityTable>'
					,@activityTable
				)
					,'<activityCodeColumn>'
					,@activityCodeColumn
				)	
					,'<attributeContextID>'
					,@attributeContextID
				)
					,'<referenceColumn>'
					,@referenceColumn
				)
					,'<referenceTable>'
					,@referenceTable
				)
					,'<referenceCodeColumn>'
					,@referenceCodeColumn
				)
					,'<contextCode>'
					,@contextCode
				)


			if @sql is null
				select @sql = 
					REPLACE(
					REPLACE(
					REPLACE(
					REPLACE(
						 @sqlTemplateNoReferenceTable
						,'<activityTable>'
						,@activityTable
					)
						,'<activityCodeColumn>'
						,@activityCodeColumn
					)	
						,'<attributeContextID>'
						,@attributeContextID
					)
						,'<contextCode>'
						,@contextCode
					)


			if @sql is not null
				if @debug = 1
					print @sql
				else
				begin
					print 'Column: ' + @activityCodeColumn + ', Context: ' + @contextCode
					execute(@sql)
					set @NewRows = @NewRows + @@ROWCOUNT
				end


			FETCH NEXT FROM activityCodeColumnCursor
			INTO @activityCodeColumn
		END
	  
		CLOSE activityCodeColumnCursor
		DEALLOCATE activityCodeColumnCursor


		FETCH NEXT FROM contextCursor
		INTO @attributeContextID, @contextCode
	END
  
	CLOSE contextCursor
	DEALLOCATE contextCursor

commit
﻿CREATE proc [Map].[BuildLocationValues] as

declare @LocationAttributeID int = 
						(
						select
							AttributeID
						from
							Map.Attribute
						where
							AttributeCode = 'LOCATION'
						)

insert into [Map].[AttributeContext]
(
	AttributeID
	,ContextID
)

select
	@LocationAttributeID
	,Context.ContextID
from
	Map.AttributeContext

inner join Map.Context
on	AttributeContext.ContextID = Context.ContextID
and	Context.ContextCode not in ('L', 'N')

inner join Map.Attribute
on	AttributeContext.AttributeID = Attribute.AttributeID
and	Attribute.AttributeCode in ('WARD', 'SITE')

where
	not exists
		(
		select
			1
		from
			Map.AttributeContext AttributeContextAlreadyExists

		where
			AttributeContextAlreadyExists.ContextID = Context.ContextID
		and AttributeContextAlreadyExists.AttributeID = @LocationAttributeID
		)

--insert wards and sites as locations

insert into [Map].[Value]
(
	[AttributeContextID]
	,[ValueCode]
	,[Value]
	,[Created]
	,[Updated]
	,[ByWhom]
)

select
	Location.AttributeContextID 
	,ValueCode = ValueCode
	,Value
	,getdate()
	,getdate()
	,suser_name()

from
	Map.Value

inner join Map.AttributeContext
on	Value.AttributeContextID = AttributeContext.AttributeContextID

inner join Map.Context
on	AttributeContext.ContextID = Context.ContextID
and	Context.ContextCode not in ('L', 'N', 'O')

inner join Map.Attribute
on	AttributeContext.AttributeID = Attribute.AttributeID
and	Attribute.AttributeCode in ('WARD', 'SITE')

inner join Map.AttributeContext Location
on	Context.ContextID = Location.ContextID
and	Location.AttributeID = @LocationAttributeID

where
	not exists
		(
		select
			1
		from
			Map.Value AlreadyExists

		where
			AlreadyExists.AttributeContextID = Location.AttributeContextID 
		and	AlreadyExists.ValueCode = Value.ValueCode
		)
					
--insert local wards and sites as local locations
insert into [Map].[Value]
(
	[AttributeContextID]
	,[ValueCode]
	,[Value]
	,[Created]
	,[Updated]
	,[ByWhom]
)

select
	Location.AttributeContextID 
	,ValueCode = ValueCode
	,Value
	,getdate()
	,getdate()
	,suser_name()
from
	Map.Value

inner join Map.AttributeContext
on	Value.AttributeContextID = AttributeContext.AttributeContextID

inner join Map.Context
on	AttributeContext.ContextID = Context.ContextID
and	Context.ContextCode in ('L')

inner join Map.Attribute
on	AttributeContext.AttributeID = Attribute.AttributeID
and	Attribute.AttributeCode in ('WARD', 'SITE')

inner join Map.AttributeContext Location
on	Location.ContextID = Context.ContextID
and	Location.AttributeID = @LocationAttributeID

where
	not exists
		(
		select
			1
		from
			Map.Value AlreadyExists

		where
			AlreadyExists.AttributeContextID = Location.AttributeContextID 
		and	AlreadyExists.ValueCode = Value.ValueCode
		)

and Value.Value != 'Unassigned'
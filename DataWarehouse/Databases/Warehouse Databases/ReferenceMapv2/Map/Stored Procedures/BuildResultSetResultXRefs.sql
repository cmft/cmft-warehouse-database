﻿
CREATE proc [Map].[BuildResultSetResultXRefs] as 

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@updated int

select
	@updated = 0


declare @DuplicateResultSetResult table
(
ValueID int
)

insert @DuplicateResultSetResult
(
ValueID
)

select
	ValueID
from
	Map.Value
inner join Map.ContextAttribute
on	ContextAttribute.AttributeContextID = Value.AttributeContextID
and	ContextAttribute.AttributeCode in ('RESULTSET', 'RESULT')
and ContextAttribute.ContextCode = 'L'
where
	exists
	(
	select
		1
	from
		Map.Value Duplicate
	where	
		Duplicate.AttributeContextID = Value.AttributeContextID
	and	Duplicate.ValueCode = Value.ValueCode
	having
		count(*) > 1
	)

-- *** GPICE ***

--Result set

update ValueXref
set 
	ValueID =  Value.ValueID
	,ValueXrefID = LocalMap.ValueID
	,Updated = getdate()
	,ByWhom = suser_name()
from
	Map.Value

inner join Map.ValueXref
on	ValueXref.ValueID = Value.ValueID

inner join Map.AttributeContext
on AttributeContext.AttributeContextID = Value.AttributeContextID

inner join	Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID
and	Attribute.AttributeCode = 'RESULTSET'

inner join	Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.ContextCode = 'CEN||GPICE'

inner join [$(Warehouse)].Result.GPICEInvestigationReference
on	GPICEInvestigationReference.ReferenceMapInvestigationCode = Value.ValueCode

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULTSET'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalMap
on	LocalMap.ValueCode = GPICEInvestigationReference.InvestigationCode
and	LocalMap.Value = GPICEInvestigationReference.InvestigationName

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULTSET'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalXref
on	LocalXref.ValueID = ValueXref.ValueXrefID

where
	LocalXref.ValueCode = 'L||RESULTSET' -- only update defaulted xrefs		

select
	 @updated = @@rowcount

--Result

update ValueXref
set 
	ValueID =  Value.ValueID
	,ValueXrefID = LocalMap.ValueID
	,Updated = getdate()
	,ByWhom = suser_name()
from
	Map.Value

inner join Map.ValueXref
on	ValueXref.ValueID = Value.ValueID

inner join Map.AttributeContext
on AttributeContext.AttributeContextID = Value.AttributeContextID

inner join	Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID
and	Attribute.AttributeCode = 'RESULT'

inner join	Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.ContextCode = 'CEN||GPICE'

inner join [$(Warehouse)].Result.GPICEResultReference
on	GPICEResultReference.ReferenceMapResultCode = Value.ValueCode

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULT'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalMap
on	LocalMap.ValueCode = GPICEResultReference.ResultCode
and	LocalMap.Value = GPICEResultReference.ResultName

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULT'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalXref
on	LocalXref.ValueID = ValueXref.ValueXrefID

where
	LocalXref.ValueCode = 'L||RESULT' -- only update defaulted xrefs

select
	 @updated = @updated + @@rowcount

-- *** CENICE ***

--Result set

update ValueXref
set 
	ValueID =  Value.ValueID
	,ValueXrefID = LocalMap.ValueID
	,Updated = getdate()
	,ByWhom = suser_name()
from
	Map.Value

inner join Map.ValueXref
on	ValueXref.ValueID = Value.ValueID

inner join Map.AttributeContext
on AttributeContext.AttributeContextID = Value.AttributeContextID

inner join	Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID
and	Attribute.AttributeCode = 'RESULTSET'

inner join	Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.ContextCode = 'CEN||ICE'

inner join [$(Warehouse)].Result.CENICEInvestigationReference
on	CENICEInvestigationReference.ReferenceMapInvestigationCode = Value.ValueCode

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULTSET'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalMap
on	LocalMap.ValueCode = CENICEInvestigationReference.InvestigationCode
and	LocalMap.Value = CENICEInvestigationReference.InvestigationName

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULTSET'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalXref
on	LocalXref.ValueID = ValueXref.ValueXrefID

where
	LocalXref.ValueCode = 'L||RESULTSET' -- only update defaulted xrefs

select
	 @updated = @updated + @@rowcount

--Result

update ValueXref
set 
	ValueID =  Value.ValueID
	,ValueXrefID = LocalMap.ValueID
	,Updated = getdate()
	,ByWhom = suser_name()
from
	Map.Value

inner join Map.ValueXref
on	ValueXref.ValueID = Value.ValueID

inner join Map.AttributeContext
on AttributeContext.AttributeContextID = Value.AttributeContextID

inner join	Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID
and	Attribute.AttributeCode = 'RESULT'

inner join	Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.ContextCode = 'CEN||ICE'

inner join [$(Warehouse)].Result.CENICEResultReference
on	CENICEResultReference.ReferenceMapResultCode = Value.ValueCode

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULT'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalMap
on	LocalMap.ValueCode = CENICEResultReference.ResultCode
and	LocalMap.Value = CENICEResultReference.ResultName

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULT'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalXref
on	LocalXref.ValueID = ValueXref.ValueXrefID

where
	LocalXref.ValueCode = 'L||RESULT' -- only update defaulted xrefs

select
	 @updated = @updated + @@rowcount

-- *** CWS to CRIS Result set ***

update ValueXref
set 
	ValueID =  Value.ValueID
	,ValueXrefID = LocalMap.ValueID
	,Updated = getdate()
	,ByWhom = suser_name()
from
	Map.Value

inner join Map.ValueXref
on	ValueXref.ValueID = Value.ValueID

inner join Map.AttributeContext
on AttributeContext.AttributeContextID = Value.AttributeContextID

inner join	Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID
and	Attribute.AttributeCode = 'RESULTSET'

inner join	Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.ContextCode = 'CEN||PAS'

inner join [$(Warehouse)].Result.CWSServiceCodeCRISExamCode
on	Value.ValueCode = cast(CWSServiceCodeCRISExamCode.CWSServiceCode as varchar)

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULTSET'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalMap
on	LocalMap.ValueCode = CWSServiceCodeCRISExamCode.CRISExamCode

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULTSET'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalXref
on	LocalXref.ValueID = ValueXref.ValueXrefID

where
	LocalXref.ValueCode = 'L||RESULTSET' -- only update defaulted xrefs
and not exists -- we may have duplicate codes in local so only map where it isn't a duplicate (as can only map on code unlike ICE)
	(
	select
		1
	from
		@DuplicateResultSetResult DuplicateResultSetResult
	where
		DuplicateResultSetResult.ValueID = LocalMap.ValueID
	)

select
	 @updated = @updated + @@rowcount

--CWS to Apex Result set

update ValueXref
set 
	ValueID =  Value.ValueID
	,ValueXrefID = LocalMap.ValueID
	,Updated = getdate()
	,ByWhom = suser_name()
from
	Map.Value

inner join Map.ValueXref
on	ValueXref.ValueID = Value.ValueID

inner join Map.AttributeContext
on AttributeContext.AttributeContextID = Value.AttributeContextID

inner join	Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID
and	Attribute.AttributeCode = 'RESULTSET'

inner join	Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.ContextCode = 'CEN||PAS'

inner join [$(Warehouse)].Result.APEXOrderPASService
on	Value.ValueCode = cast(APEXOrderPASService.PASServiceCode as varchar)

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULTSET'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalMap
on	LocalMap.ValueCode = APEXOrderPASService.ApexOrderCode

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULTSET'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalXref
on	LocalXref.ValueID = ValueXref.ValueXrefID

where
	LocalXref.ValueCode = 'L||RESULTSET' -- only update defaulted xrefs
and not exists -- we may have duplicate codes in local so only map where it isn't a duplicate (as can only map on code unlike ICE)
	(
	select
		1
	from
		@DuplicateResultSetResult DuplicateResultSetResult
	where
		DuplicateResultSetResult.ValueID = LocalMap.ValueID
	)

select
	 @updated = @updated + @@rowcount

-- *** CWS to Apex Result ***

update ValueXref
set 
	ValueID =  Value.ValueID
	,ValueXrefID = LocalMap.ValueID
	,Updated = getdate()
	,ByWhom = suser_name()
from
	Map.Value

inner join Map.ValueXref
on	ValueXref.ValueID = Value.ValueID

inner join Map.AttributeContext
on AttributeContext.AttributeContextID = Value.AttributeContextID

inner join	Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID
and	Attribute.AttributeCode = 'RESULT'

inner join	Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.ContextCode = 'CEN||PAS'

left join [$(Warehouse)].Result.APEXResultCWSResult
on	CWSResultCode = Value.ValueCode

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULT'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalMap
on	LocalMap.ValueCode = coalesce(APEXResultCWSResult.ResultCode, Value.ValueCode)

inner join
	(
	select
		ValueID
		,ValueCode
		,Value
	from
		Map.Value

	inner join Map.AttributeContext
	on AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join	Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID
	and	Attribute.AttributeCode = 'RESULT'

	inner join	Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.ContextCode = 'L'

	) LocalXref
on	LocalXref.ValueID = ValueXref.ValueXrefID

where
	LocalXref.ValueCode = 'L||RESULT'
and not exists -- we may have duplicate codes in local so only map where it isn't a duplicate (as can only map on code unlike ICE)
	(
	select
		1
	from
		@DuplicateResultSetResult DuplicateResultSetResult
	where
		DuplicateResultSetResult.ValueID = LocalMap.ValueID
	)

select
	 @updated = @updated + @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Elapsed time: ' + cast(@Elapsed as varchar) + ' minutes'
print @Stats

exec Audit.WriteLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿CREATE procedure [Map].[BuildMissingXrefs] as

--prevent parallel running and consequent duplication of xrefs
set transaction isolation level serializable

begin transaction

	--Insert new non-Base Context Values
	insert
	into
		Map.Value
		(
		 AttributeContextID
		,ValueCode
		,Value
		,Created
		,ByWhom
		)
	select
		 AttributeContext.AttributeContextID
		,ValueCode = Context.ContextCode + '||' + Attribute.AttributeCode
		,Value = 'Unassigned'
		,Created = GETDATE()
		,ByWhom = SUSER_NAME()
	from
		Map.AttributeContext

	inner join Map.Context
	on	Context.ContextID = AttributeContext.ContextID
	and	Context.BaseContext = 0

	inner join Map.Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID

	where
		not exists
		(
		select
			1
		from
			Map.Value
		where
			Value.AttributeContextID = AttributeContext.AttributeContextID
		and	Value.ValueCode = Context.ContextCode + '||' + Attribute.AttributeCode
		)



	--build new default cross-references

	declare @LocalContextID int
	declare @NationalContextID int
	declare @OtherContextID int

	select
		 @LocalContextID =
			(
			select
				ContextID
			from
				Map.Context
			where
				ContextCode = 'L'
			)
		,@NationalContextID =
			(
			select
				ContextID
			from
				Map.Context
			where
				ContextCode = 'N'
			)
		,@OtherContextID =
			(
			select
				ContextID
			from
				Map.Context
			where
				ContextCode = 'O'
			)



	-- Build Work Tables


	-- Local Xrefs

	select
		 Value.ValueID
		,XrefAttributeContextID = XrefAttributeContext.AttributeContextID
		,XrefValueCode = 'L||' + Attribute.AttributeCode
	into
		#wrkLocal
	from
		--base values that have missing Local mappings
		(
		select
			 Value.ValueID
			,Value.AttributeContextID
		from
			Map.Value
	
		inner join Map.AttributeContext
		on	AttributeContext.AttributeContextID = Value.AttributeContextID
		and	AttributeContext.ContextID != @LocalContextID
		and	AttributeContext.ContextID != @NationalContextID
		and	AttributeContext.ContextID != @OtherContextID

		where
			not exists
				(
				select
					1
				from
					Map.ValueXref Xref

				inner join Map.Value XrefValue
				on	XrefValue.ValueID = Xref.ValueXrefID

				inner join Map.AttributeContext XrefAttributeContext
				on	XrefAttributeContext.AttributeContextID = XrefValue.AttributeContextID
				and	XrefAttributeContext.ContextID = @LocalContextID

				where
					Xref.ValueID = Value.ValueID
				and	XrefAttributeContext.AttributeID = AttributeContext.AttributeID
				)
		) Value


	inner join Map.AttributeContext -- value's AttributeContext
	on	AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join Map.Attribute --value's Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID

	inner join Map.AttributeContext XrefAttributeContext -- all AttributeContexts for this attribute
	on	XrefAttributeContext.AttributeID = Attribute.AttributeID
	and	XrefAttributeContext.ContextID = @LocalContextID -- filter for just Local


	--National Xrefs

	select
		 Value.ValueID
		,XrefAttributeContextID = XrefAttributeContext.AttributeContextID
		,XrefValueCode = 'N||' + Attribute.AttributeCode
	into
		#wrkNational
	from
		--base values that have missing National mappings
		(
		select
			 Value.ValueID
			,Value.AttributeContextID
		from
			Map.Value
	
		inner join Map.AttributeContext
		on	AttributeContext.AttributeContextID = Value.AttributeContextID
		and	AttributeContext.ContextID != @LocalContextID
		and	AttributeContext.ContextID != @NationalContextID
		and	AttributeContext.ContextID != @OtherContextID

		where
			not exists
				(
				select
					1
				from
					Map.ValueXref Xref

				inner join Map.Value XrefValue
				on	XrefValue.ValueID = Xref.ValueXrefID

				inner join Map.AttributeContext XrefAttributeContext
				on	XrefAttributeContext.AttributeContextID = XrefValue.AttributeContextID
				and	XrefAttributeContext.ContextID = @NationalContextID

				where
					Xref.ValueID = Value.ValueID
				and	XrefAttributeContext.AttributeID = AttributeContext.AttributeID
				)
		) Value


	inner join Map.AttributeContext -- value's AttributeContext
	on	AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join Map.Attribute --value's Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID

	inner join Map.AttributeContext XrefAttributeContext -- all AttributeContexts for this attribute
	on	XrefAttributeContext.AttributeID = Attribute.AttributeID
	and	XrefAttributeContext.ContextID = @NationalContextID -- filter for just National


	--Other Xrefs

	select
		 Value.ValueID
		,XrefAttributeContextID = XrefAttributeContext.AttributeContextID
		,XrefValueCode = 'O||' + Attribute.AttributeCode
	into
		#wrkOther
	from
		--base values that have missing Other mappings
		(
		select
			 Value.ValueID
			,Value.AttributeContextID
		from
			Map.Value
	
		inner join Map.AttributeContext
		on	AttributeContext.AttributeContextID = Value.AttributeContextID
		and	AttributeContext.ContextID != @LocalContextID
		and	AttributeContext.ContextID != @NationalContextID
		and	AttributeContext.ContextID != @OtherContextID

		where
			not exists
				(
				select
					1
				from
					Map.ValueXref Xref

				inner join Map.Value XrefValue
				on	XrefValue.ValueID = Xref.ValueXrefID

				inner join Map.AttributeContext XrefAttributeContext
				on	XrefAttributeContext.AttributeContextID = XrefValue.AttributeContextID
				and	XrefAttributeContext.ContextID = @OtherContextID

				where
					Xref.ValueID = Value.ValueID
				and	XrefAttributeContext.AttributeID = AttributeContext.AttributeID
				)
		) Value


	inner join Map.AttributeContext -- value's AttributeContext
	on	AttributeContext.AttributeContextID = Value.AttributeContextID

	inner join Map.Attribute --value's Attribute
	on	Attribute.AttributeID = AttributeContext.AttributeID

	inner join Map.AttributeContext XrefAttributeContext -- all AttributeContexts for this attribute
	on	XrefAttributeContext.AttributeID = Attribute.AttributeID
	and	XrefAttributeContext.ContextID = @OtherContextID -- filter for just Other


	insert
	into
		Map.ValueXref
		(
		 ValueID
		,ValueXrefID
		,Created
		,ByWhom
		)

	select
		 ValueID = #wrkLocal.ValueID
		,ValueXrefID = XrefValue.ValueID
		,Created = GETDATE()
		,ByWhom = SUSER_NAME()
	from
		#wrkLocal

	inner join 
		(
		select
			 ValueID
			,AttributeContextID
			,ValueCode
		from
			Map.Value
		where
			left(ValueCode , 3) = 'L||'
		) XrefValue -- get Xref value

	on	XrefValue.AttributeContextID = #wrkLocal.XrefAttributeContextID
	and	XrefValue.ValueCode = #wrkLocal.XrefValueCode

	union

	select
		 ValueID = #wrkNational.ValueID
		,ValueXrefID = XrefValue.ValueID
		,Created = GETDATE()
		,ByWhom = SUSER_NAME()
	from
		#wrkNational

	inner join 
		(
		select
			 ValueID
			,AttributeContextID
			,ValueCode
		from
			Map.Value
		where
			left(ValueCode , 3) = 'N||'
		) XrefValue -- get Xref value

	on	XrefValue.AttributeContextID = #wrkNational.XrefAttributeContextID
	and	XrefValue.ValueCode = #wrkNational.XrefValueCode

	union

	select
		 ValueID = #wrkOther.ValueID
		,ValueXrefID = XrefValue.ValueID
		,Created = GETDATE()
		,ByWhom = SUSER_NAME()
	from
		#wrkOther

	inner join 
		(
		select
			 ValueID
			,AttributeContextID
			,ValueCode
		from
			Map.Value
		where
			left(ValueCode , 3) = 'O||'
		) XrefValue -- get Xref value

	on	XrefValue.AttributeContextID = #wrkOther.XrefAttributeContextID
	and	XrefValue.ValueCode = #wrkOther.XrefValueCode



	drop table #wrkLocal
	drop table #wrkNational
	drop table #wrkOther

commit
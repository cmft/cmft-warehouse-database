﻿CREATE VIEW [Map].[ContextAttribute] AS

select
       [AttributeContextID]
       ,AttributeContext.[ContextID]
       ,ContextCode
       ,Context
       ,AttributeContext.[AttributeID]
       ,AttributeCode
       ,Attribute

from
       Map.AttributeContext
  
inner join Map.Context
on Context.ContextID = AttributeContext.ContextID

inner join Map.Attribute
on Attribute.AttributeID = AttributeContext.AttributeID
﻿CREATE VIEW [Map].[MappedAttribute] AS

SELECT
	A.AttributeID,
	A.Attribute,
	BaseContextID = BC.ContextID,
	BaseContext = BC.Context,
	MapContextID = MC.ContextID,
	MapContext = MC.Context,
	AllValues = COALESCE(C.AllValues,0),
	MappedValues = COALESCE(B.MappedValues,0),
	UnmappedValues = COALESCE(C.AllValues,0) - COALESCE(B.MappedValues,0)
FROM Map.Attribute A
JOIN Map.AttributeContext BAC ON BAC.AttributeID = A.AttributeID
JOIN Map.Context BC ON BC.ContextID = BAC.ContextID
AND BC.BaseContext = 1

JOIN Map.AttributeContext MAC ON MAC.AttributeID = A.AttributeID
JOIN Map.Context MC ON MC.ContextID = MAC.ContextID
AND MC.BaseContext = 0

LEFT JOIN
(	
	SELECT 
		A.AttributeID,
		BaseContextID = BC.ContextID,
		MapContextID = MC.ContextID,
		MappedValues = COUNT(*)
	FROM Map.Value BV
	JOIN Map.ValueXref XV ON XV.ValueID = BV.ValueID
	JOIN Map.Value MV ON MV.ValueID = XV.ValueXrefID

	JOIN Map.AttributeContext BAC ON BAC.AttributeContextID = BV.AttributeContextID
	JOIN Map.Attribute A ON A.AttributeID = BAC.AttributeID
	JOIN Map.Context BC ON BC.ContextID = BAC.ContextID
	AND BC.BaseContext = 1

	JOIN Map.AttributeContext MAC ON MAC.AttributeContextID = MV.AttributeContextID
	AND MAC.AttributeID = A.AttributeID
	JOIN Map.Context MC ON MC.ContextID = MAC.ContextID
	AND MC.BaseContext = 0

	GROUP BY
	A.AttributeID,
		BC.ContextID,
		MC.ContextID
) AS B 
ON B.AttributeID = A.AttributeID
AND B.BaseContextID = BC.ContextID
AND B.MapContextID = MC.ContextID

LEFT JOIN
(
	SELECT 
		A.AttributeID,
		BaseContextID = BC.ContextID,
		AllValues = COUNT(*)
	FROM Map.Value BV

	JOIN Map.AttributeContext BAC ON BAC.AttributeContextID = BV.AttributeContextID
	JOIN Map.Attribute A ON A.AttributeID = BAC.AttributeID
	JOIN Map.Context BC ON BC.ContextID = BAC.ContextID
	AND BC.BaseContext = 1
	GROUP BY
		A.AttributeID,
		BC.ContextID
) AS C
ON C.AttributeID = A.AttributeID
AND C.BaseContextID = BC.ContextID
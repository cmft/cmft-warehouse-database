﻿CREATE TABLE [RTT].[PathwayStatusBase] (
    [PathwayStatusCode]         VARCHAR (5)  NOT NULL,
    [PathwayStatus]             VARCHAR (50) NULL,
    [NationalPathwayStatusCode] VARCHAR (5)  NULL
);


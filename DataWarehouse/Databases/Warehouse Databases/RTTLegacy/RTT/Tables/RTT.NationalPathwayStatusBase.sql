﻿CREATE TABLE [RTT].[NationalPathwayStatusBase] (
    [NationalPathwayStatusCode] VARCHAR (5)  NOT NULL,
    [NationalPathwayStatus]     VARCHAR (50) NULL
);


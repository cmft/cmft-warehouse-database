﻿EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Dusia.Romaniuk';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Rachel.Royston';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Dusia.Romaniuk';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Infounit members';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\adam.black';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Diane.Thomas';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmftslamlink';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Philip.Huitson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmmc\tom.smith';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Rachel.Royston';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'CMMC\Malcom.hodson';


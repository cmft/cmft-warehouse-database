﻿
CREATE view [Numerator].[RTTOutpatientWaiting0To12Weeks] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.CensusDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.RTTOutpatientWaiting0To12Weeks'

where Dataset.DatasetCode = 'RTT'
and d.PathwayStatusCode = 'OPW'
and d.AdjustedFlag = 'N'
and WaitDays between 0 and 83


﻿
CREATE view Numerator.CANTTreatedWithin62DaysOfScreeningServiceReferralLU as

-- Based on SP CancerRegister.dbo.RPT_SCREEN_TREAT

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CANTTreatedWithin62DaysOfScreeningServiceReferralLU'
	
where DatasetCode = 'CANT'
and D.TumourStatusCode not in ('4', '5')
and coalesce(D.TreatmentNo,0) <> 2
and coalesce(D.InappropriateReferralCode,0) <> '1'
and D.ReceiptOfReferralDate is not null
and D.PriorityTypeCode = '02' -- Urgent
and D.SourceForOutpatientCode = '17' -- National Screening Programme
and D.PrimaryDiagnosisCode1 not in ('C62', 'C620', 'C621', 'C629', 'C910', 'C920', 'C924', 'C925', 'C930', 'C942', 'C950')

and not
(
	coalesce(D.PrimaryDiagnosisCode,'') = 'C44' 
	and coalesce(D.HistologyCode,'') in ('M80903','M80913','M80923','M80933','M80943','M80953','M81103')
)

and not 
(
	coalesce(D.PrimaryDiagnosisCode,'') <> 'D05' 
	and left(D.PrimaryDiagnosisCode,1) = 'D'
)

and 
(
	D.DateOfBirth IS NULL 
	or convert(int,round(datediff(hour,D.DateOfBirth,coalesce(D.DiagnosisDate, GetDate()))/8766,0)) >= 16
)

and datediff(d,D.ReceiptOfReferralDate,D.TreatmentDate)
- (
	case 
		when D.TreatmentSettingCode in ('01','02') 
		and D.DecisionToTreatAdjustmentReasonCode = '8' 
		then coalesce(D.DecisionToTreatAdjustment,0) 
		else 0 
	END
) 
- coalesce(D.FirstAppointmentWaitingTimeAdjusted,0) 
- (
	case 
		when D.FirstAppointmentAdjustmentReasonCode = '3' 
		and D.CancelledAppointmentDate IS NOT NULL 
		then coalesce(datediff(d, D.ReceiptOfReferralDate, D.CancelledAppointmentDate), 0) 
		else 0 
	end
)
<= 62


and CancerSite = 'Lung'

﻿




CREATE view [Numerator].[IncidentChildSafeguardingAlerts] as

-- Count Safeguarding Alerts

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentChildSafeguardingAlerts'
	
where
	Dataset.DatasetCode = 'INC'

and
	(
	WrkDataset.IncidentTypeCode = '0x5420' 
	or WrkDataset.IncidentCauseGroupCode in ('0x6420','0x5320') 
	or WrkDataset.IncidentCause1Code in ('0x7247','0x7744')
	) -- Safeguarding

and IncidentInvolvingChild = 1






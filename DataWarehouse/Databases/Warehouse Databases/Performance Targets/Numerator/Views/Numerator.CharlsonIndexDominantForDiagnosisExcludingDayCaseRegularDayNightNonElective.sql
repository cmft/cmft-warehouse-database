﻿


CREATE view [Numerator].[CharlsonIndexDominantForDiagnosisExcludingDayCaseRegularDayNightNonElective]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.EpisodeStartDateID
	,ServicePointID
	,Value = CharlsonIndex

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CharlsonIndexDominantForDiagnosisExcludingDayCaseRegularDayNightNonElective'

where
	DatasetCode = 'APC'
and	DominantForDiagnosis = 1
and	NationalPatientClassificationCode not in ('2','3','4')
and	PatientCategoryCode = 'NE'




﻿CREATE view [Numerator].[NeonatalBirthInHospTempBelow36] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.NeonatalBirthInHospTempBelow36'
	
where DatasetCode = 'NEO'
and AdmissionTemperature < 36.0
and upper(left(NeonatalReferralType,6)) = 'INBORN' -- Born in hospital
and DaysCooled = 0 -- Exclude babies who have been cooled

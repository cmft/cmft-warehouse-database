﻿CREATE view [Numerator].[AEUnplannedReAttendance] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EncounterDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AEUnplannedReAttendance'
	
where Dataset.DatasetCode = 'AES'
and d.StageCode = 'INDEPARTMENTADJUSTED'
and d.UnplannedReattend7Days <> 0

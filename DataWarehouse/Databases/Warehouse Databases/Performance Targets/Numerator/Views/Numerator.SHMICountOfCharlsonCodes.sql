﻿CREATE view [Numerator].[SHMICountOfCharlsonCodes]

as

with CharlsonCode as
(
	select DiagnosisCode from WarehouseOLAPMergedV2.WH.Diagnosis where IsCharlson = 1
)

select
	DatasetID
	,DatasetRecno
	,NumeratorID
	,SiteID
	,DirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID
	,ServicePointID
	,Value  
from
(
	select
		DatasetID = d.DatasetID
		,DatasetRecno = d.DatasetRecno
		,NumeratorID = Numerator.NumeratorID
		,SiteID = d.EndSiteID
		,DirectorateID = d.EndDirectorateID
		,SpecialtyID = d.SpecialtyID
		,ClinicianID = d.ClinicianID
		,DateID = d.EpisodeEndDateID
		,ServicePointID
	,Value = -- Add 1 for each Charlson code
			case when p.DiagnosisCode is not null then 1 else 0 end
			+ case when s.DiagnosisCode is not null then 1 else 0 end
			+ case when s1.DiagnosisCode is not null then 1 else 0 end
			+ case when s2.DiagnosisCode is not null then 1 else 0 end
			+ case when s3.DiagnosisCode is not null then 1 else 0 end
			+ case when s4.DiagnosisCode is not null then 1 else 0 end
			+ case when s5.DiagnosisCode is not null then 1 else 0 end
			+ case when s6.DiagnosisCode is not null then 1 else 0 end
			+ case when s7.DiagnosisCode is not null then 1 else 0 end
			+ case when s8.DiagnosisCode is not null then 1 else 0 end
			+ case when s9.DiagnosisCode is not null then 1 else 0 end
			+ case when s10.DiagnosisCode is not null then 1 else 0 end
			+ case when s11.DiagnosisCode is not null then 1 else 0 end
			+ case when s12.DiagnosisCode is not null then 1 else 0 end
			+ case when s13.DiagnosisCode is not null then 1 else 0 end

	from
		dbo.WrkDataset d
	inner join
		dbo.Dataset Dataset
	on	d.DatasetID = Dataset.DatasetID

	inner join dbo.Numerator
	on	Numerator.NumeratorLogic = 'Numerator.SHMICountOfCharlsonCodes'

	left join CharlsonCode p on d.PrimaryDiagnosisCode = p.DiagnosisCode
	left join CharlsonCode s on	d.SubsidiaryDiagnosisCode = s.DiagnosisCode
	left join CharlsonCode s1 on d.SecondaryDiagnosisCode1 = s1.DiagnosisCode
	left join CharlsonCode s2 on d.SecondaryDiagnosisCode2 = s2.DiagnosisCode
	left join CharlsonCode s3 on d.SecondaryDiagnosisCode3 = s3.DiagnosisCode
	left join CharlsonCode s4 on d.SecondaryDiagnosisCode4 = s4.DiagnosisCode
	left join CharlsonCode s5 on d.SecondaryDiagnosisCode5 = s5.DiagnosisCode
	left join CharlsonCode s6 on d.SecondaryDiagnosisCode6 = s6.DiagnosisCode
	left join CharlsonCode s7 on d.SecondaryDiagnosisCode7 = s7.DiagnosisCode
	left join CharlsonCode s8 on d.SecondaryDiagnosisCode8 = s8.DiagnosisCode
	left join CharlsonCode s9 on d.SecondaryDiagnosisCode9 = s9.DiagnosisCode
	left join CharlsonCode s10 on d.SecondaryDiagnosisCode10 = s10.DiagnosisCode
	left join CharlsonCode s11 on d.SecondaryDiagnosisCode11 = s11.DiagnosisCode
	left join CharlsonCode s12 on d.SecondaryDiagnosisCode12 = s12.DiagnosisCode
	left join CharlsonCode s13 on d.SecondaryDiagnosisCode13 = s13.DiagnosisCode

	where
		Dataset.DatasetCode = 'APC'
	and
		d.DominantForDiagnosis = 1
) x
where Value > 0 -- Don't write out records with 0 value

﻿


CREATE view [Numerator].[IncidentMedicationErrorGrade1AgeOver65]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentMedicationErrorGrade1AgeOver65'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		IncidentTypeCode = '0x5120' 
	or	IncidentCauseGroupCode = '0x4620'
	) -- Medication Error
and	WrkDataset.IncidentInvolvingElderly = 1
and	WrkDataset.IncidentGradeCode --= '0x4220' -- Harm Grade = 1
								 in	(
									'0x4220' --1 No Harm
									,'0x4D20' --1U No Harm Unconfirmed
									) --CH 07012015 Grading changed


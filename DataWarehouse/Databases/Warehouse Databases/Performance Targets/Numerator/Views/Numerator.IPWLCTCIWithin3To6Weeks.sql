﻿CREATE view [Numerator].[IPWLCTCIWithin3To6Weeks] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.StartSiteID   
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = CensusDateID -- Census Date
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IPWLCTCIWithin3To6Weeks'

where Dataset.DatasetCode = 'IPWLC'

-- TCI Date is within 2 weeks of given CensusDate
and datediff(d,d.CensusDate,d.TCIDate) between 14 and 41

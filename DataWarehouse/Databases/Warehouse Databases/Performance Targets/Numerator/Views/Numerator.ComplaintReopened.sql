﻿








CREATE view [Numerator].[ComplaintReopened] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.ReceiptDateID
	,WrkDataset.ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ComplaintReopened'
	
where
	Dataset.DatasetCode = 'COMP'
and	WrkDataset.CaseTypeCode in ('0x4120','0x4A20') --formal complaint and acute trust complaint
and WrkDataset.Reopened = 1
and ReceiptDate >= '1 apr 2014' 
and SequenceNo = 1









﻿create view [Numerator].[SESSActual210MinSession] as

-- Derive Actual 210 Minute Sessions from (Actual) Duration / 210 minutes

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.SessionDateID
	,ServicePointID
	,Value = cast(Duration as decimal(19,4)) / 210

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.SESSActual210MinSession'
	
where DatasetCode = 'SESS'

and CancelledFlag = 0

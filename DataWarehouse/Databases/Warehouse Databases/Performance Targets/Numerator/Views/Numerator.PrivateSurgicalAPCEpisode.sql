﻿
CREATE view [Numerator].[PrivateSurgicalAPCEpisode]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EpisodeStartDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.PrivateSurgicalAPCEpisode'

where
	Dataset.DatasetCode = 'APC'
and
	d.StartDirectorateCode in (0,62)
and 
	d.StartWardCode in ('SUBA','SUBP')
and 
	d.NationalSpecialtyCode < '272'


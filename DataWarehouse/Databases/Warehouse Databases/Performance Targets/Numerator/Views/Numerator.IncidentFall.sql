﻿

CREATE view [Numerator].[IncidentFall]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentFall'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		WrkDataset.IncidentCauseGroupCode = '0x3120' 
	or	WrkDataset.IncidentCause1Code in ('0x7042','0x7542','0x7142','0x5220','0x7342','0x7242')
	) -- Falls



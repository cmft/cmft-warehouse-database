﻿CREATE view [Numerator].[PATREGDuplicatePatientRegistration] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.RegistrationDateID
	,ServicePointID
	,Value = 1
from WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.PATREGDuplicatePatientRegistration'
	
where DatasetCode = 'PATREG'
and IsDuplicate = 1
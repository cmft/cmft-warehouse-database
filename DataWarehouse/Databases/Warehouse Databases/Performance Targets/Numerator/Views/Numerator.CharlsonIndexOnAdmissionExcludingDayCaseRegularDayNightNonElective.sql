﻿


CREATE view [Numerator].[CharlsonIndexOnAdmissionExcludingDayCaseRegularDayNightNonElective]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID
	,Value = CharlsonIndex

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CharlsonIndexOnAdmissionExcludingDayCaseRegularDayNightNonElective'

where
	DatasetCode = 'APC'
and	FirstEpisodeInSpellIndicator = 1
and	NationalPatientClassificationCode not in ('2','3','4')
and	PatientCategoryCode = 'NE'




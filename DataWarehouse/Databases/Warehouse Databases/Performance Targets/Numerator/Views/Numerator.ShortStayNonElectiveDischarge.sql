﻿CREATE view [Numerator].[ShortStayNonElectiveDischarge]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ShortStayNonElectiveDischarge'

where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	PatientCategoryCode = 'NE'
and 
	datediff(hh,d.AdmissionTime, d.DischargeTime) < 24

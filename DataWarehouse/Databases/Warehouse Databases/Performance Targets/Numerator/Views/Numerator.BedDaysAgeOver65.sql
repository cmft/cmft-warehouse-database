﻿CREATE view [Numerator].[BedDaysAgeOver65]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = datediff(dd,d.AdmissionTime, d.DischargeTime)

from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.BedDaysAgeOver65'

where
	Dataset.DatasetCode = 'APC'
and
	d.NationalLastEpisodeInSpellCode = 1
and
	datediff(dd,d.AdmissionTime, d.DischargeTime) > 0
and
	convert(int,round(datediff(hour,DateOfBirth,AdmissionTime)/8766,0)) >= 65

﻿create view [Numerator].[SESSPlanned210MinSession] as

-- Derive Planned 210 Minute Sessions from Planned Duration / 210 minutes

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.SessionDateID
	,ServicePointID
	,Value = cast(PlannedDuration as decimal(19,4)) / 210

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.SESSPlanned210MinSession'
	
where DatasetCode = 'SESS'

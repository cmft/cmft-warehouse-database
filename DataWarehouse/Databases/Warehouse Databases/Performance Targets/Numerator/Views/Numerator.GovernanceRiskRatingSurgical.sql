﻿

CREATE view [Numerator].GovernanceRiskRatingSurgical as


select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = StartSiteID 
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDataset.ActivityDateID
	,ServicePointID
	,Value = GovernanceRiskRating
from 
	dbo.WrkDataset 
inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.GovernanceRiskRatingSurgical'

where 
	Dataset.DatasetCode = 'GOVERNANCE'
and StartDirectorateID = 40 -- Specialist Medicine
and GovernanceRiskRating is not null


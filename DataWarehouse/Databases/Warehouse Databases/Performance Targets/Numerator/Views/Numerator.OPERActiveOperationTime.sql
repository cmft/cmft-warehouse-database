﻿

CREATE view [Numerator].[OPERActiveOperationTime] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.OperationDateID
	,ServicePointID
	,Value = AnaesthetistActiveTheatreTime
from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OPERActiveOperationTime'
	
where 
	DatasetCode = 'OPER'
and SessionSourceUniqueID <>0



﻿create view [Numerator].[RENQCalcium2Point1To2Point5] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.RENQCalcium2Point1To2Point5'
	
where DatasetCode = 'RENQ'
and ModalityCode = '90:55425' -- HD
and ModalitySettingCode in ('90:55435','90:55433') -- Hospital or Satellite
and EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients' 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'
and (DateOfDeath is null or DateOfDeath > LastDayOfQuarter) -- Include only patients alive at quarter end
and CaAdjNum between 2.1 and 2.5

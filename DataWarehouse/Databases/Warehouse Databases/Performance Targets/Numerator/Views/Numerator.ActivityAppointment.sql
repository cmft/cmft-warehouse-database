﻿
Create view [Numerator].[ActivityAppointment] as

--supercedes [Numerator].[ManualItemID889]

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.Numerator

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
--on	NumeratorLogic = 'Numerator.ManualItemID888'
on	NumeratorLogic = 'Numerator.ActivityAppointment'
	
--where
--	Dataset.DatasetCode = 'MAN'
--and ItemID = '888'
	
where
	Dataset.DatasetCode = 'ACTIVITY'
and WrkDataset.Numerator = 1
and WrkDataset.ItemID = '889'


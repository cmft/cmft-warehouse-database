﻿






CREATE view [Numerator].[VTERiskAssessmentCompleteCoding] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.VTERiskAssessmentCompleteCoding'

where
	Dataset.DatasetCode = 'APC'
and WrkDataset.ClinicalCodingCompleteDate is not null
and	WrkDataset.FirstEpisodeInSpellIndicator = 1
and WrkDataset.VTE = 1
and WrkDataset.VTECompleteAndOrExclusion = 1










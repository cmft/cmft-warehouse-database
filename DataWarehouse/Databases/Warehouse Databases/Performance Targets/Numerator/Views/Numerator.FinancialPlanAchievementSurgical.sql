﻿
CREATE view [Numerator].FinancialPlanAchievementSurgical as


select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = StartSiteID 
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDataset.ActivityDateID
	,ServicePointID
	,Value = FinanceRiskRating
from 
	dbo.WrkDataset 
inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FinancialPlanAchievementSurgical'

where 
	Dataset.DatasetCode = 'FINANCE'
and StartDirectorateID = 40 -- Surgical



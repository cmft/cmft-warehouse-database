﻿
CREATE view [Numerator].FinancialPlanAchievementStMarys as


select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = StartSiteID 
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDataset.ActivityDateID
	,ServicePointID
	,Value = FinanceRiskRating
from 
	dbo.WrkDataset  

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FinancialPlanAchievementStMarys'

where 
	Dataset.DatasetCode = 'FINANCE'
and StartDirectorateID = 43 -- St Marys



﻿CREATE view [Numerator].[CriticalCare48HourReadmission]

as

-- Indicator to be redefined using MIDAS not PAS

-- Select Spell A which has a following Spell B in ITU / CDU within 48 hours for the same Patient
select
	DatasetID = A.DatasetID
	,DatasetRecno = A.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = A.EndSiteID		
	,DirectorateID = A.EndDirectorateID
	,SpecialtyID = A.SpecialtyID
	,ClinicianID = A.ClinicianID
	,DateID = A.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset A

inner join dbo.Dataset
on	Dataset.DatasetID = A.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.CriticalCare48HourReadmission'

where
	Dataset.DatasetCode = 'APC'
and	
	A.NationalLastEpisodeInSpellCode = 1 

	-- Look for a later Spell B in Critical Care, within 48 hours	
and 
	exists
	(
		select 1 from WrkDataset B
		where B.DatasetID = A.DatasetID 
		and B.FirstEpisodeInSpellIndicator = 1
		and B.SourcePatientNo = A.SourcePatientNo
		and B.AdmissionTime > A.DischargeTime
		and datediff(hour,A.DischargeTime, B.AdmissionTime) <= 48
		and (B.StartWardCode like 'HDU%' or B.StartWardCode like 'ITU%')
	)

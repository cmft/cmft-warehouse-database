﻿CREATE view [Numerator].[WSTransferFromMAUToWardAgeOver65] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.WardStayStartDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.WSTransferFromMAUToWardAgeOver65'

where Dataset.DatasetCode = 'WS'
and StartWardCode in ('MAU','AMU')
and WardStayEndActivityCode = 'TR'
and convert(int,round(datediff(hour,DateOfBirth,AdmissionTime)/8766,0)) >= 65

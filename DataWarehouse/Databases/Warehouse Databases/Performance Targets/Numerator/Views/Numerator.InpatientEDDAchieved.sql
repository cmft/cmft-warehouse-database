﻿

create view [Numerator].[InpatientEDDAchieved]

as

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join	dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.InpatientEDDAchieved'

inner join	dbo.Specialty
on	Specialty.SourceSpecialtyID = WrkDataset.SpecialtyID

where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	NationalIntendedManagementCode not in ('3','4')
and
	WrkDataset.NationalSpecialtyCode <> '501'
and
	SourceSpecialty not like '%DIALYSIS%'
and
	left(SourceSpecialtyCode, 2) <> 'IH'
and
	StartWardCode <> '76A'	
and
	left(StartWardCode, 3) <> 'SUB'
and
	not (
			StartWardCode = 'ESTU' 
			and datediff(hour, WrkDataset.AdmissionTime, WrkDataset.DischargeTime) < 6
		)
and
	EddCreatedTime is not null
and
	datediff(day, WrkDataset.ExpectedDateOfDischarge, WrkDataset.DischargeTime) <= 0



﻿



CREATE view [dbo].[ItemHierarchy]

as


select
	 Hierarchy.HierarchyID
	,Item.ItemID
	,Item.Item
	,ItemType.ItemType
	,Measure.Measure

	,ReportMeasure = 
		coalesce(
			 ReportMeasureHierarchy.ReportMeasure
			,ReportMeasure.ReportMeasure
		)

	,Weight = coalesce(Hierarchy.Weight, 1)

	,Item.Reversed

	,IndicatorDirection = 
		case
		when Item.Reversed = 0
		then 'Higher value represents better performance'
		else 'Lower value represents better performance'
		end

	,Hierarchy.ParentID
	,Item.Factor
	,Item.NumeratorID
	,Numerator.Numerator
	,Item.DenominatorID
	,Denominator = coalesce(Denominator.Denominator, 'N/A')

	,ReportImage =
		coalesce(
			 HierarchyReportImage.ReportImage
			,ItemReportImage.ReportImage
		)

	,ReportColour =
		coalesce(
			 HierarchyReportColour.ReportColour
			,ItemReportColour.ReportColour
		)

	,ReportBackgroundColour =
		coalesce(
			 HierarchyReportBackgroundColour.ReportColour
			,ItemReportBackgroundColour.ReportColour
		)

	,Hierarchy.ReportPriority
	,Hierarchy.ReportNavigation

	,ReportingPeriod =
		coalesce(
			 ReportingPeriod.ReportingPeriod
			,'Monthly'
		)

	,Hierarchy.ReportOrder
	,Hierarchy.ReportColumns

	,AmberThresholdUpper =
		coalesce(
			 Hierarchy.AmberThresholdUpper
			,1.0
		)

	,AmberThresholdLower =
		coalesce(
			 Hierarchy.AmberThresholdLower
			,0.95
		)

	,ExecutiveDirectorOwner = ExecutiveDirectorOwner.Owner
	,CorporateDirectorOwner = CorporateDirectorOwner.Owner
	,CommitteeOwner = CommitteeOwner.Owner

	,ItemDescription = Item.Description
	,Item.Source
	,Item.FurtherGuidance

	,HasAreaOfConcern =
		cast(
			case
			when exists
				(
				select
					1
				from
					dbo.Narrative
				where
					Narrative.NarrativeTypeID in (2, 3, 4)
				and	Narrative.ItemID = Item.ItemID
				and	Narrative.Active = 1
				)
			then 1
			else 0
			end

			as bit
		)

	,DefaultTargetType = TargetType.TargetType
	,ItemFormat = ItemFormat.ItemFormat
	,Item.DivisionCompliance
	,Item.ServicePointCompliance

	,DefaultNumeratorValue =
		coalesce(
			 Item.DefaultNumeratorValue
			,-1
		)
	,TargetTimeSeries = TargetTimeSeries.TargetTimeSeries

	,ReportDrillthroughEnabled =
		cast(
			coalesce(
				 Item.ReportDrillthroughEnabled
				,0
			)
			as bit
		)
	,TargetEnabled =
		--cast(
		--	coalesce(
		--		 Item.TargetEnabled
		--		,1
		--	)
		--	as bit
		--)
		CAST(	
			case
				when Item.ItemTypeID in (3,4) then
					coalesce(Item.TargetEnabled,0)
				else coalesce(Item.TargetEnabled,1)
			end
		as bit)
	,LastTargetValueOverride =
		cast(
			coalesce(
				 Item.LastTargetValueOverride
				,0
			)
			as bit
		)
	,AbsoluteAmberThreshold =
		cast(
			coalesce(
				 Item.AbsoluteAmberThreshold
				,0
			)
			as bit
		)	
	,TargetVisible =
		cast(
			coalesce(
				 Item.TargetVisible
				,1
			)
			as bit
		)	
	,TrendTimeSeriesOverride =
		cast(
			coalesce(
				Hierarchy.TrendTimeSeriesOverride
				,Item.TrendTimeSeriesOverride
				,0
			)
			as bit
		)	

	,ReportType = ReportType.ReportType	

from
	dbo.Hierarchy

inner join dbo.Item
on	Item.ItemID = Hierarchy.ItemID

inner join dbo.ItemType
on	Item.ItemTypeID = ItemType.ItemTypeID

left join dbo.Numerator
on	Numerator.NumeratorID = Item.NumeratorID

left join dbo.Denominator
on	Denominator.DenominatorID = Item.DenominatorID

left outer join dbo.Measure Measure
on	Measure.MeasureID = Item.MeasureID

left outer join dbo.ReportMeasure
on	ReportMeasure.ReportMeasureID = Item.ReportMeasureID

left outer join dbo.ReportMeasure ReportMeasureHierarchy
on	ReportMeasureHierarchy.ReportMeasureID = Hierarchy.ReportMeasureID

left join dbo.ReportImage HierarchyReportImage
on	HierarchyReportImage.ReportImageID = Hierarchy.ReportImageID

left join dbo.ReportImage ItemReportImage
on	ItemReportImage.ReportImageID = Item.ReportImageID

left join dbo.ReportColour HierarchyReportColour
on	HierarchyReportColour.ReportColourID = Hierarchy.ReportColourID

left join dbo.ReportColour ItemReportColour
on	ItemReportColour.ReportColourID = Item.ReportColourID

left join dbo.ReportColour HierarchyReportBackgroundColour
on	HierarchyReportBackgroundColour.ReportColourID = Hierarchy.ReportBackgroundColourID

left join dbo.ReportColour ItemReportBackgroundColour
on	ItemReportBackgroundColour.ReportColourID = Item.ReportBackgroundColourID

left join dbo.ReportingPeriod
on	ReportingPeriod.ReportingPeriodID = Item.ReportingPeriodID

left join dbo.Owner ExecutiveDirectorOwner
on	ExecutiveDirectorOwner.OwnerID = Item.ExecutiveDirectorOwnerID

left join dbo.Owner CorporateDirectorOwner
on	CorporateDirectorOwner.OwnerID = Item.CorporateDirectorOwnerID

left join dbo.Owner CommitteeOwner
on	CommitteeOwner.OwnerID = Item.CommitteeOwnerID

inner join dbo.TargetType
on	TargetType.TargetTypeID = Item.DefaultTargetTypeID

left join dbo.ItemFormat
on	ItemFormat.ItemFormatID = Item.ItemFormatID

left join dbo.TargetTimeSeries
on	TargetTimeSeries.TargetTimeSeriesID = Item.TargetTimeSeriesID

left join dbo.ReportType
on	ReportType.ReportTypeID = Hierarchy.ReportTypeID

where
	Item.Active = 1
and Hierarchy.Active = 1



















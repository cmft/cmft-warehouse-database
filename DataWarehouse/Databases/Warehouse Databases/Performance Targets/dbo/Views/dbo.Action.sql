﻿create view dbo.Action
as
select
	NarrativeID
	,ItemID
	,NarrativeTypeID
	,Narrative
	,Updated
	,ByWhom
from
	dbo.Narrative
where
	NarrativeTypeID = 3
and	not exists
			(
				select
					1
				from
					dbo.Narrative Later
				where
					Later.ItemID = Narrative.ItemID
				and Later.NarrativeTypeID = Narrative.NarrativeTypeID
				and	Later.Updated > Narrative.Updated
			)
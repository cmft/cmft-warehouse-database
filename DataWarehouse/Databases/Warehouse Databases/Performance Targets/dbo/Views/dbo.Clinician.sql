﻿






CREATE view [dbo].[Clinician] as

select
	 SourceClinicianID
	,SourceClinicianCode 
	,SourceClinician
	,SourceContext
	,LocalClinicianID 
	,LocalClinicianCode
	,LocalClinician 
	,NationalClinicianID 
	,ClinicianBase.NationalClinicianCode
	,NationalClinician 				
	,SourceContextCode
	,MainSpecialtyCode
	,MainSpecialty
	,ClinicanType
	,CentralPortalClinician = 
				case
				when CentralPortalClinician.NationalClinicianCode is not null
				then 'Yes'
				else 'No'
				end
from
	dbo.ClinicianBase
left join dbo.CentralPortalClinician
on	ClinicianBase.NationalClinicianCode = CentralPortalClinician.NationalClinicianCode


















﻿CREATE procedure [dbo].[BuildCANNonUrgentReferralDiagnosedWithCancerAll] as

exec BuildCANNonUrgentReferralDiagnosedWithCancer 'CO', 'Colorectal'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'GY', 'Gynaecology'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'HA', 'Haematology'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'HN', 'Head and Neck'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'LU', 'Lung'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'OT', 'Other'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'PA', 'Paediatric'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'SA', 'Sarcoma'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'SK', 'Skin'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'UG', 'Upper GI'
exec BuildCANNonUrgentReferralDiagnosedWithCancer 'UR', 'Urology'


﻿


CREATE procedure [dbo].[BuildWrkDataset] 
(
	@DatasetCode varchar(10)
	,@Start date = null
)

as

-- 20140722 RR, Added National Admission Method code to identify those who came via A&E
-- 20140723 RR included Finance Small dataset containing achievement of Financial plan
-- 20140804 RR included Cancelled Operation dataset
-- 20140807 RR included Friends and Family dataset
-- 20140808 RR/DG Revised APC
-- 20141015 RR excluded Finance Small dataset containing achievement of Financial plan, and added Finance Expenditure Actual and Budget
-- 20150109 RR added ACAgencySpend to HR section
-- 20150401 CH More complaint grades not to be included, plus ResponseDate
-- 20150610	RR HR process updated to account for additional columns relating to month in HR process.
-- 20150706 CH Outstanding Complaints added as a new census date based dataset was required for new indicators
-- 20150811 RR/CB added Activity data to Board Assurance
-- 20160120 CH added Discharge fields to RF dataset, and changed INC to look at PatientSafety S
-- 20160203 CB added RTT dataset 20160204 RR commented this section out until reviewed (tried running this section, stopped it after 45mins)

set @Start = coalesce(@Start, dateadd(year, -4, getdate()))


declare	@StartTime datetime = getdate()
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)


declare @deleted int
declare @inserted int

declare @LocalStart date = @Start
declare @LocalDatasetCode varchar(10) = @DatasetCode

declare @PATREGLocalStart date = @Start
declare @PATREGLocalDatasetCode varchar(10) = @DatasetCode

declare @LITLocalStart date = @Start
declare @LITLocalDatasetCode varchar(10) = @DatasetCode

declare @COMPLocalStart date = @Start
declare @COMPLocalDatasetCode varchar(10) = @DatasetCode

-- 20150706 CH Outstanding Complaints added as a new census date based dataset was required for new indicators
declare @OUTCOMPLocalStart date = @Start
declare @OUTCOMPLocalDatasetCode varchar(10) = @DatasetCode

--declare @SUBCOMPLocalStart date = @Start
--declare @SUBCOMPLocalDatasetCode varchar(10) = @DatasetCode

declare @OPERLocalStart date = @Start
declare @OPERLocalDatasetCode varchar(10) = @DatasetCode

declare @SESSLocalStart date = @Start
declare @SESSLocalDatasetCode varchar(10) = @DatasetCode

declare @IPWLCLocalStart date = @Start
declare @IPWLCLocalDatasetCode varchar(10) = @DatasetCode

declare @OPWLCLocalStart date = '1 Apr 2013'
declare @OPWLCLocalDatasetCode varchar(10) = @DatasetCode

declare @AESLocalStart date = @Start
declare @AESLocalDatasetCode varchar(10) = @DatasetCode

declare @RTTLocalStart date = '1 Jan 2015'
declare @RTTLocalDatasetCode varchar(10) = @DatasetCode

declare @CASELocalStart date = @Start
declare @CASELocalDatasetCode varchar(10) = @DatasetCode

declare @IPWLELocalStart date = @Start
declare @IPWLELocalDatasetCode varchar(10) = @DatasetCode

declare @AELocalStart date = @Start
declare @AELocalDatasetCode varchar(10) = @DatasetCode

declare @APCLocalStart date = @Start
declare @APCLocalDatasetCode varchar(10) = @DatasetCode

declare @OPLocalStart date = @Start
declare @OPLocalDatasetCode varchar(10) = @DatasetCode

declare @WSLocalStart date = @Start
declare @WSLocalDatasetCode varchar(10) = @DatasetCode

declare @COMLocalStart date = @Start
declare @COMLocalDatasetCode varchar(10) = @DatasetCode

declare @RFLocalStart date = @Start
declare @RFLocalDatasetCode varchar(10) = @DatasetCode

declare @COMRLocalStart date = @Start
declare @COMRLocalDatasetCode varchar(10) = @DatasetCode

declare @CWLLocalStart date = @Start
declare @CWLLocalDatasetCode varchar(10) = @DatasetCode

declare @INCLocalStart date = @Start
declare @INCLocalDatasetCode varchar(10) = @DatasetCode

declare @FALLLocalStart date = @Start
declare @FALLLocalDatasetCode varchar(10) = @DatasetCode

declare @PREULCLocalStart date = @Start
declare @PREULCLocalDatasetCode varchar(10) = @DatasetCode

declare @UTILocalStart date = @Start
declare @UTILocalDatasetCode varchar(10) = @DatasetCode

declare @VTELocalStart date = @Start
declare @VTELocalDatasetCode varchar(10) = @DatasetCode

declare @QCRLocalStart date = @Start
declare @QCRLocalDatasetCode varchar(10) = @DatasetCode

declare @NEOLocalStart date = @Start
declare @NEOLocalDatasetCode varchar(10) = @DatasetCode

declare @MATLocalStart date = @Start
declare @MATLocalDatasetCode varchar(10) = @DatasetCode

declare @RENQLocalStart date = @Start
declare @RENQLocalDatasetCode varchar(10) = @DatasetCode

declare @RENLocalStart date = @Start
declare @RENLocalDatasetCode varchar(10) = @DatasetCode

declare @CANTLocalStart date = @Start
declare @CANTLocalDatasetCode varchar(10) = @DatasetCode

declare @CANLocalStart date = @Start
declare @CANLocalDatasetCode varchar(10) = @DatasetCode

declare @MANLocalStart date = @Start
declare @MANLocalDatasetCode varchar(10) = @DatasetCode

--20140723 RR added the following to account for the Finance Small dataset containing achievement of Financial plan
declare @FINANCELocalStart date = @Start
declare @FINANCELocalDatasetCode varchar(10) = @DatasetCode

declare @GOVERNANCELocalStart date = @Start
declare @GOVERNANCELocalDatasetCode varchar(10) = @DatasetCode

declare @HRLocalStart date = @Start
declare @HRLocalDatasetCode varchar(10) = @DatasetCode

declare @STAFFLEVELLocalStart date = @Start
declare @STAFFLEVELLocalDatasetCode varchar(10) = @DatasetCode

declare @AlertLocalStart date = @Start
declare @AlertLocalDatasetCode varchar(10) = @DatasetCode

-- 20140804 RR added the following for Cancelled Operation dataset
declare @CancOpsLocalStart date = @Start
declare @CancOpsLocalDatasetCode varchar(10) = @DatasetCode 

-- 20140807 RR added the following for Friends and Family dataset
declare @FFTLocalStart date = @Start
declare @FFTLocalDatasetCode varchar(10) = @DatasetCode


declare @HSMRLocalStart date = @Start
declare @HSMRLocalDatasetCode varchar(10) = @DatasetCode

--20150811 RR added Activity
declare @ActivityLocalStart date = @Start
declare @ActivityLocalDatasetCode varchar(10) = @DatasetCode

--20151005 CH added Activity
declare @BedOccupancyLocalStart date = @Start
declare @BedOccupancyLocalDatasetCode varchar(10) = @DatasetCode

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare @DatasetID smallint =
	(
	select
		DatasetID
	from
		dbo.Dataset
	where
		Dataset.DatasetCode = @DatasetCode
	)

--delete
--from
--	dbo.WrkDataset
--where
--	DatasetID = @DatasetID

/*	
	Deletes taking far too long so table now truncated at start of the process. This can always be re-instated if 
	we decide to tag the build dataset process at the end of each build model process in womv2
*/
	
--select
--	@deleted = @@rowcount

/* HR */

-- 20150109 RR added ACAgencySpend to HR section
-- 20150610	RR HR process updated to account for additional columns relating to month in HR process.


if @HRLocalDatasetCode = 'HR' 

begin
	
	insert dbo.WrkDataset 

	(
		DatasetRecno
		,DatasetID
		,CensusDateID
		,AppraisalRequired
		,AppraisalCompleted
		,ClinicalMandatoryTrainingRequired
		,ClinicalMandatoryTrainingCompleted
		,CorporateMandatoryTrainingRequired
		,CorporateMandatoryTrainingCompleted
		,RecentAppointment
		,LongTermAppointment
		,SicknessAbsenceFTE
		,SicknessEstablishmentFTE
		,Headcount
		,FTE
		,StartersHeadcount
		,StartersFTE
		,LeaversHeadcount
		,LeaversFTE
		,ClinicalBudgetWTE
		,ClinicalContractedWTE
		,ServicePointID
		,StartDirectorateID 
		,EndDirectorateID
		,StartSiteID 
		,EndSiteID 
		,SpecialtyID 
		,ClinicianID 
		,ACAgencySpend
		,HeadcountMonthly 
		,FTEMonthly 
		,StartersHeadcountMonthly 
		,StartersFTEMonthly 
		,LeaversHeadcountMonthly 
		,LeaversFTEMonthly 
		,BMERecentAppointment
		,BMELongTermAppointment
		,ESRSIP	
		,GeneralLedgerEst	
		,ESRSIPBand5	
		,GeneralLedgerEstBand5
		,HeadcountPeriodStart	
		,HeadcountPeriodEnd	
		,FTERetentionIndex	
		,HeadcountRetentionIndex	
		,BMEHeadcountPeriodStart	
		,BMEHeadcountPeriodEnd	
		,BMEFTERetentionIndex	
		,BMEHeadcountRetentionIndex
		)

	select
		BaseSummary.MergeSummaryRecno
		,DatasetID = @DatasetID
		,BaseSummaryReference.CensusDateID
		,AppraisalRequired
		,AppraisalCompleted
		,ClinicalMandatoryTrainingRequired
		,ClinicalMandatoryTrainingCompleted
		,CorporateMandatoryTrainingRequired
		,CorporateMandatoryTrainingCompleted
		,RecentAppointment
		,LongTermAppointment
		,SicknessAbsenceFTE
		,SicknessEstablishmentFTE
		,Headcount3mthRolling
		,FTE3mthRolling
		,StartersHeadcount3mthRolling
		,StartersFTE3mthRolling
		,LeaversHeadcount3mthRolling
		,LeaversFTE3mthRolling
		,ClinicalBudgetWTE
		,ClinicalContractedWTE
		,BaseSummaryReference.WardID
		,DirectorateID = Directorate.DirectorateID
		,DirectorateID = Directorate.DirectorateID
		,StartSiteID = Site.SourceSiteID
		,EndSiteID = Site.SourceSiteID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,ConsultantID = Consultant.SourceConsultantID
		,ACAgencySpend = (ACAgencySpend/1000)
		,HeadcountMonthly 
		,FTEMonthly 
		,StartersHeadcountMonthly 
		,StartersFTEMonthly 
		,LeaversHeadcountMonthly 
		,LeaversFTEMonthly
		,BMERecentAppointment
		,BMELongTermAppointment
		,ESRSIP	
		,GeneralLedgerEst	
		,ESRSIPBand5	
		,GeneralLedgerEstBand5
		,HeadcountPeriodStart	
		,HeadcountPeriodEnd	
		,FTERetentionIndex	
		,HeadcountRetentionIndex	
		,BMEHeadcountPeriodStart	
		,BMEHeadcountPeriodEnd	
		,BMEFTERetentionIndex	
		,BMEHeadcountRetentionIndex
	from
		WarehouseOLAPMergedV2.HR.BaseSummary

	inner join WarehouseOLAPMergedV2.HR.BaseSummaryReference
	on	BaseSummary.MergeSummaryRecno = BaseSummaryReference.MergeSummaryRecno

	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = coalesce(BaseSummary.DirectorateCode, '9')

	inner join WarehouseOLAPMergedV2.WH.Site
	on Site.SourceContextCode = 'CMFT||ESREXT'
	and Site.SourceSiteCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = 'CMFT||ESREXT'
	and Specialty.SourceSpecialtyCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CMFT||ESREXT'
	and Consultant.SourceConsultantCode = '-1'

select
	@inserted = @@rowcount

end


-- 20140723 RR added the following to bring in data for achievement of financial plan
-- 20141015 RR Item changed.  Finance now providing data on Expenditure, budget and actual

if @FINANCELocalDatasetCode = 'FINANCE' 

begin

	insert into WrkDataset
		(DatasetID
		,DatasetRecno
		,CensusDate
		,CensusDateID
		,FinanceBudget 
		,FinanceAnnualBudget
		,FinanceActual 
		,Division
		,EndDirectorateID
		,EndSiteID
		,SpecialtyID
		,ClinicianID
		,ServicePointID
		)
	select
		DatasetID = @DatasetID
		,DatasetRecno = Expenditure.MergeExpenditureRecno 
		,CensusDate = Expenditure.CensusDate
		,CensusDateID = Reference.CensusDateID
		,FinanceBudget = Expenditure.Budget
		,FinanceAnnualBudget = -- Expenditure.AnnualBudget
							(Expenditure.AnnualBudget / cast(12 as float)) * cast(right(FinancialMonthKey, 2) as int)
		,FinanceActual = Expenditure.Actual
		,Division = Expenditure.Division
		,EndDirectorateID = coalesce(Directorate.DirectorateID,15)					
		,EndSiteID = Site.SourceSiteID	
		,SpecialtyID = Specialty.SourceSpecialtyID
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID	= ServicePoint.SourceServicePointID	
	from
		WarehouseOLAPMergedV2.Finance.BaseExpenditure Expenditure
	
	inner join WarehouseOLAPMergedV2.Finance.BaseExpenditureReference Reference
	on Expenditure.MergeExpenditureRecno = Reference.MergeExpenditureRecno

	inner join WarehouseOLAPMergedV2.Wh.Calendar 
	on Reference.CensusDateID = Calendar.DateID
		
	inner join WarehouseOLAPMergedV2.Allocation.DatasetAllocation
	on	DatasetAllocation.DatasetRecno = Expenditure.MergeExpenditureRecno
	and	DatasetAllocation.DatasetCode = 'FINEXP'
	and	DatasetAllocation.AllocationTypeID = 10

	inner join WarehouseOLAPMergedV2.Allocation.Allocation
	on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
	and	Allocation.AllocationID = DatasetAllocation.AllocationID
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Allocation.SourceAllocationID = Directorate.DirectorateCode
		
	inner join WarehouseOLAPMergedV2.WH.Site
	on Site.SourceContextCode = 'CMFT||ORCL'
	and Site.SourceSiteCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = 'CMFT||ORCL'
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CMFT||ORCL'
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
		
	where
		Expenditure.CensusDate >= @FINANCELocalStart
		
/*	insert into WrkDataset
	(
		DatasetID
		,DatasetRecno
		,ActivityDate
		,ActivityDateID
		,StartDirectorateID
		,EndDirectorateID
		,FinanceRiskRating
		,StartSiteID
		,SpecialtyID
		,ClinicianID
		,ServicePointID
	)
	select
		DatasetID = @DatasetID
		,DatasetRecno = Fin.ID 
		,ActivityDate = Fin.[Month]
		,ActivityDateID = Calendar.DateID
		,StartDirectorateID = coalesce(Directorate.DirectorateID,15)
		,EndDirectorateID = coalesce(Directorate.DirectorateID,15)					
		,FinanceRiskRating = 
				case
					when RiskRating = 'R' then 1
					when RiskRating = 'A' then 2
					when RiskRating = 'G' then 3
					else null
				end
				
		,StartSiteID = Site.SourceSiteID	
		,SpecialtyID = Specialty.SourceSpecialtyID
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID	= ServicePoint.SourceServicePointID	
	from
		SmallDatasets.Finance.RiskRatingByDivision Fin
		
	inner join WarehouseOLAPMergedV2.WH.Calendar Calendar
	on	Calendar.TheDate = Fin.[Month]
	
	left join WarehouseOLAPMergedV2.WH.Directorate
	on	Fin.Division = Directorate.Directorate
		
	inner join WarehouseOLAPMergedV2.WH.Site
	on Site.SourceContextCode = 'CMFT||PERFMON'
	and Site.SourceSiteCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = 'CMFT||PERFMON'
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CMFT||PERFMON'
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
		
	where
		Fin.[Month] > @FINANCELocalStart
*/
select
	@inserted = @@rowcount
end

--------

-- 20140827 RR added the following to bring in data for GovernanceRiskRating
/* Achievement of Financial Plan */

if @GOVERNANCELocalDatasetCode = 'Governance' 

begin

	insert into WrkDataset
	(
		DatasetID
		,DatasetRecno
		,ActivityDate
		,ActivityDateID
		,StartDirectorateID
		,EndDirectorateID
		,GovernanceRiskRating
		,StartSiteID
		,SpecialtyID
		,ClinicianID
		,ServicePointID
	)
	select
		DatasetID = @DatasetID
		,DatasetRecno = Gov.ID 
		,ActivityDate = Gov.[Month]
		,ActivityDateID = Calendar.DateID
		,StartDirectorateID = coalesce(Directorate.DirectorateID,15)
		,EndDirectorateID = coalesce(Directorate.DirectorateID,15)					
		,GovernanceRiskRating = 
				case
					when RiskRating = 'R' then 1
					when RiskRating = 'AR' then 2
					when RiskRating = 'AG' then 2
					when RiskRating = 'G' then 3
					else null
				end
		,StartSiteID = Site.SourceSiteID	
		,SpecialtyID = Specialty.SourceSpecialtyID
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID	= ServicePoint.SourceServicePointID	

	from
		SmallDatasets.Governance.RiskRatingByDivision Gov
		
	inner join WarehouseOLAPMergedV2.WH.Calendar Calendar
	on	Calendar.TheDate = Gov.[Month]
	
	left join WarehouseOLAPMergedV2.WH.Directorate
	on	Gov.Division = Directorate.Directorate
		
	inner join WarehouseOLAPMergedV2.WH.Site
	on Site.SourceContextCode = 'CMFT||PERFMON'
	and Site.SourceSiteCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = 'CMFT||PERFMON'
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CMFT||PERFMON'
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
		
	where
		Gov.[Month] >= @GOVERNANCELocalStart

select
	@inserted = @@rowcount
end

--------

/* Patient Registrations */

if @PATREGLocalDatasetCode = 'PATREG' 

begin

	insert into WrkDataset
	
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,SourcePatientNo
		,RegistrationDate
		,RegistrationDateID
		,IsDuplicate
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID
		,StartServicePointID
	)
	
	-- Take SourcePatientNo and RegistrationDate from Warehouse.PAS.Patient

	-- Take duplicates from WarehouseOLAPMergedV2.PAS.BasePatientDuplication, 
	-- ignoring first registered no for each real patient (IsEarliestRegistration = 'Y')
	
	-- DG 27.2.14 - Registration date not populated?

	select
		DatasetRecno = Registration.SourcePatientNo
		,DatasetID = @DatasetID
		,ContextCode = 'CEN||PAS'
		,Registration.SourcePatientNo
		,Registration.RegistrationDate
		,Calendar.DateID
		,IsDuplicate = 
						case 
						when DuplicateRegistration.SourcePatientNo is null 
						then 0 
						else 1 
						end
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = SourceServicePointID
		,StartServicePointID = SourceServicePointID
		
	from
		Warehouse.PAS.Patient Registration
		
	inner join WarehouseOLAPMergedV2.WH.Calendar 
	on	Calendar.TheDate = Registration.RegistrationDate
	
	left join WarehouseOLAPMergedV2.Patient.BaseDuplication DuplicateRegistration 
	on	DuplicateRegistration.IsEarliestRegistration = 'N' 
	and DuplicateRegistration.SourcePatientNo = Registration.SourcePatientNo
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = 'CEN||PAS'
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = 'CEN||PAS'
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CEN||PAS'
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
	
	
	where
		--datediff(year, Registration.RegistrationDate, getdate()) <= 2
		Registration.RegistrationDate >= @PATREGLocalStart

select
	@inserted = @@rowcount
	
end

/*  Litigation (Claims) */

if @LITLocalDatasetCode = 'LIT' 

begin

	insert into WrkDataset
	
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,AdvisedDate
		,AdvisedDateID
		,ReasonCode
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID

	)
	
	select
		DatasetRecno = L.LitigationID
		,DatasetID = @DatasetID
		,L.ContextCode
		,L.AdvisedDate
		,Calendar.DateID
		,L.ReasonCode
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = SourceServicePointID

	from
		WarehouseOLAPMergedV2.Litigation.BaseLitigation L
		
	inner join WarehouseOLAPMergedV2.WH.Calendar 
	on	Calendar.TheDate = L.AdvisedDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = L.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = L.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = L.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
	
	where
		L.AdvisedDate > @LITLocalStart

select
	@inserted = @@rowcount
end

/* Complaints */

if @COMPLocalDatasetCode = 'COMP' 

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,ReceiptDateID
		,CategoryTypeCode
		,CaseTypeCode
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID
		,Complaint25DayTargetDateID
		,Complaint40DayTargetDateID	
		,Complaint25DayTargetDate
		,Complaint40DayTargetDate	
		,ReceiptDate
		,ConsentDate
		,ResolutionDate
		,SequenceNo
		,Reopened
		,ResponseDate
	)


	select
		DatasetRecno = BaseComplaint.MergeComplaintRecno
		,DatasetID = @DatasetID
		,BaseComplaint.ContextCode
		,BaseComplaintReference.ReceiptDateID
		,BaseComplaint.CategoryTypeCode
		,BaseComplaint.CaseTypeCode
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = BaseComplaintReference.WardID
		,Complaint25DayTargetDateID = BaseComplaintReference.TargetDate25DayID
		,Complaint40DayTargetDateID = BaseComplaintReference.TargetDate40DayID
		,Complaint25DayTargetDate = BaseComplaint.TargetDate25Day
		,Complaint40DayTargetDate = BaseComplaint.TargetDate40Day
		,BaseComplaint.ReceiptDate
		,BaseComplaint.ConsentDate
		,BaseComplaint.ResolutionDate
		,BaseComplaint.SequenceNo
		,BaseComplaint.Reopened
		,BaseComplaint.ResponseDate

	from
		WarehouseOLAPMergedV2.Complaint.BaseComplaint

	inner join WarehouseOLAPMergedV2.Complaint.BaseComplaintReference
	on	BaseComplaint.MergeComplaintRecno = BaseComplaintReference.MergeComplaintRecno
		
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = coalesce(BaseComplaint.DirectorateCode, '9')
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = BaseComplaint.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = BaseComplaint.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = BaseComplaint.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	where
		BaseComplaint.ReceiptDate >= @COMPLocalStart
	and BaseComplaint.GradeCode not in ('0x4F20' --De-Escalated
										,'0x5220' --withdrawn
										,'0x5320' --out of time
										,'0x5420' --Consent/Info Not Received  CH 20150401                       
										,'0x5120') --Escalated CH 20150702

select
	@inserted = @@rowcount
	
end

/* Outstanding Complaints */
--20150706 CH Outstanding Complaints added as a new census date based dataset was required for new indicators

if @OUTCOMPLocalDatasetCode = 'OUTCOMP' 

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,ReceiptDateID
		,CategoryTypeCode
		,CaseTypeCode
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID
		,Complaint25DayTargetDateID
		,Complaint40DayTargetDateID	
		,Complaint25DayTargetDate
		,Complaint40DayTargetDate	
		,ReceiptDate
		,ConsentDate
		,ResolutionDate
		,SequenceNo
		,Reopened
		,ResponseDate
		,CensusDateID
		,CensusDate
	)


	select
		DatasetRecno = BaseComplaint.MergeOutstandingRecno
		,DatasetID = @DatasetID --47
		,BaseComplaint.ContextCode
		,BaseComplaintReference.ReceiptDateID
		,BaseComplaint.CategoryTypeCode
		,BaseComplaint.CaseTypeCode
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = BaseComplaintReference.WardID
		,Complaint25DayTargetDateID = BaseComplaintReference.TargetDate25DayID
		,Complaint40DayTargetDateID = BaseComplaintReference.TargetDate40DayID
		,Complaint25DayTargetDate = BaseComplaint.TargetDate25Day
		,Complaint40DayTargetDate = BaseComplaint.TargetDate40Day
		,BaseComplaint.ReceiptDate
		,BaseComplaint.ConsentDate
		,BaseComplaint.ResolutionDate
		,BaseComplaint.SequenceNo
		,BaseComplaint.Reopened
		,BaseComplaint.ResponseDate
		,CensusDateID = DateID
		,CensusDate = TheDate

	from
		WarehouseOLAPMergedV2.Complaint.BaseOutstandingComplaint BaseComplaint

	inner join WarehouseOLAPMergedV2.Complaint.BaseComplaintReference
	on	BaseComplaint.MergeComplaintRecno = BaseComplaintReference.MergeComplaintRecno
		
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = coalesce(BaseComplaint.DirectorateCode, '9')
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = BaseComplaint.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = BaseComplaint.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = BaseComplaint.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Calendar
	on	Calendar.TheDate = BaseComplaint.CensusDate
	and Calendar.LastDayOfMonth = BaseComplaint.CensusDate	
	
	where
		BaseComplaint.ReceiptDate >= @OUTCOMPLocalStart
	--and StatusTypeCode <> 'E' -- Awaiting Consent/Info  --removed CH 09072015                           	
	and BaseComplaint.GradeCode not in ('0x4F20' --De-Escalated
										,'0x5220' --withdrawn
										,'0x5320' --out of time
										,'0x5420' --Consent/Info Not Received                      
										,'0x5120') --Escalated CH 20150702

select
	@inserted = @@rowcount
	
end
	
/*	Theatre Operations */

if @OPERLocalDatasetCode = 'OPER' 

begin

	insert into WrkDataset
	
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		--,StartDirectorateID
		--,StartDirectorateCode
		--,SpecialtyID
		--,SpecialtyCode
		--,ClinicianID
		--,ClinicianCode
		--,StartSiteID
		--,StartSiteCode
		,OperationDate
		,OperationDateID
		,CancelReasonCode
		,TurnaroundTime		
		,TurnaroundTimeCount
		--,ServicePointID
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID
		,StartServicePointID
		,UtilisationMinutes
		,AnaesthetistActiveTheatreTime 
		,SurgeonActiveTheatreTime 
		,CancelledOnDay 
		,SessionSourceUniqueID
	)
	
	select
		DatasetRecno = F.MergeRecno
		,DatasetID = @DatasetID
		,B.ContextCode
		--,StartDirectorateID = null -- No Directorate on Theatre.BaseOperationDetail table
		--,StartDirectorateCode = null
		--,SpecialtyID = S.SourceSpecialtyID
		--,SpecialtyCode = Specialty.NationalSpecialtyCode
		--,ClinicianID = null -- Consultant on Theatre.BaseOperationReference is from Theatre.Staff table. Leave null for now.
		--,ClinicianCode = null
		--,StartSiteID = null -- Site should presumably be derived from Theatre, but no xref on Theatre.Theatre table. Leave null for now
		--,StartSiteCode = null
		,OperationDate = C.TheDate
		,OperationDateID = C.DateID
		,CancelReasonCode = CR.SourceCancelReasonCode
		,TurnaroundTime = datediff(minute, B.OperationEndDate, B.NextOperationAnaestheticInductionTime) -- Please note this is a different definition to that in the Fact table, Fact table excludes negative results.  The definition provided by the Transformation team  - 'Where a subsequent patient’s anaesthesia is induced prior to the previous patient entering recovery the turnaround time will be a negative numerical value.'
		,TurnaroundTimeCount = 
			case 
				when B.NextOperationAnaestheticInductionTime is not null then 1 
				else 0 
			end
		--,F.TheatreID
		,StartDirectorateID = F.DirectorateID--Directorate.DirectorateID
		,EndDirectorateID = F.DirectorateID--Directorate.DirectorateID					
		,SpecialtyID = F.SessionSpecialtyID
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = F.SessionConsultantID
		,ServicePointID = SourceServicePointID
		,StartServicePointID = SourceServicePointID

		,UtilisationMinutes =
			datediff(
				 minute
				,coalesce(
					 B.AnaestheticInductionTime
					,B.InAnaestheticTime
				)
				,coalesce(
					 B.OperationEndDate
					,B.InRecoveryTime
					,B.ReadyToDepartTime
				)
			)
			
		,AnaesthetistActiveTheatreTime = 
			case 
				when B.AnaestheticInductionTime is not null 
					and coalesce(B.InRecoveryTime,B.OperationEndDate) is not null
				then datediff(minute
								,B.AnaestheticInductionTime
								,coalesce(
										B.InRecoveryTime
										,B.OperationEndDate
										)
								)
				else null
			end
		
		,SurgeonActiveTheatreTime = 
			case 
				when B.OperationStartDate is not null 
					and coalesce(B.InRecoveryTime,B.OperationEndDate) is not null
				then datediff(minute
								,B.OperationStartDate
								,coalesce(
										B.InRecoveryTime
										,B.OperationEndDate
										)
								)
				else null
			end
			
		,CancelledOnDay = 
			case
				when cast(BaseCancellation.CancellationDate as date) = cast(C.TheDate as date) 
				then 1
				else 0
			end
		,B.SessionSourceUniqueID
	from
		WarehouseOLAPMergedV2.Theatre.FactOperation F
	
	inner join WarehouseOLAPMergedV2.Theatre.BaseOperationDetail B 
	on	B.MergeRecno = F.MergeRecno
	
	inner join WarehouseOLAPMergedV2.Theatre.CancelReason CR 
	on	CR.SourceCancelReasonID = F.CancelReasonID
	
	--inner join WarehouseOLAPMergedV2.WH.Specialty S 
	--on	S.SourceSpecialtyID = F.SessionSpecialtyID
	
	inner join WarehouseOLAPMergedV2.WH.Calendar C 
	on	C.DateID = F.OperationDateID
	
	inner join WarehouseOLAPMergedV2.WH.Context 
	on	F.ContextID = Context.ContextID
	
	--inner join WarehouseOLAPMergedV2.WH.Directorate
	--on	Directorate.DirectorateCode = '9'
	
	--inner join WarehouseOLAPMergedV2.WH.Specialty
	--on Specialty.SourceSpecialtyID = F.SessionSpecialtyID
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = Context.ContextCode
	and Site.SourceSiteCode = '-1'
	
	--inner join WarehouseOLAPMergedV2.Theatre.Staff
	--on	Staff.SourceStaffID = F.SessionConsultantID
	--inner join WarehouseOLAPMergedV2.WH.Consultant
	--on	Consultant.SourceContextCode = Context.ContextCode
	--and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = F.TheatreID
	
	left join WarehouseOLAPMergedV2.Theatre.BaseCancellation
	on	BaseCancellation.OperationDetailSourceUniqueID = B.SourceUniqueID
	and	BaseCancellation.ContextCode = B.ContextCode
	and	not exists
		(
		select
			1
		from
			WarehouseOLAPMergedV2.Theatre.BaseCancellation Previous
		where
			Previous.OperationDetailSourceUniqueID = B.SourceUniqueID
		and	Previous.ContextCode = B.ContextCode
		and	(
				Previous.CancellationDate > BaseCancellation.CancellationDate
			or	(
					Previous.CancellationDate = BaseCancellation.CancellationDate
				and	Previous.SourceUniqueID > BaseCancellation.SourceUniqueID
				)
			)
	)
	where
		C.TheDate >= @OPERLocalStart

select
	@inserted = @@rowcount
	
end
	
/*	Theatre Sessions	*/

if @SESSLocalDatasetCode = 'SESS' 

begin
	
	-- Query runs very slowly as a single insert, but quickly if view Theatre.FactSession is executed first
	-- Use a temp table to force this order 
	
	begin
		select 
			MergeRecno
			,SpecialtyID
			,ConsultantID
			,SessionDateID
			,CancelledFlag
			,PlannedSessionDuration
			,ActualDuration
			,EarlyStart
			,LateStart
			,EarlyFinish
			,LateFinish
			,EarlyStartMinutes
			,LateStartMinutes
			,EarlyFinishMinutes
			,LateFinishMinutes
			,AllTurnaroundsWithin15Minutes
			,TheatreID	
			,DirectorateID
		into
			#FactSession 
		from
			WarehouseOLAPMergedV2.Theatre.FactSession

		insert into WrkDataset
		
		(
			DatasetRecno
			,DatasetID
			,ContextCode
			--,StartDirectorateID
			--,StartDirectorateCode
			--,SpecialtyID
			--,SpecialtyCode
			--,ClinicianID
			--,ClinicianCode
			--,StartSiteID
			--,StartSiteCode
			,SessionDate
			,SessionDateID
			,CancelledFlag
			,PlannedDuration
			,Duration
			,EarlyStart
			,LateStart
			,EarlyFinish
			,LateFinish
			,EarlyStartMinutes
			,LateStartMinutes
			,EarlyFinishMinutes
			,LateFinishMinutes
			,AllTurnaroundsWithin15Minutes
			--,ServicePointID	
			,StartDirectorateID
			,EndDirectorateID
			,SpecialtyID
			,StartSiteID
			,EndSiteID
			,ClinicianID
			,ServicePointID
			,StartServicePointID
			,SessionSourceUniqueID
			
		)
		
		select
			DatasetRecno = F.MergeRecno
			,DatasetID = @DatasetID
			,B.ContextCode
			--,StartDirectorateID = null -- No Directorate on Theatre.BaseOperationDetail table
			--,StartDirectorateCode = null
			--,S.SourceSpecialtyID
			--,Specialty.NationalSpecialtyCode
			--,ClinicianID = null -- Consultant on Theatre.BaseOperationReference is from Theatre.Staff table. Leave null for now.
			--,ClinicianCode = null
			--,StartSiteID = null -- Site should presumably be derived from Theatre, but no xref on Theatre.Theatre table. Leave null for now
			--,StartSiteCode = null
			,SessionDate = C.TheDate
			,SessionDateID = F.SessionDateID
			,F.CancelledFlag
			,PlannedDuration = F.PlannedSessionDuration
			,Duration = F.ActualDuration
			,F.EarlyStart
			,F.LateStart
			,F.EarlyFinish
			,F.LateFinish
			,F.EarlyStartMinutes
			,F.LateStartMinutes
			,F.EarlyFinishMinutes
			,F.LateFinishMinutes
			,F.AllTurnaroundsWithin15Minutes
			--,F.TheatreID	
			,StartDirectorateID = F.DirectorateID--Directorate.DirectorateID
			,EndDirectorateID = F.DirectorateID--Directorate.DirectorateID					
			,SpecialtyID = F.SpecialtyID	
			,StartSiteID = Site.SourceSiteID	
			,EndSiteID = Site.SourceSiteID	
			,ClinicianID = F.ConsultantID
			,ServicePointID = SourceServicePointID
			,StartServicePointID = SourceServicePointID
			,B.SourceUniqueID
		from
			#FactSession F
			
		inner join WarehouseOLAPMergedV2.Theatre.BaseSession B 
		on B.MergeRecno = F.MergeRecno
		
		--inner join WarehouseOLAPMergedV2.WH.Specialty S 
		--on S.SourceSpecialtyID = F.SpecialtyID
		
		inner join WarehouseOLAPMergedV2.WH.Calendar C 
		on C.DateID = F.SessionDateID

		--inner join WarehouseOLAPMergedV2.WH.Directorate
		--on	Directorate.DirectorateCode = '9'
		
		--inner join WarehouseOLAPMergedV2.WH.Specialty -- Don't need we can pull ID direct from the FACT table
		--on Specialty.SourceSpecialtyID = F.SpecialtyID
		
		inner join WarehouseOLAPMergedV2.WH.Site
		on	Site.SourceContextCode = B.ContextCode
		and Site.SourceSiteCode = '-1'
		
		--inner join WarehouseOLAPMergedV2.Theatre.Staff -- don't need we can pull ID direct from the FACT table
		--on	Staff.SourceStaffID = F.ConsultantID
		--inner join WarehouseOLAPMergedV2.WH.Consultant
		--on	Consultant.SourceContextCode = B.ContextCode
		--and Consultant.SourceConsultantCode = '-1'
		
		inner join WarehouseOLAPMergedV2.WH.ServicePoint
		on ServicePoint.SourceServicePointID = F.TheatreID

		where
			C.TheDate >= @SESSLocalStart
	end
		
select
	@inserted = @@rowcount
	
end
	
/*	IP Waiting List Census - Loads census for Active waiters, first-day-of-week census only - changed to last day of month CH 02/02/2015*/ 
	
if @IPWLCLocalDatasetCode = 'IPWLC'

begin

	insert into WrkDataset
	
	(
		DatasetRecno
		,DatasetID
		,SourceUniqueID
		,ContextCode
		,StartDirectorateCode
		,SpecialtyCode
		,ClinicianCode
		,StartSiteCode
		,CensusDate
		,CensusDateID
		,TCIDate
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID
		,StartServicePointID
		,Duration
	)

	select
		E.MergeEncounterRecno
		,DatasetID = @DatasetID
		,E.SourceUniqueID
		,E.ContextCode
		,StartDirectorateCode = Directorate.DirectorateCode
		,E.SpecialtyCode
		,ClinicianCode = E.ConsultantCode
		,StartSiteCode = E.SiteCode
		,E.CensusDate
		,R.CensusDateID
		,E.TCIDate
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = R.SpecialtyID
		,StartSiteID = R.SiteID
		,EndSiteID = R.SiteID
		,ClinicianID = R.ConsultantID
		,ServicePointID = R.WardID	
		,StartServicePointID = R.WardID
		,Duration = [LengthOfWait]/7 -- to get weeks	
		
	from
		WarehouseOLAPMergedV2.APCWL.BaseEncounter E
		
	inner join WarehouseOLAPMergedV2.APCWL.BaseEncounterReference R 
	on R.MergeEncounterRecno = E.MergeEncounterRecno
	
	inner join WarehouseOLAPMergedV2.WH.WaitTypeBase TY 
	on TY.WaitTypeID = R.WaitTypeID
	
	inner join WarehouseOLAPMergedV2.WH.Calendar Census 
	--on Census.TheDate = E.CensusDate and Census.TheDate = Census.FirstDayOfWeek -- Take first day of week census only
	on Census.TheDate = E.CensusDate and Census.TheDate = Census.LastDayOfMonth -- We only require last day of month as census
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	coalesce(E.DirectorateCode, '9') = Directorate.DirectorateCode
		
	where
		TY.WaitType = 'Active'
	and	E.CensusDate >= @IPWLCLocalStart

select
	@inserted = @@rowcount
	
end	

	
/*	OP Waiting List Census - Loads census for Active waiters, Lats day of the month census only */
	
if @OPWLCLocalDatasetCode = 'OPWLC'

begin

	insert into WrkDataset
	
	(
		DatasetRecno
		,DatasetID
		,SourceUniqueID
		,ContextCode
		,StartDirectorateCode
		,SpecialtyCode
		,ClinicianCode
		,StartSiteCode
		,CensusDate
		,CensusDateID
		,TCIDate
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID
		,StartServicePointID
		,Duration
		,NationalFirstAttendanceCode
	)

	select
		E.MergeEncounterRecno
		,DatasetID = @DatasetID
		,E.SourceUniqueID
		,E.ContextCode
		,StartDirectorateCode = Directorate.DirectorateCode
		,E.SpecialtyCode
		,ClinicianCode = E.ConsultantCode
		,StartSiteCode = E.SiteCode
		,E.CensusDate
		,R.CensusDateID
		,E.TCIDate
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = R.SpecialtyID
		,StartSiteID = R.SiteID
		,EndSiteID = R.SiteID
		,ClinicianID = R.ConsultantID
		,ServicePointID = Clinic.SourceClinicID	
		,StartServicePointID = Clinic.SourceClinicID
		,Duration = [LengthOfWait]/7 -- to get weeks	
		,NationalFirstAttendanceCode
		
	from
		WarehouseOLAPMergedV2.OPWL.BaseEncounter E
		
	inner join WarehouseOLAPMergedV2.OPWL.BaseEncounterReference R 
	on R.MergeEncounterRecno = E.MergeEncounterRecno
	
	inner join WarehouseOLAPMergedV2.WH.WaitTypeBase TY 
	on TY.WaitTypeID = R.WaitTypeID
	
	inner join WarehouseOLAPMergedV2.WH.Calendar Census 
	on Census.TheDate = case when E.CensusDate = '24 May 2015' then '31 May 2015' else E.CensusDate end and case when E.CensusDate = '24 May 2015' then '31 May 2015' else E.CensusDate end = Census.LastDayOfMonth -- We only require last day of month as census

	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	coalesce(E.DirectorateCode, '9') = Directorate.DirectorateCode

	inner join WarehouseOLAPMergedV2.OP.Clinic
	on	coalesce(E.ClinicCode, '-1') = Clinic.SourceClinicCode
	and E.ContextCode = Clinic.SourceContextCode 

	left join WarehouseOLAPMergedV2.OP.FirstAttendance
	on	FirstAttendance.SourceFirstAttendanceID = R.FirstAttendanceID
			
	where
		TY.WaitType = 'Active'
	and	E.CensusDate >= @OPWLCLocalStart

select
	@inserted = @@rowcount
	
end	
	
/*	A&E Stages, from AE,FactEncounter */

if @AESLocalDatasetCode = 'AES'

begin

	insert into WrkDataset
	
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,SourcePatientNo
		--,EndDirectorateID
		--,EndDirectorateCode
		--,SpecialtyID
		--,Specialty
		--,ClinicianID
		--,ClinicianCode
		--,EndSiteID
		--,EndSiteCode
		,StageCode
		,BreachStatusCode
		,DepartmentTypeCode
		,Duration
		,UnplannedReattend7Days
		,LeftWithoutBeingSeen
		,ArrivalModeCode
		,AttendanceDisposalCode
		,EncounterDate
		,EncounterDateID
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID
		,StartServicePointID
	)
	
	select
		DatasetRecno = Fact.MergeEncounterRecno
		,DatasetID = @DatasetID
		,Encounter.ContextCode
		,Encounter.DistrictNo
		--,EndDirectorateID = null --TBA
		--,EndDirectorateCode = null
		--,SpecialtyID = null
		--,Specialty = null
		--,ClinicianID = null
		--,ClinicianCode = null
		--,EndSiteID = null
		--,EndSiteCode = null
		,Stage.StageCode
		,BreachStatus.BreachStatusCode
		,Encounter.DepartmentTypeCode
		,Duration = Fact.StageDurationMinutes
		,UnplannedReattend7Day = Fact.UnplannedReattend7DayCases
		,LeftWithoutBeingSeen = Fact.LeftWithoutBeingSeenCases
		,ArrivalMode.NationalArrivalModeCode
		,AttendanceDisposal.NationalAttendanceDisposalCode
		,EncounterDate = Calendar.TheDate
		,EncounterDateID = Fact.StageStartDateID
		
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID
		,StartSiteID = Site.SourceSiteID
		,EndSiteID = Site.SourceSiteID
		,ClinicianID = StaffMember.SourceStaffMemberID
		,ServicePointID = ServicePoint.SourceServicePointID	
		,StartServicePointID = ServicePoint.SourceServicePointID				
	from
		WarehouseOLAPMergedV2.AE.FactEncounter Fact

	inner join WarehouseOLAPMergedV2.AE.BaseEncounter Encounter 
	on Encounter.MergeEncounterRecno = Fact.MergeEncounterRecno
	 
	inner join WarehouseOLAPMergedV2.AE.Stage 
	on Stage.StageID = Fact.StageID
	 
	inner join WarehouseOLAPMergedV2.WH.Calendar 
	on Calendar.DateID = Fact.StageStartDateID
	 
	inner join WarehouseOLAPMergedV2.AE.BreachStatus 
	on BreachStatus.BreachStatusID = Fact.StageBreachStatusID
	 
	inner join WarehouseOLAPMergedV2.AE.ArrivalMode 
	on ArrivalMode.SourceArrivalModeID = Fact.ArrivalModeID

	inner join WarehouseOLAPMergedV2.AE.AttendanceDisposal 
	on AttendanceDisposal.SourceAttendanceDisposalID = Fact.AttendanceDisposalID
	 
	inner join WarehouseOLAPMergedV2.AE.DepartmentType 
	on DepartmentType.DepartmentTypeID = Fact.DepartmentTypeID
	
	-- joins below changed to left for performance reasons
	
	left outer join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	left outer join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = Encounter.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	left outer join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = Encounter.ContextCode
	and Site.SourceSiteCode = '-1'
	
	left outer join WarehouseOLAPMergedV2.AE.StaffMember
	on	StaffMember.SourceContextCode = Encounter.ContextCode
	and coalesce(Encounter.StaffMemberCode, '-1') =  StaffMember.SourceStaffMemberCode
	
	left outer join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
	
	where
		Stage.StageCode in 
							(
							'ARRIVALTOTRIAGEADJUSTED'
							,'SEENFORTREATMENTADJUSTED'
							,'INDEPARTMENTADJUSTED'
							)
	
	--and	datediff(day, Encounter.ArrivalDate, getdate()) <= 200
	and	Encounter.ArrivalDate >= @AESLocalStart
	

select
	@inserted = @@rowcount
	
end


-- Casenote Numbers
if @CASELocalDatasetCode = 'CASE'

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,SourcePatientNo
		,CasenoteNumber
		,AllocatedDate
		,AllocatedDateID
		,WithdrawnDate
		,NewRegistration
		,ExistingRegistration
		,StartDirectorateID 
		,EndDirectorateID 		
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID
		,ClinicianID 
		,ServicePointID 
		,StartServicePointID
	)

select
		DatasetRecno = Base.MergeRecno
		,DatasetID = @DatasetID
		,Base.ContextCode
		,Base.SourcePatientNo
		,Base.CasenoteNumber
		,Base.AllocatedDate
		,AllocatedDateID = AllocateDate.DateID
		,Base.WithdrawnDate
		,NewRegistration = 
							case
							when not exists -- no previously allocated Casenote Number
										(
										select
											1
										from
											WarehouseOLAPMergedV2.Casenote.Base Earlier
										where
											Earlier.SourcePatientNo = Base.SourcePatientNo
										and (
											Earlier.AllocatedDate < Base.AllocatedDate
											or (
													Earlier.AllocatedDate = Base.AllocatedDate 
												and	Earlier.MergeRecno < Base.MergeRecno
												)
											)
										)
							then 1
							end
		,ExistingRegistration = 
							case
							when exists --at least one previously allocated Casenote Number
										(
										select
											1
										from
											WarehouseOLAPMergedV2.Casenote.Base Earlier
										where
											Earlier.SourcePatientNo = Base.SourcePatientNo
										and (
											Earlier.AllocatedDate < Base.AllocatedDate
											or (
													Earlier.AllocatedDate = Base.AllocatedDate 
												and	Earlier.MergeRecno < Base.MergeRecno
												)
											)
										)
							then 1
							end
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID
		,StartSiteID = Site.SourceSiteID
		,EndSiteID = Site.SourceSiteID
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID	
		,StartServicePointID = ServicePoint.SourceServicePointID	
	from
		WarehouseOLAPMergedV2.Casenote.Base
		
	inner join WarehouseOLAPMergedV2.WH.Calendar AllocateDate 
	on	AllocateDate.TheDate = Base.AllocatedDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = Base.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = Base.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = Base.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
	
	where
		Base.AllocatedDate >= @CASELocalStart

select
	@inserted = @@rowcount
	
end
	
/*	Waiting List Entry	*/

if @IPWLELocalDatasetCode = 'IPWLE'

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,SourcePatientNo
		,CasenoteNumber
		,EpisodeNo
		,ActivityDate
		,ActivityDateID
		,StartDirectorateID 
		,EndDirectorateID 		
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID
		,ClinicianID 
		,ServicePointID 
		,StartServicePointID
	)

	select
		DatasetRecno = W.WaitingListEntryRecno
		,DatasetID = @DatasetID
		,W.ContextCode
		,W.SourcePatientNo
		,W.CasenoteNumber
		,EpisodeNo = W.SourceEntityRecno
		,W.ActivityDate
		,ActivityDate.DateID	
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID
		,StartSiteID = Site.SourceSiteID
		,EndSiteID = Site.SourceSiteID
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID	
		,StartServicePointID = ServicePoint.SourceServicePointID	
	from
		WarehouseOLAPMergedV2.APCWL.BaseWaitingListEntry W
		
	inner join WarehouseOLAPMergedV2.WH.Calendar ActivityDate 
	on ActivityDate.TheDate = W.ActivityDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = W.ContextCode
	and Specialty.SourceSpecialtyCode = coalesce(W.SpecialtyCode, '-1')
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = W.ContextCode
	and Site.SourceSiteCode = coalesce(W.SiteCode, '-1')
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = W.ContextCode
	and Consultant.SourceConsultantCode = coalesce(W.ConsultantCode, '-1')
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
	
	where
		--datediff(year, W.ActivityDate, getdate()) <= 2
		W.ActivityDate >= @IPWLELocalStart
		
select
	@inserted = @@rowcount

end

/*	AE	*/

if @AELocalDatasetCode = 'AE'

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,SourcePatientNo
		--,EndDirectorateID
		--,EndDirectorateCode
		--,SpecialtyID
		--,Specialty
		--,ClinicianID
		--,ClinicianCode
		--,EndSiteID
		,ArrivalDateID
		,EndSiteCode
		,ArrivalTime
		,DepartureTime
		,InitialAssessmentTime
		,EncounterDateID
		,ArrivalModeCode
		,AttendanceCategoryCode
		,AttendanceDisposalCode	
		,StartDirectorateID 
		,EndDirectorateID 			
		,SpecialtyID 
		,StartSiteID
		,EndSiteID 
		,ClinicianID 
		,ServicePointID 
		,StartServicePointID
	)
	
	select
		DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,Encounter.ContextCode
		,DistrictNo
		--,EndDirectorateID = null --TBA
		--,EndDirectorateCode = null
		--,SpecialtyID = null
		--,Specialty = null
		--,ClinicianID = null
		--,ClinicianCode = null
		--,EndSiteID = Reference.SiteID
		,ArrivalDateID = Reference.EncounterEndDateID
		,Site.NationalSiteCode
		,Encounter.ArrivalTime
		,Encounter.DepartureTime
		,Encounter.InitialAssessmentTime
		,EncounterDateID = Reference.EncounterEndDateID
		,ArrivalMode.NationalArrivalModeCode
		,AttendanceCategory.NationalAttendanceCategoryCode
		,AttendanceDisposal.NationalAttendanceDisposalCode
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID
		,StartSiteID = Reference.SiteID
		,EndSiteID = Reference.SiteID
		,ClinicianID = Reference.StaffMemberID
		,ServicePointID = ServicePoint.SourceServicePointID	
		,StartServicePointID = ServicePoint.SourceServicePointID	
			  
	from
		WarehouseOLAPMergedV2.AE.BaseEncounter Encounter
	
	inner join WarehouseOLAPMergedV2.AE.BaseEncounterReference Reference 
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno
	
	left join WarehouseOLAPMergedV2.WH.Site 
	on Site.SourceSiteID = Reference.SiteID

	left join WarehouseOLAPMergedV2.AE.ArrivalMode 
	on ArrivalMode.SourceArrivalModeID = Reference.ArrivalModeID

	left join WarehouseOLAPMergedV2.AE.AttendanceCategory 
	on AttendanceCategory.SourceAttendanceCategoryID = Reference.AttendanceCategoryID

	left join WarehouseOLAPMergedV2.AE.AttendanceDisposal 
	on AttendanceDisposal.SourceAttendanceDisposalID = Reference.AttendanceDisposalID
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = Encounter.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	where
		--datediff(year, Encounter.ArrivalDate, getdate()) <= 2
		Encounter.ArrivalDate >= @AELocalStart
		
select
	@inserted = @@rowcount
	
end
	
/* APC */

if @APCLocalDatasetCode = 'APC'

begin

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,SourcePatientNo
		,DateOfBirth
		,ProviderSpellNo
		,SourceEncounterNo
		,StartDirectorateCode
		,EndDirectorateCode
		,SpecialtyCode
		,Specialty
		,NationalSpecialtyCode
		,ClinicianCode
		,StartSiteCode
		,EndSiteCode
		,StartWardCode
		,EndWardCode
		
		,AdmissionDateID
		,AdmissionDate
		,AdmissionTime
		,DischargeDateID
		,DischargeDate
		,DischargeTime
		,EpisodeStartDateID
		,EpisodeStartDate
		,EpisodeEndDateID
		,EpisodeEndDate
		
		,FirstEpisodeInSpellIndicator
		,NationalLastEpisodeInSpellCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,SecondaryDiagnosisCode13
		,PrimaryProcedureCode
		,PrimaryProcedureDate
		,ClinicalCodingCompleteDate
		,ClinicalCodingCompleteDateID
		
		,AdmissionMethodCode
		,AdmissionTypeCode
		,NationalAdmissionSourceCode
		,NationalDischargeMethodCode
		,NationalDischargeDestinationCode
		,NationalPatientClassificationCode
		,NationalIntendedManagementCode
		
		,EddCreatedTime
		,ExpectedDateOfDischarge
		,DischargeSummaryRequired
		,DischargeSummarySignedTime
		,PatientCategoryCode
		,HarmFreeCare
		
		,Division
		,MortalityReviewStatusCode
		,DominantForDiagnosis
		,Died
		,RiskScore
		,HRGCode
		,ComplicationComorbidity
		,TheatrePrimaryProcedureExists
		,PatientClassificationCode
		,PalliativeCare
		,CasenoteNumber
		
		,EpisodeNo
		,EncounterDateID
		,CharlsonDiagnosisCodeCount
		,StartDirectorateID 
		,EndDirectorateID 			
		,SpecialtyID 
		,StartSiteID
		,EndSiteID 
		,ClinicianID 
		,ServicePointID 
		,StartServicePointID -- Need to sort naming conventions out here
		,HasAssessmentUnitEpisode
		,IsWellBabySpell
		,CharlsonIndex
		,VTE
		,VTECompleteAndOrExclusion
		,FirstOperationInSpellTime
		,Readmission28Day
		,Readmission28DayDenominator
	) 
	select --top 10 
		DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,Encounter.ContextCode
		,Encounter.SourcePatientNo
		,Encounter.DateOfBirth
		,Encounter.GlobalProviderSpellNo -- 20140808 This goes in ProviderSpellNo field, may need to rename to Global, but need to understand the impact, ie how many views reference Provider spell no and encounter no.
		,Encounter.GlobalEpisodeNo	-- 20140808 This goes in SourceEncounterNo field, may need to rename to Global, but need to understand the impact, ie how many views reference Provider spell no and encounter no.
		,Encounter.StartDirectorateCode -- DG 21.10.14 - Should be using the value stamped on each particular consultant episode
		,Encounter.EndDirectorateCode --  DG 21.10.14 - Should be using the value stamped on each particular consultant episode

		,Encounter.SpecialtyCode
		,Specialty = Specialty.SourceSpecialty
		,NationalSpecialtyCode = Specialty.NationalSpecialtyCode
		,Encounter.ConsultantCode
		,Encounter.StartSiteCode
		,Encounter.EndSiteCode
		,Encounter.StartWardTypeCode
		,Encounter.EndWardTypeCode
		
		,Encounter.AdmissionDateID
		,Encounter.AdmissionDate
		,Encounter.AdmissionTime
		,Encounter.DischargeDateID
		,Encounter.DischargeDate
		,Encounter.DischargeTime
		,Encounter.EpisodeStartDateID
		,Encounter.EpisodeStartDate
		,Encounter.EpisodeEndDateID
		,Encounter.EpisodeEndDate
		
		,Encounter.FirstEpisodeInSpellIndicator
		,NationalLastEpisodeInSpellCode = NationalLastEpisodeInSpellIndicator			
		,Encounter.PrimaryDiagnosisCode
		,Encounter.SubsidiaryDiagnosisCode
		,Encounter.SecondaryDiagnosisCode1
		,Encounter.SecondaryDiagnosisCode2
		,Encounter.SecondaryDiagnosisCode3
		,Encounter.SecondaryDiagnosisCode4
		,Encounter.SecondaryDiagnosisCode5
		,Encounter.SecondaryDiagnosisCode6
		,Encounter.SecondaryDiagnosisCode7
		,Encounter.SecondaryDiagnosisCode8
		,Encounter.SecondaryDiagnosisCode9
		,Encounter.SecondaryDiagnosisCode10
		,Encounter.SecondaryDiagnosisCode11
		,Encounter.SecondaryDiagnosisCode12
		,Encounter.SecondaryDiagnosisCode13
		,Encounter.PrimaryProcedureCode
		,Encounter.PrimaryProcedureDate
		,Encounter.ClinicalCodingCompleteDate 
		,ClinicalCodingCompleteDateID = CalendarClinicalCodingComplete.DateID
		
		,AdmissionMethodCode = AdmissionMethod.NationalAdmissionMethodCode
		,AdmissionTypeCode = AdmissionMethod.AdmissionTypeCode
		,NationalAdmissionSourceCode = AdmissionSource.NationalAdmissionSourceCode
		,NationalDischargeMethodCode = DischargeMethod.NationalDischargeMethodCode
		,NationalDischargeDestinationCode
		,NationalPatientClassificationCode = PatientClassification.NationalPatientClassificationCode
		,NationalIntendedManagementCode = IntendedManagement.NationalIntendedManagementCode
			
		,Encounter.EddCreatedTime
		,Encounter.ExpectedDateofDischarge
		,DischargeSummaryRequired =
					case
					when DischargeLetterExclusion.AllocationID = 
						(
						select
							Allocation.AllocationID
						from
							WarehouseOLAPMergedV2.Allocation.Allocation
						where
							Allocation.SourceAllocationID = '-1'
						and	Allocation.AllocationTypeID = 14
						)
					then 1
					else 0
					end
		,DischargeSummarySignedTime = BaseDocument.SignedTime 
		,Encounter.PatientCategoryCode		
		,HarmFreeCare =
			case
			when not exists
						(
						select
							1
						from
							(
							-- need to work on conditions of each harm type?
							select
								MergeEncounterRecno
							from
								WarehouseOLAPMergedV2.APC.BaseEncounterBaseFall
							union
							select
								MergeEncounterRecno
							from
								WarehouseOLAPMergedV2.APC.BaseEncounterBasePressureUlcer
							union
							select
								MergeEncounterRecno
							from
								WarehouseOLAPMergedV2.APC.BaseEncounterBaseUrinaryTractInfection
							union
							select
								MergeEncounterRecno
							from
								WarehouseOLAPMergedV2.APC.BaseEncounterBaseVTECondition
							) HarmFreeCare
						where
							Encounter.MergeEncounterRecno = HarmFreeCare.MergeEncounterRecno
						)
			then 1
			end	
		
		,Division = StartDirectorate.Division
		,MortalityReviewStatusCode = ReviewStatus.LocalReviewStatusCode--20160114 RR discussed with GS, changed from SourceReviewStatusCode to Local
		,DominantForDiagnosis = BaseSHMI.DominantForDiagnosis
		,Died = BaseSHMI.Died
		,RiskScore = BaseSHMI.RiskScore
		,HRGCode = HRG4Spell.HRGCode
		,ComplicationComorbidity = HRG.ComplicationComorbidity -- HRG has complications and / or co-morbidities
		,TheatrePrimaryProcedureExists = -- Theatre system primary procedure code exists
			cast(
				case
				when exists 
						(
						select
							1					
						from
							WarehouseOLAPMergedV2.APC.BaseEncounterProcedureDetail EncounterProc 		

						inner join WarehouseOLAPMergedV2.Theatre.BaseProcedureDetail TheatreProc 
						on	EncounterProc.ProcedureDetailSourceUniqueID = TheatreProc.SourceUniqueID
						and TheatreProc.ContextCode = EncounterProc.ProcedureDetailContextCode
						
						where
							Encounter.EncounterRecno = EncounterProc.EncounterRecno
						and Encounter.ContextCode = EncounterProc.EncounterContextCode
						and TheatreProc.PrimaryProcedureFlag = '1'				
						) 
				then 1
				else 0
				end
			as bit)
		,Encounter.PatientClassificationCode
		,PalliativeCare = 
						case
						when
							exists
							(
							select
								1
							from
								WarehouseOLAPMergedV2.APC.BaseDiagnosis
							where
								BaseDiagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
							and BaseDiagnosis.DiagnosisCode = 'Z51.5'											
							)
						then 1
						else 0
						end
		,Encounter.CasenoteNumber
		
		,EpisodeNo = Encounter.SourceSpellNo
		,EncounterDateID = Encounter.EpisodeStartDateID
		,CharlsonDiagnosisCodeCount = Charlson.CharlsonDiagnosisCodeCount
		,StartDirectorateID = StartDirectorate.DirectorateID
		,EndDirectorateID = EndDirectorate.DirectorateID					
		,SpecialtyID = Encounter.SpecialtyID
		,StartSiteID = Encounter.StartSiteID
		,EndSiteID = Encounter.EndSiteID
		,ClinicianID = Encounter.ConsultantID
		,ServicePointID = Encounter.EndWardID	
		,StartServicePointID = Encounter.StartWardID

		,HasAssessmentUnitEpisode = 
			case
			when exists
				(
				select
					1
				from
					WarehouseOLAPMergedV2.APC.BaseWardStay

				inner join WarehouseOLAPMergedV2.APC.Encounter EncounterWardStay
				on	EncounterWardStay.ProviderSpellNo = BaseWardStay.ProviderSpellNo
				and	EncounterWardStay.ContextCode = BaseWardStay.ContextCode
				and EncounterWardStay.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo

				inner join (
					select
						AssessmentWardCode = item 
					from
						WarehouseOLAPMergedV2.Utility.fn_ListToTable('DASS,ESTU,PMAU,MAU,MAU2,AMU,OMU,POAU,SAL,SAU',',')
					) AssessmentWard
				on	AssessmentWard.AssessmentWardCode = BaseWardStay.WardCode
				)
			then 1
			else 0
			end
		,IsWellBabySpell =
			case
			when
				Specialty.NationalSpecialtyCode = '424'
			and	(
				select
					count(distinct Specialty.NationalSpecialtyCode)
				from
					WarehouseOLAPMergedV2.APC.BaseEncounter

				inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference Reference
				on	BaseEncounter.MergeEncounterRecno = Reference.MergeEncounterRecno
				and BaseEncounter.Reportable = 1

				inner join WarehouseOLAPMergedV2.WH.Specialty
				on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

				where
					BaseEncounter.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
				) = 1
			then 1
			else 0
			end
		,Encounter.CharlsonIndex
		,VTE =
			case
			when
				(
					(
						Encounter.AgeCode like '%Years'
					and	left(Encounter.AgeCode , 2) > '17'
					)
				or	Encounter.AgeCode = '99+'
				)
			and not --regular day or night
				(
					AdmissionMethod.AdmissionTypeCode = 'EL' --elective
				and	PatientClassification.NationalPatientClassificationCode in ( '3' , '4' )
				)

			then 1
			else 0
			end
		,VTECompleteAndOrExclusion =
			case
			when
				Encounter.VTECategoryCode = 'C'
			or	coalesce(
					VTEExclusion.AllocationID
					,
					(
					select
						Allocation.AllocationID
					from
						WarehouseOLAPMergedV2.Allocation.Allocation
					where
						Allocation.SourceAllocationID = '-1'
					and	Allocation.AllocationTypeID = 3
					)
				) != 
					(
					select
						Allocation.AllocationID
					from
						WarehouseOLAPMergedV2.Allocation.Allocation
					where
						Allocation.SourceAllocationID = '-1'
					and	Allocation.AllocationTypeID = 3
					)			
			then 1
			else 0
			end
		,FirstOperationInSpellTime = FirstOperationInSpell.OperationDate
		,Readmission28Day = 
			case
			when FactReadmission.ReadmissionEncounterMergeEncounterRecno is not null
			then 1
			else 0
			end
		,Readmission28DayDenominator = 
			case
			when FactReadmissionDenominator.SpellEncounterMergeEncounterRecno is not null
			then 1
			else 0
			end
	from
		WarehouseOLAPMergedV2.APC.Encounter

	left join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
	on Encounter.StartDirectorateCode = StartDirectorate.DirectorateCode

	left join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
	on Encounter.EndDirectorateCode = EndDirectorate.DirectorateCode

	left join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID

	left join WarehouseOLAPMergedV2.APC.AdmissionMethod
	on	AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID

	left join WarehouseOLAPMergedV2.APC.AdmissionSource
	on	AdmissionSource.SourceAdmissionSourceID = Encounter.AdmissionSourceID

	left join WarehouseOLAPMergedV2.APC.DischargeMethod
	on	DischargeMethod.SourceDischargeMethodID = Encounter.DischargeMethodID

	left join WarehouseOLAPMergedV2.APC.DischargeDestination
	on	DischargeDestination.SourceDischargeDestinationID = Encounter.DischargeDestinationID

	left join WarehouseOLAPMergedV2.APC.IntendedManagement
	on	IntendedManagement.SourceIntendedManagementID = Encounter.IntendedManagementID

	left join WarehouseOLAPMergedV2.APC.PatientClassification
	on	PatientClassification.SourcePatientClassificationID = Encounter.PatientClassificationID

	left join WarehouseOLAPMergedV2.WH.Calendar CalendarClinicalCodingComplete
	on CalendarClinicalCodingComplete.TheDate = Encounter.ClinicalCodingCompleteDate

	left join WarehouseOLAPMergedV2.APC.BaseSHMI
	on Encounter.MergeEncounterRecno = BaseSHMI.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.APC.HRG4Spell 
	on HRG4Spell.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.WH.HRG 
	on HRG.HRGCode = HRG4Spell.HRGCode

	left join
		(
		select
			MergeEncounterRecno
			,CharlsonDiagnosisCodeCount = count(1)
		from
			WarehouseOLAPMergedV2.APC.BaseDiagnosis
			
		inner join WarehouseOLAPMergedV2.WH.Diagnosis
		on	BaseDiagnosis.DiagnosisCode = Diagnosis.DiagnosisCode
		and	IsCharlson = 1

		group by
			MergeEncounterRecno	

		) Charlson
	on	Charlson.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.APC.FactMortalityEncounter
	on FactMortalityEncounter.MergeEncounterRecno = Encounter.MergeEncounterRecno
	
	left join WarehouseOLAPMergedV2.Mortality.ReviewStatus
	on	ReviewStatus.SourceReviewStatusID = FactMortalityEncounter.MortalityReviewStatusID

	left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation VTEExclusion
	on	VTEExclusion.AllocationTypeID = 3
	and	VTEExclusion.DatasetCode = 'APC'
	and	VTEExclusion.DatasetRecno= Encounter.MergeEncounterRecno

	left outer join
		(
		select
			GlobalProviderSpellNo
			,OperationDate = min(OperationDate)
		from
			WarehouseOLAPMergedV2.APC.Operation

		inner join WarehouseOLAPMergedV2.APC.Encounter
		on Operation.MergeEncounterRecno = Encounter.MergeEncounterRecno

		group by
			GlobalProviderSpellNo

		) FirstOperationInSpell
	on	Encounter.GlobalProviderSpellNo = FirstOperationInSpell.GlobalProviderSpellNo

	left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation DischargeLetterExclusion
	on	DischargeLetterExclusion.AllocationTypeID = 14
	and	DischargeLetterExclusion.DatasetCode = 'APC'
	and	DischargeLetterExclusion.DatasetRecno= Encounter.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.APC.BaseEncounterBaseDocument 
	on	BaseEncounterBaseDocument.MergeEncounterRecno = Encounter.MergeEncounterRecno
	and	BaseEncounterBaseDocument.SequenceNo = 1

	left join	WarehouseOLAPMergedV2.Dictation.BaseDocument
	on BaseEncounterBaseDocument.MergeDocumentRecno = BaseDocument.MergeDocumentRecno

	left join WarehouseOLAPMergedV2.APC.FactReadmission
	on	FactReadmission.InitialAdmissionEncounterMergeEncounterRecno = Encounter.MergeEncounterRecno
	and	FactReadmission.ReadmissionTypeID = 2 --CQUIN - 28 day readmission
	and	FactReadmission.TrustWide = 1

	left join WarehouseOLAPMergedV2.APC.FactReadmissionDenominator
	on	FactReadmissionDenominator.SpellEncounterMergeEncounterRecno = Encounter.MergeEncounterRecno
	and	FactReadmissionDenominator.ReadmissionTypeID = 2 --CQUIN - 28 day readmission

	where
		Encounter.EpisodeStartTime >= @APCLocalStart
		
select
	@inserted = @@rowcount
	
end


if @OPLocalDatasetCode = 'OP'

begin

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,SourcePatientNo
		,DateOfBirth
		,SourceEncounterNo
		--,StartDirectorateID
		,StartDirectorateCode
		--,EndDirectorateID
		,EndDirectorateCode
		--,SpecialtyID
		,SpecialtyCode
		,Specialty
		,NationalSpecialtyCode
		--,ClinicianID
		,ClinicianCode
		--,StartSiteID
		,StartSiteCode
		--,EndSiteID
		,EndSiteCode
		,PrimaryDiagnosisCode
		,PrimaryProcedureCode
		,PrimaryProcedureDate
		,AppointmentDateID
		,AppointmentDate
		,NationalAttendanceStatusCode
		,NationalFirstAttendanceCode
		,AppointmentOutcomeCode
		,EncounterDateID
		--,ServicePointID
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
		
	) 
	select
		DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,Encounter.ContextCode
		,SourcePatientNo
		,DateOfBirth
		,SourceEncounterNo
		--,StartDirectorateID = Directorate.DirectorateID
		,Encounter.DirectorateCode
		--,EndDirectorateID = Directorate.DirectorateID
		,Encounter.DirectorateCode
		--,SpecialtyID
		,SpecialtyCode
		,Specialty = Specialty.SourceSpecialty
		,NationalSpecialtyCode = Specialty.NationalSpecialtyCode
		--,ConsultantID
		,ConsultantCode
		--,SiteID
		,SiteCode
		--,SiteID
		,SiteCode
		,PrimaryDiagnosisCode
		,PrimaryOperationCode
		,PrimaryProcedureDate
		,AppointmentDateID
		,AppointmentDate
		,NationalAttendanceStatusCode
		,NationalFirstAttendanceCode
		,AppointmentOutcomeCode
		,EncounterDateID = AppointmentDateID
		--,Reference.ClinicID
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Reference.ReferralSpecialtyID
		,StartSiteID = Reference.SiteID
		,EndSiteID = Reference.SiteID
		,ClinicianID = Reference.ConsultantID
		,ServicePointID = Reference.ClinicID	
		,StartServicePointID = Reference.ClinicID	
	from
		WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

	inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.WH.Directorate
	on Encounter.DirectorateCode = Directorate.DirectorateCode

	left join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceSpecialtyID = Reference.ReferralSpecialtyID

	left join WarehouseOLAPMergedV2.OP.FirstAttendance
	on	FirstAttendance.SourceFirstAttendanceID = Reference.DerivedFirstAttendanceID

	left join WarehouseOLAPMergedV2.OP.AttendanceStatus
	on	AttendanceStatus.SourceAttendanceStatusID = Reference.AttendanceStatusID

	where
		--Encounter.AppointmentDate between DATEADD(year, -2, getdate()) and getdate()
		Encounter.AppointmentDate >= @OPLocalStart

select
	@inserted = @@rowcount

end

/*	Ward Stay, joined to Episode, using Episode within which Ward Stay starts 
	(and taking the latest possible Episode if the Ward Stay start falls in more than one) */

if @WSLocalDatasetCode = 'WS'

begin

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,DateOfBirth
		--,StartDirectorateID
		,StartDirectorateCode
		--,EndDirectorateID
		,EndDirectorateCode
		--,SpecialtyID
		,SpecialtyCode
		,Specialty
		,NationalSpecialtyCode
		--,ClinicianID
		,ClinicianCode
		--,StartSiteID
		,StartSiteCode
		--,EndSiteID
		,EndSiteCode
		,StartWardCode
		,EndWardCode
		,WardStayStartDate
		,WardStayStartDateID
		,WardStayEndActivityCode
		,AdmissionTime
		--,ServicePointID
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	) 
	select
		 DatasetRecno = WardStay.MergeEncounterRecno
		,DatasetID = @DatasetID
		,WardStay.ContextCode
		,Episode.DateOfBirth
		--,StartDirectorateID = StartDirectorate.DirectorateID
		,StartDirectorateCode
		--,EndDirectorateID = EndDirectorate.DirectorateID
		,EndDirectorateCode
		--,EpisodeReference.SpecialtyID
		,Episode.SpecialtyCode
		,Specialty = Specialty.SourceSpecialty
		,NationalSpecialtyCode = Specialty.NationalSpecialtyCode
		--,EpisodeReference.ConsultantID
		,Episode.ConsultantCode
		--,StartSiteID = WardStayReference.SiteID
		,WardStay.SiteCode
		--,EndSiteID = WardStayReference.SiteID
		,WardStay.SiteCode
		,WardStay.WardCode
		,WardStay.WardCode
		,WardStay.StartDate
		,CalendarWardStayStart.DateID
		,WardStay.EndActivityCode
		,Episode.AdmissionTime
		--,WardStayReference.WardID	
		,StartDirectorateID = StartDirectorate.DirectorateID
		,EndDirectorateID = EndDirectorate.DirectorateID	
		,SpecialtyID = EpisodeReference.SpecialtyID
		,StartSiteID = WardStayReference.SiteID
		,EndSiteID = WardStayReference.SiteID
		,ClinicianID = EpisodeReference.ConsultantID
		,ServicePointID = WardStayReference.WardID
		,StartServicePointID = WardStayReference.WardID
	from
		WarehouseOLAPMergedV2.APC.BaseWardStay WardStay

	-- Join to Episode
	inner join WarehouseOLAPMergedV2.APC.BaseEncounter Episode
	on	Episode.ProviderSpellNo = WardStay.ProviderSpellNo -- in same Spell
	and WardStay.StartDate between Episode.EpisodeStartDate and coalesce(Episode.EpisodeEndDate, getdate()) -- within which the Ward Stay start falls
	and not exists -- and there exists no later episode which also qualifies
				(
					select
						1 
					from
						WarehouseOLAPMergedV2.APC.BaseEncounter LaterEpisode
					where
						LaterEpisode.ProviderSpellNo = WardStay.ProviderSpellNo -- in same Spell
					and WardStay.StartDate between LaterEpisode.EpisodeStartDate and coalesce(LaterEpisode.EpisodeEndDate, getdate()) -- within which the Ward Stay start falls
					and 
					(
							LaterEpisode.EpisodeStartDate > Episode.EpisodeStartDate -- this one starts late
						or
							coalesce(LaterEpisode.EpisodeEndDate, getdate()) > coalesce(Episode.EpisodeEndDate, getdate()) -- or ends later
						or -- (tie-breaker) starts and ends at the same time and has a later Recno
							(
								LaterEpisode.EpisodeStartDate = Episode.EpisodeStartDate
								and	coalesce(LaterEpisode.EpisodeEndDate, getdate()) = coalesce(Episode.EpisodeEndDate, getdate())
								and LaterEpisode.MergeEncounterRecno > Episode.MergeEncounterRecno
							)
					)
				)
	
	inner join WarehouseOLAPMergedV2.APC.BaseWardStayReference WardStayReference
	on	WardStayReference.MergeEncounterRecno = WardStay.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference EpisodeReference
	on	EpisodeReference.MergeEncounterRecno = Episode.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
	on Episode.StartDirectorateCode = StartDirectorate.DirectorateCode

	left join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
	on Episode.EndDirectorateCode = EndDirectorate.DirectorateCode

	left join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceSpecialtyID = EpisodeReference.SpecialtyID

	left join WarehouseOLAPMergedV2.WH.Calendar CalendarWardStayStart
	on CalendarWardStayStart.TheDate = WardStay.StartDate
	
	where
		--WardStay.StartTime > DATEADD(year, -2, getdate())
		WardStay.StartTime >= @WSLocalStart
		
select
	@inserted = @@rowcount
	
end
	
/* COM */

if @COMLocalDatasetCode = 'COM'

begin

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,DateOfBirth
		,SourceEncounterNo
		--,StartDirectorateID
		,StartDirectorateCode
		--,EndDirectorateID
		,EndDirectorateCode
		--,SpecialtyID
		,SpecialtyCode
		--,ClinicianID
		,ClinicianCode
		--,StartSiteID
		--,EndSiteID
		,ContactDate
		,ContactDateID
		,NHSNumber
		,Postcode
		,RegisteredGPPracticeCode
		,CommissionerCode
		,SexCode
		,StaffTeamCode
		,ConsultationMediumTypeGroup
		,ConsultationMediumTypeGroupChannel
		,EncounterOutcomeCode
		,WaitDays
		,ReferralID
		,PrimaryVisit
		,ReferralSourceCode
		,EncounterDateID
		--,ServicePointID
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	) 

	select
		DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,Encounter.ContextCode
		,PatientDateOfBirth
		,Encounter.MergeEncounterRecno
		--,StartDirectorateID = Directorate.DirectorateID
		,StartDirectorateCode = Directorate.DirectorateCode
		--,EndDirectorateID = Directorate.DirectorateID
		,EndDirectorateCode = Directorate.DirectorateCode
		--,Reference.SpecialtyID
		,Specialty.SpecialtyCode
		--,Reference.ProfessionalCarerID
		,ProfessionalCarer.ProfessionalCarerCode
		--,StartSiteID = Reference.LocationID
		--,EndSiteID = Reference.LocationID
		,Encounter.StartDate
		,Reference.StartDateID
		,Encounter.PatientNHSNumber
		,Encounter.PatientPostcode
		,Encounter.PatientCurrentRegisteredPracticeCode
		,Encounter.CommissionerCode
		,Encounter.PatientSexID
		,Encounter.StaffTeamID
		,ConsultationMedium.ConsultationMediumTypeGroup
		,ConsultationMedium.ConsultationMediumTypeGroupChannel
		,Encounter.EncounterOutcomeCode
		,Encounter.WaitDays
		,Encounter.ReferralID
		,PrimaryVisit = 
						case 
						when PrimaryVisit.EncounterSourceUniqueID is null 
						then 0 
						else 1 
						end
		,ReferralSourceCode = Encounter.ReferralSourceID
		,EncounterDateID = Reference.StartDateID
		--,Reference.LocationID
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID
		,SpecialtyID = Reference.SpecialtyID
		,StartSiteID = Reference.LocationID
		,EndSiteID = Reference.LocationID
		,ClinicianID = Reference.ProfessionalCarerID
		,ServicePointID = Reference.LocationID
		,StartServicePointID = Reference.LocationID
			
	from
		WarehouseOLAPMergedV2.COM.BaseEncounter Encounter

	inner join WarehouseOLAPMergedV2.COM.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join -- Join to DiagnosisProcedureEvent table to check if Encounter includes a Primary Visit
	(
	
		select distinct 
			EncounterSourceUniqueID	
		from
			WarehouseOLAPMergedV2.COM.BaseDiagnosisProcedureEvent E
			
		inner join WarehouseOLAPMergedV2.COM.DiagnosisProcedure D 
		on	D.DiagnosisProcedureID = E.DiagnosisProcedureID
		and D.DiagnosisProcedureCode = 'CC00119' -- Primary Visit
		
	) PrimaryVisit
	on PrimaryVisit.EncounterSourceUniqueID = Encounter.SourceUniqueID
	
	left outer join WarehouseOLAPMergedV2.COM.Specialty
	on	Specialty.SourceValueID = Reference.SpecialtyID

	left outer join WarehouseOLAPMergedV2.COM.ProfessionalCarer 
	on	ProfessionalCarer.ProfessionalCarerID = Encounter.EncounterProfessionalCarerID
	and	ProfessionalCarer.ContextCode = Encounter.ContextCode

	left outer join WarehouseOLAPMergedV2.COM.Location
	on	Location.SourceValueID = Reference.LocationID
	
	left outer join WarehouseOLAPMergedV2.COM.SpecialtyDirectorateMap
	on	SpecialtyDirectorateMap.SpecialtyID = Encounter.SpecialtyID
	
	left outer join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateID = coalesce(SpecialtyDirectorateMap.DirectorateID, 15) -- just until we can get establsih who is maintaining 5ntc.Services in TISData
	
	left outer join WarehouseOLAPMergedV2.COM.ConsultationMedium 
	on	ConsultationMedium.SourceValueID = Reference.ConsultationMediumID
	
	where
		--Encounter.StartDate between DATEADD(year, -2, getdate()) and GETDATE()
		Encounter.StartDate between @COMLocalStart and getdate()
		and Encounter.RecordStatus = 'C'
		and Encounter.JointActivityFlag = 0
		and Encounter.Archived = 'N'

select
	@inserted = @@rowcount
	
end
	
/* RF */

if @RFLocalDatasetCode = 'RF'

begin

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,SourceEncounterNo
		--,StartSiteID
		,StartSiteCode
		--,StartDirectorateID
		,StartDirectorateCode
		--,SpecialtyID
		,SpecialtyCode
		,Specialty
		,NationalSpecialtyCode
		--,ClinicianID
		,ClinicianCode
		,ReferralDateID
		,ReferralDate
		,DischargeDate
		,DischargeTime
		,NationalSourceOfReferralCode
		,CasenoteNumber
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	) 
	select
		 DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,Encounter.ContextCode
		,Encounter.SourceEncounterNo
		--,StartSiteID = Site.SourceSiteID
		,StartSiteCode = Site.SourceSiteCode
		--,StartDirectorateID = Directorate.DirectorateID
		,StartDirectorateCode = Directorate.DirectorateCode
		--,Reference.SpecialtyID
		,Encounter.SpecialtyCode
		,Specialty = Specialty.SourceSpecialty
		,NationalSpecialtyCode = Specialty.NationalSpecialtyCode
		--,Reference.ConsultantID
		,Encounter.ConsultantCode
		,Reference.ReferralDateID
		,Encounter.ReferralDate
		,Encounter.DischargeDate
		,Encounter.DischargeTime
		,SourceOfReferral.NationalSourceOfReferralCode
		,CasenoteNumber = Encounter.CasenoteNo
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID
		,SpecialtyID = Reference.SpecialtyID
		,StartSiteID = Reference.SiteID
		,EndSiteID = Reference.SiteID
		,ClinicianID = Reference.ConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID
	from
		WarehouseOLAPMergedV2.RF.BaseEncounter Encounter

	inner join WarehouseOLAPMergedV2.RF.BaseEncounterReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.WH.Site Site
	on Site.SourceSiteID = Reference.SiteID

	left join WarehouseOLAPMergedV2.WH.Directorate Directorate
	on coalesce(Encounter.DirectorateCode, '9') = Directorate.DirectorateCode

	left join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

	left join [WarehouseOLAPMergedV2].[OP].[SourceOfReferral]
	on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	where
		--Encounter.ReferralDate between DATEADD(year, -2, getdate()) and GETDATE()
		Encounter.ReferralDate between @RFLocalStart and getdate()

select
	@inserted = @@rowcount
	
end
	
/* COMR */

if @COMRLocalDatasetCode = 'COMR'

begin

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,DateOfBirth
		,SourceEncounterNo
		--,StartDirectorateID
		,StartDirectorateCode
		--,EndDirectorateID
		,EndDirectorateCode
		--,SpecialtyID
		,SpecialtyCode
		--,ClinicianID
		,ClinicianCode
		--,StartSiteID
		--,EndSiteID
		,ReferralDate
		,ReferralDateID
		,ReferralReasonCode
		,ReferralID
		,EncounterDateID
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	) 

	select
		DatasetRecno = Referral.MergeEncounterRecno
		,DatasetID = @DatasetID
		,Referral.ContextCode
		,PatientDateOfBirth
		,Referral.MergeEncounterRecno
		--,StartDirectorateID = Directorate.DirectorateID
		,StartDirectorateCode = Directorate.DirectorateCode
		--,EndDirectorateID = Directorate.DirectorateID
		,EndDirectorateCode = Directorate.DirectorateCode
		--,Reference.ReferredToSpecialtyID
		,Specialty.SpecialtyCode
		--,Reference.[ReferredToProfessionalCarerID]
		,ProfessionalCarer.ProfessionalCarerCode
		--,StartSiteID = Location.SourceValueID
		--,EndSiteID = Location.SourceValueID
		,Referral.ReferralReceivedDate
		,Reference.ReferralReceivedDateID
		,Referral.ReasonForReferralID
		,Referral.SourceUniqueID
		,EncounterDateID = Reference.ReferralReceivedDateID
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID
		,SpecialtyID = Reference.ReferredToSpecialtyID
		,StartSiteID = Location.SourceValueID
		,EndSiteID = Location.SourceValueID
		,ClinicianID = Reference.ReferredToProfessionalCarerID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID
	from
		WarehouseOLAPMergedV2.COM.BaseReferral Referral

	inner join WarehouseOLAPMergedV2.COM.BaseReferralReference Reference
	on	Reference.MergeEncounterRecno = Referral.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.COM.Specialty
	on	Specialty.SourceValueID = Reference.ReferredToSpecialtyID

	left join WarehouseOLAPMergedV2.COM.ProfessionalCarer 
	on	ProfessionalCarer.ProfessionalCarerID = Referral.ReferredToProfessionalCarerID
	and ProfessionalCarer.ContextCode = Referral.ContextCode

	left join WarehouseOLAPMergedV2.COM.Location
	on Location.LocationID = '-1'
	
	left join WarehouseOLAPMergedV2.COM.SpecialtyDirectorateMap
	on SpecialtyDirectorateMap.SpecialtyID = Referral.ReferredToSpecialtyID
	
	left join WarehouseOLAPMergedV2.WH.Directorate
	on Directorate.DirectorateID = coalesce(SpecialtyDirectorateMap.DirectorateID, 15) -- just until we can get establsih who is maintaining 5ntc.Services in TISData
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	where
		--Referral.ReferralReceivedDate between DATEADD(year, -2, getdate()) and GETDATE()
		Referral.ReferralReceivedDate between @COMRLocalStart and getdate()
		and Referral.RecordStatus = 'C'
		and Referral.ArchiveFlag = 'N'
		and Referral.ParentID is null

select
	@inserted = @@rowcount
	
end
	
/* CWL */


if @CWLLocalDatasetCode = 'CWL'

begin

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		--,StartDirectorateID
		,StartDirectorateCode
		--,EndDirectorateID
		,EndDirectorateCode
		--,SpecialtyID
		,SpecialtyCode
		--,ClinicianID
		,ClinicianCode
		--,StartSiteID
		--,EndSiteID
		,CensusDate
		,CensusDateID
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	) 

	select
		DatasetRecno = Encounter.MergeEncounterRecno
		,DatasetID = @DatasetID
		,Encounter.ContextCode
		--,StartDirectorateID = Directorate.DirectorateID
		,StartDirectorateCode = Directorate.DirectorateCode
		--,EndDirectorateID = Directorate.DirectorateID
		,EndDirectorateCode = Directorate.DirectorateCode
		--,Reference.SpecialtyID
		,Specialty.SpecialtyCode
		--,Reference.ProfessionalCarerID
		,ProfessionalCarer.ProfessionalCarerCode
		--,StartSiteID = Location.SourceValueID
		--,EndSiteID = Location.SourceValueID
		,Encounter.CensusDate
		,Reference.CensusDateID
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID
		,SpecialtyID = Reference.SpecialtyID
		,StartSiteID = Location.SourceValueID
		,EndSiteID = Location.SourceValueID
		,ClinicianID = Reference.ProfessionalCarerID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID
	from
		WarehouseOLAPMergedV2.COM.BaseWait Encounter

	inner join WarehouseOLAPMergedV2.COM.BaseWaitReference Reference
	on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.COM.Specialty
	on	Specialty.SourceValueID = Reference.SpecialtyID

	left join WarehouseOLAPMergedV2.COM.ProfessionalCarer 
	on	ProfessionalCarer.ProfessionalCarerID = Encounter.ReferredToProfessionalCarerID
	and	ProfessionalCarer.ContextCode = Encounter.ContextCode

	left join WarehouseOLAPMergedV2.COM.Location
	on Location.LocationID = '-1'
	
	left join WarehouseOLAPMergedV2.COM.SpecialtyDirectorateMap
	on SpecialtyDirectorateMap.SpecialtyID = Encounter.ReferredToSpecialtyID
	
	left join WarehouseOLAPMergedV2.WH.Directorate
	on Directorate.DirectorateID =  coalesce(SpecialtyDirectorateMap.DirectorateID, 15) -- just until we can get establsih who is maintaining 5ntc.Services in TISData
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	where
		--Encounter.CensusDate between DATEADD(year, -2, getdate()) and GETDATE()
		Encounter.CensusDate between @CWLLocalStart and getdate()
		and Encounter.ArchiveFlag = 'N'
		and Encounter.ParentID is null

select
	@inserted = @@rowcount
	
end
	
	
	
/* INC */

if @INCLocalDatasetCode = 'INC' 

begin

	-- Write out a record for each Incident involving at least one Patient

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,IncidentDate
		,IncidentDateID
		,IncidentGradeCode
		,IncidentCauseGroupCode
		,IncidentCause1Code
		,IncidentTypeCode
		,PatientSafety
		,IncidentInvolvingChild
		,IncidentInvolvingAdult
		,IncidentInvolvingElderly
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
		,SourceDepartmentCode

	) 

	select
		DatasetRecno = BaseIncident.MergeIncidentRecno
		,DatasetID = @DatasetID
		,BaseIncident.ContextCode
		,BaseIncident.IncidentDate
		,BaseIncidentReference.IncidentDateID
		,BaseIncident.IncidentGradeCode
		,BaseIncident.CauseGroupCode
		,BaseIncident.CauseCode1
		,BaseIncident.IncidentTypeCode
		,BaseIncident.PatientSafety
		,IncidentInvolvingChild = 
								case 
								when CMFT.Dates.GetAge(BaseIncident.DateOfBirth, BaseIncident.IncidentDate) < 18
								then 1 
								end
		,IncidentInvolvingAdult = 
								case 
								when CMFT.Dates.GetAge(BaseIncident.DateOfBirth, BaseIncident.IncidentDate) > 18
								then 1 
								end
		,IncidentInvolvingElderly = 
								case 
								when CMFT.Dates.GetAge(BaseIncident.DateOfBirth, BaseIncident.IncidentDate) >= 65								
								then 1 
								end
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = BaseIncidentReference.WardID
		,StartServicePointID = BaseIncidentReference.WardID
		,SourceDepartmentCode = BaseIncident.DepartmentCode
	from
		WarehouseOLAPMergedV2.Incident.BaseIncident

	inner join WarehouseOLAPMergedV2.Incident.BaseIncidentReference
	on	BaseIncidentReference.MergeIncidentRecno = BaseIncident.MergeIncidentRecno
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = coalesce(BaseIncident.DirectorateCode, '9')
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = BaseIncident.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = BaseIncident.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = BaseIncident.ContextCode
	and Consultant.SourceConsultantCode = '-1'

	where
		BaseIncident.IncidentDate between @INCLocalStart and getdate()
	and	BaseIncident.SequenceNo = 1 
	and BaseIncident.PatientSafety = 'S'
	and BaseIncident.SiteCode not in ('0x4A20','0x4B20') --Non CMFT & PCT incidents
	and BaseIncident.IncidentTypeCode not in ('0x5A20') --Excelenece Reporting to be excluded

			
select
	@inserted = @@rowcount
	
end
	
/* FALL */

if @FALLLocalDatasetCode = 'FALL'

begin

	insert into dbo.WrkDataset
	(
		DatasetRecno
		,DatasetID
		,FallDateID
		,SourceFallSeverityCode
		,Validated
		,StartDirectorateID
		,EndDirectorateID		
		,SpecialtyID
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID
	)	

	select
		BaseFall.MergeFallRecno
		,DatasetID = @DatasetID
		,FallDateID = BaseFallReference.FallDateID
		,FallSeverity.SourceFallSeverityCode
		,Validated
		,StartDirectorateID = StartDirectorate.DirectorateID
		,EndDirectorateID = EndDirectorate.DirectorateID					
		,SpecialtyID = BaseEncounterReference.SpecialtyID	
		,StartSiteID = BaseEncounterReference.StartSiteID	
		,EndSiteID = BaseEncounterReference.EndSiteID
		,ClinicianID = BaseEncounterReference.ConsultantID
		,ServicePointID = BaseFallReference.WardID

	from
		WarehouseOLAPMergedV2.APC.BaseFall

	inner join WarehouseOLAPMergedV2.APC.BaseFallReference
	on	BaseFallReference.MergeFallRecno = BaseFall.MergeFallRecno

	inner join WarehouseOLAPMergedV2.APC.FallSeverity
	on BaseFallReference.InitialSeverityID = FallSeverity.SourceFallSeverityID

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseFall
	on	BaseFall.MergeFallRecno = BaseEncounterBaseFall.MergeFallRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounter
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterBaseFall.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
	on	coalesce(BaseEncounter.StartDirectorateCode, '9') = StartDirectorate.DirectorateCode

	inner join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
	on	coalesce(BaseEncounter.EndDirectorateCode, '9') = EndDirectorate.DirectorateCode

select
	@inserted = @@rowcount
	
end


/* Pressure Ulcer */

if @PREULCLocalDatasetCode = 'PREULC'

begin

	insert into dbo.WrkDataset
	(
		DatasetRecno
		,DatasetID
		,IdentifiedTime
		,IdentifiedDateID
		,SourcePressureUlcerCategoryCode
		,Validated
		,AdmissionTime
		,StartDirectorateID
		,EndDirectorateID		
		,SpecialtyID
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID
	)	

	select
		BasePressureUlcer.PressureUlcerRecno
		,DatasetID = @DatasetID
		,BasePressureUlcer.IdentifiedTime
		,BasePressureUlcerReference.IdentifiedDateID
		,PressureUlcerCategory.SourcePressureUlcerCategoryCode
		,Validated
		,BaseEncounter.AdmissionTime
		,StartDirectorateID = StartDirectorate.DirectorateID
		,EndDirectorateID = EndDirectorate.DirectorateID					
		,SpecialtyID = BaseEncounterReference.SpecialtyID	
		,StartSiteID = BaseEncounterReference.StartSiteID	
		,EndSiteID = BaseEncounterReference.EndSiteID
		,ClinicianID = BaseEncounterReference.ConsultantID
		,ServicePointID = BasePressureUlcerReference.WardID

	from
		WarehouseOLAPMergedV2.APC.BasePressureUlcer

	inner join WarehouseOLAPMergedV2.APC.BasePressureUlcerReference
	on	BasePressureUlcerReference.MergePressureUlcerRecno = BasePressureUlcer.MergePressureUlcerRecno

	inner join WarehouseOLAPMergedV2.APC.PressureUlcerCategory
	on BasePressureUlcerReference.CategoryID = PressureUlcerCategory.SourcePressureUlcerCategoryID

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterBasePressureUlcer
	on	BasePressureUlcer.MergePressureUlcerRecno = BaseEncounterBasePressureUlcer.MergePressureUlcerRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounter
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterBasePressureUlcer.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
	on	coalesce(BaseEncounter.StartDirectorateCode, '9') = StartDirectorate.DirectorateCode

	inner join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
	on	coalesce(BaseEncounter.EndDirectorateCode, '9') = EndDirectorate.DirectorateCode


select
	@inserted = @@rowcount
	
end

/* UTI */

if @UTILocalDatasetCode = 'UTI'

begin

	insert into dbo.WrkDataset
	(
		DatasetRecno
		,DatasetID
		,SymptomStartDate
		,SymptomStartDateID
		,UrinaryTestRequestedDateID
		,TreatmentStartDate
		,CatheterInsertedDateID
		,SourceCatheterTypeCode
		,AdmissionTime
		,AdmissionDate
		,StartDirectorateID
		,EndDirectorateID		
		,SpecialtyID
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID
	)	

	select
		BaseUrinaryTractInfection.UrinaryTractInfectionRecno
		,DatasetID = @DatasetID
		,BaseUrinaryTractInfection.SymptomStartDate
		,BaseUrinaryTractInfectionReference.SymptomStartDateID
		,UrinaryTestRequestedDateID
		,TreatmentStartDate
		,CatheterInsertedDateID
		,CatheterType.SourceCatheterTypeCode
		,AdmissionDate = BaseEncounter.AdmissionDate
		,AdmissionTime = BaseEncounter.AdmissionTime
		,StartDirectorateID = StartDirectorate.DirectorateID
		,EndDirectorateID = EndDirectorate.DirectorateID					
		,SpecialtyID = BaseEncounterReference.SpecialtyID	
		,StartSiteID = BaseEncounterReference.StartSiteID	
		,EndSiteID = BaseEncounterReference.EndSiteID
		,ClinicianID = BaseEncounterReference.ConsultantID
		,ServicePointID = BaseUrinaryTractInfectionReference.WardID

	from
		WarehouseOLAPMergedV2.APC.BaseUrinaryTractInfection

	inner join WarehouseOLAPMergedV2.APC.BaseUrinaryTractInfectionReference
	on	BaseUrinaryTractInfectionReference.MergeUrinaryTractInfectionRecno = BaseUrinaryTractInfection.MergeUrinaryTractInfectionRecno

	inner join WarehouseOLAPMergedV2.APC.CatheterType
	on BaseUrinaryTractInfectionReference.CatheterTypeID = CatheterType.SourceCatheterTypeID

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseUrinaryTractInfection
	on	BaseUrinaryTractInfection.MergeUrinaryTractInfectionRecno = BaseEncounterBaseUrinaryTractInfection.MergeUrinaryTractInfectionRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounter
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterBaseUrinaryTractInfection.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
	on	coalesce(BaseEncounter.StartDirectorateCode, '9') = StartDirectorate.DirectorateCode

	inner join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
	on	coalesce(BaseEncounter.EndDirectorateCode, '9') = EndDirectorate.DirectorateCode


select
	@inserted = @@rowcount
	
end


/* VTE */

if @VTELocalDatasetCode = 'VTECOND'

begin

	insert into dbo.WrkDataset
	(
		DatasetRecno
		,DatasetID
		,DiagnosisDateID
		,MedicationRequired
		,MedicationStartDateID
		,SourceVTETypeCode
		,DiagnosisDate
		,AdmissionDate
		,StartDirectorateID
		,EndDirectorateID		
		,SpecialtyID
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID
	)	

	select
		BaseVTECondition.VTEConditionRecno
		,DatasetID = @DatasetID
		,BaseVTEConditionReference.DiagnosisDateID
		,BaseVTECondition.MedicationRequired
		,BaseVTEConditionReference.MedicationStartDateID
		,VTEType.SourceVTETypeCode
		,BaseVTECondition.DiagnosisDate
		,BaseEncounter.AdmissionDate
		,StartDirectorateID = StartDirectorate.DirectorateID
		,EndDirectorateID = EndDirectorate.DirectorateID					
		,SpecialtyID = BaseEncounterReference.SpecialtyID	
		,StartSiteID = BaseEncounterReference.StartSiteID	
		,EndSiteID = BaseEncounterReference.EndSiteID
		,ClinicianID = BaseEncounterReference.ConsultantID
		,ServicePointID = BaseVTEConditionReference.WardID

	from
		WarehouseOLAPMergedV2.APC.BaseVTECondition

	inner join WarehouseOLAPMergedV2.APC.BaseVTEConditionReference
	on	BaseVTEConditionReference.MergeVTEConditionRecno = BaseVTECondition.MergeVTEConditionRecno

	inner join WarehouseOLAPMergedV2.APC.VTEType
	on BaseVTEConditionReference.VTETypeID = VTEType.SourceVTETypeID

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseVTECondition
	on	BaseVTECondition.MergeVTEConditionRecno = BaseEncounterBaseVTECondition.MergeVTEConditionRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounter
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterBaseVTECondition.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
	on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
	on	coalesce(BaseEncounter.StartDirectorateCode, '9') = StartDirectorate.DirectorateCode

	inner join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
	on	coalesce(BaseEncounter.EndDirectorateCode, '9') = EndDirectorate.DirectorateCode

select
	@inserted = @@rowcount
	
end

/* QCR */

if @QCRLocalDatasetCode = 'QCR'

begin

	insert into dbo.WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,SourceEncounterNo
		--,StartDirectorateID
		--,StartDirectorateCode
		--,EndDirectorateID
		--,EndDirectorateCode
		--,SpecialtyID
		--,SpecialtyCode
		--,ClinicianID
		--,ClinicianCode
		--,StartSiteID
		--,EndSiteID
		,QCRAuditDate
		,QCRAuditDateID
		,QCRQuestionCode
		,QCRCustomListCode
		,QCRAnswer
		--,ServicePointID
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	) 
	
	select
		DatasetRecno = Encounter.AuditAnswerRecno
		,DatasetID = @DatasetID
		,Encounter.ContextCode
		,Encounter.AuditAnswerRecno
		--,StartDirectorateID = null -- TBA
		--,StartDirectorateCode = null
		--,EndDirectorateID = null
		--,EndDirectorateCode = null
		--,SpecialtyID = null
		--,Specialty = null
		--,ClinicianID = 2089344
		--,ClinicianCode = '-1'
		--,StartSiteID = null
		--,EndSiteID = null
		,Encounter.AuditDate
		,Reference.AuditDateID
		,QCRQuestionCode = Encounter.QuestionCode
		,QCRCustomListCode = CustomListQuestion.CustomListCode
		,QCRAnswer = Encounter.Answer
		--,Location.SourceValueID
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = Reference.WardID
		,StartServicePointID = Reference.WardID
	from
		WarehouseOLAPMergedV2.QCR.BaseAuditAnswer Encounter

	inner join WarehouseOLAPMergedV2.QCR.BaseAuditAnswerReference Reference
	on	Reference.AuditAnswerRecno = Encounter.AuditAnswerRecno

	--left join WarehouseOLAPMergedV2.COM.Location
	--on Location.SourceValueID = Reference.LocationID
	
	left join WarehouseOLAPMergedV2.QCR.CustomListQuestion 
	on CustomListQuestion.QuestionCode = Encounter.QuestionCode
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	coalesce (Encounter.DirectorateCode,'9') = Directorate.DirectorateCode
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = Encounter.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = Encounter.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = Encounter.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	--inner join WarehouseOLAPMergedV2.WH.ServicePoint
	--on ServicePoint.SourceServicePointID = '-1'
	
	where
		--Encounter.AuditDate between DATEADD(year, -2, getdate()) and GETDATE()
		Encounter.AuditDate between @QCRLocalStart and getdate()
	and Encounter.Answer in (0,1) -- Take Yes and No responses only: ignore nulls

select
	@inserted = @@rowcount
	
end
	
/* NEO */			--Commented out due to development work CH 16/09/2014

--if @LocalDatasetCode = 'NEO'

--begin

--	insert into WrkDataset
	
--	(
--		DatasetRecno
--		,DatasetID
--		,ContextCode
--		--,StartDirectorateID
--		--,StartDirectorateCode
--		--,EndDirectorateID
--		--,EndDirectorateCode
--		--,SpecialtyID
--		--,Specialty
--		--,ClinicianID
--		--,ClinicianCode
--		--,StartSiteID
--		--,EndSiteID
--		,AdmissionDate
--		,AdmissionDateID
--		,DischargeDate
--		,DischargeDateID
--		,GestationWeeks
--		,BirthWeight
--		,NeonatalReferralType
--		,ROPScreenDueDate
--		,ROPScreenDate
--		,AdmissionTemperature
--		,DischargeDestinationCode
--		,DaysCooled
--		,IntensiveCareDays
--		,StartDirectorateID 
--		,EndDirectorateID 				
--		,SpecialtyID 
--		,StartSiteID 
--		,EndSiteID 
--		,ClinicianID
--		,ServicePointID 
--		,StartServicePointID
		
--	)
	
--	select
--		DatasetRecno = Encounter.EncounterRecno
--		,DatasetID = @DatasetID
--		,Encounter.ContextCode
--		--,StartDirectorateID = null -- TBA
--		--,StartDirectorateCode = null
--		--,EndDirectorateID = null
--		--,EndDirectorateCode = null
--		--,SpecialtyID = null
--		--,Specialty = null
--		--,ClinicianID = 2089344
--		--,ClinicianCode = '-1'
--		--,StartSiteID = null
--		--,EndSiteID = null
--		,AdmissionDate = cast(Encounter.AdmissionTime as date)
--		,AdmissionDate.DateID
--		,DischargeDate = cast(Encounter.DischargeTime as date)
--		,DischargeDateID = DischargeDate.DateID
--		,Patient.GestationWeeks
--		,Patient.BirthWeight
--		,NeonatalReferralType = Encounter.ReferralType
--		,Patient.ROPScreenDueDate
--		,Encounter.ROPScreenDate
--		,Encounter.AdmissionTemperature
--		,Encounter.DischargeDestinationCode
--		,DaysCooled = coalesce(Cooled.DaysCooled, 0)
--		,IntensiveCareDays = coalesce(IntensiveCare.IntensiveCareDays, 0)
--		,StartDirectorateID = Directorate.DirectorateID
--		,EndDirectorateID = Directorate.DirectorateID					
--		,SpecialtyID = Specialty.SourceSpecialtyID	
--		,StartSiteID = Site.SourceSiteID	
--		,EndSiteID = Site.SourceSiteID	
--		,ClinicianID = Consultant.SourceConsultantID
--		,ServicePointID = ServicePoint.SourceServicePointID
--		,StartServicePointID = ServicePoint.SourceServicePointID
	  
--	from 
--		WarehouseOLAPMergedV2.Neonatal.BaseEncounter Encounter

--	inner join WarehouseOLAPMergedV2.Neonatal.BasePatient Patient
--	on Patient.NHSNumber = Encounter.NHSNumber
	
--	left join -- Join to Assessment table to pick up no of days the baby was cooled
--	(
--		select 
--			NHSNumber
--			,EpisodeNo
--			,DaysCooled = count(*)
--		from
--			WarehouseOLAPMergedV2.Neonatal.BaseAssessment
--		where
--			CooledToday = 1
--		group by
--			NHSNumber
--			,EpisodeNo
	
--	) Cooled
--	on Cooled.NHSNumber = Encounter.NHSNumber
--	and Cooled.EpisodeNo = Encounter.EpisodeNo
	
--	left join -- Join to Assessment table to pick up no of days the baby received intensive care
--	(
--		select 
--			NHSNumber
--			,EpisodeNo
--			,IntensiveCareDays = count(*)
--		from
--			WarehouseOLAPMergedV2.Neonatal.BaseAssessment
--		where
--			NeonatalLevelOfCareCode = '1'
--		group by
--			NHSNumber
--			,EpisodeNo
	
--	) IntensiveCare
	
--	on	IntensiveCare.NHSNumber = Encounter.NHSNumber
--	and IntensiveCare.EpisodeNo = Encounter.EpisodeNo
	
--	left join WarehouseOLAPMergedV2.WH.Calendar AdmissionDate
--	on	cast(Encounter.AdmissionTime as date) = AdmissionDate.TheDate
	    
--	left join WarehouseOLAPMergedV2.WH.Calendar DischargeDate
--	on	cast(Encounter.DischargeTime as date) = DischargeDate.TheDate
	
--	inner join WarehouseOLAPMergedV2.WH.Directorate
--	on	Directorate.DirectorateCode = '9'
	
--	inner join WarehouseOLAPMergedV2.WH.Specialty
--	on	Specialty.SourceContextCode = Encounter.ContextCode
--	and Specialty.SourceSpecialtyCode = '-1'
	
--	inner join WarehouseOLAPMergedV2.WH.Site
--	on	Site.SourceContextCode = Encounter.ContextCode
--	and Site.SourceSiteCode = '-1'
	
--	inner join WarehouseOLAPMergedV2.WH.Consultant
--	on	Consultant.SourceContextCode = Encounter.ContextCode
--	and Consultant.SourceConsultantCode = '-1'
	
--	inner join WarehouseOLAPMergedV2.WH.ServicePoint
--	on ServicePoint.SourceServicePointID = '-1'
	

--	where
--		 cast(Encounter.AdmissionTime as date) >= @NEOLocalStart

--select
--	@inserted = @@rowcount
	
--end

/* MAT */

if @MATLocalDatasetCode = 'MAT'

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		--,StartDirectorateID
		--,StartDirectorateCode
		--,EndDirectorateID
		--,EndDirectorateCode
		--,SpecialtyID
		--,Specialty
		--,ClinicianID
		--,ClinicianCode
		--,StartSiteID
		--,EndSiteID
		,InfantDateOfBirth
		,InfantDateOfBirthID
		,MethodDelivery
		,Perineum
		,BirthOrder
		,BirthOutcome
		
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	)
	select
		DatasetRecno = Birth.BirthRecno
		,DatasetID = @DatasetID
		,Birth.ContextCode
		--,StartDirectorateID = null -- TBA
		--,StartDirectorateCode = null
		--,EndDirectorateID = null
		--,EndDirectorateCode = null
		--,SpecialtyID = null
		--,Specialty = null
		--,ClinicianID = 2089344
		--,ClinicianCode = '-1'
		--,StartSiteID = null
		--,EndSiteID = null
		,Birth.InfantDateOfBirth
		,InfantDateOfBirthID = DateOfBirth.DateID
		,Birth.MethodDelivery
		,Birth.Perineum
		,Birth.BirthOrder
		,Birth.Outcome
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID
	  
	from 
		WarehouseOLAPMergedV2.Maternity.BaseBirth Birth
	    
	inner join WarehouseOLAPMergedV2.WH.Calendar DateOfBirth -- DG 28.2.14 - changed to inner join
	on cast(Birth.InfantDateOfBirth as date) = DateOfBirth.TheDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '101' -- 28/01/2016 RR Was 9 - default, Marianne L advised that the birth data should be under St Mary's not unnassigned (mortality dashboard).  Discussed with DG, ideally the directorate needs to be done in the ETL process.  E-mailed BR to see if there is any reason why it wouldn't be St Mary's?
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = Birth.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = Birth.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = Birth.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
	
	where
		--datediff(year, Birth.InfantDateOfBirth, getdate()) <= 2
		Birth.InfantDateOfBirth >= @MATLocalStart

select
	@inserted = @@rowcount
	
end
	
/* RENQ */
	
if @RENQLocalDatasetCode = 'RENQ' -- Renal Quarterly Census

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		--,EndDirectorateID
		--,EndDirectorateCode
		--,SpecialtyID
		--,Specialty
		--,ClinicianID
		--,ClinicianCode
		--,EndSiteID
		,[LastDayOfQuarter]
		,[LastDayOfQuarterID]
		,[DateOfBirth]
		,[DateOfDeath]
		,[ModalityCode]
		,[ModalitySettingCode]
		,[IsPrimary]
		,[DialysisProviderCode]
		,[EventDetailCode]
		,[StartDate]
		,[StopDate]
		,[ReasonForChangeCode]
		,[VascularAccessCode]
		,[HCO3Num]
		,[PostVitalsSittingBloodPressureSystolic]
		,[PostVitalsSittingBloodPressureDiastolic]
		,[ActualRunTime]
		,[PreVitalsWeightInKg]
		,[PostVitalsWeightInKg]
		,[URRNum]
		,[UltrafiltrationVolume]
		,[HgBNum]
		,[FerritinNum]
		,[TimingQualifier]
		,[PhosNum]
		,[CaAdjNum]
		,[PTHIntactNum]
		,[TransplantListStatusCode]
		,[OnEPO]
		,[PreviousHDModalityExists]
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	)

	select
		DatasetRecno = R.PatientRenalModalityRecno
		,DatasetID = @DatasetID
		,R.ContextCode
		--,EndDirectorateID = null
		--,EndDirectorateCode = null
		--,SpecialtyID = null
		--,Specialty = null
		--,ClinicianID = null
		--,ClinicianCode = null
		--,EndSiteID = null
		,R.[LastDayOfQuarter]
		,LastDayOfQuarterID = QtrEnd.DateID
		,R.[DateOfBirth]
		,R.[DateOfDeath]
		,R.[ModalityCode]
		,R.[ModalitySettingCode]
		,R.[IsPrimary]
		,R.[DialysisProviderCode]
		,R.[EventDetailCode]
		,R.[StartDate]
		,R.[StopDate]
		,R.[ReasonForChangeCode]
		,R.[VascularAccessCode]
		,R.[HCO3Num]
		,R.[PostVitalsSittingBloodPressureSystolic]
		,R.[PostVitalsSittingBloodPressureDiastolic]
		,R.[ActualRunTime]
		,R.[PreVitalsWeightInKg]
		,R.[PostVitalsWeightInKg]
		,R.[URRNum]
		,R.[UltrafiltrationVolume]
		,R.[HgBNum]
		,R.[FerritinNum]
		,R.[TimingQualifier]
		,R.[PhosNum]
		,R.[CaAdjNum]
		,R.[PTHIntactNum]
		,R.[TransplantListStatusCode]
		,R.[OnEPO]
		,R.[PreviousHDModalityExists]
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID
	  
	from
		WarehouseOLAPMergedV2.Renal.QuarterlyCensus R

	inner join WarehouseOLAPMergedV2.WH.Calendar QtrEnd -- DG 28.2.14 - changed to inner join
	on R.LastDayOfQuarter = QtrEnd.TheDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = R.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = R.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = R.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	where
		--R.LastDayOfQuarter > DATEADD(year, -5, getdate())
		R.LastDayOfQuarter >= @RENQLocalStart

select
	@inserted = @@rowcount
	
end
	
/* REN - Renal Modality */

if @RENLocalDatasetCode = 'REN' 

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,StartDate
		,StartDateID
		,StopDate
		,ContextCode
		,ModalityCode
		,ModalitySettingCode
		,EventDetailCode
		,DialysisProviderCode
		,VascularAccessCode
		,VascularAccessAt90DaysCode
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	)
	
	select
		M.PatientRenalModalityRecno
		,DatasetID = @DatasetID
		,M.StartDate
		,Start.DateID
		,M.StopDate
		,M.ContextCode
		,M.ModalityCode
		,M.ModalitySettingCode
		,M.EventDetailCode
		,M.DialysisProviderCode
		,VascularAccessCode = A0.TimelineEventDetailCode
		,VascularAccessAt90DaysCode = A90.TimelineEventDetailCode
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID

	from
		WarehouseOLAPMergedV2.Renal.BasePatientRenalModality M

	left join WarehouseOLAPMergedV2.Renal.BasePatientVascularAccess A0 
	on	A0.PatientObjectID = M.PatientObjectID
	and A0.StartDate >= M.StartDate
	and not exists
					(
						select
							1 
						from
							WarehouseOLAPMergedV2.Renal.BasePatientVascularAccess A
						where
							A.PatientObjectID = M.PatientObjectID
						and A.StartDate >= M.StartDate
						and 
							(
								A.StartDate < A0.StartDate 
							or 
								(
									A.StartDate = A0.StartDate 
								and A.SourceUniqueID < A0.SourceUniqueID
								)
							)
					)

	left join WarehouseOLAPMergedV2.Renal.BasePatientVascularAccess A90 
	on	A90.PatientObjectID = M.PatientObjectID
	and A90.StartDate >= dateadd(day, 90, M.StartDate)

	and not exists
	(
		select
			1 
		from
			WarehouseOLAPMergedV2.Renal.BasePatientVascularAccess A
		where
			A.PatientObjectID = M.PatientObjectID
		and A.StartDate >= dateadd(day, 90, M.StartDate)
		and (
				A.StartDate < A90.StartDate 
			or 
				(
					A.StartDate = A90.StartDate 
				and A.SourceUniqueID < A90.SourceUniqueID
				)
			)
	)

	inner join WarehouseOLAPMergedV2.WH.Calendar Start
	on cast(M.StartDate as date) = Start.TheDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = M.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = M.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = M.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	where
		--M.StartDate > DATEADD(year, -5, getdate())
		M.StartDate >= @RENLocalStart

select
	@inserted = @@rowcount
	
end
	
/* CANT - Cancer Definitive Treatment */

if @CANTLocalDatasetCode = 'CANT' 

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		--,SpecialtyID
		,SpecialtyCode
		,ReferralDate
		,AppointmentDate
		,AppointmentDateID
		,PriorityTypeCode
		,InappropriateReferralCode
		,TumourStatusCode
		,HistologyCode
		,CancerSite
		,CancerTypeCode
		,PrimaryDiagnosisCode
		,PrimaryDiagnosisCode1
		,DiagnosisDate
		,DecisionToTreatAdjustmentReasonCode
		,DecisionToTreatAdjustment
		,ReceiptOfReferralDate
		,ReferralSourceCode
		,SourceForOutpatientCode
		,CancelledAppointmentDate
		,FirstAppointmentWaitingTimeAdjusted
		,FirstAppointmentAdjustmentReasonCode
		,ConsultantUpgradeDate
		,TreatmentDateID
		,TreatmentDate
		,DecisionToTreatDate
		,TreatmentEventTypeCode
		,TreatmentSettingCode
		,TreatmentNo
		,InitialTreatmentCode
		,WaitingTimeAdjustment
		,AdjustmentReasonCode
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	)
	select
		DatasetRecno = R.MergeRecno
		,DatasetID = @DatasetID
		,R.ContextCode
		--,RR.SpecialtyID
		,SpecialtyCode = Specialty.NationalSpecialtyCode
		,ReferralDate = R.ReceiptOfReferralDate
		,AppointmentDate = R.FirstAppointmentDate
		,AppointmentDateID = ApptDate.DateID
		,R.PriorityTypeCode
		,R.InappropriateReferralCode
		,R.TumourStatusCode
		,R.HistologyCode
		,R.CancerSite
		,R.CancerTypeCode
		,R.PrimaryDiagnosisCode
		,R.PrimaryDiagnosisCode1
		,R.DiagnosisDate
		,R.DecisionToTreatAdjustmentReasonCode
		,R.DecisionToTreatAdjustment
		,R.ReceiptOfReferralDate
		,ReferralSourceCode = R.SourceOfReferralCode
		,R.SourceForOutpatientCode
		,R.CancelledAppointmentDate
		,R.FirstAppointmentWaitingTimeAdjusted
		,R.FirstAppointmentAdjustmentReasonCode
		,R.ConsultantUpgradeDate
		,TreatmentDateID = TDate.DateID
		,TreatmentDate = T.FirstDefinitiveTreatmentDate
		,T.DecisionToTreatDate
		,T.TreatmentEventTypeCode
		,T.TreatmentSettingCode
		,T.TreatmentNo
		,T.InitialTreatmentCode
		,T.WaitingTimeAdjustment
		,T.AdjustmentReasonCode	
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = RR.SpecialtyID
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID
				
	from
		WarehouseOLAPMergedV2.SCR.BaseDefinitiveTreatment T  
		
	inner join WarehouseOLAPMergedV2.SCR.BaseReferral R 
	on T.ReferralUniqueRecordID = R.UniqueRecordId
	
	inner join WarehouseOLAPMergedV2.SCR.BaseReferralReference RR 
	on RR.MergeRecno = R.MergeRecno
	
	inner join WarehouseOLAPMergedV2.WH.Specialty 
	on Specialty.SourceSpecialtyID = RR.SpecialtyID
	
	left join WarehouseOLAPMergedV2.WH.Calendar ApptDate 
	on ApptDate.TheDate = R.FirstAppointmentDate
	
	inner join WarehouseOLAPMergedV2.WH.Calendar TDate 
	on TDate.TheDate = T.FirstDefinitiveTreatmentDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = R.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = R.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	where
		--datediff(year,T.FirstDefinitiveTreatmentDate,getdate()) <= 2
		T.FirstDefinitiveTreatmentDate >= @CANTLocalStart

select
	@inserted = @@rowcount 
	
end
	
/* CAN -- Cancer Referral */

if @CANLocalDatasetCode = 'CAN' 

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		--,SpecialtyID
		,SpecialtyCode
		,ReferralDate
		,ReferralDateID
		,AppointmentDate
		,AppointmentDateID
		,PriorityTypeCode
		,InappropriateReferralCode
		,TumourStatusCode
		,HistologyCode
		,CancerSite
		,CancerTypeCode
		,PrimaryDiagnosisCode
		,PrimaryDiagnosisCode1
		,DiagnosisDate
		,DiagnosisDateID
		,DecisionToTreatAdjustmentReasonCode
		,DecisionToTreatAdjustment
		,ReceiptOfReferralDate
		,ReferralSourceCode
		,SourceForOutpatientCode
		,CancelledAppointmentDate
		,FirstAppointmentWaitingTimeAdjusted
		,FirstAppointmentAdjustmentReasonCode
		,WasPatientDiscussedAtMDT
		,PerformanceStatusCode
		,MDTDate
		,MDTDateID
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	)
	select
		DatasetRecno = R.MergeRecno
		,DatasetID = @DatasetID
		,R.ContextCode
		--,RR.SpecialtyID
		,SpecialtyCode = Specialty.NationalSpecialtyCode
		,ReferralDate = R.DecisionToReferDate
		,ReferralDateID = ReferralDate.DateID
		,AppointmentDate = R.FirstAppointmentDate
		,AppointmentDateID = ApptDate.DateID
		,R.PriorityTypeCode
		,R.InappropriateReferralCode
		,R.TumourStatusCode
		,R.HistologyCode
		,R.CancerSite
		,R.CancerTypeCode
		,R.PrimaryDiagnosisCode
		,R.PrimaryDiagnosisCode1
		,R.DiagnosisDate
		,DiagnosisDateID = DiagnosisDate.DateID
		,R.DecisionToTreatAdjustmentReasonCode
		,R.DecisionToTreatAdjustment
		,R.ReceiptOfReferralDate
		,ReferralSourceCode = R.SourceOfReferralCode
		,R.SourceForOutpatientCode
		,R.CancelledAppointmentDate
		,R.FirstAppointmentWaitingTimeAdjusted
		,R.FirstAppointmentAdjustmentReasonCode
		--,P.WasPatientDiscussedAtMDT 
		--,P.PerformanceStatusCode
		--,P.MDTDate
		--,MDTDate.DateID
		,WasPatientDiscussedAtMDT = null
		,PerformanceStatusCode = null
		,MDTDate = null
		,MDTDateID = null
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = RR.SpecialtyID
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID
				
	from
		WarehouseOLAPMergedV2.SCR.BaseReferral R
		
	inner join WarehouseOLAPMergedV2.SCR.BaseReferralReference RR 
	on	RR.MergeRecno = R.MergeRecno
	
	inner join WarehouseOLAPMergedV2.WH.Specialty 
	on	Specialty.SourceSpecialtyID = RR.SpecialtyID
	
	--left outer join WarehouseOLAPMergedV2.SCR.BaseCarePlan P 
	--on	P.ReferralUniqueRecordId = R.UniqueRecordId
	
	left outer join WarehouseOLAPMergedV2.WH.Calendar ApptDate 
	on	ApptDate.TheDate = R.FirstAppointmentDate
	
	inner join WarehouseOLAPMergedV2.WH.Calendar ReferralDate 
	on ReferralDate.TheDate = R.ReceiptOfReferralDate
	
	left outer join WarehouseOLAPMergedV2.WH.Calendar DiagnosisDate 
	on	DiagnosisDate.TheDate = R.DiagnosisDate
	
	--left outer join WarehouseOLAPMergedV2.WH.Calendar MDTDate 
	--on	MDTDate.TheDate = P.MDTDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = R.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = R.ContextCode
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	where
		--datediff(year,R.ReceiptOfReferralDate,getdate()) <= 2
		R.ReceiptOfReferralDate >= @CANLocalStart

select
	@inserted = @@rowcount
	
end

/* MAN -- Manually Collated Data */

if @MANLocalDatasetCode = 'MAN' 

begin

	insert into WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,ItemID
		,Date
		,DateID
		,Numerator
		,Denominator
		
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
	)
	select
		DatasetRecno
		,DatasetID = @DatasetID
		,ContextCode
		,ItemID 
		,Date 	
		,DateID = Calendar.DateID
		,Numerator 
		,Denominator 
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID
		,StartServicePointID = ServicePoint.SourceServicePointID

		from
			(
			select
				DatasetRecno = IndicatorValues.Id
				,ContextCode = 'CMFT||PERFMON'
				,ItemID = IndicatorIdentifier
				,Date = convert(
								varchar (11),
								cast('1' 
									+ '/' + cast(Month as varchar) 
									+ '/' + cast(
												case 
												when Month < 4   -- temp fix until Perf Mon issue investigated		
												then YearStart + 1 
												else YearStart 
												end 
											as varchar) 
								as date)
								,103)
				,Numerator = 
							case
							when charindex('/', Value) > 0
							then left(Value, charindex('/', Value) -1)
							else Value
							end
				,Denominator = 
							case
							when charindex('/', Value) > 0
							then substring(Value, charindex('/', Value) + 1, len(Value))
							else Value
							end			
			from
				PerformanceMonitoring.dbo.Indicators

			inner join PerformanceMonitoring.dbo.IndicatorValues
			on	Indicators.Id = Indicator

			) ManualIndicator
	
	left outer join WarehouseOLAPMergedV2.WH.Calendar 
	on	TheDate = ManualIndicator.Date
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = 'CMFT||PERFMON'
	and Site.SourceSiteCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = 'CMFT||PERFMON'
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CMFT||PERFMON'
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

select
	@inserted = @@rowcount
	
end


/* STAFFLEVEL -- Staff Level */

if @STAFFLEVELLocalDatasetCode = 'STAFFLEVEL' 

begin

insert into dbo.WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,SourceShift
		,RegisteredNursePlan
		,RegisteredNurseActual
		,NonRegisteredNursePlan
		,NonRegisteredNurseActual
		,CensusDateID
		,StartDirectorateID 
		,EndDirectorateID 	
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID 
		,ServicePointID 
		,StartServicePointID 
	)

	select
		DatasetRecno = BaseStaffingLevel.MergeStaffingLevelRecno
		,DatasetID = @DatasetID
		,BaseStaffingLevel.ContextCode
		,SourceShift
		,RegisteredNursePlan
		,RegisteredNurseActual
		,NonRegisteredNursePlan
		,NonRegisteredNurseActual
		,BaseStaffingLevelReference.CensusDateID
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = BaseStaffingLevelReference.WardID
		,StartServicePointID = BaseStaffingLevelReference.WardID

	from
		WarehouseOLAPMergedV2.APC.BaseStaffingLevel

	inner join WarehouseOLAPMergedV2.APC.BaseStaffingLevelReference
	on	BaseStaffingLevel.MergeStaffingLevelRecno = BaseStaffingLevelReference.MergeStaffingLevelRecno

	inner join WarehouseOLAPMergedV2.WH.Shift
	on Shift.SourceShiftID = BaseStaffingLevelReference.ShiftID
		
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = coalesce(BaseStaffingLevel.DirectorateCode, '9')
	
	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = BaseStaffingLevel.ContextCode
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = BaseStaffingLevel.ContextCode
	and Site.SourceSiteCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = BaseStaffingLevel.ContextCode
	and Consultant.SourceConsultantCode = '-1'

select
	@inserted = @@rowcount
	
end


/* ALERT -- Alert */

if @AlertLocalDatasetCode = 'ALERT' 

begin

	insert into dbo.WrkDataset

	(
		DatasetRecno
		,DatasetID
		,CreatedTime
		,CreatedDateID
		,ClosedTime
		,StartDirectorateID 
		,EndDirectorateID 	
		,SpecialtyID 
		,StartSiteID
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
	)
		
	select
		BaseAlert.MergeAlertRecno
		,DatasetID = @DatasetID
		,CreatedTime
		,CreatedDateID = BaseAlertReference.CreatedDateID
		,ClosedTime
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID					
		,SpecialtyID = BaseAlertReference.SpecialtyID	
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = BaseAlertReference.LocationID

	from
		WarehouseOLAPMergedV2.Observation.BaseAlert

	inner join WarehouseOLAPMergedV2.Observation.BaseAlertReference
	on	BaseAlertReference.MergeAlertRecno = BaseAlert.MergeAlertRecno

	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	coalesce(BaseAlert.DirectorateCode, '9') = Directorate.DirectorateCode

	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = BaseAlert.ContextCode
	and Site.SourceSiteCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = BaseAlert.ContextCode
	and Consultant.SourceConsultantCode = '-1'

	where
		BaseAlert.CreatedDate >= @AlertLocalStart


select
	@inserted = @@rowcount
	
end


-- 20140804 RR
/* Cancelled Operations */
 
if @CancOpsLocalDatasetCode = 'CanOp' 

begin
	
	Insert into dbo.WrkDataset
	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,SpecialtyID
		,ClinicianID
		,StartWardCode
		,CancellationDate
		,CancellationDateID
		,CancellationReason
		,ReportableBreach
		,StartDirectorateID
		,EndDirectorateID
		,StartSiteID
		,EndSiteID
		,ServicePointID
		,StartServicePointID
	)
	select
		DatasetRecno = Base.MergeCancelledOperationRecno
		,DatasetID = @DatasetID
		,ContextCode = Base.ContextCode
		,SpecialtyID = Reference.SpecialtyID
		,ClinicianID = Reference.ConsultantID
		,StartWardCode = Base.WardCode
		,CancellationDate = Base.CancellationDate
		,CancellationDateID = Reference.CancelledOperationDateID
		,CancellationReason = Base.CancellationReason
		,ReportableBreach = Base.ReportableBreach
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID
		,StartSiteID = Site.SourceSiteID
		,EndSiteID = Site.SourceSiteID
		,ServicePointID = Reference.WardID
		,StartServicePointID = Reference.WardID		
    from 
		WarehouseOLAPMergedV2.Theatre.BaseCancelledOperation Base

	inner join WarehouseOLAPMergedV2.Theatre.BaseCancelledOperationReference Reference
	on Base.MergeCancelledOperationRecno = Reference.MergeCancelledOperationRecno

	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = coalesce(Base.DirectorateCode, '9')

	inner join WarehouseOLAPMergedV2.WH.Site
	on Site.SourceContextCode = 'CMFT||CanOp'
	and Site.SourceSiteCode = '-1'

	where
		(
		Removed is null
	and	(
			CancellationReason <> 'Medical reason' 
		or 	CancellationReason is null
		)
	and  Base.CancellationDate >= @CancOpsLocalStart
		)

select
	@inserted = @@rowcount

end

-- 20140807 RR Friends & Family
/* FFT */

if @FFTLocalDatasetCode = 'FFTRTN' 

begin
	
	insert dbo.WrkDataset

	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,CensusDate
		,EndWardCode
		,EndSiteCode
		
		,CensusDateID
		,ServicePointID
		,StartSiteID 
		,EndSiteID 
		,SpecialtyID 
		,ClinicianID 
		,StartDirectorateID 
		,EndDirectorateID
		
		,FFTReturnType 
		,ExtremelyLikeley 
		,Likely 
		,NeitherLikelyNorUnlikely 
		,Unlikely 
		,ExtremelyUnlikely 
		,DontKnow 
		,Responses 
		,EligibleResponders 
	)

	select
		FFT.MergeFFTRecno
		,DatasetID = @DatasetID
		,ContextCode = FFT.ContextCode
		,CensusDate = FFT.CensusDate
		,EndWardCode = FFT.WardCode
		,EndSiteCode = FFT.HospitalSiteCode
		
		,CensusDateID = Reference.CensusDateID
		,ServicePointID  = Reference.WardID
		,StartSiteID = Reference.SiteID
		,EndSiteID = Reference.SiteID
		,SpecialtyID = Specialty.SourceSpecialtyID -- default
		,ClinicianID = Consultant.SourceConsultantID -- default
		,DirectorateID = Directorate.DirectorateID -- default
		,DirectorateID = Directorate.DirectorateID -- default
		
		,FFTReturnType = FFT.[Return]
		,ExtremelyLikeley = FFT.[1ExtremelyLikely]
		,Likely = FFT.[2Likely]
		,NeitherLikelyNorUnlikely = FFT.[3NeitherLikelyNorUnlikely]
		,Unlikely = FFT.[4Unlikely]
		,ExtremelyUnlikely = FFT.[5ExtremelyUnlikely]
		,DontKnow = FFT.[6DontKnow]
		,Responses = FFT.Responses
		,EligibleResponders = FFT.EligibleResponders
	from
		WarehouseOLAPMergedV2.PEX.BaseFriendsFamilyTestReturn FFT

	inner join WarehouseOLAPMergedV2.PEX.BaseFriendsFamilyTestReturnReference Reference
	on FFT.MergeFFTRecno = Reference.MergeFFTRecno

	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	coalesce(FFT.DirectorateCode, '9') = Directorate.DirectorateCode

	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = 'CMFT||FFTRTN'
	and Specialty.SourceSpecialtyCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CMFT||FFTRTN'
	and Consultant.SourceConsultantCode = '-1'

select
	@inserted = @@rowcount

end


if @HSMRLocalDatasetCode = 'HSMR' 

begin
	
	insert dbo.WrkDataset

	(
		DatasetRecno
		,DatasetID
		,ContextCode
		,DischargeDateID
		,RiskScore
		,Died
		,StartDirectorateID
		,EndDirectorateID	
		,StartSiteID 
		,EndSiteID
		,SpecialtyID 
		,ClinicianID 
		,ServicePointID
		,StartServicePointID
	)

	select
		DatasetRecno = BaseHSMR.HSMRRecno
		,DatasetID = @DatasetID
		,ContextCode
		,DischargeDateID = Calendar.DateID
		,RiskScore
		,Died
		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID			
		,StartSiteID = Site.SourceSiteID	
		,EndSiteID = Site.SourceSiteID	
		,SpecialtyID = Specialty.SourceSpecialtyID
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID	= ServicePoint.SourceServicePointID	
		,StartServicePointID = ServicePoint.SourceServicePointID	
	from
		WarehouseOLAPMergedV2.APC.BaseHSMR
		
	inner join WarehouseOLAPMergedV2.WH.Calendar
	on	Calendar.TheDate = BaseHSMR.DischargeDate
	
	inner join WarehouseOLAPMergedV2.WH.Directorate
	on	Directorate.DirectorateCode = '9'
		
	inner join WarehouseOLAPMergedV2.WH.Site
	on Site.SourceContextCode = 'CMFT||DRFOST'
	and Site.SourceSiteCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceContextCode = 'CMFT||DRFOST'
	and Specialty.SourceSpecialtyCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CMFT||DRFOST'
	and Consultant.SourceConsultantCode = '-1'
	
	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'
		
	where
		DischargeDate > @HSMRLocalStart
	and not exists
				(
				select
					1
				from
					WarehouseOLAPMergedV2.APC.BaseHSMR Later
				where
					Later.DistrictNo = BaseHSMR.DistrictNo
				and Later.AdmissionDate = BaseHSMR.AdmissionDate
				and	Later.DischargeDate = BaseHSMR.DischargeDate
				and	Later.ConsultantCode = BaseHSMR.ConsultantCode
				and	Later.SourceUniqueID > BaseHSMR.SourceUniqueID
				)

select
	@inserted = @@rowcount

end

-- 20150811	RR added activity
--2015-09-08 CCB modified to include ActivityMetricCode

if @ActivityLocalDatasetCode = 'ACTIVITY' 

begin
	
	insert dbo.WrkDataset
		(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,ItemID
		,Date
		,DateID
		,Numerator
		,Denominator		
		,StartDirectorateID 
		,EndDirectorateID 				
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID
		,ActivityMetricCode
		)
	select
		 DatasetRecno 
		,DatasetID
		,ContextCode
		,ItemID 
		,Date 
		,DateID 
		,Numerator 
		,Denominator
		,StartDirectorateID 
		,EndDirectorateID 
		,SpecialtyID 
		,StartSiteID 
		,EndSiteID 
		,ClinicianID
		,ServicePointID 
		,StartServicePointID 
		,ActivityMetricCode
	from
		(
		--numerator
		select 
			 DatasetRecno = Activity.MergeEncounterRecno
			,DatasetID = @DatasetID
			,Context.ContextCode

			,ItemID = -- CCB 2015-09-07 remove this and handle in the Numerator Templates
				case ActivityMetric.MetricCode
				when 'DCSPL' then 888--Day Case Spells
				when 'IESPL' then 888--Inpatient Elective Spells
				when 'FATT' then 889--First Attendances
				when 'RATT' then 889--Review Attendances
				else null
				end

			,Date = Calendar.TheDate --uurgh
			,DateID = Activity.EncounterDateID 
			,Numerator = 1
			,Denominator = null
			,StartDirectorateID = Activity.DirectorateID
			,EndDirectorateID = Activity.DirectorateID
			,SpecialtyID = Activity.SpecialtyID
			,StartSiteID = Site.SourceSiteID	
			,EndSiteID = Site.SourceSiteID	
			,ClinicianID = Consultant.SourceConsultantID
			,ServicePointID = ServicePoint.SourceServicePointID
			,StartServicePointID = ServicePoint.SourceServicePointID
			,ActivityMetricCode = ActivityMetric.MetricCode
		from
			WarehouseOLAPMergedV2.WH.FactActivity Activity

		inner join WarehouseOLAPMergedV2.WH.ActivityMetric
		on	ActivityMetric.MetricID = Activity.MetricID
		and	ActivityMetric.MetricParentCode in
				(
				 'ATT' -- OP Attenders
				,'ELSPL' -- Elective Spells
				,'NESPL' -- Non-Elective Spells
				)
		and	ActivityMetric.MetricCode not in
				(
				 'RDSPL' -- Regular Day Spells
				,'RNSPL' -- Regular Night Spells
				)
			
		inner join WarehouseOLAPMergedV2.WH.Calendar
		on	Calendar.DateID = Activity.EncounterDateID

		inner join WarehouseOLAPMergedV2.WH.Context
		on	Context.ContextID = Activity.ContextID
			
		inner join WarehouseOLAPMergedV2.WH.Site
		on	Site.SourceContextCode = Context.ContextCode
		and Site.SourceSiteCode = '-1'

		inner join WarehouseOLAPMergedV2.WH.Specialty
		on	Specialty.SourceContextCode = Context.ContextCode
		and Specialty.SourceSpecialtyCode = '-1'
			
		inner join WarehouseOLAPMergedV2.WH.Consultant
		on	Consultant.SourceContextCode = Context.ContextCode
		and Consultant.SourceConsultantCode = '-1'
			
		inner join WarehouseOLAPMergedV2.WH.ServicePoint
		on ServicePoint.SourceServicePointID = '-1'

		where
			Activity.ConsolidatedView = 1
		and	Activity.Reportable = 1

		union

		--denominator
		select
			 DatasetRecno
			,DatasetID = @DatasetID
			,ContextCode
			,ItemID
			,Date
			,DateID
			,Numerator = null
			,Denominator = sum(Denominator)
			,StartDirectorateID
			,EndDirectorateID
			,SpecialtyID
			,StartSiteID
			,EndSiteID
			,ClinicianID
			,ServicePointID
			,StartServicePointID
			,ActivityMetricCode
		from
			(
			select
				 DatasetRecno = 0 --Activity.MergeEncounterRecno
				,DatasetID = @DatasetID
				,ContextCode = null

				,ItemID = -- CCB 2015-09-07 remove this and handle in the Numerator Templates
					case ActivityMetric.MetricCode
					when 'DCSPL' then 888--Day Case Spells
					when 'IESPL' then 888--Inpatient Elective Spells
					when 'FATT' then 889--First Attendances
					when 'RATT' then 889--Review Attendances
					else null
					end

				,Date = Calendar.TheDate --uurgh
				,DateID = Activity.DateID --again, uurgh
				,Numerator = null
				,Denominator = Activity.Cases
				,StartDirectorateID = Activity.DirectorateID
				,EndDirectorateID = Activity.DirectorateID

				,SpecialtyID =
					(
					select top 1 --arbitrarily pick the first value
						SourceSpecialtyID
					from
						WarehouseOLAPMergedV2.WH.Specialty
					where
						Specialty.NationalSpecialtyCode = Activity.NationalSpecialtyCode
					)

				,StartSiteID = Site.SourceSiteID	
				,EndSiteID = Site.SourceSiteID	
				,ClinicianID = Consultant.SourceConsultantID
				,ServicePointID = ServicePoint.SourceServicePointID
				,StartServicePointID = ServicePoint.SourceServicePointID
				,ActivityMetricCode = ActivityMetric.MetricCode
				--,Activity.Cases
				--,Activity.Value
			from
				WarehouseOLAPMergedV2.WH.FactActivityPlan Activity

			inner join WarehouseOLAPMergedV2.WH.ActivityMetric
			on	ActivityMetric.MetricID = Activity.MetricID
			and	ActivityMetric.MetricParentCode in
					(
					 'ATT' -- OP Attenders
					,'ELSPL' -- Elective Spells
					,'NESPL' -- Non-Elective Spells
					)
			and	ActivityMetric.MetricCode not in
					(
					 'RDSPL' -- Regular Day Spells
					,'RNSPL' -- Regular Night Spells
					)

			inner join WarehouseOLAPMergedV2.WH.Calendar
			on	Calendar.DateID = Activity.DateID

			and	Calendar.TheDate < SYSDATETIME() --find a better way to do this...
			
			inner join WarehouseOLAPMergedV2.WH.Site
			on	Site.SourceContextCode = 'CMFT||PERFMON'
			and Site.SourceSiteCode = '-1'
			
			inner join WarehouseOLAPMergedV2.WH.Consultant
			on	Consultant.SourceContextCode = 'CMFT||PERFMON'
			and Consultant.SourceConsultantCode = '-1'
			
			inner join WarehouseOLAPMergedV2.WH.ServicePoint
			on ServicePoint.SourceServicePointID = '-1'
			) ActivityPlan

		group by
			 DatasetRecno
			,ContextCode
			,ItemID
			,Date
			,DateID
			,StartDirectorateID
			,EndDirectorateID
			,SpecialtyID
			,StartSiteID
			,EndSiteID
			,ClinicianID
			,ServicePointID
			,StartServicePointID
			,ActivityMetricCode
		)Activity

select
	@inserted = @@rowcount

end

-- 20150811CH added Bed Occupancy

if @BedOccupancyLocalDatasetCode = 'BEDOCC' 

begin
	
	insert dbo.WrkDataset
		(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,Date	
		,DateID
		,StartDirectorateCode	
		,StartDirectorateID			
		,SpecialtyCode
		,SpecialtyID
		,StartSiteCode
		,StartSiteID
		,StartWardCode
		,ClinicianCode
		,ClinicianID
		,ServicePointID
		,BedOccupancyNights
		,MedicalOutlier
		,AvailableInpatientBeds
		)

		select
			DatasetRecno = BaseBedOccupancy.MergeBedOccupancyRecno
			,DatasetID = @DatasetID 
			,ContextCode = Context.ContextCode
			,Date = BaseBedOccupancy.BedOccupancyDate
			,DateID = Calendar.DateID
			--,StartDirectorateCode = BaseEncounterDirectorate.DirectorateCode --Episodic Directorate was agreed (at the time - CH 31/12/2015)
			--,StartDirectorateID = BaseEncounterDirectorate.DirectorateID
			,StartDirectorateCode = BaseWardStayDirectorate.DirectorateCode --CH 31/12/2015 after discussion with GS
			,StartDirectorateID = BaseWardStayDirectorate.DirectorateID
			,SpecialtyCode = Specialty.SourceSpecialtyCode  -- default CH 31/12/2015
			,SpecialtyID = Specialty.SourceSpecialtyID  -- default CH 31/12/2015
			,StartSiteCode = Site.SourceSiteCode
			,StartSiteID = Site.SourceSiteID
			,StartWardCode = BaseWardStayWard.SourceWardCode
			,ClinicianCode = Consultant.SourceConsultantCode  -- default CH 31/12/2015
			,ClinicianID = Consultant.SourceConsultantID  -- default CH 31/12/2015
			,ServicePointID = ServicePoint.SourceServicePointID
			,BedOccupancyNights = BaseBedOccupancy.Nights
			,MedicalOutlier =	case 
									when BaseEncounterDirectorate.DivisionCode in ('MEDSP','MEDAC') and BaseWardStayDirectorate.DivisionCode not in ('MEDSP','MEDAC') 
									then 1
									else 0
								end
			,AvailableInpatientBeds = null

		from
			WarehouseOLAPMergedV2.APC.BaseEncounter

		inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
		on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
				
		inner join WarehouseOLAPMergedV2.BedOccupancy.Base BaseBedOccupancy
		on	BaseBedOccupancy.APCEncounterMergeEncounterRecno = BaseEncounter.MergeEncounterRecno

		inner join WarehouseOLAPMergedV2.APC.BaseWardStay 
		on BaseWardStay.MergeEncounterRecno = BaseBedOccupancy.WardStayMergeEncounterRecno

		inner join WarehouseOLAPMergedV2.WH.Directorate BaseEncounterDirectorate
		on BaseEncounterDirectorate.DirectorateCode = BaseEncounter.StartDirectorateCode	

		inner join WarehouseOLAPMergedV2.WH.Directorate BaseWardStayDirectorate
		on BaseWardStayDirectorate.DirectorateCode = BaseWardStay.DirectorateCode	
	
		inner join WarehouseOLAPMergedV2.APC.Ward BaseWardStayWard
		on	BaseWardStayWard.SourceWardCode = BaseWardStay.WardCode
		and BaseWardStayWard.SourceContextCode = BaseWardStay.ContextCode
		
		inner join WarehouseOLAPMergedV2.WH.Site	
		on	Site.SourceSiteCode = BaseWardStay.SiteCode	
		and Site.SourceContextCode = BaseWardStay.ContextCode
				
		--inner join WarehouseOLAPMergedV2.WH.Specialty	
		--on	Specialty.SourceSpecialtyCode = BaseEncounter.SpecialtyCode	
		--and Specialty.SourceContextCode = BaseEncounter.ContextCode
				
		--inner join WarehouseOLAPMergedV2.WH.Consultant	
		--on	Consultant.SourceConsultantCode = BaseEncounter.ConsultantCode	
		--and Consultant.SourceContextCode = BaseEncounter.ContextCode

		--default Consultant and Specialty agreed as Bed Occupancy is about the ward and bed not the Specialty and Consultant (mirrors the configurnation below)

		inner join WarehouseOLAPMergedV2.WH.Specialty
		on	Specialty.SourceContextCode = 'CEN||PAS'
		and Specialty.SourceSpecialtyCode = '-1'

		inner join WarehouseOLAPMergedV2.WH.Consultant
		on	Consultant.SourceContextCode = 'CEN||PAS'
		and Consultant.SourceConsultantCode = '-1'

		inner join WarehouseOLAPMergedV2.WH.ServicePoint
		on  ServicePoint.SourceServicePointID = BaseWardStayWard.SourceWardID

		inner join WarehouseOLAPMergedV2.WH.Calendar
		on	Calendar.TheDate = BaseBedOccupancy.BedOccupancyDate

		inner join WarehouseOLAPMergedV2.WH.Context
		on	Context.ContextCode = BaseEncounter.ContextCode	

		union

		--WardBedConfiguration
		select
			DatasetRecno = 0
			,DatasetID = @DatasetID 
			,ContextCode = WardDirecorate.ContextCode
			,Date = Calendar.TheDate
			,DateID = Calendar.DateID
			,StartDirectorateCode = WardDirecorate.DirectorateCode
			,StartDirectorateID = WardDirecorate.DirectorateID
			,SpecialtyCode = Specialty.SourceSpecialtyCode -- default
			,SpecialtyID = Specialty.SourceSpecialtyID -- default
			,StartSiteCode = WardDirecorate.SiteCode
			,StartSiteID = WardDirecorate.SiteID
			,StartWardCode = WardDirecorate.WardCode
			,ClinicianCode = Consultant.SourceConsultantCode -- default
			,ClinicianID = Consultant.SourceConsultantID -- default
			,ServicePointID = ServicePoint.SourceServicePointID
			,BedOccupancyNights = null
			,MedicalOutlier = null
			,AvailableInpatientBeds = WardBedConfiguration.InpatientBeds
		from
			WarehouseOLAPMergedV2.BedOccupancy.WardBedConfiguration
				
		inner join
			(
			select 
				DirectorateCode = Directorate.DirectorateCode
				,DirectorateID = Directorate.DirectorateID
				,WardCode = Ward.SourceWardCode
				,WardID = Ward.SourceWardID
				,SiteCode = Site.SourceSiteCode
				,SiteID = Site.SourceSiteID
				,ContextCode = Ward.SourceContextCode
       
			from 
				WarehouseOLAPMergedV2.Allocation.RuleBase
       
			inner join WarehouseOLAPMergedV2.Allocation.Allocation
			on Allocation.AllocationID = RuleBase.AllocationID

			inner join WarehouseOLAPMergedV2.WH.Ward
			on Ward.SourceWardCode = RuleBase.WardCode
			and Ward.SourceContextCode = RuleBase.SourceContextCode

			inner join WarehouseOLAPMergedV2.WH.Directorate
			on Directorate.DirectorateCode = Allocation.SourceAllocationID

			inner join WarehouseOLAPMergedV2.WH.Site
			on Site.SourceSiteCode = RuleBase.SourceSiteCode
			and Site.SourceContextCode = RuleBase.SourceContextCode

			where
				   Rulebase.SourceContextCode in ('CEN||PAS','TRA||UG')
			and Rulebase.WardCode is not null 
			and Rulebase.Template = 'Allocation.DirectorateBySiteWard'
			) WardDirecorate

		on	WardBedConfiguration.WardCode = WardDirecorate.WardCode
		and WardBedConfiguration.ContextCode = WardDirecorate.ContextCode

		inner join WarehouseOLAPMergedV2.WH.Calendar
		on Calendar.TheDate between WardBedConfiguration.StartDate and coalesce(WardBedConfiguration.EndDate, getdate())-->= '1 apr 2019'
		
		inner join WarehouseOLAPMergedV2.WH.ServicePoint
		on  ServicePoint.SourceServicePointID = WardDirecorate.WardID
		
		inner join WarehouseOLAPMergedV2.WH.Specialty
		on	Specialty.SourceContextCode = 'CEN||PAS'
		and Specialty.SourceSpecialtyCode = '-1'

		inner join WarehouseOLAPMergedV2.WH.Consultant
		on	Consultant.SourceContextCode = 'CEN||PAS'
		and Consultant.SourceConsultantCode = '-1'

select
	@inserted = @@rowcount

end

-- CB 2016-02-03 added RTT

if @RTTLocalDatasetCode = 'RTT'

begin

	if object_id('tempdb..#RTTEncounter') is not null
	drop table #RTTEncounter
	;

	with CensusSet
	(
		CensusDate
	)
	as
	(
	select
		CensusDate = TheDate
	from
		WarehouseOLAPMergedV2.WH.Calendar
	where
		TheDate = LastDayOfMonth
	and	TheDate between @RTTLocalStart and getdate() --@RTTLocalStart hardcoded to '1 Jan 2015' at the top of this script
	)



	select
		 EncounterRecno = Encounter.EncounterRecNo
		,Encounter.path_directorate
		,Encounter.PathwayStatusCode
		,Encounter.BreachBandCode
		,Encounter.path_spec
		,specialty = Encounter.Specialty
		,CensusDate = Encounter.Censusdate
		,path_cons =
			case
				when path_directorate = 'Trafford' 
				then left(Encounter.path_cons,CHARINDEX(',',Encounter.path_cons)-1)
				else Encounter.path_cons
			end
	into
		#RTTEncounter
	from
		RTTLegacy.RTT.Encounter
	
	inner join RTTLegacy.RTT.Fact
	on Encounter.FactEncounterRecNo = Fact.EncounterRecno

	inner join CensusSet
	on	CensusSet.CensusDate = Encounter.Censusdate

	where
		left(Encounter.type , 3) != 'DAA'
	and Encounter.AdjustedFlag = 'N'
	and Encounter.PathwayStatusCode in
			(
			 'IPW' --Inpatient Waiters
			,'OPW' --Outpatient Waiters
			,'ACS' --Admitted Clock Stops
			,'OPT' --Outpatient Treatments
			)
	and Fact.ReportableFlag = 1
	;


	insert into WrkDataset
	(
		 DatasetRecno
		,DatasetID
		,ContextCode
		,PathwayStatusCode
		,BreachStatusCode
		,StartDirectorateCode
		,EndDirectorateCode
		,SpecialtyCode
		,ClinicianCode
		,StartSiteCode
		,EndSiteCode
		,CensusDate
		,CensusDateID
		,StartDirectorateID
		,EndDirectorateID
		,SpecialtyID
		,StartSiteID
		,EndSiteID
		,ClinicianID
		,ServicePointID
		,StartServicePointID
	)

	select
		 Encounter.EncounterRecno
		,DatasetID = @DatasetID

		,ContextCode =
			case
			when Encounter.path_directorate = 'Trafford' then 'TRA||UG'
			else 'CEN||PAS'
			end

		,PathwayStatusCode
		--,PathwayStatus

		,BreachStatusCode = 
			case
			when left(BreachBandCode , 2) = 'BR' then 'B'
			else 'N'
			end

		--,Encounter.path_directorate
		--,Encounter.path_cons
		--,Encounter.path_spec
		,StartDirectorateCode = Directorate.DirectorateCode
		,EndDirectorateCode = Directorate.DirectorateCode

		,SpecialtyCode = coalesce(Encounter.path_spec, Encounter.specialty)

		,ClinicianCode =
 			coalesce(ConsultantCentral.SourceConsultantCode , ConsultantTrafford.SourceConsultantCode , ConsultantTraffordNotSpecified.SourceConsultantCode)

		,StartSiteCode =
			coalesce(SiteCentral.SourceSiteCode , SiteTrafford.SourceSiteCode)

		,EndSiteCode =
			coalesce(SiteCentral.SourceSiteCode , SiteTrafford.SourceSiteCode)

		,Encounter.CensusDate
		,CensusDateID = Census.DateID
		--,Encounter.TCIDate

		,StartDirectorateID = Directorate.DirectorateID
		,EndDirectorateID = Directorate.DirectorateID 

		,SpecialtyID =
			coalesce(SpecialtyCentral.SourceSpecialtyID , SpecialtyTrafford.SourceSpecialtyID)

		,StartSiteID =
			coalesce(SiteCentral.SourceSiteID , SiteTrafford.SourceSiteID)

		,EndSiteID =
			coalesce(SiteCentral.SourceSiteID , SiteTrafford.SourceSiteID)


		,ClinicianID = 
			coalesce(ConsultantCentral.SourceConsultantID , ConsultantTrafford.SourceConsultantID , ConsultantTraffordNotSpecified.SourceConsultantID)

		,ServicePointID = -1
		,StartServicePointID = -1
	
	from
		#RTTEncounter Encounter

	inner join WarehouseOLAPMergedV2.WH.Calendar Census 
	on Census.TheDate = Encounter.CensusDate


	--Directorate
	left join
		(
		select
			DirectorateID
			,DirectorateCode
			,Directorate =
				case
				when Directorate.Directorate = 'Saint Mary''s' then 'St Mary''s Hospital'
				when Directorate.Directorate = 'Specialist Surgery' then 'Spec Surgery'
				when Directorate.Directorate = 'Specialist Medicine' then 'Spec Medicine'
				when Directorate.Directorate = 'Acute & Rehabilitation' then 'Acute & Rehab'
				when Directorate.Directorate = 'Clinical & Scientific' then 'Clin & Scient'
				when Directorate.Directorate = 'Emergency Service' then 'Emergency Serv'
				when Directorate.Directorate = 'Mental Health Trust' then 'MH TRUST'
				else Directorate.Directorate
				end
		from
			WarehouseOLAPMergedV2.WH.Directorate Directorate
		where 
			not exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.WH.Directorate Latest
			where
				Latest.Directorate = Directorate.Directorate
			and Latest.DirectorateID < Directorate.DirectorateID
			)
		) Directorate
	on	Directorate.Directorate = coalesce(Encounter.path_directorate,'N/A')

	--Consultant
	left join WarehouseOLAPMergedV2.WH.Consultant ConsultantCentral
	on	Encounter.path_directorate != 'Trafford'
	and	ConsultantCentral.SourceContextCode = 'CEN||PAS'
	and	ConsultantCentral.SourceConsultantCode = coalesce(Encounter.path_cons , '-1')

	left join
		(
		select
			 Consultant.SourceConsultantID
			,Consultant.SourceConsultantCode
			,ConsultantSurname = ConsultantTrafford.surname
		from
			TraffordReferenceData.dbo.INFODEPT_cab_consultants ConsultantTrafford

		left join WarehouseOLAPMergedV2.WH.Consultant
		on	Consultant.SourceContextCode = 'TRA||UG'
		and	Consultant.SourceConsultantCode = ConsultantTrafford.sds_code

		where
			not exists
				(
				select
					1
				from
					TraffordReferenceData.dbo.INFODEPT_cab_consultants ConsultantTraffordDuplicate
				where
					ConsultantTraffordDuplicate.surname = ConsultantTrafford.surname
				and	ConsultantTraffordDuplicate.sds_code < ConsultantTrafford.sds_code
				)
		) ConsultantTrafford
	on	Encounter.path_directorate = 'Trafford'
	and	Encounter.path_cons = ConsultantTrafford.ConsultantSurname --(ConsultantTrafford.ConsultantSurname + '%')

	left join WarehouseOLAPMergedV2.WH.Consultant ConsultantTraffordNotSpecified
	on	ConsultantTraffordNotSpecified.SourceContextCode = 'TRA||UG'
	and	ConsultantTraffordNotSpecified.SourceConsultantCode = '-1'

	--Specialty
	left join WarehouseOLAPMergedV2.WH.Specialty SpecialtyCentral
	on	coalesce(Encounter.path_directorate,'') != 'Trafford'
	and	SpecialtyCentral.SourceContextCode = 'CEN||PAS'
	and	SpecialtyCentral.SourceSpecialtyCode = coalesce(encounter.path_spec, encounter.specialty)

	left join WarehouseOLAPMergedV2.WH.Specialty SpecialtyTrafford
	on	Encounter.path_directorate = 'Trafford'
	and	SpecialtyTrafford.SourceContextCode = 'TRA||UG'
	and	SpecialtyTrafford.NationalSpecialtyCode = coalesce(encounter.path_spec, encounter.specialty)
	and	not exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.WH.Specialty SpecialtyDuplicate
			where
				SpecialtyDuplicate.NationalSpecialtyID = SpecialtyTrafford.NationalSpecialtyID
			and	SpecialtyDuplicate.SourceContextCode = SpecialtyTrafford.SourceContextCode
			and	SpecialtyDuplicate.SourceSpecialtyID < SpecialtyTrafford.SourceSpecialtyID
			)

	--Site
	left join WarehouseOLAPMergedV2.WH.Site SiteCentral
	on	SiteCentral.SourceContextCode = 'CEN||PAS'
	and SiteCentral.SourceSiteCode = '-1'
	and	coalesce(Encounter.path_directorate,'') != 'Trafford'

	left join WarehouseOLAPMergedV2.WH.Site SiteTrafford
	on	SiteTrafford.SourceContextCode = 'TRA||UG'
	and SiteTrafford.SourceSiteCode = '-1'
	and	Encounter.path_directorate = 'Trafford'


select
	@inserted = @@rowcount

end



/* Write to audit log */


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Dataset: ' + @DatasetCode +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'
		

print @Stats


exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	
	
-- Set default values for Clinician, Directorate, Site and Specialty fields

--exec dbo.BuildWrkDatsetSetDefaultValues @DatasetCode











GO


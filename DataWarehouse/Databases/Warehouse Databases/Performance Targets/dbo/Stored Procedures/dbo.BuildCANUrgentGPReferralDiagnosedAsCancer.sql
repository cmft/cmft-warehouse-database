﻿
CREATE procedure [dbo].[BuildCANUrgentGPReferralDiagnosedAsCancer] (@type char(2), @site varchar(100)) as

-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CAN urgent GP referral diagnosed with cancer - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CAN urgent GP referral diagnosed with cancer - ' + @type, 'Numerator.CANUrgentGPReferralDiagnosedWithCancer' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Denominator record, if it does not exist
declare @DenominatorID int = (select DenominatorID from Denominator where Denominator = 'CAN urgent GP referral - ' + @type)

if @DenominatorID is null
begin
	insert into Denominator (Denominator, DenominatorLogic)
	values ('CAN urgent GP referral - ' + @type, 'Denominator.CANUrgentGPReferral' + @type)
	select @DenominatorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = (select ItemID from Item where Item = 'Percentage of 2 week wait cancer referrals with a diagnosis of cancer - ' + @site)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Percentage of 2 week wait cancer referrals with a diagnosis of cancer - ' + @site, 2, @NumeratorID, @DenominatorID, 1, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = @DenominatorID where ItemID = @ItemID
end


-- Create view DenominatorCANUrgentGPReferral with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Denominator.CANUrgentGPReferral' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)
	
declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.ReferralDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = ''' + @view + '''
	
where DatasetCode = ''CAN''
and D.CancerTypeCode <> ''16''
and D.PriorityTypeCode = ''03'' -- To be seen within 2 weeks
and D.ReferralSourceCode in (''03'',''98'') -- GP or Dentist 

and CancerSite = ''' + @site + ''''

exec(@sql)


-- Create view NumeratorCANUrgentGPReferralDiagnosedWithCancer with a suffix for the given CancerTypeCode

set @view = 'Numerator.CANUrgentGPReferralDiagnosedWithCancer' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)
	
set @sql = 
'
CREATE view ' + @view + ' as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.ReferralDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where DatasetCode = ''CAN''
and D.CancerTypeCode <> ''16''
and D.PriorityTypeCode = ''03'' -- To be seen within 2 weeks
and D.ReferralSourceCode in (''03'',''98'') -- GP or Dentist 
and left(D.PrimaryDiagnosisCode, 1) = ''C''

and CancerSite = ''' + @site + ''''

exec(@sql)





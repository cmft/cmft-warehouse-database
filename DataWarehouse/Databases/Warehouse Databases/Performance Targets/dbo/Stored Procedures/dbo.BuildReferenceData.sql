﻿CREATE proc [dbo].[BuildReferenceData]

as

exec dbo.BuildSpecialtyBase
exec dbo.BuildSiteBase
exec dbo.BuildClinicianBase
exec dbo.BuildServicePointBase
exec dbo.BuildDirectorateBase

insert into dbo.CalendarBase
(
DateID
,TheDate
,DayOfWeek
,DayOfWeekKey
,LongDate
,TheMonth
,MonthName
,FinancialQuarter
,FinancialYear
,FinancialMonthKey
,FinancialQuarterKey
,CalendarQuarter
,CalendarSemester
,CalendarYear
,WeekNoKey
,WeekNo
,FirstDayOfWeek
,LastDayOfWeek
,FullMonthName
)

select
	DateID
	,TheDate
	,DayOfWeek
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,MonthName
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	--,LastCompleteMonth =
	--	case
	--	when exists
	--		(
	--		select
	--			1
	--		from
	--			dbo.Parameter
	--		where
	--			Parameter.Parameter = 'LASTCOMPLETEMONTH'
	--		and	Parameter.DateValue = Calendar.TheDate
	--		)

	--	then 1
	--	else 0
	--	end
	,WeekNoKey
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	,FullMonthName =
		datename(
			 month
			,TheDate
		) + ' ' +
		datename(
			 year
			,TheDate
		)

from
	WarehouseOLAPMergedV2.WH.Calendar
where
	not exists
			(
			select
				1
			from
				dbo.CalendarBase
			where
				Calendar.DateID = CalendarBase.DateID
			)




--insert into dbo.DirectorateBase
--(
--DirectorateID
--,DirectorateCode
--,Directorate
--,Division
--)

--select
--	DirectorateID
--	,DirectorateCode
--	,Directorate
--	,Division = DivisionLabel
--from
--	WarehouseOLAPMergedV2.WH.Directorate
--where
--	not exists
--			(
--			select
--				1
--			from
--				dbo.DirectorateBase
--			where
--				Directorate.DirectorateID = DirectorateBase.DirectorateID
--			)




--insert into dbo.ClinicianBase
--(
--SourceClinicianID
--,SourceClinicianCode
--,SourceClinician
--,SourceContext
--,LocalClinicianID
--,LocalClinicianCode
--,LocalClinician
--,NationalClinicianID
--,NationalClinicianCode
--,NationalClinician
--,SourceContextCode
--,ProviderCode
--,MainSpecialtyCode
--,MainSpecialty
--,Title
--,Initials
--,Forename
--,Surname
--,ClinicanType
--)

--select
--	 SourceClinicianID = Member.SourceValueID
--	,SourceClinicianCode = Member.SourceValueCode
--	,SourceClinician = Member.SourceValue
--	,SourceContext = Member.SourceContext
--	,LocalClinicianID = Member.LocalValueID
--	,LocalClinicianCode = Member.LocalValueCode
--	,LocalClinician = Member.LocalValue
--	,NationalClinicianID = Member.NationalValueID
--	,NationalClinicianCode = coalesce(Member.NationalValueCode,'N/A') -- Placeholder for National code and value
--	,NationalClinician = 
--					coalesce(Member.NationalValueCode,'N/A')
--					+ ' - ' + 
--								(
--								select
--									SourceConsultant
--								from
--									WarehouseOLAPMergedV2.WH.Consultant
--								where
--									Member.NationalValueCode = Consultant.NationalConsultantCode
--								and
--									not exists
--												(
--													select
--														1
--													from
--														WarehouseOLAPMergedV2.WH.Consultant Previous
--													where
--														Consultant.NationalConsultantCode = Previous.NationalConsultantCode
--													and
--														Previous.SourceConsultantID < Consultant.SourceConsultantID
--												)
														
--								)															
--	,SourceContextCode = Member.SourceContextCode
--	,Consultant.ProviderCode
--	,Consultant.MainSpecialtyCode
--	,Consultant.MainSpecialty
--	,Title = coalesce(Consultant.Title, ProfessionalCarer.Title)
--	,Initials = Consultant.Initials
--	,Forename = ProfessionalCarer.Forename
--	,Surname = coalesce(Consultant.Surname, ProfessionalCarer.Surname)
--	,ClinicanType = Member.Attribute
--from
--	WarehouseOLAPMergedV2.WH.Member

--left outer join	WarehouseOLAPMergedV2.WH.Consultant Consultant
--on	Consultant.SourceConsultantID = Member.SourceValueID

--left outer join WarehouseOLAPMergedV2.COM.ProfessionalCarerBase ProfessionalCarer
--on	cast(ProfessionalCarer.ProfessionalCarerID as varchar(255)) = Member.SourceValueCode 

--where
--	Member.AttributeCode in ('CONSUL', 'PROFCARER', 'STAFFM')
--and	not exists
--			(
--			select
--				1
--			from
--				dbo.ClinicianBase
--			where
--				Member.SourceValueID = ClinicianBase.SourceClinicianID
--			)





--insert into dbo.ServicePointBase
--(
--SourceContextCode
--,SourceContext
--,SourceServicePointID
--,SourceServicePointCode
--,SourceServicePoint
--,LocalServicePointID
--,LocalServicePointCode
--,LocalServicePoint
--,NationalServicePointID
--,NationalServicePointCode
--,NationalServicePoint
--,ServicePointType
--)

--select
--	 SourceContextCode
--	,SourceContext
--	,SourceServicePointID
--	,SourceServicePointCode
--	,SourceServicePoint
--	,LocalServicePointID
--	,LocalServicePointCode
--	,LocalServicePoint
--	,NationalServicePointID
--	,NationalServicePointCode
--	,NationalServicePoint
--	,ServicePointType
--from
--	WarehouseOLAPMergedV2.WH.ServicePoint
--where
--	not exists
--			(
--			select
--				1
--			from
--				dbo.ServicePointBase
--			where
--				ServicePoint.SourceServicePointID = ServicePointBase.SourceServicePointID
--			)



--insert into dbo.SiteBase
--(
--SourceContextCode
--,SourceContext
--,SourceSiteID
--,SourceSiteCode
--,SourceSite
--,LocalSiteID
--,LocalSiteCode
--,LocalSite
--,NationalSiteID
--,NationalSiteCode
--,NationalSite
--)

--select 
--	SourceContextCode
--	,SourceContext
--	,SourceSiteID = SourceValueID
--	,SourceSiteCode = SourceValueCode
--	,SourceSite = SourceValue
--	,LocalSiteID = LocalValueID
--	,LocalSiteCode = LocalValueCode
--	,LocalSite = LocalValue
--	,NationalSiteID = NationalValueID
--	,NationalSiteCode = coalesce(NationalValueCode,'N/A') 
--	,NationalSite = coalesce(NationalValue,'N/A')
--from
--	WarehouseOLAPMergedV2.WH.Member
--where
--	AttributeCode in ('SITE','COMLOC')
--and	not exists
--			(
--			select
--				1
--			from
--				dbo.SiteBase
--			where
--				Member.SourceValueID = SiteBase.SourceSiteID
--			)


--insert into dbo.SpecialtyBase
--(
--SourceContextCode
--,SourceContext
--,SourceSpecialtyID
--,SourceSpecialtyCode
--,SourceSpecialty
--,SourceSpecialtyLabel
--,LocalSpecialtyID
--,LocalSpecialtyCode
--,LocalSpecialty
--,NationalSpecialtyID
--,NationalSpecialtyCode
--,NationalSpecialty
--,NationalSpecialtyLabel
--)
--select
--	SourceContextCode
--	,SourceContext
--	,SourceSpecialtyID = SourceValueID
--	,SourceSpecialtyCode = SourceValueCode
--	,SourceSpecialty = SourceValue
--	,SourceSpecialtyLabel = coalesce(SourceValueCode + ' : ' + SourceValue, 'N/A')
--	,LocalSpecialtyID = LocalValueID
--	,LocalSpecialtyCode = LocalValueCode
--	,LocalSpecialty = LocalValue
--	,NationalSpecialtyID = NationalValueID
--	,NationalSpecialtyCode = coalesce(NationalValueCode, 'N/A') -- Placeholder for National code and value
--	,NationalSpecialty = coalesce(NationalValue, 'N/A')
--	,NationalSpecialtyLabel = coalesce(NationalValueCode + ' : ' + NationalValue, 'N/A')
--from
--	WarehouseOLAPMergedV2.WH.Member
--where
--	AttributeCode in ('SPEC','COMSPEC')
--and	not exists
--			(
--			select
--				1
--			from
--				dbo.SpecialtyBase
--			where
--				Member.SourceValueID = SpecialtyBase.SourceSpecialtyID
--			)
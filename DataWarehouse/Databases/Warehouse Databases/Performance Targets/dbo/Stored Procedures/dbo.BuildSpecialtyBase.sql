﻿
create proc [dbo].[BuildSpecialtyBase]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	 dbo.SpecialtyBase target
using
	(
	select
		SourceContextCode
		,SourceContext
		,SourceSpecialtyID = SourceValueID
		,SourceSpecialtyCode = SourceValueCode
		,SourceSpecialty = SourceValue
		,SourceSpecialtyLabel = coalesce(SourceValueCode + ' : ' + SourceValue, 'N/A')
		,LocalSpecialtyID = LocalValueID
		,LocalSpecialtyCode = LocalValueCode
		,LocalSpecialty = LocalValue
		,NationalSpecialtyID = NationalValueID
		,NationalSpecialtyCode = coalesce(NationalValueCode, 'N/A') -- Placeholder for National code and value
		,NationalSpecialty = coalesce(NationalValue, 'N/A')
		,NationalSpecialtyLabel = coalesce(NationalValueCode + ' : ' + NationalValue, 'N/A')
	from
		WarehouseOLAPMergedV2.WH.Member
	where
		AttributeCode in ('SPEC','COMSPEC')

	) source
	on	source.SourceSpecialtyID= target.SourceSpecialtyID

	
	when not matched by source

	then delete

	when not matched
	then
		insert
			(
			SourceContextCode
			,SourceContext
			,SourceSpecialtyID
			,SourceSpecialtyCode
			,SourceSpecialty
			,SourceSpecialtyLabel
			,LocalSpecialtyID
			,LocalSpecialtyCode
			,LocalSpecialty
			,NationalSpecialtyID
			,NationalSpecialtyCode
			,NationalSpecialty
			,NationalSpecialtyLabel
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceContextCode
			,source.SourceContext
			,source.SourceSpecialtyID
			,source.SourceSpecialtyCode
			,source.SourceSpecialty
			,source.SourceSpecialtyLabel
			,source.LocalSpecialtyID
			,source.LocalSpecialtyCode
			,source.LocalSpecialty
			,source.NationalSpecialtyID
			,source.NationalSpecialtyCode
			,source.NationalSpecialty
			,source.NationalSpecialtyLabel
			,getdate()
			,getdate()
			,suser_name()			
			)

	when matched
	and not
		(	
			isnull(target.SourceContextCode, 0) = isnull(source.SourceContextCode, 0)
		and isnull(target.SourceContext, 0) = isnull(source.SourceContext, 0)	
		and	isnull(target.SourceSpecialtyID, 0) = isnull(source.SourceSpecialtyID, 0)	
		and isnull(target.SourceSpecialtyCode, 0) = isnull(source.SourceSpecialtyCode, 0)					
		and isnull(target.SourceSpecialty, 0) = isnull(source.SourceSpecialty, 0)	
		and	isnull(target.SourceSpecialtyLabel, 0) = isnull(source.SourceSpecialtyLabel, 0)
		and	isnull(target.LocalSpecialtyID, 0) = isnull(source.LocalSpecialtyID, 0)	
		and	isnull(target.LocalSpecialtyCode, 0) = isnull(source.LocalSpecialtyCode, 0)
		and	isnull(target.LocalSpecialty, 0) = isnull(source.LocalSpecialty, 0)
		and	isnull(target.NationalSpecialtyID, 0) = isnull(source.NationalSpecialtyID, 0)		
		and isnull(target.NationalSpecialtyCode, 0) = isnull(source.NationalSpecialtyCode, 0)		
		and isnull(target.NationalSpecialty, 0) = isnull(source.NationalSpecialty, 0)
		and isnull(target.NationalSpecialtyLabel, 0) = isnull(source.NationalSpecialtyLabel, 0)

		)
	then
		update
		set
			target.SourceContextCode = source.SourceContextCode
			,target.SourceContext = source.SourceContext
			,target.SourceSpecialtyID = source.SourceSpecialtyID
			,target.SourceSpecialtyCode = source.SourceSpecialtyCode
			,target.SourceSpecialty = source.SourceSpecialty	
			,target.SourceSpecialtyLabel = source.SourceSpecialtyLabel
			,target.LocalSpecialtyID = source.LocalSpecialtyID						
			,target.LocalSpecialtyCode = source.LocalSpecialtyCode
			,target.LocalSpecialty = source.LocalSpecialty	
			,target.NationalSpecialtyID = source.NationalSpecialtyID
			,target.NationalSpecialtyCode = source.NationalSpecialtyCode
			,target.NationalSpecialty = source.NationalSpecialty
			,target.NationalSpecialtyLabel = source.NationalSpecialtyLabel
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime







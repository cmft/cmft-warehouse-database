﻿CREATE procedure [dbo].[BuildCANTTreatedWithin31DaysOfCancerDiagnosisAll] as

exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'CO', 'Colorectal'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'GY', 'Gynaecology'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'HA', 'Haematology'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'HN', 'Head and Neck'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'LU', 'Lung'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'OT', 'Other'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'PA', 'Paediatric'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'SA', 'Sarcoma'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'SK', 'Skin'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'UG', 'Upper GI'
exec BuildCANTTreatedWithin31DaysOfCancerDiagnosis 'UR', 'Urology'
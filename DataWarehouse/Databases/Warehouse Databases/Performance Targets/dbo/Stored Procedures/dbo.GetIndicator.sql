﻿
create proc dbo.GetIndicator
(
	@IndicatorID int = null
)

as

select
	[IndicatorID]
	,[Indicator]
	,[Numerator]
	,[Denominator]
	,[Measure]
	,[IndicatorDirection]
	,[IndicatorStatus]
	,[Factor]
	,[IndicatorDataSource]
	,[Owner]
from
	[dbo].[Indicator]
where
	IndicatorID = @IndicatorID
or	@IndicatorID is null
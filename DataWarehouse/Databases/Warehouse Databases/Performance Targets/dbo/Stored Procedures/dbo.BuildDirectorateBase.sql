﻿
create proc [dbo].[BuildDirectorateBase]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	 dbo.DirectorateBase target
using
	(
	
	select
		DirectorateID
		,DirectorateCode
		,Directorate
		,Division = DivisionLabel
	from
		WarehouseOLAPMergedV2.WH.Directorate

	) source
	on	source.DirectorateID = target.DirectorateID

	
	when not matched by source

	then delete

	when not matched
	then
		insert
			(
			DirectorateID
			,DirectorateCode
			,Directorate
			,Division
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.DirectorateID
			,source.DirectorateCode
			,source.Directorate
			,source.Division
			,getdate()
			,getdate()
			,suser_name()			
			)

	when matched
	and not
		(	
			isnull(target.DirectorateID, 0) = isnull(source.DirectorateID, 0)	
		and isnull(target.DirectorateCode, 0) = isnull(source.DirectorateCode, 0)					
		and isnull(target.Directorate, 0) = isnull(source.Directorate, 0)		
		and isnull(target.Division, 0) = isnull(source.Division, 0)		
		)
	then
		update
		set
			target.DirectorateID = source.DirectorateID
			,target.DirectorateCode = source.DirectorateCode
			,target.Directorate = source.Directorate
			,target.Division = source.Division	
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime







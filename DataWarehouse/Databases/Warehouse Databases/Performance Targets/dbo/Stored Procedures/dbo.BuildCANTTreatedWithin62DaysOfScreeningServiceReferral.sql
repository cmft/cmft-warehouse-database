﻿CREATE procedure [dbo].[BuildCANTTreatedWithin62DaysOfScreeningServiceReferral](@type char(2), @site varchar(100)) as

-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CANT Patient treated < 62 days of screening service referral - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CANT Patient treated < 62 days of screening service referral - ' + @type, 'Numerator.CANTTreatedWithin62DaysOfScreeningServiceReferral' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Denominator record, if it does not exist
declare @DenominatorID int = (select DenominatorID from Denominator where Denominator = 'CANT screening service referral - ' + @type)

if @DenominatorID is null
begin
	insert into Denominator (Denominator, DenominatorLogic)
	values ('CANT screening service referral - ' + @type, 'Denominator.CANTScreeningServiceReferral' + @type)
	select @DenominatorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = 
(select ItemID from Item 
	where Item = 'Percentage of patients receiving first definitive treatment for cancer within 62 days of referral from an NHS Cancer Screening Service - ' + @site)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Percentage of patients receiving first definitive treatment for cancer within 62 days of referral from an NHS Cancer Screening Service - ' + @site
		,2, @NumeratorID, @DenominatorID, 1, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = @DenominatorID where ItemID = @ItemID
end


-- Create view DenominatorCANTScreeningServiceReferral with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Denominator.CANTScreeningServiceReferral' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)
	
declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_SCREEN_TREAT

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = ''' + @view + '''
	
where DatasetCode = ''CANT''
and D.TumourStatusCode not in (''4'', ''5'')
and coalesce(D.TreatmentNo,0) <> 2
and coalesce(D.InappropriateReferralCode,0) <> ''1''
and D.ReceiptOfReferralDate is not null
and D.PriorityTypeCode = ''02'' -- Urgent
and D.SourceForOutpatientCode = ''17'' -- National Screening Programme
and D.PrimaryDiagnosisCode1 not in (''C62'', ''C620'', ''C621'', ''C629'', ''C910'', ''C920'', ''C924'', ''C925'', ''C930'', ''C942'', ''C950'')

and not
(
	coalesce(D.PrimaryDiagnosisCode,'''') = ''C44'' 
	and coalesce(D.HistologyCode,'''') in (''M80903'',''M80913'',''M80923'',''M80933'',''M80943'',''M80953'',''M81103'')
)

and not 
(
	coalesce(D.PrimaryDiagnosisCode,'''') <> ''D05'' 
	and left(D.PrimaryDiagnosisCode,1) = ''D''
)

and 
(
	D.DateOfBirth IS NULL 
	or convert(int,round(datediff(hour,D.DateOfBirth,coalesce(D.DiagnosisDate, GetDate()))/8766,0)) >= 16
)

and CancerSite = ''' + @site + ''''

exec(@sql)


-- Create view NumeratorCANTTreatedWithin62DaysOfScreeningServiceReferral with a suffix for the given CancerTypeCode

set @view = 'Numerator.CANTTreatedWithin62DaysOfScreeningServiceReferral' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)
	
set @sql = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_SCREEN_TREAT

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where DatasetCode = ''CANT''
and D.TumourStatusCode not in (''4'', ''5'')
and coalesce(D.TreatmentNo,0) <> 2
and coalesce(D.InappropriateReferralCode,0) <> ''1''
and D.ReceiptOfReferralDate is not null
and D.PriorityTypeCode = ''02'' -- Urgent
and D.SourceForOutpatientCode = ''17'' -- National Screening Programme
and D.PrimaryDiagnosisCode1 not in (''C62'', ''C620'', ''C621'', ''C629'', ''C910'', ''C920'', ''C924'', ''C925'', ''C930'', ''C942'', ''C950'')

and not
(
	coalesce(D.PrimaryDiagnosisCode,'''') = ''C44'' 
	and coalesce(D.HistologyCode,'''') in (''M80903'',''M80913'',''M80923'',''M80933'',''M80943'',''M80953'',''M81103'')
)

and not 
(
	coalesce(D.PrimaryDiagnosisCode,'''') <> ''D05'' 
	and left(D.PrimaryDiagnosisCode,1) = ''D''
)

and 
(
	D.DateOfBirth IS NULL 
	or convert(int,round(datediff(hour,D.DateOfBirth,coalesce(D.DiagnosisDate, GetDate()))/8766,0)) >= 16
)

and datediff(d,D.ReceiptOfReferralDate,D.TreatmentDate)
- (
	case 
		when D.TreatmentSettingCode in (''01'',''02'') 
		and D.DecisionToTreatAdjustmentReasonCode = ''8'' 
		then coalesce(D.DecisionToTreatAdjustment,0) 
		else 0 
	END
) 
- coalesce(D.FirstAppointmentWaitingTimeAdjusted,0) 
- (
	case 
		when D.FirstAppointmentAdjustmentReasonCode = ''3'' 
		and D.CancelledAppointmentDate IS NOT NULL 
		then coalesce(datediff(d, D.ReceiptOfReferralDate, D.CancelledAppointmentDate), 0) 
		else 0 
	end
)
<= 62


and CancerSite = ''' + @site + ''''

exec(@sql)




﻿CREATE procedure [dbo].[BuildLinkedDQValidationViewAll] as

exec BuildLinkedDQValidationView ValidateCommissioner
exec BuildLinkedDQValidationView ValidateDateOnWaitingList
exec BuildLinkedDQValidationView ValidateDischargeDestination
exec BuildLinkedDQValidationView ValidateDischargeMethod
exec BuildLinkedDQValidationView ValidateEthnicCategory
exec BuildLinkedDQValidationView ValidateGPInvalid
exec BuildLinkedDQValidationView ValidateGPPractice
exec BuildLinkedDQValidationView ValidateGPPracticeInvalid
exec BuildLinkedDQValidationView ValidateHRG
exec BuildLinkedDQValidationView ValidateHRGInvalid
exec BuildLinkedDQValidationView ValidateNHSNumber
exec BuildLinkedDQValidationView ValidateNHSNumberStatus
exec BuildLinkedDQValidationView ValidateOperationStatusInvalid
exec BuildLinkedDQValidationView ValidatePatientTitle
exec BuildLinkedDQValidationView ValidatePCT
exec BuildLinkedDQValidationView ValidatePCTInvalid
exec BuildLinkedDQValidationView ValidatePostcode
exec BuildLinkedDQValidationView ValidatePostcodeInvalid
exec BuildLinkedDQValidationView ValidatePracticeDefault
exec BuildLinkedDQValidationView ValidatePrimaryDiagnosis
exec BuildLinkedDQValidationView ValidatePrimaryDiagnosisInvalid
exec BuildLinkedDQValidationView ValidateReferrer
exec BuildLinkedDQValidationView ValidateReferrerInvalid
exec BuildLinkedDQValidationView ValidateRTTPathwayID
exec BuildLinkedDQValidationView ValidateRTTStatus
exec BuildLinkedDQValidationView ValidateSpecialty
exec BuildLinkedDQValidationView ValidateTreatmentFunctionInvalid

-- Symphony DQ Indicators
exec BuildLinkedDQValidationView ValidateAmbulanceArrivalTime
exec BuildLinkedDQValidationView ValidateAmbulanceInitialAssessmentTime
exec BuildLinkedDQValidationView ValidateAmbulanceRegistrationToAssessment15
exec BuildLinkedDQValidationView ValidateAttendanceConclusionTime
exec BuildLinkedDQValidationView ValidateDepartureTime
exec BuildLinkedDQValidationView ValidateDisposalCode
exec BuildLinkedDQValidationView ValidateDisposalInvalid
exec BuildLinkedDQValidationView ValidateFirstInvestigation
exec BuildLinkedDQValidationView ValidateFirstTreatment
exec BuildLinkedDQValidationView ValidateInitialAssessmentTime
exec BuildLinkedDQValidationView ValidateRegisteredToSeen60
exec BuildLinkedDQValidationView ValidateSeenForTreatmentTime

/*
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Ambulance Arrival Time', 'dbo.NumeratorDQValidateAmbulanceArrivalTime', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Ambulance Initial Assessment Time', 'dbo.NumeratorDQValidateAmbulanceInitialAssessmentTime', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Ambulance Registration To Assessment 15', 'dbo.NumeratorDQValidateAmbulanceRegistrationToAssessment15', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Attendance Conclusion Time', 'dbo.NumeratorDQValidateAttendanceConclusionTime', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Departure Time', 'dbo.NumeratorDQValidateDepartureTime', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Disposal Code', 'dbo.NumeratorDQValidateDisposalCode', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Disposal Invalid', 'dbo.NumeratorDQValidateDisposalInvalid', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate First Investigation', 'dbo.NumeratorDQValidateFirstInvestigation', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate First Treatment', 'dbo.NumeratorDQValidateFirstTreatment', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Initial Assessment Time', 'dbo.NumeratorDQValidateInitialAssessmentTime', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Registered To Seen 60', 'dbo.NumeratorDQValidateRegisteredToSeen60', 0)
insert into Numerator (Numerator, NumeratorLogic, Active) values ('Symphony DQ Validate Seen For Treatment Time', 'dbo.NumeratorDQValidateSeenForTreatmentTime', 0)
*/

﻿CREATE procedure [dbo].[BuildCANPatientDiscussedAtMDT] (@type char(2), @site varchar(100)) as

-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CAN Patient Discussed at MDT - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CAN Patient Discussed at MDT - ' + @type, 'Numerator.CANPatientDiscussedAtMDT' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = (select ItemID from Item where Item = 'Total number of patients discussed at MDT - ' + @site)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Total number of patients discussed at MDT - ' + @site, 2, @NumeratorID, null, 4, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = null where ItemID = @ItemID
end


-- Create view Numerator.CANPatientDiscussedAtMDT with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Numerator.CANPatientDiscussedAtMDT' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)

declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.MDTDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where DatasetCode = ''CAN''
and D.MDTDate is not null -- MDT patients only
and D.WasPatientDiscussedAtMDT = ''Y''

and CancerSite = ''' + @site + ''''

exec(@sql)





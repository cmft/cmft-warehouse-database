﻿CREATE TABLE [dbo].[ItemType] (
    [ItemTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [ItemType]   VARCHAR (50) NULL,
    CONSTRAINT [PK_EntityType] PRIMARY KEY CLUSTERED ([ItemTypeID] ASC)
);


﻿CREATE TABLE [dbo].[CalendarBase] (
    [DateID]              INT           NOT NULL,
    [TheDate]             DATE          NOT NULL,
    [DayOfWeek]           NVARCHAR (30) NULL,
    [DayOfWeekKey]        INT           NULL,
    [LongDate]            VARCHAR (20)  NULL,
    [TheMonth]            NVARCHAR (34) NULL,
    [MonthName]           VARCHAR (3)   NULL,
    [FinancialQuarter]    NVARCHAR (70) NULL,
    [FinancialYear]       NVARCHAR (35) NULL,
    [FinancialMonthKey]   INT           NULL,
    [FinancialQuarterKey] INT           NULL,
    [CalendarQuarter]     VARCHAR (34)  NULL,
    [CalendarSemester]    VARCHAR (12)  NOT NULL,
    [CalendarYear]        NVARCHAR (30) NULL,
    [WeekNoKey]           VARCHAR (50)  NULL,
    [WeekNo]              VARCHAR (50)  NULL,
    [FirstDayOfWeek]      DATE          NULL,
    [LastDayOfWeek]       DATE          NULL,
    [FullMonthName]       NVARCHAR (61) NULL,
    CONSTRAINT [PK__Calendar__A426F253299A4BA3] PRIMARY KEY CLUSTERED ([DateID] ASC)
);


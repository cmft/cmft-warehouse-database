﻿CREATE TABLE [dbo].[ReportType] (
    [ReportTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [ReportType]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ReportType] PRIMARY KEY CLUSTERED ([ReportTypeID] ASC)
);


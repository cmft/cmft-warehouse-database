﻿CREATE TABLE [dbo].[TargetTimeSeries] (
    [TargetTimeSeriesID] INT          IDENTITY (1, 1) NOT NULL,
    [TargetTimeSeries]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TargetTimeSeries] PRIMARY KEY CLUSTERED ([TargetTimeSeriesID] ASC)
);


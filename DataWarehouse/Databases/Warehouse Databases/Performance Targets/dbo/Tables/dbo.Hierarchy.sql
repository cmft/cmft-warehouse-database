﻿CREATE TABLE [dbo].[Hierarchy] (
    [HierarchyID]              INT           IDENTITY (1, 1) NOT NULL,
    [ItemID]                   INT           NULL,
    [ParentID]                 INT           NULL,
    [Weight]                   FLOAT (53)    CONSTRAINT [DF__Hierarchy__Weigh__257187A8] DEFAULT ((1)) NULL,
    [SequenceNo]               INT           CONSTRAINT [DF_Hierarchy_Order] DEFAULT ((1)) NULL,
    [Active]                   BIT           DEFAULT ((1)) NULL,
    [ReportMeasureID]          INT           DEFAULT ((1)) NULL,
    [Highlight]                BIT           NULL,
    [ReportImageID]            INT           NULL,
    [ReportColourID]           INT           NULL,
    [ReportBackgroundColourID] INT           NULL,
    [ReportPriority]           INT           NULL,
    [ReportNavigation]         VARCHAR (MAX) NULL,
    [ReportOrder]              INT           NULL,
    [ReportColumns]            INT           NULL,
    [AmberThresholdUpper]      FLOAT (53)    CONSTRAINT [DF_Hierarchy_AmberThresholdUpperPercent] DEFAULT ((1)) NULL,
    [AmberThresholdLower]      FLOAT (53)    CONSTRAINT [DF_Hierarchy_AmberThresholdLowerPercent] DEFAULT ((0.95)) NULL,
    [TrendTimeSeriesOverride]  BIT           NULL,
    [ReportTypeID]             INT           NULL,
    PRIMARY KEY CLUSTERED ([HierarchyID] ASC)
);


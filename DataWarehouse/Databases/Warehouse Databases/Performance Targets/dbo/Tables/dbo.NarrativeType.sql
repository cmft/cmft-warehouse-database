﻿CREATE TABLE [dbo].[NarrativeType] (
    [NarrativeTypeID]    INT          IDENTITY (1, 1) NOT NULL,
    [NarrativeType]      VARCHAR (50) NOT NULL,
    [NarrativeTypeOrder] INT          NOT NULL,
    CONSTRAINT [PK__Narrativ__D528408803FC509B] PRIMARY KEY CLUSTERED ([NarrativeTypeID] ASC)
);


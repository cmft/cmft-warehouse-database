﻿CREATE TABLE [dbo].[SpecialtyBase] (
    [SourceContextCode]      VARCHAR (20)   NOT NULL,
    [SourceContext]          VARCHAR (100)  NOT NULL,
    [SourceSpecialtyID]      INT            NOT NULL,
    [SourceSpecialtyCode]    VARCHAR (100)  NOT NULL,
    [SourceSpecialty]        VARCHAR (900)  NOT NULL,
    [SourceSpecialtyLabel]   VARCHAR (1003) NOT NULL,
    [LocalSpecialtyID]       INT            NULL,
    [LocalSpecialtyCode]     VARCHAR (50)   NULL,
    [LocalSpecialty]         VARCHAR (200)  NULL,
    [NationalSpecialtyID]    INT            NULL,
    [NationalSpecialtyCode]  VARCHAR (50)   NULL,
    [NationalSpecialty]      VARCHAR (200)  NULL,
    [NationalSpecialtyLabel] VARCHAR (253)  NULL,
    [Created]                DATETIME       NULL,
    [Updated]                DATETIME       NULL,
    [ByWhom]                 VARCHAR (100)  NULL,
    CONSTRAINT [PK__Specialt__31CA60B550B418C4] PRIMARY KEY CLUSTERED ([SourceSpecialtyID] ASC)
);


﻿CREATE TABLE [dbo].[ReportMeasure] (
    [ReportMeasureID] INT          IDENTITY (1, 1) NOT NULL,
    [ReportMeasure]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ReportMeasure] PRIMARY KEY CLUSTERED ([ReportMeasureID] ASC)
);


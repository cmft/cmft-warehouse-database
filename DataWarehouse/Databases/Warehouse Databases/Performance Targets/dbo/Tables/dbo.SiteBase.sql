﻿CREATE TABLE [dbo].[SiteBase] (
    [SourceContextCode] VARCHAR (20)  NOT NULL,
    [SourceContext]     VARCHAR (100) NOT NULL,
    [SourceSiteID]      INT           NOT NULL,
    [SourceSiteCode]    VARCHAR (100) NOT NULL,
    [SourceSite]        VARCHAR (900) NOT NULL,
    [LocalSiteID]       INT           NULL,
    [LocalSiteCode]     VARCHAR (50)  NULL,
    [LocalSite]         VARCHAR (200) NULL,
    [NationalSiteID]    INT           NULL,
    [NationalSiteCode]  VARCHAR (50)  NULL,
    [NationalSite]      VARCHAR (200) NULL,
    [Created]           DATETIME      NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (100) NULL,
    CONSTRAINT [PK__SiteBase__1FDE9D014A071B35] PRIMARY KEY CLUSTERED ([SourceSiteID] ASC)
);


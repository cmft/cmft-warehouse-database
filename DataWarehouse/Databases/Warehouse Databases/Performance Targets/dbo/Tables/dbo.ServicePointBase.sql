﻿CREATE TABLE [dbo].[ServicePointBase] (
    [SourceContextCode]        VARCHAR (20)  NULL,
    [SourceContext]            VARCHAR (100) NULL,
    [SourceServicePointID]     INT           NOT NULL,
    [SourceServicePointCode]   VARCHAR (100) NULL,
    [SourceServicePoint]       VARCHAR (900) NOT NULL,
    [LocalServicePointID]      INT           NULL,
    [LocalServicePointCode]    VARCHAR (50)  NULL,
    [LocalServicePoint]        VARCHAR (200) NULL,
    [NationalServicePointID]   INT           NULL,
    [NationalServicePointCode] VARCHAR (50)  NULL,
    [NationalServicePoint]     VARCHAR (200) NULL,
    [ServicePointType]         VARCHAR (100) NOT NULL,
    [Created]                  DATETIME      NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (100) NULL,
    CONSTRAINT [PK__ServiceP__BAEDA94345426618] PRIMARY KEY CLUSTERED ([SourceServicePointID] ASC)
);


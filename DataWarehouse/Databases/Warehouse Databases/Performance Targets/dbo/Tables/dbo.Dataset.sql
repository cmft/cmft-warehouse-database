﻿CREATE TABLE [dbo].[Dataset] (
    [DatasetID]   INT           IDENTITY (1, 1) NOT NULL,
    [DatasetCode] VARCHAR (10)  NOT NULL,
    [Dataset]     VARCHAR (255) NULL,
    [Active]      BIT           CONSTRAINT [DF__Dataset__Active__28A3C565] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_DQ_Dataset] PRIMARY KEY CLUSTERED ([DatasetID] ASC) WITH (FILLFACTOR = 90)
);


﻿CREATE TABLE [dbo].[ReportColour] (
    [ReportColourID] INT           IDENTITY (1, 1) NOT NULL,
    [ReportColour]   VARCHAR (50)  NOT NULL,
    [Remark]         VARCHAR (MAX) NULL,
    CONSTRAINT [PK_ReportColour] PRIMARY KEY CLUSTERED ([ReportColourID] ASC)
);


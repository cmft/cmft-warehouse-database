﻿CREATE TABLE [dbo].[Owner] (
    [OwnerID] INT           IDENTITY (1, 1) NOT NULL,
    [Owner]   VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Owner] PRIMARY KEY CLUSTERED ([OwnerID] ASC)
);


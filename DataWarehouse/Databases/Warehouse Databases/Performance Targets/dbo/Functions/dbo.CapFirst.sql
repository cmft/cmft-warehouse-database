﻿CREATE FUNCTION dbo.CapFirst(@input NVARCHAR(4000)) RETURNS NVARCHAR(4000)
AS 
BEGIN
DECLARE @position INT
WHILE IsNull(@position,Len(@input)) > 1
SELECT @input = Stuff(@input,IsNull(@position,1),1,upper(substring(@input,IsNull(@position,1),1))), 
@position = charindex(' ',@input,IsNull(@position,1)) + 1
RETURN (@input)
END
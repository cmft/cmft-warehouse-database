﻿






CREATE view [Target].[FinancialExpenditureAnnualBudgetOphthalmology]

as

select
	TargetID = Target.TargetID
	,DateID = WrkDataset.CensusDateID
	,Value = FinanceAnnualBudget

from
	dbo.WrkDataset

inner join dbo.Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

	
inner join dbo.Target
on	Target.TargetLogic = 'Target.FinancialExpenditureAnnualBudgetOphthalmology'

where
	DatasetCode = 'FINANCE'
and EndDirectorateID = 9 -- Ophthalmology -DG - 21.10.14 - Following suit with the other templates set for these indicators: 'Denominator.FinancialExpenditureBudget<Division>
and FinanceAnnualBudget is not null










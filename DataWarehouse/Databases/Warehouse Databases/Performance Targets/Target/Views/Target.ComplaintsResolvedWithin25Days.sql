﻿




CREATE view [Target].[ComplaintsResolvedWithin25Days]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.8
from
	dbo.Calendar	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ComplaintsResolvedWithin25Days'

--where
--	TheDate between '1 apr 2009' and getdate()







﻿CREATE view [Target].[ShelfordGroupComorbiditesAverage]

as


select
	--ItemID = Item.ID 
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.277

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ShelfordGroupComorbiditesAverage'

where
	TheDate between '1 apr 2009' and getdate()
--and
--	datepart(day, TheDate) = 1

﻿

CREATE view [Target].[SummaryHospitalMortalityIndicator]

as


select
	--ItemID = Item.ID 
	TargetID = Target.TargetID
	,DateID 
	,Value = 100

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.SummaryHospitalMortalityIndicator'

where
	TheDate between '1 apr 2009' and getdate()
--and
--	datepart(day, TheDate) = 1



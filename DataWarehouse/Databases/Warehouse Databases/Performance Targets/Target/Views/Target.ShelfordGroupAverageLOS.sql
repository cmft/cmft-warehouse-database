﻿

CREATE view [Target].[ShelfordGroupAverageLOS]

as


select
	--ItemID = Item.ID 
	TargetID = Target.TargetID
	,DateID 
	,Value =	case 
				when Calendar.FinancialYear = '2014/2015' 
				then 4.8
				else 4.7 --'2015/2016' --most up to date unless stated above
				end
	

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ShelfordGroupAverageLOS'

where
	TheDate between '1 apr 2009' and getdate()



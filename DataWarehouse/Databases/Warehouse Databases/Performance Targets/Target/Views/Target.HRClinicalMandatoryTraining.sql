﻿





CREATE view [Target].[HRClinicalMandatoryTraining]

as

select
	TargetID = Target.TargetID
	,DateID 
	,Value = case 
				when TheDate < '1 apr 2015' then 0.8
				else 0.9
			end

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.HRClinicalMandatoryTraining'

--where
--	TheDate between '1 apr 2009' and getdate()








﻿




CREATE view [Target].[QCRTarget]

as

select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.85

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.QCRTarget'






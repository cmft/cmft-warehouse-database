﻿



CREATE view [Target].[CdiffLapseOfCare]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 5.5
	
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CdiffLapseOfCare'

where day(TheDate) = 1

--where
--	TheDate between '1 apr 2009' and getdate()



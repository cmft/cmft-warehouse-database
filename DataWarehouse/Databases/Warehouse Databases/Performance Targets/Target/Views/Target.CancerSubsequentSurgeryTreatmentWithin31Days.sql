﻿

CREATE view [Target].[CancerSubsequentSurgeryTreatmentWithin31Days]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.94

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CancerSubsequentSurgeryTreatmentWithin31Days'

--where
--	TheDate between '1 apr 2009' and getdate()




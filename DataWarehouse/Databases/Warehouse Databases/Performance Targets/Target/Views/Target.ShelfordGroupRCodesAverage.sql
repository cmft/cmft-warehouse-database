﻿CREATE view [Target].[ShelfordGroupRCodesAverage]

as


select
	--ItemID = Item.ID 
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.045

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ShelfordGroupRCodesAverage'

where
	TheDate between '1 apr 2009' and getdate()
--and
--	datepart(day, TheDate) = 1

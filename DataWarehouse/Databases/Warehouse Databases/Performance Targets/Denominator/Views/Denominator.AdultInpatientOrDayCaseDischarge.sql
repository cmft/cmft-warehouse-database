﻿
CREATE view [Denominator].[AdultInpatientOrDayCaseDischarge]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.AdultInpatientOrDayCaseDischarge'

where
	DatasetCode = 'APC'
and
	NationalLastEpisodeInSpellCode = 1
and
	datediff(yy,d.DateOfBirth,d.AdmissionDate) >= 18
and
	PatientCategoryCode in ('DC','EL','NE')



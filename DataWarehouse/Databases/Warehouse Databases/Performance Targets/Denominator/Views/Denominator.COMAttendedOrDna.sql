﻿create view [Denominator].[COMAttendedOrDna]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ContactDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.COMAttendedOrDna'
	
where Dataset.DatasetCode = 'COM'
and d.EncounterOutcomeCode in ('ATTD','DNAT')

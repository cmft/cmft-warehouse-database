﻿CREATE view [Denominator].[NeonatalBabyBornInHospitalNotCooled] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID  
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID
	,Value = 1
	,DischargeDate

from dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.NeonatalBabyBornInHospitalNotCooled'

where DatasetCode = 'NEO'
and upper(left(NeonatalReferralType,6)) = 'INBORN' -- Born in hospital
and DaysCooled = 0 -- Exclude babies who have been cooled

﻿

CREATE view [Denominator].[InpatientAdmissionEligibleForEDD]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID   
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1 
from
	dbo.WrkDataset
inner join
	dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientAdmissionEligibleForEDD'

where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	NationalIntendedManagementCode not in ('3','4')
and
	NationalSpecialtyCode <> '501'
and
	Specialty not like '%DIALYSIS%'
and
	left(SpecialtyCode, 2) <> 'IH'
and
	StartWardCode <> '76A'	
and
	left(StartWardCode, 3) <> 'SUB'
and
	not (
			StartWardCode = 'ESTU' 
			and datediff(hour, AdmissionTime, DischargeTime) < 6
		)



﻿

CREATE view [Denominator].[TurnaroundTime] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.OperationDateID
	,ServicePointID
	,Value = TurnaroundTimeCount

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.TurnaroundTime'
	
where Dataset.DatasetCode = 'OPER'



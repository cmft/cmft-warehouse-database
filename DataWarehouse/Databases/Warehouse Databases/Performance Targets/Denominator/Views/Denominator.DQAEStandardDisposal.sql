﻿CREATE view [Denominator].[DQAEStandardDisposal] as

select 
	DatasetID = v.DatasetID
	,DatasetRecno = v.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EncounterDateID
	,ServicePointID
	,Value = 1

-- Pick up dataset and recno from DQ output table ...
from WarehouseOLAPMergedV2.DQ.DatasetDenominator v

-- ... for relevant logic
inner join WarehouseOLAPMergedV2.DQ.Denominator e
on e.DenominatorID = v.DenominatorID
and e.DenominatorLogic = 'DQ.DenominatorAEStandardDisposal'

-- Join to WrkDataset on DatasetCode and DatasetRecno for remaining required fields
inner join WarehouseOLAPMergedV2.DQ.Dataset ds1 on ds1.DatasetID = v.DatasetID
inner join dbo.Dataset ds2 on ds2.DatasetCode = ds1.DatasetCode

inner join dbo.WrkDataset d on d.DatasetID = ds2.DatasetID
and d.DatasetRecno = v.DatasetRecno

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.DQAEStandardDisposal'

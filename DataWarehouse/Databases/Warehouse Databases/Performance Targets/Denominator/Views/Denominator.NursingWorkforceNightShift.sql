﻿






CREATE view [Denominator].NursingWorkforceNightShift as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.NursingWorkforceNightShift'
	
where
	Dataset.DatasetCode = 'STAFFLEVEL'
and WrkDataset.SourceShift = 'Night'







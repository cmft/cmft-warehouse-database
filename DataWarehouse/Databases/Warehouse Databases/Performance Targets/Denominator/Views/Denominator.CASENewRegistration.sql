﻿






CREATE view [Denominator].[CASENewRegistration] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AllocatedDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset
--with (index(DatasetID_SourcePatientNo))

inner join dbo.Dataset
on	Dataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.CASENewRegistration'
	
where
	Dataset.DatasetCode = 'CASE'
and WrkDataset.NewRegistration = 1

--and not exists -- New Registration = no previously allocated Casenote Number
--(
--	select
--		1
--	from
--		dbo.WrkDataset Earlier
--	where
--		Earlier.DatasetID = WrkDataset.DatasetID
--	and Earlier.SourcePatientNo = WrkDataset.SourcePatientNo
--	and (
--		Earlier.AllocatedDate < WrkDataset.AllocatedDate
--		or (
--				Earlier.AllocatedDate = WrkDataset.AllocatedDate 
--			and	Earlier.DatasetRecno < WrkDataset.DatasetRecno
--			)
--		)
--)







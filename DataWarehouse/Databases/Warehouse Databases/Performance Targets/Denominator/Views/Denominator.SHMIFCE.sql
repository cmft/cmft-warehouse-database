﻿CREATE view [Denominator].[SHMIFCE]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.SHMIFCE'

where
	DatasetCode = 'APC'
and
	DischargeDateID is not null
and
	d.DominantForDiagnosis = 1

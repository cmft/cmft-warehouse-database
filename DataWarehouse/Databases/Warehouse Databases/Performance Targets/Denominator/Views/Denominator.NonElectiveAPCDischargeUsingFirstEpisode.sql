﻿
CREATE view [Denominator].[NonElectiveAPCDischargeUsingFirstEpisode]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset
inner join
	dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.NonElectiveAPCDischargeUsingFirstEpisode'

where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	PatientCategoryCode = 'NE'
and 
	DischargeDate is not null


﻿create view [Denominator].[APCDischargeWithDiabetesAndHeartFailure] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.APCDischargeWithDiabetesAndHeartFailure'

where
	D.NationalLastEpisodeInSpellCode = 1 

and
	Dataset.DatasetCode = 'APC'

and left(D.PrimaryDiagnosisCode,3) = 'I50' -- Heart failure
and
(
	left(D.SecondaryDiagnosisCode1,3) between 'E08' and 'E13' -- Diabetes
	or left(D.SecondaryDiagnosisCode2,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode3,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode4,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode5,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode6,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode7,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode8,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode9,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode10,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode11,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode12,3) between 'E08' and 'E13'
	or left(D.SecondaryDiagnosisCode13,3) between 'E08' and 'E13'			
)

﻿create view [Denominator].[RTTAttendance] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.CensusDateID
	,ServicePointID
	,Value = 1
	,CensusDate
from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.RTTAttendance'

where Dataset.DatasetCode = 'RTT'
and d.PathwayStatusCode = 'OPT'
and d.AdjustedFlag = 'N'

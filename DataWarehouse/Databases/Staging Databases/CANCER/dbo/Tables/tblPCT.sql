﻿CREATE TABLE [dbo].[tblPCT](
	[PCT] [char](3) NULL,
	[PCTNAME] [char](50) NULL,
	[SHA] [char](3) NULL,
	[Search_Word] [char](20) NULL,
	[Active] [bit] NULL CONSTRAINT [DF_tblPCT_Active]  DEFAULT (0)
) ON [PRIMARY]
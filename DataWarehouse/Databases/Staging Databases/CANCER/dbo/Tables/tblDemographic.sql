﻿CREATE TABLE [dbo].[tblDemographic](
	[NHS_NUMBER] [char](20) NULL,
	[CASENOTE] [char](15) NULL,
	[Forename] [char](25) NULL,
	[Surname] [char](30) NULL,
	[DoB] [datetime] NULL,
	[Tumor_Area] [char](25) NULL,
	[Referral_Dummy_Serial] [numeric](19, 0) NULL,
	[Dummy_Serial] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblType_MF](
	[Type_Code] [char](5) NULL,
	[Type_Desc] [char](20) NULL,
	[Type_LDF] [char](1) NULL,
	[Priority_Type] [char](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OEDemog](
	[D1] [char](12) NULL,
	[D2] [char](10) NULL,
	[D3] [char](25) NULL,
	[D4] [char](25) NULL,
	[D5] [datetime] NULL,
	[D6] [char](15) NULL,
	[D7] [char](10) NULL,
	[D8] [char](25) NULL,
	[D9] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[D10] [char](75) NULL,
	[old-demog-serial] [numeric](18, 0) NULL,
	[D11] [numeric](18, 0) NULL
) ON [PRIMARY]
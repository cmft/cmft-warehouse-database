﻿CREATE TABLE [dbo].[OEMDT_Name](
	[MDT_Code] [char](3) NULL,
	[MDT_Name] [char](50) NULL,
	[MDT_Job] [char](50) NULL,
	[MDT_Tel] [char](15) NULL,
	[LDF] [char](1) NULL,
	[Sort_Order] [tinyint] NULL,
	[Dummy_Serial] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
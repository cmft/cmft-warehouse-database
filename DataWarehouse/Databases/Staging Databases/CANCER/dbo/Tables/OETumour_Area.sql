﻿CREATE TABLE [dbo].[OETumour_Area](
	[Tumour_Code] [char](5) NULL,
	[Tumour_Desc] [char](25) NULL,
	[Fast_Track] [char](1) NULL,
	[LDF] [char](1) NULL,
	[Sort_Order] [smallint] NULL,
	[Dummy_Serial] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
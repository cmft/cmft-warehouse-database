﻿CREATE TABLE [dbo].[OE_XRef](
	[T_Sort] [int] NULL,
	[E_Sort] [int] NULL,
	[Table_Name] [char](25) NULL,
	[Field_Name] [char](25) NULL,
	[T_Desc] [char](50) NULL,
	[E_Desc] [char](50) NULL,
	[lookup-table] [char](25) NULL,
	[LDF] [char](1) NULL,
	[XRef_Serial] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
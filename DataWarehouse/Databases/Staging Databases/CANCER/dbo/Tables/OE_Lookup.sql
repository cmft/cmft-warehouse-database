﻿CREATE TABLE [dbo].[OE_Lookup](
	[Type] [char](5) NULL,
	[Desc] [char](110) NULL,
	[Return] [char](5) NULL,
	[Sort_Order] [smallint] NULL,
	[LDF] [char](1) NULL,
	[Dummy_Serial] [smallint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
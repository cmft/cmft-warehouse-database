﻿CREATE TABLE [dbo].[tblINFOCOM_HSCs](
	[Casenote] [char](14) NULL,
	[Patient_Name] [char](30) NULL,
	[RefReasonCode] [char](3) NULL,
	[Cons] [char](6) NULL,
	[Spec] [char](4) NULL,
	[Ref_Date] [datetime] NULL,
	[Comments] [char](50) NULL,
	[DummySerial] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Matched_PTL] [char](1) NULL
) ON [PRIMARY]
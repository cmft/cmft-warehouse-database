﻿CREATE TABLE [dbo].[OE_ICD](
	[ICD] [char](10) NULL,
	[Desc] [char](75) NULL,
	[LDF] [char](1) NULL
) ON [PRIMARY]
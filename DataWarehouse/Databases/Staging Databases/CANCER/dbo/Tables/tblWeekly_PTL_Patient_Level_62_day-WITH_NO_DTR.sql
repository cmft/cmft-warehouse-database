﻿CREATE TABLE [dbo].[tblWeekly_PTL_Patient_Level_62_day-WITH_NO_DTR](
	[Type] [char](5) NULL,
	[SPECIALTY] [char](25) NULL,
	[CASENOTE] [char](15) NULL,
	[NAME] [char](55) NULL,
	[STATUS] [char](10) NULL,
	[GP_DATE] [datetime] NULL,
	[REF_DATE] [datetime] NULL,
	[EFF_REF_DATE] [datetime] NULL,
	[SUSPDAYS] [int] NULL,
	[Decision_Treat_Date] [datetime] NULL,
	[First_Treat_Date] [datetime] NULL,
	[Cat] [char](10) NULL,
	[NOGPD] [tinyint] NULL,
	[NOREF] [tinyint] NULL,
	[BREACH] [tinyint] NULL,
	[TWNOFTD] [tinyint] NULL,
	[TWWFTD] [tinyint] NULL,
	[TWTBR] [tinyint] NULL,
	[NWNOFTD] [tinyint] NULL,
	[NWWFTD] [tinyint] NULL,
	[NWTBR] [tinyint] NULL,
	[FUNOFTD] [tinyint] NULL,
	[FUWFTD] [tinyint] NULL,
	[FUTBR] [tinyint] NULL,
	[TOKND] [tinyint] NULL,
	[TBREACHND] [tinyint] NULL,
	[TOK] [tinyint] NULL,
	[TBREACH] [tinyint] NULL,
	[DUMMY_SERIAL] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Referral_Serial] [numeric](19, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblConsultant](
	[CONS] [char](6) NOT NULL,
	[SURNAME] [char](24) NULL,
	[INIT] [char](6) NULL,
	[TTL] [char](4) NULL,
	[SPEC] [char](4) NULL
) ON [PRIMARY]
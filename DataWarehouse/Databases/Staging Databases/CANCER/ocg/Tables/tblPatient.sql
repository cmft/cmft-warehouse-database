﻿CREATE TABLE [ocg].[tblPatient](
	[P1] [nchar](12) NULL,
	[P2] [nchar](5) NULL,
	[P3] [nchar](5) NULL,
	[P4] [nchar](10) NULL,
	[P5] [nchar](50) NULL,
	[P6] [nchar](50) NULL,
	[P7] [nchar](8) NULL,
	[P8] [nchar](1) NULL,
	[P9] [datetime] NULL,
	[SerialPatient] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
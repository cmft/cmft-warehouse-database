﻿CREATE TABLE [ocg].[tblLookup](
	[Group] [nchar](5) NULL,
	[RetVal] [nchar](10) NULL,
	[Desc] [nchar](150) NULL,
	[SortOrder] [int] NULL,
	[LDF] [nchar](1) NULL,
	[SerialLookup] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[UserType] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
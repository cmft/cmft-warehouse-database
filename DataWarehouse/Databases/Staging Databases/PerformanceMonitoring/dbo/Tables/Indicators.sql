﻿CREATE TABLE [dbo].[Indicators](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IndicatorIdentifier] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[GoalIdentifier] [int] NOT NULL,
	[Weighting] [nvarchar](max) NULL,
	[PrimaryLead] [int] NOT NULL,
	[SecondaryLead] [int] NOT NULL,
	[Area] [int] NOT NULL,
	[GoalId] [int] NOT NULL,
	[YearStart] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
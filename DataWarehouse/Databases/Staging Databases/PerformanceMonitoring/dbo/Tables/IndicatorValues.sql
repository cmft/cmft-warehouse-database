﻿CREATE TABLE [dbo].[IndicatorValues](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Division] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Value] [nvarchar](max) NULL,
	[AnswerType] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Indicator] [int] NOT NULL,
	[MappingId] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
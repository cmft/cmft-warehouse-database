﻿CREATE TABLE [dbo].[SupplementaryDocuments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Division] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Indicator] [int] NOT NULL,
	[BlobData] [varbinary](max) NULL,
	[ContentType] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Goals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GoalIdentifier] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Framework] [int] NOT NULL,
	[YearStart] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
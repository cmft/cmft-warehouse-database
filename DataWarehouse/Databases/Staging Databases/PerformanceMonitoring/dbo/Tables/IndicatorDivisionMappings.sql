﻿CREATE TABLE [dbo].[IndicatorDivisionMappings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IndicatorId] [int] NOT NULL,
	[DivisionId] [int] NOT NULL
) ON [PRIMARY]
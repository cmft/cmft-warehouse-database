﻿CREATE TABLE [dbo].[SystemLogging](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[LogLevel] [varchar](50) NULL,
	[Indicator] [varchar](250) NULL,
	[Month] [varchar](50) NULL,
	[Year] [varchar](50) NULL,
	[Message] [varchar](250) NULL,
	[UserName] [varchar](250) NULL,
	[OldValue] [varchar](50) NULL,
	[NewValue] [varchar](50) NULL
) ON [PRIMARY]
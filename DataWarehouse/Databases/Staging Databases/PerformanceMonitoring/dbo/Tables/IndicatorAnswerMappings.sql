﻿CREATE TABLE [dbo].[IndicatorAnswerMappings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IndicatorId] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[AnswerType] [int] NOT NULL,
	[FullQuestionText] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
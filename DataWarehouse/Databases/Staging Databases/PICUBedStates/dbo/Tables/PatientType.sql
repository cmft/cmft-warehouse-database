﻿CREATE TABLE [dbo].[PatientType](
	[PatientTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserAudit](
	[UserAuditID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[UnitID] [int] NULL,
	[Type] [int] NULL,
	[AuditDate] [datetime] NULL,
	[SuperUserID] [int] NULL,
	[DateAllocated] [datetime] NULL,
	[Active] [tinyint] NULL,
	[UserName] [nvarchar](20) NULL,
	[Password] [nvarchar](12) NULL,
	[ExpiryDate] [datetime] NULL,
	[GraceLogins] [int] NULL,
	[Edited] [tinyint] NULL,
	[Deleted] [tinyint] NULL,
	[Logins] [int] NULL,
	[PwdChange] [tinyint] NULL
) ON [PRIMARY]
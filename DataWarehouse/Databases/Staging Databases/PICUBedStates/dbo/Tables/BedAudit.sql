﻿CREATE TABLE [dbo].[BedAudit](
	[UnitID] [int] NULL,
	[BedAuditID] [int] IDENTITY(1,1) NOT NULL,
	[AuditDate] [datetime] NULL,
	[BedID] [int] NULL,
	[UserID] [int] NULL,
	[Code] [varchar](20) NULL,
	[Active] [tinyint] NULL,
	[Status] [int] NULL,
	[OperationID] [int] NULL,
	[Dependency] [int] NULL,
	[Edited] [tinyint] NULL,
	[Deleted] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ECMO](
	[ECMOID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[BedID] [int] NOT NULL CONSTRAINT [DF_ECMO_BedID]  DEFAULT (2),
	[CurrentStatus] [int] NOT NULL,
	[BedAuditID] [int] NOT NULL
) ON [PRIMARY]
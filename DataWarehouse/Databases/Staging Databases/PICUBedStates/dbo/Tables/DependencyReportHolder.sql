﻿CREATE TABLE [dbo].[DependencyReportHolder](
	[DependencyReportHolderId] [int] IDENTITY(1,1) NOT NULL,
	[UnitId] [int] NULL,
	[DayStart] [datetime] NULL,
	[Dep1] [int] NULL,
	[Dep2] [int] NULL,
	[Dep3] [int] NULL,
	[Dep4] [int] NULL,
	[amMax] [money] NULL,
	[pmMax] [money] NULL,
	[dMin] [int] NULL,
	[dMax] [int] NULL
) ON [PRIMARY]
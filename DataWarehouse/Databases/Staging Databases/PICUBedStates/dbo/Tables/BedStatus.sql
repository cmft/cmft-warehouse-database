﻿CREATE TABLE [dbo].[BedStatus](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](35) NULL,
	[Indicator] [nvarchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DailyDischargeDates](
	[DischargeDateID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[AvgDelay] [bigint] NOT NULL
) ON [PRIMARY]
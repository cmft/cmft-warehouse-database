﻿CREATE TABLE [dbo].[Unit](
	[UnitID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[PatientType] [int] NULL,
	[Phone] [nvarchar](23) NULL,
	[Fax] [nvarchar](23) NULL,
	[Hospital] [nvarchar](30) NULL,
	[Address1] [nvarchar](35) NULL,
	[Address2] [nvarchar](35) NULL,
	[Address3] [nvarchar](35) NULL,
	[Address4] [nvarchar](35) NULL,
	[Postcode] [nvarchar](10) NULL,
	[WebPage] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
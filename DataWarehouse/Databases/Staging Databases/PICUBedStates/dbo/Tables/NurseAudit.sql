﻿CREATE TABLE [dbo].[NurseAudit](
	[NurseAuditID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NULL,
	[AuditDate] [datetime] NULL,
	[UserID] [int] NULL,
	[Complement] [int] NULL,
	[Not Available] [int] NULL
) ON [PRIMARY]
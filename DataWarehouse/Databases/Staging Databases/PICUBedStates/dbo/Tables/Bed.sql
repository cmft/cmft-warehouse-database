﻿CREATE TABLE [dbo].[Bed](
	[UnitID] [int] NULL,
	[BedID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Active] [tinyint] NULL,
	[CurrentStatus] [int] NULL,
	[OperationID] [int] NULL,
	[Dependency] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CapacityReportHolder](
	[CapacityReportHolderId] [int] IDENTITY(1,1) NOT NULL,
	[UnitId] [int] NULL,
	[MonthStart] [datetime] NULL,
	[OverCapacity] [int] NULL
) ON [PRIMARY]
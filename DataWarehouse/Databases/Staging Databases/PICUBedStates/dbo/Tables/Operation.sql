﻿CREATE TABLE [dbo].[Operation](
	[OperationID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](35) NULL,
	[ShownDesc] [nvarchar](35) NULL,
	[CurrentStatus] [int] NOT NULL,
	[NewStatus] [int] NOT NULL
) ON [PRIMARY]
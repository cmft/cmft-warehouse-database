﻿CREATE TABLE [dbo].[BedBAK](
	[UnitID] [int] NULL,
	[BedID] [int] NOT NULL,
	[Code] [nvarchar](4) NULL,
	[Active] [tinyint] NULL,
	[CurrentStatus] [int] NULL,
	[OperationID] [int] NULL,
	[Dependency] [int] NULL
) ON [PRIMARY]
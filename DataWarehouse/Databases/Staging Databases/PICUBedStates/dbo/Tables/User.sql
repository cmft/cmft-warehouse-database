﻿CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NULL,
	[Type] [int] NULL,
	[DateAllocated] [datetime] NULL,
	[Active] [tinyint] NULL,
	[UserName] [nvarchar](20) NULL,
	[Password] [nvarchar](12) NULL,
	[ExpiryDate] [datetime] NULL,
	[GraceLogins] [int] NULL,
	[Logins] [int] NULL,
	[PwdChange] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DischargeDates](
	[DischargeID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[BedID] [int] NOT NULL,
	[AuditDate] [datetime] NOT NULL CONSTRAINT [DF_DischargeDates_AuditDate]  DEFAULT (getdate()),
	[CurrentStatus] [smallint] NOT NULL,
	[Timelapse] [nvarchar](50) NOT NULL,
	[Discharged] [bit] NOT NULL CONSTRAINT [DF_DischargeDates_Discharged]  DEFAULT (0),
	[PairValue] [int] NOT NULL CONSTRAINT [DF_DischargeDates_PairValue]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Dependency](
	[DependencyID] [int] IDENTITY(1,1) NOT NULL,
	[DependencyLevel] [tinyint] NULL,
	[Dependency] [float] NULL,
	[DependencyControl] [float] NULL,
	[Description] [nvarchar](35) NULL
) ON [PRIMARY]
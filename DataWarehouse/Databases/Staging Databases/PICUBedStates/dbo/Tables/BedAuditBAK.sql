﻿CREATE TABLE [dbo].[BedAuditBAK](
	[UnitID] [int] NULL,
	[BedAuditID] [int] NOT NULL,
	[AuditDate] [datetime] NULL,
	[BedID] [int] NULL,
	[UserID] [int] NULL,
	[Code] [nvarchar](4) NULL,
	[Active] [tinyint] NULL,
	[Status] [int] NULL,
	[OperationID] [int] NULL,
	[Dependency] [int] NULL,
	[Edited] [tinyint] NULL,
	[Deleted] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_cloud].[tblWorkstationApplicationVersions](
	[WorkstationID] [uniqueidentifier] NOT NULL,
	[ApplicationName] [nvarchar](250) NOT NULL,
	[Version] [nvarchar](50) NOT NULL,
	[LastUpdated] [datetime] NOT NULL
) ON [PRIMARY]
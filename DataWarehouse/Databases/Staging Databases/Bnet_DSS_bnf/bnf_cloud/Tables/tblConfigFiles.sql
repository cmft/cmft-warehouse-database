﻿CREATE TABLE [bnf_cloud].[tblConfigFiles](
	[fileName] [nvarchar](200) NOT NULL,
	[fileType] [nvarchar](10) NOT NULL,
	[version] [int] NOT NULL,
	[comment] [nvarchar](4000) NULL,
	[uploadTime] [datetime] NOT NULL,
	[fromComputer] [nvarchar](200) NULL,
	[machineUser] [nvarchar](200) NULL,
	[fileContent] [varbinary](max) NULL,
	[uniqueId] [uniqueidentifier] NOT NULL,
	[isActive] [bit] NOT NULL,
	[originalFileSize] [bigint] NOT NULL,
	[compressedFileSize] [bigint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
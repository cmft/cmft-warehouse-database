﻿CREATE TABLE [bnf_cloud].[tblUserAccountCredentialsHistory](
	[UserCredentialsHistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblUserAccountCredentialsHistory_UserCredentialsHistoryId]  DEFAULT (newid()),
	[UserId] [uniqueidentifier] NOT NULL,
	[PasswordSalt] [nvarchar](100) NOT NULL,
	[PasswordHash] [nchar](40) NOT NULL,
	[ReplacedDatestamp] [datetime] NOT NULL CONSTRAINT [DF_tblUserAccountCredentialsHistory_ReplacedDatestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
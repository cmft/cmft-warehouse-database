﻿CREATE TABLE [bnf_cloud].[tblLocations](
	[LocationID] [uniqueidentifier] NOT NULL,
	[LocationName] [nvarchar](200) NOT NULL,
	[EntityReaderGroupID] [uniqueidentifier] NOT NULL,
	[EntityWriterGroupID] [uniqueidentifier] NOT NULL,
	[PowerUserGroupID] [uniqueidentifier] NOT NULL,
	[ReportUserGroupID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_cloud].[tblSessions](
	[SessionID] [uniqueidentifier] NOT NULL,
	[SessionPassword] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblSessions_SessionPassword]  DEFAULT (newid()),
	[UserID] [uniqueidentifier] NOT NULL,
	[StartTime] [datetime] NOT NULL CONSTRAINT [DF_tblSessions_StartTime]  DEFAULT (getutcdate()),
	[LoginIPAddress] [nvarchar](60) NOT NULL,
	[LastActivityTime] [datetime] NOT NULL,
	[SignedOut] [bit] NOT NULL,
	[WorkstationID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblSessions_WorkstationID]  DEFAULT ('{00000000-0000-0000-0000-000000000000}')
) ON [PRIMARY]
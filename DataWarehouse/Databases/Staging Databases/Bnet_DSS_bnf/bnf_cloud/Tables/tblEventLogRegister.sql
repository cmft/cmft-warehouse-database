﻿CREATE TABLE [bnf_cloud].[tblEventLogRegister](
	[EventLogName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[InsertProcedureName] [nvarchar](250) NULL,
	[SearchProcedureName] [nvarchar](250) NULL,
	[SelectProcedureName] [nvarchar](250) NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_cloud].[tblUserFingerprints](
	[UserID] [uniqueidentifier] NOT NULL,
	[FingerIndex] [int] NOT NULL,
	[FingerprintBlob] [varbinary](max) NOT NULL,
	[SDKUsed] [nvarchar](50) NULL,
	[DateRegistered] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
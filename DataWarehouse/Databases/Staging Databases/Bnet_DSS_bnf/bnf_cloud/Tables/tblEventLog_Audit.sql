﻿CREATE TABLE [bnf_cloud].[tblEventLog_Audit](
	[LogEntryID] [uniqueidentifier] NOT NULL,
	[EntryTime] [datetime] NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[WorkstationID] [uniqueidentifier] NULL,
	[EventSource] [nvarchar](50) NOT NULL,
	[EventSummary] [nvarchar](2000) NOT NULL,
	[EventContent] [xml] NOT NULL,
	[Severity] [int] NOT NULL,
	[TargetID] [uniqueidentifier] NULL,
	[TargetType] [nvarchar](50) NULL,
	[TargetLabel] [nvarchar](250) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
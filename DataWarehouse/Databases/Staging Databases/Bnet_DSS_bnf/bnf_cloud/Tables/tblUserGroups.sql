﻿CREATE TABLE [bnf_cloud].[tblUserGroups](
	[GroupID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblUserGroups_GroupID]  DEFAULT (newid()),
	[GroupName] [nvarchar](250) NOT NULL,
	[Descriptor] [nvarchar](max) NULL,
	[Virtual] [bit] NOT NULL CONSTRAINT [DF_tblUserGroups_Sealed]  DEFAULT ((0)),
	[CompressedDOM] [varbinary](max) NULL,
	[LastPdupID] [uniqueidentifier] NULL,
	[RawDOM] [xml] NULL,
	[RawDOMPdupID] [uniqueidentifier] NULL,
	[LastUpdate] [datetime] NULL CONSTRAINT [DF_tblUserGroups_LastUpdate]  DEFAULT (getutcdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
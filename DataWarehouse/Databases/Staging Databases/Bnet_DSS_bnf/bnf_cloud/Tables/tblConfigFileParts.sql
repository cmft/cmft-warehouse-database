﻿CREATE TABLE [bnf_cloud].[tblConfigFileParts](
	[fileName] [nvarchar](200) NOT NULL,
	[fileType] [nvarchar](10) NOT NULL,
	[uploadTime] [datetime] NOT NULL,
	[partContent] [varbinary](max) NULL,
	[uniqueId] [uniqueidentifier] NOT NULL,
	[chunkSize] [int] NOT NULL,
	[chunkId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
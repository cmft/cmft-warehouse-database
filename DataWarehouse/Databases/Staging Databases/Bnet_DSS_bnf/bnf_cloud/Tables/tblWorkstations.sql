﻿CREATE TABLE [bnf_cloud].[tblWorkstations](
	[WorkstationID] [uniqueidentifier] NOT NULL,
	[WorkstationName] [nvarchar](250) NOT NULL,
	[Location] [nvarchar](250) NULL,
	[ContactDetails] [nvarchar](500) NULL,
	[Notes] [ntext] NULL,
	[RoomID] [nvarchar](200) NULL,
	[BedID] [nvarchar](200) NULL,
	[AttributeList] [nvarchar](max) NULL,
	[SystemDiagnostics] [xml] NULL,
	[Disabled] [bit] NOT NULL CONSTRAINT [DF_tblWorkstations_Disabled]  DEFAULT ((0)),
	[LastQueried] [datetime] NOT NULL CONSTRAINT [DF_tblWorkstations_RegistrationLastQueried]  DEFAULT (getutcdate()),
	[LastUpdated] [datetime] NOT NULL CONSTRAINT [DF_tblWorkstations_RegistrationLastUpdated]  DEFAULT (getutcdate()),
	[Created] [datetime] NOT NULL CONSTRAINT [DF_tblWorkstations_RegistrationCreated]  DEFAULT (getutcdate()),
	[SourceIPAddress] [nvarchar](50) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
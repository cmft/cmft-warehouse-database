﻿CREATE TABLE [bnf_cloud].[tblServiceStatistics](
	[CloudID] [uniqueidentifier] NOT NULL,
	[ServiceID] [nvarchar](200) NOT NULL,
	[CloudName] [nvarchar](200) NOT NULL,
	[StatsXml] [nvarchar](max) NOT NULL,
	[StatsVersionID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblServiceStatistics_StartsVersionID]  DEFAULT (newid()),
	[LastUpdated] [datetime] NOT NULL CONSTRAINT [DF_tblServiceStatistics_LastUpdated]  DEFAULT (getutcdate()),
	[TimeReceivedFromRemoteCloud] [datetime] NULL,
	[TimeLastPosted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
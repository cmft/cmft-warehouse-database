﻿CREATE TABLE [bnf_cloud].[tblGroupMembershipOfGroups](
	[MemberGroupID] [uniqueidentifier] NOT NULL,
	[ParentGroupID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
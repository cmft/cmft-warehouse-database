﻿CREATE TABLE [bnf_cloud].[tblXferChunks](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ChunkID] [uniqueidentifier] NOT NULL,
	[ChunkData] [varbinary](max) NOT NULL,
	[expires] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
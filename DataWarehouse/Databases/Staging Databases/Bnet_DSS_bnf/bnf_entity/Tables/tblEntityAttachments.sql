﻿CREATE TABLE [bnf_entity].[tblEntityAttachments](
	[AttachmentID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblEntityAttachments_AttachmentID]  DEFAULT (newid()),
	[EntityID] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[MimeType] [nvarchar](250) NOT NULL,
	[Blob] [varbinary](max) NOT NULL,
	[FileSize] [int] NOT NULL,
	[Filename] [nvarchar](250) NULL,
	[FileExtension] [nvarchar](50) NULL,
	[Comment] [nvarchar](max) NULL,
	[Categories] [nvarchar](1000) NULL,
	[CreatedByUserName] [nvarchar](200) NULL,
	[CreatedByUserID] [uniqueidentifier] NULL,
	[CreatedOnWorkstationID] [uniqueidentifier] NULL,
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_tblEntityAttachments_CreatedDateTime]  DEFAULT (getutcdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
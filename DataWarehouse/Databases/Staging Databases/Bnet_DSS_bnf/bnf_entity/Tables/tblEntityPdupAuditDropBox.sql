﻿CREATE TABLE [bnf_entity].[tblEntityPdupAuditDropBox](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DateTimestamp] [datetime] NOT NULL DEFAULT (getutcdate()),
	[targetCloud] [nvarchar](200) NOT NULL,
	[PdupXml] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
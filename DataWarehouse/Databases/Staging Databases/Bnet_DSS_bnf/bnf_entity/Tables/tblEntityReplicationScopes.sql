﻿CREATE TABLE [bnf_entity].[tblEntityReplicationScopes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CloudID] [uniqueidentifier] NOT NULL,
	[CloudName] [nvarchar](200) NOT NULL,
	[Scope] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
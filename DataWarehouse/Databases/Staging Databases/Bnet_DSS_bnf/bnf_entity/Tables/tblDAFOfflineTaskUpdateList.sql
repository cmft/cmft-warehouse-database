﻿CREATE TABLE [bnf_entity].[tblDAFOfflineTaskUpdateList](
	[DAFCloudTaskID] [int] NOT NULL,
	[EntityID] [uniqueidentifier] NOT NULL,
	[lastEntityUpdate] [datetime] NOT NULL,
	[created] [datetime] NOT NULL
) ON [PRIMARY]
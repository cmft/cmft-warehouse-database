﻿CREATE TABLE [bnf_entity].[tblEntitySQLCLRLogging](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventTimestamp] [datetime] NOT NULL DEFAULT (getutcdate()),
	[EventSource] [nvarchar](50) NOT NULL,
	[EventID] [int] NOT NULL,
	[Summary] [nvarchar](250) NOT NULL,
	[EventXml] [nvarchar](max) NOT NULL,
	[Severity] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
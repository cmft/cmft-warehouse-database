﻿CREATE TABLE [bnf_entity].[tblDAFTaskRequests](
	[TaskRequestID] [uniqueidentifier] NOT NULL,
	[TaskDefinition] [xml] NOT NULL,
	[RequestedTimestamp] [datetime] NOT NULL,
	[RequestedBy] [uniqueidentifier] NULL,
	[SourceWorkstationID] [uniqueidentifier] NULL,
	[ExecuteStartTimestamp] [datetime] NULL,
	[ExecuteEndTimestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
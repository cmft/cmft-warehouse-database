﻿CREATE TABLE [bnf_entity].[tblDAFCloudTaskRegister](
	[DAFCloudTaskID] [int] IDENTITY(1,1) NOT NULL,
	[DAFServerID] [int] NOT NULL,
	[DAFTaskID] [nvarchar](100) NOT NULL,
	[EntityType] [nvarchar](100) NULL,
	[SubType] [nvarchar](100) NULL,
	[Offline] [bit] NOT NULL CONSTRAINT [DF_tblDAFCloudTaskRegister_Offline]  DEFAULT ((0))
) ON [PRIMARY]
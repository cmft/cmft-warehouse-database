﻿CREATE TABLE [bnf_entity].[tblEntitySQLCLRSettings](
	[SettingName] [nvarchar](200) NOT NULL,
	[SettingValue] [nvarchar](max) NOT NULL,
	[SettingIntValue] [int] NOT NULL CONSTRAINT [DF_tblEntitySQLCLRSettings_SettingIntValue]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
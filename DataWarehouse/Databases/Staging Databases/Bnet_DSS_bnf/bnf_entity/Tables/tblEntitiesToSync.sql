﻿CREATE TABLE [bnf_entity].[tblEntitiesToSync](
	[EntityID] [uniqueidentifier] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[domHash] [nvarchar](100) NULL
) ON [PRIMARY]
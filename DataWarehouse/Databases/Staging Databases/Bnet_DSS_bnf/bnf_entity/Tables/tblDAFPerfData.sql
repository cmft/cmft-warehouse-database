﻿CREATE TABLE [bnf_entity].[tblDAFPerfData](
	[PerfItemID] [int] IDENTITY(1,1) NOT NULL,
	[created] [datetime] NOT NULL,
	[iParam1] [int] NOT NULL,
	[iParam2] [int] NULL,
	[iParam3] [int] NULL,
	[sParam1] [nvarchar](200) NULL,
	[sParam2] [nvarchar](200) NULL,
	[sParam3] [nvarchar](200) NULL
) ON [PRIMARY]
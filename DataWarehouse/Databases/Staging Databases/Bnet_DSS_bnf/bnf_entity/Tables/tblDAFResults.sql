﻿CREATE TABLE [bnf_entity].[tblDAFResults](
	[EntityID] [uniqueidentifier] NOT NULL,
	[DAFID] [nvarchar](100) NOT NULL,
	[LocationID] [uniqueidentifier] NULL,
	[LastPdupID] [uniqueidentifier] NOT NULL,
	[CalculatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_tblDataAnalysisResults_CalculatedDateTime]  DEFAULT (getutcdate()),
	[ExpiryDateTime] [datetime] NULL CONSTRAINT [DF_tblDataAnalysisResults_ExpiryDateTime]  DEFAULT (getutcdate()),
	[ExpireOnEntityChange] [bit] NOT NULL CONSTRAINT [DF_tblDataAnalysisResults_KeepInSyncWithEntity]  DEFAULT ((1)),
	[ResultInt1] [int] NULL,
	[ResultInt2] [int] NULL,
	[ResultInt3] [int] NULL,
	[ResultSummary] [nvarchar](500) NULL,
	[ResultDetail] [xml] NULL,
	[GenerationError] [bit] NOT NULL CONSTRAINT [DF_tblDAFResults_GenerationError]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
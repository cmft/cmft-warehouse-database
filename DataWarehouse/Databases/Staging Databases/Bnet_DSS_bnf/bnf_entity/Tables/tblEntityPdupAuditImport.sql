﻿CREATE TABLE [bnf_entity].[tblEntityPdupAuditImport](
	[PdupID] [uniqueidentifier] NOT NULL,
	[Result] [nchar](10) NULL,
	[TargetDB] [nvarchar](100) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_entity].[tblEntityPdupsFailures](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DateTimestamp] [datetime] NOT NULL DEFAULT (getutcdate()),
	[PdupXml] [nvarchar](max) NOT NULL,
	[ErrorMsg] [nvarchar](max) NULL,
	[stage] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
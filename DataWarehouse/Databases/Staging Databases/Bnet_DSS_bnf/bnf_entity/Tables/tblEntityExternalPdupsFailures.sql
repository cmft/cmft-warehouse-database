﻿CREATE TABLE [bnf_entity].[tblEntityExternalPdupsFailures](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DateTimestamp] [datetime] NOT NULL,
	[PdupXml] [nvarchar](max) NOT NULL,
	[ErrorMsg] [nvarchar](max) NULL,
	[stage] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
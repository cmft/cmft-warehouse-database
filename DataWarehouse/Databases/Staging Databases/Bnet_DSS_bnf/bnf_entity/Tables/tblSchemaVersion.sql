﻿CREATE TABLE [bnf_entity].[tblSchemaVersion](
	[SchemaVersion] [int] NOT NULL,
	[ReleaseDate] [datetime] NOT NULL,
	[Comments] [nvarchar](500) NULL
) ON [PRIMARY]
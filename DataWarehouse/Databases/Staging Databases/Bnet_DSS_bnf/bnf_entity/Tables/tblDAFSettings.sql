﻿CREATE TABLE [bnf_entity].[tblDAFSettings](
	[ScheduledTaskID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduledTaskName] [nvarchar](200) NOT NULL,
	[DAFServerID] [int] NOT NULL,
	[taskStrXML] [nvarchar](max) NOT NULL,
	[sInterval] [xml] null, --AS ([bnf_entity].[ufGetTaskScheduleType]([taskStrXML])),
	[sAction] [xml] null , --AS ([bnf_entity].[ufGetTaskActionType]([taskStrXML])),
	[sTaskName] [xml] null, --  AS ([bnf_entity].[ufGetTaskName]([taskStrXML])),
	[bDisabled] [xml] null --  AS ([bnf_entity].[ufGetIsDisabled]([taskStrXML]))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
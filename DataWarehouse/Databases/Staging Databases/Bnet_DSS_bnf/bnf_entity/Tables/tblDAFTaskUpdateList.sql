﻿CREATE TABLE [bnf_entity].[tblDAFTaskUpdateList](
	[DAFCloudTaskID] [int] NOT NULL,
	[EntityID] [uniqueidentifier] NOT NULL,
	[lastEntityUpdate] [datetime] NOT NULL,
	[created] [datetime] NOT NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[InProgress] [bit] NOT NULL,
	[ProcStarted] [datetime] NULL
) ON [PRIMARY]
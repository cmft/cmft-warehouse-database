﻿CREATE TABLE [bnf_entity].[tblDAFInstances](
	[DAFServerID] [int] IDENTITY(1,1) NOT NULL,
	[DAFServerName] [nvarchar](100) NOT NULL,
	[DAFServerPath] [nvarchar](500) NOT NULL,
	[bRestartFlag] [bit] NOT NULL CONSTRAINT [DF_tblDAFInstances_bRestart]  DEFAULT ((0))
) ON [PRIMARY]
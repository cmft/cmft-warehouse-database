﻿CREATE TABLE [bnf_entity].[tblEntityPdupAudit](
	[PdupID] [uniqueidentifier] NOT NULL,
	[EntityID] [uniqueidentifier] NOT NULL,
	[Datestamp] [datetime] NOT NULL,
	[PdupXml] [xml] NOT NULL,
	[DIGID] [varchar](200) NULL,
	[WorkstationID] [uniqueidentifier] NULL,
	[UserID] [uniqueidentifier] NULL,
	[ID] [bigint] IDENTITY(30000000,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [bnf_entity].[tblDAFTaskEntityTypes](
	[DAFCloudTaskID] [int] NOT NULL,
	[EntityType] [nvarchar](100) NOT NULL,
	[SubType] [nvarchar](100) NOT NULL
) ON [PRIMARY]
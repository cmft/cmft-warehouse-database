﻿CREATE TABLE [bnf_entity].[tblEntities](
	[EntityID] [uniqueidentifier] NOT NULL,
	[EntityType] [nvarchar](100) NOT NULL,
	[SubType] [nvarchar](100) NULL,
	[LocationID] [uniqueidentifier] NULL,
	[LastPdupID] [uniqueidentifier] NOT NULL,
	[LastUpdate] [datetime] NULL,
	[CompressedDOM] [varbinary](max) NOT NULL,
	[DeletedTimestamp] [datetime] NULL,
	[EntityLabel1] [nvarchar](500) NULL,
	[EntityLabel2] [nvarchar](500) NULL,
	[EntityLabel3] [nvarchar](500) NULL,
	[eID] [bigint] IDENTITY(1,1) NOT NULL,
	[RowVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
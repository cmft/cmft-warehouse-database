﻿CREATE TABLE [bnf_entity].[tblEntityReplicationSubscriptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CloudID] [uniqueidentifier] NOT NULL,
	[EntityID] [uniqueidentifier] NOT NULL,
	[LastPdupID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
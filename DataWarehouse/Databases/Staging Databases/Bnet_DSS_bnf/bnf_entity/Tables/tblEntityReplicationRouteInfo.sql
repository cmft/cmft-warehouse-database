﻿CREATE TABLE [bnf_entity].[tblEntityReplicationRouteInfo](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[RouteType] [nvarchar](5) NOT NULL,
	[CloudName] [nvarchar](200) NOT NULL,
	[CloudID] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](4000) NULL,
	[RemoteCloudURL] [nvarchar](250) NULL,
	[RemoteCloudUsername] [nvarchar](50) NULL,
	[RemoteCloudPassword] [nvarchar](50) NULL,
	[RemoteCloudPSK] [nvarchar](50) NULL,
	[PDUPCOUNT] [int] NULL,
	[Info] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
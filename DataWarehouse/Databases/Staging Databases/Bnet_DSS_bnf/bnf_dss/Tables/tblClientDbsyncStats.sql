﻿CREATE TABLE [bnf_dss].[tblClientDbsyncStats](
	[ClientID] [uniqueidentifier] NOT NULL,
	[TableName] [nvarchar](255) NOT NULL,
	[CareLocation] [uniqueidentifier] NOT NULL,
	[RemoteCount] [int] NOT NULL,
	[MasterCount] [int] NOT NULL
) ON [PRIMARY]
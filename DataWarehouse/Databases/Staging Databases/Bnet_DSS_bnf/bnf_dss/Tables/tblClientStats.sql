﻿CREATE TABLE [bnf_dss].[tblClientStats](
	[ClientID] [uniqueidentifier] NOT NULL,
	[LastHitTime] [datetime] NOT NULL,
	[CareLocation] [uniqueidentifier] NOT NULL,
	[EntityType] [varchar](255) NOT NULL,
	[SubType] [varchar](255) NOT NULL,
	[RemoteEntityCount] [int] NOT NULL,
	[MasterEntityCount] [int] NOT NULL,
	[lastEntityUpdated] [datetime] NULL,
	[dafGenErrorCount] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_dss].[tblBuildRequest](
	[BuildID] [uniqueidentifier] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[WorkstationID] [uniqueidentifier] NOT NULL,
	[DateRequested] [datetime] NOT NULL,
	[DateStarted] [datetime] NULL,
	[DateCompleted] [datetime] NULL,
	[BuildConfig] [xml] NOT NULL,
	[Status] [int] NOT NULL,
	[DB] [varbinary](max) NULL,
	[Messages] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
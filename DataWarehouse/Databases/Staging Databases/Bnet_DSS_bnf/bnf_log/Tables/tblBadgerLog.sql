﻿CREATE TABLE [bnf_log].[tblBadgerLog](
	[EventLogID] [uniqueidentifier] NOT NULL,
	[EventTimestamp] [datetime] NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[WorkstationID] [uniqueidentifier] NULL,
	[SessionID] [uniqueidentifier] NULL,
	[EventSource] [nvarchar](50) NOT NULL,
	[ReferenceID] [uniqueidentifier] NULL,
	[LogName] [nvarchar](50) NOT NULL,
	[Summary] [nvarchar](250) NOT NULL,
	[EventXml] [nvarchar](max) NOT NULL,
	[Severity] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
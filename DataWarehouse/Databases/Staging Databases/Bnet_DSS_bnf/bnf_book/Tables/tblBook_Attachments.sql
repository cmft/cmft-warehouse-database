﻿CREATE TABLE [bnf_book].[tblBook_Attachments](
	[AttachmentID] [uniqueidentifier] NOT NULL,
	[Blob] [varbinary](max) NOT NULL,
	[Filename] [nvarchar](250) NOT NULL,
	[FileLength] [int] NOT NULL,
	[FileExtension] [nvarchar](50) NOT NULL,
	[MimeType] [nvarchar](100) NOT NULL,
	[IsCompressed] [bit] NOT NULL,
	[CreateTimestamp] [datetime] NOT NULL,
	[UploadedBy] [uniqueidentifier] NOT NULL,
	[IsPublished] [bit] NOT NULL,
	[BlobSHA1] [nchar](40) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
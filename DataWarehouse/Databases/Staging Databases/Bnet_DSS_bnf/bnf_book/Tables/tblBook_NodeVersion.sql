﻿CREATE TABLE [bnf_book].[tblBook_NodeVersion](
	[NodeVersionID] [uniqueidentifier] NOT NULL,
	[BookNodeID] [uniqueidentifier] NOT NULL,
	[ContentTypeID] [int] NOT NULL,
	[CreatedTimestamp] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[UpdatedTimestamp] [datetime] NOT NULL,
	[UpdatedBy] [uniqueidentifier] NOT NULL,
	[TitleText] [nvarchar](250) NOT NULL,
	[DescriptionText] [nvarchar](1000) NULL,
	[KeywordText] [nvarchar](1000) NULL,
	[TemplateNodeID] [uniqueidentifier] NULL,
	[PublishedTimestamp] [datetime] NULL,
	[PublishedBy] [uniqueidentifier] NULL,
	[PropertyXml] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
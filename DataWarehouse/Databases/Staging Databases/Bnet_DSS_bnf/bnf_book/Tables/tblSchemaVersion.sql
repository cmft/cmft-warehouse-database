﻿CREATE TABLE [bnf_book].[tblSchemaVersion](
	[SchemaVersion] [int] NOT NULL,
	[ReleaseDate] [datetime] NOT NULL,
	[Comments] [nvarchar](500) NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_book].[tblBook_Node](
	[BookNodeID] [uniqueidentifier] NOT NULL,
	[BookID] [uniqueidentifier] NOT NULL,
	[NodeTypeID] [int] NOT NULL,
	[ContentTypeID] [int] NOT NULL,
	[ParentNodeID] [uniqueidentifier] NULL,
	[LiveVersionID] [uniqueidentifier] NULL,
	[DraftVersionID] [uniqueidentifier] NULL,
	[ACL] [nvarchar](max) NULL,
	[CreatedTimestamp] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[DeletedTimestamp] [datetime] NULL,
	[DeletedBy] [uniqueidentifier] NULL,
	[SiblingIndex] [int] NOT NULL,
	[Alias] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
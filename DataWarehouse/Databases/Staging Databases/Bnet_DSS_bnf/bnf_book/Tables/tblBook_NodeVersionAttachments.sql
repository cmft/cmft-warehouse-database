﻿CREATE TABLE [bnf_book].[tblBook_NodeVersionAttachments](
	[NodeVersionID] [uniqueidentifier] NOT NULL,
	[AttachmentID] [uniqueidentifier] NOT NULL,
	[AttachmentTypeID] [int] NOT NULL,
	[AttachmentName] [nvarchar](250) NULL
) ON [PRIMARY]
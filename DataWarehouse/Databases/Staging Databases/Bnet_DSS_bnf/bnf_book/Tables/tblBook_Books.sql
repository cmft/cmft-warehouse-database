﻿CREATE TABLE [bnf_book].[tblBook_Books](
	[BookNodeID] [uniqueidentifier] NOT NULL,
	[PublicationMode] [int] NOT NULL,
	[IsBadgerNetConfig] [bit] NOT NULL,
	[Alias] [nvarchar](100) NOT NULL
) ON [PRIMARY]
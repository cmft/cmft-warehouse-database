﻿CREATE TABLE [bnf_book].[tblBook_ContentType](
	[BookContentTypeID] [int] NOT NULL,
	[BookContentTypeName] [nvarchar](50) NOT NULL,
	[ContentIsVersionable] [bit] NOT NULL CONSTRAINT [DF_tblBook_ContentType_ContentIsVersionable]  DEFAULT ((0))
) ON [PRIMARY]
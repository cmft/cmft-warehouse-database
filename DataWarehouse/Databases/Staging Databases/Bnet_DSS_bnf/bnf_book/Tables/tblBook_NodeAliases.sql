﻿CREATE TABLE [bnf_book].[tblBook_NodeAliases](
	[BookID] [uniqueidentifier] NOT NULL,
	[NodeAlias] [nvarchar](100) NOT NULL,
	[BookNodeID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
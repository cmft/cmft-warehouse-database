﻿CREATE TABLE [dbo].[Factor Levels](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](50) NULL,
	[factor] [nvarchar](50) NULL,
	[factor level] [float] NULL,
	[factor <>] [nvarchar](1) NULL,
	[units] [nvarchar](10) NULL
) ON [PRIMARY]
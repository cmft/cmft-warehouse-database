﻿CREATE TABLE [dbo].[IP/OP](
	[DEPARTMENT NO] [nvarchar](50) NULL,
	[INPATIENT_NO] [int] IDENTITY(1,1) NOT NULL,
	[ADMISSION DATE] [datetime] NULL,
	[DISCHARGE DATE] [datetime] NULL,
	[ADMISSION REASON] [ntext] NULL,
	[SURGERY] [nvarchar](50) NULL,
	[WARD] [nvarchar](15) NULL,
	[PLACE] [nvarchar](15) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
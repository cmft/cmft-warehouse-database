﻿CREATE TABLE [dbo].[stock treatment](
	[treatment] [nvarchar](50) NULL,
	[batch] [nvarchar](20) NULL,
	[vials] [float] NULL,
	[units per bottle] [float] NULL
) ON [PRIMARY]
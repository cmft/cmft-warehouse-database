﻿CREATE TABLE [dbo].[GP](
	[DEPARTMENT NO] [nvarchar](50) NOT NULL,
	[GP Title] [nvarchar](50) NULL,
	[GP Initials] [nvarchar](10) NULL,
	[GP Surname] [nvarchar](50) NULL,
	[GP Address1] [nvarchar](50) NULL,
	[GP Address2] [nvarchar](50) NULL,
	[GP Address3] [nvarchar](50) NULL,
	[GP Address4] [nvarchar](50) NULL,
	[GP Post Code] [nvarchar](50) NULL,
	[GP phone no] [nvarchar](50) NULL,
	[GP dha] [nvarchar](10) NULL,
	[GP PCT] [nvarchar](10) NULL
) ON [PRIMARY]
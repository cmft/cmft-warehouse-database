﻿CREATE TABLE [dbo].[audit erase](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[user] [nvarchar](50) NULL,
	[recno] [nvarchar](50) NULL,
	[patient] [nvarchar](50) NULL,
	[form] [nvarchar](50) NULL,
	[date] [datetime] NULL,
	[mtime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[patient - order of bleeds bleed position](
	[DEPARTMENT NO] [nvarchar](50) NULL,
	[TDATE] [datetime] NULL,
	[ONSET DATE] [datetime] NULL,
	[BLEED] [nvarchar](10) NULL,
	[BLEED POSITION] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbladverse_events](
	[Adverse_Events_ID] [int] IDENTITY(1,1) NOT NULL,
	[Department_No] [nvarchar](50) NULL,
	[Event_Date] [datetime] NULL,
	[Event_Type] [int] NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
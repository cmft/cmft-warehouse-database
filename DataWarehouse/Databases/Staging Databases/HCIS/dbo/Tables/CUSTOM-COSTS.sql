﻿CREATE TABLE [dbo].[CUSTOM-COSTS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MONTH] [datetime] NULL,
	[COST] [float] NULL,
	[DHA] [nvarchar](50) NULL
) ON [PRIMARY]
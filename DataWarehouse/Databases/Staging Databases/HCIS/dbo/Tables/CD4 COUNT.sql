﻿CREATE TABLE [dbo].[CD4 COUNT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](50) NULL,
	[cd4 count] [float] NULL,
	[cd4 count date] [datetime] NULL,
	[cd4 operator] [nvarchar](1) NULL
) ON [PRIMARY]
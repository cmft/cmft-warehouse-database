﻿CREATE TABLE [dbo].[HIV LOAD](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](50) NULL,
	[hiv load] [float] NULL,
	[hiv load date] [datetime] NULL,
	[hiv operator] [nvarchar](1) NULL
) ON [PRIMARY]
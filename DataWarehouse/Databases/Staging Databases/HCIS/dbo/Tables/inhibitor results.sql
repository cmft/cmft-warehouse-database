﻿CREATE TABLE [dbo].[inhibitor results](
	[mno] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](15) NULL,
	[idate] [datetime] NULL,
	[result] [float] NULL,
	[method] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[weight/height](
	[mno] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](15) NULL,
	[date weighed] [datetime] NULL,
	[weight] [float] NULL,
	[date measured] [datetime] NULL,
	[height] [int] NULL,
	[bmi] [decimal](5, 2) NULL
) ON [PRIMARY]
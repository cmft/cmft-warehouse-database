﻿CREATE TABLE [dbo].[tblPatientReminders](
	[department no] [nvarchar](50) NOT NULL,
	[mdate] [datetime] NULL,
	[reminder] [nvarchar](50) NULL
) ON [PRIMARY]
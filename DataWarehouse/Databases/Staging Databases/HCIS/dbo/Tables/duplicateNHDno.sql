﻿CREATE TABLE [dbo].[duplicateNHDno](
	[Department no] [nvarchar](50) NOT NULL,
	[NHD no] [nvarchar](50) NULL,
	[Date of Birth] [datetime] NULL
) ON [PRIMARY]
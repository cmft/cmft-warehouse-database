﻿CREATE TABLE [dbo].[Combo : PCT](
	[pct] [nvarchar](255) NULL,
	[current_contract] [float] NULL,
	[previous_contract] [float] NULL,
	[actual_cost] [float] NULL
) ON [PRIMARY]
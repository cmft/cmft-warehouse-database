﻿CREATE TABLE [dbo].[PatientDeathsChecked](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateChecked] [nvarchar](50) NULL,
	[Counter] [int] NULL
) ON [PRIMARY]
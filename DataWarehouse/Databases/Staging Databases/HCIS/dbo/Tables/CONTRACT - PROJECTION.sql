﻿CREATE TABLE [dbo].[CONTRACT - PROJECTION](
	[dha] [nvarchar](50) NULL,
	[high user] [nvarchar](50) NULL,
	[surgery] [nvarchar](50) NULL,
	[COST] [money] NULL,
	[TDATE] [datetime] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
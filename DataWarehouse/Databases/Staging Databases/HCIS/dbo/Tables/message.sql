﻿CREATE TABLE [dbo].[message](
	[mno] [int] IDENTITY(1,1) NOT NULL,
	[message] [ntext] NULL,
	[mdate] [datetime] NULL,
	[mtime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[home treatment delivery](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](50) NULL,
	[delivery_type] [nvarchar](50) NULL,
	[delivery_location] [nvarchar](255) NULL,
	[delivery_provider] [nvarchar](200) NULL,
	[prescription_expiry_date] [datetime] NULL,
	[prescription_details] [ntext] NULL,
	[default_delivery_cost] [money] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[STOCK LEVEL](
	[Treatment] [nvarchar](255) NOT NULL,
	[250s] [float] NULL,
	[500s] [float] NULL,
	[1000s] [float] NULL,
	[Units] [float] NULL
) ON [PRIMARY]
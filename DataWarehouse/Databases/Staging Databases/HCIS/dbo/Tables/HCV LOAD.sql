﻿CREATE TABLE [dbo].[HCV LOAD](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](50) NULL,
	[hcv load] [float] NULL,
	[hcv load date] [datetime] NULL,
	[hcv operator] [nvarchar](1) NULL
) ON [PRIMARY]
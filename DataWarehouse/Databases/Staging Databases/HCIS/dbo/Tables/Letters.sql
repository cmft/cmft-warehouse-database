﻿CREATE TABLE [dbo].[Letters](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Letter_type] [nvarchar](50) NULL,
	[letter_header] [nvarchar](50) NULL,
	[Letter_text] [ntext] NULL,
	[append_name] [nvarchar](3) NULL,
	[append_hf] [nvarchar](3) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[audit logon](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[user] [nvarchar](50) NULL,
	[state] [nvarchar](30) NULL,
	[date] [datetime] NULL,
	[mtime] [datetime] NULL
) ON [PRIMARY]
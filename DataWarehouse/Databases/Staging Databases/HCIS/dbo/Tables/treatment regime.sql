﻿CREATE TABLE [dbo].[treatment regime](
	[mcount] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](50) NULL,
	[regime] [nvarchar](255) NULL,
	[notes] [ntext] NULL,
	[treatment] [nvarchar](255) NULL,
	[treatment2] [nvarchar](50) NULL,
	[treatment3] [nvarchar](50) NULL,
	[checking] [nvarchar](3) NULL,
	[expected_product_usage] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
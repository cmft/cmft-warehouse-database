﻿CREATE TABLE [dbo].[audit pasOLD](
	[pas_audit_id] [int] IDENTITY(1,1) NOT NULL,
	[muser] [nvarchar](50) NULL,
	[patient] [nvarchar](20) NULL,
	[update_date] [datetime] NULL,
	[update_time] [datetime] NULL,
	[hcis_field] [nvarchar](50) NULL,
	[hcis_value] [nvarchar](255) NULL,
	[pas_value] [nvarchar](255) NULL
) ON [PRIMARY]
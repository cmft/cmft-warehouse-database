﻿CREATE TABLE [dbo].[Version](
	[version] [nvarchar](50) NOT NULL,
	[vdate] [nvarchar](50) NULL,
	[latest] [nvarchar](50) NULL,
	[dateChecked] [nvarchar](50) NULL,
	[StockReportDate] [datetime] NULL
) ON [PRIMARY]
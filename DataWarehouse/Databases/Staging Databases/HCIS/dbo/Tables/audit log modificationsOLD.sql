﻿CREATE TABLE [dbo].[audit log modificationsOLD](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[user] [nvarchar](50) NULL,
	[patient] [nvarchar](50) NULL,
	[ref] [nvarchar](15) NULL,
	[old_value] [ntext] NULL,
	[value] [ntext] NULL,
	[form] [nvarchar](50) NULL,
	[field] [nvarchar](50) NULL,
	[date] [datetime] NULL,
	[change_time] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
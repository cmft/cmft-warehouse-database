﻿CREATE TABLE [dbo].[other therapy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[department no] [nvarchar](50) NULL,
	[product] [nvarchar](255) NULL,
	[mstart] [datetime] NULL,
	[mend] [datetime] NULL,
	[dose] [nvarchar](255) NULL
) ON [PRIMARY]
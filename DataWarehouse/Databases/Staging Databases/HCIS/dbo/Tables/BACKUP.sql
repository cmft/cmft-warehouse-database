﻿CREATE TABLE [dbo].[BACKUP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BACKUP] [datetime] NULL,
	[DISK] [float] NULL,
	[directory] [nvarchar](255) NULL
) ON [PRIMARY]
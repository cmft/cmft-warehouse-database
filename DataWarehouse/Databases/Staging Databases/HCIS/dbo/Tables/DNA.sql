﻿CREATE TABLE [dbo].[DNA](
	[department no] [nvarchar](50) NULL,
	[appointment time] [datetime] NULL,
	[appointment date] [datetime] NULL,
	[appointments missed] [float] NULL,
	[counter] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OVERUSERS](
	[department no] [nvarchar](15) NULL,
	[forename] [nvarchar](20) NULL,
	[surname] [nvarchar](25) NULL,
	[title] [nvarchar](10) NULL,
	[user] [nvarchar](5) NULL,
	[treatment] [nvarchar](50) NULL,
	[tdate] [datetime] NULL,
	[units] [float] NULL,
	[cost] [money] NULL,
	[position] [nvarchar](255) NULL,
	[TREATMENT NO] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
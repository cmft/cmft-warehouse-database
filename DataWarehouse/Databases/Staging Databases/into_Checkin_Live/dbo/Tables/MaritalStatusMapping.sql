﻿CREATE TABLE [dbo].[MaritalStatusMapping](
	[Input] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[Default] [bit] NULL,
	[ResourceKey] [nvarchar](50) NULL,
	[ShownInFrontEnd] [bit] NULL
) ON [PRIMARY]
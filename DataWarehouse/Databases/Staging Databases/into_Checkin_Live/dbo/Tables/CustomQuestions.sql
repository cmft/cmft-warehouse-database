﻿CREATE TABLE [dbo].[CustomQuestions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ResourceKey] [nvarchar](255) NULL,
	[SortOrder] [int] NULL,
	[GoToReceptionAnswer] [nvarchar](255) NULL,
	[Enabled] [bit] NULL,
	[LowerAge] [tinyint] NULL,
	[UpperAge] [tinyint] NULL,
	[Gender] [char](1) NULL,
	[VisitType] [char](1) NULL,
	[ActivityEnabled] [bit] NULL,
	[ActivityLabel] [varchar](255) NULL,
	[Grouping] [varchar](25) NULL,
	[ControlType] [varchar](25) NULL,
	[AutoUpdatePAS] [bit] NULL,
	[ControlHeight] [int] NULL,
	[ControlWidth] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicLocations](
	[LocationCode] [nchar](3) NULL,
	[Location] [nvarchar](80) NULL,
	[AreaCode] [nchar](2) NULL,
	[WaitingAreaText] [nvarchar](max) NULL,
	[WaitingAreaImage] [nvarchar](max) NULL,
	[DemographicChangeText] [nvarchar](max) NULL,
	[DemographicChangeImage] [nvarchar](max) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Archive] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
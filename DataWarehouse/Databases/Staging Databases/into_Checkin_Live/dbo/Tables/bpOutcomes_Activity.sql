﻿CREATE TABLE [dbo].[bpOutcomes_Activity](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [int] NULL,
	[FunctionName] [varchar](75) NULL,
	[DateTimeStamp] [datetime] NULL
) ON [PRIMARY]
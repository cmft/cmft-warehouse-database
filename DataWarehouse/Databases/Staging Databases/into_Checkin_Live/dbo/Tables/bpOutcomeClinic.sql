﻿CREATE TABLE [dbo].[bpOutcomeClinic](
	[OutcomeFormCode] [smallint] NOT NULL,
	[OutcomeFormCodeSub] [int] NOT NULL,
	[clinicCode] [varchar](10) NOT NULL,
	[IsCancerClinic] [bit] NULL,
	[IsMPA] [bit] NULL,
	[LastUpdatedBy] [varchar](20) NULL,
	[LastUpdatedDate] [smalldatetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Resources](
	[ResourceKey] [nvarchar](255) NOT NULL,
	[ResourceValue] [ntext] NOT NULL,
	[Country] [int] NOT NULL,
	[Language] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
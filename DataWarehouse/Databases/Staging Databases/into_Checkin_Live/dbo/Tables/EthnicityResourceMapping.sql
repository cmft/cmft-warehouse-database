﻿CREATE TABLE [dbo].[EthnicityResourceMapping](
	[Input] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[ResourceKey] [nvarchar](50) NOT NULL,
	[Default] [bit] NULL,
	[ShownInFrontEnd] [bit] NULL,
	[SortOrder] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[bpRTTTracker](
	[RTTID] [int] IDENTITY(1,1) NOT NULL,
	[PreviousRTT] [varchar](50) NULL,
	[AttendStatus] [int] NULL,
	[Apptype] [varchar](1) NULL,
	[OutcomeOption_Key] [int] NULL,
	[OutcomeOptions] [varchar](120) NULL,
	[DerivedRTT] [varchar](10) NULL,
	[RTTEfect] [varchar](50) NULL
) ON [PRIMARY]
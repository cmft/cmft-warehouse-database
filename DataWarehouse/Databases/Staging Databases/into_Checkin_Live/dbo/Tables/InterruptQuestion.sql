﻿CREATE TABLE [dbo].[InterruptQuestion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ResourceKey] [nvarchar](255) NULL,
	[SortOrder] [int] NULL,
	[GoToReceptionAnswer] [nvarchar](255) NULL,
	[Enabled] [bit] NOT NULL,
	[LowerAge] [tinyint] NULL,
	[UpperAge] [tinyint] NULL,
	[Gender] [char](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ResidencyUpdate](
	[ResidencyUpdateId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [nvarchar](50) NULL,
	[PatientName] [nvarchar](80) NULL,
	[Date] [datetime] NULL,
	[IsUKResidentFor12Months] [bit] NULL,
	[IsFutureUKPermanentResident] [bit] NULL,
	[UKArrivalMonth] [int] NULL,
	[UKArrivalDay] [int] NULL,
	[UKArrivalYear] [int] NULL,
	[Exported] [bit] NULL
) ON [PRIMARY]
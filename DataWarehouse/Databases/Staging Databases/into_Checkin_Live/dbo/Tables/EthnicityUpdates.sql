﻿CREATE TABLE [dbo].[EthnicityUpdates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [nvarchar](50) NULL,
	[PatientName] [nvarchar](80) NULL,
	[Ethnicity] [nvarchar](200) NULL,
	[Date] [datetime] NULL,
	[Exported] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[GenderMapping](
	[Input] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[Default] [bit] NULL
) ON [PRIMARY]
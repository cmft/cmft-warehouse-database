﻿CREATE TABLE [dbo].[bpoutpat_HL7](
	[FieldID] [bigint] IDENTITY(1,1) NOT NULL,
	[AppointmentID] [varchar](50) NULL,
	[MessageType] [varchar](50) NULL,
	[hl7TimeStamp] [datetime] NULL,
	[hl7Field] [varchar](250) NULL,
	[hl7Data] [varchar](250) NULL,
	[PatientID] [varchar](250) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Languages](
	[CountryID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL,
	[CountryName] [nvarchar](255) NULL,
	[LanguageName] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[bpRTTOutcome](
	[RTTLookup_ID] [varchar](10) NOT NULL,
	[RTTDescription] [varchar](120) NULL,
	[RTTLorenzo] [varchar](80) NULL,
	[RTTEffect] [varchar](80) NULL,
	[RTTClock_Image] [varchar](50) NULL
) ON [PRIMARY]
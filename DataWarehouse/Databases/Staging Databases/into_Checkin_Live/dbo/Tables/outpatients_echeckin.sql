﻿CREATE TABLE [dbo].[outpatients_echeckin](
	[fname1] [varchar](50) NULL,
	[fname2] [char](10) NULL,
	[sname] [varchar](50) NULL,
	[sex] [char](10) NULL,
	[dob] [char](10) NULL,
	[NatPracCode] [char](10) NULL,
	[FullGPCode] [varchar](50) NULL,
	[EthnicGroup] [varchar](50) NULL,
	[pcode] [char](10) NULL,
	[AppointmentID] [varchar](50) NULL,
	[PatientID] [char](10) NULL,
	[consultant] [varchar](50) NULL,
	[specialty] [varchar](50) NULL,
	[Clinic] [varchar](50) NULL,
	[AppDate] [varchar](50) NULL,
	[Apptime] [varchar](50) NULL,
	[AppDateTime] [varchar](50) NULL,
	[Services] [int] NULL,
	[telephone] [varchar](50) NULL,
	[mobile] [varchar](50) NULL,
	[Title] [char](10) NULL,
	[ID] [int] NULL,
	[Barcode] [nvarchar](50) NULL
) ON [PRIMARY]
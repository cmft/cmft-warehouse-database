﻿CREATE TABLE [dbo].[RelationshipResourceMapping](
	[Input] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[ResourceKey] [nvarchar](50) NOT NULL,
	[Default] [bit] NULL,
	[ShownInFrontEnd] [bit] NULL
) ON [PRIMARY]
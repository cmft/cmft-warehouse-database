﻿CREATE TABLE [dbo].[bpAppointmentOutcomesLock](
	[AppointmentOutcomes_ID] [int] NOT NULL,
	[DteLocked] [datetime] NOT NULL,
	[Owner] [nvarchar](16) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Document](
	[Document_Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentType_Id] [int] NOT NULL,
	[Appointment_Id] [int] NULL,
	[Patient_Id] [nvarchar](50) NOT NULL,
	[SignatureFile] [varchar](255) NULL,
	[PDFFile] [varchar](255) NULL,
	[CreatedDateTime] [smalldatetime] NULL,
	[CompletedBy] [varchar](255) NULL,
	[MarketingAccepted] [bit] NULL,
	[AuditAccepted] [bit] NULL,
	[Exported] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ColumnMappingStrategies](
	[MappingId] [int] IDENTITY(1,1) NOT NULL,
	[ColumnName] [ntext] NOT NULL,
	[MappingStrategy] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
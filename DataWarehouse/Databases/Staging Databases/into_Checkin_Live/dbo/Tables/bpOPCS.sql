﻿CREATE TABLE [dbo].[bpOPCS](
	[OutcomesLookup_ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](4) NULL,
	[Code] [varchar](48) NULL,
	[Description] [varchar](200) NULL,
	[DisplayOrder] [int] NULL,
	[OutcomeForm] [int] NULL,
	[LastUpdatedBy] [varchar](20) NULL,
	[LastUpdatedDate] [smalldatetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OutcomeForm](
	[OutcomeForm_ID] [int] IDENTITY(1,1) NOT NULL,
	[OutcomeFormParent_ID] [int] NULL,
	[OutcomeFormName] [varchar](80) NULL,
	[OutcomeFormDescription] [varchar](250) NULL,
	[UsesNotes] [bit] NULL,
	[LastUpdatedBy] [uniqueidentifier] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LoggingActionCodes](
	[Code] [smallint] NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
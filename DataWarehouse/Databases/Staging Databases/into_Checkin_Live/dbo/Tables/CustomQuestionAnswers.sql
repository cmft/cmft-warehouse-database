﻿CREATE TABLE [dbo].[CustomQuestionAnswers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QuestionId] [int] NULL,
	[ResourceKey] [nvarchar](255) NULL,
	[CodeValue] [varchar](20) NULL,
	[GoToReceptionAnswer] [bit] NULL,
	[SortOrder] [int] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
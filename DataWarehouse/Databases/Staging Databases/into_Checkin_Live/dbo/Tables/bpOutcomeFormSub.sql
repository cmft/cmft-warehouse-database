﻿CREATE TABLE [dbo].[bpOutcomeFormSub](
	[OutcomeFormCodeSub] [int] IDENTITY(1,1) NOT NULL,
	[OutcomeFormCode] [smallint] NOT NULL,
	[OutcomeFormDescriptionSub] [varchar](80) NULL,
	[LastUpdatedBy] [varchar](20) NULL,
	[LastUpdatedDate] [smalldatetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[bpoutpat](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NHSNumber] [nvarchar](50) NULL,
	[PatientID] [nvarchar](50) NULL,
	[Title] [char](25) NULL,
	[fname1] [varchar](80) NULL,
	[fname2] [varchar](80) NULL,
	[sname] [varchar](80) NULL,
	[dob] [nvarchar](8) NULL,
	[Sex] [char](1) NULL,
	[Telephone] [varchar](50) NULL,
	[Mobile1] [varchar](50) NULL,
	[Mobile2] [varchar](50) NULL,
	[natpraccode] [nvarchar](10) NULL,
	[GPName] [nvarchar](255) NULL,
	[FullGPCode] [nvarchar](10) NULL,
	[EthnicGroup] [nvarchar](35) NULL,
	[Address1] [nchar](50) NULL,
	[Address2] [nchar](50) NULL,
	[Address3] [nchar](50) NULL,
	[Address4] [nchar](50) NULL,
	[pcode] [nvarchar](10) NULL,
	[AppDate] [smalldatetime] NULL,
	[AppointmentID] [nvarchar](50) NULL,
	[AppDateTime] [smalldatetime] NULL,
	[Apptime] [nvarchar](8) NULL,
	[Clinic] [nvarchar](500) NULL,
	[Clinic_Day] [varchar](3) NULL,
	[Clinic_Period] [varchar](2) NULL,
	[Clinic_Type] [varchar](2) NULL,
	[location] [nvarchar](80) NULL,
	[Visit_Type] [varchar](3) NULL,
	[Consultant_Login_Code] [varchar](50) NULL,
	[Consultant_ID] [varchar](50) NULL,
	[Conabbr] [varchar](10) NULL,
	[consultant] [nvarchar](50) NULL,
	[specialty] [nvarchar](75) NULL,
	[AttendStatusClinic] [smallint] NULL,
	[TimeArrival] [datetime] NULL,
	[DteUpd] [datetime] NULL,
	[DtePosted] [datetime] NULL,
	[NotesTime] [datetime] NULL,
	[ManualTime] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[pds] [bit] NULL CONSTRAINT [DF_bpoutpat_pds]  DEFAULT ((0)),
	[demog] [nvarchar](7) NULL,
	[Barcode] [nvarchar](50) NULL,
	[ArrTicket] [nvarchar](10) NULL,
	[Exception] [int] NULL,
	[ErrorText] [varchar](50) NULL,
	[Services] [int] NULL,
	[LastRTTStatus] [varchar](4) NULL,
	[refrl_refno] [nvarchar](25) NULL,
	[PatientPathwayID] [varchar](50) NULL,
	[OutcomeForm] [varchar](80) NULL,
	[OutcomeFormSub] [varchar](80) NULL,
	[DemographicUpdates] [nvarchar](50) NULL,
	[Exported] [bit] NULL CONSTRAINT [DF_bpoutpat_Exported]  DEFAULT ((0)),
	[LocationCode] [nchar](10) NULL,
	[AreaCode] [nchar](10) NULL,
	[Mobile2Work] [nvarchar](50) NULL,
	[Religion] [nvarchar](50) NULL,
	[CivilState] [nvarchar](50) NULL,
	[NextKinName] [nvarchar](50) NULL,
	[NextKinRelationship] [nvarchar](50) NULL,
	[NextKinAddress1] [nvarchar](50) NULL,
	[NextKinAddress2] [nvarchar](50) NULL,
	[NextKinAddress3] [nvarchar](50) NULL,
	[NextKinAddress4] [nvarchar](50) NULL,
	[NextKinPostcode] [nvarchar](50) NULL,
	[NextKinTelephone] [nvarchar](50) NULL,
	[UKResidency] [nvarchar](50) NULL,
	[BeforeClinicPreActivity] [nvarchar](50) NULL,
	[InTouchAppointID] [nvarchar](50) NULL,
	[AttendClinicStatus] [bit] NULL,
	[Disability] [nvarchar](35) NULL,
	[PracticeName] [nvarchar](50) NULL,
	[PracticeAddressLine1] [nvarchar](50) NULL,
	[PracticeAddressLine2] [nvarchar](50) NULL,
	[PracticeAddressLine3] [nvarchar](50) NULL,
	[PracticeAddressLine4] [nvarchar](50) NULL,
	[PracticePostcode] [nvarchar](50) NULL,
	[ShowDemographicQuestions] [bit] NULL,
	[HospitalNumber] [nvarchar](50) NULL,
	[PatientAlias] [nvarchar](70) NULL,
	[TransportReq] [int] NULL,
	[IsVulnerable] [nchar](10) NULL,
	[Porter] [int] NULL,
	[Interpreter] [nchar](10) NULL,
	[TransportUpdatedBy] [uniqueidentifier] NULL,
	[PorterUpdatedBy] [uniqueidentifier] NULL,
	[InterpreterUpdatedBy] [uniqueidentifier] NULL,
	[LastDemographicCheck] [smalldatetime] NULL,
	[PatientNameSuffix] [nvarchar](25) NULL,
	[GPSurname] [nvarchar](255) NULL,
	[GPForename1] [nvarchar](255) NULL,
	[GPForename2] [nvarchar](255) NULL,
	[GPPrefix] [nvarchar](255) NULL,
	[GPSuffix] [nvarchar](255) NULL,
	[ethnicGroup_Desc] [nvarchar](255) NULL,
	[religion_Desc] [nvarchar](255) NULL,
	[CivilState_Desc] [nvarchar](255) NULL,
	[NextKinSurname] [nvarchar](50) NULL,
	[NextKinForename1] [nvarchar](50) NULL,
	[NextKinPrefix] [nvarchar](50) NULL,
	[NextKinSuffix] [nvarchar](50) NULL,
	[PracticeAddressLine5] [nvarchar](50) NULL,
	[SourcePAS] [nvarchar](50) NULL,
	[ReceptionAlert] [bit] NULL,
	[ReceptionAlertReason] [varchar](255) NULL,
	[ReceptionAlertUpdatedBy] [varchar](50) NULL,
	[SchoolName] [varchar](255) NULL,
	[EmailAddress1] [varchar](255) NULL,
	[VisuallyImpaired] [bit] NULL CONSTRAINT [DF_bpoutpat_VisuallyImpaired]  DEFAULT ((0)),
	[VisuallyImpairedUpdatedBy] [uniqueidentifier] NULL,
	[IsVulnerableUpdatedBy] [varchar](50) NULL,
	[PatientClass] [nvarchar](50) NULL,
	[NextKinForename2] [nvarchar](50) NULL,
	[InsurancePlanId] [nvarchar](50) NULL,
	[InsuranceCompanyName] [nvarchar](50) NULL,
	[InsuranceCompanyAddress_1] [nvarchar](255) NULL,
	[InsuranceCompanyAddress_2] [nvarchar](255) NULL,
	[InsuranceCompanyAddress_3] [nvarchar](255) NULL,
	[InsuranceCompanyAddress_4] [nvarchar](255) NULL,
	[InsuranceCompanyAddress_Postcode] [nchar](10) NULL,
	[InsuranceCompanyPhoneNumber] [nvarchar](50) NULL,
	[InsurancePlanEffectiveDate] [smalldatetime] NULL,
	[InsuranceAuthorizationNumber] [nvarchar](50) NULL,
	[InsurancePolicyNumber] [nvarchar](50) NULL,
	[DentistName] [nvarchar](255) NULL,
	[DentistFirstname] [nvarchar](255) NULL,
	[DentistSurname] [nvarchar](255) NULL,
	[DentistCode] [nvarchar](255) NULL,
	[DentistPreffix] [nvarchar](50) NULL,
	[DentistSuffix] [nvarchar](50) NULL,
	[DentistPracticeName] [nvarchar](50) NULL,
	[DentistAddress1] [nvarchar](255) NULL,
	[DentistAddress2] [nvarchar](255) NULL,
	[DentistAddress3] [nvarchar](255) NULL,
	[DentistAddress4] [nvarchar](255) NULL,
	[DentistAddress5] [nvarchar](255) NULL,
	[DentistPostcode] [nvarchar](10) NULL,
	[IsWalkin] [bit] NULL,
	[Race] [nvarchar](255) NULL,
	[Race_Desc] [nvarchar](255) NULL
) ON [PRIMARY]
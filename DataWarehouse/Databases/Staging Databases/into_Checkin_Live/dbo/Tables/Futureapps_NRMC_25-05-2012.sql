﻿CREATE TABLE [dbo].[Futureapps_NRMC_25-05-2012](
	[Patient NHS Number] [varchar](50) NULL,
	[Patient ID] [varchar](50) NULL,
	[Title] [varchar](50) NULL,
	[Forename] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[Date of Birth] [varchar](50) NULL,
	[Sex] [varchar](50) NULL,
	[Home Telephone] [varchar](50) NULL,
	[Mobile Phone 1] [varchar](50) NULL,
	[GP Name] [varchar](50) NULL,
	[Full GP Code] [varchar](50) NULL,
	[Ethnic Group] [varchar](50) NULL,
	[Post Code (Patient)] [varchar](50) NULL,
	[Appt ID] [varchar](50) NULL,
	[Appt Date Time] [varchar](50) NULL,
	[Clinic Code] [varchar](50) NULL,
	[Consultant] [varchar](50) NULL,
	[Specialty] [varchar](50) NULL,
	[Barcode] [varchar](50) NULL,
	[Location Code] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[Area Code] [varchar](50) NULL,
	[Address 1 Patient] [varchar](50) NULL,
	[Address 2 Patient] [varchar](50) NULL,
	[Address 3 Patient] [varchar](50) NULL,
	[Address 4 Patient] [varchar](50) NULL,
	[Mobile 2 (Work)] [varchar](50) NULL,
	[Visit Type (New or Follow up)] [varchar](50) NULL,
	[Current RTT Status] [varchar](50) NULL,
	[Referral Ref No] [varchar](50) NULL,
	[Patient Pathway ID] [varchar](50) NULL,
	[Religion] [varchar](50) NULL,
	[MaritalStatus] [varchar](50) NULL,
	[Next of Kin Name] [varchar](50) NULL,
	[NoK Address 1] [varchar](50) NULL,
	[NoK Address 2] [varchar](50) NULL,
	[NoK Address 3] [varchar](50) NULL,
	[NoK Address 4] [varchar](50) NULL,
	[NoK Postcode] [varchar](50) NULL,
	[NoK Phone] [varchar](50) NULL,
	[Clinic Pre-Activity] [varchar](50) NULL,
	[Patient Alias] [varchar](50) NULL,
	[Transport Req] [varchar](50) NULL,
	[RiskFactorCode] [varchar](50) NULL,
	[Disability] [varchar](50) NULL,
	[Hospital Number] [varchar](50) NULL,
	[Interpreter] [varchar](50) NULL,
	[ShowDemographicQuestions] [varchar](50) NULL,
	[NoK Relation] [varchar](50) NULL,
	[GP Practice] [varchar](50) NULL,
	[Porter] [varchar](50) NULL,
	[LastDemographicCheck] [varchar](50) NULL
) ON [PRIMARY]
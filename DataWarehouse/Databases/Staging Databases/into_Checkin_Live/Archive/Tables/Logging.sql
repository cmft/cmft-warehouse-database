﻿CREATE TABLE [Archive].[Logging](
	[LoggingId] [int] NOT NULL,
	[CheckinSessionId] [uniqueidentifier] NOT NULL,
	[CurrentState] [int] NOT NULL,
	[CurrentUrl] [nvarchar](1000) NOT NULL,
	[Action] [int] NOT NULL,
	[PreviousState] [int] NULL,
	[PreviousUrl] [nvarchar](1000) NULL,
	[DataInput] [nvarchar](max) NULL,
	[DateCreated] [datetime] NOT NULL,
	[KioskCode] [nvarchar](50) NULL,
	[AreaCode] [nvarchar](50) NULL,
	[ClinicCode] [nvarchar](500) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
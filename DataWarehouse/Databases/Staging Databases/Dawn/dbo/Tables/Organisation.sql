﻿CREATE TABLE [dbo].[Organisation](
	[pkiOrganisationID] [int] NOT NULL,
	[fkiOrganisationTypeID] [int] NOT NULL,
	[cName] [varchar](100) NOT NULL,
	[cAddress1] [varchar](100) NULL,
	[cAddress2] [varchar](50) NULL,
	[cTown] [varchar](50) NULL,
	[cCounty] [varchar](50) NULL,
	[cPostcode] [varchar](50) NULL,
	[cTelephone] [varchar](20) NULL,
	[cFax] [varchar](50) NULL,
	[cEmail] [varchar](100) NULL,
	[fkiHealthAuthorityID] [int] NOT NULL,
	[lInUse] [bit] NOT NULL,
	[cOrganisationCode] [varchar](20) NULL
) ON [PRIMARY]
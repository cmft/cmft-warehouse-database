﻿CREATE TABLE [dbo].[TargetRange](
	[pkiTargetRangeID] [int] NOT NULL,
	[cDescription] [varchar](50) NOT NULL,
	[nTargetINR] [float] NOT NULL,
	[nLowerINRlimit] [float] NOT NULL,
	[nUpperINRlimit] [float] NOT NULL,
	[nMaxInterval] [int] NOT NULL,
	[nDefaultInterval] [int] NOT NULL,
	[nWarnStatus] [float] NOT NULL,
	[nWarnDuration] [int] NOT NULL,
	[lTestChangeAlarm] [bit] NOT NULL,
	[cCodeName] [varchar](6) NULL,
	[nAlarmDoseChangePercent] [int] NOT NULL,
	[nAlarmINRchange] [float] NOT NULL,
	[nAlarmINRdoseChangeFailedTrigger] [float] NOT NULL,
	[lInUse] [bit] NOT NULL,
	[GUID] [varchar](50) NOT NULL
) ON [PRIMARY]
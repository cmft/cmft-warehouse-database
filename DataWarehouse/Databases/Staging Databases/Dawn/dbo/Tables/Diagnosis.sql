﻿CREATE TABLE [dbo].[Diagnosis](
	[pkiDiagnosisID] [int] NOT NULL,
	[cDescription] [varchar](100) NOT NULL,
	[cCodeName] [varchar](25) NULL,
	[lInUse] [bit] NOT NULL,
	[fkiDiagnosisGroupID] [int] NULL,
	[GUID] [varchar](50) NOT NULL
) ON [PRIMARY]
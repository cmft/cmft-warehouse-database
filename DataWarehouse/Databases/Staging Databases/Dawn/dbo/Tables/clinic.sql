﻿CREATE TABLE [dbo].[clinic](
	[pkiClinicID] [int] NOT NULL,
	[cDescription] [varchar](50) NOT NULL,
	[nDNAInterval] [int] NULL,
	[nAdvanceDays] [int] NULL,
	[nMaxCapacity] [int] NULL,
	[lExcludeWeekends] [bit] NOT NULL,
	[fkiOrganisationID] [int] NOT NULL,
	[lInUse] [bit] NOT NULL,
	[fkiApplicationAreaID] [int] NULL,
	[cLocalCode] [varchar](50) NULL
) ON [PRIMARY]
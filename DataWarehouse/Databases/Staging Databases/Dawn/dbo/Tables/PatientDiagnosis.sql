﻿CREATE TABLE [dbo].[PatientDiagnosis](
	[pkiPatientDiagnosisID] [int] NOT NULL,
	[fkiPatientID] [int] NOT NULL,
	[fkiDiagnosisID] [int] NOT NULL,
	[dDiagnosisDate] [datetime] NOT NULL,
	[mDiagnosisNotes] [text] NULL,
	[fkiRecordingHCProfessionalID] [int] NULL,
	[fkiDiagnosisStageID] [int] NULL,
	[lActive] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
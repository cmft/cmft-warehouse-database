﻿CREATE TABLE [dbo].[HealthAuthority](
	[pkiHealthAuthorityID] [int] NOT NULL,
	[cName] [varchar](50) NOT NULL,
	[cAddress1] [varchar](100) NULL,
	[cAddress2] [varchar](50) NULL,
	[cTown] [varchar](50) NULL,
	[cCounty] [varchar](50) NULL,
	[cPostCode] [varchar](10) NULL,
	[cTelephone] [varchar](20) NULL,
	[cFax] [varchar](50) NULL,
	[cEmail] [varchar](100) NULL,
	[lInUse] [bit] NOT NULL
) ON [PRIMARY]
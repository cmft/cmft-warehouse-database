﻿CREATE TABLE [dbo].[Tbl_PatientsExtra](
	[idnum] [int] NOT NULL,
	[SurfaceArea] [real] NULL,
	[PatRecNo] [nvarchar](10) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUserID] [nvarchar](3) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUserID] [nvarchar](3) NULL,
	[NHnumber] [nvarchar](10) NULL,
	[NHnumValid] [nvarchar](4) NULL,
	[Title] [nvarchar](5) NULL,
	[Address1] [nvarchar](35) NULL,
	[Address2] [nvarchar](35) NULL,
	[Address3] [nvarchar](35) NULL,
	[Address4] [nvarchar](35) NULL,
	[AliasSurname] [nvarchar](20) NULL,
	[AliasForename] [nvarchar](15) NULL,
	[PPflag] [nvarchar](1) NULL,
	[EpisodeNum] [nvarchar](12) NULL,
	[Speciality] [nvarchar](4) NULL,
	[Allergy] [nvarchar](255) NULL,
	[NumberOfBagLabels] [float] NULL,
	[RepeatInterval] [smallint] NULL,
	[RepeatStatus] [nvarchar](1) NULL,
	[CreatedTerminal] [nvarchar](15) NULL,
	[UpdatedTerminal] [nvarchar](15) NULL,
	[Diagnosis] [nvarchar](255) NULL,
	[EthnicOrigin] [nvarchar](4) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Tbl_Cons](
	[Consultant] [nvarchar](4) NOT NULL,
	[CONSNAME] [nvarchar](40) NULL,
	[Directorate] [nvarchar](4) NULL,
	[Specialties] [nvarchar](50) NULL,
	[ID] [int] NULL,
	[Site] [nvarchar](3) NULL,
	[Period] [nvarchar](2) NULL
) ON [PRIMARY]
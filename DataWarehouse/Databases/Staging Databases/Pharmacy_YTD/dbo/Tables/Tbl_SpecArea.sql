﻿CREATE TABLE [dbo].[Tbl_SpecArea](
	[Code] [nvarchar](4) NULL,
	[Area] [nvarchar](32) NULL,
	[Order] [int] NULL,
	[CostCode] [nvarchar](50) NULL
) ON [PRIMARY]
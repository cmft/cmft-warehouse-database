﻿CREATE TABLE [dbo].[Tbl_Patients](
	[Patid] [nvarchar](10) NULL,
	[CaseNo] [nvarchar](10) NULL,
	[OldCaseNo] [nvarchar](10) NULL,
	[Surname] [nvarchar](20) NULL,
	[Forename] [nvarchar](255) NULL,
	[DOB] [nvarchar](8) NULL,
	[Sex] [nvarchar](1) NULL,
	[Ward] [nvarchar](4) NULL,
	[Consultant] [nvarchar](4) NULL,
	[Weight] [nvarchar](6) NULL,
	[Height] [nvarchar](6) NULL,
	[Status] [nvarchar](1) NULL,
	[ptr1] [nvarchar](6) NULL,
	[ptr2] [nvarchar](6) NULL,
	[ptr3] [nvarchar](6) NULL,
	[ptr4] [nvarchar](6) NULL,
	[ptr5] [nvarchar](6) NULL,
	[ptr6] [nvarchar](6) NULL,
	[ptr7] [nvarchar](6) NULL,
	[ptr8] [nvarchar](6) NULL,
	[ptr9] [nvarchar](6) NULL,
	[ptr10] [nvarchar](6) NULL,
	[PostCode] [nvarchar](8) NULL,
	[GP] [nvarchar](4) NULL,
	[Site] [nvarchar](3) NULL,
	[ID] [int] NULL,
	[name] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Tbl_WRDS](
	[Ward] [nvarchar](5) NOT NULL,
	[WardName] [nvarchar](40) NULL,
	[Directorate] [nvarchar](4) NULL,
	[WardCode] [nvarchar](4) NULL,
	[Specialty] [nvarchar](5) NULL,
	[ID] [int] NULL,
	[Site] [nvarchar](3) NULL,
	[Period] [nvarchar](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Tbl_STOCK_LISTS](
	[Ward] [nvarchar](5) NULL,
	[id] [int] NOT NULL,
	[Description] [nvarchar](56) NULL,
	[Stock_Level] [nvarchar](6) NULL,
	[Imprest_Level] [real] NULL,
	[Pack_Size] [real] NULL,
	[NSVcode] [nvarchar](7) NULL,
	[Print_Label] [nvarchar](1) NULL,
	[ScreenPosn] [int] NULL,
	[Site] [nvarchar](3) NULL,
	[Period] [nvarchar](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Tbl_BNFSECT](
	[BNFCode] [nvarchar](5) NOT NULL,
	[BNFDesc] [nvarchar](80) NULL,
	[FullBNF] [nvarchar](13) NULL,
	[ID] [int] NULL,
	[Site] [nvarchar](3) NULL,
	[Period] [nvarchar](2) NULL
) ON [PRIMARY]
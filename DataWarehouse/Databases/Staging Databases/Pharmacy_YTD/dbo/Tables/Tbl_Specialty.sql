﻿CREATE TABLE [dbo].[Tbl_Specialty](
	[Code] [nvarchar](4) NOT NULL,
	[Expansion] [nvarchar](50) NULL,
	[Directorate] [nvarchar](4) NULL,
	[ID] [int] NULL,
	[Site] [nvarchar](3) NULL,
	[Period] [nvarchar](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Staging].[Trafford_BPT_Export1](
	[Hospital Number] [int] NOT NULL,
	[Name] [varchar](150) NULL,
	[AE Admission] [datetime] NOT NULL,
	[NHS Number] [float] NULL,
	[Admission To Surgery] [datetime] NULL,
	[Orth GMC] [int] NULL,
	[Geri GMC] [int] NULL,
	[Assessment Protocol] [varchar](5) NULL,
	[Geri Grade] [varchar](50) NULL,
	[Time to Geri Assessment] [float] NULL,
	[MDT Assessment] [varchar](5) NULL,
	[Falls Assessment] [varchar](200) NULL,
	[Bone Protection Medication] [varchar](200) NULL,
	[BPT Uplift] [varchar](5) NULL,
	[PCT_Name] [varchar](100) NULL,
	[CCG_Name] [varchar](100) NULL
) ON [PRIMARY]
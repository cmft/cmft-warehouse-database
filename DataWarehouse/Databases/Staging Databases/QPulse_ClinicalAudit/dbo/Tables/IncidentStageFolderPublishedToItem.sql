﻿CREATE TABLE [dbo].[IncidentStageFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentStageFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
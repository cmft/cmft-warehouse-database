﻿CREATE TABLE [dbo].[CompetencyTemplateRequirement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompetencyTemplateLevelID] [int] NULL,
	[Title] [nvarchar](2000) NULL,
	[TemplateID] [int] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
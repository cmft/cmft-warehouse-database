﻿CREATE TABLE [dbo].[EquipmentItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](200) NULL,
	[EquipmentItemTypeID] [int] NULL,
	[Description] [nvarchar](2000) NULL,
	[AssociatedDocumentID] [int] NULL,
	[SerialNumber] [nvarchar](2000) NULL,
	[ManufacturerID] [int] NULL,
	[EquipmentItemLocationID] [int] NULL,
	[Cost] [money] NULL,
	[PurchaseDate] [datetime] NULL,
	[IsAvailable] [bit] NULL,
	[UnavailableReasonID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[UserDefinedField1] [nvarchar](2000) NULL,
	[UserDefinedField2] [nvarchar](2000) NULL,
	[UserDefinedField3] [nvarchar](2000) NULL,
	[UserDefinedField4] [nvarchar](2000) NULL,
	[UserDefinedField5] [nvarchar](2000) NULL,
	[UserDefinedField6] [nvarchar](2000) NULL,
	[UserDefinedField7] [nvarchar](2000) NULL,
	[UserDefinedField8] [nvarchar](2000) NULL,
	[UserDefinedField9] [nvarchar](2000) NULL,
	[DateCreated] [datetime] NULL,
	[CurrencyID] [int] NULL,
	[DepartmentID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
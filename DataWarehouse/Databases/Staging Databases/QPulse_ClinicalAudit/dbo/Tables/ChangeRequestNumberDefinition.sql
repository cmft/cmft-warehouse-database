﻿CREATE TABLE [dbo].[ChangeRequestNumberDefinition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Prefix] [nvarchar](200) NULL,
	[Counter] [int] NULL,
	[Suffix] [nvarchar](200) NULL
) ON [PRIMARY]
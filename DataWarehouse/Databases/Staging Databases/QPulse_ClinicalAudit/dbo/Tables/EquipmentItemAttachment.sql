﻿CREATE TABLE [dbo].[EquipmentItemAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[StorageType] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[RemoveFiles] [bit] NULL,
	[TargetFolderName] [nvarchar](200) NULL,
	[TargetLocation] [nvarchar](2000) NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
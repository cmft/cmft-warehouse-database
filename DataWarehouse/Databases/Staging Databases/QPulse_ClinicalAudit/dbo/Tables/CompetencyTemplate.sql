﻿CREATE TABLE [dbo].[CompetencyTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](2000) NULL,
	[Description] [ntext] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
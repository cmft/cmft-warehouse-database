﻿CREATE TABLE [dbo].[DocumentFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DocumentFolderID] [int] NULL,
	[GroupID] [int] NULL,
	[PersonID] [int] NULL
) ON [PRIMARY]
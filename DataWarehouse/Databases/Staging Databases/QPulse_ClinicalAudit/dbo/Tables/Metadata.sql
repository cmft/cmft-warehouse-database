﻿CREATE TABLE [dbo].[Metadata](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Metadata] [ntext] NULL,
	[Metadata3] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
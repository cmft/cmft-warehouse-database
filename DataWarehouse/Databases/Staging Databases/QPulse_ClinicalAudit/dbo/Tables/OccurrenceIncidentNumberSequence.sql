﻿CREATE TABLE [dbo].[OccurrenceIncidentNumberSequence](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Prefix] [nvarchar](200) NULL,
	[Number] [bigint] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[InitiativeAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RaisedAgainstInitiativeID] [int] NULL,
	[CompletedByPersonID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[TargetDate] [datetime] NULL,
	[CompletedList] [datetime] NULL,
	[Response] [ntext] NULL,
	[Details] [ntext] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
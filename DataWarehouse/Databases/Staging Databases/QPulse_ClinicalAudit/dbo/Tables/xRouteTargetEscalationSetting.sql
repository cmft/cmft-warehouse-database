﻿CREATE TABLE [dbo].[xRouteTargetEscalationSetting](
	[EscalationID] [int] NOT NULL,
	[RouteTargetID] [int] NOT NULL,
	[Threshold] [int] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
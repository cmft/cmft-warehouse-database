﻿CREATE TABLE [dbo].[DocumentTag](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RevisionID] [int] NULL,
	[TagSource] [nvarchar](200) NULL,
	[TagReference] [nvarchar](200) NULL,
	[TagText] [nvarchar](200) NULL,
	[CreatedDate] [datetime] NULL
) ON [PRIMARY]
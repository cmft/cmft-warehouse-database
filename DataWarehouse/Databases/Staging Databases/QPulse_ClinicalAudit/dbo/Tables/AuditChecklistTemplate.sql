﻿CREATE TABLE [dbo].[AuditChecklistTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](2000) NULL,
	[CreatedByPersonID] [int] NULL,
	[Description] [nvarchar](2000) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
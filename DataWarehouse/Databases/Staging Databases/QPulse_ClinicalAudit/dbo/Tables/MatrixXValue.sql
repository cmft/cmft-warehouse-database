﻿CREATE TABLE [dbo].[MatrixXValue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatrixID] [int] NULL,
	[SortOrder] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](2000) NULL
) ON [PRIMARY]
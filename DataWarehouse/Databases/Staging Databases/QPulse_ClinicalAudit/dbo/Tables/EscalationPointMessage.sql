﻿CREATE TABLE [dbo].[EscalationPointMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Body] [image] NULL,
	[Format] [smallint] NULL,
	[Subject] [nvarchar](200) NULL,
	[AssocModule] [smallint] NOT NULL DEFAULT ((-1)),
	[ReportObjectType] [nvarchar](100) NULL,
	[GotoInclude] [tinyint] NULL CONSTRAINT [DF_EscalationMessageIncludeGoto]  DEFAULT ((0)),
	[Priority] [smallint] NOT NULL CONSTRAINT [DF_EscalationPriority]  DEFAULT ((0)),
	[CanIncludeICalendar] [bit] NULL CONSTRAINT [DF_EscalationCanIncludeICalendar]  DEFAULT ((0)),
	[ICalendarEnabled] [bit] NULL CONSTRAINT [DF_EscalationICalendarEnabled]  DEFAULT ((0)),
	[ICalendarFieldMappings] [ntext] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
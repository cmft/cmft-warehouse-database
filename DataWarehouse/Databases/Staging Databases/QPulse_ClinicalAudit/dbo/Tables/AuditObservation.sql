﻿CREATE TABLE [dbo].[AuditObservation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NULL,
	[RaisedByPersonID] [int] NULL,
	[DateRaised] [datetime] NULL,
	[Number] [int] NULL,
	[Details] [ntext] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL,
	[RaisedFromQuestionID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
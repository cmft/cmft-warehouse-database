﻿CREATE TABLE [dbo].[UserView](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[ModuleKey] [int] NULL,
	[ViewInfo] [ntext] NULL,
	[UserID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentTemplateStageWorkflow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateID] [int] NULL,
	[TemplateStageID] [int] NULL,
	[WorkflowOrder] [int] NULL,
	[SortOrder] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentTemplateAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateStageID] [int] NOT NULL,
	[OwnerPersonID] [int] NULL,
	[TargetDateDuration] [int] NULL,
	[Description] [nvarchar](4000) NULL
) ON [PRIMARY]
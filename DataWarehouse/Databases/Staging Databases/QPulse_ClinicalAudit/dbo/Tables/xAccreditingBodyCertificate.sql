﻿CREATE TABLE [dbo].[xAccreditingBodyCertificate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CetificationBodyID] [int] NOT NULL,
	[CertificateID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DynamicRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](1000) NULL,
	[StateDef] [ntext] NULL,
	[SecurableObjectType] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
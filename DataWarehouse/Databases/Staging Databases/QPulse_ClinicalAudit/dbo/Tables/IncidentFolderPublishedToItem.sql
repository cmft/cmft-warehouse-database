﻿CREATE TABLE [dbo].[IncidentFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
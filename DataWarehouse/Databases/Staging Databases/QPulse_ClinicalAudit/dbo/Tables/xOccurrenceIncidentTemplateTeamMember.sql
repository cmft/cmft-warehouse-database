﻿CREATE TABLE [dbo].[xOccurrenceIncidentTemplateTeamMember](
	[TemplateStageID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[Note] [nvarchar](2000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentTemplateStage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateID] [int] NOT NULL,
	[StageTypeID] [int] NOT NULL,
	[OwnerPersonID] [int] NULL,
	[TargetDateDuration] [int] NULL,
	[Description] [nvarchar](4000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[InformationPoint](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InitiativeID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[Details] [ntext] NULL,
	[InformationPointTypeID] [int] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[MessageRecipient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MessageSettingConditionID] [int] NULL,
	[PersonID] [int] NULL,
	[StaticRoleID] [int] NULL,
	[DynamicRoleID] [int] NULL,
	[RecipientType] [smallint] NOT NULL,
	[NotifyType] [smallint] NOT NULL,
	[Data] [nvarchar](200) NULL,
	[GroupID] [int] NULL,
	[IsManagerRecipient] [bit] NOT NULL CONSTRAINT [DF_MessageManagerRecipient]  DEFAULT ((0)),
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
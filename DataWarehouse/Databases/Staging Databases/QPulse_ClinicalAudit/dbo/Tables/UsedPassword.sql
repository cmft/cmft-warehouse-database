﻿CREATE TABLE [dbo].[UsedPassword](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[UsedPass] [nvarchar](50) NULL,
	[Added] [datetime] NULL
) ON [PRIMARY]
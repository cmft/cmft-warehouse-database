﻿CREATE TABLE [dbo].[OccurrenceAnalysisFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[OccurrenceAnalysisFolderID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
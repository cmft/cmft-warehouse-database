﻿CREATE TABLE [dbo].[OccurrenceIncidentTemplateRiskAssessment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateID] [int] NOT NULL,
	[Title] [nvarchar](4000) NOT NULL,
	[Comment] [nvarchar](255) NULL,
	[MatrixUsed] [nvarchar](200) NULL,
	[MatrixXValue] [nvarchar](200) NULL,
	[MatrixYValue] [nvarchar](200) NULL,
	[RiskValue] [nvarchar](200) NULL,
	[DefaultActionID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[MatrixXAxisTitle] [nvarchar](200) NULL,
	[MatrixYAxisTitle] [nvarchar](200) NULL,
	[UseCurrentUserIDAsOwner] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CustomBranding](
	[ID] [int] NOT NULL,
	[ImageData] [varbinary](max) NULL,
	[ImageFileName] [nvarchar](260) NULL,
	[BackColour] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
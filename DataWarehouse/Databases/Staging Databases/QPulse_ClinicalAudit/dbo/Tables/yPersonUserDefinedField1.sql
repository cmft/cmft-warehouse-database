﻿CREATE TABLE [dbo].[yPersonUserDefinedField1](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[ParentID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[SortOrder] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
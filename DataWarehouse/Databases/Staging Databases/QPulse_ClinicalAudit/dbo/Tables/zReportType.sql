﻿CREATE TABLE [dbo].[zReportType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[ReportFormID] [int] NULL,
	[IsVisible] [bit] NULL,
	[Version] [timestamp] NULL,
	[OccurrenceNumberSequenceID] [int] NULL,
	[IncidentNumberSequenceID] [int] NULL,
	[OccurrenceIncidentTemplateID] [int] NULL,
	[LocationIsMandatory] [bit] NULL,
	[ImageData] [varbinary](max) NULL,
	[ConfidentialByDefault] [bit] NULL,
	[ImageFileName] [nvarchar](260) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
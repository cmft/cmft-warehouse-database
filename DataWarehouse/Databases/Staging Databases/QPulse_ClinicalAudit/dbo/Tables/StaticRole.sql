﻿CREATE TABLE [dbo].[StaticRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](1000) NULL,
	[CreatedDate] [datetime] NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[IsSystem] [bit] NULL,
	[CreatedByPersonID] [int] NULL,
	[MapsToADGroup] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
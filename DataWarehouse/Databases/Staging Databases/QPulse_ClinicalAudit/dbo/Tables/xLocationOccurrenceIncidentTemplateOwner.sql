﻿CREATE TABLE [dbo].[xLocationOccurrenceIncidentTemplateOwner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[PersonID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ySupplierStandardType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
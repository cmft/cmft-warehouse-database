﻿CREATE TABLE [dbo].[zOccurrenceIncidentStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[IsVisible] [bit] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
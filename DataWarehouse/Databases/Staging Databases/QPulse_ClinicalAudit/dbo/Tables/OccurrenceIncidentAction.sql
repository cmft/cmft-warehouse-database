﻿CREATE TABLE [dbo].[OccurrenceIncidentAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](200) NULL,
	[OwnerPersonID] [int] NULL,
	[TargetDate] [datetime] NULL,
	[Description] [nvarchar](2000) NULL,
	[ResponseDetails] [ntext] NULL,
	[ClosedDate] [datetime] NULL,
	[ClosedByPersonID] [int] NULL,
	[StageID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
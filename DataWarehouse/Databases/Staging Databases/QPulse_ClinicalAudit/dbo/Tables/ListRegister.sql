﻿CREATE TABLE [dbo].[ListRegister](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](200) NOT NULL,
	[TableName] [nvarchar](200) NOT NULL,
	[FetchContext] [nvarchar](200) NOT NULL,
	[Columns] [nvarchar](2000) NULL,
	[SearchableColumns] [nvarchar](2000) NULL,
	[IsHierarchical] [bit] NOT NULL CONSTRAINT [DF_ListRegister_IsHierarchical]  DEFAULT ((1)),
	[SelectColumns] [nvarchar](2000) NULL,
	[DisplayColumn] [nvarchar](50) NOT NULL CONSTRAINT [DF_ListRegister_DisplayColumn]  DEFAULT (N'Name')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Permission](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ObjectFunctionID] [int] NULL,
	[SecurableObjectTypeID] [int] NULL,
	[InstanceKey] [int] NULL,
	[RoleType] [int] NULL,
	[AppliesToDynamicRoleID] [int] NULL,
	[AppliesToPersonID] [int] NULL,
	[AppliesToStaticRoleID] [int] NULL,
	[DenyToViewer] [bit] NULL,
	[StateDef] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
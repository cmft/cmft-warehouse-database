﻿CREATE TABLE [dbo].[PersonReview](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[Details] [ntext] NULL,
	[DueDate] [datetime] NULL,
	[PerformedDate] [datetime] NULL,
	[ReviewerID] [int] NULL,
	[ReviewTypeID] [int] NULL,
	[AttachmentStorageType] [int] NULL,
	[AttachmentName] [nvarchar](200) NULL,
	[AttachmentTargetFolderName] [nvarchar](200) NULL,
	[AttachmentTargetLocation] [nvarchar](2000) NULL,
	[AttachmentRemoveFiles] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
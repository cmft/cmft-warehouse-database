﻿CREATE TABLE [dbo].[JobRoleCompetency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobRoleID] [int] NOT NULL,
	[Title] [nvarchar](2000) NOT NULL,
	[Description] [ntext] NULL,
	[LevelID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
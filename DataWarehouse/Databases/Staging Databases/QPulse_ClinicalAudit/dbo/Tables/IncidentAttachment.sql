﻿CREATE TABLE [dbo].[IncidentAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[StorageType] [int] NULL,
	[TargetLocation] [nvarchar](2000) NULL,
	[TargetFolderName] [nvarchar](200) NULL,
	[RemoveFiles] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
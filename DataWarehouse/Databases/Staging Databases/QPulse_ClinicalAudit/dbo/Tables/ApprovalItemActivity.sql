﻿CREATE TABLE [dbo].[ApprovalItemActivity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalItemID] [int] NULL,
	[ActivityType] [int] NULL,
	[AcitivtyDate] [datetime] NULL,
	[PerformedByPersonID] [int] NULL,
	[Comment] [ntext] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
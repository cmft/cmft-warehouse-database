﻿CREATE TABLE [dbo].[MessageSettingConditions](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[MessageSettingID] [int] NOT NULL,
	[Condition] [ntext] NULL,
	[Priority] [smallint] NOT NULL CONSTRAINT [DF_Priority]  DEFAULT ((0)),
	[Name] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
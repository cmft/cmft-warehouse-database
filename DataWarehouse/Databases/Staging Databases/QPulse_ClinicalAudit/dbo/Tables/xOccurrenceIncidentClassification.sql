﻿CREATE TABLE [dbo].[xOccurrenceIncidentClassification](
	[IncidentID] [int] NOT NULL,
	[ClassificationItemID] [int] NOT NULL,
	[FindingID] [int] NULL,
	[NCID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL
) ON [PRIMARY]
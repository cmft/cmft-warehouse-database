﻿CREATE TABLE [dbo].[OccurrenceFolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Comment] [nvarchar](2000) NULL,
	[FolderCategoryID] [int] NULL,
	[FolderType] [int] NULL,
	[Status] [int] NULL,
	[Condition] [ntext] NULL,
	[ViewInfo] [ntext] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[LastModified] [datetime] NULL,
	[RegisterViewType] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
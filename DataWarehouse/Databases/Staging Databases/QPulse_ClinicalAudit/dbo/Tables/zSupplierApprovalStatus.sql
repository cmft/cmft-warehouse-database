﻿CREATE TABLE [dbo].[zSupplierApprovalStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
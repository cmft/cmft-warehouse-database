﻿CREATE TABLE [dbo].[xAuditPersonCheckout](
	[AuditID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[CheckedOutDateTime] [datetime] NOT NULL,
	[CheckoutIdentifier] [nvarchar](255) NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ApprovalItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalCycleID] [int] NOT NULL,
	[NotifiedPersonID] [int] NULL,
	[NotifyDate] [datetime] NULL,
	[WorkflowOrder] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[SortOrder] [int] NULL,
	[ResponseID] [int] NULL,
	[RespondedByPersonID] [int] NULL,
	[ResponseDate] [datetime] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[IncidentTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](2000) NULL,
	[CreatedByPersonID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[StatusID] [int] NULL,
	[TargetDuration] [int] NULL,
	[Stages] [image] NULL,
	[IsDefaultTemplate] [bit] NULL,
	[TemplateModule] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
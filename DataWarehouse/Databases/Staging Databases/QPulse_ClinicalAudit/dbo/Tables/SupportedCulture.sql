﻿CREATE TABLE [dbo].[SupportedCulture](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CultureInfoString] [varchar](10) NULL
) ON [PRIMARY]
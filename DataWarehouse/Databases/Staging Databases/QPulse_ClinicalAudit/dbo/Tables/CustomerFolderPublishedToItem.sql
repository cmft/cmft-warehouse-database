﻿CREATE TABLE [dbo].[CustomerFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
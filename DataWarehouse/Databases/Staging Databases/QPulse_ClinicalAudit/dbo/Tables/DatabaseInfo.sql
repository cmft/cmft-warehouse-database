﻿CREATE TABLE [dbo].[DatabaseInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime] NULL,
	[ScriptVersion] [int] NULL,
	[MinorScriptVersion] [int] NULL,
	[ServerVersion] [nvarchar](20) NULL,
	[LicenseHash] [int] NULL
) ON [PRIMARY]
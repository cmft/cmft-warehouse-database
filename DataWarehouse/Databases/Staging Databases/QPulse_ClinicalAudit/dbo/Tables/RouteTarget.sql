﻿CREATE TABLE [dbo].[RouteTarget](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RouteID] [int] NOT NULL,
	[PersonID] [int] NULL,
	[ReferenceGroupID] [int] NULL
) ON [PRIMARY]
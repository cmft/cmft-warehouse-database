﻿CREATE TABLE [dbo].[ReportItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[Title] [nvarchar](200) NULL,
	[ModuleKey] [int] NULL,
	[ReportType] [int] NULL,
	[ReportStream] [ntext] NULL,
	[Context] [int] NULL,
	[BinaryStream] [image] NULL,
	[PaperKind] [int] NOT NULL CONSTRAINT [DF_DefaultPaperKind]  DEFAULT ((9)),
	[PaperName] [nvarchar](200) NULL,
	[PaperWidth] [int] NOT NULL CONSTRAINT [DF_DefaultPaperWidth]  DEFAULT ((2101)),
	[PaperHeight] [int] NOT NULL CONSTRAINT [DF_DefaultPaperHeight]  DEFAULT ((2969)),
	[ReportUnit] [int] NOT NULL CONSTRAINT [DF_DefaultReportUnit]  DEFAULT ((1)),
	[Landscape] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[SignaturePoint](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Meaning] [nvarchar](2000) NULL,
	[Module] [int] NOT NULL,
	[IsActive] [bit] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AuditFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AuditFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PeopleFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PeopleFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
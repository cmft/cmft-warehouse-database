﻿CREATE TABLE [dbo].[IncidentStage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[Summary] [ntext] NULL,
	[TargetDate] [datetime] NULL,
	[ClosedDate] [datetime] NULL,
	[ClosedByPersonID] [int] NULL,
	[WorkflowOrder] [int] NULL,
	[CostOfStage] [money] NULL,
	[TimeForStage] [int] NULL,
	[IncidentStageTypeID] [int] NULL,
	[SortOrder] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[ShowActionsWhenEmpty] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
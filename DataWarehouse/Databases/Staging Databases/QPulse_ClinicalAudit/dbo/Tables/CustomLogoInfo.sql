﻿CREATE TABLE [dbo].[CustomLogoInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](200) NULL,
	[Format] [int] NULL,
	[ImageData] [image] NULL,
	[DateCreated] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
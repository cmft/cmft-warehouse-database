﻿CREATE TABLE [dbo].[OccurrenceAttachmentItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AttachmentID] [int] NULL,
	[AbsolutePath] [nvarchar](2000) NULL,
	[DisplayFileName] [nvarchar](200) NULL,
	[Extension] [nvarchar](200) NULL,
	[FileData] [image] NULL,
	[FileSize] [bigint] NULL,
	[IsEmbedded] [bit] NULL,
	[IsIndex] [bit] NULL,
	[ModifiedDate] [datetime] NULL,
	[RelativePath] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[ModuleAttachmentOption](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleKey] [int] NULL,
	[ForceEmbed] [bit] NULL,
	[DeleteOriginal] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ApproverTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApproverPersonID] [int] NULL,
	[WorkflowOrder] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[SortOrder] [int] NULL,
	[ApprovalTemplateID] [int] NULL,
	[ApproverGroupID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
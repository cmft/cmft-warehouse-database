﻿CREATE TABLE [dbo].[CacheChangeNotification](
	[TableName] [nvarchar](2000) NOT NULL,
	[ChangeID] [int] NULL,
	[LastUpdated] [datetime] NULL
) ON [PRIMARY]
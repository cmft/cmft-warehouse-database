﻿CREATE TABLE [dbo].[OccurrenceIncidentStageFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OccurrenceIncidentStageFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
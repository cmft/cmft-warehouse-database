﻿CREATE TABLE [dbo].[ChangeRequestFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChangeRequestFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
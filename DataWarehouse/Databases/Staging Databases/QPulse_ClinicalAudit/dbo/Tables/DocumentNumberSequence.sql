﻿CREATE TABLE [dbo].[DocumentNumberSequence](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Prefix] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
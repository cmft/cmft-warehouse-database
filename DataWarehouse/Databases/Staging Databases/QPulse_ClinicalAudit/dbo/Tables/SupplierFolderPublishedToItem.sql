﻿CREATE TABLE [dbo].[SupplierFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CustomFieldInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Path] [nvarchar](2000) NULL,
	[IsReportable] [bit] NULL,
	[IsDefaultReportable] [bit] NULL,
	[IsCustomisable] [bit] NULL,
	[ModuleKey] [int] NULL,
	[IsCollection] [bit] NULL,
	[DefaultSearchFields] [nvarchar](2000) NULL,
	[CanQuickAdd] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
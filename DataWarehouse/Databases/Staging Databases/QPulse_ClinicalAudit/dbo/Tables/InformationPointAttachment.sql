﻿CREATE TABLE [dbo].[InformationPointAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InformationPointID] [int] NULL,
	[URI] [nvarchar](2000) NULL,
	[FileImage] [image] NULL,
	[StorageType] [int] NULL,
	[IsDefault] [bit] NULL,
	[Name] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
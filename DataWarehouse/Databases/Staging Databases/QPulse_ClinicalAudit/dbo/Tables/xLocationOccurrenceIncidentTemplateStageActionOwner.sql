﻿CREATE TABLE [dbo].[xLocationOccurrenceIncidentTemplateStageActionOwner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActionID] [int] NOT NULL,
	[PersonID] [int] NULL,
	[TemplateStageOwnerID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OccurrenceFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
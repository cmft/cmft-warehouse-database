﻿CREATE TABLE [dbo].[EscalationPoint](
	[Condition] [ntext] NULL,
	[LifeTime] [int] NULL,
	[EscalationID] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LifeTimeActive] [bit] NULL,
	[MessageID] [int] NULL,
	[CountThreshold] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
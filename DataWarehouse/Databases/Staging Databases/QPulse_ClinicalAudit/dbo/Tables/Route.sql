﻿CREATE TABLE [dbo].[Route](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
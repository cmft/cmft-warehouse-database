﻿CREATE TABLE [dbo].[AuditWorkload](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[SearchCriteria] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
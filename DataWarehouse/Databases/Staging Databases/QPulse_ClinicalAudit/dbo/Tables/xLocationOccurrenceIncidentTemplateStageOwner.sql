﻿CREATE TABLE [dbo].[xLocationOccurrenceIncidentTemplateStageOwner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StageID] [int] NOT NULL,
	[PersonID] [int] NULL,
	[TemplateOwnerID] [int] NULL
) ON [PRIMARY]
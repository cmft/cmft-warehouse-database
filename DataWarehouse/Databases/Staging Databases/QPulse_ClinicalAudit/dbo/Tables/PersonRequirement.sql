﻿CREATE TABLE [dbo].[PersonRequirement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompetencyID] [int] NOT NULL,
	[Title] [nvarchar](2000) NOT NULL,
	[Comment] [ntext] NULL,
	[PlannedDate] [datetime] NULL,
	[ScheduledDate] [datetime] NULL,
	[CompletedDate] [datetime] NULL,
	[TrainingEventTemplateID] [int] NULL,
	[PersonTrainingEventID] [int] NULL,
	[Autotrack] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
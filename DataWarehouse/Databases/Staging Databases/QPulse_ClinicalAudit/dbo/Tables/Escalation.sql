﻿CREATE TABLE [dbo].[Escalation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Active] [bit] NULL CONSTRAINT [DF_EscalationActive]  DEFAULT ((0)),
	[BaseFilter] [ntext] NULL,
	[Title] [varchar](50) NULL,
	[Description] [nvarchar](200) NULL,
	[EscalationType] [smallint] NULL,
	[Module] [smallint] NULL,
	[DataTypeName] [varchar](100) NULL,
	[Fields] [ntext] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
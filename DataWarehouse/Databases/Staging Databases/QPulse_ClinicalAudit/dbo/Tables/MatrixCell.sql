﻿CREATE TABLE [dbo].[MatrixCell](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatrixID] [int] NULL,
	[MatrixXValueID] [int] NULL,
	[MatrixYValueID] [int] NULL,
	[MatrixResultID] [int] NULL
) ON [PRIMARY]
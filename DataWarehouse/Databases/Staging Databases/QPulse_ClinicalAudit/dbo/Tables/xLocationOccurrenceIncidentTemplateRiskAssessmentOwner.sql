﻿CREATE TABLE [dbo].[xLocationOccurrenceIncidentTemplateRiskAssessmentOwner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RiskAssessmentID] [int] NOT NULL,
	[PersonID] [int] NULL,
	[TemplateOwnerID] [int] NULL
) ON [PRIMARY]
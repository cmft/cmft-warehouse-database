﻿CREATE TABLE [dbo].[SupplierCertificateAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierCertificateID] [int] NULL,
	[URI] [nvarchar](2000) NULL,
	[FileImage] [image] NULL,
	[StorageType] [int] NULL,
	[IsDefault] [bit] NULL,
	[Name] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[ChangeRequestAttachmentItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChangeRequestID] [int] NULL,
	[FileData] [image] NULL,
	[IsIndex] [bit] NULL,
	[DisplayFileName] [nvarchar](200) NULL,
	[Extension] [nvarchar](200) NULL,
	[FileSize] [bigint] NULL,
	[AbsolutePath] [nvarchar](2000) NULL,
	[RelativePath] [nvarchar](200) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsEmbedded] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
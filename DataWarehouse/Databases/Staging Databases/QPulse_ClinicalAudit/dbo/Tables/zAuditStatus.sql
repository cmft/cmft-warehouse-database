﻿CREATE TABLE [dbo].[zAuditStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY]
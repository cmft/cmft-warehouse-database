﻿CREATE TABLE [dbo].[PersonTrainingEventNote](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonTrainingEventID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[NoteText] [ntext] NULL,
	[NoteSubject] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
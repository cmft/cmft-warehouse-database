﻿CREATE TABLE [dbo].[zCompetencyLevel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
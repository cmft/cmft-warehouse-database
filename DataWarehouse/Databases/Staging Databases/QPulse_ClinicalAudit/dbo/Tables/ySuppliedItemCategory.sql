﻿CREATE TABLE [dbo].[ySuppliedItemCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[Name] [nvarchar](200) NULL,
	[CalculatedPath] [nvarchar](2000) NULL
) ON [PRIMARY]
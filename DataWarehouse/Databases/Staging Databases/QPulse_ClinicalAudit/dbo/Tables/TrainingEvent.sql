﻿CREATE TABLE [dbo].[TrainingEvent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventTemplateID] [int] NULL,
	[Title] [nvarchar](200) NULL,
	[EventCategoryID] [int] NULL,
	[EventLocationID] [int] NULL,
	[Description] [ntext] NULL,
	[IsRenewable] [bit] NULL,
	[Objectives] [ntext] NULL,
	[Duration] [int] NULL,
	[DurationMeasure] [int] NULL,
	[ScheduledDate] [datetime] NULL,
	[CompletedDate] [datetime] NULL,
	[DocumentRevisionID] [int] NULL,
	[TrainerSupplierID] [int] NULL,
	[TrainerPersonID] [int] NULL,
	[EvaluationsRequired] [bit] NULL,
	[CostPerAttendee] [money] NULL,
	[CurrencyID] [int] NULL,
	[UserDefinedField1] [int] NULL,
	[UserDefinedField2] [int] NULL,
	[UserDefinedField3] [nvarchar](200) NULL,
	[UserDefinedField4] [nvarchar](200) NULL,
	[UserDefinedField5ID] [int] NULL,
	[UserDefinedField6ID] [int] NULL,
	[IsDeleted] [bit] NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[OwnerPersonID] [int] NULL,
	[IsDefault] [bit] NULL,
	[Description] [nvarchar](2000) NULL,
	[TargetDateDuration] [int] NULL,
	[ChildData] [image] NULL,
	[StatusID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[Version] [timestamp] NULL,
	[IsRelational] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
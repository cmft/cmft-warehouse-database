﻿CREATE TABLE [dbo].[Batch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentID] [int] NULL,
	[Number] [nvarchar](200) NULL,
	[ItemCount] [int] NULL,
	[RejectCount] [int] NULL,
	[StockLocation] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[WorkOrderNumberID] [int] NULL,
	[CustomerReferenceID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ApprovalCycle](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InitiatorPersonID] [int] NULL,
	[RevisionID] [int] NULL,
	[StartDate] [datetime] NULL,
	[ReasonForSubmission] [ntext] NULL,
	[CreatedByPersonID] [int] NULL,
	[RevisionReference] [nvarchar](200) NULL,
	[StatusID] [int] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
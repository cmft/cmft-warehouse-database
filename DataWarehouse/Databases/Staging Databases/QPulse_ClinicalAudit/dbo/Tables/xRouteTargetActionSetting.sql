﻿CREATE TABLE [dbo].[xRouteTargetActionSetting](
	[ActionID] [int] NOT NULL,
	[RouteTargetID] [int] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Signature](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Details] [nvarchar](200) NULL,
	[RecordType] [int] NOT NULL,
	[RecordID] [int] NOT NULL,
	[SignedItem] [nvarchar](2000) NOT NULL,
	[SignerPersonID] [int] NOT NULL,
	[SignedDate] [datetime] NOT NULL
) ON [PRIMARY]
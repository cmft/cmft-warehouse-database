﻿CREATE TABLE [dbo].[xOccurrenceIncidentStageNonConformance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StageID] [int] NOT NULL,
	[NonConformanceID] [int] NOT NULL
) ON [PRIMARY]
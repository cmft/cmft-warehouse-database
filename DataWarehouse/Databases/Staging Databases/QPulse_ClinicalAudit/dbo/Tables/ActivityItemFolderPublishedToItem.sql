﻿CREATE TABLE [dbo].[ActivityItemFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityItemFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
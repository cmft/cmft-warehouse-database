﻿CREATE TABLE [dbo].[xRouteTargetMessageSetting](
	[MessageID] [int] NOT NULL,
	[RouteTargetID] [int] NOT NULL,
	[RecipientType] [int] NOT NULL,
	[Enabled] [bit] NULL,
	[Mandatory] [int] NULL
) ON [PRIMARY]
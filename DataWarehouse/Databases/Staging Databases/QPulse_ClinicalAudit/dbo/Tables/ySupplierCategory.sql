﻿CREATE TABLE [dbo].[ySupplierCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[ParentID] [int] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
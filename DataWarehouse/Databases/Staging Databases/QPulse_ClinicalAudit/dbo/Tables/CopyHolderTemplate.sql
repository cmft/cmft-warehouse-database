﻿CREATE TABLE [dbo].[CopyHolderTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CopyHolderListTemplateID] [int] NULL,
	[CopyHolderPersonID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[CopyTypeID] [int] NULL,
	[Comment] [nvarchar](200) NULL,
	[CopyNumber] [nvarchar](200) NULL,
	[CopyHolderGroupID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CopyHolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CopyNumber] [nvarchar](200) NULL,
	[RevisionID] [int] NULL,
	[PersonID] [int] NULL,
	[NotifiedDate] [datetime] NULL,
	[AcknowledgedByPersonID] [int] NULL,
	[AcknowledgedDate] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[CopyTypeID] [int] NULL,
	[Comment] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
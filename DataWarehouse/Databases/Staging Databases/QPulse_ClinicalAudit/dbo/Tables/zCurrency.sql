﻿CREATE TABLE [dbo].[zCurrency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[Name] [nvarchar](200) NULL,
	[SortOrder] [int] NULL,
	[Symbol] [nchar](5) NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentFolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Comment] [nvarchar](2000) NULL,
	[FolderCategoryID] [int] NULL,
	[FolderType] [int] NULL,
	[Status] [int] NULL,
	[Condition] [ntext] NULL,
	[ViewInfo] [ntext] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[Version] [timestamp] NOT NULL,
	[LastModified] [datetime] NULL,
	[RegisterViewType] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Validator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ElementName] [nvarchar](50) NOT NULL,
	[ValidatorName] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[DisplayName] [nvarchar](50) NOT NULL,
	[Parameter] [nvarchar](50) NULL
) ON [PRIMARY]
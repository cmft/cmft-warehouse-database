﻿CREATE TABLE [dbo].[PostRequirement](
	[PostRequirementID] [int] NOT NULL,
	[RequiredWTE] [money] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[PostID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
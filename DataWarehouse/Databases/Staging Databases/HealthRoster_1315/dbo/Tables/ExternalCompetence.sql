﻿CREATE TABLE [dbo].[ExternalCompetence](
	[ExternalCompetenceID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[Code] [nvarchar](50) NULL,
	[CompetenceType] [nvarchar](255) NULL,
	[Cluster] [nvarchar](255) NULL,
	[ExternallyOwned] [bit] NOT NULL,
	[MatchedCompetenceID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
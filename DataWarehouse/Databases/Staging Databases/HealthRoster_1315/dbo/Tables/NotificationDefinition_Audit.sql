﻿CREATE TABLE [dbo].[NotificationDefinition_Audit](
	[NotificationDefinitionID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

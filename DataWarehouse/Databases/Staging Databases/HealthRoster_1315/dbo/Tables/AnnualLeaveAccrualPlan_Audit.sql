﻿CREATE TABLE [dbo].[AnnualLeaveAccrualPlan_Audit](
	[AnnualLeaveAccrualPlanID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

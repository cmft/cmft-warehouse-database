﻿CREATE TABLE [dbo].[Pattern](
	[PatternID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Category] [int] NOT NULL,
	[Priority] [int] NOT NULL,
	[ApplicabilityType] [int] NOT NULL,
	[Behaviour] [xml] NULL,
	[Sequences] [xml] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[ShiftGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
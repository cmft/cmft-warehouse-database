﻿CREATE TABLE [dbo].[EntitlementAdjustment_Audit](
	[EntitlementAdjustmentID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

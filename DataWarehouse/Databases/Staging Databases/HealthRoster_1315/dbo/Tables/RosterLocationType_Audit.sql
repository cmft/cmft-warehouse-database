﻿CREATE TABLE [dbo].[RosterLocationType_Audit](
	[RosterLocationTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

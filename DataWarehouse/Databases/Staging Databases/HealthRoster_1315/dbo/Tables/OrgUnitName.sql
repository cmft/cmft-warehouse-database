﻿CREATE TABLE [dbo].[OrgUnitName](
	[OrgUnitNameID] [int] NOT NULL,
	[ShortName] [nvarchar](60) NOT NULL,
	[LongName] [nvarchar](255) NULL,
	[ExternalName] [nvarchar](255) NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
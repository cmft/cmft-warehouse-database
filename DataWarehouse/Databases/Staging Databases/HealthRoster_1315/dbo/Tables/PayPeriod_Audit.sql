﻿CREATE TABLE [dbo].[PayPeriod_Audit](
	[PayPeriodID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

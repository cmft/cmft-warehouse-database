﻿CREATE TABLE [dbo].[Pattern_Audit](
	[PatternID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

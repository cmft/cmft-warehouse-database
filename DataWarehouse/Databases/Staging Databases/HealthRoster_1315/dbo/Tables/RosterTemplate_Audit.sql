﻿CREATE TABLE [dbo].[RosterTemplate_Audit](
	[RosterTemplateID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

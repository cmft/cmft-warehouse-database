﻿CREATE TABLE [dbo].[UnitPreference](
	[UnitPreferenceID] [int] NOT NULL,
	[UnitPreferenceType] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
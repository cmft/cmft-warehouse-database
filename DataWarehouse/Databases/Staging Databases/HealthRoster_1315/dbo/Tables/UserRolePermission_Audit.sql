﻿CREATE TABLE [dbo].[UserRolePermission_Audit](
	[UserRolePermissionID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

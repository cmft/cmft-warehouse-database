﻿CREATE TABLE [dbo].[TempStaffUnavailability_Audit](
	[TempStaffUnavailabilityID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

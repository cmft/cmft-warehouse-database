﻿CREATE TABLE [dbo].[EntitlementAdjustment](
	[EntitlementAdjustmentID] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[ValidDate] [datetime] NOT NULL,
	[EntitlementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
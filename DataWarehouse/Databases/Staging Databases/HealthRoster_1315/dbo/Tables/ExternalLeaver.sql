﻿CREATE TABLE [dbo].[ExternalLeaver](
	[ExternalLeaverID] [int] NOT NULL,
	[StaffNumber] [nvarchar](255) NOT NULL,
	[DateLeft] [datetime] NOT NULL,
	[State] [int] NOT NULL,
	[FailureMessage] [nvarchar](512) NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
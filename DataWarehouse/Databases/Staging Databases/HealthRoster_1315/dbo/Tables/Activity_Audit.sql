﻿CREATE TABLE [dbo].[Activity_Audit](
	[ActivityID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

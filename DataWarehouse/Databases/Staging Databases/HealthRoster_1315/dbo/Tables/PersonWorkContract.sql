﻿CREATE TABLE [dbo].[PersonWorkContract](
	[PersonWorkContractID] [int] NOT NULL,
	[ContractedTimePerWeek] [int] NOT NULL,
	[DefaultNonEffectiveTime] [int] NULL,
	[WTDOptOut] [bit] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[ContractTypeID] [int] NULL,
	[GradeWorkContractID] [int] NOT NULL,
	[PersonID] [int] NULL,
	[HoursAccountID] [int] NULL,
	[PostingID] [int] NULL,
	[LockVersion] [smallint] NOT NULL,
	[voa_class] [char](1) NOT NULL
) ON [PRIMARY]
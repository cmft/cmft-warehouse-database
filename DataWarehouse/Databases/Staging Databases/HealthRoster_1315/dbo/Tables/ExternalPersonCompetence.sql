﻿CREATE TABLE [dbo].[ExternalPersonCompetence](
	[ExternalPersonCompetenceID] [int] NOT NULL,
	[StaffNumber] [nvarchar](255) NOT NULL,
	[CertificateNumber] [nvarchar](50) NULL,
	[EffectiveFrom] [datetime] NOT NULL,
	[EffectiveTo] [datetime] NULL,
	[DateGained] [datetime] NULL,
	[Information] [nvarchar](512) NULL,
	[State] [int] NOT NULL,
	[FailureMessage] [nvarchar](512) NOT NULL,
	[ExternalCompetenceID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
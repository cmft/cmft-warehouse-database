﻿CREATE TABLE [dbo].[Metric_Audit](
	[MetricID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

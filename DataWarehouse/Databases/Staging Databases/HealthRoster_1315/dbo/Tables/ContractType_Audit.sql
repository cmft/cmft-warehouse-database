﻿CREATE TABLE [dbo].[ContractType_Audit](
	[ContractTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

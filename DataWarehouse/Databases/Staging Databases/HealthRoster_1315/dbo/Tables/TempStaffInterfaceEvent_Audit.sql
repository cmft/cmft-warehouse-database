﻿CREATE TABLE [dbo].[TempStaffInterfaceEvent_Audit](
	[TempStaffInterfaceEventID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

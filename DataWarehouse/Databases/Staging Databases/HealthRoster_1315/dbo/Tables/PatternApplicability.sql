﻿CREATE TABLE [dbo].[PatternApplicability](
	[PatternApplicabilityID] [int] NOT NULL,
	[OccGroupID] [int] NULL,
	[GradeID] [int] NULL,
	[GradeTypeID] [int] NULL,
	[GradeTypeCategoryID] [int] NULL,
	[CompetenceID] [int] NULL,
	[TeamID] [int] NULL,
	[PostingID] [int] NULL,
	[PersonID] [int] NULL,
	[PatternID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
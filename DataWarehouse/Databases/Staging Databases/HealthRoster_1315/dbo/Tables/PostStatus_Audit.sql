﻿CREATE TABLE [dbo].[PostStatus_Audit](
	[PostStatusID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

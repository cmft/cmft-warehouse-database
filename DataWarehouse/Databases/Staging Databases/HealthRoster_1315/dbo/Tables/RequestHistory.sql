﻿CREATE TABLE [dbo].[RequestHistory](
	[RequestHistoryID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[RequestAuditEvent] [int] NOT NULL,
	[IsAfterRosterApproval] [bit] NOT NULL,
	[State] [int] NOT NULL,
	[AuthenticatedID] [int] NOT NULL,
	[AuthorisedID] [int] NOT NULL,
	[DutyRequestID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OrgUnit](
	[OrgUnitID] [int] NOT NULL,
	[Code] [nvarchar](50) NULL,
	[IsBusinessUnit] [bit] NOT NULL,
	[EmploymentType] [int] NOT NULL,
	[ExternalReference] [nvarchar](100) NULL,
	[TempStaffApprovalRequired] [bit] NOT NULL,
	[IncludedInGatewayMatching] [bit] NOT NULL,
	[ActivatedForGateway] [bit] NOT NULL,
	[AwardRuleEngineNotificationMode] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[InformationID] [int] NULL,
	[ExtensionID] [int] NOT NULL,
	[OrgUnitLevelID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
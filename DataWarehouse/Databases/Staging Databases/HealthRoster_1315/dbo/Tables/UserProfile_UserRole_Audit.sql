﻿CREATE TABLE [dbo].[UserProfile_UserRole_Audit](
	[UserProfileID] [int] NOT NULL,
	[UserRoleID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

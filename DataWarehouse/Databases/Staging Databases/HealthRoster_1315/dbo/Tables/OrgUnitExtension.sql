﻿CREATE TABLE [dbo].[OrgUnitExtension](
	[OrgUnitExtensionID] [int] NOT NULL,
	[FinalisationPeriodStart] [datetime] NULL,
	[FinalisationPeriodEnd] [datetime] NULL,
	[FinalisationTaskID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Country](
	[CountryID] [int] NOT NULL,
	[Abbreviation] [nvarchar](3) NOT NULL,
	[IsObsolete] [bit] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OrgUnitExtension_Audit](
	[OrgUnitExtensionID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[AwardRuleEngineNotification](
	[AwardRuleEngineNotificationID] [int] NOT NULL,
	[NotificationType] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NOT NULL,
	[AdditionalInfo] [xml] NULL,
	[Requested] [bit] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
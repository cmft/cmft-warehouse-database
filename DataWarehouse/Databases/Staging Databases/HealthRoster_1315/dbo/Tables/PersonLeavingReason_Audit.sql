﻿CREATE TABLE [dbo].[PersonLeavingReason_Audit](
	[PersonLeavingReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[OrgStruct](
	[OrgStructID] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[ParentOrgUnitID] [int] NOT NULL,
	[SubordinateOrgUnitID] [int] NOT NULL,
	[OrgStructureTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
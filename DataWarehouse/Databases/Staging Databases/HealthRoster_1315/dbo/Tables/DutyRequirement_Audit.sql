﻿CREATE TABLE [dbo].[DutyRequirement_Audit](
	[DutyRequirementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

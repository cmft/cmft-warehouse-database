﻿CREATE TABLE [dbo].[CostElement](
	[CostElementID] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[CostElementDefinitionID] [int] NOT NULL,
	[GradeWorkContractID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ChangeSet_Detail](
	[ChangeSetID] [bigint] NOT NULL,
	[Command] [xml] NULL,
	[Changes] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[OrgUnitBudget_Audit](
	[OrgUnitBudgetID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[OrgUnitPatientType](
	[OrgUnitPatientTypeID] [int] NOT NULL,
	[SpecificHPPD] [money] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[PatientTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WorkRate](
	[WorkRateID] [int] NOT NULL,
	[Code] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[ReportingCategory] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[ESRElementMappingID] [int] NULL,
	[CostElementDefinitionID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
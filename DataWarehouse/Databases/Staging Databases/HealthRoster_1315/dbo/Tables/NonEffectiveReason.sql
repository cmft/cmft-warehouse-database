﻿CREATE TABLE [dbo].[NonEffectiveReason](
	[NonEffectiveReasonID] [int] NOT NULL,
	[ShortName] [nvarchar](10) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
	[NonEffectiveReasonGroup] [int] NOT NULL,
	[CanOverlapBankAgencyDuties] [bit] NOT NULL,
	[Export] [bit] NOT NULL,
	[HoursBasedAbsence] [bit] NOT NULL,
	[ExternalAbsenceType] [nvarchar](100) NULL,
	[ExternalAbsenceCode] [nvarchar](100) NULL,
	[ExternalAbsenceReference] [nvarchar](50) NULL,
	[DefaultTimesheetHours] [int] NULL,
	[DefaultStartTime] [datetime] NULL,
	[DefaultEndTime] [datetime] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
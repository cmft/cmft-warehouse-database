﻿CREATE TABLE [dbo].[CrossReference_Audit](
	[CrossReferenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

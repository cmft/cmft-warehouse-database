﻿CREATE TABLE [dbo].[OrgUnitCensusPeriodPercentage_Audit](
	[OrgUnitCensusPeriodPercentageID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

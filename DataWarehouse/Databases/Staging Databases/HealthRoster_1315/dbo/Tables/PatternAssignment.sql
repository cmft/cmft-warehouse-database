﻿CREATE TABLE [dbo].[PatternAssignment](
	[PatternAssignmentID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[StartingWeekIndex] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[PostingID] [int] NULL,
	[PostID] [int] NULL,
	[ParentPatternAssignmentID] [int] NULL,
	[PatternID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Competence](
	[CompetenceID] [int] NOT NULL,
	[Code] [nvarchar](50) NULL,
	[ExpiryDurationInMonths] [int] NULL,
	[CompetenceType] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[InformationID] [int] NULL,
	[CompetenceClusterID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OrgUnitCensusPeriodPercentage](
	[OrgUnitCensusPeriodPercentageID] [int] NOT NULL,
	[HPPDPercentage] [money] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[DefaultCensusPeriodID] [int] NOT NULL,
	[OrgUnitCensusPeriodSetID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
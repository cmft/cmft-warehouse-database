﻿CREATE TABLE [dbo].[RuleApplicability](
	[RuleApplicabilityID] [int] NOT NULL,
	[PostingID] [int] NULL,
	[OccGroupID] [int] NULL,
	[GradeID] [int] NULL,
	[GradeTypeID] [int] NULL,
	[GradeTypeCategoryID] [int] NULL,
	[CompetenceID] [int] NULL,
	[TeamID] [int] NULL,
	[PersonID] [int] NULL,
	[RosteringRuleID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
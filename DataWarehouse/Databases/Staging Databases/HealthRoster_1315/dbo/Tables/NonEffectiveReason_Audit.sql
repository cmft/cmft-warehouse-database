﻿CREATE TABLE [dbo].[NonEffectiveReason_Audit](
	[NonEffectiveReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

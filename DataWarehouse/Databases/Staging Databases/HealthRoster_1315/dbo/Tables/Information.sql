﻿CREATE TABLE [dbo].[Information](
	[InformationID] [int] NOT NULL,
	[Comments] [nvarchar](512) NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CostCentre](
	[CostCentreID] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
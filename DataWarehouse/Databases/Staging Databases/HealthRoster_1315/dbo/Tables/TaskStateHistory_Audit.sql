﻿CREATE TABLE [dbo].[TaskStateHistory_Audit](
	[TaskStateHistoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

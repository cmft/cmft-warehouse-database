﻿CREATE TABLE [dbo].[ExternalTitle_Audit](
	[ExternalTitleID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

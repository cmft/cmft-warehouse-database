﻿CREATE TABLE [dbo].[TempStaffAvailability_Audit](
	[TempStaffAvailabilityID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

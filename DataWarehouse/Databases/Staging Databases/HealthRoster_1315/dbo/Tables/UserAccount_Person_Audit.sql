﻿CREATE TABLE [dbo].[UserAccount_Person_Audit](
	[UserAccountID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

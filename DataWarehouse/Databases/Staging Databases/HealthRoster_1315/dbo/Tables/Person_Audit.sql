﻿CREATE TABLE [dbo].[Person_Audit](
	[PersonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

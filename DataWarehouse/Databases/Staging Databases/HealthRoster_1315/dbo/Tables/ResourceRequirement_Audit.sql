﻿CREATE TABLE [dbo].[ResourceRequirement_Audit](
	[ResourceRequirementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

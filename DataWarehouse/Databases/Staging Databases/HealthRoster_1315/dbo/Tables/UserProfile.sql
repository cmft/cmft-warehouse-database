﻿CREATE TABLE [dbo].[UserProfile](
	[UserProfileID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
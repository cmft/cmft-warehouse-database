﻿CREATE TABLE [dbo].[UserAccount](
	[UserAccountID] [int] NOT NULL,
	[LoginName] [nvarchar](255) NOT NULL,
	[DisplayName] [nvarchar](255) NULL,
	[ConsecutiveFailures] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[LoginNameNotifiedOn] [datetime] NULL,
	[SuspensionDate] [datetime] NULL,
	[CanImpersonate] [bit] NOT NULL,
	[EmailAddress] [nvarchar](255) NULL,
	[ExternalIdentity] [nvarchar](255) NULL,
	[AuthenticationType] [tinyint] NOT NULL,
	[IsSystem] [bit] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[UserProfileID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
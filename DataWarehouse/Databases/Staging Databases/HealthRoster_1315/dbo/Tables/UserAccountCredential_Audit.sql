﻿CREATE TABLE [dbo].[UserAccountCredential_Audit](
	[UserAccountCredentialID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

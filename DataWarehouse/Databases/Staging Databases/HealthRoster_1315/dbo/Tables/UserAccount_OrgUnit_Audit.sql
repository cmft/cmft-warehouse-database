﻿CREATE TABLE [dbo].[UserAccount_OrgUnit_Audit](
	[UserAccountID] [int] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

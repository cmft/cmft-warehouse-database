﻿CREATE TABLE [dbo].[DutyAuditHistory](
	[DutyAuditHistoryID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[DutyAuditEvent] [int] NOT NULL,
	[IsAfterRosterApproval] [bit] NOT NULL,
	[DutyAuditDetails] [xml] NULL,
	[AuthenticatedID] [int] NOT NULL,
	[AuthorisedID] [int] NOT NULL,
	[DutyID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
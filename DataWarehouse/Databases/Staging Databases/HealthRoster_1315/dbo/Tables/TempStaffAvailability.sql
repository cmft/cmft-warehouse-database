﻿CREATE TABLE [dbo].[TempStaffAvailability](
	[TempStaffAvailabilityID] [int] NOT NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NOT NULL,
	[Comments] [nvarchar](512) NULL,
	[ValidDate] [datetime] NOT NULL,
	[PersonID] [int] NOT NULL,
	[TempStaffDefaultAvailabilityID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
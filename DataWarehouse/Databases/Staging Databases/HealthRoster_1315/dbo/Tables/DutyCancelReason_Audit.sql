﻿CREATE TABLE [dbo].[DutyCancelReason_Audit](
	[DutyCancelReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[UserAccount_Audit](
	[UserAccountID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

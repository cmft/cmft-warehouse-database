﻿CREATE TABLE [dbo].[Task_Audit](
	[TaskID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[Setting](
	[SettingID] [int] NOT NULL,
	[SettingType] [int] NOT NULL,
	[SettingValue] [nvarchar](1024) NULL,
	[Category] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
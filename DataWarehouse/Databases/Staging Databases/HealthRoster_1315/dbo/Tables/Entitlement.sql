﻿CREATE TABLE [dbo].[Entitlement](
	[EntitlementID] [int] NOT NULL,
	[EntitlementType] [int] NOT NULL,
	[EntitlementBasis] [int] NOT NULL,
	[EntitlementMins] [int] NOT NULL,
	[EntitlementDays] [money] NOT NULL,
	[EntitlementUnits] [int] NOT NULL,
	[PeriodStart] [datetime] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[PersonID] [int] NULL,
	[PostingID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
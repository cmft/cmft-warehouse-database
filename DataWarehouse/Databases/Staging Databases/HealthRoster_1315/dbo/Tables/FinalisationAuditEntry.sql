﻿CREATE TABLE [dbo].[FinalisationAuditEntry](
	[FinalisationAuditEntryID] [int] NOT NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NOT NULL,
	[IncludeBank] [bit] NOT NULL,
	[IncludeSubstantive] [bit] NOT NULL,
	[AwardsInterpretationState] [int] NOT NULL,
	[RunAt] [datetime] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[TeamID] [int] NULL,
	[UserAccountID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CategoryType](
	[CategoryTypeID] [int] NOT NULL,
	[DisplayName] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[OrgUnitLevelID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
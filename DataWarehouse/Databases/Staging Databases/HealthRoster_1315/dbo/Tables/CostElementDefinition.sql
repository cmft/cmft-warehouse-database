﻿CREATE TABLE [dbo].[CostElementDefinition](
	[CostElementDefinitionID] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[Basis] [int] NOT NULL,
	[IsObsolete] [bit] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
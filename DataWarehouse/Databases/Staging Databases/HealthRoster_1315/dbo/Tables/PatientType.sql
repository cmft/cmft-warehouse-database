﻿CREATE TABLE [dbo].[PatientType](
	[PatientTypeID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[HPPDMultiplier] [money] NOT NULL,
	[RequiresUnitSpecificHPPD] [bit] NOT NULL,
	[ExternalReference] [nvarchar](100) NULL,
	[SortOrder] [int] NULL,
	[IsObsolete] [bit] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
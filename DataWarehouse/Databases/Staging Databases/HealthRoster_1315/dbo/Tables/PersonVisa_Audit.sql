﻿CREATE TABLE [dbo].[PersonVisa_Audit](
	[PersonVisaID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[TimesheetEntryReason](
	[TimesheetEntryReasonID] [int] NOT NULL,
	[IsObsolete] [bit] NOT NULL,
	[TimesheetEntryReasonType] [int] NOT NULL,
	[IsRosteredReason] [bit] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
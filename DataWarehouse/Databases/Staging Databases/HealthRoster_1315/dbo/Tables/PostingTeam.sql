﻿CREATE TABLE [dbo].[PostingTeam](
	[PostingTeamID] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[TeamID] [int] NULL,
	[PostingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
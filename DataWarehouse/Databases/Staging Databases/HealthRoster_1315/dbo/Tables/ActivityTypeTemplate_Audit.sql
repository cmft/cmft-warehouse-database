﻿CREATE TABLE [dbo].[ActivityTypeTemplate_Audit](
	[ActivityTypeTemplateID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

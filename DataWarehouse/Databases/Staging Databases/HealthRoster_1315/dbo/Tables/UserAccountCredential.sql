﻿CREATE TABLE [dbo].[UserAccountCredential](
	[UserAccountCredentialID] [int] NOT NULL,
	[HashedPassword] [nvarchar](255) NOT NULL,
	[ActionRequired] [tinyint] NOT NULL,
	[PasswordSalt] [nvarchar](255) NULL,
	[PasswordNotified] [datetime] NULL,
	[PasswordAlgorithm] [tinyint] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[UserAccountID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ZircadianUnitMapping](
	[ZircadianUnitMappingID] [int] NOT NULL,
	[ZircadianID] [nvarchar](255) NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
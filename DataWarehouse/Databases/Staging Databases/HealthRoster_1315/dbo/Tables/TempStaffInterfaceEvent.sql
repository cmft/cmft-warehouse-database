﻿CREATE TABLE [dbo].[TempStaffInterfaceEvent](
	[TempStaffInterfaceEventID] [int] NOT NULL,
	[OccuredOn] [datetime] NOT NULL,
	[EventType] [int] NOT NULL,
	[Information] [xml] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
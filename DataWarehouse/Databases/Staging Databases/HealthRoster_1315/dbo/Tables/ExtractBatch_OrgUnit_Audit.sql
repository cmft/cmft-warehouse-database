﻿CREATE TABLE [dbo].[ExtractBatch_OrgUnit_Audit](
	[ExtractBatchID] [int] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[Note_Audit](
	[NoteID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

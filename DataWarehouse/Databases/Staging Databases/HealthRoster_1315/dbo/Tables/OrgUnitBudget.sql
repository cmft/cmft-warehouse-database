﻿CREATE TABLE [dbo].[OrgUnitBudget](
	[OrgUnitBudgetID] [int] NOT NULL,
	[AnnualBudget] [money] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
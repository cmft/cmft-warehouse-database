﻿CREATE TABLE [dbo].[UserRolePermission](
	[UserRolePermissionID] [int] NOT NULL,
	[AccessControlGroup] [int] NOT NULL,
	[UserRoleID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NotificationRequest_Audit](
	[NotificationRequestID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

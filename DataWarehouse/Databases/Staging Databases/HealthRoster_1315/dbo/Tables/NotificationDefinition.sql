﻿CREATE TABLE [dbo].[NotificationDefinition](
	[NotificationDefinitionID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [int] NOT NULL,
	[Recipient] [int] NOT NULL,
	[PrimaryEntity] [int] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[TemplateType] [int] NOT NULL,
	[FixedAddress] [nvarchar](255) NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
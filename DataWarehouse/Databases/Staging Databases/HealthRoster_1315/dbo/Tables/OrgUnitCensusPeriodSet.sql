﻿CREATE TABLE [dbo].[OrgUnitCensusPeriodSet](
	[OrgUnitCensusPeriodSetID] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[DefaultCensusPeriodSetID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
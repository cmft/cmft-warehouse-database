﻿CREATE TABLE [dbo].[ExternalUnit_Audit](
	[ExternalUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

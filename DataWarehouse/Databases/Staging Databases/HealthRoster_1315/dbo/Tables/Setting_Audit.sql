﻿CREATE TABLE [dbo].[Setting_Audit](
	[SettingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[DefaultCensusPeriod](
	[DefaultCensusPeriodID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Start] [datetime] NOT NULL,
	[DefaultCensusPeriodSetID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Grade_Audit](
	[GradeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

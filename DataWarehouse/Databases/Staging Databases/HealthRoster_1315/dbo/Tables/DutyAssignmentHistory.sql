﻿CREATE TABLE [dbo].[DutyAssignmentHistory](
	[DutyAssignmentHistoryID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[DutyAuditEvent] [int] NOT NULL,
	[IsAfterRosterApproval] [bit] NOT NULL,
	[AssignmentAuditDetails] [xml] NULL,
	[AuthenticatedID] [int] NOT NULL,
	[AuthorisedID] [int] NOT NULL,
	[DutyAssignmentID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
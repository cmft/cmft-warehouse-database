﻿CREATE TABLE [dbo].[TimesheetSlot](
	[TimesheetSlotID] [int] NOT NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[ResourceRequirementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NotificationTemplate_Audit](
	[NotificationTemplateID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

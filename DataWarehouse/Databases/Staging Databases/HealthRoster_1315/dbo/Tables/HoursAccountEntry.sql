﻿CREATE TABLE [dbo].[HoursAccountEntry](
	[HoursAccountEntryID] [int] NOT NULL,
	[Balance] [int] NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[BalanceType] [int] NOT NULL,
	[HoursAccountID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
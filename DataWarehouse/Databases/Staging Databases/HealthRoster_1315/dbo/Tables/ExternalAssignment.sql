﻿CREATE TABLE [dbo].[ExternalAssignment](
	[ExternalAssignmentID] [int] NOT NULL,
	[AssignmentNumber] [nvarchar](255) NOT NULL,
	[Position] [nvarchar](255) NULL,
	[ContractedTimePerWeek] [int] NOT NULL,
	[PrimaryAssignment] [bit] NOT NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NOT NULL,
	[Defunct] [bit] NOT NULL,
	[ExternalPersonID] [int] NOT NULL,
	[ExternalUnitID] [int] NOT NULL,
	[ExternalGradeID] [int] NOT NULL,
	[MatchedPostingID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
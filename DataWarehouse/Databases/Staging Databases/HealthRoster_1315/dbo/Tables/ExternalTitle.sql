﻿CREATE TABLE [dbo].[ExternalTitle](
	[ExternalTitleID] [int] NOT NULL,
	[ExternalName] [nvarchar](255) NOT NULL,
	[MatchedTitleID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
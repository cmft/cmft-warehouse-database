﻿CREATE TABLE [dbo].[CostElementDefinition_Audit](
	[CostElementDefinitionID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

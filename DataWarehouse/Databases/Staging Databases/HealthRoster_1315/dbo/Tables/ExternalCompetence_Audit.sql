﻿CREATE TABLE [dbo].[ExternalCompetence_Audit](
	[ExternalCompetenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

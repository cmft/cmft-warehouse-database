﻿CREATE TABLE [dbo].[TempStaffDefaultAvailability](
	[TempStaffDefaultAvailabilityID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ShortName] [nvarchar](20) NOT NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NOT NULL,
	[WorkTime] [int] NOT NULL,
	[AvailabilityType] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ExternalUnit_OrgUnit_Audit](
	[ExternalUnitID] [int] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

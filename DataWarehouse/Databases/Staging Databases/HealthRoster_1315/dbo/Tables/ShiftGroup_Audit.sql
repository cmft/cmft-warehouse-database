﻿CREATE TABLE [dbo].[ShiftGroup_Audit](
	[ShiftGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[TempStaffBookingReason_Audit](
	[TempStaffBookingReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

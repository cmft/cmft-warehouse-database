﻿CREATE TABLE [dbo].[UserPreference_Audit](
	[UserPreferenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

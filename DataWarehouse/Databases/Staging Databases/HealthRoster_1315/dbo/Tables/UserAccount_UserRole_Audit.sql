﻿CREATE TABLE [dbo].[UserAccount_UserRole_Audit](
	[UserAccountID] [int] NOT NULL,
	[UserRoleID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

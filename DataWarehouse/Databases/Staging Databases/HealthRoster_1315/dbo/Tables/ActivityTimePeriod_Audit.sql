﻿CREATE TABLE [dbo].[ActivityTimePeriod_Audit](
	[ActivityTimePeriodID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[JobPlanMapping](
	[JobPlanMappingID] [int] NOT NULL,
	[ZircadianID] [uniqueidentifier] NOT NULL,
	[DoctorMappingID] [int] NOT NULL,
	[PatternAssignmentID] [int] NOT NULL,
	[PostingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
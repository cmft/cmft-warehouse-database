﻿CREATE TABLE [dbo].[PatientCensusPeriod](
	[PatientCensusPeriodID] [int] NOT NULL,
	[ValidDate] [datetime] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[DefaultCensusPeriodID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
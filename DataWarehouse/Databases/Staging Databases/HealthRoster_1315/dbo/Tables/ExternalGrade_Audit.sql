﻿CREATE TABLE [dbo].[ExternalGrade_Audit](
	[ExternalGradeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[RuleApplicability_Audit](
	[RuleApplicabilityID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

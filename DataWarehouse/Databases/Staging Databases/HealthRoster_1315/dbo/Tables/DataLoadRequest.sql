﻿CREATE TABLE [dbo].[DataLoadRequest](
	[DataLoadRequestID] [int] NOT NULL,
	[SourceFilename] [nvarchar](260) NOT NULL,
	[RequestType] [int] NOT NULL,
	[RequestState] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[TempFile] [nvarchar](260) NULL,
	[ErrorOutput] [xml] NULL,
	[UserAccountID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
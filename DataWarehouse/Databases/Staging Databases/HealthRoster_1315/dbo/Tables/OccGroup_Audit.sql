﻿CREATE TABLE [dbo].[OccGroup_Audit](
	[OccGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

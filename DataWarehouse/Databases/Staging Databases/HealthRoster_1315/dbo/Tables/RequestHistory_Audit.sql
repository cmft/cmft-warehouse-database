﻿CREATE TABLE [dbo].[RequestHistory_Audit](
	[RequestHistoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[GradeType](
	[GradeTypeID] [int] NOT NULL,
	[DefaultWTE] [int] NULL,
	[HeadroomPercentage] [money] NULL,
	[UsedInHPPDCalculation] [bit] NOT NULL,
	[SetCancelDemandOnAddEditUnavailability] [bit] NOT NULL,
	[IsTraining] [bit] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[GradeTypeCategoryID] [int] NOT NULL,
	[DefaultBankGradeForCostingID] [int] NULL,
	[DefaultAgencyGradeForCostingID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
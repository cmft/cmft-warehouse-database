﻿CREATE TABLE [dbo].[OrgUnitCostCentre](
	[OrgUnitCostCentreID] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[CostCentreID] [int] NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
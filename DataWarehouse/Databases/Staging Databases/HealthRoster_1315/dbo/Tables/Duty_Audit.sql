﻿CREATE TABLE [dbo].[Duty_Audit](
	[DutyID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

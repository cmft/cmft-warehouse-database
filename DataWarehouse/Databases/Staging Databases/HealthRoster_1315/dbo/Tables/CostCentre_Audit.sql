﻿CREATE TABLE [dbo].[CostCentre_Audit](
	[CostCentreID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

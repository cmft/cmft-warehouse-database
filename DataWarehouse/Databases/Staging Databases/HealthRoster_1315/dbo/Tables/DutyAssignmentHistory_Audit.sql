﻿CREATE TABLE [dbo].[DutyAssignmentHistory_Audit](
	[DutyAssignmentHistoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[PersonEmploymentStatusHistory_Audit](
	[PersonEmploymentStatusHistoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

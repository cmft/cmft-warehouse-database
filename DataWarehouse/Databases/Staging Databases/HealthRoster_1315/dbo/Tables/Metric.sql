﻿CREATE TABLE [dbo].[Metric](
	[MetricID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
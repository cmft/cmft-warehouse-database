﻿CREATE TABLE [dbo].[GlobalWorkContract](
	[GlobalWorkContractID] [int] NOT NULL,
	[ContractedTimePerWeek] [int] NOT NULL,
	[DefaultNonEffectiveTime] [int] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[EmploymentType] [int] NULL,
	[CanRecordContractedActivities] [bit] NULL,
	[EnsureCancelledDutiesPresentDuringAnnualLeave] [bit] NULL,
	[GlobalWorkingRestrictionSetID] [int] NULL,
	[PayChannelID] [int] NULL,
	[EnterpriseWorkContractID] [int] NULL,
	[GradeID] [int] NULL,
	[LockVersion] [smallint] NOT NULL,
	[voa_class] [char](1) NOT NULL
) ON [PRIMARY]
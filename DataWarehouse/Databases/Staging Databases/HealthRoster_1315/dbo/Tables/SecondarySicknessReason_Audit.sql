﻿CREATE TABLE [dbo].[SecondarySicknessReason_Audit](
	[SecondarySicknessReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

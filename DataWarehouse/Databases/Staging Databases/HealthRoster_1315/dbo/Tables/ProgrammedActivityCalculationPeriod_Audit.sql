﻿CREATE TABLE [dbo].[ProgrammedActivityCalculationPeriod_Audit](
	[ProgrammedActivityCalculationPeriodID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[ExternalAssignmentChange_Audit](
	[ExternalAssignmentChangeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

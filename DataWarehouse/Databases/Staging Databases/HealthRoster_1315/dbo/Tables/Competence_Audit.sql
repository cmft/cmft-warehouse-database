﻿CREATE TABLE [dbo].[Competence_Audit](
	[CompetenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

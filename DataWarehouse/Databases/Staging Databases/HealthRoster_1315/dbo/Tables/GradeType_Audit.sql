﻿CREATE TABLE [dbo].[GradeType_Audit](
	[GradeTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

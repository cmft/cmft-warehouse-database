﻿CREATE TABLE [dbo].[WorkingDirective](
	[WorkingDirectiveID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[WeeksInRefPeriod] [int] NOT NULL,
	[MinContinuousRestPerDay] [int] NOT NULL,
	[UpperWeeklyWorkTimeThreshold] [int] NOT NULL,
	[LowerWeeklyWorkTimeThreshold] [int] NOT NULL,
	[UpperAvgWeeklyWorkTimeThreshold] [int] NOT NULL,
	[LowerAvgWeeklyWorkTimeThreshold] [int] NOT NULL,
	[MinRestPeriodThreshold1] [int] NOT NULL,
	[MinRestPeriodThreshold2] [int] NOT NULL,
	[MinRestDayThreshold1] [int] NOT NULL,
	[MinRestDayThreshold2] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[OrgUnitID] [int] NULL,
	[GradeTypeID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
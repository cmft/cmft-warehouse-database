﻿CREATE TABLE [dbo].[UserPreference](
	[UserPreferenceID] [int] NOT NULL,
	[PanelId] [nvarchar](255) NOT NULL,
	[Preferences] [xml] NULL,
	[UserAccountID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
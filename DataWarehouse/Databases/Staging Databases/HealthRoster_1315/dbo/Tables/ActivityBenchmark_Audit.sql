﻿CREATE TABLE [dbo].[ActivityBenchmark_Audit](
	[ActivityBenchmarkID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[OrgUnitPatientType_Audit](
	[OrgUnitPatientTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

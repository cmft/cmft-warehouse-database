﻿CREATE TABLE [dbo].[PayPeriod](
	[PayPeriodID] [int] NOT NULL,
	[PeriodNumber] [int] NOT NULL,
	[PeriodState] [int] NOT NULL,
	[ScheduledPeriodCutOffDate] [datetime] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[PayChannelID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ExtractLogArchiveEntry_Audit](
	[ExtractLogArchiveEntryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

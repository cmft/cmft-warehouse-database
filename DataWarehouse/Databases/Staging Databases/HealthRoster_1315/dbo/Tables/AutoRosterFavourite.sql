﻿CREATE TABLE [dbo].[AutoRosterFavourite](
	[AutoRosterFavouriteID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Icon] [int] NOT NULL,
	[AutoRosterSettings] [xml] NULL,
	[UserAccountID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
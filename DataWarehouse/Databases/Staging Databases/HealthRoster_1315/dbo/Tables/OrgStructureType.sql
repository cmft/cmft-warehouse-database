﻿CREATE TABLE [dbo].[OrgStructureType](
	[OrgStructureTypeID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
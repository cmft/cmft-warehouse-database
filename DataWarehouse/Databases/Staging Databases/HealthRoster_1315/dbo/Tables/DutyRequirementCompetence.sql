﻿CREATE TABLE [dbo].[DutyRequirementCompetence](
	[DutyRequirementCompetenceID] [int] NOT NULL,
	[IsMandatory] [bit] NOT NULL,
	[MinRequired] [int] NULL,
	[CompetenceID] [int] NOT NULL,
	[DutyRequirementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Activity](
	[ActivityID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[StartOffset] [int] NOT NULL,
	[ValidDate] [datetime] NOT NULL,
	[DutyCancelReasonID] [int] NULL,
	[AdditionalDutyReasonID] [int] NULL,
	[ActivityTypeTemplateID] [int] NULL,
	[AdHocActivityReasonID] [int] NULL,
	[RosterID] [int] NOT NULL,
	[RosterLocationID] [int] NULL,
	[ActivityTypeID] [int] NOT NULL,
	[PatternAssignmentID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
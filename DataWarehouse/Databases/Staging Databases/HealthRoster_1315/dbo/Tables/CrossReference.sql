﻿CREATE TABLE [dbo].[CrossReference](
	[CrossReferenceID] [int] NOT NULL,
	[EntityName] [nvarchar](50) NOT NULL,
	[EntityID] [int] NOT NULL,
	[MapsTable] [nvarchar](255) NOT NULL,
	[MapsKey] [nvarchar](256) NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PersonNonEffectivePeriodHistory_Audit](
	[PersonNonEffectivePeriodHistoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

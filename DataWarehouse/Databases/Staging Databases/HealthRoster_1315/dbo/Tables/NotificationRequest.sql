﻿CREATE TABLE [dbo].[NotificationRequest](
	[NotificationRequestID] [int] NOT NULL,
	[To] [nvarchar](255) NULL,
	[EntityID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[RequestedAt] [datetime] NOT NULL,
	[SentAt] [datetime] NULL,
	[AdditionalData] [xml] NULL,
	[Results] [xml] NULL,
	[NotificationDefinitionID] [int] NOT NULL,
	[SenderID] [int] NOT NULL,
	[RecipientUserID] [int] NULL,
	[RecipientPersonID] [int] NULL,
	[TaskID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
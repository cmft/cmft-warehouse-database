﻿CREATE TABLE [dbo].[RosterApprovalHistory](
	[RosterApprovalHistoryID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[ApprovalStatus] [int] NOT NULL,
	[Comments] [nvarchar](255) NULL,
	[AuthorisedUserAccountID] [int] NOT NULL,
	[AuthenticatedUserAccountID] [int] NOT NULL,
	[RosterID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
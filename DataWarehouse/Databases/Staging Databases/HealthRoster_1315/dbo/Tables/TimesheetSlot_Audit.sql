﻿CREATE TABLE [dbo].[TimesheetSlot_Audit](
	[TimesheetSlotID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[DutyChangeReason_Audit](
	[DutyChangeReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

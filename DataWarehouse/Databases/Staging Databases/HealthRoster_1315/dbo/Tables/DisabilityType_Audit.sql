﻿CREATE TABLE [dbo].[DisabilityType_Audit](
	[DisabilityTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[ExternalAssignmentChange](
	[ExternalAssignmentChangeID] [int] NOT NULL,
	[ContractedTimePerWeek] [int] NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[State] [int] NOT NULL,
	[FailureMessage] [nvarchar](512) NOT NULL,
	[NewEndDate] [datetime] NULL,
	[ExternalAssignmentID] [int] NULL,
	[ExternalGradeID] [int] NULL,
	[ExternalUnitID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AssignmentSyncronisationStaging_Audit](
	[AssignmentSyncronisationStagingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[ActivityBenchmark](
	[ActivityBenchmarkID] [int] NOT NULL,
	[ShortCode] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[IsObsolete] [bit] NOT NULL,
	[ActivityCategoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OrgUnit_ActivityType_Audit](
	[OrgUnitID] [int] NOT NULL,
	[ActivityTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[PatternAssignmentDemandGroup_Audit](
	[PatternAssignmentDemandGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

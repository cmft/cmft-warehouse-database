﻿CREATE TABLE [dbo].[OrgUnitBaseHPPD_Audit](
	[OrgUnitBaseHPPDID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

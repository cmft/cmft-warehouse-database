﻿CREATE TABLE [dbo].[TaskProgress](
	[TaskProgressID] [int] NOT NULL,
	[Progress] [xml] NULL,
	[On] [datetime] NOT NULL,
	[TaskID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
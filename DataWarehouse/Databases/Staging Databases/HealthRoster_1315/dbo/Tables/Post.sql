﻿CREATE TABLE [dbo].[Post](
	[PostID] [int] NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[PrimaryGradeWorkContractID] [int] NOT NULL,
	[CostCentreID] [int] NULL,
	[OriginalPostID] [int] NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[JobPlanMapping_Audit](
	[JobPlanMappingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[PersonVisa](
	[PersonVisaID] [int] NOT NULL,
	[Number] [nvarchar](50) NOT NULL,
	[IssueingOffice] [nvarchar](100) NOT NULL,
	[VisaType] [int] NOT NULL,
	[Entries] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[CountryID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
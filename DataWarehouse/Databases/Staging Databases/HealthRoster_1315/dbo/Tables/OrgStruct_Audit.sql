﻿CREATE TABLE [dbo].[OrgStruct_Audit](
	[OrgStructID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

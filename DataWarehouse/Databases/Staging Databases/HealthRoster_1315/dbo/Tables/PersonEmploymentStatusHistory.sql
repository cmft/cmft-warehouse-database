﻿CREATE TABLE [dbo].[PersonEmploymentStatusHistory](
	[PersonEmploymentStatusHistoryID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[EmploymentStatus] [int] NOT NULL,
	[AuthorisedUserAccountID] [int] NOT NULL,
	[AuthenticatedUserAccountID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ExternalPersonCompetence_Audit](
	[ExternalPersonCompetenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

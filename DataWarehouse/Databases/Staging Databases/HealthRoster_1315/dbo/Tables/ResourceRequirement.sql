﻿CREATE TABLE [dbo].[ResourceRequirement](
	[ResourceRequirementID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ResourceRequirementType] [int] NOT NULL,
	[HeadroomPercentage] [money] NULL,
	[InCharge] [bit] NOT NULL,
	[GradeID] [int] NULL,
	[GradeTypeID] [int] NULL,
	[GradeTypeCategoryID] [int] NULL,
	[CostCentreID] [int] NULL,
	[RosterLocationID] [int] NULL,
	[TeamID] [int] NULL,
	[RosterID] [int] NULL,
	[RosterTemplateID] [int] NULL,
	[ResourcingOrgUnitID] [int] NOT NULL,
	[PatternAssignmentID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
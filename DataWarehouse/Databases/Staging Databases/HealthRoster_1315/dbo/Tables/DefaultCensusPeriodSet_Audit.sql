﻿CREATE TABLE [dbo].[DefaultCensusPeriodSet_Audit](
	[DefaultCensusPeriodSetID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

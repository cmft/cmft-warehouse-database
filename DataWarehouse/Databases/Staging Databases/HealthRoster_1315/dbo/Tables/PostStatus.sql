﻿CREATE TABLE [dbo].[PostStatus](
	[PostStatusID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsBudgeted] [bit] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[PostID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CategoryType_Audit](
	[CategoryTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

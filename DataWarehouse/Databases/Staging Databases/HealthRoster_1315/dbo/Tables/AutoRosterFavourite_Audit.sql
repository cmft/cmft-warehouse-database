﻿CREATE TABLE [dbo].[AutoRosterFavourite_Audit](
	[AutoRosterFavouriteID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

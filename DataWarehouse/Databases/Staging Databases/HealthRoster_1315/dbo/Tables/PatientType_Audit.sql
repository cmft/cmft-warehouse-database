﻿CREATE TABLE [dbo].[PatientType_Audit](
	[PatientTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

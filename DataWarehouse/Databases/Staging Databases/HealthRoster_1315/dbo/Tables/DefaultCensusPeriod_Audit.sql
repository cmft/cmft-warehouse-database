﻿CREATE TABLE [dbo].[DefaultCensusPeriod_Audit](
	[DefaultCensusPeriodID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

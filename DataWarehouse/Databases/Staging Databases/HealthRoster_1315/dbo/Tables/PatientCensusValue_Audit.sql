﻿CREATE TABLE [dbo].[PatientCensusValue_Audit](
	[PatientCensusValueID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[DutyAuditHistory_Audit](
	[DutyAuditHistoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

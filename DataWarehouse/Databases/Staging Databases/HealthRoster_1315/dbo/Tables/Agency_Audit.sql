﻿CREATE TABLE [dbo].[Agency_Audit](
	[AgencyID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[PostingTeam_Audit](
	[PostingTeamID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

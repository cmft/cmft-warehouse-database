﻿CREATE TABLE [dbo].[PatternAssignmentDemandGroup](
	[PatternAssignmentDemandGroupID] [int] NOT NULL,
	[ValidDate] [datetime] NOT NULL,
	[PatternAssignmentID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
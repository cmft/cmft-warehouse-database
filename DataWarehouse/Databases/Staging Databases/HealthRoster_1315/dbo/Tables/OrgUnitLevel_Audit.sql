﻿CREATE TABLE [dbo].[OrgUnitLevel_Audit](
	[OrgUnitLevelID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

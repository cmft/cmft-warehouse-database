﻿CREATE TABLE [dbo].[ActivityType_Audit](
	[ActivityTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[ChangeSet](
	[ChangeSetID] [bigint] NOT NULL,
	[UserSessionID] [int] NOT NULL,
	[CommandTypeID] [int] NOT NULL,
	[Created] [datetime2](2) NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[WorkRate_Audit](
	[WorkRateID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

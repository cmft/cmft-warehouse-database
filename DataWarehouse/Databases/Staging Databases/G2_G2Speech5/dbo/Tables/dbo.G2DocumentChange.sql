﻿CREATE TABLE [dbo].[G2DocumentChange](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DocumentID] [int] NOT NULL,
	[ActionID] [int] NOT NULL,
	[OldValue] [varchar](50) NULL,
	[NewValue] [varchar](50) NULL,
	[UserID] [int] NULL,
	[WorkstationID] [int] NULL,
	[ApplicationID] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_G2DocumentChange_TimeStamp]  DEFAULT (getdate())
) ON [PRIMARY]
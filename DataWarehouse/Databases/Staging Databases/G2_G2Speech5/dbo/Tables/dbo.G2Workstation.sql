﻿CREATE TABLE [dbo].[G2Workstation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Serialport] [varchar](50) NULL,
	[Tempfilelocation] [varchar](256) NULL,
	[InstalledAuth] [int] NULL CONSTRAINT [DF_G2Workstation_InstalledAuth]  DEFAULT ((1)),
	[DefaultLanguage] [int] NULL CONSTRAINT [DF_G2Workstation_DefaultLanguage]  DEFAULT ((2)),
	[LogPath] [varchar](256) NULL,
	[Reserved01] [varchar](100) NULL,
	[Reserved02] [varchar](100) NULL,
	[Reserved03] [varchar](100) NULL,
	[Reserved04] [varchar](100) NULL,
	[Reserved05] [varchar](100) NULL,
	[UserDefined01] [varchar](100) NULL,
	[UserDefined02] [varchar](100) NULL,
	[UserDefined03] [varchar](100) NULL,
	[UserDefined04] [varchar](100) NULL,
	[UserDefined05] [varchar](100) NULL
) ON [PRIMARY]
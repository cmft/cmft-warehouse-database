﻿CREATE TABLE [dbo].[G2SiteConfig](
	[SiteID] [int] NOT NULL,
	[KeyName] [varchar](50) NOT NULL,
	[KeyValue] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[G2StatusChange](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DocumentID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[UserID] [int] NULL,
	[WorkStationID] [int] NULL,
	[ApplicationID] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_G2StatusChange_TimeStamp]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[G2UpdateLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Occurred] [datetime] NULL CONSTRAINT [DF_G2UpdateLog_Occurred]  DEFAULT (getdate()),
	[ScriptVersion] [varchar](50) NULL,
	[ErrorMessage] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
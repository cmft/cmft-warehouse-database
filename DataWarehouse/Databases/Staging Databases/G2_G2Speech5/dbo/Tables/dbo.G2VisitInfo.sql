﻿CREATE TABLE [dbo].[G2VisitInfo](
	[DocumentID] [int] NOT NULL,
	[AdmissionDate] [datetime] NULL,
	[VisitDate] [datetime] NULL,
	[DischargeDate] [datetime] NULL
) ON [PRIMARY]
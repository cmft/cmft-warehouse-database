﻿CREATE TABLE [dbo].[RI_G2Person](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[External_ID] [varchar](50) NOT NULL,
	[Type] [int] NULL,
	[Title] [varchar](50) NULL,
	[Initials] [varchar](50) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](100) NULL,
	[MaidenName] [varchar](50) NULL,
	[Sex] [varchar](50) NULL,
	[DateOfBirth] [datetime] NULL,
	[Street] [varchar](100) NULL,
	[HouseNumber] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[State_Province] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[PostalCode] [varchar](50) NULL,
	[HomeTelephoneNumber] [varchar](25) NULL,
	[MobileTelephoneNumber] [varchar](25) NULL,
	[Email] [varchar](100) NULL,
	[OG_Number] [varchar](50) NULL,
	[Alias] [varchar](50) NULL,
	[Specialism] [varchar](50) NULL,
	[Phoneem] [varchar](50) NULL,
	[UserDefined01] [varchar](500) NULL,
	[UserDefined02] [varchar](500) NULL,
	[UserDefined03] [varchar](500) NULL,
	[UserDefined04] [varchar](500) NULL,
	[UserDefined05] [varchar](500) NULL,
	[UserDefined06] [varchar](500) NULL,
	[UserDefined07] [varchar](500) NULL,
	[UserDefined08] [varchar](500) NULL,
	[UserDefined09] [varchar](500) NULL,
	[UserDefined10] [varchar](500) NULL,
	[Inserted] [datetime] NULL,
	[Reserved01] [varchar](100) NULL,
	[Reserved02] [varchar](100) NULL,
	[Reserved03] [varchar](100) NULL,
	[Reserved04] [varchar](100) NULL,
	[Reserved05] [varchar](100) NULL
) ON [PRIMARY]
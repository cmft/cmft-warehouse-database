﻿CREATE TABLE [dbo].[G2DocumentType](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Reserved01] [varchar](100) NULL,
	[Reserved02] [varchar](100) NULL,
	[Reserved03] [varchar](100) NULL,
	[Reserved04] [varchar](100) NULL,
	[Reserved05] [varchar](100) NULL,
	[UserDefined01] [varchar](100) NULL,
	[UserDefined02] [varchar](100) NULL,
	[UserDefined03] [varchar](100) NULL,
	[UserDefined04] [varchar](100) NULL,
	[UserDefined05] [varchar](100) NULL
) ON [PRIMARY]
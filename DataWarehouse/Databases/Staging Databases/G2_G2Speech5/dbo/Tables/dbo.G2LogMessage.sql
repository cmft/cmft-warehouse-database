﻿CREATE TABLE [dbo].[G2LogMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[date_time] [datetime] NULL,
	[message] [char](255) NULL,
	[WorkstationID] [int] NULL,
	[UserID] [int] NULL,
	[DocumentID] [int] NULL,
	[errornr] [int] NULL,
	[MessageType] [char](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RI_G2Text](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LinkTableID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[RequestID] [int] NULL,
	[BlobID] [int] NULL,
	[Status] [int] NULL,
	[TextFormat] [int] NOT NULL,
	[TextType] [int] NOT NULL,
	[TextValue] [text] NOT NULL,
	[ReferencePointer] [varchar](255) NULL,
	[Inserted] [datetime] NULL,
	[Reserved01] [varchar](100) NULL,
	[Reserved02] [varchar](100) NULL,
	[Reserved03] [varchar](100) NULL,
	[Reserved04] [varchar](100) NULL,
	[Reserved05] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
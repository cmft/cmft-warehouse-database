﻿CREATE TABLE [dbo].[RI_G2DocumentRequest](
	[DocumentID] [int] NOT NULL,
	[RequestID] [int] NOT NULL,
	[MessageID] [varchar](50) NULL,
	[Reserved01] [varchar](100) NULL,
	[Reserved02] [varchar](100) NULL,
	[Reserved03] [varchar](100) NULL,
	[Reserved04] [varchar](100) NULL,
	[Reserved05] [varchar](100) NULL
) ON [PRIMARY]
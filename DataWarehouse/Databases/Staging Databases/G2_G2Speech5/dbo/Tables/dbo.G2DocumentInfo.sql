﻿CREATE TABLE [dbo].[G2DocumentInfo](
	[DocumentID] [int] NOT NULL,
	[Running] [datetime] NULL,
	[DictationTime] [float] NULL,
	[CorrectionTime] [float] NULL,
	[PassThroughTime] [float] NULL,
	[SmSoundQualityAcceptable] [bit] NULL,
	[SmClippingRatio] [float] NULL,
	[SmSignalLevel] [int] NULL,
	[SmSignalToNoiseRatio] [int] NULL,
	[TextCharacterCount] [int] NULL,
	[TextWordCount] [int] NULL
) ON [PRIMARY]
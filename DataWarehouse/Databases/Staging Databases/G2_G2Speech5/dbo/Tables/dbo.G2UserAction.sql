﻿CREATE TABLE [dbo].[G2UserAction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ActionID] [int] NOT NULL,
	[UserID] [int] NULL,
	[WorkstationID] [int] NULL,
	[ApplicationID] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_G2UserAction_TimeStamp]  DEFAULT (getdate()),
	[Inserted] [datetime] NOT NULL CONSTRAINT [DF_G2UserAction_Inserted]  DEFAULT (getdate())
) ON [PRIMARY]
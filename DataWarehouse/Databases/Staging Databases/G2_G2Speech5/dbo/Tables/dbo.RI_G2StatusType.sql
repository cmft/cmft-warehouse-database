﻿CREATE TABLE [dbo].[RI_G2StatusType](
	[ID] [int] NOT NULL,
	[Alias] [varchar](50) NOT NULL,
	[Reserved01] [varchar](100) NULL,
	[Reserved02] [varchar](100) NULL,
	[Reserved03] [varchar](100) NULL,
	[Reserved04] [varchar](100) NULL,
	[Reserved05] [varchar](100) NULL
) ON [PRIMARY]
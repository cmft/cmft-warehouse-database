﻿CREATE TABLE [dbo].[RI_G2Persons](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NULL,
	[InfoID] [int] NULL,
	[PersonID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[TypeID] [int] NULL,
	[Inserted] [datetime] NULL,
	[Reserved01] [varchar](100) NULL,
	[Reserved02] [varchar](100) NULL,
	[Reserved03] [varchar](100) NULL,
	[Reserved04] [varchar](100) NULL,
	[Reserved05] [varchar](100) NULL
) ON [PRIMARY]
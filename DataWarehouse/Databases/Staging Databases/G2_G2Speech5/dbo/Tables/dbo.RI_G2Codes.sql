﻿CREATE TABLE [dbo].[RI_G2Codes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NULL,
	[LinkTableID] [int] NULL,
	[ClassID] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Type] [int] NULL,
	[Inserted] [datetime] NULL,
	[Reserved01] [varchar](100) NULL,
	[Reserved02] [varchar](100) NULL,
	[Reserved03] [varchar](100) NULL,
	[Reserved04] [varchar](100) NULL,
	[Reserved05] [varchar](100) NULL
) ON [PRIMARY]
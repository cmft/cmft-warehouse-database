﻿CREATE TABLE [Outcome].[Clinician](
	[ClinicianId] [int] IDENTITY(1,1) NOT NULL,
	[SpecialtyId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]
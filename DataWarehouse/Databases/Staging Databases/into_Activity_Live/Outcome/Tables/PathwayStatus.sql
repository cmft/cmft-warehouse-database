﻿CREATE TABLE [Outcome].[PathwayStatus](
	[PathwayStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[PrimaryCodes] [varchar](255) NULL,
	[SecondaryCodes] [varchar](255) NULL,
	[ShowProvider] [bit] NULL,
	[ShowSpecialty] [bit] NULL,
	[IsActive] [bit] NULL,
	[Site] [nchar](10) NULL,
	[OrderIndex] [int] NULL,
	[Type] [char](1) NULL,
	[IsDeleted] [bit] NULL,
	[LastUpdatedDate] [smalldatetime] NULL,
	[LastUpdatedBy] [uniqueidentifier] NULL
) ON [PRIMARY]
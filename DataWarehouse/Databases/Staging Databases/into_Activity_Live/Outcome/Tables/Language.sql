﻿CREATE TABLE [Outcome].[Language](
	[Language_ID] [int] NOT NULL,
	[Language] [varchar](50) NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Outcome].[SpecialtyProcedure](
	[SpecialtyProcedureId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[SpecialtyId] [int] NOT NULL,
	[SpecialtyProcedureSectionId] [int] NULL,
	[Notes] [varchar](100) NULL,
	[HasLaterality] [bit] NOT NULL,
	[HasSite] [bit] NOT NULL,
	[PrimaryCodes] [varchar](255) NULL,
	[SecondaryCodes] [varchar](255) NULL,
	[MandatoryIds] [varchar](255) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[LastUpdatedDate] [smalldatetime] NULL,
	[LastUpdatedBy] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Outcome].[HelperText](
	[HelperText_ID] [int] IDENTITY(1,1) NOT NULL,
	[PageLocation] [nvarchar](255) NULL,
	[HelperText] [nvarchar](2000) NULL,
	[Show] [bit] NULL,
	[LastUpdatedDate] [smalldatetime] NULL,
	[LastUpdatedBy] [uniqueidentifier] NULL
) ON [PRIMARY]
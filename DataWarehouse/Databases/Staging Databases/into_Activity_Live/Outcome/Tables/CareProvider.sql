﻿CREATE TABLE [Outcome].[CareProvider](
	[CareProviderId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Code] [nvarchar](75) NULL,
	[Address1] [nvarchar](255) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[LastUpdatedBy] [uniqueidentifier] NULL,
	[LastUpdatedDate] [smalldatetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Outcome].[PatientOutcomeTest](
	[PatientOutcomeTestId] [int] IDENTITY(1,1) NOT NULL,
	[PatientOutcomeId] [int] NULL,
	[TestId] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Outcome].[SpecialtyProcedureSection](
	[SpecialtyProcedureSectionId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[OrderIndex] [tinyint] NOT NULL,
	[SpecialtyId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[LastUpdatedDate] [smalldatetime] NULL,
	[LastUpdatedBy] [uniqueidentifier] NULL
) ON [PRIMARY]
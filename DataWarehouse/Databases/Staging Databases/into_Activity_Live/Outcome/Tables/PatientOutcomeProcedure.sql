﻿CREATE TABLE [Outcome].[PatientOutcomeProcedure](
	[SpecialtyProcedureId] [int] NOT NULL,
	[PatientOutcomeId] [int] NOT NULL,
	[Site] [varchar](50) NULL,
	[LeftSide] [bit] NOT NULL,
	[RightSide] [bit] NOT NULL,
	[IsPrimary] [bit] NOT NULL,
	[IsSecondary] [bit] NULL
) ON [PRIMARY]
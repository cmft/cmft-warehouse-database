﻿CREATE TABLE [Outcome].[Test](
	[TestId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[PrimaryCodes] [varchar](255) NULL,
	[SecondaryCodes] [varchar](255) NULL,
	[IsActive] [bit] NOT NULL,
	[Site] [nchar](10) NULL,
	[OrderIndex] [int] NULL
) ON [PRIMARY]
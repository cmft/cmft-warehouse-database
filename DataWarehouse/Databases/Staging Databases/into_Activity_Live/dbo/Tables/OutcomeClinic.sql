﻿CREATE TABLE [dbo].[OutcomeClinic](
	[OutcomeClinic_ID] [int] IDENTITY(1,1) NOT NULL,
	[OutcomeForm_ID] [int] NULL,
	[GroupName_ID] [int] NULL,
	[ClinicCode] [varchar](75) NULL,
	[Site_ID] [int] NULL,
	[MainWait_ID] [int] NULL,
	[Subwait_ID] [int] NULL,
	[Comments] [varchar](6000) NULL,
	[IsCancerClinic] [bit] NULL,
	[IsMPA] [bit] NULL,
	[LateLimit] [int] NULL,
	[DNALimit] [int] NULL,
	[LastUpdatedBy] [uniqueidentifier] NULL,
	[LastUpdatedDate] [smalldatetime] NULL,
	[IsDeleted] [bit] NULL,
	[BlockClinic] [bit] NULL,
	[BlockClinicMessage] [nvarchar](250) NULL,
	[CheckinAfterDuration] [int] NULL,
	[CheckinBeforeDuration] [int] NULL,
	[CheckinToLocation] [int] NULL,
	[PreActivityClinic] [int] NULL,
	[ClinicDescription] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[infoCallMessage](
	[CallMessage_ID] [int] IDENTITY(1,1) NOT NULL,
	[CallMessage] [varchar](255) NULL,
	[MessageRaisedDate] [datetime] NULL,
	[MessageRaisedBy] [uniqueidentifier] NULL
) ON [PRIMARY]
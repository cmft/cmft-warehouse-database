﻿CREATE TABLE [dbo].[Emp_Details](
	[Emp_Name] [varchar](10) NULL,
	[Company] [varchar](15) NULL,
	[Join_Date] [datetime] NULL,
	[Resigned_Date] [datetime] NULL
) ON [PRIMARY]
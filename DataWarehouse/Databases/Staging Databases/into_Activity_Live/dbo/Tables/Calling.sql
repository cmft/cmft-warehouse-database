﻿CREATE TABLE [dbo].[Calling](
	[Specialty_ID] [int] IDENTITY(1,1) NOT NULL,
	[Escalation1Duration] [decimal](18, 3) NULL,
	[Escalation1Display] [varchar](50) NULL,
	[Escalation1Audio] [varchar](50) NULL,
	[Escalation2Duration] [int] NULL,
	[Escalation2Display] [varchar](50) NULL,
	[Escalation2Audio] [varchar](50) NULL,
	[Escalation3Duration] [int] NULL,
	[Escalation3Display] [varchar](50) NULL,
	[Escalation3Audio] [varchar](50) NULL,
	[Alert1HiVisXSLTPath] [varchar](255) NULL,
	[Alert1XSLTPath] [varchar](255) NULL,
	[Alert2HiVisXSLTPath] [varchar](255) NULL,
	[Alert2XSLTPath] [varchar](255) NULL,
	[Alert3HiVisXSLTPath] [varchar](255) NULL,
	[Alert3XSLTPath] [varchar](255) NULL,
	[CallMessage] [varchar](255) NULL
) ON [PRIMARY]
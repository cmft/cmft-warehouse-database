﻿CREATE TABLE [dbo].[SystemSettings](
	[Version] [varchar](30) NULL,
	[AutoArrivalLookAhead] [int] NULL,
	[AddDemographicUpdateEvent] [bit] NULL,
	[AutoArrivalToLocationOnly] [int] NULL,
	[ExpiryDate] [smalldatetime] NULL,
	[ExpiryMessage] [varchar](500) NULL,
	[CallingSortOrder] [varchar](10) NULL,
	[AllowMultipleChronologicalArrivals] [bit] NULL DEFAULT ((0)),
	[IgnoreInvalidClinics] [bit] NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[User2User](
	[ParentUser] [uniqueidentifier] NOT NULL,
	[ChildUser] [uniqueidentifier] NOT NULL,
	[AddedBy] [uniqueidentifier] NULL,
	[AddedDate] [smalldatetime] NULL,
	[RemovedBy] [uniqueidentifier] NULL,
	[RemovedDate] [smalldatetime] NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]
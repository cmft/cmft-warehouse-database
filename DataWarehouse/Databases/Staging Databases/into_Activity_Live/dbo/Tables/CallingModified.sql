﻿CREATE TABLE [dbo].[CallingModified](
	[Appointment_ID] [int] NOT NULL,
	[Escalation1Duration] [int] NULL,
	[Escalation1Display] [varchar](50) NULL,
	[Escalation1Audio] [varchar](50) NULL,
	[Escalation2Duration] [int] NULL,
	[Escalation2Display] [varchar](50) NULL,
	[Escalation2Audio] [varchar](50) NULL,
	[Escalation3Duration] [int] NULL,
	[Escalation3Display] [varchar](50) NULL,
	[Escalation3Audio] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OutcomeForm](
	[OutcomeForm_ID] [int] IDENTITY(1,1) NOT NULL,
	[OutcomeFormParent_ID] [int] NULL,
	[OutcomeFormName] [varchar](250) NULL,
	[OutcomeFormDescription] [varchar](250) NULL,
	[UsesNotes] [bit] NULL CONSTRAINT [DF_OutcomeForm_UsesNotes]  DEFAULT ((0)),
	[LastUpdatedBy] [uniqueidentifier] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL CONSTRAINT [DF_OutcomeForm_IsDeleted]  DEFAULT ((0)),
	[NationalCode] [varchar](3) NULL,
	[HasLaterality] [bit] NULL CONSTRAINT [DF_OutcomeForm_HasLaterality]  DEFAULT ((0)),
	[HasSite] [bit] NULL CONSTRAINT [DF_OutcomeForm_HasSite]  DEFAULT ((0)),
	[HasCodes] [bit] NULL CONSTRAINT [DF_OutcomeForm_HasCodes]  DEFAULT ((0)),
	[IsActive] [bit] NULL CONSTRAINT [DF_OutcomeForm_IsActive]  DEFAULT ((0)),
	[AutoCall] [bit] NULL CONSTRAINT [DF_OutcomeForm_AutoCall]  DEFAULT ((0))
) ON [PRIMARY]
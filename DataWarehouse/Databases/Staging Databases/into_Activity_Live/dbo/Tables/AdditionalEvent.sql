﻿CREATE TABLE [dbo].[AdditionalEvent](
	[AdditionalEvent_ID] [int] IDENTITY(1,1) NOT NULL,
	[Appointment_ID] [int] NULL,
	[Event_ID] [int] NULL,
	[CompleteDate] [datetime] NULL,
	[CompleteBy] [uniqueidentifier] NULL
) ON [PRIMARY]
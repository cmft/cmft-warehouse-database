﻿CREATE TABLE [dbo].[TimeFilter](
	[TimeFilter_ID] [int] IDENTITY(1,1) NOT NULL,
	[TimeFilterStart] [varchar](5) NULL,
	[TimeFilterEnd] [varchar](5) NULL,
	[TimeFilterName] [varchar](45) NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]
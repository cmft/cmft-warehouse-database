﻿CREATE TABLE [dbo].[AllocateConsultantLogging](
	[AllocateConsultantLogging_ID] [int] IDENTITY(1,1) NOT NULL,
	[Appointment_ID] [int] NULL,
	[ConsultantCode] [varchar](125) NULL,
	[Allocate_User_GUID] [uniqueidentifier] NULL,
	[Allocate_Date] [smalldatetime] NULL
) ON [PRIMARY]
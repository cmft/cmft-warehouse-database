﻿CREATE TABLE [dbo].[Message](
	[Message_ID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [text] NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[IsAlert] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
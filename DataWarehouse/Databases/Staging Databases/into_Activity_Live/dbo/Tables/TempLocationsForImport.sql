﻿CREATE TABLE [dbo].[TempLocationsForImport](
	[Location] [varchar](255) NULL,
	[Area] [varchar](255) NULL,
	[Capacity] [int] NULL,
	[Room] [varchar](255) NULL
) ON [PRIMARY]
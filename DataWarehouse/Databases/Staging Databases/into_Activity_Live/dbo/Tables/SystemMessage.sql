﻿CREATE TABLE [dbo].[SystemMessage](
	[SystemMessage_ID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [text] NULL,
	[MessageRaisedDate] [datetime] NULL,
	[MessageRaisedBy] [uniqueidentifier] NULL,
	[MessageStartDate] [datetime] NULL,
	[MessageExpiresDate] [datetime] NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
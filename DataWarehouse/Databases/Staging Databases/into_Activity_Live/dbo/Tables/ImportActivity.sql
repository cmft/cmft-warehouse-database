﻿CREATE TABLE [dbo].[ImportActivity](
	[Clinic] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Location] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[bpExceptionCodes](
	[Code] [bigint] NULL,
	[Description] [varchar](128) NULL,
	[Informational] [bit] NULL,
	[Public] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Room](
	[Room_ID] [int] IDENTITY(1,1) NOT NULL,
	[Room] [varchar](45) NULL,
	[RoomCode] [varchar](45) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MessageSchedule](
	[MessageSchedule_ID] [int] IDENTITY(1,1) NOT NULL,
	[Message_ID] [int] NULL,
	[Display_ID] [int] NULL,
	[MessageType] [int] NULL,
	[StartTime] [varchar](5) NULL,
	[EndTime] [varchar](5) NULL,
	[IsActive] [bit] NULL,
	[Day] [int] NULL,
	[EndDateTime] [smalldatetime] NULL,
	[Interval] [int] NULL,
	[ScheduleType] [int] NULL,
	[StartDateTime] [smalldatetime] NULL,
	[User_GUID] [uniqueidentifier] NULL
) ON [PRIMARY]
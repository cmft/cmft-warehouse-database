﻿CREATE TABLE [dbo].[Display](
	[Display_ID] [int] IDENTITY(1,1) NOT NULL,
	[DisplayType] [int] NULL,
	[DisplayName] [varchar](75) NULL,
	[IPAddress] [varchar](75) NULL,
	[ImagePath] [varchar](150) NULL,
	[Description] [varchar](2000) NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[Location] [varchar](75) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EventAudit](
	[EventAudit_ID] [int] IDENTITY(1,1) NOT NULL,
	[AuditType] [varchar](25) NULL,
	[AuditDescription] [varchar](2000) NULL,
	[Appointment_ID] [int] NULL,
	[Event_ID] [int] NULL,
	[AppoinementEvent_ID] [int] NULL,
	[User_GUID] [uniqueidentifier] NULL,
	[AuditDate] [datetime] NULL
) ON [PRIMARY]
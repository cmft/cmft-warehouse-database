﻿CREATE TABLE [dbo].[Location](
	[Location_ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentLocation_ID] [int] NULL,
	[Location] [varchar](75) NULL,
	[FloorLevel] [varchar](50) NULL,
	[Capacity] [int] NULL,
	[ScreenToCall] [varchar](255) NULL,
	[Telephone] [varchar](30) NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsMainWait] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[GroupAlias] [varchar](255) NULL,
	[SubGroupAlias] [varchar](255) NULL,
	[AutoCall] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TempUsersForImport](
	[UserName] [varchar](75) NULL,
	[FirstName] [varchar](75) NULL,
	[Surname] [varchar](75) NULL,
	[Email] [varchar](75) NULL,
	[ConsultantCode] [varchar](75) NULL,
	[Imported] [bit] NULL
) ON [PRIMARY]
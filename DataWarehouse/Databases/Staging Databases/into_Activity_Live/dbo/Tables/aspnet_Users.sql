﻿CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[UserName] [nvarchar](256) NOT NULL,
	[LoweredUserName] [nvarchar](256) NOT NULL,
	[MobileAlias] [nvarchar](16) NULL DEFAULT (NULL),
	[IsAnonymous] [bit] NOT NULL DEFAULT ((0)),
	[LastActivityDate] [datetime] NOT NULL,
	[FirstName] [varchar](75) NULL,
	[SurName] [varchar](75) NULL,
	[TrustEmail] [varchar](75) NULL,
	[Department] [varchar](75) NULL,
	[DefaultLocation_ID] [int] NULL,
	[ConsultantCode] [varchar](75) NULL,
	[PreSelectMe] [bit] NULL,
	[PrivatePatients] [bit] NULL,
	[Specialty_ID] [int] NULL
) ON [PRIMARY]
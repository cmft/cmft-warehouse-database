﻿CREATE TABLE [dbo].[KioskAreaLocation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[KioskAreaID] [int] NOT NULL,
	[LocationID] [int] NULL,
	[AllowFirstCheckin] [bit] NULL,
	[AllowSecondCheckin] [bit] NULL,
	[LocationMessage] [nvarchar](500) NULL,
	[SecondLocationMessage] [nvarchar](500) NULL,
	[LocationImage] [nvarchar](250) NULL,
	[ChangeDemographicsMessage] [nvarchar](500) NULL,
	[ChangeDemographicsImage] [nvarchar](250) NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[CountryCode] [int] NULL,
	[LanguageCode] [int] NULL,
	[IsDeleted] [bit] NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[AdditionalInformation] [nchar](500) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserAction](
	[UserAction_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserAction] [varchar](45) NULL,
	[URL] [varchar](125) NULL,
	[Enabled] [bit] NULL,
	[DisplayOrder] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[KioskArea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[ReceptionMessage] [nvarchar](500) NULL,
	[ReceptionImage] [nvarchar](250) NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[CountryCode] [int] NULL,
	[LanguageCode] [int] NULL,
	[Code] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[ShowDisclaimer] [bit] NULL,
	[DisclaimerMessage] [nvarchar](500) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[EarlyMessage] [nvarchar](500) NULL,
	[LateMessage] [nvarchar](500) NULL
) ON [PRIMARY]
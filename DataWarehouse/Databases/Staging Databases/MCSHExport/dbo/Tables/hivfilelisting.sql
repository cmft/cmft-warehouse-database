﻿CREATE TABLE [dbo].[hivfilelisting](
	[name] [nvarchar](max) NULL,
	[directory] [bit] NULL,
	[size] [bigint] NULL,
	[date_created] [datetime] NULL,
	[date_modified] [datetime] NULL,
	[extension] [nvarchar](max) NULL,
	[num]  AS (left([name],len([name])-len([extension]))),
	[PatientNo]  AS (case when left([name],(2))='GT' then replace(left([name],len([name])-len([extension])),' ','/') else 'G'+replace(left([name],len([name])-len([extension])),' ','/') end)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
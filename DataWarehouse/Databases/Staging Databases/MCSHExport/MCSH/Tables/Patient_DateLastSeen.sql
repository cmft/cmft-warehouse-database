﻿CREATE TABLE [MCSH].[Patient_DateLastSeen](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[DateLastSeen] [date] NULL
) ON [PRIMARY]
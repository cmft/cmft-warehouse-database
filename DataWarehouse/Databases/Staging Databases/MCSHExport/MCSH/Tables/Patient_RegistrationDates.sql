﻿CREATE TABLE [MCSH].[Patient_RegistrationDates](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[RegistrationDate] [smalldatetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [MCSH].[Patient_ContactDetails](
	[PatientID] [bigint] NOT NULL,
	[HomePhone] [varchar](50) NULL,
	[WorkPhone] [varchar](50) NULL,
	[Mobile] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[LetterOnly_Home] [varchar](50) NULL,
	[TelephoneOnly] [varchar](50) NULL,
	[LetterOrTelephone] [varchar](50) NULL,
	[LetterOnly_Work] [varchar](50) NULL,
	[LetterOnly_Friend] [varchar](50) NULL,
	[TextMessage] [varchar](50) NULL,
	[CareOf_ContactPhone] [varchar](50) NULL
) ON [PRIMARY]
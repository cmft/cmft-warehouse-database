﻿CREATE TABLE [MCSH].[Patient_HIVInfo](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[DateOfFirstPosHIVTest] [varchar](50) NULL,
	[CurrentHIVStage] [varchar](50) NULL,
	[CurrentHIVStageID] [bigint] NULL
) ON [PRIMARY]
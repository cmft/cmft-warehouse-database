﻿CREATE TABLE [Export].[Patient_Messages](
	[PatientMessageID] [bigint] NOT NULL,
	[PatientID] [bigint] NULL,
	[PatientNo] [varchar](1024) NULL,
	[DisplayDate] [smalldatetime] NOT NULL,
	[ResourceTypeID] [bigint] NOT NULL,
	[ResourceType] [varchar](1024) NOT NULL,
	[Message] [varchar](1024) NOT NULL,
	[Actioned] [bit] NULL,
	[ActionDate] [smalldatetime] NULL,
	[CollectionDateTime] [smalldatetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Export].[Patient_RIPFlag](
	[patientid] [bigint] NOT NULL,
	[patientno] [varchar](50) NULL,
	[RIPFlag] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Export].[Patient_SyphilisFlag](
	[PatientID] [bigint] NULL,
	[PatientNo] [varchar](50) NULL,
	[SyphilisFlag] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Export].[Patient_HIVFlag](
	[PatientID] [bigint] NULL,
	[PatientNo] [varchar](50) NULL,
	[HIVFlag] [int] NOT NULL
) ON [PRIMARY]
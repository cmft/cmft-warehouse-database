﻿CREATE TABLE [Export].[Patient_Recalls](
	[PatientRecallID] [bigint] NOT NULL,
	[PatientID] [bigint] NULL,
	[PatientNo] [varchar](50) NULL,
	[RecallDate] [smalldatetime] NULL,
	[ResourceTypeID] [bigint] NULL,
	[ResourceType] [varchar](1024) NULL,
	[RecallReasonID] [bigint] NULL,
	[RecallReason] [varchar](1024) NULL,
	[Comment] [varchar](1024) NULL,
	[Actioned] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL
) ON [PRIMARY]
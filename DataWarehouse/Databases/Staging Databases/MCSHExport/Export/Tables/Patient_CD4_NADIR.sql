﻿CREATE TABLE [Export].[Patient_CD4_NADIR](
	[PatientID] [bigint] NULL,
	[PatientNumber] [varchar](50) NULL,
	[CD4_NADIR] [float] NULL
) ON [PRIMARY]
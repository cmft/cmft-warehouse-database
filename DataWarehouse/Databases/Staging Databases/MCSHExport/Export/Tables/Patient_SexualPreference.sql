﻿CREATE TABLE [Export].[Patient_SexualPreference](
	[patientid] [bigint] NOT NULL,
	[sexualpreferenceid] [bigint] NOT NULL,
	[sexualpreference] [varchar](1024) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserRole](
	[UserRoleID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[IsActive] [bit] NOT NULL,
	[IsMigrationOnly] [bit] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
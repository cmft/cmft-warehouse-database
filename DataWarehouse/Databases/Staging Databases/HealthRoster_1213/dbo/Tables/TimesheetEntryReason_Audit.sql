﻿CREATE TABLE [dbo].[TimesheetEntryReason_Audit](
	[TimesheetEntryReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

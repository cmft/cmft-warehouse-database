﻿CREATE TABLE [dbo].[RelationshipType_Audit](
	[RelationshipTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

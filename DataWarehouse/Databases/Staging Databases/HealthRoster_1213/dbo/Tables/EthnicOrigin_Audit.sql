﻿CREATE TABLE [dbo].[EthnicOrigin_Audit](
	[EthnicOriginID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

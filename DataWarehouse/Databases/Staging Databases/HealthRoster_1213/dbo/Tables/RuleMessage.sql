﻿CREATE TABLE [dbo].[RuleMessage](
	[RuleMessageID] [int] NOT NULL,
	[Severity] [int] NOT NULL,
	[Messages] [xml] NULL,
	[RosteringRuleID] [int] NOT NULL,
	[HoursAssignmentID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
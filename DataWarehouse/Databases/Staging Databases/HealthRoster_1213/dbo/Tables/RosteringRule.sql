﻿CREATE TABLE [dbo].[RosteringRule](
	[RosteringRuleID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsObsolete] [bit] NOT NULL,
	[ApplicabilityType] [int] NOT NULL,
	[Parameters] [xml] NULL,
	[RuleDefinitionID] [int] NOT NULL,
	[InformationID] [int] NULL,
	[ShiftGroupID] [int] NULL,
	[GlobalRosteringRuleID] [int] NULL,
	[LockVersion] [smallint] NOT NULL,
	[voa_class] [tinyint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
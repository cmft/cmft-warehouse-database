﻿CREATE TABLE [dbo].[Post_Audit](
	[PostID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

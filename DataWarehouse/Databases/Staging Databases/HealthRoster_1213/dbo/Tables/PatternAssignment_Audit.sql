﻿CREATE TABLE [dbo].[PatternAssignment_Audit](
	[PatternAssignmentID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

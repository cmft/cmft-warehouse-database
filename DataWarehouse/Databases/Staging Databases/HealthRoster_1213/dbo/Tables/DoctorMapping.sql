﻿CREATE TABLE [dbo].[DoctorMapping](
	[DoctorMappingID] [int] NOT NULL,
	[HealthRosterID] [int] NOT NULL,
	[ZircadianID] [uniqueidentifier] NOT NULL,
	[ZircadianGMCNumber] [nvarchar](255) NOT NULL,
	[LastSynchronised] [datetime] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
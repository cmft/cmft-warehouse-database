﻿CREATE TABLE [dbo].[ExtractRunStateHistory_Audit](
	[ExtractRunStateHistoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

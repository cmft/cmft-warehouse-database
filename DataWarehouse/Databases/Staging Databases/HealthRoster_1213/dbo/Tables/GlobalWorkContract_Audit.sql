﻿CREATE TABLE [dbo].[GlobalWorkContract_Audit](
	[GlobalWorkContractID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

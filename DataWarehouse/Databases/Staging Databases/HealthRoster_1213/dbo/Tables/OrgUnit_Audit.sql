﻿CREATE TABLE [dbo].[OrgUnit_Audit](
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

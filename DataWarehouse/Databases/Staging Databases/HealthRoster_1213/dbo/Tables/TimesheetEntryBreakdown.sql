﻿CREATE TABLE [dbo].[TimesheetEntryBreakdown](
	[TimesheetEntryBreakdownID] [int] NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[WorkTime] [int] NOT NULL,
	[PayTime] [int] NOT NULL,
	[Units] [int] NULL,
	[NegotiatedHourlyRate] [money] NULL,
	[NegotiatedFixedAmount] [money] NULL,
	[ManuallyEntered] [bit] NOT NULL,
	[Type] [int] NOT NULL,
	[OvertimeApplicabilityType] [int] NOT NULL,
	[TimesheetEntryReasonID] [int] NULL,
	[WorkRateID] [int] NULL,
	[DutyAssignmentID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
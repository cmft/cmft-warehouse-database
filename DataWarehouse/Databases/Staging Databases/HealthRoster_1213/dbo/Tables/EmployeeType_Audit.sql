﻿CREATE TABLE [dbo].[EmployeeType_Audit](
	[EmployeeTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

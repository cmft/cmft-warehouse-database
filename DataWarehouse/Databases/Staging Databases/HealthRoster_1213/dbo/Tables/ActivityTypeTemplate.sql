﻿CREATE TABLE [dbo].[ActivityTypeTemplate](
	[ActivityTypeTemplateID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ShortName] [nvarchar](25) NULL,
	[IsObsolete] [bit] NOT NULL,
	[PrimaryInfo] [xml] NULL,
	[ActivityTypeID] [int] NOT NULL,
	[ShiftGroupID] [int] NOT NULL,
	[DefaultRosterLocationID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
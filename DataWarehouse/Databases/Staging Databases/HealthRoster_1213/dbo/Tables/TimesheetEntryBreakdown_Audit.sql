﻿CREATE TABLE [dbo].[TimesheetEntryBreakdown_Audit](
	[TimesheetEntryBreakdownID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

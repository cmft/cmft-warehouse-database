﻿CREATE TABLE [dbo].[PatternApplicability_Audit](
	[PatternApplicabilityID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[DutyRequirement](
	[DutyRequirementID] [int] NOT NULL,
	[OffsetDay] [int] NOT NULL,
	[MinDuties] [int] NOT NULL,
	[MaxDuties] [int] NOT NULL,
	[RequirementType] [int] NOT NULL,
	[ValidDate] [datetime] NOT NULL,
	[ShiftID] [int] NOT NULL,
	[PatternAssignmentDemandGroupID] [int] NULL,
	[ActivityID] [int] NULL,
	[ResourceRequirementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
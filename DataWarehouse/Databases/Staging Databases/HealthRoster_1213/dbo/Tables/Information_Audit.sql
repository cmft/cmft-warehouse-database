﻿CREATE TABLE [dbo].[Information_Audit](
	[InformationID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

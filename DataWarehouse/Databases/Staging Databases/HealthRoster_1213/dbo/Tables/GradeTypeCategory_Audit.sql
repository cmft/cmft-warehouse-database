﻿CREATE TABLE [dbo].[GradeTypeCategory_Audit](
	[GradeTypeCategoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[DoctorMapping_Audit](
	[DoctorMappingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[AssignmentReason_Audit](
	[AssignmentReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

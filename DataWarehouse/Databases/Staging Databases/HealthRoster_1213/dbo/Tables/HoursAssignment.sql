﻿CREATE TABLE [dbo].[HoursAssignment](
	[HoursAssignmentID] [int] NOT NULL,
	[WorkTime] [int] NOT NULL,
	[ContractedTime] [int] NOT NULL,
	[PayrollState] [int] NOT NULL,
	[AssignmentMechanism] [int] NOT NULL,
	[RuleAssessmentResult] [int] NOT NULL,
	[Cost] [money] NOT NULL,
	[FinalisedDate] [datetime] NULL,
	[ValidDate] [datetime] NOT NULL,
	[ActualDutyStart] [datetime] NULL,
	[ActualDutyEnd] [datetime] NULL,
	[DiaryFlags] [int] NULL,
	[HasRequest] [bit] NULL,
	[SubmittedDate] [datetime] NULL,
	[ConfirmationState] [int] NULL,
	[PostingID] [int] NOT NULL,
	[PatternAssignmentID] [int] NULL,
	[PersonNonEffectivePeriodID] [int] NULL,
	[DutyCancelReasonID] [int] NULL,
	[DutyChangeReasonID] [int] NULL,
	[AssignmentReasonID] [int] NULL,
	[ActualRosterLocationID] [int] NULL,
	[SlaveDutyID] [int] NULL,
	[CombinedShiftID] [int] NULL,
	[DutyID] [int] NULL,
	[LockVersion] [smallint] NOT NULL,
	[voa_class] [char](1) NOT NULL
) ON [PRIMARY]
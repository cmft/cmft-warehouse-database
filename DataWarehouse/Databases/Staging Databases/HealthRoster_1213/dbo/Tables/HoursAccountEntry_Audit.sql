﻿CREATE TABLE [dbo].[HoursAccountEntry_Audit](
	[HoursAccountEntryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

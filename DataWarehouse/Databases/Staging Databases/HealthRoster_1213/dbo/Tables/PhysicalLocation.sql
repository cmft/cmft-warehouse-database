﻿CREATE TABLE [dbo].[PhysicalLocation](
	[PhysicalLocationID] [int] NOT NULL,
	[IsObsolete] [bit] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[AddressLine1] [nvarchar](100) NULL,
	[AddressLine2] [nvarchar](100) NULL,
	[AddressLine3] [nvarchar](100) NULL,
	[TownCity] [nvarchar](50) NULL,
	[PostCode] [nvarchar](20) NULL,
	[CountryID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
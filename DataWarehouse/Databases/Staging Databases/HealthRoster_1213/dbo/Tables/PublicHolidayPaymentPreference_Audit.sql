﻿CREATE TABLE [dbo].[PublicHolidayPaymentPreference_Audit](
	[PublicHolidayPaymentPreferenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[ExternalEmployeeType](
	[ExternalEmployeeTypeID] [int] NOT NULL,
	[ExternalName] [nvarchar](255) NOT NULL,
	[MatchedEmployeeTypeID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
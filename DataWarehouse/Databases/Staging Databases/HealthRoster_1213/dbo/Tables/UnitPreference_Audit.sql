﻿CREATE TABLE [dbo].[UnitPreference_Audit](
	[UnitPreferenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[ExtractRunStateHistory](
	[ExtractRunStateHistoryID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[RunState] [int] NOT NULL,
	[AuthorisedUserAccountID] [int] NOT NULL,
	[AuthenticatedUserAccountID] [int] NOT NULL,
	[ExtractRunID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
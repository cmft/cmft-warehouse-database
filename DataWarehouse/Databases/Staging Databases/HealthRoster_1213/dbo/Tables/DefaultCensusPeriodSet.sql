﻿CREATE TABLE [dbo].[DefaultCensusPeriodSet](
	[DefaultCensusPeriodSetID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsObsolete] [bit] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Note](
	[NoteID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[Text] [nvarchar](255) NOT NULL,
	[SystemGenerated] [bit] NOT NULL,
	[ExternalAuthorName] [nvarchar](200) NULL,
	[ExternalDateCreated] [datetime] NULL,
	[AuthorisedUserAccountID] [int] NOT NULL,
	[NoteGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
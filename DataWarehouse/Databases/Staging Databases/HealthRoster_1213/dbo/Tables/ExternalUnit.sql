﻿CREATE TABLE [dbo].[ExternalUnit](
	[ExternalUnitID] [int] NOT NULL,
	[ExternalUnitName] [nvarchar](255) NOT NULL,
	[DefaultMatchingOrgUnitID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
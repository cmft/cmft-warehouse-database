﻿CREATE TABLE [dbo].[RosterLocation_Audit](
	[RosterLocationID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

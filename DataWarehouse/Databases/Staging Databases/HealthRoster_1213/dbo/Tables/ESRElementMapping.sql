﻿CREATE TABLE [dbo].[ESRElementMapping](
	[ESRElementMappingID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Level0Name] [nvarchar](255) NOT NULL,
	[Level1Name] [nvarchar](255) NULL,
	[Level1Type] [int] NOT NULL,
	[Level1Value] [nvarchar](255) NULL,
	[Level2Name] [nvarchar](255) NULL,
	[Level2Type] [int] NOT NULL,
	[Level2Value] [nvarchar](255) NULL,
	[Level3Name] [nvarchar](255) NULL,
	[Level3Type] [int] NOT NULL,
	[Level3Value] [nvarchar](255) NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
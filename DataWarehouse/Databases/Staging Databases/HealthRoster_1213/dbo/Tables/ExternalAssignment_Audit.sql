﻿CREATE TABLE [dbo].[ExternalAssignment_Audit](
	[ExternalAssignmentID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

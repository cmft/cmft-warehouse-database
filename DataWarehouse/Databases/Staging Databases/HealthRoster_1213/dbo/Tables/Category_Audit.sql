﻿CREATE TABLE [dbo].[Category_Audit](
	[CategoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[TaskProgress_Audit](
	[TaskProgressID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

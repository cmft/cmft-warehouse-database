﻿CREATE TABLE [dbo].[Posting_Audit](
	[PostingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

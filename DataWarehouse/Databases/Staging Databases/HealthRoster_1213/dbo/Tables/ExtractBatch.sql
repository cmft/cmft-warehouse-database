﻿CREATE TABLE [dbo].[ExtractBatch](
	[ExtractBatchID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ExtractType] [int] NOT NULL,
	[PayChannelID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
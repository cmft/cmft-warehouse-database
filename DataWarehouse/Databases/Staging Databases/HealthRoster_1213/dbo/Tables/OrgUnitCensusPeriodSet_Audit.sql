﻿CREATE TABLE [dbo].[OrgUnitCensusPeriodSet_Audit](
	[OrgUnitCensusPeriodSetID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

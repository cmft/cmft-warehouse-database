﻿CREATE TABLE [dbo].[LongDayFactor](
	[LongDayFactorID] [int] NOT NULL,
	[Mins] [int] NOT NULL,
	[WTE] [money] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[GradeTypeCategoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
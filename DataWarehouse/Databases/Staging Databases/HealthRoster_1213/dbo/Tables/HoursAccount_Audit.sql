﻿CREATE TABLE [dbo].[HoursAccount_Audit](
	[HoursAccountID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

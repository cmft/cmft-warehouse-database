﻿CREATE TABLE [dbo].[SecondarySicknessReason](
	[SecondarySicknessReasonID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ExternalCode] [nvarchar](100) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[NonEffectiveReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DutyRequest](
	[DutyRequestID] [int] NOT NULL,
	[EmployeeComments] [nvarchar](255) NULL,
	[DenialComments] [nvarchar](255) NULL,
	[State] [int] NOT NULL,
	[PostingID] [int] NOT NULL,
	[DutyRequirementID] [int] NOT NULL,
	[DutyAssignmentID] [int] NULL,
	[SlaveDutyRequirementID] [int] NULL,
	[CombinedShiftID] [int] NULL,
	[NoteGroupID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ExtractLogEntry_Audit](
	[ExtractLogEntryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

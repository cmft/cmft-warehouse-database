﻿CREATE TABLE [dbo].[CompetenceCluster_Audit](
	[CompetenceClusterID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

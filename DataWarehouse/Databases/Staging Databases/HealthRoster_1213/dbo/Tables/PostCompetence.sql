﻿CREATE TABLE [dbo].[PostCompetence](
	[PostCompetenceID] [int] NOT NULL,
	[Weighting] [int] NOT NULL,
	[IsAssumed] [bit] NOT NULL,
	[IsMandatory] [bit] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[CompetenceID] [int] NOT NULL,
	[PostID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
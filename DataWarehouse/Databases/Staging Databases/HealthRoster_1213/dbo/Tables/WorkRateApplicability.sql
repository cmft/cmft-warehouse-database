﻿CREATE TABLE [dbo].[WorkRateApplicability](
	[WorkRateApplicabilityID] [int] NOT NULL,
	[DaysValidOn] [tinyint] NOT NULL,
	[AppliesOnPublicHolidays] [int] NOT NULL,
	[ApplicableStartTime] [datetime] NULL,
	[ApplicableEndTime] [datetime] NULL,
	[ExcludeTempStaffDuty] [bit] NOT NULL,
	[IncludeSubOrganisations] [bit] NOT NULL,
	[ShiftType] [int] NOT NULL,
	[ApplicableProcess] [int] NOT NULL,
	[EnterpriseWorkContractID] [int] NULL,
	[OrgUnitID] [int] NULL,
	[WorkRateID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AdditionalDutyReason_Audit](
	[AdditionalDutyReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

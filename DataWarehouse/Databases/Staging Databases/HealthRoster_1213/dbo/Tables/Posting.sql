﻿CREATE TABLE [dbo].[Posting](
	[PostingID] [int] NOT NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[IsPrimary] [bit] NOT NULL,
	[AssignmentNumber] [nvarchar](50) NULL,
	[RosterSortIndex] [int] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[AnnualLeaveAccrualPlanID] [int] NULL,
	[PostingStartReasonID] [int] NULL,
	[PostingEndReasonID] [int] NULL,
	[PostingTypeID] [int] NULL,
	[PostID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	[NoteGroupID] [int] NULL,
	[PostingWorkContractID] [int] NULL,
	[PostingWorkingRestrictionSetID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RosteringRule_Audit](
	[RosteringRuleID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

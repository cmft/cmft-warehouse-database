﻿CREATE TABLE [dbo].[ExternalGrade](
	[ExternalGradeID] [int] NOT NULL,
	[ExternalPayscale] [nvarchar](255) NOT NULL,
	[ExternalOccCode] [nvarchar](255) NOT NULL,
	[ExternalPayroll] [nvarchar](255) NOT NULL,
	[ExternalIsBank] [nvarchar](255) NOT NULL,
	[MatchedGrade] [nvarchar](255) NULL,
	[MatchGradeWithWildcard] [bit] NOT NULL,
	[MatchedDefaultGradeID] [int] NULL,
	[MatchedEnterpriseWorkContractID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OrgUnitBaseHPPD](
	[OrgUnitBaseHPPDID] [int] NOT NULL,
	[HPPDValue] [money] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[OrgUnitID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
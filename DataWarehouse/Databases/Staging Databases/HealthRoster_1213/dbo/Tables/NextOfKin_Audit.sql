﻿CREATE TABLE [dbo].[NextOfKin_Audit](
	[NextOfKinID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

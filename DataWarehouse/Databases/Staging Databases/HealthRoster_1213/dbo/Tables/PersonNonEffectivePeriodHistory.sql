﻿CREATE TABLE [dbo].[PersonNonEffectivePeriodHistory](
	[PersonNonEffectivePeriodHistoryID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[NonEffectiveAuditEvent] [int] NOT NULL,
	[PersonNonEffectivePeriodAuditDetails] [xml] NULL,
	[AuthenticatedID] [int] NOT NULL,
	[AuthorisedID] [int] NOT NULL,
	[PersonNonEffectivePeriodID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
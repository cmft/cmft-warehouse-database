﻿CREATE TABLE [dbo].[Report_Audit](
	[ReportID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

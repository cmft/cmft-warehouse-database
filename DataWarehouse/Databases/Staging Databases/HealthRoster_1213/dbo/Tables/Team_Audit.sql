﻿CREATE TABLE [dbo].[Team_Audit](
	[TeamID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

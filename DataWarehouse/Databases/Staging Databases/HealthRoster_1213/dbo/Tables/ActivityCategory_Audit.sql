﻿CREATE TABLE [dbo].[ActivityCategory_Audit](
	[ActivityCategoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

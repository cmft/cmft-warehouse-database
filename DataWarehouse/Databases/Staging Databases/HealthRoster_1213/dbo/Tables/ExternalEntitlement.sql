﻿CREATE TABLE [dbo].[ExternalEntitlement](
	[ExternalEntitlementID] [int] NOT NULL,
	[StaffNumber] [nvarchar](255) NOT NULL,
	[AssignmentNumber] [nvarchar](255) NOT NULL,
	[Minutes] [int] NOT NULL,
	[EffectiveFrom] [datetime] NOT NULL,
	[EntitlementType] [nvarchar](255) NOT NULL,
	[State] [int] NOT NULL,
	[FailureMessage] [nvarchar](512) NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PhysicalLocation_Audit](
	[PhysicalLocationID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

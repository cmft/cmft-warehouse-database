﻿CREATE TABLE [dbo].[UserSession](
	[UserSessionID] [int] NOT NULL,
	[AuthenticatedUserID] [int] NOT NULL,
	[AuthorisedUserID] [int] NOT NULL,
	[Started] [datetime2](2) NOT NULL
) ON [PRIMARY]

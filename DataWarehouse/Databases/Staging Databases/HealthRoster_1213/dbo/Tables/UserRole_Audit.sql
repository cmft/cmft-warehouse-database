﻿CREATE TABLE [dbo].[UserRole_Audit](
	[UserRoleID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

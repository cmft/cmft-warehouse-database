﻿CREATE TABLE [dbo].[PostingStartReason_Audit](
	[PostingStartReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[PatientCensusPeriod_Audit](
	[PatientCensusPeriodID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

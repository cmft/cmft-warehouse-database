﻿CREATE TABLE [dbo].[WorkRateApplicability_DutyCancelReason_Audit](
	[WorkRateApplicabilityID] [int] NOT NULL,
	[DutyCancelReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

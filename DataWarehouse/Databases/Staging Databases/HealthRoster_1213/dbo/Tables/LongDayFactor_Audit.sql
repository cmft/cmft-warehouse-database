﻿CREATE TABLE [dbo].[LongDayFactor_Audit](
	[LongDayFactorID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

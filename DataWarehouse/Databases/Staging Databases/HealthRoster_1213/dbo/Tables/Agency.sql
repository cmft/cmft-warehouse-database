﻿CREATE TABLE [dbo].[Agency](
	[AgencyID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[Abbreviation] [nvarchar](16) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[IsObsolete] [bit] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OrgUnitCostCentre_Audit](
	[OrgUnitCostCentreID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

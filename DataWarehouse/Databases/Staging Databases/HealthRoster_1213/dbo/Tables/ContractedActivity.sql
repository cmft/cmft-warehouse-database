﻿CREATE TABLE [dbo].[ContractedActivity](
	[ContractedActivityID] [int] NOT NULL,
	[ContractedActivitiesPerWeek] [money] NOT NULL,
	[ContractedHoursPerWeek] [money] NOT NULL,
	[ContractedWeeksPerYear] [int] NOT NULL,
	[ActivityCountPerWeek] [money] NULL,
	[PersonWorkContractID] [int] NOT NULL,
	[ActivityTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
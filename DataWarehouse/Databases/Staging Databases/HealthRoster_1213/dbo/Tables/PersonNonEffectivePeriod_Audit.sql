﻿CREATE TABLE [dbo].[PersonNonEffectivePeriod_Audit](
	[PersonNonEffectivePeriodID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

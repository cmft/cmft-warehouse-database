﻿CREATE TABLE [dbo].[HomeContact](
	[HomeContactID] [int] NOT NULL,
	[AddressLine1] [nvarchar](100) NULL,
	[AddressLine2] [nvarchar](100) NULL,
	[AddressLine3] [nvarchar](100) NULL,
	[TownCity] [nvarchar](50) NULL,
	[PostCode] [nvarchar](20) NULL,
	[HomeTelephone] [nvarchar](50) NULL,
	[WorkTelephone] [nvarchar](50) NULL,
	[MobileTelephone] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Bleeper] [nvarchar](50) NULL,
	[Email1] [nvarchar](255) NULL,
	[Email2] [nvarchar](255) NULL,
	[WebAddress] [nvarchar](255) NULL,
	[PersonID] [int] NOT NULL,
	[CountryID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
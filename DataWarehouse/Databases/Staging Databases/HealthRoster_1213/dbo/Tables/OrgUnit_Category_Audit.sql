﻿CREATE TABLE [dbo].[OrgUnit_Category_Audit](
	[OrgUnitID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[TempStaffDefaultAvailability_Audit](
	[TempStaffDefaultAvailabilityID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[AdHocActivityReason_Audit](
	[AdHocActivityReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

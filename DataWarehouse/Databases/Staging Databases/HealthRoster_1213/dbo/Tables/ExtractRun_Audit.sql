﻿CREATE TABLE [dbo].[ExtractRun_Audit](
	[ExtractRunID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

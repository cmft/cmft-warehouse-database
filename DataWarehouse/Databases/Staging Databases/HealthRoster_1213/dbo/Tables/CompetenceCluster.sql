﻿CREATE TABLE [dbo].[CompetenceCluster](
	[CompetenceClusterID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[CompetenceGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
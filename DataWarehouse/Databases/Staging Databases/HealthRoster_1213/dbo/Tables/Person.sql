﻿CREATE TABLE [dbo].[Person](
	[PersonID] [int] NOT NULL,
	[StaffNumber] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](40) NOT NULL,
	[Forenames] [nvarchar](32) NULL,
	[MiddleInitials] [nvarchar](32) NULL,
	[DateOfBirth] [datetime] NULL,
	[KnownAs] [nvarchar](50) NULL,
	[MaidenName] [nvarchar](50) NULL,
	[DateJoinedHealthService] [datetime] NULL,
	[AdditionalStaffIdentifier] [nvarchar](255) NULL,
	[NINumber] [nvarchar](30) NULL,
	[PreviousEmployer] [nvarchar](50) NULL,
	[EmploymentStatus] [int] NOT NULL,
	[PlaceOfBirth] [nvarchar](100) NULL,
	[DisabledNumber] [nvarchar](10) NULL,
	[ReviewDate] [datetime] NULL,
	[NextReviewDate] [datetime] NULL,
	[DateJoined] [datetime] NOT NULL,
	[DateLeaving] [datetime] NOT NULL,
	[EthnicOriginID] [int] NULL,
	[GenderID] [int] NULL,
	[TitleID] [int] NULL,
	[MaritalStatusID] [int] NULL,
	[EmployeeTypeID] [int] NOT NULL,
	[PersonLeavingReasonID] [int] NULL,
	[AnnualLeaveAccrualPlanID] [int] NULL,
	[CountryOfBirthID] [int] NULL,
	[NationalityID] [int] NULL,
	[AgencyID] [int] NULL,
	[DisabilityTypeID] [int] NULL,
	[PublicHolidayPaymentPreferenceID] [int] NULL,
	[HoursAccountID] [int] NOT NULL,
	[NoteGroupID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
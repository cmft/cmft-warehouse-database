﻿CREATE TABLE [dbo].[PayChannel](
	[PayChannelID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[StartDayOfMonth] [int] NOT NULL,
	[StartMonthOfYear] [tinyint] NOT NULL,
	[PeriodsStartFrom] [datetime] NOT NULL,
	[OffsetDaysToPeriodCutOff] [int] NOT NULL,
	[PayFrequency] [int] NOT NULL,
	[ExtractConfiguration] [xml] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
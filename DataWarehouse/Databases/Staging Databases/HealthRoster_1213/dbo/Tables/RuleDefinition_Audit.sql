﻿CREATE TABLE [dbo].[RuleDefinition_Audit](
	[RuleDefinitionID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

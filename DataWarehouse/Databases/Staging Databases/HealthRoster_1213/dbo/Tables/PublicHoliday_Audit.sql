﻿CREATE TABLE [dbo].[PublicHoliday_Audit](
	[PublicHolidayID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[TempStaffUnavailability](
	[TempStaffUnavailabilityID] [int] NOT NULL,
	[Comments] [nvarchar](512) NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[PersonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ExternalEmployeeType_Audit](
	[ExternalEmployeeTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

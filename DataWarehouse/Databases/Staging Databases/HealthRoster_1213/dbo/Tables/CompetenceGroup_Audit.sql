﻿CREATE TABLE [dbo].[CompetenceGroup_Audit](
	[CompetenceGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

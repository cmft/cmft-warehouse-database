﻿CREATE TABLE [dbo].[FinalisationAuditEntry_Audit](
	[FinalisationAuditEntryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

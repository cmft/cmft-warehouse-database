﻿CREATE TABLE [dbo].[ExtractLogArchiveEntry](
	[ExtractLogArchiveEntryID] [int] NOT NULL,
	[AssignmentNumber] [nvarchar](255) NOT NULL,
	[OperationType] [int] NOT NULL,
	[ExtractLogEntryWarnings] [xml] NULL,
	[ExtractRunID] [int] NOT NULL,
	[HoursAssignmentID] [int] NOT NULL,
	[PersonWorkContractID] [int] NOT NULL,
	[OrgUnitID] [int] NULL,
	[CostCentreID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[HoursAssignment_Audit](
	[HoursAssignmentID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

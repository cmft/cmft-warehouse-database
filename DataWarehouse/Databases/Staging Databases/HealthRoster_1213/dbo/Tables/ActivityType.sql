﻿CREATE TABLE [dbo].[ActivityType](
	[ActivityTypeID] [int] NOT NULL,
	[ShortCode] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[IsObsolete] [bit] NOT NULL,
	[Swappable] [bit] NOT NULL,
	[ActivityBenchmarkID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AwardRuleEngineNotification_Audit](
	[AwardRuleEngineNotificationID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

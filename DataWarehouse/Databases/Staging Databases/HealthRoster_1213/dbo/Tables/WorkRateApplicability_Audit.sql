﻿CREATE TABLE [dbo].[WorkRateApplicability_Audit](
	[WorkRateApplicabilityID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[Shift_Audit](
	[ShiftID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

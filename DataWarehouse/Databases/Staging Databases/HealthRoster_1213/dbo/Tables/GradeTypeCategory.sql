﻿CREATE TABLE [dbo].[GradeTypeCategory](
	[GradeTypeCategoryID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DefaultWTE] [int] NULL,
	[RegistrationCategory] [int] NOT NULL,
	[HeadroomPercentage] [money] NULL,
	[UsedInHPPDCalculation] [bit] NOT NULL,
	[SortOrder] [int] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[DefaultBankGradeForCostingID] [int] NULL,
	[DefaultAgencyGradeForCostingID] [int] NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
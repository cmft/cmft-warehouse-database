﻿CREATE TABLE [dbo].[Task](
	[TaskID] [int] NOT NULL,
	[TaskType] [nvarchar](255) NOT NULL,
	[State] [int] NOT NULL,
	[Source] [xml] NULL,
	[Results] [xml] NULL,
	[RunAt] [datetime] NOT NULL,
	[RequestedRunAt] [datetime] NOT NULL,
	[Priority] [int] NOT NULL,
	[CanStartAttempts] [int] NOT NULL,
	[EntityId] [int] NOT NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[Hostname] [nvarchar](100) NOT NULL,
	[ThreadId] [nvarchar](255) NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
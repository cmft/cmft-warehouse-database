﻿CREATE TABLE [dbo].[DutyRequest_Audit](
	[DutyRequestID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[PostRequirement_Audit](
	[PostRequirementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

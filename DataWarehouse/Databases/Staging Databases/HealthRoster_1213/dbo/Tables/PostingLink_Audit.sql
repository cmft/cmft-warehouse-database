﻿CREATE TABLE [dbo].[PostingLink_Audit](
	[PostingLinkID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

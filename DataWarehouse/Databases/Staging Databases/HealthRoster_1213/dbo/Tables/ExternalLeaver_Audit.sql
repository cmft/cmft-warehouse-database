﻿CREATE TABLE [dbo].[ExternalLeaver_Audit](
	[ExternalLeaverID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

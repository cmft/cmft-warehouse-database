﻿CREATE TABLE [dbo].[TaskStateHistory](
	[TaskStateHistoryID] [int] NOT NULL,
	[On] [datetime] NOT NULL,
	[State] [int] NOT NULL,
	[AuthorisedUserAccountID] [int] NOT NULL,
	[AuthenticatedUserAccountID] [int] NOT NULL,
	[TaskID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
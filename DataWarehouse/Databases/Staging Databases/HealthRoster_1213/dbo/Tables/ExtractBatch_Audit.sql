﻿CREATE TABLE [dbo].[ExtractBatch_Audit](
	[ExtractBatchID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

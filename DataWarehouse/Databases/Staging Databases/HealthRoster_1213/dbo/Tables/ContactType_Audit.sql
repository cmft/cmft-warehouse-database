﻿CREATE TABLE [dbo].[ContactType_Audit](
	[ContactTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[CostElementRate](
	[CostElementRateID] [int] NOT NULL,
	[UpperRate] [money] NOT NULL,
	[LowerRate] [money] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[CostElementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
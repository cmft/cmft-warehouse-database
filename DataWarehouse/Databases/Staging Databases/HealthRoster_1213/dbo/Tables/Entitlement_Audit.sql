﻿CREATE TABLE [dbo].[Entitlement_Audit](
	[EntitlementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

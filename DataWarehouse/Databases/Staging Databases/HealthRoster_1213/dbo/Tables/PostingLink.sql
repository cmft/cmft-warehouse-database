﻿CREATE TABLE [dbo].[PostingLink](
	[PostingLinkID] [int] NOT NULL,
	[PostingLinkType] [int] NOT NULL,
	[ParentPostingID] [int] NOT NULL,
	[SubordinatePostingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
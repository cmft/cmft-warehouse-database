﻿CREATE TABLE [dbo].[PostingEndReason_Audit](
	[PostingEndReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

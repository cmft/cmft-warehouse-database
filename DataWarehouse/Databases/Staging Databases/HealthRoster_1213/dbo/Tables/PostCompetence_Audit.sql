﻿CREATE TABLE [dbo].[PostCompetence_Audit](
	[PostCompetenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

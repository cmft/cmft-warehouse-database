﻿CREATE TABLE [dbo].[PersonCompetence](
	[PersonCompetenceID] [int] NOT NULL,
	[CertificateNumber] [nvarchar](50) NULL,
	[DateGained] [datetime] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[ObtainedFromReasonID] [int] NULL,
	[CompetenceID] [int] NOT NULL,
	[InformationID] [int] NULL,
	[PersonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
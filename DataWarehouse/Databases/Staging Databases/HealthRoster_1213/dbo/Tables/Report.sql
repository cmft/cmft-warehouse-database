﻿CREATE TABLE [dbo].[Report](
	[ReportID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[Request] [nvarchar](255) NOT NULL,
	[ReportCategory] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
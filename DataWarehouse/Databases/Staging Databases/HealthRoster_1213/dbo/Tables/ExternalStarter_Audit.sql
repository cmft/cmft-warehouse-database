﻿CREATE TABLE [dbo].[ExternalStarter_Audit](
	[ExternalStarterID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

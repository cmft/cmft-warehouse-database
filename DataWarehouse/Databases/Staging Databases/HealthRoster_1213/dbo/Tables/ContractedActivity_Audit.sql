﻿CREATE TABLE [dbo].[ContractedActivity_Audit](
	[ContractedActivityID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

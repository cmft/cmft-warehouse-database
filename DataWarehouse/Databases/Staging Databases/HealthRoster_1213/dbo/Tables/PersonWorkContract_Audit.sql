﻿CREATE TABLE [dbo].[PersonWorkContract_Audit](
	[PersonWorkContractID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[WorkRateBehaviourDefinition](
	[WorkRateBehaviourDefinitionID] [int] NOT NULL,
	[AutoGenerationPriority] [tinyint] NULL,
	[IncludeAllWorkTimeInPayHours] [bit] NOT NULL,
	[IncludeAllRestTimeInPayHours] [bit] NOT NULL,
	[BlockAllocatedWorkTimeFromOtherRates] [bit] NOT NULL,
	[AlwaysAppliesForWholeDutyRegardlessOfOtherRates] [bit] NOT NULL,
	[CountsTowardsContractedHours] [bit] NOT NULL,
	[ApplyToWholeShiftIfMajorityOverlapExists] [bit] NOT NULL,
	[IsEnteredInUnits] [bit] NOT NULL,
	[MaxUnitValue] [int] NULL,
	[MinUnitValue] [int] NULL,
	[CountsAsADO] [bit] NOT NULL,
	[WorkRateID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
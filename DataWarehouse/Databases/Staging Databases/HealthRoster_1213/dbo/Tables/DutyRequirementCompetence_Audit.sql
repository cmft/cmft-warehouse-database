﻿CREATE TABLE [dbo].[DutyRequirementCompetence_Audit](
	[DutyRequirementCompetenceID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[NoteGroup_Audit](
	[NoteGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

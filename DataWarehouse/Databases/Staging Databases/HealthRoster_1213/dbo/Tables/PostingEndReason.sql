﻿CREATE TABLE [dbo].[PostingEndReason](
	[PostingEndReasonID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[ExternalReference] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
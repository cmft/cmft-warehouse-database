﻿CREATE TABLE [dbo].[DataLoadRequest_Audit](
	[DataLoadRequestID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[PayChannel_Audit](
	[PayChannelID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[WorkingRestrictionSet_Audit](
	[WorkingRestrictionSetID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

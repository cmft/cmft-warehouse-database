﻿CREATE TABLE [dbo].[PostingType_Audit](
	[PostingTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

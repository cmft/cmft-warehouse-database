﻿CREATE TABLE [dbo].[ExternalEntitlement_Audit](
	[ExternalEntitlementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

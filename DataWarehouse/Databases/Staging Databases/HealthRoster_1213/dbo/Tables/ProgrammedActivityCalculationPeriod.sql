﻿CREATE TABLE [dbo].[ProgrammedActivityCalculationPeriod](
	[ProgrammedActivityCalculationPeriodID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Start] [datetime] NOT NULL,
	[Value] [money] NOT NULL,
	[HoursType] [int] NOT NULL,
	[GlobalWorkContractID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
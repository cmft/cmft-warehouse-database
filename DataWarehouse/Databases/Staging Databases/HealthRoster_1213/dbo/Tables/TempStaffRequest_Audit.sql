﻿CREATE TABLE [dbo].[TempStaffRequest_Audit](
	[TempStaffRequestID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

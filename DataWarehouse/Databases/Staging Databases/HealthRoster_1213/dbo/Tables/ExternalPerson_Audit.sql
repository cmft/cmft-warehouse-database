﻿CREATE TABLE [dbo].[ExternalPerson_Audit](
	[ExternalPersonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

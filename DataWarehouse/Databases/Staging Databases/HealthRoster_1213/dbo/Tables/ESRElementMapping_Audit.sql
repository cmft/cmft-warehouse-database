﻿CREATE TABLE [dbo].[ESRElementMapping_Audit](
	[ESRElementMappingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[ObtainedFromReason_Audit](
	[ObtainedFromReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[OrgStructureType_Audit](
	[OrgStructureTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

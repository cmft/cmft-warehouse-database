﻿CREATE TABLE [dbo].[Gender_Audit](
	[GenderID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

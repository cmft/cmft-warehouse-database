﻿CREATE TABLE [dbo].[OrgUnitName_Audit](
	[OrgUnitNameID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [dbo].[Grade](
	[GradeID] [int] NOT NULL,
	[ShortName] [nvarchar](20) NOT NULL,
	[LongName] [nvarchar](50) NOT NULL,
	[Equivalence] [smallint] NOT NULL,
	[ExternalReference] [nvarchar](100) NULL,
	[SubjectiveCode] [nvarchar](50) NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[GradeTypeID] [int] NOT NULL,
	[OccGroupID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
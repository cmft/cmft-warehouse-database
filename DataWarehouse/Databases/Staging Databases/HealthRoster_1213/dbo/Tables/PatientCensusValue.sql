﻿CREATE TABLE [dbo].[PatientCensusValue](
	[PatientCensusValueID] [int] NOT NULL,
	[PatientCount] [int] NULL,
	[PatientCensusPeriodID] [int] NOT NULL,
	[PatientTypeID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RosterApprovalHistory_Audit](
	[RosterApprovalHistoryID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

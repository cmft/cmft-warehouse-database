﻿CREATE TABLE [dbo].[UserProfile_Audit](
	[UserProfileID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

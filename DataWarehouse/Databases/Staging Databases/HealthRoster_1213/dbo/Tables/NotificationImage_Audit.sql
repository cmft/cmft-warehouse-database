﻿CREATE TABLE [dbo].[NotificationImage_Audit](
	[NotificationImageID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

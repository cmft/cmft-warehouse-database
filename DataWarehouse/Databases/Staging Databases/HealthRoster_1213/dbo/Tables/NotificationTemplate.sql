﻿CREATE TABLE [dbo].[NotificationTemplate](
	[NotificationTemplateID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Locale] [nvarchar](50) NOT NULL,
	[Subject] [nvarchar](255) NULL,
	[Body] [nvarchar](max) NULL,
	[IsStock] [bit] NOT NULL,
	[NotificationDefinitionID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[ZircadianUnitMapping_Audit](
	[ZircadianUnitMappingID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

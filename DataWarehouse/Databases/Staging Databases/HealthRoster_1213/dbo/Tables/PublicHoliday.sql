﻿CREATE TABLE [dbo].[PublicHoliday](
	[PublicHolidayID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[OnDate] [datetime] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
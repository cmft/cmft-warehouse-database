﻿CREATE TABLE [dbo].[ActivityTimePeriod](
	[ActivityTimePeriodID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PeriodStart] [datetime] NOT NULL,
	[LockVersion] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CostElement_Audit](
	[CostElementID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

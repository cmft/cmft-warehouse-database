﻿CREATE TABLE [dbo].[ThirdPartyReason_Audit](
	[ThirdPartyReasonID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

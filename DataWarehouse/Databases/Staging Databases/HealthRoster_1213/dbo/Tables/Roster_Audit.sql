﻿CREATE TABLE [dbo].[Roster_Audit](
	[RosterID] [int] NOT NULL,
	[LockVersion] [smallint] NOT NULL,
	[ChangeSetID] [bigint] NOT NULL
) ON [PRIMARY]

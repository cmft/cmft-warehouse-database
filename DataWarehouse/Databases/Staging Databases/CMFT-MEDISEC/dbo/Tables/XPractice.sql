﻿CREATE TABLE [dbo].[XPractice](
	[prcode] [varchar](6) NULL,
	[praddr1] [varchar](25) NULL,
	[praddr2] [varchar](25) NULL,
	[praddr3] [varchar](25) NULL,
	[praddr4] [varchar](25) NULL,
	[prpc] [varchar](8) NULL,
	[prformed] [varchar](8) NULL,
	[prdissolved] [varchar](8) NULL,
	[prgpfh] [varchar](5) NULL,
	[prmain] [varchar](1) NULL,
	[prtel] [varchar](13) NULL
) ON [PRIMARY]
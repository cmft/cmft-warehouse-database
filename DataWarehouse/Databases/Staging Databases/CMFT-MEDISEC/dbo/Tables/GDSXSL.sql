﻿CREATE TABLE [dbo].[GDSXSL](
	[gdsxslid] [int] IDENTITY(1,1) NOT NULL,
	[gdsxsldscode] [char](10) NULL,
	[gdsxsldoctype] [char](50) NULL,
	[gdsxsltemplate] [char](50) NULL
) ON [PRIMARY]
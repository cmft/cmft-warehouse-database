﻿CREATE TABLE [dbo].[ZConsExtra](
	[cexcons] [char](6) NOT NULL,
	[cextitle] [char](4) NULL,
	[cexsurn] [char](24) NULL,
	[cexfn1] [char](20) NULL,
	[cexswfuser] [char](1) NOT NULL,
	[cexireqstart] [datetime] NULL,
	[cexqual] [char](40) NULL,
	[cexjobtitle] [char](50) NULL,
	[cextel] [char](13) NULL,
	[cexfax] [char](13) NULL,
	[cexemail] [char](100) NULL,
	[cexcons1] [char](100) NULL,
	[cexcons2] [char](100) NULL,
	[cexcons3] [char](100) NULL,
	[cexcons4] [char](100) NULL
) ON [PRIMARY]
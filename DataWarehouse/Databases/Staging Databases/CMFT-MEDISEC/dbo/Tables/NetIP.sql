﻿CREATE TABLE [dbo].[NetIP](
	[ippr] [char](10) NOT NULL,
	[ipaddrstart] [char](11) NOT NULL,
	[ipaddrmin] [int] NOT NULL,
	[ipaddrmax] [int] NOT NULL,
	[ipdatestart] [datetime] NOT NULL,
	[ipdateend] [datetime] NOT NULL,
	[ipdateexpire] [datetime] NOT NULL,
	[ipcomment] [char](255) NOT NULL,
	[ipprprefix] [char](8) NULL,
	[ipxml] [char](1) NULL,
	[ipTIF] [char](1) NULL,
	[ippwd] [char](8) NULL
) ON [PRIMARY]
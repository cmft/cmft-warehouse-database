﻿CREATE TABLE [dbo].[NetAlert](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PRACTICE] [varchar](10) NOT NULL,
	[DATEON] [char](12) NOT NULL,
	[DATEOFF] [char](12) NOT NULL,
	[MSG] [varchar](255) NOT NULL
) ON [PRIMARY]
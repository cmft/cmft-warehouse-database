﻿CREATE TABLE [dbo].[OutClinic](
	[oclindate] [datetime] NOT NULL,
	[oclinhosp] [char](4) NOT NULL,
	[oclinspec] [char](5) NOT NULL,
	[oclindiary] [char](8) NOT NULL,
	[oclinstream] [char](1) NOT NULL,
	[oclintotpats] [int] NULL,
	[oclintotrecorded] [int] NULL,
	[oclintotlettersproduced] [int] NULL,
	[oclintotlettersoutstanding] [int] NULL,
	[oclinsuspcan] [char](1) NULL
) ON [PRIMARY]
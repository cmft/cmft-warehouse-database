﻿CREATE TABLE [dbo].[NetZip](
	[nzuser] [char](8) NULL,
	[nzpr] [char](6) NULL,
	[nzstamp] [datetime] NULL,
	[nztype] [char](1) NULL,
	[nzletters] [int] NULL,
	[nzpath] [char](256) NULL,
	[nzsql] [char](256) NULL,
	[nzzipid] [int] IDENTITY(1,1) NOT NULL,
	[nzack] [char](1) NOT NULL,
	[nzfilename] [char](256) NULL
) ON [PRIMARY]
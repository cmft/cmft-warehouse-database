﻿CREATE TABLE [dbo].[ZDictator](
	[dbdept] [char](100) NULL,
	[dbcode] [char](6) NULL,
	[dbsurname] [char](30) NULL,
	[dbforename] [char](20) NULL,
	[dbactive] [char](1) NULL,
	[dbsignname] [char](100) NULL,
	[dbsigntitle] [char](100) NULL
) ON [PRIMARY]
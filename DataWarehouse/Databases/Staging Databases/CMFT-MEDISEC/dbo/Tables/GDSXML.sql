﻿CREATE TABLE [dbo].[GDSXML](
	[gdsxpn] [char](8) NOT NULL,
	[gdsxdoa] [char](10) NOT NULL,
	[gdsxfb] [char](2) NOT NULL,
	[gdsxdscode] [char](20) NOT NULL,
	[gdsxxml] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
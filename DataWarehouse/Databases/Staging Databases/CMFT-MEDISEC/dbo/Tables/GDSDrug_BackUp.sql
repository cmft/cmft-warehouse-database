﻿CREATE TABLE [dbo].[GDSDrug_BackUp](
	[drugkey] [int] IDENTITY(1,1) NOT NULL,
	[drugname] [char](100) NOT NULL,
	[drugdefroute] [char](100) NOT NULL,
	[drugdefdose] [char](100) NOT NULL,
	[drugdeffreq] [char](100) NOT NULL,
	[drugdefdays] [char](100) NOT NULL,
	[drugdefsupdis] [char](100) NOT NULL,
	[drugdefrunsout] [char](100) NOT NULL,
	[drugdefnotes] [char](100) NOT NULL,
	[drugcontrolledflag] [char](1) NOT NULL,
	[drugwarning] [varchar](500) NULL,
	[drugsortno] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Contact](
	[conkey] [int] IDENTITY(1,1) NOT NULL,
	[connatcode] [char](8) NOT NULL,
	[concreatedby] [char](12) NOT NULL,
	[concreated] [datetime] NOT NULL,
	[conamendedby] [char](12) NOT NULL,
	[conamended] [datetime] NOT NULL,
	[consurn] [char](30) NOT NULL,
	[confn1] [char](30) NOT NULL,
	[confn2] [char](30) NOT NULL,
	[contitle] [char](10) NOT NULL,
	[conprint] [char](100) NOT NULL,
	[contel] [char](14) NOT NULL,
	[confax] [char](14) NOT NULL,
	[conemail] [char](50) NOT NULL,
	[conjobtitle] [char](100) NOT NULL,
	[conparentorg] [char](8) NOT NULL,
	[conabkey] [int] NOT NULL
) ON [PRIMARY]
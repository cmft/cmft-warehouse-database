﻿CREATE TABLE [dbo].[InpspellDups](
	[inpduppn] [char](8) NOT NULL,
	[inpdupdoa] [datetime] NOT NULL,
	[inpdupfb] [char](2) NOT NULL,
	[inpdupdelstamp] [datetime] NULL
) ON [PRIMARY]
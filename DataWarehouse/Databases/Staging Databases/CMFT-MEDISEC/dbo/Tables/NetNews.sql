﻿CREATE TABLE [dbo].[NetNews](
	[newsid] [int] IDENTITY(1,1) NOT NULL,
	[newspr] [char](10) NOT NULL,
	[newsdatetime] [datetime] NOT NULL,
	[newstype] [char](1) NOT NULL,
	[newsmsg] [varchar](255) NOT NULL,
	[newspn] [char](10) NOT NULL,
	[newspriority] [char](1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Config](
	[cfgpwexpirydays] [int] NULL,
	[cfgpwattempts] [int] NULL,
	[cfghelpmsg] [char](256) NULL,
	[cfgunsignedletters] [int] NULL
) ON [PRIMARY]
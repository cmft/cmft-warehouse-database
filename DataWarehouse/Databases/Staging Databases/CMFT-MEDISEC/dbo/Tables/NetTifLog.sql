﻿CREATE TABLE [dbo].[NetTifLog](
	[tifid] [int] IDENTITY(1,1) NOT NULL,
	[tifstamp] [datetime] NULL,
	[tifpr] [char](6) NULL,
	[tifpwd] [char](8) NULL,
	[tifip] [char](15) NULL,
	[tifzipid] [int] NULL,
	[tifevent] [char](10) NULL,
	[tifstatus] [char](7) NULL,
	[tifmsg] [char](255) NULL
) ON [PRIMARY]
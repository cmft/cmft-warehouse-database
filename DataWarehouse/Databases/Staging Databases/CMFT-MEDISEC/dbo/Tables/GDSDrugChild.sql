﻿CREATE TABLE [dbo].[GDSDrugChild](
	[drugkey] [int] IDENTITY(1,1) NOT NULL,
	[drugname] [char](100) NOT NULL DEFAULT (' '),
	[drugdefroute] [char](100) NOT NULL DEFAULT (' '),
	[drugdefdose] [char](100) NOT NULL DEFAULT (' '),
	[drugdeffreq] [char](100) NOT NULL DEFAULT (' '),
	[drugdefdays] [char](100) NOT NULL DEFAULT ('7'),
	[drugdefsupdis] [char](100) NOT NULL DEFAULT ('YES'),
	[drugdefrunsout] [char](100) NOT NULL DEFAULT (' '),
	[drugdefnotes] [char](100) NOT NULL DEFAULT (' '),
	[drugcontrolledflag] [char](1) NOT NULL DEFAULT ('N'),
	[drugwarning] [varchar](500) NULL,
	[drugsortno] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SystemConfig](
	[cfgname] [varchar](50) NOT NULL,
	[cfgvalue] [varchar](50) NOT NULL CONSTRAINT [DF_Config_cfgvalue]  DEFAULT ('')
) ON [PRIMARY]
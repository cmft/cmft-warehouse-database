﻿CREATE TABLE [dbo].[MPI](
	[mpipn] [char](8) NOT NULL,
	[mpisurn] [char](24) NOT NULL,
	[mpifn1] [char](20) NOT NULL,
	[mpifn2] [char](1) NULL,
	[mpititle] [char](5) NULL,
	[mpidob] [datetime] NULL,
	[mpisex] [char](1) NULL,
	[mpipaddr1] [char](20) NULL,
	[mpipaddr2] [char](20) NULL,
	[mpipaddr3] [char](20) NULL,
	[mpipaddr4] [char](20) NULL,
	[mpippc] [char](8) NULL,
	[mpipdha] [char](3) NULL,
	[mpitelhome] [char](13) NULL,
	[mpitelbus] [char](13) NULL,
	[mpinhsno] [char](10) NULL,
	[mpinhsnostatus] [char](2) NULL,
	[mpiovs] [char](1) NULL,
	[mpidod] [datetime] NULL,
	[mpigp] [char](8) NULL,
	[mpipr] [char](6) NULL,
	[mpigdp] [char](8) NULL,
	[mpieps] [char](1) NULL
) ON [PRIMARY]
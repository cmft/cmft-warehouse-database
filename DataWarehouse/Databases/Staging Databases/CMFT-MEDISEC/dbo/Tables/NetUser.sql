﻿CREATE TABLE [dbo].[NetUser](
	[webname] [char](8) NOT NULL,
	[webpassword] [varchar](8) NULL,
	[webusertype] [varchar](1) NULL,
	[webpractice] [varchar](6) NULL,
	[webinits] [varchar](3) NULL,
	[websurn] [varchar](27) NULL,
	[webfn1] [varchar](20) NULL,
	[webtitle] [varchar](4) NULL,
	[webaddr1] [varchar](25) NULL,
	[webaddr2] [varchar](25) NULL,
	[webaddr3] [varchar](25) NULL,
	[webaddr4] [varchar](25) NULL,
	[webpc] [varchar](8) NULL,
	[webtel] [varchar](13) NULL,
	[webemail] [varchar](100) NULL,
	[weblastaccessed] [datetime] NULL,
	[webnewdocs] [int] NULL,
	[webip] [varchar](16) NULL,
	[webloginfails] [int] NULL,
	[webpwchanged] [datetime] NULL,
	[webipassigned] [varchar](20) NULL,
	[webfax] [varchar](20) NULL,
	[websuper] [char](1) NULL
) ON [PRIMARY]
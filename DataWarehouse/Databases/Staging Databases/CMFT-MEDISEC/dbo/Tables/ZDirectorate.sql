﻿CREATE TABLE [dbo].[ZDirectorate](
	[dirspeccode] [char](5) NOT NULL,
	[dirspecdesc] [char](20) NULL,
	[dirdirectorate] [char](20) NULL
) ON [PRIMARY]
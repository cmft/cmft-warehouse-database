﻿CREATE TABLE [dbo].[PRACTICE](
	[prac_id] [int] NOT NULL,
	[prac_code] [varchar](20) NOT NULL,
	[prac_name] [varchar](40) NULL,
	[prac_areacode] [varchar](20) NULL,
	[prac_dhacode] [varchar](20) NULL
) ON [PRIMARY]
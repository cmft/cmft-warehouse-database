﻿CREATE TABLE [dbo].[Patient](
	[pat_id] [int] NOT NULL,
	[PAT_EXTZIS_ID] [nvarchar](50) NULL,
	[PAT_INTZIS_ID] [varchar](20) NULL,
	[PAT_NATIONALNUMBER1] [varchar](40) NULL,
	[PAT_NATIONALNUMBER2] [varchar](40) NULL,
	[PAT_TITLE] [varchar](8) NULL,
	[PAT_FAMILYNAME] [varchar](80) NULL,
	[PAT_GIVENNAME] [varchar](80) NULL,
	[PAT_MAIDENNAME] [varchar](80) NULL,
	[PAT_SEX] [char](1) NULL,
	[PAT_BIRTHDATE] [datetime] NULL,
	[PAT_STREET1] [varchar](80) NULL,
	[PAT_STREET2] [varchar](80) NULL,
	[PAT_CITY] [varchar](40) NULL,
	[PAT_STATE] [varchar](40) NULL,
	[PAT_ZIPCODE] [varchar](12) NULL,
	[PAT_PHONE1] [varchar](25) NULL,
	[PAT_DEATHDATE] [datetime] NULL,
	[pat_number] [varchar](25) NULL,
	[prac_id] [int] NULL,
	[pracadd_id] [int] NULL,
	[relig_id] [int] NULL,
	[MARISTAT_ID] [int] NULL,
	[ETHORG_ID] [int] NULL
) ON [PRIMARY]
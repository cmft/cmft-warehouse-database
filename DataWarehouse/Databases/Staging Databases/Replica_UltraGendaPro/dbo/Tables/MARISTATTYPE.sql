﻿CREATE TABLE [dbo].[MARISTATTYPE](
	[MARISTAT_ID] [int] NOT NULL,
	[MARISTAT_CODE] [varchar](8) NULL,
	[MARISTAT_NAME] [varchar](40) NULL,
	[MARISTAT_DELETED] [bit] NOT NULL,
	[RowId] [uniqueidentifier] NOT NULL,
	[TimestampPro] [timestamp] NOT NULL
) ON [PRIMARY]
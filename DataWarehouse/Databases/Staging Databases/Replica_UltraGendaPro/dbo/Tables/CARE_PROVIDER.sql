﻿CREATE TABLE [dbo].[CARE_PROVIDER](
	[CPR_ID] [int] NOT NULL,
	[CPR_ABBREV] [varchar](20) NULL,
	[CPR_TYPE] [char](1) NULL,
	[CPR_AUTHCODE] [varchar](20) NULL,
	[CPR_FAMILYNAME] [varchar](40) NULL,
	[CPR_GIVENNAME] [varchar](40) NULL,
	[CPR_TITLE] [varchar](10) NULL,
	[CPR_EMAIL] [varchar](50) NULL,
	[CPR_PHONETYPE1] [varchar](10) NULL,
	[CPR_PHONE1] [varchar](20) NULL,
	[CPR_PHONETYPE2] [varchar](10) NULL,
	[CPR_PHONE2] [varchar](20) NULL,
	[CPR_DELETED] [bit] NOT NULL
) ON [PRIMARY]
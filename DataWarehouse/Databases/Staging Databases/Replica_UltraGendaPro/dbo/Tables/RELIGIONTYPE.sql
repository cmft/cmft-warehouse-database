﻿CREATE TABLE [dbo].[RELIGIONTYPE](
	[RELIG_ID] [int] NOT NULL,
	[RELIG_CODE] [varchar](8) NULL,
	[RELIG_NAME] [varchar](40) NULL,
	[RELIG_DELETED] [bit] NOT NULL,
	[RowId] [uniqueidentifier] NOT NULL,
	[TimestampPro] [timestamp] NOT NULL
) ON [PRIMARY]
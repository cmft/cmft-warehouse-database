﻿CREATE TABLE [dbo].[IOPCS](
	[OPCS] [nchar](5) NOT NULL,
	[PROCDESC] [nchar](60) NOT NULL,
	[CHGBL] [nchar](1) NULL,
	[EFG] [nchar](4) NULL,
	[LDF] [nchar](1) NULL,
	[Diagnostic_Flag] [nchar](1) NULL,
	[Diag_Line_Number] [nchar](4) NULL
) ON [PRIMARY]
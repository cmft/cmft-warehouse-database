﻿CREATE TABLE [dbo].[IPCPCT](
	[PC] [char](8) NOT NULL,
	[Old_SHA] [char](3) NULL,
	[Old_PCT] [char](5) NULL,
	[SHA] [char](3) NULL,
	[PCT] [char](5) NULL
) ON [PRIMARY]
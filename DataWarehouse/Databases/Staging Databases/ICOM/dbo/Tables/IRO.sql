﻿CREATE TABLE [dbo].[IRO](
	[OrgCode] [char](8) NOT NULL,
	[OrgName] [char](100) NULL,
	[Add1] [char](35) NULL,
	[Add2] [char](35) NULL,
	[Add3] [char](35) NULL,
	[Add4] [char](35) NULL,
	[Add5] [char](35) NULL,
	[PC] [char](8) NULL,
	[OpenDate] [char](8) NULL,
	[CloseDate] [char](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ICountry](
	[Country] [char](25) NULL,
	[PC_Pseudo] [char](10) NOT NULL,
	[Continent] [char](15) NULL,
	[Capital] [char](25) NULL
) ON [PRIMARY]
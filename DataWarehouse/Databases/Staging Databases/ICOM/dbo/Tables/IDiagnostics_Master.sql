﻿CREATE TABLE [dbo].[IDiagnostics_Master](
	[Line_ID] [char](3) NULL,
	[Group_ID] [char](4) NULL,
	[Area] [char](25) NULL,
	[Grouping] [char](40) NULL,
	[Description] [char](95) NULL
) ON [PRIMARY]
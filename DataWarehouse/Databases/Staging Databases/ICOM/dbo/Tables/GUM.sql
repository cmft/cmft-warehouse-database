﻿CREATE TABLE [dbo].[GUM](
	[MONTHNUM] [int] NOT NULL,
	[CLINHELD] [int] NULL,
	[CLINCANC] [int] NULL,
	[NEWATT] [int] NULL,
	[NEWDNA] [int] NULL,
	[FOLATT] [int] NULL,
	[FOLDNA] [int] NULL,
	[GPWREF] [int] NULL,
	[OTHREF] [int] NULL
) ON [PRIMARY]
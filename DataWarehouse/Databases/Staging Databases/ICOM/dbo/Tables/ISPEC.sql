﻿CREATE TABLE [dbo].[ISPEC](
	[MAINSPEF] [int] NULL,
	[SPEFDESC] [char](30) NULL,
	[SPEC] [char](4) NOT NULL,
	[DHSS] [char](4) NULL,
	[SPECDESC] [char](30) NULL,
	[SPEFCAT] [int] NULL,
	[SCATDESC] [char](25) NULL,
	[CAMRSPEC] [int] NULL,
	[CAMRSPNM] [char](30) NULL,
	[KORNERSPEC] [int] NULL,
	[KORNERDESC] [char](30) NULL
) ON [PRIMARY]
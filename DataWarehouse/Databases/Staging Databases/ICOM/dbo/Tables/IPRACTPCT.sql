﻿CREATE TABLE [dbo].[IPRACTPCT](
	[PRACT] [char](6) NOT NULL,
	[Old_HA] [char](3) NULL,
	[Old_PCT] [char](5) NULL,
	[HA] [char](3) NULL,
	[PCT] [char](5) NULL
) ON [PRIMARY]
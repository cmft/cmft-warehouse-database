﻿CREATE TABLE [dbo].[PTCA](
	[Monthnum] [int] NULL,
	[oldPCT] [char](5) NULL,
	[PCT] [char](5) NULL,
	[NEIP] [int] NULL,
	[ELIP] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CABG](
	[Monthnum] [int] NULL,
	[oldPCT] [char](5) NULL,
	[PCT] [char](5) NULL,
	[CABG] [int] NULL,
	[OTH_HRT] [int] NULL,
	[PRIVATE] [int] NULL
) ON [PRIMARY]
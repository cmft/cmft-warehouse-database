﻿CREATE TABLE [dbo].[IPCPCT_New](
	[Postcode_fixed] [char](8) NULL,
	[Postcode_var] [char](8) NOT NULL,
	[SHA] [char](3) NULL,
	[PCT] [char](5) NULL
) ON [PRIMARY]
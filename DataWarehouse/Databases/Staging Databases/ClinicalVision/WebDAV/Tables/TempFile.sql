﻿CREATE TABLE [WebDAV].[TempFile](
	[uuid] [nvarchar](40) NOT NULL,
	[name] [nvarchar](255) NULL,
	[clientLocation] [nvarchar](255) NULL,
	[created] [datetime] NULL,
	[lockId] [nvarchar](40) NULL,
	[lockedBy] [nvarchar](255) NULL,
	[lockedUntil] [datetime] NULL,
	[lockLength] [numeric](19, 0) NULL,
	[contents] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
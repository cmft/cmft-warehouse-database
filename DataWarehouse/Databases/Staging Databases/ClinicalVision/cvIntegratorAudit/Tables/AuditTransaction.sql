﻿CREATE TABLE [cvIntegratorAudit].[AuditTransaction](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TransactionTime] [datetime] NULL,
	[User] [nvarchar](20) NULL,
	[UserType] [nvarchar](40) NULL,
	[UserName] [nvarchar](255) NULL,
	[Location] [nvarchar](40) NULL,
	[Source] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[Subject] [nvarchar](20) NULL,
	[SubjectType] [nvarchar](40) NULL,
	[SubjectName] [nvarchar](255) NULL
) ON [PRIMARY]
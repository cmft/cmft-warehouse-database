﻿CREATE TABLE [RenalTransplant].[SystemDefaults](
	[oid] [int] NOT NULL,
	[RenalDefaultOID] [int] NULL,
	[TransplantDefaultOID] [int] NULL
) ON [PRIMARY]
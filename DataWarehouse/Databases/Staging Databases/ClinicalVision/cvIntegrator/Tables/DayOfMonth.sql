﻿CREATE TABLE [cvIntegrator].[DayOfMonth](
	[oid] [int] NOT NULL,
	[rev_ScheduledJobDayOfMonthsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DayOfMonth] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [cvIntegrator].[ScheduledJob](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Weekly] [tinyint] NULL,
	[Monthly] [tinyint] NULL,
	[AtTime] [datetime] NULL,
	[LastRan] [datetime] NULL,
	[FromDate] [datetime] NULL,
	[FromDateFilterEnable] [tinyint] NULL,
	[Name] [nvarchar](40) NULL,
	[Description] [nvarchar](1000) NULL,
	[RunStatus] [nvarchar](1000) NULL
) ON [PRIMARY]
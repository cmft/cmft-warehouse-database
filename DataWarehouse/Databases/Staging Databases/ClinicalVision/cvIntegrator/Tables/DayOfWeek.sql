﻿CREATE TABLE [cvIntegrator].[DayOfWeek](
	[oid] [int] NOT NULL,
	[rev_ScheduledJobDayOfWeeksOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DayOfWeek] [int] NULL
) ON [PRIMARY]
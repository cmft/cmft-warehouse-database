﻿CREATE TABLE [cvIntegrator].[LabResultMapping](
	[oid] [int] NOT NULL,
	[MappableMsgFieldOID] [int] NULL,
	[TimingQualifier] [int] NULL,
	[IsMicrobiology] [tinyint] NULL DEFAULT ('0'),
	[VendorId] [nvarchar](100) NULL
) ON [PRIMARY]
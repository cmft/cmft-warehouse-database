﻿CREATE TABLE [cvIntegrator].[CrownWebLogEntry](
	[oid] [int] NOT NULL,
	[CrownWebMessageOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Severity] [int] NULL,
	[CreationDate] [datetime] NULL,
	[ReportDate] [datetime] NULL,
	[Text] [nvarchar](1000) NULL
) ON [PRIMARY]
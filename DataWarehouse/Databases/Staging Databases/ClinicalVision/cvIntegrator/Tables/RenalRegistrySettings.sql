﻿CREATE TABLE [cvIntegrator].[RenalRegistrySettings](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ServerMappingFilesDirPath] [nvarchar](100) NULL,
	[TreatmentCentreID] [nvarchar](100) NULL
) ON [PRIMARY]
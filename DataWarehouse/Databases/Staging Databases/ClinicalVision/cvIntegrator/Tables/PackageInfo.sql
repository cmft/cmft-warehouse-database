﻿CREATE TABLE [cvIntegrator].[PackageInfo](
	[Version] [nvarchar](255) NOT NULL,
	[UUID] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [cvIntegrator].[RenalRegistryReportData](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Year] [int] NULL,
	[Quarter] [int] NULL,
	[PatientSelectData] [nvarchar](max) NULL,
	[RenalRegistryReportData] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [cvIntegrator].[SiteCodeMapping](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[CV4EnumCode] [int] NULL,
	[CV4Enum] [int] NULL,
	[FieldValue] [nvarchar](100) NULL
) ON [PRIMARY]
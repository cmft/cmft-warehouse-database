﻿CREATE TABLE [cvIntegrator].[CodeMapData](
	[oid] [int] NOT NULL,
	[CodeMapOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[valChanged] [tinyint] NULL DEFAULT ('0'),
	[Key1] [nvarchar](100) NULL,
	[Key2] [nvarchar](100) NULL,
	[RenalRegistryCode] [nvarchar](50) NULL,
	[Modality] [nvarchar](100) NULL
) ON [PRIMARY]
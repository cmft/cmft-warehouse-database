﻿CREATE TABLE [cvIntegrator].[OutgoingInterface](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Type] [int] NULL,
	[Enabled] [tinyint] NULL DEFAULT ('0'),
	[Url] [nvarchar](100) NULL
) ON [PRIMARY]
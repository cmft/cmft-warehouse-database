﻿CREATE TABLE [cvIntegrator].[MappableMsgField](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[FieldID] [int] NULL,
	[CV4Enum] [int] NULL,
	[InterfaceType] [int] NULL
) ON [PRIMARY]
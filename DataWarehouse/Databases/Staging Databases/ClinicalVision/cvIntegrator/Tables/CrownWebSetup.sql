﻿CREATE TABLE [cvIntegrator].[CrownWebSetup](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ESAMedicationGroup] [int] NULL,
	[IronMedicationGroup] [int] NULL,
	[VitaminDMedicationGroup] [int] NULL
) ON [PRIMARY]
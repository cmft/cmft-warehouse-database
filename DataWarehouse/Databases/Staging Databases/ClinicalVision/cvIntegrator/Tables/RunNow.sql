﻿CREATE TABLE [cvIntegrator].[RunNow](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[ReTransmit] [tinyint] NULL,
	[PatientMRNFilterEnable] [tinyint] NULL,
	[FromDateFilterEnable] [tinyint] NULL,
	[ToDateFilterEnable] [tinyint] NULL
) ON [PRIMARY]
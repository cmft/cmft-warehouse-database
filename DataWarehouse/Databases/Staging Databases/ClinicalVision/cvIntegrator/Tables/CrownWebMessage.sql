﻿CREATE TABLE [cvIntegrator].[CrownWebMessage](
	[oid] [int] NOT NULL,
	[ResponseOID] [int] NULL,
	[SubmissionOID] [int] NULL,
	[LogOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ReportDate] [datetime] NULL,
	[Status] [int] NULL,
	[Type] [int] NULL,
	[Revision] [int] NULL,
	[Latest] [tinyint] NULL,
	[CreationDate] [datetime] NULL,
	[AckDate] [datetime] NULL,
	[DefResDate] [datetime] NULL,
	[Identifier] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [cvIntegrator].[BillingGeneralSettings](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SendingFacility] [nvarchar](100) NULL
) ON [PRIMARY]
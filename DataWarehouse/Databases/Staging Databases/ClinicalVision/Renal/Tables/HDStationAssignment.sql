﻿CREATE TABLE [Renal].[HDStationAssignment](
	[oid] [int] NOT NULL,
	[PatientDateRangeOID] [int] NULL,
	[PatternOID] [int] NULL,
	[RenalPatientOID] [int] NULL,
	[DialysisStationOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DurationInMinutes] [int] NULL,
	[RenalShift] [int] NULL,
	[StartTime] [datetime] NULL DEFAULT (getdate()),
	[DefaultIsolationStatus] [int] NULL,
	[StopTime] [datetime] NULL DEFAULT (getdate()),
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
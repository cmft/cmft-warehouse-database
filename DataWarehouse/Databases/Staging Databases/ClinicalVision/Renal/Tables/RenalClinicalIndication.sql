﻿CREATE TABLE [Renal].[RenalClinicalIndication](
	[oid] [int] NOT NULL,
	[rev_RenalPatientRenalClinicalIndicationsOID] [int] NULL,
	[rev_RenalRegInfoRenalClinicalIndicationsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ESRDClinicalIndication] [int] NULL
) ON [PRIMARY]
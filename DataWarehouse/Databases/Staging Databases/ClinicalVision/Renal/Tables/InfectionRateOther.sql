﻿CREATE TABLE [Renal].[InfectionRateOther](
	[oid] [int] NOT NULL,
	[rev_InfectionRateDefaultsInfectionRateOthersOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Other] [int] NULL
) ON [PRIMARY]
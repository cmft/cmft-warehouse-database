﻿CREATE TABLE [Renal].[RenalCoMorbidConditions](
	[oid] [int] NOT NULL,
	[rev_RenalPatientRenalCoMorbidConditionsesOID] [int] NULL,
	[rev_RenalRegInfoRenalCoMorbidConditionsesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ESRDCoMorbidConditions] [int] NULL,
	[Found] [int] NULL
) ON [PRIMARY]
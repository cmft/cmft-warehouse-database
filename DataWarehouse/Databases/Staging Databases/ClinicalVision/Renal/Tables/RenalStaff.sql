﻿CREATE TABLE [Renal].[RenalStaff](
	[oid] [int] NOT NULL,
	[RenalTrayGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
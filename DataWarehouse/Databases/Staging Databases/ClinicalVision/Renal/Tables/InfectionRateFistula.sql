﻿CREATE TABLE [Renal].[InfectionRateFistula](
	[oid] [int] NOT NULL,
	[rev_InfectionRateDefaultsInfectionRateFistulasOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Fistula] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Renal].[InfectionRateTempCath](
	[oid] [int] NOT NULL,
	[rev_InfectionRateDefaultsInfectionRateTempCathsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TemporaryCatheter] [int] NULL
) ON [PRIMARY]
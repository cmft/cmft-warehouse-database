﻿CREATE TABLE [Renal].[RenalTrayGroup](
	[oid] [int] NOT NULL,
	[DefaultRenalSubjectTrayOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
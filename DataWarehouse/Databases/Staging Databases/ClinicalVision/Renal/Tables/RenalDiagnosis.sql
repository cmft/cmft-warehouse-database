﻿CREATE TABLE [Renal].[RenalDiagnosis](
	[oid] [int] NOT NULL,
	[OnsetDate] [datetime] NULL,
	[RenalPrimaryDiagnosis] [int] NULL,
	[BasisForDiagnosis] [int] NULL,
	[OtherDiagnosis] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
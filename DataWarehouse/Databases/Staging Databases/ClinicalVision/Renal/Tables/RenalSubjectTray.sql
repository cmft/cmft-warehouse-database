﻿CREATE TABLE [Renal].[RenalSubjectTray](
	[oid] [int] NOT NULL,
	[RenalTrayGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Renal].[PedriaticRegistration](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PedriaticPatient] [tinyint] NULL DEFAULT ('0'),
	[ParentsGenetRelationship] [tinyint] NULL DEFAULT ('0'),
	[SingleAdultResponsible] [tinyint] NULL DEFAULT ('0'),
	[OtherMembersAlsoESRF] [tinyint] NULL DEFAULT ('0'),
	[OtherMemSameRenalDisease] [tinyint] NULL DEFAULT ('0'),
	[DTRefAdultRenalSvcs] [datetime] NULL,
	[DateRegisteredWithBAPN] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Renal].[InfectionRateNaturalGraft](
	[oid] [int] NOT NULL,
	[rev_InfectionRateDefaultsInfectionRateNaturalGraftsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[NaturalGraft] [int] NULL
) ON [PRIMARY]
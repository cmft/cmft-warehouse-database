﻿CREATE TABLE [Renal].[InfectionRatePermCath](
	[oid] [int] NOT NULL,
	[rev_InfectionRateDefaultsInfectionRatePermCathsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PermanentCatheter] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Renal].[RenalAssociatedCause](
	[oid] [int] NOT NULL,
	[rev_RenalRegInfoRenalAssociatedCausesOID] [int] NULL,
	[rev_RenalPatientRenalAssociatedCausesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ESRDAssociatedCause] [int] NULL
) ON [PRIMARY]
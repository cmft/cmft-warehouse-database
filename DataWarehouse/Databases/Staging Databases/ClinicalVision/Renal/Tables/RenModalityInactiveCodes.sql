﻿CREATE TABLE [Renal].[RenModalityInactiveCodes](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RenalModality] [int] NULL,
	[InactiveDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Renal].[AssociatedCauseOfDeath](
	[oid] [int] NOT NULL,
	[rev_RenalMortalityAssociatedCauseOfDeathsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ESRDCauseOfDeath] [int] NULL
) ON [PRIMARY]
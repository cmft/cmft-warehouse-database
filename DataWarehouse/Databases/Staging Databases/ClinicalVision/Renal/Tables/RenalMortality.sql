﻿CREATE TABLE [Renal].[RenalMortality](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ESRDCauseOfDeath] [int] NULL,
	[RRTAtDeath] [int] NULL,
	[RenalTransplant] [int] NULL,
	[DateofLastTransplant] [datetime] NULL,
	[KidneyFunctioningAtDeath] [int] NULL,
	[RRTResumed] [int] NULL,
	[Comments] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
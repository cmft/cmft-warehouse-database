﻿CREATE TABLE [Renal].[RenalRegistration](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RegistrationDate] [datetime] NULL DEFAULT (getdate()),
	[InactiveDate] [datetime] NULL,
	[ReasonInactive] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Renal].[RenalRegInfo](
	[oid] [int] NOT NULL,
	[PhysicalExamOID] [int] NULL,
	[RenalDiagnosisOID] [int] NULL,
	[RenalPatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Remarks] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Renal].[RenalReportGroupPatients](
	[oid] [int] NOT NULL,
	[RenalPatientOID] [int] NULL,
	[RenalReportGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
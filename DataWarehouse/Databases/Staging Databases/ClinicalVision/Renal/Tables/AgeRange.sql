﻿CREATE TABLE [Renal].[AgeRange](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[FromAge] [float] NULL,
	[FromAgeMonths] [float] NULL,
	[ThruAge] [float] NULL,
	[ThruAgeMonths] [float] NULL,
	[FromAgeUnits] [int] NULL,
	[ThruAgeUnits] [int] NULL,
	[ThruAgeMonthUnits] [int] NULL,
	[FromAgeINYears] [float] NULL,
	[ThruAgeINYears] [float] NULL,
	[FromAgeMonthUnits] [int] NULL
) ON [PRIMARY]
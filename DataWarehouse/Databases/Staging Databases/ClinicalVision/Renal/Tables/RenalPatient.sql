﻿CREATE TABLE [Renal].[RenalPatient](
	[oid] [int] NOT NULL,
	[PedriaticRegistrationOID] [int] NULL,
	[PrimaryHDStationAssignmentOID] [int] NULL,
	[PrimaryNephrologistOID] [int] NULL,
	[RegistrationHeightOID] [int] NULL,
	[RegistrationWeightOID] [int] NULL,
	[RenalDiagnosisOID] [int] NULL,
	[RenalMortalityOID] [int] NULL,
	[RenalRegistrationOID] [int] NULL,
	[RoundingAssignmentOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DateFirstSeen] [datetime] NULL,
	[PreDialysisFollowUp] [int] NULL,
	[PreDialysisErythropoietin] [int] NULL,
	[UKRROptOut] [tinyint] NULL DEFAULT ('0'),
	[Remarks] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
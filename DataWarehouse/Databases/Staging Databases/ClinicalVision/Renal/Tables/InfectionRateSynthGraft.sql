﻿CREATE TABLE [Renal].[InfectionRateSynthGraft](
	[oid] [int] NOT NULL,
	[rev_InfectionRateDefaultsInfectionRateSynthGraftsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SyntheticGraft] [int] NULL
) ON [PRIMARY]
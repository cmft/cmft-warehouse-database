﻿CREATE TABLE [Renal].[InfectionRateDefaults](
	[oid] [int] NOT NULL,
	[DialysisProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
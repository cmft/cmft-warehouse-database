﻿CREATE TABLE [Core].[OtherProvider](
	[oid] [int] NOT NULL,
	[AddressGroupOID] [int] NULL,
	[PhoneNumberGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ProviderType] [int] NULL,
	[Name] [nvarchar](40) NULL
) ON [PRIMARY]
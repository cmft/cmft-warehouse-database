﻿CREATE TABLE [Core].[Doctors](
	[oid] [int] NOT NULL,
	[MedicalStaffOfRecordOID] [int] NULL,
	[PatientDateRangeOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
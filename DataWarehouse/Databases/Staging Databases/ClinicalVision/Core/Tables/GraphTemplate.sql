﻿CREATE TABLE [Core].[GraphTemplate](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[GraphName] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[SubjectKey] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
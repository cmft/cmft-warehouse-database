﻿CREATE TABLE [Core].[MachineProgram](
	[oid] [int] NOT NULL,
	[rev_MachineSettingsMachineProgramsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MachineProgram] [int] NULL
) ON [PRIMARY]
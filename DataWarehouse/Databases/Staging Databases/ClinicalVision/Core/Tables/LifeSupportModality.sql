﻿CREATE TABLE [Core].[LifeSupportModality](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ECMO] [tinyint] NULL,
	[PGE] [tinyint] NULL,
	[Ventilator] [tinyint] NULL,
	[IABP] [tinyint] NULL,
	[IVInatropes] [tinyint] NULL,
	[OtherMechanism] [tinyint] NULL,
	[OtherLifeSupportMechanism] [nvarchar](100) NULL
) ON [PRIMARY]
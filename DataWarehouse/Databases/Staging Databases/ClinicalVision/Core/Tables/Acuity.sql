﻿CREATE TABLE [Core].[Acuity](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Ambulatory] [int] NULL,
	[HearingImpairment] [int] NULL,
	[IsAmbulatory] [int] NULL,
	[IsInterpreterRequired] [int] NULL,
	[SpeechImpairment] [int] NULL,
	[SightImpairment] [int] NULL,
	[WheelchairStatus] [int] NULL,
	[Oxygen] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[InfectionEtiology](
	[oid] [int] NOT NULL,
	[rev_InfectionPDInfectionEtiologiesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[InfectionEtiology] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[AdminDualSignoffSetting](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RequireSecondaryPIN] [tinyint] NULL
) ON [PRIMARY]
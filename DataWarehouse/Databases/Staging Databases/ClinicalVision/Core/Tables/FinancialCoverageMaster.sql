﻿CREATE TABLE [Core].[FinancialCoverageMaster](
	[oid] [int] NOT NULL,
	[AddressGroupOID] [int] NULL,
	[DateRangeOID] [int] NULL,
	[PhoneNumberGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[Name] [nvarchar](45) NULL,
	[Notes] [nvarchar](max) NULL,
	[InternalCode] [nvarchar](40) NULL,
	[ImportID] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
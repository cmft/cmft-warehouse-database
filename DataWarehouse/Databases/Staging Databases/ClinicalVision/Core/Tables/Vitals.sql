﻿CREATE TABLE [Core].[Vitals](
	[oid] [int] NOT NULL,
	[HeightOID] [int] NULL,
	[SittingBPandHROID] [int] NULL,
	[StandingBPandHROID] [int] NULL,
	[TemperatureOID] [int] NULL,
	[WeightOID] [int] NULL,
	[Source] [nvarchar](40) NULL
) ON [PRIMARY]
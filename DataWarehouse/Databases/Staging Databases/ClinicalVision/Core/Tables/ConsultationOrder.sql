﻿CREATE TABLE [Core].[ConsultationOrder](
	[oid] [int] NOT NULL,
	[ConsultRequestOID] [int] NULL,
	[ConsultType] [int] NULL,
	[IsCareTeamDocumentation] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
﻿CREATE TABLE [Core].[MilitaryHistory](
	[oid] [int] NOT NULL,
	[MilitaryServiceDatesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MilitaryService] [int] NULL,
	[VeteranStatus] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
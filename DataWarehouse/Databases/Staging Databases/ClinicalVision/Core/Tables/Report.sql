﻿CREATE TABLE [Core].[Report](
	[oid] [int] NOT NULL,
	[rev_HomePageInfoIncludedReportsOID] [int] NULL,
	[rev_HomePageInfoExcludedReportsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Weight](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Weight] [float] NULL,
	[WeightUnits] [int] NULL
) ON [PRIMARY]
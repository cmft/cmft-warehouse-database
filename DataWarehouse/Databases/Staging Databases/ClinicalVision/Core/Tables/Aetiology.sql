﻿CREATE TABLE [Core].[Aetiology](
	[oid] [int] NOT NULL,
	[rev_AbstractComplicationAetiologiesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[InfectionAetiology] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Specimen](
	[oid] [int] NOT NULL,
	[VolumeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SpecimenSource] [int] NULL,
	[BodySite] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[Medication](
	[oid] [int] NOT NULL,
	[AdministrationOID] [int] NULL,
	[DoseOID] [int] NULL,
	[DrugOrderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicationFrequency] [int] NULL,
	[AllowSubstitutions] [tinyint] NULL,
	[DrugType] [int] NULL,
	[SupplySource] [int] NULL,
	[MedicationGroup] [int] NULL,
	[NonESRD] [tinyint] NULL DEFAULT ('0'),
	[ReptDrugName] [nvarchar](4000) NULL
) ON [PRIMARY]
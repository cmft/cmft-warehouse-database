﻿CREATE TABLE [Core].[ServiceMasterConsult](
	[oid] [int] NOT NULL,
	[CTDefaultOID] [int] NULL,
	[ConsultType] [int] NULL,
	[IsCareTeamDocumentation] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Mortality](
	[oid] [int] NOT NULL,
	[CauseOfDeathOID] [int] NULL,
	[PlaceOfDeathAddressOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AutopsyPerformed] [int] NULL,
	[AutopsyReportDate] [datetime] NULL,
	[HospitalStatusAtDeath] [int] NULL,
	[DateRecorded] [datetime] NULL,
	[StateOfRecord] [int] NULL,
	[CountyOfRecord] [int] NULL,
	[PlaceOfDeath] [int] NULL,
	[InformationSource] [int] NULL,
	[HeadTrauma] [int] NULL,
	[ChestTrauma] [int] NULL,
	[AbdomenTrauma] [int] NULL,
	[OtherTrauma] [int] NULL,
	[FirstBrainStemDateTime] [datetime] NULL,
	[SecondBrainStemDateTime] [datetime] NULL,
	[FixedDilatedPupils] [datetime] NULL,
	[haveConingTime] [int] NULL,
	[ConingTime] [datetime] NULL,
	[isBrainInjuryAdmission] [int] NULL,
	[BrainInjuryDateTime] [datetime] NULL,
	[VentilationStartDateTime] [datetime] NULL,
	[VentilationDays] [int] NULL,
	[VentilationEnd] [datetime] NULL,
	[CirculatoryArrestDateTime] [datetime] NULL,
	[MechExtCardiacMassUsed] [int] NULL,
	[CardiacArrestDateTime] [datetime] NULL,
	[TreatWithdrawnDateTime] [datetime] NULL,
	[ResuscitationStart] [datetime] NULL,
	[ResuscitationEnd] [datetime] NULL,
	[TransplantOnly] [tinyint] NULL DEFAULT ('0'),
	[Research] [tinyint] NULL DEFAULT ('0'),
	[PFiscalCoroner] [tinyint] NULL DEFAULT ('0'),
	[TransplantOrResearch] [tinyint] NULL DEFAULT ('0'),
	[ODRChecked] [tinyint] NULL DEFAULT ('0'),
	[RegisteredDonor] [tinyint] NULL DEFAULT ('0'),
	[DonorCardCarried] [tinyint] NULL DEFAULT ('0'),
	[ControlledUncontrolled] [int] NULL,
	[RespiratoryArrestDateTime] [datetime] NULL,
	[AutopsyReportNo] [nvarchar](20) NULL,
	[DeathCertificateNo] [nvarchar](20) NULL,
	[RecorderOfDeath] [nvarchar](40) NULL,
	[OtherTraumaText] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[MimsIndication](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IndicationText] [nvarchar](max) NULL,
	[IndicationTextTagFree] [nvarchar](max) NULL,
	[Type] [nvarchar](50) NULL,
	[StreamlinedAuthorityCode] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
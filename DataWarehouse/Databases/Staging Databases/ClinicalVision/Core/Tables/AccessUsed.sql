﻿CREATE TABLE [Core].[AccessUsed](
	[oid] [int] NOT NULL,
	[VascularAccessOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[VascularAccessType] [int] NULL,
	[VascularAccessLocation] [int] NULL,
	[InfectionAccess] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[DirectNotification](
	[oid] [int] NOT NULL,
	[MessageOID] [int] NULL,
	[InternalOID] [int] NULL,
	[ObservationOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[NotifyCareTeam] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
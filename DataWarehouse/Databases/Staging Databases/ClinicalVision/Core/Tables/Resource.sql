﻿CREATE TABLE [Core].[Resource](
	[oid] [int] NOT NULL,
	[DefaultScheduleOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[CarePlanGoal](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
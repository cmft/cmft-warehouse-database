﻿CREATE TABLE [Core].[Survey](
	[oid] [int] NOT NULL,
	[ReasonSurveyRefused] [int] NULL,
	[SurveyStatus] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[HemodialysisSession](
	[oid] [int] NOT NULL,
	[DialysisMachinesOID] [int] NULL,
	[DialysisStationOID] [int] NULL,
	[PatientOID] [int] NULL,
	[HemodialysisServiceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Time] [datetime] NULL,
	[SessionId] [nvarchar](50) NULL,
	[InterfaceId] [nvarchar](50) NULL,
	[PatientDialysisId] [nvarchar](50) NULL,
	[MachineId] [nvarchar](50) NULL,
	[PortId] [nvarchar](50) NULL
) ON [PRIMARY]
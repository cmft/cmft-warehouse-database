﻿CREATE TABLE [Core].[Allergy](
	[oid] [int] NOT NULL,
	[AllergenOID] [int] NULL,
	[AllergySeverity] [int] NULL,
	[AllergyReaction] [int] NULL
) ON [PRIMARY]
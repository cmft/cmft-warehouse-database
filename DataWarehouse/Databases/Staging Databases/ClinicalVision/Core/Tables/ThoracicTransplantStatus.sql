﻿CREATE TABLE [Core].[ThoracicTransplantStatus](
	[oid] [int] NOT NULL,
	[ThoracicPrimaryDiagnosis] [int] NULL,
	[ThoracicPrimaryDiagOth] [nvarchar](40) NULL
) ON [PRIMARY]
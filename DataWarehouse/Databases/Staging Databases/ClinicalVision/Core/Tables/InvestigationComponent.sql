﻿CREATE TABLE [Core].[InvestigationComponent](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[BooleanValue] [tinyint] NULL DEFAULT ('0'),
	[CodedValue] [int] NULL,
	[DateTimeValue] [datetime] NULL,
	[IntegerValue] [int] NULL,
	[RealValue] [float] NULL,
	[CodesetType] [int] NULL,
	[DataType] [int] NULL,
	[DurationValue] [datetime] NULL,
	[DurationDaysValue] [int] NULL,
	[StringValue] [nvarchar](1000) NULL,
	[Name] [nvarchar](255) NULL,
	[CodedStringValue] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Justification](
	[oid] [int] NOT NULL,
	[LabResultOID] [int] NULL,
	[rev_OrderTableJustificationsOID] [int] NULL,
	[rev_ServiceJustificationsOID] [int] NULL,
	[rev_ServiceMasterJustificationsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[LabResultName] [int] NULL,
	[OrderOrService] [int] NULL
) ON [PRIMARY]
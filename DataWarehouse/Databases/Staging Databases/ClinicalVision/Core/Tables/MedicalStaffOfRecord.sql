﻿CREATE TABLE [Core].[MedicalStaffOfRecord](
	[oid] [int] NOT NULL,
	[MedicalStaffOID] [int] NULL,
	[OtherPhysicianOID] [int] NULL,
	[rev_SurgicalHistorySurgicalTeamOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
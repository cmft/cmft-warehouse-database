﻿CREATE TABLE [Core].[ReferralInformation](
	[oid] [int] NOT NULL,
	[PersonProvidingReferralInformationOID] [int] NULL,
	[ReferringPhysicianOID] [int] NULL,
	[ReferringProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicalCondition] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[MetabolicAndNutrition](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AlbGlobRatioNum] [float] NULL,
	[AlbuminNum] [float] NULL,
	[AmylaseNum] [float] NULL,
	[CPKNum] [float] NULL,
	[GlobulinNum] [float] NULL,
	[GlucoseNum] [float] NULL,
	[LDHNum] [float] NULL,
	[LipaseNum] [float] NULL,
	[PreAlbuminNum] [float] NULL,
	[ProteinNum] [float] NULL,
	[ZincNum] [float] NULL,
	[MagnesiumNum] [float] NULL,
	[Show] [tinyint] NULL,
	[CreatininePhosphokinaseNum] [float] NULL,
	[Notes] [tinyint] NULL DEFAULT ('0'),
	[AlbGlobRatioStr] [nvarchar](50) NULL,
	[AlbGlobRatioFlg] [nvarchar](20) NULL,
	[AlbuminStr] [nvarchar](50) NULL,
	[AlbuminFlg] [nvarchar](20) NULL,
	[AmylaseStr] [nvarchar](50) NULL,
	[AmylaseFlg] [nvarchar](20) NULL,
	[CreatininePhosphokinaseStr] [nvarchar](50) NULL,
	[CPKFlg] [nvarchar](20) NULL,
	[GlobulinStr] [nvarchar](50) NULL,
	[GlobulinFlg] [nvarchar](20) NULL,
	[GlucoseStr] [nvarchar](50) NULL,
	[GlucoseFlg] [nvarchar](20) NULL,
	[LDHStr] [nvarchar](50) NULL,
	[LDHFlg] [nvarchar](20) NULL,
	[LipaseStr] [nvarchar](50) NULL,
	[LipaseFlg] [nvarchar](20) NULL,
	[PreAlbuminStr] [nvarchar](50) NULL,
	[PreAlbuminFlg] [nvarchar](20) NULL,
	[ProteinStr] [nvarchar](50) NULL,
	[ProteinFlg] [nvarchar](20) NULL,
	[ZincStr] [nvarchar](50) NULL,
	[ZincFlg] [nvarchar](20) NULL,
	[MagnesiumStr] [nvarchar](50) NULL,
	[MagnesiumFlg] [nvarchar](20) NULL,
	[CreatininePhosphokinaseFlg] [nvarchar](20) NULL
) ON [PRIMARY]
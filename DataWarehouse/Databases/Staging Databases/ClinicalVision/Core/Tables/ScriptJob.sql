﻿CREATE TABLE [Core].[ScriptJob](
	[oid] [int] NOT NULL,
	[OwnerType] [int] NULL,
	[OwnerOID] [nvarchar](50) NULL,
	[ScriptName] [nvarchar](50) NULL
) ON [PRIMARY]
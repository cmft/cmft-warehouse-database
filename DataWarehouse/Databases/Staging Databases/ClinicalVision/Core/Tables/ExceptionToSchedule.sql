﻿CREATE TABLE [Core].[ExceptionToSchedule](
	[oid] [int] NOT NULL,
	[ScheduleOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[OldDate] [datetime] NULL,
	[ReasonForException] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
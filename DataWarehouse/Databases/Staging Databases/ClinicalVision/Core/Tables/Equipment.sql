﻿CREATE TABLE [Core].[Equipment](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[EquipmentType] [int] NULL,
	[importID] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[Name] [nvarchar](40) NULL,
	[SerialNumber] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[HemodialysisService](
	[oid] [int] NOT NULL,
	[AccessUsedOID] [int] NULL,
	[HemodialysisOID] [int] NULL,
	[HemodialysisTreatmentSummaryOID] [int] NULL,
	[LastHemodialysisServiceOID] [int] NULL,
	[MachineCheckOID] [int] NULL,
	[PostVitalsOID] [int] NULL,
	[PreVitalsOID] [int] NULL,
	[StartHDFlowsheetOID] [int] NULL,
	[StopHDFlowsheetOID] [int] NULL,
	[TargetFluidLossOID] [int] NULL,
	[PreSummaryOID] [int] NULL,
	[PostSummaryOID] [int] NULL,
	[IsConnected] [tinyint] NULL,
	[importID] [nvarchar](50) NULL,
	[ServiceIdentifier] [nvarchar](50) NULL
) ON [PRIMARY]
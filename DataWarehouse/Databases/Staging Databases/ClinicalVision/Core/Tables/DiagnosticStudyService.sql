﻿CREATE TABLE [Core].[DiagnosticStudyService](
	[oid] [int] NOT NULL,
	[DiagnosticTestOID] [int] NULL,
	[LabTestOID] [int] NULL,
	[DiagnosticResultedStatus] [int] NULL,
	[AccessionNumber] [nvarchar](20) NULL
) ON [PRIMARY]
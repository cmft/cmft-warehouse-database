﻿CREATE TABLE [Core].[LungFactors](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[FVC] [int] NULL,
	[FeV1] [int] NULL,
	[pCO2] [int] NULL,
	[FeV1FVC] [float] NULL,
	[IVTxPulmonarySepsis] [int] NULL,
	[CorticosteroidDependency] [int] NULL,
	[SixMinuteWalk] [int] NULL,
	[PanResistantBacterialInf] [int] NULL,
	[ThoracotomyLeft] [int] NULL,
	[ThoracotomyRight] [int] NULL
) ON [PRIMARY]
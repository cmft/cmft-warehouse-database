﻿CREATE TABLE [Core].[NewDocumentation](
	[oid] [int] NOT NULL,
	[ActivityOID] [int] NULL,
	[ScheduleOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ScheduledDateTime] [datetime] NULL
) ON [PRIMARY]
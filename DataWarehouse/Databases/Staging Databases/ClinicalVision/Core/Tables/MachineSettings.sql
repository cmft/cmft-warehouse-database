﻿CREATE TABLE [Core].[MachineSettings](
	[oid] [int] NOT NULL,
	[DialysateBathOID] [int] NULL,
	[DialysateTemperatureOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[BloodFlow] [int] NULL,
	[DialysateFlow] [int] NULL,
	[Conductivity] [float] NULL
) ON [PRIMARY]
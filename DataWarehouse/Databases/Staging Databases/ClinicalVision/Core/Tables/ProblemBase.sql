﻿CREATE TABLE [Core].[ProblemBase](
	[oid] [int] NOT NULL,
	[ConfirmedByOID] [int] NULL,
	[InitialAssessmentByOID] [int] NULL,
	[ConfirmationMethod] [int] NULL,
	[ConfirmationStatus] [int] NULL,
	[ReasonProblemStopped] [int] NULL,
	[ProblemListCategory] [int] NULL
) ON [PRIMARY]
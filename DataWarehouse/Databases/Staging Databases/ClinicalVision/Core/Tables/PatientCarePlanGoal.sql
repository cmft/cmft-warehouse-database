﻿CREATE TABLE [Core].[PatientCarePlanGoal](
	[oid] [int] NOT NULL,
	[CarePlanGoalOID] [int] NULL,
	[DateRangeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[GoalMet] [tinyint] NULL,
	[DateMet] [datetime] NULL,
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
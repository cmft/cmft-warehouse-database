﻿CREATE TABLE [Core].[OrderNotification](
	[oid] [int] NOT NULL,
	[MedicalStaffOfRecordOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[NotifyPhysician] [tinyint] NULL DEFAULT ('0'),
	[NotifyPatient] [tinyint] NULL DEFAULT ('0'),
	[CallBackPhone] [nvarchar](30) NULL
) ON [PRIMARY]
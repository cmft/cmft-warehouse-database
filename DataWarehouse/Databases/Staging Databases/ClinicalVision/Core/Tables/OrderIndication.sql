﻿CREATE TABLE [Core].[OrderIndication](
	[oid] [int] NOT NULL,
	[MedicalDiagnosisOID] [int] NULL,
	[ProblemDiagnosisOID] [int] NULL,
	[ServiceMasterProblemDiagnosisOID] [int] NULL,
	[MimsIndicationOID] [int] NULL,
	[OrderTableOtherOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
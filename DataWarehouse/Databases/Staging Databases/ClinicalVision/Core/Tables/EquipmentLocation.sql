﻿CREATE TABLE [Core].[EquipmentLocation](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[EquipmentOID] [int] NULL,
	[LocationOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
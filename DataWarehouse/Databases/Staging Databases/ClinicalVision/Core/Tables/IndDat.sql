﻿CREATE TABLE [Core].[IndDat](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IndCode] [int] NULL,
	[Indication] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[IndicationTagFree] [nvarchar](max) NULL,
	[NoteTagFree] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
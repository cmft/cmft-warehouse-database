﻿CREATE TABLE [Core].[AvailabilityDate](
	[oid] [int] NOT NULL,
	[ProviderOID] [int] NULL,
	[ScheduleOID] [int] NULL,
	[PersonOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL,
	[StopDate] [datetime] NULL,
	[Additional] [tinyint] NULL,
	[DeleteRestartField] [datetime] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
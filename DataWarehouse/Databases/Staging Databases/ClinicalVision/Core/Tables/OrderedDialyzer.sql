﻿CREATE TABLE [Core].[OrderedDialyzer](
	[oid] [int] NOT NULL,
	[BackupDialyzerOID] [int] NULL,
	[PrimaryDialyzerOID] [int] NULL,
	[SecondaryDialyzerOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Reuse] [tinyint] NULL DEFAULT ('0'),
	[ReasonNotReuse] [int] NULL
) ON [PRIMARY]
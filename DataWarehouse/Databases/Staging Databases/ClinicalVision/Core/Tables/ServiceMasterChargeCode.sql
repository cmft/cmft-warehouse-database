﻿CREATE TABLE [Core].[ServiceMasterChargeCode](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[DaysOfWeekOID] [int] NULL,
	[ChargeMasterOID] [int] NULL,
	[ServiceMasterOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[WhenToCharge] [int] NULL,
	[AllowChargeOnAdmit] [tinyint] NULL,
	[AllowChargeOnDischarge] [tinyint] NULL,
	[SupplySource] [int] NULL,
	[ChargeSuspensionDates] [tinyint] NULL,
	[IncludeSuspensionStart] [tinyint] NULL,
	[IncludeSuspensionStop] [tinyint] NULL
) ON [PRIMARY]
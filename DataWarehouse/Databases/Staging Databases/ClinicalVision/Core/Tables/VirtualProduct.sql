﻿CREATE TABLE [Core].[VirtualProduct](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[VirtualProductId] [int] NULL,
	[Order] [int] NULL,
	[VirtualProduct] [nvarchar](4000) NULL,
	[VirtualProductTagFree] [nvarchar](4000) NULL,
	[Title] [nvarchar](255) NULL,
	[Subtitle] [nvarchar](100) NULL
) ON [PRIMARY]
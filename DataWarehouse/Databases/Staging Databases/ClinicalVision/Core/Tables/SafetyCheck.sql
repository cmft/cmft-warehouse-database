﻿CREATE TABLE [Core].[SafetyCheck](
	[oid] [int] NOT NULL,
	[DialyzerCheck1OID] [int] NULL,
	[DialyzerCheck2OID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SterilantNegSaline] [int] NULL,
	[SterilantNegativeSetup] [int] NULL,
	[DialyzerCheckedPatient] [int] NULL
) ON [PRIMARY]
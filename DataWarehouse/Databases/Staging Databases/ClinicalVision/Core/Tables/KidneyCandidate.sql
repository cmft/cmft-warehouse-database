﻿CREATE TABLE [Core].[KidneyCandidate](
	[oid] [int] NOT NULL,
	[GeneralFactorsOID] [int] NULL,
	[KidneyFactorsOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
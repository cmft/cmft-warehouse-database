﻿CREATE TABLE [Core].[IdentificationNumber](
	[oid] [int] NOT NULL,
	[IdentifiersOID] [int] NULL,
	[PatientDateRangeOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IDNumberType] [int] NULL,
	[IDNumber] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
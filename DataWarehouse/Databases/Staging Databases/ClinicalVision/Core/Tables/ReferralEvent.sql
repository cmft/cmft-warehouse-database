﻿CREATE TABLE [Core].[ReferralEvent](
	[oid] [int] NOT NULL,
	[CareTeamOID] [int] NULL,
	[LocationOID] [int] NULL,
	[MedicalStaffOfRecordOID] [int] NULL,
	[ProviderOfRecordOID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[MedicalPracticeOfRecord](
	[oid] [int] NOT NULL,
	[MedicalPracticeOID] [int] NULL,
	[OtherProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
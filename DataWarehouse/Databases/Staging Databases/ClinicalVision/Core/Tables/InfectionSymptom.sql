﻿CREATE TABLE [Core].[InfectionSymptom](
	[oid] [int] NOT NULL,
	[rev_InfectionSymptomsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[InfectionSymptom] [int] NULL
) ON [PRIMARY]
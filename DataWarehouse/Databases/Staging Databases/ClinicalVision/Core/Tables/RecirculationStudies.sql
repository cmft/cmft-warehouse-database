﻿CREATE TABLE [Core].[RecirculationStudies](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RecirculationRatioNum] [float] NULL,
	[BloodUreaNitrogenArterialNum] [float] NULL,
	[BloodUreaNitrogenPeripheralNum] [float] NULL,
	[BloodUreaNitrogenVenousNum] [float] NULL,
	[Show] [tinyint] NULL,
	[Notes] [tinyint] NULL DEFAULT ('0'),
	[RecirculationRatioStr] [nvarchar](50) NULL,
	[RecirculationRatioFlg] [nvarchar](20) NULL,
	[BloodUreaNitrogenArterialStr] [nvarchar](50) NULL,
	[BloodUreaNitrogenArterialFlg] [nvarchar](20) NULL,
	[BloodUreaNitrogenPeripheralStr] [nvarchar](50) NULL,
	[BloodUreaNitrogenPeripheralFlg] [nvarchar](20) NULL,
	[BloodUreaNitrogenVenousStr] [nvarchar](50) NULL,
	[BloodUreaNitrogenVenousFlg] [nvarchar](20) NULL
) ON [PRIMARY]
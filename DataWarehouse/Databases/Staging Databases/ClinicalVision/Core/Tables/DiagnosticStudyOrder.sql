﻿CREATE TABLE [Core].[DiagnosticStudyOrder](
	[oid] [int] NOT NULL,
	[DiagnosticTestOID] [int] NULL,
	[OrdersExportFlag] [tinyint] NULL DEFAULT ('1')
) ON [PRIMARY]
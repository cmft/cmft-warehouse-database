﻿CREATE TABLE [Core].[ReportGroupPatient](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[ReportGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[DialysisProvider](
	[oid] [int] NOT NULL,
	[InfectionOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
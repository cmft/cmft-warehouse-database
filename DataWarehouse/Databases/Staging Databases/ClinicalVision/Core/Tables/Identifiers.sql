﻿CREATE TABLE [Core].[Identifiers](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IDNumberType] [int] NULL,
	[Unique] [tinyint] NULL DEFAULT ('1'),
	[GenerateNext] [tinyint] NULL DEFAULT ('1'),
	[LastUsed] [int] NULL,
	[Length] [int] NULL,
	[ZeroFill] [tinyint] NULL DEFAULT ('0'),
	[Prefex] [nvarchar](50) NULL,
	[Append] [nvarchar](50) NULL
) ON [PRIMARY]
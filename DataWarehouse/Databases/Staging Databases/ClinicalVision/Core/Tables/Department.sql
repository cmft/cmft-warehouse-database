﻿CREATE TABLE [Core].[Department](
	[oid] [int] NOT NULL,
	[ProviderIDOID] [int] NULL,
	[ProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicalSpeciality] [int] NULL,
	[Name] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Pattern](
	[oid] [int] NOT NULL,
	[Option1OID] [int] NULL,
	[Option2OID] [int] NULL,
	[Option3OID] [int] NULL,
	[Option4OID] [int] NULL,
	[Option5OID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[UseOption1] [tinyint] NULL DEFAULT ('1'),
	[UseOption2] [tinyint] NULL DEFAULT ('0'),
	[UseOption3] [tinyint] NULL DEFAULT ('0'),
	[UseOption4] [tinyint] NULL DEFAULT ('0'),
	[UseOption5] [tinyint] NULL DEFAULT ('0'),
	[StringValue] [nvarchar](255) NULL,
	[PersistedInstanceName] [nvarchar](100) NULL
) ON [PRIMARY]
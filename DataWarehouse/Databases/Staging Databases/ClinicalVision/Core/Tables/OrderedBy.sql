﻿CREATE TABLE [Core].[OrderedBy](
	[oid] [int] NOT NULL,
	[SignatureOnFile] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
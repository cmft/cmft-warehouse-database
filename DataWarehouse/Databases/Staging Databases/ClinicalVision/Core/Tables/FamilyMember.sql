﻿CREATE TABLE [Core].[FamilyMember](
	[oid] [int] NOT NULL,
	[rev_FamilyHistoryFamilyMembersOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Relation] [int] NULL,
	[FamilyGeneralHealth] [int] NULL,
	[FamilyCauseOfDeath] [int] NULL,
	[Age] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
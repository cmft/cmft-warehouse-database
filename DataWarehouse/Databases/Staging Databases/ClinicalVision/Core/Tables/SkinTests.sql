﻿CREATE TABLE [Core].[SkinTests](
	[oid] [int] NOT NULL,
	[SkinTest] [int] NULL,
	[SkinTestReason] [int] NULL,
	[Reaction] [int] NULL
) ON [PRIMARY]
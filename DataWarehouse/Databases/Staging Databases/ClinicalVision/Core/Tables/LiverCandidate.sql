﻿CREATE TABLE [Core].[LiverCandidate](
	[oid] [int] NOT NULL,
	[GeneralFactorsOID] [int] NULL,
	[LiverAndIntestineFactorsOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
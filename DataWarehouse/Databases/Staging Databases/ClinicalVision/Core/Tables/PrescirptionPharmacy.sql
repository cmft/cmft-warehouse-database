﻿CREATE TABLE [Core].[PrescirptionPharmacy](
	[oid] [int] NOT NULL,
	[PrescriptionPatientPharmacyOID] [int] NULL,
	[PrescriptionPharmacyOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
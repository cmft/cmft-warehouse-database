﻿CREATE TABLE [Core].[SupportingCharge](
	[oid] [int] NOT NULL,
	[ChargeMasterOID] [int] NULL,
	[DateRangeOID] [int] NULL,
	[rev_ServiceMasterChargeCodeSupportingChargesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[ProblemDiagnosis](
	[oid] [int] NOT NULL,
	[CommonDiagnosisOID] [int] NULL,
	[IndicationOID] [int] NULL,
	[rev_BabyProblemDiagnosisesOID] [int] NULL,
	[rev_PregnancyProblemDiagnosisesOID] [int] NULL,
	[rev_ServiceMasterIndicationsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ReptDiagnosis] [nvarchar](255) NULL,
	[ReptDiagnosisCode] [nvarchar](1000) NULL,
	[DateRangeOID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Hemodialysis](
	[oid] [int] NOT NULL,
	[AnticoagulationTherapyOID] [int] NULL,
	[DialysisMachinesOID] [int] NULL,
	[DialysisStationOID] [int] NULL,
	[MachineSettingsOID] [int] NULL,
	[OrderedDialyzerOID] [int] NULL,
	[RenalMedicalDeviceOID] [int] NULL,
	[TargetBloodToProcessOID] [int] NULL,
	[TargetFluidToRemoveOID] [int] NULL,
	[TargetWeightOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[HDFrequency] [int] NULL,
	[SpecialProcedures] [nvarchar](max) NULL,
	[OtherTarget] [nvarchar](40) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[CTDefault](
	[oid] [int] NOT NULL,
	[ObservationTemplateOID] [int] NULL,
	[Template1OID] [int] NULL,
	[Template2OID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](100) NULL
) ON [PRIMARY]
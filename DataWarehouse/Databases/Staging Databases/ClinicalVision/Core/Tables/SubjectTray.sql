﻿CREATE TABLE [Core].[SubjectTray](
	[oid] [int] NOT NULL,
	[TrayGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](30) NULL
) ON [PRIMARY]
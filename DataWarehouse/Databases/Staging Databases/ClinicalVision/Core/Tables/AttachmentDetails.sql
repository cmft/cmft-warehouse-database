﻿CREATE TABLE [Core].[AttachmentDetails](
	[oid] [int] NOT NULL,
	[AttachmentContentOID] [int] NULL,
	[InternalStaffOfRecordOID] [int] NULL,
	[ObservationOID] [int] NULL,
	[PatientOID] [int] NULL,
	[LetterOID] [int] NULL,
	[LegalDocumentOID] [int] NULL,
	[LabResultOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AttachmentType] [int] NULL,
	[Date] [datetime] NULL DEFAULT (getdate()),
	[FileName] [nvarchar](255) NULL,
	[Description] [nvarchar](1000) NULL
) ON [PRIMARY]
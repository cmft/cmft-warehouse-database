﻿CREATE TABLE [Core].[TransplantInsurance](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TransplantCoverage] [int] NULL,
	[YearlyDeductible] [float] NULL,
	[DeductiblePerAdmission] [float] NULL,
	[AnnualMaximumBenefit] [float] NULL,
	[LifetimeMaximumBenefit] [float] NULL,
	[DrugCoverage] [int] NULL,
	[GenericPayment] [float] NULL,
	[BrandNamePayment] [float] NULL,
	[OutOfPocket] [nvarchar](20) NULL
) ON [PRIMARY]
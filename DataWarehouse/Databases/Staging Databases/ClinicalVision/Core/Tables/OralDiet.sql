﻿CREATE TABLE [Core].[OralDiet](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Energy] [int] NULL,
	[DietConsistency] [int] NULL,
	[Protein] [int] NULL
) ON [PRIMARY]
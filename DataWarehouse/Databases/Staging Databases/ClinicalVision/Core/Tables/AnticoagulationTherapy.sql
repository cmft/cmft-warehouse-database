﻿CREATE TABLE [Core].[AnticoagulationTherapy](
	[oid] [int] NOT NULL,
	[AnticoagulationOID] [int] NULL,
	[Bolus1OID] [int] NULL,
	[Bolus2OID] [int] NULL,
	[Bolus3OID] [int] NULL,
	[Bolus4OID] [int] NULL,
	[LoadDoseOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AnticoagulationMethod] [int] NULL,
	[AdministrationStopTime] [int] NULL,
	[InfuseRate] [nvarchar](20) NULL
) ON [PRIMARY]
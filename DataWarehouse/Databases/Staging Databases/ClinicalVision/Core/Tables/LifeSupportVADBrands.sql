﻿CREATE TABLE [Core].[LifeSupportVADBrands](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[CardioWest] [tinyint] NULL,
	[Abiomed] [tinyint] NULL,
	[Novacar] [tinyint] NULL,
	[Heartmate] [tinyint] NULL,
	[Thoratec] [tinyint] NULL,
	[OtherVAD] [tinyint] NULL,
	[OtherVADBrands] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[InfOtherCulturesObtained](
	[oid] [int] NOT NULL,
	[rev_InfectionVascularAccessOtherCulturesObtainedsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[CultureObtained] [int] NULL
) ON [PRIMARY]
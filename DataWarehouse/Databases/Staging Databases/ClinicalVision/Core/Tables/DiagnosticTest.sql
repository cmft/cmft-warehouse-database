﻿CREATE TABLE [Core].[DiagnosticTest](
	[oid] [int] NOT NULL,
	[ClinicalDataOID] [int] NULL,
	[SpecimenOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TestFrequency] [int] NULL,
	[NonESRD] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
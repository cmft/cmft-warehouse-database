﻿CREATE TABLE [Core].[BirthPlace](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StateOrProvince] [int] NULL,
	[Country] [int] NULL,
	[County] [int] NULL,
	[CityOfBirth] [nvarchar](40) NULL
) ON [PRIMARY]
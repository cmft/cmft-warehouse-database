﻿CREATE TABLE [Core].[DaysOfWeek](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Monday] [tinyint] NULL DEFAULT ('0'),
	[Tuesday] [tinyint] NULL DEFAULT ('0'),
	[Wednesday] [tinyint] NULL DEFAULT ('0'),
	[Thursday] [tinyint] NULL DEFAULT ('0'),
	[Friday] [tinyint] NULL DEFAULT ('0'),
	[Saturday] [tinyint] NULL DEFAULT ('0'),
	[Sunday] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
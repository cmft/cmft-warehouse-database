﻿CREATE TABLE [Core].[DateRangeNoDefault](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL,
	[StopDate] [datetime] NULL
) ON [PRIMARY]
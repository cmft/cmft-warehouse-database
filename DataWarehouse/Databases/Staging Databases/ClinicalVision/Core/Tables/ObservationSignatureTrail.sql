﻿CREATE TABLE [Core].[ObservationSignatureTrail](
	[oid] [int] NOT NULL,
	[CoSignatureOID] [int] NULL,
	[RequiredSignatureOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
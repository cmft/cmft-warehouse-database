﻿CREATE TABLE [Core].[DrugOrder](
	[oid] [int] NOT NULL,
	[DrugFormularyOID] [int] NULL,
	[DrugNameOID] [int] NULL,
	[MimsDrugOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
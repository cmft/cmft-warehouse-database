﻿CREATE TABLE [Core].[ProviderOfRecord](
	[oid] [int] NOT NULL,
	[OtherProviderOID] [int] NULL,
	[ProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
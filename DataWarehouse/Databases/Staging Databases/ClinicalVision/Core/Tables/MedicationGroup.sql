﻿CREATE TABLE [Core].[MedicationGroup](
	[oid] [int] NOT NULL,
	[rev_AdministrationDualSignOffSettingMedicationGroupsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicationGroup] [int] NULL
) ON [PRIMARY]
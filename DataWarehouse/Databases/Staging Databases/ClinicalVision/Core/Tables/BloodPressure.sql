﻿CREATE TABLE [Core].[BloodPressure](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Systolic] [int] NULL,
	[Diastolic] [int] NULL,
	[HeartRate] [int] NULL,
	[PhysicalOrientation] [int] NULL,
	[BloodPressureMethod] [int] NULL
) ON [PRIMARY]
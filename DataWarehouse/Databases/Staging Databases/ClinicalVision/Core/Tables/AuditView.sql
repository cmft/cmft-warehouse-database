﻿CREATE TABLE [Core].[AuditView](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AuditViewTime] [datetime] NULL,
	[User] [nvarchar](20) NULL,
	[UserName] [nvarchar](255) NULL,
	[ViewName] [nvarchar](255) NULL,
	[Location] [nvarchar](40) NULL,
	[UserType] [nvarchar](40) NULL,
	[Subject] [nvarchar](20) NULL,
	[SubjectType] [nvarchar](40) NULL,
	[SubjectName] [nvarchar](255) NULL
) ON [PRIMARY]
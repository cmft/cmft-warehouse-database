﻿CREATE TABLE [Core].[ProdDat](
	[oid] [int] NOT NULL,
	[SubDatOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ProdCode] [int] NULL,
	[Section] [int] NULL,
	[SubSection] [nvarchar](50) NULL,
	[ProductInfoXml] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
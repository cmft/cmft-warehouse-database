﻿CREATE TABLE [Core].[Complications](
	[oid] [int] NOT NULL,
	[TransplantComplication] [int] NULL,
	[TransplantIntervention] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[PersonName](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PersonSuffix] [int] NULL,
	[PersonPrefix] [int] NULL,
	[PersonDegree] [int] NULL,
	[FirstName] [nvarchar](30) NULL,
	[MiddleName] [nvarchar](30) NULL,
	[LastName] [nvarchar](30) NULL,
	[SurnamePrefix] [nvarchar](50) NULL
) ON [PRIMARY]
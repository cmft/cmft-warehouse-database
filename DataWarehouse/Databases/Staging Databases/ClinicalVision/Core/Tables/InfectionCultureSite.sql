﻿CREATE TABLE [Core].[InfectionCultureSite](
	[oid] [int] NOT NULL,
	[rev_InfectionVascularAccessOtherCultureSitesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[InfectionCultureSite] [int] NULL
) ON [PRIMARY]
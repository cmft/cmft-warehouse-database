﻿CREATE TABLE [Core].[ICD9Code](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Code] [nvarchar](20) NULL,
	[Category] [nvarchar](1000) NULL,
	[Subcategory] [nvarchar](1000) NULL,
	[Description] [nvarchar](1000) NULL,
	[Scheme] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[PatientDateRangeNoDefault](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PatientStartDate] [datetime] NULL,
	[PatientStopDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[CodeSet](
	[UUID] [nvarchar](40) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Private] [tinyint] NULL,
	[Hidden] [tinyint] NULL,
	[Annotation] [nvarchar](255) NULL,
	[LastUpdate] [datetime] NULL,
	[Customer] [tinyint] NULL,
	[EnumerationID] [nvarchar](255) NULL,
	[Ordering] [tinyint] NULL
) ON [PRIMARY]
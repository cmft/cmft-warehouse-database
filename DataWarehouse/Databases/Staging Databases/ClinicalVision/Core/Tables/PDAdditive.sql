﻿CREATE TABLE [Core].[PDAdditive](
	[oid] [int] NOT NULL,
	[rev_CAPDPDAdditivesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PDAdditive] [int] NULL,
	[Dose] [float] NULL,
	[OrderUnits] [int] NULL,
	[Instructions] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[PeritonealDialysis](
	[oid] [int] NOT NULL,
	[APDOID] [int] NULL,
	[CAPDOID] [int] NULL,
	[DaysOfWeekOID] [int] NULL,
	[MedicalDeviceOID] [int] NULL,
	[NetVolumeRemovalOID] [int] NULL,
	[TargetWeightOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PDFrequency] [int] NULL,
	[PDExchangeMethod] [int] NULL,
	[ExchangeType] [int] NULL,
	[ExchangesPerDay] [int] NULL,
	[PDCalcium] [int] NULL,
	[OtherTargetOutcomes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
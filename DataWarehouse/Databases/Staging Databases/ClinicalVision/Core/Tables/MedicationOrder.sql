﻿CREATE TABLE [Core].[MedicationOrder](
	[oid] [int] NOT NULL,
	[MedicationOID] [int] NULL,
	[Immunization] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
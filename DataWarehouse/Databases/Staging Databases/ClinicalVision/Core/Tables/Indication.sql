﻿CREATE TABLE [Core].[Indication](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[UKDiagnosisCode] [nvarchar](max) NULL,
	[USDiagnosisCode] [nvarchar](max) NULL,
	[ReptDiagnosisCode] [nvarchar](1000) NULL,
	[ReptDiagnosisDescription] [nvarchar](255) NULL,
	[DiagnosisCode] [nvarchar](1000) NULL,
	[DiagnosisDescription] [nvarchar](255) NULL,
	[USIndicationSummary] [nvarchar](255) NULL,
	[UKIndicationSummary] [nvarchar](1000) NULL,
	[Scheme] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
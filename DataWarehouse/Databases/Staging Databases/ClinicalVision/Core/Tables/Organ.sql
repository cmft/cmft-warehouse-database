﻿CREATE TABLE [Core].[Organ](
	[oid] [int] NOT NULL,
	[rev_TransplantHistoryOrgansOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Organ] [int] NULL
) ON [PRIMARY]
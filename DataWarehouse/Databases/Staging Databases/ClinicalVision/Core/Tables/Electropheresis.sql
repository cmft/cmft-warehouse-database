﻿CREATE TABLE [Core].[Electropheresis](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Alpha1Num] [float] NULL,
	[PEAlbuminNum] [float] NULL,
	[TotalProteinNum] [float] NULL,
	[GammaNum] [float] NULL,
	[BetaNum] [float] NULL,
	[Alpha2Num] [float] NULL,
	[percentM2SpikeNum] [float] NULL,
	[percentA2AlphaNum] [float] NULL,
	[percentA1AlphaNum] [float] NULL,
	[percentAlbuminNum] [float] NULL,
	[percentMSpikeNum] [float] NULL,
	[percentGGammaNum] [float] NULL,
	[percentBBetaNum] [float] NULL,
	[Show] [tinyint] NULL,
	[Notes] [tinyint] NULL DEFAULT ('0'),
	[Alpha1Str] [nvarchar](50) NULL,
	[Alpha1Flg] [nvarchar](20) NULL,
	[PEAlbuminStr] [nvarchar](50) NULL,
	[PEAlbuminFlg] [nvarchar](20) NULL,
	[TotalProteinStr] [nvarchar](50) NULL,
	[TotalProteinFlg] [nvarchar](20) NULL,
	[GammaStr] [nvarchar](50) NULL,
	[GammaFlg] [nvarchar](20) NULL,
	[BetaStr] [nvarchar](50) NULL,
	[BetaFlg] [nvarchar](20) NULL,
	[Alpha2Str] [nvarchar](50) NULL,
	[Alpha2Flg] [nvarchar](20) NULL,
	[percentM2SpikeStr] [nvarchar](50) NULL,
	[percentM2SpikeFlg] [nvarchar](20) NULL,
	[percentA2AlphaStr] [nvarchar](50) NULL,
	[percentA2AlphaFlg] [nvarchar](20) NULL,
	[percentA1AlphaStr] [nvarchar](50) NULL,
	[percentA1AlphaFlg] [nvarchar](20) NULL,
	[percentAlbuminStr] [nvarchar](50) NULL,
	[percentAlbuminFlg] [nvarchar](20) NULL,
	[percentMSpikeStr] [nvarchar](50) NULL,
	[percentMSpikeFlg] [nvarchar](20) NULL,
	[percentGGammaStr] [nvarchar](50) NULL,
	[percentGGammaFlg] [nvarchar](20) NULL,
	[percentBBetaStr] [nvarchar](50) NULL,
	[percentBBetaFlg] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[APD](
	[oid] [int] NOT NULL,
	[DrainTimeOID] [int] NULL,
	[DwellTimeOID] [int] NULL,
	[InflowVolumeOID] [int] NULL,
	[LastFillVolumeOID] [int] NULL,
	[OutflowVolumeOID] [int] NULL,
	[SolutionTemperatureOID] [int] NULL,
	[TotalTherapyTimeOID] [int] NULL,
	[TotalTherapyVolumeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[LastFillSolution] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[ChargeMaster](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[ProcedureCodeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[BillingInterface] [int] NULL,
	[TimeStamp] [datetime] NULL,
	[ChargeCode] [nvarchar](50) NULL,
	[Description] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
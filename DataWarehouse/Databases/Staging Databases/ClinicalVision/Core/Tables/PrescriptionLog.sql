﻿CREATE TABLE [Core].[PrescriptionLog](
	[oid] [int] NOT NULL,
	[CalledInByOID] [int] NULL,
	[PrescirptionPharmacyOID] [int] NULL,
	[MedicationOID] [int] NULL,
	[MimsIndicationOID] [int] NULL,
	[PatientOID] [int] NULL,
	[MedicationOrderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PrescriptionDate] [datetime] NULL DEFAULT (getdate()),
	[NoOfRefills] [int] NULL,
	[AllowGeneric] [int] NULL,
	[DispenseAsWritten] [int] NULL,
	[PbsStatus] [int] NULL,
	[NoDays] [int] NULL,
	[ScriptNo] [int] NULL,
	[NoOfPacks] [int] NULL,
	[Supply] [nvarchar](20) NULL,
	[Notes] [nvarchar](max) NULL,
	[AuthorityCode] [nvarchar](50) NULL,
	[AuthorityPrescriptionNo] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
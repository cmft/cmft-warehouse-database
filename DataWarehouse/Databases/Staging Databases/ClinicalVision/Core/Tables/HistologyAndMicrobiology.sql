﻿CREATE TABLE [Core].[HistologyAndMicrobiology](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[HistologyNum] [float] NULL,
	[Show] [tinyint] NULL,
	[Notes] [tinyint] NULL DEFAULT ('0'),
	[HistologyStr] [nvarchar](50) NULL,
	[HistologyFlg] [nvarchar](20) NULL
) ON [PRIMARY]
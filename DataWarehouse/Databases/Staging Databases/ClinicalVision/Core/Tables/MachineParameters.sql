﻿CREATE TABLE [Core].[MachineParameters](
	[oid] [int] NOT NULL,
	[rev_MachineProgramMachineParametersOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Number] [int] NULL,
	[MachineParameter] [int] NULL,
	[AdministrationTime] [int] NULL
) ON [PRIMARY]
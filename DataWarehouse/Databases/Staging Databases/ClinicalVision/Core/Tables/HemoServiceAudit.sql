﻿CREATE TABLE [Core].[HemoServiceAudit](
	[oid] [int] NOT NULL,
	[InternalOID] [int] NULL,
	[rev_HemodialysisServiceHemoServiceAuditsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DateTime] [datetime] NULL DEFAULT (getdate()),
	[TimingQualifier] [int] NULL,
	[ServiceStatus] [int] NULL
) ON [PRIMARY]
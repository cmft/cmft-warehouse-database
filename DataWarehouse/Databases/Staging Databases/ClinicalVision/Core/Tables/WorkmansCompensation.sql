﻿CREATE TABLE [Core].[WorkmansCompensation](
	[oid] [int] NOT NULL,
	[PatientInsuranceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ClaimDate] [datetime] NULL,
	[LastDateWorked] [datetime] NULL,
	[DateReturnedToFullDuty] [datetime] NULL,
	[DateReturnedToPartialDuty] [datetime] NULL,
	[ClaimNumber] [nvarchar](20) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
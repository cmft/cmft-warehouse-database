﻿CREATE TABLE [Core].[Image](
	[oid] [int] NOT NULL,
	[rev_ObservationImagesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ReferenceName] [nvarchar](40) NULL,
	[Image] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[LabResult](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[LabTestOID] [int] NULL,
	[rev_DiagnosticTestLabResultsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Sequence] [int] NULL,
	[LabResultName] [int] NULL,
	[ResultNumeric] [float] NULL,
	[Organism] [int] NULL,
	[ResultCode] [int] NULL,
	[ResultType] [int] NULL,
	[ResultStatus] [int] NULL,
	[ResultDate] [datetime] NULL,
	[hasNotes] [tinyint] NULL DEFAULT ('0'),
	[isLegacyResult] [tinyint] NULL DEFAULT ('0'),
	[isCopyingFlag] [tinyint] NULL DEFAULT ('0'),
	[MiscellaneousName] [nvarchar](40) NULL,
	[ResultString] [nvarchar](50) NULL,
	[ResultNormalRange] [nvarchar](max) NULL,
	[ResultFlag] [nvarchar](20) NULL,
	[ResultComment] [nvarchar](max) NULL,
	[ResultUnits] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[DonorStatus](
	[oid] [int] NOT NULL,
	[CadavericDonorCandidateOID] [int] NULL,
	[LivingDonorCandidateOID] [int] NULL,
	[TransplantCentersOID] [int] NULL,
	[TransplantOrgan] [int] NULL,
	[DonorType] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[ChargeUnAuthInsurance](
	[oid] [int] NOT NULL,
	[DateRangeNoDefaultOID] [int] NULL,
	[ChargeMasterOID] [int] NULL,
	[InsurancePlanOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
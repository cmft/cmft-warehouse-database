﻿CREATE TABLE [Core].[ViewFavoriteEntry](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ViewCompoundTypeID] [nvarchar](100) NULL,
	[ViewName] [nvarchar](255) NULL
) ON [PRIMARY]
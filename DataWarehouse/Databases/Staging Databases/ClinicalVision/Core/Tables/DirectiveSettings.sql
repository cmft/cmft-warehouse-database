﻿CREATE TABLE [Core].[DirectiveSettings](
	[oid] [int] NOT NULL,
	[rev_IDLineSettingsDirectiveSettingsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Directive] [int] NULL,
	[ShowOnIDLine] [tinyint] NULL DEFAULT ('0'),
	[ShortName] [nvarchar](30) NULL
) ON [PRIMARY]
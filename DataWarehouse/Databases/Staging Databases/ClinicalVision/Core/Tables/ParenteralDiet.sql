﻿CREATE TABLE [Core].[ParenteralDiet](
	[oid] [int] NOT NULL,
	[AminoAcidsVolumeOID] [int] NULL,
	[DextroseVolumeOID] [int] NULL,
	[LipidsVolumeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ParenteralRoute] [int] NULL,
	[DextrosePercent] [float] NULL,
	[AminoAcidsPercent] [float] NULL,
	[LipidsPercent] [float] NULL,
	[DextroseAARate] [nvarchar](40) NULL,
	[LipidsAdminRate] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[AddressGroup](
	[oid] [int] NOT NULL,
	[MailingAddressOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
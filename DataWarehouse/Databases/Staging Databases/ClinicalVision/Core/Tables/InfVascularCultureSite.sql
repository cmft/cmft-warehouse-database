﻿CREATE TABLE [Core].[InfVascularCultureSite](
	[oid] [int] NOT NULL,
	[rev_InfectionVascularAccessVascularCultureSitesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[VascularAccessLocation] [int] NULL
) ON [PRIMARY]
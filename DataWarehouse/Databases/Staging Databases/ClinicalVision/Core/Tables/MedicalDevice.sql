﻿CREATE TABLE [Core].[MedicalDevice](
	[oid] [int] NOT NULL,
	[ActualEquipmentOID] [int] NULL,
	[VendorOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[EquipmentType] [int] NULL,
	[OtherDescription] [nvarchar](30) NULL
) ON [PRIMARY]
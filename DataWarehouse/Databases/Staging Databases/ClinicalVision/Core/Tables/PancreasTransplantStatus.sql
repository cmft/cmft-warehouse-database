﻿CREATE TABLE [Core].[PancreasTransplantStatus](
	[oid] [int] NOT NULL,
	[PancPrimaryDiagnosis] [int] NULL,
	[PancPrimaryDiagnosisOth] [nvarchar](40) NULL
) ON [PRIMARY]
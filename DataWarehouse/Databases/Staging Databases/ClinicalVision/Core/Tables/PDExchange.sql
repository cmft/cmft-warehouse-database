﻿CREATE TABLE [Core].[PDExchange](
	[oid] [int] NOT NULL,
	[VolumeOID] [int] NULL,
	[rev_APDPDExchangesOID] [int] NULL,
	[rev_CAPDPDExchangesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ExchangeNo] [int] NULL,
	[PDSolution] [int] NULL,
	[Quantity] [int] NULL,
	[TimeOfDay] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[ContactPerson](
	[oid] [int] NOT NULL,
	[AddressGroupOID] [int] NULL,
	[NameOID] [int] NULL,
	[PhoneNumberGroupOID] [int] NULL,
	[StaffOID] [int] NULL,
	[rev_ProviderContactPersonsOID] [int] NULL,
	[rev_VendorContactPersonsOID] [int] NULL,
	[rev_FinancialCoverageMasterContactPersonsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Comment] [nvarchar](40) NULL,
	[EMailAddress] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
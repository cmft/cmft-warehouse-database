﻿CREATE TABLE [Core].[InfectionSymptomOther](
	[oid] [int] NOT NULL,
	[rev_InfectionVascularAccessInfectionSymptomOthersOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[OtherSiteInfections] [int] NULL
) ON [PRIMARY]
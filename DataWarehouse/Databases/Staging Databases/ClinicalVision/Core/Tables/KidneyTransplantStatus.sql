﻿CREATE TABLE [Core].[KidneyTransplantStatus](
	[oid] [int] NOT NULL,
	[KidneyCandidateOID] [int] NULL,
	[KidPrimaryDiagnosis] [int] NULL,
	[ReasonNotCandidate] [int] NULL,
	[RegistryCode] [int] NULL,
	[KidPrimaryDiagnosisOther] [nvarchar](40) NULL
) ON [PRIMARY]
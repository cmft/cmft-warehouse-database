﻿CREATE TABLE [Core].[InfectionOrganism](
	[oid] [int] NOT NULL,
	[rev_InfectionOrganismsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Organism] [int] NULL
) ON [PRIMARY]
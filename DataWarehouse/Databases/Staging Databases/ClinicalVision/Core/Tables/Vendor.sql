﻿CREATE TABLE [Core].[Vendor](
	[oid] [int] NOT NULL,
	[AddressGroupOID] [int] NULL,
	[PhoneNumberGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IsActive] [tinyint] NULL DEFAULT ('1'),
	[importID] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[Name] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL,
	[ProductLines] [nvarchar](40) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
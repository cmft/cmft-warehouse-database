﻿CREATE TABLE [Core].[LiverFactors](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Encephalopathy] [int] NULL,
	[VaricealBleeding] [int] NULL,
	[Ascites] [int] NULL,
	[PrevUpperAbdSurgery] [int] NULL,
	[SponBacterialPeritonitis] [int] NULL,
	[MarkedMuscleWasting] [int] NULL,
	[PortalVeinThrombosis] [int] NULL,
	[HistoryTIPSS] [int] NULL
) ON [PRIMARY]
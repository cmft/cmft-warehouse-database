﻿CREATE TABLE [Core].[DocumentCentreDefaults](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MaximumDocumentSizeKB] [int] NULL
) ON [PRIMARY]
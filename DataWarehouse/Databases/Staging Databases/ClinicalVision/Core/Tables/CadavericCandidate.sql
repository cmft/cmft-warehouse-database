﻿CREATE TABLE [Core].[CadavericCandidate](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[BasedSoleyOnDoc] [int] NULL,
	[CauseOfDeath] [int] NULL,
	[CircumstancesOfDeath] [int] NULL,
	[Documentation] [int] NULL,
	[DeathReportedToME] [int] NULL,
	[FormalRequestMade] [int] NULL,
	[ConsentKnownToFamily] [int] NULL,
	[MechanismOfDeath] [int] NULL,
	[ReasonNotSuitable] [int] NULL,
	[SuitableForProcurement] [int] NULL,
	[WrittenConsentObtained] [int] NULL,
	[CauseOfDeathOther] [nvarchar](40) NULL,
	[DocumentationOther] [nvarchar](40) NULL,
	[ReasonNotSuitableOther] [nvarchar](40) NULL,
	[WrittenConsentObtainedOth] [nvarchar](40) NULL
) ON [PRIMARY]
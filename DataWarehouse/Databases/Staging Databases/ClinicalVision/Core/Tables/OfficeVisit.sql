﻿CREATE TABLE [Core].[OfficeVisit](
	[oid] [int] NOT NULL,
	[InternalStaffOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[OfficeVisitFrequency] [int] NULL,
	[ClinicVisitType] [int] NULL,
	[Clinic] [int] NULL,
	[CareTeamConference] [int] NULL
) ON [PRIMARY]
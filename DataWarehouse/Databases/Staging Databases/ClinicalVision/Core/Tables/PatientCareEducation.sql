﻿CREATE TABLE [Core].[PatientCareEducation](
	[oid] [int] NOT NULL,
	[CarePlanEducationObjectiveOID] [int] NULL,
	[DateRangeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ObjectiveMet] [tinyint] NULL,
	[DateMet] [datetime] NULL,
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
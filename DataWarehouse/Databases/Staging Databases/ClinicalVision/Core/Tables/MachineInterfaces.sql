﻿CREATE TABLE [Core].[MachineInterfaces](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Enabled] [tinyint] NULL,
	[PatientIdentifier] [tinyint] NULL,
	[StationID] [tinyint] NULL,
	[MachineID] [tinyint] NULL,
	[SessionAttributesSent] [tinyint] NULL,
	[RetainDays] [int] NULL,
	[Name] [nvarchar](100) NULL
) ON [PRIMARY]
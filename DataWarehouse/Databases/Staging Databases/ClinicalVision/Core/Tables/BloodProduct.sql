﻿CREATE TABLE [Core].[BloodProduct](
	[oid] [int] NOT NULL,
	[AdministrationOID] [int] NULL,
	[VolumeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[BloodProductFrequency] [int] NULL,
	[Units] [int] NULL,
	[BloodGroup] [int] NULL,
	[BloodRhesus] [int] NULL,
	[PatientBandRequired] [int] NULL
) ON [PRIMARY]
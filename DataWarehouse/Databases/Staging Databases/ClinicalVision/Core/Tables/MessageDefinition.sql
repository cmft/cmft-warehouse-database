﻿CREATE TABLE [Core].[MessageDefinition](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SendEmail] [tinyint] NULL,
	[TriggeringAction] [int] NULL,
	[NotificationMessageType] [int] NULL,
	[Disabled] [tinyint] NULL,
	[MessageType] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[Criteria] [nvarchar](max) NULL,
	[AffectedTypeID] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
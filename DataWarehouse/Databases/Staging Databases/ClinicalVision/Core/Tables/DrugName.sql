﻿CREATE TABLE [Core].[DrugName](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Medication] [nvarchar](100) NULL,
	[USDrugName] [nvarchar](max) NULL,
	[UKDrugName] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
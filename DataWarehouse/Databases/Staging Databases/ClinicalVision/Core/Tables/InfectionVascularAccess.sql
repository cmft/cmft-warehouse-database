﻿CREATE TABLE [Core].[InfectionVascularAccess](
	[oid] [int] NOT NULL,
	[AccessUsedOID] [int] NULL,
	[VasAccessInfectionType] [int] NULL,
	[OtherSiteInfection] [tinyint] NULL DEFAULT ('0'),
	[Culture] [int] NULL,
	[OtherCulture] [int] NULL,
	[isNewAccessEvent] [tinyint] NULL DEFAULT ('0'),
	[OtherSiteSymptoms] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
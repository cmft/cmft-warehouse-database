﻿CREATE TABLE [Core].[ClinicalData](
	[oid] [int] NOT NULL,
	[HeightOID] [int] NULL,
	[WeightOID] [int] NULL,
	[OtherValue] [nvarchar](20) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
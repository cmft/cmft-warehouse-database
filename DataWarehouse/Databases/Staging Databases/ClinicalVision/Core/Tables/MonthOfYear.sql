﻿CREATE TABLE [Core].[MonthOfYear](
	[oid] [int] NOT NULL,
	[rev_PatternMonthOfYearsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MonthOfYear] [int] NULL
) ON [PRIMARY]
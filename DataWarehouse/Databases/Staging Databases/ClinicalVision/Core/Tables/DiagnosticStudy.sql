﻿CREATE TABLE [Core].[DiagnosticStudy](
	[oid] [int] NOT NULL,
	[ProcedureCodeOID] [int] NULL,
	[ResultStatus] [int] NULL,
	[DepartmentCodes] [int] NULL,
	[ReportNumber] [nvarchar](20) NULL,
	[ProviderOfRecordOID] [int] NULL,
	[OtherProcedure] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[InsurancePlan](
	[oid] [int] NOT NULL,
	[CompanyName] [int] NULL,
	[PlanType] [int] NULL,
	[TransplantCoverage] [int] NULL,
	[YearlyDeductible] [float] NULL,
	[DeductiblePerAdmission] [float] NULL,
	[AnnualMaximumBenefit] [float] NULL,
	[LifetimeMaximumBenefit] [float] NULL,
	[DrugCoverage] [int] NULL,
	[GenericPayment] [float] NULL,
	[BrandNamePayment] [float] NULL,
	[HospitalRestrictions] [nvarchar](max) NULL,
	[LabortoryRestrictions] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
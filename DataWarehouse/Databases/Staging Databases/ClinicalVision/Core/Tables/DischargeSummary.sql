﻿CREATE TABLE [Core].[DischargeSummary](
	[oid] [int] NOT NULL,
	[DischargedByOID] [int] NULL,
	[GeneratedDocumentOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DischargeMethod] [int] NULL,
	[DischargeDestination] [int] NULL,
	[DischargeNotes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
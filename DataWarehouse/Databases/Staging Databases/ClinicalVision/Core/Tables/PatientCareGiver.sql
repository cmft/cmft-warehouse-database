﻿CREATE TABLE [Core].[PatientCareGiver](
	[oid] [int] NOT NULL,
	[InternalOID] [int] NULL,
	[PatientOID] [int] NULL,
	[CareTeamOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL DEFAULT (getdate()),
	[StopDate] [datetime] NULL,
	[CaregiverRole] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[LungCandidate](
	[oid] [int] NOT NULL,
	[GeneralFactorsOID] [int] NULL,
	[HeartLungFactorsOID] [int] NULL,
	[HemodynamicOID] [int] NULL,
	[LungFactorsOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
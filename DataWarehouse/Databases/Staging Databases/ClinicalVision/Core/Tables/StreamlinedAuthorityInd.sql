﻿CREATE TABLE [Core].[StreamlinedAuthorityInd](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IndicationId] [nvarchar](50) NULL,
	[IndicationText] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
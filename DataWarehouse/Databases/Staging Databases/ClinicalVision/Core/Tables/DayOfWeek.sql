﻿CREATE TABLE [Core].[DayOfWeek](
	[oid] [int] NOT NULL,
	[rev_PatternDayOfWeeksOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DayOfWeek] [int] NULL
) ON [PRIMARY]
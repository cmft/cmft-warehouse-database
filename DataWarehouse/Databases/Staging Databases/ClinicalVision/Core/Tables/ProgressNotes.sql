﻿CREATE TABLE [Core].[ProgressNotes](
	[oid] [int] NOT NULL,
	[DepartmentOID] [int] NULL,
	[PreviousNoteOID] [int] NULL,
	[SupersededByOID] [int] NULL,
	[ProgressNoteType] [int] NULL,
	[Multidiscipline] [int] NULL,
	[SupersededDate] [datetime] NULL,
	[ReasonSuperseded] [int] NULL,
	[IsSuperseded] [tinyint] NULL DEFAULT ('0'),
	[DateLastChanged] [datetime] NULL,
	[Version] [int] NULL
) ON [PRIMARY]
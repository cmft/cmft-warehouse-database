﻿CREATE TABLE [Core].[PatientCareAssessment](
	[oid] [int] NOT NULL,
	[CarePlanAssessmentOID] [int] NULL,
	[DateRangeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DateMet] [datetime] NULL,
	[AssessmentMet] [tinyint] NULL,
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
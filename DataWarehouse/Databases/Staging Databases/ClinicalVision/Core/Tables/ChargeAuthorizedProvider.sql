﻿CREATE TABLE [Core].[ChargeAuthorizedProvider](
	[oid] [int] NOT NULL,
	[DateRangeNoDefaultOID] [int] NULL,
	[ChargeMasterOID] [int] NULL,
	[ProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
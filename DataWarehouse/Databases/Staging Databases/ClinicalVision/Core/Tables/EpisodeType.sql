﻿CREATE TABLE [Core].[EpisodeType](
	[oid] [int] NOT NULL,
	[rev_ServiceMasterEpisodeTypesOID] [int] NULL,
	[rev_ServiceMasterChargeCodeNotBillableEpisodeTypesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[EpisodeType] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Language](
	[oid] [int] NOT NULL,
	[rev_PersonLanguagesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Language] [int] NULL
) ON [PRIMARY]
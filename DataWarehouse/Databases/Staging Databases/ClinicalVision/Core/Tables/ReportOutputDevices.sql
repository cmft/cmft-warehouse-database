﻿CREATE TABLE [Core].[ReportOutputDevices](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Screen] [tinyint] NULL,
	[Printer] [tinyint] NULL,
	[RTF] [tinyint] NULL,
	[PDF] [tinyint] NULL,
	[MSExcel] [tinyint] NULL,
	[RBArchive] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[PatientInsurance](
	[oid] [int] NOT NULL,
	[GuarantorOID] [int] NULL,
	[InsurancePlanOID] [int] NULL,
	[PatientDateRangeOID] [int] NULL,
	[TransplantInsuranceOID] [int] NULL,
	[InsuranceCoverageGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[EmployerPlan] [int] NULL,
	[InsurancePlanStatus] [int] NULL,
	[PolicyNo] [nvarchar](20) NULL,
	[GroupNo] [nvarchar](20) NULL,
	[GroupName] [nvarchar](255) NULL
) ON [PRIMARY]
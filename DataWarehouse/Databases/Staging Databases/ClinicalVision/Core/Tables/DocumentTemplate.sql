﻿CREATE TABLE [Core].[DocumentTemplate](
	[oid] [int] NOT NULL,
	[ReportDetailsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TemplateType] [int] NULL,
	[Disabled] [tinyint] NULL DEFAULT ('0'),
	[EditCount] [int] NULL,
	[WordTemplate] [tinyint] NULL DEFAULT ('0'),
	[TemplateName] [nvarchar](100) NULL,
	[TemplateFile] [nvarchar](max) NULL,
	[Description] [nvarchar](1000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
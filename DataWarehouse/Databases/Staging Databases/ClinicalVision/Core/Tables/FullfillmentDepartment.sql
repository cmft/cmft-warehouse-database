﻿CREATE TABLE [Core].[FullfillmentDepartment](
	[oid] [int] NOT NULL,
	[ProviderOID] [int] NULL,
	[ProviderDepartmentOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
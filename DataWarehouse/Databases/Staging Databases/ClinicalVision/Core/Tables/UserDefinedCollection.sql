﻿CREATE TABLE [Core].[UserDefinedCollection](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
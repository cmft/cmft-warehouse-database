﻿CREATE TABLE [Core].[PatientTransportation](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[TransportationOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TimeStamp] [datetime] NULL,
	[PersonalTransportation] [nvarchar](100) NULL
) ON [PRIMARY]
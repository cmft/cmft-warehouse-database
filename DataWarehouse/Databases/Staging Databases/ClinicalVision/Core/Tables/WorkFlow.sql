﻿CREATE TABLE [Core].[WorkFlow](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Sequence] [int] NULL,
	[Name] [nvarchar](40) NULL
) ON [PRIMARY]
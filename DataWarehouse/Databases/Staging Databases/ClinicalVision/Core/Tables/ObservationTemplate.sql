﻿CREATE TABLE [Core].[ObservationTemplate](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TimingQualifier] [int] NULL,
	[ObservationType] [int] NULL,
	[ProductSuite] [int] NULL,
	[ProductModule] [int] NULL,
	[ShareTemplate] [tinyint] NULL DEFAULT ('0'),
	[Name] [nvarchar](40) NULL,
	[Comment] [nvarchar](40) NULL
) ON [PRIMARY]
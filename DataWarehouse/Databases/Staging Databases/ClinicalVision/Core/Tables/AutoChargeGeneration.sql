﻿CREATE TABLE [Core].[AutoChargeGeneration](
	[oid] [int] NOT NULL,
	[AbsenceCodesOID] [int] NULL,
	[DateRangeMandatoryOID] [int] NULL,
	[InternalOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DateGenerated] [datetime] NULL
) ON [PRIMARY]
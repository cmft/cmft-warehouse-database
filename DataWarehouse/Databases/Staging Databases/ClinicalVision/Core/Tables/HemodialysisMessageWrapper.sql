﻿CREATE TABLE [Core].[HemodialysisMessageWrapper](
	[oid] [int] NOT NULL,
	[rev_HemodialysisSessionHemodialysisMessageWrapperOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IsProcessed] [tinyint] NULL,
	[Message] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
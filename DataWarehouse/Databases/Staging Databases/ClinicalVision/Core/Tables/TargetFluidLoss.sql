﻿CREATE TABLE [Core].[TargetFluidLoss](
	[oid] [int] NOT NULL,
	[InternalStaffOfRecordOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[WeightOfPatient] [float] NULL,
	[IdealBodyWeight] [float] NULL,
	[IndirectSalineOnOff] [float] NULL,
	[FluidIntake] [float] NULL,
	[SalineFlushesDrugsBlood] [float] NULL
) ON [PRIMARY]
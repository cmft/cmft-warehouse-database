﻿CREATE TABLE [Core].[Releases](
	[oid] [int] NOT NULL,
	[ReleasedByOID] [int] NULL,
	[ReleaseType] [int] NULL,
	[ReleasedTo] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Location](
	[oid] [int] NOT NULL,
	[ProviderOID] [int] NULL,
	[ResourceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Ward] [int] NULL,
	[importID] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[Name] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
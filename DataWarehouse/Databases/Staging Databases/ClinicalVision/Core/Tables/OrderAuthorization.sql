﻿CREATE TABLE [Core].[OrderAuthorization](
	[oid] [int] NOT NULL,
	[InternalOID] [int] NULL,
	[MedicalStaffOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ExternalOrder] [tinyint] NULL,
	[InformationSource] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
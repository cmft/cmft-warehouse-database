﻿CREATE TABLE [Core].[PatientDateRange](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PatientStartDate] [datetime] NULL DEFAULT (getdate()),
	[PatientStopDate] [datetime] NULL
) ON [PRIMARY]
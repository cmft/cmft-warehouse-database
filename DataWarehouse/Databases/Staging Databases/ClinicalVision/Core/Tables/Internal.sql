﻿CREATE TABLE [Core].[Internal](
	[oid] [int] NOT NULL,
	[TrayGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[isRenalStaff] [tinyint] NULL,
	[isTransplantStaff] [tinyint] NULL,
	[ShowNavBar] [tinyint] NULL DEFAULT ('1'),
	[defMenu] [nvarchar](40) NULL,
	[defNavigation] [nvarchar](40) NULL,
	[defToolbar] [nvarchar](40) NULL
) ON [PRIMARY]
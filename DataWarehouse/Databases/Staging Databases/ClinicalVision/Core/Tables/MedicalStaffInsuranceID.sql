﻿CREATE TABLE [Core].[MedicalStaffInsuranceID](
	[oid] [int] NOT NULL,
	[DateRangeNoDefaultOID] [int] NULL,
	[InsurancePlanOID] [int] NULL,
	[MedicalStaffOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ProviderNo] [nvarchar](30) NULL
) ON [PRIMARY]
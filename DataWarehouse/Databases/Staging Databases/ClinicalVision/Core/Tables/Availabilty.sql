﻿CREATE TABLE [Core].[Availabilty](
	[oid] [int] NOT NULL,
	[PatternOID] [int] NULL,
	[ProviderOID] [int] NULL,
	[ResourceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL,
	[StopDate] [datetime] NULL,
	[BeginTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[LastDatePersisted] [datetime] NULL,
	[cvInternalProviderChanged] [tinyint] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
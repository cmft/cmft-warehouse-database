﻿CREATE TABLE [Core].[PatientCarePlanNotes](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[InternalStaffOfRecordOID] [int] NULL,
	[rev_PatientCarePlanInterventionInterventionNotesOID] [int] NULL,
	[rev_PatientCarePlanGoalGoalNotesOID] [int] NULL,
	[rev_PatientCarePlanEducationObjectiveEducationObjectiveNotesOID] [int] NULL,
	[rev_PatientCarePlanAssessmentAssessmentNotesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
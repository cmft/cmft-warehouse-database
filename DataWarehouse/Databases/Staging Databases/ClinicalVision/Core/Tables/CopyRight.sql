﻿CREATE TABLE [Core].[CopyRight](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Notice] [nvarchar](max) NULL,
	[Type] [nvarchar](6) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[Summary](
	[oid] [int] NOT NULL,
	[MachineDirectSummaryValuesOID] [int] NULL,
	[MachineFlowsheetSummaryValuesOID] [int] NULL,
	[UserDirectSummaryValuesOID] [int] NULL,
	[UserFlowsheetSummaryValuesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
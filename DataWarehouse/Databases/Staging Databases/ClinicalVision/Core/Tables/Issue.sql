﻿CREATE TABLE [Core].[Issue](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Issue] [nvarchar](255) NULL,
	[ProductInfoXsl] [nvarchar](max) NULL,
	[ProductInfoDtd] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
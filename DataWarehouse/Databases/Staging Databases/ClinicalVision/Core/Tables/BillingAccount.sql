﻿CREATE TABLE [Core].[BillingAccount](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[UseEpisodeAccount] [tinyint] NULL DEFAULT ('0'),
	[AccountNo] [nvarchar](20) NULL
) ON [PRIMARY]
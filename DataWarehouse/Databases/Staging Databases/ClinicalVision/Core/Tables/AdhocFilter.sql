﻿CREATE TABLE [Core].[AdhocFilter](
	[oid] [int] NOT NULL,
	[SelectionCriteriaOID] [int] NULL,
	[rev_ViewAdhocFiltersAdhocFiltersOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[FilterName] [nvarchar](100) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
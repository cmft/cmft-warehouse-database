﻿CREATE TABLE [Core].[InsAuthProvider](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[FinancialCoverageMasterOID] [int] NULL,
	[ProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ProviderNo] [nvarchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Alias](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[NameType] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
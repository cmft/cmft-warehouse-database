﻿CREATE TABLE [Core].[FluidsTemplate](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ObservationCategory] [int] NULL,
	[Name] [nvarchar](100) NULL
) ON [PRIMARY]
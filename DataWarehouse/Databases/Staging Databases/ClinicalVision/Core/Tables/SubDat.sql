﻿CREATE TABLE [Core].[SubDat](
	[oid] [int] NOT NULL,
	[SectDatOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Section] [int] NULL,
	[SubSection] [nvarchar](50) NULL,
	[Subtitle] [nvarchar](100) NULL
) ON [PRIMARY]
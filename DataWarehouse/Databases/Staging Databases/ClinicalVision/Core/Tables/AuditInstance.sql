﻿CREATE TABLE [Core].[AuditInstance](
	[oid] [int] NOT NULL,
	[AuditTransactionOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ObjectKey] [nvarchar](20) NULL,
	[ObjectType] [nvarchar](40) NULL,
	[ObjectName] [nvarchar](255) NULL,
	[Operation] [nvarchar](20) NULL,
	[AuditAttributesRTF] [nvarchar](max) NULL,
	[AuditAttributesXML] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
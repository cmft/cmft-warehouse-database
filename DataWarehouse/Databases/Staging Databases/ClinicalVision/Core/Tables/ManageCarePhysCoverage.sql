﻿CREATE TABLE [Core].[ManageCarePhysCoverage](
	[oid] [int] NOT NULL,
	[MedicalPracticeOfRecordOID] [int] NULL,
	[MedicalStaffOfRecordOID] [int] NULL,
	[PatientDateRangeOID] [int] NULL,
	[PatientInsuranceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
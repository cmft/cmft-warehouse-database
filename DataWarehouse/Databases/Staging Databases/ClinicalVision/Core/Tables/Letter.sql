﻿CREATE TABLE [Core].[Letter](
	[oid] [int] NOT NULL,
	[GeneratedDocumentOID] [int] NULL,
	[ExternalDocumentOID] [int] NULL,
	[ObservationOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PatientDateTime] [datetime] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[LetterSent] [tinyint] NULL DEFAULT ('0'),
	[LetterSentDateTime] [datetime] NULL,
	[revision] [int] NULL,
	[Description] [nvarchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[IdleJob](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](40) NULL,
	[Args] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[EmploymentHistory](
	[oid] [int] NOT NULL,
	[EmployerAddressGroupOID] [int] NULL,
	[EmployerPhoneNumberGroupOID] [int] NULL,
	[PatientDateRangeOID] [int] NULL,
	[EmploymentHistoryGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[EmploymentStatus] [int] NULL,
	[Occupation] [int] NULL,
	[OccupationOther] [nvarchar](30) NULL,
	[JobTitle] [nvarchar](30) NULL,
	[Employer] [nvarchar](30) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[Height](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Height] [float] NULL,
	[HeightUnits] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[ReportDefinition](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[EditCount] [int] NULL,
	[ReportDataSetDefinition] [nvarchar](max) NULL,
	[ReportTemplateFile] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
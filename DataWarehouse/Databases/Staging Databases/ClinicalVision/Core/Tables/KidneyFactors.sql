﻿CREATE TABLE [Core].[KidneyFactors](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ExhaustedVascularAccess] [int] NULL,
	[ExhaustedPeritonealAccess] [int] NULL,
	[AgeDiabetesOnset] [int] NULL,
	[AgeDiabetesOnsetStatus] [int] NULL,
	[CreatinineClearanceStatus] [int] NULL,
	[CreatinineClearanceMethod] [int] NULL
) ON [PRIMARY]
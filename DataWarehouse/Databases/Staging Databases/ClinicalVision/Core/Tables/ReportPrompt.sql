﻿CREATE TABLE [Core].[ReportPrompt](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AttributeBooleanValue] [tinyint] NULL,
	[AttributeDateValue] [datetime] NULL,
	[AttributeDateTimeValue] [datetime] NULL,
	[AttributeEnumerationValue] [int] NULL,
	[AttributeIntValue] [int] NULL,
	[AttributeRealValue] [float] NULL,
	[AttributeTimeValue] [datetime] NULL,
	[OffSet] [int] NULL,
	[OffsetType] [int] NULL,
	[AttributeName] [nvarchar](100) NULL,
	[AttributeStringValue] [nvarchar](255) NULL,
	[AttributeScalarTypeID] [nvarchar](100) NULL,
	[AttributeMemoValue] [nvarchar](max) NULL,
	[AttributeRTFValue] [nvarchar](max) NULL,
	[AttributeCPTProcMediVal] [nvarchar](max) NULL,
	[Prompt] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
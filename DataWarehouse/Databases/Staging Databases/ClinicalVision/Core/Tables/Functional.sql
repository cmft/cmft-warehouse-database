﻿CREATE TABLE [Core].[Functional](
	[oid] [int] NOT NULL,
	[DisabilityDetail] [int] NULL,
	[DisabilityCategory] [int] NULL
) ON [PRIMARY]
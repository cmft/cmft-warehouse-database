﻿CREATE TABLE [Core].[EmploymentHistoryGroup](
	[oid] [int] NOT NULL,
	[PrimaryEmploymentHistoryOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[LegalDocument](
	[oid] [int] NOT NULL,
	[GeneratedDocumentOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Date] [datetime] NULL,
	[InformationSource] [int] NULL,
	[TimeStamp] [datetime] NULL,
	[revision] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
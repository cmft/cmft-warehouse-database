﻿CREATE TABLE [Core].[Credentials](
	[oid] [int] NOT NULL,
	[DateRangeNoDefaultOID] [int] NULL,
	[rev_StaffCredentialsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StatusDate] [datetime] NULL,
	[Credentials] [int] NULL,
	[Required] [tinyint] NULL DEFAULT ('0'),
	[CredentialStatus] [int] NULL,
	[Comment] [nvarchar](40) NULL
) ON [PRIMARY]
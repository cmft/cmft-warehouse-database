﻿CREATE TABLE [Core].[ConsultationService](
	[oid] [int] NOT NULL,
	[ConsultRequestOID] [int] NULL,
	[GeneratedDocumentHolderOID] [int] NULL,
	[IsCareTeamDocumentation] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
﻿CREATE TABLE [Core].[LiverTransplantStatus](
	[oid] [int] NOT NULL,
	[LivPrimaryDiagnosis] [int] NULL,
	[ReasonNotCandidate] [int] NULL,
	[RegistryCode] [int] NULL,
	[LivPrimaryDiagnosisOther] [nvarchar](40) NULL
) ON [PRIMARY]
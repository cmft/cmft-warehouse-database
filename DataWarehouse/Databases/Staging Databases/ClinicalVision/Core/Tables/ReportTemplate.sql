﻿CREATE TABLE [Core].[ReportTemplate](
	[oid] [int] NOT NULL,
	[_ReportDefinitionOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ReportType] [int] NULL,
	[ReportViewType] [int] NULL,
	[EditCount] [int] NULL,
	[HotRoded] [tinyint] NULL DEFAULT ('0'),
	[QuickReport] [tinyint] NULL DEFAULT ('0'),
	[ReportName] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[CommonDiagnosis](
	[oid] [int] NOT NULL,
	[ICDCodeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicalSpecialty] [int] NULL,
	[System] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[UseCustomCode] [tinyint] NULL DEFAULT ('0'),
	[Name] [nvarchar](255) NULL,
	[Code] [nvarchar](50) NULL,
	[CustomCode] [nvarchar](255) NULL,
	[CustomICD9] [nvarchar](100) NULL,
	[DateRangeOID] [int] NULL
) ON [PRIMARY]
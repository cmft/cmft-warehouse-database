﻿CREATE TABLE [Core].[MachineCheck](
	[oid] [int] NOT NULL,
	[MachineTemperatureOID] [int] NULL,
	[StaffOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AlarmTestDone] [int] NULL,
	[PressureTestDone] [int] NULL,
	[VenousLineAirDetector] [int] NULL,
	[AirDetectorOn] [int] NULL,
	[VenousArterialAlarmOn] [int] NULL,
	[UnusedLinesClamped] [int] NULL,
	[RecircStartTime] [datetime] NULL,
	[pH] [float] NULL,
	[Conductivity] [float] NULL,
	[MethodOfSterilization] [int] NULL,
	[IndependentCond] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[WeekOfMonth](
	[oid] [int] NOT NULL,
	[rev_PatternWeekOfMonthsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[WeekOfMonth] [int] NULL
) ON [PRIMARY]
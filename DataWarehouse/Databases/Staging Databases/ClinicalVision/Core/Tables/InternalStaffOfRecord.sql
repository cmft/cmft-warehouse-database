﻿CREATE TABLE [Core].[InternalStaffOfRecord](
	[oid] [int] NOT NULL,
	[InternalOID] [int] NULL,
	[OtherStaffOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[InterfaceSource] [int] NULL
) ON [PRIMARY]
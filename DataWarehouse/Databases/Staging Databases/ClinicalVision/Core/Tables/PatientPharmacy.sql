﻿CREATE TABLE [Core].[PatientPharmacy](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[PharmacyOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TimeStamp] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Anticoagulation](
	[oid] [int] NOT NULL,
	[DrugNameOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AnticoagulationType] [int] NULL
) ON [PRIMARY]
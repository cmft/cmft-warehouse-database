﻿CREATE TABLE [Core].[Consents](
	[oid] [int] NOT NULL,
	[ObtainedByOID] [int] NULL,
	[ConsentType] [int] NULL,
	[Signedby] [nvarchar](40) NULL
) ON [PRIMARY]
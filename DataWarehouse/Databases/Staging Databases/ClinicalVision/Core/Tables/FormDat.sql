﻿CREATE TABLE [Core].[FormDat](
	[oid] [int] NOT NULL,
	[ProdDatOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ProdCode] [int] NULL,
	[FormCode] [int] NULL,
	[Form] [nvarchar](100) NULL,
	[Co] [nvarchar](max) NULL,
	[Brand] [nvarchar](100) NULL,
	[Drowsy] [nvarchar](50) NULL,
	[BrandTagFree] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
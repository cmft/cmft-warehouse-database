﻿CREATE TABLE [Core].[RenalMedicalDevice](
	[oid] [int] NOT NULL,
	[DialysisMachineOID] [int] NULL,
	[VendorOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[EquipmentType] [int] NULL,
	[OtherEquipment] [nvarchar](40) NULL
) ON [PRIMARY]
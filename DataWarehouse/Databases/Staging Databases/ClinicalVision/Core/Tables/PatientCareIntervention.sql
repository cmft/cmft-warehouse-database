﻿CREATE TABLE [Core].[PatientCareIntervention](
	[oid] [int] NOT NULL,
	[CarePlanInterventionOID] [int] NULL,
	[DateRangeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[InterventionMet] [tinyint] NULL,
	[DateMet] [datetime] NULL,
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
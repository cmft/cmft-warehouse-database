﻿CREATE TABLE [Core].[SurveyRegionStats](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RegionName] [nvarchar](50) NULL
) ON [PRIMARY]
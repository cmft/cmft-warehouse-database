﻿CREATE TABLE [Core].[DialysateBath](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Dialysate] [int] NULL,
	[Potassium] [nvarchar](50) NULL,
	[Calcium] [nvarchar](50) NULL,
	[Sodium] [nvarchar](50) NULL,
	[Bicarb] [nvarchar](50) NULL,
	[Glucose] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[BloodProductFlowsheet](
	[oid] [int] NOT NULL,
	[Check1OID] [int] NULL,
	[Check2OID] [int] NULL,
	[PatientDateRangeOID] [int] NULL,
	[TemperatureOID] [int] NULL,
	[VolumeOID] [int] NULL,
	[BloodProductServiceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Units] [int] NULL,
	[BagNo] [nvarchar](50) NULL
) ON [PRIMARY]
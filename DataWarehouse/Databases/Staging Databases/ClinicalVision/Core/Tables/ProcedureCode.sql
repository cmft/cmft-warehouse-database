﻿CREATE TABLE [Core].[ProcedureCode](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[USProcedureCode] [nvarchar](max) NULL,
	[UKProcedureCode] [nvarchar](max) NULL,
	[ReptProcedureCode] [nvarchar](1000) NULL,
	[ReptProcedureDescription] [nvarchar](max) NULL,
	[ProcedureCode] [nvarchar](1000) NULL,
	[ProcedureDescription] [nvarchar](255) NULL,
	[USProcedureCodeSummary] [nvarchar](max) NULL,
	[UKProcedureCodeSummary] [nvarchar](max) NULL,
	[Scheme] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
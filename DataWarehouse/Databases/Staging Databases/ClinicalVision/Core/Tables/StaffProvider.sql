﻿CREATE TABLE [Core].[StaffProvider](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[StaffOID] [int] NULL,
	[ProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StaffPosition] [int] NULL,
	[MedicalSpecialty] [int] NULL,
	[EmploymentStatus] [int] NULL
) ON [PRIMARY]
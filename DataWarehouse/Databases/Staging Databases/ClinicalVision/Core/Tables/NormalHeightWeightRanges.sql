﻿CREATE TABLE [Core].[NormalHeightWeightRanges](
	[oid] [int] NOT NULL,
	[AgeRangeOID] [int] NULL,
	[HeightMAXOID] [int] NULL,
	[HeightMINOID] [int] NULL,
	[WeightMAXOID] [int] NULL,
	[WeightMINOID] [int] NULL,
	[rev_SystemDefaultsNormalHeightWeightRangesesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Sex] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[TransplantStatus](
	[oid] [int] NOT NULL,
	[CandidateRegistrationOID] [int] NULL,
	[ReferralInformationOID] [int] NULL,
	[TransplantIntakeStaffOID] [int] NULL,
	[TransplantOrgan] [int] NULL,
	[PrimaryDiagnosisCode] [int] NULL,
	[TransplantPriority] [int] NULL,
	[TransplantType] [int] NULL,
	[RegistryDate] [datetime] NULL,
	[RegistrySent] [tinyint] NULL DEFAULT ('0'),
	[MeldScore] [int] NULL,
	[TransplantEvent] [int] NULL,
	[RegistryNo] [nvarchar](20) NULL
) ON [PRIMARY]
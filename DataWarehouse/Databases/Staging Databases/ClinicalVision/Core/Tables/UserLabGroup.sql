﻿CREATE TABLE [Core].[UserLabGroup](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ShareGroup] [tinyint] NULL DEFAULT ('0'),
	[ProductSuite] [int] NULL,
	[Name] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
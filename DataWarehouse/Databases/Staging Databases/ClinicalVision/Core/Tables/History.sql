﻿CREATE TABLE [Core].[History](
	[oid] [int] NOT NULL,
	[ShareOnObservationLog] [tinyint] NULL DEFAULT ('0'),
	[ReasonHistoryTaken] [int] NULL,
	[ChiefComplaint] [nvarchar](max) NULL,
	[HistoryOfPresentIllness] [nvarchar](max) NULL,
	[PastMedicalHistory] [nvarchar](max) NULL,
	[FamilyHistory] [nvarchar](max) NULL,
	[PyschosocialHistory] [nvarchar](max) NULL,
	[SurgicalHistory] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
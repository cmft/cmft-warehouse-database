﻿CREATE TABLE [Core].[Symptom](
	[oid] [int] NOT NULL,
	[rev_AbstractComplicationSymptomsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[InfectionSymptom] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[ReportJob](
	[oid] [int] NOT NULL,
	[ReportOutputDevicesOID] [int] NULL,
	[ReportTemplateOID] [int] NULL,
	[EMailReport] [tinyint] NULL,
	[DataSetDefinition] [nvarchar](max) NULL,
	[PrinterName] [nvarchar](100) NULL,
	[OutputFolder] [nvarchar](255) NULL,
	[OutputFileName] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
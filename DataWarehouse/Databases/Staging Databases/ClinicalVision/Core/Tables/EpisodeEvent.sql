﻿CREATE TABLE [Core].[EpisodeEvent](
	[oid] [int] NOT NULL,
	[AdmittedByOID] [int] NULL,
	[DischargeSummaryOID] [int] NULL,
	[LocationOID] [int] NULL,
	[ProviderOfRecordOID] [int] NULL,
	[ProvisionalDiagnosisOID] [int] NULL,
	[EpisodeType] [int] NULL,
	[AdmissionSource] [int] NULL,
	[AdmissionType] [int] NULL,
	[Acute] [int] NULL,
	[IntendedManagement] [int] NULL,
	[EpisodeAccountNo] [nvarchar](20) NULL,
	[ReasonForAdmit] [nvarchar](max) NULL,
	[ChiefComplaint] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[RejectionEpisode](
	[oid] [int] NOT NULL,
	[TransplantSurgeryOID] [int] NULL,
	[Etiology] [int] NULL,
	[InfectionRejectionTherapy] [int] NULL
) ON [PRIMARY]
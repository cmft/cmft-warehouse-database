﻿CREATE TABLE [Core].[ParenteralAdditive](
	[oid] [int] NOT NULL,
	[DoseOID] [int] NULL,
	[rev_ParenteralDietParenteralAdditivesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DietParenteralNutrition] [int] NULL
) ON [PRIMARY]
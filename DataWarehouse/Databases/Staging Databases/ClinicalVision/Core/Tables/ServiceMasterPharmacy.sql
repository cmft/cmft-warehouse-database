﻿CREATE TABLE [Core].[ServiceMasterPharmacy](
	[oid] [int] NOT NULL,
	[MedicationGroup] [int] NULL,
	[Immunization] [tinyint] NULL DEFAULT ('0'),
	[RequiresTwoSignatures] [int] NULL
) ON [PRIMARY]
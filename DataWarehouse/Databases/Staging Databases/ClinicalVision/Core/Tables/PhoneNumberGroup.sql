﻿CREATE TABLE [Core].[PhoneNumberGroup](
	[oid] [int] NOT NULL,
	[PrimaryPhoneNumberOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
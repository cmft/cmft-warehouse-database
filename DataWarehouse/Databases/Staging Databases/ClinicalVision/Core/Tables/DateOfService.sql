﻿CREATE TABLE [Core].[DateOfService](
	[oid] [int] NOT NULL,
	[AppointmentBookDateOID] [int] NULL,
	[LocationOID] [int] NULL,
	[ProviderOID] [int] NULL,
	[MasterDateOfServiceOID] [int] NULL,
	[ActivityOID] [int] NULL,
	[ScheduleEventOID] [int] NULL,
	[PersonOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ScheduleDate] [datetime] NULL,
	[DateCancelled] [datetime] NULL,
	[CancelFlag] [tinyint] NULL,
	[ServiceMasterTypeID] [int] NULL,
	[Med] [tinyint] NULL,
	[HD] [tinyint] NULL,
	[Test] [tinyint] NULL,
	[Special] [tinyint] NULL,
	[ReasonCancel] [int] NULL,
	[Standard] [tinyint] NULL,
	[inUseFlag] [tinyint] NULL,
	[StopDate] [datetime] NULL,
	[IsMaster] [tinyint] NULL,
	[Note] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Infection](
	[oid] [int] NOT NULL,
	[DialysisMachinesOID] [int] NULL,
	[DialysisProviderOID] [int] NULL,
	[DialysisStationOID] [int] NULL,
	[EpisodeEventOID] [int] NULL,
	[InternalStaffOfRecordOID] [int] NULL,
	[VascularAccessInfOID] [int] NULL,
	[PrimaryOrganism] [int] NULL,
	[InfectionStatus] [int] NULL,
	[CultureResultActions] [int] NULL,
	[Other] [nvarchar](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[OrderTrackingNumber] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
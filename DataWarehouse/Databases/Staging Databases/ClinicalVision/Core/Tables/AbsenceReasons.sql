﻿CREATE TABLE [Core].[AbsenceReasons](
	[oid] [int] NOT NULL,
	[AbsenceCodesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ServiceReasonNotDone] [int] NULL
) ON [PRIMARY]
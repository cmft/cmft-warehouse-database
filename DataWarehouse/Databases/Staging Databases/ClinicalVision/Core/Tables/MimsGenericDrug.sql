﻿CREATE TABLE [Core].[MimsGenericDrug](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[GenCode] [int] NULL,
	[Generic] [nvarchar](100) NULL,
	[GenericTagFree] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Dialyzer](
	[oid] [int] NOT NULL,
	[VendorOID] [int] NULL,
	[VolumeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AllowReuse] [tinyint] NULL DEFAULT ('0'),
	[MaxUses] [int] NULL,
	[importID] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[CutOffPercent] [int] NULL,
	[Name] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[TemplateReportDetails](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DataWarehouse] [tinyint] NULL DEFAULT ('0'),
	[ExportAs] [int] NULL,
	[ReportFile] [image] NULL,
	[ReportFileName] [nvarchar](1000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
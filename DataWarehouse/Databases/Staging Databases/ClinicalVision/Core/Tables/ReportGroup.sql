﻿CREATE TABLE [Core].[ReportGroup](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[importID] [int] NULL,
	[Name] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
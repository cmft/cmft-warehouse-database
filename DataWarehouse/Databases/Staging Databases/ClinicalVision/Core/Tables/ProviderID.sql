﻿CREATE TABLE [Core].[ProviderID](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AllowBilling] [tinyint] NULL DEFAULT ('0'),
	[EDTAProviderNo] [nvarchar](20) NULL,
	[GPMedicalPracticeNo] [nvarchar](20) NULL,
	[MedicareProviderNo] [nvarchar](20) NULL,
	[UNOSProviderNo] [nvarchar](20) NULL,
	[UKTTSProviderNo] [nvarchar](20) NULL,
	[RenalRegistryNo] [nvarchar](20) NULL,
	[CrownWebId] [nvarchar](40) NULL,
	[TransplantProviderNo] [nvarchar](20) NULL
) ON [PRIMARY]
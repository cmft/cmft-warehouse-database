﻿CREATE TABLE [Core].[KOA](
	[oid] [int] NOT NULL,
	[rev_DialyzersKOAsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[KOA] [int] NULL,
	[QD] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Staff](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[PrimaryContactPersonOID] [int] NULL,
	[PrimaryProviderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StaffPosition] [int] NULL,
	[MedicalSpecialty] [int] NULL,
	[RequiresCoSignature] [tinyint] NULL DEFAULT ('0'),
	[CanWriteOrders] [tinyint] NULL DEFAULT ('0'),
	[ClericalTransciber] [tinyint] NULL DEFAULT ('0'),
	[VerifyTranscribedOrders] [tinyint] NULL DEFAULT ('0'),
	[StaffID] [nvarchar](40) NULL
) ON [PRIMARY]
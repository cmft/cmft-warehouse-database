﻿CREATE TABLE [Core].[Volume](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Volume] [float] NULL,
	[VolumeUnits] [int] NULL
) ON [PRIMARY]
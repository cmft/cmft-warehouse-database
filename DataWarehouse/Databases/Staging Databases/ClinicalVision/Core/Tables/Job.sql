﻿CREATE TABLE [Core].[Job](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Notify] [tinyint] NULL,
	[JobType] [int] NULL,
	[JobName] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
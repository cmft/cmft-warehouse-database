﻿CREATE TABLE [Core].[ActionItem](
	[oid] [int] NOT NULL,
	[ActionedByOID] [int] NULL,
	[RaisedByOID] [int] NULL,
	[PatientOID] [int] NULL,
	[OrderTableOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Date] [datetime] NULL DEFAULT (getdate()),
	[Actioned] [tinyint] NULL DEFAULT ('0'),
	[Subject] [nvarchar](255) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
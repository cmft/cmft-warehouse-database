﻿CREATE TABLE [Core].[GeneratedDocument](
	[oid] [int] NOT NULL,
	[DocumentTemplateOID] [int] NULL,
	[FromStaffOID] [int] NULL,
	[ExternalDocumentOID] [int] NULL,
	[ToStaffOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Document] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[OtherStaff](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicalSpecialty] [int] NULL,
	[Name] [nvarchar](30) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[RenalTransfer](
	[oid] [int] NOT NULL,
	[ReferringPhysicianOID] [int] NULL,
	[ReferringProviderOID] [int] NULL,
	[TransferredToProviderOID] [int] NULL,
	[TransplantCentersOID] [int] NULL,
	[RenalTransfer] [int] NULL,
	[ReasonForTransfer] [int] NULL,
	[ModalityAtTransfer] [int] NULL
) ON [PRIMARY]
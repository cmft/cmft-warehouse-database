﻿CREATE TABLE [Core].[StaffSkinTest](
	[oid] [int] NOT NULL,
	[rev_StaffSkinTestsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DateTime] [datetime] NULL,
	[SkinTest] [int] NULL,
	[SkinTestReason] [int] NULL,
	[Reaction] [int] NULL,
	[Comments] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
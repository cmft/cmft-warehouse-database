﻿CREATE TABLE [Core].[Temperature](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Temperature] [float] NULL,
	[TemperatureUnits] [int] NULL
) ON [PRIMARY]
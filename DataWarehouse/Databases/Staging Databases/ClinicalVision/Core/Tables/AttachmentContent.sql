﻿CREATE TABLE [Core].[AttachmentContent](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Data] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
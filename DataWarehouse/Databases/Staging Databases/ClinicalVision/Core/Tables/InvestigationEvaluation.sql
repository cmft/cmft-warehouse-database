﻿CREATE TABLE [Core].[InvestigationEvaluation](
	[oid] [int] NOT NULL,
	[rev_AbstractInvestigationEvaluationsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IntegerValue] [int] NULL,
	[RealValue] [float] NULL,
	[DateTimeValue] [datetime] NULL,
	[CodedValue] [int] NULL,
	[BooleanValue] [tinyint] NULL DEFAULT ('0'),
	[DataType] [int] NULL,
	[CodesetType] [int] NULL,
	[DurationValue] [datetime] NULL,
	[DurationDaysValue] [int] NULL,
	[StringValue] [nvarchar](1000) NULL,
	[Name] [nvarchar](255) NULL,
	[CodedStringValue] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[ObservationComponent](
	[oid] [int] NOT NULL,
	[ObservationOID] [int] NULL,
	[rev_ObservationTemplateObservationComponentsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Sequence] [int] NULL,
	[Required] [tinyint] NULL DEFAULT ('1'),
	[ObservationCategory] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
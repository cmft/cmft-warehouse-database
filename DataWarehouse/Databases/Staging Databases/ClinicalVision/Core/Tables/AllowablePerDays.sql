﻿CREATE TABLE [Core].[AllowablePerDays](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Allowable] [int] NULL,
	[AllowableDays] [int] NULL,
	[MaximumAllowed] [int] NULL,
	[MaxDays] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[PackName](
	[oid] [int] NOT NULL,
	[PackDatOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ProdCode] [int] NULL,
	[FormCode] [int] NULL,
	[PackCode] [int] NULL,
	[PackName] [nvarchar](255) NULL,
	[PackNameTagFree] [nvarchar](255) NULL
) ON [PRIMARY]
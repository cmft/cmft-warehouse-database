﻿CREATE TABLE [Core].[LabResultDefaults](
	[oid] [int] NOT NULL,
	[ProviderOfRecordOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[LabResultName] [int] NULL,
	[Method] [int] NULL,
	[LowerLimit] [float] NULL,
	[UpperLimit] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[AbstractComplication](
	[oid] [int] NOT NULL,
	[BodySite] [int] NULL,
	[Organ] [int] NULL
) ON [PRIMARY]
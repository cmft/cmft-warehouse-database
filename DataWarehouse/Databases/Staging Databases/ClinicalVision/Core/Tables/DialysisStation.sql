﻿CREATE TABLE [Core].[DialysisStation](
	[oid] [int] NOT NULL,
	[MachineInterfacesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PortID] [nvarchar](50) NULL
) ON [PRIMARY]
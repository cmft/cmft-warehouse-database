﻿CREATE TABLE [Core].[Dose](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Dose] [float] NULL,
	[OrderUnits] [int] NULL,
	[MinimumDose] [float] NULL,
	[MaximumDose] [float] NULL,
	[DoseFormattedForReporting] [nvarchar](50) NULL,
	[Dosage] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[VascularAccessEvent](
	[oid] [int] NOT NULL,
	[AccessDefinitionOID] [int] NULL,
	[SurgeonOID] [int] NULL,
	[VascularAccessOID] [int] NULL,
	[VascularEventEtiology] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[BloodType](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[BloodGroup] [int] NULL,
	[BloodRhesus] [int] NULL,
	[InfromationSource] [int] NULL,
	[ConfirmationStatus] [int] NULL,
	[ConfirmationMethod] [int] NULL
) ON [PRIMARY]
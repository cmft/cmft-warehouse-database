﻿CREATE TABLE [Core].[Nationality](
	[oid] [int] NOT NULL,
	[rev_PersonNationalitiesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Nationality] [int] NULL
) ON [PRIMARY]
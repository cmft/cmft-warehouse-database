﻿CREATE TABLE [Core].[TrayGroup](
	[oid] [int] NOT NULL,
	[DefaultSubjectTrayOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
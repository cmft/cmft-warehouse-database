﻿CREATE TABLE [Core].[HeartLungFactors](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Pneumothorax] [int] NULL,
	[InfectionIVTherapy] [int] NULL,
	[LeftVentricularRemodeling] [int] NULL,
	[Pneumoreduction] [int] NULL,
	[TransmyocardialRevasc] [int] NULL,
	[Sternotomy] [int] NULL
) ON [PRIMARY]
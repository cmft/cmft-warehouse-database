﻿CREATE TABLE [Core].[RoundingAssignment](
	[oid] [int] NOT NULL,
	[PatternOID] [int] NULL,
	[ProviderOID] [int] NULL,
	[PatientsOfOID] [int] NULL,
	[RoundingMemberOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RenalShift] [int] NULL,
	[StartDate] [datetime] NULL,
	[StopDate] [datetime] NULL,
	[cvInternalProviderID] [int] NULL,
	[cvInternalPatientsOfID] [int] NULL,
	[PatternString] [nvarchar](255) NULL
) ON [PRIMARY]
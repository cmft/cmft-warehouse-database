﻿CREATE TABLE [Core].[InsuranceCoverageGroup](
	[oid] [int] NOT NULL,
	[PatientPrimaryInsuranceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
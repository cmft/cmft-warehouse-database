﻿CREATE TABLE [Core].[HeartFactors](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SuddenDeath] [int] NULL,
	[Antiarrythmics] [int] NULL,
	[Amiodarone] [int] NULL,
	[ImplantableDefibrillator] [int] NULL,
	[ExerciseOxygenConsumption] [int] NULL,
	[ExerciseOxygenStatus] [int] NULL
) ON [PRIMARY]
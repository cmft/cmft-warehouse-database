﻿CREATE TABLE [Core].[Timeline](
	[oid] [int] NOT NULL,
	[FluidChartOID] [int] NULL,
	[GeneratedDocumentOID] [int] NULL,
	[PatientDateRangeOID] [int] NULL,
	[PhysicalExamOID] [int] NULL,
	[TimelineEpisodeEventOID] [int] NULL,
	[ReferralEventOID] [int] NULL,
	[PatientOID] [int] NULL,
	[ReferralOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[InformationSource] [int] NULL,
	[TimelineEvent] [int] NULL,
	[TimelineEventDetail] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
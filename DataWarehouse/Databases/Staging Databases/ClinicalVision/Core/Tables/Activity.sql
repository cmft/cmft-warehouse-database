﻿CREATE TABLE [Core].[Activity](
	[oid] [int] NOT NULL,
	[DefaultFilterOID] [int] NULL,
	[DefaultScheduleOID] [int] NULL,
	[PersonOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[cvInternalTouched] [tinyint] NULL
) ON [PRIMARY]
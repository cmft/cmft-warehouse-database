﻿CREATE TABLE [Core].[FinancialCoverageServices](
	[oid] [int] NOT NULL,
	[AllowablePerDaysOID] [int] NULL,
	[DateRangeOID] [int] NULL,
	[FinancialCoverageMasterOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RequiresAuthorization] [tinyint] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
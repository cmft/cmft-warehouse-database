﻿CREATE TABLE [Core].[DialyzerCheck](
	[oid] [int] NOT NULL,
	[StaffOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SterilantPresent] [int] NULL,
	[DialyzerSterilant] [int] NULL,
	[DwellTimeg4Hrs] [int] NULL,
	[DialyzerDwellTime] [int] NULL,
	[AppearanceAcceptable] [int] NULL,
	[LabelLegibleAndComplete] [int] NULL,
	[PortsCapped] [int] NULL,
	[ReuseCount] [int] NULL,
	[CheckedByPatient] [int] NULL
) ON [PRIMARY]
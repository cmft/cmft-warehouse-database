﻿CREATE TABLE [Core].[ComponentView](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Workflow] [tinyint] NULL DEFAULT ('0'),
	[Name] [nvarchar](40) NULL,
	[View] [nvarchar](40) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
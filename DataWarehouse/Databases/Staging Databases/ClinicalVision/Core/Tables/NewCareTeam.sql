﻿CREATE TABLE [Core].[NewCareTeam](
	[oid] [int] NOT NULL,
	[ResourceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL DEFAULT (getdate()),
	[StopDate] [datetime] NULL,
	[ByPatientRoundingGroup] [tinyint] NULL DEFAULT ('0'),
	[ByScheduleRoundingGroup] [tinyint] NULL DEFAULT ('0'),
	[Name] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
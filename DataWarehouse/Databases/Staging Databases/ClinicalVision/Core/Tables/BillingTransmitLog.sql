﻿CREATE TABLE [Core].[BillingTransmitLog](
	[oid] [int] NOT NULL,
	[ChargeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TransmittalDate] [datetime] NULL,
	[TransmittalAction] [int] NULL,
	[DefaultFalseTwoState] [tinyint] NULL
) ON [PRIMARY]
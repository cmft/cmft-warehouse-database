﻿CREATE TABLE [Core].[SelectionCriteria](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Count] [int] NULL,
	[Filter] [nvarchar](max) NULL,
	[Sort] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
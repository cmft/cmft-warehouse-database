﻿CREATE TABLE [Core].[PatientHealthAuthority](
	[oid] [int] NOT NULL,
	[HealthAuthorityOID] [int] NULL,
	[PatientDateRangeOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IDNo] [nvarchar](20) NULL
) ON [PRIMARY]
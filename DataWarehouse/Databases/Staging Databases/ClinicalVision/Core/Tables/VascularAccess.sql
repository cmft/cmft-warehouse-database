﻿CREATE TABLE [Core].[VascularAccess](
	[oid] [int] NOT NULL,
	[AccessDefinitionOID] [int] NULL,
	[SurgeonOID] [int] NULL,
	[Surgeon2OID] [int] NULL,
	[SurgicalHistoryOID] [int] NULL,
	[TerminationReason] [int] NULL,
	[TerminationMethod] [int] NULL,
	[DateMatured] [datetime] NULL,
	[DateReferredToLab] [datetime] NULL,
	[DateReferredForSurgery] [datetime] NULL
) ON [PRIMARY]
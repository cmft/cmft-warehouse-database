﻿CREATE TABLE [Core].[DiseaseHistory](
	[oid] [int] NOT NULL,
	[rev_FamilyMemberDiseaseHistoryOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[FamilyHistory] [int] NULL
) ON [PRIMARY]
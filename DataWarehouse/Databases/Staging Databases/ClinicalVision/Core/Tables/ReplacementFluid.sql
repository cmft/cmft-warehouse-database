﻿CREATE TABLE [Core].[ReplacementFluid](
	[oid] [int] NOT NULL,
	[VolumeOID] [int] NULL,
	[rev_ApheresisReplacementFluidsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ApheresisReplacementFluid] [int] NULL
) ON [PRIMARY]
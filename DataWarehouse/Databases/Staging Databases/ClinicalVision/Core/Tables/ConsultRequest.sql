﻿CREATE TABLE [Core].[ConsultRequest](
	[oid] [int] NOT NULL,
	[InternalStaffOID] [int] NULL,
	[MedicalStaffOfRecordOID] [int] NULL,
	[ProviderOfRecordOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ConsultFrequency] [int] NULL,
	[ForOrder] [tinyint] NULL
) ON [PRIMARY]
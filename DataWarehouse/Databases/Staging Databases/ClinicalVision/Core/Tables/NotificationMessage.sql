﻿CREATE TABLE [Core].[NotificationMessage](
	[oid] [int] NOT NULL,
	[MessageOID] [int] NULL,
	[MessageDefinitionOID] [int] NULL,
	[RecipientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
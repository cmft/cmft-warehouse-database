﻿CREATE TABLE [Core].[RenalModality](
	[oid] [int] NOT NULL,
	[DialysisProviderOID] [int] NULL,
	[RenalModality] [int] NULL,
	[RenalModalitySetting] [int] NULL,
	[RenalModalityStatus] [int] NULL,
	[ReasonModalityChange] [int] NULL
) ON [PRIMARY]
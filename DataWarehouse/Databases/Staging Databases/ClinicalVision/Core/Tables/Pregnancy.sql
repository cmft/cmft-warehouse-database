﻿CREATE TABLE [Core].[Pregnancy](
	[oid] [int] NOT NULL,
	[DueDate] [datetime] NULL,
	[DeliveryDate] [datetime] NULL,
	[GestationWeeks] [int] NULL,
	[Parity] [int] NULL,
	[Gravida] [int] NULL,
	[PregnancyOutcome] [int] NULL,
	[DeliveryMethod] [int] NULL
) ON [PRIMARY]
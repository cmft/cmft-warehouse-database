﻿CREATE TABLE [Core].[DietSupplement](
	[oid] [int] NOT NULL,
	[rev_OralDietDietSupplementsOID] [int] NULL,
	[rev_EnteralDietDietSupplementsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DietSupplement] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Administration](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Route] [int] NULL,
	[Method] [int] NULL,
	[Device] [int] NULL,
	[Site] [int] NULL,
	[AdministrationRate] [nvarchar](20) NULL
) ON [PRIMARY]
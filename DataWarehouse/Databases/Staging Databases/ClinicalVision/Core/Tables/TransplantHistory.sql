﻿CREATE TABLE [Core].[TransplantHistory](
	[oid] [int] NOT NULL,
	[GraftFailure] [int] NULL,
	[GeneticRelationship] [int] NULL,
	[DonorType] [int] NULL,
	[OtherRelationship] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[DietModifier](
	[oid] [int] NOT NULL,
	[rev_OralDietDietModifiersOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DietModifier] [int] NULL
) ON [PRIMARY]
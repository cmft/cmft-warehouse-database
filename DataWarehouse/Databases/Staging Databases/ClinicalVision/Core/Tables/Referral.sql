﻿CREATE TABLE [Core].[Referral](
	[oid] [int] NOT NULL,
	[CareTeamOID] [int] NULL,
	[MedicalStaffOfRecordOID] [int] NULL,
	[ReferringPhysicianOID] [int] NULL,
	[ReferringProviderOID] [int] NULL,
	[FunctionalStatus] [int] NULL,
	[EmploymentStatusRenal] [int] NULL,
	[LifeSupport] [int] NULL,
	[PrimaryPaymentSource] [int] NULL,
	[SecondaryPaymentSource] [int] NULL,
	[MedicalCondition] [int] NULL,
	[DateRegistryNotified] [datetime] NULL,
	[MRNAtReferringProvider] [nvarchar](12) NULL
) ON [PRIMARY]
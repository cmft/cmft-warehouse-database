﻿CREATE TABLE [Core].[SurgicalDrugs](
	[oid] [int] NOT NULL,
	[DrugOrderOID] [int] NULL,
	[rev_SurgicalHistorySurgicalDrugsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Anesthetic] [tinyint] NULL,
	[Dose] [float] NULL,
	[Units] [int] NULL,
	[Frequency] [int] NULL,
	[Comments] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[HeartCandidate](
	[oid] [int] NOT NULL,
	[GeneralFactorsOID] [int] NULL,
	[HeartFactorsOID] [int] NULL,
	[HeartLungFactorsOID] [int] NULL,
	[HemodynamicOID] [int] NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
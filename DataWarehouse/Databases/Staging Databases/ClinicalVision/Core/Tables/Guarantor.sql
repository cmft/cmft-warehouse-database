﻿CREATE TABLE [Core].[Guarantor](
	[oid] [int] NOT NULL,
	[PatientContactPersonOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[GuarantorIsSelf] [tinyint] NULL
) ON [PRIMARY]
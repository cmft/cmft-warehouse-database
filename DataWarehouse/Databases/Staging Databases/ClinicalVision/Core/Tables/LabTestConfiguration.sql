﻿CREATE TABLE [Core].[LabTestConfiguration](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[LabResultName] [int] NULL
) ON [PRIMARY]
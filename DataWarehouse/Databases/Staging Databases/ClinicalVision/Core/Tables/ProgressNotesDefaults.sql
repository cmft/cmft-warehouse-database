﻿CREATE TABLE [Core].[ProgressNotesDefaults](
	[oid] [int] NOT NULL,
	[DepartmentOID] [int] NULL,
	[GeneratedDocumentOID] [int] NULL,
	[InternalOID] [int] NULL,
	[ObservationTemplateOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ProgressNoteType] [int] NULL,
	[Multidiscipline] [int] NULL,
	[AllStaff] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[LabResultBillingRule](
	[oid] [int] NOT NULL,
	[rev_ServiceMasterChargeCodeLabResultBillingRulesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[LabResultName] [int] NULL,
	[MinValue] [float] NULL,
	[MaxValue] [float] NULL,
	[Days] [int] NULL,
	[RaiseChargeAnyway] [tinyint] NULL,
	[PublishResult] [tinyint] NULL
) ON [PRIMARY]
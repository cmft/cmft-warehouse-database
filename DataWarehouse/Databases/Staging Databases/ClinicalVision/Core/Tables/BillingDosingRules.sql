﻿CREATE TABLE [Core].[BillingDosingRules](
	[oid] [int] NOT NULL,
	[ActiveDateRangeOID] [int] NULL,
	[rev_ServiceMasterChargeCodeBillingDosingRulesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MinDose] [float] NULL,
	[MaxDose] [float] NULL,
	[OrderRoute] [int] NULL,
	[OrderUnits] [int] NULL,
	[QuantityToBill] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[MedicalStaff](
	[oid] [int] NOT NULL,
	[MedicalPracticeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AdmittingPrivileges] [tinyint] NULL DEFAULT ('1'),
	[MedicareNo] [nvarchar](20) NULL,
	[MedicaidNo] [nvarchar](20) NULL,
	[NationalCode] [nvarchar](20) NULL,
	[DEA] [nvarchar](20) NULL,
	[UPIN] [nvarchar](20) NULL,
	[MINC] [nvarchar](20) NULL,
	[BillingNo] [nvarchar](20) NULL,
	[CollegeNo] [nvarchar](20) NULL,
	[PrescriberNo] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[AppointmentBookDate](
	[oid] [int] NOT NULL,
	[AppointmentBookTemplateOID] [int] NULL,
	[ScheduleTemplateOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL DEFAULT (getdate()),
	[Additional] [tinyint] NULL DEFAULT ('0'),
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[Diet](
	[oid] [int] NOT NULL,
	[EnteralDietOID] [int] NULL,
	[OralDietOID] [int] NULL,
	[ParenteralDietOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DietType] [int] NULL,
	[DietFrequency] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[InternalProvider](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
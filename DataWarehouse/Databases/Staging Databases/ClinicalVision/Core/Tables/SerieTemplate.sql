﻿CREATE TABLE [Core].[SerieTemplate](
	[oid] [int] NOT NULL,
	[SelectionCriteriaOID] [int] NULL,
	[rev_GraphTemplateSerieTemplatesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[XIsDateTime] [tinyint] NULL,
	[XAttributeName] [nvarchar](50) NULL,
	[YAttributeName] [nvarchar](50) NULL,
	[CompoundTypeKey] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[FluidReading](
	[oid] [int] NOT NULL,
	[FluidsInOID] [int] NULL,
	[FluidsOutOID] [int] NULL,
	[PhysicalExamOID] [int] NULL,
	[FluidChartOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TimeStamp] [datetime] NULL
) ON [PRIMARY]
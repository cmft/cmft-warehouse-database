﻿CREATE TABLE [Core].[SelectableProcedureCode](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Code] [nvarchar](20) NULL,
	[Description] [nvarchar](1000) NULL,
	[Scheme] [nvarchar](20) NULL
) ON [PRIMARY]
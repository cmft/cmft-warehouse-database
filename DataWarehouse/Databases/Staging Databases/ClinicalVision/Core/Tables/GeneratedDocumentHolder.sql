﻿CREATE TABLE [Core].[GeneratedDocumentHolder](
	[oid] [int] NOT NULL,
	[GeneratedDocumentOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
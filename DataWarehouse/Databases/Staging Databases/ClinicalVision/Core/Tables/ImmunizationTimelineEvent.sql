﻿CREATE TABLE [Core].[ImmunizationTimelineEvent](
	[oid] [int] NOT NULL,
	[MedicationOrderOID] [int] NULL,
	[MedicationServiceOID] [int] NULL,
	[ImmunizationEventType] [int] NULL,
	[Disease] [int] NULL,
	[VaccinationGiven] [int] NULL,
	[WhereGiven] [int] NULL,
	[ReasonNotGiven] [int] NULL
) ON [PRIMARY]
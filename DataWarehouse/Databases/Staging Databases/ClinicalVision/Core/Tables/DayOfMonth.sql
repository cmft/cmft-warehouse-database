﻿CREATE TABLE [Core].[DayOfMonth](
	[oid] [int] NOT NULL,
	[rev_PatternDayOfMonthsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DayOfMonth] [int] NULL
) ON [PRIMARY]
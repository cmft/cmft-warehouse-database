﻿CREATE TABLE [Core].[LabGroupComponents](
	[oid] [int] NOT NULL,
	[UserLabGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[LabResultName] [int] NULL,
	[Sequence] [int] NULL
) ON [PRIMARY]
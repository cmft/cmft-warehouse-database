﻿CREATE TABLE [Core].[IdentificationPicture](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IDPicture] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
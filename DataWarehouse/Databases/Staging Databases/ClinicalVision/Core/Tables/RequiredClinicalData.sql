﻿CREATE TABLE [Core].[RequiredClinicalData](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[HeightRequired] [tinyint] NULL DEFAULT ('0'),
	[WeightRequired] [tinyint] NULL DEFAULT ('0'),
	[OtherClinicalDataRequired] [tinyint] NULL DEFAULT ('0'),
	[OtherClinicalData] [nvarchar](20) NULL
) ON [PRIMARY]
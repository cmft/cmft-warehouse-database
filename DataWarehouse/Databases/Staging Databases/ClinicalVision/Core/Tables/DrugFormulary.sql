﻿CREATE TABLE [Core].[DrugFormulary](
	[oid] [int] NOT NULL,
	[DrugNameOID] [int] NULL,
	[MimsDrugOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AllowSelection] [tinyint] NULL DEFAULT ('1'),
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[importID] [int] NULL,
	[Drug] [nvarchar](40) NULL,
	[Comment] [nvarchar](40) NULL
) ON [PRIMARY]
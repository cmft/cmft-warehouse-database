﻿CREATE TABLE [Core].[AppointmentBookTemplate](
	[oid] [int] NOT NULL,
	[DefaultCalendarOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL DEFAULT (getdate()),
	[StopDate] [datetime] NULL,
	[Name] [nvarchar](255) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
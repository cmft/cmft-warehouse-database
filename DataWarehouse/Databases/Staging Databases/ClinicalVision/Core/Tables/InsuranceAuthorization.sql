﻿CREATE TABLE [Core].[InsuranceAuthorization](
	[oid] [int] NOT NULL,
	[PatientDateRangeOID] [int] NULL,
	[ServiceMasterOID] [int] NULL,
	[PatientInsuranceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[NumberAllowed] [int] NULL,
	[CopaymentRequired] [int] NULL,
	[Copayment] [float] NULL,
	[ApprovalNo] [nvarchar](20) NULL,
	[ApprovedBy] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
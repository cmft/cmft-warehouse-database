﻿CREATE TABLE [Core].[Allergen](
	[oid] [int] NOT NULL,
	[DrugNameOID] [int] NULL,
	[MimsGenericDrugOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[CommonAllergen] [int] NULL,
	[OtherAllergen] [nvarchar](30) NULL
) ON [PRIMARY]
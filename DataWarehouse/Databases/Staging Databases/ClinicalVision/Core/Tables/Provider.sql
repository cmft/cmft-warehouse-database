﻿CREATE TABLE [Core].[Provider](
	[oid] [int] NOT NULL,
	[AddressGroupOID] [int] NULL,
	[AdministrationDualSignOffSettingOID] [int] NULL,
	[PhoneNumberGroupOID] [int] NULL,
	[ProviderIDOID] [int] NULL,
	[ResourceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ProviderType] [int] NULL,
	[importID] [int] NULL,
	[TimeStamp] [datetime] NULL DEFAULT (getdate()),
	[Active] [tinyint] NULL DEFAULT ('1'),
	[Name] [nvarchar](100) NULL,
	[InternalDepartmentNo] [nvarchar](20) NULL,
	[Comments] [nvarchar](max) NULL,
	[ShortName] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
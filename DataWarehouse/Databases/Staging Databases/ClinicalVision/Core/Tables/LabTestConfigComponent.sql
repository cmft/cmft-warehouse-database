﻿CREATE TABLE [Core].[LabTestConfigComponent](
	[oid] [int] NOT NULL,
	[rev_LabTestConfigurationLabTestConfigComponentsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[LabTestConfig] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[PDExchangeAdjustment](
	[oid] [int] NOT NULL,
	[AdjustmentVolumeOID] [int] NULL,
	[rev_CAPDPDExchangeAdjustmentsOID] [int] NULL,
	[rev_APDPDExchangeAdjustmentsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PDExchangeCriteria] [int] NULL,
	[AdjustSolution] [int] NULL,
	[Quantity] [int] NULL,
	[CriteriaComment] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[PatientCarePlan](
	[oid] [int] NOT NULL,
	[DateRangeOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicalSpeciality] [int] NULL,
	[Multidiscipline] [int] NULL,
	[FrequencyToReview] [int] NULL,
	[Objective] [nvarchar](100) NULL
) ON [PRIMARY]
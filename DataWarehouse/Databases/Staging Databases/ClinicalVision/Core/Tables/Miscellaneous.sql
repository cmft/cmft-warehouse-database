﻿CREATE TABLE [Core].[Miscellaneous](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AANum] [float] NULL,
	[CSFNum] [float] NULL,
	[SerumGoldNum] [float] NULL,
	[Show] [tinyint] NULL,
	[Notes] [tinyint] NULL,
	[AAStr] [nvarchar](50) NULL,
	[AAFlg] [nvarchar](20) NULL,
	[CSFStr] [nvarchar](50) NULL,
	[CSFFlg] [nvarchar](20) NULL,
	[SerumGoldStr] [nvarchar](50) NULL,
	[SerumGoldFlg] [nvarchar](20) NULL
) ON [PRIMARY]
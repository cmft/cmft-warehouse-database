﻿CREATE TABLE [Core].[CareTeamResourceLink](
	[oid] [int] NOT NULL,
	[ResourceOID] [int] NULL,
	[CareTeamOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL DEFAULT (getdate()),
	[StopDate] [datetime] NULL,
	[CaregiverRole] [int] NULL
) ON [PRIMARY]
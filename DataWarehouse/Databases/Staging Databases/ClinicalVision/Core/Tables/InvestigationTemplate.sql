﻿CREATE TABLE [Core].[InvestigationTemplate](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](255) NULL
) ON [PRIMARY]
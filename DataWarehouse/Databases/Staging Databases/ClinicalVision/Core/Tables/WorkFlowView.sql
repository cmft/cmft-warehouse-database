﻿CREATE TABLE [Core].[WorkFlowView](
	[oid] [int] NOT NULL,
	[ComponentViewOID] [int] NULL,
	[WorkFlowOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Page] [int] NULL,
	[Sequence] [int] NULL
) ON [PRIMARY]
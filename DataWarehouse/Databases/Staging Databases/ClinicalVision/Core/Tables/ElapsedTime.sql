﻿CREATE TABLE [Core].[ElapsedTime](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ElapsedTime] [float] NULL,
	[UnitsOfTime] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Constraint](
	[oid] [int] NOT NULL,
	[ActivityOID] [int] NULL,
	[ScheduleOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AvailableMonday] [tinyint] NULL,
	[AvailableTuesday] [tinyint] NULL,
	[AvailableWednesday] [tinyint] NULL,
	[AvailableThursday] [tinyint] NULL,
	[AvailableFriday] [tinyint] NULL,
	[AvailableSaturday] [tinyint] NULL,
	[AvailableSunday] [tinyint] NULL,
	[GenerateAppointments] [tinyint] NULL
) ON [PRIMARY]
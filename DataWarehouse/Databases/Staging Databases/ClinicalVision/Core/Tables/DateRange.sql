﻿CREATE TABLE [Core].[DateRange](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[StartDate] [datetime] NULL DEFAULT (getdate()),
	[StopDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[Evaluation](
	[oid] [int] NOT NULL,
	[ObservationComponentOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Evaluation] [int] NULL
) ON [PRIMARY]
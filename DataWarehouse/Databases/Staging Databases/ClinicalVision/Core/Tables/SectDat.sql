﻿CREATE TABLE [Core].[SectDat](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Section] [int] NULL,
	[Title] [nvarchar](255) NULL
) ON [PRIMARY]
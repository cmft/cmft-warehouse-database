﻿CREATE TABLE [Core].[DietRestriction](
	[oid] [int] NOT NULL,
	[rev_OralDietDietRestrictionsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DietRestriction] [int] NULL
) ON [PRIMARY]
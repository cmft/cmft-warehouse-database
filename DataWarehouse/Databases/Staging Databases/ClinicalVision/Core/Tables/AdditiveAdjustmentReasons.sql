﻿CREATE TABLE [Core].[AdditiveAdjustmentReasons](
	[oid] [int] NOT NULL,
	[rev_PDAdditiveAdditiveAdjustmentReasonsesOID] [int] NULL,
	[rev_PDExchangeAdjustmentAdditiveAdjustmentReasonsesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AddAdjustmentReasons] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[SummaryValues](
	[oid] [int] NOT NULL,
	[SittingBPandHROID] [int] NULL,
	[StandingBPandHROID] [int] NULL,
	[TemperatureOID] [int] NULL,
	[WeightOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Timestamp] [datetime] NULL
) ON [PRIMARY]
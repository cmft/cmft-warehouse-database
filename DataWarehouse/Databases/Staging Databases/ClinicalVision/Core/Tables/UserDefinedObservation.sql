﻿CREATE TABLE [Core].[UserDefinedObservation](
	[oid] [int] NOT NULL,
	[UserDefinedCollectionOID] [int] NULL,
	[UserDefinedDataItemName] [int] NULL,
	[UserDefinedDataCode] [int] NULL
) ON [PRIMARY]
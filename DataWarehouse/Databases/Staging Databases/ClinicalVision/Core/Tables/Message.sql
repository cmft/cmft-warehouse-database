﻿CREATE TABLE [Core].[Message](
	[oid] [int] NOT NULL,
	[PatientOID] [int] NULL,
	[SenderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[BeenReadDatetime] [datetime] NULL,
	[Deleted] [tinyint] NULL DEFAULT ('0'),
	[ReadFlag] [tinyint] NULL DEFAULT ('0'),
	[ResponseRequired] [tinyint] NULL DEFAULT ('0'),
	[SentDateTime] [datetime] NULL,
	[Urgent] [tinyint] NULL DEFAULT ('0'),
	[AffectedObjectID] [nvarchar](100) NULL,
	[Attachment] [nvarchar](max) NULL,
	[MessageBody] [nvarchar](max) NULL,
	[Title] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
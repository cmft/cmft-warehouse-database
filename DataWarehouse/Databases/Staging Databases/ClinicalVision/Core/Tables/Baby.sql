﻿CREATE TABLE [Core].[Baby](
	[oid] [int] NOT NULL,
	[BirthWeightOID] [int] NULL,
	[rev_PregnancyBabiesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Sex] [int] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Core].[PhoneNumber](
	[oid] [int] NOT NULL,
	[PhoneNumberGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PhoneUseCode] [int] NULL,
	[AreaCityCode] [nvarchar](50) NULL,
	[USPhoneNumber] [nvarchar](50) NULL,
	[UKPhoneNumber] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Extension] [nvarchar](50) NULL,
	[Comment] [nvarchar](40) NULL
) ON [PRIMARY]
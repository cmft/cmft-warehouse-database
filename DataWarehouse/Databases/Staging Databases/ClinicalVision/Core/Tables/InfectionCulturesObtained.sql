﻿CREATE TABLE [Core].[InfectionCulturesObtained](
	[oid] [int] NOT NULL,
	[rev_InfectionVascularAccessCulturesObtainedsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[CultureObtained] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[ReportSelection](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IsMasterSelection] [tinyint] NULL,
	[SelectionName] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[DXOrderResultTracking](
	[oid] [int] NOT NULL,
	[ResultsDiagnosticStudyServiceOID] [int] NULL,
	[DiagnosticStudyOrderOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TransactionDate] [datetime] NULL,
	[AccessionNumber] [nvarchar](40) NULL,
	[ErrorMessage] [nvarchar](255) NULL,
	[LabVendor] [nvarchar](100) NULL
) ON [PRIMARY]
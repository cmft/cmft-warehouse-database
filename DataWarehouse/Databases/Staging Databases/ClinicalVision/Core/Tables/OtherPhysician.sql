﻿CREATE TABLE [Core].[OtherPhysician](
	[oid] [int] NOT NULL,
	[AddressGroupOID] [int] NULL,
	[PhoneNumberGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicalSpeciality] [int] NULL,
	[Name] [nvarchar](30) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
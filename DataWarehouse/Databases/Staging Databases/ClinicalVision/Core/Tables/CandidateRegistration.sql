﻿CREATE TABLE [Core].[CandidateRegistration](
	[oid] [int] NOT NULL,
	[BloodTypeOID] [int] NULL,
	[HeightOID] [int] NULL,
	[LifeSupportModalityOID] [int] NULL,
	[LifeSupportVADBrandsOID] [int] NULL,
	[TransplantCentersOID] [int] NULL,
	[WeightOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MedicalCondition] [int] NULL,
	[LifeSupport] [int] NULL,
	[FunctionalStatus] [int] NULL,
	[EmploymentStatus] [int] NULL,
	[PrimaryPaymentSource] [int] NULL,
	[SecondaryPaymentSource] [int] NULL,
	[MeldScore] [int] NULL,
	[PELDScore] [int] NULL
) ON [PRIMARY]
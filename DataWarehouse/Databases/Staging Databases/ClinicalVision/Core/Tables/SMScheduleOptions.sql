﻿CREATE TABLE [Core].[SMScheduleOptions](
	[oid] [int] NOT NULL,
	[PatternOID] [int] NULL,
	[TemplateOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DefaultMonthlyCalendar] [tinyint] NULL DEFAULT ('0'),
	[DefaultQuarterlyCalendar] [tinyint] NULL DEFAULT ('0'),
	[DefaultSemiAnnualCalendar] [tinyint] NULL DEFAULT ('0'),
	[DefaultAnnualCalendar] [tinyint] NULL DEFAULT ('0'),
	[DefaultHDAssignment] [tinyint] NULL DEFAULT ('0'),
	[DefaultMonthly2] [tinyint] NULL DEFAULT ('0'),
	[DefaultMonthly3] [tinyint] NULL DEFAULT ('0'),
	[DefaultWeekly] [tinyint] NULL DEFAULT ('0'),
	[ExtendSchedule] [tinyint] NULL DEFAULT ('0'),
	[StopSchedule] [tinyint] NULL DEFAULT ('0')
) ON [PRIMARY]
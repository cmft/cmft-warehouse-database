﻿CREATE TABLE [Core].[PeritonealDialysisOrder](
	[oid] [int] NOT NULL,
	[APDOID] [int] NULL,
	[CAPDOID] [int] NULL,
	[PeritonealDialysisOID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[GeneralFactors](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[DiabeticStatus] [int] NULL,
	[DialysisStatus] [int] NULL,
	[PepticUlcerDisease] [int] NULL,
	[AnginaCoronaryArteryDx] [int] NULL,
	[DrugTreatedSystemicHTN] [int] NULL,
	[SymptomCerebrovascular] [int] NULL,
	[SymptomPeriphealVascular] [int] NULL,
	[DrugTreatedCOPD] [int] NULL,
	[PulmonaryEmbolismIn6m] [int] NULL,
	[PreviousTransfusions] [int] NULL,
	[PreviousMalignancy] [int] NULL,
	[PRA] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Core].[AntibioticSensitivity](
	[oid] [int] NOT NULL,
	[LabTestOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Antibiotic] [int] NULL,
	[ResultStr] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [ClusteredLocks].[Active_Servers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[location] [nvarchar](255) NULL,
	[ping] [int] NULL
) ON [PRIMARY]
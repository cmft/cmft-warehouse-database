﻿CREATE TABLE [ClusteredLocks].[Active_Locks](
	[id] [nvarchar](255) NOT NULL,
	[userId] [nvarchar](255) NULL,
	[location] [nvarchar](255) NULL,
	[timestamp] [datetime] NULL,
	[serverId] [int] NULL
) ON [PRIMARY]
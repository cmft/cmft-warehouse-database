﻿CREATE TABLE [cvAudit].[AuditAttribute](
	[oid] [int] NOT NULL,
	[AuditInstanceOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IsMemo] [tinyint] NULL,
	[AttributeName] [nvarchar](40) NULL,
	[OldValue] [nvarchar](255) NULL,
	[NewValue] [nvarchar](255) NULL,
	[OldMemo] [nvarchar](max) NULL,
	[NewMemo] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
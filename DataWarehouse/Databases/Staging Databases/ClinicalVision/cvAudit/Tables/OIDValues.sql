﻿CREATE TABLE [cvAudit].[OIDValues](
	[object_name] [char](50) NOT NULL,
	[highest_oid_used] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Transplant].[TransplantOrgan](
	[oid] [int] NOT NULL,
	[DonorOrganOID] [int] NULL,
	[LinkRecordOID] [int] NULL,
	[TransplantStatusOID] [int] NULL,
	[TransplantReferralOID] [int] NULL,
	[GraftFailureEventOID] [int] NULL,
	[TransplantPatientOID] [int] NULL,
	[GraftFunctionOnset] [int] NULL,
	[DateOnsetGraftFunction] [datetime] NULL,
	[DurDelayedGraftFunction] [int] NULL
) ON [PRIMARY]
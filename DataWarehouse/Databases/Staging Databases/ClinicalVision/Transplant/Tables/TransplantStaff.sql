﻿CREATE TABLE [Transplant].[TransplantStaff](
	[oid] [int] NOT NULL,
	[TransplantTrayGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
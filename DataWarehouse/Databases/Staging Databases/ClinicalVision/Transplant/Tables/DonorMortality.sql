﻿CREATE TABLE [Transplant].[DonorMortality](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[TransplantOnly] [int] NULL,
	[Research] [int] NULL,
	[TransplantOrResearch] [int] NULL,
	[PFiscalCoroner] [int] NULL,
	[CauseOfDeath] [nvarchar](50) NULL
) ON [PRIMARY]
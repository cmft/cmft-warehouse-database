﻿CREATE TABLE [Transplant].[TransplantTrayGroup](
	[oid] [int] NOT NULL,
	[DefaultTransplantSubjectTrayOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
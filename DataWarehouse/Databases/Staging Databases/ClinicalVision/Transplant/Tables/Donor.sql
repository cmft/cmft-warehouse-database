﻿CREATE TABLE [Transplant].[Donor](
	[oid] [int] NOT NULL,
	[DonorConsultantOID] [int] NULL,
	[RegistrationOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[UNOSID] [nvarchar](100) NULL,
	[UKTDonorNumber] [nvarchar](5) NULL
) ON [PRIMARY]
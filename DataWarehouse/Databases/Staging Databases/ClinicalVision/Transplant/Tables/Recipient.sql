﻿CREATE TABLE [Transplant].[Recipient](
	[oid] [int] NOT NULL,
	[RecipientConsultantOID] [int] NULL,
	[RegistrationOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[UKTRecipientNumber] [nvarchar](6) NULL,
	[UNOSID] [nvarchar](100) NULL
) ON [PRIMARY]
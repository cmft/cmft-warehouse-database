﻿CREATE TABLE [Transplant].[RecipientStatus](
	[oid] [int] NOT NULL,
	[RecipientStatusDetail] [int] NULL,
	[RecipientStatusReason] [int] NULL
) ON [PRIMARY]
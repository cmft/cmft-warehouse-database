﻿CREATE TABLE [Transplant].[CrossMatchLabResult](
	[oid] [int] NOT NULL,
	[CrossMatchTransplantEventOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ResultName] [int] NULL,
	[RecipientDate] [datetime] NULL,
	[DonorDate] [datetime] NULL,
	[RecipientResult] [nvarchar](50) NULL,
	[DonorResult] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Transplant].[TransplantPatient](
	[oid] [int] NOT NULL,
	[TransplantMortalityOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
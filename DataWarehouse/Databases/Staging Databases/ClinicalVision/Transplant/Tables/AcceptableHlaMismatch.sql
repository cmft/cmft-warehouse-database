﻿CREATE TABLE [Transplant].[AcceptableHlaMismatch](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[A] [nvarchar](1) NULL,
	[B] [nvarchar](1) NULL,
	[DR] [nvarchar](1) NULL,
	[Total] [nvarchar](1) NULL
) ON [PRIMARY]
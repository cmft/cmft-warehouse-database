﻿CREATE TABLE [Transplant].[LinkRecord](
	[oid] [int] NOT NULL,
	[ConfirmedByOID] [int] NULL,
	[ProviderOfRecordOID] [int] NULL,
	[TXPatientRecordOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[GeneticRelationship] [int] NULL,
	[OtherRelationship] [nvarchar](100) NULL,
	[HTANumber] [nvarchar](6) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Transplant].[TransplantSubjectTray](
	[oid] [int] NOT NULL,
	[TransplantTrayGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](30) NULL
) ON [PRIMARY]
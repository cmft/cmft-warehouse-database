﻿CREATE TABLE [Transplant].[CrossMatchTransplantEvent](
	[oid] [int] NOT NULL,
	[ActualHlaMismatchOID] [int] NULL,
	[LocationOID] [int] NULL,
	[MedicalStaffOfRecordOID] [int] NULL,
	[ProviderOfRecordOID] [int] NULL,
	[TXPatientRecordOID] [int] NULL,
	[CrossMatchResult] [int] NULL
) ON [PRIMARY]
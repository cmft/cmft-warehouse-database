﻿CREATE TABLE [Transplant].[TransplantMortality](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[CauseOfDeathUKT] [int] NULL,
	[PrimaryRecipientCOD] [int] NULL,
	[SecondaryRecipientCOD] [int] NULL,
	[MaastrichtCategory] [int] NULL,
	[OtherCauseText] [nvarchar](255) NULL,
	[OtherPrimaryCODText] [nvarchar](255) NULL,
	[OtherSecondaryCODText] [nvarchar](255) NULL
) ON [PRIMARY]
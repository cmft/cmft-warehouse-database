﻿CREATE TABLE [Transplant].[TXPatientRecord](
	[oid] [int] NOT NULL,
	[TransplantPatientOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[IdentificationNumber] [nvarchar](100) NULL,
	[MedicalRecordNo] [nvarchar](12) NULL
) ON [PRIMARY]
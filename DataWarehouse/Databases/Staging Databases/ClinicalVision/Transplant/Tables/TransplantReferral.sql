﻿CREATE TABLE [Transplant].[TransplantReferral](
	[oid] [int] NOT NULL,
	[TransplantEventOID] [int] NULL,
	[TransplantPriority] [int] NULL,
	[DonorType] [int] NULL
) ON [PRIMARY]
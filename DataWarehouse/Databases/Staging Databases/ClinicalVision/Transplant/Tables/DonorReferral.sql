﻿CREATE TABLE [Transplant].[DonorReferral](
	[oid] [int] NOT NULL,
	[TransplantOnly] [int] NULL,
	[TransplantResearch] [int] NULL,
	[ResearchOnly] [int] NULL,
	[PFiscalCoroner] [int] NULL
) ON [PRIMARY]
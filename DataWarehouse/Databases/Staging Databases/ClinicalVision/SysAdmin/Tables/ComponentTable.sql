﻿CREATE TABLE [SysAdmin].[ComponentTable](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[ComponentName] [nvarchar](40) NULL
) ON [PRIMARY]
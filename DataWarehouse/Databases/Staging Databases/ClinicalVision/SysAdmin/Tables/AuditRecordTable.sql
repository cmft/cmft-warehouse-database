﻿CREATE TABLE [SysAdmin].[AuditRecordTable](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[AuditTimestamp] [datetime] NULL,
	[Action] [int] NULL,
	[AuditDetail] [nvarchar](max) NULL,
	[Application] [nvarchar](40) NULL,
	[Device] [nvarchar](40) NULL,
	[UserDomain] [nvarchar](40) NULL,
	[UserLogin] [nvarchar](40) NULL,
	[Comment] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
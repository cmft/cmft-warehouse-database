﻿CREATE TABLE [SysAdmin].[SecuredEntityTable](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SecuredEntityName] [nvarchar](40) NULL,
	[EntityPath] [nvarchar](200) NULL,
	[EntitySelection] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
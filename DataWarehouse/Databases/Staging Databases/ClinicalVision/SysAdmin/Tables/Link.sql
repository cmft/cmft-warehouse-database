﻿CREATE TABLE [SysAdmin].[Link](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Name] [nvarchar](40) NULL,
	[URLTemplate] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
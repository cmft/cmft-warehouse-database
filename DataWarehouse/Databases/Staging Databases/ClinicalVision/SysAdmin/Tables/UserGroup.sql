﻿CREATE TABLE [SysAdmin].[UserGroup](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[UserGroupName] [nvarchar](40) NULL
) ON [PRIMARY]
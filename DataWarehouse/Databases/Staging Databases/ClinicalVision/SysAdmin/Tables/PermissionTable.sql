﻿CREATE TABLE [SysAdmin].[PermissionTable](
	[oid] [int] NOT NULL,
	[RightGroupOID] [int] NULL,
	[RoleOID] [int] NULL,
	[SecuredGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SenseOfPermission] [int] NULL,
	[PropogateValue] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [SysAdmin].[HomePageItemOrdering](
	[oid] [int] NOT NULL,
	[rev_HomePageInfoHomePageWorkFlowOrderingsOID] [int] NULL,
	[rev_HomePageInfoHomePageLinkOrderingsOID] [int] NULL,
	[rev_HomePageInfoHomePagePlaceOrderingsOID] [int] NULL,
	[rev_HomePageInfoHomePageReportOrderingsOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[Index] [int] NULL,
	[ItemIdentifier] [nvarchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SysAdmin].[RightGroup](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[GroupName] [nvarchar](40) NULL
) ON [PRIMARY]
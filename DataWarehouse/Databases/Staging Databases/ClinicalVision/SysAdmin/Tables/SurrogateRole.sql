﻿CREATE TABLE [SysAdmin].[SurrogateRole](
	[oid] [int] NOT NULL,
	[RoleConfigurationOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RoleName] [nvarchar](40) NULL
) ON [PRIMARY]
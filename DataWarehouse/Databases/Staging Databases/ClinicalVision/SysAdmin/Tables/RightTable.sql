﻿CREATE TABLE [SysAdmin].[RightTable](
	[oid] [int] NOT NULL,
	[rev_RightsOfRightGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[RightColumn] [int] NULL
) ON [PRIMARY]
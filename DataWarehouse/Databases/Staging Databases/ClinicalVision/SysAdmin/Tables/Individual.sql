﻿CREATE TABLE [SysAdmin].[Individual](
	[oid] [int] NOT NULL,
	[DefaultPackageOID] [int] NULL,
	[DefaultRoleOID] [int] NULL,
	[InternalOID] [int] NULL,
	[UserGroupOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[MaxIdleMins] [int] NULL,
	[Expiry] [datetime] NULL,
	[IndividualName] [nvarchar](40) NULL,
	[DomainName] [nvarchar](40) NULL,
	[Pin] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SysAdmin].[SecuredGroupTable](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[SecuredGroupName] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SysAdmin].[SurrogatePackage](
	[oid] [int] NOT NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PackageName] [nvarchar](40) NULL
) ON [PRIMARY]
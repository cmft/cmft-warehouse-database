﻿CREATE TABLE [SysAdmin].[HomePagePlace](
	[oid] [int] NOT NULL,
	[rev_HomePageInfoExcludedHomePagePlacesOID] [int] NULL,
	[rev_HomePageInfoIncludedHomePagePlacesOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL,
	[PlaceType] [int] NULL
) ON [PRIMARY]
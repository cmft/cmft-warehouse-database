﻿CREATE TABLE [SysAdmin].[RoleConfiguration](
	[oid] [int] NOT NULL,
	[PrescriptionReportOID] [int] NULL,
	[pid] [int] NULL,
	[tid] [int] NULL
) ON [PRIMARY]
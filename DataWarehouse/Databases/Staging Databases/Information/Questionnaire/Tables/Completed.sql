﻿CREATE TABLE [Questionnaire].[Completed](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[InfoPathFormTypeID] [int] NOT NULL,
	[InfoPathID] [int] NOT NULL,
	[DateTimeCompleted] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[DateCompleted]  AS (CONVERT([date],[DateTimeCompleted],0)),
	[TimeCompleted]  AS (CONVERT([time],[DateTimeCompleted],0))
) ON [PRIMARY]
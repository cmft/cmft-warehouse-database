﻿CREATE TABLE [Questionnaire].[Details](
	[InfoPathFormTypeID] [int] NOT NULL,
	[name] [sysname] NOT NULL,
	[version] [decimal](4, 1) NOT NULL,
	[validFrom] [date] NOT NULL,
	[validTo] [date] NULL,
	[UrlToNewForm] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
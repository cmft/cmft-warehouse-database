﻿CREATE TABLE [Questionnaire].[Answers](
	[InfoPathID] [int] NOT NULL,
	[QuestionID] [int] NOT NULL,
	[Answer] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
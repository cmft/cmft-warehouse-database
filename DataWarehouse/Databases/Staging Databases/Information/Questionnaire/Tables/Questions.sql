﻿CREATE TABLE [Questionnaire].[Questions](
	[InfoPathFormTypeID] [int] NOT NULL,
	[SectionNumber] [tinyint] NOT NULL,
	[QuestionNumber] [tinyint] NOT NULL,
	[Question] [varchar](512) NOT NULL,
	[QuestionID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
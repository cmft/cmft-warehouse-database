﻿CREATE TABLE [Information].[PROMsPatientMonitoring](
	[InfoPathID] [int] NOT NULL,
	[LastUpdated] [date] NULL,
	[Forename] [nvarchar](50) NULL,
	[Surname] [nvarchar](50) NULL,
	[HospitalNumber] [nvarchar](255) NULL,
	[PatientGroup] [nvarchar](15) NULL,
	[PreOpDate] [date] NULL,
	[Questionnaire] [bit] NULL,
	[NationalJointRegistry] [bit] NULL
) ON [PRIMARY]
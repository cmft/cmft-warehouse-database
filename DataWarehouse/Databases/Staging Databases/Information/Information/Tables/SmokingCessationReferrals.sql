﻿CREATE TABLE [Information].[SmokingCessationReferrals](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](max) NOT NULL,
	[LastName] [varchar](max) NOT NULL,
	[CaseNote] [varchar](20) NOT NULL,
	[InternalPatientNumber] [varchar](20) NOT NULL,
	[HomePhone] [varchar](max) NULL,
	[WorkPhone] [varchar](max) NULL,
	[MobilePhone] [varchar](max) NULL,
	[OtherPhoneContact] [varchar](max) NULL,
	[ReferralDate] [datetime2](7) NOT NULL,
	[InfoPathID] [int] NOT NULL,
	[IsComplete] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
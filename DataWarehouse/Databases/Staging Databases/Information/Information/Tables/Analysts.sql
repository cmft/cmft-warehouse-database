﻿CREATE TABLE [Information].[Analysts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstName] [varchar](25) NOT NULL,
	[lastName] [varchar](25) NOT NULL,
	[phone] [varchar](25) NOT NULL,
	[email] [varchar](255) NOT NULL
) ON [PRIMARY]
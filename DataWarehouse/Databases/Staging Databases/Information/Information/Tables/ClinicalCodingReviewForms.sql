﻿CREATE TABLE [Information].[ClinicalCodingReviewForms](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourcePatientNo] [int] NOT NULL,
	[EpisodeStartTime] [datetime] NOT NULL,
	[ClinicalCodingReviewFormID] [int] NOT NULL
) ON [PRIMARY]
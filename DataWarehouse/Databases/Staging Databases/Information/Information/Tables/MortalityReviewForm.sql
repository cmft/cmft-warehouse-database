﻿CREATE TABLE [Information].[MortalityReviewForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MortalityReviewDeathID] [int] NULL,
	[MortalityReviewFormID] [int] NULL
) ON [PRIMARY]
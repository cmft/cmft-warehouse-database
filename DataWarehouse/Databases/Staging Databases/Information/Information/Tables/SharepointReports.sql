﻿CREATE TABLE [Information].[SharepointReports](
	[id] [int] NOT NULL,
	[reportName] [varchar](256) NOT NULL,
	[reportGroup] [varchar](max) NOT NULL,
	[reportSubGroup] [varchar](max) NOT NULL,
	[reportFrequency] [char](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
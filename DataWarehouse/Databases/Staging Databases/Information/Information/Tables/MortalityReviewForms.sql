﻿CREATE TABLE [Information].[MortalityReviewForms](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourcePatientNo] [int] NOT NULL,
	[EpisodeStartTime] [datetime] NOT NULL,
	[MortalityReviewFormID] [int] NULL,
	[FormTypeID] [int] NULL,
	[ReviewStatus] [tinyint] NULL,
	[ContextCode] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Information].[ClinicalCodingReviewForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProviderSpellNo] [varchar](50) NOT NULL,
	[SourceEncounterNo] [varchar](20) NOT NULL,
	[ClinicalCodingReviewFormID] [int] NOT NULL
) ON [PRIMARY]
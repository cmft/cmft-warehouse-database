﻿CREATE TABLE [Information].[RehabPrescriptions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[NHSNumber] [varchar](15) NOT NULL,
	[FirstDateCompleted] [date] NOT NULL,
	[InfoPathID] [int] NOT NULL,
	[isLocked] [bit] NOT NULL DEFAULT ((0)),
	[ChildOrAdult] [char](5) NOT NULL DEFAULT ('Child'),
	[created] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[LastUpdated] [datetime] NULL,
	[DistrictNumber] [nvarchar](17) NULL
) ON [PRIMARY]
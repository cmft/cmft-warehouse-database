﻿CREATE TABLE [CARP].[Out](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[NHSNumber] [nvarchar](17) NOT NULL,
	[PathwayStartDate] [date] NOT NULL,
	[DateCARPdOut] [date] NOT NULL,
	[OrgCode] [nvarchar](6) NOT NULL,
	[DaysOnPathway] [int] NOT NULL,
	[CARPdOutFor] [nvarchar](16) NOT NULL,
	[InfoPathID] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[HospitalNumber] [nvarchar](14) NULL,
	[Forename] [nvarchar](50) NULL,
	[Surname] [nvarchar](50) NULL
) ON [PRIMARY]
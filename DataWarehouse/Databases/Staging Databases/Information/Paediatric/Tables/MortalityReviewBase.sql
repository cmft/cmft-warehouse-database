﻿CREATE TABLE [Paediatric].[MortalityReviewBase](
	[DateOfDeath] [smalldatetime] NOT NULL,
	[DateOfBirth] [datetime] NOT NULL,
	[PatientAge] [numeric](18, 0) NOT NULL,
	[PatientSurname] [varchar](30) NOT NULL,
	[PatientForename] [varchar](20) NOT NULL,
	[Sex] [varchar](6) NOT NULL,
	[AdmissionTime] [smalldatetime] NOT NULL,
	[EpisodeStartTime] [smalldatetime] NOT NULL,
	[SourcePatientNo] [varchar](20) NOT NULL,
	[CasenoteNumber] [varchar](20) NOT NULL,
	[DistrictNo] [varchar](20) NOT NULL,
	[ContextCode] [varchar](10) NOT NULL,
	[NationalSpecialty] [varchar](113) NOT NULL,
	[Consultant] [varchar](49) NOT NULL,
	[dateMortalityReviewAdded] [date] NOT NULL DEFAULT (CONVERT([date],getutcdate(),(0)))
) ON [PRIMARY]
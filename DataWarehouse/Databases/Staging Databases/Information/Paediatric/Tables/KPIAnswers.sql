﻿CREATE TABLE [Paediatric].[KPIAnswers](
	[InfoPathID] [int] NOT NULL,
	[KPIQuestionID] [int] NOT NULL,
	[Answer] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
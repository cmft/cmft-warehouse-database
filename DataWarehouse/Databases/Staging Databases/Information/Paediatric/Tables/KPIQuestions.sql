﻿CREATE TABLE [Paediatric].[KPIQuestions](
	[FormID] [int] NOT NULL,
	[DayNumber] [char](1) NULL,
	[QuestionNumber] [tinyint] NOT NULL,
	[Question] [varchar](256) NOT NULL,
	[KPIQuestionID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
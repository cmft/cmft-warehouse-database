﻿CREATE TABLE [Paediatric].[MortalityReview_Include](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[casenote] [varchar](14) NOT NULL,
	[reason] [varchar](50) NULL
) ON [PRIMARY]
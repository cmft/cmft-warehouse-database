﻿CREATE TABLE [Mortality].[ReviewStatus](
	[id] [int] NOT NULL,
	[description] [varchar](64) NOT NULL
) ON [PRIMARY]
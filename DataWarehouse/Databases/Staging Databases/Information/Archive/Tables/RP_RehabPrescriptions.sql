﻿CREATE TABLE [Archive].[RP_RehabPrescriptions](
	[id] [int] NOT NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[NHSNumber] [varchar](15) NOT NULL,
	[FirstDateCompleted] [date] NOT NULL,
	[InfoPathID] [int] NOT NULL,
	[isLocked] [bit] NOT NULL,
	[ChildOrAdult] [char](5) NOT NULL,
	[created] [datetime2](7) NOT NULL,
	[LastUpdated] [datetime] NULL,
	[DistrictNumber] [nvarchar](17) NULL
) ON [PRIMARY]
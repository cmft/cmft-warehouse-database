﻿CREATE TABLE [Archive].[RP_RehabInfoPathForms](
	[ID] [int] NOT NULL,
	[ReceivedAt] [datetime2](7) NOT NULL,
	[FormTypeID] [int] NOT NULL,
	[FormXML] [xml] NOT NULL,
	[Processed] [bit] NULL,
	[ProcessedAt] [datetime2](7) NULL,
	[AdditionalInformation] [xml] NULL,
	[LastUpdatedAt] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
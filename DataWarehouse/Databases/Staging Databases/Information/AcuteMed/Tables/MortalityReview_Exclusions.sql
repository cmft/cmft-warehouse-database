﻿CREATE TABLE [AcuteMed].[MortalityReview_Exclusions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[casenote] [varchar](14) NOT NULL,
	[reason] [varchar](20) NOT NULL
) ON [PRIMARY]
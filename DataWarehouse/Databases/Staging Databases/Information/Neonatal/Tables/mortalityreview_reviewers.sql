﻿CREATE TABLE [Neonatal].[mortalityreview_reviewers](
	[MortalityReviewFormID] [int] NOT NULL,
	[PrimaryReviewer] [nvarchar](256) NOT NULL,
	[SecondaryReviewer] [nvarchar](256) NULL
) ON [PRIMARY]
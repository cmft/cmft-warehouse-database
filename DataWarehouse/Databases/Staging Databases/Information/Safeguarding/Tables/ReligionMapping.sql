﻿CREATE TABLE [Safeguarding].[ReligionMapping](
	[PASCode] [varchar](10) NOT NULL,
	[PASDescription] [varchar](64) NOT NULL,
	[CQCDesignation] [varchar](11) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Safeguarding].[TraffordReligionMapping](
	[Code] [varchar](4) NOT NULL,
	[Description] [varchar](20) NULL,
	[CQCDescription] [varchar](20) NULL
) ON [PRIMARY]
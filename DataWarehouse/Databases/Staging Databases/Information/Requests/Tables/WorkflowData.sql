﻿CREATE TABLE [Requests].[WorkflowData](
	[requestID] [int] NOT NULL,
	[internalPriority] [char](1) NULL,
	[assignedToAnalystID] [int] NULL,
	[requestStatus] [char](1) NOT NULL,
	[dateCompleted] [datetime] NULL,
	[InfoPathID] [int] NOT NULL,
	[ts] [datetime] NOT NULL DEFAULT (sysdatetime())
) ON [PRIMARY]
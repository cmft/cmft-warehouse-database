﻿CREATE TABLE [Requests].[WorkflowHistory](
	[requestID] [int] NOT NULL,
	[internalPriority] [char](1) NULL,
	[assignedToAnalystID] [int] NULL,
	[requestStatus] [char](1) NOT NULL,
	[dateHistoryEntry] [datetime] NOT NULL,
	[dateCompleted] [datetime] NULL
) ON [PRIMARY]
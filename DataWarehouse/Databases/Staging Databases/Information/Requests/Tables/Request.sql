﻿CREATE TABLE [Requests].[Request](
	[id] [int] IDENTITY(100100,1) NOT NULL,
	[requestType] [varchar](2) NOT NULL,
	[username] [sysname] NOT NULL,
	[contactNumber] [varchar](50) NULL,
	[department] [varchar](50) NULL,
	[dateRequested] [datetime] NULL,
	[urgency] [char](1) NOT NULL,
	[dateRequired] [datetime] NULL,
	[descriptionOfRequest] [nvarchar](max) NULL,
	[frequencyOfReport] [char](1) NULL,
	[reportToBeAmendedID] [int] NULL,
	[requestorEmail] [varchar](max) NOT NULL DEFAULT (' '),
	[isDeleted] [bit] NOT NULL DEFAULT ((0)),
	[quotedReference]  AS ([requestType]+CONVERT([varchar](20),[id],(0))),
	[userRequiringAccess] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
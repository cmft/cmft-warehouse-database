﻿CREATE TABLE [NursingWorkforce].[PlannedActuals](
	[Division] [varchar](50) NOT NULL,
	[Ward] [varchar](50) NOT NULL,
	[DateFor] [date] NOT NULL,
	[WorkShift] [varchar](10) NOT NULL,
	[RN_Planned] [int] NULL,
	[RN_Actual] [int] NULL,
	[AP_CSW_Planned] [int] NULL,
	[AP_CSW_Actual] [int] NULL,
	[Comments] [varchar](300) NULL,
	[CommentsSenior] [varchar](300) NULL,
	[GapsOverfill] [varchar](150) NULL,
	[GapsShortfall] [varchar](150) NULL,
	[Actions] [varchar](150) NULL,
	[GapsOverfillAP] [varchar](150) NULL,
	[GapsShortfallAP] [varchar](150) NULL,
	[ActionsAP] [varchar](150) NULL
) ON [PRIMARY]
﻿CREATE TABLE [MDT].[MeetingCasenotesHistory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[meetingCasenoteID] [int] NOT NULL,
	[meetingID] [int] NOT NULL,
	[caseNote] [varchar](12) NOT NULL,
	[infoPathID] [int] NULL,
	[ts] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[InfopathXML] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
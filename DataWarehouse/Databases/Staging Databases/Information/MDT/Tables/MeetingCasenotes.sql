﻿CREATE TABLE [MDT].[MeetingCasenotes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[meetingID] [int] NOT NULL,
	[caseNote] [varchar](12) NOT NULL,
	[infoPathID] [int] NULL,
	[InfopathXML] [xml] NULL,
	[FirstName] [nvarchar](255) NOT NULL,
	[LastName] [nvarchar](255) NOT NULL,
	[isComplete] [bit] NOT NULL DEFAULT ((0)),
	[DateTimeAE]  [datetime2](7) not null , --AS ([Utility].[extractXMLValueDateTime]([InfoPathXML],'DateTimeAE')),
	[DateTimeAdmitted]  [datetime2](7) not null , --AS ([Utility].[extractXMLValueDateTime]([InfoPathXML],'DateTimeAdmitted')),
	[DateTimeBreach]  [datetime2](7) not null , --AS ([Utility].[extractXMLValueDateTime]([InfoPathXML],'DateTimeBreach'))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
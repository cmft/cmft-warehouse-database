﻿CREATE TABLE [MDT].[Meeting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[meetingDate] [date] NOT NULL,
	[meetingTime] [time](7) NOT NULL
) ON [PRIMARY]
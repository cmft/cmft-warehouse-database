﻿CREATE TABLE [dbo].[tmpTable](
	[PATHOLOGY_ID] [int] IDENTITY(1,1) NOT NULL,
	[CARE_ID] [int] NOT NULL,
	[N8_1_PATHOLOGY_TYPE] [varchar](2) NULL,
	[N8_2_RECEIPT_DATE] [smalldatetime] NULL,
	[N8_3_RESULT_DATE] [smalldatetime] NULL,
	[N8_4_PATHOLOGIST] [varchar](8) NULL,
	[N8_5_ORG_CODE] [varchar](5) NULL,
	[N8_6_DIAGNOSIS] [varchar](5) NULL,
	[N8_7_TUMOUR_LATERALITY] [varchar](1) NULL,
	[N8_8_INVASIVE_LESION] [real] NULL,
	[N8_9_SYNC_TUMOUR] [varchar](1) NULL,
	[L_HISTOLOGY_GROUP] [int] NULL,
	[N8_10_HISTOLOGY] [varchar](10) NULL,
	[N8_11_GRADE_DIFF] [varchar](2) NULL,
	[N8_12_CANCER_INVASION] [varchar](2) NULL,
	[N8_13_EXCISION_MARGINS] [varchar](2) NULL,
	[N8_14_NODES] [int] NULL,
	[N8_15_POSITIVE_NODES] [int] NULL,
	[N8_16_PATH_T_STAGE] [varchar](5) NULL,
	[N8_17_PATH_N_STAGE] [varchar](5) NULL,
	[N8_18_PATH_M_STAGE] [varchar](5) NULL,
	[N8_20_REPORT_NUMBER] [varchar](50) NULL,
	[N8_21_REPORT_STATUS] [int] NULL,
	[N8_22_SPECIMEN_NATURE] [int] NULL,
	[N8_23_REQUEST_ORG] [varchar](5) NULL,
	[N8_24_REQUEST_BY] [varchar](8) NULL,
	[L_PATH_T_LETTER] [varchar](3) NULL,
	[L_PATH_N_LETTER] [varchar](3) NULL,
	[L_PATH_M_LETTER] [varchar](3) NULL,
	[L_ADEQUACY_MARGINS] [int] NULL,
	[L_COMMENTS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Lookup].[Consultant](
	[id] [bigint] NULL,
	[NationalConsultantCode] [varchar](8) NULL,
	[Consultant] [varchar](30) NULL
) ON [PRIMARY]
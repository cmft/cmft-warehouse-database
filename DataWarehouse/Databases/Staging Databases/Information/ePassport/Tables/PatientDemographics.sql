﻿CREATE TABLE [ePassport].[PatientDemographics](
	[NHSNumber] [int] NOT NULL,
	[FirstName] [varchar](64) NOT NULL,
	[LastName] [varchar](64) NOT NULL,
	[DoB] [date] NOT NULL
) ON [PRIMARY]
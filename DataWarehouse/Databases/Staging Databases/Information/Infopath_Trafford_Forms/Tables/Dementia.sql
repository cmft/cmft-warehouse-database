﻿CREATE TABLE [Infopath_Trafford_Forms].[Dementia](
	[FormId] [int] NOT NULL,
	[UserName] [varchar](255) NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[SecondaryId] [int] NULL,
	[EPMINumber] [varchar](20) NULL,
	[ActivityType] [varchar](2) NULL,
	[OriginalCreationDate] [datetime] NULL,
	[Workflow] [int] NULL,
	[FormStatus] [varchar](255) NULL,
	[FormXML] [xml] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[OriginalSubmissionDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
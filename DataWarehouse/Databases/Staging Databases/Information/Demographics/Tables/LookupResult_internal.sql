﻿CREATE TABLE [Demographics].[LookupResult_internal](
	[ListID] [uniqueidentifier] NOT NULL,
	[id] [int] NOT NULL,
	[Forename] [varchar](80) NULL,
	[Surname] [varchar](80) NULL,
	[DoB] [date] NULL,
	[NHSNumber] [varchar](12) NULL,
	[DistrictNumber] [varchar](40) NULL,
	[GPCode] [varchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Demographics].[LookupList_internal](
	[id] [int] NOT NULL,
	[patientIdentifier] [varchar](14) NOT NULL,
	[patientIdentifierType] [tinyint] NOT NULL,
	[sourceDataSite] [char](3) NOT NULL,
	[ListID] [uniqueidentifier] NOT NULL,
	[NHSNumber]  AS (case when [patientIdentifierType]=(1) then replace([patientIdentifier],' ','')  end),
	[DistrictNumber]  AS (case when [patientIdentifierType]=(0) then [patientIdentifier]  end),
	[Casenote]  AS (case when [patientIdentifierType]=(2) then [patientIdentifier]  end),
	[NHSNumberExpanded]  AS (case when [patientIdentifierType]=(1) then (((left(replace([patientIdentifier],' ',''),(3))+' ')+substring(replace([patientIdentifier],' ',''),(4),(3)))+' ')+right(replace([patientIdentifier],' ',''),(4))  end)
) ON [PRIMARY]
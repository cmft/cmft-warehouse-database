﻿CREATE TABLE [Demographics].[TraffordDistrictNumbers](
	[ListID] [uniqueidentifier] NOT NULL,
	[id] [int] NOT NULL,
	[districtNumber] [bigint] NOT NULL
) ON [PRIMARY]
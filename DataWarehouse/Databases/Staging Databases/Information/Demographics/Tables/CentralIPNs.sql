﻿CREATE TABLE [Demographics].[CentralIPNs](
	[ListID] [uniqueidentifier] NOT NULL,
	[id] [int] NOT NULL,
	[ipn] [bigint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DWdefib](
	[defibid] [int] NOT NULL,
	[patid] [int] NULL,
	[defibtime] [datetime] NULL,
	[defibtype] [nvarchar](50) NULL,
	[defibmode] [nvarchar](255) NULL,
	[defibqcpr] [bit] NOT NULL,
	[defibsock] [bit] NOT NULL,
	[defibenergy] [int] NULL,
	[defibshocktotal] [int] NULL,
	[defibcardiovert] [bit] NOT NULL
) ON [PRIMARY]
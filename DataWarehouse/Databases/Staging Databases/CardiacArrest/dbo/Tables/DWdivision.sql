﻿CREATE TABLE [dbo].[DWdivision](
	[divisionid] [nvarchar](50) NULL,
	[divisionname] [nvarchar](50) NULL,
	[divisionmanager] [nvarchar](50) NULL,
	[divisionmanagerphone] [nvarchar](50) NULL,
	[divisionmanageremail] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
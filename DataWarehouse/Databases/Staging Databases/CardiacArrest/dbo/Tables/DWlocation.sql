﻿CREATE TABLE [dbo].[DWlocation](
	[locationid] [nvarchar](50) NULL,
	[locationname] [nvarchar](50) NULL,
	[directorate] [nvarchar](50) NULL,
	[Hospital] [nvarchar](50) NULL,
	[Inuse] [bit] NOT NULL,
	[locationmanager] [nvarchar](50) NULL,
	[locationmanagerphone] [nvarchar](50) NULL,
	[locationmanageremail] [nvarchar](max) NULL,
	[NCAAlocation] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[DWconsultant](
	[consid] [int] NOT NULL,
	[staffnumber] [nvarchar](50) NULL,
	[consultantname] [nvarchar](50) NULL,
	[consultantinitial] [nvarchar](50) NULL,
	[consultantspec] [nvarchar](255) NULL
) ON [PRIMARY]
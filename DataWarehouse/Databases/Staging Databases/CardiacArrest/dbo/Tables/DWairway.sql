﻿CREATE TABLE [dbo].[DWairway](
	[airwayid] [int] NOT NULL,
	[patid] [int] NULL,
	[airwaytype] [nvarchar](50) NULL,
	[airwaysize] [int] NULL,
	[airwaytime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DWgcs](
	[gcsid] [int] NOT NULL,
	[patid] [int] NULL,
	[gcseye] [int] NULL,
	[gcsverbal] [int] NULL,
	[gcsmotor] [int] NULL,
	[gcstotal] [int] NULL,
	[gcsdate] [datetime] NULL,
	[gcstime] [datetime] NULL
) ON [PRIMARY]
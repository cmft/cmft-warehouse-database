﻿CREATE TABLE [dbo].[DWcpc](
	[cpcid] [int] NOT NULL,
	[patid] [int] NULL,
	[cpc] [nvarchar](255) NULL,
	[cpcdate] [datetime] NULL,
	[cpctime] [datetime] NULL
) ON [PRIMARY]
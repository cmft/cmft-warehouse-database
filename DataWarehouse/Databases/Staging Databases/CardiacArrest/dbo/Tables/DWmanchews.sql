﻿CREATE TABLE [dbo].[DWmanchews](
	[manchewsid] [int] NOT NULL,
	[patid] [int] NULL,
	[time] [datetime2](7) NULL,
	[score] [nvarchar](50) NULL,
	[trigger] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DWdnarbefore](
	[dnarid] [int] NOT NULL,
	[patid] [int] NULL,
	[dnarreason] [nvarchar](50) NULL,
	[dnardate] [datetime] NULL,
	[dnartime] [datetime] NULL,
	[dnarcons] [nvarchar](50) NULL
) ON [PRIMARY]
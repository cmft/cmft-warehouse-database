﻿CREATE TABLE [dbo].[DWdirectorate](
	[directorateid] [nvarchar](50) NULL,
	[directoratename] [nvarchar](50) NULL,
	[division] [nvarchar](50) NULL,
	[directoratemanager] [nvarchar](50) NULL,
	[directoratemanagerphone] [nvarchar](50) NULL,
	[directoratemanageremail] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
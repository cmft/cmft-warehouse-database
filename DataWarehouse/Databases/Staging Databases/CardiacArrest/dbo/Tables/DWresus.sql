﻿CREATE TABLE [dbo].[DWresus](
	[UUID] [int] NOT NULL,
	[NHSNumber] [nvarchar](50) NULL,
	[MRIPatientURN] [nvarchar](50) NULL,
	[BHPatientURN] [nvarchar](255) NULL,
	[PatientURN] [nvarchar](255) NULL,
	[forename] [nvarchar](50) NULL,
	[surname] [nvarchar](50) NULL,
	[dob] [datetime2](7) NULL,
	[Sex] [nvarchar](50) NULL,
	[Consultant] [int] NULL,
	[Ethnicity] [nvarchar](50) NULL,
	[patientgroup] [nvarchar](50) NULL,
	[age] [int] NULL,
	[Decade] [nvarchar](50) NULL,
	[CategoryofAge] [nvarchar](50) NULL,
	[ReasonforAdmission] [nvarchar](50) NULL,
	[DateofAdmit] [datetime2](7) NULL,
	[DateofEvent] [datetime2](7) NULL,
	[WeekofEvent] [int] NULL,
	[Location] [nvarchar](50) NULL,
	[locationtext] [nvarchar](50) NULL,
	[Directorate] [nvarchar](50) NULL,
	[Division] [nvarchar](50) NULL,
	[Witnessed] [nvarchar](50) NULL,
	[Monitored] [nvarchar](50) NULL,
	[ALSinterventionnone] [bit] NOT NULL,
	[IVaccess] [bit] NOT NULL,
	[IVmedication] [bit] NOT NULL,
	[ECGmonitor] [bit] NOT NULL,
	[TrachealTube] [bit] NOT NULL,
	[Otherairwaydevice] [bit] NOT NULL,
	[Mechanicalventilation] [bit] NOT NULL,
	[Implantdefibcardioverter] [bit] NOT NULL,
	[IntraArterialCatheter] [bit] NOT NULL,
	[ImmediateCause] [nvarchar](50) NULL,
	[ImmediateCauseOther] [nvarchar](50) NULL,
	[ResuscitationAttemptText] [nvarchar](50) NULL,
	[Conscious] [bit] NOT NULL,
	[Breathing] [bit] NOT NULL,
	[Pulse] [bit] NOT NULL,
	[Presentingecgtext] [nvarchar](50) NULL,
	[TimeCPRstopped] [datetime2](7) NULL,
	[WhyCPRstoppedtext] [nvarchar](50) NULL,
	[SpontaneousCirculation] [nvarchar](50) NULL,
	[ROSCtime] [datetime2](7) NULL,
	[TimeofDeath] [datetime2](7) NULL,
	[UnsustainedROSC] [nvarchar](50) NULL,
	[CollapseTime] [datetime2](7) NULL,
	[CPRcalloutTime] [datetime2](7) NULL,
	[CPRcalloutHour] [nvarchar](50) NULL,
	[CPRarriveTime] [datetime2](7) NULL,
	[ArrestConfirmTime] [datetime2](7) NULL,
	[CPRstartTime] [datetime2](7) NULL,
	[fdefibshockTime] [datetime2](7) NULL,
	[AirwayAchieveTime] [datetime2](7) NULL,
	[fdoseEPITime] [datetime2](7) NULL,
	[CPRstartMin] [datetime2](7) NULL,
	[fdefibshockMin] [datetime2](7) NULL,
	[AirwayAchieveMin] [datetime2](7) NULL,
	[fdoseEPIMin] [datetime2](7) NULL,
	[AwakeningTime] [datetime2](7) NULL,
	[AwakeningDate] [datetime2](7) NULL,
	[Discharge/Death] [nvarchar](50) NULL,
	[DateofDischarge] [datetime2](7) NULL,
	[DischargeDestination] [nvarchar](50) NULL,
	[DischargeDestinationOther] [nvarchar](255) NULL,
	[CPC] [tinyint] NULL,
	[GCSTotal] [tinyint] NULL,
	[GCSeye] [tinyint] NULL,
	[GCSverbal] [tinyint] NULL,
	[GCSmotor] [tinyint] NULL,
	[Alivesixmonth] [nvarchar](50) NULL,
	[Aliveoneyear] [nvarchar](50) NULL,
	[DateofDeath] [datetime2](7) NULL,
	[CauseofDeath] [nvarchar](50) NULL,
	[ICD-CM] [nvarchar](50) NULL,
	[MedicalRecords] [bit] NOT NULL,
	[DeathCertificate] [bit] NOT NULL,
	[GP] [bit] NOT NULL,
	[Postmortem] [bit] NOT NULL,
	[InfoOther] [bit] NOT NULL,
	[InfoOtherText] [nvarchar](255) NULL,
	[TypeofCallouttext] [nvarchar](50) NULL,
	[comments] [nvarchar](max) NULL,
	[interventionairway] [nvarchar](50) NULL,
	[interventioncpr] [nvarchar](50) NULL,
	[interventiondefib] [nvarchar](50) NULL,
	[interventionfluid] [nvarchar](50) NULL,
	[interventiondrug] [nvarchar](50) NULL,
	[DataQuality] [nvarchar](50) NULL,
	[DataQualityText] [nvarchar](max) NULL,
	[AwaitingInformation] [nvarchar](50) NULL,
	[CriticalIncident] [bit] NOT NULL,
	[CriticalIncidentText] [nvarchar](50) NULL,
	[NCAAnumber] [int] NULL,
	[NCAAcomplete] [bit] NOT NULL,
	[NCAAlocation] [nvarchar](255) NULL,
	[reviewLastDocument] [datetime2](7) NULL,
	[reviewClinicalConcern] [nvarchar](255) NULL,
	[reviewMedicalEmergency] [nvarchar](255) NULL,
	[reviewCardiacArrest] [nvarchar](255) NULL,
	[reviewSummary] [nvarchar](max) NULL,
	[reviewobs24] [nvarchar](255) NULL,
	[reviewobsFrequency] [nvarchar](255) NULL,
	[reviewo2] [nvarchar](255) NULL,
	[reviewfluid24] [nvarchar](255) NULL,
	[reviewEWS] [nvarchar](255) NULL,
	[reviewABG] [nvarchar](255) NULL,
	[reviewSatsAction] [nvarchar](255) NULL,
	[reviewABGaction] [nvarchar](255) NULL,
	[reviewABGpH] [int] NULL,
	[reviewABGpO2] [int] NULL,
	[reviewABGpCO2] [int] NULL,
	[reviewABGBicarb] [int] NULL,
	[reviewABGLactate] [int] NULL,
	[reviewABGother] [int] NULL,
	[reviewAppropResponse] [nvarchar](255) NULL,
	[reviewResponseEWS?] [nvarchar](255) NULL,
	[reviewResponseTimely] [nvarchar](255) NULL,
	[reviewResponseDelay] [nvarchar](max) NULL,
	[reviewHighEWS24] [nvarchar](255) NULL,
	[reviewHighEWS24resp] [nvarchar](255) NULL,
	[reviewHighEWS24response] [nvarchar](255) NULL,
	[reviewElectrolytes24] [nvarchar](255) NULL,
	[reviewElectrolyte24time] [datetime2](7) NULL,
	[reviewElecK] [float] NULL,
	[reviewElecNa] [float] NULL,
	[reviewElecHb] [float] NULL,
	[reviewElecAction] [nvarchar](255) NULL,
	[reviewFailureIdentify] [nvarchar](255) NULL,
	[reviewFailureNurse] [nvarchar](255) NULL,
	[reviewFailureNone] [nvarchar](255) NULL,
	[reviewFailureDelay] [nvarchar](255) NULL,
	[reviewFailureDelayTime] [int] NULL,
	[reviewEWS3Drattend] [nvarchar](255) NULL,
	[reviewEWS3NurseDocument] [nvarchar](255) NULL,
	[reviewEWS3DrDocument] [nvarchar](255) NULL,
	[reviewEWS3DrDocumentManag] [nvarchar](255) NULL,
	[reviewEWS3escalate] [nvarchar](255) NULL,
	[reviewEWS3NoReason] [nvarchar](max) NULL,
	[reviewEWS3FailureDr] [nvarchar](255) NULL,
	[reviewRespondAppropAction] [nvarchar](255) NULL,
	[reviewRespondAppropActionDeem] [nvarchar](255) NULL,
	[reviewDeemedNoO2] [bit] NOT NULL,
	[reviewDeemedNofluid] [bit] NOT NULL,
	[reviewDeemedNotFluid] [bit] NOT NULL,
	[reviewDeemedNoABG] [bit] NOT NULL,
	[reviewDeemedNoBloods] [bit] NOT NULL,
	[reviewDeemedNoInvestig] [bit] NOT NULL,
	[reviewDeemedNoSepsis] [bit] NOT NULL,
	[reviewDeemedNoSepticScreen] [bit] NOT NULL,
	[reviewDeemedNoOrderUrgent] [bit] NOT NULL,
	[reviewDeemedNoEscalateParent] [bit] NOT NULL,
	[reviewDeemedNoEscalateCriticalCare] [bit] NOT NULL,
	[reviewDeemedNoOther] [bit] NOT NULL,
	[reviewDeemedNoOtherText] [nvarchar](max) NULL,
	[reviewABGabnormal] [nvarchar](255) NULL,
	[reviewABGabnormalTreated] [nvarchar](255) NULL,
	[reviewABGabnormalAcid] [nvarchar](255) NULL,
	[reviewABGfailureRespond] [nvarchar](255) NULL,
	[reviewABGfailureNurse] [nvarchar](255) NULL,
	[reviewABGfailureDr] [nvarchar](255) NULL,
	[reviewABGfailureDelay] [nvarchar](255) NULL,
	[reviewABGfailureDelayTime] [int] NULL,
	[ALSfollowed] [nvarchar](255) NULL,
	[ALSfollowedComments] [nvarchar](max) NULL,
	[reviewLCP] [nvarchar](255) NULL,
	[reviewDNAR] [nvarchar](255) NULL,
	[reviewDNARdelayed] [nvarchar](255) NULL,
	[reviewLevel45] [nvarchar](255) NULL,
	[reviewWardStaffedFully] [nvarchar](255) NULL,
	[reviewStaffedQual] [int] NULL,
	[reviewStaffedCSW] [int] NULL,
	[reviewStaffedAgencyQual] [int] NULL,
	[reviewStaffedAgencyCSW] [int] NULL,
	[ebmB] [nvarchar](255) NULL,
	[ebmB2] [nvarchar](255) NULL,
	[ebmB3] [nvarchar](255) NULL,
	[ebmB4] [nvarchar](255) NULL,
	[ebmB5] [nvarchar](255) NULL,
	[ebmPAI] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
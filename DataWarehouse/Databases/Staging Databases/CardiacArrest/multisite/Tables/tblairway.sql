﻿CREATE TABLE [multisite].[tblairway](
	[AirwayID] [int] NULL,
	[EventID] [int] NULL,
	[AirwayItemID] [int] NULL,
	[AirwaySize] [int] NULL,
	[TrainStaffID] [int] NULL
) ON [PRIMARY]
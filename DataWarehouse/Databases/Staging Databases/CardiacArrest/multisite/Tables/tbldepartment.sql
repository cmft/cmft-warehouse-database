﻿CREATE TABLE [multisite].[tbldepartment](
	[DepartmentID] [int] NULL,
	[DepartmentName] [nvarchar](255) NULL,
	[ParentDepartmentID] [int] NULL,
	[DataEntryID] [int] NULL,
	[NCAAData] [int] NULL,
	[ArrestValidLocation] [bit] NULL
) ON [PRIMARY]
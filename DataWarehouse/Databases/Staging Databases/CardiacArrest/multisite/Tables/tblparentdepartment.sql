﻿CREATE TABLE [multisite].[tblparentdepartment](
	[ParentDepartmentID] [int] NULL,
	[ParentDepartmentName] [nvarchar](255) NULL,
	[DirectorateID] [int] NULL,
	[DataEntryID] [int] NULL
) ON [PRIMARY]
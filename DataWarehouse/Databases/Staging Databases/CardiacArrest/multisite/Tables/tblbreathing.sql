﻿CREATE TABLE [multisite].[tblbreathing](
	[BreathingID] [int] NULL,
	[EventID] [int] NULL,
	[BreathingItemID] [int] NULL,
	[BreathingSize] [int] NULL,
	[TrainStaffID] [int] NULL
) ON [PRIMARY]
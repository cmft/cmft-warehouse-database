﻿CREATE TABLE [multisite].[tbldrugs](
	[DrugID] [int] NULL,
	[EventID] [int] NULL,
	[DrugItemID] [int] NULL,
	[DrugDose] [int] NULL,
	[DrugDoseSizeID] [int] NULL,
	[DrugTotalDoses] [int] NULL
) ON [PRIMARY]
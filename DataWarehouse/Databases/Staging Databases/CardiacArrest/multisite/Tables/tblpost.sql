﻿CREATE TABLE [multisite].[tblpost](
	[PostID] [int] NULL,
	[PostName] [nvarchar](100) NULL,
	[TrainingReq] [bit] NULL,
	[Timestamp] [varbinary](8) NULL,
	[staffgroup] [int] NULL,
	[LevelTraining] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [multisite].[tblitem](
	[ItemID] [int] NULL,
	[ItemTypeID] [int] NULL,
	[ItemName] [nvarchar](255) NULL,
	[DataEntryID] [int] NULL,
	[NumVal1] [smallint] NULL,
	[NumVal2] [nvarchar](255) NULL
) ON [PRIMARY]
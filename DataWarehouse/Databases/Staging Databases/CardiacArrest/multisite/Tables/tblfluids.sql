﻿CREATE TABLE [multisite].[tblfluids](
	[FluidsID] [int] NULL,
	[EventID] [int] NULL,
	[FluidItemID] [int] NULL,
	[FluidAmountTotal] [int] NULL
) ON [PRIMARY]
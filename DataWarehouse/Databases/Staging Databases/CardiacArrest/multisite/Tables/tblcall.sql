﻿CREATE TABLE [multisite].[tblcall](
	[CallID] [int] NULL,
	[CallInfo] [datetime] NULL,
	[DepartmentID] [int] NULL,
	[GeneralLocation] [nvarchar](255) NULL,
	[DataEntryID] [int] NULL,
	[RespondingTeam] [int] NULL,
	[PatientID] [int] NULL,
	[DepartmentFreeText] [nvarchar](255) NULL,
	[CallBacks] [int] NULL,
	[CallExtension] [int] NULL,
	[CallBoardNumber] [int] NULL,
	[OldPatientID] [int] NULL
) ON [PRIMARY]
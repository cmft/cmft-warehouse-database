﻿CREATE TABLE [multisite].[tbldirectorate](
	[DirectorateID] [int] NULL,
	[DirectorateName] [nvarchar](255) NULL,
	[DivisionID] [int] NULL,
	[DataEntryID] [int] NULL
) ON [PRIMARY]
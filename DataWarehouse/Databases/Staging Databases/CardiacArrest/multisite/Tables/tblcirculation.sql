﻿CREATE TABLE [multisite].[tblcirculation](
	[CirculationID] [int] NULL,
	[EventID] [int] NULL,
	[CirculationItemID] [int] NULL,
	[CirculationSize] [int] NULL,
	[DrugID] [int] NULL,
	[FluidID] [int] NULL
) ON [PRIMARY]
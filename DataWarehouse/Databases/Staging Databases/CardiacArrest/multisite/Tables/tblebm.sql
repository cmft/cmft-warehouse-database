﻿CREATE TABLE [multisite].[tblebm](
	[EBMID] [int] NULL,
	[EventID] [int] NULL,
	[SummaryComplete] [bit] NULL,
	[IncidentNumber] [int] NULL,
	[InitialMeeting] [datetime] NULL,
	[InitialMeetingOutcome] [int] NULL,
	[DateOfInvite] [datetime] NULL,
	[ReviewMeeting] [datetime] NULL,
	[EBMOutcome] [ntext] NULL,
	[FeedbackGiven] [bit] NULL,
	[PotentiallyAvoidable] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
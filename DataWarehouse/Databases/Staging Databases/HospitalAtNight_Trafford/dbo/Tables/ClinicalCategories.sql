﻿CREATE TABLE [dbo].[ClinicalCategories](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CinicalCategoryName] [varchar](30) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL CONSTRAINT [DF_ClinicalCategories_Order]  DEFAULT ((0))
) ON [PRIMARY]
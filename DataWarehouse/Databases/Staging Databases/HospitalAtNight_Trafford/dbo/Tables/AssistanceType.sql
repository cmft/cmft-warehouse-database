﻿CREATE TABLE [dbo].[AssistanceType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AssistanceName] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL CONSTRAINT [DF_AssistanceType_Order]  DEFAULT ((0))
) ON [PRIMARY]
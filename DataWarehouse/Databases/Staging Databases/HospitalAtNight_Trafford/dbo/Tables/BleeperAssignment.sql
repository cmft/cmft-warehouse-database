﻿CREATE TABLE [dbo].[BleeperAssignment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[AssignmentDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CallType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
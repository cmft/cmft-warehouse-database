﻿CREATE TABLE [dbo].[ServiceRequestHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ServiceRequestID] [int] NOT NULL,
	[ActionByID] [int] NOT NULL,
	[ActionTimeStamp] [datetime] NOT NULL,
	[ActionText] [varchar](max) NOT NULL,
	[ClinicalStaffID] [int] NULL,
	[WorkGroupID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
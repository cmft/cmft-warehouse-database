﻿CREATE TABLE [dbo].[WorkGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TeamName] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
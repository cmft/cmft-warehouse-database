﻿CREATE TABLE [dbo].[WINDOWPOSITION](
	[WINDOWNAME] [varchar](25) NOT NULL,
	[USERNAME] [varchar](25) NOT NULL,
	[W_LEFT] [numeric](4, 2) NULL,
	[TOP] [numeric](4, 2) NULL,
	[RESOLUTION] [int] NULL
) ON [PRIMARY]
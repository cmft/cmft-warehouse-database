﻿CREATE TABLE [dbo].[RM_BREAKAGE](
	[GUID] [varchar](36) NOT NULL,
	[REPAIR_GUID] [varchar](36) NOT NULL,
	[NAME] [varchar](250) NULL,
	[REPAIR_TYPE_GUID] [varchar](36) NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[DESCRIPTION_EXT] [varchar](50) NULL,
	[NOTE] [varchar](100) NULL,
	[ARTICLE_ID] [int] NULL,
	[QUANTITY] [numeric](6, 2) NULL
) ON [PRIMARY]
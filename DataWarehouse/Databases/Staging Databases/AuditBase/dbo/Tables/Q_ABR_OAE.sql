﻿CREATE TABLE [dbo].[Q_ABR_OAE](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[TE_DP] [smallint] NULL,
	[CL_ABR_R_RESP] [smallint] NULL,
	[CL_ABR_R_SIGN] [smallint] NULL,
	[CL_ABR_L_RESP] [smallint] NULL,
	[CL_ABR_R_VAL] [int] NULL,
	[CL_ABR_L_SIGN] [smallint] NULL,
	[TP05_R_RESP] [smallint] NULL,
	[CL_ABR_L_VAL] [int] NULL,
	[TP05_R_SIGN] [smallint] NULL,
	[TP05_L_RESP] [smallint] NULL,
	[TP05_R_VAL] [int] NULL,
	[TP05_L_SIGN] [smallint] NULL,
	[TP1_R_RESP] [smallint] NULL,
	[TP05_L_VAL] [int] NULL,
	[TP1_R_SIGN] [smallint] NULL,
	[TP1_L_RESP] [smallint] NULL,
	[TP1_R_VAL] [int] NULL,
	[TP1_L_SIGN] [smallint] NULL,
	[TP2_R_RESP] [smallint] NULL,
	[TP1_L_VAL] [int] NULL,
	[TP2_R_SIGN] [smallint] NULL,
	[TP2_L_RESP] [smallint] NULL,
	[TP2_R_VAL] [int] NULL,
	[TP2_L_SIGN] [smallint] NULL,
	[TP3_R_RESP] [smallint] NULL,
	[TP2_L_VAL] [int] NULL,
	[TP3_R_SIGN] [smallint] NULL,
	[TP3_L_RESP] [smallint] NULL,
	[TP3_R_VAL] [int] NULL,
	[TP3_L_SIGN] [smallint] NULL,
	[TP4_R_RESP] [smallint] NULL,
	[TP3_L_VAL] [int] NULL,
	[TP4_R_SIGN] [smallint] NULL,
	[TP4_L_RESP] [smallint] NULL,
	[TP4_R_VAL] [int] NULL,
	[TP4_L_SIGN] [smallint] NULL,
	[BONEC_R_RESP] [smallint] NULL,
	[TP4_L_VAL] [int] NULL,
	[BONEC_R_SIGN] [smallint] NULL,
	[BONEC_L_RESP] [smallint] NULL,
	[BONEC_R_VAL] [int] NULL,
	[BONEC_L_SIGN] [smallint] NULL,
	[CMICR_R_RESP] [smallint] NULL,
	[BONEC_L_VAL] [int] NULL,
	[CMICR_L_RESP] [smallint] NULL,
	[CMICR_R_VAL] [int] NULL,
	[OAE_R_REC] [smallint] NULL,
	[CMICR_L_VAL] [int] NULL,
	[OAE_L_REC] [smallint] NULL,
	[CERA05_R_RESP] [smallint] NULL,
	[CERA05_R_SIGN] [smallint] NULL,
	[CERA05_L_RESP] [smallint] NULL,
	[CERA05_R_VAL] [int] NULL,
	[CERA05_L_SIGN] [smallint] NULL,
	[CERA1_R_RESP] [smallint] NULL,
	[CERA05_L_VAL] [int] NULL,
	[CERA1_R_SIGN] [smallint] NULL,
	[CERA1_L_RESP] [smallint] NULL,
	[CERA1_R_VAL] [int] NULL,
	[CERA1_L_SIGN] [smallint] NULL,
	[CERA2_R_RESP] [smallint] NULL,
	[CERA1_L_VAL] [int] NULL,
	[CERA2_R_SIGN] [smallint] NULL,
	[CERA2_L_RESP] [smallint] NULL,
	[CERA2_R_VAL] [int] NULL,
	[CERA2_L_SIGN] [smallint] NULL,
	[CERA3_R_RESP] [smallint] NULL,
	[CERA2_L_VAL] [int] NULL,
	[CERA3_R_SIGN] [smallint] NULL,
	[CERA3_L_RESP] [smallint] NULL,
	[CERA3_R_VAL] [int] NULL,
	[CERA3_L_SIGN] [smallint] NULL,
	[CERA4_R_RESP] [smallint] NULL,
	[CERA3_L_VAL] [int] NULL,
	[CERA4_R_SIGN] [smallint] NULL,
	[CERA4_L_RESP] [smallint] NULL,
	[CERA4_R_VAL] [int] NULL,
	[CERA4_L_SIGN] [smallint] NULL,
	[NR] [smallint] NOT NULL,
	[CERA4_L_VAL] [int] NULL,
	[TEST_TYPE] [smallint] NULL,
	[MACHINE_VAL] [smallint] NULL,
	[INPATIENT_VAL] [smallint] NULL,
	[SCREENER_USER_ID] [int] NULL,
	[SCREENER_AGENT_ID] [int] NULL,
	[ONWARD_REFER_VAL] [smallint] NULL,
	[AUTO_ABR_R_VAL] [smallint] NULL,
	[AUTO_ABR_L_VAL] [smallint] NULL,
	[PROTOCOL_VAL] [smallint] NULL
) ON [PRIMARY]
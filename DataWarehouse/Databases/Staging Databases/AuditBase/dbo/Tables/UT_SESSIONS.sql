﻿CREATE TABLE [dbo].[UT_SESSIONS](
	[ID] [int] NOT NULL,
	[USER_ID] [int] NOT NULL,
	[COMPUTER_NAME] [varchar](100) NOT NULL,
	[LOGINTIME] [datetime2](7) NOT NULL
) ON [PRIMARY]
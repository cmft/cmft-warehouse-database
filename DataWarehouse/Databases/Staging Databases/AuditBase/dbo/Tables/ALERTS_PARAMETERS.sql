﻿CREATE TABLE [dbo].[ALERTS_PARAMETERS](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[CODE] [varchar](10) NULL,
	[NAME] [varchar](100) NOT NULL,
	[IS_ALERT] [smallint] NOT NULL,
	[PRIORITY] [smallint] NOT NULL,
	[OUTDATED] [smallint] NOT NULL
) ON [PRIMARY]
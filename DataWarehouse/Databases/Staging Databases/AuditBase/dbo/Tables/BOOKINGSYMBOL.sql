﻿CREATE TABLE [dbo].[BOOKINGSYMBOL](
	[GUID] [varchar](36) NOT NULL,
	[ID] [int] NOT NULL,
	[NAME] [varchar](60) NOT NULL,
	[BITMAP] [text] NULL,
	[PRIORITY] [smallint] NULL,
	[BOOKING_TYPE] [smallint] NOT NULL,
	[DEFAULT_DURATION] [int] NULL,
	[USER_ID] [int] NULL,
	[BOOKING_LOCATION] [varchar](50) NULL,
	[LOCATION_GUID] [varchar](36) NULL,
	[OUTDATED] [smallint] NOT NULL,
	[IS_PATHWAY_SYMBOL] [smallint] NOT NULL,
	[SMS1_DAYS] [smallint] NULL,
	[SMS1_INCL_WEEKEND] [smallint] NOT NULL,
	[SMS2_DAYS] [smallint] NULL,
	[SMS2_INCL_WEEKEND] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
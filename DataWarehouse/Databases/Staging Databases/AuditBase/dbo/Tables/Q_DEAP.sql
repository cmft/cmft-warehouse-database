﻿CREATE TABLE [dbo].[Q_DEAP](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[NOT_ADMINISTERED] [smallint] NOT NULL,
	[NR] [smallint] NOT NULL,
	[CONSONANTS] [smallint] NULL,
	[VOWELS] [smallint] NULL,
	[PHONEMES] [smallint] NULL,
	[NOTES] [varchar](254) NULL,
	[NOT_ADMIN_ID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Q_LAT](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[NOT_ADMINISTERED] [smallint] NOT NULL,
	[NR] [smallint] NOT NULL,
	[HI_STATUS_L_ID] [smallint] NULL,
	[HI_STATUS_R_ID] [smallint] NULL,
	[NUM_OF_TRIALS] [smallint] NULL,
	[AVG_MEEN_ERR] [smallint] NULL,
	[NOT_ADMIN_ID] [int] NULL
) ON [PRIMARY]
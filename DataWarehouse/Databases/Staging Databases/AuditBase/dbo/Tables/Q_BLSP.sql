﻿CREATE TABLE [dbo].[Q_BLSP](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[NOT_ADMINISTERED] [smallint] NOT NULL,
	[NR] [smallint] NOT NULL,
	[COM_MODE_ID] [int] NULL,
	[TOTAL_QUESTIONS] [smallint] NULL,
	[TOTAL_SCORE] [smallint] NULL,
	[NOT_ADMIN_ID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Q_RDLS](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[NOT_ADMINISTERED] [smallint] NOT NULL,
	[NR] [smallint] NOT NULL,
	[RAW_SCORE] [smallint] NULL,
	[STD_SCORE] [smallint] NULL,
	[PERCENT_RANK] [smallint] NULL,
	[AGE_EQUIV_CMP] [smallint] NOT NULL,
	[AGE_EQUIV_Y] [smallint] NULL,
	[AGE_EQUIV_M] [smallint] NULL,
	[AGE_EQUIV_DM] [smallint] NULL,
	[NOTES] [varchar](254) NULL,
	[NOT_ADMIN_ID] [int] NULL
) ON [PRIMARY]
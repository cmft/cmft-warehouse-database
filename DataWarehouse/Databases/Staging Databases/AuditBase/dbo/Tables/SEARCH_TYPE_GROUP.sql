﻿CREATE TABLE [dbo].[SEARCH_TYPE_GROUP](
	[SEARCH_GROUP_GUID] [varchar](36) NOT NULL,
	[SEARCH_TYPE_GUID] [varchar](36) NOT NULL,
	[PRIORITY] [int] NOT NULL
) ON [PRIMARY]
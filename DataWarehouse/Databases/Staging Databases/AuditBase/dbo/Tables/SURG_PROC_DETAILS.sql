﻿CREATE TABLE [dbo].[SURG_PROC_DETAILS](
	[ID] [int] NOT NULL,
	[NAME] [varchar](60) NOT NULL,
	[PRIORITY] [smallint] NULL,
	[OUTDATED] [smallint] NOT NULL
) ON [PRIMARY]
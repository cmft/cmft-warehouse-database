﻿CREATE TABLE [dbo].[TASK_TYPE](
	[ID] [int] NOT NULL,
	[NAME] [varchar](30) NOT NULL,
	[DESCRIPTION] [varchar](254) NULL,
	[PRIORITY] [smallint] NULL,
	[OUTDATED] [smallint] NOT NULL
) ON [PRIMARY]
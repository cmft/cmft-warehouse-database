﻿CREATE TABLE [dbo].[Q_AB_WORDS](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[NOT_ADMINISTERED] [smallint] NOT NULL,
	[LIST2_USED] [int] NULL,
	[TEST_COND_ID] [int] NULL,
	[HI_STATUS_L_ID] [int] NULL,
	[HI_STATUS_R_ID] [int] NULL,
	[LIST_USED] [int] NULL,
	[SENTENCES_PRESENT] [int] NULL,
	[SENTENCES_CORRECT] [int] NULL,
	[SENTENCES_COR_PER] [int] NULL,
	[KEYWORD_PRESENT] [int] NULL,
	[KEYWORD_CORRECT] [int] NULL,
	[KEYWORD_COR_PER] [int] NULL,
	[NOTES] [varchar](254) NULL,
	[SOUND_CONDITION] [smallint] NULL,
	[NOISE_LEVEL_ID] [int] NULL,
	[NOISE_TYPE_ID] [int] NULL,
	[PRESENT_LEVEL_ID] [int] NULL,
	[EAR_SIDE] [smallint] NULL,
	[NOT_ADMIN_ID] [int] NULL,
	[NR] [smallint] NOT NULL
) ON [PRIMARY]
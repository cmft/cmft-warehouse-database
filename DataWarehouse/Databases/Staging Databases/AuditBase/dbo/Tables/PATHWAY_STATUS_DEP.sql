﻿CREATE TABLE [dbo].[PATHWAY_STATUS_DEP](
	[ID] [int] NOT NULL,
	[BOOKING_SYMBOL_ID] [int] NOT NULL,
	[APPOINTMENT_STATUS] [varchar](1) NULL,
	[PATHWAY_STATUS_ID] [int] NOT NULL
) ON [PRIMARY]
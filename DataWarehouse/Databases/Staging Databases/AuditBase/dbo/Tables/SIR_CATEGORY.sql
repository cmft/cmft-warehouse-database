﻿CREATE TABLE [dbo].[SIR_CATEGORY](
	[ID] [int] NOT NULL,
	[NAME] [varchar](60) NOT NULL,
	[DESCRIPTION] [varchar](254) NULL,
	[PRIORITY] [smallint] NULL,
	[OUTDATED] [smallint] NOT NULL
) ON [PRIMARY]
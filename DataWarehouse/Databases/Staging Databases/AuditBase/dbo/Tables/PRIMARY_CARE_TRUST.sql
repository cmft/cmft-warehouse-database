﻿CREATE TABLE [dbo].[PRIMARY_CARE_TRUST](
	[ID] [int] NOT NULL,
	[CODE] [varchar](10) NULL,
	[NAME] [varchar](248) NOT NULL,
	[CUSTOMER_ID] [int] NULL,
	[USER_ID] [int] NULL,
	[OTHER_ID] [int] NULL
) ON [PRIMARY]
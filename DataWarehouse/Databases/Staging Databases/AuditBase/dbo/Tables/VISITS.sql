﻿CREATE TABLE [dbo].[VISITS](
	[GUID] [varchar](36) NOT NULL,
	[VISIT_ID] [int] NOT NULL,
	[CALENDAR_ID] [int] NOT NULL,
	[CAREPLAN] [varchar](250) NULL,
	[COLDS] [varchar](60) NULL,
	[OTALGIA] [varchar](60) NULL,
	[OTORRHOEA] [varchar](60) NULL,
	[AETIOLOGY] [varchar](60) NULL,
	[OTOSCOPY] [varchar](60) NULL,
	[GENERALDEV] [smallint] NULL,
	[SNOREMOUTHBREATHE] [varchar](60) NULL,
	[SPEECHDEV] [smallint] NULL,
	[SCHOOLADVICE] [smallint] NULL,
	[INFOTOFAMILY] [varchar](120) NULL,
	[OPERATION] [varchar](60) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RM_ORDER_TYPE](
	[GUID] [varchar](36) NOT NULL,
	[NAME] [varchar](40) NULL,
	[MIN_NUMBER] [int] NULL,
	[MAX_NUMBER] [int] NULL,
	[FROM_STOCK_GUID] [varchar](36) NULL,
	[TO_STOCK_GUID] [varchar](36) NULL,
	[FROM_STOCK] [int] NULL,
	[TO_STOCK] [int] NULL,
	[LOCATION_GUID] [varchar](36) NULL,
	[IS_DEVICE_REQUIRED] [numeric](1, 0) NULL,
	[COMPLETED_IMMED] [smallint] NOT NULL,
	[SET_TECHNICIAN] [smallint] NOT NULL
) ON [PRIMARY]
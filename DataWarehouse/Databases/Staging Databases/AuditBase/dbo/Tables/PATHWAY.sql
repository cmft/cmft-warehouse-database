﻿CREATE TABLE [dbo].[PATHWAY](
	[GUID] [varchar](36) NOT NULL,
	[PATHWAY_ID] [varchar](10) NOT NULL,
	[PRIMARYCODE] [varchar](20) NULL,
	[SECONDARYCODE] [varchar](20) NULL,
	[STATE] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TASK_LABEL](
	[GUID] [varchar](36) NOT NULL,
	[NAME] [varchar](30) NULL,
	[DESCRIPTION] [varchar](254) NULL,
	[TASK_TYPE_ID] [int] NULL,
	[PRIORITY] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UT_PACKAGE](
	[ID] [varchar](16) NOT NULL,
	[DB_VERSION] [int] NOT NULL,
	[DESCRIPTION] [varchar](250) NULL,
	[REQUIRED] [varchar](1) NOT NULL,
	[MAINTENANCE] [varchar](1) NOT NULL,
	[DOCUMENT] [smallint] NOT NULL
) ON [PRIMARY]
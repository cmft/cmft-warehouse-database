﻿CREATE TABLE [dbo].[RM_WORKFLOW](
	[GUID] [varchar](36) NOT NULL,
	[REPAIR_GUID] [varchar](36) NOT NULL,
	[INVOICE_GUID] [varchar](36) NULL,
	[NOTE] [varchar](100) NULL
) ON [PRIMARY]
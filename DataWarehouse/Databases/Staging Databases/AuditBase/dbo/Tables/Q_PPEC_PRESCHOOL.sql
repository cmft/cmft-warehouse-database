﻿CREATE TABLE [dbo].[Q_PPEC_PRESCHOOL](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[NOT_ADMINISTERED] [smallint] NOT NULL,
	[NR] [smallint] NOT NULL,
	[OTHER_LNG_ID] [int] NULL,
	[ATTENT_DIR_SL] [smallint] NULL,
	[ATTENT_DIR_OL] [smallint] NULL,
	[REQUESTING_SL] [smallint] NULL,
	[REQUESTING_OL] [smallint] NULL,
	[REJECTING_SL] [smallint] NULL,
	[REJECTING_OL] [smallint] NULL,
	[GREETING_SL] [smallint] NULL,
	[GREETING_OL] [smallint] NULL,
	[SELF_EXPRESS_SL] [smallint] NULL,
	[SELF_EXPRESS_OL] [smallint] NULL,
	[NAMING_SL] [smallint] NULL,
	[NAMING_OL] [smallint] NULL,
	[COMMENTING_SL] [smallint] NULL,
	[COMMENTING_OL] [smallint] NULL,
	[GIVING_INFO_SL] [smallint] NULL,
	[GIVING_INFO_OL] [smallint] NULL,
	[GAIN_ATTENT_SL] [smallint] NULL,
	[GAIN_ATTENT_OL] [smallint] NULL,
	[INTEREST_SL] [smallint] NULL,
	[INTEREST_OL] [smallint] NULL,
	[GESTURE_SL] [smallint] NULL,
	[GESTURE_OL] [smallint] NULL,
	[UTTERANCE_SL] [smallint] NULL,
	[UTTERANCE_OL] [smallint] NULL,
	[INTENTIONS_SL] [smallint] NULL,
	[INTENTIONS_OL] [smallint] NULL,
	[ANTICIPATION_SL] [smallint] NULL,
	[ANTICIPATION_OL] [smallint] NULL,
	[AMUSEMENT_SL] [smallint] NULL,
	[AMUSEMENT_OL] [smallint] NULL,
	[NEGOTIATIONS_SL] [smallint] NULL,
	[NEGOTIATIONS_OL] [smallint] NULL,
	[PARTICIPATING_SL] [smallint] NULL,
	[PARTICIPATING_OL] [smallint] NULL,
	[INITIATING_SL] [smallint] NULL,
	[INITIATING_OL] [smallint] NULL,
	[INTERACTION_SL] [smallint] NULL,
	[INTERACTION_OL] [smallint] NULL,
	[BREAKDOWN_SL] [smallint] NULL,
	[BREAKDOWN_OL] [smallint] NULL,
	[REPAIR_SL] [smallint] NULL,
	[REPAIR_OL] [smallint] NULL,
	[CLAR_REQUEST_SL] [smallint] NULL,
	[CLAR_REQUEST_OL] [smallint] NULL,
	[TERMINATING_SL] [smallint] NULL,
	[TERMINATING_OL] [smallint] NULL,
	[OVERHEATING_SL] [smallint] NULL,
	[OVERHEATING_OL] [smallint] NULL,
	[JOINING_SL] [smallint] NULL,
	[JOINING_OL] [smallint] NULL,
	[ESTABLISHED_SL] [smallint] NULL,
	[DEVELOPING_SL] [smallint] NULL,
	[NOT_RECORDED_SL] [smallint] NULL,
	[ESTABLISHED_OL] [smallint] NULL,
	[DEVELOPING_OL] [smallint] NULL,
	[NOT_RECORDED_OL] [smallint] NULL,
	[NOTES] [varchar](254) NULL,
	[NOT_ADMIN_ID] [int] NULL
) ON [PRIMARY]
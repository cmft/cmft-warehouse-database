﻿CREATE TABLE [dbo].[RM_WORKFLOW_ITEM](
	[GUID] [varchar](36) NOT NULL,
	[WORKFLOW_GUID] [varchar](36) NOT NULL,
	[ARTICLE_ID] [int] NOT NULL,
	[ARTICLE_GROUP] [varchar](250) NULL,
	[COMPONENT] [varchar](50) NULL,
	[DETAILS] [varchar](50) NULL,
	[REPAIR_TYPE_GUID] [varchar](36) NULL,
	[PRICE] [numeric](12, 4) NULL,
	[NOTE] [varchar](100) NULL,
	[WORKSHOP] [varchar](40) NULL,
	[EXP_RETURN_DATE] [date] NULL,
	[QUANTITY] [numeric](6, 2) NULL,
	[STOCK_TRANSAC_ID] [int] NULL
) ON [PRIMARY]
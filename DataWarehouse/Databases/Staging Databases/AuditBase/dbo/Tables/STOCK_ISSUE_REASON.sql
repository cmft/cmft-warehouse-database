﻿CREATE TABLE [dbo].[STOCK_ISSUE_REASON](
	[ID] [int] NOT NULL,
	[CODE] [varchar](10) NULL,
	[NAME] [varchar](40) NOT NULL,
	[PRIORITY] [smallint] NULL,
	[OUTDATED] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RM_REPAIR_TYPE](
	[GUID] [varchar](36) NOT NULL,
	[NAME] [varchar](40) NOT NULL,
	[DISCOUNT] [numeric](12, 4) NULL
) ON [PRIMARY]
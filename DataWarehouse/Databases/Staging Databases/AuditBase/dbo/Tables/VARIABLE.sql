﻿CREATE TABLE [dbo].[VARIABLE](
	[ID] [int] NOT NULL,
	[GROUP_ID] [int] NOT NULL,
	[NAME] [varchar](140) NOT NULL,
	[DESCRIPTION] [varchar](250) NULL,
	[DATATYPE] [smallint] NOT NULL,
	[BIND_NAME] [varchar](30) NULL
) ON [PRIMARY]
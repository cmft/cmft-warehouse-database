﻿CREATE TABLE [dbo].[STOCK_INVOICETYPE](
	[NAME] [varchar](250) NULL,
	[ID] [int] NOT NULL,
	[NR5] [int] NULL,
	[NR6] [int] NULL,
	[USERID] [int] NULL,
	[ISSUE_REASON_REQ] [smallint] NOT NULL,
	[DEPARTMENT_CODE] [varchar](6) NULL,
	[CONTACT_PERSON] [varchar](35) NULL,
	[ACCOUNT_NUMBER] [varchar](4) NULL
) ON [PRIMARY]
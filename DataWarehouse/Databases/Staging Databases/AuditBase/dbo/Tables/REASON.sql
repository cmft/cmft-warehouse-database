﻿CREATE TABLE [dbo].[REASON](
	[GUID] [varchar](36) NOT NULL,
	[REASON_ID] [int] NOT NULL,
	[NAME] [varchar](250) NULL,
	[TYPE] [int] NOT NULL,
	[PRIORITY] [int] NOT NULL
) ON [PRIMARY]
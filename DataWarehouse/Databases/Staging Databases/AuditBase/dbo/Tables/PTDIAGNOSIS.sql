﻿CREATE TABLE [dbo].[PTDIAGNOSIS](
	[USERID] [numeric](5, 0) NULL,
	[DIAGNOSIS_ID] [varchar](12) NOT NULL,
	[APPOINTMENT_ID] [int] NULL,
	[EARSIDE] [varchar](1) NULL,
	[CREATE_DATE] [datetime2](7) NOT NULL,
	[COMMENTS] [varchar](100) NULL,
	[PATIENT_ID] [int] NULL,
	[EXTRACODE] [varchar](2) NULL,
	[ORDER_ID] [int] NULL
) ON [PRIMARY]
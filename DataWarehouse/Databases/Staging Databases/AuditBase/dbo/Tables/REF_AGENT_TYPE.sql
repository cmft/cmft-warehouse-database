﻿CREATE TABLE [dbo].[REF_AGENT_TYPE](
	[ID] [int] NOT NULL,
	[NAME] [varchar](100) NULL,
	[AGENT_KIND] [smallint] NOT NULL,
	[IS_SYSTEM] [smallint] NOT NULL,
	[PRIORITY] [smallint] NULL
) ON [PRIMARY]
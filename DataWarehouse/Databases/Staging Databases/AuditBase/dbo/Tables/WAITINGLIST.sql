﻿CREATE TABLE [dbo].[WAITINGLIST](
	[ORDER_ID] [numeric](8, 0) NULL,
	[CREATEDATE] [datetime2](7) NOT NULL,
	[WAITINGLIST_ID] [numeric](8, 0) NOT NULL,
	[REASON_ID] [int] NULL,
	[MAXWAITINGTIME] [numeric](4, 0) NULL,
	[USERID] [numeric](5, 0) NULL,
	[IS_VISITATED] [smallint] NULL,
	[GUID] [varchar](36) NOT NULL,
	[PATIENT_ID] [int] NULL,
	[IS_LETTER_PRINTED] [smallint] NOT NULL,
	[ID] [int] NOT NULL,
	[PATHWAY_ID] [varchar](25) NULL,
	[OTHER_DATE] [date] NULL,
	[INFORMATION] [varchar](250) NULL,
	[INFORMATION_PART2] [varchar](250) NULL,
	[INFORMATION_PART3] [varchar](250) NULL,
	[INFORMATION_PART4] [varchar](250) NULL
) ON [PRIMARY]
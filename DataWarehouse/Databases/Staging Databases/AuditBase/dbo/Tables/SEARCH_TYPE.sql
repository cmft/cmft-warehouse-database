﻿CREATE TABLE [dbo].[SEARCH_TYPE](
	[GUID] [varchar](36) NOT NULL,
	[SEARCHTYPE_NAME] [varchar](50) NOT NULL,
	[JOURNAL_TYPE] [smallint] NULL,
	[PRIORITY] [int] NULL,
	[SITE_GUID] [varchar](36) NULL,
	[IS_GROUP] [smallint] NOT NULL,
	[OBSOLETE] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RM_SUB_STATUS](
	[GUID] [varchar](36) NOT NULL,
	[STATUS_ID] [int] NULL,
	[NAME] [varchar](40) NOT NULL,
	[DESCRIPTION] [varchar](40) NULL,
	[NOTE] [varchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SUPPLIER](
	[SUPPLIER_ID] [varchar](10) NOT NULL,
	[NAME] [varchar](60) NULL,
	[PRIORITY] [numeric](3, 0) NULL,
	[ADDRESS1] [varchar](36) NULL,
	[ADDRESS2] [varchar](36) NULL,
	[ADDRESS3] [varchar](36) NULL,
	[ADDRESS4] [varchar](36) NULL,
	[CONTACTPERSON] [varchar](24) NULL,
	[PHONE] [varchar](16) NULL,
	[FAX] [varchar](16) NULL,
	[USERID] [numeric](5, 0) NULL,
	[FACETTID] [varchar](12) NULL,
	[ACCOUNTNUMBER] [numeric](5, 0) NULL,
	[EMAIL] [varchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Q_IST](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[NOT_ADMINISTERED] [smallint] NOT NULL,
	[NR] [smallint] NOT NULL,
	[SPEECH] [smallint] NULL,
	[RECEPTIVE_LNG] [smallint] NULL,
	[EXPRESSIVE_LNG] [smallint] NULL,
	[PRAGMATICS] [smallint] NULL,
	[LISTENING] [smallint] NULL,
	[NOT_ADMIN_ID] [int] NULL
) ON [PRIMARY]
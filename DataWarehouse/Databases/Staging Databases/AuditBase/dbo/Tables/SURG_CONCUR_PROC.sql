﻿CREATE TABLE [dbo].[SURG_CONCUR_PROC](
	[ID] [int] NOT NULL,
	[NAME] [varchar](100) NOT NULL,
	[PRIORITY] [smallint] NULL,
	[OUTDATED] [smallint] NOT NULL,
	[CODE] [varchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[STOCK_ARTICLETYPE](
	[PAASLAG] [int] NULL,
	[DISCOUNT] [int] NULL,
	[USERID] [int] NULL,
	[PRIORITY] [int] NULL,
	[NAME] [varchar](250) NULL,
	[ID] [int] NULL
) ON [PRIMARY]
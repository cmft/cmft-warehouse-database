﻿CREATE TABLE [dbo].[Q_OPTIONS](
	[TYPE] [smallint] NULL,
	[OPT_CODE] [smallint] NULL,
	[VALUE] [smallint] NULL,
	[DESCRIPTION] [varchar](40) NULL
) ON [PRIMARY]
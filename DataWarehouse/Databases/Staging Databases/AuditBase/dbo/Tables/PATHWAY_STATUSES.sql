﻿CREATE TABLE [dbo].[PATHWAY_STATUSES](
	[ID] [int] NOT NULL,
	[OUTDATED] [smallint] NOT NULL,
	[DESCRIPTION] [varchar](250) NULL,
	[ACTION] [smallint] NOT NULL,
	[CODE] [varchar](10) NOT NULL,
	[REPORTING_CODE] [varchar](10) NULL,
	[NAME] [varchar](50) NULL,
	[PRIORITY] [int] NULL,
	[IS_OUTCOME] [smallint] NOT NULL,
	[IS_STATUS] [smallint] NOT NULL,
	[AUTO_STATUS_ID] [int] NULL,
	[AUTO_STATUS_ORDER] [smallint] NOT NULL,
	[IS_STATUS_WL] [smallint] NOT NULL,
	[AUTO_STATUS_DTDIF] [smallint] NULL
) ON [PRIMARY]
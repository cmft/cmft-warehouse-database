﻿CREATE TABLE [dbo].[MANUFACTURER](
	[ARTICLE_GROUP] [varchar](5) NULL,
	[MANUFACT_ID] [varchar](5) NULL,
	[NAME] [varchar](50) NULL,
	[PRIORITY] [numeric](3, 0) NULL,
	[USERID] [numeric](5, 0) NULL,
	[USE_LETTERS_IN_SN] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SITE](
	[GUID] [varchar](36) NOT NULL,
	[SITE_NAME] [varchar](50) NOT NULL,
	[ADDRESS] [varchar](250) NULL,
	[IN_ACTIVE] [smallint] NULL,
	[PHONE] [varchar](20) NULL,
	[PARENT_SITE_GUID] [varchar](36) NULL,
	[PRIORITY] [int] NULL,
	[IMPORT_CONFIG_GUID] [varchar](36) NULL,
	[EXPORT_CONFIG_GUID] [varchar](36) NULL,
	[NOT_PARTNER] [smallint] NOT NULL
) ON [PRIMARY]
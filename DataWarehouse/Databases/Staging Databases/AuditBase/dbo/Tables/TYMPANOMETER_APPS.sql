﻿CREATE TABLE [dbo].[TYMPANOMETER_APPS](
	[ID] [int] NOT NULL,
	[NAME] [varchar](30) NOT NULL,
	[PRIORITY] [smallint] NULL,
	[NOAH_MODULE_ID] [int] NULL
) ON [PRIMARY]
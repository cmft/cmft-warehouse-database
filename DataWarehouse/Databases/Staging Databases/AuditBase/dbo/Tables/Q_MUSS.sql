﻿CREATE TABLE [dbo].[Q_MUSS](
	[ID] [int] NOT NULL,
	[GUID] [varchar](36) NULL,
	[PATIENT_ID] [int] NOT NULL,
	[USER_ID] [int] NULL,
	[APPOINTMENT_ID] [int] NULL,
	[CREATE_DATE] [date] NOT NULL,
	[CURR_VERSION] [smallint] NOT NULL,
	[NOT_ADMINISTERED] [smallint] NOT NULL,
	[NR] [smallint] NOT NULL,
	[ITMAIS_CATEG_ID] [int] NULL,
	[NOTES] [text] NULL,
	[TOTAL_SCORE] [numeric](10, 2) NULL,
	[TOTAL_SCORE_PER] [smallint] NULL,
	[NOT_ADMIN_ID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
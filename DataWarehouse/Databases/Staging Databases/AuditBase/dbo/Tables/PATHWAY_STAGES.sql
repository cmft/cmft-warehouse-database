﻿CREATE TABLE [dbo].[PATHWAY_STAGES](
	[ID] [int] NOT NULL,
	[NAME] [varchar](15) NOT NULL,
	[DESCRIPTION] [varchar](100) NULL,
	[IS_ON_OVERVIEW] [smallint] NOT NULL,
	[PRIORITY] [smallint] NOT NULL,
	[IS_TREAT_STAGE] [smallint] NOT NULL
) ON [PRIMARY]
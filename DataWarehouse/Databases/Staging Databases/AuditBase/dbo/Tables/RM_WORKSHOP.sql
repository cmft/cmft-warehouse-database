﻿CREATE TABLE [dbo].[RM_WORKSHOP](
	[GUID] [varchar](36) NOT NULL,
	[NAME] [varchar](40) NULL,
	[PRIORITY] [smallint] NOT NULL
) ON [PRIMARY]
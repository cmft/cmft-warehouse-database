﻿CREATE TABLE [dbo].[VMODHIST](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[REGIME] [varchar](16) NOT NULL,
	[CYCLE] [numeric](3, 0) NOT NULL,
	[DAYNO] [numeric](3, 0) NOT NULL,
	[DRUGNAME] [varchar](30) NOT NULL,
	[REASON] [varchar](6) NULL CONSTRAINT [DF_VMODHIST_REASON]  DEFAULT (' '),
	[ACTION] [text] NULL,
	[OLDDOSE] [numeric](7, 2) NULL,
	[NEWDOSE] [numeric](7, 2) NULL,
	[APPT_DATE] [datetime] NOT NULL,
	[USER1] [varchar](12) NULL,
	[MODDATE] [datetime] NOT NULL,
	[REG_CYC] [numeric](3, 0) NULL,
	[UNIQ_KEY] [numeric](10, 0) NOT NULL,
	[POST_TREAT] [bit] NOT NULL,
	[DISPLAYNO] [numeric](3, 0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
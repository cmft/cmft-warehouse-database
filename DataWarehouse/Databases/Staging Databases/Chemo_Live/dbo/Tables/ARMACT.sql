﻿CREATE TABLE [dbo].[ARMACT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ARMID] [varchar](10) NOT NULL,
	[CYCLENAME] [varchar](16) NULL,
	[ACTID] [varchar](1) NOT NULL,
	[PROTID] [varchar](10) NOT NULL,
	[ORDERNUM] [int] NULL,
	[DISPLAYNO] [numeric](3, 0) NULL
) ON [PRIMARY]
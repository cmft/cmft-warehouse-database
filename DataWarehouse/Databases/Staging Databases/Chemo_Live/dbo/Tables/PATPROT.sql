﻿CREATE TABLE [dbo].[PATPROT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[PROTID] [varchar](10) NOT NULL,
	[ARMID] [varchar](10) NOT NULL,
	[DPDATE] [datetime] NULL,
	[STATUS] [varchar](1) NULL,
	[GIVEPROTID] [varchar](10) NULL
) ON [PRIMARY]
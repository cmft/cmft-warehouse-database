﻿CREATE TABLE [dbo].[WORKSUM](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DAYNO] [numeric](3, 0) NULL,
	[TEXT] [varchar](25) NULL,
	[HASTEST] [bit] NULL,
	[HASDRUG] [bit] NULL,
	[HASREVIEW] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_WS_Select_Criteria_Drugs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SelectCriteria_Id] [uniqueidentifier] NOT NULL,
	[Drug_Or_Addi] [varchar](60) NOT NULL,
	[FinalConc_Low] [decimal](9, 2) NULL,
	[FinalConc_High] [decimal](9, 2) NULL,
	[Dose_Low] [decimal](9, 2) NULL,
	[Dose_High] [decimal](9, 2) NULL,
	[PrepMap] [nvarchar](50) NULL
) ON [PRIMARY]
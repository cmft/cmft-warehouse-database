﻿CREATE TABLE [dbo].[ARM](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID1] [varchar](10) NOT NULL,
	[PROTID] [varchar](10) NOT NULL,
	[TEXT] [varchar](30) NULL,
	[DESCRIPT] [text] NULL,
	[TYPE] [varchar](1) NULL,
	[ELIGSHORT] [varchar](40) NULL,
	[ELIGLONG] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
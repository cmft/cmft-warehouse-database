﻿CREATE TABLE [dbo].[Tbl_WS_Select_Criteria](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SelectCriteria_Id] [uniqueidentifier] NOT NULL,
	[Template_Id] [uniqueidentifier] NULL,
	[Route] [varchar](30) NULL,
	[Vehicle] [varchar](60) NULL,
	[Container] [varchar](60) NULL,
	[Description] [varchar](100) NULL,
	[Regimen] [varchar](60) NULL,
	[Veh_Vol_Low] [decimal](9, 2) NULL,
	[Veh_Vol_High] [decimal](9, 2) NULL
) ON [PRIMARY]
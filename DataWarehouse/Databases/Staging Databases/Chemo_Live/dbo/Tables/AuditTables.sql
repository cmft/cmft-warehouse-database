﻿CREATE TABLE [dbo].[AuditTables](
	[TableID] [int] IDENTITY(1,1) NOT NULL,
	[Owner] [sysname] NOT NULL,
	[TableName] [sysname] NOT NULL,
	[AuditTable] [bit] NOT NULL,
	[AuditInserts] [bit] NOT NULL,
	[AuditUpdates] [bit] NOT NULL,
	[AuditDeletes] [bit] NOT NULL,
	[AuditDeletesAll] [bit] NOT NULL,
	[UniqueKeyType] [char](1) NOT NULL,
	[UserColumn] [sysname] NOT NULL DEFAULT (' ')
) ON [PRIMARY]
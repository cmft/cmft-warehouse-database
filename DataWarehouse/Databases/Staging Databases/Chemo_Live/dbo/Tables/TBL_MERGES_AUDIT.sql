﻿CREATE TABLE [dbo].[TBL_MERGES_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DATE_TRX] [datetime] NOT NULL,
	[USERID] [varchar](12) NOT NULL,
	[NUMBER_OLD] [varchar](20) NOT NULL,
	[NUMBER_NEW] [varchar](20) NOT NULL,
	[ACTION] [varchar](50) NOT NULL,
	[RequestID] [int] NULL
) ON [PRIMARY]
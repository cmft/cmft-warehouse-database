﻿CREATE TABLE [dbo].[REMOTE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SITENAME] [varchar](25) NOT NULL,
	[CHEMOVIEW] [varchar](25) NULL,
	[RTCAREVIEW] [varchar](25) NULL,
	[PATDRUGVW] [varchar](25) NULL,
	[TCOURSEVW] [varchar](25) NULL,
	[TMAPPTSVW] [varchar](25) NULL,
	[LOCALNUMB] [varchar](25) NULL,
	[PMIVIEW] [varchar](25) NULL
) ON [PRIMARY]
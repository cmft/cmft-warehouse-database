﻿CREATE TABLE [dbo].[PATLETT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[PL_TYPE] [varchar](1) NOT NULL,
	[PL_KEY] [varchar](3) NOT NULL,
	[PL_DATE] [datetime] NOT NULL,
	[PL_COPIES] [numeric](2, 0) NOT NULL,
	[PL_TEXT] [text] NULL,
	[PL_CARENO] [varchar](2) NOT NULL,
	[PL_CONFIRM] [varchar](1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
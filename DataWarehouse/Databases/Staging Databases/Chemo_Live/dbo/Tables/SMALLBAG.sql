﻿CREATE TABLE [dbo].[SMALLBAG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CID] [varchar](10) NULL,
	[VEHICLE] [varchar](60) NULL,
	[STARTVOL] [numeric](4, 0) NULL,
	[ENDVOL] [numeric](4, 0) NULL,
	[FINALVOL] [numeric](4, 0) NULL,
	[BAGS] [varchar](20) NULL
) ON [PRIMARY]
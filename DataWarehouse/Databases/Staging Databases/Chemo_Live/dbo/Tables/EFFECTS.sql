﻿CREATE TABLE [dbo].[EFFECTS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID1] [numeric](2, 0) NOT NULL,
	[ISREQUIRE] [bit] NOT NULL,
	[PROMPT] [varchar](15) NULL,
	[TEXT] [varchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TYPE] [varchar](1) NULL,
	[ID1] [varchar](10) NOT NULL,
	[PROTID] [varchar](10) NOT NULL,
	[ELIGSHORT] [varchar](30) NULL,
	[ELIGLONG] [varchar](4000) NULL,
	[ARMID] [varchar](10) NOT NULL
) ON [PRIMARY]
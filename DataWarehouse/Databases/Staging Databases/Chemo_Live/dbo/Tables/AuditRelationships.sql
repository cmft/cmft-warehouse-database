﻿CREATE TABLE [dbo].[AuditRelationships](
	[TableID] [int] NOT NULL,
	[RelationshipID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [sysname] NOT NULL,
	[JoinExpression] [nvarchar](1024) NOT NULL
) ON [PRIMARY]
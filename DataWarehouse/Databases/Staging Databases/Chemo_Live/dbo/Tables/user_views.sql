﻿CREATE TABLE [dbo].[user_views](
	[USERID] [varchar](50) NULL,
	[view_unique_id] [uniqueidentifier] NULL,
	[TRCODE] [varchar](3) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
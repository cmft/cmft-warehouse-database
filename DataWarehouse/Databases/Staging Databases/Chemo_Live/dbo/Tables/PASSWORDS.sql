﻿CREATE TABLE [dbo].[PASSWORDS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[opuid] [varchar](12) NOT NULL,
	[oppwd] [varchar](16) NOT NULL,
	[date_changed] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Tbl_WS_Prep_audit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[PrepId] [uniqueidentifier] NULL,
	[action] [varchar](100) NULL,
	[descrip] [text] NULL,
	[r_eason] [varchar](3) NULL,
	[u_ser] [varchar](50) NULL,
	[d_ate] [datetime] NULL,
	[timestamp_column] [timestamp] NULL,
	[actionType] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PATREGIM](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[REGIME] [varchar](16) NOT NULL,
	[CYCLE] [numeric](3, 0) NOT NULL CONSTRAINT [DF_PATREGIM_CYCLE]  DEFAULT (0),
	[CYCLEDATE] [datetime] NOT NULL,
	[STATUS] [numeric](2, 0) NULL CONSTRAINT [DF_PATREGIM_STATUS]  DEFAULT (0),
	[MAXDAYS] [numeric](3, 0) NULL CONSTRAINT [DF_PATREGIM_MAXDAYS]  DEFAULT (0),
	[COST] [numeric](5, 0) NULL CONSTRAINT [DF_PATREGIM_COST]  DEFAULT (0),
	[BAND] [varchar](6) NULL,
	[AUTH] [varchar](10) NULL,
	[SADATE] [datetime] NULL,
	[SAHEIGHT] [numeric](4, 2) NULL CONSTRAINT [DF_PATREGIM_SAHEIGHT]  DEFAULT (0),
	[SAWEIGHT] [numeric](6, 2) NULL CONSTRAINT [DF_PATREGIM_SAWEIGHT]  DEFAULT (0),
	[SA] [numeric](5, 2) NULL CONSTRAINT [DF_PATREGIM_SA]  DEFAULT (0),
	[CONS] [varchar](3) NULL,
	[GFRDATE] [datetime] NULL,
	[GFR] [numeric](6, 2) NULL CONSTRAINT [DF_PATREGIM_GFR]  DEFAULT (0),
	[GFR_TYPE] [numeric](1, 0) NULL CONSTRAINT [DF_PATREGIM_GFR_TYPE]  DEFAULT (0),
	[CARENO] [varchar](2) NULL,
	[TRIAL] [varchar](16) NULL,
	[INTENTION] [varchar](6) NULL,
	[GIVEPROTID] [varchar](10) NULL,
	[ALLOUNAME] [varchar](32) NULL,
	[ALLODATE] [datetime] NULL,
	[ALLOTIME] [varchar](5) NULL,
	[TRCODE] [varchar](3) NULL,
	[EDTA] [numeric](6, 2) NULL,
	[PATCOURSEID] [uniqueidentifier] NULL,
	[TRIALNO] [varchar](30) NULL,
	[PERFORM] [varchar](6) NULL,
	[PERFORMDATE] [datetime] NULL,
	[COMORBIDITY] [varchar](6) NULL,
	[DISPLAYNO] [numeric](3, 0) NULL
) ON [PRIMARY]
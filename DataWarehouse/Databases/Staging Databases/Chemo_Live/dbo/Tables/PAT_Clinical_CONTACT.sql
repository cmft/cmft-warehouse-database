﻿CREATE TABLE [dbo].[PAT_Clinical_CONTACT](
	[CONTACT_ID] [uniqueidentifier] NOT NULL,
	[PAT_ID] [varchar](20) NULL,
	[RELATIONSHIP_ID] [varchar](10) NULL,
	[RSHIP_FROM] [datetime] NOT NULL,
	[RSHIP_TO] [datetime] NULL,
	[NATIONAL_CODE] [varchar](20) NULL,
	[LOCAL_CODE] [varchar](20) NULL,
	[PRACTICE_ID] [varchar](10) NULL
) ON [PRIMARY]
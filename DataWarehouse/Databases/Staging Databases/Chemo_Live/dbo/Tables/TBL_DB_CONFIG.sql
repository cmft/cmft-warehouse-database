﻿CREATE TABLE [dbo].[TBL_DB_CONFIG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CODE] [varchar](50) NULL,
	[DESCRIPTION] [varchar](500) NULL,
	[ITEM_VALUE] [varchar](2100) NULL
) ON [PRIMARY]
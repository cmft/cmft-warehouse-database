﻿CREATE TABLE [dbo].[LaberrorLog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[districtno] [varchar](20) NULL,
	[test_date] [datetime] NULL,
	[testname] [varchar](20) NULL,
	[unk] [varchar](20) NULL,
	[profile] [varchar](10) NULL,
	[result] [varchar](20) NULL,
	[status] [bit] NULL
) ON [PRIMARY]
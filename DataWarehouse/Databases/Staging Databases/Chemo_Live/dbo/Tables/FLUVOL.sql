﻿CREATE TABLE [dbo].[FLUVOL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DRUGNAME] [varchar](30) NOT NULL,
	[COURSE] [varchar](16) NOT NULL,
	[LOWVAL] [numeric](8, 2) NULL,
	[UPVAL] [numeric](8, 2) NULL,
	[VOLUME] [numeric](8, 0) NULL,
	[CID] [varchar](10) NULL
) ON [PRIMARY]
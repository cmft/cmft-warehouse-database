﻿CREATE TABLE [dbo].[PathUnits](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pathid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_PathUnits_pathid]  DEFAULT (newid()),
	[labcode] [varchar](50) NOT NULL,
	[units] [varchar](20) NULL,
	[reflow] [varchar](50) NULL,
	[refhigh] [varchar](50) NULL,
	[comments] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
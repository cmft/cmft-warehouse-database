﻿CREATE TABLE [dbo].[Tbl_AuditPatdrugMissingApptID](
	[DISTRICTNO] [varchar](20) NOT NULL,
	[PATCOURSEID] [uniqueidentifier] NOT NULL,
	[APPTID] [uniqueidentifier] NOT NULL,
	[AUD_DATE] [datetime] NOT NULL
) ON [PRIMARY]
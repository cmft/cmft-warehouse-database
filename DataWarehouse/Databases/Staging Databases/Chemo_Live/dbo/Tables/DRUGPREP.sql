﻿CREATE TABLE [dbo].[DRUGPREP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DRUGNAME] [varchar](30) NOT NULL,
	[VIALQTY] [numeric](6, 1) NULL,
	[UNIT] [varchar](15) NULL,
	[CONCQTY] [float] NULL,
	[COMPCODE] [varchar](30) NULL,
	[ROUTE] [varchar](15) NOT NULL,
	[COST] [numeric](7, 2) NULL,
	[MANU] [varchar](12) NULL,
	[BATCHNO] [varchar](10) NULL,
	[EXPIRY] [datetime] NULL,
	[PHARMWORK] [numeric](5, 0) NULL,
	[NURSEWORK] [numeric](5, 0) NULL,
	[BRANDNAME] [varchar](30) NULL,
	[PLCODE] [varchar](3) NULL
) ON [PRIMARY]
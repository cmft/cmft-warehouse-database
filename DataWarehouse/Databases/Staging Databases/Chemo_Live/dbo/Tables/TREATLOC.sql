﻿CREATE TABLE [dbo].[TREATLOC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrCode] [varchar](3) NULL,
	[TrName] [varchar](25) NULL,
	[PhCode] [varchar](3) NULL,
	[GFR] [varchar](6) NULL,
	[NoSchedule] [bit] NULL
) ON [PRIMARY]
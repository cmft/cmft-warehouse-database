﻿CREATE TABLE [dbo].[CTEST](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID1] [varchar](10) NOT NULL,
	[TEXT] [varchar](30) NOT NULL,
	[LABCODE] [varchar](10) NOT NULL,
	[EXPIRY] [numeric](4, 0) NULL,
	[RANGE_LOW] [numeric](8, 2) NULL,
	[RANGE_HIGH] [numeric](8, 2) NULL,
	[PROFILE] [varchar](10) NULL,
	[USEULN] [bit] NULL
) ON [PRIMARY]
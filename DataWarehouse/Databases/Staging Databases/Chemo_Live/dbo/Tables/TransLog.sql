﻿CREATE TABLE [dbo].[TransLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Service] [nchar](10) NULL,
	[Interface] [nchar](10) NOT NULL,
	[ID_Number] [varchar](40) NULL,
	[Message] [ntext] NOT NULL,
	[CreationDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
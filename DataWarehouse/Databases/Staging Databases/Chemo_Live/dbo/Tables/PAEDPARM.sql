﻿CREATE TABLE [dbo].[PAEDPARM](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDENTIFIER] [varchar](1) NULL,
	[CODE] [varchar](6) NULL,
	[TEXT] [varchar](30) NULL
) ON [PRIMARY]
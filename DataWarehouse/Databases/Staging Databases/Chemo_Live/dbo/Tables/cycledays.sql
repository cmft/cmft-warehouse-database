﻿CREATE TABLE [dbo].[cycledays](
	[Regime] [varchar](16) NOT NULL,
	[dayno] [numeric](18, 0) NOT NULL,
	[p_type] [char](1) NOT NULL,
	[slots] [int] NULL,
	[nwuFirstslot] [numeric](5, 2) NULL,
	[nwuOtherSlot] [numeric](5, 2) NULL,
	[nwuFinalSlot] [numeric](5, 2) NULL,
	[cycledaysGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_cycledays_cyledaysGuid]  DEFAULT (newid())
) ON [PRIMARY]
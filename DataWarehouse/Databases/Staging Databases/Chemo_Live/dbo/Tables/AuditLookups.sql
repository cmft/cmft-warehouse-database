﻿CREATE TABLE [dbo].[AuditLookups](
	[LookupID] [int] NOT NULL,
	[RelationshipID] [int] NOT NULL,
	[ExpressionIsColumn] [bit] NOT NULL,
	[LookupExpression] [nvarchar](1024) NOT NULL
) ON [PRIMARY]
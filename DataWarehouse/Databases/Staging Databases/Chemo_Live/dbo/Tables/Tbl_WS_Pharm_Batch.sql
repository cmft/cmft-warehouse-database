﻿CREATE TABLE [dbo].[Tbl_WS_Pharm_Batch](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[PharmCode] [nchar](10) NOT NULL,
	[BatchDate] [datetime] NOT NULL,
	[BatchNum] [int] NOT NULL
) ON [PRIMARY]
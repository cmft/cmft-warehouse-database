﻿CREATE TABLE [dbo].[REGIND](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[REGIME] [varchar](16) NOT NULL,
	[INDICATE] [varchar](6) NOT NULL,
	[HRG] [varchar](50) NULL
) ON [PRIMARY]
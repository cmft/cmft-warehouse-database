﻿CREATE TABLE [dbo].[TBL_HOTKEY_REQ](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Districtno] [nvarchar](20) NOT NULL,
	[PAS_Code] [nvarchar](5) NOT NULL,
	[Status] [nvarchar](15) NOT NULL,
	[Result] [nvarchar](200) NULL,
	[Type] [nvarchar](1) NOT NULL,
	[RowGUID] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_TBL_HOTKEY_REQ_RowGUID]  DEFAULT (newid()),
	[Created] [datetime] NOT NULL,
	[LastUpdate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PROTOCOL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID1] [varchar](10) NOT NULL,
	[TEXT] [varchar](30) NULL,
	[DESCRIP] [text] NULL,
	[HISTORY] [text] NULL,
	[RELEASE] [bit] NOT NULL,
	[ARCHIVE] [bit] NOT NULL,
	[CLASS] [int] NULL,
	[TRIALMARK] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
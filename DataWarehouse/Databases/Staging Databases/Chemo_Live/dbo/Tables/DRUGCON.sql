﻿CREATE TABLE [dbo].[DRUGCON](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DRUGNAME] [varchar](30) NULL,
	[CONNAME] [varchar](17) NULL,
	[ROUTE] [varchar](15) NULL
) ON [PRIMARY]
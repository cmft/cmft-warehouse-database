﻿CREATE TABLE [dbo].[MachProcedure](
	[machine] [varchar](5) NOT NULL,
	[proctext] [varchar](30) NOT NULL,
	[slots] [int] NOT NULL,
	[defaultproc] [bit] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
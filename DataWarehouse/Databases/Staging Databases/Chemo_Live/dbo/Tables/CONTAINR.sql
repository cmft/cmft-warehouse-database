﻿CREATE TABLE [dbo].[CONTAINR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CONNAME] [varchar](17) NULL,
	[CONTYPE] [varchar](1) NULL,
	[CONSIZE] [numeric](18, 0) NULL CONSTRAINT [DF_CONTAINR_CONSIZE]  DEFAULT (0),
	[MANU] [varchar](10) NULL,
	[BATCHNO] [varchar](10) NULL,
	[EXPIRY] [datetime] NULL,
	[STOCKCODE] [varchar](30) NULL
) ON [PRIMARY]
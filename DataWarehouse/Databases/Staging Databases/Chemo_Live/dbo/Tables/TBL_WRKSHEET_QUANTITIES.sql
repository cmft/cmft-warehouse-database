﻿CREATE TABLE [dbo].[TBL_WRKSHEET_QUANTITIES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PATNUMBER] [varchar](30) NULL,
	[WARD] [varchar](6) NULL,
	[TREATLOC] [varchar](3) NULL,
	[PHARMLOC] [varchar](3) NULL,
	[CONS] [varchar](3) NULL,
	[DTEWRKGEN] [datetime] NULL,
	[STOCKCODE] [varchar](30) NULL,
	[QUANTITY] [varchar](6) NULL,
	[DTETIMESENT] [datetime] NULL,
	[BATCHNO] [varchar](7) NULL
) ON [PRIMARY]
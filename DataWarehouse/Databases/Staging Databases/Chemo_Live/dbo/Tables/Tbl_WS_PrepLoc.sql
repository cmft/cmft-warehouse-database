﻿CREATE TABLE [dbo].[Tbl_WS_PrepLoc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Prepid] [uniqueidentifier] NOT NULL,
	[PharmLoc] [char](3) NOT NULL
) ON [PRIMARY]
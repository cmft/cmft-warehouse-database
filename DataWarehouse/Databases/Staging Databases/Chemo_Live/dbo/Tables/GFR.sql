﻿CREATE TABLE [dbo].[GFR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[GFR] [numeric](6, 2) NOT NULL,
	[SERUM_C] [numeric](6, 2) NULL,
	[CALC_DATE] [datetime] NOT NULL,
	[CALC_USER] [varchar](10) NOT NULL,
	[GFR_TYPE] [numeric](1, 0) NOT NULL,
	[EDTA] [numeric](6, 2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[z_inttestcodes](
	[testid] [varchar](50) NOT NULL,
	[labdescription] [varchar](100) NULL,
	[Charcode] [char](10) NULL,
	[profilecode] [char](10) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
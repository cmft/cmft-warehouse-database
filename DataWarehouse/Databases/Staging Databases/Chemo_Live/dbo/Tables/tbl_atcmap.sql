﻿CREATE TABLE [dbo].[tbl_atcmap](
	[vpid] [char](18) NOT NULL,
	[bnf] [char](8) NOT NULL,
	[atc] [char](7) NOT NULL,
	[ddd] [char](6) NOT NULL,
	[ddd_uomcd] [char](18) NOT NULL,
	[identity_column] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_USER](
	[SPID] [int] NOT NULL,
	[APP_USER_NAME] [varchar](100) NULL,
	[SQL_USER_NAME] [varchar](100) NULL,
	[MACHINE_HOST] [varchar](100) NULL,
	[CLIENT_TYPE] [varchar](100) NULL,
	[DATETIME] [datetime] NULL
) ON [PRIMARY]
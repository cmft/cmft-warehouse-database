﻿CREATE TABLE [dbo].[STAGING](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CID] [varchar](50) NOT NULL,
	[STAGINGSYS] [varchar](50) NOT NULL,
	[HEADER] [bit] NOT NULL,
	[TYPE] [varchar](50) NULL,
	[TIMESTAMP_COLUMN] [timestamp] NULL
) ON [PRIMARY]
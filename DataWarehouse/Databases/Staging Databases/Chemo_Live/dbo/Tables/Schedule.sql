﻿CREATE TABLE [dbo].[Schedule](
	[Tid] [varchar](76) NULL,
	[Appt_Date] [datetime] NULL,
	[Appt_Start] [varchar](5) NULL,
	[Ward] [varchar](6) NULL,
	[DAYNO] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
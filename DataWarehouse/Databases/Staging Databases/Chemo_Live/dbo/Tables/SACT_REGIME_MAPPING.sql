﻿CREATE TABLE [dbo].[SACT_REGIME_MAPPING](
	[SACT_REGIME_MAPPING_ID] [int] IDENTITY(1,1) NOT NULL,
	[SACT_REGIME_ID] [int] NOT NULL,
	[CHEMO_REGIME_ID] [int] NOT NULL,
	[TREATLOC_ID] [int] NOT NULL,
	[CREATED_ON] [smalldatetime] NOT NULL,
	[CREATED_BY] [int] NOT NULL,
	[STATUS] [int] NOT NULL DEFAULT ((1)),
	[STATUS_CHANGED_ON] [smalldatetime] NULL,
	[STATUS_CHANGED_BY] [int] NULL
) ON [PRIMARY]
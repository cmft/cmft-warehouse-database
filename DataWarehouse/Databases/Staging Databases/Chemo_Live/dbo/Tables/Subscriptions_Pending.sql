﻿CREATE TABLE [dbo].[Subscriptions_Pending](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_NO] [varchar](20) NOT NULL,
	[STATUS] [nchar](20) NOT NULL CONSTRAINT [DF_Subscriptions_Pending_STATUS]  DEFAULT ('PENDING'),
	[ROWGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Subscriptions_Pending_ROWGUID]  DEFAULT (newid()),
	[CREATED] [datetime] NOT NULL,
	[SENDAS] [varchar](3) NOT NULL CONSTRAINT [DF_Subscriptions_Pending_SENDAS]  DEFAULT ('SUB')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_PATTOXIC_CFG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TOXLABEL] [varchar](200) NULL,
	[SCORE_0] [varchar](200) NULL,
	[SCORE_1] [varchar](200) NULL,
	[SCORE_2] [varchar](200) NULL,
	[SCORE_3] [varchar](200) NULL,
	[SCORE_4] [varchar](200) NULL,
	[TOXORDER] [int] NULL
) ON [PRIMARY]
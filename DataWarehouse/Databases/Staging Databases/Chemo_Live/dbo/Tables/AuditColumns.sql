﻿CREATE TABLE [dbo].[AuditColumns](
	[TableID] [int] NOT NULL,
	[ColumnID] [int] IDENTITY(1,1) NOT NULL,
	[ColumnName] [sysname] NOT NULL,
	[AuditColumn] [bit] NOT NULL CONSTRAINT [DF_AuditColumns_Audited]  DEFAULT ((0)),
	[CaptureColumn] [bit] NOT NULL CONSTRAINT [DF_AuditColumns_Captured]  DEFAULT ((0)),
	[IsLookup] [bit] NOT NULL CONSTRAINT [DF_AuditColumns_IsLookup]  DEFAULT ((0)),
	[KeyColumn] [tinyint] NOT NULL CONSTRAINT [DF_AuditColumns_KeyColumn]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatIntervention](
	[PatInterventionGuid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_PatIntervention_PatInterventionGuid]  DEFAULT (newid()),
	[ApptId] [uniqueidentifier] NULL,
	[InterventionGuid] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Tbl_WS_Template_audit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Template_ID] [uniqueidentifier] NOT NULL,
	[ActionDescrption] [varchar](100) NULL,
	[ActionType] [int] NULL,
	[Description] [text] NULL,
	[ReasonCode] [varchar](3) NULL,
	[UserId] [varchar](50) NULL,
	[UserName] [varchar](50) NULL,
	[AuditDateTime] [datetime] NULL,
	[timestamp_column] [timestamp] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[INFUSOR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TEXT] [varchar](15) NOT NULL,
	[INFTYPE] [numeric](1, 0) NOT NULL,
	[BATCHNO] [varchar](10) NULL,
	[EXPIRY] [datetime] NULL,
	[MANU] [varchar](12) NULL,
	[RATE] [numeric](5, 2) NULL CONSTRAINT [DF_INFUSOR_RATE]  DEFAULT (0)
) ON [PRIMARY]
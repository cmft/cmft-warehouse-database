﻿CREATE TABLE [dbo].[tbl_ErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_ErrorLog_ErrorDate]  DEFAULT (getdate()),
	[ErrorCode] [int] NOT NULL,
	[StoredProcedure] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_CSTransactionSaline](
	[TransactionSalineID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[ListNumber] [nvarchar](50) NULL,
	[LotNumber] [nvarchar](50) NULL,
	[ExpiryDate] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_PrivilegeLevel](
	[PrivilegeLevelID] [int] IDENTITY(1,1) NOT NULL,
	[PrivilegeLevel] [smallint] NOT NULL,
	[AccessRightID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_UserComponentTrainingStatus](
	[UserComponentTrainingStatusID] [smallint] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[TrainingProfileComponentID] [smallint] NOT NULL,
	[TrainingStatusID] [smallint] NOT NULL,
	[TrainingDate] [datetime] NULL,
	[ModifiedByUserID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Comments] [nchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_LanguageTranslation](
	[LanguageTranslationID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](255) NOT NULL,
	[RowID] [int] NOT NULL,
	[1033] [nvarchar](50) NULL,
	[2057] [nvarchar](50) NULL,
	[1031] [nvarchar](50) NULL,
	[1036] [nvarchar](50) NULL,
	[1030] [nvarchar](50) NULL,
	[3079] [nvarchar](50) NULL,
	[5127] [nvarchar](50) NULL,
	[1043] [nvarchar](50) NULL,
	[1040] [nvarchar](50) NULL
) ON [PRIMARY]
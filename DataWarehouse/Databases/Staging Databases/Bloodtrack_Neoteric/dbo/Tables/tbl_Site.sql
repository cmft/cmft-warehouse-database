﻿CREATE TABLE [dbo].[tbl_Site](
	[SiteID] [smallint] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Site_StatusID]  DEFAULT (0),
	[SiteName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[PostCode] [nvarchar](20) NULL,
	[SiteCode] [nvarchar](50) NULL,
	[ChangedByUserID] [int] NOT NULL,
	[LastChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_Site_SiteChangedDate]  DEFAULT (getdate()),
	[UserAutoAdd] [tinyint] NOT NULL CONSTRAINT [DF_tbl_Site_UserAutoAdd]  DEFAULT (1),
	[UserDefaultGroupID] [int] NULL
) ON [PRIMARY]
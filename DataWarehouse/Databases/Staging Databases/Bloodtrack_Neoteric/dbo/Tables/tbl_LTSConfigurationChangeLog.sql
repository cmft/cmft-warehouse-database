﻿CREATE TABLE [dbo].[tbl_LTSConfigurationChangeLog](
	[ChangeLogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[ChangeDate] [datetime] NOT NULL
) ON [PRIMARY]
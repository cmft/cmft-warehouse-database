﻿CREATE TABLE [dbo].[tbl_TrainingComponent](
	[TrainingComponentID] [smallint] IDENTITY(0,1) NOT NULL,
	[TrainingComponentName] [nvarchar](50) NOT NULL,
	[TrainingComponentTypeID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_TrainingComponent_StatusID]  DEFAULT (0),
	[Description] [nvarchar](50) NULL,
	[TrainingComponentURL] [nvarchar](100) NULL,
	[IsSupervised] [bit] NOT NULL,
	[RequiredForRetraining] [bit] NOT NULL CONSTRAINT [DF_tbl_TrainingComponent_RequiredForRetraining]  DEFAULT (0)
) ON [PRIMARY]
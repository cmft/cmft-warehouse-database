﻿CREATE TABLE [dbo].[tbl_MilkOrder](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[OrderNumber] [nvarchar](10) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[OrderTypeID] [smallint] NULL,
	[TransactionID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_Tests](
	[TestID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Tests_StatusID]  DEFAULT (0),
	[TestCode] [nvarchar](20) NULL,
	[SpecimenId] [int] NULL
) ON [PRIMARY]
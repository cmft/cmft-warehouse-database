﻿CREATE TABLE [dbo].[tbl_CSSurgeon](
	[CSSurgeonID] [int] IDENTITY(1,1) NOT NULL,
	[SurgeonID] [nvarchar](50) NOT NULL,
	[SurgeonLastName] [nvarchar](50) NULL,
	[SurgeonFirstName] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_MilkTransactionPatient](
	[TransactionPatientID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[PatientID] [int] NOT NULL
) ON [PRIMARY]
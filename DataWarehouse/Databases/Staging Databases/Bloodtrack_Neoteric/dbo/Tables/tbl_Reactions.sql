﻿CREATE TABLE [dbo].[tbl_Reactions](
	[ReactionID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Reactions_StatusID]  DEFAULT (0),
	[ReactionCode] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_TrainingProduct](
	[ProductID] [smallint] NOT NULL,
	[TrainingExpiryWarningDays] [smallint] NULL,
	[TrainingExpiryActionID] [smallint] NULL,
	[TrainingProductID] [smallint] IDENTITY(1,1) NOT NULL,
	[TrainingProductName] [nvarchar](50) NULL,
	[TrainingExpiryPeriod] [smallint] NULL CONSTRAINT [DF_tbl_TrainingProduct_ExpiryPeriod]  DEFAULT (0),
	[InactivityExpiryPeriod] [smallint] NULL CONSTRAINT [DF_tbl_TrainingProduct_InactivityExpiryPeriod]  DEFAULT (0),
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_TrainingProduct_StatusID]  DEFAULT (0),
	[TrainingUntrainedActionID] [smallint] NULL
) ON [PRIMARY]
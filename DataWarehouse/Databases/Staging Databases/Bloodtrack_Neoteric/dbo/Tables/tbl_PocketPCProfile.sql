﻿CREATE TABLE [dbo].[tbl_PocketPCProfile](
	[ProfileID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[StatusID] [smallint] NULL CONSTRAINT [DF_tbl_PocketPCProfile_StatusID_1]  DEFAULT (0),
	[Description] [nvarchar](50) NULL
) ON [PRIMARY]
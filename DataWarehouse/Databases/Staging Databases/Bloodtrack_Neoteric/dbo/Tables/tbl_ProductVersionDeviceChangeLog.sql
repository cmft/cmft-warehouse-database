﻿CREATE TABLE [dbo].[tbl_ProductVersionDeviceChangeLog](
	[ChangeLogID] [int] IDENTITY(0,1) NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[DeviceID] [int] NOT NULL,
	[Version] [nvarchar](20) NOT NULL,
	[StatusID] [tinyint] NOT NULL,
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_ProductVersionDeviceChangeLog_ChangeDate]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_RemoteReleaseReport](
	[RemoteReleaseID] [int] IDENTITY(0,1) NOT NULL,
	[DeviceID] [int] NOT NULL,
	[StorageDeviceID] [int] NOT NULL,
	[StatusID] [smallint] NOT NULL
) ON [PRIMARY]
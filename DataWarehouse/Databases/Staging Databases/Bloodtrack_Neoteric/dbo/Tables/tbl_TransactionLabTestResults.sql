﻿CREATE TABLE [dbo].[tbl_TransactionLabTestResults](
	[TransactionLabTestResultID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NULL,
	[LabTestID] [smallint] NULL,
	[LabTestResult] [int] NULL,
	[LabTestDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_BloodProduct](
	[BloodProductID] [int] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodProduct_StatusID]  DEFAULT (0),
	[ProductCode] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[BloodProductGroupID] [int] NULL CONSTRAINT [DF_tbl_BloodProduct_BloodProductGroupID]  DEFAULT (0),
	[Requirements] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProduct_Requirements]  DEFAULT (0),
	[DefaultVolume] [smallint] NULL,
	[CourierDescription] [nvarchar](50) NULL,
	[SafeTxDescription] [nvarchar](50) NULL,
	[DefaultProduct] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProduct_DefaultProduct]  DEFAULT (0),
	[AltProductCode] [nvarchar](10) NULL,
	[Irradiated] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProduct_Irradiated]  DEFAULT (0),
	[LeukoReduced] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProduct_CMVNegative]  DEFAULT (0),
	[DownloadToTx] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProduct_TxDownload]  DEFAULT (0)
) ON [PRIMARY]
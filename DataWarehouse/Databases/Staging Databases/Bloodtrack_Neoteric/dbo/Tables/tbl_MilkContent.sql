﻿CREATE TABLE [dbo].[tbl_MilkContent](
	[ContentID] [smallint] IDENTITY(1,1) NOT NULL,
	[HasBreastMilk] [tinyint] NOT NULL,
	[HasDonorMilk] [tinyint] NOT NULL,
	[HasFormula] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkContent_StatusID]  DEFAULT (0),
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
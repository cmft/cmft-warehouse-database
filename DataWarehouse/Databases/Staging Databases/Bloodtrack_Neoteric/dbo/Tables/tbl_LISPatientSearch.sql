﻿CREATE TABLE [dbo].[tbl_LISPatientSearch](
	[LISPatientSearchID] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
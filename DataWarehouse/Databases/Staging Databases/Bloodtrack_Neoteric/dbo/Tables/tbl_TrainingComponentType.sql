﻿CREATE TABLE [dbo].[tbl_TrainingComponentType](
	[ComponentTypeID] [smallint] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_BloodProductRules](
	[BloodProductRuleID] [int] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodProductRules_StatusID]  DEFAULT (0),
	[Description] [nvarchar](50) NOT NULL,
	[RoomTempStorage] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductRules_RoomTempStorage]  DEFAULT (0),
	[RoomTempTransport] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductRules_RoomTempTransport]  DEFAULT (0),
	[RoomTempTransportLimit] [int] NULL,
	[RoomTempReturn] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductRules_RoomTempReturn]  DEFAULT (0),
	[RefrigeratedStorage] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductRules_RefrigeratedStorage]  DEFAULT (0),
	[RefrigeratedTransport] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductRules_RefrigeratedTransport]  DEFAULT (0),
	[RefrigeratedTransportLimit] [int] NULL,
	[RefrigeratedReturn] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductRules_RefrigeratedReturn]  DEFAULT (0),
	[FrozenStorage] [tinyint] NOT NULL,
	[FrozenReturn] [tinyint] NOT NULL,
	[DefaultTransportTypeID] [smallint] NOT NULL,
	[InterSiteTransportTypeID] [smallint] NOT NULL,
	[ReminderListID] [smallint] NULL
) ON [PRIMARY]
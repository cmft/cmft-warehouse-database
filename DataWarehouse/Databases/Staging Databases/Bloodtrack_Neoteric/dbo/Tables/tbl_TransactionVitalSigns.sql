﻿CREATE TABLE [dbo].[tbl_TransactionVitalSigns](
	[TransactionVitalSignID] [int] IDENTITY(0,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[VitalSignID] [int] NOT NULL,
	[VitalValue] [int] NOT NULL
) ON [PRIMARY]
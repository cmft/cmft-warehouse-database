﻿CREATE TABLE [dbo].[tbl_PatientDetails](
	[PatientGroupID] [int] NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[ExpiryDate] [datetime] NULL,
	[UnitCount] [int] NULL,
	[DateUpdated] [tinyint] NULL,
	[LastChangeDate] [datetime] NULL
) ON [PRIMARY]
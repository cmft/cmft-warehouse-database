﻿CREATE TABLE [dbo].[tbl_BarcodeMaskType](
	[BarcodeMaskTypeID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_MilkTransactionFortifier](
	[TransactionFortifierID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[OrderPartID] [smallint] NOT NULL,
	[FortifierID] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_BloodGroup](
	[BloodGroupID] [smallint] NOT NULL,
	[Barcode1] [nvarchar](10) NULL,
	[Barcode2] [nvarchar](10) NULL,
	[Barcode3] [nvarchar](10) NULL,
	[Barcode4] [nvarchar](10) NULL,
	[Barcode5] [nvarchar](10) NULL,
	[Barcode6] [nvarchar](10) NULL,
	[Barcode7] [nvarchar](10) NULL,
	[Barcode8] [nvarchar](10) NULL,
	[BloodGroupCode] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](20) NOT NULL,
	[Alternate] [nvarchar](20) NULL
) ON [PRIMARY]
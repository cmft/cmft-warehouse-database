﻿CREATE TABLE [dbo].[tbl_PatientGroup](
	[PatientGroupID] [int] NOT NULL,
	[PatientID] [int] NOT NULL,
	[Master] [tinyint] NOT NULL CONSTRAINT [DF_tbl_PatientGroup_Master]  DEFAULT (0)
) ON [PRIMARY]
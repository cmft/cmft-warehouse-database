﻿CREATE TABLE [dbo].[tbl_EmailConfiguration](
	[SMTPHost] [nvarchar](50) NULL,
	[SMTPFromAddress] [nvarchar](50) NULL,
	[AlertRecipients] [nvarchar](255) NULL,
	[ProductSuiteID] [smallint] NULL CONSTRAINT [DF_tbl_EmailConfiguration_ProductSuite]  DEFAULT ((1))
) ON [PRIMARY]
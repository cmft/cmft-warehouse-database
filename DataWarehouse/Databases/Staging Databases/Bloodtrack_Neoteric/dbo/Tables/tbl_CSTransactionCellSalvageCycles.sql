﻿CREATE TABLE [dbo].[tbl_CSTransactionCellSalvageCycles](
	[TransactionCellSalvageCycleID] [int] IDENTITY(1,1) NOT NULL,
	[CellSalvageTransactionID] [int] NOT NULL,
	[CycleID] [smallint] NULL,
	[ProcessVolume] [smallint] NULL,
	[WashVolume] [smallint] NULL,
	[ReinfusionVolume] [smallint] NULL,
	[ReinfusionVolumeForConc] [smallint] NULL
) ON [PRIMARY]
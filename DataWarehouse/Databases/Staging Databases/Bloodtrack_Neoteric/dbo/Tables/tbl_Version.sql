﻿CREATE TABLE [dbo].[tbl_Version](
	[VersionID] [int] IDENTITY(0,1) NOT NULL,
	[Version] [nvarchar](10) NOT NULL,
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_Version_ChangeDate]  DEFAULT (getdate())
) ON [PRIMARY]
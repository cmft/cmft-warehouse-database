﻿CREATE TABLE [dbo].[tbl_CSTransactionSequestrationCycles](
	[TransactionSequestrationCycleID] [int] IDENTITY(1,1) NOT NULL,
	[SequestrationTransactionID] [int] NOT NULL,
	[CycleID] [smallint] NULL,
	[ProcessVolume] [smallint] NULL,
	[PPPVolume] [smallint] NULL,
	[PRPVolume] [smallint] NULL,
	[ReinfusionVolume] [smallint] NULL,
	[ReinfusionVolumeForConc] [smallint] NULL
) ON [PRIMARY]
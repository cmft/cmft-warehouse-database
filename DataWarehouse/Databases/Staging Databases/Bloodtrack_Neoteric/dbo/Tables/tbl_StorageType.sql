﻿CREATE TABLE [dbo].[tbl_StorageType](
	[StorageTypeID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Alternate] [varchar](50) NULL
) ON [PRIMARY]
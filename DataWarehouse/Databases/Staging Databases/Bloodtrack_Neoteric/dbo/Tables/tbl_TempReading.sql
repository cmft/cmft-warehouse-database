﻿CREATE TABLE [dbo].[tbl_TempReading](
	[TempReadingID] [int] IDENTITY(1,1) NOT NULL,
	[StorageDeviceID] [int] NOT NULL,
	[TempStatusID] [smallint] NOT NULL,
	[ReadingDate] [datetime] NOT NULL,
	[TempC] [smallint] NOT NULL,
	[TempF] [smallint] NOT NULL
) ON [PRIMARY]
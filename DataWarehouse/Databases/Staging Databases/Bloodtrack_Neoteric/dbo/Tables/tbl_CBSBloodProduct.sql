﻿CREATE TABLE [dbo].[tbl_CBSBloodProduct](
	[BloodProductID] [int] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NULL,
	[ProductCode] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[BloodProductGroupID] [int] NULL,
	[Requirements] [tinyint] NULL,
	[DefaultVolume] [smallint] NULL,
	[CourierDescription] [nvarchar](50) NULL,
	[SafeTxDescription] [nvarchar](50) NULL,
	[DefaultProduct] [tinyint] NOT NULL CONSTRAINT [DF_tbl_CBSBloodProduct_DefaultProduct]  DEFAULT (0),
	[AltProductCode] [nvarchar](10) NULL,
	[Irradiated] [tinyint] NOT NULL CONSTRAINT [DF_tbl_CBSBloodProduct_Irradiated]  DEFAULT (0),
	[LeukoReduced] [tinyint] NOT NULL CONSTRAINT [DF_tbl_CBSBloodProduct_CMVNegative]  DEFAULT (0),
	[DownloadToTx] [tinyint] NOT NULL CONSTRAINT [DF_tbl_CBSBloodProduct_DownloadToTx]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_MilkFortifier](
	[FortifierID] [smallint] IDENTITY(1,1) NOT NULL,
	[Abbreviation] [nvarchar](15) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[UsedInBreastMilk] [tinyint] NOT NULL,
	[UsedInDonorMilk] [tinyint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkFortifier_StatusID]  DEFAULT (0)
) ON [PRIMARY]
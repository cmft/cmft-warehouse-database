﻿CREATE TABLE [dbo].[tbl_PocketPCFrameworkConfig](
	[FrameWorkConfigID] [int] IDENTITY(1,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_PocketFrameworkConfiguration_StatusID]  DEFAULT ((0)),
	[SettingDescription] [nvarchar](255) NOT NULL,
	[SettingValue] [nvarchar](255) NULL,
	[ProfileID] [int] NOT NULL
) ON [PRIMARY]
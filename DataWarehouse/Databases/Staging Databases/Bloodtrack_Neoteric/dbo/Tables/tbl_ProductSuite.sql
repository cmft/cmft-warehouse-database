﻿CREATE TABLE [dbo].[tbl_ProductSuite](
	[ProductSuiteID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_ProductSuite_StatusID]  DEFAULT ((0)),
	[Description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
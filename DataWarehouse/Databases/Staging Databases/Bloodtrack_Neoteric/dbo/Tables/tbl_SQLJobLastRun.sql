﻿CREATE TABLE [dbo].[tbl_SQLJobLastRun](
	[JobID] [smallint] NOT NULL,
	[ServiceID] [smallint] NOT NULL,
	[LastRunDateTime] [datetime] NOT NULL
) ON [PRIMARY]
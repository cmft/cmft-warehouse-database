﻿CREATE TABLE [dbo].[tbl_BloodOrderReasons](
	[ReasonID] [smallint] NOT NULL,
	[CategoryID] [smallint] NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodOrderReasons_StatusID]  DEFAULT (0),
	[Quantity] [smallint] NULL,
	[ReasonName] [nvarchar](50) NULL,
	[IndicationID] [smallint] NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
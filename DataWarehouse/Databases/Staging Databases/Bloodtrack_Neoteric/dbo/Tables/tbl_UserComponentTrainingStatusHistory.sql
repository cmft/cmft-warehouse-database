﻿CREATE TABLE [dbo].[tbl_UserComponentTrainingStatusHistory](
	[UserComponentTrainingStatusHistoryID] [smallint] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[CreatedWhenDate] [datetime] NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[TrainingStatusID] [smallint] NOT NULL,
	[Reason] [nvarchar](50) NULL,
	[TrainingDate] [datetime] NULL,
	[UserComponentTrainingStatusID] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_TransfusionStatus](
	[BloodUnitID] [int] NOT NULL,
	[MaxRowID] [int] NOT NULL,
	[TransactionID] [int] NOT NULL,
	[DeviceDate] [datetime] NOT NULL,
	[ConditionID] [smallint] NOT NULL,
	[PatientID] [int] NULL,
	[TransfusionStatus] [tinyint] NOT NULL,
	[TransfusionTime] [smallint] NOT NULL,
	[LocationID] [smallint] NOT NULL,
	[SiteID] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_LTManagerProfileChangeLog](
	[ChangeLogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[ProfileID] [int] NOT NULL,
	[ChangeDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_ProductUpdate](
	[ProductUpdateID] [int] IDENTITY(1,1) NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[ProductVersionID] [int] NOT NULL,
	[URL] [nvarchar](255) NOT NULL
) ON [PRIMARY]
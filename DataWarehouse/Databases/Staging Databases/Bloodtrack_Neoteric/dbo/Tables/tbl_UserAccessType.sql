﻿CREATE TABLE [dbo].[tbl_UserAccessType](
	[UserAccessTypeID] [smallint] IDENTITY(0,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_PatientConfiguration](
	[WristbandDateFormatID] [smallint] NOT NULL CONSTRAINT [DF_tbl_PatientConfiguration_WristbandDateFormatID]  DEFAULT (6),
	[PatientDataFormatID] [smallint] NOT NULL CONSTRAINT [DF_tbl_PatientConfiguration_PatientDateFormatID]  DEFAULT (0),
	[PatientNumberlength] [smallint] NOT NULL CONSTRAINT [DF_tbl_PatientConfiguration_PatientNumberlength]  DEFAULT (0),
	[PatientNumberCheckDigitID] [smallint] NOT NULL CONSTRAINT [DF_tbl_PatientConfiguration_PatientNumberCheckDigitID]  DEFAULT (0),
	[PatientNumberTypeID] [smallint] NOT NULL CONSTRAINT [DF_tbl_PatientConfiguration_PatientNumberTypeID]  DEFAULT (0),
	[PatientSearch] [smallint] NOT NULL CONSTRAINT [DF_tbl_PatientConfiguration_PatientSearch]  DEFAULT ((0))
) ON [PRIMARY]
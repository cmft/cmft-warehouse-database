﻿CREATE TABLE [dbo].[tbl_ProductRights](
	[ProductRightID] [smallint] IDENTITY(0,1) NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[AccessRightID] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_LabTests](
	[LabTestID] [smallint] NOT NULL,
	[TestName] [nvarchar](6) NULL,
	[TestDescription] [nvarchar](30) NULL,
	[Units] [nvarchar](10) NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_LabTests_StatusID]  DEFAULT (0)
) ON [PRIMARY]
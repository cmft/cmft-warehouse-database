﻿CREATE TABLE [dbo].[tbl_MilkTransactionAdditiveDose](
	[TransactionAdditiveDoseID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[OrderPartID] [smallint] NOT NULL,
	[AdditiveDoseID] [smallint] NOT NULL
) ON [PRIMARY]
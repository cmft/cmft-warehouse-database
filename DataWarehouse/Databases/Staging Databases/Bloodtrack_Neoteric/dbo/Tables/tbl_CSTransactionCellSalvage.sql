﻿CREATE TABLE [dbo].[tbl_CSTransactionCellSalvage](
	[TransactionCellSalvageID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[ActiveCellSalvageSettingsGroup] [nvarchar](50) NOT NULL,
	[SalvageStartDate] [datetime] NOT NULL,
	[SalvageDuration] [nvarchar](50) NULL,
	[SalvageTotalProcessed] [int] NULL,
	[SalvageTotalWashed] [int] NULL,
	[SalvageTotalReinfusion] [int] NULL
) ON [PRIMARY]
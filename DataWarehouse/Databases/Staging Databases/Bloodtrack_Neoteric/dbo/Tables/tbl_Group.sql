﻿CREATE TABLE [dbo].[tbl_Group](
	[GroupID] [int] IDENTITY(0,1) NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Group_StatusID]  DEFAULT (0),
	[SiteID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Group_SiteID]  DEFAULT (0),
	[PasswordRequired] [tinyint] NOT NULL CONSTRAINT [DF_tbl_Group_PasswordRequired]  DEFAULT (0),
	[PasswordValidPeriod] [smallint] NULL CONSTRAINT [DF__tbl_Group__Passw__01DE32A8]  DEFAULT (60),
	[MinimumPasswordLength] [tinyint] NOT NULL CONSTRAINT [DF_tbl_Group_MinimumPasswordLength]  DEFAULT (4),
	[TokenValidPeriod] [smallint] NOT NULL CONSTRAINT [DF_tbl_Group_TokenValidPeriod]  DEFAULT (24),
	[Description] [nvarchar](50) NULL,
	[LastChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_Group_LastChangeDate]  DEFAULT (getdate()),
	[GroupLevel] [smallint] NULL,
	[InvalidPINCount] [smallint] NULL
) ON [PRIMARY]
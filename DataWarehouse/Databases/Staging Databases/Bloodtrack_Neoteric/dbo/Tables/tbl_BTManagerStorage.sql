﻿CREATE TABLE [dbo].[tbl_BTManagerStorage](
	[ManagerStorageID] [int] IDENTITY(0,1) NOT NULL,
	[DeviceID] [int] NOT NULL,
	[StorageID] [int] NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[LastChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_BTManagerStorage_LastChangeDate]  DEFAULT (getdate()),
	[DefaultStorage] [tinyint] NOT NULL DEFAULT (0)
) ON [PRIMARY]
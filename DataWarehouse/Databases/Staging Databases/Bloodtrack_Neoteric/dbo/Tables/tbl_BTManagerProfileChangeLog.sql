﻿CREATE TABLE [dbo].[tbl_BTManagerProfileChangeLog](
	[ChangeLogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[ProfileID] [int] NOT NULL,
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_BTManagerProfileChangeLog_ChangeDate]  DEFAULT (getdate())
) ON [PRIMARY]
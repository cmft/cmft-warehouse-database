﻿CREATE TABLE [dbo].[tbl_MilkUnitFate](
	[FateID] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkUnitFate_StatusID]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_Reminders](
	[ReminderID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Reminders_StatusID]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_GroupAccessRights](
	[GroupAccessRightID] [int] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[LocationID] [smallint] NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[AccessRightID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_GroupAccessRights_StatusID]  DEFAULT (0),
	[PrivilegeLevel] [tinyint] NOT NULL,
	[LastChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_GroupAccessRights_LastChangeDate]  DEFAULT (getdate())
) ON [PRIMARY]
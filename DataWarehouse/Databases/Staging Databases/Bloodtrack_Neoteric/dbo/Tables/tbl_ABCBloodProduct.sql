﻿CREATE TABLE [dbo].[tbl_ABCBloodProduct](
	[BloodProductID] [int] NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[ProductCode] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[BloodProductGroupID] [int] NOT NULL,
	[Requirements] [tinyint] NOT NULL,
	[DefaultVolume] [smallint] NULL,
	[CourierDescription] [nvarchar](50) NULL,
	[SafeTxDescription] [nvarchar](50) NULL,
	[DefaultProduct] [tinyint] NOT NULL,
	[AltProductCode] [nvarchar](50) NULL,
	[Irradiated] [tinyint] NOT NULL CONSTRAINT [DF_tbl_ABCBloodProduct_Irradiated]  DEFAULT (0),
	[LeukoReduced] [tinyint] NOT NULL CONSTRAINT [DF_tbl_ABCBloodProduct_CMVNegative]  DEFAULT (0),
	[DownloadToTx] [tinyint] NOT NULL CONSTRAINT [DF_tbl_ABCBloodProduct_DownloadToTx]  DEFAULT (0)
) ON [PRIMARY]
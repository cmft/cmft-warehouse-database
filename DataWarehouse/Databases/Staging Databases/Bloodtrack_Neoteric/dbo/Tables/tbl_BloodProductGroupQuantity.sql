﻿CREATE TABLE [dbo].[tbl_BloodProductGroupQuantity](
	[BloodProductGroupQuantityID] [smallint] IDENTITY(1,1) NOT NULL,
	[BloodProductGroupID] [int] NOT NULL,
	[Quantities] [nvarchar](50) NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroupQuantity_StatusID]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_MilkOrderPart](
	[OrderPartID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkOrderPart_StatusID]  DEFAULT (0),
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
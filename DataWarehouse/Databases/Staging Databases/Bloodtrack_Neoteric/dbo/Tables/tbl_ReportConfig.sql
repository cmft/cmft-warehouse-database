﻿CREATE TABLE [dbo].[tbl_ReportConfig](
	[ReportConfigID] [int] IDENTITY(1,1) NOT NULL,
	[ProfileID] [int] NOT NULL,
	[ReportID] [int] NOT NULL,
	[ColumnNumber] [tinyint] NOT NULL,
	[ColumnSetting] [tinyint] NOT NULL
) ON [PRIMARY]
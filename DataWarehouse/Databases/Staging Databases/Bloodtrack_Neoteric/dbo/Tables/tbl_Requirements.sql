﻿CREATE TABLE [dbo].[tbl_Requirements](
	[RequirementID] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Requirements_StatusID]  DEFAULT (0),
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
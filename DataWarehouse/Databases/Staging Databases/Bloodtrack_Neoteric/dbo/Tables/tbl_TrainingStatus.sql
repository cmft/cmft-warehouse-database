﻿CREATE TABLE [dbo].[tbl_TrainingStatus](
	[TrainingStatusID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
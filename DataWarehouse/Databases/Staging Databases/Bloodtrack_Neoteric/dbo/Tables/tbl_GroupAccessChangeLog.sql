﻿CREATE TABLE [dbo].[tbl_GroupAccessChangeLog](
	[GroupAccessChangeID] [int] IDENTITY(1,1) NOT NULL,
	[ChangedByUserID] [int] NOT NULL,
	[GroupAccessRightID] [int] NOT NULL,
	[LocationID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_GroupAccessChangeLog_StatusID]  DEFAULT (0),
	[PrivilegeLevel] [tinyint] NOT NULL,
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_GroupAccessChangeLog_GroupAccessChangeDate]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_BloodOrderComplianceRules](
	[ComplianceRuleID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodOrderComplianceRules_StatusID]  DEFAULT (0),
	[BloodProductGroupID] [int] NULL,
	[LabTestID] [smallint] NULL,
	[MinValue] [nvarchar](20) NULL,
	[MaxValue] [nvarchar](10) NULL,
	[RuleName] [nvarchar](20) NULL,
	[RuleDescription] [nvarchar](50) NULL
) ON [PRIMARY]
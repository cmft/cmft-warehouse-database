﻿CREATE TABLE [dbo].[tbl_BloodOrderReasonCategories](
	[CategoryID] [smallint] NOT NULL,
	[BranchID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodOrderReasonCategories_StatusID]  DEFAULT (0),
	[CategoryName] [nvarchar](50) NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
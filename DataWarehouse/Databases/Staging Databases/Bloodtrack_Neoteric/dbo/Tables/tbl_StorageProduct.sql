﻿CREATE TABLE [dbo].[tbl_StorageProduct](
	[StorageProductID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceID] [int] NOT NULL,
	[BloodProductGroupID] [int] NOT NULL,
	[EmergencyBlood] [tinyint] NOT NULL CONSTRAINT [DF_tbl_StorageProduct_EmergencyBlood]  DEFAULT ((0)),
	[BloodGroupID1] [smallint] NULL,
	[BloodGroupID2] [smallint] NULL
) ON [PRIMARY]
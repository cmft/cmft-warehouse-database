﻿CREATE TABLE [dbo].[tbl_Device](
	[DeviceID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceName] [nvarchar](50) NOT NULL,
	[LocationID] [smallint] NOT NULL,
	[DeviceTypeID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Device_StatusID]  DEFAULT ((0)),
	[DeviceNumber] [nvarchar](50) NULL,
	[LISCode] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[Timezone] [smallint] NULL,
	[ChangedByUserID] [int] NOT NULL CONSTRAINT [DF_tbl_Device_ChangedByUserID]  DEFAULT ((0)),
	[LastChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_Device_LastChangeDate]  DEFAULT (getdate()),
	[LastActivityDate] [datetime] NULL,
	[IPAddress] [nvarchar](50) NULL,
	[MACAddress] [nvarchar](20) NULL,
	[OperatingSystem] [nvarchar](50) NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_Device_CreateDate]  DEFAULT (getdate()),
	[LastTransactionDate] [datetime] NULL
) ON [PRIMARY]
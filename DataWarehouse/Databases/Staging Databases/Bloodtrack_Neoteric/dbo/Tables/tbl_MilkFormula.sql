﻿CREATE TABLE [dbo].[tbl_MilkFormula](
	[FormulaID] [smallint] IDENTITY(1,1) NOT NULL,
	[Abbreviation] [nvarchar](15) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ScannedCode] [nvarchar](1) NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkFormula_StatusID]  DEFAULT (0)
) ON [PRIMARY]
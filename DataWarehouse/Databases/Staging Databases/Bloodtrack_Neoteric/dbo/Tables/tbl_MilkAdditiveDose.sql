﻿CREATE TABLE [dbo].[tbl_MilkAdditiveDose](
	[AdditiveDoseID] [smallint] IDENTITY(1,1) NOT NULL,
	[AdditiveID] [smallint] NOT NULL,
	[Abbreviation] [nvarchar](15) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ScannedCode] [nvarchar](20) NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkAdditiveDose_StatusID]  DEFAULT (0)
) ON [PRIMARY]
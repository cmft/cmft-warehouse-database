﻿CREATE TABLE [dbo].[tbl_BloodUnitFate](
	[BloodUnitFateID] [int] IDENTITY(1,1) NOT NULL,
	[FateCode] [nvarchar](20) NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodUnitFate_StatusID]  DEFAULT (0),
	[ClientType] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodUnitFate_WorkStationType]  DEFAULT (0),
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
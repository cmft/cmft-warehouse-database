﻿CREATE TABLE [dbo].[tbl_AlertType](
	[AlertTypeID] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_AlertType_StatusID]  DEFAULT (0),
	[Details] [nvarchar](256) NULL,
	[Email] [tinyint] NOT NULL CONSTRAINT [DF_tbl_AlertType_Email]  DEFAULT (0),
	[ProductSuiteID] [smallint] NULL CONSTRAINT [DF_tbl_AlertType_ProductSuiteID]  DEFAULT (1),
	[Alternate] [nvarchar](50) NULL,
	[AlternateDetails] [nvarchar](256) NULL
) ON [PRIMARY]
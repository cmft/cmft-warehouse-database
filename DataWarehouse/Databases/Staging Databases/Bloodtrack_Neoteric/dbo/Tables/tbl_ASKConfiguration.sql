﻿CREATE TABLE [dbo].[tbl_ASKConfiguration](
	[PDAAutoAdd] [tinyint] NOT NULL CONSTRAINT [DF_tbl_NUASConfiguration_PDAAutoAdd]  DEFAULT (1),
	[PDADefaultLocationID] [smallint] NOT NULL CONSTRAINT [DF_tbl_NUASConfiguration_PDADefaultLocationID]  DEFAULT (1),
	[DesktopAutoAdd] [tinyint] NOT NULL CONSTRAINT [DF_tbl_NUASConfiguration_DesktopAutoAdd]  DEFAULT (1),
	[DesktopDefaultLocationID] [smallint] NOT NULL CONSTRAINT [DF_tbl_NUASConfiguration_DesktopDefaultLocationID]  DEFAULT (2),
	[UserAutoAdd] [tinyint] NOT NULL CONSTRAINT [DF_tbl_NUASConfiguration_UserAutoAdd]  DEFAULT (1),
	[UserDefaultGroupID] [int] NOT NULL CONSTRAINT [DF_tbl_NUASConfiguration_UserDefaultGroupID]  DEFAULT (2),
	[ServerAutoAdd] [tinyint] NOT NULL CONSTRAINT [DF_tbl_ASKConfiguration_ServerAutoAdd]  DEFAULT (1),
	[ServerDefaultLocationID] [smallint] NOT NULL CONSTRAINT [DF_tbl_ASKConfiguration_ServerDefaultLocationID]  DEFAULT (3),
	[AuditUserActivity] [tinyint] NOT NULL CONSTRAINT [DF_tbl_ASKConfiguration_AuditUserActivity]  DEFAULT (0),
	[ChangedByUserID] [int] NOT NULL CONSTRAINT [DF__tbl_ASKCo__Chang__1328BA3B]  DEFAULT (0),
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF__tbl_ASKCo__Chang__114071C9]  DEFAULT (getdate()),
	[TrainingEnabled] [tinyint] NOT NULL CONSTRAINT [DF_tbl_ASKConfiguration_TrainingEnabled]  DEFAULT (1)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_ConfigProfile](
	[ConfigProfileID] [int] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_ConfigSet_StatusID]  DEFAULT (0),
	[LastChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_ConfigSet_LastChangeDate]  DEFAULT (getdate()),
	[ProductID] [smallint] NOT NULL,
	[DefaultProfile] [tinyint] NOT NULL DEFAULT (0)
) ON [PRIMARY]
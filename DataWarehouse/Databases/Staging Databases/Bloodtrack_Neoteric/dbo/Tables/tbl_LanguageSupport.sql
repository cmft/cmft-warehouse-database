﻿CREATE TABLE [dbo].[tbl_LanguageSupport](
	[LanguageID] [smallint] NOT NULL,
	[Language] [nvarchar](50) NOT NULL,
	[LCID] [int] NOT NULL,
	[CountryCode] [varchar](4) NOT NULL,
	[CultureName] [nvarchar](50) NOT NULL,
	[CultureIdentifier] [int] NOT NULL,
	[RegionID] [smallint] NOT NULL,
	[Supported] [tinyint] NOT NULL CONSTRAINT [DF_tbl_LanguageSupport_Supported]  DEFAULT ((0))
) ON [PRIMARY]
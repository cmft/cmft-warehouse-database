﻿CREATE TABLE [dbo].[tbl_Patch](
	[PatchID] [int] IDENTITY(0,1) NOT NULL,
	[Item] [nvarchar](10) NOT NULL,
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_Patch_ChangeDate]  DEFAULT (getdate())
) ON [PRIMARY]
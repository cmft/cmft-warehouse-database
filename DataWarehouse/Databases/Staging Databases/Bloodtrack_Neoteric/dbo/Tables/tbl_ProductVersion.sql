﻿CREATE TABLE [dbo].[tbl_ProductVersion](
	[ProductVersionID] [int] IDENTITY(0,1) NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[Version] [nvarchar](50) NOT NULL
) ON [PRIMARY]
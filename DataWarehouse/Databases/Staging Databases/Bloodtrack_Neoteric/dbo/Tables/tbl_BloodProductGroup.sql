﻿CREATE TABLE [dbo].[tbl_BloodProductGroup](
	[BloodProductGroupID] [int] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_StatusID]  DEFAULT (0),
	[BloodProductRuleID] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[PickupProduct] [smallint] NULL,
	[CheckCMVNegative] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_CheckCMVNegative]  DEFAULT (0),
	[CheckIrradiated] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_CheckIrradiated]  DEFAULT (0),
	[CheckLeukoreduced] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_CheckLeukoreduced]  DEFAULT (0),
	[BloodGroupTypeID] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_BloodGroupType]  DEFAULT ((0)),
	[BatchProduct] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_BatchProduct]  DEFAULT ((0)),
	[BPEXM] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_BPEXM]  DEFAULT ((0)),
	[BPST] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_BPST]  DEFAULT ((0)),
	[BPFT] [tinyint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_BPFT]  DEFAULT ((0)),
	[RemoteAllocationTypeID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodProductGroup_RemoteAllocation]  DEFAULT ((0)),
	[LISCode] [nvarchar](20) NULL
) ON [PRIMARY]
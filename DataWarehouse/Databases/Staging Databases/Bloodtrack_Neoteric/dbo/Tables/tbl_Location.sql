﻿CREATE TABLE [dbo].[tbl_Location](
	[LocationID] [smallint] IDENTITY(0,1) NOT NULL,
	[SiteID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Location_StatusID]  DEFAULT (0),
	[LocationName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[LISCode] [nvarchar](50) NULL,
	[ChangedByUserID] [int] NOT NULL,
	[LastChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_Location_LocationChangedDate]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_Product](
	[ProductID] [smallint] NOT NULL,
	[ProductTypeID] [smallint] NOT NULL,
	[ProductName] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_Product_StatusID]  DEFAULT (0),
	[Description] [nvarchar](50) NULL,
	[ProductSuiteID] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_NPSConfiguration](
	[PrintLabelID] [int] IDENTITY(1,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_NPSConfiguration_StatusID]  DEFAULT (0),
	[DeviceID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LabelTypeID] [int] NOT NULL,
	[PrintProvider] [nvarchar](50) NOT NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[DateFormatID] [smallint] NOT NULL,
	[TimeFormatID] [smallint] NOT NULL,
	[Copies] [smallint] NOT NULL CONSTRAINT [DF_tbl_NPSConfiguration_Copies]  DEFAULT (1)
) ON [PRIMARY]
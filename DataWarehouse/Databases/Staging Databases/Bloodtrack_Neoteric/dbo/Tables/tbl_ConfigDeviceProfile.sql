﻿CREATE TABLE [dbo].[tbl_ConfigDeviceProfile](
	[ConfigDeviceProfileID] [int] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_ConfigDeviceProductGroup_StatusID]  DEFAULT (0),
	[DeviceID] [int] NOT NULL,
	[ConfigProfileID] [int] NULL,
	[LastRetrievalDate] [datetime] NULL,
	[LoggingLevel] [smallint] NOT NULL CONSTRAINT [DF_tbl_ConfigDeviceProfile_LoggingLevel]  DEFAULT (2),
	[LogFileSize] [int] NOT NULL CONSTRAINT [DF_tbl_ConfigDeviceProfile_LogFileSize]  DEFAULT (5)
) ON [PRIMARY]
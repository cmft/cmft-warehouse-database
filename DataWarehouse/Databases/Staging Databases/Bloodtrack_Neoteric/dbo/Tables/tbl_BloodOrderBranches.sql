﻿CREATE TABLE [dbo].[tbl_BloodOrderBranches](
	[BranchID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_BloodOrderBranches_StatusID]  DEFAULT (0),
	[BranchName] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_TransportType](
	[TransportTypeID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
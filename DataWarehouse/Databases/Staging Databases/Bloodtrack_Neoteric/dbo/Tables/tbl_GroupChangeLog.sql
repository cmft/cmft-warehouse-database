﻿CREATE TABLE [dbo].[tbl_GroupChangeLog](
	[GroupChangeID] [int] IDENTITY(1,1) NOT NULL,
	[ChangedByUserID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_GroupChangeLog_StatusID]  DEFAULT (0),
	[PasswordRequired] [tinyint] NOT NULL,
	[PasswordValidPeriod] [smallint] NULL,
	[MinimumPasswordLength] [tinyint] NOT NULL,
	[TokenValidPeriod] [smallint] NOT NULL,
	[Description] [nvarchar](50) NULL,
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_GroupChangeLog_GroupChangeDate]  DEFAULT (getdate()),
	[GroupLevel] [smallint] NULL,
	[SiteID] [smallint] NULL,
	[InvalidPINCount] [smallint] NULL
) ON [PRIMARY]
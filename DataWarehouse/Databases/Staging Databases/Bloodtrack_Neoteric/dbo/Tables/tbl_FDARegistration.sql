﻿CREATE TABLE [dbo].[tbl_FDARegistration](
	[RegistrationNumber] [nvarchar](7) NOT NULL,
	[Prefix] [nvarchar](3) NOT NULL,
	[Description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
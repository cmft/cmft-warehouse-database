﻿CREATE TABLE [dbo].[tbl_DonationType](
	[DonationTypeID] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
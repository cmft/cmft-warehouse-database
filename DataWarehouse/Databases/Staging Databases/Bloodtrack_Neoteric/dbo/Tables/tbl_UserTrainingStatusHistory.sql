﻿CREATE TABLE [dbo].[tbl_UserTrainingStatusHistory](
	[UserTrainingStatusHistoryID] [smallint] IDENTITY(0,1) NOT NULL,
	[UserTrainingStatusID] [int] NOT NULL,
	[TrainingStatusID] [smallint] NOT NULL,
	[RecordedDate] [datetime] NOT NULL,
	[RecordedByUserID] [int] NOT NULL,
	[TrainingDate] [datetime] NULL,
	[TrainingExpiryDate] [datetime] NULL,
	[UserID] [int] NULL,
	[Comments] [nvarchar](255) NULL,
	[UserComponentTrainingStatusHistoryID] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_MilkTransactionFormula](
	[TransactionFormulaID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[OrderPartID] [smallint] NOT NULL,
	[FormulaID] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_UserTrainingHistory](
	[UserTrainingHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[DeviceID] [int] NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[TransactionTypeID] [smallint] NOT NULL,
	[Duration] [smallint] NULL,
	[TrainingDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_UserTrainingHistory_TrainingDate]  DEFAULT (getdate()),
	[ServerDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_UserTrainingHistory_ServerDate]  DEFAULT (getdate())
) ON [PRIMARY]
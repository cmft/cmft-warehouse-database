﻿CREATE TABLE [dbo].[tbl_ReminderList](
	[ReminderListID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_ReminderListNames_StatusID]  DEFAULT (0)
) ON [PRIMARY]
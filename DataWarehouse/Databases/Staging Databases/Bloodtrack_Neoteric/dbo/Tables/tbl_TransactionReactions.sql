﻿CREATE TABLE [dbo].[tbl_TransactionReactions](
	[TransactionReactionID] [int] IDENTITY(0,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[ReactionID] [int] NOT NULL
) ON [PRIMARY]
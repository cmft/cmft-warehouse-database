﻿CREATE TABLE [dbo].[tbl_UserChangeLog](
	[UserChangeID] [int] IDENTITY(1,1) NOT NULL,
	[ChangedByUserID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[GroupID] [int] NULL,
	[StatusID] [smallint] NULL CONSTRAINT [DF_tbl_UserChangeLog_StatusID]  DEFAULT (0),
	[AlternateKey] [nvarchar](50) NULL,
	[Designation] [nvarchar](50) NULL,
	[StaffNumber] [nvarchar](50) NULL,
	[UserName] [nvarchar](60) NOT NULL,
	[PasswordHash] [nvarchar](64) NULL,
	[LastName] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_UserChangeLog_UserChangeDate]  DEFAULT (getdate())
) ON [PRIMARY]
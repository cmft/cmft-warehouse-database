﻿CREATE TABLE [dbo].[tbl_BloodAttributeRules](
	[CMVNegativeCost] [tinyint] NOT NULL,
	[IrradiatedCost] [tinyint] NOT NULL,
	[LeukoReducedCost] [tinyint] NOT NULL,
	[CMVNegativeSubstitution] [tinyint] NOT NULL,
	[IrradiatedSubstitution] [tinyint] NOT NULL,
	[LeukoReducedSubstitution] [tinyint] NOT NULL
) ON [PRIMARY]
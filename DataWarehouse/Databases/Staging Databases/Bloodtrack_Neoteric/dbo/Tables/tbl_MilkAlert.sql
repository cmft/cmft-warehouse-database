﻿CREATE TABLE [dbo].[tbl_MilkAlert](
	[AlertID] [int] IDENTITY(1,1) NOT NULL,
	[AlertTypeID] [int] NOT NULL,
	[AlertDate] [datetime] NOT NULL,
	[TransactionID] [int] NULL,
	[AcknowledgeUserID] [int] NULL,
	[AcknowledgeDate] [datetime] NULL,
	[ResolutionUserID] [int] NULL,
	[ResolutionDate] [datetime] NULL,
	[ResolutionID] [int] NULL,
	[Description] [nvarchar](255) NULL,
	[DeviceID] [int] NULL,
	[SiteID] [smallint] NULL,
	[ProductID] [smallint] NULL
) ON [PRIMARY]
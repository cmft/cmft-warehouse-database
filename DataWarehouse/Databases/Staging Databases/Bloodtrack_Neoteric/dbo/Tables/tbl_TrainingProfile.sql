﻿CREATE TABLE [dbo].[tbl_TrainingProfile](
	[TrainingProfileID] [smallint] IDENTITY(0,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[TrainingProductID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NULL CONSTRAINT [DF_tbl_GroupTrainingComponents_RetrainingRequired]  DEFAULT (1)
) ON [PRIMARY]
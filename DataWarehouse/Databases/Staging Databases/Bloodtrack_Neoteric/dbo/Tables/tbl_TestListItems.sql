﻿CREATE TABLE [dbo].[tbl_TestListItems](
	[TestsListItemID] [int] IDENTITY(1,1) NOT NULL,
	[TestID] [int] NOT NULL,
	[TestListID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_TestsList_StatusID]  DEFAULT (0)
) ON [PRIMARY]
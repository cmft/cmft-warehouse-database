﻿CREATE TABLE [dbo].[tbl_ProductPatientConfiguration](
	[ProductSuiteID] [smallint] NOT NULL,
	[NumericPatientID] [tinyint] NOT NULL,
	[PartialNameMatch] [tinyint] NOT NULL,
	[PatientMatchNumberOnly] [tinyint] NOT NULL,
	[PatientGenderRequired] [tinyint] NOT NULL,
	[PatientBirthDateRequired] [tinyint] NOT NULL,
	[PatientFirstNameRequired] [tinyint] NOT NULL,
	[PatientNumber1Name] [nvarchar](50) NULL,
	[PatientNumber2Name] [nvarchar](50) NULL,
	[PatientNumber3Name] [nvarchar](50) NULL,
	[PartialNumberMatch] [tinyint] NOT NULL CONSTRAINT [DF_tbl_ProductPatientConfiguration_PartialMatch]  DEFAULT ((1)),
	[PatientConfirmation] [tinyint] NOT NULL CONSTRAINT [DF_tbl_ProductPatientConfiguration_PatientConfirmation]  DEFAULT ((0))
) ON [PRIMARY]
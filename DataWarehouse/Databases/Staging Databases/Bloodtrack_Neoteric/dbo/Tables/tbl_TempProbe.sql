﻿CREATE TABLE [dbo].[tbl_TempProbe](
	[TempProbeID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceID] [int] NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[TempProbeTypeID] [smallint] NOT NULL,
	[COMPort] [smallint] NULL
) ON [PRIMARY]
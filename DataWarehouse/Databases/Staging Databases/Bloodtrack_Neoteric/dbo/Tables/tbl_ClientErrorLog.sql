﻿CREATE TABLE [dbo].[tbl_ClientErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceID] [int] NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[UserID] [int] NULL,
	[LogTypeID] [smallint] NOT NULL,
	[LogLevel] [smallint] NULL,
	[Class] [nvarchar](50) NULL,
	[Method] [nvarchar](50) NULL,
	[Description] [nvarchar](1024) NULL,
	[ErrorDate] [datetime] NULL,
	[ServerDate] [datetime] NOT NULL,
	[Version] [nvarchar](10) NULL,
	[BuildDate] [datetime] NULL
) ON [PRIMARY]
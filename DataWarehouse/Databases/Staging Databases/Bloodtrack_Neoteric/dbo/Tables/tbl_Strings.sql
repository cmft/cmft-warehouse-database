﻿CREATE TABLE [dbo].[tbl_Strings](
	[StringID] [int] IDENTITY(0,1) NOT NULL,
	[English] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
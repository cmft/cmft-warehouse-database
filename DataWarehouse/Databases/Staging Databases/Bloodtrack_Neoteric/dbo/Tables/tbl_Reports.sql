﻿CREATE TABLE [dbo].[tbl_Reports](
	[ReportID] [int] IDENTITY(1000,1) NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[ReportName] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[InputTypeID] [smallint] NULL,
	[URL] [nvarchar](128) NOT NULL,
	[XMLInfo] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
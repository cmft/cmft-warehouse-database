﻿CREATE TABLE [dbo].[tbl_BTSConfigurationChangeLog](
	[ChangeLogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[ChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_BTSConfigurationChangeLog_ChangeDate]  DEFAULT (getdate())
) ON [PRIMARY]
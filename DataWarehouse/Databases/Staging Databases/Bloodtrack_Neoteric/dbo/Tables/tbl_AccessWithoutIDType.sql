﻿CREATE TABLE [dbo].[tbl_AccessWithoutIDType](
	[AccessWithoutID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_AccessWithoutIDType_StatusID]  DEFAULT (1),
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
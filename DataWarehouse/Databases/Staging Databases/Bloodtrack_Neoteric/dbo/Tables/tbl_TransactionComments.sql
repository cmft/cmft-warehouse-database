﻿CREATE TABLE [dbo].[tbl_TransactionComments](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[CommentedTransactionID] [int] NOT NULL,
	[TransactionComment] [nvarchar](1024) NULL,
	[ProductSuiteID] [smallint] NOT NULL CONSTRAINT [DF_tbl_TransactionComments_ProductSuite]  DEFAULT ((1))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_TrainingConfigurationHistory](
	[TrainingConfigurationHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[ChangeType] [smallint] NOT NULL,
	[RecordID] [int] NOT NULL,
	[ChangeUserID] [int] NOT NULL,
	[ChangeDate] [datetime] NOT NULL,
	[ChangeHasApplied] [smallint] NOT NULL CONSTRAINT [DF_tbl_TrainingConfigurationHistory_ChangeHasApplied]  DEFAULT ((0)),
	[ModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
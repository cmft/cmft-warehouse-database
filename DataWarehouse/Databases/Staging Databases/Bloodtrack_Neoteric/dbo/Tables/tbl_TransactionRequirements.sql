﻿CREATE TABLE [dbo].[tbl_TransactionRequirements](
	[TransactionRequirementID] [int] IDENTITY(0,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[RequirementID] [int] NOT NULL
) ON [PRIMARY]
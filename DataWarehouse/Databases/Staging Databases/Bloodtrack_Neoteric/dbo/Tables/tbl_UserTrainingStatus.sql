﻿CREATE TABLE [dbo].[tbl_UserTrainingStatus](
	[UserTrainingStatusID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[TrainingDate] [datetime] NULL CONSTRAINT [DF_tbl_UserTrainingLog_TrainingDate]  DEFAULT (getdate()),
	[TrainingExpiryDate] [datetime] NULL,
	[TrainingStatusID] [smallint] NULL,
	[RecordedByUserID] [int] NOT NULL,
	[Comments] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_MilkUnit](
	[MilkUnitID] [int] IDENTITY(1,1) NOT NULL,
	[UnitNumber] [nvarchar](30) NOT NULL,
	[ContentID] [smallint] NULL,
	[MilkUnitTypeID] [smallint] NULL,
	[OrderID] [int] NULL,
	[PatientID] [int] NULL,
	[TransactionID] [int] NULL,
	[CreationDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[StateID] [tinyint] NOT NULL,
	[ConditionID] [tinyint] NOT NULL,
	[FateID] [tinyint] NULL
) ON [PRIMARY]
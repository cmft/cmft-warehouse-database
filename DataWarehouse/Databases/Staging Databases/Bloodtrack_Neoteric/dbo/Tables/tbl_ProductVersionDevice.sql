﻿CREATE TABLE [dbo].[tbl_ProductVersionDevice](
	[ProductVersionDeviceID] [int] IDENTITY(0,1) NOT NULL,
	[ProductVersionID] [int] NOT NULL,
	[ProductID] [smallint] NOT NULL,
	[DeviceID] [int] NOT NULL,
	[Version] [nvarchar](20) NOT NULL
) ON [PRIMARY]
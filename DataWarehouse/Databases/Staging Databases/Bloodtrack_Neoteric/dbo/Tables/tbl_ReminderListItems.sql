﻿CREATE TABLE [dbo].[tbl_ReminderListItems](
	[ReminderListItemID] [int] IDENTITY(1,1) NOT NULL,
	[ReminderListID] [smallint] NOT NULL,
	[ReminderID] [int] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_ReminderList_StatusID]  DEFAULT (0)
) ON [PRIMARY]
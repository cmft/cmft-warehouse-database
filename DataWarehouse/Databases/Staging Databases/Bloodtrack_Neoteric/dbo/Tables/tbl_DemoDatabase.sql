﻿CREATE TABLE [dbo].[tbl_DemoDatabase](
	[DemoDatabase] [tinyint] NOT NULL CONSTRAINT [DF_tbl_DemoDatabase_DemoDatabase]  DEFAULT (0),
	[DemoCount] [tinyint] NOT NULL CONSTRAINT [DF_tbl_DemoDatabase_DemoCount]  DEFAULT (0)
) ON [PRIMARY]
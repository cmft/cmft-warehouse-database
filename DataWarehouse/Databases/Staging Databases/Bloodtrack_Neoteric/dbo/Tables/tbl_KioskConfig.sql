﻿CREATE TABLE [dbo].[tbl_KioskConfig](
	[KioskConfigID] [int] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_CourierConfig_StatusID]  DEFAULT (0),
	[DeviceID] [int] NOT NULL CONSTRAINT [DF_tbl_CourierConfig_DeviceID]  DEFAULT (0),
	[StorageDeviceID] [int] NOT NULL,
	[LastChangeDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_CourierConfig_LastChangedDate]  DEFAULT (getdate()),
	[DoorLockPort] [tinyint] NULL DEFAULT (4)
) ON [PRIMARY]
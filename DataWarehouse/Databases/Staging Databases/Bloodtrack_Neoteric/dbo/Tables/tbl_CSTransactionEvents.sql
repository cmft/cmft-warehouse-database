﻿CREATE TABLE [dbo].[tbl_CSTransactionEvents](
	[TransactionEventID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[EventID] [int] NOT NULL,
	[EventIDText] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_CSTransaction](
	[CSTransactionID] [int] IDENTITY(0,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[ProcedureID] [nvarchar](50) NOT NULL,
	[ProcedureStartDate] [datetime] NOT NULL,
	[ProcedureDuration] [nvarchar](50) NOT NULL,
	[TopLevelSWVersion] [nvarchar](50) NULL,
	[SurgeryType] [nvarchar](50) NULL,
	[SurgeonID] [int] NOT NULL,
	[OperatorID] [nvarchar](50) NOT NULL
) ON [PRIMARY]
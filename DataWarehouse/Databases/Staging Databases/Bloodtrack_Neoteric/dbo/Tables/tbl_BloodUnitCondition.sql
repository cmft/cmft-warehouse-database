﻿CREATE TABLE [dbo].[tbl_BloodUnitCondition](
	[ConditionID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
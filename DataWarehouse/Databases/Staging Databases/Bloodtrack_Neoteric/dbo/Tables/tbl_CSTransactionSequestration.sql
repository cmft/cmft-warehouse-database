﻿CREATE TABLE [dbo].[tbl_CSTransactionSequestration](
	[TransactionSequestrationID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[ActiveSeqSettingsGroup] [nvarchar](50) NOT NULL,
	[SeqStartDate] [datetime] NULL,
	[SeqDuration] [nvarchar](50) NULL,
	[SeqTotalPPP] [int] NULL,
	[SeqTotalPRP] [int] NULL,
	[SeqTotalProcessed] [int] NULL,
	[SeqTotalReinfusion] [int] NULL
) ON [PRIMARY]
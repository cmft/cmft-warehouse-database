﻿CREATE TABLE [dbo].[tbl_CourierStatus](
	[CourierStatusID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceID] [int] NOT NULL,
	[StorageDeviceID] [int] NULL,
	[LastHeardDate] [datetime] NULL,
	[LabelCount] [int] NULL,
	[RemoteRelease] [tinyint] NOT NULL CONSTRAINT [DF_tbl_CourierStatus_RemoteRelease]  DEFAULT ((0)),
	[OfflineAlert] [tinyint] NOT NULL CONSTRAINT [DF_tbl_CourierStatus_OfflineAlert]  DEFAULT ((0)),
	[LabelAlert] [smallint] NOT NULL CONSTRAINT [DF_tbl_CourierStatus_LabelAlert]  DEFAULT ((0)),
	[TempMonitoring] [tinyint] NOT NULL CONSTRAINT [DF_tbl_CourierStatus_TempMonitoring]  DEFAULT ((0)),
	[TempAlert] [tinyint] NOT NULL CONSTRAINT [DF_tbl_CourierStatus_TempAlert]  DEFAULT ((0)),
	[LastTemp] [smallint] NULL,
	[TempStatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_CourierStatus_TempStatusID_1]  DEFAULT ((0)),
	[MonitorType] [tinyint] NULL CONSTRAINT [DF_tbl_CourierStatus_MonitorType_1]  DEFAULT ((0))
) ON [PRIMARY]
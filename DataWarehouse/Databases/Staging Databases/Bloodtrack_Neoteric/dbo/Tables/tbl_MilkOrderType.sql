﻿CREATE TABLE [dbo].[tbl_MilkOrderType](
	[OrderTypeID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[DisplaySeqNr] [tinyint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkOrderType_StatusID]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_UserAccessLog](
	[UserAccessLogID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[UserName] [nvarchar](60) NULL,
	[UserAccessTypeID] [smallint] NOT NULL,
	[DeviceName] [nvarchar](50) NULL,
	[ProductID] [smallint] NULL,
	[ResponseCodeID] [int] NULL,
	[AccessDate] [datetime] NOT NULL CONSTRAINT [DF_tbl_UserAccessLog_AccessDate]  DEFAULT (getdate()),
	[LocationID] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_rptTrainingDetailed](
	[RowID] [smallint] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[GroupID] [smallint] NULL,
	[TrainingProductID] [smallint] NULL
) ON [PRIMARY]
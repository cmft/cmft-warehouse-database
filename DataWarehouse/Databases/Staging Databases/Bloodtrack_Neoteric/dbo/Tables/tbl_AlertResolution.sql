﻿CREATE TABLE [dbo].[tbl_AlertResolution](
	[ResolutionID] [int] IDENTITY(0,1) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_AlertResolution_StatusID]  DEFAULT (0),
	[Description] [nvarchar](256) NOT NULL,
	[IsUserComment] [tinyint] NOT NULL CONSTRAINT [DF_tbl_AlertResolution_FreeText]  DEFAULT (0),
	[ProductSuiteID] [smallint] NOT NULL CONSTRAINT [DF_tbl_AlertResolution_ProductSuiteID]  DEFAULT (1),
	[Alternate] [nvarchar](256) NULL
) ON [PRIMARY]
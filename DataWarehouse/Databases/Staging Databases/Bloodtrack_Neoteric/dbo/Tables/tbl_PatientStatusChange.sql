﻿CREATE TABLE [dbo].[tbl_PatientStatusChange](
	[TransactionID] [int] NOT NULL,
	[PatientID] [int] NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[ExpiryDate] [datetime] NULL,
	[UnitCount] [smallint] NULL,
	[BloodGroupID] [smallint] NULL,
	[CMVNegative] [tinyint] NULL,
	[Irradiated] [tinyint] NULL,
	[LeukoReduced] [tinyint] NULL
) ON [PRIMARY]
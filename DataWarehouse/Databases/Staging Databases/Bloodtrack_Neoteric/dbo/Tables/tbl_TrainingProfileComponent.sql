﻿CREATE TABLE [dbo].[tbl_TrainingProfileComponent](
	[TrainingProfileComponentID] [smallint] IDENTITY(1,1) NOT NULL,
	[TrainingProfileID] [smallint] NOT NULL,
	[TrainingComponentID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_TempProfile](
	[TempProfileID] [int] IDENTITY(1,1) NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[DeviceID] [int] NOT NULL,
	[StorageDeviceID] [int] NOT NULL,
	[SensorID] [smallint] NOT NULL,
	[MaxTempC] [smallint] NOT NULL,
	[MinTempC] [smallint] NOT NULL,
	[TempChangeC] [smallint] NOT NULL,
	[MaxTempF] [smallint] NOT NULL,
	[MinTempF] [smallint] NOT NULL,
	[TempChangeF] [smallint] NOT NULL,
	[LocalAlarm] [tinyint] NOT NULL,
	[Channel] [tinyint] NULL,
	[Offset] [smallint] NOT NULL
) ON [PRIMARY]
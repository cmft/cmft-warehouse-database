﻿CREATE TABLE [dbo].[tbl_MilkAdditive](
	[AdditiveID] [smallint] IDENTITY(1,1) NOT NULL,
	[Abbreviation] [nvarchar](15) NOT NULL,
	[Description] [nvarchar](30) NOT NULL,
	[UsedInBreastMilk] [tinyint] NOT NULL,
	[UsedInDonorMilk] [tinyint] NOT NULL,
	[UsedInFormula] [tinyint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkAdditive_StatusID]  DEFAULT (0)
) ON [PRIMARY]
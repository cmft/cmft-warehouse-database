﻿CREATE TABLE [dbo].[tbl_DeviceType](
	[DeviceTypeID] [smallint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_DeviceType_StatusID]  DEFAULT ((0)),
	[DeviceClassID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_SitePrinter](
	[SitePrinterID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [smallint] NOT NULL,
	[BloodProductGroupID] [int] NULL,
	[LabelTypeID] [int] NOT NULL,
	[PrintLabelID] [int] NOT NULL,
	[LabelEventID] [int] NOT NULL,
	[DefaultPrinter] [tinyint] NOT NULL
) ON [PRIMARY]
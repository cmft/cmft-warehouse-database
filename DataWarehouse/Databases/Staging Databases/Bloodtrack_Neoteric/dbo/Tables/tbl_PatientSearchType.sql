﻿CREATE TABLE [dbo].[tbl_PatientSearchType](
	[PatientSearchTypeID] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
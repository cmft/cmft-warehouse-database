﻿CREATE TABLE [dbo].[tbl_VitalSigns](
	[VitalSignID] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
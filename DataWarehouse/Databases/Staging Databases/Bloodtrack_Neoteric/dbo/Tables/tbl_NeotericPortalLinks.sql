﻿CREATE TABLE [dbo].[tbl_NeotericPortalLinks](
	[NeotericPortalLinkID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[FilePath] [nvarchar](255) NOT NULL,
	[StatusID] [smallint] NOT NULL,
	[Version] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tbl_CSTransactionSuctionMode](
	[TransactionSmartSuctionID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[SuctionMode] [nvarchar](50) NULL,
	[SuctionModeStart] [datetime] NULL,
	[SuctionModeDuration] [nvarchar](50) NULL
) ON [PRIMARY]
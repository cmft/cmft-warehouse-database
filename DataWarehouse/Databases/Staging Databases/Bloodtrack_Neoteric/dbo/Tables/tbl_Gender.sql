﻿CREATE TABLE [dbo].[tbl_Gender](
	[GenderID] [smallint] NOT NULL,
	[GenderCode] [nvarchar](10) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[Alternate] [nvarchar](50) NULL,
	[AlternateCode] [nvarchar](50) NULL
) ON [PRIMARY]
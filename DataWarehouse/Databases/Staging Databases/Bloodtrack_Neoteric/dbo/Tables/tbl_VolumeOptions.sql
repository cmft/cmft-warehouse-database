﻿CREATE TABLE [dbo].[tbl_VolumeOptions](
	[VolumeOptionsID] [smallint] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_VolumeOptions_StatusID]  DEFAULT (0)
) ON [PRIMARY]
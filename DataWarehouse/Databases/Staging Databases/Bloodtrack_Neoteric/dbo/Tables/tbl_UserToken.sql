﻿CREATE TABLE [dbo].[tbl_UserToken](
	[TokenID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[DeviceID] [int] NOT NULL,
	[TokenExpiryDate] [datetime] NULL,
	[ProductID] [smallint] NULL
) ON [PRIMARY]
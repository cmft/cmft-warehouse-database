﻿CREATE TABLE [dbo].[tbl_SafeTxProfile](
	[ProfileID] [int] NOT NULL,
	[StatusID] [smallint] NULL CONSTRAINT [DF_tbl_PocketPCProfile_StatusID]  DEFAULT (0),
	[CollectTestListID] [smallint] NULL,
	[AdvisorTestListID] [smallint] NULL,
	[PickupReminderListID] [smallint] NULL,
	[CollectReminderListID] [smallint] NULL
) ON [PRIMARY]
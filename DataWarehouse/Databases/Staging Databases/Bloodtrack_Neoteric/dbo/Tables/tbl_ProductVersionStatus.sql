﻿CREATE TABLE [dbo].[tbl_ProductVersionStatus](
	[StatusID] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
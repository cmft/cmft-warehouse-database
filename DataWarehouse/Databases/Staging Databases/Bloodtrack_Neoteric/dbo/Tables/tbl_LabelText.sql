﻿CREATE TABLE [dbo].[tbl_LabelText](
	[LabelTextID] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
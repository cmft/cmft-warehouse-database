﻿CREATE TABLE [dbo].[tbl_MilkComponent](
	[ComponentID] [smallint] IDENTITY(1,1) NOT NULL,
	[OrderTypeID] [smallint] NOT NULL,
	[ContentID] [smallint] NOT NULL,
	[FormulaID] [smallint] NOT NULL,
	[DisplaySeqNr] [tinyint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkOrderComponent_StatusID]  DEFAULT (0),
	[Abbreviation] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NOT NULL
) ON [PRIMARY]
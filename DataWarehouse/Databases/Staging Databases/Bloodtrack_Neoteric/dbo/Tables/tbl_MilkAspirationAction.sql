﻿CREATE TABLE [dbo].[tbl_MilkAspirationAction](
	[ActionID] [tinyint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkAspirationAction_StatusID]  DEFAULT (0),
	[Alternate] [nvarchar](50) NULL
) ON [PRIMARY]
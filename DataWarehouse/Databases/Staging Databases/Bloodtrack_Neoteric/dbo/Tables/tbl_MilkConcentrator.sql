﻿CREATE TABLE [dbo].[tbl_MilkConcentrator](
	[ConcentratorID] [smallint] IDENTITY(1,1) NOT NULL,
	[OrderTypeID] [smallint] NOT NULL,
	[AdditiveDoseID] [smallint] NOT NULL,
	[FormulaID] [smallint] NOT NULL,
	[DisplaySeqNr] [tinyint] NOT NULL,
	[StatusID] [smallint] NOT NULL CONSTRAINT [DF_tbl_MilkOrderConcentrator_StatusID]  DEFAULT (0),
	[Abbreviation] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NOT NULL
) ON [PRIMARY]
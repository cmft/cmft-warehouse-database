﻿CREATE TABLE [Inquire].[OCMFLOCATION](
	[PerformingLoc] [varchar](4) NOT NULL,
	[Description] [varchar](20) NULL,
	[MfRecStsInd] [bit] NULL
) ON [PRIMARY]
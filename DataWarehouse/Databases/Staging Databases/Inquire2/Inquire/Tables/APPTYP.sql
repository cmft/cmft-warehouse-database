﻿CREATE TABLE [Inquire].[APPTYP](
	[AppointmentTypeCode] [varchar](3) NOT NULL,
	[AppointmentTypeDesc] [varchar](20) NOT NULL,
	[ApptCategory] [varchar](3) NOT NULL,
	[ApptClassification] [varchar](1) NOT NULL,
	[Ebooking] [varchar](3) NOT NULL, -- AS (case when [eBookingInt]=(0) then 'NO' when [eBookingInt]=(1) then 'YES'  end),
	[EBookingInt] [bit] NULL,
	[NewRttPeriodInt] [bit] NULL,
	[ReminderLetterInt] [bit] NULL,
	[UrgentInt] [bit] NOT NULL,
	[SchedulingAppointmentClassification] [tinyint] NULL,
	[NewRttPeriod] [varchar](3) NOT NULL, --  AS (case when [NewRttPeriodInt]=(0) then 'NO' when [NewRttPeriodInt]=(1) then 'YES'  end),
	[ReminderLetter] [varchar](3) NOT NULL, --  AS (case when [ReminderLetterInt]=(0) then 'NO' when [ReminderLetterInt]=(1) then 'YES'  end),
	[Urgent] [varchar](3) NOT NULL, --  AS (case when [UrgentInt]=(0) then 'NO' when [UrgentInt]=(1) then 'YES'  end),
	[SchedulingApptClassificationDescription] [varchar](30) NOT NULL, --  AS (case when [SchedulingAppointmentClassification]=(1) then 'First Appointment' when [SchedulingAppointmentClassification]=(2) then 'Re-attendance'  end),
	[MfRecStsInd] [bit] NOT NULL,
	[APPTYPID] [varchar](3) NOT NULL, --  AS ([AppointmentTypeCode])
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[OPWLEntry](
	[InternalPatientNumber] [int] NOT NULL,
	[EpisodeNumber] [int] NOT NULL,
	[TreatmentCode] [varchar](8) NOT NULL,
	[OpRegDateInt] [bigint] NOT NULL,
	[OPRegDate] [date] NULL,
	[DateOnList] [date] NULL,
	[OpWlDateAppointmentRequiredInternal] [varchar](6) NULL,
	[OpWlDateAppointmentRequiredInternal_YEAR]  AS (case when [OpWlDateAppointmentRequiredInternal] IS NULL then NULL else CONVERT([smallint],left([OpWlDateAppointmentRequiredInternal],(4)),(0)) end),
	[OpWlDateAppointmentRequiredInternal_MONTH]  AS (case when [OpWlDateAppointmentRequiredInternal] IS NULL then NULL else CONVERT([tinyint],right([OpWlDateAppointmentRequiredInternal],(2)),(0)) end),
	[OpWLDateIdxDateOnWaitingListNeutral] [varchar](12) NULL,
	[SchedulingApplicationAreaExternal]  AS ('OUTPAT'),
	[AppointmentType] [varchar](3) NULL,
	[ProcedureType] [varchar](55) NULL,
	[Category] [varchar](3) NULL,
	[ShortNotice] [bit] NULL,
	[TransportCode] [char](1) NULL,
	[Comment] [varchar](55) NULL,
	[Date1stLetterSent] [varchar](10) SPARSE  NULL,
	[Date1stLetterSentInt] [varchar](12) SPARSE  NULL,
	[Date1stReminder] [varchar](10) SPARSE  NULL,
	[Date1stReminderInt] [varchar](8) SPARSE  NULL,
	[Date2ndReminder] [varchar](10) SPARSE  NULL,
	[Date2ndReminderInt] [varchar](8) SPARSE  NULL,
	[PatientResponse] [varchar](10) SPARSE  NULL,
	[PatientResponseInt] [varchar](8) SPARSE  NULL,
	[ResponseComments] [varchar](40) SPARSE  NULL,
	[LastReviewDate] [varchar](12) SPARSE  NULL,
	[LastReviewedUserId] [varchar](3) SPARSE  NULL,
	[opwlentryid] [varchar](max) null -- AS ((((((('OUTPAT||'+[treatmentcode])+'||')+CONVERT([varchar](20),[OpRegDateInt],(0)))+'||')+CONVERT([varchar](20),[internalpatientnumber],(0)))+'||')+CONVERT([varchar](20),[EpisodeNumber],(0)))
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[AMSUID](
	[UserId] [varchar](3) NOT NULL,
	[GpxgpUsername] [varchar](15) NOT NULL,
	[UserIdName] [varchar](30) SPARSE  NULL,
	[UsersHospital] [varchar](4) SPARSE  NULL,
	[UsersDepartment] [varchar](8) SPARSE  NULL,
	[GpUserIdAccountDeletionFlag] [bit] SPARSE  NULL,
	[AMSUIDID] [varchar](max) null --AS ((CONVERT([varchar](10),[UserId],0)+'||')+[gpxgpusername])
) ON [PRIMARY]
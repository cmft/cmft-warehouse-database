﻿CREATE TABLE [Inquire].[OCMTEXTRESULTS](
	[OcmTransKey] [int] NOT NULL,
	[SessionNumber] [int] NOT NULL,
	[ResultCode] [char](4) NOT NULL,
	[AgpSeqNo] [smallint] NOT NULL,
	[Comment] [varchar](max) NULL,
	[SubResultName] [varchar](max) NULL,-- AS (ltrim(rtrim(case when charindex(':',[comment])>(0) AND right([comment],(1))<>':' then left([comment],charindex(':',[comment])-(1))  end))),
	[SubResultValue] [varchar](max) NULL -- AS (ltrim(rtrim(case when charindex(':',[comment])>(0) AND right([comment],(1))<>':' then right([comment],len([comment])-charindex(':',[comment]))  end)))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
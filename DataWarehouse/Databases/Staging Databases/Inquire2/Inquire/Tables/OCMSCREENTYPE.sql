﻿CREATE TABLE [Inquire].[OCMSCREENTYPE](
	[ScreenType] [varchar](4) NOT NULL,
	[ScreenType_Desc] [varchar](20) NULL,
	[Deleted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[RTTPtEpiPathway](
	[InternalPatientNumber] [int] NOT NULL,
	[EpisodeNumber] [int] NOT NULL,
	[PathwayOrgProv] [varchar](5) NULL,
	[PathwayNumber] [varchar](25) NULL,
	[RTTPTEPIPATHWAYID]  AS ((CONVERT([varchar](12),[InternalPatientNumber],0)+'||')+CONVERT([varchar](10),[EpisodeNumber],0))
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[OCCANCEL](
	[OcmTransKey] [int] NOT NULL,
	[SessionNumber] [int] NOT NULL,
	[TransactionNumber] [int] NULL,
	[DateCancelled] [date] NULL,
	[TimeCancelled] [time](7) NULL,
	[EnteredBy] [varchar](3) NULL,
	[Status] [tinyint] NULL,
	[Comment1] [varchar](60) SPARSE  NULL,
	[Comment2] [varchar](60) SPARSE  NULL,
	[Comment3] [varchar](60) SPARSE  NULL,
	[Comment4] [varchar](60) SPARSE  NULL,
	[Comment5] [varchar](60) SPARSE  NULL,
	[Comment6] [varchar](60) SPARSE  NULL,
	[OCCANCELID] [varchar](60) NULL --AS ((CONVERT([varchar](12),[ocmtranskey],(0))+'||')+CONVERT([varchar](12),[sessionnumber],(0)))
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[OCMFDEPT](
	[HospitalCode] [smallint] NOT NULL,
	[Department] [varchar](4) NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Category] [varchar](1) NULL,
	[OCMFDEPTID]  AS ((CONVERT([varchar](4),[HospitalCode],(0))+'||')+[Department])
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[OCMFRESULTS](
	[ResultCd] [varchar](4) NOT NULL,
	[ResultDescription] [varchar](30) NULL,
	[ResultName] [varchar](15) NULL,
	[ResultType] [varchar](14) NULL,
	[UnitOfMeasurement] [varchar](8) NULL,
	[Deleted] [bit] NOT NULL,
	[OCMFRESULTSID]  AS ([ResultCd])
) ON [PRIMARY]
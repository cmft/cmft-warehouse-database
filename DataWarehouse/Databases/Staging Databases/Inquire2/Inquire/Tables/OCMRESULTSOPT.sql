﻿CREATE TABLE [Inquire].[OCMRESULTSOPT](
	[OCMRESULTSOPTID] [varchar](254) NOT NULL,
	[Description] [varchar](30) NULL,
	[FixedCost] [varchar](9) NULL,
	[HospitalCode] [varchar](4) NOT NULL,
	[OptionalResultCd] [varchar](9) NOT NULL,
	[Service] [varchar](7) NOT NULL,
	[VariableCost] [varchar](9) NULL
) ON [PRIMARY]
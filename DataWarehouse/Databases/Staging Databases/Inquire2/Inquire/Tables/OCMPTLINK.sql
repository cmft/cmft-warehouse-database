﻿CREATE TABLE [Inquire].[OCMPTLINK](
	[InternalPatientNumber] [int] NOT NULL,
	[EpisodeNumber] [int] NOT NULL,
	[TransNo] [int] NOT NULL,
	[OcmTransKey] [int] NOT NULL,
	[OCMPTLINKID]  AS ((((CONVERT([varchar](10),[InternalPatientNumber],(0))+'||')+CONVERT([varchar](10),[EpisodeNumber],(0)))+'||')+CONVERT([varchar](10),[TransNo],(0)))
) ON [PRIMARY]
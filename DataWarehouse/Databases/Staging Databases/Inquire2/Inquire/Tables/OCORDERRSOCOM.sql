﻿CREATE TABLE [Inquire].[OCORDERRSOCOM](
	[OCORDERRSOCOMID] [varchar](254) NOT NULL,
	[Comment1] [varchar](60) NULL,
	[Comment2] [varchar](60) NULL,
	[Comment3] [varchar](60) NULL,
	[Comment4] [varchar](60) NULL,
	[Comment5] [varchar](60) NULL,
	[Comment6] [varchar](60) NULL,
	[EpsiodeNumber] [varchar](9) NULL,
	[InternalPatientNumber] [varchar](9) NULL,
	[OcmResultSignOffCommentSequenceNo] [varchar](2) NOT NULL,
	[OcmTransKey] [varchar](9) NOT NULL,
	[ResultSignOffCommentDateInt] [varchar](12) NULL,
	[ResultSignOffCommentsDateTime] [varchar](16) NULL,
	[SessionNumber] [varchar](8) NOT NULL,
	[SignOffCommentUID] [varchar](20) NULL
) ON [PRIMARY]
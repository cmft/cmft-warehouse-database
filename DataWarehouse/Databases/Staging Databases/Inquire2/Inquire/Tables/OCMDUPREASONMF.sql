﻿CREATE TABLE [Inquire].[OCMDUPREASONMF](
	[DuplicateReason] [varchar](4) NOT NULL,
	[Description] [varchar](30) NULL,
	[MfRecStsInd] [bit] NOT NULL,
	[OCMDUPREASONMFID]  AS ([DuplicateReason])
) ON [PRIMARY]
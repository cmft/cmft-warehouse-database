﻿CREATE TABLE [Inquire].[OCORDERRSO](
	[OcmTransKey] [int] NOT NULL,
	[SessionNumber] [int] NOT NULL,
	[InternalPatientNumber] [int] NOT NULL,
	[EpisodeNumber] [int] NOT NULL,
	[ResultSignOffDateTime] [datetime] SPARSE  NULL,
	[SignOffDateTimeInt] [bigint] SPARSE  NULL,
	[SignOffUID] [varchar](20) SPARSE  NULL,
	[OCORDERRSOID] [varchar](100) null -- AS ((CONVERT([varchar](50),[ocmtranskey],(0))+'||')+CONVERT([varchar](50),[sessionnumber],(0)))
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[SPEC](
	[SPECID]  AS ([Specialty]),
	[Description] [varchar](30) NOT NULL,
	[DoHCode] [varchar](4) NULL,
	[HaaCode] [tinyint] NULL,
	[SpecGroup] [varchar](4) NULL,
	[Specialty] [varchar](4) NOT NULL,
	[SpecialtyType] [tinyint] NOT NULL,
	[TreatmentFunction] [bit] NOT NULL,
	[isActive] [bit] NOT NULL
) ON [PRIMARY]
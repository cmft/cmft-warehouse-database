﻿CREATE TABLE [Inquire].[PatData](
	[InternalPatientNumber] [Types].[InternalPatientNumber] NOT NULL,
	[DistrictNumber] [Types].[DistrictNumber] NULL,
	[NHSNumber] [char](12) NULL,
	[NHSNumberInt] [bigint] NULL,
	[NHSNumberStatus] [varchar](2) NULL,
	[OldNHSNumber] [varchar](17) NULL,
	[Sex] [varchar](1) NULL,
	[Sex_1] [varchar](1) NULL,
	[Title] [varchar](5) NULL,
	[Forenames] [varchar](20) NULL,
	[Surname] [varchar](24) NULL,
	[PreferredName] [varchar](20) SPARSE  NULL,
	[PreviousSurname1] [varchar](24) NULL,
	[PreviousSurname2] [varchar](24) SPARSE  NULL,
	[PreviousSurname3] [varchar](24) SPARSE  NULL,
	[PtIdentificationComments] [varchar](30) NULL,
	[PtDoB] [date] NULL,
	[PtDobDayInt] [int] null, -- AS (datepart(day,[PtDoB])),
	[PtDobMonthInt] [int] null, -- AS (datepart(month,[ptDoB])),
	[PtDobYearInt] [int] null, -- AS (datepart(year,[ptDoB])),
	[InternalDateOfBirth] [bigint] NULL,
	[PlaceOfBirth] [varchar](20) NULL,
	[PtDateOfDeath] [date] NULL,
	[PtDateOfDeathInt] [bigint] NULL,
	[PtDeathIndInt] [bit] NULL,
	[PtDeathComment1] [varchar](60) SPARSE  NULL,
	[PtDeathComment2] [varchar](60) SPARSE  NULL,
	[PtDeathInformedBy] [varchar](60) SPARSE  NULL,
	[PtDeathRecUserId] [varchar](3) SPARSE  NULL,
	[PtDeathRecordedDt] [varchar](16) SPARSE  NULL,
	[PtDeathRecordedDtInt] [varchar](12) SPARSE  NULL,
	[PtAddrLine1] [varchar](20) NULL,
	[PtAddrLine2] [varchar](20) NULL,
	[PtAddrLine3] [varchar](20) NULL,
	[PtAddrLine4] [varchar](20) NULL,
	[PtAddrPostCode] [varchar](10) NULL,
	[PtAddrComments] [varchar](30) NULL,
	[PtEmail] [varchar](60) NULL,
	[PtWorkPhone] [varchar](23) NULL,
	[PtHomePhone] [varchar](23) NULL,
	[PtMobilePhone] [varchar](23) NULL,
	[PtPostAddrLine1] [varchar](20) NULL,
	[PtPostAddrLine2] [varchar](20) NULL,
	[PtPostAddrLine3] [varchar](20) NULL,
	[PtPostAddrLine4] [varchar](20) NULL,
	[PtPostAddrPostCode] [varchar](10) NULL,
	[PtPseudoPostCode] [varchar](10) NULL,
	[Occupation] [varchar](20) SPARSE  NULL,
	[MaritalStatus] [varchar](1) NULL,
	[EthnicType] [varchar](4) NULL,
	[Religion] [varchar](4) NULL,
	[NoKName] [varchar](30) NULL,
	[NoKRelationship] [varchar](9) NULL,
	[NoKAddrLine1] [varchar](20) NULL,
	[NoKAddrLine2] [varchar](20) NULL,
	[NoKAddrLine3] [varchar](20) NULL,
	[NoKAddrLine4] [varchar](20) NULL,
	[NoKPostCode] [varchar](255) NULL,
	[NoKWorkPhone] [varchar](23) NULL,
	[NoKHomePhone] [varchar](23) NULL,
	[NoKComments] [varchar](30) SPARSE  NULL,
	[Occnspouse] [varchar](20) SPARSE  NULL,
	[HealthAuthorityCode] [varchar](3) SPARSE  NULL,
	[DistrictOfResidenceCode] [varchar](3) NULL,
	[GdpCode] [varchar](6) SPARSE  NULL,
	[GpCode] [varchar](8) NULL,
	[GeneralComments] [varchar](120) SPARSE  NULL,
	[Allergies] [varchar](15) SPARSE  NULL,
	[Allergies_1] [varchar](15) SPARSE  NULL,
	[BirthName] [varchar](24) SPARSE  NULL,
	[BloodGroup] [varchar](3) SPARSE  NULL,
	[CarerAddress] [varchar](100) SPARSE  NULL,
	[CarerName] [varchar](30) SPARSE  NULL,
	[CarerSupport] [varchar](3) SPARSE  NULL,
	[Comments] [varchar](30) SPARSE  NULL,
	[ExtAddressAddLine1] [varchar](35) SPARSE  NULL,
	[ExtAddressLine1] [varchar](35) SPARSE  NULL,
	[ExtAddressLine2] [varchar](35) SPARSE  NULL,
	[ExtAddressLine3] [varchar](35) SPARSE  NULL,
	[ExtAddressLine4] [varchar](35) SPARSE  NULL,
	[ExtAddressLine5] [varchar](35) SPARSE  NULL,
	[ExtAddressPostcd] [varchar](10) SPARSE  NULL,
	[MRSA] [varchar](3) SPARSE  NULL,
	[MRSASetDt] [date] SPARSE  NULL,
	[MRSASetDtInt] [bigint] SPARSE  NULL,
	[MedicalProfileComments] [varchar](30) SPARSE  NULL,
	[PostAddrExpiryDate] [varchar](255) SPARSE  NULL,
	[PostAddrExpiryDateInt] [varchar](255) SPARSE  NULL,
	[PostalAddressDistrictOfResidence] [varchar](3) SPARSE  NULL,
	[WorkPhone] [varchar](23) SPARSE  NULL,
	[HomePhone] [varchar](23) SPARSE  NULL,
	[PmiAlternatePatientNumberinternal] [varchar](18) SPARSE  NULL,
	[PseudoDisrtrictOfResidence] [varchar](3) SPARSE  NULL,
	[Relationship] [varchar](9) SPARSE  NULL,
	[RelationshipInt] [varchar](2) SPARSE  NULL,
	[RiskFactorCode] [varchar](6) SPARSE  NULL,
	[RiskFactorCode_1] [varchar](6) SPARSE  NULL,
	[School] [varchar](20) SPARSE  NULL,
	[UserField1] [varchar](30) SPARSE  NULL,
	[UserField2] [varchar](30) SPARSE  NULL,
	[DateTimeModified] [datetime2](0) NULL,
	[DateTimeModifiedInt] [bigint] NULL,
	[PATDATAID]  AS (CONVERT([varchar](12),[InternalPatientNumber],(0))),
	[DistrictNumber_Formatted] [varchar](max) -- AS (replicate('0',(8)-len(CONVERT([varchar],[districtnumber],0)))+CONVERT([varchar],[districtnumber],0))
) ON [PRIMARY]
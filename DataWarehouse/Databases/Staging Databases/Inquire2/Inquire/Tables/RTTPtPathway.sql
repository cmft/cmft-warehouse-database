﻿CREATE TABLE [Inquire].[RTTPtPathway](
	[InternalPatientNumber] [int] NOT NULL,
	[PathwayNumber] [varchar](25) NOT NULL,
	[PathwayOrgProv] [varchar](5) NULL,
	[PathwayCondition] [varchar](20) NULL,
	[RttCurProv] [varchar](4) NULL,
	[RttCurrentStatus] [varchar](4) NULL,
	[RttCurrentStatusDateTime] [datetime2](0) NULL,
	[RttSpeciality] [varchar](4) NULL,
	[RttOsvStatus] [bit] NULL,
	[RttPrivatePat] [bit] NULL,
	[RTTPTPATHWAYID]  AS ((CONVERT([varchar](15),[InternalPatientNumber],0)+'||')+[PathwayNumber])
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[CASENOTEDETS](
	[Casenote] [varchar](14) NOT NULL,
	[InternalPatientNumber] [int] NOT NULL,
	[Location] [varchar](4) NULL,
	[LocationDate] [date] NULL,
	[Allocated] [date] NULL,
	[Withdrawn] [date] NULL,
	[Status] [char](1) NULL,
	[Comment] [varchar](48) NULL,
	[CASENOTEDETSID] [varchar](12) null -- AS (CONVERT([varchar](12),[internalpatientnumber],(0))+[Casenote])
) ON [PRIMARY]
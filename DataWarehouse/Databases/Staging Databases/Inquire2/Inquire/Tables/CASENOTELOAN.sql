﻿CREATE TABLE [Inquire].[CASENOTELOAN](
	[InternalPatientNumber] [int] NOT NULL,
	[Casenote] [varchar](14) NOT NULL,
	[FileLoanDate] [date] NOT NULL,
	[SequenceNumber] [smallint] NOT NULL,
	[FileLoanTime] [time](7) NULL,
	[ExpectedReturnDate] [date] NULL,
	[ActualReturnDate] [date] NULL,
	[ActualReturnTime] [time](7) NULL,
	[ReasonForLoan] [varchar](30) NULL,
	[Comments] [varchar](30) NULL,
	[Borrower] [varchar](6) NULL,
	[CntPcr4860DatetimeOfTransactionUpdateNeut] [varchar](12) NULL,
	[UserId] [varchar](3) NULL
) ON [PRIMARY]
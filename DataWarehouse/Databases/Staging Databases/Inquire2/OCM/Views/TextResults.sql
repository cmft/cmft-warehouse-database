﻿create view [OCM].[TextResults]
as
select
	 PatientNumber InternalPatientNumber
	,o.OrderDate
	,o.SessionDate
	,r.* 
from
	[$(PAS)].Inquire.OCMTEXTRESULTS r

inner join [$(PAS)].Inquire.OCORDER o
on	o.OcmTransKey = r.OcmTransKey
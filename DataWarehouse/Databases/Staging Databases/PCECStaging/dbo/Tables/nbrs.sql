﻿CREATE TABLE [dbo].[nbrs](
	[num] [int] NULL,
	[dt]  AS (CONVERT([date],dateadd(day, -(1)*[num],getdate()),0))
) ON [PRIMARY]
﻿CREATE TABLE [WHBackup].[TImportPCECAECodesInfOutcomes](
	[Location] [varchar](max) NULL,
	[SourceUniqueID] [varchar](max) NULL,
	[CaseNo] [varchar](max) NULL,
	[Age] [varchar](max) NULL,
	[DateOfBirth] [varchar](max) NULL,
	[CaseType] [varchar](max) NULL,
	[CallDate] [varchar](max) NULL,
	[DayOfWeek] [varchar](max) NULL,
	[CallTime] [varchar](max) NULL,
	[Postcode] [varchar](max) NULL,
	[Codes] [varchar](max) NULL,
	[InformationalOutcomes] [varchar](max) NULL,
	[Created] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [WHBackup].[TImportPCECAEEntryToFirstCons](
	[Location] [varchar](max) NULL,
	[Provider] [varchar](max) NULL,
	[CaseNo] [varchar](max) NULL,
	[EntryTime] [varchar](max) NULL,
	[ConsultationStartTime] [varchar](max) NULL,
	[ConsultationEndTime] [varchar](max) NULL,
	[TimeTaken] [varchar](max) NULL,
	[ConsultationStartType] [varchar](max) NULL,
	[ConsultationEndType] [varchar](max) NULL,
	[Created] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
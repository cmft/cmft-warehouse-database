﻿CREATE TABLE [dbo].[TransactionType](
	[TransactionTypeID] [int] NOT NULL,
	[TransactionType] [varchar](50) NULL,
	[TransactionLogTypeID] [int] NOT NULL,
	[Comment] [varchar](550) NULL
) ON [PRIMARY]
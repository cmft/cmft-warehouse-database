﻿CREATE TABLE [ETL].[TImportTransactionGC](
	[TransactionID] [bigint] NOT NULL,
	[TransactionLogTypeID] [int] NULL,
	[UserID] [varchar](20) NULL,
	[ActionType] [varchar](20) NULL,
	[TransactionTypeID] [int] NULL,
	[TransactionTime] [bigint] NULL,
	[ColumnID] [int] NOT NULL,
	[NewValue] [varchar](max) NULL,
	[OldValue] [varchar](max) NULL,
	[DistrictNo] [varchar](20) NULL,
	[EpisodeNumber] [int] NULL,
	[ActivityTime] [bigint] NULL,
	[ConsultantEpisodeAtTransaction] [int] NULL,
	[ConsultantEpisodeCurrent] [int] NULL,
	[DateAdded] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
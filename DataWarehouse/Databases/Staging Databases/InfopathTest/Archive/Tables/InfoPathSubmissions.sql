﻿CREATE TABLE [Archive].[InfoPathSubmissions](
	[SubmissionID] [int] NOT NULL,
	[WhenSubmitted] [datetime2](7) NOT NULL,
	[AppID] [int] NOT NULL,
	[DeliveryXML] [xml] NOT NULL,
	[ArchiveDate] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [PatientListing].[Priority](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[HasExtraInfo] [bit] NULL,
	[Ordering] [tinyint] NOT NULL
) ON [PRIMARY]
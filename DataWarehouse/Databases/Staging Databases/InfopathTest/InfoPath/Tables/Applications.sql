﻿CREATE TABLE [InfoPath].[Applications](
	[AppID] [int] NOT NULL,
	[Name] [sysname] NOT NULL,
	[ApplicationSchema] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
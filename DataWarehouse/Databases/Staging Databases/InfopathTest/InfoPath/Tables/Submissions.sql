﻿CREATE TABLE [InfoPath].[Submissions](
	[SubmissionID] [int] IDENTITY(1,1) NOT NULL,
	[WhenSubmitted] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[AppID] [int] NOT NULL,
	[DeliveryXML] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
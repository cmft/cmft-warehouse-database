﻿CREATE TABLE [dbo].[SiteDeletion](
	[DeletionTime] [datetime] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[InDeletion] [bit] NOT NULL,
	[Restorable] [bit] NOT NULL
) ON [PRIMARY]
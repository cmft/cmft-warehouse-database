﻿CREATE TABLE [dbo].[SiteVersions](
	[SiteId] [uniqueidentifier] NOT NULL,
	[VersionId] [uniqueidentifier] NOT NULL,
	[Version] [nvarchar](64) NOT NULL
) ON [PRIMARY]
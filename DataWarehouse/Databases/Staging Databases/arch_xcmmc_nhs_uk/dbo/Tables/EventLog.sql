﻿CREATE TABLE [dbo].[EventLog](
	[Id] [bigint] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[ListId] [uniqueidentifier] NOT NULL,
	[ItemId] [int] NOT NULL,
	[ItemName] [nvarchar](255) NULL,
	[ItemFullUrl] [nvarchar](260) NULL,
	[EventType] [int] NOT NULL,
	[ModifiedBy] [nvarchar](255) NULL,
	[TimeLastModified] [datetime] NOT NULL,
	[EventData] [varbinary](max) NULL,
	[FormattedFragment] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
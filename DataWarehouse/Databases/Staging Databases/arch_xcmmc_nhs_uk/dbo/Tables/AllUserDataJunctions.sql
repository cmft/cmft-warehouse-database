﻿CREATE TABLE [dbo].[AllUserDataJunctions](
	[tp_SiteId] [uniqueidentifier] NOT NULL,
	[tp_DeleteTransactionId] [varbinary](16) NOT NULL,
	[tp_IsCurrentVersion] [bit] NOT NULL,
	[tp_ParentId] [uniqueidentifier] NOT NULL,
	[tp_DocId] [uniqueidentifier] NOT NULL,
	[tp_FieldId] [uniqueidentifier] NOT NULL,
	[tp_CalculatedVersion] [int] NOT NULL,
	[tp_Level] [tinyint] NOT NULL,
	[tp_UIVersion] [int] NOT NULL,
	[tp_Id] [int] NOT NULL,
	[tp_Ordinal] [int] NOT NULL,
	[tp_SourceListId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AllLookupRelationships](
	[SiteId] [uniqueidentifier] NOT NULL,
	[ListId] [uniqueidentifier] NOT NULL,
	[FieldId] [uniqueidentifier] NOT NULL,
	[LookupListId] [uniqueidentifier] NOT NULL,
	[DeleteBehavior] [tinyint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[BuildDependencies](
	[SiteId] [uniqueidentifier] NOT NULL,
	[DirName] [nvarchar](256) NOT NULL,
	[LeafName] [nvarchar](128) NOT NULL,
	[TargetDirName] [nvarchar](256) NOT NULL,
	[TargetLeafName] [nvarchar](128) NOT NULL,
	[DirectDependency] [bit] NOT NULL,
	[DeleteTransactionId] [varbinary](16) NOT NULL DEFAULT (0x)
) ON [PRIMARY]
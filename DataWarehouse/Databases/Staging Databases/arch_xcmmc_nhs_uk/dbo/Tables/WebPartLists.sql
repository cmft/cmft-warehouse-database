﻿CREATE TABLE [dbo].[WebPartLists](
	[tp_SiteId] [uniqueidentifier] NOT NULL,
	[tp_ListId] [uniqueidentifier] NOT NULL,
	[tp_WebId] [uniqueidentifier] NOT NULL,
	[tp_PageUrlID] [uniqueidentifier] NOT NULL,
	[tp_WebPartID] [uniqueidentifier] NOT NULL,
	[tp_UserID] [int] NULL,
	[tp_Level] [tinyint] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
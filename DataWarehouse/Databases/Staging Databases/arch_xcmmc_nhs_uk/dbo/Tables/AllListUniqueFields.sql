﻿CREATE TABLE [dbo].[AllListUniqueFields](
	[SiteId] [uniqueidentifier] NOT NULL,
	[ListId] [uniqueidentifier] NOT NULL,
	[FieldId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
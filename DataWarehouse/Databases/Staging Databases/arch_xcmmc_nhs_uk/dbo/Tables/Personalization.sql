﻿CREATE TABLE [dbo].[Personalization](
	[tp_SiteId] [uniqueidentifier] NOT NULL,
	[tp_WebPartID] [uniqueidentifier] NOT NULL,
	[tp_PageUrlID] [uniqueidentifier] NOT NULL,
	[tp_UserID] [int] NOT NULL,
	[tp_PartOrder] [int] NULL,
	[tp_ZoneID] [nvarchar](64) NULL,
	[tp_IsIncluded] [bit] NOT NULL,
	[tp_FrameState] [tinyint] NOT NULL,
	[tp_PerUserProperties] [varbinary](max) NULL,
	[tp_Cache] [varbinary](max) NULL,
	[tp_Size] [bigint] NOT NULL,
	[tp_Deleted] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
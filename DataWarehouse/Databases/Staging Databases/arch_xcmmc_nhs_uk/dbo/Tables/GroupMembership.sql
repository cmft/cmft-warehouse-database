﻿CREATE TABLE [dbo].[GroupMembership](
	[SiteId] [uniqueidentifier] NOT NULL,
	[GroupId] [int] NOT NULL,
	[MemberId] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NavNodes](
	[SiteId] [uniqueidentifier] NOT NULL,
	[WebId] [uniqueidentifier] NOT NULL,
	[Eid] [int] NOT NULL,
	[EidParent] [int] NOT NULL,
	[NumChildren] [int] NOT NULL,
	[RankChild] [int] NOT NULL,
	[ElementType] [tinyint] NOT NULL,
	[Url] [nvarchar](260) NULL,
	[DocId] [uniqueidentifier] NULL,
	[Name] [nvarchar](256) NOT NULL,
	[NameResource] [nvarchar](520) NULL,
	[DateLastModified] [datetime] NOT NULL,
	[NodeMetainfo] [varbinary](max) NULL,
	[NonNavPage] [bit] NOT NULL,
	[NavSequence] [bit] NOT NULL,
	[ChildOfSequence] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
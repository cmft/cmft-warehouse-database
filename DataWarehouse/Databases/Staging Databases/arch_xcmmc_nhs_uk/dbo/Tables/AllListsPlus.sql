﻿CREATE TABLE [dbo].[AllListsPlus](
	[ListId] [uniqueidentifier] NOT NULL,
	[TitleResource] [nvarchar](520) NULL,
	[DescriptionResource] [nvarchar](520) NULL,
	[DataSource] [nvarchar](max) NULL,
	[EntityId] [int] NULL,
	[ValidationFormula] [nvarchar](1024) NULL,
	[ValidationMessage] [nvarchar](1024) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[AllDocStreams](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[InternalVersion] [int] NOT NULL,
	[Content] [varbinary](max) NULL,
	[RbsId] [varbinary](64) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
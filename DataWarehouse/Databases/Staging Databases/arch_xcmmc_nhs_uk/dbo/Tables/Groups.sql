﻿CREATE TABLE [dbo].[Groups](
	[SiteId] [uniqueidentifier] NOT NULL,
	[ID] [int] NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](512) NULL,
	[Owner] [int] NOT NULL,
	[OwnerIsUser] [bit] NOT NULL,
	[DLAlias] [nvarchar](128) NULL,
	[DLErrorMessage] [nvarchar](512) NULL,
	[DLFlags] [int] NULL,
	[DLJobId] [int] NULL,
	[DLArchives] [varchar](4000) NULL,
	[RequestEmail] [nvarchar](255) NULL,
	[Flags] [int] NOT NULL DEFAULT ((0)),
	[DeletionWebId] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AuditData](
	[SiteId] [uniqueidentifier] NOT NULL,
	[ItemId] [uniqueidentifier] NOT NULL,
	[ItemType] [smallint] NOT NULL,
	[UserId] [int] NULL,
	[MachineName] [nvarchar](128) NULL,
	[MachineIp] [nvarchar](20) NULL,
	[DocLocation] [nvarchar](260) NULL,
	[LocationType] [tinyint] NULL,
	[Occurred] [datetime] NOT NULL,
	[Event] [int] NOT NULL,
	[EventName] [nvarchar](128) NULL,
	[EventSource] [tinyint] NOT NULL,
	[SourceName] [nvarchar](256) NULL,
	[EventData] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Resources](
	[WebId] [uniqueidentifier] NOT NULL,
	[ListId] [uniqueidentifier] NOT NULL,
	[ResourceName] [nvarchar](520) NOT NULL,
	[BitType] [bit] NOT NULL,
	[BitDirty] [bit] NOT NULL,
	[LCID] [int] NOT NULL,
	[NvarcharVal] [nvarchar](256) NULL,
	[NtextVal] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
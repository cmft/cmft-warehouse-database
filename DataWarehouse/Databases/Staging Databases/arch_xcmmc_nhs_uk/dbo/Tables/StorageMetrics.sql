﻿CREATE TABLE [dbo].[StorageMetrics](
	[SiteId] [uniqueidentifier] NOT NULL,
	[DocId] [uniqueidentifier] NOT NULL,
	[TotalSize] [bigint] NOT NULL DEFAULT ((0)),
	[LastModified] [datetime] NOT NULL
) ON [PRIMARY]
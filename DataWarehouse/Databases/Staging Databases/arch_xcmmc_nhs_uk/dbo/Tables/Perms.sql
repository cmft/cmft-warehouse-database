﻿CREATE TABLE [dbo].[Perms](
	[SiteId] [uniqueidentifier] NOT NULL,
	[ScopeId] [uniqueidentifier] NOT NULL,
	[RoleDefWebId] [uniqueidentifier] NOT NULL,
	[WebId] [uniqueidentifier] NOT NULL,
	[DelTransId] [varbinary](16) NOT NULL DEFAULT (0x),
	[ScopeUrl] [nvarchar](260) NOT NULL,
	[AnonymousPermMask] [dbo].[tPermMask] NOT NULL,
	[Acl] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Deps](
	[SiteId] [uniqueidentifier] NOT NULL,
	[DeleteTransactionId] [varbinary](16) NOT NULL,
	[FullUrl] [nvarchar](260) NOT NULL,
	[DepType] [tinyint] NOT NULL,
	[DepDesc] [nvarchar](270) NOT NULL,
	[Level] [tinyint] NOT NULL
) ON [PRIMARY]
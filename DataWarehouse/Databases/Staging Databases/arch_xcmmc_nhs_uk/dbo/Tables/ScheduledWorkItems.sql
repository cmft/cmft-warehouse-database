﻿CREATE TABLE [dbo].[ScheduledWorkItems](
	[SiteId] [uniqueidentifier] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[DeliveryDate] [datetime] NOT NULL,
	[Type] [uniqueidentifier] NOT NULL,
	[ParentId] [uniqueidentifier] NOT NULL,
	[ItemId] [int] NOT NULL,
	[ItemGuid] [varbinary](16) NULL,
	[BatchId] [varbinary](16) NULL,
	[WebId] [varbinary](16) NULL,
	[UserId] [int] NOT NULL,
	[Created] [datetime] NULL,
	[BinaryPayload] [varbinary](max) NULL,
	[TextPayload] [nvarchar](max) NULL,
	[ProcessingId] [uniqueidentifier] NULL,
	[ProcessMachineId] [uniqueidentifier] NULL,
	[ProcessMachinePID] [int] NULL,
	[InternalState] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[NameValuePair_Chinese_Hong_Kong_Stroke_90_CI_AS](
	[SiteId] [uniqueidentifier] NOT NULL,
	[WebId] [uniqueidentifier] NOT NULL,
	[ListId] [uniqueidentifier] NOT NULL,
	[ItemId] [int] NOT NULL,
	[Level] [tinyint] NOT NULL,
	[FieldId] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Versions](
	[VersionId] [uniqueidentifier] NOT NULL,
	[Version] [nvarchar](64) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](255) NULL,
	[TimeStamp] [datetime] NULL,
	[FinalizeTimeStamp] [datetime] NULL,
	[Mode] [int] NULL,
	[ModeStack] [int] NULL,
	[Updates] [int] NOT NULL DEFAULT ((0)),
	[Notes] [nvarchar](1024) NULL
) ON [PRIMARY]
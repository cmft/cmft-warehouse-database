﻿CREATE TABLE [dbo].[Features](
	[SiteId] [uniqueidentifier] NOT NULL,
	[WebId] [uniqueidentifier] NOT NULL,
	[FeatureId] [uniqueidentifier] NOT NULL,
	[Version] [nvarchar](64) NOT NULL DEFAULT ('0.0.0.0'),
	[TimeActivated] [datetime] NOT NULL DEFAULT (getutcdate()),
	[Flags] [int] NOT NULL,
	[Properties] [nvarchar](max) NULL,
	[PropertiesModified] [datetime] NOT NULL,
	[SolutionId] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
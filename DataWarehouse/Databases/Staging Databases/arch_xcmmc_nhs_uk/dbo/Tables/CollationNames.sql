﻿CREATE TABLE [dbo].[CollationNames](
	[CollationName] [nvarchar](48) NOT NULL,
	[Collation] [smallint] NOT NULL
) ON [PRIMARY]
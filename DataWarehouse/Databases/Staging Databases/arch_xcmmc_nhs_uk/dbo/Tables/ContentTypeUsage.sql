﻿CREATE TABLE [dbo].[ContentTypeUsage](
	[SiteId] [uniqueidentifier] NOT NULL,
	[ContentTypeId] [dbo].[tContentTypeId] NOT NULL,
	[WebId] [uniqueidentifier] NULL,
	[ListId] [uniqueidentifier] NOT NULL,
	[IsFieldId] [bit] NULL DEFAULT ((0)),
	[Class] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
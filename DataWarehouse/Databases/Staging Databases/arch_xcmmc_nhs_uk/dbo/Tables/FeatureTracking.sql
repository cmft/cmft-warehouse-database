﻿CREATE TABLE [dbo].[FeatureTracking](
	[FeatureId] [uniqueidentifier] NOT NULL,
	[SolutionId] [uniqueidentifier] NULL,
	[FirstActivated] [datetime] NOT NULL,
	[LastActivated] [datetime] NOT NULL,
	[FeatureTitle] [nvarchar](max) NULL,
	[FeatureDescription] [nvarchar](max) NULL,
	[FolderName] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
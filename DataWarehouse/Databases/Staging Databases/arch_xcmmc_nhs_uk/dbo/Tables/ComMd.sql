﻿CREATE TABLE [dbo].[ComMd](
	[SiteId] [uniqueidentifier] NOT NULL,
	[DocId] [uniqueidentifier] NOT NULL,
	[Id] [int] NOT NULL,
	[ParentId] [int] NULL,
	[CommentId] [nvarchar](255) NULL,
	[Bookmark] [nvarchar](127) NULL,
	[Author] [nvarchar](255) NULL,
	[UserId] [int] NULL,
	[Subject] [nvarchar](255) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Status] [smallint] NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[Size] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[WorkflowAssociation](
	[Id] [uniqueidentifier] NOT NULL,
	[BaseId] [uniqueidentifier] NOT NULL,
	[ParentId] [varbinary](16) NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [nvarchar](1023) NULL,
	[StatusFieldName] [nvarchar](64) NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[WebId] [varbinary](16) NULL,
	[ListId] [varbinary](16) NULL,
	[ContentTypeId] [dbo].[tContentTypeId] NULL,
	[InstanceCount] [int] NULL,
	[InstanceCountDirty] [bit] NULL,
	[TaskListId] [varbinary](16) NULL,
	[HistoryListId] [varbinary](16) NULL,
	[TaskListTitle] [nvarchar](255) NULL,
	[HistoryListTitle] [nvarchar](255) NULL,
	[Configuration] [int] NULL,
	[AutoCleanupDays] [int] NULL,
	[Author] [int] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[InstantiationParams] [nvarchar](max) NULL,
	[PermissionsManual] [dbo].[tPermMask] NULL,
	[Version] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[AllLinks](
	[SiteId] [uniqueidentifier] NOT NULL,
	[DeleteTransactionId] [varbinary](16) NOT NULL DEFAULT (0x),
	[ParentId] [uniqueidentifier] NOT NULL,
	[DocId] [uniqueidentifier] NOT NULL,
	[WebPartId] [uniqueidentifier] NULL,
	[FieldId] [uniqueidentifier] NULL,
	[LinkNumber] [int] NOT NULL,
	[TargetDirName] [nvarchar](256) NOT NULL,
	[TargetLeafName] [nvarchar](128) NOT NULL,
	[Type] [tinyint] NOT NULL,
	[Security] [tinyint] NOT NULL,
	[Dynamic] [tinyint] NOT NULL,
	[ServerRel] [bit] NOT NULL,
	[PointsToDir] [bit] NOT NULL,
	[Level] [tinyint] NOT NULL DEFAULT ((1)),
	[Search] [nvarchar](max) NOT NULL CONSTRAINT [AllLinks_Search_Default]  DEFAULT (N'')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Roles](
	[SiteId] [uniqueidentifier] NOT NULL,
	[WebId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](512) NULL,
	[PermMask] [dbo].[tPermMask] NULL,
	[PermMaskDeny] [dbo].[tPermMask] NULL,
	[Hidden] [bit] NOT NULL,
	[Type] [tinyint] NOT NULL,
	[WebGroupId] [int] NOT NULL DEFAULT ((-1)),
	[RoleOrder] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
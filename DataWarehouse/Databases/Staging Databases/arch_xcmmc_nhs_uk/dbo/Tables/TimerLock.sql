﻿CREATE TABLE [dbo].[TimerLock](
	[ID] [int] NOT NULL,
	[LockedBy] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[LockedTime] [datetime] NOT NULL
) ON [PRIMARY]
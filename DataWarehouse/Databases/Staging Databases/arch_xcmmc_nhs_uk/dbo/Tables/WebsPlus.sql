﻿CREATE TABLE [dbo].[WebsPlus](
	[WebId] [uniqueidentifier] NOT NULL,
	[TitleResource] [nvarchar](520) NULL,
	[DescriptionResource] [nvarchar](520) NULL,
	[IsTranLocked] [bit] NOT NULL DEFAULT ((0)),
	[TranLockerId] [uniqueidentifier] NULL,
	[TranLockExpiryTime] [datetime] NULL,
	[TranLockRefCount] [int] NOT NULL DEFAULT ((0)),
	[AlternateMUICultures] [nvarchar](max) NULL,
	[OverwriteMUICultures] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[NameValuePair_Hungarian_Technical_CI_AS](
	[SiteId] [uniqueidentifier] NOT NULL,
	[WebId] [uniqueidentifier] NOT NULL,
	[ListId] [uniqueidentifier] NOT NULL,
	[ItemId] [int] NOT NULL,
	[Level] [tinyint] NOT NULL,
	[FieldId] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](255) NULL
) ON [PRIMARY]
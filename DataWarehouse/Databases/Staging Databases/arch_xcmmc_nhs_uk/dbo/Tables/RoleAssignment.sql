﻿CREATE TABLE [dbo].[RoleAssignment](
	[SiteId] [uniqueidentifier] NOT NULL,
	[ScopeId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
	[PrincipalId] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[HT_Cache](
	[TimeStamp] [smalldatetime] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[DirName] [nvarchar](256) NOT NULL,
	[LeafName] [nvarchar](128) NOT NULL,
	[TransName] [nvarchar](128) NOT NULL,
	[JobType] [tinyint] NOT NULL,
	[STSDocVersion] [datetime] NOT NULL,
	[File] [varbinary](max) NULL,
	[FileSize] [int] NOT NULL,
	[MainFile] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
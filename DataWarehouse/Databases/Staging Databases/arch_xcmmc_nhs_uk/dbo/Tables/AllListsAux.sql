﻿CREATE TABLE [dbo].[AllListsAux](
	[ListID] [uniqueidentifier] NOT NULL,
	[Modified] [datetime] NOT NULL,
	[LastDeleted] [datetime] NOT NULL,
	[ItemCount] [int] NOT NULL,
	[NextAvailableId] [int] NOT NULL
) ON [PRIMARY]
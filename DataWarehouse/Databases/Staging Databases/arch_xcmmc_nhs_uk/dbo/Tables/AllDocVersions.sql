﻿CREATE TABLE [dbo].[AllDocVersions](
	[SiteId] [uniqueidentifier] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[UIVersion] [int] NOT NULL,
	[InternalVersion] [int] NULL,
	[TimeCreated] [datetime] NULL,
	[DocFlags] [int] NOT NULL,
	[MetaInfoSize] [int] NULL,
	[Size] [int] NULL,
	[MetaInfo] [dbo].[tCompressedBinary] NULL,
	[CheckinComment] [nvarchar](1023) NULL,
	[Level] [tinyint] NOT NULL,
	[DraftOwnerId] [int] NULL,
	[DeleteTransactionId] [varbinary](16) NOT NULL,
	[VirusVendorID] [int] NULL,
	[VirusStatus] [int] NULL,
	[VirusInfo] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
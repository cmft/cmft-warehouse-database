﻿CREATE TABLE [dbo].[AllFileFragments](
	[DocId] [uniqueidentifier] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Partition] [tinyint] NOT NULL,
	[Tag] [varbinary](40) NULL,
	[BlobData] [varbinary](max) NULL,
	[BlobSize] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
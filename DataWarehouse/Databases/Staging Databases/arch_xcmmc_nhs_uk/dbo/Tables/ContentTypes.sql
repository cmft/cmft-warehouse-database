﻿CREATE TABLE [dbo].[ContentTypes](
	[SiteId] [uniqueidentifier] NOT NULL,
	[Class] [tinyint] NOT NULL,
	[Scope] [nvarchar](256) NOT NULL,
	[ContentTypeId] [dbo].[tContentTypeId] NOT NULL,
	[Version] [int] NOT NULL,
	[NextChildByte] [tinyint] NOT NULL,
	[Size] [int] NOT NULL DEFAULT ((16)),
	[Definition] [nvarchar](max) NULL,
	[ResourceDir] [nvarchar](128) NULL,
	[SolutionId] [uniqueidentifier] NULL,
	[IsFromFeature] [bit] NOT NULL DEFAULT ((0)),
	[DeleteTransactionId] [varbinary](16) NOT NULL DEFAULT (0x)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
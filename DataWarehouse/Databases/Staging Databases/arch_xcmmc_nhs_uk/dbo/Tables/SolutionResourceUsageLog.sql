﻿CREATE TABLE [dbo].[SolutionResourceUsageLog](
	[SiteId] [uniqueidentifier] NOT NULL,
	[SolutionId] [uniqueidentifier] NOT NULL,
	[ResourceId] [uniqueidentifier] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[SampleCount] [int] NOT NULL,
	[ResourceUsage] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CustomActions](
	[Id] [uniqueidentifier] NOT NULL,
	[ScopeId] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[WebId] [uniqueidentifier] NOT NULL,
	[FeatureId] [uniqueidentifier] NULL,
	[ScopeType] [int] NOT NULL,
	[Properties] [nvarchar](max) NOT NULL,
	[Version] [nvarchar](64) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
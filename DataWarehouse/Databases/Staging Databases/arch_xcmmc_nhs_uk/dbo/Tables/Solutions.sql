﻿CREATE TABLE [dbo].[Solutions](
	[Name] [nvarchar](128) NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[SolutionId] [uniqueidentifier] NOT NULL,
	[Hash] [nvarchar](50) NOT NULL,
	[SolutionGalleryItemId] [int] NOT NULL,
	[Status] [smallint] NOT NULL,
	[HasAssemblies] [tinyint] NOT NULL,
	[ResourceQuota] [float] NOT NULL,
	[RecentInvocations] [int] NOT NULL,
	[ValidatorsHash] [char](64) NOT NULL,
	[ValidationErrorUrl] [nvarchar](4000) NULL,
	[ValidationErrorMessage] [nvarchar](4000) NULL,
	[Definitions] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
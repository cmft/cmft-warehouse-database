﻿CREATE TABLE [dbo].[StorageMetricsChanges](
	[SiteId] [uniqueidentifier] NOT NULL,
	[DocId] [uniqueidentifier] NOT NULL,
	[DeltaSize] [bigint] NOT NULL DEFAULT ((0)),
	[LastModified] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SiteQuota](
	[SiteId] [uniqueidentifier] NOT NULL,
	[SessionId] [smallint] NOT NULL,
	[SMOpCode] [tinyint] NOT NULL,
	[DocId] [uniqueidentifier] NOT NULL,
	[DiskUsed] [bigint] NOT NULL,
	[UpdateTimeStamp] [bit] NOT NULL
) ON [PRIMARY]
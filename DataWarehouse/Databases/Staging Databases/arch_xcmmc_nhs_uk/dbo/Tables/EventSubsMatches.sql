﻿CREATE TABLE [dbo].[EventSubsMatches](
	[EventId] [bigint] NOT NULL,
	[SubId] [uniqueidentifier] NOT NULL,
	[LookupFieldPermissionResults] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
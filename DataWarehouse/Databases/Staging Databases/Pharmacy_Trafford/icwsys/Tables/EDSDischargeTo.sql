﻿CREATE TABLE [icwsys].[EDSDischargeTo](
	[NoteID] [int] NOT NULL,
	[TransferDestinationID] [int] NOT NULL,
	[Address] [varchar](1024) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[V8TradeNames](
	[SiteNo] [int] NULL,
	[SisCode] [varchar](7) NULL,
	[BefTradeName] [varchar](30) NULL,
	[AftTradeName] [varchar](30) NULL
) ON [PRIMARY]
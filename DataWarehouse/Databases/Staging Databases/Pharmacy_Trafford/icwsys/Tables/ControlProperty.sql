﻿CREATE TABLE [icwsys].[ControlProperty](
	[ControlPropertyID] [int] IDENTITY(1,1) NOT NULL,
	[ControlTypeID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PrescriptionAlias](
	[PrescriptionAliasID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](15) NOT NULL
) ON [PRIMARY]
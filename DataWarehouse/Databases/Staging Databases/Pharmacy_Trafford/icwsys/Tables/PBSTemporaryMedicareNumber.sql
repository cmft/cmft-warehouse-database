﻿CREATE TABLE [icwsys].[PBSTemporaryMedicareNumber](
	[PBSTemporaryMedicareNumberID] [int] IDENTITY(0,1) NOT NULL,
	[TemporaryMedicareNumber] [varchar](10) NOT NULL,
	[Description] [varchar](50) NULL,
	[Detail] [varchar](255) NULL
) ON [PRIMARY]
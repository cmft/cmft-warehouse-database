﻿CREATE TABLE [icwsys].[DischargeDiagnosis](
	[NoteID] [int] NOT NULL,
	[OrderCatalogueID_Diagnosis] [int] NOT NULL,
	[Notes] [varchar](4000) NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[OutboundMsg](
	[OutboundMsgID] [int] IDENTITY(1,1) NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Window](
	[WindowID] [int] IDENTITY(1,1) NOT NULL,
	[WindowID_Parent] [int] NOT NULL,
	[WindowTypeID] [int] NOT NULL,
	[DesktopID] [int] NOT NULL,
	[ToolMenuID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Orientation] [char](1) NOT NULL,
	[Offset] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[URL] [varchar](255) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[OrderSet](
	[RequestID] [int] NOT NULL,
	[Modified] [bit] NOT NULL,
	[ContentsAreOptions] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PermissionLocation](
	[PolicyLocationID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Allow] [bit] NOT NULL
) ON [PRIMARY]
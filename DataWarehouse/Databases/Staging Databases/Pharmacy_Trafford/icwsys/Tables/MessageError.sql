﻿CREATE TABLE [icwsys].[MessageError](
	[ApplicationLogID] [int] NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WExtraSupplierData](
	[WExtraSupplierDataID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID_Site] [int] NOT NULL,
	[Supcode] [varchar](5) NULL,
	[CurrentContractData] [varchar](1024) NULL,
	[NewContractData] [varchar](1024) NULL,
	[dateofchange] [varchar](10) NULL,
	[ContactName1] [varchar](50) NULL,
	[ContactName2] [varchar](50) NULL,
	[notes] [varchar](1024) NULL
) ON [PRIMARY]
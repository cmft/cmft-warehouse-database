﻿CREATE TABLE [icwsys].[Session](
	[SessionID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Session_DateCreated]  DEFAULT (getdate()),
	[DateLastUsed] [datetime] NOT NULL CONSTRAINT [DF_Session_DateLastUsed]  DEFAULT (getdate()),
	[Disconnected] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
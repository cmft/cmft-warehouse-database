﻿CREATE TABLE [icwsys].[ScheduleTemplateFrequency](
	[ScheduleTemplateFrequencyID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[InUse] [bit] NOT NULL,
	[DSS] [tinyint] NOT NULL,
	[Day1] [bit] NOT NULL,
	[Day2] [bit] NOT NULL,
	[Day3] [bit] NOT NULL,
	[Day4] [bit] NOT NULL,
	[Day5] [bit] NOT NULL,
	[Day6] [bit] NOT NULL,
	[Day7] [bit] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
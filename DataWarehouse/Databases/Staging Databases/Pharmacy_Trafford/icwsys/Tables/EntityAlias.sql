﻿CREATE TABLE [icwsys].[EntityAlias](
	[EntityAliasID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL CONSTRAINT [DF_EntityAlias_Default]  DEFAULT (0),
	[IsValid] [bit] NULL DEFAULT (0)
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[wConfiguration](
	[WConfigurationID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Category] [varchar](255) NOT NULL,
	[Section] [varchar](255) NOT NULL,
	[Key] [varchar](255) NOT NULL,
	[Value] [varchar](1024) NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid())
) ON [PRIMARY]
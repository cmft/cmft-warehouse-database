﻿CREATE TABLE [icwsys].[Terminal](
	[LocationID] [int] NOT NULL,
	[TerminalTypeID] [int] NOT NULL,
	[DomainName] [varchar](15) NULL,
	[ComputerName] [varchar](15) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[V8Products782Error](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL DEFAULT (getdate()),
	[SisCode] [varchar](7) NULL,
	[LocationID_Site] [int] NOT NULL,
	[ErrorText] [varchar](500) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ActivityLinkRequest](
	[ActivityID] [numeric](9, 0) NOT NULL,
	[RequestID] [int] NOT NULL
) ON [PRIMARY]
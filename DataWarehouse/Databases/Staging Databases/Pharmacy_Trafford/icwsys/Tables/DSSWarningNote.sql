﻿CREATE TABLE [icwsys].[DSSWarningNote](
	[NoteID] [int] NOT NULL,
	[LogID] [int] NOT NULL,
	[Detail] [char](500) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
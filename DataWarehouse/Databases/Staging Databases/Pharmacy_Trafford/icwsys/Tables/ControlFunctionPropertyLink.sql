﻿CREATE TABLE [icwsys].[ControlFunctionPropertyLink](
	[ControlFunctionID] [int] NOT NULL,
	[ControlID] [int] NOT NULL,
	[ControlPropertyID] [int] NOT NULL
) ON [PRIMARY]
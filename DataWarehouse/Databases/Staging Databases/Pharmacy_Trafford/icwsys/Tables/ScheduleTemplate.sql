﻿CREATE TABLE [icwsys].[ScheduleTemplate](
	[ScheduleTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[Description] [varchar](512) NOT NULL,
	[InUse] [bit] NOT NULL CONSTRAINT [DF_ScheduleTemplate_InUse]  DEFAULT (1),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[DSS] [bit] NOT NULL CONSTRAINT [DF_ScheduleTemplate_DSS]  DEFAULT (0)
) ON [PRIMARY]
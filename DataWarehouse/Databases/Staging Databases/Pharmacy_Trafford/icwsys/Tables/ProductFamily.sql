﻿CREATE TABLE [icwsys].[ProductFamily](
	[ProductID] [int] NOT NULL,
	[RelatedProductID] [int] NOT NULL,
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
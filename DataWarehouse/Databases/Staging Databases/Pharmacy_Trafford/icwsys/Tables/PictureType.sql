﻿CREATE TABLE [icwsys].[PictureType](
	[PictureTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Detail] [varchar](1024) NOT NULL
) ON [PRIMARY]
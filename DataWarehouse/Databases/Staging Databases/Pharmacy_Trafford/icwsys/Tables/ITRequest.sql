﻿CREATE TABLE [icwsys].[ITRequest](
	[RequestID] [int] NOT NULL,
	[Date] [datetime] NULL,
	[ProblemDesc] [varchar](50) NULL,
	[OtherComments] [varchar](50) NULL,
	[DayID] [int] NOT NULL
) ON [PRIMARY]
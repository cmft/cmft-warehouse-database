﻿CREATE TABLE [icwsys].[PBSEPAveragePricesUpdate](
	[PBSEPAveragePricesUpdateID] [int] IDENTITY(1,1) NOT NULL,
	[PBSMasterEPAveragePricesID] [int] NULL,
	[PBSCode] [varchar](3) NULL,
	[AverageRate] [float] NULL,
	[MaxPricePayable] [float] NULL,
	[MaxPriceToPatients] [float] NULL,
	[MaxQty] [int] NULL,
	[IssueUnit] [varchar](10) NULL,
	[MaxRepeats] [int] NULL,
	[Description] [varchar](50) NULL,
	[DateofChange] [datetime] NULL,
	[Remove] [bit] NULL,
	[DateProcessed] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[MonitorContact](
	[MonitorContactID] [int] IDENTITY(1,1) NOT NULL,
	[MonitorJobID] [int] NOT NULL,
	[EntityID_Person] [int] NOT NULL,
	[MonitorContactTypeID] [int] NOT NULL,
	[ContactOnSuccess] [bit] NOT NULL CONSTRAINT [DF_MonitorContact_ContactOnSuccess]  DEFAULT ((0)),
	[ContactOnFailure] [bit] NOT NULL CONSTRAINT [DF_MonitorContact_ContactOnFailure]  DEFAULT ((1)),
	[Active] [bit] NOT NULL CONSTRAINT [DF_MonitorContact_Active]  DEFAULT ((1))
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[RangeTestOutcomeSeverity](
	[RangeTestOutcomeSeverityID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Level] [tinyint] NOT NULL
) ON [PRIMARY]
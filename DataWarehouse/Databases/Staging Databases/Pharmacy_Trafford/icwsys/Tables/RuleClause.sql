﻿CREATE TABLE [icwsys].[RuleClause](
	[RuleClauseID] [int] IDENTITY(1,1) NOT NULL,
	[RuleID] [int] NOT NULL,
	[RoutineID] [int] NOT NULL,
	[ParameterValue] [varchar](50) NULL,
	[Comparison] [varchar](2) NOT NULL,
	[Value] [float] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
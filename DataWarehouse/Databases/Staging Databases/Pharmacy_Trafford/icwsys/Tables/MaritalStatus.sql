﻿CREATE TABLE [icwsys].[MaritalStatus](
	[MaritalStatusID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL
) ON [PRIMARY]
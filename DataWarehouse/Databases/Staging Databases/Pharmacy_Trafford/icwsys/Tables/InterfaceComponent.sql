﻿CREATE TABLE [icwsys].[InterfaceComponent](
	[InterfaceComponentID] [int] IDENTITY(1,1) NOT NULL,
	[InterfaceInstanceID] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Path] [varchar](1024) NOT NULL,
	[Namespace] [varchar](255) NOT NULL,
	[InstanceName] [varchar](50) NOT NULL,
	[EntityID_User] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[DebugMode] [bit] NOT NULL,
	[Start] [bit] NOT NULL
) ON [PRIMARY]
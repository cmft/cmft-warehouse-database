﻿CREATE TABLE [icwsys].[PharmacyCounter](
	[PharmacyCounterID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[System] [varchar](50) NOT NULL,
	[Section] [varchar](50) NOT NULL,
	[Key] [varchar](50) NOT NULL,
	[LastCount] [int] NOT NULL,
	[FormatString] [varchar](50) NULL,
	[UpdateDateTime] [datetime] NOT NULL,
	[ResetType] [varchar](50) NOT NULL,
	[ResetDateTime] [datetime] NULL,
	[SessionLock] [int] NOT NULL
) ON [PRIMARY]
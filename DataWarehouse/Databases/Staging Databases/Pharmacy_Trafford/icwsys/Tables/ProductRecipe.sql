﻿CREATE TABLE [icwsys].[ProductRecipe](
	[ProductRecipeID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductID_Child] [int] NOT NULL,
	[UnitID] [int] NOT NULL,
	[Quantity] [float] NOT NULL CONSTRAINT [DF_ProductRecipe_Quantity]  DEFAULT (0),
	[Active] [bit] NOT NULL CONSTRAINT [DF_ProductRecipe_Active]  DEFAULT (1),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

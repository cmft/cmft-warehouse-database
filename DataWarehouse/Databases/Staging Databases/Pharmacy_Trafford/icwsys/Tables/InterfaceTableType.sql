﻿CREATE TABLE [icwsys].[InterfaceTableType](
	[InterfaceTableTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL
) ON [PRIMARY]
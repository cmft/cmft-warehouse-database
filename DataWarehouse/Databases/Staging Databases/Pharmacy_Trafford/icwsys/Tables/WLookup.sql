﻿CREATE TABLE [icwsys].[WLookup](
	[WLookupID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[SiteID] [int] NOT NULL,
	[WLookupContextID] [int] NOT NULL,
	[Value] [varchar](1024) NOT NULL,
	[InUse] [bit] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid())
) ON [PRIMARY]

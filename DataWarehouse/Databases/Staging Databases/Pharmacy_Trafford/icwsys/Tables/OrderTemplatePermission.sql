﻿CREATE TABLE [icwsys].[OrderTemplatePermission](
	[OrderTemplatePermissionID] [int] IDENTITY(1,1) NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[AllowView] [bit] NOT NULL CONSTRAINT [DF_OrderTemplatePermission_AllowView]  DEFAULT (1)
) ON [PRIMARY]
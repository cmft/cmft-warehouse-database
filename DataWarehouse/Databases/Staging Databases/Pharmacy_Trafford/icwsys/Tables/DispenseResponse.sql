﻿CREATE TABLE [icwsys].[DispenseResponse](
	[ResponseID] [int] NOT NULL,
	[Done] [bit] NOT NULL
) ON [PRIMARY]
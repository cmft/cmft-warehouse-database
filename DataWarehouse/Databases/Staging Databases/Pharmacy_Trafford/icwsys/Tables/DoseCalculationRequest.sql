﻿CREATE TABLE [icwsys].[DoseCalculationRequest](
	[DoseCalculationRequestID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[Data_XML] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
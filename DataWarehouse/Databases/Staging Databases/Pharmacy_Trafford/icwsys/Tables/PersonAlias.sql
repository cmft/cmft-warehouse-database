﻿CREATE TABLE [icwsys].[PersonAlias](
	[PersonAliasID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Forename] [varchar](128) NULL,
	[Surname] [varchar](128) NOT NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]

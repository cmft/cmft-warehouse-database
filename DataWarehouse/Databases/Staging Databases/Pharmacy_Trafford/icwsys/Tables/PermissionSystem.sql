﻿CREATE TABLE [icwsys].[PermissionSystem](
	[PermissionSystemID] [int] IDENTITY(1,1) NOT NULL,
	[PolicySystemID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[Allow] [bit] NOT NULL
) ON [PRIMARY]
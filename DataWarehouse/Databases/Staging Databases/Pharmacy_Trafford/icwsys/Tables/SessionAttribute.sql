﻿CREATE TABLE [icwsys].[SessionAttribute](
	[SessionAttributeID] [int] IDENTITY(1,1) NOT NULL,
	[SessionID] [int] NULL,
	[Attribute] [varchar](128) NOT NULL,
	[Value] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Cookie](
	[CookieID] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Cookie_GUID]  DEFAULT (newid()),
	[LastUse] [datetime] NOT NULL CONSTRAINT [DF_Cookie_LastUse]  DEFAULT (getdate())
) ON [PRIMARY]
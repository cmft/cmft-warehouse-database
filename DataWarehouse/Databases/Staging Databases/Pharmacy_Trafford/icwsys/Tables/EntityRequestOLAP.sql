﻿CREATE TABLE [icwsys].[EntityRequestOLAP](
	[EntityRequestOLAPID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[RequestTypeID] [int] NOT NULL,
	[RequestTypeID_Parent] [int] NOT NULL,
	[RequestDate] [datetime] NOT NULL,
	[Year] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Day] [int] NOT NULL,
	[Count] [int] NOT NULL
) ON [PRIMARY]
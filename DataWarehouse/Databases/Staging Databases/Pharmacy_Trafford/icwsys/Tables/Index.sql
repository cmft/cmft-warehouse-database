﻿CREATE TABLE [icwsys].[Index](
	[IndexID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Unique] [bit] NOT NULL CONSTRAINT [DF_Index_Unique]  DEFAULT (0)
) ON [PRIMARY]
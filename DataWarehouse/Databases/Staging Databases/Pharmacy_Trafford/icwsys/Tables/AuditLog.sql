﻿CREATE TABLE [icwsys].[AuditLog](
	[AuditLogID] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[Table] [varchar](128) NOT NULL,
	[TableID] [int] NOT NULL,
	[PrimaryKey] [int] NOT NULL,
	[User] [varchar](128) NOT NULL,
	[EntityID_User] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_AuditLog_CreatedDate]  DEFAULT (getdate()),
	[Terminal] [varchar](128) NOT NULL,
	[LocationID] [int] NOT NULL,
	[LogType] [char](1) NOT NULL,
	[DataXML] [text] NOT NULL,
	[Registered] [bit] NOT NULL DEFAULT (0),
	[PrimaryKeyB] [int] NOT NULL DEFAULT (0),
	[ActivityID] [uniqueidentifier] NOT NULL DEFAULT (newid())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
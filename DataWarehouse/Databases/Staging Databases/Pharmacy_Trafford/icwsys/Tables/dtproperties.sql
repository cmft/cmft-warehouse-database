﻿CREATE TABLE [icwsys].[dtproperties](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[objectid] [int] NULL,
	[property] [varchar](64) NOT NULL,
	[value] [varchar](255) NULL,
	[uvalue] [sysname] NULL,
	[lvalue] [image] NULL,
	[version] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
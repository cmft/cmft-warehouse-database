﻿CREATE TABLE [icwsys].[PrescriptionInfusion](
	[RequestID] [int] NOT NULL,
	[UnitID_InfusionDuration] [int] NULL,
	[UnitID_RateMass] [int] NULL,
	[UnitID_RateTime] [int] NULL,
	[Continuous] [bit] NULL,
	[InfusionDuration] [float] NULL,
	[InfusionDurationLow] [float] NULL,
	[Rate] [float] NULL,
	[RateMin] [float] NULL,
	[RateMax] [float] NULL,
	[SupplimentaryText] [varchar](1024) NULL,
	[InfusionLineID] [int] NOT NULL DEFAULT (0)
) ON [PRIMARY]
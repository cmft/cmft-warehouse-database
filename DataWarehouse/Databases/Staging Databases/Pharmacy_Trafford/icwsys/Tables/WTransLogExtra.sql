﻿CREATE TABLE [icwsys].[WTransLogExtra](
	[WTransLogExtraID] [int] NOT NULL,
	[WTransLogID] [int] NOT NULL,
	[YearMonth] [varchar](6) NULL,
	[LabelText] [varchar](255) NULL,
	[StartDate] [int] NULL,
	[BatchNumber] [int] NULL,
	[LocationID_Site] [int] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[TempDoseChanges](
	[NSVCode] [varchar](7) NULL,
	[Printformv] [nvarchar](255) NULL,
	[doseperissueunit] [float] NULL,
	[dosingunits] [nvarchar](255) NULL,
	[dpsform] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[RepeatDispensingPrescriptionLinkDispensing](
	[PrescriptionID] [int] NOT NULL,
	[DispensingID] [int] NOT NULL,
	[InUse] [bit] NULL,
	[Quantity] [float] NOT NULL,
	[RepeatDispensingPrescriptionLinkDispensingID] [int] IDENTITY(1,1) NOT NULL,
	[SessionLock] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[RequestBatchNumber](
	[RequestBatchNumberID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[BatchNumber] [varchar](50) NULL,
	[BatchExpiryDate] [datetime] NULL
) ON [PRIMARY]
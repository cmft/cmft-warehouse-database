﻿CREATE TABLE [icwsys].[Table](
	[TableID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Table_DateCreated]  DEFAULT (getdate()),
	[SystemTable] [bit] NOT NULL CONSTRAINT [DF_Table_SystemTable]  DEFAULT (0),
	[Reference] [bit] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[DisplayName] [varchar](256) NOT NULL DEFAULT (''),
	[DisplayWeighting] [int] NOT NULL DEFAULT (0)
) ON [PRIMARY]
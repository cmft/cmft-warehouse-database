﻿CREATE TABLE [icwsys].[RoutineType](
	[RoutineTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EntityTypeBackup](
	[EntityTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
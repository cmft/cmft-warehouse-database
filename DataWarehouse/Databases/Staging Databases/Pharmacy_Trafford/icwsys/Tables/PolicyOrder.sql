﻿CREATE TABLE [icwsys].[PolicyOrder](
	[PolicyOrderID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](50) NOT NULL
) ON [PRIMARY]
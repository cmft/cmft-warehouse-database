﻿CREATE TABLE [icwsys].[EntityOrderHistory](
	[EntityOrderHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[OrderTemplateID] [int] NULL,
	[RequestTypeID] [int] NULL,
	[DateLastUsed] [datetime] NOT NULL CONSTRAINT [DF_EntityOrderHistory_DateLastUsed]  DEFAULT (getdate()),
	[Usage] [int] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[AuditLogException](
	[AuditLogExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL
) ON [PRIMARY]
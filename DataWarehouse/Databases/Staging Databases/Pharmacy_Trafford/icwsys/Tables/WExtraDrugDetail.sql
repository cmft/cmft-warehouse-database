﻿CREATE TABLE [icwsys].[WExtraDrugDetail](
	[WExtraDrugDetailID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID_Site] [int] NOT NULL,
	[NSVCode] [varchar](7) NULL,
	[NewContractNumber] [varchar](10) NULL,
	[NewContractPrice] [varchar](9) NULL,
	[DateofChange] [varchar](10) NULL,
	[SupplierCode] [varchar](5) NULL,
	[DateUpdated] [varchar](10) NULL,
	[NewStopDate] [varchar](10) NULL,
	[UpDatedBy] [varchar](3) NULL,
	[DateEntered] [varchar](10) NULL,
	[NewSupRefNo] [varchar](36) NULL,
	[NewSupplierTradeName] [varchar](30) NULL,
	[SetDefaultSupplier] [bit] NULL
) ON [PRIMARY]
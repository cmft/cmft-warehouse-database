﻿CREATE TABLE [icwsys].[RuleRange](
	[RuleRangeID] [int] IDENTITY(1,1) NOT NULL,
	[RuleID] [int] NOT NULL,
	[RangeID] [int] NOT NULL,
	[ColumnID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
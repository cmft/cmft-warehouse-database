﻿CREATE TABLE [icwsys].[RobotLoadingMsgTemplate](
	[RobotLoadingMsgTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[RobotName] [varchar](25) NULL,
	[MessageType] [varchar](25) NULL,
	[Name] [varchar](50) NULL,
	[MessageTemplate] [varchar](250) NULL
) ON [PRIMARY]
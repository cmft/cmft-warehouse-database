﻿CREATE TABLE [icwsys].[ProductCustomisation](
	[ProductCustomisationID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[ReportGroupID] [int] NOT NULL
) ON [PRIMARY]

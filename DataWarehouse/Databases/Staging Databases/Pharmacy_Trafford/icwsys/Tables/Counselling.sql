﻿CREATE TABLE [icwsys].[Counselling](
	[RequestID] [int] NOT NULL,
	[Comment] [char](100) NULL,
	[CounsellingTypeID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ProductOrderCatalogueRootOLAP](
	[ProductID] [int] NOT NULL,
	[OrderCatalogueRootID] [int] NOT NULL,
	[OrderCatalogueRoot] [varchar](255) NOT NULL
) ON [PRIMARY]
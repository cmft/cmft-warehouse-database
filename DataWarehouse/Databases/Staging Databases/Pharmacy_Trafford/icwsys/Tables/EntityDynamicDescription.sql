﻿CREATE TABLE [icwsys].[EntityDynamicDescription](
	[EntityDynamicDescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[EntityTypeID] [int] NOT NULL,
	[ColumnID] [int] NULL,
	[ColumnID_Lookup] [int] NULL,
	[Order] [int] NOT NULL,
	[Description] [varchar](128) NULL
) ON [PRIMARY]

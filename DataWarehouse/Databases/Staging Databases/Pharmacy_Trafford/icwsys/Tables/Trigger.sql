﻿CREATE TABLE [icwsys].[Trigger](
	[TriggerID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Script] [varchar](7000) NOT NULL
) ON [PRIMARY]
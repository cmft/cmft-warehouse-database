﻿CREATE TABLE [icwsys].[ReportGroup](
	[ReportGroupID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Detail] [varchar](1024) NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[OrderReportType](
	[OrderReportTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NULL
) ON [PRIMARY]
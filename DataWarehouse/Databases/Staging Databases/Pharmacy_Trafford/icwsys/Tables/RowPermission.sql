﻿CREATE TABLE [icwsys].[RowPermission](
	[RowPermissionID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[PrimaryKey] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[PolicyID] [int] NOT NULL,
	[Allow] [bit] NOT NULL
) ON [PRIMARY]
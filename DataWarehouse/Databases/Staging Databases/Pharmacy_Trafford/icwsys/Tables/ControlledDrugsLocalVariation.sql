﻿CREATE TABLE [icwsys].[ControlledDrugsLocalVariation](
	[ControlledDrugsLocalVariationID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductRouteID] [int] NOT NULL,
	[CDHandwriting] [bit] NULL,
	[CDCupboard] [bit] NULL
) ON [PRIMARY]
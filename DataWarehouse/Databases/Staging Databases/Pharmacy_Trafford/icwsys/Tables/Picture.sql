﻿CREATE TABLE [icwsys].[Picture](
	[PictureID] [int] IDENTITY(1,1) NOT NULL,
	[PictureTypeID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[ImageFormat] [varchar](3) NULL,
	[Data] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

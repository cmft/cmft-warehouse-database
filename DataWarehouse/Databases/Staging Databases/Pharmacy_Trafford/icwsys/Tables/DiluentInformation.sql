﻿CREATE TABLE [icwsys].[DiluentInformation](
	[DiluentInformationID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NULL,
	[DiluentProductID] [int] NULL,
	[DiluentQuantity] [float] NULL,
	[FinalVolume] [float] NULL,
	[InfusionDeviceID] [int] NULL,
	[Instructions] [text] NULL,
	[Nominal] [bit] NOT NULL DEFAULT (0),
	[Exact] [bit] NOT NULL DEFAULT (0),
	[FinalConcentration] [float] NULL,
	[Dose_Calculated] [bit] NOT NULL DEFAULT (0),
	[FinalVolume_Calculated] [bit] NOT NULL DEFAULT (0),
	[FinalConcentration_Calculated] [bit] NOT NULL DEFAULT (0),
	[DiluentQuantity_Calculated] [bit] NOT NULL DEFAULT (0),
	[VolumeOfIngredients] [float] NULL,
	[VolumeOfLiquidIngredients] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[wStoreArchiveLog](
	[StoreArchiveLogID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Status] [char](1) NOT NULL,
	[Counter] [int] NOT NULL,
	[Created] [datetime] NOT NULL DEFAULT (getdate()),
	[Archived] [bit] NOT NULL,
	[Error] [varchar](1000) NULL,
	[SourceID] [int] NOT NULL
) ON [PRIMARY]
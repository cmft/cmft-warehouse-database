﻿CREATE TABLE [icwsys].[InterfaceTable](
	[InterfaceTableID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[InterfaceTableTypeID] [int] NOT NULL,
	[InterfaceComponentID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[SMSPrintBatch](
	[PrintBatchID] [int] NOT NULL,
	[ConsultantID] [int] NULL,
	[SMSPrintBatchModeID] [int] NOT NULL
) ON [PRIMARY]
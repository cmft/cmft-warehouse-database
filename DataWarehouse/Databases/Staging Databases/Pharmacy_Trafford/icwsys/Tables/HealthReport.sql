﻿CREATE TABLE [icwsys].[HealthReport](
	[NoteID] [int] NOT NULL,
	[Date] [datetime] NULL,
	[Diet] [varchar](50) NULL,
	[Mobile] [bit] NULL
) ON [PRIMARY]
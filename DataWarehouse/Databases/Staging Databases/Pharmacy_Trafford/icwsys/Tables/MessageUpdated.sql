﻿CREATE TABLE [icwsys].[MessageUpdated](
	[MessageUpdatedID] [int] IDENTITY(1,1) NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL,
	[TableID] [int] NOT NULL,
	[PrimaryKey] [int] NOT NULL
) ON [PRIMARY]
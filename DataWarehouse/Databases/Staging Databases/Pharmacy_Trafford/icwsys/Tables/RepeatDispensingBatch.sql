﻿CREATE TABLE [icwsys].[RepeatDispensingBatch](
	[RepeatDispensingBatchID] [int] IDENTITY(1,1) NOT NULL,
	[BatchDescription] [varchar](8000) NOT NULL,
	[StatusID] [int] NOT NULL,
	[Factor] [int] NOT NULL
) ON [PRIMARY]
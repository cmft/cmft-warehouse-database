﻿CREATE TABLE [icwsys].[PermissionEntity](
	[PolicyEntityID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[EntityTypeID] [int] NOT NULL,
	[Allow] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[OrderReport](
	[OrderReportID] [int] IDENTITY(1,1) NOT NULL,
	[OrderReportTypeID] [int] NOT NULL,
	[MediaTypeID] [int] NOT NULL,
	[RequestTypeID] [int] NOT NULL,
	[ResponseTypeID] [int] NOT NULL,
	[NoteTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[RichTextDocumentID] [int] NOT NULL,
	[RoutineID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[OrderTemplateID_Orderset] [int] NOT NULL DEFAULT (0),
	[ModalityID] [int] NOT NULL DEFAULT (0)
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[FreeFormatLabel](
	[RequestID] [int] NOT NULL,
	[LabelText] [varchar](250) NULL,
	[HeaderText] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[NoteAlias](
	[NoteAliasID] [int] IDENTITY(1,1) NOT NULL,
	[NoteID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]

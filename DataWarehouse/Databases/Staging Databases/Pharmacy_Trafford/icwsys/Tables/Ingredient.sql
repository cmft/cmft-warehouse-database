﻿CREATE TABLE [icwsys].[Ingredient](
	[IngredientID] [int] IDENTITY(0,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[UnitID] [int] NULL,
	[Quantity] [float] NULL,
	[QuantityMin] [float] NULL,
	[QuantityMax] [float] NULL,
	[UnitID_Time] [int] NULL
) ON [PRIMARY]

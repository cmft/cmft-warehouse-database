﻿CREATE TABLE [icwsys].[ControlFunction](
	[ControlFunctionID] [int] IDENTITY(1,1) NOT NULL,
	[ControlFunctionTypeID] [int] NOT NULL,
	[ControlID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
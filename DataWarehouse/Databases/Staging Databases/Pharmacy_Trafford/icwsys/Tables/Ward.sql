﻿CREATE TABLE [icwsys].[Ward](
	[LocationID] [int] NOT NULL,
	[WardTypeID] [int] NOT NULL,
	[Male] [bit] NOT NULL,
	[Female] [bit] NOT NULL,
	[BedCount] [int] NOT NULL,
	[SingleRooms] [int] NOT NULL,
	[out_of_use] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[ResponseTypeAlias](
	[ResponseTypeAliasID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseTypeID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]
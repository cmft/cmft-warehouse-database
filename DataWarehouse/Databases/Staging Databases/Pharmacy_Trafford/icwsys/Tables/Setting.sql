﻿CREATE TABLE [icwsys].[Setting](
	[System] [varchar](50) NOT NULL,
	[Section] [varchar](50) NOT NULL,
	[Key] [varchar](50) NOT NULL,
	[Value] [varchar](512) NOT NULL,
	[Description] [varchar](1024) NULL,
	[RoleID] [int] NOT NULL DEFAULT (0)
) ON [PRIMARY]
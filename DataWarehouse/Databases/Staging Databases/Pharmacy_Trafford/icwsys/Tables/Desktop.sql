﻿CREATE TABLE [icwsys].[Desktop](
	[DesktopID] [int] IDENTITY(1,1) NOT NULL,
	[PolicyID] [int] NOT NULL,
	[ToolMenuID_Menu] [int] NOT NULL,
	[ToolMenuID_Toolbar] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[PictureName] [varchar](255) NULL,
	[ShowInShortcutBar] [bit] NOT NULL CONSTRAINT [DF_Desktop_ShowInShortcutBar]  DEFAULT (0)
) ON [PRIMARY]
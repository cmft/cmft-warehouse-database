﻿CREATE TABLE [icwsys].[Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[Individual] [bit] NOT NULL CONSTRAINT [DF_Role_Individual]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[SupportRequest](
	[RequestID] [int] NOT NULL,
	[Date] [datetime] NULL,
	[Problem] [text] NULL,
	[SeverityID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
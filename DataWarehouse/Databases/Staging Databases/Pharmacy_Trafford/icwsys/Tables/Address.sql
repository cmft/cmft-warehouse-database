﻿CREATE TABLE [icwsys].[Address](
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[BoxNumber] [varchar](30) NULL,
	[DoorNumber] [varchar](10) NULL,
	[Building] [varchar](50) NULL,
	[Street] [varchar](50) NULL,
	[Town] [varchar](50) NULL,
	[LocalAuthority] [varchar](50) NULL,
	[District] [varchar](50) NULL,
	[PostCode] [varchar](15) NULL,
	[Province] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Notes] [varchar](1024) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[KeyDate](
	[KeyDateID] [int] IDENTITY(1,1) NOT NULL,
	[Epoch] [datetime] NOT NULL,
	[Estimated] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ResponseDynamicDescription](
	[ResponseDynamicDescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseTypeID] [int] NOT NULL,
	[Short] [bit] NOT NULL,
	[ColumnID] [int] NULL,
	[ColumnID_Lookup] [int] NULL,
	[Order] [int] NOT NULL,
	[Description] [varchar](128) NULL
) ON [PRIMARY]

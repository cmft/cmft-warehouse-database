﻿CREATE TABLE [icwsys].[RecordOfEntityAliasMerge](
	[RecordOfEntityAliasMergeID] [int] IDENTITY(1,1) NOT NULL,
	[NoteID] [int] NOT NULL,
	[EntityID_Before] [int] NOT NULL,
	[EntityID_After] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL CONSTRAINT [DF_RecordOfEntityAliasMerge_Default]  DEFAULT (0)
) ON [PRIMARY]
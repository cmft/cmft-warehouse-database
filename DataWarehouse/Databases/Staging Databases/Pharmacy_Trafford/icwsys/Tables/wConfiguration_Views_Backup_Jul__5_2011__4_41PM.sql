﻿CREATE TABLE [icwsys].[wConfiguration_Views_Backup_Jul__5_2011__4_41PM](
	[WConfigurationID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Category] [varchar](255) NOT NULL,
	[Section] [varchar](255) NOT NULL,
	[Key] [varchar](255) NOT NULL,
	[Value] [varchar](1024) NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
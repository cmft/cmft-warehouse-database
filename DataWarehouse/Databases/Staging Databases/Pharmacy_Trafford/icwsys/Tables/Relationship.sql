﻿CREATE TABLE [icwsys].[Relationship](
	[ColumnID_PK] [int] NOT NULL,
	[ColumnID_FK] [int] NOT NULL,
	[Inheritance] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ColumnAlias](
	[ColumnAliasID] [int] IDENTITY(1,1) NOT NULL,
	[ColumnID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](128) NOT NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]
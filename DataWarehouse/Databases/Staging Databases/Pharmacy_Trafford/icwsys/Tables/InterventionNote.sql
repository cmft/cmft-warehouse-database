﻿CREATE TABLE [icwsys].[InterventionNote](
	[NoteID] [int] NOT NULL,
	[Detail] [varchar](2048) NOT NULL
) ON [PRIMARY]

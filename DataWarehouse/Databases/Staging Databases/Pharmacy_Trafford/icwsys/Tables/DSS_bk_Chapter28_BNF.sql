﻿CREATE TABLE [icwsys].[DSS_bk_Chapter28_BNF](
	[OrderCatalogueID] [int] NOT NULL,
	[OrderCatalogueID_Parent] [int] NOT NULL,
	[OrderCatalogueRootID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](512) NULL,
	[Hidden] [bit] NOT NULL,
	[ImageURL] [varchar](128) NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL,
	[OrderCatalogueRootID_Link] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[AdministeredProduct](
	[AdministeredProductID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[BatchNumber] [varchar](50) NULL,
	[BatchExpiryDate] [datetime] NULL
) ON [PRIMARY]

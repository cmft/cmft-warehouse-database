﻿CREATE TABLE [icwsys].[AMPPLinkProductLegalClass](
	[ProductID] [int] NOT NULL,
	[ProductLegalClassID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]

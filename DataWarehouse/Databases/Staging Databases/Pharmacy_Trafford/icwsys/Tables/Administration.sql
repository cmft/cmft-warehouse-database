﻿CREATE TABLE [icwsys].[Administration](
	[ResponseID] [int] NOT NULL,
	[ProductRouteID] [int] NOT NULL,
	[AdministeredDate] [datetime] NULL,
	[AdministeredTime] [datetime] NULL,
	[NotGiven] [bit] NOT NULL,
	[Partial] [bit] NOT NULL,
	[ArbTextID_Reason] [int] NULL,
	[EntityID_Checker] [int] NULL
) ON [PRIMARY]

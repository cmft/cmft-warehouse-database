﻿CREATE TABLE [icwsys].[ProductFormAlias](
	[ProductFormAliasID] [int] IDENTITY(1,1) NOT NULL,
	[ProductFormID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](50) NOT NULL,
	[Default] [bit] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

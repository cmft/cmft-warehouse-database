﻿CREATE TABLE [icwsys].[SiteProductDataAlias](
	[SiteProductDataAliasID] [int] IDENTITY(1,1) NOT NULL,
	[SiteProductDataID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
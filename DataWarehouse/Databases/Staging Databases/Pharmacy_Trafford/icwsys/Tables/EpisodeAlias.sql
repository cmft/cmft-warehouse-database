﻿CREATE TABLE [icwsys].[EpisodeAlias](
	[EpisodeAliasID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL CONSTRAINT [DF_EpisodeAlias_Default]  DEFAULT (0)
) ON [PRIMARY]

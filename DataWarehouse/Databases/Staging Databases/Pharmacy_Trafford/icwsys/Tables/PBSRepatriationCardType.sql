﻿CREATE TABLE [icwsys].[PBSRepatriationCardType](
	[PBSRepatriationCardTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Code] [varchar](1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Detail] [varchar](255) NULL
) ON [PRIMARY]
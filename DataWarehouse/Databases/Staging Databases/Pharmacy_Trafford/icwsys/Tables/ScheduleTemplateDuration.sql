﻿CREATE TABLE [icwsys].[ScheduleTemplateDuration](
	[ScheduleTemplateDurationID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[Duration] [int] NOT NULL,
	[InUse] [bit] NOT NULL,
	[DSS] [tinyint] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
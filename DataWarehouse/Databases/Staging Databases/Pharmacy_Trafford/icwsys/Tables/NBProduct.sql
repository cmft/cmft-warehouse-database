﻿CREATE TABLE [icwsys].[NBProduct](
	[ProductID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Display] [bit] NOT NULL,
	[MDA] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[AttachedNote](
	[NoteID] [int] NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_AttachedNote_Enabled]  DEFAULT (1)
) ON [PRIMARY]
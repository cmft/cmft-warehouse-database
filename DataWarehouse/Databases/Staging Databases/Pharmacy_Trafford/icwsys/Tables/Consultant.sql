﻿CREATE TABLE [icwsys].[Consultant](
	[EntityID] [int] NOT NULL,
	[EntityID_Secretary] [int] NULL,
	[GMCCode] [varchar](20) NULL,
	[Qualification] [varchar](50) NULL,
	[out_of_use] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
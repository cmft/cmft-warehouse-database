﻿CREATE TABLE [icwsys].[RequestAlias](
	[RequestAliasID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]

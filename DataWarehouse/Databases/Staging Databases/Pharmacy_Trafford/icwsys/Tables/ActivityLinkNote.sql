﻿CREATE TABLE [icwsys].[ActivityLinkNote](
	[ActivityID] [numeric](9, 0) NOT NULL,
	[NoteID] [int] NOT NULL
) ON [PRIMARY]
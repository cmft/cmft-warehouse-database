﻿CREATE TABLE [icwsys].[Response](
	[ResponseID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseTypeID] [int] NOT NULL,
	[RequestID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Response_CreatedDate]  DEFAULT (getdate()),
	[ResponseDate] [datetime] NOT NULL CONSTRAINT [DF_Response_ResponseDate]  DEFAULT (getdate()),
	[Description] [varchar](128) NOT NULL,
	[ShortDescription] [varchar](50) NOT NULL
) ON [PRIMARY]

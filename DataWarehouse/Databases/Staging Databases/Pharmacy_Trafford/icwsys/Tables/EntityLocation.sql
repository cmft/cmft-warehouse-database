﻿CREATE TABLE [icwsys].[EntityLocation](
	[EntityLocationID] [int] IDENTITY(1,1) NOT NULL,
	[EntityLocationTypeID] [int] NULL,
	[EntityID] [int] NOT NULL,
	[EntityTypeID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Preference] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL CONSTRAINT [DF_EntityLocation_StartDate]  DEFAULT (getdate()),
	[StopDate] [datetime] NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_EntityLocation_Active]  DEFAULT (0)
) ON [PRIMARY]

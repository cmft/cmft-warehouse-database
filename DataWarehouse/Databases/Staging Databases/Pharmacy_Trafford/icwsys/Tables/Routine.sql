﻿CREATE TABLE [icwsys].[Routine](
	[RoutineID] [int] IDENTITY(1,1) NOT NULL,
	[RoutineTypeID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[ShortDescription] [varchar](16) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[IndexKey](
	[IndexKeyID] [int] IDENTITY(1,1) NOT NULL,
	[IndexID] [int] NOT NULL,
	[ColumnID] [int] NOT NULL,
	[Order] [int] NOT NULL CONSTRAINT [DF_IndexKey_Order]  DEFAULT (1),
	[SortOrder] [char](1) NOT NULL CONSTRAINT [DF_IndexKey_SortOrder]  DEFAULT ('A')
) ON [PRIMARY]
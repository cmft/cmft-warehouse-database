﻿CREATE TABLE [icwsys].[InterfaceTypeRegister](
	[InterfaceTypeRegisterID] [int] IDENTITY(1,1) NOT NULL,
	[TableID_Type] [int] NOT NULL,
	[ID_Row] [int] NOT NULL,
	[EntityID_Interface] [int] NOT NULL
) ON [PRIMARY]
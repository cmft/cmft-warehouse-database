﻿CREATE TABLE [icwsys].[RequestTypeStatusNote](
	[RequestTypeStatusNoteID] [int] IDENTITY(1,1) NOT NULL,
	[RequestTypeID] [int] NOT NULL,
	[NoteTypeID] [int] NOT NULL,
	[ApplyVerb] [varchar](50) NOT NULL,
	[DeactivateVerb] [varchar](50) NOT NULL,
	[CanDisable] [bit] NOT NULL DEFAULT (0),
	[ShowInWorkList] [bit] NOT NULL DEFAULT (1),
	[UseInPendingTray] [bit] NOT NULL DEFAULT (0),
	[UserAuthentication] [bit] NOT NULL DEFAULT (0),
	[PreconditionRoutine] [varchar](128) NULL,
	[TextWhenApplied] [varchar](128) NOT NULL DEFAULT (' '),
	[TextWhenDisabled] [varchar](128) NOT NULL DEFAULT (' '),
	[AllowDuplicates] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
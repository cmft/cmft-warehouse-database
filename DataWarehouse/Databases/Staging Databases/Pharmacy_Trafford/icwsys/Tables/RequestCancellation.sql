﻿CREATE TABLE [icwsys].[RequestCancellation](
	[NoteID] [int] NOT NULL,
	[RequestID] [int] NOT NULL,
	[DiscontinuationReasonID] [int] NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PCTPrescriber](
	[EntityID] [int] NOT NULL,
	[PCTPrescriberTypeID] [int] NOT NULL,
	[NZMCNumber] [varchar](10) NOT NULL,
	[PCTLegacyCode] [varchar](5) NULL
) ON [PRIMARY]
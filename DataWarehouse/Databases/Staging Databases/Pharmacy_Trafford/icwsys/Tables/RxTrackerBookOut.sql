﻿CREATE TABLE [icwsys].[RxTrackerBookOut](
	[ResponseID] [int] NOT NULL,
	[Comments] [text] NULL,
	[RxTrackerReturnMethodID] [int] NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
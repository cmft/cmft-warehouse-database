﻿CREATE TABLE [icwsys].[Location](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID_Parent] [int] NOT NULL CONSTRAINT [DF_Location_LocationID_Parent]  DEFAULT (0),
	[LocationTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[V8Message](
	[V8MessageID] [int] IDENTITY(1,1) NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
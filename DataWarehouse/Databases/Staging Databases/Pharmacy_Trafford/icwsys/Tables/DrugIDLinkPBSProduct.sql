﻿CREATE TABLE [icwsys].[DrugIDLinkPBSProduct](
	[DrugID] [int] NOT NULL,
	[PBSMasterProductID] [int] NOT NULL,
	[Local] [bit] NOT NULL,
	[DrugIDLinkPBSProductID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ControlRulePropertyLink](
	[ControlRuleID] [int] NOT NULL,
	[ControlID] [int] NOT NULL,
	[ControlPropertyID] [int] NOT NULL
) ON [PRIMARY]
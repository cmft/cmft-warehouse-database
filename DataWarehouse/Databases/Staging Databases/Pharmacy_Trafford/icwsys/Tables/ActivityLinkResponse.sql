﻿CREATE TABLE [icwsys].[ActivityLinkResponse](
	[ActivityID] [numeric](9, 0) NOT NULL,
	[ResponseID] [int] NOT NULL
) ON [PRIMARY]
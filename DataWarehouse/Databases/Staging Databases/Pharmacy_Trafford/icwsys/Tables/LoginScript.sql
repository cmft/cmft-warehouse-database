﻿CREATE TABLE [icwsys].[LoginScript](
	[LoginScriptID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[Precedence] [int] NOT NULL CONSTRAINT [DF_LoginScript_Precedence]  DEFAULT (0),
	[RoutineID] [int] NOT NULL
) ON [PRIMARY]
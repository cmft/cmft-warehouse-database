﻿CREATE TABLE [icwsys].[DSSLegalClassification](
	[DSSLegalClassificationID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CDCupboard] [bit] NOT NULL,
	[CDHandwriting] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
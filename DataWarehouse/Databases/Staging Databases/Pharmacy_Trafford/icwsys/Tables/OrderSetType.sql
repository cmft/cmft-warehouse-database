﻿CREATE TABLE [icwsys].[OrderSetType](
	[OrderTemplateID] [int] NOT NULL,
	[Immediate] [bit] NOT NULL CONSTRAINT [DF_OrderSetType_Immediate]  DEFAULT (0),
	[IgnorePending] [bit] NOT NULL CONSTRAINT [DF_OrderSetType_IgnorePending]  DEFAULT (0),
	[HoursPending] [int] NOT NULL CONSTRAINT [DF_OrderSetType_HoursPending]  DEFAULT (48),
	[ContentsAreOptions] [bit] NOT NULL CONSTRAINT [DF_OrderSetType_ContentsAreOptions]  DEFAULT (0),
	[SelectionOnly] [int] NOT NULL DEFAULT (0)
) ON [PRIMARY]

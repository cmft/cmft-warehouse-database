﻿CREATE TABLE [icwsys].[NoteDependancy](
	[NoteDependancyID] [int] IDENTITY(1,1) NOT NULL,
	[NoteID] [int] NOT NULL,
	[RequestID_Requisit] [int] NOT NULL,
	[NoteID_Requisit] [int] NOT NULL,
	[OffsetMinutes] [int] NOT NULL,
	[DependancyTypeID] [int] NULL,
	[IndexOrder] [int] NULL,
	[Mandatory] [bit] NOT NULL
) ON [PRIMARY]

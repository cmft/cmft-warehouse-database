﻿CREATE TABLE [icwsys].[ResponseStatus](
	[ResponseID] [int] NOT NULL,
	[authorised] [bit] NOT NULL DEFAULT (0),
	[viewed] [bit] NOT NULL DEFAULT (0),
	[ResultRead] [bit] NOT NULL DEFAULT (0),
	[ResultActioned] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
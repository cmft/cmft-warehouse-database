﻿CREATE TABLE [icwsys].[DispensingInstruction](
	[NoteID] [int] NOT NULL,
	[Detail] [varchar](4096) NOT NULL
) ON [PRIMARY]

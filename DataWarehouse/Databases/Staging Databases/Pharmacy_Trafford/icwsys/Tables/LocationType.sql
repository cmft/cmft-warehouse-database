﻿CREATE TABLE [icwsys].[LocationType](
	[LocationTypeID] [int] IDENTITY(1,1) NOT NULL,
	[LocationTypeID_Parent] [int] NOT NULL CONSTRAINT [DF_LocationType_LocationTypeID_Parent]  DEFAULT (0),
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL
) ON [PRIMARY]

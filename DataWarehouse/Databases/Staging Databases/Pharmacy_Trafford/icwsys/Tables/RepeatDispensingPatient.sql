﻿CREATE TABLE [icwsys].[RepeatDispensingPatient](
	[RepeatDispensingPatientID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[SupplyDays] [int] NULL,
	[BagLabels] [int] NULL,
	[ADM] [bit] NULL,
	[InUse] [bit] NOT NULL,
	[SupplyPatternID] [int] NULL
) ON [PRIMARY]
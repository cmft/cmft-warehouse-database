﻿CREATE TABLE [icwsys].[RoutineParameter](
	[RoutineParameterID] [int] IDENTITY(1,1) NOT NULL,
	[RoutineID] [int] NOT NULL,
	[RoutineID_Lookup] [int] NULL,
	[Order] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[DataType] [varchar](50) NOT NULL,
	[Length] [int] NOT NULL,
	[DefaultValue] [varchar](128) NOT NULL DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ProductLinkDSSLegalClassification](
	[ProductLinkDSSLegalClassificationID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductRouteID] [int] NOT NULL,
	[DSSLegalClassificationID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
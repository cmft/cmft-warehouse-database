﻿CREATE TABLE [icwsys].[Request](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID_Parent] [int] NOT NULL CONSTRAINT [DF_Request_RequestID_Parent]  DEFAULT (0),
	[RequestTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[ScheduleID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Request_CreatedDate]  DEFAULT (getdate()),
	[RequestDate] [datetime] NOT NULL CONSTRAINT [DF_Request_RequestDate]  DEFAULT (getdate()),
	[Description] [varchar](256) NOT NULL,
	[RequestID_Batch] [int] NULL,
	[CommitBatchID] [int] NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[ToolMenu](
	[ToolMenuID] [int] IDENTITY(1,1) NOT NULL,
	[ToolMenuID_Parent] [int] NOT NULL,
	[ToolMenuTypeID] [int] NOT NULL,
	[PictureName] [varchar](255) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](255) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[Shortcut] [char](1) NULL,
	[HotKey] [char](1) NULL,
	[Divider] [bit] NOT NULL,
	[ButtonData] [varchar](128) NULL DEFAULT ('')
) ON [PRIMARY]
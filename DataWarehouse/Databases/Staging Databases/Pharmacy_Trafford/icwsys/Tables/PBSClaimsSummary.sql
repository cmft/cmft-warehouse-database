﻿CREATE TABLE [icwsys].[PBSClaimsSummary](
	[SiteID] [int] NOT NULL,
	[ClaimPeriodNumber] [varchar](4) NOT NULL,
	[SiteNumber] [int] NULL,
	[DateClosed] [datetime] NULL,
	[DateSubmitted] [datetime] NULL,
	[Cat1_Count] [int] NULL,
	[Cat1_Value] [money] NULL,
	[Cat2_Count] [int] NULL,
	[Cat2_Value] [money] NULL,
	[Cat3_Count] [int] NULL,
	[Cat3_Value] [money] NULL,
	[Cat4_Count] [int] NULL,
	[Cat4_Value] [money] NULL,
	[Cat5_Count] [int] NULL,
	[Cat5_Value] [money] NULL,
	[Total_Count] [float] NULL,
	[Total_Value] [money] NULL
) ON [PRIMARY]
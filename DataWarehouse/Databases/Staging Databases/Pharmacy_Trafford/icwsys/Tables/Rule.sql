﻿CREATE TABLE [icwsys].[Rule](
	[RuleID] [int] IDENTITY(1,1) NOT NULL,
	[OrderCommsEventID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](512) NULL,
	[DssReferenceID] [int] NOT NULL,
	[DssReferencePage] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[EntityID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Person](
	[EntityID] [int] NOT NULL,
	[Title] [varchar](128) NULL,
	[Initials] [varchar](10) NULL,
	[Forename] [varchar](128) NULL,
	[Surname] [varchar](128) NOT NULL,
	[Mobile] [varchar](20) NULL,
	[Pager] [varchar](20) NULL
) ON [PRIMARY]

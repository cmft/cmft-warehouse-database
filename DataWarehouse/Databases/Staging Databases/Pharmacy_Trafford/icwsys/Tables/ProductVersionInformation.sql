﻿CREATE TABLE [icwsys].[ProductVersionInformation](
	[ProductVersionInformationID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[EntityID_Update] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL DEFAULT (getdate()),
	[EntityID_QA] [int] NULL,
	[QaDate] [datetime] NULL
) ON [PRIMARY]
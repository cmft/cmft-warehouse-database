﻿CREATE TABLE [icwsys].[DischargeSummary](
	[RequestID] [int] NOT NULL,
	[OutOfHours] [bit] NOT NULL,
	[TreatmentOutcome] [varchar](1500) NULL,
	[MedicationDiscontinued] [varchar](1500) NULL,
	[MedicationStarted] [varchar](1500) NULL,
	[GPInformation] [varchar](1500) NULL,
	[PatientInformation] [varchar](1500) NULL,
	[DischargeDestinationID] [int] NOT NULL,
	[CertificateDuration] [int] NULL,
	[DischargeAddress] [varchar](500) NULL,
	[DischargeDate] [datetime] NOT NULL,
	[MedicalCertificateID] [int] NOT NULL
) ON [PRIMARY]

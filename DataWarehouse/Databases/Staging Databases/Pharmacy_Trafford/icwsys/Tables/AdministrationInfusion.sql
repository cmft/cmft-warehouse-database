﻿CREATE TABLE [icwsys].[AdministrationInfusion](
	[ResponseID] [int] NOT NULL,
	[Volume] [float] NOT NULL,
	[UnitID_Volume] [int] NOT NULL,
	[Action] [varchar](50) NOT NULL,
	[Ended] [datetime] NULL
) ON [PRIMARY]

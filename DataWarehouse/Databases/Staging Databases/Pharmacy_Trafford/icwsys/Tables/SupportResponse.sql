﻿CREATE TABLE [icwsys].[SupportResponse](
	[ResponseID] [int] NOT NULL,
	[Date] [datetime] NULL,
	[Comments] [text] NULL,
	[Complete] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
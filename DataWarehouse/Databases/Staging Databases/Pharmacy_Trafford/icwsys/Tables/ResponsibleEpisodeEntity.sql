﻿CREATE TABLE [icwsys].[ResponsibleEpisodeEntity](
	[ResponsibleEpisodeEntityID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[EntityRoleID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL CONSTRAINT [DF_ResponsibleEpisodeEntity_StartDate]  DEFAULT (getdate()),
	[EndDate] [datetime] NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_ResponsibleEpisodeEntity_Active]  DEFAULT (1)
) ON [PRIMARY]

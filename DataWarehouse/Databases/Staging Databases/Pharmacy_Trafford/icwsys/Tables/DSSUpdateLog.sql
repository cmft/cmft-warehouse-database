﻿CREATE TABLE [icwsys].[DSSUpdateLog](
	[DSSUpdateLogID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[EntityID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Data] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
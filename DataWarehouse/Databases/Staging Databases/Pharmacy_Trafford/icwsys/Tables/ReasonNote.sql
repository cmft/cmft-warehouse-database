﻿CREATE TABLE [icwsys].[ReasonNote](
	[NoteID] [int] NOT NULL,
	[OrderCatalogueID] [int] NULL,
	[ArbTextID] [int] NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
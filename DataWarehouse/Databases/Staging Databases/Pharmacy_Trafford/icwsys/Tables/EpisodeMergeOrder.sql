﻿CREATE TABLE [icwsys].[EpisodeMergeOrder](
	[RequestID] [int] NOT NULL,
	[EpisodeID_Master] [int] NOT NULL,
	[EpisodeID_Merged] [int] NOT NULL
) ON [PRIMARY]
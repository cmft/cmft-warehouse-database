﻿CREATE TABLE [icwsys].[RuleAction](
	[RuleActionID] [int] IDENTITY(1,1) NOT NULL,
	[RuleActionTypeID] [int] NOT NULL,
	[RuleID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[EntityID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ObservationNoUnit](
	[NoteID] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[ObservationDate] [datetime] NOT NULL,
	[ObservationTime] [datetime] NOT NULL
) ON [PRIMARY]
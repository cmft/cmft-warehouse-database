﻿CREATE TABLE [icwsys].[EpisodeLocation](
	[EpisodeLocationID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL DEFAULT (getdate()),
	[EndDate] [datetime] NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
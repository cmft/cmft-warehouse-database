﻿CREATE TABLE [icwsys].[CounselResponse](
	[ResponseID] [int] NOT NULL,
	[Comment] [char](100) NULL,
	[CounsellingResponseID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[NumericResult](
	[ResponseID] [int] NOT NULL,
	[HighRange] [float] NULL,
	[LowRange] [float] NULL,
	[Preliminary] [bit] NULL,
	[RangeTestOutcomeID] [int] NULL,
	[UnitID] [int] NULL,
	[Value] [float] NULL
) ON [PRIMARY]
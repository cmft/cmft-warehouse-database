﻿CREATE TABLE [icwsys].[Permission](
	[PolicyID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[Allow] [bit] NOT NULL CONSTRAINT [DF_Permission_Allow]  DEFAULT (1),
	[PermissionID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
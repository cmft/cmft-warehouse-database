﻿CREATE TABLE [icwsys].[NextOfKin](
	[EntityID] [int] NOT NULL,
	[WorkTelephoneNo] [varchar](20) NULL,
	[FamilyRelationshipID] [int] NOT NULL
) ON [PRIMARY]

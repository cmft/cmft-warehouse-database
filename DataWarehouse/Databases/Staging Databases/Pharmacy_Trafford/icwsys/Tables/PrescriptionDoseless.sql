﻿CREATE TABLE [icwsys].[PrescriptionDoseless](
	[RequestID] [int] NOT NULL,
	[DirectionText] [varchar](1024) NULL,
	[NoDoseInfo] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

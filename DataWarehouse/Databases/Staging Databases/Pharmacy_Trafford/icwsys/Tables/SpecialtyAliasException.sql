﻿CREATE TABLE [icwsys].[SpecialtyAliasException](
	[SpecialtyAliasExceptionID] [int] IDENTITY(0,1) NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Failed] [int] NOT NULL,
	[LastFailure] [datetime] NOT NULL
) ON [PRIMARY]
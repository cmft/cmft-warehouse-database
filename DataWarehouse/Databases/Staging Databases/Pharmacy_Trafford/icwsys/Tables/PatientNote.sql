﻿CREATE TABLE [icwsys].[PatientNote](
	[NoteID] [int] NOT NULL,
	[Notes] [varchar](50) NULL,
	[HighPriority] [bit] NULL,
	[SuppliedBy] [int] NOT NULL
) ON [PRIMARY]
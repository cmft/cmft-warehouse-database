﻿CREATE TABLE [icwsys].[Message](
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Message_MessageGuid]  DEFAULT (newid()),
	[Text] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
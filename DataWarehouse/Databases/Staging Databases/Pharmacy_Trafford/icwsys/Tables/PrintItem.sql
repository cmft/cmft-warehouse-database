﻿CREATE TABLE [icwsys].[PrintItem](
	[PrintItemID] [int] IDENTITY(1,1) NOT NULL,
	[PrintBatchID] [int] NOT NULL,
	[OrderReportID] [int] NOT NULL,
	[RecordID] [int] NULL,
	[Data] [text] NULL,
	[PrintedDate] [datetime] NULL,
	[PrintStatusID] [int] NOT NULL,
	[PrintGroup] [int] NULL,
	[Copies] [int] NOT NULL DEFAULT (1)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ProductOrder](
	[RequestID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NULL,
	[PrintableDirection] [varchar](72) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WWardStockList](
	[ScreenPosn] [int] NULL,
	[NSVcode] [varchar](7) NULL,
	[TitleText] [varchar](56) NULL,
	[PrintLabel] [varchar](1) NULL,
	[SiteName] [varchar](5) NULL,
	[TopupLvl] [smallint] NULL,
	[LastIssue] [smallint] NULL,
	[PackSize] [int] NULL,
	[LastIssueDate] [varchar](10) NULL,
	[LocalCode] [varchar](7) NULL,
	[SiteID] [int] NULL,
	[Barcode] [varchar](15) NULL,
	[DailyIssue] [int] NULL,
	[WWardStockListID] [int] IDENTITY(1,1) NOT NULL,
	[WSupplierID] [int] NULL
) ON [PRIMARY]

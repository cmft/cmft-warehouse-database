﻿CREATE TABLE [icwsys].[Unit](
	[UnitID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID_LCD] [int] NOT NULL,
	[UnitTypeID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Abbreviation] [varchar](10) NOT NULL,
	[Multiple] [float] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

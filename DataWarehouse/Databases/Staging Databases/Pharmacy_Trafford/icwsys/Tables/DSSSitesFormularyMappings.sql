﻿CREATE TABLE [icwsys].[DSSSitesFormularyMappings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrderTemplateID] [int] NULL,
	[OrderCatalogueID] [int] NULL,
	[OrderCatalogueRootID] [int] NULL
) ON [PRIMARY]
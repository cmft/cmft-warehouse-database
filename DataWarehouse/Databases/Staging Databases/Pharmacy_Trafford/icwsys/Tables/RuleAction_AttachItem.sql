﻿CREATE TABLE [icwsys].[RuleAction_AttachItem](
	[RuleAction_AttachItemID] [int] IDENTITY(1,1) NOT NULL,
	[RuleActionID] [int] NOT NULL,
	[Number] [tinyint] NOT NULL,
	[RequestTypeID] [int] NOT NULL,
	[NoteTypeID] [int] NOT NULL,
	[ResponseTypeID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
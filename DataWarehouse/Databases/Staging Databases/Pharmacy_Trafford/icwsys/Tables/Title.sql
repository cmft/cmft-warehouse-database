﻿CREATE TABLE [icwsys].[Title](
	[TitleID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL
) ON [PRIMARY]
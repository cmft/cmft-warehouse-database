﻿CREATE TABLE [icwsys].[PatientDemographicData](
	[NoteID] [int] NOT NULL,
	[WorkTelephoneNo] [varchar](20) NULL,
	[Occupation] [varchar](255) NULL,
	[EmploymentStatusID] [int] NULL,
	[MaritalStatusID] [int] NULL,
	[ReligionID] [int] NULL,
	[LanguageSpokenID] [int] NULL,
	[EthnicGroupID] [int] NULL,
	[DateOfDeath] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[AdministrationDiluentReconstitution](
	[AdministrationDiluentReconstitutionID] [int] IDENTITY(1,1) NOT NULL,
	[AdministrationDiluentInformationID] [int] NULL,
	[Concentration] [float] NULL,
	[DisplacementVolume] [float] NULL,
	[DoseRequired] [float] NULL,
	[Instruction] [varchar](256) NULL,
	[ProductID] [int] NULL,
	[ReconstituteProductID] [int] NULL,
	[ReconstitutionRequired] [bit] NOT NULL,
	[VialSize] [int] NULL,
	[Volume] [float] NULL,
	[Concentration_Calculated] [bit] NOT NULL,
	[Concentration_Unit] [varchar](10) NULL,
	[Concentration_UnitID] [int] NULL,
	[Concentration_Multiple] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[DrugAdministrationResponse](
	[ResponseID] [int] NOT NULL,
	[RequestID_Prescription] [int] NOT NULL,
	[Detail] [varchar](2048) NULL
) ON [PRIMARY]

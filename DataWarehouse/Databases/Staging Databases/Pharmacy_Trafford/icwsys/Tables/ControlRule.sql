﻿CREATE TABLE [icwsys].[ControlRule](
	[ControlRuleID] [int] IDENTITY(1,1) NOT NULL,
	[ControlFunctionID] [int] NOT NULL,
	[Operator] [varchar](20) NOT NULL,
	[CompareValue] [varchar](50) NOT NULL
) ON [PRIMARY]
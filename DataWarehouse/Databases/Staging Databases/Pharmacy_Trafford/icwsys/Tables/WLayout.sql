﻿CREATE TABLE [icwsys].[WLayout](
	[WLayoutID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID_Site] [int] NOT NULL,
	[PatientsPerSheet] [int] NULL,
	[Layout] [varchar](50) NULL,
	[LineText] [varchar](1024) NULL,
	[IngLineText] [varchar](1024) NULL,
	[Prescription] [varchar](5000) NULL,
	[name] [varchar](10) NULL,
	[WManufacturingStatusID] [int] NOT NULL DEFAULT (0),
	[EntityID_Drafted] [int] NOT NULL DEFAULT (0),
	[EntityID_Approved] [int] NOT NULL DEFAULT (0),
	[DateDrafted] [datetime] NULL DEFAULT (getdate()),
	[DateApproved] [datetime] NULL DEFAULT (getdate()),
	[VersionNumber] [int] NOT NULL DEFAULT (1)
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Gender](
	[GenderID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](20) NOT NULL
) ON [PRIMARY]

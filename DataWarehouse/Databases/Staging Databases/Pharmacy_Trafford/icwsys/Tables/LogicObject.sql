﻿CREATE TABLE [icwsys].[LogicObject](
	[LogicObjectID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[ExecutionStep] [char](1) NOT NULL,
	[ExecutionOrder] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Path] [varchar](512) NOT NULL
) ON [PRIMARY]
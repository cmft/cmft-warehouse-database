﻿CREATE TABLE [icwsys].[PatientCareDetails](
	[NoteID] [int] NOT NULL,
	[SpecialtyID] [int] NOT NULL,
	[PrivatePatient] [bit] NOT NULL DEFAULT (0),
	[PatientPaymentCategoryID] [int] NULL CONSTRAINT [DF__PatientCa__PaymentCat]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PermissionOrder](
	[PolicyOrderID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[Allow] [bit] NOT NULL
) ON [PRIMARY]
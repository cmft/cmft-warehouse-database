﻿CREATE TABLE [icwsys].[ArbText](
	[ArbTextID] [int] IDENTITY(1,1) NOT NULL,
	[ArbTextTypeID] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[InUse] [bit] NOT NULL CONSTRAINT [DF_ArbText_InUse]  DEFAULT (1),
	[DSS] [bit] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
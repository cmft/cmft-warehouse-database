﻿CREATE TABLE [icwsys].[RepeatDispensingBatchStatus](
	[StatusID] [int] NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Code] [char](1) NOT NULL
) ON [PRIMARY]
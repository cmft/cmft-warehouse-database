﻿CREATE TABLE [icwsys].[InterfaceRegister](
	[InterfaceRegisterID] [int] IDENTITY(1,1) NOT NULL,
	[AuditLogID] [numeric](9, 0) NOT NULL,
	[EntityID_Interface] [int] NOT NULL
) ON [PRIMARY]
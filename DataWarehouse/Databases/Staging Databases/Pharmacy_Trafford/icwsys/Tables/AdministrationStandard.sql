﻿CREATE TABLE [icwsys].[AdministrationStandard](
	[ResponseID] [int] NOT NULL,
	[Dose] [float] NOT NULL,
	[UnitID_Dose] [int] NOT NULL
) ON [PRIMARY]

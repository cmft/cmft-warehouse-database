﻿CREATE TABLE [icwsys].[AMP](
	[ProductID] [int] NOT NULL,
	[EntityID_Manufacturer] [int] NOT NULL,
	[Divisible] [bit] NOT NULL,
	[ProductLabelInstructionID] [int] NOT NULL DEFAULT (0),
	[ProductLabelWarningID] [int] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

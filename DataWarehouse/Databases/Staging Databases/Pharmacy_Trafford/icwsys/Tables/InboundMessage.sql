﻿CREATE TABLE [icwsys].[InboundMessage](
	[InboundMessageID] [int] IDENTITY(1,1) NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
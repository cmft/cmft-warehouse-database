﻿CREATE TABLE [icwsys].[RichTextDocument](
	[RichTextDocumentID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

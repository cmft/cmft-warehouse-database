﻿CREATE TABLE [icwsys].[Patient](
	[EntityID] [int] NOT NULL,
	[GenderID] [int] NOT NULL,
	[EntityID_NextOfKin] [int] NULL,
	[DOB] [datetime] NULL,
	[DOBEstYear] [bit] NOT NULL CONSTRAINT [DF_Patient_DOBEstYear]  DEFAULT (0),
	[DOBEstMonth] [bit] NOT NULL CONSTRAINT [DF_Patient_DOBEstMonth]  DEFAULT (0),
	[DOBEstDay] [bit] NOT NULL CONSTRAINT [DF_Patient_DOBEstDay]  DEFAULT (0),
	[NHSNumber] [varchar](10) NULL,
	[NHSNumberValid] [varchar](4) NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[DiscontinuationReason](
	[DiscontinuationReasonID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[ClinicalReason] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[RequestTypeLinkResponseType](
	[RequestTypeLinkResponseTypeID] [int] IDENTITY(1,1) NOT NULL,
	[RequestTypeID] [int] NOT NULL,
	[ResponseTypeID] [int] NOT NULL,
	[AllowDuplicates] [bit] NOT NULL CONSTRAINT [DF_RequestTypeLinkResponseType_AllowDuplicates]  DEFAULT (0)
) ON [PRIMARY]
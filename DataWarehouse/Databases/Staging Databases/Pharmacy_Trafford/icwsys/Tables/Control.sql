﻿CREATE TABLE [icwsys].[Control](
	[ControlID] [int] IDENTITY(1,1) NOT NULL,
	[ControlTypeID] [int] NOT NULL,
	[ControlSpanID] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[TabIndex] [int] NOT NULL,
	[BreakAfter] [bit] NOT NULL,
	[Sync] [bit] NOT NULL,
	[Caption] [varchar](50) NULL,
	[Bold] [bit] NOT NULL,
	[Italic] [bit] NOT NULL,
	[LineCount] [int] NULL,
	[Tag] [varchar](50) NULL
) ON [PRIMARY]
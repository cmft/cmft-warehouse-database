﻿CREATE TABLE [icwsys].[RuleAction_Warning](
	[RuleActionID] [int] IDENTITY(1,1) NOT NULL,
	[Overrideable] [bit] NOT NULL,
	[Text] [varchar](2048) NOT NULL
) ON [PRIMARY]
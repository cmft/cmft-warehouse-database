﻿CREATE TABLE [icwsys].[OCXToken](
	[OCXTokenID] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [int] NOT NULL,
	[RandomNumber] [int] NOT NULL,
	[Seed] [int] NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_OCXToken_Created]  DEFAULT (getdate())
) ON [PRIMARY]
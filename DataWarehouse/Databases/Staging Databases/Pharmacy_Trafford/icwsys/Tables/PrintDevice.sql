﻿CREATE TABLE [icwsys].[PrintDevice](
	[PrintDeviceID] [int] IDENTITY(1,1) NOT NULL,
	[CookieID] [int] NOT NULL,
	[MediaTypeID] [int] NOT NULL,
	[Description] [varchar](128) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[DSS_bk28_02_BNF](
	[OrderCatalogueID] [int] NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
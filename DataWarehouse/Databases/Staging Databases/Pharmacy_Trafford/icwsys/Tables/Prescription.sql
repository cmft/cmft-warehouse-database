﻿CREATE TABLE [icwsys].[Prescription](
	[RequestID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductRouteID] [int] NOT NULL,
	[ScheduleID_Administration] [int] NOT NULL,
	[PRN] [bit] NOT NULL CONSTRAINT [DF_Prescription_PRN]  DEFAULT (0),
	[UnitID_Duration] [int] NULL,
	[Duration] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[StopDate] [datetime] NULL,
	[ArbTextID_Direction] [int] NULL,
	[AdministrationStatusID] [int] NULL
) ON [PRIMARY]

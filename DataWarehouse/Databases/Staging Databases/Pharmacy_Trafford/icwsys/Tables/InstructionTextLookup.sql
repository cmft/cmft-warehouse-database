﻿CREATE TABLE [icwsys].[InstructionTextLookup](
	[ProductFormID] [int] NOT NULL,
	[ProductRouteID] [int] NOT NULL,
	[AdministrationInstructionID] [int] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[MessageReply](
	[MessageReplyID] [int] IDENTITY(1,1) NOT NULL,
	[SourceMsgGuid] [uniqueidentifier] NOT NULL,
	[ReplyMsgGuid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PBSEPWaterUpdate](
	[PBSEPWaterUpdateID] [int] IDENTITY(1,1) NOT NULL,
	[PBSMasterEPWaterID] [int] NULL,
	[PBSCode] [varchar](6) NULL,
	[ManufacturersCode] [varchar](2) NULL,
	[AdditionalCosts] [varchar](10) NULL,
	[DateofChange] [datetime] NULL,
	[Remove] [bit] NULL,
	[DateProcessed] [datetime] NULL
) ON [PRIMARY]
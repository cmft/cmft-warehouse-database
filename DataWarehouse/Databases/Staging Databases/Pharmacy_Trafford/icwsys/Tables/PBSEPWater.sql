﻿CREATE TABLE [icwsys].[PBSEPWater](
	[PBSEPWaterID] [int] IDENTITY(1,1) NOT NULL,
	[PBSMasterEPWaterID] [int] NULL,
	[PBSCode] [varchar](6) NULL,
	[ManufacturersCode] [varchar](2) NULL,
	[AdditionalCosts] [varchar](10) NULL,
	[DateLastUpdated] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ControlSpan](
	[ControlSpanID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[Top] [int] NOT NULL,
	[Left] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[Width] [int] NOT NULL,
	[Visible] [bit] NOT NULL,
	[SizePercentage] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
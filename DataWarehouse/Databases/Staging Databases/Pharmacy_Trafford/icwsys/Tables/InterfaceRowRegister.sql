﻿CREATE TABLE [icwsys].[InterfaceRowRegister](
	[InterfaceRowRegisterID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[CompletionStatusID] [int] NOT NULL,
	[Table] [varchar](128) NOT NULL,
	[ID] [int] NOT NULL,
	[ID2] [int] NOT NULL DEFAULT (0),
	[Revision] [int] NOT NULL DEFAULT (0),
	[Changed] [datetime] NOT NULL DEFAULT (getdate()),
	[DBVersion] [timestamp] NULL
) ON [PRIMARY]
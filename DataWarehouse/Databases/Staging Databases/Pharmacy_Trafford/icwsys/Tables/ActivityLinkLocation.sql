﻿CREATE TABLE [icwsys].[ActivityLinkLocation](
	[ActivityID] [numeric](9, 0) NOT NULL,
	[LocationID] [int] NOT NULL
) ON [PRIMARY]
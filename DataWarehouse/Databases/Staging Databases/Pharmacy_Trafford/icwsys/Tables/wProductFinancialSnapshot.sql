﻿CREATE TABLE [icwsys].[wProductFinancialSnapshot](
	[wProductFinancialSnapshot] [int] IDENTITY(1,1) NOT NULL,
	[ProductStockID] [int] NULL,
	[Period] [varchar](2) NOT NULL,
	[Cost] [float] NOT NULL DEFAULT (0),
	[StockLevel] [float] NOT NULL DEFAULT (0),
	[LossesGains] [float] NOT NULL DEFAULT (0),
	[VatRate] [varchar](1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ReprintBatch](
	[ReprintBatchID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[EntityID] [int] NOT NULL,
	[LocationID] [int] NOT NULL
) ON [PRIMARY]
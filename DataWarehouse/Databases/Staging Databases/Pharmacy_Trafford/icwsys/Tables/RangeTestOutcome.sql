﻿CREATE TABLE [icwsys].[RangeTestOutcome](
	[RangeTestOutcomeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[PRVStyle] [varchar](50) NULL,
	[RangeTestOutcomeSeverityID] [int] NOT NULL
) ON [PRIMARY]
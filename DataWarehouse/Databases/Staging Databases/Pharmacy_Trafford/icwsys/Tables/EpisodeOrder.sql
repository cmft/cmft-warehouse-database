﻿CREATE TABLE [icwsys].[EpisodeOrder](
	[RequestID] [int] NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[EntityID_Owner] [int] NOT NULL,
	[OrderTemplateID] [int] NOT NULL DEFAULT (0)
) ON [PRIMARY]

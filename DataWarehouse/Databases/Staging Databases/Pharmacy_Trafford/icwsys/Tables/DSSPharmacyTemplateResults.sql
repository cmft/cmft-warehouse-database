﻿CREATE TABLE [icwsys].[DSSPharmacyTemplateResults](
	[ResultsID] [int] IDENTITY(1,1) NOT NULL,
	[DrugID] [int] NULL,
	[ProductID] [int] NULL,
	[siscode] [varchar](7) NULL,
	[LabelDescription] [varchar](56) NULL,
	[ProductType] [varchar](50) NULL,
	[ProductRouteID] [int] NULL,
	[ProductRouteDescription] [varchar](50) NULL,
	[ProductDescription] [varchar](128) NULL,
	[TopLevelProductID] [int] NULL,
	[TopLevelProductDescription] [varchar](128) NULL,
	[TemplateFound] [bit] NULL,
	[Comment] [varchar](512) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Episode](
	[EpisodeID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID_Parent] [int] NOT NULL CONSTRAINT [DF_Episode_EpisodeID_Parent]  DEFAULT (0),
	[StatusID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[EpisodeTypeID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Episode_DateCreated]  DEFAULT (getdate()),
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[CaseNo] [varchar](25) NULL CONSTRAINT [DF_Episode_CaseNo]  DEFAULT ('None')
) ON [PRIMARY]

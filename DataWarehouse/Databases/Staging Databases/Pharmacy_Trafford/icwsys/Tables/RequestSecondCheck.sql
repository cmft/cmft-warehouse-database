﻿CREATE TABLE [icwsys].[RequestSecondCheck](
	[RequestSecondCheckID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[EntityID] [int] NOT NULL
) ON [PRIMARY]
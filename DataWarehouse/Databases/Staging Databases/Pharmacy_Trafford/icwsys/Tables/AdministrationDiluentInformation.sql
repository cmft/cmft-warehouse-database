﻿CREATE TABLE [icwsys].[AdministrationDiluentInformation](
	[AdministrationDiluentInformationID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseID] [int] NULL,
	[DiluentProductID] [int] NULL,
	[DiluentQuantity] [float] NULL,
	[FinalVolume] [float] NULL,
	[InfusionDeviceID] [int] NULL,
	[Instructions] [text] NULL,
	[Nominal] [bit] NOT NULL,
	[Exact] [bit] NOT NULL,
	[FinalConcentration] [float] NULL,
	[Dose_Calculated] [bit] NOT NULL,
	[FinalVolume_Calculated] [bit] NOT NULL,
	[FinalConcentration_Calculated] [bit] NOT NULL,
	[DiluentQuantity_Calculated] [bit] NOT NULL,
	[VolumeOfIngredients] [float] NULL,
	[VolumeOfLiquidIngredients] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
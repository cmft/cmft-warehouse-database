﻿CREATE TABLE [icwsys].[LocationAddress](
	[LocationAddressID] [int] IDENTITY(1,1) NOT NULL,
	[AddressID] [int] NOT NULL,
	[AddressTypeID] [int] NOT NULL,
	[LocationID] [int] NOT NULL
) ON [PRIMARY]

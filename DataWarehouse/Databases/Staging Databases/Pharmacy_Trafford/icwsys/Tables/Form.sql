﻿CREATE TABLE [icwsys].[Form](
	[FormID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[DisplayDefault] [bit] NOT NULL,
	[Width] [int] NOT NULL,
	[Height] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WLookup_bk_of_pre_bnf61_V2_Warns_Instrs](
	[WLookupID] [int] NOT NULL,
	[Code] [varchar](10) NULL,
	[SiteID] [int] NOT NULL,
	[WLookupContextID] [int] NOT NULL,
	[Value] [varchar](1024) NOT NULL,
	[InUse] [bit] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
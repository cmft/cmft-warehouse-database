﻿CREATE TABLE [icwsys].[AllergyReaction](
	[NoteID] [int] NOT NULL,
	[NoteID_Allergy] [int] NOT NULL,
	[SeverityID] [int] NOT NULL,
	[OrderCatalogueID] [int] NULL CONSTRAINT [DF_AllergyReaction_OrderCatalogueID_DssTerm]  DEFAULT (0),
	[Detail] [varchar](1024) NOT NULL CONSTRAINT [DF_AllergyReaction_Detail]  DEFAULT ('')
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[Note](
	[NoteID] [int] IDENTITY(1,1) NOT NULL,
	[NoteTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[NoteID_Thread] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Note_CreatedDate]  DEFAULT (getdate())
) ON [PRIMARY]

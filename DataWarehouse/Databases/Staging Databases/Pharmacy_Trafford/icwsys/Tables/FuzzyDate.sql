﻿CREATE TABLE [icwsys].[FuzzyDate](
	[FuzzyDateID] [int] IDENTITY(1,1) NOT NULL,
	[KeyDateID] [int] NOT NULL,
	[Offset] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
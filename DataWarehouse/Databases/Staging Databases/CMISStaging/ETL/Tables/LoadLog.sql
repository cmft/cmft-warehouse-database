﻿CREATE TABLE [ETL].[LoadLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[logLine] [nvarchar](max) NOT NULL,
	[stamp] [datetime2](7) NOT NULL DEFAULT (sysdatetime())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
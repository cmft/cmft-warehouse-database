﻿CREATE TABLE [ETL].[Version](
	[LastUpdated] [datetime2](7) NOT NULL,
	[DateEffective] [datetime2](7) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[GASTRO](
	[I_NUMBER] [nchar](12) NULL,
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[NEC] [nchar](1) NULL,
	[NEC_TREATMENT] [nvarchar](50) NULL,
	[GASTRO_REFLUX] [nvarchar](50) NULL,
	[REFLUX_TREATMENT] [nvarchar](50) NULL,
	[GASTRO_COMMENTS] [nvarchar](500) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
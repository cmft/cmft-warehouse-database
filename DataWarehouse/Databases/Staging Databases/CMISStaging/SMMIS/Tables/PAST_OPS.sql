﻿CREATE TABLE [SMMIS].[PAST_OPS](
	[M_NUMBER] [nchar](12) NULL,
	[OPERATION] [nvarchar](50) NULL,
	[OP_DATE] [datetime2](7) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[APPROXIMATION] [nchar](1) NULL,
	[PLACE] [nvarchar](240) NULL,
	[ID] [nvarchar](384) NOT NULL,
	[LOCATION] [nvarchar](35) NULL,
	[OPERATION_CHAR] [nchar](1) NULL
) ON [PRIMARY]
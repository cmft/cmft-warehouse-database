﻿CREATE TABLE [SMMIS].[ANAESTHETIST](
	[ANA_CODE] [nchar](6) NOT NULL,
	[ANA_GRADE] [nchar](5) NULL,
	[ANA_SURNAME] [nvarchar](35) NULL,
	[ANA_INITIALS] [nchar](5) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[ACTIVE] [nchar](1) NULL
) ON [PRIMARY]
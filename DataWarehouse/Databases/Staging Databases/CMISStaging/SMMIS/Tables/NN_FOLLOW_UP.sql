﻿CREATE TABLE [SMMIS].[NN_FOLLOW_UP](
	[I_NUMBER] [nchar](12) NOT NULL,
	[SPELL] [nchar](12) NULL,
	[EPISODE] [nchar](2) NULL,
	[APPT_DATE] [datetime2](7) NULL,
	[APPT_TYPE] [nchar](1) NOT NULL,
	[OTHER_APPT_TYPE] [nvarchar](35) NULL,
	[APPT_HOSP] [nchar](1) NULL,
	[OTHER_APPT_HOSP] [nvarchar](35) NULL,
	[APPT_CONS] [nchar](1) NULL,
	[OTHER_CONS] [nvarchar](35) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[NUCHAL](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[FETUS_ID] [numeric](2, 0) NOT NULL,
	[NUCHAL_DATE] [datetime2](7) NOT NULL,
	[NUCHAL_SKIN_FOLD] [nchar](1) NULL,
	[NUCHAL_RISK_FACTOR] [numeric](6, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[NUCHAL_GEST_WKS] [numeric](2, 0) NULL,
	[NUCHAL_GEST_DAYS] [numeric](1, 0) NULL,
	[NUCHAL_PLACE] [nchar](1) NULL,
	[NUCHAL_HOSPITAL] [nchar](1) NULL,
	[NUCHAL_PLACE_TXT] [nvarchar](50) NULL
) ON [PRIMARY]
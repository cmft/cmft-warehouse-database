﻿CREATE TABLE [SMMIS].[AN_VISIT_DEFAULTS](
	[USER_ID] [nvarchar](10) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[RISK_BOOKING] [nchar](1) NULL,
	[PARITY] [nchar](1) NULL,
	[VISIT_DATING] [nchar](1) NULL,
	[VISIT_18_WKS_US] [nchar](1) NULL,
	[VISIT_18_WKS_MW] [nchar](1) NULL,
	[VISIT_22_WKS] [nchar](1) NULL,
	[VISIT_24_WKS] [nchar](1) NULL,
	[VISIT_26_WKS] [nchar](1) NULL,
	[VISIT_28_WKS] [nchar](1) NULL,
	[VISIT_30_WKS] [nchar](1) NULL,
	[VISIT_32_WKS] [nchar](1) NULL,
	[VISIT_34_WKS] [nchar](1) NULL,
	[VISIT_36_WKS] [nchar](1) NULL,
	[VISIT_37_WKS] [nchar](1) NULL,
	[VISIT_38_WKS] [nchar](1) NULL,
	[VISIT_39_WKS] [nchar](1) NULL,
	[VISIT_40_WKS] [nchar](1) NULL,
	[VISIT_41_WKS] [nchar](1) NULL
) ON [PRIMARY]
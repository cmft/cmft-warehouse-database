﻿CREATE TABLE [SMMIS].[US_FETAL_BASE](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[FETUS_ID] [numeric](2, 0) NOT NULL,
	[DATE_OF_SCAN] [datetime2](7) NOT NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[PRESENTATION] [nchar](1) NULL,
	[PRESENTATION_COMMENT] [nvarchar](30) NULL,
	[PLACENTAL_SITE] [nchar](1) NULL,
	[PLACENTAL_SITE_COMMENT] [nvarchar](30) NULL,
	[LIQUOR_VOLUME] [nchar](1) NULL,
	[LIQUOR_VOLUME_MEASURE] [nvarchar](384) NULL,
	[BI_PARIETAL_DIAMETER] [nvarchar](384) NULL,
	[HEAD_CIRCUMFERENCE] [nvarchar](384) NULL,
	[ABDOMINAL_CIRCUMFERENCE] [nvarchar](384) NULL,
	[FEMUR_LENGTH] [nvarchar](384) NULL,
	[COMMENTS] [nvarchar](300) NULL,
	[CROWN_RUMP_LENGTH_CM] [numeric](3, 1) NULL,
	[ABNORMALITY_AT_DATING_US] [nchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[NN4B_REQUESTS](
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[INFANT_NUMBER] [nchar](12) NULL,
	[NHS_NUMBER] [nchar](10) NULL,
	[REQUEST_STATUS] [numeric](3, 0) NULL,
	[REQUEST_MESSAGE] [nvarchar](250) NULL,
	[REQUEST_1ST_DATE] [datetime2](7) NULL,
	[REQUEST_LAST_DATE] [datetime2](7) NULL,
	[REQUEST_COUNT] [numeric](8, 0) NULL,
	[PROCESS_DATE] [datetime2](7) NULL,
	[PROCESS_COUNT] [numeric](8, 0) NULL,
	[FORCE] [nvarchar](5) NULL,
	[LOGFILE] [nvarchar](80) NULL
) ON [PRIMARY]
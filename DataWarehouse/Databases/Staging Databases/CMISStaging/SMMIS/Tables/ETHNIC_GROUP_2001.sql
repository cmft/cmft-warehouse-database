﻿CREATE TABLE [SMMIS].[ETHNIC_GROUP_2001](
	[LABEL_LEVEL] [nchar](1) NOT NULL,
	[LABEL_ORDER] [nchar](2) NOT NULL,
	[LABEL_PREFIX] [nchar](1) NOT NULL,
	[LABEL_BODY] [nchar](3) NOT NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
	[OPCS_CODE] [nchar](2) NULL,
	[PAS_CODE] [nchar](2) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[MANC_PAS_CODE] [nvarchar](2) NULL,
	[PAS_CODE_IN] [nvarchar](2) NULL,
	[GROW_UK2012_CODE] [numeric](2, 0) NULL
) ON [PRIMARY]
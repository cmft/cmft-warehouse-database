﻿CREATE TABLE [SMMIS].[CVS](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[FETUS_ID] [numeric](2, 0) NOT NULL,
	[CVS_DATE] [datetime2](7) NOT NULL,
	[CHORIONIC_VILLUS] [nchar](1) NULL,
	[CVS_RESULT] [nchar](1) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[CVS_GEST_WKS] [numeric](2, 0) NULL,
	[CVS_GEST_DAYS] [numeric](1, 0) NULL,
	[COMMENTS] [nvarchar](90) NULL
) ON [PRIMARY]
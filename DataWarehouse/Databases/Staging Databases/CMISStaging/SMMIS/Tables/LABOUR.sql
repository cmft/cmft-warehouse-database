﻿CREATE TABLE [SMMIS].[LABOUR](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[BN_SCREEN1_IND] [nchar](1) NULL,
	[NUMBER_INFANTS] [nchar](1) NULL,
	[ONE_PARENT] [nchar](1) NULL,
	[LABOUR_ONSET] [nchar](1) NULL,
	[AUGMENTATION] [nchar](1) NULL,
	[ONSET_FIRST_DATE] [datetime2](7) NULL,
	[ONSET_FIRST_TIME] [nchar](4) NULL,
	[ONSET_SECOND_DATE] [datetime2](7) NULL,
	[ONSET_SECOND_TIME] [nchar](4) NULL,
	[METHOD_IND_AUG] [nvarchar](30) NULL,
	[BN_SCREEN2_IND] [nchar](1) NULL,
	[PYREXIA] [nchar](1) NULL,
	[BLOOD_LOSS] [nvarchar](5) NULL,
	[PERINEUM] [nchar](2) NULL,
	[NON_PERINEAL_TEARS] [nchar](1) NULL,
	[SUTURE] [nchar](1) NULL,
	[MANUAL_REMOVAL] [nchar](1) NULL,
	[PLACENTA_MEMBRANES] [nchar](1) NULL,
	[MEMBRANES] [nchar](1) NULL,
	[LAB_COMPL] [nchar](1) NULL,
	[LAB_COMPL_TEXT] [nvarchar](500) NULL,
	[INTENDED_FEEDING] [nchar](1) NULL,
	[LAB_RESEARCH] [nchar](6) NULL,
	[SUPPORT] [nchar](1) NULL,
	[D_SCREEN1_IND] [nchar](1) NULL,
	[ECLAMPSIA] [nchar](1) NULL,
	[THROMBOEMBOLISM] [nchar](1) NULL,
	[ERPC] [nchar](1) NULL,
	[PUERPERAL_PSYCHOSIS] [nchar](1) NULL,
	[INFECTION] [nchar](3) NULL,
	[DISCH_HB_TEST] [nchar](1) NULL,
	[DISCH_HAEMOGLOBIN] [nchar](4) NULL,
	[ANTI_D] [nchar](1) NULL,
	[RUBELLA_VAC] [nchar](1) NULL,
	[BLOOD_TRANS] [nvarchar](3) NULL,
	[STERILIZATION] [nchar](1) NULL,
	[CONTRACEPTION] [nvarchar](30) NULL,
	[D_MAT_COMPL] [nvarchar](30) NULL,
	[D_MAT_COMPL_TEXT] [nvarchar](100) NULL,
	[D_SCREEN2_IND] [nchar](1) NULL,
	[APPOINTMENT] [nchar](1) NULL,
	[RESEARCH_DISCH] [nchar](6) NULL,
	[AGE_AT_DEL] [nchar](2) NULL,
	[DURATION_1ST_STAGE] [nchar](4) NULL,
	[DURATION_2ND_STAGE] [nchar](4) NULL,
	[DURATION_LABOUR] [nchar](4) NULL,
	[PN_LEN_STAY] [nchar](2) NULL,
	[AN_COMPLICATIONS_ICD] [nchar](1) NULL,
	[DEL_COMPLICATIONS_ICD] [nchar](1) NULL,
	[PUERP_COMPLICATIONS_ICD] [nchar](1) NULL,
	[MAT_OPERATIONS_ICD] [nchar](1) NULL,
	[TEN_SMOKING_STAGE] [nchar](1) NULL,
	[TEN_CIGS_PER_DAY] [numeric](2, 0) NULL,
	[TEN_PARTNER_SMOKE] [nchar](1) NULL,
	[DISCH_SMOKING_STAGE] [nchar](1) NULL,
	[DISCH_CIGS_PER_DAY] [numeric](2, 0) NULL,
	[DISCH_PARTNER_SMOKE] [nchar](1) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[RISK_DELIVERY] [nchar](1) NULL,
	[DISCH_MEDICATION] [nchar](1) NULL,
	[DISCH_MEDICATION_TEXT] [nvarchar](100) NULL,
	[DISCH_PARTNER_CIGS_PER_DAY] [numeric](2, 0) NULL,
	[TEN_PARTNER_CIGS_PER_DAY] [numeric](2, 0) NULL,
	[SKIN_TO_SKIN] [nchar](1) NULL,
	[SKIN_TO_SKIN_TEXT] [nvarchar](50) NULL,
	[ANAL_MUCOSA] [nchar](1) NULL,
	[TEMP_MAX_LABOUR] [numeric](3, 1) NULL,
	[EPISIOTOMY_SUTURED] [nchar](1) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[BREAST_FEED_INITIATED] [nchar](1) NULL,
	[DATE_1ST_BREAST_FEED] [datetime2](7) NULL,
	[TIME_1ST_BREAST_FEED] [nchar](4) NULL,
	[PN_LEN_STAY_HOURS] [numeric](2, 0) NULL,
	[PN_LEN_STAY_MINUTES] [numeric](2, 0) NULL,
	[TRANS_BP_SYSTOLIC] [numeric](3, 0) NULL,
	[TRANS_BP_DIASTOLIC] [numeric](3, 0) NULL,
	[TRANS_TO_COMMUNITY_DATE] [datetime2](7) NULL,
	[TRANSFERRING_HOSP_NUMBER] [nchar](12) NULL,
	[CMW_TEAM] [nvarchar](20) NULL,
	[CMW_DEFAULT_MIDWIFE] [nvarchar](8) NULL,
	[CMW_BACKUP_MIDWIFE] [nvarchar](8) NULL,
	[CMW_SOURCE] [nchar](1) NULL,
	[CMW_HOSPITAL_NAME] [nvarchar](35) NULL,
	[CMW_HOSPITAL_CHAR] [nchar](1) NULL,
	[CMW_HOSPITAL_ORG_CODE] [nvarchar](8) NULL,
	[TRANS_COMMENTS] [nvarchar](1000) NULL,
	[CMW_M_DISCH_DATE] [datetime2](7) NULL,
	[CMW_M_DISCH_CONDITION] [nvarchar](1000) NULL,
	[CP_NORMAL_LABOUR] [nchar](1) NULL,
	[CP_REASON_NOT_USED] [nvarchar](30) NULL,
	[CP_OTHER_REASON_NOT_USED] [nvarchar](100) NULL,
	[PN_CARE] [nchar](1) NULL,
	[PN_CARE_HOSP] [nchar](1) NULL,
	[PN_CARE_OTHER] [nvarchar](35) NULL,
	[NON_PERINEAL_SUTURE] [nchar](1) NULL,
	[TIME_SUTURE_STARTED] [nchar](1) NULL,
	[REASON_SUTURE_DELAYED] [nvarchar](500) NULL,
	[STERILISATION_TEXT] [nvarchar](100) NULL,
	[ONE_TO_ONE_CARE] [nchar](1) NULL,
	[FGM_REVERSAL] [nchar](1) NULL,
	[FGM_REVERSAL_STAGE] [nchar](1) NULL,
	[FGM_REFERRAL_TO_CP_MW] [nchar](1) NULL,
	[DEL_GBS_CARRIER] [nchar](1) NULL,
	[DISCHARGED_TO_HV_DATE] [datetime2](7) NULL,
	[DISCHARGED_TO_HV_TIME] [nchar](4) NULL,
	[PLANNED_DISCHARGE_DATE] [datetime2](7) NULL,
	[PLANNED_DISCHARGE_TIME] [nchar](4) NULL,
	[PN_COMPLICATIONS] [nvarchar](30) NULL,
	[PN_COMPLICATIONS_TEXT] [nvarchar](80) NULL,
	[PN_FEEDING_ADVICE] [nvarchar](80) NULL,
	[HANDOVER] [nchar](1) NULL,
	[HANDED_OVER_BY] [nvarchar](200) NULL,
	[PN_DISCH_COMPL_CONFIDENTIAL] [nchar](1) NULL,
	[ONE_TO_ONE_REASON] [nvarchar](200) NULL
) ON [PRIMARY]
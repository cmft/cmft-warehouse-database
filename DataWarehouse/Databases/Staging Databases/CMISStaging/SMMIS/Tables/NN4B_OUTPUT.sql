﻿CREATE TABLE [SMMIS].[NN4B_OUTPUT](
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[INFANT_NUMBER] [nvarchar](12) NULL,
	[NHS_NUMBER] [nchar](10) NULL,
	[REQUEST_STATUS] [numeric](3, 0) NULL,
	[REQUEST_MESSAGE] [nvarchar](70) NULL,
	[CMIS_MESSAGE] [nvarchar](1000) NULL,
	[CIS_MESSAGE] [nvarchar](1000) NULL,
	[MESSAGE_ID] [numeric](8, 0) NULL
) ON [PRIMARY]
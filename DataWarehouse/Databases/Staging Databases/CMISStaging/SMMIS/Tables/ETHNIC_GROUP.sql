﻿CREATE TABLE [SMMIS].[ETHNIC_GROUP](
	[LABEL_LEVEL] [nchar](1) NOT NULL,
	[LABEL_ORDER] [nchar](2) NOT NULL,
	[LABEL_PREFIX] [nchar](1) NOT NULL,
	[LABEL_BODY] [nchar](3) NOT NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
	[OPCS_CODE] [nchar](2) NULL,
	[PAS_CODE] [nchar](2) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[POSTCODE](
	[POSTCODE] [nvarchar](8) NOT NULL,
	[DISTRES] [nchar](3) NULL,
	[PCG_RESIDENCE] [nchar](5) NULL
) ON [PRIMARY]
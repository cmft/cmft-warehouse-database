﻿CREATE TABLE [SMMIS].[NUMBER_INDEX](
	[HOSP_NUMBER] [nchar](12) NOT NULL,
	[MOTHER_INFANT] [nchar](1) NULL,
	[NHS_NUMBER] [nchar](10) NULL,
	[NHS_NUMBER_DATE] [datetime2](7) NULL,
	[NHS_NUMBER_STATUS] [numeric](2, 0) NULL,
	[F_SYS_NUMBER] [nchar](20) NULL,
	[CSN_1] [nchar](20) NULL,
	[CSN_2] [nchar](20) NULL,
	[CSN_3] [nchar](20) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[NN4B_STATUS] [numeric](38, 0) NULL,
	[DUP_NHS_NUMBER] [nchar](10) NULL,
	[RESTRICTED_ACCESS] [nchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[NN_LIST](
	[I_NUMBER] [nchar](12) NULL,
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[LOCATION] [nchar](1) NULL,
	[CURRENT_LOC] [nchar](1) NULL,
	[CURRENT_VENTILATION] [nchar](1) NULL,
	[CURRENT_TPN] [nchar](1) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
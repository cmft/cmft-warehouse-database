﻿CREATE TABLE [SMMIS].[OPERATION](
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[CODE_ORDER] [numeric](2, 0) NOT NULL,
	[OP_CHAR] [nchar](1) NULL,
	[OP_CODE] [nchar](6) NULL,
	[OP_DATE] [datetime2](7) NULL,
	[OP_HOSP] [nchar](1) NULL,
	[OP_HOSP_OTHER] [nvarchar](35) NULL,
	[OP_CONS] [nvarchar](35) NULL,
	[OP_COMMENTS] [nvarchar](200) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
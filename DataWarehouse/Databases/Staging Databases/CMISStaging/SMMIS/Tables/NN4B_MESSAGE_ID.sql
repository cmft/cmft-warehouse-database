﻿CREATE TABLE [SMMIS].[NN4B_MESSAGE_ID](
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[NEXT_ID] [numeric](8, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[ANALGESIA](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[LABOUR_ANALG] [nvarchar](30) NULL,
	[OTHER_LAB_ANALG] [nvarchar](50) NULL,
	[DELIVERY_ANALG] [nvarchar](30) NULL,
	[OTHER_DEL_ANALG] [nvarchar](50) NULL,
	[POST_DEL_ANALG] [nvarchar](30) NULL,
	[OTHER_POST_DEL_ANALG] [nvarchar](50) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[AS_ASSISTANCE] [nchar](1) NULL
) ON [PRIMARY]
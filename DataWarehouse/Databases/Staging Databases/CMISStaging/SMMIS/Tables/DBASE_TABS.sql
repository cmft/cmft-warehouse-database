﻿CREATE TABLE [SMMIS].[DBASE_TABS](
	[TABLE_NAME] [nvarchar](35) NOT NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[QQ_KEY_01] [nvarchar](35) NULL,
	[QQ_KEY_02] [nvarchar](35) NULL,
	[QQ_KEY_03] [nvarchar](35) NULL,
	[QQ_KEY_04] [nvarchar](35) NULL,
	[QQ_KEY_05] [nvarchar](35) NULL,
	[QQ_KEY_06] [nvarchar](35) NULL,
	[QQ_KEY_07] [nvarchar](35) NULL,
	[QQ_KEY_08] [nvarchar](35) NULL
) ON [PRIMARY]
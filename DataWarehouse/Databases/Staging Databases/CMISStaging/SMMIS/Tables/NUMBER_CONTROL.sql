﻿CREATE TABLE [SMMIS].[NUMBER_CONTROL](
	[ID_CHAR] [nchar](1) NOT NULL,
	[TEMP_H_NUMBER] [nchar](12) NULL,
	[CURR_MIN_H_NUMBER] [nchar](12) NULL,
	[CURR_MAX_H_NUMBER] [nchar](12) NULL,
	[NEXT_MIN_H_NUMBER] [nchar](12) NULL,
	[NEXT_MAX_H_NUMBER] [nchar](12) NULL,
	[CURR_MIN_SPELL] [numeric](7, 0) NULL,
	[CURR_MAX_SPELL] [numeric](7, 0) NULL,
	[NEXT_MIN_SPELL] [numeric](7, 0) NULL,
	[NEXT_MAX_SPELL] [numeric](7, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[SITE_ID] [nchar](2) NULL,
	[AUTO_ALLOCATE] [nchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[DOCUMENT_LIST](
	[USER_ID] [nvarchar](10) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[DOCUMENT_REF] [nvarchar](8) NOT NULL,
	[DOCUMENT_NAME] [nvarchar](30) NULL,
	[DOC_DESCRIPTION] [nvarchar](50) NULL,
	[SCREEN_REF] [nvarchar](3) NULL,
	[SELECTED] [nvarchar](1) NULL,
	[DOCUMENT_ORDER] [nvarchar](4) NULL,
	[NO_COPIES_INDIV] [nvarchar](4) NULL,
	[NO_COPIES_BATCH] [nvarchar](4) NULL,
	[HARDCOPY] [nchar](1) NULL,
	[ALLOW_ELECTRONIC] [nchar](1) NULL,
	[ELECTRONIC_COPY_LIFE_DAYS] [numeric](4, 0) NULL,
	[EMAIL_DESTINATION] [nvarchar](30) NULL
) ON [PRIMARY]
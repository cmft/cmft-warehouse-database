﻿CREATE TABLE [SMMIS].[FORCED_LOGOUTS](
	[USER_NAME] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[TYPE] [nvarchar](20) NULL
) ON [PRIMARY]
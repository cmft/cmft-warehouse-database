﻿CREATE TABLE [SMMIS].[NN_DRUG](
	[I_NUMBER] [nchar](12) NOT NULL,
	[SPELL] [nchar](12) NULL,
	[EPISODE] [nchar](2) NULL,
	[DRUG] [nvarchar](50) NOT NULL,
	[DRUG_OTHER] [nvarchar](50) NULL,
	[DRUG_DOSE] [nvarchar](4) NULL,
	[DRUG_UNIT] [nvarchar](2) NULL,
	[DRUG_FREQUENCY] [nvarchar](2) NULL,
	[OTHER_CONS] [nvarchar](35) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
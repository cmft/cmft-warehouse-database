﻿CREATE TABLE [SMMIS].[ITEM_TYPE](
	[USER_ID] [nvarchar](10) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[TYPE_ID] [nchar](1) NULL,
	[DESCRIPTION] [nvarchar](30) NULL
) ON [PRIMARY]
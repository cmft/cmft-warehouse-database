﻿CREATE TABLE [SMMIS].[PRINTERS](
	[ID_NO] [numeric](2, 0) NOT NULL,
	[LOCATION] [nvarchar](35) NULL,
	[TYPE] [nchar](1) NULL,
	[WARD] [nchar](1) NULL,
	[FIRST_VC] [numeric](2, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[UNIX_ID] [nvarchar](10) NULL
) ON [PRIMARY]
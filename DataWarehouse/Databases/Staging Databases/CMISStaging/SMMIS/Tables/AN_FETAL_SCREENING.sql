﻿CREATE TABLE [SMMIS].[AN_FETAL_SCREENING](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[FETUS_ID] [numeric](2, 0) NOT NULL,
	[OVERALL_RISK] [numeric](6, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
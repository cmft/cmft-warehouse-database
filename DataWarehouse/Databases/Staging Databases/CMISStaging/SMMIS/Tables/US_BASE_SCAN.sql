﻿CREATE TABLE [SMMIS].[US_BASE_SCAN](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[DATE_OF_SCAN] [datetime2](7) NOT NULL,
	[SCAN_TYPE] [nchar](1) NOT NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[NUMBER_OF_FETUSES] [numeric](2, 0) NULL,
	[DATE_EDD_ULTRA] [datetime2](7) NULL,
	[GESTATION] [numeric](2, 0) NULL,
	[GESTATION_DAYS] [numeric](1, 0) NULL,
	[CHORIONICITY] [nchar](1) NULL
) ON [PRIMARY]
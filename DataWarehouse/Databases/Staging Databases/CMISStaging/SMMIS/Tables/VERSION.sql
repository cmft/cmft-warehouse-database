﻿CREATE TABLE [SMMIS].[VERSION](
	[VERSION_NUMBER] [nvarchar](12) NOT NULL,
	[SCRIPT_NUMBER] [nvarchar](12) NOT NULL,
	[CHANGES] [nvarchar](100) NULL,
	[RELEASE_DATE] [datetime2](7) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
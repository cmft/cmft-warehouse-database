﻿CREATE TABLE [SMMIS].[DOCUMENT_USERS](
	[DD_KEY] [nvarchar](384) NOT NULL,
	[USER_NAME] [nvarchar](10) NOT NULL,
	[ADDED_BY] [nvarchar](10) NULL,
	[ADDED_ON] [datetime2](7) NULL
) ON [PRIMARY]
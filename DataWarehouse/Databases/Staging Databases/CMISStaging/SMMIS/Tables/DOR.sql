﻿CREATE TABLE [SMMIS].[DOR](
	[ORG_CODE] [nchar](8) NOT NULL,
	[ORG_NAME] [nvarchar](50) NULL,
	[OPEN_DATE] [datetime2](7) NULL,
	[CLOSE_DATE] [datetime2](7) NULL,
	[STATUS_CODE] [nchar](1) NULL,
	[CATEGORY] [nchar](1) NULL,
	[PRIORITY] [nchar](2) NULL,
	[CONT_TOTAL] [nchar](5) NULL,
	[CONT_REMAIN] [nchar](5) NULL,
	[CONT_REORDER] [nchar](5) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
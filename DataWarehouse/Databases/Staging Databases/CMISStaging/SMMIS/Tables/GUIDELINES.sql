﻿CREATE TABLE [SMMIS].[GUIDELINES](
	[FUNCTION] [nvarchar](20) NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
	[FILENAME] [nvarchar](30) NOT NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
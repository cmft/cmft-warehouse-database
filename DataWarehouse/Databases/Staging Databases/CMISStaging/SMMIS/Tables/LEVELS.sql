﻿CREATE TABLE [SMMIS].[LEVELS](
	[RANK] [numeric](1, 0) NOT NULL,
	[ANTENATAL] [nvarchar](30) NULL,
	[ANAESTHETIC] [nvarchar](30) NULL,
	[NEONATAL] [nvarchar](30) NULL,
	[REPORTING] [nvarchar](30) NULL,
	[FILE_MAIN] [nvarchar](30) NULL,
	[SYS_MAIN] [nvarchar](30) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[REMOTE] [nvarchar](30) NULL,
	[COMMUNITY] [nvarchar](30) NULL
) ON [PRIMARY]
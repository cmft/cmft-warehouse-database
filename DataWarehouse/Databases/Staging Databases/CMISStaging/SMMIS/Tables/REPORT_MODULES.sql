﻿CREATE TABLE [SMMIS].[REPORT_MODULES](
	[ID] [numeric](10, 0) NOT NULL,
	[DESCRIPTION] [nvarchar](35) NULL,
	[ACTIVE] [nchar](1) NULL,
	[ORDERING] [numeric](10, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[CONSULTANT](
	[CON_CODE] [nchar](4) NOT NULL,
	[C_SURNAME] [nvarchar](35) NULL,
	[CON_CHAR] [nchar](1) NOT NULL,
	[C_INITIALS] [nvarchar](4) NULL,
	[TITLE] [nvarchar](10) NULL,
	[GMC_CODE] [nchar](7) NULL,
	[GMC_TYPE] [nchar](1) NULL,
	[SPECIALTY] [nvarchar](5) NULL,
	[OBS_PAED] [nchar](1) NOT NULL,
	[PAS_CODE] [nchar](8) NULL,
	[ACTIVE] [nchar](1) NULL,
	[ACTIVE_START] [datetime2](7) NULL,
	[ACTIVE_END] [datetime2](7) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[SPEC_02] [nvarchar](5) NULL,
	[SPEC_03] [nvarchar](5) NULL,
	[SPEC_04] [nvarchar](5) NULL,
	[SPEC_05] [nvarchar](5) NULL,
	[SPEC_06] [nvarchar](5) NULL,
	[SPEC_07] [nvarchar](5) NULL,
	[SPEC_08] [nvarchar](5) NULL,
	[SPEC_09] [nvarchar](5) NULL,
	[SPEC_10] [nvarchar](5) NULL,
	[SITE_ID_01] [nchar](2) NULL,
	[SITE_ID_02] [nchar](2) NULL,
	[SITE_ID_03] [nchar](2) NULL,
	[SITE_ID_04] [nchar](2) NULL,
	[PAS_VALUE] [nvarchar](10) NULL,
	[MAIN_SPECIALTY] [nvarchar](5) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[AN_CARE](
	[INIT] [nchar](1) NULL,
	[TYPE] [nvarchar](20) NULL,
	[BOOKING_LETTER] [nvarchar](40) NULL,
	[INTENDED_CARE] [numeric](2, 0) NULL,
	[MANAGEMENT] [numeric](2, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[SPECIALTY](
	[SPECIALTY] [nvarchar](5) NOT NULL,
	[LOCAL_SPEC] [nvarchar](8) NULL,
	[DESCRIPTION] [nvarchar](35) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
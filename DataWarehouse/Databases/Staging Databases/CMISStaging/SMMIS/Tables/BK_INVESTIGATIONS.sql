﻿CREATE TABLE [SMMIS].[BK_INVESTIGATIONS](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[TYPE] [nvarchar](384) NOT NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[TEST_DATE] [datetime2](7) NULL,
	[RESULT_VALUE] [nvarchar](384) NULL,
	[RESULT_MENU] [nchar](1) NULL,
	[RESULT_TEXT] [nvarchar](50) NULL,
	[TEST_STATUS] [nchar](1) NULL,
	[ACTION] [nvarchar](80) NULL,
	[ACTION_DATE] [datetime2](7) NULL
) ON [PRIMARY]
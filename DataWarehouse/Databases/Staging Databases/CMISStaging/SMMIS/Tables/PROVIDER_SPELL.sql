﻿CREATE TABLE [SMMIS].[PROVIDER_SPELL](
	[HOSP_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[SPELL] [nchar](12) NOT NULL,
	[MOTHER_INFANT] [nchar](1) NULL,
	[SPELL_STATUS] [nchar](1) NULL,
	[SPELL_START_DATE] [datetime2](7) NULL,
	[SPELL_START_TIME] [nchar](4) NULL,
	[SPELL_END_DATE] [datetime2](7) NULL,
	[SPELL_END_TIME] [nchar](4) NULL,
	[SPELL_LEN_DAYS] [numeric](3, 0) NULL,
	[SPELL_LEN_HOURS] [numeric](2, 0) NULL,
	[SPELL_LEN_MINUTES] [numeric](2, 0) NULL,
	[CODE_OF_BUYER] [nchar](5) NULL,
	[CONTRACT_NUMBER] [nchar](6) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
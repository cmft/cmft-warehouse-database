﻿CREATE TABLE [SMMIS].[INFO_PRINT](
	[M_NUMBER] [nchar](12) NULL,
	[OCCURRENCE] [numeric](2, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[DESCRIPTION] [nvarchar](35) NULL
) ON [PRIMARY]
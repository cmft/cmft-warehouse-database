﻿CREATE TABLE [SMMIS].[COMPILE_LOG](
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[STATUS] [nvarchar](20) NULL,
	[COMPILE_STRING] [nvarchar](255) NULL
) ON [PRIMARY]
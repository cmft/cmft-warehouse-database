﻿CREATE TABLE [SMMIS].[TPN](
	[I_NUMBER] [nchar](12) NOT NULL,
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NULL,
	[TPN_START_DATE] [datetime2](7) NOT NULL,
	[TPN_END_DATE] [datetime2](7) NULL,
	[TPN_START_AGE] [numeric](4, 0) NULL,
	[TPN_END_AGE] [numeric](4, 0) NULL,
	[TPN_DAYS] [numeric](4, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
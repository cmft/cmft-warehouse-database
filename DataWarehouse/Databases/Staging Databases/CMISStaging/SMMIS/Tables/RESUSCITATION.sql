﻿CREATE TABLE [SMMIS].[RESUSCITATION](
	[I_NUMBER] [nchar](12) NULL,
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[ARTIFICIAL_VENT] [nchar](1) NULL,
	[CARDIAC_MASSAGE] [nchar](1) NULL,
	[RESUSC_DRUGS] [nvarchar](30) NULL,
	[RESUSC_DRUGS_OTHER] [nvarchar](50) NULL,
	[MIN_O2] [numeric](3, 0) NULL,
	[MAX_O2] [numeric](3, 0) NULL,
	[MAX_BASE_EXCESS] [nvarchar](384) NULL,
	[CRIB_SCORE] [numeric](2, 0) NULL,
	[RESUS_COMMENTS] [nvarchar](500) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[MAX_FIO2] [nvarchar](384) NULL
) ON [PRIMARY]
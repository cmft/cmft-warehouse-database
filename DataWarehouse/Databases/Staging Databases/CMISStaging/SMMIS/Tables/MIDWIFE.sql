﻿CREATE TABLE [SMMIS].[MIDWIFE](
	[PIN_NUMBER] [nvarchar](8) NOT NULL,
	[SURNAME] [nvarchar](35) NULL,
	[FORENAMES] [nvarchar](35) NULL,
	[TITLE] [nvarchar](5) NULL,
	[DATE_OF_BIRTH] [datetime2](7) NULL,
	[JOB_TITLE] [nvarchar](35) NULL,
	[JOB_GRADE] [nchar](1) NULL,
	[JOB_STATUS] [nchar](1) NULL,
	[DATE_REGISTRATION] [datetime2](7) NULL,
	[DATE_EXPIRY] [datetime2](7) NULL,
	[DATE_REFRESHER] [datetime2](7) NULL,
	[ACTIVE_STATUS] [nchar](1) NULL,
	[TEAM] [nchar](1) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
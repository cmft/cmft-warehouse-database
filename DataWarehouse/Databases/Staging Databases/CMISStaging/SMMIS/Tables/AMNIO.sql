﻿CREATE TABLE [SMMIS].[AMNIO](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[AMNIO_DATE] [datetime2](7) NOT NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[RESULT] [nchar](1) NULL,
	[COMMENTS] [nvarchar](90) NULL,
	[FETUS_ID] [numeric](2, 0) NOT NULL,
	[AMNIO_GEST_WKS] [numeric](2, 0) NULL,
	[AMNIO_GEST_DAYS] [numeric](1, 0) NULL
) ON [PRIMARY]
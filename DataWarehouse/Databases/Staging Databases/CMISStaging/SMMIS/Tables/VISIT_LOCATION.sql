﻿CREATE TABLE [SMMIS].[VISIT_LOCATION](
	[INIT] [nchar](1) NOT NULL,
	[SCREEN_DISPLAY] [nvarchar](30) NULL,
	[BOOKING_LETTER] [nvarchar](30) NULL,
	[CONTACT_LINE] [nvarchar](80) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[ACTIVE] [nchar](1) NULL
) ON [PRIMARY]
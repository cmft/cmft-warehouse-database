﻿CREATE TABLE [SMMIS].[AUDIO_OPTHAL](
	[I_NUMBER] [nchar](12) NULL,
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[TRANS_BEFORE] [nchar](1) NULL,
	[ROP] [nchar](1) NULL,
	[CRYOTHERAPY] [nchar](1) NULL,
	[OPTH_COMMENTS] [nvarchar](500) NULL,
	[AUDIOLOGY] [nchar](1) NULL,
	[AUDIO_COMMENTS] [nvarchar](500) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[TREATMENT_DATE] [datetime2](7) NULL,
	[CRYO_TREATMENT_DATE] [datetime2](7) NULL
) ON [PRIMARY]
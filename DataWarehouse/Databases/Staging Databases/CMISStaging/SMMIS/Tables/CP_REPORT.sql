﻿CREATE TABLE [SMMIS].[CP_REPORT](
	[START_DATE] [datetime2](7) NULL,
	[END_DATE] [datetime2](7) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[TRUST_NAME] [nvarchar](35) NULL,
	[HOSP_NAME] [nvarchar](35) NULL,
	[HOSP_CHAR] [nchar](1) NULL,
	[PLACE_BIRTH_CENTRE] [numeric](4, 0) NULL,
	[PLACE_HOME] [numeric](4, 0) NULL,
	[PLACE_HOSPITAL] [numeric](4, 0) NULL,
	[PLACE_OTHER] [numeric](4, 0) NULL,
	[PLACE_TOTAL] [numeric](4, 0) NULL,
	[CP_NOT_USED_37_GEST] [numeric](4, 0) NULL,
	[CP_NOT_USED_42_GEST] [numeric](4, 0) NULL,
	[CP_NOT_USED_APH] [numeric](4, 0) NULL,
	[CP_NOT_USED_LOW_HB] [numeric](4, 0) NULL,
	[CP_NOT_USED_LOW_PLAT] [numeric](4, 0) NULL,
	[CP_NOT_USED_SROM_24] [numeric](4, 0) NULL,
	[CP_NOT_USED_MECONIUM] [numeric](4, 0) NULL,
	[CP_NOT_USED_BMI_35] [numeric](4, 0) NULL,
	[CP_NOT_USED_PET] [numeric](4, 0) NULL,
	[CP_NOT_USED_IUGR] [numeric](4, 0) NULL,
	[CP_NOT_USED_OLIGOHYDRA] [numeric](4, 0) NULL,
	[CP_NOT_USED_POLYHYDRA] [numeric](4, 0) NULL,
	[CP_NOT_USED_ABN_PRES] [numeric](4, 0) NULL,
	[CP_NOT_USED_BREECH] [numeric](4, 0) NULL,
	[CP_NOT_USED_ABN_FH] [numeric](4, 0) NULL,
	[CP_NOT_USED_PYREXIA] [numeric](4, 0) NULL,
	[CP_NOT_USED_EPIDURAL] [numeric](4, 0) NULL,
	[CP_NOT_USED_INDUCTION] [numeric](4, 0) NULL,
	[CP_NOT_USED_PREV_CS] [numeric](4, 0) NULL,
	[CP_NOT_USED_RAISED_BP] [numeric](4, 0) NULL,
	[CP_NOT_USED_ELCS] [numeric](4, 0) NULL,
	[CP_NOT_USED_OTHER] [numeric](4, 0) NULL,
	[CP_NOT_USED_MISSING] [numeric](4, 0) NULL,
	[CP_NOT_USED_TOTAL] [numeric](4, 0) NULL,
	[PATH_USED_PRIMIP] [numeric](4, 0) NULL,
	[PATH_USED_MULTIP] [numeric](4, 0) NULL,
	[PATH_USED_MISSING] [numeric](4, 0) NULL,
	[PATH_USED_TOTAL] [numeric](4, 0) NULL,
	[TEL_CALLS_1] [numeric](4, 0) NULL,
	[TEL_CALLS_2] [numeric](4, 0) NULL,
	[TEL_CALLS_3] [numeric](4, 0) NULL,
	[TEL_CALLS_4] [numeric](4, 0) NULL,
	[TEL_CALLS_MISSING] [numeric](4, 0) NULL,
	[TEL_CALLS_TOTAL] [numeric](4, 0) NULL,
	[NO_TEL_CALLS] [numeric](4, 0) NULL,
	[NUMBER_TEL_CALLS_TOTAL] [numeric](4, 0) NULL,
	[PART2_ASSESSMENTS_1] [numeric](4, 0) NULL,
	[PART2_ASSESSMENTS_2] [numeric](4, 0) NULL,
	[PART2_ASSESSMENTS_3] [numeric](4, 0) NULL,
	[PART2_ASSESSMENTS_4] [numeric](4, 0) NULL,
	[PART2_ASSESSMENTS_MISSING] [numeric](4, 0) NULL,
	[PART2_ASSESSMENTS_TOTAL] [numeric](4, 0) NULL,
	[PART2_LOW_RISK] [numeric](4, 0) NULL,
	[PART2_HIGH_RISK] [numeric](4, 0) NULL,
	[PART2_RISK_MISSING] [numeric](4, 0) NULL,
	[PART2_RISK_TOTAL] [numeric](4, 0) NULL,
	[PART3_START_HIGH_RISK] [numeric](4, 0) NULL,
	[PART3_START_TOTAL] [numeric](4, 0) NULL,
	[VE_4_HOURS_YES] [numeric](4, 0) NULL,
	[VE_4_HOURS_NO] [numeric](4, 0) NULL,
	[VE_4_HOURS_MISSING] [numeric](4, 0) NULL,
	[VE_4_HOURS_TOTAL] [numeric](4, 0) NULL,
	[START_ACTIVE_LAB_YES] [numeric](4, 0) NULL,
	[START_ACTIVE_LAB_NO] [numeric](4, 0) NULL,
	[START_ACTIVE_LAB_MISSING] [numeric](4, 0) NULL,
	[START_ACTIVE_LAB_TOTAL] [numeric](4, 0) NULL,
	[P3_CX_DILATION_LESS_THAN_4] [numeric](4, 0) NULL,
	[P3_CX_DILATION_4] [numeric](4, 0) NULL,
	[P3_CX_DILATION_5] [numeric](4, 0) NULL,
	[P3_CX_DILATION_6] [numeric](4, 0) NULL,
	[P3_CX_DILATION_7] [numeric](4, 0) NULL,
	[P3_CX_DILATION_8] [numeric](4, 0) NULL,
	[P3_CX_DILATION_9] [numeric](4, 0) NULL,
	[P3_CX_DILATION_10] [numeric](4, 0) NULL,
	[P3_CX_DILATION_MISSING] [numeric](4, 0) NULL,
	[P3_CX_DILATION_TOTAL] [numeric](4, 0) NULL,
	[CX_DILATN_1ST_STG_YES] [numeric](4, 0) NULL,
	[CX_DILATN_1ST_STG_NO] [numeric](4, 0) NULL,
	[CX_DILATN_1ST_STG_NA] [numeric](4, 0) NULL,
	[CX_DILATN_1ST_STG_LEFT_PATH] [numeric](4, 0) NULL,
	[CX_DILATN_1ST_STG_MISSING] [numeric](4, 0) NULL,
	[CX_DILATN_1ST_STG_TOTAL] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_ARM] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_MOB] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_ORAL] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_BATH] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_PAIN] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_CONS] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_OTHER] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_MISSING] [numeric](4, 0) NULL,
	[INTERVENT_1ST_STG_TOTAL] [numeric](4, 0) NULL,
	[NO_INTERVENT_1ST_STG] [numeric](4, 0) NULL,
	[ARM_SATIS_PROG_YES] [numeric](4, 0) NULL,
	[ARM_SATIS_PROG_NO] [numeric](4, 0) NULL,
	[ARM_SATIS_PROG_MISSING] [numeric](4, 0) NULL,
	[ARM_SATIS_PROG_TOTAL] [numeric](4, 0) NULL,
	[VE_SATIS_PROG_YES] [numeric](4, 0) NULL,
	[VE_SATIS_PROG_NO] [numeric](4, 0) NULL,
	[VE_SATIS_PROG_MISSING] [numeric](4, 0) NULL,
	[VE_SATIS_PROG_TOTAL] [numeric](4, 0) NULL,
	[HEAD_DESC_HRLY_YES] [numeric](4, 0) NULL,
	[HEAD_DESC_HRLY_NO] [numeric](4, 0) NULL,
	[HEAD_DESC_HRLY_BABY] [numeric](4, 0) NULL,
	[HEAD_DESC_HRLY_LEFT_PTHWY] [numeric](4, 0) NULL,
	[HEAD_DESC_HRLY_MISSING] [numeric](4, 0) NULL,
	[HEAD_DESC_HRLY_TOTAL] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_YES] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_NO] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_BABY] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LEFT_PTHWY] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_MISSING] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_TOTAL] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_VE] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_ARM] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_POSTN] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_ANALG] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_PUSH] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_CONS] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_BABY] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_OTHER] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_MISSING] [numeric](4, 0) NULL,
	[INTER_BEFORE_1HR_LST_TOTAL] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_YES] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_NO] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_BABY] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LEFT_PTHWY] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_MISSING] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_TOTAL] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_VE] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_ARM] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_POSTN] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_ANALG] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_PUSH] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_CONS] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_BABY] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_OTHER] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_MISSING] [numeric](4, 0) NULL,
	[INTER_AFTER_1HR_LST_TOTAL] [numeric](4, 0) NULL,
	[TOT_INTERVENT_1] [numeric](4, 0) NULL,
	[TOT_INTERVENT_2] [numeric](4, 0) NULL,
	[TOT_INTERVENT_3] [numeric](4, 0) NULL,
	[TOT_INTERVENT_4] [numeric](4, 0) NULL,
	[TOT_INTERVENT_MISSING] [numeric](4, 0) NULL,
	[TOT_INTERVENT_TOTAL] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_2] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_2_4] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_4_6] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_6_8] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_8_10] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_10_12] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_OVER_12] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_MISSING] [numeric](4, 0) NULL,
	[LEN_1ST_STAGE_TOTAL] [numeric](4, 0) NULL,
	[LEN_2ND_STAGE_30] [numeric](4, 0) NULL,
	[LEN_2ND_STAGE_31_60] [numeric](4, 0) NULL,
	[LEN_2ND_STAGE_61_90] [numeric](4, 0) NULL,
	[LEN_2ND_STAGE_91_120] [numeric](4, 0) NULL,
	[LEN_2ND_STAGE_121_150] [numeric](4, 0) NULL,
	[LEN_2ND_STAGE_OVER_150] [numeric](4, 0) NULL,
	[LEN_2ND_STAGE_MISSING] [numeric](4, 0) NULL,
	[LEN_2ND_STAGE_TOTAL] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_5] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_6_10] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_11_15] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_16_20] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_21_30] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_31_40] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_OVER_40] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_MISSING] [numeric](4, 0) NULL,
	[LEN_3RD_STAGE_TOTAL] [numeric](4, 0) NULL,
	[THIRD_STAGE_MGMT_PHYSIO] [numeric](4, 0) NULL,
	[THIRD_STAGE_MGMT_ACTIVE] [numeric](4, 0) NULL,
	[THIRD_STAGE_MGMT_MAN_REMOV] [numeric](4, 0) NULL,
	[THIRD_STAGE_MGMT_MISSING] [numeric](4, 0) NULL,
	[THIRD_STAGE_MGMT_TOTAL] [numeric](4, 0) NULL,
	[MODE_OF_BIRTH_SVD] [numeric](4, 0) NULL,
	[MODE_OF_BIRTH_VENTOUSE] [numeric](4, 0) NULL,
	[MODE_OF_BIRTH_FORCEPS] [numeric](4, 0) NULL,
	[MODE_OF_BIRTH_C_SECTION] [numeric](4, 0) NULL,
	[MODE_OF_BIRTH_OTHER] [numeric](4, 0) NULL,
	[MODE_OF_BIRTH_MISSING] [numeric](4, 0) NULL,
	[MODE_OF_BIRTH_TOTAL] [numeric](4, 0) NULL,
	[APGAR_ONE_2] [numeric](4, 0) NULL,
	[APGAR_ONE_3_4] [numeric](4, 0) NULL,
	[APGAR_ONE_5_6] [numeric](4, 0) NULL,
	[APGAR_ONE_7_8] [numeric](4, 0) NULL,
	[APGAR_ONE_9_10] [numeric](4, 0) NULL,
	[APGAR_ONE_MISSING] [numeric](4, 0) NULL,
	[APGAR_ONE_TOTAL] [numeric](4, 0) NULL,
	[APGAR_FIVE_2] [numeric](4, 0) NULL,
	[APGAR_FIVE_3_4] [numeric](4, 0) NULL,
	[APGAR_FIVE_5_6] [numeric](4, 0) NULL,
	[APGAR_FIVE_7_8] [numeric](4, 0) NULL,
	[APGAR_FIVE_9_10] [numeric](4, 0) NULL,
	[APGAR_FIVE_MISSING] [numeric](4, 0) NULL,
	[APGAR_FIVE_TOTAL] [numeric](4, 0) NULL,
	[LEFT_PTHWY_ABNORM_FHT] [numeric](4, 0) NULL,
	[LEFT_PTHWY_MECONIUM] [numeric](4, 0) NULL,
	[LEFT_PTHWY_EPIDURAL] [numeric](4, 0) NULL,
	[LEFT_PTHWY_FAIL_PROG] [numeric](4, 0) NULL,
	[LEFT_PTHWY_OTHER] [numeric](4, 0) NULL,
	[LEFT_PTHWY_MISSING] [numeric](4, 0) NULL,
	[LEFT_PTHWY_TOTAL] [numeric](4, 0) NULL,
	[LEFT_PATH_PART_1] [numeric](4, 0) NULL,
	[LEFT_PATH_PART_2] [numeric](4, 0) NULL,
	[LEFT_PATH_PART_3] [numeric](4, 0) NULL,
	[LEFT_PATH_MISSING] [numeric](4, 0) NULL,
	[LEFT_PATH_TOTAL] [numeric](4, 0) NULL
) ON [PRIMARY]
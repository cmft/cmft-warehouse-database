﻿CREATE TABLE [SMMIS].[REPORTS_SUBREPORTS](
	[ID] [numeric](10, 0) NOT NULL,
	[REP_ID] [numeric](10, 0) NULL,
	[REPORT_FILENAME] [nvarchar](30) NULL,
	[ORDERING] [numeric](10, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
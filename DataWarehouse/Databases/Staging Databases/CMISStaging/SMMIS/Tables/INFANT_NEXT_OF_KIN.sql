﻿CREATE TABLE [SMMIS].[INFANT_NEXT_OF_KIN](
	[I_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[IDENTIFIER] [numeric](2, 0) NOT NULL,
	[SURNAME] [nvarchar](35) NULL,
	[FORENAMES] [nvarchar](35) NULL,
	[ADDRESS_1] [nvarchar](35) NULL,
	[ADDRESS_2] [nvarchar](35) NULL,
	[ADDRESS_3] [nvarchar](35) NULL,
	[ADDRESS_4] [nvarchar](35) NULL,
	[POST_CODE] [nvarchar](8) NULL,
	[COUNTRY] [nchar](3) NULL,
	[TELEPHONE] [nvarchar](35) NULL,
	[RELATION] [nchar](1) NULL,
	[OTHER_RELATION] [nvarchar](15) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[MOBILE_PHONE] [nvarchar](35) NULL
) ON [PRIMARY]
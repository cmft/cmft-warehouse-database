﻿CREATE TABLE [SMMIS].[INFO_SHEETS](
	[FUNCTION] [nvarchar](20) NULL,
	[DESCRIPTION] [nvarchar](35) NULL,
	[FILENAME] [nvarchar](12) NOT NULL,
	[SELECTED] [nvarchar](1) NULL,
	[NUM_COPIES] [numeric](2, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
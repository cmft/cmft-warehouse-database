﻿CREATE TABLE [SMMIS].[COMPILE_EXCEPTIONS](
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[OBJECT_NAME] [nvarchar](30) NULL,
	[REASON_TO_NOT_COMPILE] [nvarchar](100) NULL
) ON [PRIMARY]
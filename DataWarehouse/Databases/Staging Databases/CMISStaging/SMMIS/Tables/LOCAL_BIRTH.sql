﻿CREATE TABLE [SMMIS].[LOCAL_BIRTH](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[BIRTH_ORDER] [nchar](1) NOT NULL,
	[I_NUMBER] [nchar](12) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
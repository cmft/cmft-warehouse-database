﻿CREATE TABLE [SMMIS].[HOSP_SPECIFIC](
	[ROW_USAGE] [nvarchar](30) NOT NULL,
	[ROW_DATA] [nvarchar](80) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[USER_EDITABLE] [nchar](1) NULL,
	[DESCRIPTION] [nvarchar](200) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[CENTILE](
	[PREGNANCY] [numeric](1, 0) NOT NULL,
	[SEX] [nchar](1) NOT NULL,
	[GESTATION] [numeric](2, 0) NOT NULL,
	[BIRTH_WT_1] [numeric](3, 0) NULL,
	[BIRTH_WT_2] [numeric](3, 0) NULL,
	[BIRTH_WT_3] [numeric](3, 0) NULL,
	[BIRTH_WT_4] [numeric](3, 0) NULL,
	[BIRTH_WT_5] [numeric](3, 0) NULL,
	[BIRTH_WT_6] [numeric](3, 0) NULL,
	[BIRTH_WT_7] [numeric](3, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[CODING_TRANSLATION](
	[ICD10_CODE] [nvarchar](6) NOT NULL,
	[READ_CODE] [nvarchar](8) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
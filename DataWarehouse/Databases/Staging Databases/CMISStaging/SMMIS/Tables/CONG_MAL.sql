﻿CREATE TABLE [SMMIS].[CONG_MAL](
	[I_NUMBER] [nchar](12) NULL,
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[CONG_MAL_STATUS] [nchar](1) NULL,
	[CONG_MAL_TYPE] [nvarchar](30) NULL,
	[OTH_CONG_MAL_TYPE] [nvarchar](50) NULL,
	[CONG_MAL_COMMENTS] [nvarchar](500) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[DOC_ITEM_TYPE](
	[USER_ID] [nvarchar](10) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[TYPE_ID] [nvarchar](1) NULL,
	[DESCRIPTION] [nvarchar](30) NULL
) ON [PRIMARY]
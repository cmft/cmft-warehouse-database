﻿CREATE TABLE [SMMIS].[IFMAPDIAG](
	[USER_ID] [nvarchar](10) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[FIELD_NUMBER] [numeric](10, 0) NULL,
	[START_POSITION] [numeric](10, 0) NULL,
	[DATA_ITEM_REF] [nvarchar](10) NULL,
	[DATA_ITEM_SIZE] [numeric](10, 0) NULL,
	[FSYS_REF] [nvarchar](10) NULL,
	[FSYS_DESCRIPTION] [nvarchar](50) NULL,
	[FSYS_SIZE] [numeric](10, 0) NULL
) ON [PRIMARY]
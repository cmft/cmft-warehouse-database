﻿CREATE TABLE [SMMIS].[NN_HAEMATOLOGY](
	[I_NUMBER] [nchar](12) NULL,
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[TRANSFUSIONS] [numeric](2, 0) NULL,
	[ERYTHROPOIETIN] [nchar](1) NULL,
	[BLOOD_GROUP] [nchar](1) NULL,
	[BLOOD_PRODUCTS] [nvarchar](30) NULL,
	[BLOOD_PRODUCTS_OTHER] [nvarchar](50) NULL,
	[RHESUS] [nchar](1) NULL,
	[EXCH_TRANSFUSION] [nchar](1) NULL,
	[OTHER_EXCH_TRANS] [nvarchar](35) NULL,
	[HAEM_COMMENTS] [nvarchar](500) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
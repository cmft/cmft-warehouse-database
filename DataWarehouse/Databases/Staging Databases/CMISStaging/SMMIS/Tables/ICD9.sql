﻿CREATE TABLE [SMMIS].[ICD9](
	[ICD9_CODE] [nvarchar](6) NOT NULL,
	[DISEASE] [nvarchar](60) NULL,
	[M_AN] [nchar](1) NULL,
	[M_OPS] [nchar](1) NULL,
	[M_DEL] [nchar](1) NULL,
	[M_PUERP] [nchar](1) NULL,
	[AN_AN] [nchar](1) NULL,
	[AN_OPS] [nchar](1) NULL,
	[AN_PUERP] [nchar](1) NULL,
	[I_COMPS] [nchar](1) NULL,
	[I_ABS] [nchar](1) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
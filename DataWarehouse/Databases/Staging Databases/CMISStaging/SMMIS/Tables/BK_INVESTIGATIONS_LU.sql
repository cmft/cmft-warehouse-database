﻿CREATE TABLE [SMMIS].[BK_INVESTIGATIONS_LU](
	[TYPE] [numeric](2, 0) NOT NULL,
	[COLUMN_NAME] [nvarchar](35) NULL,
	[LIST_ITEM] [nchar](1) NULL,
	[DESCRIPTION] [nvarchar](35) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[AN_FETAL_EXAM](
	[VISIT_ID] [nvarchar](384) NOT NULL,
	[FETUS_ID] [numeric](1, 0) NOT NULL,
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[FUNDAL_HEIGHT] [numeric](2, 0) NULL,
	[FUNDAL_HEIGHT_UNIT] [nchar](1) NULL,
	[FUNDAL_HEIGHT_REASON_NOT_DONE] [nvarchar](100) NULL,
	[LIE] [nchar](1) NULL,
	[PRESENTATION] [nchar](1) NULL,
	[ENGAGEMENT] [nchar](1) NULL,
	[FETAL_HEART_HEARD] [nchar](1) NULL,
	[FETAL_MOVEMENT_FELT] [nchar](1) NULL,
	[REFERRED_FOR_SCAN] [nchar](1) NULL,
	[EST_FETAL_WEIGHT] [numeric](4, 0) NULL
) ON [PRIMARY]
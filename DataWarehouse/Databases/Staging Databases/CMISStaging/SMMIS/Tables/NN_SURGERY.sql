﻿CREATE TABLE [SMMIS].[NN_SURGERY](
	[I_NUMBER] [nchar](12) NULL,
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[CODE_ORDER] [numeric](2, 0) NULL,
	[SURG_CHAR] [nchar](1) NULL,
	[SURG_CODE] [nchar](6) NULL,
	[SURG_DATE] [datetime2](7) NOT NULL,
	[SURG_DESCRIPTION] [nvarchar](60) NULL,
	[SURG_HOSP] [nchar](1) NULL,
	[OTHER_SURG_HOSP] [nvarchar](35) NULL,
	[SURG_CONS] [nchar](1) NULL,
	[OTHER_SURG_CONS] [nvarchar](35) NULL,
	[SURG_COMMENTS] [nvarchar](200) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL
) ON [PRIMARY]
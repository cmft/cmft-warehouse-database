﻿CREATE TABLE [SMMIS].[MIDWIFE_TEAM](
	[TEAM_INITIAL] [nchar](1) NOT NULL,
	[TEAM] [nvarchar](35) NULL,
	[ACTIVE_STATUS] [nchar](1) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[ORDER_NUM] [numeric](38, 0) NULL,
	[SITE_ID_01] [nchar](2) NULL,
	[SITE_ID_02] [nchar](2) NULL,
	[SITE_ID_03] [nchar](2) NULL,
	[SITE_ID_04] [nchar](2) NULL,
	[DIGITAL_PEN_NAME] [nvarchar](50) NULL,
	[EMAIL_ID] [numeric](10, 0) NULL
) ON [PRIMARY]
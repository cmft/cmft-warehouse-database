﻿CREATE TABLE [SMMIS].[LIST_OPTIONS_MAP](
	[FORM_REF] [nvarchar](5) NOT NULL,
	[TAB_NUMBER] [numeric](1, 0) NOT NULL,
	[TABLE_NAME] [nvarchar](30) NOT NULL,
	[COLUMN_NAME] [nvarchar](30) NOT NULL,
	[KEY_CHAR] [nvarchar](1) NOT NULL,
	[ORDER_NUM] [numeric](3, 0) NULL
) ON [PRIMARY]
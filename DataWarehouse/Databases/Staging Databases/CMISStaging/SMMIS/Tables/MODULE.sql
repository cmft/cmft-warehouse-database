﻿CREATE TABLE [SMMIS].[MODULE](
	[USER_ID] [nvarchar](10) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[MODULES] [nvarchar](30) NULL,
	[SUBMODULES] [nvarchar](30) NULL,
	[SCREEN_REF] [nvarchar](2) NULL
) ON [PRIMARY]
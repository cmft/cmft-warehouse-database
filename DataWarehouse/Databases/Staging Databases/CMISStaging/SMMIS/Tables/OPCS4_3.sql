﻿CREATE TABLE [SMMIS].[OPCS4_3](
	[OPCS4_CODE] [nvarchar](6) NOT NULL,
	[DISEASE] [nvarchar](120) NULL,
	[M_OPS] [nchar](1) NULL,
	[AN_OPS] [nchar](1) NULL,
	[I_OPS] [nchar](1) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[COUNTRY](
	[COUNTRY_CODE] [nchar](3) NOT NULL,
	[COUNTRY_NAME] [nvarchar](50) NULL,
	[ACTIVE] [nchar](1) NULL,
	[TB_INCIDENCES_PER_100K] [numeric](6, 0) NULL,
	[TB_INCIDENCES_DATA_SOURCE] [nvarchar](50) NULL,
	[TB_INCIDENCES_DATA_SOURCE_YEAR] [numeric](4, 0) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SMMIS].[INCOMPLETE_RECS](
	[M_NUMBER] [nchar](12) NOT NULL,
	[OCCURRENCE] [numeric](2, 0) NOT NULL,
	[BIRTH_ORDER] [nchar](3) NOT NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL,
	[REASON] [nvarchar](150) NULL,
	[M_SURNAME] [nvarchar](35) NULL,
	[FORENAMES] [nvarchar](35) NULL,
	[C_SURNAME] [nvarchar](35) NULL,
	[C_INITIALS] [nvarchar](4) NULL,
	[TITLE] [nvarchar](5) NULL,
	[DATE_OF_BIRTH] [datetime2](7) NULL,
	[I_NUMBER] [nchar](12) NULL,
	[CONSULTANT] [nvarchar](50) NULL
) ON [PRIMARY]
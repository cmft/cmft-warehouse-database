﻿CREATE TABLE [SMMIS].[DIAGNOSIS](
	[SPELL] [nchar](12) NOT NULL,
	[EPISODE] [nchar](2) NOT NULL,
	[CODE_TYPE] [nchar](1) NOT NULL,
	[CODE_ORDER] [numeric](2, 0) NOT NULL,
	[DIAG_CHAR] [nchar](1) NULL,
	[DIAG_CODE] [nchar](6) NULL,
	[DIAG_DESCRIPTION] [nvarchar](513) NULL,
	[DIAG_DATE] [datetime2](7) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[LOCATION] [nvarchar](35) NULL
) ON [PRIMARY]
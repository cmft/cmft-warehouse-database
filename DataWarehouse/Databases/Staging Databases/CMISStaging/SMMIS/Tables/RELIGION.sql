﻿CREATE TABLE [SMMIS].[RELIGION](
	[RELIGION_CODE] [nchar](4) NOT NULL,
	[RELIGION_NAME] [nvarchar](30) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[MANC_PAS_VALUE] [nvarchar](5) NULL,
	[PAS_VALUE] [nvarchar](25) NULL,
	[ACTIVE] [nchar](1) NULL,
	[SNOMED_CODE] [nvarchar](4) NULL
) ON [PRIMARY]
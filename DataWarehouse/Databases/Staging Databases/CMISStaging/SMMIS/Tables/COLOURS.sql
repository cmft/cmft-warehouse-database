﻿CREATE TABLE [SMMIS].[COLOURS](
	[SCHEME_NO] [nvarchar](384) NOT NULL,
	[WELCOME_SCREEN] [nvarchar](20) NULL,
	[LOGIN_SCREEN] [nvarchar](20) NULL,
	[PARENT_HEADER] [nvarchar](20) NULL,
	[PARENT_PANEL] [nvarchar](20) NULL,
	[PARENT_BACK_COLOUR] [nvarchar](20) NULL,
	[GRID_LISTBOX] [nvarchar](20) NULL,
	[DISABLED_CONTROL] [nvarchar](20) NULL,
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[FONT_COLOUR] [nvarchar](20) NULL
) ON [PRIMARY]
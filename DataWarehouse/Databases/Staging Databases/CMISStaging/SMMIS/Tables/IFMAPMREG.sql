﻿CREATE TABLE [SMMIS].[IFMAPMREG](
	[DATE_STAMP] [datetime2](7) NULL,
	[USER_ID] [nvarchar](10) NULL,
	[FIELD_NUMBER] [numeric](10, 0) NULL,
	[START_POSITION] [numeric](10, 0) NULL,
	[DATA_ITEM_REF] [nvarchar](10) NULL,
	[DATA_ITEM_SIZE] [numeric](10, 0) NULL,
	[FSYS_REF] [nvarchar](35) NULL,
	[FSYS_DESCRIPTION] [nvarchar](50) NULL,
	[FSYS_SIZE] [numeric](10, 0) NULL,
	[DATA_ITEM_REF_IN] [nvarchar](10) NULL,
	[IFIN_FIELD_NAME] [nvarchar](30) NULL,
	[FORM_REF] [nvarchar](5) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Labs].[RefRanges](
	[TestCode] [nvarchar](max) NULL,
	[Units] [varchar](50) NULL,
	[Age] [varchar](50) NULL,
	[TimePeriod] [varchar](50) NULL,
	[TimePeriodType] [varchar](1) NULL,
	[Sex_Code] [varchar](50) NULL,
	[S_Alert_Range_High] [varchar](50) NULL,
	[S_Alert_Range_Low] [varchar](50) NULL,
	[S_Ref_Range_High] [varchar](50) NULL,
	[S_Ref_Range_Low] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Theatre].[ChameleonValidationOperation](
	[OperationNumber] [numeric](9, 0) NOT NULL,
	[EpisodeNumber] [varchar](40) NULL,
	[OperationStatus] [varchar](1) NULL,
	[districtnumber] [nvarchar](14) NULL,
	[Casenote] [varchar](8000) NULL,
	[LastName] [varchar](60) NULL,
	[DateOfBirth] [datetime] NULL,
	[AgeYears] [numeric](3, 0) NOT NULL,
	[AgeMonth] [numeric](3, 0) NOT NULL,
	[AgeDays] [numeric](9, 0) NOT NULL,
	[Gender] [varchar](1) NULL,
	[TheatreDescription] [varchar](60) NULL,
	[OperationDescription] [varchar](max) NULL,
	[Operation] [varchar](max) NULL,
	[Procedure] [varchar](max) NULL,
	[Findings] [varchar](max) NULL,
	[PostOpInstruction] [varchar](max) NULL,
	[IncisionDetails] [varchar](max) NULL,
	[Surg1_LastName] [varchar](60) NULL,
	[Surg1_FirstName] [varchar](60) NULL,
	[Surg2_LastName] [varchar](60) NULL,
	[Surg2_FirstName] [varchar](60) NULL,
	[Surg3_LastName] [varchar](60) NULL,
	[Surg3_FirstName] [varchar](60) NULL,
	[StartDateTime] [datetime] NULL,
	[FinishDateTime] [datetime] NULL,
	[OperationDate] [datetime] NULL,
	[OperationType] [varchar](10) NULL,
	[InAnaestheticsDateTime] [datetime] NULL,
	[InSuiteDateTime] [datetime] NULL,
	[InRecDateTime] [datetime] NULL,
	[OP_ANAES_IND_DATE_TIME] [datetime] NULL,
	[OP_READY_DEPART_DATE_TIME] [datetime] NULL,
	[OP_HOSP_DIS_DATE_TIME] [datetime] NULL,
	[OP_ADMIT_DATE_TIME] [datetime] NULL,
	[OP_OR_READY_DATE_TIME] [datetime] NULL,
	[OP_DRESSING_DATE_TIME] [datetime] NULL,
	[OP_SETUP_COMP_DATE_TIME] [datetime] NULL,
	[OP_UPD_DATE_TIME] [datetime] NULL,
	[OP_EXTUBATION_DATE_TIME] [datetime] NULL,
	[OP_SENT_FOR_DATE_TIME] [datetime] NULL,
	[OP_PORTER_LEFT_DATE_TIME] [datetime] NULL,
	[OP_ANAES_READY_DATE_TIME] [datetime] NULL,
	[OP_PREP_READY_DATE_TIME] [datetime] NULL,
	[OP_TEAM_TIMEOUT_DATE_TIME] [datetime] NULL,
	[OP_REQ_TIME] [datetime] NULL,
	[surg1_SpecCode] [nvarchar](255) NULL,
	[surg1_SpecDescription] [nvarchar](100) NULL,
	[surg2_SpecCode] [nvarchar](255) NULL,
	[surg2_SpecDescription] [nvarchar](100) NULL,
	[surg3_SpecCode] [nvarchar](255) NULL,
	[surg3_SpecDescription] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
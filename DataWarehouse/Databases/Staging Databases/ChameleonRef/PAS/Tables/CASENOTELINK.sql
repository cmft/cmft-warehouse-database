﻿CREATE TABLE [PAS].[CASENOTELINK](
	[InternalPatientNumber] [int] NOT NULL,
	[EntityTypeKey] [int] NOT NULL,
	[ExternalNumberKey] [nvarchar](20) NOT NULL
) ON [PRIMARY]
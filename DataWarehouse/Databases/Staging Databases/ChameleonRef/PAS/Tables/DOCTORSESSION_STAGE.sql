﻿CREATE TABLE [PAS].[DOCTORSESSION_STAGE](
	[DOCTORSESSIONID] [varchar](254) NOT NULL,
	[ClinicCode] [varchar](8) NULL,
	[ClinicType] [varchar](3) NULL,
	[ClinicTypeDesc] [varchar](30) NULL,
	[EBSIndicator] [varchar](3) NULL,
	[EBSIndicatorInt] [varchar](1) NULL,
	[EffectiveDate] [varchar](8) NOT NULL,
	[EffectiveDate_1] [varchar](10) NULL,
	[Frequency] [varchar](2) NOT NULL,
	[SchedResMfCd] [varchar](8) NOT NULL,
	[SchedulingResourceSessionDescription] [varchar](3) NULL,
	[SessionStart] [varchar](4) NOT NULL,
	[SessionStartTm] [varchar](7) NULL,
	[SessionStopTm] [varchar](7) NULL,
	[SignificantFacility] [varchar](2) NULL,
	[Stop] [varchar](4) NULL,
	[Template] [varchar](1) NOT NULL
) ON [PRIMARY]
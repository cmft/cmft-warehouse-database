﻿CREATE TABLE [PAS].[DOCTORTIMESLOTSERVICE](
	[DOCTORTIMESLOTSERVICEID] [varchar](254) NOT NULL,
	[AppointTypeDesc] [varchar](20) NULL,
	[ApptType] [varchar](3) NOT NULL,
	[CABServiceCode] [varchar](6) NOT NULL,
	[CABServiceDesc] [varchar](50) NULL,
	[EBSStatus] [varchar](3) NULL,
	[EBSStatusDesc] [varchar](38) NULL,
	[EffectiveDate] [varchar](8) NOT NULL,
	[Frequency] [varchar](2) NOT NULL,
	[SchedResMfCd] [varchar](8) NOT NULL,
	[SessionStart] [varchar](4) NOT NULL,
	[SessionStartExt] [varchar](7) NULL,
	[StartTime] [varchar](4) NOT NULL,
	[StartTimeExt] [varchar](7) NULL,
	[Template] [varchar](1) NOT NULL,
	[UniqueServiceID] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [PAS].[DOCTORTIMESLOT_STAGE](
	[DOCTORTIMESLOTID] [varchar](254) NOT NULL,
	[EBSIndicator] [varchar](3) NULL,
	[EBSIndicatorInt] [varchar](1) NULL,
	[EffectiveDate] [varchar](8) NOT NULL,
	[Frequency] [varchar](2) NOT NULL,
	[PendingDeleteInd] [varchar](1) NULL,
	[ReportToLocation] [varchar](4) NULL,
	[SchedResMfCd] [varchar](8) NOT NULL,
	[SessionStart] [varchar](4) NOT NULL,
	[StartTime] [varchar](4) NOT NULL,
	[StopTime] [varchar](4) NULL,
	[Template] [varchar](1) NOT NULL,
	[TimeslotPatients] [varchar](3) NULL,
	[TimeslotPatientsDefaultedInd] [varchar](1) NULL,
	[TimeslotTime] [varchar](11) NULL
) ON [PRIMARY]
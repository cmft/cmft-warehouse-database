﻿CREATE TABLE [PAS].[DOCTORTEMPLATE_STAGE](
	[DOCTORTEMPLATEID] [varchar](254) NOT NULL,
	[Comment] [varchar](65) NULL,
	[EffectiveDate] [varchar](8) NOT NULL,
	[EffectiveDate_1] [varchar](10) NULL,
	[Frequency] [varchar](2) NOT NULL,
	[Location] [varchar](4) NULL,
	[SchedResMfCd] [varchar](8) NOT NULL,
	[StopDate] [varchar](8) NULL,
	[StopDate_1] [varchar](10) NULL,
	[Template] [varchar](1) NOT NULL
) ON [PRIMARY]
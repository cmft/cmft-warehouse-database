﻿CREATE TABLE [PAS].[Clinics](
	[SourceContextCode] [varchar](8) NOT NULL,
	[SourceClinicCode] [varchar](8) NOT NULL,
	[SourceClinic] [varchar](42) NULL,
	[HospitalCode] [varchar](4) NULL,
	[IsAvailableToGP] [bit] NULL,
	[FunctionCode] [varchar](4) NULL,
	[ConsultantCode] [varchar](6) NULL,
	[LocationCode] [varchar](4) NULL,
	[Specialty] [varchar](4) NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]
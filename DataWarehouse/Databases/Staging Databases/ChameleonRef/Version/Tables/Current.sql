﻿CREATE TABLE [Version].[Current](
	[Major] [int] NOT NULL DEFAULT ((0)),
	[Minor] [int] NOT NULL DEFAULT ((0)),
	[Revision] [int] NOT NULL DEFAULT ((0)),
	[Build] [int] NOT NULL DEFAULT ((0)),
	[Version] [varchar](20), -- AS ((((((CONVERT([varchar](20),[major],(0))+'.')+CONVERT([varchar](20),[minor],(0)))+'.')+CONVERT([varchar](20),[revision],(0)))+'.')+CONVERT([varchar](20),[build],(0))),
	[rolledOutDate] [datetime2](7) NOT NULL,
	[Notes] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[WH_OP_Clinics](
	[SourceContextCode] [varchar](20) NOT NULL,
	[SourceContext] [varchar](100) NOT NULL,
	[SourceClinicID] [int] NOT NULL,
	[SourceClinicCode] [varchar](100) NOT NULL,
	[SourceClinic] [varchar](900) NOT NULL,
	[LocalClinicID] [int] NULL,
	[LocalClinicCode] [varchar](50) NULL,
	[LocalClinic] [varchar](200) NULL,
	[NationalClinicID] [int] NULL,
	[NationalClinicCode] [varchar](50) NULL,
	[NationalClinic] [varchar](200) NULL
) ON [PRIMARY]
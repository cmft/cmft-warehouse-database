﻿CREATE TABLE [dbo].[RefRanges](
	[Alert_Range_Action_Q] [nvarchar](6) NULL,
	[Alert_Range_High] [nvarchar](6) NULL,
	[Alert_Range_Low] [nvarchar](6) NULL,
	[Alert_Range_Q_Priority] [nvarchar](6) NULL,
	[Alpha_Result] [nvarchar](3) NULL,
	[Decimal_Places] [float] NULL,
	[Equipment] [nvarchar](30) NULL,
	[Equipment_Code] [nvarchar](6) NULL,
	[Lab_Limit_Action_Q] [nvarchar](6) NULL,
	[Lab_Limit_High] [nvarchar](6) NULL,
	[Lab_Limit_Low] [nvarchar](6) NULL,
	[Lab_Limit_Q_Priority] [nvarchar](6) NULL,
	[Method] [nvarchar](30) NULL,
	[Method_Code] [nvarchar](6) NULL,
	[Ref_Range_Action_Q] [nvarchar](6) NULL,
	[Ref_Range_High] [nvarchar](6) NULL,
	[Ref_Range_Low] [nvarchar](6) NULL,
	[Ref_Range_Q_Priority] [nvarchar](6) NULL,
	[Test_Codes] [nvarchar](25) NULL,
	[Units] [nvarchar](15) NULL,
	[Age] [nvarchar](6) NULL,
	[Queue_Code] [nvarchar](6) NULL,
	[Race_Code] [nvarchar](6) NULL,
	[Ref_Comment_High] [nvarchar](30) NULL,
	[Ref_Comment_High_Code] [nvarchar](6) NULL,
	[Ref_Comment_Low] [nvarchar](30) NULL,
	[Ref_Comment_Low_Code] [nvarchar](6) NULL,
	[Ref_Range_Code] [nvarchar](6) NULL,
	[Ref_Range_Type] [nvarchar](3) NULL,
	[Ref_Rng_Desc] [nvarchar](30) NULL,
	[S_Alert_Range_High] [nvarchar](6) NULL,
	[S_Alert_Range_Low] [nvarchar](6) NULL,
	[S_Ref_Range_High] [nvarchar](6) NULL,
	[S_Ref_Range_Low] [nvarchar](6) NULL,
	[Sex_Code] [nvarchar](1) NULL,
	[Species_Code] [nvarchar](6) NULL,
	[Chargeband_Code] [nvarchar](6) NULL,
	[Direct_Cost] [float] NULL,
	[Indirect_Cost] [float] NULL,
	[Price] [float] NULL,
	[Priority_Code] [nvarchar](30) NULL,
	[Total_Cost] [float] NULL,
	[Priority] [nvarchar](30) NULL,
	[Chargeband] [nvarchar](30) NULL
) ON [PRIMARY]
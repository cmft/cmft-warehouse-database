﻿CREATE TABLE [DataSet].[Info](
	[id] [int] IDENTITY(100000,1) NOT FOR REPLICATION NOT NULL,
	[DSName] [sysname] NOT NULL,
	[LastUpdated] [datetime2](7) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LKP_CasenoteLocation](
	[CasenoteLocationCode] [nvarchar](255) NOT NULL,
	[IsACodingOffice] [bit] NULL DEFAULT ((0)),
	[ExcludeFromCASearchList] [bit] NULL DEFAULT ((0))
) ON [PRIMARY]
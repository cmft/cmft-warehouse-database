﻿CREATE TABLE [dbo].[LKP_DNF](
	[SourcePatientNo] [int] NULL,
	[SourceUniqueID] [int] NOT NULL,
	[Header1] [nvarchar](max) NULL,
	[Header2] [nvarchar](max) NULL,
	[Header3] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
	[DownloadTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
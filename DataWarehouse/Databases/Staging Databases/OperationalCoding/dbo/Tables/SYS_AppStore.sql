﻿CREATE TABLE [dbo].[SYS_AppStore](
	[AppID] [int] IDENTITY(1,1) NOT NULL,
	[AppName] [nvarchar](255) NULL,
	[AppDescription] [nvarchar](255) NULL,
	[DeployedFlag] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AUD_Sample](
	[SampleID] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NOT NULL,
	[MergeEncounterRecNo] [nvarchar](25) NOT NULL,
	[ConcludedFlag] [int] NULL CONSTRAINT [DF_AUD_Sample2_ConcludedFlag_1]  DEFAULT ((0)),
	[Coder_StaffID] [int] NULL,
	[ClinicianName] [nvarchar](100) NULL,
	[ConcludedTime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LKP_Specialty](
	[SpecialtyCode] [nvarchar](255) NOT NULL,
	[SubCodingTeamID] [int] NOT NULL CONSTRAINT [DF_Table_1_SubCodingTeam]  DEFAULT ((99)),
	[CodingTeamID] [int] NOT NULL CONSTRAINT [DF_LKP_Specialty3_CodingTeamID]  DEFAULT ((99)),
	[DistributionList] [int] NULL,
	[Exclusion] [bit] NULL CONSTRAINT [DF_LKP_Specialty3_Exclusion]  DEFAULT ((0))
) ON [PRIMARY]
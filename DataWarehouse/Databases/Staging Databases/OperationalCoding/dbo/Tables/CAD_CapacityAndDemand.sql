﻿CREATE TABLE [dbo].[CAD_CapacityAndDemand](
	[ActivityDate] [datetime] NOT NULL,
	[CodingTeamID] [int] NOT NULL,
	[Demand] [int] NULL,
	[Capacity] [int] NULL,
	[Backlog] [int] NULL,
	[SpareCapacity] [int] NULL,
	[Census] [datetime] NOT NULL
) ON [PRIMARY]
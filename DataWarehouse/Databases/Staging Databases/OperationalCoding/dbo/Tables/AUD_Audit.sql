﻿CREATE TABLE [dbo].[AUD_Audit](
	[AuditID] [int] IDENTITY(1,1) NOT NULL,
	[Sponsor] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[Title] [nvarchar](50) NULL
) ON [PRIMARY]
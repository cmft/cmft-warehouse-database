﻿CREATE TABLE [dbo].[LKP_Distribution](
	[DistributionID] [int] IDENTITY(1,1) NOT NULL,
	[DistributionList] [int] NULL,
	[StaffID] [int] NULL,
	[Method] [nvarchar](255) NULL DEFAULT ('To')
) ON [PRIMARY]
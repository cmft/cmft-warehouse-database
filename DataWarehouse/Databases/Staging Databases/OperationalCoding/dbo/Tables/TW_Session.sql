﻿CREATE TABLE [dbo].[TW_Session](
	[SessionID] [int] IDENTITY(1,1) NOT NULL,
	[StaffDayID] [int] NULL,
	[Workload] [int] NULL,
	[Minutes] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LKP_StaffGroup](
	[StaffGroupID] [int] IDENTITY(1,1) NOT NULL,
	[StaffGroup] [nvarchar](255) NULL,
	[IsCoder] [bit] NULL
) ON [PRIMARY]
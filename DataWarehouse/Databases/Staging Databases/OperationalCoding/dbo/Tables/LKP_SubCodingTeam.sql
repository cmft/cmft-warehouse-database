﻿CREATE TABLE [dbo].[LKP_SubCodingTeam](
	[SubCodingTeamID] [int] NOT NULL,
	[SubCodingTeamName] [nvarchar](255) NULL,
	[SortOrder] [decimal](6, 2) NULL CONSTRAINT [DF_LKP_SubCodingTeam_SortOrder]  DEFAULT ((0)),
	[CodingTeamID] [int] NULL
) ON [PRIMARY]
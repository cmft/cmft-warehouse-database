﻿CREATE TABLE [dbo].[MOD_Scenario](
	[ScenarioID] [int] IDENTITY(1,1) NOT NULL,
	[StaffID] [int] NULL,
	[Status] [int] NULL DEFAULT ((1)),
	[ModelFilename] [nvarchar](255) NULL,
	[RowAdded] [datetime] NULL DEFAULT (getdate()),
	[ScenarioName] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TW_LeaveRequest](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[StaffID] [int] NULL,
	[LeaveTypeID] [int] NULL,
	[LeaveStartTime] [datetime] NULL,
	[LeaveEndTime] [datetime] NULL,
	[LeaveDuration] [int] NULL,
	[RequestMadeTime] [datetime] NULL CONSTRAINT [DF_TW_LeaveRequest_RequestMadeTime]  DEFAULT (getdate()),
	[AssessedTime] [datetime] NULL,
	[AssessedBy_StaffID] [int] NULL,
	[StatusID] [int] NULL CONSTRAINT [DF_TW_LeaveRequest_StatusID]  DEFAULT ((1))
) ON [PRIMARY]
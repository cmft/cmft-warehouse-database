﻿CREATE TABLE [dbo].[CAD_Trajectory](
	[ActivityDate] [date] NOT NULL,
	[CodingTeamID] [int] NOT NULL,
	[Demand] [int] NULL,
	[Capacity] [int] NULL,
	[Backlog] [int] NULL,
	[SpareCapacity] [int] NULL
) ON [PRIMARY]
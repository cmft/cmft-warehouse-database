﻿CREATE TABLE [dbo].[SYS_ReportFilter](
	[FilterID] [int] IDENTITY(1,1) NOT NULL,
	[FilterName] [nvarchar](255) NULL,
	[FilterWHEREClause] [nvarchar](255) NULL,
	[IsExportEnabled] [bit] NULL DEFAULT ((0)),
	[SortOrder] [int] NULL
) ON [PRIMARY]
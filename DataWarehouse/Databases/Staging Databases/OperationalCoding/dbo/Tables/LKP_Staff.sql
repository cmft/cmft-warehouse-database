﻿CREATE TABLE [dbo].[LKP_Staff](
	[StaffID] [int] IDENTITY(1,1) NOT NULL,
	[PASUsername] [nvarchar](255) NULL,
	[CleanName] [nvarchar](255) NULL,
	[ParentCodingTeam] [nvarchar](255) NULL,
	[StaffGroup] [nvarchar](255) NULL,
	[Active] [bit] NULL CONSTRAINT [DF_LKP_Staff2_Active_1]  DEFAULT ((0)),
	[ADLogin] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[ContractedMinsPerWeek] [int] NULL,
	[AnnualLeaveEntitlement] [int] NULL
) ON [PRIMARY]
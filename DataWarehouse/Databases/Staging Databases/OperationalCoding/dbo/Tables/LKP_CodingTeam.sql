﻿CREATE TABLE [dbo].[LKP_CodingTeam](
	[CodingTeamID] [int] IDENTITY(1,1) NOT NULL,
	[CodingTeamName] [nvarchar](255) NULL,
	[SortOrder] [decimal](6, 2) NULL DEFAULT ((0)),
	[ParentCodingTeam] [nvarchar](255) NULL,
	[ColourCodeG] [int] NULL,
	[ColourCodeB] [int] NULL,
	[ColourCodeR] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SYS_Event](
	[ArchiveID] [int] NOT NULL,
	[ADLogin] [nvarchar](255) NULL,
	[EventName] [nvarchar](255) NULL,
	[EventTime] [smalldatetime] NULL CONSTRAINT [DF_ARC_Event_EventTime]  DEFAULT (getdate()),
	[AppID] [int] NULL,
	[EventArgument] [nvarchar](255) NULL,
	[Version] [nvarchar](10) NULL,
	[PCName] [nvarchar](50) NULL
) ON [PRIMARY]
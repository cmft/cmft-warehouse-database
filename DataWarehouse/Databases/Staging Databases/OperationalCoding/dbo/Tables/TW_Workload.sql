﻿CREATE TABLE [dbo].[TW_Workload](
	[WorkloadID] [int] IDENTITY(1,1) NOT NULL,
	[Workload] [nvarchar](255) NULL,
	[CodingTeamID] [int] NULL
) ON [PRIMARY]
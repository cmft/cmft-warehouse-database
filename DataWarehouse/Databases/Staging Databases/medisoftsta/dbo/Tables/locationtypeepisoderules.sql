﻿CREATE TABLE [dbo].[locationtypeepisoderules](
	[ltr_id] [uniqueidentifier] NOT NULL,
	[ltr_ety_id] [uniqueidentifier] NOT NULL,
	[ltr_lot_id] [uniqueidentifier] NOT NULL,
	[ltr_lot_source] [varchar](4) NOT NULL
) ON [PRIMARY]
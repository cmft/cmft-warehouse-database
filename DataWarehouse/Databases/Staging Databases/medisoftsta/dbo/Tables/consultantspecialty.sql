﻿CREATE TABLE [dbo].[consultantspecialty](
	[csp_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[csp_code] [varchar](4) NOT NULL,
	[csp_specialty] [varchar](255) NOT NULL,
	[csp_sign_off] [varchar](255) NOT NULL
) ON [PRIMARY]
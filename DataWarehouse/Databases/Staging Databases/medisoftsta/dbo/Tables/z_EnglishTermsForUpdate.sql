﻿CREATE TABLE [dbo].[z_EnglishTermsForUpdate](
	[MedisoftID] [nvarchar](255) NULL,
	[MedisoftConceptID] [nvarchar](255) NULL,
	[Concept Name] [nvarchar](255) NULL,
	[EnglishDesc] [nvarchar](255) NULL,
	[Correction] [nvarchar](255) NULL
) ON [PRIMARY]
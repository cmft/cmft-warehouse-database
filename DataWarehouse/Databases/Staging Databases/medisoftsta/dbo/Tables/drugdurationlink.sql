﻿CREATE TABLE [dbo].[drugdurationlink](
	[dul_id] [uniqueidentifier] NOT NULL,
	[dul_dru_id] [uniqueidentifier] NULL,
	[dul_ddu_id] [uniqueidentifier] NULL,
	[dul_order] [int] NOT NULL,
	[dul_available] [bit] NOT NULL,
	[timestamp] [binary](8) NULL
) ON [PRIMARY]
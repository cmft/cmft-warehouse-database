﻿CREATE TABLE [dbo].[outputcriteriaenable](
	[oce_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputcriteriaenable_oce_id]  DEFAULT (newid()),
	[oce_out_id] [uniqueidentifier] NULL,
	[oce_criteriaenable] [varchar](255) NULL
) ON [PRIMARY]
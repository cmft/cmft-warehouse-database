﻿CREATE TABLE [dbo].[ClinicalMenuLink](
	[cml_cct_id] [uniqueidentifier] NOT NULL,
	[cml_cfd_id] [uniqueidentifier] NOT NULL,
	[cml_cmn_id] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[specialities$](
	[speciality_code] [float] NULL,
	[speciality] [nvarchar](255) NULL,
	[speciality_sign_off] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[drugroute](
	[dro_id] [uniqueidentifier] NOT NULL,
	[dro_code] [varchar](255) NOT NULL,
	[dro_desc] [varchar](255) NOT NULL,
	[dro_order] [int] NOT NULL,
	[timestamp] [binary](8) NULL,
	[dro_flexible] [bit] NULL DEFAULT (0),
	[dro_descex] [varchar](255) NULL
) ON [PRIMARY]
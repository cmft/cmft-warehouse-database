﻿CREATE TABLE [dbo].[transactionfailureresponse](
	[tra_id] [uniqueidentifier] NOT NULL,
	[tra_error] [int] NOT NULL,
	[tra_action] [int] NOT NULL
) ON [PRIMARY]
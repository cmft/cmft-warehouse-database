﻿CREATE TABLE [dbo].[diagnosiscategory](
	[dct_id] [uniqueidentifier] NOT NULL,
	[dct_code] [varchar](255) NULL,
	[dct_dgt_id] [uniqueidentifier] NULL,
	[dct_order] [int] NULL,
	[dct_menu_no] [int] IDENTITY(1,1) NOT NULL,
	[dct_oct_code] [varchar](4) NULL
) ON [PRIMARY]
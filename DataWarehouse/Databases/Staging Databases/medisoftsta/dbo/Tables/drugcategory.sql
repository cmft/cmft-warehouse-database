﻿CREATE TABLE [dbo].[drugcategory](
	[dca_id] [uniqueidentifier] NOT NULL,
	[dca_desc] [varchar](255) NOT NULL,
	[dca_order] [int] NULL,
	[dca_descshort] [varchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[visualacuityvaluenear](
	[van_id] [uniqueidentifier] NOT NULL,
	[van_value] [varchar](255) NOT NULL,
	[van_key] [varchar](255) NOT NULL,
	[van_notation] [int] NULL,
	[van_rank] [int] NULL
) ON [PRIMARY]
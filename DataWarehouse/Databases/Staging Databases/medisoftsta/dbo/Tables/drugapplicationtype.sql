﻿CREATE TABLE [dbo].[drugapplicationtype](
	[dat_id] [uniqueidentifier] NOT NULL,
	[dat_code] [varchar](255) NOT NULL,
	[dat_desc] [varchar](255) NOT NULL,
	[dat_fixed] [bit] NOT NULL,
	[dat_processset] [bit] NOT NULL,
	[dat_defaultsforconsultants] [bit] NULL,
	[dat_allowedit] [bit] NOT NULL CONSTRAINT [DF__drugappli__dat_a__3A81B327]  DEFAULT (1),
	[dat_colourcode] [int] NULL
) ON [PRIMARY]
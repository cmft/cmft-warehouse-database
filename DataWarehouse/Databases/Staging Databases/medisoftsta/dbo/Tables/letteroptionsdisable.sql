﻿CREATE TABLE [dbo].[letteroptionsdisable](
	[lod_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_letteroptionsdisable_lod_id]  DEFAULT (newid()),
	[lod_let_id] [uniqueidentifier] NULL,
	[lod_optiondisable] [nvarchar](50) NULL
) ON [PRIMARY]
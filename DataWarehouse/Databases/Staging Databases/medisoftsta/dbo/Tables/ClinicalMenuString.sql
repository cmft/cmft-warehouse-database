﻿CREATE TABLE [dbo].[ClinicalMenuString](
	[cms_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_ClinicalMenuString_cms_id]  DEFAULT (newid()),
	[cms_string] [varchar](256) NOT NULL
) ON [PRIMARY]
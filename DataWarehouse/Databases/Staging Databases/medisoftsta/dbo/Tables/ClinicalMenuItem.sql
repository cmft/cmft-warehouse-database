﻿CREATE TABLE [dbo].[ClinicalMenuItem](
	[cmi_cmn_id] [int] NOT NULL,
	[cmi_order] [int] NOT NULL,
	[cmi_cms_id] [uniqueidentifier] NOT NULL,
	[cmi_ctm_id] [uniqueidentifier] NULL,
	[cmi_child_id] [int] NULL,
	[cmi_nad] [bit] NOT NULL DEFAULT (0),
	[cmi_code] [varchar](10) NULL,
	[cmi_action] [tinyint] NOT NULL DEFAULT (0)
) ON [PRIMARY]
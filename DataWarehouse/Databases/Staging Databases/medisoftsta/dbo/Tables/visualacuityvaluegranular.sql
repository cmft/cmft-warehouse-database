﻿CREATE TABLE [dbo].[visualacuityvaluegranular](
	[vag_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[vag_vav_id] [uniqueidentifier] NOT NULL,
	[vag_value] [varchar](255) NOT NULL,
	[vag_owner] [varchar](255) NOT NULL,
	[vag_order] [int] NOT NULL,
	[vag_valuelogmar] [varchar](10) NULL
) ON [PRIMARY]
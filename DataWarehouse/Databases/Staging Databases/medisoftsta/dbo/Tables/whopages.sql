﻿CREATE TABLE [dbo].[whopages](
	[whp_id] [int] IDENTITY(1,1) NOT NULL,
	[whp_whs_id] [int] NULL,
	[whp_desc] [varchar](255) NULL,
	[whp_number] [int] NULL,
	[whp_action] [int] NULL,
	[whp_active] [bit] NULL
) ON [PRIMARY]
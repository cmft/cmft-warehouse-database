﻿CREATE TABLE [dbo].[performanceelement](
	[pel_id] [uniqueidentifier] NOT NULL,
	[pel_no] [int] NOT NULL,
	[pel_name] [varchar](255) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[drugroutelink](
	[drl_id] [uniqueidentifier] NOT NULL,
	[drl_dru_id] [uniqueidentifier] NULL,
	[drl_dro_id] [uniqueidentifier] NULL,
	[drl_order] [int] NOT NULL,
	[drl_available] [bit] NOT NULL,
	[timestamp] [binary](8) NULL
) ON [PRIMARY]
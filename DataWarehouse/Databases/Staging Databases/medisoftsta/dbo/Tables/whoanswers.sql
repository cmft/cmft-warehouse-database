﻿CREATE TABLE [dbo].[whoanswers](
	[whr_id] [int] IDENTITY(1,1) NOT NULL,
	[whr_wha_id] [int] NULL,
	[whr_label] [varchar](255) NULL,
	[whr_color] [int] NULL,
	[whr_active] [bit] NULL
) ON [PRIMARY]
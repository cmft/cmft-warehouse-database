﻿CREATE TABLE [dbo].[ClinicalTerms](
	[ctm_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_ClinicalTerms_ctm_id]  DEFAULT (newid()),
	[ctm_cfd_id] [uniqueidentifier] NOT NULL,
	[ctm_description] [varchar](256) NOT NULL,
	[ctm_diagnosis] [int] NULL,
	[ctm_snomed] [varchar](32) NULL,
	[ctm_exclude_xml] [int] NULL CONSTRAINT [DF_ClinicalTerms_ctm_exclude_xml]  DEFAULT (0),
	[ctm_cpg_id] [uniqueidentifier] NULL,
	[ctm_grade] [int] NULL,
	[ctm_action] [tinyint] NOT NULL DEFAULT (0),
	[ctm_abbreviation] [varchar](25) NULL,
	[ctm_descriptor1] [varchar](255) NULL,
	[ctm_descriptor2] [varchar](255) NULL,
	[ctm_searchable] [bit] NULL DEFAULT (1),
	[ctm_duplicateof] [uniqueidentifier] NULL,
	[ctm_order] [int] NULL DEFAULT ((0)),
	[ctm_stp_id] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[grade](
	[gra_id] [uniqueidentifier] NOT NULL,
	[gra_code] [varchar](255) NOT NULL,
	[gra_desc] [varchar](255) NOT NULL,
	[gra_seniorityorder] [tinyint] NOT NULL,
	[timestamp] [binary](8) NULL,
	[gra_listorder] [tinyint] NULL,
	[gra_current] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputoptions](
	[oop_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputoptions_oop_id]  DEFAULT (newid()),
	[oop_out_id] [uniqueidentifier] NULL,
	[oop_ocr_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[usertype](
	[ust_id] [uniqueidentifier] NOT NULL,
	[ust_code] [varchar](8) NULL,
	[ust_desc] [varchar](255) NOT NULL,
	[timestamp] [timestamp] NOT NULL,
	[ust_valid] [bit] NULL,
	[ust_no] [int] NULL
) ON [PRIMARY]
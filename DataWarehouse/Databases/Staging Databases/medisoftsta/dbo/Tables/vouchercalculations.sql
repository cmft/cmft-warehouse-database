﻿CREATE TABLE [dbo].[vouchercalculations](
	[voh_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[voh_type] [int] NOT NULL,
	[voh_xlow] [float] NULL,
	[voh_xhigh] [float] NULL,
	[voh_ylow] [float] NULL,
	[voh_yhigh] [float] NULL,
	[voh_code] [varchar](5) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputexportfield](
	[oxf_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputexportfields_oxf_id]  DEFAULT (newid()),
	[oxf_order] [smallint] NULL,
	[oxf_category] [varchar](50) NULL,
	[oxf_caption] [varchar](50) NULL,
	[oxf_fieldname] [varchar](50) NULL,
	[oxf_cursorname] [varchar](50) NULL,
	[oxf_lookup] [varchar](50) NULL,
	[oxf_newfield] [varchar](50) NULL,
	[oxf_excludeif] [varchar](50) NULL,
	[oxf_enabled] [bit] NULL,
	[oxf_out_id] [uniqueidentifier] NULL
) ON [PRIMARY]
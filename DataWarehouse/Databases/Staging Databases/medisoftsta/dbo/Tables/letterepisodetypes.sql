﻿CREATE TABLE [dbo].[letterepisodetypes](
	[lpy_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_letterepisodetypes_lpy_id]  DEFAULT (newid()),
	[lpy_let_id] [uniqueidentifier] NULL,
	[lpy_ety_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[trialdata](
	[tri_interval] [float] NULL,
	[tri_name] [nvarchar](255) NULL,
	[tri_value] [float] NULL,
	[tri_code] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[diagnosismappinglink](
	[dml_id] [uniqueidentifier] NULL,
	[dml_dma_id] [uniqueidentifier] NULL,
	[dml_rft_id_maptype] [uniqueidentifier] NULL,
	[dml_fks_id] [uniqueidentifier] NULL,
	[dml_mandatory] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputstandardoptions](
	[oso_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputstandardoptions_oso_id]  DEFAULT (newid()),
	[oso_owner] [int] NULL,
	[oso_code] [varchar](50) NULL,
	[oso_description] [varchar](50) NULL,
	[oso_order] [int] NULL
) ON [PRIMARY]
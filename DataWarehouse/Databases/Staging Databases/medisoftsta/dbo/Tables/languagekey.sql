﻿CREATE TABLE [dbo].[languagekey](
	[lan_id] [uniqueidentifier] NULL,
	[lan_name] [varchar](255) NULL,
	[lan_code] [varchar](255) NULL
) ON [PRIMARY]
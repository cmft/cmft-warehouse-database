﻿CREATE TABLE [dbo].[SqlBroker](
	[BrokerID] [int] IDENTITY(1,1) NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ObjectMethodID] [int] NOT NULL,
	[ObjectMethodName] [varchar](255) NOT NULL,
	[MethodSQL] [varchar](2000) NOT NULL,
	[ObjectClassName] [varchar](255) NULL
) ON [PRIMARY]
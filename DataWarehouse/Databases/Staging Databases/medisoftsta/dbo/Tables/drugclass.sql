﻿CREATE TABLE [dbo].[drugclass](
	[dcl_id] [uniqueidentifier] NOT NULL,
	[dcl_desc] [varchar](255) NOT NULL,
	[dcl_dca_id] [uniqueidentifier] NULL,
	[dcl_order] [int] NULL,
	[timestamp] [binary](8) NULL
) ON [PRIMARY]
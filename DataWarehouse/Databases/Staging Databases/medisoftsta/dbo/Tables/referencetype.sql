﻿CREATE TABLE [dbo].[referencetype](
	[rfy_id] [uniqueidentifier] NOT NULL,
	[rfy_code] [varchar](255) NOT NULL,
	[rfy_desc] [varchar](255) NOT NULL,
	[rfy_order] [int] NULL,
	[rfy_available] [bit] NULL,
	[rfy_medicalhistory] [bit] NULL,
	[rfy_rfc_no] [int] NULL,
	[rfy_canqualify] [bit] NULL DEFAULT (0)
) ON [PRIMARY]
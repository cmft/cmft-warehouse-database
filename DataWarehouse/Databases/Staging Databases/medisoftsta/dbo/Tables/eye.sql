﻿CREATE TABLE [dbo].[eye](
	[eye_id] [uniqueidentifier] NOT NULL,
	[eye_code] [varchar](255) NOT NULL,
	[eye_desc] [varchar](255) NOT NULL,
	[eye_leftright] [bit] NOT NULL,
	[eye_both] [bit] NOT NULL,
	[eye_blank] [bit] NOT NULL,
	[eye_num_eyes] [int] NULL,
	[eye_order] [tinyint] NULL,
	[timestamp] [binary](8) NULL,
	[eye_dro_id] [uniqueidentifier] NULL,
	[eye_ctm_id] [uniqueidentifier] NULL
) ON [PRIMARY]
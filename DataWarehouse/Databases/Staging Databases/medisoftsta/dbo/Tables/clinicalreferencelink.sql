﻿CREATE TABLE [dbo].[clinicalreferencelink](
	[crl_id] [uniqueidentifier] NOT NULL,
	[crl_rfy_code] [varchar](32) NOT NULL,
	[crl_rft_code] [varchar](32) NOT NULL,
	[crl_ctm_id] [uniqueidentifier] NULL,
	[crl_rfy_codesummary] [varchar](10) NULL,
	[crl_negative] [bit] NULL,
	[crl_history] [bit] NULL,
	[crl_enabled] [bit] NULL DEFAULT (1)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputcriteriaoperator](
	[oop_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputcriteriaoperator_oop_id]  DEFAULT (newid()),
	[oop_operator] [varchar](50) NULL,
	[oop_datatype] [varchar](50) NULL
) ON [PRIMARY]
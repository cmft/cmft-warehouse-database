﻿CREATE TABLE [dbo].[drugduration](
	[ddu_id] [uniqueidentifier] NOT NULL,
	[ddu_code] [varchar](255) NOT NULL,
	[ddu_desc] [varchar](255) NOT NULL,
	[ddu_order] [int] NOT NULL,
	[ddu_intervalno] [int] NULL,
	[ddu_intervaltype] [varchar](2) NULL,
	[timestamp] [binary](8) NULL,
	[ddu_descex] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SqlBrokerFilter](
	[BrokerFilterID] [int] IDENTITY(1,1) NOT NULL,
	[BrokerObjectID] [int] NOT NULL,
	[FilterID] [int] NOT NULL,
	[FilterSQL] [varchar](2000) NOT NULL
) ON [PRIMARY]
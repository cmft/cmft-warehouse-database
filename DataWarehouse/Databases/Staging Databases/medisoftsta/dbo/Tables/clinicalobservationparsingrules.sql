﻿CREATE TABLE [dbo].[clinicalobservationparsingrules](
	[cop_id] [uniqueidentifier] NOT NULL,
	[cop_cpg_id] [uniqueidentifier] NOT NULL,
	[cop_order] [int] NULL,
	[cop_cfd_id_qualifier] [uniqueidentifier] NOT NULL,
	[cop_prepend] [varchar](255) NULL,
	[cop_append] [varchar](255) NULL
) ON [PRIMARY]
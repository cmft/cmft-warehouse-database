﻿CREATE TABLE [dbo].[clinicaltermstring](
	[cts_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[cts_ctm_id] [uniqueidentifier] NULL,
	[cts_value] [varchar](256) NOT NULL,
	[cts_context] [tinyint] NOT NULL
) ON [PRIMARY]
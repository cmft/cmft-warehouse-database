﻿CREATE TABLE [dbo].[operationcategory](
	[oct_id] [uniqueidentifier] NOT NULL,
	[oct_code] [varchar](10) NULL,
	[oct_desc] [varchar](255) NOT NULL,
	[oct_order] [int] NOT NULL CONSTRAINT [DF__operation__oct_o__47540065]  DEFAULT (0),
	[oct_membership] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputgraphlegends](
	[ogl_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputgraphlegends_ogl_id]  DEFAULT (newid()),
	[ogl_ogr_id] [uniqueidentifier] NULL,
	[ogl_setnumber] [int] NULL,
	[ogl_legendtext] [varchar](50) NULL,
	[ogl_legendcolor] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[diagnosis](
	[dgn_id] [uniqueidentifier] NOT NULL,
	[dgn_dct_id] [uniqueidentifier] NULL,
	[dgn_dgt_id] [uniqueidentifier] NULL,
	[dgn_code] [varchar](255) NULL,
	[dgn_code_1] [varchar](255) NULL,
	[dgn_code_2] [varchar](255) NULL,
	[dgn_code_3] [varchar](255) NULL,
	[dgn_code_4] [varchar](255) NULL,
	[dgn_code_5] [varchar](255) NULL,
	[dgn_rft_id_system] [uniqueidentifier] NULL,
	[dgn_comment] [varchar](255) NULL,
	[dgn_sublist] [int] NULL,
	[dgn_order] [int] NULL,
	[dgn_dgt_id_broad] [uniqueidentifier] NULL,
	[dgn_searchable] [bit] NULL DEFAULT (1),
	[dgn_duplicateof] [uniqueidentifier] NULL,
	[dgn_flag] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
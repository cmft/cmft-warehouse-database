﻿CREATE TABLE [dbo].[diabeticassesmentrules](
	[dar_id] [uniqueidentifier] NOT NULL,
	[dar_ruledesc] [varchar](1000) NULL,
	[dar_termsrequired] [int] NULL,
	[dar_gradeletternsc] [varchar](10) NULL,
	[dar_gradenumbernsc] [int] NULL,
	[dar_dgc_id_international] [uniqueidentifier] NULL,
	[dar_dgc_id_etdrs] [uniqueidentifier] NULL,
	[dar_gradenumberetdrs] [varchar](10) NULL,
	[dar_dgn_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[securitysystemaclold](
	[n_acl_id] [int] IDENTITY(2000,1) NOT NULL,
	[n_sec_id] [int] NULL,
	[n_sys_id] [int] NULL,
	[n_acc_level] [int] NULL
) ON [PRIMARY]
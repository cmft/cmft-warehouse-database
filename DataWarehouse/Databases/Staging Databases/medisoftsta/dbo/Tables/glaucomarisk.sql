﻿CREATE TABLE [dbo].[glaucomarisk](
	[gri_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_glaucomarisk_gri_id]  DEFAULT (newid()),
	[gri_low_range] [numeric](15, 2) NOT NULL,
	[gri_high_range] [numeric](15, 2) NOT NULL,
	[gri_classification] [varchar](255) NOT NULL,
	[gri_stars] [int] NOT NULL
) ON [PRIMARY]
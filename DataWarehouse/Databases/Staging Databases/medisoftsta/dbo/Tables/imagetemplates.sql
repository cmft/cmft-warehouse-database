﻿CREATE TABLE [dbo].[imagetemplates](
	[itp_id] [uniqueidentifier] NOT NULL,
	[itp_no] [int] NOT NULL,
	[itp_image] [image] NULL,
	[itp_enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
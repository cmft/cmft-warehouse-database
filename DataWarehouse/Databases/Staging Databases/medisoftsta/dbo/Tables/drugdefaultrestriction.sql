﻿CREATE TABLE [dbo].[drugdefaultrestriction](
	[dcr_id] [uniqueidentifier] NULL CONSTRAINT [DF_drudefaultrestriction_dcr_id]  DEFAULT (newid()),
	[dcr_dat_code] [varchar](4) NULL,
	[dcr_defaultentity_no] [tinyint] NULL,
	[dcr_defaultentity_allowed] [bit] NULL
) ON [PRIMARY]
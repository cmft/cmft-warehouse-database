﻿CREATE TABLE [dbo].[diabeticassesmentrulesmember](
	[drm_id] [uniqueidentifier] NOT NULL,
	[drm_dar_id] [uniqueidentifier] NOT NULL,
	[drm_dig_id] [uniqueidentifier] NOT NULL,
	[drm_value] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[captionsimport](
	[Caption] [nvarchar](255) NULL,
	[CaptionOwner] [nvarchar](255) NULL,
	[CaptionControl] [nvarchar](255) NULL,
	[CaptionIndex] [float] NULL,
	[CaptionSource] [nvarchar](255) NULL
) ON [PRIMARY]
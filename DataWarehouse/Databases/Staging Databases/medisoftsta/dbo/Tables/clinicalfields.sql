﻿CREATE TABLE [dbo].[clinicalfields](
	[cfd_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_clinicalfields_cfd_id]  DEFAULT (newid()),
	[cfd_name] [varchar](128) NULL,
	[cfd_cmn_id_s] [int] NULL,
	[cfd_cmn_id_c] [int] NULL,
	[cfd_order] [int] NULL,
	[cfd_cft_id] [uniqueidentifier] NULL,
	[cfd_code] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[genericentitytype](
	[gnt_id] [int] IDENTITY(1,1) NOT NULL,
	[gnt_code] [varchar](4) NOT NULL,
	[gnt_desc] [varchar](255) NOT NULL,
	[gnt_order] [int] NOT NULL,
	[gnt_available] [bit] NOT NULL,
	[gnt_category_no] [int] NULL,
	[gnt_isgenent] [bit] NOT NULL,
	[gnt_audit_userid] [int] NOT NULL,
	[gnt_audit_date] [datetime] NOT NULL
) ON [PRIMARY]
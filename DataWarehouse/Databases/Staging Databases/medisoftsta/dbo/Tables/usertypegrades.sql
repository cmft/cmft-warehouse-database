﻿CREATE TABLE [dbo].[usertypegrades](
	[utg_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_usertypegrades_utg_id]  DEFAULT (newid()),
	[utg_ust_id] [uniqueidentifier] NULL,
	[utg_gra_id] [uniqueidentifier] NULL,
	[utg_ust_desc] [varchar](255) NULL,
	[utg_gra_desc] [varchar](255) NULL,
	[utg_valid] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputstandardcriteria](
	[oqc_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputstandardcriteria_oqc_id]  DEFAULT (newid()),
	[oqc_code] [varchar](50) NULL,
	[oqc_desc] [varchar](50) NULL,
	[oqc_operator] [varchar](50) NULL,
	[oqc_datatype] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[diagnosismap](
	[dma_id] [uniqueidentifier] NULL,
	[dma_dgn_id] [uniqueidentifier] NULL,
	[dma_name] [nvarchar](50) NULL,
	[dma_prompt] [bit] NOT NULL,
	[dma_percent] [int] NULL
) ON [PRIMARY]
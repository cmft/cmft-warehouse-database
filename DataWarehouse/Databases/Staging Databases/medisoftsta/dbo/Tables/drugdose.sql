﻿CREATE TABLE [dbo].[drugdose](
	[dos_id] [uniqueidentifier] NOT NULL,
	[dos_code] [varchar](255) NULL,
	[dos_desc] [varchar](255) NOT NULL,
	[dos_order] [int] NOT NULL,
	[timestamp] [binary](8) NULL,
	[dos_flexible] [bit] NULL DEFAULT (0),
	[dos_descex] [varchar](255) NULL
) ON [PRIMARY]
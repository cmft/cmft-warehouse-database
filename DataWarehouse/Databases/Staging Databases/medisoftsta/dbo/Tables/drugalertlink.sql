﻿CREATE TABLE [dbo].[drugalertlink](
	[dal_id] [uniqueidentifier] NOT NULL,
	[dal_dru_id] [uniqueidentifier] NULL,
	[dal_ale_id] [uniqueidentifier] NULL,
	[dal_order] [int] NOT NULL,
	[dal_available] [bit] NOT NULL,
	[timestamp] [binary](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[x_Route](
	[Order] [float] NULL,
	[Route] [nvarchar](255) NULL,
	[Comments] [nvarchar](255) NULL,
	[Abbn] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[x_DrugAbbreviations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Drug] [nvarchar](255) NULL,
	[Abbreviation] [nvarchar](255) NULL
) ON [PRIMARY]
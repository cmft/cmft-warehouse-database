﻿CREATE TABLE [dbo].[databaseindexes](
	[TableName] [sysname] NOT NULL,
	[TableID] [int] NOT NULL,
	[IndexName] [sysname] NULL,
	[IndexID] [int] NOT NULL,
	[IndexType] [nvarchar](60) NULL,
	[FillFactor] [tinyint] NOT NULL,
	[IsPadded] [bit] NULL,
	[ColumnName] [sysname] NULL,
	[IndexColumnOrdinal] [tinyint] NOT NULL,
	[IsPrimaryKey] [bit] NULL,
	[IsIncludedColumn] [bit] NULL,
	[DateAdded] [datetime] NOT NULL,
	[ColumnList] [varchar](8000) NULL
) ON [PRIMARY]
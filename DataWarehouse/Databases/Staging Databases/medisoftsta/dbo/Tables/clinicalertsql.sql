﻿CREATE TABLE [dbo].[clinicalertsql](
	[cas_cac_id] [uniqueidentifier] NOT NULL,
	[cas_order] [int] NOT NULL,
	[cas_sql] [varchar](1000) NOT NULL,
	[cas_freetext] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
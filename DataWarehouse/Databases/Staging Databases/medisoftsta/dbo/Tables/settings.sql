﻿CREATE TABLE [dbo].[settings](
	[set_id] [int] NOT NULL,
	[set_value] [varchar](255) NOT NULL,
	[set_description] [varchar](255) NOT NULL,
	[timestamp] [timestamp] NOT NULL
) ON [PRIMARY]
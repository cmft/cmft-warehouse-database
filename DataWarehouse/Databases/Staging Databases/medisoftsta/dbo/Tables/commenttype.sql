﻿CREATE TABLE [dbo].[commenttype](
	[cty_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_commenttype_cty_id]  DEFAULT (newid()),
	[cty_code] [varchar](255) NOT NULL,
	[cty_desc] [varchar](255) NOT NULL,
	[timestamp] [binary](8) NULL,
	[cty_ctm_id] [uniqueidentifier] NULL
) ON [PRIMARY]
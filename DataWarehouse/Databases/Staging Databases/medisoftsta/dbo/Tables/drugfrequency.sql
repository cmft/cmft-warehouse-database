﻿CREATE TABLE [dbo].[drugfrequency](
	[dfr_id] [uniqueidentifier] NOT NULL,
	[dfr_code] [varchar](255) NOT NULL,
	[dfr_desc] [varchar](255) NOT NULL,
	[dfr_order] [int] NOT NULL,
	[timestamp] [binary](8) NULL,
	[dfr_descex] [varchar](255) NULL
) ON [PRIMARY]
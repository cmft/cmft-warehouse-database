﻿CREATE TABLE [dbo].[x_LuminousValidationLogic](
	[Form(s)] [nvarchar](255) NULL,
	[Query Name] [nvarchar](255) NULL,
	[Target Variable] [nvarchar](255) NULL,
	[Query Logic] [nvarchar](255) NULL,
	[Resolution/Query Text] [varchar](2000) NULL,
	[Query Type] [nvarchar](255) NULL,
	[User-Validate] [nvarchar](255) NULL
) ON [PRIMARY]
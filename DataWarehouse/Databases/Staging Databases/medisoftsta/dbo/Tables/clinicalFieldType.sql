﻿CREATE TABLE [dbo].[clinicalFieldType](
	[cft_id] [uniqueidentifier] NOT NULL,
	[cft_code] [varchar](8) NOT NULL,
	[cft_desc] [varchar](64) NOT NULL
) ON [PRIMARY]
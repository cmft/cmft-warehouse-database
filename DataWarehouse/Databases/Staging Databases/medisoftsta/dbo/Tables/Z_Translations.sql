﻿CREATE TABLE [dbo].[Z_Translations](
	[English term] [nvarchar](255) NULL,
	[Portuguese term] [nvarchar](255) NULL,
	[Order] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalCategory](
	[cct_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_ClinicalCategory_cct_id]  DEFAULT (newid()),
	[cct_name] [varchar](128) NOT NULL,
	[cct_order] [int] NULL,
	[cct_membershipnumber] [int] NOT NULL CONSTRAINT [DF__clinicalc__cct_m__3A8CA01F]  DEFAULT (3)
) ON [PRIMARY]
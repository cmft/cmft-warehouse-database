﻿CREATE TABLE [dbo].[outputfield](
	[ofd_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputfield_ofd_id]  DEFAULT (newid()),
	[ofd_out_id] [uniqueidentifier] NULL,
	[ofd_ocr_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[clientmenus](
	[mcl_menu_no] [int] NOT NULL,
	[mcl_menu_name] [nvarchar](255) NULL,
	[mcl_item_no] [int] NOT NULL,
	[mcl_item_name] [nvarchar](255) NULL,
	[mcl_Item_icon] [int] NULL,
	[mcl_level] [int] NULL,
	[mcl_visible] [bit] NULL,
	[mcl_acl_id] [int] NULL,
	[mcl_parent_acl_id] [int] NULL,
	[mcl_parent] [bit] NULL,
	[mcl_parent_menu_no] [int] NULL,
	[mcl_membership] [int] NULL,
	[mcl_id] [uniqueidentifier] NULL
) ON [PRIMARY]
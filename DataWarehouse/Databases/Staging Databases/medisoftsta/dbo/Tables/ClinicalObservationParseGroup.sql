﻿CREATE TABLE [dbo].[ClinicalObservationParseGroup](
	[cpg_id] [uniqueidentifier] NULL,
	[cpg_code] [varchar](10) NULL,
	[cpg_desc] [varchar](255) NULL,
	[cpg_prependQualifier] [bit] NOT NULL DEFAULT (1)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputquery](
	[ouq_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputquery_ouq_id]  DEFAULT (newid()),
	[ouq_xslfile] [nvarchar](50) NULL,
	[ouq_sql] [nvarchar](2000) NULL,
	[ouq_key] [int] NULL,
	[ouq_title] [nvarchar](100) NULL
) ON [PRIMARY]
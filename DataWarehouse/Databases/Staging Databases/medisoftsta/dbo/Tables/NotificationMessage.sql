﻿CREATE TABLE [dbo].[NotificationMessage](
	[nmg_id] [int] NOT NULL,
	[nmg_type] [tinyint] NOT NULL,
	[nmg_message] [varchar](255) NOT NULL
) ON [PRIMARY]
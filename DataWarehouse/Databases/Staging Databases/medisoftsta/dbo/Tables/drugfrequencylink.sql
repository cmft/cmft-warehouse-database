﻿CREATE TABLE [dbo].[drugfrequencylink](
	[dfl_id] [uniqueidentifier] NOT NULL,
	[dfl_dfr_id] [uniqueidentifier] NULL,
	[dfl_dru_id] [uniqueidentifier] NULL,
	[dfl_order] [int] NOT NULL,
	[dfl_available] [bit] NOT NULL,
	[timestamp] [binary](8) NULL
) ON [PRIMARY]
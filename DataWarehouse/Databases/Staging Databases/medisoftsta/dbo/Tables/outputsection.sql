﻿CREATE TABLE [dbo].[outputsection](
	[ose_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputsection_ose_id]  DEFAULT (newid()),
	[ose_out_id] [uniqueidentifier] NULL,
	[ose_number] [int] NULL,
	[ose_code] [varchar](50) NULL,
	[ose_description] [varchar](50) NULL
) ON [PRIMARY]
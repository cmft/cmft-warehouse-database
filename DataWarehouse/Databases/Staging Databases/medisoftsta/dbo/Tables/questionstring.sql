﻿CREATE TABLE [dbo].[questionstring](
	[qsr_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_questionstring_qsr_id]  DEFAULT (newid()),
	[qsr_type_code] [varchar](4) NOT NULL,
	[qsr_code] [varchar](4) NOT NULL,
	[qsr_answer] [int] NOT NULL,
	[gender_code] [varchar](8) NULL,
	[qsr_string] [varchar](128) NOT NULL
) ON [PRIMARY]
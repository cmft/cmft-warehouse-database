﻿CREATE TABLE [dbo].[uiobjects](
	[uio_id] [uniqueidentifier] NULL,
	[uio_object_type] [nvarchar](255) NULL,
	[uio_object] [nvarchar](255) NULL
) ON [PRIMARY]
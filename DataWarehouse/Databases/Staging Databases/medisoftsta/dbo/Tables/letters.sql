﻿CREATE TABLE [dbo].[letters](
	[let_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_letters_let_id]  DEFAULT (newid()),
	[let_type] [tinyint] NULL,
	[let_name] [nvarchar](255) NULL,
	[let_available] [bit] NULL,
	[let_code] [nvarchar](255) NULL,
	[let_xsl_file] [varchar](64) NULL,
	[let_edit] [int] NULL,
	[let_hide_unedited] [bit] NOT NULL DEFAULT (0),
	[let_orientation] [int] NULL,
	[let_esend] [bit] NULL DEFAULT (0),
	[let_flag] [int] NULL
) ON [PRIMARY]
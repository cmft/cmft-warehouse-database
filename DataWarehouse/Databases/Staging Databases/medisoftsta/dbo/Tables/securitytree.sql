﻿CREATE TABLE [dbo].[securitytree](
	[n_sys_id] [int] NOT NULL,
	[c_sys_name] [varchar](255) NULL,
	[n_sys_parent_id] [int] NULL,
	[treelevel] [int] NULL,
	[treeorder] [int] NULL,
	[parentonly] [bit] NULL,
	[isenabled] [bit] NULL
) ON [PRIMARY]
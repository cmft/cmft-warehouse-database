﻿CREATE TABLE [dbo].[drugunit](
	[drn_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_drugunit_drn_id]  DEFAULT (newid()),
	[drn_code] [varchar](255) NOT NULL,
	[drn_desc] [varchar](255) NOT NULL,
	[drn_order] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputfieldcategory](
	[ofc_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputfieldcategory_ofc_id]  DEFAULT (newid()),
	[ofc_description] [varchar](255) NULL
) ON [PRIMARY]
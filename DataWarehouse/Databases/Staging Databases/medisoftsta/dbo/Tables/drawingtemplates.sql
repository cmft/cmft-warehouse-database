﻿CREATE TABLE [dbo].[drawingtemplates](
	[drt_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_drawingtemplates_drt_id]  DEFAULT (newid()),
	[drt_itemno] [int] NULL,
	[drt_enabled] [bit] NULL,
	[drt_description] [varchar](255) NULL,
	[drt_filename] [varchar](255) NULL,
	[drt_defaultshapetab] [int] NULL,
	[drt_laterality] [bit] NULL,
	[drt_shapetabs] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[captions](
	[cap_id] [uniqueidentifier] NOT NULL,
	[cap_lan_id] [uniqueidentifier] NULL,
	[cap_desc] [varchar](255) NULL,
	[cap_name] [varchar](255) NULL,
	[cap_owner] [varchar](255) NULL,
	[cap_owner_type] [varchar](255) NULL,
	[cap_index] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[clinicliststeprule](
	[clr_id] [int] IDENTITY(1,1) NOT NULL,
	[clr_cru_id] [int] NULL,
	[clr_stp_id] [int] NULL,
	[clr_fks_id] [uniqueidentifier] NULL,
	[clr_audit_userid] [int] NOT NULL DEFAULT ((-1)),
	[clr_audit_date] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]
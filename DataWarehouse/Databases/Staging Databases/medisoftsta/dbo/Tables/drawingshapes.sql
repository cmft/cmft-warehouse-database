﻿CREATE TABLE [dbo].[drawingshapes](
	[drs_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_drawingshapw_drs_id]  DEFAULT (newid()),
	[drs_ctm_id] [uniqueidentifier] NULL,
	[drs_name] [varchar](255) NULL,
	[drs_itemno] [int] NULL,
	[drs_filename] [varchar](255) NULL,
	[drs_shape] [varchar](50) NULL,
	[drs_abbreviate] [varchar](255) NULL,
	[drs_xpos] [int] NULL,
	[drs_ypos] [int] NULL,
	[drs_xbound] [int] NULL,
	[drs_ybound] [int] NULL,
	[drs_color] [int] NULL,
	[drs_extra] [varchar](255) NULL
) ON [PRIMARY]
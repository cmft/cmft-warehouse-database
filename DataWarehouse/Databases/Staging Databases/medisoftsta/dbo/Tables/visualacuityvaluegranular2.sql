﻿CREATE TABLE [dbo].[visualacuityvaluegranular2](
	[vge_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[vge_vav_id] [uniqueidentifier] NULL,
	[vge_rank] [int] NULL,
	[vge_owner] [varchar](255) NULL,
	[vge_order] [int] NULL,
	[vge_valuelogmar] [varchar](10) NULL
) ON [PRIMARY]
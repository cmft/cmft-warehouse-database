﻿CREATE TABLE [dbo].[drugalert](
	[ale_id] [uniqueidentifier] NOT NULL,
	[ale_desc] [varchar](255) NOT NULL,
	[ale_order] [int] NOT NULL,
	[timestamp] [binary](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[letteroptions](
	[lop_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_letteroptions_lop_id]  DEFAULT (newid()),
	[lop_code] [nvarchar](50) NULL,
	[lop_desc] [nvarchar](50) NULL
) ON [PRIMARY]
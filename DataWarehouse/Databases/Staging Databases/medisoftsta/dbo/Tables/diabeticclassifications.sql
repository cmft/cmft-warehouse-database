﻿CREATE TABLE [dbo].[diabeticclassifications](
	[dgc_id] [uniqueidentifier] NOT NULL,
	[dgc_classno] [int] NULL,
	[dgc_classitem] [varchar](255) NULL,
	[dgc_classitemrank] [int] NULL,
	[dgc_dgn_id] [uniqueidentifier] NULL,
	[dgc_ctm_id] [uniqueidentifier] NULL
) ON [PRIMARY]
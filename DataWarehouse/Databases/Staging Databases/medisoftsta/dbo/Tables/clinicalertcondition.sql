﻿CREATE TABLE [dbo].[clinicalertcondition](
	[cac_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__clinicale__cac_i__5FCF6C7E]  DEFAULT (newid()),
	[cac_cal_id] [uniqueidentifier] NOT NULL,
	[cac_lateralised] [bit] NULL CONSTRAINT [DF__clinicale__cac_l__60C390B7]  DEFAULT (0),
	[cac_order] [int] NULL,
	[cac_disabled] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
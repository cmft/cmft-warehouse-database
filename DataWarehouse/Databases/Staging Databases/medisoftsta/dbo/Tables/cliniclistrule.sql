﻿CREATE TABLE [dbo].[cliniclistrule](
	[cru_id] [int] IDENTITY(1,1) NOT NULL,
	[cru_ruletype] [int] NULL,
	[cru_ruleno] [int] NULL,
	[cru_name] [varchar](255) NULL,
	[cru_sql] [varchar](255) NULL,
	[cru_audit_userid] [int] NOT NULL DEFAULT ((-1)),
	[cru_audit_date] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]
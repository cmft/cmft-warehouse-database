﻿CREATE TABLE [dbo].[episodetype](
	[ety_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_episodetype_ety_id]  DEFAULT (newid()),
	[ety_code] [varchar](255) NOT NULL,
	[ety_desc] [varchar](255) NOT NULL,
	[ety_classname] [varchar](255) NULL,
	[ety_order] [int] NULL,
	[ety_platform_flags] [int] NULL,
	[ety_referral] [bit] NULL,
	[ety_option] [int] NULL,
	[ety_sys_id] [int] NULL
) ON [PRIMARY]
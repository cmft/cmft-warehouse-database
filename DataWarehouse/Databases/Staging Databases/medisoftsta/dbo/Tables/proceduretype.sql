﻿CREATE TABLE [dbo].[proceduretype](
	[prt_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__procedure__prt_i__53B9D74A]  DEFAULT (newid()),
	[prt_code] [varchar](10) NULL,
	[prt_desc] [varchar](255) NULL
) ON [PRIMARY]
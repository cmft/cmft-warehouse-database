﻿CREATE TABLE [dbo].[medicalreading](
	[med_id] [uniqueidentifier] NOT NULL,
	[med_code] [varchar](255) NOT NULL,
	[med_desc] [varchar](255) NOT NULL,
	[med_warn_min] [float] NULL,
	[med_warn_max] [float] NULL,
	[med_unit] [varchar](8) NULL,
	[timestamp] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[staticsettings](
	[sts_id] [int] NOT NULL,
	[sts_value] [varchar](255) NULL,
	[sts_description] [varchar](255) NULL
) ON [PRIMARY]
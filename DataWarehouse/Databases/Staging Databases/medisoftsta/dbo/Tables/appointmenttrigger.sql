﻿CREATE TABLE [dbo].[appointmenttrigger](
	[apt_id] [uniqueidentifier] NOT NULL,
	[apt_fks_id] [uniqueidentifier] NULL,
	[apt_clinic] [int] NULL,
	[apt_source] [int] NULL
) ON [PRIMARY]
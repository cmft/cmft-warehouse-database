﻿CREATE TABLE [dbo].[drugapplicationdrug](
	[dad_id] [uniqueidentifier] NOT NULL,
	[dad_dru_id] [uniqueidentifier] NOT NULL,
	[dad_dat_id] [uniqueidentifier] NOT NULL,
	[dad_order] [int] NULL,
	[dad_available] [bit] NULL
) ON [PRIMARY]
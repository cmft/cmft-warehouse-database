﻿CREATE TABLE [dbo].[outputcategory](
	[oca_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputcategory_oca_id]  DEFAULT (newid()),
	[oca_desc] [varchar](255) NULL,
	[oca_indent] [smallint] NULL,
	[oca_hassubfolder] [bit] NULL
) ON [PRIMARY]
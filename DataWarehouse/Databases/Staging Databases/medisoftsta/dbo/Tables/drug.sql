﻿CREATE TABLE [dbo].[drug](
	[dru_id] [uniqueidentifier] NOT NULL,
	[dru_dcl_id] [uniqueidentifier] NULL,
	[dru_desc] [varchar](255) NOT NULL,
	[dru_order] [int] NULL,
	[dru_abbreviation] [varchar](255) NULL
) ON [PRIMARY]
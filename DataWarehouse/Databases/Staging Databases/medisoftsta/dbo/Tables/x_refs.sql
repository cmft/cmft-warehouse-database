﻿CREATE TABLE [dbo].[x_refs](
	[TitleDisp] [nvarchar](255) NULL,
	[SystemCode] [nvarchar](255) NULL,
	[Order] [float] NULL,
	[TitleLang] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[visualacuityvalue](
	[vav_id] [uniqueidentifier] NOT NULL,
	[vav_snellenmetres] [varchar](255) NOT NULL,
	[vav_snellenfeet] [varchar](255) NOT NULL,
	[vav_snellenfraction] [varchar](255) NOT NULL,
	[vav_mar] [varchar](255) NOT NULL,
	[vav_logmar] [varchar](255) NOT NULL,
	[vav_cpd38cm] [varchar](255) NOT NULL,
	[vav_snellenmetreslogmardom] [varchar](255) NOT NULL,
	[vav_snellenfeetlogmardom] [varchar](255) NOT NULL,
	[vav_snellenfractionlogmardom] [varchar](255) NOT NULL,
	[vav_marlogmardom] [varchar](255) NOT NULL,
	[vav_logmarlogmardom] [varchar](255) NOT NULL,
	[vav_cpd38cmlogmardom] [varchar](255) NOT NULL,
	[vav_letterscore] [varchar](255) NULL,
	[vav_rank] [int] NULL
) ON [PRIMARY]
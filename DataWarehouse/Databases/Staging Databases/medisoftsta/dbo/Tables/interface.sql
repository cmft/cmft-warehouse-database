﻿CREATE TABLE [dbo].[interface](
	[int_id] [uniqueidentifier] NOT NULL,
	[int_setting] [varchar](255) NULL,
	[int_desc] [varchar](255) NULL,
	[timestamp] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[applicationstring](
	[ast_id] [uniqueidentifier] NOT NULL,
	[ast_number] [int] NOT NULL,
	[ast_string] [varchar](8000) NOT NULL,
	[ast_lan_id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
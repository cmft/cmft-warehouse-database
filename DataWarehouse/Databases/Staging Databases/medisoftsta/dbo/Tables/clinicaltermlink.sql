﻿CREATE TABLE [dbo].[clinicaltermlink](
	[ctl_id] [uniqueidentifier] NOT NULL,
	[ctl_ctm_id] [uniqueidentifier] NOT NULL,
	[ctl_pro_id] [uniqueidentifier] NOT NULL,
	[ctl_rft_id_link_type] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
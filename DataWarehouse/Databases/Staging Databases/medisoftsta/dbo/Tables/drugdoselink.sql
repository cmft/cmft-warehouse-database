﻿CREATE TABLE [dbo].[drugdoselink](
	[dol_id] [uniqueidentifier] NOT NULL,
	[dol_dru_id] [uniqueidentifier] NULL,
	[dol_dos_id] [uniqueidentifier] NULL,
	[dol_order] [int] NOT NULL,
	[dol_available] [bit] NOT NULL,
	[timestamp] [binary](8) NULL
) ON [PRIMARY]
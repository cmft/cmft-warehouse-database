﻿CREATE TABLE [dbo].[outputoptionsenable](
	[ooe_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputoptionsenable_ooe_id]  DEFAULT (newid()),
	[ooe_out_id] [uniqueidentifier] NULL,
	[ooe_optionenable] [varchar](255) NULL
) ON [PRIMARY]
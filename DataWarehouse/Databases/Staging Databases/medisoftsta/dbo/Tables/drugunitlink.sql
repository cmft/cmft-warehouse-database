﻿CREATE TABLE [dbo].[drugunitlink](
	[dnl_id] [uniqueidentifier] NOT NULL,
	[dnl_dru_id] [uniqueidentifier] NULL,
	[dnl_drn_id] [uniqueidentifier] NULL,
	[dnl_order] [int] NOT NULL,
	[dnl_available] [bit] NOT NULL
) ON [PRIMARY]
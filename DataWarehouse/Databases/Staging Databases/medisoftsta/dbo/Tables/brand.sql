﻿CREATE TABLE [dbo].[brand](
	[bra_id] [uniqueidentifier] NOT NULL,
	[bra_mode] [int] NOT NULL,
	[bra_caption] [varchar](255) NOT NULL,
	[bra_helptitle] [varchar](255) NOT NULL,
	[bra_splashimage] [varchar](255) NOT NULL
) ON [PRIMARY]
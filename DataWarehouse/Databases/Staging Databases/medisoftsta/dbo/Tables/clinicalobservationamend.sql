﻿CREATE TABLE [dbo].[clinicalobservationamend](
	[ctn_id] [uniqueidentifier] NOT NULL,
	[ctn_ctm_id] [uniqueidentifier] NOT NULL,
	[ctn_replace] [varchar](255) NULL,
	[ctn_cfd_id_qualifier] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
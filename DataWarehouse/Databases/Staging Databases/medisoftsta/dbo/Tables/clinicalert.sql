﻿CREATE TABLE [dbo].[clinicalert](
	[cal_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__clinicale__cal_i__3C74E891]  DEFAULT (newid()),
	[cal_description] [varchar](100) NULL,
	[cal_message] [varchar](2000) NOT NULL CONSTRAINT [DF__clinicale__cal_m__3D690CCA]  DEFAULT (''),
	[cal_grade] [tinyint] NOT NULL CONSTRAINT [DF__clinicale__cal_g__3E5D3103]  DEFAULT (0),
	[cal_freetext] [text] NULL,
	[cal_enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[genericentity](
	[gen_id] [int] IDENTITY(1,1) NOT NULL,
	[gen_code] [varchar](255) NOT NULL,
	[gen_descshort] [varchar](255) NOT NULL,
	[gen_desclong] [varchar](1000) NULL,
	[gen_descextra] [varchar](255) NULL,
	[gen_number] [int] NULL,
	[gen_order] [int] NOT NULL,
	[gen_available] [bit] NULL,
	[gen_gnt_id] [int] NOT NULL,
	[gen_highlight_answer] [int] NULL,
	[gen_interfacelookup1] [int] NULL,
	[gen_interfacelookup2] [int] NULL,
	[gen_membershipnumber] [int] NULL,
	[gen_audit_userid] [int] NOT NULL,
	[gen_audit_date] [datetime] NOT NULL
) ON [PRIMARY]
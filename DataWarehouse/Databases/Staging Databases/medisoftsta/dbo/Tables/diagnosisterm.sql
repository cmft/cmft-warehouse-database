﻿CREATE TABLE [dbo].[diagnosisterm](
	[dgt_id] [uniqueidentifier] NOT NULL,
	[dgt_lan_id] [uniqueidentifier] NULL,
	[dgt_desc] [varchar](255) NULL
) ON [PRIMARY]
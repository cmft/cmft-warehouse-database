﻿CREATE TABLE [dbo].[referencecategory](
	[ryc_id] [uniqueidentifier] NULL CONSTRAINT [DF_referencecategory_ryc_id]  DEFAULT (newid()),
	[ryc_no] [int] NULL,
	[ryc_desc] [varchar](50) NULL,
	[ryc_image] [tinyint] NULL
) ON [PRIMARY]
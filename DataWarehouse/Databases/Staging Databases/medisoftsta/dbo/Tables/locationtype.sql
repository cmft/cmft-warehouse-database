﻿CREATE TABLE [dbo].[locationtype](
	[lot_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[lot_desc] [varchar](255) NULL,
	[lot_code] [varchar](4) NULL,
	[lot_explicit] [bit] NULL
) ON [PRIMARY]
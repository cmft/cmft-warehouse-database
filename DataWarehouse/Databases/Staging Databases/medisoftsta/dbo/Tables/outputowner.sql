﻿CREATE TABLE [dbo].[outputowner](
	[oco_id] [uniqueidentifier] NULL,
	[oco_ownercode] [varchar](50) NULL,
	[oco_ownermemberno] [int] NULL
) ON [PRIMARY]
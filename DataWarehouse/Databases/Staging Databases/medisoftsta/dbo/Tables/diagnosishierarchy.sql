﻿CREATE TABLE [dbo].[diagnosishierarchy](
	[dhi_id] [uniqueidentifier] NOT NULL,
	[dhi_dgn_id] [uniqueidentifier] NULL,
	[dhi_dct_id] [uniqueidentifier] NULL,
	[dhi_dgt_id] [uniqueidentifier] NULL,
	[dhi_desctemp] [varchar](255) NULL,
	[dhi_order] [tinyint] NULL,
	[dhi_parent_id] [uniqueidentifier] NULL,
	[dhi_menu_no] [int] IDENTITY(100,1) NOT NULL,
	[dhi_parent_menu_no] [int] NULL,
	[dhi_child_no] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[interfacemappings](
	[inm_id] [uniqueidentifier] NOT NULL,
	[inm_col_name] [varchar](50) NULL,
	[inm_col_number] [int] NULL,
	[inm_property] [varchar](50) NULL,
	[inm_object_id] [int] NULL
) ON [PRIMARY]
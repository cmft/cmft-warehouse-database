﻿CREATE TABLE [dbo].[diabeticassesmentgrades](
	[dig_id] [uniqueidentifier] NOT NULL,
	[dig_rft_id] [uniqueidentifier] NULL,
	[dig_rft_desc] [varchar](255) NULL,
	[dig_gradeletternsc] [varchar](10) NULL,
	[dig_gradenumbernsc] [int] NULL,
	[dig_dgc_id_international] [uniqueidentifier] NULL,
	[dig_dgc_id_etdrs] [uniqueidentifier] NULL,
	[dig_gradenumberetdrs] [int] NULL,
	[dig_code] [varchar](255) NULL,
	[dig_rfy_desc] [varchar](255) NULL,
	[dig_rulemember] [int] NULL,
	[dig_dgn_id] [uniqueidentifier] NULL,
	[dig_mode] [int] NULL
) ON [PRIMARY]
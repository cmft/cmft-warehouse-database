﻿CREATE TABLE [dbo].[whoscreens](
	[whs_id] [int] IDENTITY(1,1) NOT NULL,
	[whs_desc] [varchar](255) NULL,
	[whs_order] [int] NULL,
	[whs_pages] [int] NULL,
	[whs_active] [bit] NULL
) ON [PRIMARY]
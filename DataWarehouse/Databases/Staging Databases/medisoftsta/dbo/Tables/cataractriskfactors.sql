﻿CREATE TABLE [dbo].[cataractriskfactors](
	[crf_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[crf_name] [varchar](255) NULL,
	[crf_factorno] [int] NULL,
	[crf_factorrange1] [varchar](10) NULL,
	[crf_factorrange2] [varchar](10) NULL,
	[crf_ratio] [decimal](5, 2) NULL
) ON [PRIMARY]
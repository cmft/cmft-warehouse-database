﻿CREATE TABLE [dbo].[luminousmapper](
	[lmp_id] [uniqueidentifier] NOT NULL,
	[lmp_medisoft_id] [uniqueidentifier] NOT NULL,
	[lmp_medisoft_type] [int] NULL,
	[lmp_luminous] [varchar](50) NULL,
	[lmp_luminous_index] [int] NULL,
	[lmp_eye_id] [uniqueidentifier] NULL,
	[lmp_medisoft_answer] [int] NULL DEFAULT ((2))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[proceduretypelink](
	[ptl_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__procedure__ptl_i__50DD6A9F]  DEFAULT (newid()),
	[ptl_pro_id] [uniqueidentifier] NULL,
	[ptl_prt_id] [uniqueidentifier] NULL
) ON [PRIMARY]
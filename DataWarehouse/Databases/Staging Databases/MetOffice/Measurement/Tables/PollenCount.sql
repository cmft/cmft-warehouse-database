﻿CREATE TABLE [Measurement].[PollenCount](
	[Date] [date] NOT NULL,
	[Cory] [int] NOT NULL,
	[Alnu] [int] NOT NULL,
	[Sali] [int] NOT NULL,
	[Betu] [int] NOT NULL,
	[Frax] [int] NOT NULL,
	[Ulmu] [int] NOT NULL,
	[Quer] [int] NOT NULL,
	[Plat] [int] NOT NULL,
	[Poac] [int] NOT NULL,
	[Urti] [int] NOT NULL,
	[Arte] [int] NOT NULL,
	[Ambr] [int] NOT NULL,
	[Counter Name] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Measurement].[PollenTypeNameTranslation](
	[LatinNameAbbr] [char](4) NOT NULL,
	[LatinName] [varchar](40) NOT NULL,
	[CommonName] [varchar](40) NOT NULL
) ON [PRIMARY]
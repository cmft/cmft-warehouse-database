﻿CREATE TABLE [dbo].[PatientAlternateIDHISTORY](
	[PatientID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[StaffID] [int] NULL,
	[AlternatePatientID] [nvarchar](50) NULL,
	[ListPatientIDType] [int] NULL
) ON [PRIMARY]
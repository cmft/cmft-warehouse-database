﻿CREATE TABLE [dbo].[GP_PracticeHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Practice_Name] [nvarchar](256) NULL,
	[Practice_Code] [nvarchar](6) NULL,
	[Link_Address_ID] [uniqueidentifier] NULL
) ON [PRIMARY]
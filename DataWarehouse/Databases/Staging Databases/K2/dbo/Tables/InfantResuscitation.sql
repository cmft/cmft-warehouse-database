﻿CREATE TABLE [dbo].[InfantResuscitation](
	[BabyNumber] [int] NOT NULL,
	[ListResuscitationID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
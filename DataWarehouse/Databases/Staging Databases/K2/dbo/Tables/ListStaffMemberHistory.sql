﻿CREATE TABLE [dbo].[ListStaffMemberHistory](
	[StaffId] [int] NOT NULL,
	[StaffIDType] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[SurName] [nvarchar](50) NULL,
	[Password] [varchar](20) NULL,
	[DataAccessLevel] [int] NULL,
	[FingerPrint] [varbinary](7800) NULL,
	[AuthenticatingStaffID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[StaffType] [int] NULL,
	[Identifier] [nvarchar](50) NULL,
	[Initials] [varchar](4) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientMerge](
	[DiscardedPatientID] [uniqueidentifier] NOT NULL,
	[RetainedPatientID] [uniqueidentifier] NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL
) ON [PRIMARY]
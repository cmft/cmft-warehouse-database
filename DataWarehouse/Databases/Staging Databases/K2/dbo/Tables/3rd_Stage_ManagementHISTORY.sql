﻿CREATE TABLE [dbo].[3rd_Stage_ManagementHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Chosen_Management_Type] [int] NULL,
	[Physiological_Management_Actions] [nvarchar](100) NULL,
	[Change_to_Active_Management] [bit] NULL,
	[Reason_for_Change_to_Active_Management] [nvarchar](100) NULL,
	[Consent_for_Change_Given] [bit] NULL,
	[Time_of_Change_to_Active_Management] [datetime] NULL,
	[Active_Management_Actions] [nvarchar](100) NULL,
	[Time_cord_clamped_and_cut] [datetime] NULL,
	[Milking_of_the_blood_from_cord] [int] NULL,
	[Cord_Clamping_Immediate] [bit] NULL,
	[Consent_Obtained] [int] NULL,
	[Management_Comments] [nvarchar](300) NULL,
	[Other_Reason_for_Change_to_Active_Management] [nvarchar](100) NULL,
	[Risk_Assessment_Carried_Out] [bit] NULL
) ON [PRIMARY]
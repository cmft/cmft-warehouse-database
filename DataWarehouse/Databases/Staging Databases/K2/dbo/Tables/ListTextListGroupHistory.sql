﻿CREATE TABLE [dbo].[ListTextListGroupHistory](
	[Type] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[Group] [varchar](100) NULL,
	[ParentGroup] [int] NOT NULL,
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NULL
) ON [PRIMARY]
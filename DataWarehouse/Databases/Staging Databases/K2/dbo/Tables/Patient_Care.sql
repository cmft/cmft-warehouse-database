﻿CREATE TABLE [dbo].[Patient_Care](
	[Assigned_Midwife] [nvarchar](100) NULL,
	[Assigned_Midwife_ID] [int] NULL,
	[Assigned_Consultant] [nvarchar](100) NULL,
	[Assigned_Consultant_ID] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurrence] [datetime] NULL
) ON [PRIMARY]
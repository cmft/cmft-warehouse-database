﻿CREATE TABLE [dbo].[InfantBreechDelivery](
	[BabyNumber] [int] NOT NULL,
	[ListBreechDeliveryTypeID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientConditionsHistory](
	[ID] [int] NOT NULL,
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[CaseNumber] [uniqueidentifier] NULL,
	[ObstetricCondition] [int] NULL,
	[MaternalCondition] [int] NULL,
	[ConditionCount] [int] NULL,
	[RiskAndConfidentiality] [int] NULL,
	[Confidential] [int] NULL,
	[Risk] [int] NULL
) ON [PRIMARY]
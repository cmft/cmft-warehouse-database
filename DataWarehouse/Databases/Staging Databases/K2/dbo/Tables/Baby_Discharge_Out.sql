﻿CREATE TABLE [dbo].[Baby_Discharge_Out](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_Infant_Delivery_ID] [uniqueidentifier] NULL,
	[Document_Contents] [nvarchar](4000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Guardian_Software_UpdatesHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Version_Number] [nvarchar](20) NULL,
	[Software] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
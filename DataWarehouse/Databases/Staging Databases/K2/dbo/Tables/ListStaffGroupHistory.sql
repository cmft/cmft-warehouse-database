﻿CREATE TABLE [dbo].[ListStaffGroupHistory](
	[ID] [int] NOT NULL,
	[GroupName] [nvarchar](50) NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffId] [int] NOT NULL,
	[Site_ID] [int] NULL
) ON [PRIMARY]
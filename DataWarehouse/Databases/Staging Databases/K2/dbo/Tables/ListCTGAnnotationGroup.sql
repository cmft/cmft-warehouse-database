﻿CREATE TABLE [dbo].[ListCTGAnnotationGroup](
	[ID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL
) ON [PRIMARY]
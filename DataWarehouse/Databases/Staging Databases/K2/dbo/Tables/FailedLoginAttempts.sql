﻿CREATE TABLE [dbo].[FailedLoginAttempts](
	[LoginEntered] [nvarchar](100) NULL,
	[TimeEntered] [datetime] NULL,
	[FailedAttempts] [int] NULL
) ON [PRIMARY]
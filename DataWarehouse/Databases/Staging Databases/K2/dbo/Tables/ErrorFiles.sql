﻿CREATE TABLE [dbo].[ErrorFiles](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Location] [int] NULL,
	[File] [nvarchar](max) NULL,
	[Computer_Name] [nvarchar](50) NULL,
	[MAC_Address] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
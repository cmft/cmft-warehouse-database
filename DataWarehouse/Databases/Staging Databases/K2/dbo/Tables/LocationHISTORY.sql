﻿CREATE TABLE [dbo].[LocationHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[MAC_Address] [nvarchar](20) NULL,
	[Computer_Name] [nvarchar](50) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CMiS_LabourOutcome_OutHISTORY](
	[PregnancyID] [uniqueidentifier] NULL,
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[PatientID] [uniqueidentifier] NULL
) ON [PRIMARY]
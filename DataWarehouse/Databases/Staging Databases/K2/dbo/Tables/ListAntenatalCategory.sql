﻿CREATE TABLE [dbo].[ListAntenatalCategory](
	[ID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[MaternalCondition] [int] NULL
) ON [PRIMARY]
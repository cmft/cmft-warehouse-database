﻿CREATE TABLE [dbo].[BinaryEventData](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[BLOB] [varbinary](8000) NULL
) ON [PRIMARY]
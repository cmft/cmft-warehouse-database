﻿CREATE TABLE [dbo].[ListTextListItem](
	[Type] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[Entry] [varchar](1000) NULL,
	[ParentGroup] [int] NOT NULL,
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NULL
) ON [PRIMARY]
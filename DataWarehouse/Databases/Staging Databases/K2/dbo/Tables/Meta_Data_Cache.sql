﻿CREATE TABLE [dbo].[Meta_Data_Cache](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Meta_Data] [nvarchar](max) NULL,
	[VersionNo] [int] NULL,
	[DatePublished] [datetime] NULL,
	[PublishNotes] [nvarchar](max) NULL,
	[PublishedBy] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
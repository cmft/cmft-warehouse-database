﻿CREATE TABLE [dbo].[Pain_Score](
	[pain_score] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[DataTransaction_TimeOfOccurence] [datetime] NULL
) ON [PRIMARY]
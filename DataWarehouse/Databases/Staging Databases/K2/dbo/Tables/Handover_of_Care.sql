﻿CREATE TABLE [dbo].[Handover_of_Care](
	[Background] [nvarchar](500) NULL,
	[Maternal_Assessment] [nvarchar](500) NULL,
	[Fetal_Assessment] [nvarchar](500) NULL,
	[Recommendation] [nvarchar](500) NULL,
	[Currently_Assigned_Midwife] [nvarchar](100) NULL,
	[Outstanding_Actions] [nvarchar](500) NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurrence] [datetime] NULL,
	[Additional_Assessments] [nvarchar](100) NULL,
	[Currently_Assigned_Midwife_ID] [int] NULL,
	[Neonatal_Resuscitaire_Checked] [int] NULL,
	[Checklist_Completed] [int] NULL,
	[Other_Pumps_Checked] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[GenericFloatParameter](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurrence] [datetime] NOT NULL,
	[ParameterValue] [float] NOT NULL,
	[ParameterType] [int] NOT NULL
) ON [PRIMARY]
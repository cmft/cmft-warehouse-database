﻿CREATE TABLE [dbo].[Shoulder_DystociaHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Scribe] [nvarchar](100) NULL,
	[Scribe_ID] [int] NULL,
	[Delivery_of_Head] [int] NULL,
	[Snr_Midwife_called] [datetime] NULL,
	[Snr_Midwife_arrived] [datetime] NULL,
	[Registrar_called] [datetime] NULL,
	[Registrar_arrived] [datetime] NULL,
	[Ob_Consultant_called] [datetime] NULL,
	[Ob_Consultant_arrived] [datetime] NULL,
	[Paediatrician_called] [datetime] NULL,
	[Paedatrician_arrived] [datetime] NULL,
	[Anaesthetist_called] [datetime] NULL,
	[Anaesthetist_arrived] [datetime] NULL,
	[Anae_Nurse_called] [datetime] NULL,
	[Anae_Nurse_arrived] [datetime] NULL,
	[Other_staff_present] [nvarchar](500) NULL,
	[Snr_Midwife_help] [bit] NULL,
	[Registrar_help] [bit] NULL,
	[Ob_Consultant_help] [bit] NULL,
	[Paedatrician_help] [bit] NULL,
	[Anaesthetist_help] [bit] NULL,
	[Anae_Nurse_help] [bit] NULL,
	[Link_Infant_Delivery_ID] [uniqueidentifier] NULL,
	[Evaluate_for_episiotomy] [bit] NULL,
	[Eval_for_Epis_detail] [nvarchar](250) NULL,
	[Eval_for_epis_time] [datetime] NULL,
	[Eval_for_epis_staff] [nvarchar](100) NULL,
	[Eval_for_epis_order] [int] NULL,
	[epis_staff_ID] [int] NULL,
	[Legs_McRoberts] [bit] NULL,
	[Legs_McRoberts_detail] [nvarchar](500) NULL,
	[Legs_McRoberts_time] [datetime] NULL,
	[Legs_McRoberts_staff] [nvarchar](100) NULL,
	[Legs_McRoberts_staff_id] [int] NULL,
	[Legs_McRoberts_order] [int] NULL,
	[Suprapubic_Pressure] [bit] NULL,
	[Suprapubic_Pressure_detail] [nvarchar](250) NULL,
	[Suprapubic_Pressure_time] [datetime] NULL,
	[Suprapubic_Pressure_staff] [nvarchar](100) NULL,
	[Suprapubic_Pressure_staff_id] [int] NULL,
	[Suprapubic_Pressure_order] [int] NULL,
	[Enter] [bit] NULL,
	[Enter_detail] [nvarchar](250) NULL,
	[Enter_time] [datetime] NULL,
	[Enter_staff] [nvarchar](100) NULL,
	[Enter_staff_id] [int] NULL,
	[Enter_order] [int] NULL,
	[Remove_Posterior_Arm] [bit] NULL,
	[Remove_Posterior_Arm_detail] [nvarchar](250) NULL,
	[Remove_Posterior_Arm_time] [datetime] NULL,
	[Remove_Posterior_Arm_staff] [nvarchar](100) NULL,
	[Remove_Posterior_Arm_staff_id] [int] NULL,
	[Remove_Posterior_Arm_order] [int] NULL,
	[Rotate_on_All_Fours] [bit] NULL,
	[Rotate_on_All_Fours_detail] [nvarchar](250) NULL,
	[Rotate_on_All_Fours_time] [datetime] NULL,
	[Rotate_on_All_Fours_staff] [nvarchar](100) NULL,
	[Rotate_on_All_Fours_staff_id] [int] NULL,
	[Rotate_on_All_Fours_order] [int] NULL,
	[Time_of_Delivery_of_Head] [datetime] NULL,
	[At_delivery_baby_head_was_facing] [int] NULL,
	[Fetal_injury] [int] NULL,
	[Fetal_injury_detail] [nvarchar](250) NULL,
	[Were_other_staff_present] [bit] NULL,
	[Sdel_1st_attempt_time] [datetime] NULL,
	[Sdel_1st_attempt_staff] [nvarchar](99) NULL,
	[Sdel_1st_attempt_staffID] [int] NULL,
	[Sdel_2nd_attempt_time] [datetime] NULL,
	[Sdel_2nd_attempt_staff] [nvarchar](99) NULL,
	[Sdel_2nd_attempt_staffID] [int] NULL,
	[Shoulder_Delivery_attempts] [int] NULL,
	[Shoulder_Dystocia_type] [int] NULL,
	[Rubin_manoeuvre] [bit] NULL,
	[Rubin_manoeuvre_time] [datetime] NULL,
	[Wood_Screw] [bit] NULL,
	[Wood_Screw_time] [datetime] NULL,
	[Free_Text] [nvarchar](500) NULL,
	[Brachial_Plexus_Injury] [int] NULL,
	[Paediatrician_informed] [int] NULL,
	[Scribe_Sheet_Completed_and_Filed] [bit] NULL,
	[Predisposing_Factors] [int] NULL,
	[Systematic_Emergency_Management] [int] NULL,
	[Record_Keeping_Requirements] [int] NULL,
	[RCOG_Reporting_Form] [int] NULL,
	[Brachial_Plexus_Follow_Up] [int] NULL,
	[Paediatric_Assessment_at_Delivery] [nvarchar](500) NULL,
	[Time_of_Delivery_of_Body] [datetime] NULL,
	[Calls_for_Help] [bit] NULL,
	[Reasons_for_No_Calls] [nvarchar](500) NULL,
	[Scribe_Grade] [int] NULL,
	[Eval_for_Episiotomy_Grade] [int] NULL,
	[Legs_McRoberts_Staff_Grade] [int] NULL,
	[Suprapubic_Pressure_Staff_Grade] [int] NULL,
	[Enter_Staff_Grade] [int] NULL,
	[Remove_Posterior_Arm_Staff_Grade] [int] NULL,
	[Rotate_on_All_Fours_Staff_Grade] [int] NULL,
	[Location_of_Delivery] [nvarchar](200) NULL,
	[Obstetric_SHO_help] [bit] NULL,
	[Obstetric_SHO_called] [datetime] NULL,
	[Obstetric_SHO_arrived] [datetime] NULL,
	[Coord_Midwife_name] [nvarchar](100) NULL,
	[Coord_Midwife_ID] [int] NULL,
	[Registrar_name] [nvarchar](100) NULL,
	[Registrar_ID] [int] NULL,
	[Obstetric_SHO_name] [nvarchar](100) NULL,
	[Obstetric_SHO_ID] [int] NULL,
	[Anae_Nurse_name] [nvarchar](100) NULL,
	[Anae_Nurse_ID] [int] NULL,
	[Anaesthetist_name] [nvarchar](100) NULL,
	[Anaesthetist_ID] [int] NULL,
	[Ob_Consultant_name] [nvarchar](100) NULL,
	[Ob_Consultant_ID] [int] NULL,
	[Rubins_II_order] [int] NULL,
	[Rubins_II_detail] [nvarchar](250) NULL,
	[Rubins_II_name] [nvarchar](100) NULL,
	[Rubins_II_id] [int] NULL,
	[Wood_Screw_order] [int] NULL,
	[Wood_Screw_detail] [nvarchar](250) NULL,
	[Wood_Screw_name] [nvarchar](100) NULL,
	[Wood_Screw_id] [int] NULL,
	[Reverse_Wood_Screw] [bit] NULL,
	[Reverse_Wood_Screw_time] [datetime] NULL,
	[Reverse_Wood_Screw_order] [int] NULL,
	[Reverse_Wood_Screw_detail] [nvarchar](250) NULL,
	[Reverse_Wood_Screw_name] [nvarchar](100) NULL,
	[Reverse_Wood_Screw_id] [int] NULL,
	[Zavaelli] [bit] NULL,
	[Zavanelli_time] [datetime] NULL,
	[Zavanelli_order] [int] NULL,
	[Zavaelli_detail] [nvarchar](250) NULL,
	[Zavanelli_name] [nvarchar](100) NULL,
	[Zavanelli_id] [int] NULL,
	[Rubins_II_grade] [int] NULL,
	[Wood_Screw_grade] [int] NULL,
	[Reverse_Wood_Screw_grade] [int] NULL,
	[Zavaelli_grade] [int] NULL,
	[Emergency_Call_Made] [bit] NULL,
	[Emergency_Call_Time] [datetime] NULL,
	[Fracture_Clavicle] [bit] NULL,
	[Fracture_Clavicle_time] [datetime] NULL,
	[Fracture_Clavicle_order] [int] NULL,
	[Fracture_Clavicle_detail] [nvarchar](500) NULL,
	[Fracture_Clavicle_name] [nvarchar](100) NULL,
	[Fracture_Clavicle_id] [int] NULL,
	[Fracture_Clavicle_grade] [int] NULL,
	[Paediatrician_name] [nvarchar](100) NULL,
	[Paediatrician_ID] [int] NULL,
	[Symphysiotomy] [bit] NULL,
	[Symphysiotomy_time] [datetime] NULL,
	[Symphysiotomy_order] [int] NULL,
	[Symphysiotomy_detail] [nvarchar](500) NULL,
	[Symphysiotomy_name] [nvarchar](100) NULL,
	[Symphysiotomy_id] [int] NULL,
	[Symphysiotomy_grade] [int] NULL,
	[Senior_registrar] [bit] NULL,
	[Senior_reg_called] [datetime] NULL,
	[Senior_reg_arrived] [datetime] NULL,
	[Senior_reg_name] [nvarchar](100) NULL,
	[senior_reg_id] [int] NULL,
	[Reason_Not_Present] [nvarchar](200) NULL,
	[Routine_Traction] [bit] NULL,
	[Routine_Traction_details] [nvarchar](500) NULL,
	[Routine_Traction_time] [datetime] NULL,
	[Routine_Traction_staff] [nvarchar](50) NULL,
	[Routine_Traction_staff_ID] [int] NULL,
	[Routine_Traction_order] [int] NULL,
	[Routine_Traction_grade] [int] NULL,
	[Remove_posterior_arm_which_arm] [int] NULL,
	[Suprapubic_Pressure_from_maternal] [int] NULL,
	[Routine_Traction_from_maternal] [int] NULL,
	[Arm_Weakness] [int] NULL,
	[Potential_Fracture] [int] NULL,
	[Review_and_Follow_Up_Cons_Neonat] [int] NULL,
	[Reason_for_No_Review_or_Follow_Up] [nvarchar](500) NULL,
	[Repeated_Manoeuvre_1] [bit] NULL,
	[Repeated_Manoeuvre_1_Date_Time] [datetime] NULL,
	[Repeated_Manoeuvre_1_Type] [int] NULL,
	[Repeated_Manoeuvre_1_Order] [int] NULL,
	[Repeated_Manoeuvre_1_Staff_Name] [nvarchar](100) NULL,
	[Repeated_Manoeuvre_1_Staff_ID] [int] NULL,
	[Repeated_Manoeuvre_2] [bit] NULL,
	[Repeated_Manoeuvre_2_Date_Time] [datetime] NULL,
	[Repeated_Manoeuvre_2_Type] [int] NULL,
	[Repeated_Manoeuvre_2_Order] [int] NULL,
	[Repeated_Manoeuvre_2_Staff_Name] [nvarchar](100) NULL,
	[Repeated_Manoeuvre_2_Staff_ID] [int] NULL,
	[Repeated_Manoeuvre_3] [bit] NULL,
	[Repeated_Manoeuvre_3_Date_Time] [datetime] NULL,
	[Repeated_Manoeuvre_3_Type] [int] NULL,
	[Repeated_Manoeuvre_3_Order] [int] NULL,
	[Repeated_Manoeuvre_3_Staff_Name] [nvarchar](100) NULL,
	[Repeated_Manoeuvre_3_Staff_ID] [int] NULL,
	[Repeated_Manoeuvre_4] [bit] NULL,
	[Repeated_Manoeuvre_4_Date_Time] [datetime] NULL,
	[Repeated_Manoeuvre_4_Type] [int] NULL,
	[Repeated_Manoeuvre_4_Order] [int] NULL,
	[Repeated_Manoeuvre_4_Staff_Name] [nvarchar](100) NULL,
	[Repeated_Manoeuvre_4_Staff_ID] [int] NULL,
	[Repeated_Manoeuvre_5] [bit] NULL,
	[Repeated_Manoeuvre_5_Date_Time] [datetime] NULL,
	[Repeated_Manoeuvre_5_Type] [int] NULL,
	[Repeated_Manoeuvre_5_Order] [int] NULL,
	[Repeated_Manoeuvre_5_Staff_Name] [nvarchar](100) NULL,
	[Repeated_Manoeuvre_5_Staff_ID] [int] NULL,
	[Repeated_Manoeuvre_6] [bit] NULL,
	[Repeated_Manoeuvre_6_Date_Time] [datetime] NULL,
	[Repeated_Manoeuvre_6_Type] [int] NULL,
	[Repeated_Manoeuvre_6_Order] [int] NULL,
	[Repeated_Manoeuvre_6_Staff_Name] [nvarchar](100) NULL,
	[Repeated_Manoeuvre_6_Staff_ID] [int] NULL,
	[Repeated_Manoeuvre_7] [bit] NULL,
	[Repeated_Manoeuvre_7_Date_Time] [datetime] NULL,
	[Repeated_Manoeuvre_7_Type] [int] NULL,
	[Repeated_Manoeuvre_7_Order] [int] NULL,
	[Repeated_Manoeuvre_7_Staff_Name] [nvarchar](100) NULL,
	[Repeated_Manoeuvre_7_Staff_ID] [int] NULL,
	[Repeated_Manoeuvre_8] [bit] NULL,
	[Repeated_Manoeuvre_8_Date_Time] [datetime] NULL,
	[Repeated_Manoeuvre_8_Type] [int] NULL,
	[Repeated_Manoeuvre_8_Order] [int] NULL,
	[Repeated_Manoeuvre_8_Staff_Name] [nvarchar](100) NULL,
	[Repeated_Manoeuvre_8_Staff_ID] [int] NULL,
	[Internal_Manoeuvres] [bit] NULL,
	[Internal_time] [datetime] NULL,
	[Internal_order] [int] NULL,
	[Internal_detail] [nvarchar](250) NULL,
	[Internal_name] [nvarchar](100) NULL,
	[Internal_id] [int] NULL,
	[Internal_grade] [int] NULL,
	[Delivery_Achieved_With_Current_Method] [int] NULL,
	[Baby_Admitted_to_NICU] [int] NULL,
	[Baby_Assessed_By] [nvarchar](100) NULL,
	[Baby_Assessed_By_ID] [int] NULL,
	[Baby_Assessed_By_Grade] [int] NULL,
	[Recumbent_Bed_Position] [bit] NULL
) ON [PRIMARY]
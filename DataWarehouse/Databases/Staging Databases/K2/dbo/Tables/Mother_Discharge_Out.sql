﻿CREATE TABLE [dbo].[Mother_Discharge_Out](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Document_Contents] [nvarchar](4000) NULL
) ON [PRIMARY]
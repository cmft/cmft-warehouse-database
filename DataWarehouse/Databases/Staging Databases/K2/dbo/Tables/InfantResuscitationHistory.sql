﻿CREATE TABLE [dbo].[InfantResuscitationHistory](
	[BabyNumber] [int] NOT NULL,
	[ListResuscitationID] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Report](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Report] [nvarchar](max) NULL,
	[IsCustom] [bit] NULL,
	[StaffID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient_Transfer](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Transfer_Date_Time] [datetime] NULL,
	[Transfer_Source] [int] NULL,
	[Transfer_Destination] [int] NULL
) ON [PRIMARY]
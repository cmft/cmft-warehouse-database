﻿CREATE TABLE [dbo].[ListAntenatalSubCategory](
	[ID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[ListAntenatalCategoryID] [int] NOT NULL
) ON [PRIMARY]
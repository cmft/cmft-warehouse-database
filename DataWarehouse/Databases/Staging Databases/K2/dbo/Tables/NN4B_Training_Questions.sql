﻿CREATE TABLE [dbo].[NN4B_Training_Questions](
	[ID] [uniqueidentifier] NULL,
	[QuestionID] [int] NULL,
	[QuestionNumber] [int] NULL,
	[Question] [nvarchar](4000) NULL
) ON [PRIMARY]
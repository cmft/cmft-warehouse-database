﻿CREATE TABLE [dbo].[List_User_Massive_Obstetric_Haemorrhage_Tranexamic_Acid_Dose](
	[ID] [int] NULL,
	[CodedValue] [nvarchar](20) NULL,
	[DisplayText] [nvarchar](100) NULL,
	[DescriptionText] [nvarchar](2000) NULL,
	[Group] [int] NULL,
	[ParentID] [int] NULL,
	[ImageID] [int] NULL,
	[OrderID] [int] NULL,
	[Active] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Early_Labour](
	[Contact_Commenced] [datetime] NULL,
	[Contact_Ceased] [datetime] NULL,
	[Plan_of_Care] [nvarchar](1000) NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurrence] [datetime] NULL
) ON [PRIMARY]
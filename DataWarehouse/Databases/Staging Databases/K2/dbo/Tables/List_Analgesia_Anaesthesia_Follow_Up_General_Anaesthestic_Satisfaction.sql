﻿CREATE TABLE [dbo].[List_Analgesia_Anaesthesia_Follow_Up_General_Anaesthestic_Satisfaction](
	[ID] [int] NULL,
	[DisplayText] [nvarchar](100) NULL,
	[DescriptionText] [nvarchar](2000) NULL,
	[Group] [int] NULL,
	[ParentID] [int] NULL,
	[ImageID] [int] NULL,
	[OrderID] [int] NULL,
	[Active] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AttachmentHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[FileName] [nvarchar](500) NULL,
	[FileContents] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
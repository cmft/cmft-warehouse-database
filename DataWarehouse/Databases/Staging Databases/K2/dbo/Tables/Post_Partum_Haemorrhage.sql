﻿CREATE TABLE [dbo].[Post_Partum_Haemorrhage](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Blood_Loss] [int] NULL,
	[Drugs_given] [nvarchar](100) NULL,
	[Surgical_treatment] [nvarchar](100) NULL,
	[Other_surgical_detail] [nvarchar](1000) NULL,
	[Blood_products] [nvarchar](100) NULL,
	[blood_units] [int] NULL,
	[FFP_units] [int] NULL,
	[Other_blood_detail] [nvarchar](100) NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[PPH_diagnosed] [int] NULL,
	[platelets_units] [int] NULL,
	[other_units] [int] NULL,
	[Shift_Coord_called] [bit] NULL,
	[Shift_Coord_called_at] [datetime] NULL,
	[Shift_Coord_arrived_at] [datetime] NULL,
	[Con_Obstetrician_called] [bit] NULL,
	[Con_Obstetrician_called_at] [datetime] NULL,
	[Con_Obstetrician_arrived_at] [datetime] NULL,
	[Con_Anaesthetist_called] [bit] NULL,
	[Con_Anaesthetist_called_at] [datetime] NULL,
	[Con_Anaesthetist_arrived_at] [datetime] NULL,
	[Con_Haematologist_called] [bit] NULL,
	[Con_Haematologist_called_at] [datetime] NULL,
	[Con_Haematologist_arrived_at] [datetime] NULL,
	[Blood_Bank_called] [bit] NULL,
	[Blood_Bank_called_at] [datetime] NULL,
	[Scribe_Sheet_Completed_and_Filed] [bit] NULL,
	[Drop_in_Haemoglobin] [bit] NULL,
	[Small_Blood_Loss_Risks] [nvarchar](100) NULL,
	[Other_Major_Obstetric_Haemorrhage_Criteria] [nvarchar](250) NULL,
	[Obstetrician_ST_12_Present] [bit] NULL,
	[Obstetrician_ST_37_Present] [bit] NULL,
	[Anaesthetist_ST_37_Present] [bit] NULL,
	[Third_Midwife_Present] [bit] NULL,
	[ODP_Present] [bit] NULL,
	[Blood_Runner_Nominated] [bit] NULL,
	[PPH_Measures_Taken] [nvarchar](100) NULL,
	[Blood_Testing] [nvarchar](100) NULL,
	[Hysterectomy_Dw_2nd_Consultant] [bit] NULL,
	[PPH_Place_of_Treatment] [int] NULL,
	[Cryoprecipitate_Units] [int] NULL,
	[Anaesthesia] [nvarchar](100) NULL,
	[Anaesthetist] [nvarchar](100) NULL,
	[Anaesthetist_ID] [int] NULL,
	[Second_Anaesthetist] [nvarchar](100) NULL,
	[Second_Anaesthetist_ID] [int] NULL
) ON [PRIMARY]
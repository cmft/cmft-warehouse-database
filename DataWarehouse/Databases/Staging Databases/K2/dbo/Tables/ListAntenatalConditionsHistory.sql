﻿CREATE TABLE [dbo].[ListAntenatalConditionsHistory](
	[IDCode] [varchar](10) NOT NULL,
	[ConditionName] [varchar](50) NOT NULL,
	[Confidential] [int] NULL,
	[Risk] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[Abbreviation] [varchar](10) NULL,
	[Obstetric] [int] NULL,
	[ID] [int] NOT NULL,
	[IDSource] [int] NULL
) ON [PRIMARY]
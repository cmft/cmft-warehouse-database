﻿CREATE TABLE [dbo].[CMiS_HL7_Message_OutHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[HL7Message] [nvarchar](2000) NULL
) ON [PRIMARY]
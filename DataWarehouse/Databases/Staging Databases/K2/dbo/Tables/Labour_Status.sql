﻿CREATE TABLE [dbo].[Labour_Status](
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[CaseNumber] [uniqueidentifier] NULL,
	[Cervical_Dilatation] [int] NULL,
	[Examination_Time] [datetime] NULL
) ON [PRIMARY]
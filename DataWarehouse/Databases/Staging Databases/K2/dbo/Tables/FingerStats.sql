﻿CREATE TABLE [dbo].[FingerStats](
	[StaffID] [int] NOT NULL,
	[TimeTaken] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
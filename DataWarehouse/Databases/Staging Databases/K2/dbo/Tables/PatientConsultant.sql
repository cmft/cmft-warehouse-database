﻿CREATE TABLE [dbo].[PatientConsultant](
	[TimeEntered] [datetime] NOT NULL,
	[PatientConsultantID] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[PatientConsultantName] [varchar](49) NULL
) ON [PRIMARY]
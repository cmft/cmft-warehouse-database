﻿CREATE TABLE [dbo].[NSTSessionAssignment](
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NULL,
	[CaseNumber] [uniqueidentifier] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
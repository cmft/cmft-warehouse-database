﻿CREATE TABLE [dbo].[Fluid_Balance](
	[Input_Oral_volume_taken] [float] NULL,
	[Input_Intravenous_volume_started] [float] NULL,
	[Ouput_Urine_volume_measured] [float] NULL,
	[Ouput_Vomit_volume_measured] [float] NULL,
	[Drain_Output] [float] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurence] [datetime] NULL,
	[Transfusion__volume_input] [float] NULL,
	[Blood_loss__volume] [float] NULL,
	[Intake_Comments] [nvarchar](1000) NULL,
	[Out_To_Toilet] [bit] NULL,
	[Total_Intake] [float] NULL,
	[Total_Output] [float] NULL,
	[Blood_Loss_Integer] [int] NULL,
	[Vaginal_Loss] [float] NULL,
	[Other_Fluid_Loss] [float] NULL
) ON [PRIMARY]
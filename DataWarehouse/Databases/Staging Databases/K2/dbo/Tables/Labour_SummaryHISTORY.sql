﻿CREATE TABLE [dbo].[Labour_SummaryHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Labour_Status] [int] NULL,
	[Labour_Onset] [int] NULL,
	[Start_of_Labour] [datetime] NULL,
	[Rupture_of_Membranes_Time] [datetime] NULL,
	[Time_of_Second_Stage] [datetime] NULL,
	[Time_Pushing_Commenced] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[BloodPressure](
	[TimeEntered] [datetime] NOT NULL,
	[Systolic] [int] NULL,
	[Diastolic] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[MAP] [int] NULL
) ON [PRIMARY]
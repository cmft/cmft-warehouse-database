﻿CREATE TABLE [dbo].[PDFDocuments](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Document] [varbinary](max) NULL,
	[Generation_Date] [datetime] NULL,
	[Sent_Date] [datetime] NULL,
	[CaseNumber] [uniqueidentifier] NULL,
	[Raised_Date] [datetime] NULL,
	[Session_Start_Time] [datetime] NULL,
	[Session_End_Time] [datetime] NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Hospital_ID] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
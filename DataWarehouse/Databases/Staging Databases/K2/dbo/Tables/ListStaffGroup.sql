﻿CREATE TABLE [dbo].[ListStaffGroup](
	[ID] [int] NOT NULL,
	[GroupName] [nvarchar](50) NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[Site_ID] [int] NULL
) ON [PRIMARY]
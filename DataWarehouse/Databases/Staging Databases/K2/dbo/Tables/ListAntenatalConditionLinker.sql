﻿CREATE TABLE [dbo].[ListAntenatalConditionLinker](
	[ListAntenatalSubCategoryID] [int] NOT NULL,
	[ListAntenatalConditionID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[ID] [int] NOT NULL
) ON [PRIMARY]
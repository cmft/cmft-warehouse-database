﻿CREATE TABLE [dbo].[User_Privileges](
	[ID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Privileges] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Hysterotomy_NotesHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Hysterotomy_indicators] [nvarchar](100) NULL,
	[other_indicator_detail] [nvarchar](1000) NULL,
	[Surgeon] [nvarchar](100) NULL,
	[Surgeon_ID] [int] NULL,
	[Surgical_Assistant] [nvarchar](100) NULL,
	[Surgical_Assitant_ID] [int] NULL,
	[Anaesthetist] [nvarchar](100) NULL,
	[Anaesthetist_ID] [int] NULL,
	[Second_Anaesthetist] [nvarchar](100) NULL,
	[Second_Anaesthetist_ID] [int] NULL,
	[Procedure_Start] [datetime] NULL,
	[Incision_type] [int] NULL,
	[Uterine_incision] [nvarchar](100) NULL,
	[Old_scar_removed] [bit] NULL,
	[Procedure_Anaesthesia] [nvarchar](100) NULL,
	[Regional_Anaesthesia] [int] NULL,
	[Regional_continued_from_Labour] [bit] NULL,
	[Difficulty_opening_abdomen] [bit] NULL,
	[Bladder_pushdown] [bit] NULL,
	[Bladder_pushdown_detail] [nvarchar](1000) NULL,
	[Difficulty_opening_abdomin_text] [nvarchar](1000) NULL,
	[Tubes_and_Ovaries_Normal] [int] NULL,
	[Tubes_and_Ovaries_abnormal_detail] [nvarchar](1000) NULL,
	[Tubal_Ligation] [bit] NULL,
	[Tubal_Ligation_detail] [nvarchar](1000) NULL,
	[Ovarian_Cystecomy] [bit] NULL,
	[Ovarian_Cystecomy_detail] [nvarchar](1000) NULL,
	[Other_Procedure] [bit] NULL,
	[Other_Procedure_detail] [nvarchar](max) NULL,
	[Delivery_problems] [nvarchar](100) NULL,
	[Other_delivery_difficulty_detail] [nvarchar](1000) NULL,
	[Wrigleys_forceps_used] [bit] NULL,
	[Cavity_checked] [bit] NULL,
	[Uterus_repair] [int] NULL,
	[Uterus_suture_material] [int] NULL,
	[Extra_sutures_needed] [bit] NULL,
	[Extra_sutures_details] [nvarchar](1000) NULL,
	[BLynch_sutures] [bit] NULL,
	[Pelvic_Peritoneum_repaired] [bit] NULL,
	[pelvic_perit_repair_material] [int] NULL,
	[Pelvic_drain] [bit] NULL,
	[Abdominal_Peritoneum_repaired] [bit] NULL,
	[abdom_perit_repair_material] [int] NULL,
	[Sheath_repaired_with] [int] NULL,
	[Sub_sheath_drain] [bit] NULL,
	[Fat_suturing_required] [bit] NULL,
	[Fat_suture_material] [int] NULL,
	[Skin_suture_method] [int] NULL,
	[Skin_suture_material] [int] NULL,
	[Surgicel_used] [bit] NULL,
	[Surgicel_location] [nvarchar](500) NULL,
	[Urine_clear_at_end] [bit] NULL,
	[swab_count_correct] [bit] NULL,
	[needle_count_correct] [bit] NULL,
	[num_sutures] [int] NULL,
	[PV_check] [bit] NULL,
	[PR_check] [bit] NULL,
	[Patient_care_plan] [nvarchar](100) NULL,
	[PN_analgesia] [nvarchar](100) NULL,
	[Procedure_comments] [nvarchar](max) NULL,
	[Advice_for_next_delivery] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
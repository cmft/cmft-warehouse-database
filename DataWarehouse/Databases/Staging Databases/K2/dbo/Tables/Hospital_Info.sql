﻿CREATE TABLE [dbo].[Hospital_Info](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Hospital_Name] [nvarchar](100) NULL,
	[License] [nvarchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[List_Combined_Spinal_Epidural_attempted_insertion_spaces](
	[ID] [int] NULL,
	[DisplayText] [nvarchar](100) NULL,
	[DescriptionText] [nvarchar](2000) NULL,
	[Group] [int] NULL,
	[ParentID] [int] NULL,
	[ImageID] [int] NULL,
	[OrderID] [int] NULL,
	[Active] [int] NULL,
	[CodedValue] [nvarchar](20) NULL
) ON [PRIMARY]
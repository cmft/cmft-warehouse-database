﻿CREATE TABLE [dbo].[SessionAssignmentHistory](
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[CaseNumber] [uniqueidentifier] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
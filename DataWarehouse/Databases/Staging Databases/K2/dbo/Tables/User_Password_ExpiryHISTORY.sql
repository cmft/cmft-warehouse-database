﻿CREATE TABLE [dbo].[User_Password_ExpiryHISTORY](
	[ID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[ExpiryPolicy] [int] NULL,
	[PasswordLastChanged] [datetime] NULL
) ON [PRIMARY]
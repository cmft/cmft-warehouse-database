﻿CREATE TABLE [dbo].[CMiS_LabourOutcome_Out](
	[PregnancyID] [uniqueidentifier] NULL,
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[PatientID] [uniqueidentifier] NULL
) ON [PRIMARY]
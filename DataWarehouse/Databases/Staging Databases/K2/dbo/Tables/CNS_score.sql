﻿CREATE TABLE [dbo].[CNS_score](
	[CNS_score] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[DataTransaction_TimeOfOccurence] [datetime] NULL,
	[Neurological_Response] [int] NULL
) ON [PRIMARY]
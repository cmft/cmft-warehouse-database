﻿CREATE TABLE [dbo].[PatientOutcomeHistory](
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[ListLabourOutcomeID] [int] NOT NULL,
	[ListMotherTransferredID] [int] NOT NULL,
	[NumInfantsID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[DateOfDecisionOperativeDelivery] [datetime] NULL
) ON [PRIMARY]
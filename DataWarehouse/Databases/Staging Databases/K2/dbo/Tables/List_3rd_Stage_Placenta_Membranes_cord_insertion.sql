﻿CREATE TABLE [dbo].[List_3rd_Stage_Placenta_Membranes_cord_insertion](
	[ID] [int] NULL,
	[DisplayText] [nvarchar](100) NULL,
	[DescriptionText] [nvarchar](2000) NULL,
	[Group] [int] NULL,
	[ParentID] [int] NULL,
	[ImageID] [int] NULL,
	[OrderID] [int] NULL,
	[Active] [int] NULL,
	[CodedValue] [nvarchar](20) NULL
) ON [PRIMARY]
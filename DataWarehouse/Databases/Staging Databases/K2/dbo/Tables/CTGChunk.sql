﻿CREATE TABLE [dbo].[CTGChunk](
	[BLOB] [varbinary](8000) NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StartTickCount] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Second_Stage_Obs](
	[Effective_Pushing] [bit] NULL,
	[Head_Descent] [bit] NULL,
	[Maternal_Position_Checked] [bit] NULL,
	[Bladder_Care_Checked] [bit] NULL,
	[Emotional_Needs_Considered] [bit] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurence] [datetime] NULL,
	[Additional_Notes] [nvarchar](1000) NULL
) ON [PRIMARY]
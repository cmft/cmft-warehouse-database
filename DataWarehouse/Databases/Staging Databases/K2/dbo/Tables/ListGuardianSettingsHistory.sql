﻿CREATE TABLE [dbo].[ListGuardianSettingsHistory](
	[ID] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[Settings] [varbinary](7984) NULL
) ON [PRIMARY]
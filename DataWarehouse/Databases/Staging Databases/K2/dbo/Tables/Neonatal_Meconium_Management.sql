﻿CREATE TABLE [dbo].[Neonatal_Meconium_Management](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_Infant_Delivery_ID] [uniqueidentifier] NULL,
	[Baby_Received_12hrs_of_Observations] [bit] NULL,
	[Signs_of_Respiratory_Distress] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DataTransaction](
	[TransactionID] [int] NOT NULL,
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TransType] [int] NULL,
	[TimeOfOccurence] [datetime] NULL,
	[SupercededID] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[AuthenticationMethod] [int] NULL
) ON [PRIMARY]
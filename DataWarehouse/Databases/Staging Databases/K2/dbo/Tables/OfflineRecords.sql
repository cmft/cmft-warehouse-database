﻿CREATE TABLE [dbo].[OfflineRecords](
	[ID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[DataRecords] [nvarchar](max) NULL,
	[PregnancyID] [uniqueidentifier] NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Surname] [nvarchar](256) NULL,
	[FirstName] [nvarchar](256) NULL,
	[DOB] [datetime] NULL,
	[HospitalPatientID] [nvarchar](50) NULL,
	[DisplayHospitalID] [nvarchar](50) NULL,
	[SiteID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Anaesthetic_Interventions_Summary](
	[Date_of_Summary] [datetime] NULL,
	[Intervention_Anaesthetist] [nvarchar](100) NULL,
	[Intervention_Anaesthetist_ID] [int] NULL,
	[Epidural_In_Situ] [int] NULL,
	[Epidural_In_Situ_Other_Details] [nvarchar](250) NULL,
	[Epidural_Used_for_Anaesthesia] [int] NULL,
	[Epidural_Used_Other_Detail] [nvarchar](250) NULL,
	[Reason_for_Epidural_Not_Used] [nvarchar](100) NULL,
	[Reason_Not_Used_Other_Detail] [nvarchar](250) NULL,
	[Comments_on_Use_for_Anaesthesia] [nvarchar](250) NULL,
	[Anaesthetic] [nvarchar](100) NULL,
	[Other_Anaesthetic_Detail] [nvarchar](250) NULL,
	[Complications] [int] NULL,
	[Complications_Other_Detail] [nvarchar](250) NULL,
	[Operation] [nvarchar](100) NULL,
	[Operation_Other_Detail] [nvarchar](250) NULL,
	[Summary_Comments] [nvarchar](250) NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Second_Anaesthetist] [nvarchar](250) NULL,
	[Second_Anaesthetist_ID] [int] NULL
) ON [PRIMARY]
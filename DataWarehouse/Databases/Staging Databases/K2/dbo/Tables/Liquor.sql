﻿CREATE TABLE [dbo].[Liquor](
	[TimeEntered] [datetime] NOT NULL,
	[ListPrimaryStatus] [int] NULL,
	[ListSecondaryStatus] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[Liquor_condition] [nvarchar](100) NULL
) ON [PRIMARY]
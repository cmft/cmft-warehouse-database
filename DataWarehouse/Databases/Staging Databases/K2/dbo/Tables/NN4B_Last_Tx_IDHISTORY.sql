﻿CREATE TABLE [dbo].[NN4B_Last_Tx_IDHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Latest_Tx_ID] [int] NULL
) ON [PRIMARY]
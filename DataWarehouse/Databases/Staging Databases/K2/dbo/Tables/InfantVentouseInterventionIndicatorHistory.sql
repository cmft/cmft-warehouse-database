﻿CREATE TABLE [dbo].[InfantVentouseInterventionIndicatorHistory](
	[BabyNumber] [int] NOT NULL,
	[ListVentouseInterventionIndicatorID] [int] NOT NULL,
	[IndicatorPriority] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
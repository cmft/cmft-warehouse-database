﻿CREATE TABLE [dbo].[Infant_Ventouse_Delivery](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Ventouse_Indicators] [nvarchar](100) NULL,
	[Link_Infant_Delivery_ID] [uniqueidentifier] NULL,
	[Other_Indicator_Details] [nvarchar](max) NULL,
	[Procedure_Start] [datetime] NULL,
	[Time_of_decision_for_ventouse] [datetime] NULL,
	[Who_made_ventouse_decision] [nvarchar](max) NULL,
	[Who_made_ventouse_decision_ID] [int] NULL,
	[Ventouse_failed] [bit] NULL,
	[Reasons_for_failure] [nvarchar](100) NULL,
	[Time_stopped_ventouse] [datetime] NULL,
	[Ventouse_Indication] [int] NULL,
	[Decision_Maker_Grade] [int] NULL,
	[Reason_target_time_exceeded] [nvarchar](500) NULL,
	[Risks_Explained] [bit] NULL,
	[Nenoatal_Staff_Informed] [bit] NULL,
	[Doctor_Present] [bit] NULL,
	[Incident_Form_Completed] [int] NULL,
	[Discussed_with_Consultant] [bit] NULL,
	[Reason_not_discussed_with_consultant] [nvarchar](1000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
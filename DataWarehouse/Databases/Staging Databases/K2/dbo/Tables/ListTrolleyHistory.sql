﻿CREATE TABLE [dbo].[ListTrolleyHistory](
	[ID] [int] NOT NULL,
	[Description] [varchar](19) NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[Site_ID] [int] NULL
) ON [PRIMARY]
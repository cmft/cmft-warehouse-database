﻿CREATE TABLE [dbo].[InfantVentouseDelivery](
	[BabyNumber] [int] NOT NULL,
	[ListVentouseTypeID] [int] NOT NULL,
	[ListVentouseRotationID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[ListVentouseCupID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientConditions](
	[ID] [int] NOT NULL,
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[ObstetricCondition] [int] NOT NULL,
	[MaternalCondition] [int] NULL,
	[ConditionCount] [int] NULL,
	[RiskAndConfidentiality] [int] NULL,
	[Confidential] [int] NULL,
	[Risk] [int] NULL
) ON [PRIMARY]
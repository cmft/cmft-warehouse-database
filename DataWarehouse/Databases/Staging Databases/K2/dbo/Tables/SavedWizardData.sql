﻿CREATE TABLE [dbo].[SavedWizardData](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Wizard_ID] [int] NULL,
	[Wizard_Parameter] [uniqueidentifier] NULL,
	[Stored_Values] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
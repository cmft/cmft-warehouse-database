﻿CREATE TABLE [dbo].[FetalBloodProcedureSamples](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[SampleID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Smoking_Follow_UpHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Smoking_Status] [int] NULL,
	[Date_of_Last_Cigarette] [datetime] NULL,
	[CO_Reading] [float] NULL,
	[Smoking_Cessation_Referral] [int] NULL,
	[Date_Seen_by_Midwifery_Assistant] [datetime] NULL
) ON [PRIMARY]
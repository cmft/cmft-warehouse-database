﻿CREATE TABLE [dbo].[Drug_Dosage](
	[Magnesium_Sulphate_Administration] [int] NULL,
	[Magnesium_Sulphate_Dose] [float] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurrence] [datetime] NULL,
	[Syringe_Replenished] [bit] NULL,
	[Obstetric_Review] [bit] NULL
) ON [PRIMARY]
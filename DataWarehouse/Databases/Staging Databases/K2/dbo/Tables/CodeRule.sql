﻿CREATE TABLE [dbo].[CodeRule](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[CodeSet] [nvarchar](100) NULL,
	[CodeText] [nvarchar](100) NULL,
	[CodeDesciption] [nvarchar](3000) NULL,
	[ExpressionText] [nvarchar](1000) NULL
) ON [PRIMARY]
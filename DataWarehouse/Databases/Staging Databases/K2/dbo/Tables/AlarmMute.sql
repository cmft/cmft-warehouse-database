﻿CREATE TABLE [dbo].[AlarmMute](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[AlarmId] [int] NULL,
	[MutedInRoomID] [int] NULL
) ON [PRIMARY]
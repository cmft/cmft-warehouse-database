﻿CREATE TABLE [dbo].[Infant_Transfer_from_LWHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_Infant_Delivery_ID] [uniqueidentifier] NULL,
	[Transfer_Destination] [int] NULL,
	[Transfer_Date_Time] [datetime] NULL,
	[Other_Destination_detail] [nvarchar](max) NULL,
	[NICU_Transfer_Bed] [int] NULL,
	[Arm_band_checked] [bit] NULL,
	[Skin_to_Skin_Transfer] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
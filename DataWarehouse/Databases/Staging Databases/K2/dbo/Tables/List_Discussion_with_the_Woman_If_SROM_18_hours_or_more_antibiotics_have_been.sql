﻿CREATE TABLE [dbo].[List_Discussion_with_the_Woman_If_SROM_18_hours_or_more_antibiotics_have_been](
	[ID] [int] NULL,
	[DisplayText] [nvarchar](100) NULL,
	[DescriptionText] [nvarchar](2000) NULL,
	[Group] [int] NULL,
	[ParentID] [int] NULL,
	[ImageID] [int] NULL,
	[OrderID] [int] NULL,
	[Active] [int] NULL,
	[CodedValue] [nvarchar](20) NULL
) ON [PRIMARY]
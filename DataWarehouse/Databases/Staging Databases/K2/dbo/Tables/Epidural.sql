﻿CREATE TABLE [dbo].[Epidural](
	[TimeEntered] [datetime] NOT NULL,
	[ListEpiduralDoseTypeID] [int] NULL,
	[ListEpiduralTypeID] [int] NULL,
	[ListEpiduralReasonID] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[ListAnalgesicType] [int] NULL,
	[ListOutcome] [int] NULL,
	[Complications] [int] NULL
) ON [PRIMARY]
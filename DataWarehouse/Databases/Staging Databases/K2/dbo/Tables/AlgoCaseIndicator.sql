﻿CREATE TABLE [dbo].[AlgoCaseIndicator](
	[CaseNumber] [nvarchar](40) NULL,
	[FileName] [nvarchar](255) NULL,
	[TimeEntered] [datetime] NULL,
	[VersionNo] [nvarchar](255) NULL,
	[OutcomeText] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
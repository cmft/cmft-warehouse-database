﻿CREATE TABLE [dbo].[System_MessageHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Message] [nvarchar](max) NULL,
	[Valid_From] [datetime] NULL,
	[Expires_On] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
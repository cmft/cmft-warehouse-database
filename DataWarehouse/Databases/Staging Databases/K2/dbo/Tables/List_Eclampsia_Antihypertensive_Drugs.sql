﻿CREATE TABLE [dbo].[List_Eclampsia_Antihypertensive_Drugs](
	[ID] [int] NULL,
	[DisplayText] [nvarchar](100) NULL,
	[DescriptionText] [nvarchar](2000) NULL,
	[Group] [int] NULL,
	[ParentID] [int] NULL,
	[ImageID] [int] NULL,
	[OrderID] [int] NULL,
	[Active] [int] NULL
) ON [PRIMARY]
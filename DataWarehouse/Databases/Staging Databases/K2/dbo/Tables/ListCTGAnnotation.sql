﻿CREATE TABLE [dbo].[ListCTGAnnotation](
	[Annotation] [nvarchar](11) NULL,
	[AnnotationDescription] [varchar](1000) NOT NULL,
	[AnnotationID] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[DisplayOnCTG] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
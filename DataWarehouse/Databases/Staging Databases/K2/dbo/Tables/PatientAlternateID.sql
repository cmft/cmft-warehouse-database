﻿CREATE TABLE [dbo].[PatientAlternateID](
	[PatientID] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[AlternatePatientID] [varchar](50) NOT NULL,
	[ListPatientIDType] [int] NOT NULL
) ON [PRIMARY]
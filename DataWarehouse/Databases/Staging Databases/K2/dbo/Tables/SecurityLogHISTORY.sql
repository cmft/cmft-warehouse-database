﻿CREATE TABLE [dbo].[SecurityLogHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Type] [int] NULL,
	[Description] [nvarchar](100) NULL,
	[SessionId] [uniqueidentifier] NULL
) ON [PRIMARY]
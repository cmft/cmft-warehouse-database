﻿CREATE TABLE [dbo].[PASConnectionSettings](
	[ID] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[ConnectionInfo1] [varchar](100) NOT NULL,
	[ConnectionInfo2] [varchar](100) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[InfantCaesareanSection](
	[BabyNumber] [int] NOT NULL,
	[ListCSectionTypeID] [int] NOT NULL,
	[ListCSectionAnaesthesiaID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[ListCSectionPriorityID] [int] NOT NULL,
	[ListCSectionRegionalAnaesthesiaID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
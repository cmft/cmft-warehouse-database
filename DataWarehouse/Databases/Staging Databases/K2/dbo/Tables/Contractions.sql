﻿CREATE TABLE [dbo].[Contractions](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[ContractionsPer10Mins] [int] NULL,
	[ListContractionStrengthID] [int] NULL,
	[Period] [int] NULL,
	[ListContractionDuration] [int] NULL,
	[ContractionType] [int] NULL,
	[Resting_Tone_Present] [bit] NULL,
	[Comments_on_Contractions] [nvarchar](500) NULL,
	[Contractions_Present] [bit] NULL
) ON [PRIMARY]
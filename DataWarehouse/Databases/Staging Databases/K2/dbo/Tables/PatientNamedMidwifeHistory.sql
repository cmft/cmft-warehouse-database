﻿CREATE TABLE [dbo].[PatientNamedMidwifeHistory](
	[TimeEntered] [datetime] NOT NULL,
	[MidwifeID] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[MidwifeName] [varchar](49) NULL
) ON [PRIMARY]
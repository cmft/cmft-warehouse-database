﻿CREATE TABLE [dbo].[ForwardReferenceHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[TableName] [nvarchar](100) NULL,
	[DiscardedID] [uniqueidentifier] NULL,
	[RetainedID] [uniqueidentifier] NULL
) ON [PRIMARY]
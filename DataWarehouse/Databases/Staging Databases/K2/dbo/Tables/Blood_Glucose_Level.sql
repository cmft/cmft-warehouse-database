﻿CREATE TABLE [dbo].[Blood_Glucose_Level](
	[Blood_Glucose_Level] [float] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[Blood_Glucose_Regime] [int] NULL,
	[Continuous_Infusion_Required] [int] NULL,
	[Insulin_Infusion_Rate] [float] NULL
) ON [PRIMARY]
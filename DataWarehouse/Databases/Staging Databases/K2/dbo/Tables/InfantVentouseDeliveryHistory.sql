﻿CREATE TABLE [dbo].[InfantVentouseDeliveryHistory](
	[BabyNumber] [int] NOT NULL,
	[ListVentouseTypeID] [int] NOT NULL,
	[ListVentouseRotationID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[ListVentouseCupID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Risk_ManagementHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Obstetric_risks] [nvarchar](100) NULL,
	[Anaesthetic_risks] [nvarchar](100) NULL,
	[Neonatal_risks] [nvarchar](100) NULL,
	[Incident_reported_to_shift_leader] [int] NULL,
	[Why_incident_not_reported] [nvarchar](500) NULL,
	[Was_Incident_Report_filled] [int] NULL,
	[Reason_IR_form_not_completed] [nvarchar](500) NULL,
	[IR_form_ID] [nvarchar](25) NULL,
	[maternal_incidents] [nvarchar](100) NULL,
	[fetal_or_neonatal_incidents] [nvarchar](100) NULL,
	[Corporate_Incidents] [nvarchar](100) NULL,
	[Other_Obstetric_Incidents] [nvarchar](200) NULL,
	[Other_Neonatal_Incidents] [nvarchar](200) NULL,
	[Incident_Reporting_on_DATIX] [nvarchar](100) NULL
) ON [PRIMARY]
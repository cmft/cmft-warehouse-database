﻿CREATE TABLE [dbo].[DialInParamsHistory](
	[ID] [int] NOT NULL,
	[ComputerName] [varchar](49) NOT NULL,
	[PortNumber] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[FetalBloodProcedure](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[ProcedureStartTime] [datetime] NULL,
	[LowestpH] [float] NULL,
	[BaseDef] [float] NULL,
	[Dilatation] [int] NULL,
	[ListMaternalPositionID] [int] NULL,
	[Duration] [int] NULL,
	[NumOfSamples] [int] NULL,
	[sample1_ID] [uniqueidentifier] NULL,
	[sample2_ID] [uniqueidentifier] NULL,
	[sample3_ID] [uniqueidentifier] NULL,
	[sample4_ID] [uniqueidentifier] NULL,
	[sample5_ID] [uniqueidentifier] NULL,
	[BaseExcess] [float] NULL,
	[associated_blood_lactate] [float] NULL,
	[Consultant_Informed] [bit] NULL,
	[Repeated_FBS] [bit] NULL,
	[Time_of_Previous_Attempt] [datetime] NULL,
	[Time_for_Next_FBS] [int] NULL,
	[Other_Time_for_Next_FBS] [int] NULL,
	[Reason_For_FBS] [int] NULL,
	[Reason_Not_Lateral_Position] [nvarchar](250) NULL,
	[Time_Sample_Obtained] [datetime] NULL,
	[Other_Time_for_Next_FBS_Comment] [nvarchar](100) NULL,
	[Consent_Obtained] [bit] NULL,
	[Procedure_Explained] [bit] NULL,
	[Type_of_Consent] [int] NULL,
	[Reason_for_No_Consent] [nvarchar](250) NULL,
	[Disscuss_with_Senior_Registrar] [bit] NULL,
	[Personnel] [nvarchar](100) NULL,
	[Personnel_ID] [int] NULL,
	[Personnel_Role] [int] NULL,
	[Personnel_Grade] [int] NULL,
	[Number_of_Attempts_to_Obtain_Fetal_Blood] [int] NULL,
	[Number_of_Previous_FBS] [int] NULL,
	[Reason_Result_Not_Obtained] [int] NULL,
	[Other_Reason_Result_Not_Obtained] [nvarchar](500) NULL,
	[Reason_For_Undertaking_FBS] [nvarchar](150) NULL,
	[Indication_for_FBS_Procedure] [int] NULL,
	[Other_Indication_for_FBS_Procedure] [nvarchar](250) NULL,
	[Explained] [bit] NULL,
	[Procedure_Duration] [int] NULL,
	[SampleSource] [int] NULL
) ON [PRIMARY]
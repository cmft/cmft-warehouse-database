﻿CREATE TABLE [dbo].[Investigation_Measurement_Notes](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_Investigation_Measurements_ID] [uniqueidentifier] NULL,
	[Text_Line] [nvarchar](256) NULL,
	[SetID] [int] NULL
) ON [PRIMARY]
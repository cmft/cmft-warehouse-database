﻿CREATE TABLE [dbo].[InfantCaesareanInterventionIndicatorHistory](
	[BabyNumber] [int] NOT NULL,
	[ListCSectionInterventionIndicatorID] [int] NOT NULL,
	[IntPriority] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
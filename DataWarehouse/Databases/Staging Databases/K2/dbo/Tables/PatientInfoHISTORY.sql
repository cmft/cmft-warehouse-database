﻿CREATE TABLE [dbo].[PatientInfoHISTORY](
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[CaseNumber] [uniqueidentifier] NULL,
	[Info] [nvarchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PCLocationHistory](
	[ListRoomID] [int] NULL,
	[ListTrolleyID] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NULL,
	[SessionNumber] [uniqueidentifier] NULL
) ON [PRIMARY]
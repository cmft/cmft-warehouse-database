﻿CREATE TABLE [dbo].[CTGFeatures](
	[BLOB] [varbinary](8000) NULL,
	[TimeEntered] [datetime] NOT NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
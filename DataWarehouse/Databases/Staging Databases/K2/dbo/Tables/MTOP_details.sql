﻿CREATE TABLE [dbo].[MTOP_details](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Certificate_A_signed] [bit] NULL,
	[Yellow_Form_signed_and_returned] [bit] NULL
) ON [PRIMARY]
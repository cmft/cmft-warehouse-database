﻿CREATE TABLE [dbo].[Bladder_Care](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Bladder_Catheterised] [bit] NULL,
	[Catheter_Type] [int] NULL,
	[Catheterisation_Time] [datetime] NULL,
	[Catheter_To_Be_Removed_after] [int] NULL,
	[Removal_to_be_Decided_Postnatally] [bit] NULL,
	[Catheter_Removal] [int] NULL,
	[Other_Catheter_Removal] [nvarchar](200) NULL,
	[Catheter_In_Situ] [bit] NULL,
	[Urine_Passed] [bit] NULL,
	[Time_of_first_void] [datetime] NULL,
	[Volume_of_first_void] [int] NULL,
	[Catheter_Removal_Decision] [int] NULL,
	[Indwelling_Catheter_Insertion_Time] [datetime] NULL,
	[Indwelling_Catheter_Removal_Time] [datetime] NULL,
	[Indwelling_Catheter_Inserted] [bit] NULL,
	[Indwelling_Catheter_Removed] [bit] NULL,
	[In_Out_Catheter_Inserted_Time] [datetime] NULL,
	[In_Out_Catheter_Inserted] [bit] NULL,
	[Other_Catheter_Removal_Plan_Details] [nvarchar](200) NULL,
	[In_Out_Catheter_Volume] [int] NULL,
	[Indwelling_Catheter_Volume] [int] NULL,
	[Indwelling_Left_In_Situ] [int] NULL,
	[Voiding_Chart_Commenced] [bit] NULL,
	[Catheter_Removed_For] [int] NULL,
	[Additional_Catheter_Used] [bit] NULL,
	[Catheter_Serial_Number] [nvarchar](15) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LabourStatus](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[Status] [int] NULL,
	[Indicators] [int] NULL,
	[Labour_Start] [datetime] NULL,
	[Onset_of_Labour] [int] NULL,
	[Time_Contractions_Established] [datetime] NULL,
	[History] [nvarchar](100) NULL,
	[Presentation_at_Onset] [int] NULL
) ON [PRIMARY]
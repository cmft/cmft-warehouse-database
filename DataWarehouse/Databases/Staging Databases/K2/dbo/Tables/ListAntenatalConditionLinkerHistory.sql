﻿CREATE TABLE [dbo].[ListAntenatalConditionLinkerHistory](
	[ListAntenatalSubCategoryID] [int] NOT NULL,
	[ListAntenatalConditionID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[ID] [int] NOT NULL
) ON [PRIMARY]
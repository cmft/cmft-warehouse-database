﻿CREATE TABLE [dbo].[Index_Reporting](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Report] [nvarchar](200) NULL,
	[Completion_Time] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientPregnancy](
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
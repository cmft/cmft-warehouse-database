﻿CREATE TABLE [dbo].[MaternalTemp](
	[TimeEntered] [datetime] NOT NULL,
	[Temperature] [float] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
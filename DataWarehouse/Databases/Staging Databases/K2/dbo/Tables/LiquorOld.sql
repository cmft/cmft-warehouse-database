﻿CREATE TABLE [dbo].[LiquorOld](
	[TimeEntered] [datetime] NOT NULL,
	[LiqNone] [int] NULL,
	[LiqClear] [int] NULL,
	[LiqBlood] [int] NULL,
	[LiqThin] [int] NULL,
	[LiqThick] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
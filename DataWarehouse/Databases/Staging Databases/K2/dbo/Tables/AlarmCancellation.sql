﻿CREATE TABLE [dbo].[AlarmCancellation](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[AlarmID] [int] NULL
) ON [PRIMARY]
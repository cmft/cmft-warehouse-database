﻿CREATE TABLE [dbo].[Respiratory_Rate](
	[respiratory_rate] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NN4B_Training_Users](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Firstname] [nvarchar](40) NULL,
	[Surname] [nvarchar](40) NULL,
	[UserID] [uniqueidentifier] NULL,
	[NmcNumber] [nvarchar](40) NULL
) ON [PRIMARY]
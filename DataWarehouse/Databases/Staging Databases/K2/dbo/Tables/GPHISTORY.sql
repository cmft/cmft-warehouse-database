﻿CREATE TABLE [dbo].[GPHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_GP_Practice_ID] [uniqueidentifier] NULL,
	[Prefix] [nvarchar](8) NULL,
	[Firstname] [nvarchar](256) NULL,
	[Middle_name] [nvarchar](50) NULL,
	[Lastname] [nvarchar](256) NULL,
	[GP_National_Code] [nvarchar](8) NULL
) ON [PRIMARY]
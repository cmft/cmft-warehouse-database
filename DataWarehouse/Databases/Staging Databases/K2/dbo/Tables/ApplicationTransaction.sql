﻿CREATE TABLE [dbo].[ApplicationTransaction](
	[TransactionID] [int] NOT NULL,
	[TableType] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TablePrimary] [int] NOT NULL,
	[AuthenticatingStaffID] [int] NOT NULL,
	[Active] [int] NOT NULL
) ON [PRIMARY]
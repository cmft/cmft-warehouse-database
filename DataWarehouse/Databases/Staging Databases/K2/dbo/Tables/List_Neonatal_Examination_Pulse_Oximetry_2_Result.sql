﻿CREATE TABLE [dbo].[List_Neonatal_Examination_Pulse_Oximetry_2_Result](
	[ID] [int] NULL,
	[DisplayText] [nvarchar](100) NULL,
	[DescriptionText] [nvarchar](2000) NULL,
	[Group] [int] NULL,
	[ParentID] [int] NULL,
	[ImageID] [int] NULL,
	[OrderID] [int] NULL,
	[Active] [int] NULL,
	[CodedValue] [nvarchar](20) NULL
) ON [PRIMARY]
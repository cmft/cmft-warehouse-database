﻿CREATE TABLE [dbo].[Configuration_FilesHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Version_Number] [int] NULL,
	[File_Contents] [nvarchar](max) NULL,
	[File_Name] [nvarchar](50) NULL,
	[PublishNotes] [nvarchar](max) NULL,
	[PublishedBy] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
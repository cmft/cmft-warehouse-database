﻿CREATE TABLE [dbo].[User_Password_Expiry](
	[ID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[ExpiryPolicy] [int] NULL,
	[PasswordLastChanged] [datetime] NULL
) ON [PRIMARY]
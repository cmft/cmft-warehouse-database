﻿CREATE TABLE [dbo].[NetworkSettings](
	[ID] [int] NOT NULL,
	[BroadcastIPAddress] [nvarchar](21) NULL,
	[BroadcastPort] [int] NOT NULL,
	[LiveUpdateFrequency] [int] NOT NULL,
	[LiveUpdateCheckFrequency] [int] NOT NULL,
	[LiveUpdateThreshold] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL
) ON [PRIMARY]
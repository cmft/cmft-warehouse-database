﻿CREATE TABLE [dbo].[JIMCom_Diagnostics_MessageHISTORY](
	[Source_Room_ID] [int] NULL,
	[Dest_Room_ID] [int] NULL,
	[Succesfull_Callback] [bit] NULL,
	[ID] [int] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[TimeEntered] [datetime] NULL
) ON [PRIMARY]
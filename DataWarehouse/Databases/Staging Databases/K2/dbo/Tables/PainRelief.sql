﻿CREATE TABLE [dbo].[PainRelief](
	[TimeEntered] [datetime] NOT NULL,
	[ListPainReliefTypeID] [int] NULL,
	[Dosage] [float] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
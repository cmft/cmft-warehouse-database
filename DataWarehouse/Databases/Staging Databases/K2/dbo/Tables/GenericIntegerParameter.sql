﻿CREATE TABLE [dbo].[GenericIntegerParameter](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[ParameterValue] [int] NULL,
	[ParameterType] [int] NULL
) ON [PRIMARY]
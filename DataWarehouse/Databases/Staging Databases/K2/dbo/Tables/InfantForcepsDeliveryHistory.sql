﻿CREATE TABLE [dbo].[InfantForcepsDeliveryHistory](
	[BabyNumber] [int] NOT NULL,
	[ListForcepsTypeID] [int] NOT NULL,
	[ListForcepsRotationID] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NULL
) ON [PRIMARY]
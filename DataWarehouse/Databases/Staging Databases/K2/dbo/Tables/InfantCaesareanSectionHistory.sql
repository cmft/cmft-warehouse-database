﻿CREATE TABLE [dbo].[InfantCaesareanSectionHistory](
	[BabyNumber] [int] NOT NULL,
	[ListCSectionTypeID] [int] NOT NULL,
	[ListCSectionAnaesthesiaID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[ListCSectionPriorityID] [int] NOT NULL,
	[ListCSectionRegionalAnaesthesiaID] [int] NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Insertion_of_Cervical_SutureHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[Procedure_Date_and_Time] [datetime] NULL,
	[Surgeon] [nvarchar](100) NULL,
	[Surgeon_ID] [int] NULL,
	[Surgeon_Grade] [int] NULL,
	[Assistant] [nvarchar](100) NULL,
	[Assistant_ID] [int] NULL,
	[Assistant_Grade] [int] NULL,
	[Procedure_Anaesthetic] [int] NULL,
	[Performed_Antenatally] [bit] NULL,
	[Procedure_Timing] [int] NULL,
	[Insertion_EBL] [int] NULL,
	[Suture_Material] [int] NULL,
	[Plan_for_Removal] [nvarchar](500) NULL,
	[Follow_Up_Required] [bit] NULL,
	[Follow_Up_Date_Time] [datetime] NULL,
	[Type_of_Suture] [int] NULL,
	[Other_Type_of_Suture_] [nvarchar](500) NULL
) ON [PRIMARY]
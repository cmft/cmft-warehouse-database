﻿CREATE TABLE [dbo].[Postcode_to_Borough](
	[Postcode] [nvarchar](255) NULL,
	[Region_Code] [nvarchar](255) NULL,
	[Sub_Region] [nvarchar](255) NULL,
	[Region] [nvarchar](255) NULL
) ON [PRIMARY]
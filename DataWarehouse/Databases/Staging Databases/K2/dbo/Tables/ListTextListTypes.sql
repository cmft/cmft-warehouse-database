﻿CREATE TABLE [dbo].[ListTextListTypes](
	[TextListDescriptor] [varchar](1000) NOT NULL,
	[ListItemType] [int] NOT NULL,
	[ListGroupType] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ApplicationAudit](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[LocationID] [uniqueidentifier] NULL,
	[ApplicationName] [nvarchar](50) NULL,
	[ApplicationVersion] [nvarchar](50) NULL,
	[ClinicalModelVersion] [int] NULL,
	[SettingsFiles] [nvarchar](255) NULL,
	[SiteID] [int] NULL,
	[OSVersion] [nvarchar](50) NULL,
	[Room] [nvarchar](50) NULL,
	[ShellFileVersion] [nvarchar](50) NULL,
	[HardwareRevision] [nvarchar](50) NULL,
	[SerialNo] [nvarchar](50) NULL
) ON [PRIMARY]
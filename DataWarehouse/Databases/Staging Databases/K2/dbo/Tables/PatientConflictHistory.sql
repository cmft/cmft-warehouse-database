﻿CREATE TABLE [dbo].[PatientConflictHistory](
	[RetainedPatientID] [varchar](50) NOT NULL,
	[DiscardedPatientID] [varchar](50) NULL,
	[TimeEntered] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[List_Session_Shoulder_Dystocia_Posterior_Arm_Left_or_Right](
	[ID] [int] NULL,
	[CodedValue] [nvarchar](20) NULL,
	[DisplayText] [nvarchar](100) NULL,
	[DescriptionText] [nvarchar](2000) NULL,
	[Group] [int] NULL,
	[ParentID] [int] NULL,
	[ImageID] [int] NULL,
	[OrderID] [int] NULL,
	[Active] [int] NULL
) ON [PRIMARY]
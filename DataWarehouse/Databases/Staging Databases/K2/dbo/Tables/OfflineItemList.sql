﻿CREATE TABLE [dbo].[OfflineItemList](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[PregnancyID] [uniqueidentifier] NULL,
	[LocationID] [uniqueidentifier] NULL
) ON [PRIMARY]
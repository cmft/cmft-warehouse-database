﻿CREATE TABLE [dbo].[Artificial_Rupture_Of_Membranes](
	[ARM_Successful] [bit] NULL,
	[Reason_For_ARM] [nvarchar](100) NULL,
	[Other_Reason_For_ARM] [nvarchar](250) NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL
) ON [PRIMARY]
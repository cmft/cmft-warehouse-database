﻿CREATE TABLE [dbo].[Patient_TransferHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Transfer_Date_Time] [datetime] NULL,
	[Transfer_Source] [int] NULL,
	[Transfer_Destination] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PCLocation](
	[ListRoomID] [int] NULL,
	[ListTrolleyID] [int] NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
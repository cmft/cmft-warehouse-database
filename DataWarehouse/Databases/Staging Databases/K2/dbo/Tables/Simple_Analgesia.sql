﻿CREATE TABLE [dbo].[Simple_Analgesia](
	[Type] [int] NULL,
	[Action] [int] NULL,
	[Other_PR_details] [nvarchar](200) NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[pain_relief_dose] [float] NULL,
	[pain_relief_units] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Ward](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Name] [nvarchar](100) NULL
) ON [PRIMARY]
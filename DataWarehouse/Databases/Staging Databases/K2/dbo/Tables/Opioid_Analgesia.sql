﻿CREATE TABLE [dbo].[Opioid_Analgesia](
	[Type] [int] NULL,
	[Dose] [float] NULL,
	[Dose_units] [nvarchar](20) NULL,
	[Route] [int] NULL,
	[Action] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[Other_Details] [nvarchar](100) NULL,
	[PCA_Pump_Number] [int] NULL,
	[Indications_for_PCA] [nvarchar](100) NULL,
	[Other_Indications_for_PCA] [nvarchar](500) NULL
) ON [PRIMARY]
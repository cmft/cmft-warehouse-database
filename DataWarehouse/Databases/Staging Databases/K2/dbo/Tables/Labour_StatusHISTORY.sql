﻿CREATE TABLE [dbo].[Labour_StatusHISTORY](
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[CaseNumber] [uniqueidentifier] NULL,
	[Cervical_Dilatation] [int] NULL,
	[Examination_Time] [datetime] NULL
) ON [PRIMARY]
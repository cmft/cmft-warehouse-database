﻿CREATE TABLE [dbo].[FailedLoginAttemptsHISTORY](
	[LoginEntered] [nvarchar](100) NULL,
	[TimeEntered] [datetime] NULL,
	[FailedAttempts] [int] NULL
) ON [PRIMARY]
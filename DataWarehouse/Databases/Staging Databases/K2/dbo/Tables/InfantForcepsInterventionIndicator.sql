﻿CREATE TABLE [dbo].[InfantForcepsInterventionIndicator](
	[BabyNumber] [int] NOT NULL,
	[ListForcepsInterventionIndicatorID] [int] NOT NULL,
	[IndicatorPriority] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[CaseNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
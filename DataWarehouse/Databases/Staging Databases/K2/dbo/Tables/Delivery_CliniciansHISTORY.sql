﻿CREATE TABLE [dbo].[Delivery_CliniciansHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_Infant_Delivery_ID] [uniqueidentifier] NULL,
	[Delivery_Clinician] [nvarchar](100) NULL,
	[Delivery_Clinician_ID] [int] NULL,
	[Assisting_Clinician] [nvarchar](100) NULL,
	[Assisting_Clinician_ID] [int] NULL,
	[Supervised_by] [nvarchar](100) NULL,
	[Supervised_by_ID] [int] NULL,
	[present1] [nvarchar](100) NULL,
	[present1_ID] [int] NULL,
	[present2] [nvarchar](100) NULL,
	[present2_ID] [int] NULL,
	[present3] [nvarchar](100) NULL,
	[present3_ID] [int] NULL,
	[present4] [nvarchar](100) NULL,
	[present4_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[present5] [nvarchar](100) NULL,
	[present5_ID] [int] NULL,
	[present6] [nvarchar](100) NULL,
	[present6_ID] [int] NULL,
	[present7] [nvarchar](100) NULL,
	[present7_ID] [int] NULL,
	[present8] [nvarchar](100) NULL,
	[present8_ID] [int] NULL,
	[present9] [nvarchar](100) NULL,
	[present9_ID] [int] NULL,
	[present10] [nvarchar](100) NULL,
	[present10_ID] [int] NULL,
	[Delivery_clinician_role] [int] NULL,
	[Assisting_clinician_role] [int] NULL,
	[Supervisor_role] [int] NULL,
	[present1_role] [int] NULL,
	[present2_role] [int] NULL,
	[present3_role] [int] NULL,
	[present4_role] [int] NULL,
	[present5_role] [int] NULL,
	[present6_role] [int] NULL,
	[present7_role] [int] NULL,
	[present8_role] [int] NULL,
	[present9_role] [int] NULL,
	[present10_role] [int] NULL,
	[Midwife_in_Charge] [nvarchar](100) NULL,
	[Midwife_in_Charge_ID] [int] NULL,
	[Anaesthetist] [nvarchar](100) NULL,
	[Anaesthetist_ID] [int] NULL,
	[Obstetric_Registrar] [nvarchar](100) NULL,
	[Obstetric_Registrar_ID] [int] NULL,
	[Consultant_Obstetrician] [nvarchar](100) NULL,
	[Consultant_Obstetrcian_ID] [int] NULL,
	[Paediatric_SHO] [nvarchar](100) NULL,
	[Paediatric_SHO_ID] [int] NULL,
	[Paediatric_Registrar] [nvarchar](100) NULL,
	[Consultant_Paediatrician] [nvarchar](100) NULL,
	[Consultant_Paediatrician_ID] [int] NULL,
	[Paediatric_Registrar_ID] [int] NULL,
	[Obstetric_SHO] [nvarchar](100) NULL,
	[Obstetric_SHO_ID] [int] NULL,
	[Obstetric_Registrar_2] [nvarchar](100) NULL,
	[Obstetric_Registrar_2ID] [int] NULL,
	[Consultant_On_Call] [nvarchar](100) NULL,
	[Consultant_On_Call_ID] [int] NULL,
	[Other_Health_Professional_3] [nvarchar](100) NULL,
	[Other_Health_Professional_3_ID] [int] NULL,
	[Other_Health_Professional_3_Role] [int] NULL
) ON [PRIMARY]
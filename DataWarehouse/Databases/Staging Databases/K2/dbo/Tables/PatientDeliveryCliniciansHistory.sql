﻿CREATE TABLE [dbo].[PatientDeliveryCliniciansHistory](
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[ClinicianDelivering] [varchar](50) NOT NULL,
	[OtherClinician1] [varchar](50) NOT NULL,
	[OtherClinician2] [varchar](50) NOT NULL,
	[ClinicianDeliveringID] [int] NOT NULL,
	[SupervisingMidwifeID] [int] NOT NULL,
	[OtherClinician1ID] [int] NOT NULL,
	[OtherClinician2ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AlarmActivation](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[AlarmID] [int] NULL,
	[AlarmLevel] [int] NULL,
	[AlarmType] [int] NULL,
	[AlarmSubType] [int] NULL,
	[AlarmEnabled] [int] NULL,
	[ChannelID] [int] NULL,
	[Param1] [int] NULL,
	[Param2] [int] NULL
) ON [PRIMARY]
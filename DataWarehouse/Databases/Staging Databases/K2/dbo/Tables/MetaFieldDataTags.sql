﻿CREATE TABLE [dbo].[MetaFieldDataTags](
	[TableID] [int] NULL,
	[FieldID] [int] NULL,
	[TagID] [int] NULL,
	[Reference] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MetaConditionsHISTORY](
	[ID] [int] NULL,
	[Name] [nvarchar](100) NULL,
	[Confidential] [int] NULL,
	[HiRisk] [int] NULL
) ON [PRIMARY]
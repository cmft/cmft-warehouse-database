﻿CREATE TABLE [dbo].[Drugs_Fluids](
	[Drug_or_Fluid] [int] NULL,
	[Dose] [float] NULL,
	[Dose_Units] [nvarchar](100) NULL,
	[Administration_Route] [nvarchar](250) NULL,
	[Other] [nvarchar](500) NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurrence] [datetime] NULL
) ON [PRIMARY]
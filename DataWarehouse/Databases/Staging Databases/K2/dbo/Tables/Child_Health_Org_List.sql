﻿CREATE TABLE [dbo].[Child_Health_Org_List](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Org_Name] [nvarchar](100) NULL,
	[Org_Code] [nvarchar](5) NULL
) ON [PRIMARY]
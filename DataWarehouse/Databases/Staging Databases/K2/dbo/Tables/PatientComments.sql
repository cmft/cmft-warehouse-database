﻿CREATE TABLE [dbo].[PatientComments](
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[Comments] [varchar](1000) NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL
) ON [PRIMARY]
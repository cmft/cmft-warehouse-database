﻿CREATE TABLE [dbo].[Water_Pool_Monitoring](
	[Temperature] [float] NULL,
	[Woman_Enters_Pool] [bit] NULL,
	[Woman_Exits_Pool] [bit] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[TimeOfOccurrence] [datetime] NULL,
	[Woman_in_Pool] [bit] NULL,
	[Checklist] [nvarchar](100) NULL,
	[Reason_For_Exiting_Pool] [nvarchar](100) NULL,
	[Other_Reason_for_Exiting_Pool] [nvarchar](500) NULL
) ON [PRIMARY]
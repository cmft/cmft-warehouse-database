﻿CREATE TABLE [dbo].[PatientAllergyHistory](
	[CaseNumber] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffID] [int] NOT NULL,
	[Patient_has_Allergies] [int] NULL,
	[Allergy_list] [nvarchar](100) NULL,
	[Anaphlaxis] [bit] NULL,
	[Carries_Epipen] [bit] NULL,
	[Allergy_Band_in_Place] [bit] NULL,
	[Other_Allergies] [nvarchar](127) NULL,
	[Non_Drug_Allergies] [nvarchar](100) NULL,
	[Reactions] [nvarchar](127) NULL,
	[Any_Reactions] [bit] NULL,
	[Does_Woman_Have_AntiD_Antibodies] [int] NULL,
	[Has_Woman_Been_Cross_Matched] [int] NULL,
	[Why_has_Cross_Match_not_Been_Carried_Out] [nvarchar](120) NULL
) ON [PRIMARY]
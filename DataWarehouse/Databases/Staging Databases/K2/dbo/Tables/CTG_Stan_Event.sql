﻿CREATE TABLE [dbo].[CTG_Stan_Event](
	[Event_Type] [int] NULL,
	[Event_Description] [nvarchar](140) NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL
) ON [PRIMARY]
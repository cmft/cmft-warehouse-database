﻿CREATE TABLE [dbo].[LocalAlarmThreshold](
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[BaseHi] [int] NULL,
	[BaseLo] [int] NULL,
	[BaseEnabled] [bit] NULL,
	[VarHi] [int] NULL,
	[VarLo] [int] NULL,
	[VarEnabled] [bit] NULL,
	[SigQualDuration] [int] NULL,
	[SigQualEnabled] [bit] NULL,
	[CoincidenceEnabled] [bit] NULL,
	[ContractFrequency] [int] NULL,
	[ContractDuration] [int] NULL,
	[ContractEnabled] [bit] NULL
) ON [PRIMARY]
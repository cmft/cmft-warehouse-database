﻿CREATE TABLE [dbo].[ListStaffMember](
	[StaffId] [int] NOT NULL,
	[StaffIDType] [int] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[SurName] [nvarchar](50) NULL,
	[Password] [varchar](20) NULL,
	[DataAccessLevel] [int] NULL,
	[FingerPrint] [varbinary](7800) NULL,
	[AuthenticatingStaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[StaffType] [int] NOT NULL,
	[Identifier] [nvarchar](50) NULL,
	[Initials] [varchar](4) NULL
) ON [PRIMARY]
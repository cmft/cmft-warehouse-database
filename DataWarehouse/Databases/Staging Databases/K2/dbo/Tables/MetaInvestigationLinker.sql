﻿CREATE TABLE [dbo].[MetaInvestigationLinker](
	[ID] [int] NULL,
	[Link_MetaConsultation_ID] [int] NULL,
	[Name] [nvarchar](100) NULL,
	[Role] [nvarchar](100) NULL,
	[Description] [nvarchar](1000) NULL
) ON [PRIMARY]
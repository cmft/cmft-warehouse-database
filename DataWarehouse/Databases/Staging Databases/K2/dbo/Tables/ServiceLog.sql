﻿CREATE TABLE [dbo].[ServiceLog](
	[ID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Service_Type] [nvarchar](100) NULL,
	[Details] [nvarchar](250) NULL
) ON [PRIMARY]
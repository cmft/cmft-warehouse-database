﻿CREATE TABLE [dbo].[FetalBloodSample](
	[TimeEntered] [datetime] NOT NULL,
	[Result1] [float] NULL,
	[Result2] [float] NULL,
	[Unsuccessful] [int] NULL,
	[Dilatation] [int] NULL,
	[ListMaternalPositionID] [int] NULL,
	[Duration] [int] NULL,
	[Result1BaseDef] [float] NULL,
	[Result2BaseDef] [float] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Twin2_Delivery_ChecklistHISTORY](
	[ID] [uniqueidentifier] NULL,
	[TimeEntered] [datetime] NULL,
	[Link_ListStaffMember_ID] [int] NULL,
	[Link_PatientPregnancy_ID] [uniqueidentifier] NULL,
	[CEFM_Checked_and_Secured] [bit] NULL,
	[Palpation_or_Scan_for_Presentation] [bit] NULL,
	[Operator] [nvarchar](100) NULL,
	[Operator_ID] [int] NULL,
	[Operator_Grade] [int] NULL,
	[Presentation] [int] NULL,
	[Lie_Stabilised] [bit] NULL,
	[IPV_Attempted] [bit] NULL,
	[IPV_Successful] [bit] NULL,
	[IPV_Unsuccessful_Reason] [nvarchar](200) NULL,
	[Syntocinon_Started] [bit] NULL,
	[Membranes_Ruptured] [bit] NULL,
	[Rupture_Date_and_Time] [datetime] NULL,
	[Name] [nvarchar](100) NULL,
	[Name_ID] [int] NULL,
	[Grade] [int] NULL,
	[Staff_informed] [bit] NULL
) ON [PRIMARY]
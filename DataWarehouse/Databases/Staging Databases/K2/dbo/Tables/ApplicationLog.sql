﻿CREATE TABLE [dbo].[ApplicationLog](
	[LogTime] [datetime] NOT NULL,
	[MACAddress] [varchar](30) NULL,
	[Type] [varchar](50) NULL,
	[Details] [varchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientAnaestheticHistory](
	[StaffID] [int] NULL,
	[TimeEntered] [datetime] NULL,
	[CaseNumber] [uniqueidentifier] NULL,
	[Previous_Anaesthetic_Complications] [bit] NULL,
	[Previous_Anaesthetic_Complications_Detail] [nvarchar](500) NULL
) ON [PRIMARY]
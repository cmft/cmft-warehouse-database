﻿CREATE TABLE [dbo].[CTGAnnotations](
	[Description] [nvarchar](max) NULL,
	[Annotation] [nvarchar](11) NULL,
	[TimeEntered] [datetime] NOT NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[DisplayOnCTG] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
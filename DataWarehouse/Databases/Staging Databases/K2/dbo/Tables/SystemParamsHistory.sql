﻿CREATE TABLE [dbo].[SystemParamsHistory](
	[ID] [int] NOT NULL,
	[SessionTo] [int] NOT NULL,
	[AutoDischarge] [int] NOT NULL,
	[AlertTimeAnonSession] [int] NOT NULL,
	[MinTimeToAllowRemoteDis] [int] NOT NULL,
	[MaxTimeLimitEventEntry] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[AlarmMuteTO] [int] NOT NULL,
	[DisconnectTO] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Early_Warning_Scores](
	[Temperature_score] [int] NULL,
	[systolic_pressure_score] [int] NULL,
	[diastolic_pressure_score] [int] NULL,
	[maternal_heart_rate_score] [int] NULL,
	[respiratory_rate_score] [int] NULL,
	[cns_score] [int] NULL,
	[spo2_score] [int] NULL,
	[EWS_total] [int] NULL,
	[SessionNumber] [uniqueidentifier] NOT NULL,
	[TimeEntered] [datetime] NOT NULL,
	[DataTransaction_TimeOfOccurence] [datetime] NULL,
	[Yellow_Scores] [int] NULL,
	[Red_Scores] [int] NULL,
	[urinalysis_score] [int] NULL,
	[urine_volume_score] [int] NULL,
	[supplemental_oxygen] [int] NULL,
	[EWS_comments] [nvarchar](2000) NULL,
	[Calculate_MEWS] [int] NULL
) ON [PRIMARY]
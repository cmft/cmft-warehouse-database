﻿CREATE TABLE [dbo].[NotGeneticsReasonTrial2](
	[GeneticsID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonID] [int] NOT NULL,
	[Description] [varchar](100) NOT NULL
) ON [PRIMARY]
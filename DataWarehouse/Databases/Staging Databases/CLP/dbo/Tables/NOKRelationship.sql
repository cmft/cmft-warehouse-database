﻿CREATE TABLE [dbo].[NOKRelationship](
	[ID] [int] NOT NULL,
	[Relationship] [char](10) NOT NULL,
	[Description] [varchar](25) NOT NULL
) ON [PRIMARY]
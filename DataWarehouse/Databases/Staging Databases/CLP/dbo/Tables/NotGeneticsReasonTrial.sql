﻿CREATE TABLE [dbo].[NotGeneticsReasonTrial](
	[GeneticsID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonID] [int] NOT NULL,
	[Description] [varchar](30) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Clinic](
	[ClinicID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Purpose] [text] NULL,
	[Info] [text] NULL,
	[RequestBy] [text] NULL,
	[Outcome] [char](4) NULL,
	[ClinicType] [char](10) NULL,
	[Time] [char](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
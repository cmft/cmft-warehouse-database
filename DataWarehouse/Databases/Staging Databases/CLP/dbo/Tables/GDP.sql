﻿CREATE TABLE [dbo].[GDP](
	[GDPCode] [char](6) NOT NULL,
	[Title] [varchar](4) NULL,
	[Initials] [varchar](4) NULL,
	[Surname] [varchar](24) NULL,
	[Address1] [varchar](20) NULL,
	[Address2] [varchar](20) NULL,
	[Address3] [varchar](20) NULL,
	[Address4] [varchar](20) NULL,
	[Postcode] [varchar](10) NULL,
	[Phone] [varchar](23) NULL
) ON [PRIMARY]
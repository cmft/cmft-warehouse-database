﻿CREATE TABLE [dbo].[Purchaser](
	[PurchaserID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[PurchaserCode] [char](3) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Names](
	[NameID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL CONSTRAINT [DF_Names_Date_4__16]  DEFAULT (getdate()),
	[Surname] [varchar](24) NULL,
	[Forename] [varchar](20) NULL,
	[PatientID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NextJointClinicRequest](
	[RequesterID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) NOT NULL
) ON [PRIMARY]
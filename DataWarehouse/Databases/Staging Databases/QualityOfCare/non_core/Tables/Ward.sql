﻿CREATE TABLE [non_core].[Ward](
	[WardID] [int] IDENTITY(1,1) NOT NULL,
	[WardName] [nvarchar](255) NULL,
	[Password] [nvarchar](255) NULL,
	[WardCategoryTypeID] [int] NULL,
	[Hospital] [nvarchar](50) NULL,
	[WardCategoryGroupID] [int] NULL,
	[Password_WM] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[Active] [bit] NOT NULL,
	[WardDivisionID] [int] NULL,
	[ContactEmail] [nvarchar](50) NULL,
	[PASCode] [char](4) NULL,
	[ParentWardID] [int] NULL,
	[IsParentWard] [bit] NULL,
	[PasswordEnc] [varbinary](128) NULL,
	[Password_WMEnc] [varbinary](128) NULL
) ON [PRIMARY]
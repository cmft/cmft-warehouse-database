﻿CREATE TABLE [dbo].[WardCategoryType](
	[WardCategoryTypeID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[WardDescription] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AuditHead](
	[AuditHeadID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AdultChildID] [int] NULL,
	[Title] [nvarchar](255) NULL,
	[Active] [int] NULL CONSTRAINT [auditactive_DF]  DEFAULT ((0)),
	[AuditHeadGUID] [nvarchar](255) NULL,
	[AuditHeadDate] [datetime] NULL,
	[WardCategoryID] [int] NULL
) ON [PRIMARY]
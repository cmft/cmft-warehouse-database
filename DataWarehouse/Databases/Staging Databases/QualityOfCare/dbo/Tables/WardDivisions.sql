﻿CREATE TABLE [dbo].[WardDivisions](
	[WardDivisionID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[WardDivisionDescription] [nvarchar](50) NULL
) ON [PRIMARY]
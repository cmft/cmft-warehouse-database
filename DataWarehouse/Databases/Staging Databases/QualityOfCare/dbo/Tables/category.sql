﻿CREATE TABLE [dbo].[category](
	[categoryID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[category] [nvarchar](255) NULL
) ON [PRIMARY]
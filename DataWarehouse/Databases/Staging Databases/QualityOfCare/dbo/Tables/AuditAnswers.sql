﻿CREATE TABLE [dbo].[AuditAnswers](
	[AuditAnswersID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AuditHeadAnswersID] [int] NULL CONSTRAINT [DF__audit_ans__head___300424B4]  DEFAULT ((0)),
	[QuestionID] [int] NULL CONSTRAINT [DF__audit_ans__quest__30F848ED]  DEFAULT ((0)),
	[Item] [int] NULL CONSTRAINT [DF__audit_answ__item__31EC6D26]  DEFAULT ((0)),
	[Answer] [nvarchar](10) NULL
) ON [PRIMARY]
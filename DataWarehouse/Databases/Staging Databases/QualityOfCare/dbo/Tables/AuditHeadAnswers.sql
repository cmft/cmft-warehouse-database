﻿CREATE TABLE [dbo].[AuditHeadAnswers](
	[AuditHeadAnswersID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GUID] [nvarchar](255) NULL,
	[WardID] [int] NULL CONSTRAINT [DF__audit_hea__WardI__1BFD2C07]  DEFAULT ((0)),
	[AuditID] [int] NULL,
	[AdultChildID] [int] NULL CONSTRAINT [DF__audit_hea__adult__1DE57479]  DEFAULT ((0)),
	[AuditTypeID] [int] NULL CONSTRAINT [DF__audit_hea__Audit__1ED998B2]  DEFAULT ((0)),
	[SubmittedAuditDate] [datetime] NULL,
	[ActualAuditDate] [datetime] NULL,
	[SubmittedBy] [nvarchar](100) NULL
) ON [PRIMARY]
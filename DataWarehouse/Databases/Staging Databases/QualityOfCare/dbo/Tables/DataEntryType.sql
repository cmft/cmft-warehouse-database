﻿CREATE TABLE [dbo].[DataEntryType](
	[DataEntryTypeID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[DataEntryTypeDescription] [nvarchar](50) NULL
) ON [PRIMARY]
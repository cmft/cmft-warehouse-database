﻿CREATE TABLE [dbo].[AuditType](
	[AuditTypeID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AuditTypeDescription] [nvarchar](255) NOT NULL,
	[AuditOrder] [int] NOT NULL,
	[Items] [int] NOT NULL
) ON [PRIMARY]
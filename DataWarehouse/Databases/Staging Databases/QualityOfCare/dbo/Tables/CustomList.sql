﻿CREATE TABLE [dbo].[CustomList](
	[CustomListID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomListDescription] [nvarchar](50) NOT NULL
) ON [PRIMARY]
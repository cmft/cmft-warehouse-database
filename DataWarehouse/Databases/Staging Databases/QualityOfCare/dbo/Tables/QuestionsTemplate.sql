﻿CREATE TABLE [dbo].[QuestionsTemplate](
	[QuestionID] [int] IDENTITY(10000,1) NOT FOR REPLICATION NOT NULL,
	[AuditTypeID] [int] NOT NULL,
	[QuestionText] [nvarchar](max) NULL,
	[Guidance] [nvarchar](max) NULL,
	[add_text] [nvarchar](255) NULL,
	[Detail] [nvarchar](255) NULL,
	[Limit] [int] NULL,
	[QuestionOrder] [int] NULL,
	[AuditHeadMap] [varchar](8) NULL,
	[DataEntryTypeID] [int] NULL,
	[Enabled] [bit] NOT NULL DEFAULT ((1)),
	[AccumulateSampleSize] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
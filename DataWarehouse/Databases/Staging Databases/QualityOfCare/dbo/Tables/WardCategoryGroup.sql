﻿CREATE TABLE [dbo].[WardCategoryGroup](
	[WardCategoryGroupID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupDescription] [nvarchar](50) NOT NULL
) ON [PRIMARY]
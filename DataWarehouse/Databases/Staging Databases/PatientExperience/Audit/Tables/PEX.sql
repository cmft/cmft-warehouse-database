﻿CREATE TABLE [Audit].[PEX](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[XmlFileID] [int] NOT NULL,
	[processedDate] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[processType] [char](1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Staging].[XmlFiles](
	[FName] [varchar](255) NOT NULL,
	[xmlContent] [xml] NOT NULL,
	[dateImported] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[XmlFileID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
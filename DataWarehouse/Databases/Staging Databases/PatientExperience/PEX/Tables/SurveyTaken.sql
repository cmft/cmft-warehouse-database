﻿CREATE TABLE [PEX].[SurveyTaken](
	[id] [bigint] NULL,
	[ResponseID] [int] NULL,
	[SurveyID] [int] NULL,
	[DateTaken] [datetime] NULL,
	[DeviceNum] [int] NULL,
	[LocationID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [PEX].[SurveyAnswers](
	[id] [bigint] NULL,
	[SurveyTakenID] [bigint] NULL,
	[SurveyQuestionID] [bigint] NULL,
	[SurveyAnswerID] [int] NULL,
	[AnswerText] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
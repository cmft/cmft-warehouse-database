﻿CREATE TABLE [PEX].[SurveyQuestions](
	[ID] [bigint] NULL,
	[SurveyID] [int] NULL,
	[QuestionPosition] [int] NULL,
	[QuestionID] [int] NULL,
	[QuestionText] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
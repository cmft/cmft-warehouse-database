﻿CREATE TABLE [dbo].[FFTLocationMap](
	[LtnNbr] [int] NOT NULL,
	[LtnNme] [varchar](50) NOT NULL,
	[LtnNme2] [varchar](50) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[DvnNme] [varchar](50) NOT NULL,
	[Rtn] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[FFTLocationSelect](
	[LtnNme] [varchar](50) NOT NULL,
	[LtnDtl] [varchar](50) NOT NULL,
	[LtnTyp] [varchar](50) NULL,
	[LtnDiv] [varchar](50) NULL,
	[Rtn] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [NeonatalDetailed].[abdoxray](
	[BabyNHSNumber] [nvarchar](30) NULL,
	[rectime] [datetime] NULL,
	[rectimeMonth] [int] NULL,
	[rectimeyear] [int] NULL,
	[episode] [int] NULL,
	[UnitName] [nvarchar](150) NULL,
	[xrayPerformed] [nvarchar](100) NULL,
	[WhereXRayDone] [nvarchar](30) NULL,
	[NEC] [int] NULL,
	[XrayAppearance] [nvarchar](100) NULL,
	[ClinicalFindings] [ntext] NULL,
	[NECLaparotomy] [nvarchar](100) NULL,
	[NECInspectionConfirm] [nvarchar](100) NULL,
	[NECHistologyConfirm] [nvarchar](100) NULL,
	[NECPeritonealdrain] [nvarchar](2) NULL,
	[TransferredForManagementNEC] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
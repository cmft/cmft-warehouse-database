﻿CREATE TABLE [NeonatalDetailed].[diagnosis](
	[BabyNHSNumber] [nvarchar](30) NULL,
	[Episode] [int] NULL,
	[EpisodePoint] [nvarchar](30) NULL,
	[BadgerCode] [int] NULL,
	[Diagnosis] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
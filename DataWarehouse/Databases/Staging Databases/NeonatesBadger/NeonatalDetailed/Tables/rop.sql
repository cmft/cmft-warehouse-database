﻿CREATE TABLE [NeonatalDetailed].[rop](
	[BabyNHSNumber] [nvarchar](30) NULL,
	[rectime] [datetime] NULL,
	[episode] [int] NULL,
	[UnitName] [nvarchar](150) NULL,
	[ropscreen_left] [nvarchar](100) NULL,
	[ropscreen_right] [nvarchar](100) NULL,
	[ropscreen_cryolaser] [nvarchar](100) NULL,
	[clockHours_right] [int] NULL,
	[clockHours_left] [int] NULL,
	[MaxZone_right] [int] NULL,
	[MaxZone_left] [int] NULL,
	[PlusDisease_right] [int] NULL,
	[PlusDisease_left] [int] NULL,
	[ROP_Outcome] [nvarchar](50) NULL,
	[ROP_Notes] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
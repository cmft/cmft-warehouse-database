﻿CREATE TABLE [NeonatalDetailed].[CranialUSS](
	[BabyNHSNumber] [nvarchar](30) NULL,
	[rectime] [datetime] NULL,
	[episode] [int] NULL,
	[UnitName] [nvarchar](150) NULL,
	[CranialUSSleft] [ntext] NULL,
	[CranialUSSright] [ntext] NULL,
	[LeftVentricularDilation] [nvarchar](10) NULL,
	[RightVentricularDilation] [nvarchar](10) NULL,
	[LeftPorencephalicCysts] [nvarchar](10) NULL,
	[RightPorencephalicCysts] [nvarchar](10) NULL,
	[post_haemorrhagic_hydrocephalus] [nvarchar](10) NULL,
	[CysticPvl] [nvarchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Secure].[Credentials](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[credentialInfo] [xml] NULL,
	[username] [sysname] NULL,
	[password] [sysname] NULL,
	[description] [varchar](max) NULL,
	[accessmethod] [varchar](256) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Secure].[Scripts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[brief] [varchar](64) NOT NULL,
	[information] [varchar](max) NOT NULL,
	[ts] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
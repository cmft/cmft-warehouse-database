﻿CREATE TABLE [dbo].[WorkflowStatus](
	[FormType] [varchar](250) NOT NULL,
	[WorkflowValue] [int] NOT NULL,
	[WorkflowStatus] [varchar](50) NOT NULL,
	[ViewName] [varchar](50) NOT NULL CONSTRAINT [DF_WorkflowStatus_ViewName]  DEFAULT ('Default'),
	[SuperUserViewName] [varchar](50) NOT NULL CONSTRAINT [DF_WorkflowStatus_SuperUserViewName]  DEFAULT ('Default')
) ON [PRIMARY]
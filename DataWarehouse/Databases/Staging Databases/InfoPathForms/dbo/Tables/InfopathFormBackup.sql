﻿CREATE TABLE [dbo].[InfopathFormBackup](
	[FormId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](255) NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[SecondaryId] [int] NULL,
	[FormType] [varchar](250) NOT NULL,
	[FormStatus] [varchar](255) NULL,
	[FormXML] [xml] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
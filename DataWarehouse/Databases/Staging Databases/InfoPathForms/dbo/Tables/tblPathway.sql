﻿CREATE TABLE [dbo].[tblPathway](
	[PathwayId] [int] IDENTITY(1,1) NOT NULL,
	[PathwayTypeId] [int] NOT NULL,
	[EPMINumber] [varchar](20) NOT NULL,
	[AttendanceId] [varchar](20) NULL,
	[ActivityType] [varchar](2) NOT NULL,
	[UserName] [varchar](255) NOT NULL,
	[PWClockStartEvent] [char](1) NULL,
	[PWClockStart] [datetime] NULL,
	[AltTriggerDate] [datetime] NULL,
	[AdmissionDate] [datetime] NULL,
	[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_tblPathway_DateAdded]  DEFAULT (getdate()),
	[Status] [char](1) NULL CONSTRAINT [DF_tblPathway_Status]  DEFAULT ('A'),
	[ClinicalStatus] [char](1) NULL
) ON [PRIMARY]
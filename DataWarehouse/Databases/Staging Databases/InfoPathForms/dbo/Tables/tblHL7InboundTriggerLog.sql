﻿CREATE TABLE [dbo].[tblHL7InboundTriggerLog](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[Object] [varchar](150) NULL,
	[Event] [varchar](50) NULL,
	[Result] [varchar](20) NULL,
	[Comment] [varchar](300) NULL,
	[UserName] [varchar](50) NOT NULL CONSTRAINT [DF_tblHL7MessageLog_UserName]  DEFAULT (suser_name()),
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_tblHL7MessageLog_TimeStamp]  DEFAULT (getdate())
) ON [PRIMARY]
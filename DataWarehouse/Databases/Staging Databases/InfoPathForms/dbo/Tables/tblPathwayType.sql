﻿CREATE TABLE [dbo].[tblPathwayType](
	[PathwayTypeId] [int] IDENTITY(1,1) NOT NULL,
	[PathwayType] [varchar](30) NOT NULL,
	[ClockStartDefaultEvent] [char](1) NULL,
	[ClockStartNewPathway] [bit] NULL,
	[ClockStartFirstForm] [bit] NULL,
	[AltTriggerDesc] [varchar](50) NULL,
	[Active] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[GenerateMissingClerking](
	[ProviderSpellNo] [nvarchar](20) NOT NULL,
	[EPMINumber] [nvarchar](20) NULL,
	[AdmissionDate] [datetime] NULL,
	[DischargeDate] [datetime] NULL,
	[AdmissionWardCode] [nvarchar](40) NULL,
	[DischargeWardCode] [nvarchar](40) NULL,
	[CreationDate] [datetime] NULL
) ON [PRIMARY]
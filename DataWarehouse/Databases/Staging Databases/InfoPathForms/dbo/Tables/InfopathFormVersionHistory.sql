﻿CREATE TABLE [dbo].[InfopathFormVersionHistory](
	[FormId] [int] NOT NULL,
	[UserName] [varchar](255) NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[SecondaryId] [int] NULL,
	[EPMINumber] [varchar](20) NULL,
	[ActivityType] [varchar](2) NULL,
	[OriginalCreationDate] [datetime] NULL,
	[Workflow] [int] NULL,
	[FormType] [varchar](250) NOT NULL,
	[FormStatus] [varchar](255) NULL,
	[FormXML] [xml] NOT NULL,
	[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_InfopathFormPreviousVersion_DateAdded]  DEFAULT (getdate()),
	[Deleted] [bit] NOT NULL CONSTRAINT [DF_InfopathFormPreviousVersion_Deleted]  DEFAULT ((0)),
	[OriginalSubmissionDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPathwayDef_v01](
	[PathwayDefID] [int] NOT NULL,
	[PathwayTypeID] [int] NOT NULL,
	[FormType] [varchar](250) NOT NULL,
	[FormViewPoint] [char](1) NULL,
	[LaunchFormAtPWStart] [bit] NULL,
	[ClockStartAtFormSubmit] [bit] NULL,
	[MinQty] [int] NOT NULL,
	[MaxQty] [int] NOT NULL,
	[Dependencies] [varchar](100) NULL,
	[MWE] [varchar](100) NULL,
	[ReqTimeFromStart] [varchar](10) NULL,
	[ReqMinsFromStart] [int] NULL,
	[EventHandler] [varchar](max) NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
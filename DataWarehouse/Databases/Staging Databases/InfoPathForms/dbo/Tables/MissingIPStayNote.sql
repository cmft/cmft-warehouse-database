﻿CREATE TABLE [dbo].[MissingIPStayNote](
	[Status] [varchar](12) NOT NULL,
	[DistrictNo] [nvarchar](20) NULL,
	[EPMInumber] [nvarchar](20) NULL,
	[ProviderspellNo] [nvarchar](20) NULL,
	[admissionDate] [datetime] NULL,
	[DischargeDate] [datetime] NULL,
	[Rundate] [datetime] NOT NULL
) ON [PRIMARY]
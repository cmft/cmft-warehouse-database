﻿CREATE TABLE [dbo].[MissingDocs](
	[Status] [varchar](12) NOT NULL,
	[CurrentNo] [varchar](20) NULL,
	[DocumentDate] [datetime] NULL,
	[AdditionalInformation] [varchar](500) NULL,
	[Rundate] [datetime] NOT NULL
) ON [PRIMARY]
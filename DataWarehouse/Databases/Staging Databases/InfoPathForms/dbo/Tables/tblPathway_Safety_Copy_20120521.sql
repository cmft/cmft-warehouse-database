﻿CREATE TABLE [dbo].[tblPathway_Safety_Copy_20120521](
	[PathwayId] [int] IDENTITY(1,1) NOT NULL,
	[PathwayTypeId] [int] NOT NULL,
	[EPMINumber] [varchar](20) NOT NULL,
	[AttendanceId] [varchar](20) NULL,
	[ActivityType] [varchar](2) NOT NULL,
	[UserName] [varchar](255) NOT NULL,
	[PWClockStartEvent] [char](1) NULL,
	[PWClockStart] [datetime] NULL,
	[AltTriggerDate] [datetime] NULL,
	[AdmissionDate] [datetime] NULL,
	[DateAdded] [datetime] NOT NULL,
	[Status] [char](1) NULL,
	[ClinicalStatus] [char](1) NULL
) ON [PRIMARY]
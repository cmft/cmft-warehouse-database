﻿CREATE TABLE [dbo].[tblPathwayDef](
	[PathwayDefID] [int] NOT NULL,
	[PathwayTypeID] [int] NOT NULL,
	[FormType] [varchar](250) NOT NULL,
	[AutoSubmitEvent] [varchar](128) NULL,
	[LaunchFormAtPWStart] [bit] NULL,
	[ClockStartAtFormSubmit] [bit] NULL,
	[MinQty] [int] NOT NULL CONSTRAINT [DF_tblPathwayDef_MinQty]  DEFAULT ((0)),
	[MaxQty] [int] NOT NULL CONSTRAINT [DF_tblPathwayDef_MaxQty]  DEFAULT ((0)),
	[Dependencies] [varchar](max) NULL,
	[MWE] [varchar](100) NULL,
	[ReqTimeFromStart] [varchar](10) NULL,
	[ReqMinsFromStart]  [int] null , --AS ([dbo].[fnGetMinutesFromStart]([ReqTimeFromStart])),
	[EventHandler] [varchar](max) NULL,
	[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_tblPathwayDef_DateAdded]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
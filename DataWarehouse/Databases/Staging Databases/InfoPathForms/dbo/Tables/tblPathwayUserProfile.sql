﻿CREATE TABLE [dbo].[tblPathwayUserProfile](
	[UserName] [varchar](250) NOT NULL,
	[CanCreatePathway] [char](1) NULL,
	[CanUpdatePWStatus] [char](1) NULL,
	[CanUpdateClinStatus] [char](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPathwayForm](
	[PathwayId] [int] NOT NULL,
	[FormId] [int] NOT NULL,
	[FormStatus] [char](1) NOT NULL CONSTRAINT [DF_tblPathwayForm_FormStatus]  DEFAULT ('C')
) ON [PRIMARY]
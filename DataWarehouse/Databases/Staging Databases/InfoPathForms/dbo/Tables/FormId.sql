﻿CREATE TABLE [dbo].[FormId](
	[FormType] [varchar](250) NOT NULL,
	[NextFormId] [int] NOT NULL CONSTRAINT [DF_FormId_NextDocId]  DEFAULT ((1))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[FormCategory](
	[CategoryCode] [int] NOT NULL,
	[ParentCategoryCode] [int] NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
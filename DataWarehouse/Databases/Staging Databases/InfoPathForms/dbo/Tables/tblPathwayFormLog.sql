﻿CREATE TABLE [dbo].[tblPathwayFormLog](
	[Unifier] [int] IDENTITY(1,1) NOT NULL,
	[LogId] [varchar](50) NOT NULL,
	[PathwayId] [int] NOT NULL,
	[FormId] [int] NULL,
	[FormDesc] [varchar](250) NOT NULL,
	[State] [varchar](10) NULL,
	[Mode] [varchar](50) NULL,
	[Username] [varchar](250) NOT NULL,
	[Hostname] [varchar](250) NULL,
	[IP] [varchar](20) NULL,
	[Created] [datetime] NOT NULL
) ON [PRIMARY]
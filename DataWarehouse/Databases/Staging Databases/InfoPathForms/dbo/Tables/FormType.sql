﻿CREATE TABLE [dbo].[FormType](
	[FormType] [varchar](250) NOT NULL,
	[Code] [varchar](3) NOT NULL,
	[CategoryCode] [int] NOT NULL,
	[XSNName] [varchar](250) NULL,
	[FileNameFormat] [varchar](250) NULL,
	[SharepointSite] [varchar](250) NULL,
	[SharepointLibrary] [varchar](250) NULL,
	[Description] [varchar](250) NULL,
	[EPRTableName] [varchar](50) NULL,
	[EPRAdditionalInformation] [varchar](50) NULL,
	[IP] [bit] NOT NULL CONSTRAINT [DF_FormType_IP]  DEFAULT ((0)),
	[OP] [bit] NOT NULL CONSTRAINT [DF_FormType_OP]  DEFAULT ((0)),
	[DefaultWorkflowValue] [int] NOT NULL,
	[ShowInTree] [bit] NOT NULL CONSTRAINT [DF_FormType_ShowInTree]  DEFAULT ((1)),
	[ShowExistingFormsOnPortal] [bit] NOT NULL CONSTRAINT [DF_FormType_ShowExistingFormsOnPortal]  DEFAULT ((1)),
	[KeyDate] [smallint] NOT NULL CONSTRAINT [DF_FormType_KeyDate]  DEFAULT ((2)),
	[SSOEnforce] [bit] NOT NULL CONSTRAINT [DF_FormType_SSOEnforce]  DEFAULT ((0))
) ON [PRIMARY]
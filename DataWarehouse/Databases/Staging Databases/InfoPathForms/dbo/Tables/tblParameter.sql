﻿CREATE TABLE [dbo].[tblParameter](
	[Parameter] [varchar](128) NOT NULL,
	[TextValue] [varchar](max) NULL,
	[NumericValue] [decimal](18, 0) NULL,
	[DateValue] [datetime] NULL,
	[FloatValue] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
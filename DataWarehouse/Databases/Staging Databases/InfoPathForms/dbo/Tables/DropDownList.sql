﻿CREATE TABLE [dbo].[DropDownList](
	[ListName] [varchar](50) NOT NULL,
	[ItemLabel] [varchar](255) NOT NULL,
	[ItemValue] [varchar](50) NOT NULL,
	[Ordering] [int] NULL,
	[Deleted] [bit] NOT NULL CONSTRAINT [DF_DropDownList_Deleted]  DEFAULT ((0))
) ON [PRIMARY]
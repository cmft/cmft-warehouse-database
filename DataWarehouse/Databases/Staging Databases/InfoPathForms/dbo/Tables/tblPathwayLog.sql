﻿CREATE TABLE [dbo].[tblPathwayLog](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[Object] [varchar](150) NULL,
	[Event] [varchar](200) NULL,
	[Comment] [varchar](200) NULL,
	[UserName] [varchar](50) NOT NULL CONSTRAINT [DF_tblPathwayLog_UserName]  DEFAULT (suser_name()),
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_tblPathwayLog_TimeStamp]  DEFAULT (getdate())
) ON [PRIMARY]
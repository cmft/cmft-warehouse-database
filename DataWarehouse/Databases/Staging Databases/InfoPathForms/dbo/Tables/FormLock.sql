﻿CREATE TABLE [dbo].[FormLock](
	[LockId] [uniqueidentifier] NOT NULL,
	[FormLibrary] [varchar](250) NOT NULL,
	[FormFileName] [varchar](250) NOT NULL,
	[Username] [varchar](250) NOT NULL,
	[Hostname] [varchar](250) NOT NULL,
	[IP] [varchar](20) NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_FormLock_Created]  DEFAULT (getdate())
) ON [PRIMARY]
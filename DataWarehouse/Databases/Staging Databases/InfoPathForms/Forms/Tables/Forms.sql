﻿CREATE TABLE [Forms].[Forms](
	[FormId] [int] NOT NULL,
	[UserName] [varchar](255) NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[SecondaryId] [int] NULL,
	[EPMINumber] [varchar](20) NULL,
	[ActivityType] [varchar](2) NULL,
	[OriginalCreationDate] [datetime] NULL,
	[Workflow] [int] NULL,
	[FormType] [varchar](250) NOT NULL,
	[FormStatus] [varchar](255) NULL,
	[FormXML] [nvarchar](max) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[OriginalSubmissionDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
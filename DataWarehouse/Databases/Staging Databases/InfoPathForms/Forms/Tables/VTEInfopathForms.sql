﻿CREATE TABLE [Forms].[VTEInfopathForms](
	[FormId] [int] NULL,
	[UserName] [varchar](255) NULL,
	[AdmissionNumber] [varchar](20) NULL,
	[SecondaryId] [int] NULL,
	[EPMINumber] [varchar](20) NULL,
	[ActivityType] [varchar](2) NULL,
	[OriginalCreationDate] [datetime] NULL,
	[Workflow] [int] NULL,
	[FormType] [varchar](250) NULL,
	[FormStatus] [varchar](255) NULL,
	[FormXML] [varchar](max) NULL,
	[DateAdded] [datetime] NULL,
	[Deleted] [bit] NULL,
	[OriginalSubmissionDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
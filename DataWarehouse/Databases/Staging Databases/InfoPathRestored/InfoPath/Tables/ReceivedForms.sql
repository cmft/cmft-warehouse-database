﻿CREATE TABLE [InfoPath].[ReceivedForms](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReceivedAt] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[FormTypeID] [int] NOT NULL DEFAULT ((-1)),
	[FormXML] [xml] NOT NULL,
	[Processed] [bit] NULL DEFAULT ((0)),
	[ProcessedAt] [datetime2](7) NULL,
	[AdditionalInformation] [xml] NULL,
	[LastUpdatedAt] [datetime2](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
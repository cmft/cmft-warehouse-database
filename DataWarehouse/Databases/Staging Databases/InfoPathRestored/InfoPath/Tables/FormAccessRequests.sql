﻿CREATE TABLE [InfoPath].[FormAccessRequests](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[formID] [int] NOT NULL,
	[userName] [sysname] NOT NULL,
	[accessLevel] [varchar](26) NOT NULL,
	[supportingInfo] [varchar](max) NULL,
	[requestDateTime] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
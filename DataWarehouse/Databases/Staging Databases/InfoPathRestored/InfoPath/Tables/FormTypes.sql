﻿CREATE TABLE [InfoPath].[FormTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormTypeName] [nvarchar](255) NOT NULL,
	[ApplicationID] [int] NOT NULL DEFAULT ((-1)),
	[SubmissionXMLSchema] [xml] NULL,
	[IsSimpleType] [bit] NOT NULL DEFAULT (CONVERT([bit],(0),0)),
	[baseUrl] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
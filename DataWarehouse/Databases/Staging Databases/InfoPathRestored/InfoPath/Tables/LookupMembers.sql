﻿CREATE TABLE [InfoPath].[LookupMembers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LookupID] [int] NOT NULL,
	[LookupOrder] [int] NOT NULL,
	[Value] [nvarchar](255) NOT NULL,
	[DisplayValue] [nvarchar](255) NOT NULL,
	[LookupFilterValue] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [InfoPath].[Lookups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LookupName] [nvarchar](255) NOT NULL,
	[ApplicationID] [int] NOT NULL
) ON [PRIMARY]
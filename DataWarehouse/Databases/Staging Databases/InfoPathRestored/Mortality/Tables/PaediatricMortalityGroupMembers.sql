﻿CREATE TABLE [Mortality].[PaediatricMortalityGroupMembers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MemberName] [varchar](128) NOT NULL,
	[Title] [varchar](256) NOT NULL,
	[EmailTo] [varchar](255) NOT NULL,
	[CC_Name] [varchar](128) NULL,
	[CC_EmailTo] [varchar](255) NULL,
	[CC_ExtNo] [int] NULL,
	[isReviewer] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
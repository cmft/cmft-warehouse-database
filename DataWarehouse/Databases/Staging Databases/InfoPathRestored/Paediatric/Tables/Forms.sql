﻿CREATE TABLE [Paediatric].[Forms](
	[id] [int] NOT NULL,
	[FormTypeID] [int] NOT NULL,
	[casenote] [varchar](12) NOT NULL,
	[surname] [varchar](64) NOT NULL,
	[createdAt] [datetime2](7) NOT NULL DEFAULT (getutcdate()),
	[lastUpdated] [datetime2](7) NULL,
	[isComplete] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
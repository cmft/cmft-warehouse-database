﻿CREATE TABLE [cmft].[Role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [cmft].[name] NOT NULL,
	[description] [cmft].[description] NOT NULL
) ON [PRIMARY]
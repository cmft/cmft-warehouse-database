﻿CREATE TABLE [cmft].[Person](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstName] [cmft].[name] NOT NULL,
	[lastName] [cmft].[name] NOT NULL,
	[emailName] [cmft].[name] NOT NULL,
	[networkLogin] [nvarchar](255) NULL,
	[loginDomain] [nvarchar](255) NULL,
	[emailAddress] [nvarchar](255) NOT NULL
) ON [PRIMARY]
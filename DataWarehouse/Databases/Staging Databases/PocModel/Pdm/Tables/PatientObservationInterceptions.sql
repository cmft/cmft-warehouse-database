﻿CREATE TABLE [Pdm].[PatientObservationInterceptions](
	[Id] [uniqueidentifier] NOT NULL,
	[Created] [datetimeoffset](7) NOT NULL,
	[Status] [int] NOT NULL,
	[StatusChangedAt] [datetimeoffset](7) NULL,
	[Observation_Id] [uniqueidentifier] NOT NULL,
	[StateChangedBy_Id] [uniqueidentifier] NULL,
	[RejectReason] [nvarchar](max) NULL,
	[ScheduledRetryCount] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Pdm].[PatientObservationInterceptionRules](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Summary] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Type] [int] NOT NULL,
	[Severity] [int] NOT NULL,
	[Expression] [ntext] NOT NULL,
	[Interupting] [bit] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[ExecutionOrder] [int] NOT NULL,
	[Created] [datetimeoffset](7) NOT NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
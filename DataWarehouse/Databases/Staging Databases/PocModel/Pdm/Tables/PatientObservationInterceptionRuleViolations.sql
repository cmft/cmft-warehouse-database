﻿CREATE TABLE [Pdm].[PatientObservationInterceptionRuleViolations](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Summary] [nvarchar](max) NOT NULL,
	[Type] [int] NOT NULL,
	[Severity] [int] NOT NULL,
	[Interupting] [bit] NOT NULL,
	[ExecutionOrder] [int] NOT NULL,
	[Created] [datetimeoffset](7) NOT NULL,
	[Interception_Id] [uniqueidentifier] NOT NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
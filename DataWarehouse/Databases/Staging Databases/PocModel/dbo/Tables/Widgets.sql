﻿CREATE TABLE [dbo].[Widgets](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Header] [nvarchar](128) NOT NULL,
	[Parameter] [nvarchar](128) NULL,
	[Row] [int] NOT NULL,
	[Column] [int] NOT NULL,
	[Height] [nvarchar](128) NULL,
	[Width] [nvarchar](128) NULL,
	[Dashboard_Id] [uniqueidentifier] NOT NULL,
	[WidgetType_Id] [uniqueidentifier] NULL
) ON [PRIMARY]
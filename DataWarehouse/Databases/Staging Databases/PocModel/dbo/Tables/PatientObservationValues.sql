﻿CREATE TABLE [dbo].[PatientObservationValues](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[ValueString] [nvarchar](128) NULL,
	[ValueType] [nvarchar](128) NULL,
	[HasMeasuringError] [bit] NOT NULL,
	[LowerReferenceRange] [float] NULL,
	[UpperReferenceRange] [float] NULL,
	[LowerCriticalRange] [float] NULL,
	[UpperCriticalRange] [float] NULL,
	[Parameter_Id] [uniqueidentifier] NOT NULL,
	[PatientObservation_Id] [uniqueidentifier] NOT NULL,
	[ObservationMethod] [int] NOT NULL DEFAULT ((0)),
	[ValueInterpretation] [int] NOT NULL DEFAULT ((0)),
	[RangeIndicator] [int] NOT NULL DEFAULT ((0)),
	[SignificantFigure] [int] NOT NULL DEFAULT ((4)),
	[Visible] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
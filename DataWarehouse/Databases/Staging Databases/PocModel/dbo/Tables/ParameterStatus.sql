﻿CREATE TABLE [dbo].[ParameterStatus](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Disabled] [bit] NOT NULL,
	[Locked] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Device_Id] [uniqueidentifier] NOT NULL,
	[Parameter_Id] [uniqueidentifier] NOT NULL,
	[Status] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
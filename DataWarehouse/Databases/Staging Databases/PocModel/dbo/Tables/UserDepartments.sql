﻿CREATE TABLE [dbo].[UserDepartments](
	[UserId] [uniqueidentifier] NOT NULL,
	[DepartmentId] [uniqueidentifier] NOT NULL,
	[DepartmentScope] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SystemRangeSettings](
	[Id] [uniqueidentifier] NOT NULL,
	[MeasurementCount] [int] NOT NULL,
	[Factor] [float] NOT NULL,
	[FixedSd] [float] NOT NULL,
	[IsUsingFixSd] [bit] NOT NULL,
	[ForceIssuesBasedOnEvaluation] [bit] NOT NULL,
	[DeviceType_Id] [uniqueidentifier] NOT NULL,
	[Parameter_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
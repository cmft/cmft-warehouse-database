﻿CREATE TABLE [dbo].[Patients](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[PatientId] [nvarchar](255) NULL,
	[AccessionNumber] [nvarchar](max) NULL,
	[AlternativePatientId] [nvarchar](max) NULL,
	[Prefix] [nvarchar](128) NULL,
	[FirstName] [nvarchar](512) NULL,
	[MiddleName] [nvarchar](512) NULL,
	[LastName] [nvarchar](512) NULL,
	[DateTimeOfBirth] [datetimeoffset](7) NULL,
	[Note] [nvarchar](max) NULL,
	[Sex] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[DeviceTypeProfiles](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](255) NOT NULL,
	[DeviceType_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
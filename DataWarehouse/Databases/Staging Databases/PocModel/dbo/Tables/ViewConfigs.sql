﻿CREATE TABLE [dbo].[ViewConfigs](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](512) NOT NULL,
	[View] [nvarchar](512) NULL,
	[Default] [bit] NOT NULL,
	[SortingConfig] [nvarchar](512) NULL,
	[GroupingConfig] [nvarchar](512) NULL,
	[FilterConfig] [nvarchar](512) NULL,
	[PreventDelete] [bit] NOT NULL,
	[PageSize] [int] NOT NULL,
	[User_Id] [uniqueidentifier] NULL
) ON [PRIMARY]
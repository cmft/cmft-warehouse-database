﻿CREATE TABLE [dbo].[Dashboards](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[User_Id] [uniqueidentifier] NULL,
	[DashboardLayout_Id] [uniqueidentifier] NULL,
	[DashboardType] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
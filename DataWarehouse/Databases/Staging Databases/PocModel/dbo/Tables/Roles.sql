﻿CREATE TABLE [dbo].[Roles](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
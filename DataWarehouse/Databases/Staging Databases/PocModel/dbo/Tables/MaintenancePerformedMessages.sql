﻿CREATE TABLE [dbo].[MaintenancePerformedMessages](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Note] [nvarchar](max) NULL,
	[PerformedDate] [datetimeoffset](7) NOT NULL,
	[Device_Id] [uniqueidentifier] NOT NULL,
	[Operator_Id] [uniqueidentifier] NULL,
	[AmbiguousTimezoneName] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Devices](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[InstallationNumber] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DisplayName] [nvarchar](max) NULL,
	[DeviceSoftwareVersion] [nvarchar](max) NULL,
	[Hidden] [bit] NOT NULL,
	[IpAddress] [nvarchar](max) NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL,
	[DeviceType_Id] [uniqueidentifier] NOT NULL,
	[Department_Id] [uniqueidentifier] NULL,
	[Responsible_Id] [uniqueidentifier] NULL,
	[Status] [int] NOT NULL DEFAULT ((0)),
	[DeviceName]  AS (isnull(isnull(nullif([DisplayName],''),nullif([Name],'')),nullif([InstallationNumber],'')))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
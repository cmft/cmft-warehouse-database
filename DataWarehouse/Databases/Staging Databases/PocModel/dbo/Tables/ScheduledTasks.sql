﻿CREATE TABLE [dbo].[ScheduledTasks](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[TimeStamp] [datetimeoffset](7) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DashboardColumns](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Width] [nvarchar](128) NULL,
	[Column] [int] NOT NULL,
	[DashboardLayout_Id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[QualityControlObservations](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[ObservationTime] [datetimeoffset](7) NOT NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL,
	[IsAnyObservationValueViolatingControlRanges] [bit] NOT NULL,
	[SampleNumber] [nvarchar](128) NULL,
	[Note] [nvarchar](512) NULL,
	[ReagentLot_Id] [uniqueidentifier] NOT NULL,
	[Device_Id] [uniqueidentifier] NOT NULL,
	[Operator_Id] [uniqueidentifier] NULL,
	[QualityControlType] [int] NOT NULL DEFAULT ((0)),
	[Status] [int] NOT NULL DEFAULT ((0)),
	[TimeReceived] [datetimeoffset](7) NOT NULL DEFAULT ('0001-01-01T00:00:00.000+00:00'),
	[HasMeasuringErrors] [bit] NOT NULL DEFAULT ((0)),
	[AmbiguousTimezoneName] [nvarchar](max) NULL,
	[IsPossibleDuplicate] [bit] NOT NULL DEFAULT ((0)),
	[HasPossibleDuplicate] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
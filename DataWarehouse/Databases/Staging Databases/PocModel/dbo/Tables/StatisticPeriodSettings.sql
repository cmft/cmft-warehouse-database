﻿CREATE TABLE [dbo].[StatisticPeriodSettings](
	[Id] [uniqueidentifier] NOT NULL,
	[MeasurementCount] [int] NOT NULL,
	[ForceIssuesBasedOnEvaluation] [bit] NOT NULL,
	[DeviceType_Id] [uniqueidentifier] NOT NULL,
	[Parameter_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
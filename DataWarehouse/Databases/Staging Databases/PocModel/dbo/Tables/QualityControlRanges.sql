﻿CREATE TABLE [dbo].[QualityControlRanges](
	[Id] [uniqueidentifier] NOT NULL,
	[IsCalculated] [bit] NOT NULL,
	[IsUsingFixSd] [bit] NOT NULL,
	[Low] [float] NOT NULL,
	[High] [float] NOT NULL,
	[LowerManufacturerRange] [float] NULL,
	[UpperManufacturerRange] [float] NULL,
	[StartDate] [datetimeoffset](7) NOT NULL,
	[EndDate] [datetimeoffset](7) NOT NULL,
	[Device_Id] [uniqueidentifier] NOT NULL,
	[Parameter_Id] [uniqueidentifier] NOT NULL,
	[ReagentLot_Id] [uniqueidentifier] NOT NULL,
	[EvaluatedAgainst] [int] NOT NULL,
	[QualityControlSystem] [int] NOT NULL
) ON [PRIMARY]
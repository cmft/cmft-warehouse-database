﻿CREATE TABLE [dbo].[DevicePropertyBlobs](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Data] [nvarchar](max) NOT NULL,
	[Received] [datetimeoffset](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
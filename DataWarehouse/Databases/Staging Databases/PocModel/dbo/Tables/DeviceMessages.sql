﻿CREATE TABLE [dbo].[DeviceMessages](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[MessageId] [nvarchar](255) NOT NULL,
	[Message] [nvarchar](2048) NOT NULL,
	[Timestamp] [datetimeoffset](7) NOT NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL,
	[Device_Id] [uniqueidentifier] NOT NULL,
	[Severity] [int] NOT NULL DEFAULT ((0)),
	[AmbiguousTimezoneName] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
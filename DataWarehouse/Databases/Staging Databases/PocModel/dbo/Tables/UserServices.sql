﻿CREATE TABLE [dbo].[UserServices](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](64) NOT NULL,
	[Expiry] [datetimeoffset](7) NULL,
	[User_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
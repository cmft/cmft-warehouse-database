﻿CREATE TABLE [dbo].[RuleSets](
	[Id] [uniqueidentifier] NOT NULL,
	[Type] [int] NOT NULL,
	[Expression] [ntext] NOT NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
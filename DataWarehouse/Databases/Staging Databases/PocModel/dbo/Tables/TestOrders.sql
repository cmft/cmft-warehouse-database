﻿CREATE TABLE [dbo].[TestOrders](
	[Id] [uniqueidentifier] NOT NULL,
	[AccessionNumber] [nvarchar](128) NOT NULL,
	[Created] [datetimeoffset](7) NOT NULL,
	[Modified] [datetimeoffset](7) NOT NULL,
	[Operator_Id] [uniqueidentifier] NULL,
	[Patient_Id] [uniqueidentifier] NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
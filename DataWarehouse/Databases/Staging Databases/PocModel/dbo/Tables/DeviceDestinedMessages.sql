﻿CREATE TABLE [dbo].[DeviceDestinedMessages](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[OriginatorId] [uniqueidentifier] NOT NULL,
	[DeliveredTime] [datetimeoffset](7) NULL,
	[SubIdentifier] [nvarchar](max) NULL,
	[Created] [datetimeoffset](7) NOT NULL,
	[SerializedMessage] [nvarchar](max) NOT NULL,
	[DeliveryRequestedBy_Id] [uniqueidentifier] NOT NULL,
	[MessageState] [int] NOT NULL DEFAULT ((0)),
	[MessageType] [int] NOT NULL DEFAULT ((0)),
	[AttemptedDeliveryCount] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[CalibrationObservations](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[ObservationTime] [datetimeoffset](7) NOT NULL,
	[SampleNumber] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NOT NULL,
	[Note] [nvarchar](512) NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL,
	[Device_Id] [uniqueidentifier] NOT NULL,
	[Operator_Id] [uniqueidentifier] NULL,
	[Status] [int] NOT NULL DEFAULT ((0)),
	[TimeReceived] [datetimeoffset](7) NOT NULL DEFAULT ('0001-01-01T00:00:00.000+00:00'),
	[AmbiguousTimezoneName] [nvarchar](max) NULL,
	[IsPossibleDuplicate] [bit] NOT NULL DEFAULT ((0)),
	[HasPossibleDuplicate] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
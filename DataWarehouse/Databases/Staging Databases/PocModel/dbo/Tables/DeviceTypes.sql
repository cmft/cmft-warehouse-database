﻿CREATE TABLE [dbo].[DeviceTypes](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](255) NOT NULL,
	[IsUnitUse] [bit] NOT NULL
) ON [PRIMARY]
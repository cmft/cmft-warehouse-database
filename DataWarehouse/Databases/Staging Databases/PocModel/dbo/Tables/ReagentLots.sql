﻿CREATE TABLE [dbo].[ReagentLots](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[LotNumber] [nvarchar](255) NOT NULL,
	[ExpirationDate] [datetimeoffset](7) NULL,
	[Reagent_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
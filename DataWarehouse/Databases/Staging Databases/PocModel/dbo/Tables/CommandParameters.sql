﻿CREATE TABLE [dbo].[CommandParameters](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](256) NOT NULL,
	[DataType] [nvarchar](512) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[Command_Id] [uniqueidentifier] NOT NULL,
	[SortOrder] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Departments](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](255) NOT NULL,
	[Hospital_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ErrorMessages](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Code] [nvarchar](256) NULL,
	[Text] [nvarchar](max) NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL,
	[QualityControlObservationValue_Id] [uniqueidentifier] NULL,
	[QualityControlObservation_Id] [uniqueidentifier] NULL,
	[StatisticsPeriod_Id] [uniqueidentifier] NULL,
	[CalibrationObservationValue_Id] [uniqueidentifier] NULL,
	[CalibrationObservation_Id] [uniqueidentifier] NULL,
	[PatientObservationValue_Id] [uniqueidentifier] NULL,
	[PatientObservation_Id] [uniqueidentifier] NULL,
	[Severity] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
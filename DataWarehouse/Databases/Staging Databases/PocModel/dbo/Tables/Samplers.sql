﻿CREATE TABLE [dbo].[Samplers](
	[Id] [uniqueidentifier] NOT NULL,
	[SamplerId] [nvarchar](255) NOT NULL,
	[ParameterProfile_Id] [uniqueidentifier] NULL,
	[TestOrder_Id] [uniqueidentifier] NULL
) ON [PRIMARY]
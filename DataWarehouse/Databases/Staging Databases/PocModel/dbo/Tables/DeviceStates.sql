﻿CREATE TABLE [dbo].[DeviceStates](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[TimeStamp] [datetimeoffset](7) NOT NULL,
	[Reason] [nvarchar](max) NOT NULL,
	[TimeUntilIdle] [time](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
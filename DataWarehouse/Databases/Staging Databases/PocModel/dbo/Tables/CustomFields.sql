﻿CREATE TABLE [dbo].[CustomFields](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
	[PatientObservation_Id] [uniqueidentifier] NULL,
	[QualityControlObservationValue_Id] [uniqueidentifier] NULL,
	[QualityControlObservation_Id] [uniqueidentifier] NULL,
	[PatientObservationValue_Id] [uniqueidentifier] NULL,
	[Patient_Id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
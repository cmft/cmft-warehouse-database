﻿CREATE TABLE [dbo].[ConversionOverrides](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[TargetUnit] [nvarchar](30) NOT NULL,
	[RangeStart] [float] NOT NULL,
	[RangeEnd] [float] NOT NULL,
	[ConversionFactor] [float] NULL,
	[ConversionOffset] [float] NULL,
	[SigFigures] [int] NULL,
	[NumDecimals] [int] NULL,
	[Parameter_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
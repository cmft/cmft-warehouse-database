﻿CREATE TABLE [dbo].[MaintenanceItems](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[MaintenanceItemType] [int] NOT NULL,
	[ExpirationDate] [datetimeoffset](7) NOT NULL,
	[Lot] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
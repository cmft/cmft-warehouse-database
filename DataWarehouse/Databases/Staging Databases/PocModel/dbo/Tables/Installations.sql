﻿CREATE TABLE [dbo].[Installations](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerKey] [nvarchar](max) NULL,
	[InstallationKey] [nvarchar](max) NULL,
	[CreatedDate] [datetimeoffset](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
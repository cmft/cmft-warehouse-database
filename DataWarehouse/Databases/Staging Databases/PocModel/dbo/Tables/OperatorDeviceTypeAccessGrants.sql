﻿CREATE TABLE [dbo].[OperatorDeviceTypeAccessGrants](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[AccessGranted] [bit] NOT NULL,
	[ExpiryDate] [datetimeoffset](7) NULL,
	[Profile_Id] [uniqueidentifier] NOT NULL,
	[GrantingUser_Id] [uniqueidentifier] NOT NULL,
	[Operator_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
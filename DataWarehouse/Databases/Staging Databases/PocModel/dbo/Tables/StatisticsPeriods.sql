﻿CREATE TABLE [dbo].[StatisticsPeriods](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[StartDate] [datetimeoffset](7) NOT NULL,
	[EndDate] [datetimeoffset](7) NOT NULL,
	[IsIssueEvaluationDone] [bit] NOT NULL,
	[Count] [int] NOT NULL,
	[Mean] [float] NOT NULL,
	[StandardDeviation] [float] NOT NULL,
	[CvPercent] [float] NOT NULL,
	[RootMeanSquareDeviation] [float] NOT NULL,
	[MaxRootMeanSquareDeviation] [float] NOT NULL,
	[Bias] [float] NOT NULL,
	[PeriodType] [nvarchar](512) NOT NULL,
	[Lot2DateSd] [float] NULL,
	[Lot2DateMean] [float] NULL,
	[LowerManufacturerRange] [float] NULL,
	[UpperManufacturerRange] [float] NULL,
	[Parameter_Id] [uniqueidentifier] NOT NULL,
	[Device_Id] [uniqueidentifier] NOT NULL,
	[ReagentLot_Id] [uniqueidentifier] NOT NULL,
	[EvaluatedState] [int] NOT NULL DEFAULT ((0)),
	[EvaluatedAgainst] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
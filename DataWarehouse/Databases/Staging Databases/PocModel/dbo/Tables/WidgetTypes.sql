﻿CREATE TABLE [dbo].[WidgetTypes](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](256) NOT NULL,
	[DisplayNameResourceKey] [nvarchar](1024) NOT NULL,
	[Controller] [nvarchar](256) NULL,
	[Action] [nvarchar](1024) NULL,
	[ValidForDashboardType] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
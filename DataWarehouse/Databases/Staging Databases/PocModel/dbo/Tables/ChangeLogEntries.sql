﻿CREATE TABLE [dbo].[ChangeLogEntries](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Column] [nvarchar](max) NULL,
	[OldValue] [nvarchar](max) NULL,
	[NewValue] [nvarchar](max) NULL,
	[ChangeLog_Id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
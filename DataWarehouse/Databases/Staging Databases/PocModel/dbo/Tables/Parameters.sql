﻿CREATE TABLE [dbo].[Parameters](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](30) NOT NULL,
	[UnitOfMeasure] [nvarchar](30) NOT NULL,
	[DisplayUnit] [nvarchar](30) NOT NULL,
	[ParameterType] [nvarchar](16) NOT NULL,
	[Order] [int] NOT NULL DEFAULT ((0)),
	[DeviceType_Id] [uniqueidentifier] NOT NULL,
	[CalculateStatistics] [bit] NOT NULL DEFAULT ((0)),
	[DisplayLayoutSection] [nvarchar](max) NOT NULL DEFAULT ('')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
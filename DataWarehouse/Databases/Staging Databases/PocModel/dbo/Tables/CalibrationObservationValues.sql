﻿CREATE TABLE [dbo].[CalibrationObservationValues](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Dimension] [nvarchar](max) NULL,
	[ValueString] [nvarchar](128) NULL,
	[ValueType] [nvarchar](128) NULL,
	[HasMeasuringError] [bit] NOT NULL,
	[LowerRange] [float] NULL,
	[UpperRange] [float] NULL,
	[BaseParameter_Id] [uniqueidentifier] NOT NULL,
	[CalibrationObservation_Id] [uniqueidentifier] NOT NULL,
	[ObservationMethod] [int] NOT NULL DEFAULT ((0)),
	[ValueInterpretation] [int] NOT NULL DEFAULT ((0)),
	[RangeIndicatorFromDevice] [int] NOT NULL DEFAULT ((0)),
	[SignificantFigure] [int] NOT NULL DEFAULT ((4)),
	[Parameter_Id] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[Visible] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
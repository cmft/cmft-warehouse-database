﻿CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[Username] [nvarchar](255) NOT NULL,
	[PasswordDevice] [nvarchar](255) NULL,
	[Barcode] [nvarchar](128) NULL,
	[Prefix] [nvarchar](32) NULL,
	[FirstName] [nvarchar](255) NULL,
	[MiddleName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[PhoneNumber] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[IsEnabled] [bit] NOT NULL
) ON [PRIMARY]
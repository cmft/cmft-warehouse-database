﻿CREATE TABLE [dbo].[RangeUsageDefinedBySettings](
	[Id] [uniqueidentifier] NOT NULL,
	[DeviceType_Id] [uniqueidentifier] NOT NULL,
	[Parameter_Id] [uniqueidentifier] NOT NULL,
	[RangeUsageDefinedByType] [int] NOT NULL
) ON [PRIMARY]
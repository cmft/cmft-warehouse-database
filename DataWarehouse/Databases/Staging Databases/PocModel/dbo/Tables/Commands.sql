﻿CREATE TABLE [dbo].[Commands](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Active] [bit] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Method] [nvarchar](255) NOT NULL,
	[Device_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
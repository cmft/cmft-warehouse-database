﻿CREATE TABLE [dbo].[Comments](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[LinkedTo_EntityName] [nvarchar](max) NULL,
	[LinkedTo_EntityId] [uniqueidentifier] NULL,
	[PostedOn] [datetimeoffset](7) NOT NULL,
	[Content] [nvarchar](4000) NOT NULL,
	[PostedBy_Id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
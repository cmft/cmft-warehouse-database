﻿CREATE TABLE [dbo].[OperatorObservationStatus](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[PeriodStart] [datetimeoffset](7) NOT NULL,
	[PeriodEnd] [datetimeoffset](7) NOT NULL,
	[QcResultCount] [int] NOT NULL,
	[FailedQcResultCount] [int] NOT NULL,
	[PatientResultCount] [int] NOT NULL,
	[FailedPatientResultCount] [int] NOT NULL,
	[AcceptableQcResultCount] [int] NOT NULL,
	[AcceptableFailedQcResultCount] [int] NOT NULL,
	[AcceptablePatientResultCount] [int] NOT NULL,
	[AcceptableFailedPatientResultCount] [int] NOT NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NOT NULL,
	[Operator_Id] [uniqueidentifier] NULL,
	[DeviceType_Id] [uniqueidentifier] NULL
) ON [PRIMARY]
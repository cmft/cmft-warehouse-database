﻿CREATE TABLE [dbo].[Hospitals](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](128) NOT NULL
) ON [PRIMARY]
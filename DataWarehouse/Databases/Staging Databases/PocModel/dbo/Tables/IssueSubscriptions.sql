﻿CREATE TABLE [dbo].[IssueSubscriptions](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Subscribed] [bit] NOT NULL,
	[Issue_Id] [uniqueidentifier] NOT NULL,
	[User_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
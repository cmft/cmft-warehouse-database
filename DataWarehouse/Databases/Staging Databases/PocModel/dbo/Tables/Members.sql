﻿CREATE TABLE [dbo].[Members](
	[Id] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NULL,
	[PasswordSalt] [nvarchar](128) NULL,
	[PasswordFailedCount] [int] NOT NULL,
	[PasswordFailedWindowStart] [datetimeoffset](7) NULL,
	[IsLockedOut] [bit] NOT NULL,
	[IsApproved] [bit] NOT NULL,
	[Created] [datetimeoffset](7) NOT NULL,
	[LastLogin] [datetimeoffset](7) NULL,
	[LastActivity] [datetimeoffset](7) NULL,
	[LastPasswordChanged] [datetimeoffset](7) NULL,
	[LastLogout] [datetimeoffset](7) NULL
) ON [PRIMARY]
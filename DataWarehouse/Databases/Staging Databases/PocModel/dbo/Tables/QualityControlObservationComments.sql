﻿CREATE TABLE [dbo].[QualityControlObservationComments](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Comment] [nvarchar](2048) NULL,
	[LastUpdatedUtcTime] [datetimeoffset](7) NULL,
	[User_Id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ChangeLogs](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[RevisionStamp] [datetimeoffset](7) NOT NULL,
	[LinkedTo_EntityName] [nvarchar](max) NULL,
	[LinkedTo_EntityId] [uniqueidentifier] NULL,
	[Action] [nvarchar](max) NULL,
	[RevisionedBy_Id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
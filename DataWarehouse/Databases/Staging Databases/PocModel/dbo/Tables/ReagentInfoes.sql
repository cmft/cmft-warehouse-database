﻿CREATE TABLE [dbo].[ReagentInfoes](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Identifier] [nvarchar](400) NOT NULL,
	[ProductId] [nvarchar](200) NOT NULL,
	[LevelId] [nvarchar](200) NOT NULL,
	[RunningTime] [int] NULL
) ON [PRIMARY]
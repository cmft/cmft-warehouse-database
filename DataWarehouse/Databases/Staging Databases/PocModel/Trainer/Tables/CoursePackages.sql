﻿CREATE TABLE [Trainer].[CoursePackages](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[PackageType] [nvarchar](max) NOT NULL,
	[Schema] [nvarchar](64) NOT NULL,
	[SchemaVersion] [nvarchar](64) NOT NULL,
	[EntryPoint] [nvarchar](max) NOT NULL,
	[Keyword] [nvarchar](max) NULL,
	[Contents] [varbinary](max) NOT NULL,
	[Created] [datetimeoffset](7) NOT NULL,
	[Course_Id] [uniqueidentifier] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
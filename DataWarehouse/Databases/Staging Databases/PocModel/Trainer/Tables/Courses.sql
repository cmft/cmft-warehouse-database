﻿CREATE TABLE [Trainer].[Courses](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[IsHidden] [bit] NOT NULL DEFAULT ((0)),
	[IsPublished] [bit] NOT NULL DEFAULT ((0)),
	[Created] [datetimeoffset](7) NOT NULL DEFAULT ('0001-01-01T00:00:00.000+00:00')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
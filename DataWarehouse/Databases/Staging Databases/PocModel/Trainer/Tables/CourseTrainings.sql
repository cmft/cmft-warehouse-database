﻿CREATE TABLE [Trainer].[CourseTrainings](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Course_Id] [uniqueidentifier] NOT NULL,
	[Assignee_Id] [uniqueidentifier] NOT NULL,
	[AssignedBy_Id] [uniqueidentifier] NOT NULL,
	[Due] [datetimeoffset](7) NULL,
	[Duration] [nvarchar](max) NULL,
	[ScoreRaw] [real] NOT NULL,
	[Score] [real] NOT NULL,
	[CommittedAt] [datetimeoffset](7) NULL,
	[Created] [datetimeoffset](7) NOT NULL DEFAULT ('0001-01-01T00:00:00.000+00:00'),
	[StatusSuccess] [nvarchar](max) NOT NULL DEFAULT (''),
	[StatusCompletion] [nvarchar](max) NOT NULL DEFAULT (''),
	[Package_Id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
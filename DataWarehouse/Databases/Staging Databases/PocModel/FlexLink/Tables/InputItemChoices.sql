﻿CREATE TABLE [FlexLink].[InputItemChoices](
	[Id] [uniqueidentifier] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[InputItem_Id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
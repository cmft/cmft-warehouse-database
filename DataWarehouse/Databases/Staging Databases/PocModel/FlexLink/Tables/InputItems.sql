﻿CREATE TABLE [FlexLink].[InputItems](
	[Id] [uniqueidentifier] NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Format] [nvarchar](max) NULL,
	[IsRequired] [bit] NOT NULL,
	[IsReadOnly] [bit] NOT NULL,
	[DefaultValue] [nvarchar](max) NULL,
	[ParameterProfile_Id] [uniqueidentifier] NULL,
	[Type] [int] NOT NULL,
	[ProfileParameter_Id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
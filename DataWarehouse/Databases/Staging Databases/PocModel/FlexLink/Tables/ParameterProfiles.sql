﻿CREATE TABLE [FlexLink].[ParameterProfiles](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [nvarchar](max) NOT NULL,
	[SortOrder] [int] NOT NULL DEFAULT ((0)),
	[Hidden] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
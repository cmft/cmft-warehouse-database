﻿CREATE TABLE [FlexLink].[InputItemValues](
	[Id] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](max) NULL,
	[InputItem_Id] [uniqueidentifier] NULL,
	[Sampler_Id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
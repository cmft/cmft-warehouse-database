﻿CREATE TABLE [FlexLink].[ProfileParameter](
	[Id] [uniqueidentifier] NOT NULL,
	[ParameterName] [nvarchar](128) NOT NULL,
	[ParameterProfile_Id] [uniqueidentifier] NULL,
	[UnitOfMeasure] [nvarchar](128) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Safeguarding].[STAGETYPES](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](60) NULL,
	[ORDER] [varchar](2) NULL,
	[DISPLAY] [bit] NULL,
	[NUM_DAYS] [numeric](3, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[CASE_TYPE] [varchar](2) NULL,
	[DAYS_FROM] [varchar](1) NULL
) ON [PRIMARY]
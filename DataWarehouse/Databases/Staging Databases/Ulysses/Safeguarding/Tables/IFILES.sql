﻿CREATE TABLE [Safeguarding].[IFILES](
	[PROG_NAME] [varchar](10) NULL,
	[FILE_NAME] [varchar](25) NULL,
	[FILE_DESC] [varchar](29) NULL,
	[FILE_INDEX] [varchar](40) NULL,
	[FIELD_IND] [varchar](120) NULL,
	[DATA_TYPE] [varchar](1) NULL
) ON [PRIMARY]
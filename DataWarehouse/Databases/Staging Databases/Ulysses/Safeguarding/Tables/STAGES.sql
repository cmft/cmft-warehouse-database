﻿CREATE TABLE [Safeguarding].[STAGES](
	[CASE_CODE] [varchar](4) NULL,
	[STAGE] [varchar](2) NULL,
	[TARGET_DT] [date] NULL,
	[COMPLET_DT] [date] NULL,
	[REASON] [varchar](2) NULL,
	[DETAIL] [text] NULL,
	[ENTERED_BY] [varchar](4) NULL,
	[ENTERED_DT] [date] NULL,
	[ENTERED_TM] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Safeguarding].[AUDIT](
	[DATE] [date] NULL,
	[TIME] [varchar](6) NULL,
	[USER] [varchar](10) NULL,
	[ACTION] [varchar](40) NULL,
	[OPERATION] [varchar](2) NULL,
	[RECORD_ID] [varchar](4) NULL,
	[ERROR] [text] NULL,
	[RECORD_NUM] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
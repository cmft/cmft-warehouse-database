﻿CREATE TABLE [Safeguarding].[CASE](
	[CASE_CODE] [varchar](4) NULL,
	[PATIENT] [varchar](4) NULL,
	[CASE_NO] [varchar](12) NULL,
	[ALERT_TYPE] [varchar](2) NULL,
	[CASE_TYPE] [varchar](2) NULL,
	[SI] [bit] NULL,
	[ALERT_DT] [date] NULL,
	[ALERT_TM] [varchar](5) NULL,
	[ALERTER] [varchar](30) NULL,
	[REFER_DT] [date] NULL,
	[REFER_TM] [varchar](5) NULL,
	[AL_MANAGER] [varchar](30) NULL,
	[DATE_ALLOC] [date] NULL,
	[ALLOC_DUE] [date] NULL,
	[CASE_MANAG] [varchar](30) NULL,
	[DETAILS] [text] NULL,
	[INVESTIGAT] [varchar](30) NULL,
	[ORGANISE] [varchar](2) NULL,
	[SITE] [varchar](2) NULL,
	[SITE_TYPE] [varchar](2) NULL,
	[DEPARTMENT] [varchar](2) NULL,
	[DIRECTORAT] [varchar](2) NULL,
	[SPECIALTY] [varchar](2) NULL,
	[DIVISION] [varchar](2) NULL,
	[RES_SITE] [varchar](2) NULL,
	[RES_DEPT] [varchar](2) NULL,
	[RES_DIRECT] [varchar](2) NULL,
	[PCT] [varchar](2) NULL,
	[COMMISSION] [varchar](2) NULL,
	[STATUS_TY] [varchar](2) NULL,
	[ABUSE_LOC] [varchar](2) NULL,
	[ALLEGED] [varchar](4) NULL,
	[ALLEGED_TY] [varchar](1) NULL,
	[ALLEGED_RE] [varchar](2) NULL,
	[ABUSE_DETS] [text] NULL,
	[FORM_OFFER] [varchar](2) NULL,
	[FORM_RET] [varchar](2) NULL,
	[OUT_DETAIL] [text] NULL,
	[DATE_CLOSE] [date] NULL,
	[SEC_GROUP] [varchar](2) NULL,
	[OPTIONS] [numeric](2, 0) NULL,
	[ALERTER_TY] [varchar](1) NULL,
	[MANAGER_TY] [varchar](1) NULL,
	[INVEST_TY] [varchar](1) NULL,
	[INT_STATUS] [numeric](1, 0) NULL,
	[IS_CASE] [varchar](1) NULL,
	[REASON_NO] [varchar](2) NULL,
	[CLIENT_TYP] [varchar](2) NULL,
	[INC_ELSEWH] [varchar](1) NULL,
	[INC_DATE] [date] NULL,
	[INC_TIME] [varchar](5) NULL,
	[FEEDBACK] [bit] NULL,
	[COMMENTS] [text] NULL,
	[RES_ORG] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
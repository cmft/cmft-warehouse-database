﻿CREATE TABLE [Safeguarding].[CASEQUESTIONTYPES](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](60) NULL,
	[ORDER] [varchar](2) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[CASE_TYPE] [varchar](2) NULL,
	[DETAILS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Safeguarding].[AGENCIESINVOLVED](
	[CASE_CODE] [varchar](4) NULL,
	[AGENCY] [varchar](2) NULL,
	[ROLE] [varchar](2) NULL,
	[DETAILS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
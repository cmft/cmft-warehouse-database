﻿CREATE TABLE [Safeguarding].[PCT](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](5) NULL,
	[ABBREVIAT2] [varchar](5) NULL,
	[ABBREVIAT3] [varchar](5) NULL
) ON [PRIMARY]
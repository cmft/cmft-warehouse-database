﻿CREATE TABLE [Safeguarding].[NATUREOFABUSE](
	[CASE_CODE] [varchar](4) NULL,
	[ABUSE] [varchar](2) NULL,
	[OUTCOME] [varchar](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Safeguarding].[STAFFINVOLVED](
	[CASE_CODE] [varchar](4) NULL,
	[STAFF] [varchar](4) NULL,
	[ROLE] [varchar](2) NULL,
	[DETAILS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
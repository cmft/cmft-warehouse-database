﻿CREATE TABLE [Summary].[Links](
	[MAIN_TYPE] [varchar](1) NULL,
	[MAIN_CODE] [varchar](4) NULL,
	[LINK_TYPE] [varchar](1) NULL,
	[LINK_CODE] [varchar](4) NULL,
	[MAIN_CODE2] [varchar](4) NULL,
	[LINK_CODE2] [varchar](4) NULL
) ON [PRIMARY]
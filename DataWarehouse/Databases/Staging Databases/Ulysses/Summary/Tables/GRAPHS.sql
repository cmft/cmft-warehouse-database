﻿CREATE TABLE [Summary].[GRAPHS](
	[SYSTEM] [varchar](1) NULL,
	[USER] [varchar](2) NULL,
	[TITLE] [varchar](50) NULL,
	[DESCRIPT] [text] NULL,
	[GRAPH] [text] NULL,
	[DESKTOP] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
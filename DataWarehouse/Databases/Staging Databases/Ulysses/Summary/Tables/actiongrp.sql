﻿CREATE TABLE [Summary].[actiongrp](
	[GRP_CODE] [varchar](4) NULL,
	[TITLE] [varchar](50) NULL,
	[DETAILS] [text] NULL,
	[ORGANISE] [varchar](2) NULL,
	[OWNER] [varchar](4) NULL,
	[TARGET_DT] [date] NULL,
	[TARGET] [varchar](20) NULL,
	[REVIEW_FRQ] [varchar](2) NULL,
	[DATE_SET] [date] NULL,
	[COMPLETE] [date] NULL,
	[NEXT_REV] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
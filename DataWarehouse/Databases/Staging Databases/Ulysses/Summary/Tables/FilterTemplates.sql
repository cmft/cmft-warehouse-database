﻿CREATE TABLE [Summary].[FilterTemplates](
	[USER_NAME] [varchar](10) NULL,
	[FILE] [varchar](20) NULL,
	[TITLE] [varchar](50) NULL,
	[DEFAULT] [bit] NULL,
	[DETAILS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
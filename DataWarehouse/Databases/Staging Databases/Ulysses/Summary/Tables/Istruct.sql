﻿CREATE TABLE [Summary].[Istruct](
	[FILE] [varchar](20) NULL,
	[FILE_DESC] [varchar](20) NULL,
	[FIELD_NAME] [varchar](10) NULL,
	[FIELD_TYPE] [varchar](1) NULL,
	[FIELD_LEN] [numeric](3, 0) NULL,
	[FIELD_DEC] [numeric](2, 0) NULL,
	[FIELD_OLD] [varchar](10) NULL,
	[REMOVE] [bit] NULL,
	[DEFAULT] [varchar](30) NULL,
	[DATA_TYPE] [varchar](1) NULL
) ON [PRIMARY]
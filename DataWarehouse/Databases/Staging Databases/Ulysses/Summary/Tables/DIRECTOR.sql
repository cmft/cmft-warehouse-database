﻿CREATE TABLE [Summary].[DIRECTOR](
	[CODE] [varchar](2) NULL,
	[DIRECTORAT] [varchar](50) NULL,
	[NUMBER_ST] [numeric](5, 0) NULL,
	[GROUP] [varchar](2) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[EXCLD_KO41] [bit] NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[SITE_LOC] [varchar](1) NULL
) ON [PRIMARY]
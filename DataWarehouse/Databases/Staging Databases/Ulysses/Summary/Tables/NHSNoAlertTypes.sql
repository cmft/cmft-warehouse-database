﻿CREATE TABLE [Summary].[NHSNoAlertTypes](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[IMAGE] [numeric](1, 0) NULL,
	[DEFAULT] [bit] NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
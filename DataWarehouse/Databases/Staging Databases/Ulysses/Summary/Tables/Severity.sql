﻿CREATE TABLE [Summary].[Severity](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[SCORE] [numeric](3, 0) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[MATRIX] [varchar](2) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[INFORM_REL] [varchar](1) NULL
) ON [PRIMARY]
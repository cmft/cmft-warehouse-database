﻿CREATE TABLE [Summary].[DEPTDIV](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Summary].[IManuals](
	[SYSTEM] [varchar](20) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[FILE] [varchar](60) NULL,
	[ORDER] [numeric](2, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Summary].[GROUPS](
	[GROUP_CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[SECURITY] [bit] NULL,
	[TEAM] [bit] NULL,
	[WEB] [bit] NULL,
	[SYSTEM] [numeric](5, 0) NULL,
	[DIARY] [bit] NULL,
	[INACTIVE] [bit] NULL
) ON [PRIMARY]
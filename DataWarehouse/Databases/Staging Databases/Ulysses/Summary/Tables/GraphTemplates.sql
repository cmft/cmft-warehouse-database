﻿CREATE TABLE [Summary].[GraphTemplates](
	[SYSTEM] [varchar](1) NULL,
	[TITLE] [varchar](50) NULL,
	[DEFAULT] [bit] NULL,
	[GRAPH] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
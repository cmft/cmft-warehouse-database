﻿CREATE TABLE [Summary].[SITE](
	[CODE] [varchar](2) NULL,
	[SITE] [varchar](50) NULL,
	[AUTHORITY] [varchar](25) NULL,
	[SITE_TYPE] [varchar](2) NULL,
	[NUMBER_ST] [numeric](5, 0) NULL,
	[RECEIPT] [bit] NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[SITE_LOC] [varchar](1) NULL,
	[ADDRESS] [text] NULL,
	[EXCLD_KO41] [bit] NULL,
	[ORGANISE] [varchar](2) NULL,
	[NPSA1] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[KO41_CODE] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
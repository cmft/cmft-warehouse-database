﻿CREATE TABLE [Summary].[JOBTITLE](
	[CODE] [varchar](2) NULL,
	[JOB_TITLE] [varchar](50) NULL,
	[COST] [numeric](3, 0) NULL,
	[JOB_TYPE] [varchar](2) NULL,
	[NUMBER] [numeric](10, 0) NULL,
	[COST_REPLA] [numeric](3, 0) NULL,
	[COST_MANAG] [numeric](3, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[NPSA1] [varchar](10) NULL,
	[JOB_STATUS] [varchar](2) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[SIRS2] [varchar](2) NULL,
	[SIRS9] [varchar](2) NULL
) ON [PRIMARY]
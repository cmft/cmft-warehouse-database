﻿CREATE TABLE [Summary].[actiongrplnk](
	[GRP_CODE] [varchar](4) NULL,
	[ACT_CODE] [varchar](4) NULL,
	[SUB_ACT] [varchar](4) NULL,
	[HAS_SUBACT] [bit] NULL,
	[ORDER] [numeric](3, 0) NULL
) ON [PRIMARY]
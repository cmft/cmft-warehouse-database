﻿CREATE TABLE [Summary].[REVFRQTY](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[DAYS] [numeric](5, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
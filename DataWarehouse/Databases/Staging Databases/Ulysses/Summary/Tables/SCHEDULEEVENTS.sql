﻿CREATE TABLE [Summary].[SCHEDULEEVENTS](
	[CODE] [varchar](2) NULL,
	[TYPE] [varchar](2) NULL,
	[DESCRIPT] [varchar](30) NULL,
	[START_DT] [date] NULL,
	[START_TM] [varchar](8) NULL,
	[DETAILS] [text] NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
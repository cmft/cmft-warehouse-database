﻿CREATE TABLE [Summary].[recommendations](
	[SYSTEM] [varchar](1) NULL,
	[MAIN_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[RECOM_TYPE] [varchar](2) NULL,
	[TARGET_DT] [date] NULL,
	[REC_DETAIL] [text] NULL,
	[COMPLET_DT] [date] NULL,
	[OUTCOME] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
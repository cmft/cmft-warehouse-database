﻿CREATE TABLE [Risk].[FACTLINK](
	[FACTOR] [varchar](2) NULL,
	[SEVERITY] [varchar](2) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Risk].[RESPONTY](
	[CODE] [varchar](1) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[USAGE] [varchar](20) NULL,
	[SCORE] [numeric](3, 0) NULL,
	[TYPE] [varchar](1) NULL,
	[COLOUR] [numeric](10, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
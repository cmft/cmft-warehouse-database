﻿CREATE TABLE [Risk].[Prinobjs](
	[OBJ_CODE] [varchar](4) NULL,
	[TITLE] [varchar](200) NULL,
	[DETAILS] [text] NULL,
	[ORGANISE] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
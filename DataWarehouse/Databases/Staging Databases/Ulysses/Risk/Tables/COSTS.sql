﻿CREATE TABLE [Risk].[COSTS](
	[ASS_CODE] [varchar](4) NULL,
	[MAIN_HEAD] [varchar](2) NULL,
	[SUB_HEAD] [varchar](2) NULL,
	[COST_TYPE] [varchar](2) NULL,
	[COST_DESC] [varchar](30) NULL,
	[AMOUNT] [numeric](10, 2) NULL,
	[VAT] [numeric](8, 2) NULL,
	[PAY_REC] [varchar](1) NULL,
	[INVOICE] [varchar](12) NULL,
	[COST_DATE] [date] NULL,
	[RESOURCETY] [varchar](2) NULL,
	[APPROVED] [varchar](2) NULL
) ON [PRIMARY]
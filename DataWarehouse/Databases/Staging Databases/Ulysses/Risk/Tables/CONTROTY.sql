﻿CREATE TABLE [Risk].[CONTROTY](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[SCORE] [numeric](6, 2) NULL,
	[DISP_CONT] [bit] NULL,
	[DISP_ALL] [bit] NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Risk].[CONTROLS](
	[RISK_CODE] [varchar](4) NULL,
	[CONTROL] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[CONT_GAPS] [text] NULL,
	[EFFECTIVE] [varchar](2) NULL,
	[ASSUR_INT] [text] NULL,
	[ASSUR_IND] [text] NULL,
	[ASSUR_GAPS] [text] NULL,
	[ASSUR_LEV] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
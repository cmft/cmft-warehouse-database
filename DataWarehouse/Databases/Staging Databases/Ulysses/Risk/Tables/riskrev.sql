﻿CREATE TABLE [Risk].[riskrev](
	[RISK_CODE] [varchar](4) NULL,
	[REVIEW_DT] [date] NULL,
	[REVIEW_BY] [varchar](4) NULL,
	[DETAILS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Risk].[EXTIFILE](
	[MOVE] [bit] NULL,
	[PROG_NAME] [varchar](10) NULL,
	[FILE_NAME] [varchar](8) NULL,
	[FILE_DESC] [varchar](29) NULL,
	[FILE_INDEX] [varchar](100) NULL,
	[FIELD_IND] [varchar](200) NULL,
	[RDD_NAME] [varchar](10) NULL,
	[DATA_FILE] [bit] NULL
) ON [PRIMARY]
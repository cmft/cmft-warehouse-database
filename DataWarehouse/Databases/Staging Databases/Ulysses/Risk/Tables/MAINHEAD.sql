﻿CREATE TABLE [Risk].[MAINHEAD](
	[CODE] [varchar](2) NULL,
	[HEADING] [varchar](50) NULL,
	[WEIGHTING] [numeric](2, 0) NULL,
	[QUEST_TYPE] [varchar](2) NULL,
	[LEGISLATIO] [text] NULL,
	[ID] [varchar](5) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Risk].[ASSESSTY](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[REASS_DAYS] [numeric](3, 0) NULL,
	[TYPE] [varchar](2) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[YEAR] [varchar](10) NULL,
	[ID] [varchar](3) NULL,
	[INACTIVE] [bit] NULL,
	[MULTILEVEL] [bit] NULL,
	[ANSWER_TY] [varchar](1) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
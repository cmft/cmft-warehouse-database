﻿CREATE TABLE [Risk].[RISKTARG](
	[RISK_CODE] [varchar](4) NULL,
	[CURRENT] [bit] NULL,
	[SEVERITY] [varchar](2) NULL,
	[LIKELIHOOD] [varchar](2) NULL,
	[RISK] [varchar](2) NULL,
	[RISK_RATE] [numeric](6, 0) NULL,
	[COST] [numeric](8, 0) NULL,
	[UNIQUE] [varchar](1) NULL,
	[NSEVERITY] [numeric](4, 0) NULL,
	[SOLUTION] [varchar](2) NULL,
	[DETAIL] [text] NULL,
	[BENEFIT] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Risk].[ATTACH](
	[ASS_CODE] [varchar](10) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[ATTACH_ID] [varchar](6) NULL,
	[DETAILS] [text] NULL,
	[ATTACH_DT] [date] NULL,
	[ATTACH_TM] [varchar](5) NULL,
	[SHOW_ONWEB] [bit] NULL,
	[ENTERED_BY] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
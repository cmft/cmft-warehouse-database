﻿CREATE TABLE [Risk].[PRINCTLS](
	[RISK_CODE] [varchar](4) NULL,
	[CONTROL_NO] [varchar](2) NULL,
	[CONTROL] [varchar](2) NULL,
	[DATE_SET] [date] NULL,
	[ENTRY_DATE] [date] NULL,
	[COMPLETE] [bit] NULL,
	[DETAILS] [text] NULL,
	[EFFECTIVE] [varchar](2) NULL,
	[ASSUR_INT] [text] NULL,
	[ASSUR_LEV] [varchar](2) NULL,
	[ASSUR_IND] [text] NULL,
	[ASSUR_GAPS] [text] NULL,
	[CONT_GAPS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
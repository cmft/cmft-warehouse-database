﻿CREATE TABLE [Risk].[PERS_INV](
	[RISK_CODE] [varchar](4) NULL,
	[TYPE] [varchar](1) NULL,
	[TYPE_CODE] [varchar](2) NULL,
	[NO_INVOLVE] [numeric](4, 0) NULL,
	[FREQUENCY] [varchar](2) NULL
) ON [PRIMARY]
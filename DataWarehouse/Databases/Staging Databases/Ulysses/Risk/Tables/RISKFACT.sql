﻿CREATE TABLE [Risk].[RISKFACT](
	[RISK_CODE] [varchar](4) NULL,
	[FACTOR] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[SEVERITY] [varchar](2) NULL,
	[FACT_TYPE] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
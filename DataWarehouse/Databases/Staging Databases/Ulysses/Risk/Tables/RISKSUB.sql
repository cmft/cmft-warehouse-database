﻿CREATE TABLE [Risk].[RISKSUB](
	[ASS_CODE] [varchar](4) NULL,
	[MAIN_HEAD] [varchar](2) NULL,
	[SUB_HEAD] [varchar](2) NULL,
	[WEIGHTING] [numeric](3, 0) NULL,
	[BEF_SEVER] [numeric](2, 0) NULL,
	[BEF_PROB] [numeric](2, 0) NULL,
	[AFT_SEVER] [numeric](2, 0) NULL,
	[AFT_PROB] [numeric](2, 0) NULL,
	[RESPONSE] [varchar](1) NULL,
	[SCORE] [numeric](3, 0) NULL,
	[NOTE] [text] NULL,
	[COMPLETE] [bit] NULL,
	[TARGET_DT] [date] NULL,
	[END_DATE] [date] NULL,
	[LEAD_STAFF] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
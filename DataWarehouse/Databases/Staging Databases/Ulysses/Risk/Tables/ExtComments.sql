﻿CREATE TABLE [Risk].[ExtComments](
	[EXTCODE] [varchar](4) NULL,
	[COMMENT_NO] [numeric](3, 0) NULL,
	[COMMENT] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
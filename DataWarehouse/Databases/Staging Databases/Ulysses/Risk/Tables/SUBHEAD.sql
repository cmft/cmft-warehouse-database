﻿CREATE TABLE [Risk].[SUBHEAD](
	[CODE] [varchar](2) NULL,
	[MAIN_HEAD] [varchar](2) NULL,
	[HEADING] [varchar](70) NULL,
	[CRITERION] [text] NULL,
	[DETAILS] [text] NULL,
	[SEQ_NO] [numeric](6, 2) NULL,
	[WEIGHTING] [numeric](3, 0) NULL,
	[ANSWER_TYP] [varchar](1) NULL,
	[ID] [varchar](5) NULL,
	[SEQ_DESC] [varchar](10) NULL,
	[VERIFICAT] [text] NULL,
	[NOTES] [text] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
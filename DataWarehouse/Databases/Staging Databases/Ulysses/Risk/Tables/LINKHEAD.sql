﻿CREATE TABLE [Risk].[LINKHEAD](
	[CODE] [varchar](2) NULL,
	[MAIN_HEAD] [varchar](2) NULL,
	[SEQ_NO] [numeric](3, 0) NULL,
	[ID] [varchar](5) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
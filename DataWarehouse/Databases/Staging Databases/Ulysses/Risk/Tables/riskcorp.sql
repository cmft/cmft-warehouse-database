﻿CREATE TABLE [Risk].[riskcorp](
	[RISK_CODE] [varchar](4) NULL,
	[RESP_TYPE] [varchar](2) NULL,
	[RISK_REAS] [text] NULL,
	[WHEN_OCCUR] [varchar](1) NULL,
	[DATE_OCCUR] [date] NULL,
	[RISK_FREQ] [varchar](2) NULL,
	[RISK_NOTES] [text] NULL,
	[PLAN_DECIS] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
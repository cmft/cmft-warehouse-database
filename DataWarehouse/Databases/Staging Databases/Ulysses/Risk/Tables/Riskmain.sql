﻿CREATE TABLE [Risk].[Riskmain](
	[ASS_CODE] [varchar](4) NULL,
	[MAIN_HEAD] [varchar](2) NULL,
	[WEIGHTING] [numeric](2, 0) NULL,
	[ACCEPTABLE] [varchar](2) NULL,
	[CONTROLS] [varchar](2) NULL,
	[NOTE] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
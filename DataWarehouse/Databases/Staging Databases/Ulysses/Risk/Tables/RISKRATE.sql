﻿CREATE TABLE [Risk].[RISKRATE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[SCORE] [varchar](50) NULL,
	[RES_SCORE] [numeric](3, 0) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[COLOUR] [numeric](10, 0) NULL,
	[FROM_SCORE] [numeric](3, 0) NULL,
	[TO_SCORE] [numeric](3, 0) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[REVIEW_DAY] [numeric](3, 0) NULL
) ON [PRIMARY]
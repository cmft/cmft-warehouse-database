﻿CREATE TABLE [Risk].[RISKREGTYPE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[CORPORATE] [bit] NULL,
	[PNR] [bit] NULL,
	[PREFIX] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[DEFAULT] [bit] NULL,
	[ASSESSMENT] [bit] NULL,
	[LEVEL] [numeric](1, 0) NULL
) ON [PRIMARY]
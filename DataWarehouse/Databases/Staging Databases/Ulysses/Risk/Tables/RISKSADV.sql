﻿CREATE TABLE [Risk].[RISKSADV](
	[RISK_CODE] [varchar](4) NULL,
	[STATUS_TYP] [varchar](1) NULL,
	[MONIT_GRP] [varchar](2) NULL,
	[STAKE_HOL] [varchar](2) NULL,
	[CONTROL_PT] [varchar](2) NULL,
	[VAL_COMMIT] [varchar](2) NULL,
	[COMP_LEVEL] [varchar](2) NULL,
	[RISK_AWARE] [varchar](2) NULL,
	[IMPACT_PRE] [varchar](2) NULL,
	[GOVER_PILL] [varchar](2) NULL,
	[FUND_STAT] [varchar](2) NULL,
	[PER_FUND] [numeric](3, 0) NULL,
	[YEAR_FUND] [varchar](2) NULL,
	[UND_SEVER] [varchar](2) NULL,
	[UND_LIKELI] [varchar](2) NULL,
	[UND_RATE] [varchar](2) NULL,
	[UND_RRATE] [numeric](6, 0) NULL,
	[INVOLVED] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
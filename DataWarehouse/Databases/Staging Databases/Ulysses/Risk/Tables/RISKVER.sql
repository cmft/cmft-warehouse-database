﻿CREATE TABLE [Risk].[RISKVER](
	[ASS_CODE] [varchar](4) NULL,
	[MAIN_HEAD] [varchar](2) NULL,
	[SUB_HEAD] [varchar](2) NULL,
	[VERIFY_TY] [varchar](2) NULL,
	[VERIFIED] [bit] NULL,
	[COMMENT] [text] NULL,
	[STAFF] [varchar](20) NULL,
	[STANDARDS] [bit] NULL,
	[V_SUB_HEAD] [varchar](2) NULL,
	[EVIDEN_TYP] [varchar](2) NULL,
	[DATE_ENTER] [date] NULL,
	[NOTES] [text] NULL,
	[TARGET_DT] [date] NULL,
	[RESPONSE] [varchar](1) NULL,
	[SCORE] [numeric](3, 0) NULL,
	[ORDER] [numeric](2, 0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
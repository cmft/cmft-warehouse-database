﻿CREATE TABLE [PALS].[LetField](
	[FIELD_NAME] [varchar](140) NULL,
	[DESCRIPT] [varchar](20) NULL,
	[FILEID] [varchar](2) NULL,
	[SCN_NO] [varchar](2) NULL,
	[TYPE] [varchar](1) NULL,
	[WIDTH] [numeric](2, 0) NULL,
	[DEC] [numeric](1, 0) NULL,
	[EXT_FILE] [varchar](8) NULL,
	[LOOK_UP] [varchar](8) NULL,
	[REF_NO] [varchar](4) NULL
) ON [PRIMARY]
﻿CREATE TABLE [PALS].[Rgad_res](
	[TO] [numeric](3, 0) NULL,
	[DESC] [varchar](11) NULL,
	[SYSTEM] [varchar](11) NULL,
	[GROUP] [numeric](3, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [PALS].[Contpers](
	[ENQ_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[PERSON_COD] [varchar](4) NULL,
	[TYPE] [varchar](1) NULL,
	[ROLE] [varchar](2) NULL,
	[TRUST] [bit] NULL
) ON [PRIMARY]
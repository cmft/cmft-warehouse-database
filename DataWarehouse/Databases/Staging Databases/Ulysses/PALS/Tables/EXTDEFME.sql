﻿CREATE TABLE [PALS].[EXTDEFME](
	[EXTCODE] [varchar](4) NULL,
	[EXTCODE2] [varchar](4) NULL,
	[SYSTEM] [varchar](2) NULL,
	[COMBINE] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [PALS].[EXTDEFDT](
	[EXTCODE] [varchar](4) NULL,
	[SELECT_TYP] [numeric](1, 0) NULL,
	[IDENT] [varchar](4) NULL,
	[OPTION] [varchar](4) NULL,
	[GROUP] [numeric](2, 0) NULL
) ON [PRIMARY]
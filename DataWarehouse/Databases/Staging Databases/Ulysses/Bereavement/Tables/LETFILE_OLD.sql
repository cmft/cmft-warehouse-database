﻿CREATE TABLE [Bereavement].[LETFILE_OLD](
	[FILEID] [varchar](2) NULL,
	[ORDER] [varchar](1) NULL,
	[NAME] [varchar](20) NULL,
	[DESCRIPT] [varchar](21) NULL,
	[CONNECT] [varchar](30) NULL,
	[INDEX] [varchar](10) NULL,
	[INDEX_EXP] [varchar](50) NULL,
	[SOFTSEEK] [bit] NULL,
	[RELATED] [varchar](11) NULL,
	[LOOK_NUM] [numeric](1, 0) NULL,
	[LOOK_EXT] [varchar](2) NULL,
	[REF_NO] [varchar](4) NULL,
	[AUTOFILTER] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
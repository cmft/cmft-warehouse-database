﻿CREATE TABLE [Bereavement].[Service](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](30) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](5) NULL
) ON [PRIMARY]
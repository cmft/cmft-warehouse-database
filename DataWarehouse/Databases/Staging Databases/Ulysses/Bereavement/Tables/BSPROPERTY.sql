﻿CREATE TABLE [Bereavement].[BSPROPERTY](
	[BS_CODE] [varchar](4) NULL,
	[DATE_RECV] [date] NULL,
	[TARGET_DIS] [date] NULL,
	[TYPE] [varchar](2) NULL,
	[DESCRIPT] [varchar](2) NULL,
	[QTY] [numeric](3, 0) NULL,
	[DETAILS] [text] NULL,
	[DIS_DATE] [date] NULL,
	[DIS_METHOD] [varchar](2) NULL,
	[GIVEN_BY] [varchar](40) NULL,
	[RECEIPT_NO] [varchar](12) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Bereavement].[IMAINT1](
	[GROUP] [varchar](10) NULL,
	[FILE_NAME] [varchar](50) NULL,
	[CAPTION] [varchar](30) NULL,
	[CALLMETHOD] [varchar](15) NULL,
	[MERGEMAINT] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Bereavement].[BSDONATION](
	[BS_CODE] [varchar](4) NULL,
	[DONATION] [varchar](2) NULL,
	[REGISTER] [bit] NULL,
	[CONSENT] [bit] NULL,
	[OFFERED] [bit] NULL,
	[REMOVED] [bit] NULL
) ON [PRIMARY]
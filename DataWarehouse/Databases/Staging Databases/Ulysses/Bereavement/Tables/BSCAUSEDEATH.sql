﻿CREATE TABLE [Bereavement].[BSCAUSEDEATH](
	[BS_CODE] [varchar](4) NULL,
	[CAUSE] [text] NULL,
	[TYPE] [varchar](1) NULL,
	[CAUSE1] [varchar](2) NULL,
	[CAUSE2] [varchar](2) NULL,
	[CAUSE3] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Bereavement].[BSINFORM](
	[BS_CODE] [varchar](4) NULL,
	[INF_TYPE] [varchar](1) NULL,
	[PERSON_COD] [varchar](4) NULL,
	[RELATION] [varchar](2) NULL
) ON [PRIMARY]
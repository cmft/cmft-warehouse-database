﻿CREATE TABLE [Bereavement].[FUNERAL](
	[BS_CODE] [varchar](4) NULL,
	[DISPOSAL] [varchar](1) NULL,
	[TYPE] [varchar](1) NULL,
	[DIRECTOR] [varchar](2) NULL,
	[FUNERAL_DT] [date] NULL,
	[FUNERAL_TM] [varchar](5) NULL,
	[PLACE] [varchar](2) NULL,
	[PRIEST] [varchar](30) NULL,
	[COUNTRY] [varchar](2) NULL,
	[F_F_I] [varchar](1) NULL,
	[F_F_R] [varchar](1) NULL,
	[REASON_HOS] [varchar](2) NULL,
	[DETAILS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
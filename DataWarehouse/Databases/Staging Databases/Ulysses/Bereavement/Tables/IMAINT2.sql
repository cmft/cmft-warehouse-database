﻿CREATE TABLE [Bereavement].[IMAINT2](
	[FILE_NAME] [varchar](12) NULL,
	[FIELD_NAME] [varchar](50) NULL,
	[CAPTION] [varchar](30) NULL,
	[LOOKUP] [varchar](12) NULL,
	[HIDE] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident].[Medicati](
	[INC_CODE] [varchar](4) NULL,
	[ERROR] [numeric](1, 0) NULL,
	[DRUG_DUE] [varchar](35) NULL,
	[DRUG_GIVEN] [varchar](35) NULL,
	[TIME_DUE] [varchar](5) NULL,
	[DOSE_DUE] [varchar](10) NULL,
	[DOSE_GIVEN] [varchar](10) NULL,
	[ROUTE_PRES] [varchar](20) NULL,
	[ROUTE_ACT] [varchar](20) NULL,
	[NAME_GIVE] [varchar](25) NULL,
	[AUTHORISED] [bit] NULL,
	[PERSON_TYP] [varchar](2) NULL,
	[INJ_PROP] [varchar](2) NULL,
	[INJ_ACT] [varchar](2) NULL
) ON [PRIMARY]
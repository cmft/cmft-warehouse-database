﻿CREATE TABLE [Incident].[INCIDENTDETAILSTYPE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[ORDER] [varchar](2) NULL,
	[WEBFIELD] [numeric](1, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[NPSA_CODE] [varchar](10) NULL,
	[WEBSECT] [numeric](1, 0) NULL
) ON [PRIMARY]
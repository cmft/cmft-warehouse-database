﻿CREATE TABLE [Incident].[EXTDEFCO](
	[EXTCODE] [varchar](4) NULL,
	[COND] [varchar](50) NULL,
	[WIDTH] [numeric](2, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident].[Others](
	[OTHER_CODE] [varchar](4) NULL,
	[TYPE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[VALUE] [numeric](8, 2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident].[MANUFACT](
	[CODE] [varchar](2) NULL,
	[COMPANY_NA] [varchar](50) NULL,
	[ADDRESS] [text] NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Incident].[sirsflds](
	[ORDER] [numeric](3, 0) NULL,
	[FILE] [varchar](50) NULL,
	[FIELD_NAME] [varchar](50) NULL,
	[LOOK_CODE] [varchar](100) NULL,
	[REF_CODE] [varchar](40) NULL,
	[ENTITY] [varchar](30) NULL,
	[DESCRIPT] [varchar](100) NULL,
	[MANDATORY] [bit] NULL,
	[MAND_CHECK] [varchar](10) NULL,
	[TITLE] [varchar](50) NULL
) ON [PRIMARY]
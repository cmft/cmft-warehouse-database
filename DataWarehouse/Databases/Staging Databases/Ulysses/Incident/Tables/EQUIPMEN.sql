﻿CREATE TABLE [Incident].[EQUIPMEN](
	[INC_CODE] [varchar](5) NULL,
	[EQUIPMENT] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[RETAINED] [bit] NULL,
	[EQUIP_CODE] [varchar](4) NULL,
	[QUANTITY] [numeric](5, 0) NULL,
	[LOCATION] [varchar](30) NULL,
	[CE_CONT] [varchar](1) NULL,
	[FATALITY] [bit] NULL,
	[INJURY] [bit] NULL,
	[CONT_NAME] [varchar](30) NULL,
	[DECONTAM] [varchar](2) NULL,
	[MFR_CONT] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
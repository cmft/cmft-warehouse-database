﻿CREATE TABLE [Incident].[NPSACSET](
	[CODE] [varchar](1) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[CARE_SET] [varchar](4) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident].[REPORTTY](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[REPORT] [varchar](8) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[WEBFIELD] [numeric](2, 0) NULL
) ON [PRIMARY]
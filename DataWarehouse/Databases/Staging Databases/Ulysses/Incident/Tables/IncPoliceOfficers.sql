﻿CREATE TABLE [Incident].[IncPoliceOfficers](
	[INC_CODE] [varchar](4) NULL,
	[OFFICER] [varchar](50) NULL,
	[ID_NUM] [varchar](20) NULL
) ON [PRIMARY]
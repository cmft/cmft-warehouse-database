﻿CREATE TABLE [Incident].[Injuries](
	[INC_CODE] [varchar](5) NULL,
	[TYPE] [varchar](2) NULL,
	[SITE] [varchar](2) NULL,
	[EXTENT] [varchar](2) NULL
) ON [PRIMARY]
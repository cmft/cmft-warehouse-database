﻿CREATE TABLE [Incident].[GRADETYP](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[COLOUR] [numeric](10, 0) NULL,
	[NPSA2] [varchar](10) NULL,
	[NPSA3] [varchar](10) NULL,
	[NPSA4] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[INFORM_REL] [varchar](1) NULL
) ON [PRIMARY]
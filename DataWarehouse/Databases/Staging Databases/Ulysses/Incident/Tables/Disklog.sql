﻿CREATE TABLE [Incident].[Disklog](
	[SET] [varchar](1) NULL,
	[DATE] [date] NULL,
	[TIME] [varchar](8) NULL,
	[NAME] [varchar](20) NULL,
	[NEXT_BAK] [bit] NULL
) ON [PRIMARY]
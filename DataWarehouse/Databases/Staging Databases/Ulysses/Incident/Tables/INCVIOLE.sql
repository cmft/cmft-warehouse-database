﻿CREATE TABLE [Incident].[INCVIOLE](
	[INC_CODE] [varchar](5) NULL,
	[MHA_SECTIO] [varchar](2) NULL,
	[WEAPON] [varchar](2) NULL,
	[RESTRAINT] [varchar](2) NULL,
	[CPA_TYPE] [varchar](2) NULL,
	[OBS_TYPE] [varchar](2) NULL
) ON [PRIMARY]
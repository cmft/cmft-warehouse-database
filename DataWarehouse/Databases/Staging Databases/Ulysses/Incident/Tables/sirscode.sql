﻿CREATE TABLE [Incident].[sirscode](
	[REF_CODE] [varchar](10) NULL,
	[ITEM_CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](100) NULL,
	[VALUE] [varchar](20) NULL,
	[LABEL] [varchar](100) NULL
) ON [PRIMARY]
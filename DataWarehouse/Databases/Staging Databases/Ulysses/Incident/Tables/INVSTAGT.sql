﻿CREATE TABLE [Incident].[INVSTAGT](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[ORDER] [varchar](2) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[NUM_DAYS] [numeric](3, 0) NULL,
	[DAYS_FROM] [varchar](1) NULL
) ON [PRIMARY]
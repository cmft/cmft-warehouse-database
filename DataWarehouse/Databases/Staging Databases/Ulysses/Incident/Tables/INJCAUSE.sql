﻿CREATE TABLE [Incident].[INJCAUSE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](80) NULL,
	[HSE_NUMBER] [numeric](2, 0) NULL,
	[CFOA_GRP] [varchar](2) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
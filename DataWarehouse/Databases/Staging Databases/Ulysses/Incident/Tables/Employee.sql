﻿CREATE TABLE [Incident].[Employee](
	[INC_CODE] [varchar](5) NULL,
	[FULL_PART] [varchar](1) NULL,
	[WTE] [numeric](2, 0) NULL,
	[WORK] [bit] NULL,
	[TIME_HOME] [varchar](5) NULL,
	[AUTH_WORK] [bit] NULL,
	[ACCI_ENTRY] [bit] NULL,
	[START_ABS] [date] NULL,
	[DAYS_ABS] [numeric](3, 0) NULL,
	[COST_ABS] [numeric](6, 0) NULL,
	[END_ABS] [date] NULL,
	[TRAINED] [bit] NULL,
	[PPE] [bit] NULL,
	[REPLACEMEN] [numeric](6, 0) NULL,
	[MANAGE_TM] [numeric](6, 0) NULL,
	[HOURS_LOST] [numeric](6, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident].[CONTRIB](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[GROUP] [varchar](2) NULL,
	[NPSA1] [varchar](10) NULL,
	[PARS_TYPE] [varchar](1) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[SIRS3] [varchar](2) NULL
) ON [PRIMARY]
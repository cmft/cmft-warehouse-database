﻿CREATE TABLE [Incident].[drugname_new](
	[CODE] [varchar](4) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[TYPE] [varchar](2) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](5) NULL,
	[DRUG_CODE] [varchar](10) NULL
) ON [PRIMARY]
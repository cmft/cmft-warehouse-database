﻿CREATE TABLE [Incident].[INCCFOA](
	[INC_CODE] [varchar](5) NULL,
	[ON_DUTY] [varchar](1) NULL,
	[CFOA_GRP] [varchar](2) NULL,
	[CFOA_TYPE] [varchar](2) NULL,
	[DRESS_TYPE] [varchar](2) NULL,
	[DETAILS] [varchar](40) NULL
) ON [PRIMARY]
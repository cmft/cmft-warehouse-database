﻿CREATE TABLE [RFI].[Reqstat](
	[REQ_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[REASON] [varchar](2) NULL,
	[STOP_DATE] [date] NULL,
	[TARGET] [date] NULL,
	[START_DATE] [date] NULL,
	[CORRESPOND] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
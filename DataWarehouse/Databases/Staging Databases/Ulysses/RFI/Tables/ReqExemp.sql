﻿CREATE TABLE [RFI].[ReqExemp](
	[REQ_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[EXEMPTION] [varchar](2) NULL,
	[REASON] [varchar](2) NULL
) ON [PRIMARY]
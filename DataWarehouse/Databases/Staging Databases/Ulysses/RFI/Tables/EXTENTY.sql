﻿CREATE TABLE [RFI].[EXTENTY](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[DAYS_RSP] [numeric](2, 0) NULL,
	[TYPE] [varchar](1) NULL,
	[CORRESPOND] [text] NULL,
	[FEES] [bit] NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
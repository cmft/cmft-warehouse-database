﻿CREATE TABLE [RFI].[REQDETS](
	[REQ_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[SUBJ_CODE] [varchar](4) NULL,
	[OFFICER] [varchar](4) NULL,
	[ADD_DETAIL] [text] NULL,
	[SHARED] [bit] NULL,
	[CAT_TYPE] [varchar](2) NULL,
	[CATEGORY] [varchar](2) NULL,
	[ORGANISE] [varchar](2) NULL,
	[SITE] [varchar](2) NULL,
	[DEPARTMENT] [varchar](2) NULL,
	[DEPTFUNC] [varchar](2) NULL,
	[DIRECTORAT] [varchar](2) NULL,
	[DIVISION] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
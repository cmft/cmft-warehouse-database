﻿CREATE TABLE [RFI].[Estruct](
	[FILE] [varchar](8) NULL,
	[FILE_DESC] [varchar](20) NULL,
	[FIELD_NAME] [varchar](10) NULL,
	[FIELD_TYPE] [varchar](1) NULL,
	[FIELD_LEN] [numeric](3, 0) NULL,
	[FIELD_DEC] [numeric](2, 0) NULL,
	[CONVERT] [bit] NULL,
	[REMOVE] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [RFI].[costs](
	[REQ_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[COST_TYPE] [varchar](2) NULL,
	[COST_DESC] [varchar](30) NULL,
	[AMOUNT] [numeric](10, 2) NULL,
	[VAT] [numeric](8, 2) NULL,
	[PAY_REC] [varchar](1) NULL,
	[INVOICE] [varchar](12) NULL,
	[COST_DATE] [date] NULL,
	[TYPE] [varchar](1) NULL
) ON [PRIMARY]
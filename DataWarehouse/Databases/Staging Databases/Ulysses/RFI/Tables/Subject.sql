﻿CREATE TABLE [RFI].[Subject](
	[SUBJ_CODE] [varchar](4) NULL,
	[SUBJECT] [varchar](50) NULL,
	[CAT_TYPE] [varchar](2) NULL,
	[CATEGORY] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[LOCATION] [text] NULL,
	[CUSTODIAN] [varchar](4) NULL,
	[ORGANISE] [varchar](2) NULL,
	[DISCLOSED] [varchar](1) NULL,
	[ENTIRE_DOC] [bit] NULL,
	[PUBLISHED] [bit] NULL,
	[PUB_LOC] [text] NULL,
	[REQ_TYPE] [varchar](2) NULL,
	[SUBJ_VER] [varchar](4) NULL,
	[VERSION] [numeric](2, 0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
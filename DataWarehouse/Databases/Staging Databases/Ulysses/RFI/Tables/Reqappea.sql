﻿CREATE TABLE [RFI].[Reqappea](
	[REQ_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[APPEAL_NO] [numeric](6, 0) NULL,
	[APPEAL_TYP] [varchar](2) NULL,
	[RAISED_BY] [varchar](2) NULL,
	[RECEIVE_DT] [date] NULL,
	[ACKNOW_DT] [date] NULL,
	[ACKNOW_DAY] [numeric](2, 0) NULL,
	[COMPLET_DT] [date] NULL,
	[TARGET_DT] [date] NULL,
	[DELAY_REAS] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[AP_REASON] [varchar](2) NULL,
	[AP_MANAGER] [varchar](4) NULL,
	[AP_OFFICER] [varchar](4) NULL,
	[STATUS_TY] [varchar](2) NULL,
	[OUTCOME] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
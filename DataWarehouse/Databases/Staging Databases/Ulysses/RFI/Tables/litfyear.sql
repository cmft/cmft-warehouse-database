﻿CREATE TABLE [RFI].[litfyear](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](7) NULL,
	[START_DATE] [date] NULL,
	[END_DATE] [date] NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](5) NULL
) ON [PRIMARY]
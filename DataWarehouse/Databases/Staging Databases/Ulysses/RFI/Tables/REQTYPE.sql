﻿CREATE TABLE [RFI].[REQTYPE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[DAYS_RSP] [numeric](2, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[CAL_DAYS] [bit] NULL,
	[WORK_HRS] [numeric](2, 0) NULL,
	[WORK_MINS] [numeric](2, 0) NULL,
	[HRLY_RATE] [numeric](6, 2) NULL,
	[PERCENTAGE] [numeric](3, 0) NULL,
	[FEE_LIMIT] [numeric](8, 2) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[PREFIX] [varchar](10) NULL,
	[FIRST_DAY] [bit] NULL,
	[DAYS_ROPN] [numeric](2, 0) NULL
) ON [PRIMARY]
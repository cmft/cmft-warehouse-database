﻿CREATE TABLE [RFI].[Reqfees](
	[REQ_CODE] [varchar](4) NULL,
	[WORK_HRS] [numeric](2, 0) NULL,
	[WORK_MINS] [numeric](2, 0) NULL,
	[HRLY_RATE] [numeric](6, 2) NULL,
	[PERCENTAGE] [numeric](3, 0) NULL,
	[MARGINAL] [numeric](8, 2) NULL,
	[TOTAL_COST] [numeric](8, 2) NULL,
	[FEE_LIMIT] [numeric](8, 2) NULL,
	[CHARGEABLE] [numeric](8, 2) NULL,
	[AMNT_CHARG] [numeric](8, 2) NULL,
	[DETAILS] [text] NULL,
	[SENT_DATE] [date] NULL,
	[RECEIVE_DT] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
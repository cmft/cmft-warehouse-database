﻿CREATE TABLE [RFI].[ATTACH](
	[REQ_CODE] [varchar](4) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[ATTACH_NO] [numeric](4, 0) NULL,
	[ATTACH_ID] [varchar](6) NULL,
	[DETAILS] [text] NULL,
	[ATTACH_DT] [date] NULL,
	[ATTACH_TM] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Safewin].[ExtUsers](
	[USER_CODE] [varchar](4) NULL,
	[USER_NAME] [varchar](10) NULL,
	[PASSWORD] [varchar](20) NULL,
	[FULL_NAME] [varchar](20) NULL,
	[SUPERVISOR] [bit] NULL,
	[ALLOWED_ON] [bit] NULL,
	[LAST_LOGIN] [varchar](17) NULL,
	[INFO] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
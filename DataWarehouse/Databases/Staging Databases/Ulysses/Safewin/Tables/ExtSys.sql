﻿CREATE TABLE [Safewin].[ExtSys](
	[SYS_CODE] [varchar](2) NULL,
	[username] [nvarchar](128) NULL,
	[SYS_NAME] [varchar](12) NULL,
	[TITLE] [varchar](50) NULL,
	[MULTI_SYS] [numeric](1, 0) NULL
) ON [PRIMARY]
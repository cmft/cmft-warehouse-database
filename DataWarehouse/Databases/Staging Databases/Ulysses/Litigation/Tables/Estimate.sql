﻿CREATE TABLE [Litigation].[Estimate](
	[LIT_CODE] [varchar](4) NULL,
	[ENTRY_DATE] [date] NULL,
	[SOURCE] [varchar](2) NULL,
	[DAMAGES] [numeric](8, 0) NULL,
	[DEFENCE] [numeric](8, 0) NULL,
	[PLAINTIFF] [numeric](8, 0) NULL,
	[PROGNOSIS] [varchar](2) NULL,
	[SETTLE_PER] [numeric](3, 0) NULL,
	[LIABLE_PER] [numeric](3, 0) NULL,
	[SETTLE_YR] [varchar](2) NULL,
	[CHANGE_TYP] [varchar](2) NULL,
	[CURRENT] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Litigation].[LITOUTCM](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[HSE_NUMBER] [numeric](1, 0) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
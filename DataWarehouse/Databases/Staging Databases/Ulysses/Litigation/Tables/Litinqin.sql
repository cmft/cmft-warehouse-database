﻿CREATE TABLE [Litigation].[Litinqin](
	[LIT_CODE] [varchar](4) NULL,
	[INQUEST_DT] [date] NULL,
	[COURT] [varchar](15) NULL,
	[TRUST_WIT] [varchar](20) NULL,
	[LEGAL_REP] [varchar](2) NULL
) ON [PRIMARY]
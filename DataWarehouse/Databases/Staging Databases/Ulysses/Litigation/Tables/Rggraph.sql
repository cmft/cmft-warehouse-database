﻿CREATE TABLE [Litigation].[Rggraph](
	[X_AXIS] [varchar](10) NULL,
	[Y1_AXIS] [numeric](12, 2) NULL,
	[Y2_AXIS] [numeric](12, 2) NULL,
	[Y3_AXIS] [numeric](12, 2) NULL,
	[DESCRIPT] [varchar](40) NULL
) ON [PRIMARY]
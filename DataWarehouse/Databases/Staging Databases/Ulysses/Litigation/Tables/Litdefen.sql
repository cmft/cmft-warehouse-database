﻿CREATE TABLE [Litigation].[Litdefen](
	[LIT_CODE] [varchar](4) NULL,
	[DEFENDANT] [varchar](2) NULL,
	[LIABLE_PER] [numeric](3, 0) NULL,
	[TYPE] [varchar](1) NULL,
	[INSURER] [varchar](2) NULL,
	[EXCESS] [numeric](10, 0) NULL,
	[LIMIT] [numeric](10, 0) NULL
) ON [PRIMARY]
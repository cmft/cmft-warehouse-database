﻿CREATE TABLE [Litigation].[Litdocrq](
	[LIT_CODE] [varchar](5) NULL,
	[REQ_DATE] [date] NULL,
	[REQ_BY] [varchar](20) NULL,
	[FORWARD_DT] [date] NULL,
	[FORWARD_TO] [varchar](20) NULL
) ON [PRIMARY]
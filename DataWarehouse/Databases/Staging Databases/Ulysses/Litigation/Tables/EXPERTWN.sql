﻿CREATE TABLE [Litigation].[EXPERTWN](
	[CODE] [varchar](2) NULL,
	[SURNAME] [varchar](15) NULL,
	[FORENAME] [varchar](15) NULL,
	[ADDRESS1] [varchar](30) NULL,
	[ADDRESS2] [varchar](30) NULL,
	[ADDRESS3] [varchar](30) NULL,
	[ADDRESS4] [varchar](30) NULL,
	[POSTCODE1] [varchar](4) NULL,
	[POSTCODE2] [varchar](3) NULL,
	[DISCIPLINE] [varchar](2) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
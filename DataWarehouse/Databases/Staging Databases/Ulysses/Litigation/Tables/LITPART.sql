﻿CREATE TABLE [Litigation].[LITPART](
	[LIT_CODE] [varchar](4) NULL,
	[SURNAME] [varchar](15) NULL,
	[FORENAME] [varchar](15) NULL,
	[ROLE] [varchar](2) NULL,
	[FOR_CLAIM] [bit] NULL,
	[INVOLVEMNT] [text] NULL,
	[ADDRESS1] [varchar](30) NULL,
	[ADDRESS2] [varchar](30) NULL,
	[ADDRESS3] [varchar](30) NULL,
	[ADDRESS4] [varchar](30) NULL,
	[POSTCODE1] [varchar](4) NULL,
	[POSTCODE2] [varchar](3) NULL,
	[DISCIPLINE] [varchar](2) NULL,
	[CN_SPECIAL] [varchar](2) NULL,
	[TELEPHONE] [varchar](30) NULL,
	[EMAIL] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
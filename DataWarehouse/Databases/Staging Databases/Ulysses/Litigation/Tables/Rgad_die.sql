﻿CREATE TABLE [Litigation].[Rgad_die](
	[TO] [varchar](1) NULL,
	[DESC] [varchar](25) NULL,
	[SYSTEM] [varchar](10) NULL,
	[GROUP] [numeric](3, 0) NULL
) ON [PRIMARY]
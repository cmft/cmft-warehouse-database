﻿CREATE TABLE [Litigation].[Rgad_mth](
	[TO] [date] NULL,
	[DESC] [varchar](6) NULL,
	[SYSTEM] [varchar](11) NULL,
	[GROUP] [numeric](3, 0) NULL
) ON [PRIMARY]
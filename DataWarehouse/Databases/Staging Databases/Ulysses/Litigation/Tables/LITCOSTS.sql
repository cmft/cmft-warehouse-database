﻿CREATE TABLE [Litigation].[LITCOSTS](
	[LIT_CODE] [varchar](4) NULL,
	[COST_TYPE] [varchar](2) NULL,
	[COST_DESC] [varchar](50) NULL,
	[AMOUNT] [numeric](11, 2) NULL,
	[PAY_REC] [varchar](1) NULL,
	[INVOICE] [varchar](50) NULL,
	[FIN_YEAR] [varchar](2) NULL,
	[COST_DATE] [date] NULL,
	[VAT] [numeric](10, 2) NULL,
	[DEFENDANT] [varchar](2) NULL
) ON [PRIMARY]
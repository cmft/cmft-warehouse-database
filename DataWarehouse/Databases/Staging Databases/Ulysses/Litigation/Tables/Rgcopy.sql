﻿CREATE TABLE [Litigation].[Rgcopy](
	[CODE] [varchar](2) NULL,
	[TITLE] [varchar](40) NULL,
	[MAIN_FILE] [varchar](2) NULL,
	[UNIQUE] [bit] NULL
) ON [PRIMARY]
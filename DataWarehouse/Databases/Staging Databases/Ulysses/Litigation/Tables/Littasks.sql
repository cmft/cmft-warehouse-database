﻿CREATE TABLE [Litigation].[Littasks](
	[LIT_CODE] [varchar](2) NULL,
	[TASK] [varchar](20) NULL,
	[ASSIGN_TO] [varchar](15) NULL,
	[TARGET_DT] [date] NULL,
	[COMPLETED] [bit] NULL,
	[PRIORITY] [numeric](1, 0) NULL
) ON [PRIMARY]
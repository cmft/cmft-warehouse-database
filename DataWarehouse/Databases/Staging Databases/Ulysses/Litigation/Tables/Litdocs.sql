﻿CREATE TABLE [Litigation].[Litdocs](
	[LIT_CODE] [varchar](4) NULL,
	[DOC_CODE] [varchar](1) NULL,
	[DOC_DESC] [varchar](20) NULL,
	[WRITTEN_BY] [varchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Litigation].[Rgad_fp](
	[TO] [varchar](1) NULL,
	[DESC] [varchar](15) NULL,
	[SYSTEM] [varchar](10) NULL,
	[GROUP] [numeric](3, 0) NULL
) ON [PRIMARY]
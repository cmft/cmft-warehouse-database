﻿CREATE TABLE [Litigation].[Rgad_24h](
	[TO] [varchar](5) NULL,
	[DESC] [varchar](20) NULL,
	[SYSTEM] [varchar](11) NULL,
	[GROUP] [numeric](3, 0) NULL
) ON [PRIMARY]
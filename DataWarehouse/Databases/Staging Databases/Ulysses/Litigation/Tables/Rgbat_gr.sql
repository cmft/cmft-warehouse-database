﻿CREATE TABLE [Litigation].[Rgbat_gr](
	[FILE_NUM] [varchar](2) NULL,
	[TITLE1] [varchar](60) NULL,
	[TITLE2] [varchar](60) NULL,
	[SUB_TOTAL1] [varchar](20) NULL,
	[SUB_TOTAL2] [varchar](20) NULL,
	[SUB_TOTAL3] [varchar](20) NULL,
	[SUB_TITLE1] [varchar](20) NULL,
	[SUB_TITLE2] [varchar](20) NULL,
	[SUB_TITLE3] [varchar](20) NULL,
	[DATASET] [numeric](1, 0) NULL
) ON [PRIMARY]
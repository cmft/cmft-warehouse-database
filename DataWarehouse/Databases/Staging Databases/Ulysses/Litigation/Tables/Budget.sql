﻿CREATE TABLE [Litigation].[Budget](
	[LIT_CODE] [varchar](4) NULL,
	[PAY_PERIOD] [date] NULL,
	[AMOUNT] [numeric](10, 0) NULL,
	[FINAN_YEAR] [varchar](2) NULL,
	[ENTRY_DATE] [date] NULL,
	[CURRENT] [bit] NULL
) ON [PRIMARY]
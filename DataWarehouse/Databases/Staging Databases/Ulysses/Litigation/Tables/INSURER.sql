﻿CREATE TABLE [Litigation].[INSURER](
	[CODE] [varchar](2) NULL,
	[COMPANY_ID] [varchar](50) NULL,
	[NAME] [varchar](40) NULL,
	[EXCESS_AMT] [numeric](10, 0) NULL,
	[UPTO1] [numeric](10, 2) NULL,
	[PER1] [numeric](3, 0) NULL,
	[UPTO2] [numeric](10, 2) NULL,
	[PER2] [numeric](3, 0) NULL,
	[UPTO3] [numeric](10, 2) NULL,
	[PER3] [numeric](3, 0) NULL,
	[CNST] [bit] NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[TYPE] [varchar](1) NULL,
	[EFFECT_DT] [date] NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
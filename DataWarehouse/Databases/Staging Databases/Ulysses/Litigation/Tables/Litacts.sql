﻿CREATE TABLE [Litigation].[Litacts](
	[LIT_CODE] [varchar](2) NULL,
	[ACTION] [varchar](20) NULL,
	[ASSIGN_TO] [varchar](15) NULL,
	[COMPLETED] [date] NULL,
	[PRIORITY] [numeric](1, 0) NULL
) ON [PRIMARY]
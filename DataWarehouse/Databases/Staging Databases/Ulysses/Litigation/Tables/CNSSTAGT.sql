﻿CREATE TABLE [Litigation].[CNSSTAGT](
	[CODE] [varchar](2) NULL,
	[ORDER] [varchar](2) NULL,
	[DESCRIPT] [varchar](30) NULL,
	[TYPE] [varchar](5) NULL,
	[INACTIVE] [bit] NULL,
	[ELS_CODE] [varchar](2) NULL,
	[NO_DAYS] [numeric](3, 0) NULL,
	[ACTION_TY] [varchar](2) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
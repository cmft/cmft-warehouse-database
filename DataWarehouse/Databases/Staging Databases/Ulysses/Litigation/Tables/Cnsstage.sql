﻿CREATE TABLE [Litigation].[Cnsstage](
	[LIT_CODE] [varchar](4) NULL,
	[STAGE] [varchar](2) NULL,
	[DATE] [date] NULL,
	[TARGET] [date] NULL,
	[REMINDER] [date] NULL,
	[ACTUAL] [date] NULL,
	[ACTION_TY] [varchar](2) NULL,
	[DETAIL] [text] NULL,
	[DELAY_REAS] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
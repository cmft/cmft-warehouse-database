﻿CREATE TABLE [Litigation].[Lit_staf](
	[STAFF_CODE] [varchar](2) NULL,
	[STATUS] [varchar](2) NULL,
	[SPECIALTY] [varchar](2) NULL,
	[PERCENTAGE] [numeric](3, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Litigation].[LITDETS](
	[LIT_CODE] [varchar](4) NULL,
	[PROCEEDING] [bit] NULL,
	[LIAB_PROB] [varchar](2) NULL,
	[TRIAL_DATE] [date] NULL,
	[HEARING_LE] [varchar](20) NULL,
	[LIAB_TRANS] [bit] NULL,
	[LIAB_WHO] [varchar](40) NULL,
	[LIAB_DATE] [date] NULL,
	[NOVEL_C_R] [varchar](1) NULL,
	[PUB_RELS] [text] NULL,
	[LEGAL_AID] [varchar](2) NULL,
	[FULL_LIMIT] [varchar](1) NULL,
	[INSURER] [varchar](2) NULL,
	[CERT_NUM] [varchar](25) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
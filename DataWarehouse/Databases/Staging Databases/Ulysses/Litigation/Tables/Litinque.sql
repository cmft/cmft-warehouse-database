﻿CREATE TABLE [Litigation].[Litinque](
	[LIT_CODE] [varchar](4) NULL,
	[PM_DATE] [date] NULL,
	[CORONER] [varchar](15) NULL,
	[VERDICT] [varchar](2) NULL,
	[PRIM_DEATH] [varchar](30) NULL,
	[OTHER_DETS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
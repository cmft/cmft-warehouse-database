﻿CREATE TABLE [Litigation].[Litperso](
	[LIT_CODE] [varchar](4) NULL,
	[PERSON_NUM] [varchar](1) NULL,
	[PERSON_COD] [varchar](4) NULL,
	[TYPE] [varchar](1) NULL,
	[STATUS] [varchar](2) NULL,
	[SPECIALTY] [varchar](2) NULL,
	[PERCENTAGE] [numeric](3, 0) NULL,
	[ROLE] [varchar](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Litigation].[Cnsinvol](
	[LIT_CODE] [varchar](4) NULL,
	[BODY] [varchar](40) NULL,
	[PERCENTAGE] [numeric](3, 0) NULL
) ON [PRIMARY]
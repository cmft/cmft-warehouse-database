﻿CREATE TABLE [Litigation].[CNSSTATU](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[CNST_CODE] [varchar](6) NULL,
	[TYPE] [varchar](2) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Litigation].[Rgfile](
	[FILE_IDENT] [varchar](2) NULL,
	[ORDER] [varchar](1) NULL,
	[FILE_NAME] [varchar](10) NULL,
	[FILE_DESC] [varchar](21) NULL,
	[CONNECT] [varchar](40) NULL,
	[FILE_INDEX] [varchar](10) NULL,
	[FIELD_IND] [varchar](60) NULL,
	[RELATED] [varchar](30) NULL,
	[LOOK_NUM] [numeric](1, 0) NULL,
	[LOOK_EXT] [varchar](2) NULL,
	[COPY_INDEX] [bit] NULL,
	[GROUP] [varchar](5) NULL,
	[LOOK_INDEX] [varchar](10) NULL,
	[LOOK_FIELD] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SABS].[alertactions](
	[ALERT_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[REPLY_TYPE] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[REASON] [varchar](2) NULL,
	[ENTRY_DATE] [date] NULL,
	[ENTRY_TIME] [varchar](5) NULL,
	[ENTRY_BY] [varchar](4) NULL,
	[TYPE] [numeric](1, 0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [SABS].[DISTRIBUTIONGROUP](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[COLOUR] [numeric](10, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[DIST_TYPE] [varchar](2) NULL,
	[ORDER] [varchar](2) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SABS].[ifiles](
	[PROG_NAME] [varchar](10) NULL,
	[FILE_NAME] [varchar](30) NULL,
	[FILE_DESC] [varchar](29) NULL,
	[FILE_INDEX] [varchar](40) NULL,
	[FIELD_IND] [varchar](120) NULL,
	[DATA_TYPE] [varchar](1) NULL
) ON [PRIMARY]
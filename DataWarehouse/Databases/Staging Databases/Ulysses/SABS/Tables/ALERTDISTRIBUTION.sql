﻿CREATE TABLE [SABS].[ALERTDISTRIBUTION](
	[ALERT_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[TYPE] [varchar](2) NULL,
	[DIST_CODE] [varchar](2) NULL,
	[STAFF_CODE] [varchar](4) NULL,
	[SENT] [numeric](2, 0) NULL,
	[RECEIVED] [numeric](2, 0) NULL,
	[REPLY_TYPE] [varchar](2) NULL,
	[READ] [varchar](20) NULL,
	[RESPON_BY1] [date] NULL,
	[RESPON_BY2] [date] NULL,
	[RESPON_BY3] [date] NULL,
	[REMIND_DT] [date] NULL,
	[REMIND_TM] [varchar](5) NULL,
	[CHILD] [varchar](4) NULL,
	[MAIN_ALERT] [varchar](4) NULL
) ON [PRIMARY]
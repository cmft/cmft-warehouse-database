﻿CREATE TABLE [SABS].[Ifields_old](
	[FILE] [varchar](20) NULL,
	[FIELD_DESC] [varchar](40) NULL,
	[FIELD_NAME] [varchar](10) NULL,
	[FIELD_TYPE] [varchar](1) NULL,
	[FIELD_LEN] [numeric](3, 0) NULL,
	[FIELD_DEC] [numeric](2, 0) NULL,
	[LOOK_UP] [varchar](20) NULL,
	[HIDE] [bit] NULL,
	[REMOVE] [bit] NULL,
	[CONVERT] [bit] NULL
) ON [PRIMARY]
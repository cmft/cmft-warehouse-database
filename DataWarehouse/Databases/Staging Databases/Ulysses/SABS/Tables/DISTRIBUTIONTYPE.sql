﻿CREATE TABLE [SABS].[DISTRIBUTIONTYPE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[DEFAULT] [bit] NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[PREFIX] [varchar](10) NULL
) ON [PRIMARY]
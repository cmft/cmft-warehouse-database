﻿CREATE TABLE [PPI].[questionnairelist](
	[QUEST_GRP1] [varchar](2) NULL,
	[QUEST_GRP2] [varchar](2) NULL,
	[UNIQUE] [varchar](2) NULL,
	[DESCRIPT] [varchar](80) NULL,
	[TYPE] [varchar](1) NULL,
	[WIDTH] [numeric](4, 1) NULL,
	[ORDER] [varchar](2) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
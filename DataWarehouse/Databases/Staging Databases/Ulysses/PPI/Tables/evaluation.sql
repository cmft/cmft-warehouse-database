﻿CREATE TABLE [PPI].[evaluation](
	[PROJ_CODE] [varchar](4) NULL,
	[ACT_CODE] [varchar](2) NULL,
	[EVAL_CODE] [varchar](2) NULL,
	[EVIDENCE] [varchar](2) NULL,
	[DETAILS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
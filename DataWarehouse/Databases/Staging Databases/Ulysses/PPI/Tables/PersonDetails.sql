﻿CREATE TABLE [PPI].[PersonDetails](
	[PERSON_COD] [varchar](4) NULL,
	[MARITAL_ST] [varchar](2) NULL,
	[SPEC_NEEDS] [varchar](2) NULL,
	[PREF_CONT] [varchar](2) NULL,
	[LEVEL_CONT] [varchar](2) NULL,
	[SURVEILLAN] [varchar](2) NULL,
	[NOT_CONT] [bit] NULL,
	[NOTES] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
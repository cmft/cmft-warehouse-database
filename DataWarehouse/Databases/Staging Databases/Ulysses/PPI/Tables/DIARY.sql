﻿CREATE TABLE [PPI].[DIARY](
	[STAFF_CODE] [varchar](4) NULL,
	[APP_DATE] [date] NULL,
	[TIME] [numeric](5, 2) NULL,
	[DURATION] [numeric](3, 0) NULL,
	[DESCRIPT] [varchar](40) NULL,
	[LOCATION] [varchar](40) NULL,
	[NOTE] [text] NULL,
	[WHO_INVOL] [text] NULL,
	[DIARY_TYPE] [varchar](1) NULL,
	[ID] [varchar](4) NULL,
	[LOC_TYPE] [varchar](2) NULL,
	[TOTAL_INV] [numeric](3, 0) NULL,
	[MAX_INV] [numeric](3, 0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [PPI].[questionnaire](
	[PROJ_CODE] [varchar](4) NULL,
	[ACT_CODE] [varchar](2) NULL,
	[TYPE] [varchar](2) NULL,
	[QUEST_GRP1] [varchar](2) NULL,
	[QUEST_GRP2] [varchar](2) NULL,
	[ANSWER] [varchar](2) NULL,
	[SPECIFY1] [varchar](20) NULL,
	[SPECIFY2] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
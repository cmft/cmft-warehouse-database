﻿CREATE TABLE [PPI].[QUESTIONNAIRETYPE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[COLOUR1] [numeric](10, 0) NULL,
	[COLOUR2] [numeric](10, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[RULES] [text] NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[ORDER] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [PPI].[FEEDBACK](
	[PROJ_CODE] [varchar](4) NULL,
	[ACT_CODE] [varchar](2) NULL,
	[FEED_CODE] [varchar](2) NULL,
	[FEED_TYPE] [varchar](2) NULL,
	[NUM_PEOPLE] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[PERSON] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [PPI].[PersonInterests](
	[PERSON_COD] [varchar](4) NULL,
	[THEME] [varchar](2) NULL,
	[SUB_THEME] [varchar](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [PPI].[THEMES](
	[PROJ_CODE] [varchar](4) NULL,
	[ACT_CODE] [varchar](2) NULL,
	[THEM_CODE] [varchar](2) NULL,
	[THEME] [varchar](2) NULL,
	[SUB_THEME] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[RESPONDERS] [varchar](2) NULL,
	[SITE] [varchar](2) NULL,
	[DEPARTMENT] [varchar](2) NULL,
	[DIRECTOR] [varchar](2) NULL,
	[SPECIALTY] [varchar](2) NULL,
	[FEED_TYPE] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [PPI].[DiaryTyp](
	[CODE] [varchar](1) NULL,
	[DESCRIPT] [varchar](60) NULL,
	[ABBREVIATE] [varchar](5) NULL,
	[INACTIVE] [bit] NULL,
	[COLOUR] [numeric](10, 0) NULL
) ON [PRIMARY]
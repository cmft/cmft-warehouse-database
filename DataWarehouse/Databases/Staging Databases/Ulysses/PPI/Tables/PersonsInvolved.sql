﻿CREATE TABLE [PPI].[PersonsInvolved](
	[PROJ_CODE] [varchar](4) NULL,
	[ACT_CODE] [varchar](2) NULL,
	[PERSON] [varchar](4) NULL,
	[DATE_SENT] [date] NULL,
	[RESPONSE] [varchar](2) NULL,
	[ATTENDED] [bit] NULL,
	[FOLLOWUP] [varchar](2) NULL,
	[FOLLOW_DET] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [PPI].[AUDIT](
	[DATE] [date] NULL,
	[TIME] [varchar](6) NULL,
	[USER] [varchar](10) NULL,
	[ACTION] [varchar](40) NULL,
	[OPERATION] [varchar](2) NULL,
	[RECORD_ID] [varchar](4) NULL,
	[ERROR] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
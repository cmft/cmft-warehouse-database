﻿CREATE TABLE [Complain].[LETTERS](
	[COMP_CODE] [varchar](4) NULL,
	[LETTER_TYP] [varchar](2) NULL,
	[DATE_DUE] [date] NULL,
	[DATE_SENT] [date] NULL,
	[LETTER] [text] NULL,
	[CREATED_BY] [varchar](10) NULL,
	[ATTACH_ID] [varchar](6) NULL,
	[TYPE] [varchar](2) NULL,
	[TITLE] [varchar](40) NULL,
	[TIME_SENT] [varchar](5) NULL,
	[LET_CODE] [varchar](2) NULL,
	[STAFF_CODE] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Complain].[compdelayreason](
	[COMP_CODE] [varchar](4) NULL,
	[REASON] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[NEW_DATE] [date] NULL,
	[LAST_DATE] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
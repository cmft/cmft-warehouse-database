﻿CREATE TABLE [Complain].[Review](
	[COMP_CODE] [varchar](4) NULL,
	[REQUEST_DT] [date] NULL,
	[ACKNOW_DT] [date] NULL,
	[CONTINUE] [varchar](1) NULL,
	[REFUSE_DET] [text] NULL,
	[REFUSE_CAT] [varchar](2) NULL,
	[DATE_INFOR] [date] NULL,
	[APPOINT_DT] [date] NULL,
	[REPORT_DT] [date] NULL,
	[CONTACT_DT] [date] NULL,
	[REVIEW_DET] [text] NULL,
	[OUTCOME] [varchar](2) NULL,
	[DRAFT_REP] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Complain].[hcc_comp](
	[COMP_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](1) NULL,
	[START_DATE] [date] NULL,
	[END_DATE] [date] NULL,
	[OUTCOME] [varchar](2) NULL,
	[OUT_DETAIL] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
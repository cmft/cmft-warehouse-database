﻿CREATE TABLE [Complain].[COMPREOPENED](
	[COMP_CODE] [varchar](4) NULL,
	[RECEIVE_DT] [date] NULL,
	[ACKNOW_DT] [date] NULL,
	[ACKNOW_DAY] [numeric](3, 0) NULL,
	[INIT_CONT] [date] NULL,
	[INIT_DUE] [date] NULL,
	[RESPON_DUE] [date] NULL,
	[RESPON_DT] [date] NULL,
	[RESOLVED] [bit] NULL,
	[REASON] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[ENTERED_BY] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
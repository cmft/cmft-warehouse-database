﻿CREATE TABLE [Complain].[Rgsetup](
	[NAME] [varchar](20) NULL,
	[SYS_NAME] [varchar](80) NULL,
	[VERSION] [varchar](3) NULL,
	[NUMBER] [varchar](1) NULL,
	[ORS_ANDS] [bit] NULL,
	[USER_FILE] [varchar](8) NULL,
	[LAST_NO] [numeric](5, 0) NULL,
	[DISP_EXT] [bit] NULL
) ON [PRIMARY]
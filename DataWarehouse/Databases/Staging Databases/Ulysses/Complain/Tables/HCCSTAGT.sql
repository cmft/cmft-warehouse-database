﻿CREATE TABLE [Complain].[HCCSTAGT](
	[CODE] [varchar](2) NULL,
	[ORDER] [varchar](2) NULL,
	[DESCRIPT] [varchar](40) NULL,
	[DISPLAY] [bit] NULL,
	[NO_DAYS] [numeric](3, 0) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Complain].[imaint2](
	[FILE_NAME] [varchar](20) NULL,
	[FIELD_NAME] [varchar](10) NULL,
	[CAPTION] [varchar](30) NULL,
	[LOOKUP] [varchar](20) NULL,
	[HIDE] [bit] NULL,
	[ORDER] [varchar](2) NULL
) ON [PRIMARY]
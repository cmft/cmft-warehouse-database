﻿CREATE TABLE [Complain].[Compcont](
	[COMP_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[CONT_TYPE] [varchar](2) NULL,
	[CONT_DATE] [date] NULL,
	[DETAILS] [text] NULL,
	[SPENT_HRS] [numeric](2, 0) NULL,
	[SPENT_MINS] [numeric](2, 0) NULL,
	[CONT_TIME] [varchar](5) NULL,
	[SHOW_ONWEB] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Complain].[Received](
	[COMP_CODE] [varchar](4) NULL,
	[REC_FROM] [varchar](2) NULL,
	[RECEIVE_DT] [date] NULL,
	[ACKNOW_DT] [date] NULL
) ON [PRIMARY]
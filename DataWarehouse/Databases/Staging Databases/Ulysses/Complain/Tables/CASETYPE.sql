﻿CREATE TABLE [Complain].[CASETYPE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[DAYS_RSP] [numeric](2, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[KO41_TYPE] [varchar](1) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[DAYS_RSP2] [numeric](2, 0) NULL,
	[DAYS_ROPN1] [numeric](2, 0) NULL,
	[DAYS_ROPN2] [numeric](2, 0) NULL,
	[DAYS_RSP3] [numeric](2, 0) NULL,
	[PREFIX] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Complain].[RISKRATE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[SCORE] [varchar](50) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[COLOUR] [numeric](10, 0) NULL,
	[FROM_SCORE] [numeric](3, 0) NULL,
	[TO_SCORE] [numeric](3, 0) NULL
) ON [PRIMARY]
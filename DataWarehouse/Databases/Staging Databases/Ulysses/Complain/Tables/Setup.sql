﻿CREATE TABLE [Complain].[Setup](
	[SYS_NAME] [varchar](20) NULL,
	[VERSION] [varchar](3) NULL,
	[SHADE] [varchar](1) NULL,
	[RESPONSE_T] [numeric](2, 0) NULL
) ON [PRIMARY]
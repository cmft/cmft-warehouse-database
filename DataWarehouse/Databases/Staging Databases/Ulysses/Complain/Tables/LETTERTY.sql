﻿CREATE TABLE [Complain].[LETTERTY](
	[LETTER_NO] [varchar](2) NULL,
	[DESCRIPT] [varchar](30) NULL,
	[LETTER] [text] NULL,
	[CREATED_DT] [varchar](19) NULL,
	[CREATED_BY] [varchar](10) NULL,
	[MODIFIE_DT] [varchar](19) NULL,
	[MODIFIE_BY] [varchar](10) NULL,
	[FILE_NAME] [text] NULL,
	[INACTIVE] [bit] NULL,
	[PAGE_SET] [varchar](20) NULL,
	[CHANGE_DT] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
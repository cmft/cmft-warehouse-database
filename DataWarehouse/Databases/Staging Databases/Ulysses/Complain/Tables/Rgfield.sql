﻿CREATE TABLE [Complain].[Rgfield](
	[FIELD_NAME] [varchar](140) NULL,
	[FIELD_DESC] [varchar](20) NULL,
	[IN] [varchar](2) NULL,
	[SCN_NO] [varchar](2) NULL,
	[TYPE] [varchar](1) NULL,
	[WIDTH] [numeric](2, 0) NULL,
	[DEC] [numeric](1, 0) NULL,
	[DISP_DESC] [varchar](15) NULL,
	[EXT_FILE] [varchar](11) NULL,
	[LOOK_UP] [varchar](2) NULL,
	[LOCK] [bit] NULL,
	[DATE_INDEX] [varchar](8) NULL,
	[DUMMY] [bit] NULL
) ON [PRIMARY]
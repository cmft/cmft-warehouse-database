﻿CREATE TABLE [Complain].[Comperso](
	[COMP_CODE] [varchar](4) NULL,
	[PERSON_NUM] [varchar](1) NULL,
	[PERSON_COD] [varchar](4) NULL,
	[TYPE] [varchar](1) NULL,
	[REASON_INV] [varchar](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Complain].[HCC_STAGE](
	[COMP_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](1) NULL,
	[STAGE] [varchar](2) NULL,
	[START] [date] NULL,
	[TARGET] [date] NULL,
	[REMINDER] [date] NULL,
	[ACTUAL] [date] NULL,
	[DETAIL] [text] NULL,
	[DELAY_REAS] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
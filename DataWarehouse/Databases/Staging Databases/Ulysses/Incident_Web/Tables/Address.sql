﻿CREATE TABLE [Incident_Web].[Address](
	[NAME] [varchar](30) NULL,
	[ADDRESS1] [varchar](30) NULL,
	[ADDRESS2] [varchar](30) NULL,
	[ADDRESS3] [varchar](30) NULL,
	[POSTCODE] [varchar](8) NULL,
	[NAME_TEL] [varchar](30) NULL,
	[BUSINESS] [varchar](30) NULL,
	[NUM_EMPLOY] [numeric](5, 0) NULL,
	[ROLE_COMP] [varchar](1) NULL,
	[JOBTITLE] [varchar](30) NULL,
	[TELEPHONE] [varchar](20) NULL
) ON [PRIMARY]
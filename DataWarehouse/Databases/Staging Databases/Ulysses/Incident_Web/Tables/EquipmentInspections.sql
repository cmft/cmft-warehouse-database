﻿CREATE TABLE [Incident_Web].[EquipmentInspections](
	[EQUIP_CODE] [varchar](4) NULL,
	[INSPECT_DT] [date] NULL,
	[INSPECT_TM] [varchar](5) NULL,
	[WHO_BY] [varchar](4) NULL,
	[DETAILS] [text] NULL,
	[STATUS_TY] [varchar](2) NULL,
	[OUTCOME] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Incident_Web].[LESSONSLEARNED](
	[INC_CODE] [varchar](4) NULL,
	[UNIQUE] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[TYPE] [varchar](2) NULL,
	[COMPLET_DT] [date] NULL,
	[AUTHOR] [varchar](4) NULL,
	[RCA_CODE] [varchar](4) NULL,
	[PROB_CODE] [varchar](2) NULL,
	[CONT_CODE] [varchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
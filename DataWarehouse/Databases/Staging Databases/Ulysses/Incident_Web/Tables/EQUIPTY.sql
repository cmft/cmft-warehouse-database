﻿CREATE TABLE [Incident_Web].[EQUIPTY](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[HSE_NUMBER] [numeric](2, 0) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[MDA_TYPE] [varchar](2) NULL,
	[NPSA1] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL
) ON [PRIMARY]
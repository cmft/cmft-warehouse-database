﻿CREATE TABLE [Incident_Web].[ATTACH](
	[INC_CODE] [varchar](4) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[ATTACH_ID] [varchar](6) NULL,
	[DETAILS] [text] NULL,
	[FILE_NAME] [text] NULL,
	[ATTACH_DT] [date] NULL,
	[ATTACH_TM] [varchar](5) NULL,
	[SHOW_ONWEB] [bit] NULL,
	[MAIN_TYPE] [varchar](1) NULL,
	[ENTERED_BY] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Incident_Web].[Rgad_age](
	[TO] [numeric](3, 0) NULL,
	[DESC] [varchar](11) NULL,
	[SYSTEM] [varchar](11) NULL,
	[GROUP] [numeric](3, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident_Web].[EquipmentLocation](
	[EQUIP_CODE] [varchar](4) NULL,
	[ORGANISE] [varchar](2) NULL,
	[SITE] [varchar](2) NULL,
	[DEPARTMENT] [varchar](2) NULL,
	[DETAILS] [text] NULL,
	[CONTACT] [varchar](4) NULL,
	[FROM_DATE] [date] NULL,
	[TO_DATE] [date] NULL,
	[IN_USE] [varchar](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
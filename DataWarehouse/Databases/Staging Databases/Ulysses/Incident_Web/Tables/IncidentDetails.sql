﻿CREATE TABLE [Incident_Web].[IncidentDetails](
	[INC_CODE] [varchar](4) NULL,
	[TYPE] [varchar](2) NULL,
	[DETAIL] [text] NULL,
	[ENTERED_DT] [date] NULL,
	[ENTERED_TM] [varchar](5) NULL,
	[ENTERED_BY] [varchar](4) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Incident_Web].[DOCTORS](
	[INC_CODE] [varchar](5) NULL,
	[GP] [varchar](4) NULL,
	[PRACTICE] [varchar](4) NULL,
	[CONSULTANT] [varchar](2) NULL,
	[DIAGNOSIS] [varchar](35) NULL
) ON [PRIMARY]
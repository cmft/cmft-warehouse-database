﻿CREATE TABLE [Incident_Web].[INTMEDS](
	[INC_CODE] [varchar](5) NULL,
	[DRUG_GIVEN] [varchar](4) NULL,
	[DOSE_GIVEN] [varchar](20) NULL,
	[ROUTE] [varchar](2) NULL,
	[TIME] [varchar](5) NULL,
	[AUTH_BY] [varchar](4) NULL
) ON [PRIMARY]
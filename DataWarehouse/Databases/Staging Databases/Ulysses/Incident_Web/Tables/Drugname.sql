﻿CREATE TABLE [Incident_Web].[Drugname](
	[CODE] [varchar](4) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[TYPE] [varchar](2) NULL,
	[INACTIVE] [bit] NULL,
	[ABBREVIATE] [varchar](5) NULL,
	[DRUG_CODE] [varchar](10) NULL,
	[LINK_ID] [varchar](10) NULL,
	[CONT_DRUG] [bit] NULL,
	[PROTECT] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident_Web].[Violence](
	[INC_CODE] [varchar](4) NULL,
	[COST_DAMAG] [numeric](7, 0) NULL,
	[CHARGE] [bit] NULL,
	[INC_FIRE] [bit] NULL,
	[DET_MHA] [bit] NULL,
	[SECLUSION] [bit] NULL,
	[NUMBER] [numeric](6, 0) NULL,
	[POLICE] [bit] NULL,
	[WEAPON] [bit] NULL
) ON [PRIMARY]
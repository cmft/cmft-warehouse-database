﻿CREATE TABLE [Incident_Web].[EXTDEFME](
	[EXTCODE] [varchar](4) NULL,
	[EXTCODE2] [varchar](4) NULL,
	[SYSTEM] [varchar](2) NULL,
	[COMBINE] [bit] NULL,
	[MULTI_SYS] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident_Web].[Ifiles](
	[PROG_NAME] [varchar](10) NULL,
	[FILE_NAME] [varchar](30) NULL,
	[FILE_DESC] [varchar](29) NULL,
	[FILE_INDEX] [varchar](50) NULL,
	[FIELD_IND] [varchar](80) NULL,
	[DATA_TYPE] [varchar](1) NULL
) ON [PRIMARY]
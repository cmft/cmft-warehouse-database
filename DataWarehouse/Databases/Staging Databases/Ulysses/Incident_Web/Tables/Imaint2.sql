﻿CREATE TABLE [Incident_Web].[Imaint2](
	[FILE_NAME] [varchar](20) NULL,
	[FIELD_NAME] [varchar](10) NULL,
	[CAPTION] [varchar](30) NULL,
	[LOOKUP] [varchar](30) NULL,
	[HIDE] [bit] NULL,
	[ORDER] [varchar](2) NULL
) ON [PRIMARY]
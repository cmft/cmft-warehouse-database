﻿CREATE TABLE [Incident_Web].[REP_SENT](
	[INC_CODE] [varchar](4) NULL,
	[REPORT_TYP] [varchar](2) NULL,
	[SENT] [varchar](1) NULL,
	[REPORT_DT] [date] NULL,
	[REPORT_BY] [varchar](25) NULL,
	[REF_NUM] [varchar](30) NULL,
	[DETAILS1] [text] NULL,
	[DETAILS2] [text] NULL,
	[TO_REPORT] [varchar](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
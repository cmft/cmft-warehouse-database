﻿CREATE TABLE [Incident_Web].[INCPERSO](
	[INC_CODE] [varchar](4) NULL,
	[PERSON_NUM] [varchar](1) NULL,
	[PERSON_COD] [varchar](4) NULL,
	[ROLE] [varchar](2) NULL,
	[TYPE] [varchar](1) NULL,
	[PERSON_TY] [varchar](2) NULL,
	[STAFF_HST] [varchar](2) NULL,
	[OPTIONSY] [numeric](3, 0) NULL,
	[OPTIONSN] [numeric](3, 0) NULL
) ON [PRIMARY]
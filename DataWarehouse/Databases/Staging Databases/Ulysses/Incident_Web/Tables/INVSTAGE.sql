﻿CREATE TABLE [Incident_Web].[INVSTAGE](
	[INC_CODE] [varchar](4) NULL,
	[STAGE] [varchar](2) NULL,
	[START_DATE] [date] NULL,
	[START_TIME] [varchar](5) NULL,
	[DETAIL] [text] NULL,
	[TARGET_DT] [date] NULL,
	[COMPLET_BY] [varchar](4) NULL,
	[START_DT] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
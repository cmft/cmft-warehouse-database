﻿CREATE TABLE [Incident_Web].[Imaint1](
	[GROUP] [varchar](10) NULL,
	[FILE_NAME] [varchar](50) NULL,
	[CAPTION] [varchar](50) NULL,
	[CALLMETHOD] [varchar](20) NULL,
	[MERGEMAINT] [numeric](1, 0) NULL,
	[ORDERFIELD] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident_Web].[NPSACODE](
	[REF_CODE] [varchar](10) NULL,
	[LABEL] [varchar](100) NULL,
	[ITEM_CODE] [varchar](10) NULL,
	[PARENT] [varchar](10) NULL,
	[DESCRIPT] [varchar](100) NULL,
	[CAREMATRIX] [varchar](10) NULL
) ON [PRIMARY]
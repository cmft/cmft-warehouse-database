﻿CREATE TABLE [Incident_Web].[Dis_rpt](
	[INC_CODE] [varchar](4) NULL,
	[DISEASE_TY] [varchar](2) NULL,
	[DISEASE_NO] [varchar](20) NULL,
	[WORK_TYPE] [varchar](2) NULL,
	[DR_REPORT] [varchar](25) NULL,
	[STATE_DATE] [date] NULL
) ON [PRIMARY]
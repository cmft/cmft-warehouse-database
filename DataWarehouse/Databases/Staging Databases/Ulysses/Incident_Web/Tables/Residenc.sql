﻿CREATE TABLE [Incident_Web].[Residenc](
	[INC_CODE] [varchar](5) NULL,
	[SITE] [varchar](2) NULL,
	[DEPARTMENT] [varchar](2) NULL,
	[ADMISS_DT] [date] NULL,
	[ORGANISE] [varchar](2) NULL
) ON [PRIMARY]
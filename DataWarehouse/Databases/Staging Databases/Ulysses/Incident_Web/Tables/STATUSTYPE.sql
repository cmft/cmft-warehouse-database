﻿CREATE TABLE [Incident_Web].[STATUSTYPE](
	[CODE] [varchar](2) NULL,
	[DESCRIPT] [varchar](50) NULL,
	[INACTIVE] [bit] NULL,
	[COLOUR] [numeric](10, 0) NULL,
	[WEB_PAGE] [varchar](10) NULL,
	[TYPE1] [varchar](1) NULL,
	[TYPE2] [varchar](1) NULL,
	[ABBREVIATE] [varchar](10) NULL,
	[ABBREVIAT2] [varchar](10) NULL,
	[ABBREVIAT3] [varchar](10) NULL,
	[DESCRIPT2] [varchar](50) NULL,
	[DAYS_GRACE] [numeric](2, 0) NULL,
	[CLOSED] [bit] NULL
) ON [PRIMARY]
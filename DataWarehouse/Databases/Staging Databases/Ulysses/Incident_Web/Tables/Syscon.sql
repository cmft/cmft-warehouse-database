﻿CREATE TABLE [Incident_Web].[Syscon](
	[DEVICE] [varchar](1) NULL,
	[ROTATION] [numeric](1, 0) NULL,
	[WEEK_DAY] [numeric](1, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Incident_Web].[NPSAFLDS](
	[FILE] [varchar](50) NULL,
	[FIELD_NAME] [varchar](50) NULL,
	[LOOK_CODE] [varchar](100) NULL,
	[LOOKUP_FLD] [varchar](30) NULL,
	[LOOK_TEXT] [varchar](100) NULL,
	[SEND_TEXT] [varchar](1) NULL,
	[REF_CODE] [varchar](10) NULL,
	[ENTITY] [varchar](30) NULL,
	[DESCRIPT] [varchar](100) NULL,
	[MANDATORY] [numeric](2, 0) NULL,
	[MAND_CHECK] [varchar](10) NULL,
	[TITLE] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Stats].[ColumnAnalysis](
	[schemaName] [sysname] NOT NULL,
	[tableName] [sysname] NOT NULL,
	[columnName] [sysname] NOT NULL,
	[hasNulls] [bit] NULL,
	[isNumeric] [bit] NULL,
	[maxLen] [int] NULL,
	[nullRows] [int] NULL,
	[allNull]  AS (case when [maxLen] IS NULL then (1) else (0) end)
) ON [PRIMARY]
﻿CREATE TABLE [IVF].[RForm](
	[FormName] [varchar](100) NULL,
	[Version] [int] NULL,
	[FormType] [varchar](5) NULL,
	[MergeID] [int] NULL,
	[CentreCode] [nvarchar](max) NULL,
	[EnvelopeNumber] [nvarchar](max) NULL,
	[EnvelopeReceived] [nvarchar](max) NULL,
	[FormNumber] [nvarchar](max) NULL,
	[FormVerID] [nvarchar](max) NULL,
	[FormDate] [nvarchar](max) NULL,
	[PatientRegisteredDate] [nvarchar](max) NULL,
	[PatientDonorNo] [nvarchar](max) NULL,
	[CurrentForenames] [nvarchar](max) NULL,
	[CurrentSurname] [nvarchar](max) NULL,
	[BirthSurname] [nvarchar](max) NULL,
	[PatientDOB] [nvarchar](max) NULL,
	[BirthTown] [nvarchar](max) NULL,
	[BirthCountry] [nvarchar](max) NULL,
	[NHSNumber] [nvarchar](max) NULL,
	[PatientEthnicGroupID] [nvarchar](max) NULL,
	[IVFTotalPreviousPregnancies] [nvarchar](max) NULL,
	[TotalLiveBirths] [nvarchar](max) NULL,
	[InfertilityOvulatoryDisorder] [nvarchar](max) NULL,
	[RecState] [nvarchar](max) NULL,
	[HasPartner] [nvarchar](max) NULL,
	[InfertilityDuration] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
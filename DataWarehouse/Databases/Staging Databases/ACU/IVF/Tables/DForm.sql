﻿CREATE TABLE [IVF].[DForm](
	[FormName] [varchar](100) NULL,
	[Version] [int] NULL,
	[FormType] [varchar](5) NULL,
	[MergeID] [int] NULL,
	[CentreCode] [nvarchar](max) NULL,
	[EnvelopeNumber] [nvarchar](max) NULL,
	[EnvelopeReceived] [nvarchar](max) NULL,
	[FormNumber] [nvarchar](max) NULL,
	[FormVerID] [nvarchar](max) NULL,
	[FormDate] [nvarchar](max) NULL,
	[DonorNo] [nvarchar](max) NULL,
	[DonorDOB] [nvarchar](max) NULL,
	[ApproxDOB] [nvarchar](max) NULL,
	[CurrentSurname] [nvarchar](max) NULL,
	[CurrentForenames] [nvarchar](max) NULL,
	[MaleFemale] [nvarchar](max) NULL,
	[BirthTown] [nvarchar](max) NULL,
	[BirthDistrict] [nvarchar](max) NULL,
	[GametesFirstUsed] [nvarchar](max) NULL,
	[PreviousDonations] [nvarchar](max) NULL,
	[OwnChildren] [nvarchar](max) NULL,
	[HeightM] [nvarchar](max) NULL,
	[WeightKGS] [nvarchar](max) NULL,
	[EthnicGroupLookUpID] [nvarchar](max) NULL,
	[EyeColourLookUpID] [nvarchar](max) NULL,
	[HairColourLookUpID] [nvarchar](max) NULL,
	[SkinColourLookUpID] [nvarchar](max) NULL,
	[ReligionLookUpID] [nvarchar](max) NULL,
	[Occupation] [nvarchar](max) NULL,
	[RecState] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
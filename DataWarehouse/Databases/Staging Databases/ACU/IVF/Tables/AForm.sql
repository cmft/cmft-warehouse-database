﻿CREATE TABLE [IVF].[AForm](
	[FormName] [varchar](100) NULL,
	[Version] [int] NULL,
	[FormType] [varchar](5) NULL,
	[MergeID] [int] NULL,
	[CentreCode] [nvarchar](max) NULL,
	[EnvelopeNumber] [nvarchar](max) NULL,
	[EnvelopeReceived] [nvarchar](max) NULL,
	[FormNumber] [nvarchar](max) NULL,
	[FormVerID] [nvarchar](max) NULL,
	[FormItemID] [nvarchar](max) NULL,
	[FormDate] [nvarchar](max) NULL,
	[PatientDonorNo] [nvarchar](max) NULL,
	[CurrentSurname] [nvarchar](max) NULL,
	[TreatmentDate] [nvarchar](max) NULL,
	[RecState] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
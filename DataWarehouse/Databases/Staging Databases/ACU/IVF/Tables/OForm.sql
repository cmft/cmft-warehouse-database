﻿CREATE TABLE [IVF].[OForm](
	[FormName] [varchar](100) NULL,
	[Version] [int] NULL,
	[FormType] [varchar](5) NULL,
	[MergeID] [int] NULL,
	[CentreCode] [nvarchar](max) NULL,
	[EnvelopeNumber] [nvarchar](max) NULL,
	[EnvelopeReceived] [nvarchar](max) NULL,
	[FormNumber] [nvarchar](max) NULL,
	[FormVerID] [nvarchar](max) NULL,
	[FormDate] [nvarchar](max) NULL,
	[PatientDonorNo] [nvarchar](max) NULL,
	[CurrentSurname] [nvarchar](max) NULL,
	[IVF] [nvarchar](max) NULL,
	[IVFForm] [nvarchar](max) NULL,
	[CycleDate] [nvarchar](max) NULL,
	[PulsatingSacs] [nvarchar](max) NULL,
	[Heart1SacNo] [nvarchar](max) NULL,
	[Heart1GestationWeeks] [nvarchar](max) NULL,
	[PregnancyState1] [nvarchar](max) NULL,
	[Heart1DeliveryDate] [nvarchar](max) NULL,
	[Heart2Termination] [nvarchar](max) NULL,
	[Heart3Termination] [nvarchar](max) NULL,
	[BirthCountry] [nvarchar](max) NULL,
	[RecState] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
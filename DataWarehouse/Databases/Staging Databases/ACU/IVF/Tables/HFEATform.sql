﻿CREATE TABLE [IVF].[HFEATform](
	[FormNumber] [nvarchar](50) NULL,
	[PatientDonorNo] [nvarchar](10) NULL,
	[CurrentSurname] [nvarchar](50) NULL
) ON [PRIMARY]
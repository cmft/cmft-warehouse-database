﻿CREATE TABLE [Staging].[CONSITEM](
	[SerNo] [varchar](max) NULL,
	[CatSerno] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[LatestSerno] [varchar](max) NULL,
	[SortKey] [varchar](max) NULL,
	[Units] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
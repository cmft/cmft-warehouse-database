﻿CREATE TABLE [Staging].[LETTER](
	[Letter_Number] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[File_name] [varchar](max) NULL,
	[Envelope] [varchar](max) NULL,
	[Lettertype] [varchar](max) NULL,
	[LetterCode] [varchar](max) NULL,
	[LetterKey] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[KPINR](
	[Upper] [varchar](max) NULL,
	[Lower] [varchar](max) NULL,
	[KPITEXT] [varchar](max) NULL,
	[gst] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[GSTText] [varchar](max) NULL,
	[GSTTxtBox] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
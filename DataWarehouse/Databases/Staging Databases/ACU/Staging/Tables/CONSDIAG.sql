﻿CREATE TABLE [Staging].[CONSDIAG](
	[hospnum] [varchar](max) NULL,
	[snumber] [varchar](max) NULL,
	[Diagnosis] [varchar](max) NULL,
	[apptsnum] [varchar](max) NULL,
	[sortdiag] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
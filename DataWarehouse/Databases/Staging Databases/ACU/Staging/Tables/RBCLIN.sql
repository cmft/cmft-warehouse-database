﻿CREATE TABLE [Staging].[RBCLIN](
	[clindate] [varchar](max) NULL,
	[appnum] [varchar](max) NULL,
	[calendar] [varchar](max) NULL,
	[dtcalendar] [varchar](max) NULL,
	[maxnum] [varchar](max) NULL,
	[num_avail] [varchar](max) NULL,
	[dayy] [varchar](max) NULL,
	[snumber] [varchar](max) NULL,
	[AppPM] [varchar](max) NULL,
	[AppAM] [varchar](max) NULL,
	[Updt] [varchar](max) NULL,
	[remov] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
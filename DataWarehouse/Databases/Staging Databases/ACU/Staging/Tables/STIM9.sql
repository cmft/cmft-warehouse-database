﻿CREATE TABLE [Staging].[STIM9](
	[hospnum] [varchar](max) NULL,
	[PatientName] [varchar](max) NULL,
	[Age] [varchar](max) NULL,
	[AttemptNo] [varchar](max) NULL,
	[Protocol] [varchar](max) NULL,
	[eggcollect] [varchar](max) NULL,
	[TotFollicles] [varchar](max) NULL,
	[follicles] [varchar](max) NULL,
	[tot_oocyte] [varchar](max) NULL,
	[pn_2] [varchar](max) NULL,
	[transfer] [varchar](max) NULL,
	[preg] [varchar](max) NULL,
	[IUP_num] [varchar](max) NULL,
	[outcome] [varchar](max) NULL,
	[stimdays] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
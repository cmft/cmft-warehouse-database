﻿CREATE TABLE [Staging].[PROC](
	[Procedure] [varchar](max) NULL,
	[Proc_Code] [varchar](max) NULL,
	[Code_Surg] [varchar](max) NULL,
	[Code_DCU] [varchar](max) NULL,
	[Code_Anaest] [varchar](max) NULL,
	[Code_Histology] [varchar](max) NULL,
	[Cost_Surg] [varchar](max) NULL,
	[Cost_DCU] [varchar](max) NULL,
	[Cost_Anaest] [varchar](max) NULL,
	[Cost_Hist] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[Proc_Cost] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
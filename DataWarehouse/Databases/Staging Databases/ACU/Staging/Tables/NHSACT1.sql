﻿CREATE TABLE [Staging].[NHSACT1](
	[hospnum] [varchar](max) NULL,
	[Action] [varchar](max) NULL,
	[DtAdded] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[ActionSNum] [varchar](max) NULL,
	[PCT] [varchar](max) NULL,
	[SortKey] [varchar](max) NULL,
	[PCTAction] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
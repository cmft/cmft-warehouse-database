﻿CREATE TABLE [Staging].[NHSWORK](
	[hospnum] [varchar](max) NULL,
	[TreatDate] [varchar](max) NULL,
	[TreatCost] [varchar](max) NULL,
	[DrugCost] [varchar](max) NULL,
	[TotalCost] [varchar](max) NULL,
	[Outcome] [varchar](max) NULL,
	[Audit] [varchar](max) NULL,
	[Source] [varchar](max) NULL,
	[NHS_List_Year] [varchar](max) NULL,
	[NHSSortKey] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[IVFSortKey] [varchar](max) NULL,
	[IUISortKey] [varchar](max) NULL,
	[Treatment] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[CLINMISS](
	[Name] [varchar](max) NULL,
	[hospnum] [varchar](max) NULL,
	[missing] [varchar](max) NULL,
	[dob] [varchar](max) NULL,
	[letter] [varchar](max) NULL,
	[snumber] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
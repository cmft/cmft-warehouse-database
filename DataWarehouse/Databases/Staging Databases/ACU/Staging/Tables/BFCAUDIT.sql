﻿CREATE TABLE [Staging].[BFCAUDIT](
	[PatientName] [varchar](max) NULL,
	[treatdate] [varchar](max) NULL,
	[embryofee] [varchar](max) NULL,
	[drugfee] [varchar](max) NULL,
	[income] [varchar](max) NULL,
	[outcome] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
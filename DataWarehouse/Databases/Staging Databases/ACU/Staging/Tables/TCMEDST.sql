﻿CREATE TABLE [Staging].[TCMEDST](
	[Name] [varchar](max) NULL,
	[Dose] [varchar](max) NULL,
	[Frequency] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[HCG] [varchar](max) NULL,
	[TCMEDS_Snum] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
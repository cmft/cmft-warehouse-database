﻿CREATE TABLE [Staging].[DONREC1](
	[Don_Name] [varchar](max) NULL,
	[Rec_Name] [varchar](max) NULL,
	[Don_Hosp] [varchar](max) NULL,
	[Rec_Hosp] [varchar](max) NULL,
	[Don_Text] [varchar](max) NULL,
	[Rec_Text] [varchar](max) NULL,
	[Title1] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
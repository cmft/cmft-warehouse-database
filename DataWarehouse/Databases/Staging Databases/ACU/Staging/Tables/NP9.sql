﻿CREATE TABLE [Staging].[NP9](
	[hospnum] [varchar](max) NULL,
	[fsurname] [varchar](max) NULL,
	[fforename] [varchar](max) NULL,
	[faddress1] [varchar](max) NULL,
	[faddress2] [varchar](max) NULL,
	[faddress3] [varchar](max) NULL,
	[faddress4] [varchar](max) NULL,
	[fpostcode] [varchar](max) NULL,
	[finitials] [varchar](max) NULL,
	[name] [varchar](max) NULL,
	[consult] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
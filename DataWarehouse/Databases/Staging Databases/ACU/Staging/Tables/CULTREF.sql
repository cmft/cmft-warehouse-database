﻿CREATE TABLE [Staging].[CULTREF](
	[Batch] [varchar](max) NULL,
	[Media] [varchar](max) NULL,
	[StartDate] [varchar](max) NULL,
	[UseBy] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[AddedBy] [varchar](max) NULL,
	[Media1] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[CALCAT](
	[Serno] [varchar](max) NULL,
	[CatalogName] [varchar](max) NULL,
	[Cal1] [varchar](max) NULL,
	[Cal2] [varchar](max) NULL,
	[Cal3] [varchar](max) NULL,
	[Cal4] [varchar](max) NULL,
	[Cal5] [varchar](max) NULL,
	[Cal6] [varchar](max) NULL,
	[Cal7] [varchar](max) NULL,
	[Cal8] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
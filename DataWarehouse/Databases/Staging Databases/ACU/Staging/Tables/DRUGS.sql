﻿CREATE TABLE [Staging].[DRUGS](
	[Drug_Name] [varchar](max) NULL,
	[Drug_Cost] [varchar](max) NULL,
	[Drug_Dose] [varchar](max) NULL,
	[Drug_group] [varchar](max) NULL,
	[Drug_ID] [varchar](max) NULL,
	[cmk_Cost] [varchar](max) NULL,
	[Drug_Unit] [varchar](max) NULL,
	[Drug_UnitName] [varchar](max) NULL,
	[SNum] [varchar](max) NULL,
	[type] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
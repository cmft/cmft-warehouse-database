﻿CREATE TABLE [Staging].[IVFAUDIT](
	[hospnum] [varchar](max) NULL,
	[treatdate] [varchar](max) NULL,
	[outcome] [varchar](max) NULL,
	[name] [varchar](max) NULL,
	[source] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
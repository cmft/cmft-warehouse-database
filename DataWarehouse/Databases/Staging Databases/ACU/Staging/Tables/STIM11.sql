﻿CREATE TABLE [Staging].[STIM11](
	[hospnum] [varchar](max) NULL,
	[PatientName] [varchar](max) NULL,
	[preg] [varchar](max) NULL,
	[eggcollect] [varchar](max) NULL,
	[follicles] [varchar](max) NULL,
	[oocytes] [varchar](max) NULL,
	[embryos] [varchar](max) NULL,
	[transfer] [varchar](max) NULL,
	[outcome] [varchar](max) NULL,
	[AttemptNo] [varchar](max) NULL,
	[Age] [varchar](max) NULL,
	[TotFollicles] [varchar](max) NULL,
	[Protocol] [varchar](max) NULL,
	[stimdays] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
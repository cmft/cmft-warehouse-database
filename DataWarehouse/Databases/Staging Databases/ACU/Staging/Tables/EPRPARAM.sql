﻿CREATE TABLE [Staging].[EPRPARAM](
	[ScanDocsDir] [varchar](max) NULL,
	[CacheDir] [varchar](max) NULL,
	[ArchNotes] [varchar](max) NULL,
	[StorNotes] [varchar](max) NULL,
	[ArchNum] [varchar](max) NULL,
	[scanref] [varchar](max) NULL,
	[ScanDir] [varchar](max) NULL,
	[ArchiveDir] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
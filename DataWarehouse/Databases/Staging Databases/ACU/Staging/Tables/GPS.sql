﻿CREATE TABLE [Staging].[GPS](
	[Practice_Name] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[Surname] [varchar](max) NULL,
	[Forenames] [varchar](max) NULL,
	[Initials] [varchar](max) NULL,
	[Address_Line1] [varchar](max) NULL,
	[Address_Line2] [varchar](max) NULL,
	[Address_Line3] [varchar](max) NULL,
	[Address_Line4] [varchar](max) NULL,
	[Address_Line5] [varchar](max) NULL,
	[Postcode] [varchar](max) NULL,
	[Telephone_Numbe] [varchar](max) NULL,
	[Snum] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[TCMEDS](
	[Name] [varchar](max) NULL,
	[Dose] [varchar](max) NULL,
	[Frequency] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[HCG] [varchar](max) NULL,
	[Units] [varchar](max) NULL,
	[SortKey] [varchar](max) NULL,
	[Current1] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
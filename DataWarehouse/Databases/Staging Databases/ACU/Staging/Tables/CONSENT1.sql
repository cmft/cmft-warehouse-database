﻿CREATE TABLE [Staging].[CONSENT1](
	[Descr] [varchar](max) NULL,
	[Hospnum] [varchar](max) NULL,
	[ConsentSNum] [varchar](max) NULL,
	[DateObtained] [varchar](max) NULL,
	[Result] [varchar](max) NULL,
	[Comment] [varchar](max) NULL,
	[AssignedBy] [varchar](max) NULL,
	[Snum] [varchar](max) NULL,
	[IDX1] [varchar](max) NULL,
	[Expired] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
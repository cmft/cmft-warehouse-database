﻿CREATE TABLE [Staging].[FIVE](
	[hospnum] [varchar](max) NULL,
	[PatientName] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[Date] [varchar](max) NULL,
	[Bridge] [varchar](max) NULL,
	[HFEA] [varchar](max) NULL,
	[Source] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
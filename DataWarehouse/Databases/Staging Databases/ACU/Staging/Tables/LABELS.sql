﻿CREATE TABLE [Staging].[LABELS](
	[Description] [varchar](max) NULL,
	[PageType] [varchar](max) NULL,
	[NoOnSheet] [varchar](max) NULL,
	[Width] [varchar](max) NULL,
	[Height] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
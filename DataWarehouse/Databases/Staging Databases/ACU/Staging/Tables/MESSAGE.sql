﻿CREATE TABLE [Staging].[MESSAGE](
	[dt_start] [varchar](max) NULL,
	[dt_done] [varchar](max) NULL,
	[Pt_Name] [varchar](max) NULL,
	[message] [varchar](max) NULL,
	[complete] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[recip] [varchar](max) NULL,
	[hospnum] [varchar](max) NULL,
	[statu_s] [varchar](max) NULL,
	[Mes_Time] [varchar](max) NULL,
	[Urgent] [varchar](max) NULL,
	[partner] [varchar](max) NULL,
	[Arch_By] [varchar](max) NULL,
	[Arch_Date] [varchar](max) NULL,
	[Arch_Time] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
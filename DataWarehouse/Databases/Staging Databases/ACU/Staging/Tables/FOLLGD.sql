﻿CREATE TABLE [Staging].[FOLLGD](
	[SNumber] [varchar](max) NULL,
	[FollGramSnumber] [varchar](max) NULL,
	[Medication] [varchar](max) NULL,
	[Med_Date] [varchar](max) NULL,
	[Name] [varchar](max) NULL,
	[Dose] [varchar](max) NULL,
	[Frequency] [varchar](max) NULL,
	[Searchfield] [varchar](max) NULL,
	[drugSnum] [varchar](max) NULL,
	[HCG] [varchar](max) NULL,
	[Treat_Date] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[VORLAGEN](
	[SerienNr] [varchar](max) NULL,
	[Beschreibung] [varchar](max) NULL,
	[Tastenkürzel1] [varchar](max) NULL,
	[Tastenkürzel2] [varchar](max) NULL,
	[Kurzname] [varchar](max) NULL,
	[Vorlage] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[CAL_SCAN](
	[Name] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[Datex] [varchar](max) NULL,
	[reason] [varchar](max) NULL,
	[notes] [varchar](max) NULL,
	[Timex] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[SortKey] [varchar](max) NULL,
	[hospnum] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
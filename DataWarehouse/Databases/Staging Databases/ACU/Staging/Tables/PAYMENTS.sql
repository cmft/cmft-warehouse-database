﻿CREATE TABLE [Staging].[PAYMENTS](
	[sernum] [varchar](max) NULL,
	[file_number] [varchar](max) NULL,
	[mainindex] [varchar](max) NULL,
	[DateReceived] [varchar](max) NULL,
	[ChequeDate] [varchar](max) NULL,
	[ChequeValue] [varchar](max) NULL,
	[FromName] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
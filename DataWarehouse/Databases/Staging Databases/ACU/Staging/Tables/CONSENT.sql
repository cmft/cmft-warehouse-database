﻿CREATE TABLE [Staging].[CONSENT](
	[Descr] [varchar](max) NULL,
	[MonthsExpire] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[Result] [varchar](max) NULL,
	[DateObtained] [varchar](max) NULL,
	[ReqIVF] [varchar](max) NULL,
	[ReqICSI] [varchar](max) NULL,
	[ReqFET] [varchar](max) NULL,
	[ReqGen] [varchar](max) NULL,
	[ReqIUI] [varchar](max) NULL,
	[ReqDonor] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
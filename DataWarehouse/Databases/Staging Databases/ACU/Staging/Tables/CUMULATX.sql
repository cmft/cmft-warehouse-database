﻿CREATE TABLE [Staging].[CUMULATX](
	[hospnum] [varchar](max) NULL,
	[Name] [varchar](max) NULL,
	[treatdate] [varchar](max) NULL,
	[outcome] [varchar](max) NULL,
	[IUP] [varchar](max) NULL,
	[diagnosis] [varchar](max) NULL,
	[AttNumber] [varchar](max) NULL,
	[snumber] [varchar](max) NULL,
	[TreatType] [varchar](max) NULL,
	[age] [varchar](max) NULL,
	[foll] [varchar](max) NULL,
	[eggs] [varchar](max) NULL,
	[transferred] [varchar](max) NULL,
	[funding] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[L5](
	[CentreCode] [varchar](max) NULL,
	[ClientRef] [varchar](max) NULL,
	[CorrectForm] [varchar](max) NULL,
	[Surname] [varchar](max) NULL,
	[FormDate] [varchar](max) NULL,
	[FormNumber] [varchar](max) NULL,
	[OPtRecState] [varchar](max) NULL,
	[PatDonNum] [varchar](max) NULL,
	[VersionNum] [varchar](max) NULL,
	[FollSortKey] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[HFEADListRef] [varchar](max) NULL,
	[StartClin] [varchar](max) NULL,
	[HFEAFeedback] [varchar](max) NULL,
	[StartDate] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
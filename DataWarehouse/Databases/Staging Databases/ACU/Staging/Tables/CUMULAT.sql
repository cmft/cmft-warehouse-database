﻿CREATE TABLE [Staging].[CUMULAT](
	[Cycle] [varchar](max) NULL,
	[IUP] [varchar](max) NULL,
	[Eggcol] [varchar](max) NULL,
	[ET] [varchar](max) NULL,
	[Datex] [varchar](max) NULL,
	[IUP_Cycle] [varchar](max) NULL,
	[IUP_EggColl] [varchar](max) NULL,
	[IUP_ET] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[Cleavage] [varchar](max) NULL,
	[Fert] [varchar](max) NULL,
	[Good_Emb] [varchar](max) NULL,
	[Filter] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[GPS1](
	[gpf] [varchar](max) NULL,
	[gps] [varchar](max) NULL,
	[name] [varchar](max) NULL,
	[std] [varchar](max) NULL,
	[tel] [varchar](max) NULL,
	[a1] [varchar](max) NULL,
	[a2] [varchar](max) NULL,
	[a3] [varchar](max) NULL,
	[a4] [varchar](max) NULL,
	[pc] [varchar](max) NULL,
	[snum] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
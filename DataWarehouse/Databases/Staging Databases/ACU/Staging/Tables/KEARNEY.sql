﻿CREATE TABLE [Staging].[KEARNEY](
	[hospnum] [varchar](max) NULL,
	[name] [varchar](max) NULL,
	[fdob] [varchar](max) NULL,
	[prev_ivf] [varchar](max) NULL,
	[agreed] [varchar](max) NULL,
	[NHS_List_Year] [varchar](max) NULL,
	[SURNAME] [varchar](max) NULL,
	[PCG] [varchar](max) NULL,
	[POSTCODE] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[INVUSED](
	[serno] [varchar](max) NULL,
	[cihistserno] [varchar](max) NULL,
	[Quantity] [varchar](max) NULL,
	[HospNum] [varchar](max) NULL,
	[SortKey] [varchar](max) NULL,
	[Treatment] [varchar](max) NULL,
	[PatName] [varchar](max) NULL,
	[AllocDate] [varchar](max) NULL,
	[ViewFlag] [varchar](max) NULL,
	[ModFlag] [varchar](max) NULL,
	[DelFlag] [varchar](max) NULL,
	[CISerno] [varchar](max) NULL,
	[SelKey] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
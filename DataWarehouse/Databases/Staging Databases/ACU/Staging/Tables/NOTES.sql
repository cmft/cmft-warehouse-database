﻿CREATE TABLE [Staging].[NOTES](
	[hospnum] [varchar](max) NULL,
	[Created] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[Author] [varchar](max) NULL,
	[Cr_Time] [varchar](max) NULL,
	[Revised] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[timeindex] [varchar](max) NULL,
	[revisetext] [varchar](max) NULL,
	[Name] [varchar](max) NULL,
	[scanned] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
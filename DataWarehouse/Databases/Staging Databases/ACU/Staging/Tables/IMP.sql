﻿CREATE TABLE [Staging].[IMP](
	[dewar] [varchar](max) NULL,
	[CannNumb] [varchar](max) NULL,
	[CaneNumb] [varchar](max) NULL,
	[FirstName] [varchar](max) NULL,
	[Surname] [varchar](max) NULL,
	[DOB] [varchar](max) NULL,
	[DateFreeze] [varchar](max) NULL,
	[ReinvMonth] [varchar](max) NULL,
	[NbStageEmbFroz] [varchar](max) NULL,
	[ExpiryDate] [varchar](max) NULL,
	[Comments] [varchar](max) NULL,
	[MFSNumber] [varchar](max) NULL,
	[PINumber] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[FrCode] [varchar](max) NULL,
	[NbEmbsRemaing] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
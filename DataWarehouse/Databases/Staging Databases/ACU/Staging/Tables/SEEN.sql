﻿CREATE TABLE [Staging].[SEEN](
	[Name] [varchar](max) NULL,
	[Added_By] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[Time_Added] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[hospnum] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[BASELINE](
	[snumber] [varchar](max) NULL,
	[scan_dt] [varchar](max) NULL,
	[e2] [varchar](max) NULL,
	[rt_ovary] [varchar](max) NULL,
	[lt_ovary] [varchar](max) NULL,
	[endo] [varchar](max) NULL,
	[follgramsnumber] [varchar](max) NULL,
	[uterus] [varchar](max) NULL,
	[notes] [varchar](max) NULL,
	[SortKey] [varchar](max) NULL,
	[hospnum] [varchar](max) NULL,
	[Last_Bleed] [varchar](max) NULL,
	[Scan_by] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
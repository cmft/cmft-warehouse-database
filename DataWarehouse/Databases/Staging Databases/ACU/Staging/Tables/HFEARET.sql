﻿CREATE TABLE [Staging].[HFEARET](
	[Hospnum] [varchar](max) NULL,
	[HFEADate] [varchar](max) NULL,
	[FormVersion] [varchar](max) NULL,
	[OurID] [varchar](max) NULL,
	[ErrorLine] [varchar](max) NULL,
	[Status1] [varchar](max) NULL,
	[OurID1] [varchar](max) NULL,
	[RecordedBy] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
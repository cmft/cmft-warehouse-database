﻿CREATE TABLE [Staging].[EOIVF](
	[hospnum] [varchar](max) NULL,
	[name] [varchar](max) NULL,
	[refbysnum] [varchar](max) NULL,
	[treatdate] [varchar](max) NULL,
	[outcome] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
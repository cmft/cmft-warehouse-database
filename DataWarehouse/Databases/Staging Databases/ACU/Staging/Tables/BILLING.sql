﻿CREATE TABLE [Staging].[BILLING](
	[sernum] [varchar](max) NULL,
	[File_Number] [varchar](max) NULL,
	[mainindex] [varchar](max) NULL,
	[Performedby] [varchar](max) NULL,
	[Activity] [varchar](max) NULL,
	[Details] [varchar](max) NULL,
	[Duration] [varchar](max) NULL,
	[HourlyRate] [varchar](max) NULL,
	[TotalCost] [varchar](max) NULL,
	[Date] [varchar](max) NULL,
	[Time] [varchar](max) NULL,
	[Bill] [varchar](max) NULL,
	[StatemntDate] [varchar](max) NULL,
	[SortDate] [varchar](max) NULL,
	[InvoiceDate] [varchar](max) NULL,
	[BIsernum] [varchar](max) NULL,
	[PaidDate] [varchar](max) NULL,
	[InvoiceNo] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
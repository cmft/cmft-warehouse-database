﻿CREATE TABLE [Staging].[PCT](
	[PCT] [varchar](max) NULL,
	[AgeMin] [varchar](max) NULL,
	[AgeMax] [varchar](max) NULL,
	[ParityPrim] [varchar](max) NULL,
	[ParitySec] [varchar](max) NULL,
	[BMIMax] [varchar](max) NULL,
	[BMIMin] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
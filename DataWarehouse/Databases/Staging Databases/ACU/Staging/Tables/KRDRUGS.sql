﻿CREATE TABLE [Staging].[KRDRUGS](
	[hospnum] [varchar](max) NULL,
	[name] [varchar](max) NULL,
	[treatmentdate] [varchar](max) NULL,
	[totcost] [varchar](max) NULL,
	[cheked] [varchar](max) NULL,
	[PPA_CODE] [varchar](max) NULL,
	[PCG] [varchar](max) NULL,
	[POSTCODE] [varchar](max) NULL,
	[SURNAME] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
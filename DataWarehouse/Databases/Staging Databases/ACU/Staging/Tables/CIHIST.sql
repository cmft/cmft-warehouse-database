﻿CREATE TABLE [Staging].[CIHIST](
	[Serno] [varchar](max) NULL,
	[CISerno] [varchar](max) NULL,
	[BatchNo] [varchar](max) NULL,
	[DateFrom] [varchar](max) NULL,
	[DateTo] [varchar](max) NULL,
	[Supplier] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[SortKey] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[CALLET](
	[Calendar] [varchar](max) NULL,
	[Category] [varchar](max) NULL,
	[Template] [varchar](max) NULL,
	[CreatedBy] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
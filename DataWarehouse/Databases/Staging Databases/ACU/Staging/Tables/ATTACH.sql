﻿CREATE TABLE [Staging].[ATTACH](
	[AttKey] [varchar](max) NULL,
	[attposn] [varchar](max) NULL,
	[FileName] [varchar](max) NULL,
	[Category] [varchar](max) NULL,
	[Path] [varchar](max) NULL,
	[PathFile] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[AttDate] [varchar](max) NULL,
	[passwrd] [varchar](max) NULL,
	[incoming] [varchar](max) NULL,
	[ViewFlag] [varchar](max) NULL,
	[ModFlag] [varchar](max) NULL,
	[DelFlag] [varchar](max) NULL,
	[CatDate] [varchar](max) NULL,
	[DatTitle] [varchar](max) NULL,
	[Priority] [varchar](max) NULL,
	[PriorityDate] [varchar](max) NULL,
	[Image] [varchar](max) NULL,
	[ImageGallery] [varchar](max) NULL,
	[fittext] [varchar](max) NULL,
	[DispPath] [varchar](max) NULL,
	[Sound] [varchar](max) NULL,
	[SoundGallery] [varchar](max) NULL,
	[DictatedBy] [varchar](max) NULL,
	[Transcribe] [varchar](max) NULL,
	[TranscribedBy] [varchar](max) NULL,
	[TranscrWhen] [varchar](max) NULL,
	[TransComments] [varchar](max) NULL,
	[SoundTransc] [varchar](max) NULL,
	[TranscribeOpen] [varchar](max) NULL,
	[ref] [varchar](max) NULL,
	[ref2] [varchar](max) NULL,
	[InvoiceNo] [varchar](max) NULL,
	[InvoiceAmt] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
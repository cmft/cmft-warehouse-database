﻿CREATE TABLE [Staging].[CATEGOR](
	[refno] [varchar](max) NULL,
	[Categor] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[DateAdded] [varchar](max) NULL,
	[AddedBy] [varchar](max) NULL,
	[timestamp] [varchar](max) NULL,
	[DateStamp] [varchar](max) NULL,
	[ChangeStamp] [varchar](max) NULL,
	[FieldUpdate] [varchar](max) NULL,
	[InFile] [varchar](max) NULL,
	[WithWhat] [varchar](max) NULL,
	[FilLocField] [varchar](max) NULL,
	[catsearch] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[MEGAEX](
	[Name] [varchar](max) NULL,
	[Pt_Number] [varchar](max) NULL,
	[Egg_Collect] [varchar](max) NULL,
	[drug1] [varchar](max) NULL,
	[Treat_Date] [varchar](max) NULL,
	[Oocytes] [varchar](max) NULL,
	[embryologist] [varchar](max) NULL,
	[Embryos] [varchar](max) NULL,
	[Transferred] [varchar](max) NULL,
	[Outcome] [varchar](max) NULL,
	[Emb_Frozen] [varchar](max) NULL,
	[Audited] [varchar](max) NULL,
	[Age] [varchar](max) NULL,
	[Attempt] [varchar](max) NULL,
	[Treatment] [varchar](max) NULL,
	[Protocol] [varchar](max) NULL,
	[Tot_IU] [varchar](max) NULL,
	[Days_Stim] [varchar](max) NULL,
	[Source] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
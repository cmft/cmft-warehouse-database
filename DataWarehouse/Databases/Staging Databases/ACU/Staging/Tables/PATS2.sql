﻿CREATE TABLE [Staging].[PATS2](
	[mptnumber] [varchar](max) NULL,
	[maleinf] [varchar](max) NULL,
	[coupleinf] [varchar](max) NULL,
	[dibirths] [varchar](max) NULL,
	[lastunit] [varchar](max) NULL,
	[regreason] [varchar](max) NULL,
	[GonadStartDate] [varchar](max) NULL,
	[ArchSeq] [varchar](max) NULL,
	[NameSrch] [varchar](max) NULL,
	[ffax] [varchar](max) NULL,
	[femail] [varchar](max) NULL,
	[GPSNumber] [varchar](max) NULL,
	[Prev_ivfhere] [varchar](max) NULL,
	[Prev_ivftot] [varchar](max) NULL,
	[DateHFEA1] [varchar](max) NULL,
	[consult] [varchar](max) NULL,
	[counsel_fee] [varchar](max) NULL,
	[ffore_other] [varchar](max) NULL,
	[mfore_other] [varchar](max) NULL,
	[clinician] [varchar](max) NULL,
	[agreed] [varchar](max) NULL,
	[insurance] [varchar](max) NULL,
	[gpname2] [varchar](max) NULL,
	[oi_tot] [varchar](max) NULL,
	[dt_letter] [varchar](max) NULL,
	[dt_appt] [varchar](max) NULL,
	[rem_ove] [varchar](max) NULL,
	[W_notes] [varchar](max) NULL,
	[refbysnum] [varchar](max) NULL,
	[disc_obt] [varchar](max) NULL,
	[welfare_obt] [varchar](max) NULL,
	[hospnum1] [varchar](max) NULL,
	[fethnic] [varchar](max) NULL,
	[methnic] [varchar](max) NULL,
	[feye_colour] [varchar](max) NULL,
	[meye_colour] [varchar](max) NULL,
	[f_p1_eye_colour] [varchar](max) NULL,
	[f_p2_eye_colour] [varchar](max) NULL,
	[m_p1_eye_colour] [varchar](max) NULL,
	[m_p2_eye_colour] [varchar](max) NULL,
	[f_hair_colour] [varchar](max) NULL,
	[m_hair_colour] [varchar](max) NULL,
	[f_skin_colour] [varchar](max) NULL,
	[m_skin_colour] [varchar](max) NULL,
	[f_build] [varchar](max) NULL,
	[m_build] [varchar](max) NULL,
	[f_religion] [varchar](max) NULL,
	[m_religion] [varchar](max) NULL,
	[f_occupation] [varchar](max) NULL,
	[m_occupation] [varchar](max) NULL,
	[f_interests] [varchar](max) NULL,
	[m_interests] [varchar](max) NULL,
	[m_prev_don] [varchar](max) NULL,
	[f_prev_don] [varchar](max) NULL,
	[f_blood_group] [varchar](max) NULL,
	[m_blood_group] [varchar](max) NULL,
	[f_cmv] [varchar](max) NULL,
	[m_cmv] [varchar](max) NULL,
	[hfea_donor_note] [varchar](max) NULL,
	[ht_m] [varchar](max) NULL,
	[weight_m] [varchar](max) NULL,
	[HFEA_D] [varchar](max) NULL,
	[mobile] [varchar](max) NULL,
	[occ_fem] [varchar](max) NULL,
	[occ_male] [varchar](max) NULL,
	[d_sperm] [varchar](max) NULL,
	[donor_reg] [varchar](max) NULL,
	[sex] [varchar](max) NULL,
	[HFEA_D1] [varchar](max) NULL,
	[donor_reg1] [varchar](max) NULL,
	[donor_center] [varchar](max) NULL,
	[donor_center1] [varchar](max) NULL,
	[title] [varchar](max) NULL,
	[LTS] [varchar](max) NULL,
	[STS] [varchar](max) NULL,
	[box_no] [varchar](max) NULL,
	[hospnum] [varchar](max) NULL,
	[fsurname] [varchar](max) NULL,
	[fdob] [varchar](max) NULL,
	[faddress1] [varchar](max) NULL,
	[faddress2] [varchar](max) NULL,
	[faddress3] [varchar](max) NULL,
	[faddress4] [varchar](max) NULL,
	[fpostcode] [varchar](max) NULL,
	[msurname] [varchar](max) NULL,
	[mforename] [varchar](max) NULL,
	[mdob] [varchar](max) NULL,
	[gpname] [varchar](max) NULL,
	[ftelephoneh] [varchar](max) NULL,
	[ftelephonew] [varchar](max) NULL,
	[mtelephoneh] [varchar](max) NULL,
	[mtelephonew] [varchar](max) NULL,
	[finitials] [varchar](max) NULL,
	[notes] [varchar](max) NULL,
	[gpaddress1] [varchar](max) NULL,
	[gpaddress2] [varchar](max) NULL,
	[gpaddress3] [varchar](max) NULL,
	[gpaddress4] [varchar](max) NULL,
	[gppostcode] [varchar](max) NULL,
	[serial] [varchar](max) NULL,
	[fforename] [varchar](max) NULL,
	[refhealth] [varchar](max) NULL,
	[name] [varchar](max) NULL,
	[dha] [varchar](max) NULL,
	[name2] [varchar](max) NULL,
	[ivfpreg_tot] [varchar](max) NULL,
	[ivfoff] [varchar](max) NULL,
	[diinfo] [varchar](max) NULL,
	[previui] [varchar](max) NULL,
	[wk24] [varchar](max) NULL,
	[misc] [varchar](max) NULL,
	[ectopic] [varchar](max) NULL,
	[ht] [varchar](max) NULL,
	[weight] [varchar](max) NULL,
	[rubella] [varchar](max) NULL,
	[duration] [varchar](max) NULL,
	[clomid] [varchar](max) NULL,
	[fsh] [varchar](max) NULL,
	[lh] [varchar](max) NULL,
	[prolactin] [varchar](max) NULL,
	[progest] [varchar](max) NULL,
	[testost] [varchar](max) NULL,
	[E2] [varchar](max) NULL,
	[lap] [varchar](max) NULL,
	[hsg] [varchar](max) NULL,
	[semen] [varchar](max) NULL,
	[donor] [varchar](max) NULL,
	[lapres] [varchar](max) NULL,
	[endomild] [varchar](max) NULL,
	[endomod] [varchar](max) NULL,
	[endosev] [varchar](max) NULL,
	[adhesmild] [varchar](max) NULL,
	[adhesmod] [varchar](max) NULL,
	[adhessev] [varchar](max) NULL,
	[pcod] [varchar](max) NULL,
	[rtpatent] [varchar](max) NULL,
	[ltpatent] [varchar](max) NULL,
	[fibroids] [varchar](max) NULL,
	[hsgpatlt] [varchar](max) NULL,
	[hsgpatrt] [varchar](max) NULL,
	[hysteros] [varchar](max) NULL,
	[hystpolyps] [varchar](max) NULL,
	[smoker] [varchar](max) NULL,
	[sfa] [varchar](max) NULL,
	[bmi] [varchar](max) NULL,
	[endomet] [varchar](max) NULL,
	[adhpid] [varchar](max) NULL,
	[patency] [varchar](max) NULL,
	[patlap] [varchar](max) NULL,
	[hsgres] [varchar](max) NULL,
	[hystres] [varchar](max) NULL,
	[spermres] [varchar](max) NULL,
	[diag] [varchar](max) NULL,
	[chlamydia] [varchar](max) NULL,
	[hvs] [varchar](max) NULL,
	[referredby] [varchar](max) NULL,
	[source] [varchar](max) NULL,
	[Fbirthsname] [varchar](max) NULL,
	[fbirthfname] [varchar](max) NULL,
	[ftown] [varchar](max) NULL,
	[fregdist] [varchar](max) NULL,
	[fcountry] [varchar](max) NULL,
	[mbirthsname] [varchar](max) NULL,
	[mbirthfname] [varchar](max) NULL,
	[mtown] [varchar](max) NULL,
	[mregdist] [varchar](max) NULL,
	[mcountry] [varchar](max) NULL,
	[otherivfpreg] [varchar](max) NULL,
	[livebirth] [varchar](max) NULL,
	[dur_year] [varchar](max) NULL,
	[dur_lstpreg] [varchar](max) NULL,
	[dipreg] [varchar](max) NULL,
	[diprev] [varchar](max) NULL,
	[ditotal] [varchar](max) NULL,
	[Patient] [varchar](max) NULL,
	[note_req] [varchar](max) NULL,
	[note_recd] [varchar](max) NULL,
	[note_reas] [varchar](max) NULL,
	[note_ret] [varchar](max) NULL,
	[OTS] [varchar](max) NULL,
	[refdate] [varchar](max) NULL,
	[maddress1] [varchar](max) NULL,
	[maddress2] [varchar](max) NULL,
	[maddress3] [varchar](max) NULL,
	[maddress4] [varchar](max) NULL,
	[mpostcode] [varchar](max) NULL,
	[mtelephonem] [varchar](max) NULL,
	[memail] [varchar](max) NULL,
	[aipreg] [varchar](max) NULL,
	[aicycle_here] [varchar](max) NULL,
	[aicycle_tot] [varchar](max) NULL,
	[ai_LB] [varchar](max) NULL,
	[ent_date] [varchar](max) NULL,
	[froz2pn] [varchar](max) NULL,
	[froz_cl] [varchar](max) NULL,
	[froz_bl] [varchar](max) NULL,
	[lst_trans] [varchar](max) NULL,
	[prev_hosp] [varchar](max) NULL,
	[fsurl] [varchar](max) NULL,
	[lap1] [varchar](max) NULL,
	[hysteros1] [varchar](max) NULL,
	[hsg1] [varchar](max) NULL,
	[DateAdded] [varchar](max) NULL,
	[AddedBy] [varchar](max) NULL,
	[timestamp] [varchar](max) NULL,
	[DateStamp] [varchar](max) NULL,
	[ChangeStamp] [varchar](max) NULL,
	[MailingExclude] [varchar](max) NULL,
	[RegDate] [varchar](max) NULL,
	[regdate2] [varchar](max) NULL,
	[fsh_high] [varchar](max) NULL,
	[fsh_mean] [varchar](max) NULL,
	[fsh_recent] [varchar](max) NULL,
	[fsh_number] [varchar](max) NULL,
	[fsh_total] [varchar](max) NULL,
	[fsh_date_rec] [varchar](max) NULL,
	[fsh_low] [varchar](max) NULL,
	[fsh_date_high] [varchar](max) NULL,
	[fsh_date_low] [varchar](max) NULL,
	[nts_mde] [varchar](max) NULL,
	[wocdt] [varchar](max) NULL,
	[export1] [varchar](max) NULL,
	[hsggot] [varchar](max) NULL,
	[pathgot] [varchar](max) NULL,
	[wocgot] [varchar](max) NULL,
	[consgot] [varchar](max) NULL,
	[hsgattach] [varchar](max) NULL,
	[consattach] [varchar](max) NULL,
	[wocattach] [varchar](max) NULL,
	[hfeareg] [varchar](max) NULL,
	[trialet_dt] [varchar](max) NULL,
	[trialet] [varchar](max) NULL,
	[mens_cycle] [varchar](max) NULL,
	[mens1] [varchar](max) NULL,
	[mens2] [varchar](max) NULL,
	[Dysmen] [varchar](max) NULL,
	[IMB] [varchar](max) NULL,
	[PCB] [varchar](max) NULL,
	[PVdischarge] [varchar](max) NULL,
	[IMBnotes] [varchar](max) NULL,
	[PCBnotes] [varchar](max) NULL,
	[Dysmennotes] [varchar](max) NULL,
	[Menarche] [varchar](max) NULL,
	[last_smear] [varchar](max) NULL,
	[cx_treat] [varchar](max) NULL,
	[cycle_notes] [varchar](max) NULL,
	[IUCD] [varchar](max) NULL,
	[COC] [varchar](max) NULL,
	[POP] [varchar](max) NULL,
	[Depot] [varchar](max) NULL,
	[cont_other] [varchar](max) NULL,
	[miscF] [varchar](max) NULL,
	[miscM] [varchar](max) NULL,
	[LiveF] [varchar](max) NULL,
	[LiveM] [varchar](max) NULL,
	[Still_BirthF] [varchar](max) NULL,
	[Still_BirthM] [varchar](max) NULL,
	[ectopicF] [varchar](max) NULL,
	[ectopicM] [varchar](max) NULL,
	[Rel_duration] [varchar](max) NULL,
	[prev_inf] [varchar](max) NULL,
	[aware_fert] [varchar](max) NULL,
	[freq_sex] [varchar](max) NULL,
	[inf_notes] [varchar](max) NULL,
	[cat_tubal] [varchar](max) NULL,
	[cat_endo] [varchar](max) NULL,
	[cat_cx] [varchar](max) NULL,
	[cat_MF] [varchar](max) NULL,
	[cat_ASA] [varchar](max) NULL,
	[cat_anov] [varchar](max) NULL,
	[cat_unex] [varchar](max) NULL,
	[clomid_dose] [varchar](max) NULL,
	[clomid_cyc] [varchar](max) NULL,
	[clomid_notes] [varchar](max) NULL,
	[cyclo_dose] [varchar](max) NULL,
	[cyclo_cyc] [varchar](max) NULL,
	[cyclo_notes] [varchar](max) NULL,
	[prev_iui] [varchar](max) NULL,
	[prev_iui_notes] [varchar](max) NULL,
	[prev_ivf] [varchar](max) NULL,
	[prev_ivf_notes] [varchar](max) NULL,
	[prev_notes] [varchar](max) NULL,
	[PMH_PID] [varchar](max) NULL,
	[PMH_chlamydia] [varchar](max) NULL,
	[PMH_surg_abdo] [varchar](max) NULL,
	[PMH_surg_pelvic] [varchar](max) NULL,
	[PMH_other] [varchar](max) NULL,
	[PMH_notes] [varchar](max) NULL,
	[FH_ov_ca] [varchar](max) NULL,
	[FH_cf] [varchar](max) NULL,
	[FH_breast_ca] [varchar](max) NULL,
	[hsg_req] [varchar](max) NULL,
	[lap_req] [varchar](max) NULL,
	[orchidoplexy] [varchar](max) NULL,
	[male_std] [varchar](max) NULL,
	[hernia] [varchar](max) NULL,
	[male_std_age] [varchar](max) NULL,
	[hernia_age] [varchar](max) NULL,
	[mumps] [varchar](max) NULL,
	[varicocele] [varchar](max) NULL,
	[male_medhist] [varchar](max) NULL,
	[male_allergies] [varchar](max) NULL,
	[male_medication] [varchar](max) NULL,
	[fem_smoke] [varchar](max) NULL,
	[male_smoke] [varchar](max) NULL,
	[fem_smoke_num] [varchar](max) NULL,
	[male_smok_num] [varchar](max) NULL,
	[fem_drink_unit] [varchar](max) NULL,
	[male_drink_unit] [varchar](max) NULL,
	[fem_drink] [varchar](max) NULL,
	[male_drink] [varchar](max) NULL,
	[notes_lmp] [varchar](max) NULL,
	[mens3] [varchar](max) NULL,
	[pvdis_notes] [varchar](max) NULL,
	[top_female] [varchar](max) NULL,
	[top_male] [varchar](max) NULL,
	[inf_since] [varchar](max) NULL,
	[cyc_notes1] [varchar](max) NULL,
	[newrefby] [varchar](max) NULL,
	[newrefdt] [varchar](max) NULL,
	[newrefmod] [varchar](max) NULL,
	[tamoxy_dose] [varchar](max) NULL,
	[tamoxy_cyc] [varchar](max) NULL,
	[tamoxy_notes] [varchar](max) NULL,
	[PMH_nil] [varchar](max) NULL,
	[FH_other] [varchar](max) NULL,
	[FH_notes] [varchar](max) NULL,
	[pmhmorefert] [varchar](max) NULL,
	[pmhmorefh] [varchar](max) NULL,
	[pmhmoremed] [varchar](max) NULL,
	[prev_fert] [varchar](max) NULL,
	[F_allergies] [varchar](max) NULL,
	[F_medication] [varchar](max) NULL,
	[medhistby] [varchar](max) NULL,
	[medhistmod] [varchar](max) NULL,
	[medhistdt] [varchar](max) NULL,
	[orchid] [varchar](max) NULL,
	[mumps_notes] [varchar](max) NULL,
	[sorthsg] [varchar](max) NULL,
	[sortlap] [varchar](max) NULL,
	[sortscan] [varchar](max) NULL,
	[sortsfa] [varchar](max) NULL,
	[sortref] [varchar](max) NULL,
	[sortnotes] [varchar](max) NULL,
	[minits] [varchar](max) NULL,
	[lcasemsurl] [varchar](max) NULL,
	[Cmisc] [varchar](max) NULL,
	[Ctop] [varchar](max) NULL,
	[CLiveBirth] [varchar](max) NULL,
	[cStillBirth] [varchar](max) NULL,
	[Cectopic] [varchar](max) NULL,
	[prelistlet] [varchar](max) NULL,
	[archive] [varchar](max) NULL,
	[scangot] [varchar](max) NULL,
	[spermgot] [varchar](max) NULL,
	[spermreq] [varchar](max) NULL,
	[NHS_List_Year] [varchar](max) NULL,
	[NHSSortkey] [varchar](max) NULL,
	[Scan_Notes] [varchar](max) NULL,
	[Notes_Path] [varchar](max) NULL,
	[EPR] [varchar](max) NULL,
	[NHS_Number] [varchar](max) NULL,
	[discussedF] [varchar](max) NULL,
	[discussedO] [varchar](max) NULL,
	[discussedS] [varchar](max) NULL,
	[discussedM] [varchar](max) NULL,
	[discussedFU] [varchar](max) NULL,
	[discussedSU] [varchar](max) NULL,
	[discussedC] [varchar](max) NULL,
	[discF] [varchar](max) NULL,
	[discO] [varchar](max) NULL,
	[discS] [varchar](max) NULL,
	[discM] [varchar](max) NULL,
	[discFU] [varchar](max) NULL,
	[discSU] [varchar](max) NULL,
	[discNotes] [varchar](max) NULL,
	[discSUcycle] [varchar](max) NULL,
	[discSUet] [varchar](max) NULL,
	[discSUlb] [varchar](max) NULL,
	[next_ivf_name] [varchar](max) NULL,
	[next_ivf_prot] [varchar](max) NULL,
	[next_ivf_notes] [varchar](max) NULL,
	[next_ivf_dt] [varchar](max) NULL,
	[next_ivf_iu] [varchar](max) NULL,
	[liinkref] [varchar](max) NULL,
	[LTF_dt1] [varchar](max) NULL,
	[LTF_st] [varchar](max) NULL,
	[LTF_disc_yr] [varchar](max) NULL,
	[LTF_disc] [varchar](max) NULL,
	[LTF_cons] [varchar](max) NULL,
	[LTF_hosp] [varchar](max) NULL,
	[LTF_reason] [varchar](max) NULL,
	[LTF_comm] [varchar](max) NULL,
	[LTF_account] [varchar](max) NULL,
	[LTF_misc] [varchar](max) NULL,
	[LTF_bu] [varchar](max) NULL,
	[cryo_cde] [varchar](max) NULL,
	[LTF] [varchar](max) NULL,
	[LTF_disc1] [varchar](max) NULL,
	[nraction] [varchar](max) NULL,
	[callby] [varchar](max) NULL,
	[info_sent] [varchar](max) NULL,
	[conv] [varchar](max) NULL,
	[tour] [varchar](max) NULL,
	[app_date] [varchar](max) NULL,
	[dr] [varchar](max) NULL,
	[time] [varchar](max) NULL,
	[ivf] [varchar](max) NULL,
	[icsi] [varchar](max) NULL,
	[iui] [varchar](max) NULL,
	[datte] [varchar](max) NULL,
	[aih] [varchar](max) NULL,
	[aid] [varchar](max) NULL,
	[ovum_r] [varchar](max) NULL,
	[ovum_d] [varchar](max) NULL,
	[egg_share] [varchar](max) NULL,
	[pgd] [varchar](max) NULL,
	[mi] [varchar](max) NULL,
	[other_inf] [varchar](max) NULL,
	[prev] [varchar](max) NULL,
	[general] [varchar](max) NULL,
	[wherex] [varchar](max) NULL,
	[hfea] [varchar](max) NULL,
	[gp] [varchar](max) NULL,
	[tv] [varchar](max) NULL,
	[friend] [varchar](max) NULL,
	[yellow] [varchar](max) NULL,
	[press] [varchar](max) NULL,
	[internet] [varchar](max) NULL,
	[cons] [varchar](max) NULL,
	[other_hear] [varchar](max) NULL,
	[cons_snum] [varchar](max) NULL,
	[enqnotes] [varchar](max) NULL,
	[samesex] [varchar](max) NULL,
	[singlewoman] [varchar](max) NULL,
	[di_malepartner] [varchar](max) NULL,
	[di_husband] [varchar](max) NULL,
	[referral] [varchar](max) NULL,
	[directappt] [varchar](max) NULL,
	[gender] [varchar](max) NULL,
	[website] [varchar](max) NULL,
	[pinkparent] [varchar](max) NULL,
	[suggestion] [varchar](max) NULL,
	[pesa] [varchar](max) NULL,
	[prev_invest] [varchar](max) NULL,
	[prev_inves_wher] [varchar](max) NULL,
	[sa] [varchar](max) NULL,
	[vasectomy] [varchar](max) NULL,
	[appwith] [varchar](max) NULL,
	[enqname] [varchar](max) NULL,
	[added] [varchar](max) NULL,
	[tel] [varchar](max) NULL,
	[email] [varchar](max) NULL,
	[eng] [varchar](max) NULL,
	[m_bmi] [varchar](max) NULL,
	[iui_K_D] [varchar](max) NULL,
	[openday] [varchar](max) NULL,
	[openday1] [varchar](max) NULL,
	[prev_hosp_01] [varchar](max) NULL,
	[Partner_GP] [varchar](max) NULL,
	[PrevPart] [varchar](max) NULL,
	[Pathist_conv] [varchar](max) NULL,
	[Mob_OKtoCont] [varchar](max) NULL,
	[lenhosp] [varchar](max) NULL,
	[faddress5] [varchar](max) NULL,
	[demograph_chk] [varchar](max) NULL,
	[salutation] [varchar](max) NULL,
	[mtitle] [varchar](max) NULL,
	[NameSrchP] [varchar](max) NULL,
	[Ac_Notes] [varchar](max) NULL,
	[Ac_Amount] [varchar](max) NULL,
	[Ac_Date] [varchar](max) NULL,
	[Ac_Sent] [varchar](max) NULL,
	[Ac_Outstanding] [varchar](max) NULL,
	[Ac_Total] [varchar](max) NULL,
	[Ac_Credit] [varchar](max) NULL,
	[InfoNote] [varchar](max) NULL,
	[Accept_White] [varchar](max) NULL,
	[Accept_Carib] [varchar](max) NULL,
	[Accept_African] [varchar](max) NULL,
	[Accept_Black] [varchar](max) NULL,
	[Accept_Indian] [varchar](max) NULL,
	[Accept_Bang] [varchar](max) NULL,
	[Accept_Pak] [varchar](max) NULL,
	[Accept_Chin] [varchar](max) NULL,
	[Accept_E_Blue] [varchar](max) NULL,
	[Accept_E_Brown] [varchar](max) NULL,
	[Accept_E_Green] [varchar](max) NULL,
	[Accept_E_Grey] [varchar](max) NULL,
	[Accept_E_Hazel] [varchar](max) NULL,
	[Accept_S_Light] [varchar](max) NULL,
	[Accept_S_Medium] [varchar](max) NULL,
	[Accept_S_Dark] [varchar](max) NULL,
	[Accept_H_Black] [varchar](max) NULL,
	[Accept_H_BrownD] [varchar](max) NULL,
	[Accept_H_BrownL] [varchar](max) NULL,
	[Accept_H_Red] [varchar](max) NULL,
	[Accept_H_Blonde] [varchar](max) NULL,
	[Accept_By] [varchar](max) NULL,
	[Image] [varchar](max) NULL,
	[ImageGallery] [varchar](max) NULL,
	[PatientPhoto] [varchar](max) NULL,
	[HFEAVersion] [varchar](max) NULL,
	[qwert] [varchar](max) NULL,
	[PassportNum] [varchar](max) NULL,
	[Country_Issue] [varchar](max) NULL,
	[cat_noMale] [varchar](max) NULL,
	[cat_OvFail] [varchar](max) NULL,
	[cat_SameSex] [varchar](max) NULL,
	[cat_UtProb] [varchar](max) NULL,
	[cat_OtherEndo] [varchar](max) NULL,
	[cat_Meno] [varchar](max) NULL,
	[cat_Genetic] [varchar](max) NULL,
	[cat_other] [varchar](max) NULL,
	[IVFBirth] [varchar](max) NULL,
	[phfeareg] [varchar](max) NULL,
	[M_NHS_Number] [varchar](max) NULL,
	[M_Passport] [varchar](max) NULL,
	[M_CountryPass] [varchar](max) NULL,
	[AcctChargeTot] [varchar](max) NULL,
	[AcctBilledTot] [varchar](max) NULL,
	[AcctPaidTot] [varchar](max) NULL,
	[AcctBalance] [varchar](max) NULL,
	[LastStatmntAmt] [varchar](max) NULL,
	[LastBillSerNo] [varchar](max) NULL,
	[LastStatmntDate] [varchar](max) NULL,
	[Billable] [varchar](max) NULL,
	[Pats_Status] [varchar](max) NULL,
	[AcctNonZero] [varchar](max) NULL,
	[MenstrualLoss] [varchar](max) NULL,
	[MajorIlness] [varchar](max) NULL,
	[NuffieldPatient] [varchar](max) NULL,
	[NuffieldPartner] [varchar](max) NULL,
	[OR_Agreed] [varchar](max) NULL,
	[msurl] [varchar](max) NULL,
	[cdrom] [varchar](max) NULL,
	[Rec_Priority] [varchar](max) NULL,
	[OR_Remove] [varchar](max) NULL,
	[Rec_Hosp] [varchar](max) NULL,
	[Rec_List] [varchar](max) NULL,
	[Don_Hosp] [varchar](max) NULL,
	[f_educ] [varchar](max) NULL,
	[DonorIndex] [varchar](max) NULL,
	[DonRecText] [varchar](max) NULL,
	[HTeleP] [varchar](max) NULL,
	[WTelP] [varchar](max) NULL,
	[MTelP] [varchar](max) NULL,
	[PartTelP] [varchar](max) NULL,
	[L1Snum] [varchar](max) NULL,
	[L3SNum] [varchar](max) NULL,
	[HFEA_D_Ref] [varchar](max) NULL,
	[L2Snum] [varchar](max) NULL,
	[PartnerPhoto] [varchar](max) NULL,
	[PCTGroup] [varchar](max) NULL,
	[PCTList] [varchar](max) NULL,
	[Dt_Rem_WL] [varchar](max) NULL,
	[W_ListStatus] [varchar](max) NULL,
	[OnProgramme] [varchar](max) NULL,
	[OffProgramme] [varchar](max) NULL,
	[OffList] [varchar](max) NULL,
	[MolarC] [varchar](max) NULL,
	[MolarF] [varchar](max) NULL,
	[NeoDthC] [varchar](max) NULL,
	[NeoDthF] [varchar](max) NULL,
	[NeoDthM] [varchar](max) NULL,
	[MolarM] [varchar](max) NULL,
	[Consang] [varchar](max) NULL,
	[Temp1] [varchar](max) NULL,
	[Source_Old] [varchar](max) NULL,
	[HFEAPtRegNo] [varchar](max) NULL,
	[HFEAPtRegDt] [varchar](max) NULL,
	[NextApptBy] [varchar](max) NULL,
	[WishesAppt] [varchar](max) NULL,
	[Disc_Info] [varchar](max) NULL,
	[PassportNumP] [varchar](max) NULL,
	[Country_IssueP] [varchar](max) NULL,
	[NHS_NumberP] [varchar](max) NULL,
	[overseas] [varchar](max) NULL,
	[PrevSponPreg] [varchar](max) NULL,
	[PartGender] [varchar](max) NULL,
	[PartnerDisabled] [varchar](max) NULL,
	[PartnerFInf] [varchar](max) NULL,
	[PartnerAzoo] [varchar](max) NULL,
	[PartnerOligo] [varchar](max) NULL,
	[PartnerGD] [varchar](max) NULL,
	[PartLastUK] [varchar](max) NULL,
	[PtDonNum] [varchar](max) NULL,
	[PartDonNum] [varchar](max) NULL,
	[Pager] [varchar](max) NULL,
	[SMSFMob] [varchar](max) NULL,
	[SMSMMob] [varchar](max) NULL,
	[ET_Recommend] [varchar](max) NULL,
	[AMH] [varchar](max) NULL,
	[Team] [varchar](max) NULL,
	[AcctBalance1] [varchar](max) NULL,
	[AccTChgDate] [varchar](max) NULL,
	[CCNumber] [varchar](max) NULL,
	[SecCode] [varchar](max) NULL,
	[IssueNum] [varchar](max) NULL,
	[CardName] [varchar](max) NULL,
	[ValidTo] [varchar](max) NULL,
	[ValidFrom] [varchar](max) NULL,
	[HasPrevPart] [varchar](max) NULL,
	[mdobtext] [varchar](max) NULL,
	[fdobtext] [varchar](max) NULL,
	[PatResearch] [varchar](max) NULL,
	[PartResearch] [varchar](max) NULL,
	[PatContRes] [varchar](max) NULL,
	[PartContRes] [varchar](max) NULL,
	[PatNCRes] [varchar](max) NULL,
	[PartNCRes] [varchar](max) NULL,
	[Salut1] [varchar](max) NULL,
	[Salut2] [varchar](max) NULL,
	[Salut3] [varchar](max) NULL,
	[Salut4] [varchar](max) NULL,
	[Salut5] [varchar](max) NULL,
	[Salut6] [varchar](max) NULL,
	[CVPatient] [varchar](max) NULL,
	[CVPartner] [varchar](max) NULL,
	[PatMessage] [varchar](max) NULL,
	[IgnoreKPI] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
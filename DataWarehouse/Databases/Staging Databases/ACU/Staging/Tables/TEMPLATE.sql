﻿CREATE TABLE [Staging].[TEMPLATE](
	[Description] [varchar](max) NULL,
	[Department] [varchar](max) NULL,
	[Cr_Date] [varchar](max) NULL,
	[Ed_Date] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[Notes] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
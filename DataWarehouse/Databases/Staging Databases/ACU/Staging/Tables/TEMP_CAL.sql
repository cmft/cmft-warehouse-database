﻿CREATE TABLE [Staging].[TEMP_CAL](
	[Name] [varchar](max) NULL,
	[App] [varchar](max) NULL,
	[DtFrom] [varchar](max) NULL,
	[DtTo] [varchar](max) NULL,
	[Category] [varchar](max) NULL,
	[TempName] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
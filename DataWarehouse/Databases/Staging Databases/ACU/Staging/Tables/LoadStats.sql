﻿CREATE TABLE [Staging].[LoadStats](
	[schemaName] [nvarchar](256) NOT NULL,
	[tablename] [nvarchar](256) NOT NULL,
	[inImport] [bit] NOT NULL,
	[rowsImported] [int] NULL,
	[lastImported] [datetime] NULL,
	[lastTransform] [datetime] NULL,
	[transformSQL] [nvarchar](max) NULL,
	[lastImportedFileModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
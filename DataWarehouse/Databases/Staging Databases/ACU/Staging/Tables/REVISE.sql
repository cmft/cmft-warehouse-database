﻿CREATE TABLE [Staging].[REVISE](
	[hospnum] [varchar](max) NULL,
	[Created] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[Author] [varchar](max) NULL,
	[Cr_Time] [varchar](max) NULL,
	[Revised] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[timeindex] [varchar](max) NULL,
	[NotesSNumber] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
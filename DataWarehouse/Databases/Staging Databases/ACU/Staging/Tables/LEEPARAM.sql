﻿CREATE TABLE [Staging].[LEEPARAM](
	[age1] [varchar](max) NULL,
	[age2] [varchar](max) NULL,
	[date1] [varchar](max) NULL,
	[date2] [varchar](max) NULL,
	[non_aud] [varchar](max) NULL,
	[viable] [varchar](max) NULL,
	[excel] [varchar](max) NULL,
	[excelname] [varchar](max) NULL,
	[excelnum] [varchar](max) NULL,
	[treattype] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
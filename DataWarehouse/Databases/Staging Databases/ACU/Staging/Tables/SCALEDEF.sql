﻿CREATE TABLE [Staging].[SCALEDEF](
	[WSize] [varchar](max) NULL,
	[HSize] [varchar](max) NULL,
	[ScaleCode] [varchar](max) NULL,
	[LFont] [varchar](max) NULL,
	[WHLKey] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[NHSACT](
	[PCT] [varchar](max) NULL,
	[Action] [varchar](max) NULL,
	[SNumber] [varchar](max) NULL,
	[PCTAction] [varchar](max) NULL,
	[ManualShow] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
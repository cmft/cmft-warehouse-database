﻿CREATE TABLE [Staging].[SUBPERS](
	[Pers_Num] [varchar](max) NULL,
	[Drug] [varchar](max) NULL,
	[frequency] [varchar](max) NULL,
	[duration] [varchar](max) NULL,
	[TotalC] [varchar](max) NULL,
	[TotalQ] [varchar](max) NULL,
	[UKey] [varchar](max) NULL,
	[DrugSNum] [varchar](max) NULL,
	[totamps] [varchar](max) NULL,
	[dose] [varchar](max) NULL,
	[unit] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Staging].[ACUMES](
	[SNumber] [varchar](max) NULL,
	[Message_Text] [varchar](max) NULL,
	[Message_Start] [varchar](max) NULL,
	[ReadDate] [varchar](max) NULL,
	[Message_End] [varchar](max) NULL,
	[CreatedBy] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
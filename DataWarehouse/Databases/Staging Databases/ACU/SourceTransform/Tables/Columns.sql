﻿CREATE TABLE [SourceTransform].[Columns](
	[schemaName] [sysname] NOT NULL,
	[tableName] [sysname] NOT NULL,
	[columnName] [sysname] NOT NULL,
	[transformExpression] [varchar](100) NULL,
	[columnIndex] [int] NOT NULL,
	[Export] [bit] NULL
) ON [PRIMARY]
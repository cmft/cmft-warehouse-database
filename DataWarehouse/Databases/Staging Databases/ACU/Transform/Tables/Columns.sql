﻿CREATE TABLE [Transform].[Columns](
	[schemaName] [sysname] NOT NULL,
	[tableName] [sysname] NOT NULL,
	[columnName] [sysname] NOT NULL,
	[transformExpression] [varchar](max) NULL,
	[realDataLength] [int] NULL,
	[columnIndex] [int] NOT NULL,
	[Transform] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
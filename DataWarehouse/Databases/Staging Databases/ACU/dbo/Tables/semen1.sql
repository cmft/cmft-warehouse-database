﻿CREATE TABLE [dbo].[semen1](
	[hospnum] [varchar](10) NULL,
	[sfadate] [datetime] NULL,
	[emissionat] [varchar](max) NULL,
	[liquefaction] [varchar](10) NULL,
	[viscosity] [varchar](6) NULL,
	[pH] [smallint] NULL,
	[motilitypc] [varchar](max) NULL,
	[progression] [smallint] NULL,
	[volume] [real] NULL,
	[sperm1ml] [smallint] NULL,
	[totsp] [smallint] NULL,
	[NormalForms] [smallint] NULL,
	[AbnormalHeads] [smallint] NULL,
	[MidPcDefects] [smallint] NULL,
	[TailDefects] [smallint] NULL,
	[notes] [varchar](250) NULL,
	[MAR] [varchar](13) NULL,
	[dayab] [real] NULL,
	[survival] [smallint] NULL,
	[invoice] [smallint] NULL,
	[Source] [varchar](24) NULL,
	[SpermStored] [varchar](1) NULL,
	[InitVolume] [smallint] NULL,
	[InitTotSpermNo] [smallint] NULL,
	[InitMotility] [smallint] NULL,
	[InitNormForms] [smallint] NULL,
	[FinalPrepSNum] [smallint] NULL,
	[FinalMotility] [smallint] NULL,
	[FinalNormForms] [smallint] NULL,
	[FinalSInsemPEgg] [smallint] NULL,
	[PatientName] [varchar](35) NULL,
	[PartnerName] [varchar](36) NULL,
	[Cells] [smallint] NULL,
	[PrepMethod] [varchar](20) NULL,
	[DonorCode] [varchar](13) NULL,
	[DonorBank] [varchar](50) NULL,
	[dilution] [smallint] NULL,
	[IVFOITest] [varchar](10) NULL,
	[SNumber] [smallint] NULL,
	[fee] [smallint] NULL,
	[semen_fee_dt] [varchar](10) NULL,
	[SortKey] [varchar](29) NULL,
	[prep_mot] [smallint] NULL,
	[mot_count] [smallint] NULL,
	[prog_prep] [real] NULL,
	[purpose] [varchar](max) NULL,
	[Time_an] [datetime] NULL,
	[complete] [varchar](7) NULL,
	[performed] [varchar](17) NULL,
	[place] [varchar](6) NULL,
	[illness] [varchar](13) NULL,
	[odour] [varchar](max) NULL,
	[appear] [varchar](max) NULL,
	[ro_cells] [varchar](6) NULL,
	[epith_cells] [varchar](max) NULL,
	[erythro] [varchar](max) NULL,
	[micro] [varchar](max) NULL,
	[clump] [varchar](11) NULL,
	[debris] [varchar](6) NULL,
	[leuc] [varchar](max) NULL,
	[Bacter] [varchar](10) NULL,
	[time_pr] [varchar](max) NULL,
	[conc_init] [real] NULL,
	[conc_prep] [varchar](8) NULL,
	[conc_24] [varchar](max) NULL,
	[mot_init] [real] NULL,
	[mot_prep] [real] NULL,
	[mot_24] [smallint] NULL,
	[pmot_init] [smallint] NULL,
	[pmot_prep] [smallint] NULL,
	[pmot_24] [varchar](max) NULL,
	[prog_init] [real] NULL,
	[pro_prep] [real] NULL,
	[prog_24] [varchar](max) NULL,
	[abn_init] [varchar](4) NULL,
	[abn_prep] [smallint] NULL,
	[abn_24] [varchar](max) NULL,
	[oligo] [varchar](max) NULL,
	[astheno] [varchar](max) NULL,
	[terato] [varchar](max) NULL,
	[azoo] [varchar](max) NULL,
	[asab] [varchar](max) NULL,
	[day2count] [varchar](max) NULL,
	[day2mot] [varchar](max) NULL,
	[day2prog] [varchar](max) NULL,
	[day2abn] [varchar](max) NULL,
	[day1easy] [varchar](max) NULL,
	[day2easy] [varchar](max) NULL,
	[normo] [varchar](max) NULL,
	[dil_prep] [varchar](max) NULL,
	[insem_c] [varchar](max) NULL,
	[pats2rec] [varchar](1) NULL,
	[single] [varchar](max) NULL,
	[lesbian] [varchar](max) NULL,
	[Frozen] [varchar](max) NULL,
	[yield] [real] NULL,
	[insem_vol] [varchar](3) NULL,
	[embryologist] [varchar](18) NULL,
	[criteria] [varchar](max) NULL,
	[cyccode] [varchar](11) NULL,
	[MotA] [real] NULL,
	[MotB] [real] NULL,
	[MotC] [real] NULL,
	[MotD] [real] NULL,
	[MotAP] [real] NULL,
	[MotBP] [real] NULL,
	[MotCP] [real] NULL,
	[MotDP] [real] NULL,
	[Morp_Normal] [smallint] NULL,
	[Morp_Border] [smallint] NULL,
	[Morp_HD] [smallint] NULL,
	[Morp_Neck] [smallint] NULL,
	[Morp_TD] [smallint] NULL,
	[IBT] [smallint] NULL,
	[IgA] [smallint] NULL,
	[IgG] [smallint] NULL,
	[authorised] [varchar](18) NULL,
	[dtbpvol] [real] NULL,
	[dtbpmot] [real] NULL,
	[dtbpimmot] [smallint] NULL,
	[dtbnonmot] [smallint] NULL,
	[dtbimmot] [smallint] NULL,
	[SpermImport] [varchar](max) NULL,
	[SpermImportRef] [varchar](19) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[ACUColumns](
	[schemaName] [varchar](30) NULL,
	[tableName] [sysname] NOT NULL,
	[columnName] [varchar](128) NULL,
	[dataType] [varchar](50) NULL,
	[length] [int] NULL,
	[scale] [smallint] NULL,
	[radix] [smallint] NULL,
	[nullable] [smallint] NULL,
	[columnIndex] [bigint] NULL
) ON [PRIMARY]
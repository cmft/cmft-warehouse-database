﻿CREATE TABLE [dbo].[ConfigFiles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [sysname] NOT NULL,
	[configFileGroup] [int] NOT NULL
) ON [PRIMARY]
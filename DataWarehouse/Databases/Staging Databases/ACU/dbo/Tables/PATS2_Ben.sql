﻿CREATE TABLE [dbo].[PATS2_Ben](
	[Row] [smallint] NULL,
	[mptnumber] [varchar](max) NULL,
	[OnProgramme] [varchar](max) NULL,
	[OffProgramme] [varchar](max) NULL,
	[source] [varchar](max) NULL,
	[NHSSORTKEY] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
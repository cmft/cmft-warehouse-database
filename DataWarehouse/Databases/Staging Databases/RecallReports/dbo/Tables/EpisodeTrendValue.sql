﻿CREATE TABLE [dbo].[EpisodeTrendValue](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[TrendDescriptionKey] [int] NULL,
	[Value] [float] NULL,
	[Time] [datetime] NULL,
	[ValueCategory] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EpisodeMethodCOMonitoring](
	[EpisodeKey] [int] NOT NULL,
	[MonitoringUsed] [bit] NOT NULL,
	[MonitorTechniqueKey] [int] NULL,
	[MonitorTechniqueDesc] [nvarchar](50) NULL
) ON [PRIMARY]
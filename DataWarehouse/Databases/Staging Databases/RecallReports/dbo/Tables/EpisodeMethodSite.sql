﻿CREATE TABLE [dbo].[EpisodeMethodSite](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[MethodKey] [int] NULL,
	[MethodDescription] [nvarchar](100) NULL,
	[TypeKey] [int] NULL,
	[TypeDescription] [nvarchar](100) NULL,
	[ItemSize] [real] NULL,
	[CannulaKey] [int] NULL,
	[CannulaDescription] [nvarchar](100) NULL,
	[LeftRightKey] [int] NULL,
	[LeftRightDescription] [nvarchar](100) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
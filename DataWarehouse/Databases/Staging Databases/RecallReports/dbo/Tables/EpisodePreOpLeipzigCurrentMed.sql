﻿CREATE TABLE [dbo].[EpisodePreOpLeipzigCurrentMed](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[Dose] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL
) ON [PRIMARY]
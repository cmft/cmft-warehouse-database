﻿CREATE TABLE [dbo].[EpisodePreOpGeneral](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[HeightInCm] [real] NULL,
	[WeightInKg] [real] NULL,
	[BSA] [real] NULL,
	[WithOutFoodSince] [datetime] NULL,
	[WithOutDrinkSince] [datetime] NULL,
	[ASAStatus] [smallint] NULL,
	[ASAEmergency] [bit] NOT NULL,
	[Systolic] [smallint] NULL,
	[Diastolic] [smallint] NULL,
	[HeartRate] [smallint] NULL,
	[ToothStatusDiscussed] [bit] NULL,
	[Note] [nvarchar](3000) NULL
) ON [PRIMARY]
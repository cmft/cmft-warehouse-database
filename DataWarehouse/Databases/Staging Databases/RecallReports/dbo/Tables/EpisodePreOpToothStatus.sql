﻿CREATE TABLE [dbo].[EpisodePreOpToothStatus](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[CodeKey] [int] NULL,
	[Code] [nvarchar](100) NULL,
	[Description] [nvarchar](255) NULL,
	[UpLeftQuadrant] [bit] NOT NULL,
	[UpRightQuadrant] [bit] NOT NULL,
	[LowLeftQuadrant] [bit] NOT NULL,
	[LowRightQuadrant] [bit] NOT NULL
) ON [PRIMARY]
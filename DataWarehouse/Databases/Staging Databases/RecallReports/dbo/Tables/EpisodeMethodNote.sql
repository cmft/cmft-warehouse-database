﻿CREATE TABLE [dbo].[EpisodeMethodNote](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[Note] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
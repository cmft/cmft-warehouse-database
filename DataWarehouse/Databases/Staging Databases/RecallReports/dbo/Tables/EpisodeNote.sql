﻿CREATE TABLE [dbo].[EpisodeNote](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[NoteKey] [int] NULL,
	[NoteTitleKey] [int] NULL,
	[NoteTitle] [nvarchar](100) NULL,
	[AuthorKey] [int] NULL,
	[Author] [nvarchar](60) NULL,
	[Time] [datetime] NULL,
	[Text] [text] NULL,
	[ParentNoteKey] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
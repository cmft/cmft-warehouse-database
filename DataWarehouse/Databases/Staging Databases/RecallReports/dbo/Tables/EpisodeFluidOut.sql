﻿CREATE TABLE [dbo].[EpisodeFluidOut](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[FluidKey] [int] NULL,
	[FluidDescription] [nvarchar](100) NULL,
	[CategoryKey] [int] NULL,
	[CategoryDescription] [nvarchar](100) NULL,
	[Amount] [real] NULL,
	[UnitsKey] [int] NULL,
	[UnitsDescription] [nvarchar](100) NULL,
	[BodyMassKey] [int] NULL,
	[BodyMassDescription] [nvarchar](100) NULL,
	[Time] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EpisodeMethodBlock](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[MethodKey] [int] NULL,
	[MethodDescription] [nvarchar](100) NULL,
	[TypeKey] [int] NULL,
	[TypeDescription] [nvarchar](100) NULL,
	[CatheterSize] [real] NULL,
	[NeedleSize] [real] NULL,
	[InsulatedNeedle] [bit] NOT NULL,
	[NerveStimulator] [bit] NOT NULL,
	[LeftRightKey] [int] NULL,
	[LeftRightDescription] [nvarchar](100) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
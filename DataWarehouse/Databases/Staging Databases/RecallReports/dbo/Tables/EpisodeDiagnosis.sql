﻿CREATE TABLE [dbo].[EpisodeDiagnosis](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[DiagnosisKey] [int] NULL,
	[DiagnosisCode] [nvarchar](100) NULL,
	[DiagnosisDescription] [nvarchar](255) NULL,
	[StatusKey] [int] NULL,
	[StatusCode] [nvarchar](100) NULL,
	[StatusDescription] [nvarchar](255) NULL
) ON [PRIMARY]
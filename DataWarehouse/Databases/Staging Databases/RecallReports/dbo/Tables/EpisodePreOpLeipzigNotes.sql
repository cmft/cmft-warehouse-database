﻿CREATE TABLE [dbo].[EpisodePreOpLeipzigNotes](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[CategoryKey] [smallint] NULL,
	[TimeBeforeOp] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[Application] [nvarchar](50) NULL,
	[Signature] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EpisodeTrendDescription](
	[EpisodeKey] [int] NOT NULL,
	[TrendDescriptionKey] [int] IDENTITY(1,1) NOT NULL,
	[TrendKey] [int] NULL,
	[TrendDescription] [nvarchar](100) NULL,
	[MonitorKey] [int] NULL,
	[MonitorDescription] [nvarchar](50) NULL,
	[ArtefactRejectionOn] [bit] NOT NULL,
	[ArtefactHigh] [float] NULL,
	[ArtefactLow] [float] NULL,
	[UnitsKey] [int] NULL,
	[UnitsDescription] [nvarchar](100) NULL,
	[Colour] [int] NULL,
	[Graph] [int] NULL,
	[ScaleSide] [int] NULL,
	[Symbol] [int] NULL,
	[DisplayUnits] [int] NULL,
	[ChannelNumber] [int] NULL,
	[SampleRate] [int] NULL,
	[RtpDisplayed] [int] NULL
) ON [PRIMARY]
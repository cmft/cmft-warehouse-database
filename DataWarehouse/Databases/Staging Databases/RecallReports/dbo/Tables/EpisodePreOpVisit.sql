﻿CREATE TABLE [dbo].[EpisodePreOpVisit](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[Episodekey] [int] NOT NULL,
	[StaffKey] [int] NULL,
	[StaffName] [nvarchar](100) NULL,
	[StaffPhoneNumber] [nvarchar](30) NULL,
	[VisitDate] [datetime] NULL,
	[VisitDurationInMinutes] [smallint] NULL
) ON [PRIMARY]
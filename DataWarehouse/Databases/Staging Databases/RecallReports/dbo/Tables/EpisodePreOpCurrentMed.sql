﻿CREATE TABLE [dbo].[EpisodePreOpCurrentMed](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[CodeKey] [int] NULL,
	[Code] [nvarchar](100) NULL,
	[DrugName] [nvarchar](255) NULL,
	[Dose] [nvarchar](100) NULL,
	[Instruction] [nvarchar](100) NULL,
	[Time] [datetime] NULL
) ON [PRIMARY]
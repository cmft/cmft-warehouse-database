﻿CREATE TABLE [dbo].[EpisodePreOpPreMed](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[CodeKey] [int] NULL,
	[Code] [nvarchar](100) NULL,
	[DrugName] [nvarchar](255) NULL,
	[Dose] [nvarchar](100) NULL,
	[Units] [nvarchar](10) NULL,
	[Route] [nvarchar](100) NULL,
	[Time] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EpisodeMethodDiathermy](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[MethodKey] [int] NULL,
	[MethodDescription] [nvarchar](100) NULL,
	[Adhesive] [bit] NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
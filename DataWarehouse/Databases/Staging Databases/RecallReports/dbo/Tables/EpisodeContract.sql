﻿CREATE TABLE [dbo].[EpisodeContract](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[ContractKey] [int] NULL,
	[ContractDescription] [nvarchar](100) NULL,
	[PriorityKey] [int] NULL,
	[PriorityDescription] [nvarchar](100) NULL,
	[BedStatusKey] [int] NULL,
	[BedStatusDescription] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SessionStaff](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[SessionKey] [int] NOT NULL,
	[StaffKey] [int] NOT NULL,
	[FullName] [nvarchar](100) NULL,
	[LastName] [nvarchar](30) NULL,
	[CategoryKey] [int] NULL,
	[CategoryDescription] [nvarchar](100) NULL,
	[GradeKey] [int] NULL,
	[GradeDescription] [nvarchar](100) NULL,
	[RoleKey] [int] NULL,
	[RoleDescription] [nvarchar](100) NULL,
	[SupervisionKey] [int] NULL,
	[SupervisionDescription] [nvarchar](100) NULL,
	[SupervisorKey] [int] NULL,
	[SupervisorName] [nvarchar](100) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
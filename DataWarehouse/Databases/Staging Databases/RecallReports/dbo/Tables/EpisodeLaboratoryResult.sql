﻿CREATE TABLE [dbo].[EpisodeLaboratoryResult](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[LaboratoryResultKey] [int] NULL,
	[LabResultDescription] [nvarchar](100) NULL,
	[ResultTime] [datetime] NULL,
	[SourceKey] [int] NULL,
	[SourceDescription] [nvarchar](100) NULL,
	[StatusKey] [int] NULL,
	[StatusDescription] [nvarchar](100) NULL,
	[Value] [numeric](24, 2) NULL,
	[UnitsKey] [int] NULL,
	[UnitsDescription] [nvarchar](100) NULL,
	[Comments] [nvarchar](100) NULL
) ON [PRIMARY]
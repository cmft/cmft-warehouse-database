﻿CREATE TABLE [dbo].[EpisodePreOpLeipzigLabs](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[Value] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL
) ON [PRIMARY]
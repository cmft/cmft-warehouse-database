﻿CREATE TABLE [dbo].[EpisodePostOp](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[CardTypeKey] [int] NOT NULL,
	[CategoryKey] [int] NOT NULL,
	[CodeKey] [int] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Qualifier] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
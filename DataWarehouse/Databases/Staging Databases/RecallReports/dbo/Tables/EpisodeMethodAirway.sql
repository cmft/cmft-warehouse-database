﻿CREATE TABLE [dbo].[EpisodeMethodAirway](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[MethodKey] [int] NULL,
	[MethodDescription] [nvarchar](100) NULL,
	[TypeKey] [int] NULL,
	[TypeDescription] [nvarchar](100) NULL,
	[ItemSize] [real] NULL,
	[Length] [real] NULL,
	[CuffedPlainKey] [smallint] NULL,
	[CuffedPlainDescription] [nvarchar](100) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EpisodeAdmission](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[ConsultantKey] [int] NULL,
	[ConsultantName] [nvarchar](100) NULL,
	[SourceCategoryKey] [int] NULL,
	[SourceCategoryDesc] [nvarchar](100) NULL,
	[SourceLocationKey] [int] NULL,
	[SourceLocationDesc] [nvarchar](100) NULL,
	[IntendedDestCategoryKey] [int] NULL,
	[IntendedDestCategoryDesc] [nvarchar](100) NULL,
	[IntendedDestLocationKey] [int] NULL,
	[IntendedDestLocationDesc] [nvarchar](100) NULL,
	[ActualDestCategoryKey] [int] NULL,
	[ActualDestCategoryDesc] [nvarchar](100) NULL,
	[ActualDestLocationKey] [int] NULL,
	[ActualDestLocationDesc] [nvarchar](100) NULL
) ON [PRIMARY]
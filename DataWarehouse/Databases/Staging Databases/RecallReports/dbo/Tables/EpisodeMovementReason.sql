﻿CREATE TABLE [dbo].[EpisodeMovementReason](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[EpisodeMovementKey] [int] NULL,
	[MovementReasonKey] [int] NULL,
	[MovementReasonDescription] [nvarchar](100) NULL
) ON [PRIMARY]
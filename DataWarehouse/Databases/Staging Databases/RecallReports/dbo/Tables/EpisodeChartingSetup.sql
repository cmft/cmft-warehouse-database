﻿CREATE TABLE [dbo].[EpisodeChartingSetup](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[SetupKey] [int] NULL,
	[SetupDescription] [nvarchar](255) NULL
) ON [PRIMARY]
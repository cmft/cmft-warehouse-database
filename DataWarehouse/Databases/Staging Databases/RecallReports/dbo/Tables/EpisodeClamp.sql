﻿CREATE TABLE [dbo].[EpisodeClamp](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[Key] [int] NULL,
	[Code] [nvarchar](100) NULL,
	[Description] [nvarchar](255) NULL,
	[OnTime] [datetime] NULL,
	[OffTime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UploadLog](
	[EpisodeKey] [int] NOT NULL,
	[UploadTime] [datetime] NOT NULL,
	[LogTypeID] [smallint] NOT NULL,
	[LogDescription] [nvarchar](100) NOT NULL
) ON [PRIMARY]
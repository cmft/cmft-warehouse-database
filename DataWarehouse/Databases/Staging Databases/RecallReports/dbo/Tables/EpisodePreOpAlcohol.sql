﻿CREATE TABLE [dbo].[EpisodePreOpAlcohol](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[CodeKey] [int] NULL,
	[Code] [nvarchar](100) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
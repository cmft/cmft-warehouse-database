﻿CREATE TABLE [dbo].[EpisodePostOpLeipzigProblem](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[CategoryKey] [smallint] NULL,
	[Key] [int] NULL,
	[Description] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EpisodePreOpPreMedEffect](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[Effect] [nvarchar](100) NOT NULL
) ON [PRIMARY]
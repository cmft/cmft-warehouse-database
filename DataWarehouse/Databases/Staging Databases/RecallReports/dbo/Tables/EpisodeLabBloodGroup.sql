﻿CREATE TABLE [dbo].[EpisodeLabBloodGroup](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[ResultTime] [datetime] NULL,
	[SourceKey] [int] NULL,
	[SourceDescription] [nvarchar](100) NULL,
	[StatusKey] [int] NULL,
	[StatusDescription] [nvarchar](100) NULL,
	[BloodGroup] [nvarchar](2) NULL,
	[Rhesus] [nvarchar](10) NULL,
	[CoombsTest] [nvarchar](10) NULL,
	[IrregularAntibodies] [nvarchar](10) NULL,
	[ColdAntibodies] [nvarchar](10) NULL,
	[HaemoglobinF] [nvarchar](10) NULL,
	[G6PD] [nvarchar](10) NULL,
	[Sickle] [nvarchar](10) NULL,
	[Thalassaemia] [nvarchar](10) NULL,
	[Comments] [nvarchar](100) NULL
) ON [PRIMARY]
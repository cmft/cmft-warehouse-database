﻿CREATE TABLE [dbo].[EpisodeDrug](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[DrugKey] [int] NULL,
	[DrugDescription] [nvarchar](100) NULL,
	[CategoryKey] [int] NULL,
	[CategoryDescription] [nvarchar](100) NULL,
	[Dose] [real] NULL,
	[UnitsKey] [int] NULL,
	[UnitsDescription] [nvarchar](100) NULL,
	[BodyMassKey] [int] NULL,
	[BodyMassDescription] [nvarchar](100) NULL,
	[TimeKey] [int] NULL,
	[TimeDescription] [nvarchar](100) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[DeliveryMethodKey] [int] NULL,
	[DeliveryMethodDescription] [nvarchar](255) NULL
) ON [PRIMARY]
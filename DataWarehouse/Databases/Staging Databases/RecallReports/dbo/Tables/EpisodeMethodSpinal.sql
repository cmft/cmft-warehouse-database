﻿CREATE TABLE [dbo].[EpisodeMethodSpinal](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[MethodKey] [int] NULL,
	[MethodDescription] [nvarchar](100) NULL,
	[PositionKey] [int] NULL,
	[PositionDescription] [nvarchar](100) NULL,
	[NeedleKey] [int] NULL,
	[NeedleDescription] [nvarchar](100) NULL,
	[CatheterKey] [int] NULL,
	[CatheterDescription] [nvarchar](100) NULL,
	[CatheterInSpace] [real] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
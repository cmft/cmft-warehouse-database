﻿CREATE TABLE [dbo].[EpisodeEventArtefact](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[EpisodeEventKey] [int] NOT NULL,
	[Time] [datetime] NOT NULL,
	[Value] [float] NOT NULL,
	[Parameter] [nvarchar](100) NOT NULL
) ON [PRIMARY]
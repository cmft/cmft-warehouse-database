﻿CREATE TABLE [dbo].[Session](
	[SessionKey] [int] NOT NULL,
	[Description] [nvarchar](100) NULL,
	[SessionTypeKey] [int] NULL,
	[SessionTypeDescription] [nvarchar](100) NULL,
	[LocationKey] [int] NULL,
	[LocationDescription] [nvarchar](100) NULL,
	[LocationTypeCode] [nvarchar](10) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Duration]  AS (datediff(minute,[StartTime],[EndTime])),
	[OwnerCategoryKey] [int] NULL,
	[OwnerCategoryDescription] [nvarchar](100) NULL,
	[OwnerKey] [int] NULL,
	[OwnerName] [nvarchar](100) NULL
) ON [PRIMARY]
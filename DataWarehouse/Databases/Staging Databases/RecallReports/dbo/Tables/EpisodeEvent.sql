﻿CREATE TABLE [dbo].[EpisodeEvent](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[EventKey] [int] NULL,
	[EventCode] [nvarchar](100) NULL,
	[EventDescription] [nvarchar](255) NULL,
	[Time] [datetime] NULL,
	[CategoryKey] [smallint] NULL,
	[CategoryDescription] [nvarchar](100) NULL,
	[Critical] [bit] NOT NULL,
	[LocationKey] [int] NULL,
	[LocationDescription] [nvarchar](100) NULL
) ON [PRIMARY]
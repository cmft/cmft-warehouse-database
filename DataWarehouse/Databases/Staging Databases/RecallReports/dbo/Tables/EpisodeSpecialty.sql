﻿CREATE TABLE [dbo].[EpisodeSpecialty](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[SpecialtyKey] [int] NOT NULL,
	[SpecialtyDescription] [nvarchar](100) NULL
) ON [PRIMARY]
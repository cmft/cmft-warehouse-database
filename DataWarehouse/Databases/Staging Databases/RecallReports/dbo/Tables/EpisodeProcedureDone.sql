﻿CREATE TABLE [dbo].[EpisodeProcedureDone](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[ProcedureKey] [int] NULL,
	[ProcedureCode] [nvarchar](100) NULL,
	[ProcedureDescription] [nvarchar](255) NULL,
	[LateralityKey] [int] NULL,
	[LateralityCode] [nvarchar](100) NULL,
	[LateralityDescription] [nvarchar](255) NULL
) ON [PRIMARY]
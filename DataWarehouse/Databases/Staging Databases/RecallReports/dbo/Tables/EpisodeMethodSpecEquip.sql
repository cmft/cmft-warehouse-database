﻿CREATE TABLE [dbo].[EpisodeMethodSpecEquip](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[MethodKey] [int] NULL,
	[MethodDescription] [nvarchar](100) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
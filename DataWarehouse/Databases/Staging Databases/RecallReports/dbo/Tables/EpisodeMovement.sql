﻿CREATE TABLE [dbo].[EpisodeMovement](
	[EpisodeKey] [int] NOT NULL,
	[EpisodeMovementKey] [int] IDENTITY(1,1) NOT NULL,
	[ListFromKey] [int] NULL,
	[ListToKey] [int] NULL,
	[MovementTime] [datetime] NULL,
	[MovementMarkerKey] [int] NULL,
	[MovementMarkerDescription] [nvarchar](100) NULL,
	[ListOrder] [smallint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ListMovement](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[ListKey] [int] NULL,
	[ListMovementKey] [int] NULL,
	[SessionFromKey] [int] NULL,
	[SessionToKey] [int] NULL,
	[MovementTime] [datetime] NULL,
	[MovementMarkerKey] [int] NULL,
	[MovementMarkerDescription] [nvarchar](100) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL
) ON [PRIMARY]
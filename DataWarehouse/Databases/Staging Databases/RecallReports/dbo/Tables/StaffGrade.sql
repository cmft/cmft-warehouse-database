﻿CREATE TABLE [dbo].[StaffGrade](
	[Key] [int] NOT NULL,
	[CategoryKey] [int] NULL,
	[Description] [nvarchar](100) NULL,
	[SortOrder] [int] NULL
) ON [PRIMARY]
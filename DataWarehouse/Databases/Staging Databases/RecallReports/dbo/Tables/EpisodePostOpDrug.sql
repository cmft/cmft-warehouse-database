﻿CREATE TABLE [dbo].[EpisodePostOpDrug](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[DrugDictionaryKey] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Dose] [nvarchar](100) NULL,
	[Instruction] [nvarchar](100) NULL,
	[UserTimeStamp] [datetime] NULL
) ON [PRIMARY]
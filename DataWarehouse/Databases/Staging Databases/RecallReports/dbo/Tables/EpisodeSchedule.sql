﻿CREATE TABLE [dbo].[EpisodeSchedule](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeKey] [int] NOT NULL,
	[ListKey] [int] NULL,
	[ListOrder] [smallint] NULL,
	[ScheduledStartTime] [datetime] NULL,
	[ScheduledEndTime] [datetime] NULL,
	[ScheduledDuration] [smallint] NULL,
	[ActualStartTime] [datetime] NULL,
	[ActualEndTime] [datetime] NULL,
	[ActualDuration] [smallint] NULL,
	[LocationKey] [int] NULL,
	[LocationDescription] [nvarchar](100) NULL,
	[EmergencyCase] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ListMovementReason](
	[PrimaryKey] [int] IDENTITY(1,1) NOT NULL,
	[ListMovementKey] [int] NULL,
	[MovementReasonKey] [int] NULL,
	[MovementReasonDescription] [nvarchar](100) NULL
) ON [PRIMARY]
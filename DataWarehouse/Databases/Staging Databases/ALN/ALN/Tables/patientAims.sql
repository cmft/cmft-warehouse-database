﻿CREATE TABLE [ALN].[patientAims](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
	[DisplayOrder] [int] NOT NULL
) ON [PRIMARY]
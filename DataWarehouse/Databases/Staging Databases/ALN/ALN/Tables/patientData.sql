﻿CREATE TABLE [ALN].[patientData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[firstName] [nvarchar](32) NOT NULL,
	[lastName] [nvarchar](32) NOT NULL,
	[dateOfBirth] [date] NOT NULL,
	[districtNumber] [varchar](16) NOT NULL,
	[contactNumber] [varchar](24) NULL
) ON [PRIMARY]
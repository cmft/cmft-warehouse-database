﻿CREATE TABLE [Clinic].[FollowUp](
	[visitDataBaseID] [int] NOT NULL,
	[otherInformation] [varchar](max) NULL,
	[comments] [varchar](max) NULL,
	[recordTimeStamp] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
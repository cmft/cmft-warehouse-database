﻿CREATE TABLE [Clinic].[visitDataBase](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[dateOfVisit] [date] NOT NULL,
	[completedBy] [sysname] NOT NULL,
	[patientData_ID] [int] NOT NULL,
	[drinkingLevel] [varchar](max) NULL,
	[patientThoughtsAboutDrinkingLevel] [varchar](max) NULL,
	[hasCurrentAgencyInvolvement] [bit] NOT NULL,
	[currentAgencyInvolvementDetail] [varchar](max) NULL,
	[haveReferralsMade] [bit] NOT NULL,
	[ReferralsMadeDetail] [varchar](max) NULL,
	[hasQuestionnaireBeenProvided] [bit] NOT NULL,
	[isGPLetterSent] [bit] NOT NULL,
	[recordTimeStamp] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
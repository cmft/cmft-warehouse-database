﻿CREATE TABLE [Ward].[InitialAssessment](
	[stayDataBaseID] [int] NOT NULL,
	[drugUse] [varchar](max) NULL,
	[hasPreviousDetox] [bit] NOT NULL,
	[previousDetoxDetail] [varchar](max) NULL,
	[symptomHistory] [varchar](max) NULL,
	[physicalHealth] [varchar](max) NULL,
	[mentalHealth] [varchar](max) NULL,
	[hasDependents] [bit] NOT NULL,
	[dependentsDetail] [varchar](max) NULL,
	[outComePlanAndReferrals] [varchar](max) NULL,
	[isGPLetterSent] [bit] NOT NULL,
	[recordTimeStamp] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
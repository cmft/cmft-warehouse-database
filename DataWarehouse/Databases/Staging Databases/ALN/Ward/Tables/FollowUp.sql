﻿CREATE TABLE [Ward].[FollowUp](
	[stayDataBaseID] [int] NOT NULL,
	[patientAim] [int] NOT NULL,
	[patientAimComment] [varchar](max) NULL,
	[dischargeToDrink] [bit] NOT NULL,
	[recordTimeStamp] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿
CREATE VIEW [dbo].[vwPDDReasons]
AS
SELECT     dbo.PDDReasons.PDDReasonID, dbo.PDDReasonGroups.PDDGroup, dbo.PDDReasons.PDDReason, dbo.PDDReasons.Active
FROM         dbo.PDDReasonGroups INNER JOIN
                      dbo.PDDReasons ON dbo.PDDReasonGroups.PDDGroupID = dbo.PDDReasons.PDDGroup

GO


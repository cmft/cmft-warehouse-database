﻿CREATE TABLE [dbo].[WhiteboardGridDropDownValues](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WhiteboardGridID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PreAdmitString](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient] [varchar](8) NULL,
	[ExpDateString] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[IsThisDate] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AEStats](
	[Year] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Day] [int] NOT NULL,
	[Hospital] [char](4) NOT NULL,
	[Total] [int] NOT NULL CONSTRAINT [DF_AEStats_Total]  DEFAULT (0),
	[4hr] [int] NOT NULL CONSTRAINT [DF_AEStats_4hr]  DEFAULT (0),
	[Mean] [int] NOT NULL CONSTRAINT [DF_AEStats_Mean]  DEFAULT (0),
	[Incpt] [int] NOT NULL CONSTRAINT [DF_AEStats_Incpt]  DEFAULT (0),
	[AEDate] [char](8) NULL
) ON [PRIMARY]
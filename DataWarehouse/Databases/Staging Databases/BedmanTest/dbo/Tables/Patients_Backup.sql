﻿CREATE TABLE [dbo].[Patients_Backup](
	[Patient] [varchar](15) NOT NULL,
	[Surname] [varchar](30) NOT NULL,
	[Forename] [varchar](30) NOT NULL,
	[YoB] [char](4) NULL,
	[DoB] [datetime] NULL,
	[Gender] [char](1) NULL
) ON [PRIMARY]
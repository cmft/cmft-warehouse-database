﻿CREATE TABLE [dbo].[UserRecentCons](
	[UserID] [int] NOT NULL,
	[ConsultantID] [int] NOT NULL,
	[DateUsed] [datetime] NOT NULL CONSTRAINT [DF_UserRecentCons_DateUsed]  DEFAULT (getdate())
) ON [PRIMARY]
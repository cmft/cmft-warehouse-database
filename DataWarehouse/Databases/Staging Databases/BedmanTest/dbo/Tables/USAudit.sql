﻿CREATE TABLE [dbo].[USAudit](
	[USAuditID] [int] IDENTITY(1,1) NOT NULL,
	[USTranType] [tinyint] NOT NULL,
	[USTranDate] [datetime] NOT NULL CONSTRAINT [DF_USAudit_USTranDate]  DEFAULT (getdate()),
	[USTranUser] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[SpellID] [int] NOT NULL,
	[StatusText] [varchar](30) NOT NULL,
	[StatusTime] [datetime] NOT NULL,
	[UserID] [int] NOT NULL
) ON [PRIMARY]
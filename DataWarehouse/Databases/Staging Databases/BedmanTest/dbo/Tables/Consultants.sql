﻿CREATE TABLE [dbo].[Consultants](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ConsCode] [varchar](6) NOT NULL,
	[Initials] [varchar](6) NOT NULL,
	[Surname] [varchar](24) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_Consultants_Active]  DEFAULT (1),
	[Title] [varchar](4) NULL,
	[GMCCode] [char](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserConsultants](
	[UserID] [int] NOT NULL,
	[ConsultantID] [int] NOT NULL,
	[Permission] [smallint] NOT NULL
) ON [PRIMARY]
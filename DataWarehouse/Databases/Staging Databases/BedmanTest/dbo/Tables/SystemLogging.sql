﻿CREATE TABLE [dbo].[SystemLogging](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL CONSTRAINT [DF_SystemLogging_LogDate]  DEFAULT (getdate()),
	[LogLevel] [varchar](10) NULL,
	[LogLogger] [varchar](max) NULL,
	[LogMessage] [varchar](max) NULL,
	[LogMachineName] [varchar](100) NULL,
	[LogUserName] [varchar](50) NULL,
	[LogThread] [varchar](100) NULL,
	[LogStackTrace] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[SpellAudit](
	[SpellAuditID] [int] IDENTITY(1,1) NOT NULL,
	[SpellTranType] [tinyint] NOT NULL,
	[SpellTranUser] [int] NOT NULL,
	[SpellTranDate] [datetime] NOT NULL CONSTRAINT [DF_SpellAudit_SpellTranDate]  DEFAULT (getdate()),
	[SpellID] [int] NOT NULL,
	[CurrWard] [char](4) NULL,
	[FacilityID] [int] NULL,
	[Diagnosis] [varchar](40) NULL,
	[CurrSPec] [char](4) NULL,
	[AEDocID] [int] NULL
) ON [PRIMARY]
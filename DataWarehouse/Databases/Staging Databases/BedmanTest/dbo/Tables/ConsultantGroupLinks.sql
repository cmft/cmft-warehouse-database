﻿CREATE TABLE [dbo].[ConsultantGroupLinks](
	[LinksID] [int] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[ConsultantID] [int] NOT NULL
) ON [PRIMARY]
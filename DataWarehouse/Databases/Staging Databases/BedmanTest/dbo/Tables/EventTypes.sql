﻿CREATE TABLE [dbo].[EventTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[EventCategoryID] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL
) ON [PRIMARY]
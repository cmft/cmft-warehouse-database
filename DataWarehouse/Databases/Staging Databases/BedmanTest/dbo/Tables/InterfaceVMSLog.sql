﻿CREATE TABLE [dbo].[InterfaceVMSLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[LogText] [varchar](255) NOT NULL
) ON [PRIMARY]
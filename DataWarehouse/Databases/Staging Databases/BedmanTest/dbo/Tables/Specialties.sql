﻿CREATE TABLE [dbo].[Specialties](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SpecCode] [varchar](4) NOT NULL,
	[MainSpec] [varchar](7) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[Korner] [varchar](4) NOT NULL,
	[Main] [bit] NOT NULL CONSTRAINT [DF_Specialties_Main]  DEFAULT (0),
	[Active] [bit] NOT NULL CONSTRAINT [DF_Specialties_Active]  DEFAULT (1),
	[DrSpec] [varchar](5) NULL
) ON [PRIMARY]
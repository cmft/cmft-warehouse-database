﻿CREATE TABLE [dbo].[InterfaceApplicationLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[LoggedAction] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserSecurityGroups](
	[UserID] [int] NOT NULL,
	[TemplateID] [int] NOT NULL,
	[EffectiveDate] [datetime] NOT NULL CONSTRAINT [DF_UserSecurityGroups_EffectiveDate]  DEFAULT (getdate())
) ON [PRIMARY]
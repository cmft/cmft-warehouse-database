﻿CREATE TABLE [dbo].[InterfaceSettings](
	[ID] [int] NOT NULL,
	[NextDownloadTime] [datetime] NOT NULL,
	[Status] [char](12) NOT NULL,
	[StatusTime] [datetime] NOT NULL,
	[DayEndTime] [datetime] NOT NULL,
	[ForceDayend] [tinyint] NOT NULL,
	[Frequency] [tinyint] NOT NULL,
	[ATLDate] [char](8) NOT NULL CONSTRAINT [DF_InterfaceSettings_ATLDate]  DEFAULT (20010101),
	[ATLNo] [char](6) NOT NULL CONSTRAINT [DF_InterfaceSettings_ATLNo]  DEFAULT (1),
	[TLDate] [char](8) NOT NULL CONSTRAINT [DF_InterfaceSettings_TLDate]  DEFAULT (20010101),
	[TLNo] [char](6) NOT NULL CONSTRAINT [DF_InterfaceSettings_TLNo]  DEFAULT (1),
	[OCMDate] [char](8) NOT NULL CONSTRAINT [DF_InterfaceSettings_OCMDate]  DEFAULT (20040801),
	[OCMNo] [char](6) NOT NULL CONSTRAINT [DF_InterfaceSettings_OCMNo]  DEFAULT (1),
	[Throttle] [tinyint] NOT NULL CONSTRAINT [DF_InterfaceSettings_Throttle]  DEFAULT (0)
) ON [PRIMARY]
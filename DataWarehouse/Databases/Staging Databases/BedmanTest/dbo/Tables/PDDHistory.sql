﻿CREATE TABLE [dbo].[PDDHistory](
	[PDDHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[SpellID] [int] NOT NULL,
	[PDD] [datetime] NULL,
	[PDDreason] [int] NULL,
	[PDDComment] [varchar](50) NULL,
	[PDDEnteredBy] [int] NULL,
	[PDDEntered] [datetime] NULL,
	[Type] [char](3) NULL,
	[LocationID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserRecentConGroups](
	[UserID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[DateUsed] [datetime] NOT NULL CONSTRAINT [DF_UserRecentConGroups_DateUsed]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserLocations](
	[UserID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Permission] [smallint] NOT NULL CONSTRAINT [DF_UserLocati_Permission_9__15]  DEFAULT (0)
) ON [PRIMARY]
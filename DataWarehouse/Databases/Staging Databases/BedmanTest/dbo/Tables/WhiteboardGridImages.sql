﻿CREATE TABLE [dbo].[WhiteboardGridImages](
	[WhiteboardGridID] [int] NOT NULL,
	[WhiteboardGridImagesID] [int] NOT NULL,
	[SelectedValue] [varchar](20) NOT NULL,
	[ImageOrder] [int] NULL DEFAULT (1)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AEAttendance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AENo] [varchar](14) NOT NULL,
	[LastDownload] [datetime] NOT NULL CONSTRAINT [DF_AEAttendan_LastDownloa2__12]  DEFAULT (getdate()),
	[AttendanceDt] [datetime] NOT NULL,
	[DepartDt] [datetime] NULL,
	[Hospital] [varchar](4) NOT NULL,
	[Patient] [varchar](8) NULL,
	[Episode] [varchar](5) NULL,
	[DischargeTime] [datetime] NULL,
	[AELoc] [int] NOT NULL CONSTRAINT [DF_AEAttendance_AELoc]  DEFAULT (0),
	[PVisitNO] [varchar](30) NULL,
	[ForTraining] [tinyint] NULL CONSTRAINT [DF_AEAttendance_ForTraining]  DEFAULT (0),
	[LastOperation] [varchar](20) NULL,
	[LastTransactionID] [int] NULL
) ON [PRIMARY]
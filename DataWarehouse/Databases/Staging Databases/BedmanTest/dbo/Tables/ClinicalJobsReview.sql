﻿CREATE TABLE [dbo].[ClinicalJobsReview](
	[JobsReviewID] [int] IDENTITY(1,1) NOT NULL,
	[Patient] [varchar](15) NOT NULL,
	[EpisodeID] [varchar](5) NOT NULL,
	[LocationID] [int] NOT NULL,
	[Jobs] [text] NULL,
	[Review] [text] NULL,
	[Entered] [datetime] NOT NULL,
	[EnteredBy] [varchar](20) NULL,
	[HospSpellID] [int] NULL,
	[AEEpisode] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
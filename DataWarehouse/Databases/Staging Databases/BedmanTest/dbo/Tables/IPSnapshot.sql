﻿CREATE TABLE [dbo].[IPSnapshot](
	[Hospital] [varchar](4) NOT NULL,
	[SnapShotDate] [datetime] NULL,
	[sYear] [int] NOT NULL,
	[sMonth] [int] NOT NULL,
	[sDate] [int] NOT NULL,
	[sHour] [int] NOT NULL,
	[Admits] [int] NOT NULL CONSTRAINT [DF_IPSnapshot_Admits]  DEFAULT (0),
	[Discs] [int] NOT NULL CONSTRAINT [DF_IPSnapshot_Discs]  DEFAULT (0)
) ON [PRIMARY]
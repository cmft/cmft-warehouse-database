﻿CREATE TABLE [dbo].[FromTIE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Filename] [varchar](50) NULL,
	[UpdateTime] [datetime] NULL,
	[EVN] [varchar](10) NULL,
	[HL7Text] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
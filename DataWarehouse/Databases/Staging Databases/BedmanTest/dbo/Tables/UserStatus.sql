﻿CREATE TABLE [dbo].[UserStatus](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[SpellID] [int] NOT NULL,
	[StatusText] [varchar](50) NOT NULL,
	[StatusTime] [datetime] NOT NULL,
	[UserID] [int] NOT NULL
) ON [PRIMARY]
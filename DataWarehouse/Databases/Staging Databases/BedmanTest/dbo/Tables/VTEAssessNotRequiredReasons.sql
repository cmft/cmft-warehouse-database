﻿CREATE TABLE [dbo].[VTEAssessNotRequiredReasons](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_VTEAssessNotRequiredReasons_Active]  DEFAULT ((1)),
	[Order] [int] NOT NULL CONSTRAINT [DF_VTEAssessNotRequiredReasons_Order]  DEFAULT ((0))
) ON [PRIMARY]
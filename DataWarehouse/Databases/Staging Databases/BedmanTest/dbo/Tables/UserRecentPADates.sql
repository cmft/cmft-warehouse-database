﻿CREATE TABLE [dbo].[UserRecentPADates](
	[UserID] [int] NOT NULL,
	[DateFrom] [datetime] NOT NULL,
	[DateTo] [datetime] NOT NULL
) ON [PRIMARY]
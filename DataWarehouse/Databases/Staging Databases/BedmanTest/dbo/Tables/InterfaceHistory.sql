﻿CREATE TABLE [dbo].[InterfaceHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DownloadTime] [char](10) NULL,
	[InpatientDate] [char](8) NULL,
	[InpatientTrans] [char](8) NULL,
	[AEDate] [char](8) NULL,
	[AETrans] [char](8) NULL,
	[CreationTime] [datetime] NOT NULL CONSTRAINT [DF_InterfaceHistory_CreationTime]  DEFAULT (getdate()),
	[OCMDate] [char](8) NULL,
	[OCMTrans] [char](8) NULL
) ON [PRIMARY]
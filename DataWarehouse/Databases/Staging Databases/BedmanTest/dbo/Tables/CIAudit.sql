﻿CREATE TABLE [dbo].[CIAudit](
	[CIAuditid] [int] IDENTITY(1,1) NOT NULL,
	[CITranType] [tinyint] NOT NULL,
	[CITranUSer] [int] NOT NULL,
	[CITranDate] [datetime] NOT NULL CONSTRAINT [DF_CIAudit_CITranDate]  DEFAULT (getdate()),
	[BedReqID] [int] NOT NULL,
	[ClinIndID] [int] NOT NULL,
	[Value] [bit] NOT NULL
) ON [PRIMARY]
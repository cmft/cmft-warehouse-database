﻿CREATE TABLE [dbo].[UserLogins](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](12) NOT NULL CONSTRAINT [DF_UserLogins_UserName]  DEFAULT (''),
	[IPAddress] [char](15) NOT NULL CONSTRAINT [DF_UserLogins_IPAddress]  DEFAULT (''),
	[AccessDate] [datetime] NOT NULL CONSTRAINT [DF_UserLogins_AccessDate]  DEFAULT (getdate()),
	[Outcome] [tinyint] NOT NULL CONSTRAINT [DF_UserLogins_Outcome]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WhiteboardRemoteHostLog](
	[WhiteboardRemoteHostLogID] [int] IDENTITY(1,1) NOT NULL,
	[RemoteHostAddress] [nvarchar](20) NOT NULL,
	[RemoteHostName] [nvarchar](50) NOT NULL,
	[LastWhiteboardQuery] [nvarchar](255) NULL,
	[LastPASCode] [varchar](4) NULL,
	[LastWhiteboardConfigID] [int] NULL
) ON [PRIMARY]
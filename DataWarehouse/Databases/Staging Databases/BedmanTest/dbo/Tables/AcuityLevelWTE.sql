﻿CREATE TABLE [dbo].[AcuityLevelWTE](
	[LevelID] [int] IDENTITY(1,1) NOT NULL,
	[WardTypeID] [int] NOT NULL,
	[WTE_0] [decimal](8, 2) NOT NULL,
	[WTE_1a] [decimal](8, 2) NOT NULL,
	[WTE_1b] [decimal](8, 2) NOT NULL,
	[WTE_2] [decimal](8, 2) NOT NULL,
	[WTE_3] [decimal](8, 2) NOT NULL
) ON [PRIMARY]
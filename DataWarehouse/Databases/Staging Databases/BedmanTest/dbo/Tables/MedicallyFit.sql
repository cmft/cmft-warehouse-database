﻿CREATE TABLE [dbo].[MedicallyFit](
	[PatID] [int] IDENTITY(1,1) NOT NULL,
	[MedicallyFit] [bit] NOT NULL,
	[Patient] [varchar](20) NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LocationId] [int] NULL,
	[UpdatedBy] [varchar](12) NULL,
	[IPAddress] [varchar](20) NULL,
	[EpisodeID] [int] NULL
) ON [PRIMARY]
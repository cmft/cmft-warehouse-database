﻿CREATE TABLE [dbo].[HandoverAudit](
	[HandoverAuditID] [int] IDENTITY(1,1) NOT NULL,
	[HandoverID] [int] NULL,
	[EpisodeID] [int] NULL,
	[KnownProblems] [text] NULL,
	[CurrentProblems] [text] NULL,
	[ManagementPlan] [text] NULL,
	[UserID] [int] NULL,
	[Entered] [datetime] NULL CONSTRAINT [DF_HandoverAudit_Entered]  DEFAULT (getdate()),
	[Priority] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
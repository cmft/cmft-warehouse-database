﻿CREATE TABLE [dbo].[ClinicalIndicators](
	[ClinIndID] [int] IDENTITY(1,1) NOT NULL,
	[ClinIndDesc] [varchar](30) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
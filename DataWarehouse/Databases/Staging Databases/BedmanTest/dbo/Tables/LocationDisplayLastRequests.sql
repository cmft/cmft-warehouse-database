﻿CREATE TABLE [dbo].[LocationDisplayLastRequests](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [varchar](20) NOT NULL,
	[LocationRequestedID] [int] NOT NULL,
	[LastRequested] [datetime] NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
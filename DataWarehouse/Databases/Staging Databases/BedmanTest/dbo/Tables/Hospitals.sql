﻿CREATE TABLE [dbo].[Hospitals](
	[HospCode] [char](4) NOT NULL,
	[Description] [varchar](40) NOT NULL,
	[Include] [tinyint] NOT NULL CONSTRAINT [DF_Hospitals_Include]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[HandoverNotes](
	[HandoverID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NULL,
	[KnownProblems] [text] NULL,
	[CurrentProblems] [text] NULL,
	[ManagementPlan] [text] NULL,
	[UserID] [int] NULL,
	[Entered] [datetime] NULL CONSTRAINT [DF_HandoverNotes_Entered]  DEFAULT (getdate()),
	[Priority] [varchar](6) NULL,
	[LocationID] [int] NULL,
	[OldId] [bit] NULL CONSTRAINT [DF_HandoverNotes_OldId]  DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
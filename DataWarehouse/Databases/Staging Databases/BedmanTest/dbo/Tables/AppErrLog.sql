﻿CREATE TABLE [dbo].[AppErrLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorTime] [datetime] NOT NULL CONSTRAINT [DF_AppErrLog_ErrorTime]  DEFAULT (getdate()),
	[Error] [int] NOT NULL CONSTRAINT [DF_AppErrLog_Error]  DEFAULT (0),
	[Description] [varchar](255) NULL,
	[PCID] [varchar](15) NULL,
	[UserID] [int] NOT NULL CONSTRAINT [DF_AppErrLog_UserID]  DEFAULT (0),
	[ErrModule] [varchar](50) NULL,
	[ErrorAction] [int] NOT NULL CONSTRAINT [DF_AppErrLog_ErrorAction]  DEFAULT (0),
	[Version] [varchar](12) NULL
) ON [PRIMARY]
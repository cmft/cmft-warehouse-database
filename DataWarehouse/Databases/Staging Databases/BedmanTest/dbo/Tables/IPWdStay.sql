﻿CREATE TABLE [dbo].[IPWdStay](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient] [varchar](8) NOT NULL,
	[Episode] [varchar](5) NOT NULL,
	[WdStayStartDate] [datetime] NOT NULL,
	[Ward] [char](4) NOT NULL,
	[WdStayEndDate] [datetime] NULL,
	[WdLoS] [int] NULL
) ON [PRIMARY]
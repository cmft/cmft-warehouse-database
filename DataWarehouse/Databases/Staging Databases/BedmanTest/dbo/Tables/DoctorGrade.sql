﻿CREATE TABLE [dbo].[DoctorGrade](
	[DoctorGradeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](150) NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Activated] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DailyAcuityScore](
	[DailyScoreID] [int] IDENTITY(1,1) NOT NULL,
	[wardCode] [char](4) NOT NULL,
	[ScoresDate] [datetime] NOT NULL,
	[ApprovedBy] [varchar](20) NULL,
	[ApprovedAt] [datetime] NULL CONSTRAINT [DF_DAS_ApprovedAt]  DEFAULT (getdate()),
	[Total_0] [int] NOT NULL,
	[Total_1a] [int] NOT NULL,
	[Total_1b] [int] NOT NULL,
	[Total_2] [int] NOT NULL,
	[Total_3] [int] NOT NULL,
	[EscortHours] [decimal](10, 2) NOT NULL,
	[EscortEpisodes] [int] NOT NULL CONSTRAINT [DF_DAS_EscortEpisodes]  DEFAULT (0),
	[OpenBeds] [int] NOT NULL CONSTRAINT [DF__DAS__OpenBeds]  DEFAULT (0),
	[FundedBeds] [int] NULL,
	[Outliers] [int] NULL
) ON [PRIMARY]
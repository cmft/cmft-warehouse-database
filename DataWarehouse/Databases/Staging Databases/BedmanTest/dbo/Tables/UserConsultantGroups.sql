﻿CREATE TABLE [dbo].[UserConsultantGroups](
	[UserID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[Permission] [smallint] NOT NULL
) ON [PRIMARY]
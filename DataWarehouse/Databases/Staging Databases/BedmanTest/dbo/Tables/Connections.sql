﻿CREATE TABLE [dbo].[Connections](
	[ConnectionID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [varchar](15) NOT NULL,
	[LocationID] [int] NOT NULL,
	[ConsultantID] [int] NOT NULL CONSTRAINT [DF_Connections_ConsultantID]  DEFAULT (0),
	[GroupID] [int] NOT NULL CONSTRAINT [DF_Connections_GroupID]  DEFAULT (0),
	[UserID] [int] NOT NULL,
	[ConnectionTime] [datetime] NOT NULL CONSTRAINT [DF_Connection_Connection12__15]  DEFAULT (getdate())
) ON [PRIMARY]
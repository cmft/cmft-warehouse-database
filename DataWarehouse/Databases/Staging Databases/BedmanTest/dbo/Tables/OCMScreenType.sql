﻿CREATE TABLE [dbo].[OCMScreenType](
	[ScreenType] [varchar](4) NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[ServiceGroup] [int] NOT NULL CONSTRAINT [DF_OCMScreenType_Group]  DEFAULT (0)
) ON [PRIMARY]
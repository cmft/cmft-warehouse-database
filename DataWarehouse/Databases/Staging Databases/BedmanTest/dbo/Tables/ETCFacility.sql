﻿CREATE TABLE [dbo].[ETCFacility](
	[ID] [int] NOT NULL,
	[BedCount] [smallint] NOT NULL,
	[TrolleyCount] [smallint] NOT NULL
) ON [PRIMARY]
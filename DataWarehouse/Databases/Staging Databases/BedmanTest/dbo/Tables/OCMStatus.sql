﻿CREATE TABLE [dbo].[OCMStatus](
	[Code] [int] NOT NULL CONSTRAINT [DF_OCMStatus_Code]  DEFAULT (0),
	[Description] [varchar](30) NOT NULL,
	[StatusGroup] [int] NOT NULL CONSTRAINT [DF_OCMStatus_StatusGroup]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TemplateLocations](
	[TemplateID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Permission] [smallint] NOT NULL CONSTRAINT [DF_TemplateLocations_Permission]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AESnapshot](
	[SnapshotDate] [datetime] NOT NULL,
	[Hospital] [char](4) NOT NULL,
	[Tot0] [int] NOT NULL CONSTRAINT [DF_AESnapshot_Tot0]  DEFAULT (0),
	[Tot1] [int] NOT NULL CONSTRAINT [DF_AESnapshot_Tot1]  DEFAULT (0),
	[Tot2] [int] NOT NULL CONSTRAINT [DF_AESnapshot_Tot2]  DEFAULT (0),
	[Tot3] [int] NOT NULL CONSTRAINT [DF_AESnapshot_Tot3]  DEFAULT (0),
	[Tot4] [int] NOT NULL CONSTRAINT [DF_AESnapshot_Tot4]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Templates](
	[TemplateID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[AllLoc] [int] NULL,
	[AllLocPerms] [smallint] NULL CONSTRAINT [DF_Templates_AllLocPerms]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TieFileArc](
	[FileID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](50) NULL,
	[ArchDate] [datetime] NOT NULL,
	[Fstatus] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[HL7Outbound](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BatchID] [varchar](10) NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[EVN] [varchar](5) NOT NULL,
	[HL7Message] [varchar](max) NOT NULL,
	[AENumber] [varchar](14) NULL,
	[PatientSurname] [varchar](30) NULL,
	[ProcessedTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
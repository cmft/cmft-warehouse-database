﻿CREATE TABLE [dbo].[OCMServices](
	[ServiceCode] [int] NOT NULL CONSTRAINT [DF_OCMServices_ServicCode]  DEFAULT (0),
	[Description] [varchar](30) NOT NULL,
	[ScreenType] [varchar](4) NOT NULL,
	[active] [bit] NULL DEFAULT (1)
) ON [PRIMARY]
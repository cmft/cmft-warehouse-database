﻿CREATE TABLE [dbo].[ClinicalDisplayTriggers](
	[ID] [int] NOT NULL,
	[ClinicalProcessID] [int] NOT NULL,
	[Default] [time](7) NOT NULL,
	[TimeShort] [time](7) NOT NULL,
	[BreachImminent] [time](7) NOT NULL,
	[LocationID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PressureUlcerStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_PressureUlcerStatus_Active]  DEFAULT ((1)),
	[Order] [int] NOT NULL CONSTRAINT [DF_PressureUlcerStatus_Order]  DEFAULT ((0))
) ON [PRIMARY]
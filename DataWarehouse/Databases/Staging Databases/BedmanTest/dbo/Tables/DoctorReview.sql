﻿CREATE TABLE [dbo].[DoctorReview](
	[DoctorReviewID] [int] IDENTITY(1,1) NOT NULL,
	[HospSpellID] [int] NOT NULL,
	[DoctorGradeID] [int] NOT NULL,
	[ReviewedAt] [datetime2](0) NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DayStartTime] [datetime2](0) NOT NULL,
	[WarnTime] [datetime2](0) NOT NULL,
	[BreachTime] [datetime2](0) NOT NULL
) ON [PRIMARY]
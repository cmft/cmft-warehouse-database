﻿CREATE TABLE [dbo].[TblPatientAudit](
	[PatAudID] [int] IDENTITY(1,1) NOT NULL,
	[LastUpdated] [datetime] NULL,
	[Patient] [varchar](50) NULL,
	[FacilityID] [int] NULL,
	[LocationID] [int] NULL,
	[Status] [smallint] NULL,
	[OriginalWard] [char](10) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IPAddress] [varchar](50) NULL,
	[TransferComplete] [bit] NULL
) ON [PRIMARY]
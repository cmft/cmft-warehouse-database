﻿CREATE TABLE [dbo].[BedReason](
	[ReasonID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[Active] [tinyint] NOT NULL CONSTRAINT [DF_BedReason_Active]  DEFAULT (1),
	[Status] [tinyint] NOT NULL CONSTRAINT [DF_BedReason_Status]  DEFAULT (0)
) ON [PRIMARY]
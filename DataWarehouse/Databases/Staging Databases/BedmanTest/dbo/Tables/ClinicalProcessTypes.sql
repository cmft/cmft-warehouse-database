﻿CREATE TABLE [dbo].[ClinicalProcessTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ContactType] [varchar](20) NOT NULL
) ON [PRIMARY]
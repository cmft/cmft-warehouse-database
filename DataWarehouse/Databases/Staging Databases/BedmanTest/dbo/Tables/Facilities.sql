﻿CREATE TABLE [dbo].[Facilities](
	[FacilityID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[FacilityCode] [varchar](5) NOT NULL,
	[FacilityTypeID] [int] NOT NULL,
	[Capacity] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Description] [varchar](30) NULL,
	[Status] [tinyint] NOT NULL CONSTRAINT [DF_Facilities_Status]  DEFAULT (1),
	[ClosureReason] [int] NULL CONSTRAINT [DF_Facilities_ClosureReason]  DEFAULT (0),
	[ReopenTime] [datetime] NULL,
	[SCreenColour] [tinyint] NOT NULL CONSTRAINT [DF_Facilities_SCreenColour]  DEFAULT (0),
	[Tactive] [bit] NULL,
	[Ttime] [tinyint] NULL,
	[Ordersort] [tinyint] NULL,
	[TCI] [bit] NULL,
	[BayID] [int] NULL
) ON [PRIMARY]
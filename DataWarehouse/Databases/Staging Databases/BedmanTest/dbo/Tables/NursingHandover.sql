﻿CREATE TABLE [dbo].[NursingHandover](
	[NursingHandoverID] [int] IDENTITY(1,1) NOT NULL,
	[Patient] [varchar](15) NOT NULL,
	[HospSpellID] [int] NOT NULL,
	[Entered] [datetime] NOT NULL,
	[LocationID] [int] NULL,
	[FacilityID] [int] NULL,
	[HandoverDetails] [xml] NULL,
	[UserID] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserRecent](
	[UserID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[DateUsed] [datetime] NOT NULL CONSTRAINT [DF_UserRecent_DateUsed]  DEFAULT (getdate())
) ON [PRIMARY]
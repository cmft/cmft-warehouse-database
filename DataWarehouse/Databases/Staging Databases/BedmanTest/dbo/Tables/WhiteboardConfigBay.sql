﻿CREATE TABLE [dbo].[WhiteboardConfigBay](
	[WhiteboardConfigBayID] [int] IDENTITY(1,1) NOT NULL,
	[WhiteboardConfigID] [int] NOT NULL,
	[BayID] [int] NOT NULL,
	[Order] [int] NOT NULL CONSTRAINT [DF__Whiteboar__Order__6B84DD35]  DEFAULT ((0))
) ON [PRIMARY]
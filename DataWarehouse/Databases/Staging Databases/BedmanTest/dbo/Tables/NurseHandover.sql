﻿CREATE TABLE [dbo].[NurseHandover](
	[NursingID] [int] IDENTITY(1,1) NOT NULL,
	[Patient] [varchar](20) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[Entered] [datetime] NOT NULL,
	[LocationId] [int] NULL,
	[FacilityId] [int] NULL,
	[PatInfo] [text] NULL,
	[PlanN] [text] NULL,
	[CareIssu] [text] NULL,
	[Social] [text] NULL,
	[UserID] [varchar](20) NULL,
	[OldId] [bit] NULL CONSTRAINT [DF_NurseHandover_OldId]  DEFAULT (0),
	[Episode] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[FacilityTypes](
	[FacilityTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
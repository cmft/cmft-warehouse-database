﻿CREATE TABLE [dbo].[AuditDetails](
	[AuditID] [int] IDENTITY(1,1) NOT NULL,
	[SpellID] [int] NULL,
	[aTable] [nvarchar](50) NULL,
	[aColumn] [nvarchar](50) NULL,
	[OldValue] [nvarchar](1000) NULL,
	[NewValue] [nvarchar](1000) NULL,
	[UserID] [int] NULL,
	[DateChanged] [datetime] NULL
) ON [PRIMARY]
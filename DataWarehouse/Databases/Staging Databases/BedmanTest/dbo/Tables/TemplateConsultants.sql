﻿CREATE TABLE [dbo].[TemplateConsultants](
	[TemplateID] [int] NOT NULL,
	[ConsultantID] [int] NOT NULL,
	[Permission] [smallint] NOT NULL
) ON [PRIMARY]
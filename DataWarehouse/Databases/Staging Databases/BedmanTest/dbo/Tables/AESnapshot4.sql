﻿CREATE TABLE [dbo].[AESnapshot4](
	[TimePeriod] [int] NOT NULL,
	[Hospital] [char](4) NOT NULL,
	[Arrived] [int] NULL,
	[Discharged] [int] NULL,
	[TotalCurrent] [int] NULL,
	[Current4Hr] [int] NULL,
	[Disch4hr] [int] NULL,
	[TotalForDay] [int] NULL,
	[DayTotalCount] [int] NULL,
	[Day4hrCount] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [archive].[AETrackingStep](
	[ID] [int] NOT NULL,
	[AttendanceDt] [datetime] NOT NULL,
	[TrackingStep] [char](4) NOT NULL,
	[Comment] [varchar](30) NULL,
	[Nurse] [char](8) NULL,
	[TSDate] [datetime] NOT NULL,
	[Priority] [char](4) NULL,
	[Patient] [varchar](8) NULL,
	[Episode] [varchar](5) NULL,
	[Doctor] [varchar](8) NULL
) ON [PRIMARY]
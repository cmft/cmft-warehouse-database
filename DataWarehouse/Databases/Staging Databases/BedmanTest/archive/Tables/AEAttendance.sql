﻿CREATE TABLE [archive].[AEAttendance](
	[ID] [int] NOT NULL,
	[AENo] [varchar](14) NOT NULL,
	[LastDownload] [datetime] NOT NULL CONSTRAINT [DF_ArcAEAttendan_LastDownloa2__12]  DEFAULT (getdate()),
	[AttendanceDt] [datetime] NOT NULL,
	[DepartDt] [datetime] NULL,
	[Hospital] [varchar](4) NOT NULL,
	[Patient] [varchar](8) NULL,
	[Episode] [varchar](5) NULL,
	[DischargeTime] [datetime] NULL,
	[AELoc] [int] NOT NULL CONSTRAINT [DF_ArcAEAttendance_AELoc]  DEFAULT (0),
	[LastOperation] [varchar](20) NULL,
	[LastTransactionID] [int] NULL
) ON [PRIMARY]
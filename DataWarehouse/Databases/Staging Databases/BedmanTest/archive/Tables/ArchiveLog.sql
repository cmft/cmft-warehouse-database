﻿CREATE TABLE [archive].[ArchiveLog](
	[ArchLogID] [int] IDENTITY(1,1) NOT NULL,
	[ArchDate] [datetime] NULL,
	[ArchTable] [varchar](20) NOT NULL,
	[ArchProcess] [varchar](10) NULL,
	[ArchStart] [datetime] NOT NULL,
	[ArchFinish] [datetime] NULL,
	[NoAffected] [numeric](18, 0) NULL,
	[Duration] [varchar](10) NULL
) ON [PRIMARY]
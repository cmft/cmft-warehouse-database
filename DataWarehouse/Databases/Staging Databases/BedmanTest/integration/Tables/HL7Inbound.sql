﻿CREATE TABLE [integration].[HL7Inbound](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[Payload] [varchar](max) NOT NULL,
	[InsertedTimeStamp] [datetime] NOT NULL,
	[ProcessedTimeStamp] [datetime] NULL,
	[ProcessingNotes] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [integration].[MirthLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BedReqID] [int] NOT NULL,
	[EVN] [varchar](10) NOT NULL,
	[AENumber] [varchar](15) NOT NULL,
	[MsgTime] [datetime] NOT NULL,
	[Parameters] [xml] NOT NULL,
	[Log_TS] [datetime] NOT NULL CONSTRAINT [DF__MirthLog__Log_TS__3EB236BE]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[request](
	[HospitalNo] [varchar](100) NULL,
	[RisformNo] [varchar](100) NULL,
	[PregnancyIndicator] [varchar](100) NULL,
	[ProcedureDate] [varchar](100) NULL,
	[PatientType] [varchar](100) NULL,
	[PatientCategory] [varchar](100) NULL,
	[ReferralPoint] [varchar](100) NULL,
	[Consultant] [varchar](100) NULL,
	[RequestingDoctor] [varchar](100) NULL,
	[Transport] [varchar](100) NULL,
	[ArchiveNoforthisRequest] [varchar](100) NULL,
	[HospitalName] [varchar](100) NULL,
	[volume] [varchar](3) NULL
) ON [PRIMARY]
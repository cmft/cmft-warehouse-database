﻿CREATE TABLE [dbo].[clinicalData](
	[HospitalNo] [varchar](100) NULL,
	[RisformNo] [varchar](100) NULL,
	[ClinicalData] [varchar](300) NULL,
	[RadiologistComment] [varchar](100) NULL,
	[volume] [varchar](3) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[folderLocation](
	[HospitalNo] [varchar](100) NULL,
	[RisformNo] [varchar](100) NULL,
	[FileArchiveNumberHistory1] [varchar](max) NULL,
	[LoanedFilmsForDept1] [varchar](max) NULL,
	[FileArchiveNumberHistory2] [varchar](max) NULL,
	[LoanedFilmsForDept2] [varchar](max) NULL,
	[FileArchiveNumberHistory3] [varchar](max) NULL,
	[LoanedFilmsForDept3] [varchar](max) NULL,
	[FileArchiveNumberHistory4] [varchar](max) NULL,
	[LoanedFilmsForDept4] [varchar](max) NULL,
	[FileArchiveNumberHistory5] [varchar](max) NULL,
	[LoanedFilmsForDept5] [varchar](max) NULL,
	[FileArchiveNumberHistory6] [varchar](max) NULL,
	[LoanedFilmsForDept6] [varchar](max) NULL,
	[volume] [varchar](3) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
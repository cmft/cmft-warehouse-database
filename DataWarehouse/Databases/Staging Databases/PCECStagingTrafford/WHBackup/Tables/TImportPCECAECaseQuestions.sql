﻿CREATE TABLE [WHBackup].[TImportPCECAECaseQuestions](
	[Location] [varchar](max) NULL,
	[CaseNo] [varchar](max) NULL,
	[ActiveTime] [varchar](max) NULL,
	[CaseType] [varchar](max) NULL,
	[QuestionSet] [varchar](max) NULL,
	[SequenceNo] [varchar](max) NULL,
	[Question] [varchar](max) NULL,
	[Answer] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
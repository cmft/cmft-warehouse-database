﻿CREATE TABLE [WHBackup].[TImportPCECAERelationshipToCaller](
	[Location] [varchar](max) NULL,
	[CaseNo] [varchar](max) NULL,
	[ActiveTime] [varchar](max) NULL,
	[CaseType] [varchar](max) NULL,
	[RelationshipToCaller] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[EntryToFirstConsult_Archive](
	[CaseNo] [varchar](50) NULL,
	[EntryDate] [datetime] NULL,
	[ConsultStart] [datetime] NULL,
	[ConsultEnd] [datetime] NULL,
	[TimeTaken] [int] NULL,
	[StartConsultType] [varchar](50) NULL,
	[EndConsultType] [varchar](50) NULL,
	[DerivedFromSummary] [bit] NULL,
	[Created] [datetime] NULL CONSTRAINT [DF_dboEntryToFirstConsult_ArchiveCreated]  DEFAULT (getdate()),
	[ByWhom] [varchar](50) NULL CONSTRAINT [DF_dboEntryToFirstConsult_ArchiveByWhom]  DEFAULT (suser_name())
) ON [PRIMARY]
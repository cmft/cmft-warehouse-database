﻿CREATE TABLE [dbo].[CaseQuestionSet](
	[Location] [nvarchar](255) NULL,
	[CaseNo] [int] NULL,
	[CaseActive] [datetime] NULL,
	[CaseType] [nvarchar](255) NULL,
	[QuestionSetName] [nvarchar](max) NULL,
	[Sort] [int] NULL,
	[QuestionText] [nvarchar](max) NULL,
	[AnswerText] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
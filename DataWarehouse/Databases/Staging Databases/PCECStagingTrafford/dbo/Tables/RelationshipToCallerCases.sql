﻿CREATE TABLE [dbo].[RelationshipToCallerCases](
	[Location] [nvarchar](255) NULL,
	[caseNo] [int] NULL,
	[ActiveDate] [datetime] NULL,
	[CaseType] [nvarchar](255) NULL,
	[RTC] [nvarchar](255) NULL
) ON [PRIMARY]
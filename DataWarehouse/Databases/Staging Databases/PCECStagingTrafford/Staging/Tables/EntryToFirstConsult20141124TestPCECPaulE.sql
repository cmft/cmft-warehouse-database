﻿CREATE TABLE [Staging].[EntryToFirstConsult20141124TestPCECPaulE](
	[Location] [nvarchar](255) NULL,
	[Provider] [nvarchar](255) NULL,
	[CaseNo] [varchar](max) NULL,
	[EntryDate] [datetime] NULL,
	[ConsultStart] [datetime] NULL,
	[ConsultEnd] [datetime] NULL,
	[TimeTaken] [int] NULL,
	[StartConsultType] [nvarchar](255) NULL,
	[EndConsultType] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
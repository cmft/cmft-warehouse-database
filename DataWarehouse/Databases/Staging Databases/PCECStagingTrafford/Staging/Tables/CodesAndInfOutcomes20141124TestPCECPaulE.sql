﻿CREATE TABLE [Staging].[CodesAndInfOutcomes20141124TestPCECPaulE](
	[Location] [nvarchar](255) NULL,
	[CaseRef] [nvarchar](255) NULL,
	[CaseNo] [varchar](max) NULL,
	[Age] [nvarchar](255) NULL,
	[DOB] [datetime] NULL,
	[CaseType] [nvarchar](255) NULL,
	[DateOfCall] [nvarchar](255) NULL,
	[DayOfWeek] [nvarchar](255) NULL,
	[TImeOfCall] [nvarchar](255) NULL,
	[Postcode] [nvarchar](255) NULL,
	[Codes] [nvarchar](255) NULL,
	[InformationalOutcomes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
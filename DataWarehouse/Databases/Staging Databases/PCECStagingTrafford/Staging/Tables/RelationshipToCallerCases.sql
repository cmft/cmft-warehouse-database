﻿CREATE TABLE [Staging].[RelationshipToCallerCases](
	[Location] [nvarchar](255) NULL,
	[caseNo] [varchar](max) NULL,
	[ActiveDate] [datetime] NULL,
	[CaseType] [nvarchar](255) NULL,
	[RTC] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
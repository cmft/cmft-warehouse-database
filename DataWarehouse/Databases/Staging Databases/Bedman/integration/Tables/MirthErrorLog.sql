﻿CREATE TABLE [integration].[MirthErrorLog](
	[ErrorID] [int] IDENTITY(1,1) NOT NULL,
	[EVN] [varchar](10) NULL,
	[AENumber] [varchar](14) NULL,
	[ErrorMsg] [varchar](255) NULL,
	[MsgTime] [datetime] NULL,
	[Parameters] [xml] NULL,
	[Error_TS] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[TblStatusAudit](
	[PatAudID] [int] NOT NULL,
	[LastUpdated] [datetime] NULL,
	[Patient] [varchar](50) NULL,
	[LocationId] [int] NULL,
	[Status] [smallint] NULL,
	[FacilityID] [int] NULL,
	[OriginalWard] [char](10) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[IPAddress] [varchar](50) NULL
) ON [PRIMARY]
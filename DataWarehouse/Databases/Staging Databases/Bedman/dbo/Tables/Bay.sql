﻿CREATE TABLE [dbo].[Bay](
	[BayID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
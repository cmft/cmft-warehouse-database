﻿CREATE TABLE [dbo].[OCMServices](
	[ServiceCode] [int] NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[ScreenType] [varchar](4) NOT NULL,
	[active] [bit] NULL
) ON [PRIMARY]
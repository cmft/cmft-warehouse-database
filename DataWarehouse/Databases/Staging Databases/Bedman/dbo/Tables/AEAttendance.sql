﻿CREATE TABLE [dbo].[AEAttendance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AENo] [varchar](14) NOT NULL,
	[LastDownload] [datetime] NOT NULL,
	[AttendanceDt] [datetime] NOT NULL,
	[DepartDt] [datetime] NULL,
	[Hospital] [varchar](4) NOT NULL,
	[Patient] [varchar](8) NULL,
	[Episode] [varchar](5) NULL,
	[DischargeTime] [datetime] NULL,
	[AELoc] [int] NOT NULL,
	[PVisitNO] [varchar](30) NULL,
	[ForTraining] [tinyint] NULL,
	[LastOperation] [varchar](20) NULL,
	[LastTransactionID] [int] NULL
) ON [PRIMARY]
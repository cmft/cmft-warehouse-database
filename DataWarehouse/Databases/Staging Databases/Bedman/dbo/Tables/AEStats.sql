﻿CREATE TABLE [dbo].[AEStats](
	[Year] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Day] [int] NOT NULL,
	[Hospital] [char](4) NOT NULL,
	[Total] [int] NOT NULL,
	[4hr] [int] NOT NULL,
	[Mean] [int] NOT NULL,
	[Incpt] [int] NOT NULL,
	[AEDate] [char](8) NULL
) ON [PRIMARY]
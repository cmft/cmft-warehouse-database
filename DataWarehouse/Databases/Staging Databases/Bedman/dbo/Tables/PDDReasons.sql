﻿CREATE TABLE [dbo].[PDDReasons](
	[PDDReasonID] [int] IDENTITY(1,1) NOT NULL,
	[PDDGroup] [int] NOT NULL,
	[PDDReason] [varchar](30) NOT NULL,
	[Active] [tinyint] NOT NULL
) ON [PRIMARY]
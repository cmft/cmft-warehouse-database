﻿CREATE TABLE [dbo].[TemplateConGroups](
	[TemplateID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[Permission] [smallint] NOT NULL
) ON [PRIMARY]
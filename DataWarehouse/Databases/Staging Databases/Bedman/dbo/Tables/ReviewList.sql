﻿CREATE TABLE [dbo].[ReviewList](
	[ReviewListId] [int] IDENTITY(1,1) NOT NULL,
	[ReviewDesc] [varchar](50) NOT NULL,
	[ReviewActive] [bit] NOT NULL
) ON [PRIMARY]
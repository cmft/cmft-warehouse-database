﻿CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](20) NOT NULL,
	[Password] [varchar](12) NOT NULL,
	[ExpiryDt] [datetime] NOT NULL,
	[GraceLogins] [int] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[Locked] [bit] NOT NULL,
	[Title] [varchar](6) NULL,
	[Forename] [varchar](20) NULL,
	[Surname] [varchar](24) NULL,
	[SuperUser] [bit] NOT NULL,
	[AllLocations] [int] NULL,
	[AllLocPermission] [smallint] NULL,
	[NeverExpires] [bit] NOT NULL,
	[Notify] [bit] NOT NULL,
	[LocationID] [int] NULL,
	[Authoriser] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WhiteboardConfigBay](
	[WhiteboardConfigBayID] [int] IDENTITY(1,1) NOT NULL,
	[WhiteboardConfigID] [int] NOT NULL,
	[BayID] [int] NOT NULL,
	[Order] [int] NOT NULL
) ON [PRIMARY]
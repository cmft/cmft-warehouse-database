﻿CREATE TABLE [dbo].[LocationTypes](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PressureUlcerCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL
) ON [PRIMARY]
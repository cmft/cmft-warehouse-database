﻿CREATE TABLE [dbo].[BedReason](
	[ReasonID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[Active] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL
) ON [PRIMARY]
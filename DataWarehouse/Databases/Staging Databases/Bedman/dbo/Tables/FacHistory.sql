﻿CREATE TABLE [dbo].[FacHistory](
	[FacHistID] [int] IDENTITY(1,1) NOT NULL,
	[FacilityID] [int] NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[TranUserID] [int] NOT NULL,
	[ActionTime] [datetime] NOT NULL,
	[FacAction] [tinyint] NOT NULL,
	[FacReason] [int] NOT NULL,
	[ExpReopen] [datetime] NULL
) ON [PRIMARY]
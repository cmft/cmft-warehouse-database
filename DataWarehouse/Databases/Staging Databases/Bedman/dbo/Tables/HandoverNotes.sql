﻿CREATE TABLE [dbo].[HandoverNotes](
	[HandoverID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NULL,
	[KnownProblems] [text] NULL,
	[CurrentProblems] [text] NULL,
	[ManagementPlan] [text] NULL,
	[UserID] [int] NULL,
	[Entered] [datetime] NULL,
	[Priority] [varchar](6) NULL,
	[LocationID] [int] NULL,
	[OldId] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
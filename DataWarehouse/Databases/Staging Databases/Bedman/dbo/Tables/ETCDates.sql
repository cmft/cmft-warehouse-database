﻿CREATE TABLE [dbo].[ETCDates](
	[ArrDateElement] [datetime] NOT NULL,
	[BedID] [int] NOT NULL,
	[Occupied] [bit] NOT NULL,
	[ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ETCExtraBeds](
	[ArrDateElement] [datetime] NOT NULL,
	[BedID] [int] NOT NULL,
	[Occupied] [bit] NOT NULL,
	[ID] [int] NOT NULL
) ON [PRIMARY]
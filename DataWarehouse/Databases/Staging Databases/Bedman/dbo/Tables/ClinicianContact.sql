﻿CREATE TABLE [dbo].[ClinicianContact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HospSpellID] [int] NOT NULL,
	[Clinician] [varchar](20) NOT NULL,
	[ClinicalProcessTypeID] [int] NOT NULL,
	[EnteredDateTime] [datetime] NOT NULL,
	[EnteredByGMC] [char](8) NOT NULL
) ON [PRIMARY]
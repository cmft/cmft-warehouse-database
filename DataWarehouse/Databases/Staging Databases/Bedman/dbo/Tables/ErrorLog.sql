﻿CREATE TABLE [dbo].[ErrorLog](
	[ErrorLogID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorLogDate] [datetime] NOT NULL,
	[Error] [int] NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[System] [int] NOT NULL,
	[Location] [varchar](50) NOT NULL,
	[Data] [varchar](255) NULL
) ON [PRIMARY]
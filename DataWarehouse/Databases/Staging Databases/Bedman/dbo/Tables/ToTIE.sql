﻿CREATE TABLE [dbo].[ToTIE](
	[SymID] [int] IDENTITY(1,1) NOT NULL,
	[BatchID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[FileName] [varchar](50) NULL,
	[Type] [varchar](10) NULL,
	[PVN] [varchar](30) NULL,
	[EnteredBy] [varchar](20) NULL,
	[Message] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
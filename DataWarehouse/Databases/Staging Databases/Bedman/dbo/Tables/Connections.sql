﻿CREATE TABLE [dbo].[Connections](
	[ConnectionID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [varchar](15) NOT NULL,
	[LocationID] [int] NOT NULL,
	[ConsultantID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[ConnectionTime] [datetime] NOT NULL
) ON [PRIMARY]
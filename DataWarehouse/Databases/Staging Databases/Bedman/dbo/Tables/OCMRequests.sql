﻿CREATE TABLE [dbo].[OCMRequests](
	[OSTkey] [int] NOT NULL,
	[Patient] [varchar](8) NOT NULL,
	[Episode] [varchar](5) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[StatusDate] [datetime] NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[Service] [int] NOT NULL,
	[Originator] [varchar](20) NULL,
	[LastOperation] [varchar](20) NULL,
	[LastTransactionID] [int] NULL
) ON [PRIMARY]
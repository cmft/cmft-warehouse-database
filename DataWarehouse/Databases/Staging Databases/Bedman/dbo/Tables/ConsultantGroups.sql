﻿CREATE TABLE [dbo].[ConsultantGroups](
	[GroupID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
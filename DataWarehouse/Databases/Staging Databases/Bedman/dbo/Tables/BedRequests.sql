﻿CREATE TABLE [dbo].[BedRequests](
	[BedReqID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[BRTime] [datetime] NOT NULL,
	[BRClinician] [varchar](30) NOT NULL,
	[BRUser] [int] NOT NULL,
	[BRComment] [varchar](40) NULL,
	[BRWard] [int] NULL,
	[BATime] [datetime] NULL,
	[BAWard] [int] NULL,
	[BAComment] [varchar](40) NULL,
	[BAvTime] [datetime] NULL,
	[BAvComment] [varchar](40) NULL,
	[BAUser] [int] NULL,
	[BAvUser] [int] NULL,
	[BRTimeAdded] [datetime] NULL,
	[BATimeAdded] [datetime] NULL,
	[BAvTimeAdded] [datetime] NULL,
	[BRSpecialtyID] [int] NOT NULL,
	[BRLocation] [int] NULL,
	[BRStatus] [tinyint] NULL,
	[ProposedSpecialty] [nvarchar](30) NULL
) ON [PRIMARY]
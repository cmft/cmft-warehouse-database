﻿CREATE TABLE [dbo].[AEDoctor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AEDocCode] [varchar](8) NOT NULL,
	[Initials] [varchar](4) NULL,
	[Surname] [varchar](30) NOT NULL
) ON [PRIMARY]
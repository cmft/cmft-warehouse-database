﻿CREATE TABLE [dbo].[OCMScreenType](
	[ScreenType] [varchar](4) NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[ServiceGroup] [int] NOT NULL
) ON [PRIMARY]
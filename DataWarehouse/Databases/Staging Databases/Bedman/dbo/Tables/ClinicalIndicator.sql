﻿CREATE TABLE [dbo].[ClinicalIndicator](
	[BedReqID] [int] NOT NULL,
	[ClinIndID] [int] NOT NULL,
	[Value] [bit] NOT NULL
) ON [PRIMARY]
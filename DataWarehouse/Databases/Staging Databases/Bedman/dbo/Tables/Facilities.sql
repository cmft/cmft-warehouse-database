﻿CREATE TABLE [dbo].[Facilities](
	[FacilityID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[FacilityCode] [varchar](5) NOT NULL,
	[FacilityTypeID] [int] NOT NULL,
	[Capacity] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Description] [varchar](30) NULL,
	[Status] [tinyint] NOT NULL,
	[ClosureReason] [int] NULL,
	[ReopenTime] [datetime] NULL,
	[SCreenColour] [tinyint] NOT NULL,
	[Tactive] [bit] NULL,
	[Ttime] [tinyint] NULL,
	[Ordersort] [tinyint] NULL,
	[TCI] [bit] NULL,
	[BayID] [int] NULL
) ON [PRIMARY]
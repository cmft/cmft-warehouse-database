﻿CREATE TABLE [dbo].[DualMonitorLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PCID] [varchar](50) NULL,
	[DualMonTop] [int] NULL,
	[DualMonLeft] [int] NULL,
	[LocationLoaded] [varchar](50) NULL,
	[ErrProcLine] [varchar](255) NULL,
	[DateLoaded] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WhiteBoardGridOrder](
	[wbGridOrderID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NULL,
	[wbGridLayoutID] [int] NULL,
	[GridOrder] [int] NULL,
	[GridPage] [int] NULL,
	[Fixed] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Templates](
	[TemplateID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[AllLoc] [int] NULL,
	[AllLocPerms] [smallint] NULL
) ON [PRIMARY]
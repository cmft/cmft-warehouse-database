﻿CREATE TABLE [dbo].[AEFormat](
	[FormatID] [int] IDENTITY(1,1) NOT NULL,
	[Patient] [varchar](8) NOT NULL,
	[Hospital] [varchar](4) NULL,
	[AEloc] [int] NULL,
	[PVisitNO] [varchar](20) NOT NULL,
	[Fortraining] [tinyint] NULL,
	[AEno] [varchar](14) NOT NULL,
	[UpdateTime] [datetime] NOT NULL
) ON [PRIMARY]
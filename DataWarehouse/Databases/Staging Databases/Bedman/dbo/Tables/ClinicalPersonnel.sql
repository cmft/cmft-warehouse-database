﻿CREATE TABLE [dbo].[ClinicalPersonnel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Consultant] [varchar](50) NULL,
	[Nurse] [varchar](50) NULL,
	[PPC] [varchar](50) NULL,
	[LocationID] [int] NOT NULL,
	[DateOnWard] [datetime] NULL
) ON [PRIMARY]
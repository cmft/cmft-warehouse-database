﻿CREATE TABLE [dbo].[InterestedConsultant](
	[InterestedConsultantID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NULL,
	[Consultant] [varchar](6) NULL
) ON [PRIMARY]
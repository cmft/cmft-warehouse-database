﻿CREATE TABLE [dbo].[SnapshotJ06](
	[Surname] [varchar](24) NOT NULL,
	[Forename] [varchar](20) NOT NULL,
	[Gender] [char](1) NULL,
	[DoB] [datetime] NULL,
	[HospitalNo] [varchar](14) NULL,
	[AttendanceDt] [datetime] NULL,
	[AdmitDate] [datetime] NULL,
	[CurrWard] [char](4) NULL,
	[DepartTime] [datetime] NULL,
	[DischTime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[InterestedConsultantAudit](
	[ICAuditID] [int] IDENTITY(1,1) NOT NULL,
	[InterestedConsultantID] [int] NULL,
	[EpisodeID] [int] NULL,
	[Consultant] [varchar](6) NULL,
	[UserID] [int] NULL,
	[Entered] [datetime] NULL,
	[Removed] [tinyint] NULL
) ON [PRIMARY]
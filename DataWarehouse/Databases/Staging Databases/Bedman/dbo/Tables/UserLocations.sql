﻿CREATE TABLE [dbo].[UserLocations](
	[UserID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Permission] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Specialties](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SpecCode] [varchar](4) NOT NULL,
	[MainSpec] [varchar](7) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[Korner] [varchar](4) NOT NULL,
	[Main] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[DrSpec] [varchar](5) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Hospitals](
	[HospCode] [char](4) NOT NULL,
	[Description] [varchar](40) NOT NULL,
	[Include] [tinyint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserRecent](
	[UserID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[DateUsed] [datetime] NOT NULL
) ON [PRIMARY]
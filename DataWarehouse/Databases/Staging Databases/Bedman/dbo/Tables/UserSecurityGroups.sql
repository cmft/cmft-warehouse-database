﻿CREATE TABLE [dbo].[UserSecurityGroups](
	[UserID] [int] NOT NULL,
	[TemplateID] [int] NOT NULL,
	[EffectiveDate] [datetime] NOT NULL
) ON [PRIMARY]
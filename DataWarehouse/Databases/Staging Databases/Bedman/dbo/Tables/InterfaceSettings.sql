﻿CREATE TABLE [dbo].[InterfaceSettings](
	[ID] [int] NOT NULL,
	[NextDownloadTime] [datetime] NOT NULL,
	[Status] [char](12) NOT NULL,
	[StatusTime] [datetime] NOT NULL,
	[DayEndTime] [datetime] NOT NULL,
	[ForceDayend] [tinyint] NOT NULL,
	[Frequency] [tinyint] NOT NULL,
	[ATLDate] [char](8) NOT NULL,
	[ATLNo] [char](6) NOT NULL,
	[TLDate] [char](8) NOT NULL,
	[TLNo] [char](6) NOT NULL,
	[OCMDate] [char](8) NOT NULL,
	[OCMNo] [char](6) NOT NULL,
	[Throttle] [tinyint] NOT NULL
) ON [PRIMARY]
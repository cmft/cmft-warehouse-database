﻿CREATE TABLE [dbo].[UserLoginOutcomes](
	[OutcomeID] [tinyint] NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[OutcomeType] [tinyint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AppErrLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorTime] [datetime] NOT NULL,
	[Error] [int] NOT NULL,
	[Description] [varchar](255) NULL,
	[PCID] [varchar](15) NULL,
	[UserID] [int] NOT NULL,
	[ErrModule] [varchar](50) NULL,
	[ErrorAction] [int] NOT NULL,
	[Version] [varchar](12) NULL
) ON [PRIMARY]
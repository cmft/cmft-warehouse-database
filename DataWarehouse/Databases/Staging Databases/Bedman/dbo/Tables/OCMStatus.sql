﻿CREATE TABLE [dbo].[OCMStatus](
	[Code] [int] NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[StatusGroup] [int] NOT NULL
) ON [PRIMARY]
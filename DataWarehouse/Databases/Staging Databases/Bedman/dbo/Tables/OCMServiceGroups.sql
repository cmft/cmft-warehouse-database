﻿CREATE TABLE [dbo].[OCMServiceGroups](
	[Service] [int] NOT NULL,
	[Location] [int] NOT NULL,
	[ServiceGroup] [tinyint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TieInterfaceSettings](
	[IntID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Frequency] [tinyint] NOT NULL,
	[StatusTime] [datetime] NULL,
	[LastDownloadTime] [datetime] NULL,
	[NextDownloadTime] [datetime] NULL,
	[Status] [varchar](20) NULL,
	[Transfer] [tinyint] NULL,
	[ArchiveTime] [smallint] NULL
) ON [PRIMARY]
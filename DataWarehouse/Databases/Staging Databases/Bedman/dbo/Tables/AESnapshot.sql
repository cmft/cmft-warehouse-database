﻿CREATE TABLE [dbo].[AESnapshot](
	[SnapshotDate] [datetime] NOT NULL,
	[Hospital] [char](4) NOT NULL,
	[Tot0] [int] NOT NULL,
	[Tot1] [int] NOT NULL,
	[Tot2] [int] NOT NULL,
	[Tot3] [int] NOT NULL,
	[Tot4] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TieDownloadHistory](
	[TieID] [int] IDENTITY(1,1) NOT NULL,
	[DownloadDate] [datetime] NOT NULL,
	[FilesNo] [tinyint] NULL
) ON [PRIMARY]
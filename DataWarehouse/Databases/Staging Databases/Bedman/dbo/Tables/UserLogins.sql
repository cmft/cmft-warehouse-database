﻿CREATE TABLE [dbo].[UserLogins](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](12) NOT NULL,
	[IPAddress] [char](15) NOT NULL,
	[AccessDate] [datetime] NOT NULL,
	[Outcome] [tinyint] NOT NULL
) ON [PRIMARY]
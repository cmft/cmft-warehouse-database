﻿CREATE TABLE [dbo].[TieErrorLog](
	[ErrorID] [int] IDENTITY(1,1) NOT NULL,
	[Filename] [varchar](50) NULL,
	[Description] [varchar](255) NULL,
	[UpdateTime] [datetime] NOT NULL,
	[EVN] [varchar](10) NULL,
	[HL7Text] [text] NULL,
	[Error] [int] NULL,
	[Location] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[AESnapM](
	[Period] [int] IDENTITY(1,1) NOT NULL,
	[TimePeriod] [datetime] NOT NULL,
	[Hospital] [char](4) NULL,
	[Arrived] [int] NULL,
	[Current4Hr] [int] NULL,
	[Total4hr] [int] NULL,
	[Discharge24hr] [int] NULL
) ON [PRIMARY]
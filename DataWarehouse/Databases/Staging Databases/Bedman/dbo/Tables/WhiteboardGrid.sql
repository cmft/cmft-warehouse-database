﻿CREATE TABLE [dbo].[WhiteboardGrid](
	[wbGridLayoutID] [int] IDENTITY(1,1) NOT NULL,
	[HeaderText] [nvarchar](50) NULL,
	[DataField] [nvarchar](50) NULL,
	[Width] [int] NULL,
	[EditType] [nvarchar](50) NULL,
	[IsXMLField] [bit] NULL,
	[EditTypeSubset] [nvarchar](50) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
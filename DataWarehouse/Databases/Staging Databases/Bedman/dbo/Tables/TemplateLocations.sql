﻿CREATE TABLE [dbo].[TemplateLocations](
	[TemplateID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Permission] [smallint] NOT NULL
) ON [PRIMARY]
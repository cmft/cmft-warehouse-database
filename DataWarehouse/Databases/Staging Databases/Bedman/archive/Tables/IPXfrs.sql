﻿CREATE TABLE [archive].[IPXfrs](
	[ID] [int] NOT NULL,
	[Patient] [varchar](8) NOT NULL,
	[Episode] [varchar](5) NOT NULL,
	[XfrDate] [datetime] NOT NULL,
	[XfrCons] [char](6) NOT NULL,
	[XfrSpec] [char](4) NOT NULL,
	[XfrWard] [char](4) NOT NULL,
	[XfrHosp] [char](4) NOT NULL,
	[XfrLoc] [int] NOT NULL
) ON [PRIMARY]
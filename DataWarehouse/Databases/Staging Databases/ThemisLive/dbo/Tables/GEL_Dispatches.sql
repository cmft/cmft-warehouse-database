﻿CREATE TABLE [dbo].[GEL_Dispatches] (
    [DispatchId]    INT          NOT NULL,
    [Version]       INT          NOT NULL,
    [IsPrimary]     BIT          NOT NULL,
    [RackingMethod] INT          NOT NULL,
    [ConsignmentNo] VARCHAR (50) NOT NULL,
    [DispatchDate]  DATETIME     NULL,
    [IsInterim]     BIT          NOT NULL,
    [EnteredBy]     INT          NOT NULL,
    [EnteredOn]     DATETIME     NULL
);


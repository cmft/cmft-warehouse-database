﻿CREATE TABLE [dbo].[GEL_ActionTypes] (
    [ActionTypeId]  INT          NOT NULL,
    [Version]       INT          NOT NULL,
    [Code]          VARCHAR (15) NOT NULL,
    [Name]          VARCHAR (50) NOT NULL,
    [RequiresStage] INT          NOT NULL,
    [CausesStage]   INT          NOT NULL,
    [SortOrder]     INT          NOT NULL,
    [Selectable]    BIT          NOT NULL,
    [RemindOnDay]   INT          NOT NULL,
    [Withdrawn]     BIT          NOT NULL
);


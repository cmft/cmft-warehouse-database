﻿CREATE TABLE [dbo].[GEL_DerivativeTypes] (
    [DerivativeTypeId]  INT          NOT NULL,
    [Version]           INT          NOT NULL,
    [SampleTypeId]      INT          NOT NULL,
    [Name]              VARCHAR (30) NOT NULL,
    [GelName]           VARCHAR (30) NOT NULL,
    [DefaultVol]        INT          NOT NULL,
    [MaximumVol]        INT          NOT NULL,
    [ConcentrationTest] VARCHAR (30) NOT NULL,
    [RatioTest]         VARCHAR (30) NOT NULL,
    [Count]             INT          NOT NULL,
    [LabelRunId]        INT          NOT NULL,
    [SortOrder]         INT          NOT NULL
);


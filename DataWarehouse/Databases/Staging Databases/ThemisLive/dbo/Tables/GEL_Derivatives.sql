﻿CREATE TABLE [dbo].[GEL_Derivatives] (
    [DerivativeId]     INT          NOT NULL,
    [Version]          INT          NOT NULL,
    [SampleId]         INT          NOT NULL,
    [DerivativeTypeId] INT          NOT NULL,
    [Volume]           INT          NOT NULL,
    [TubeBarcode]      VARCHAR (15) NOT NULL,
    [A260Conc]         INT          NOT NULL,
    [A260A280]         INT          NOT NULL,
    [DSDNAConc]        INT          NOT NULL,
    [DSProtocol]       VARCHAR (50) NOT NULL,
    [DSRunTime]        DATETIME     NULL,
    [RackName]         VARCHAR (10) NOT NULL,
    [RackPosition]     VARCHAR (3)  NOT NULL,
    [InterimDataBy]    INT          NOT NULL,
    [InterimDataOn]    DATETIME     NULL,
    [TrackedBy]        INT          NOT NULL,
    [TrackedOn]        DATETIME     NULL,
    [DispatchId]       INT          NOT NULL
);


﻿CREATE TABLE [dbo].[GEL_SampleTypes] (
    [SampleTypeId]  INT          NOT NULL,
    [Version]       INT          NOT NULL,
    [Name]          VARCHAR (30) NOT NULL,
    [Code]          VARCHAR (15) NOT NULL,
    [ShortName]     VARCHAR (15) NOT NULL,
    [GelName]       VARCHAR (30) NOT NULL,
    [IsPrimary]     BIT          NOT NULL,
    [IsTumour]      BIT          NOT NULL,
    [TissueType]    INT          NOT NULL,
    [TestType]      INT          NOT NULL,
    [Anticoagulant] VARCHAR (15) NOT NULL,
    [LabelRunId]    INT          NOT NULL,
    [SortOrder]     INT          NOT NULL,
    [Withdrawn]     BIT          NOT NULL
);


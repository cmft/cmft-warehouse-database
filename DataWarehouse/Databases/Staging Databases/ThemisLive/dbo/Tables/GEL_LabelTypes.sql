﻿CREATE TABLE [dbo].[GEL_LabelTypes] (
    [LabelTypeId] INT           NOT NULL,
    [Version]     INT           NOT NULL,
    [LabelRunId]  INT           NOT NULL,
    [Printer]     VARCHAR (15)  NOT NULL,
    [Design]      VARCHAR (30)  NOT NULL,
    [Fields]      VARCHAR (200) NOT NULL,
    [Count]       INT           NOT NULL,
    [SortOrder]   INT           NOT NULL
);


﻿CREATE TABLE [dbo].[GEL_Actions] (
    [ActionId]      INT           NOT NULL,
    [Version]       INT           NOT NULL,
    [ActionTypeId]  INT           NOT NULL,
    [ParticipantId] INT           NOT NULL,
    [Context]       VARCHAR (50)  NOT NULL,
    [Comments]      VARCHAR (100) NOT NULL,
    [PerformedOn]   DATETIME      NOT NULL,
    [PerformedBy]   INT           NOT NULL,
    [RecordedOn]    DATETIME      NOT NULL,
    [RecordedBy]    INT           NOT NULL
);


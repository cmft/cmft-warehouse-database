﻿CREATE TABLE [dbo].[GEL_Samples] (
    [SampleId]      INT          NOT NULL,
    [Version]       INT          NOT NULL,
    [ParticipantId] INT          NOT NULL,
    [TakenOn]       DATETIME     NOT NULL,
    [SampleTypeId]  INT          NOT NULL,
    [LabelBarcode]  VARCHAR (15) NOT NULL,
    [LabNumber]     VARCHAR (8)  NOT NULL,
    [DisCode]       INT          NOT NULL,
    [TumourId]      INT          NOT NULL,
    [FreezeStartOn] DATETIME     NULL,
    [FreezeEndOn]   DATETIME     NULL
);


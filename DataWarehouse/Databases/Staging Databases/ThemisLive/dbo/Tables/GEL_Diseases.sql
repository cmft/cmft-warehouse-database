﻿CREATE TABLE [dbo].[GEL_Diseases] (
    [DiseaseId] INT           NOT NULL,
    [Version]   INT           NOT NULL,
    [Name]      VARCHAR (200) NOT NULL,
    [Code]      VARCHAR (15)  NOT NULL,
    [ShortName] VARCHAR (30)  NOT NULL,
    [GelName]   VARCHAR (50)  NOT NULL,
    [ICDOCode]  VARCHAR (7)   NOT NULL,
    [Depth]     INT           NOT NULL,
    [ParentId]  INT           NOT NULL,
    [IsRare]    BIT           NOT NULL,
    [Withdrawn] BIT           NOT NULL
);


﻿CREATE TABLE [dbo].[cliniclistschedule](
	[csc_id] [int] IDENTITY(1,1) NOT NULL,
	[csc_pat_id] [uniqueidentifier] NULL,
	[csc_clt_id] [int] NULL,
	[csc_clinicdatetime] [datetime] NULL,
	[csc_newpatient] [bit] NULL,
	[csc_manual] [bit] NULL DEFAULT ((0)),
	[csc_audit_userid] [int] NOT NULL DEFAULT ((-1)),
	[csc_audit_date] [datetime] NOT NULL DEFAULT (getdate()),
	[csc_patnumdisplay] [varchar](50) NULL,
	[csc_sec_id_appointmentwith] [int] NULL,
	[csc_flag] [int] NULL,
	[csc_comment] [varchar](255) NULL
) ON [PRIMARY]
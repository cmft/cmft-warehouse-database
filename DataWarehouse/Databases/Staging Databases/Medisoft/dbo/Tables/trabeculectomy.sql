﻿CREATE TABLE [dbo].[trabeculectomy](
	[trb_id] [uniqueidentifier] NOT NULL,
	[trb_position] [int] NULL,
	[trb_dru_id_preparation] [uniqueidentifier] NULL,
	[trb_rft_id_tractionsuture] [uniqueidentifier] NULL,
	[trb_rft_id_suturethickness] [uniqueidentifier] NULL,
	[trb_rft_id_suturematerial] [uniqueidentifier] NULL,
	[trb_rft_id_conjincision] [uniqueidentifier] NULL,
	[trb_paracentesis] [bit] NULL,
	[trb_paracentesisdegrees] [int] NULL,
	[trb_rft_id_viscoelastic] [uniqueidentifier] NULL,
	[trb_rft_id_viscoelasticinsitu] [uniqueidentifier] NULL,
	[trb_rft_id_flapshape] [uniqueidentifier] NULL,
	[trb_flapsizew] [decimal](5, 2) NULL,
	[trb_flapsizeh] [decimal](5, 2) NULL,
	[trb_rft_id_flapthickness] [uniqueidentifier] NULL,
	[trb_rft_id_sclerostomytechnique] [uniqueidentifier] NULL,
	[trb_sclerostomysizew] [decimal](5, 2) NULL,
	[trb_sclerostomysizeh] [decimal](5, 2) NULL,
	[trb_rft_id_antimetaboliteapplication] [uniqueidentifier] NULL,
	[trb_rft_id_antimetabolite] [uniqueidentifier] NULL,
	[trb_antimetaboliteminutes] [tinyint] NULL,
	[trb_antimetaboliteseconds] [tinyint] NULL,
	[trb_antimetabolitevolume] [decimal](5, 2) NULL,
	[trb_peripheraliridectomy] [bit] NULL,
	[trb_acmaintainer] [bit] NULL,
	[trb_scleralfixednumber] [int] NULL,
	[trb_rft_id_scleralfixedgrade] [uniqueidentifier] NULL,
	[trb_rft_id_scleralfixedmaterial] [uniqueidentifier] NULL,
	[trb_scleralreleasablenumber] [int] NULL,
	[trb_rft_id_scleralreleasablegrade] [uniqueidentifier] NULL,
	[trb_rft_id_scleralreleasablematerial] [uniqueidentifier] NULL,
	[trb_conjunctivalnumber] [int] NULL,
	[trb_rft_id_conjunctivalgrade] [uniqueidentifier] NULL,
	[trb_rft_id_conjunctivalmaterial] [uniqueidentifier] NULL,
	[trb_previous] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OZUEpisodeAnaesthesia](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[Anaesthetist] [uniqueidentifier] NULL,
	[AnaesthesiaType] [varchar](255) NULL,
	[AnaesthetistType] [varchar](200) NULL,
	[AnaesthetistGrade] [varchar](200) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dataextraction](
	[dex_id] [uniqueidentifier] NOT NULL,
	[dex_name] [varchar](50) NULL,
	[dex_sqlquery] [varchar](4000) NULL,
	[dex_frequency] [int] NULL,
	[dex_active] [bit] NOT NULL,
	[dex_lastrun] [datetime] NULL
) ON [PRIMARY]
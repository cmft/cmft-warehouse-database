﻿CREATE TABLE [dbo].[registrationstatus](
	[rgs_id] [uniqueidentifier] NOT NULL,
	[rgs_reg_id] [uniqueidentifier] NULL,
	[rgs_primary] [bit] NULL,
	[rgs_active] [bit] NULL,
	[rgs_desc] [varchar](255) NULL
) ON [PRIMARY]
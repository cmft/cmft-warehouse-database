﻿CREATE TABLE [dbo].[audit](
	[aud_id] [uniqueidentifier] NOT NULL,
	[aud_rowid] [uniqueidentifier] NULL,
	[aud_table] [varchar](255) NULL,
	[aud_use_id] [uniqueidentifier] NULL,
	[aud_tranid] [uniqueidentifier] NULL,
	[aud_timestamp] [datetime] NULL DEFAULT (getdate()),
	[aud_deleted] [bit] NULL
) ON [PRIMARY]
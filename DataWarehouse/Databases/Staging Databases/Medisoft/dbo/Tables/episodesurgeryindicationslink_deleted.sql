﻿CREATE TABLE [dbo].[episodesurgeryindicationslink_deleted](
	[eid_id] [uniqueidentifier] NOT NULL,
	[eid_epi_id] [uniqueidentifier] NULL,
	[eid_eye_id] [uniqueidentifier] NULL,
	[eid_dgn_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[address](
	[add_id] [uniqueidentifier] NOT NULL,
	[add_address1] [varchar](255) NOT NULL,
	[add_address2] [varchar](255) NOT NULL,
	[add_address3] [varchar](255) NOT NULL,
	[add_address4] [varchar](255) NOT NULL,
	[add_postcode] [varchar](255) NOT NULL
) ON [PRIMARY]
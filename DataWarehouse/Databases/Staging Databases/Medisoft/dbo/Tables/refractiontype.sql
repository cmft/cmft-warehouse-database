﻿CREATE TABLE [dbo].[refractiontype](
	[rty_id] [uniqueidentifier] NOT NULL,
	[rty_code] [varchar](255) NOT NULL,
	[rty_desc] [varchar](255) NOT NULL,
	[rty_order] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[timeevent](
	[tim_id] [uniqueidentifier] NOT NULL,
	[tim_object_id] [uniqueidentifier] NULL,
	[tim_class] [varchar](64) NOT NULL,
	[tim_event_type] [int] NOT NULL,
	[tim_timems] [int] NULL,
	[tim_use_id] [uniqueidentifier] NULL,
	[tim_computer] [varchar](128) NULL,
	[tim_time] [datetime] NULL
) ON [PRIMARY]
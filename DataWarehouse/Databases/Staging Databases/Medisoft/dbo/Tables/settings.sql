﻿CREATE TABLE [dbo].[settings](
	[set_id] [int] NOT NULL,
	[set_value] [varchar](255) NULL,
	[set_description] [varchar](255) NULL
) ON [PRIMARY]
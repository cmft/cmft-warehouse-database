﻿CREATE TABLE [dbo].[locationtypes](
	[ltl_id] [uniqueidentifier] NOT NULL,
	[ltl_loc_id] [uniqueidentifier] NOT NULL,
	[ltl_lot_id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DMOEpisodeSubjectiveVision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[Eye] [varchar](1) NULL,
	[SubjectiveChangeInVision] [varchar](20) NULL
) ON [PRIMARY]
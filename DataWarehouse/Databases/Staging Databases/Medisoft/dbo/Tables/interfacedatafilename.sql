﻿CREATE TABLE [dbo].[interfacedatafilename](
	[idf_id] [uniqueidentifier] NOT NULL,
	[idf_filename] [varchar](255) NULL,
	[idf_datetime] [datetime] NULL,
	[idf_source] [varchar](255) NULL,
	[idf_length] [bigint] NULL,
	[idf_creationtimeutc] [datetime] NULL
) ON [PRIMARY]
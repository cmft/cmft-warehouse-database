﻿CREATE TABLE [dbo].[Responses](
	[response_id] [varchar](200) NOT NULL,
	[type] [varchar](200) NOT NULL,
	[code] [varchar](200) NOT NULL,
	[message] [varchar](200) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[clinicalmenustore](
	[mst_id] [uniqueidentifier] NOT NULL,
	[mst_owner] [int] NOT NULL,
	[mst_item] [int] NULL,
	[mst_parent] [int] NULL,
	[mst_level] [int] NULL,
	[mst_itemdesc] [varchar](255) NULL,
	[mst_itemCode] [varchar](10) NULL,
	[mst_termId] [uniqueidentifier] NULL,
	[mst_itemOrder] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ETCConfig](
	[ID] [int] NOT NULL,
	[Version] [varchar](50) NULL,
	[HESCode] [varchar](50) NULL,
	[ETCPassword] [varchar](50) NULL,
	[WebServiceUserName] [varchar](50) NULL,
	[WebServicePassword] [varchar](50) NULL,
	[AutoUploadActive] [bit] NOT NULL,
	[RunEveryNMonths] [int] NOT NULL,
	[RunDay] [int] NOT NULL,
	[RunTime] [datetime] NULL
) ON [PRIMARY]
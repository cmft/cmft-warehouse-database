﻿CREATE TABLE [dbo].[operationanaesthesia](
	[oan_id] [uniqueidentifier] NOT NULL,
	[oan_ope_id] [uniqueidentifier] NULL,
	[oan_use_id_anaesthetist] [uniqueidentifier] NULL,
	[oan_rft_id_anaesthesiatype] [uniqueidentifier] NULL,
	[oan_hyalase] [bit] NULL,
	[oan_rft_id_blockmobility] [uniqueidentifier] NULL,
	[oan_rft_id_blockocularpressure] [uniqueidentifier] NULL,
	[oan_gra_id_anaesthetist] [uniqueidentifier] NULL
) ON [PRIMARY]
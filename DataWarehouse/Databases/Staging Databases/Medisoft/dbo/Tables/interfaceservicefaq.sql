﻿CREATE TABLE [dbo].[interfaceservicefaq](
	[ifq_id] [uniqueidentifier] NOT NULL,
	[ifq_servicename] [varchar](255) NULL,
	[ifq_servicemode] [varchar](255) NULL,
	[ifq_faq] [varchar](255) NULL,
	[ifq_faq_html] [ntext] NULL,
	[ifq_order] [int] NULL,
	[ifq_available] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
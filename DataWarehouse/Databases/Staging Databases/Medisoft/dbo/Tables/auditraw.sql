﻿CREATE TABLE [dbo].[auditraw](
	[aur_id] [uniqueidentifier] NOT NULL,
	[aur_aud_id] [uniqueidentifier] NOT NULL,
	[aur_row] [varchar](8000) NULL,
	[aur_processed] [bit] NULL CONSTRAINT [DF__auditraw__aur_pr__178EAB90]  DEFAULT (0),
	[aur_rowid] [uniqueidentifier] NULL,
	[aur_table] [varchar](255) NULL,
	[aur_use_id] [uniqueidentifier] NULL,
	[aur_tranid] [uniqueidentifier] NULL,
	[aur_timestamp] [datetime] NULL CONSTRAINT [DF__auditraw__aur_ti__66B66A0B]  DEFAULT (getdate())
) ON [PRIMARY]
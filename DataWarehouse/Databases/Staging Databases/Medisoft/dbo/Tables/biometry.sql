﻿CREATE TABLE [dbo].[biometry](
	[bio_id] [uniqueidentifier] NOT NULL,
	[bio_use_id] [uniqueidentifier] NULL,
	[bio_optometristdata] [bit] NOT NULL,
	[bio_simk] [bit] NOT NULL,
	[bio_iolmaster] [bit] NOT NULL
) ON [PRIMARY]
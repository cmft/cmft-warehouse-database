﻿CREATE TABLE [dbo].[ETCEpisodeTreatmentInjection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[TreatmentID] [uniqueidentifier] NOT NULL,
	[InjectionDrug] [varchar](255) NULL,
	[InjectionLocation] [varchar](255) NULL,
	[EntryFromLimbus] [decimal](5, 2) NULL,
	[LimbusAngle] [varchar](20) NULL,
	[PreviousAntiVEGFInjectionsInEye] [int] NULL,
	[LensStatus] [varchar](255) NULL
) ON [PRIMARY]
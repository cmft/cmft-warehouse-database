﻿CREATE TABLE [dbo].[surgeondefaultlaser](
	[lad_id] [uniqueidentifier] NOT NULL,
	[lad_fks_id] [uniqueidentifier] NOT NULL,
	[lad_rft_id_lens] [uniqueidentifier] NULL,
	[lad_rft_id_preparation] [uniqueidentifier] NULL,
	[lad_rft_id_instrument] [uniqueidentifier] NULL,
	[lad_wavelength] [int] NULL,
	[lad_beamdiameter] [int] NULL,
	[lad_durationmin] [decimal](5, 2) NULL,
	[lad_type] [int] NULL,
	[lad_rft_id_color] [uniqueidentifier] NULL,
	[lad_rft_id_lens2] [uniqueidentifier] NULL,
	[lad_beamdiameter2] [int] NULL,
	[lad_burns2] [int] NULL,
	[lad_first] [int] NULL
) ON [PRIMARY]
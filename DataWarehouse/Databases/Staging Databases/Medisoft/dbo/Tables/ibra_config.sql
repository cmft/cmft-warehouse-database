﻿CREATE TABLE [dbo].[ibra_config](
	[ibc_config_id] [uniqueidentifier] NOT NULL,
	[ibc_ftpserver] [varchar](255) NULL,
	[ibc_ftpusername] [varchar](50) NULL,
	[ibc_ftppassword] [varchar](50) NULL,
	[ibc_ftppath] [varchar](255) NULL,
	[ibc_patientidentifiers] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[octeye](
	[ome_id] [uniqueidentifier] NOT NULL,
	[ome_ocm_id] [uniqueidentifier] NOT NULL,
	[ome_eye_id] [uniqueidentifier] NOT NULL,
	[ome_crt] [int] NULL,
	[ome_macularvolume] [decimal](5, 2) NULL,
	[ome_pedthickness] [int] NULL,
	[ome_rft_id_other] [uniqueidentifier] NULL,
	[ome_ft] [int] NULL
) ON [PRIMARY]
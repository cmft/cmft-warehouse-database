﻿CREATE TABLE [dbo].[cliniclistvisit](
	[clv_id] [int] IDENTITY(1,1) NOT NULL,
	[clv_csc_id] [int] NULL,
	[clv_arrivaldatetime] [datetime] NULL,
	[clv_use_id_seenby] [int] NULL,
	[clv_flags] [int] NULL DEFAULT ((0)),
	[clv_rft_id_dnareason] [uniqueidentifier] NULL,
	[clv_18week] [varchar](5) NULL,
	[clv_rft_id_transport] [uniqueidentifier] NULL,
	[clv_comment] [varchar](500) NULL,
	[clv_18weekpas] [varchar](5) NULL,
	[clv_pasupdated] [varchar](500) NULL,
	[clv_pasupdatedtime] [datetime] NULL,
	[clv_updateddatetime] [datetime] NULL,
	[clv_autoupdate] [bit] NULL DEFAULT ((0)),
	[clv_audit_userid] [int] NOT NULL DEFAULT ((-1)),
	[clv_audit_date] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]
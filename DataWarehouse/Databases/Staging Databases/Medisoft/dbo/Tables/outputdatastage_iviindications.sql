﻿CREATE TABLE [dbo].[outputdatastage_iviindications](
	[ind_pat_id] [uniqueidentifier] NULL,
	[ind_eye_id] [uniqueidentifier] NULL,
	[ind_drug] [varchar](25) NULL,
	[ind_desc] [varchar](255) NULL,
	[ind_flag] [int] NULL
) ON [PRIMARY]
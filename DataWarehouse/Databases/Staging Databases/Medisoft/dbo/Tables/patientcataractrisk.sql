﻿CREATE TABLE [dbo].[patientcataractrisk](
	[pcr_id] [uniqueidentifier] NOT NULL,
	[pcr_pat_id] [uniqueidentifier] NOT NULL,
	[pcr_eye_id] [uniqueidentifier] NOT NULL,
	[pcr_risk] [numeric](15, 2) NOT NULL,
	[pcr_risksource] [int] NULL,
	[pcr_risklabel] [varchar](255) NULL
) ON [PRIMARY]
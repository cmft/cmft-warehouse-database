﻿CREATE TABLE [dbo].[OZUEpisodeTreatmentComplication](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[Eye] [char](1) NOT NULL,
	[Type] [varchar](255) NULL,
	[Description] [varchar](255) NULL
) ON [PRIMARY]
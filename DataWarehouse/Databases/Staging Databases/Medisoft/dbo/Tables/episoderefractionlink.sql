﻿CREATE TABLE [dbo].[episoderefractionlink](
	[erl_id] [uniqueidentifier] NOT NULL,
	[erl_epi_id] [uniqueidentifier] NULL,
	[erl_ref_id] [uniqueidentifier] NULL
) ON [PRIMARY]
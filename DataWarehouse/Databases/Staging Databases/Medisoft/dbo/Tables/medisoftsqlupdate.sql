﻿CREATE TABLE [dbo].[medisoftsqlupdate](
	[msu_id] [uniqueidentifier] NOT NULL,
	[msu_medisoftsqlupdate] [ntext] NULL,
	[msu_processed_timestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
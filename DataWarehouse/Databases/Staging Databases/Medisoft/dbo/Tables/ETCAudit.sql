﻿CREATE TABLE [dbo].[ETCAudit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Action] [varchar](4000) NOT NULL
) ON [PRIMARY]
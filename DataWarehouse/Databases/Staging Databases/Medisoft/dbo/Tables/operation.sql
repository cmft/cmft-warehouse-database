﻿CREATE TABLE [dbo].[operation](
	[ope_id] [uniqueidentifier] NOT NULL,
	[ope_datelisted] [datetime] NULL,
	[ope_rft_id_operationstatus] [uniqueidentifier] NULL,
	[ope_admissiondate] [datetime] NULL,
	[ope_daycase] [bit] NULL,
	[ope_waitlistinitiative] [bit] NULL,
	[ope_gra_id_surgeon] [uniqueidentifier] NULL,
	[ope_use_id_surgeon] [uniqueidentifier] NULL,
	[ope_use_id_consultant] [uniqueidentifier] NULL,
	[ope_gps_id] [uniqueidentifier] NULL,
	[ope_dischargedate] [datetime] NULL,
	[ope_privatepatient] [bit] NULL,
	[ope_directbooking] [bit] NULL,
	[ope_use_id_assistant] [uniqueidentifier] NULL,
	[ope_gra_id_assistant] [uniqueidentifier] NULL,
	[ope_discharge] [bit] NULL
) ON [PRIMARY]
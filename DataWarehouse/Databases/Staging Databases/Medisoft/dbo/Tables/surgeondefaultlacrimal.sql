﻿CREATE TABLE [dbo].[surgeondefaultlacrimal](
	[sdl_id] [uniqueidentifier] NOT NULL,
	[sdl_fks_id] [uniqueidentifier] NOT NULL,
	[sdl_rft_id_preparationskin] [uniqueidentifier] NULL,
	[sdl_rft_id_preparationnasal] [uniqueidentifier] NULL,
	[sdl_rft_id_incision] [uniqueidentifier] NULL,
	[sdl_medialcanthus] [bit] NULL,
	[sdl_periosteum] [bit] NULL,
	[sdl_rft_id_rhinostomy] [uniqueidentifier] NULL,
	[sdl_rft_id_rhinostomymethod] [uniqueidentifier] NULL,
	[sdl_rft_id_nasalflaps] [uniqueidentifier] NULL,
	[sdl_rft_id_lacrimalsacflaps] [uniqueidentifier] NULL,
	[sdl_membranectomy] [bit] NULL,
	[sdl_tubes] [bit] NULL,
	[sdl_rft_id_tubes] [uniqueidentifier] NULL,
	[sdl_rft_id_bypasstubes] [uniqueidentifier] NULL,
	[sdl_bypasstubesize] [int] NULL,
	[sdl_suture] [bit] NULL,
	[sdl_carunculectomy] [bit] NULL,
	[sdl_punctalstenosis] [bit] NULL,
	[sdl_rft_id_lacrimalsac] [uniqueidentifier] NULL,
	[sdl_rft_id_internalopening] [uniqueidentifier] NULL,
	[sdl_retrogradeintubation] [bit] NULL,
	[sdl_nasalpack] [bit] NULL
) ON [PRIMARY]
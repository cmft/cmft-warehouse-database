﻿CREATE TABLE [dbo].[interfacedataimagenet2000](
	[iit_id] [uniqueidentifier] NOT NULL,
	[iit_ipm_id] [uniqueidentifier] NULL,
	[iit_shotdate] [datetime] NULL,
	[iit_shotproc] [varchar](20) NULL,
	[iit_database] [varchar](25) NULL,
	[iit_processed] [bit] NULL,
	[iit_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
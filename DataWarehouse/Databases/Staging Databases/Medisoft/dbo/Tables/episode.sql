﻿CREATE TABLE [dbo].[episode](
	[epi_id] [uniqueidentifier] NOT NULL,
	[epi_pat_id] [uniqueidentifier] NULL,
	[epi_date] [datetime] NULL,
	[epi_daterecorded] [datetime] NULL CONSTRAINT [DF_Episode_DateRecorded]  DEFAULT (getdate()),
	[epi_ety_id] [uniqueidentifier] NULL,
	[epi_loc_id] [uniqueidentifier] NULL,
	[epi_mode] [int] NOT NULL CONSTRAINT [DF__episode__epi_mod__7A587A31]  DEFAULT (1),
	[epi_rft_id_clinic] [uniqueidentifier] NULL,
	[epi_datesaved] [datetime] NULL DEFAULT (getdate()),
	[epi_private] [bit] NULL DEFAULT (0),
	[epi_ust_no] [int] NULL
) ON [PRIMARY]
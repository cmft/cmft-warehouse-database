﻿CREATE TABLE [dbo].[slt](
	[slt_id] [uniqueidentifier] NOT NULL,
	[slt_rft_id_surgery] [uniqueidentifier] NULL,
	[slt_rft_id_instrument] [uniqueidentifier] NULL,
	[slt_rft_id_goniolens] [uniqueidentifier] NULL,
	[slt_rft_id_degreestreated] [uniqueidentifier] NULL,
	[slt_degreesfrom] [int] NULL,
	[slt_degreesto] [int] NULL,
	[slt_beamdiameter] [int] NULL,
	[slt_numberoflaserspots] [int] NULL,
	[slt_power] [float] NULL
) ON [PRIMARY]
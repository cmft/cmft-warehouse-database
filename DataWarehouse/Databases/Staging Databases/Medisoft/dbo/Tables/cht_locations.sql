﻿CREATE TABLE [dbo].[cht_locations](
	[locationid] [int] NOT NULL,
	[locationname] [varchar](200) NOT NULL,
	[selectable] [int] NOT NULL,
	[selected] [int] NOT NULL
) ON [PRIMARY]
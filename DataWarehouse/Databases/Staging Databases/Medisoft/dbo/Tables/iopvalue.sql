﻿CREATE TABLE [dbo].[iopvalue](
	[iop_eil_id] [uniqueidentifier] NOT NULL,
	[iop_eye_id] [uniqueidentifier] NOT NULL,
	[iop_value] [decimal](5, 1) NULL,
	[iop_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[iop_valueg] [decimal](5, 1) NULL,
	[iop_valuescore] [decimal](5, 1) NULL,
	[iop_mode] [int] NULL,
	[iop_valuecc] [decimal](5, 1) NULL
) ON [PRIMARY]
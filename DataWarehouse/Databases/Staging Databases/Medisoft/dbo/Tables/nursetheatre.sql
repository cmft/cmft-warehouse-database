﻿CREATE TABLE [dbo].[nursetheatre](
	[nth_id] [uniqueidentifier] NOT NULL,
	[nth_use_id_nurse] [uniqueidentifier] NULL,
	[nth_use_id_consultant] [uniqueidentifier] NULL,
	[nth_timein] [varchar](255) NOT NULL,
	[nth_timestart] [varchar](255) NOT NULL,
	[nth_timefinish] [varchar](255) NOT NULL,
	[nth_timeout] [varchar](255) NOT NULL,
	[nth_use_id_scrubnurse] [uniqueidentifier] NULL,
	[nth_use_id_runner] [uniqueidentifier] NULL,
	[nth_use_id_nurse1] [uniqueidentifier] NULL,
	[nth_use_id_nurse2] [uniqueidentifier] NULL
) ON [PRIMARY]
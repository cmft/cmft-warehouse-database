﻿CREATE TABLE [dbo].[transactionfailures](
	[tfl_id] [uniqueidentifier] NOT NULL,
	[tfl_statements] [ntext] NULL,
	[tfl_error] [int] NOT NULL,
	[tfl_use_id] [uniqueidentifier] NULL,
	[tfl_error_desc_client] [varchar](500) NULL,
	[tfl_datetime] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
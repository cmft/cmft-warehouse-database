﻿CREATE TABLE [dbo].[errorlog](
	[err_id] [uniqueidentifier] NOT NULL,
	[err_no] [int] NOT NULL,
	[err_object] [varchar](256) NULL,
	[err_proc] [varchar](256) NULL,
	[err_procloc] [varchar](256) NULL,
	[err_stack] [varchar](2000) NULL,
	[err_note] [varchar](2000) NULL,
	[err_critical] [bit] NULL,
	[err_use_id] [uniqueidentifier] NULL,
	[err_pat_id] [uniqueidentifier] NULL,
	[err_date] [datetime] NULL CONSTRAINT [err_datedefault]  DEFAULT (getdate()),
	[err_xml] [ntext] NULL,
	[err_extra] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[settingsaudit](
	[sea_id] [uniqueidentifier] NOT NULL,
	[sea_set_id] [int] NOT NULL,
	[sea_set_oldvalue] [varchar](255) NULL,
	[sea_set_newvalue] [varchar](255) NULL,
	[sea_use_id] [uniqueidentifier] NULL,
	[sea_timestamp] [datetime] NULL
) ON [PRIMARY]
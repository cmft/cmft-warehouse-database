﻿CREATE TABLE [dbo].[hl7_element](
	[ele_id] [uniqueidentifier] NOT NULL,
	[ele_item] [int] NOT NULL,
	[ele_description] [varchar](255) NOT NULL,
	[ele_seg_segment] [varchar](3) NOT NULL,
	[ele_sequence] [int] NOT NULL,
	[ele_length] [int] NULL,
	[ele_dty_datatype] [varchar](255) NULL,
	[ele_rep] [varchar](255) NULL,
	[ele_table] [varchar](255) NULL
) ON [PRIMARY]
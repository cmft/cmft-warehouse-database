﻿CREATE TABLE [dbo].[DMOEpisodeObservations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[HbA1c] [decimal](10, 2) NULL,
	[HbA1cUnits] [varchar](255) NULL,
	[BPsystolic] [int] NULL,
	[BPdiastolic] [int] NULL
) ON [PRIMARY]
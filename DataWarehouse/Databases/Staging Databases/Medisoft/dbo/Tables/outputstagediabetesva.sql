﻿CREATE TABLE [dbo].[outputstagediabetesva](
	[EpisodeID] [uniqueidentifier] NULL,
	[PatientID] [uniqueidentifier] NULL,
	[CriteriaDate] [datetime] NULL DEFAULT (getdate()),
	[SaveDate] [datetime] NULL,
	[Diabetic] [int] NULL,
	[VA] [int] NULL,
	[DiabetesType] [varchar](50) NULL
) ON [PRIMARY]
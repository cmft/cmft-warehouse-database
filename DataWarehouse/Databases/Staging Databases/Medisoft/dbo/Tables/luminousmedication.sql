﻿CREATE TABLE [dbo].[luminousmedication](
	[lum_id] [uniqueidentifier] NOT NULL,
	[lum_lub_id] [uniqueidentifier] NOT NULL,
	[lum_epi_id] [uniqueidentifier] NOT NULL,
	[lum_uploaddate] [datetime] NULL,
	[lum_commitdate] [datetime] NULL,
	[lum_createdate] [datetime] NOT NULL,
	[lum_savedate] [datetime] NOT NULL,
	[lum_dtarep1c] [int] NULL,
	[lum_cmdnam1a] [varchar](255) NULL,
	[lum_cmdrsn1a] [varchar](255) NULL,
	[lum_sitjc] [int] NULL,
	[lum_cmdstt1d] [datetime] NULL,
	[lum_cmdend1d] [datetime] NULL,
	[lum_cmdctu1c] [bit] NULL
) ON [PRIMARY]
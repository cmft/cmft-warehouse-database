﻿CREATE TABLE [dbo].[visualfieldstest](
	[vft_id] [uniqueidentifier] NOT NULL,
	[vft_rft_id_testtype] [uniqueidentifier] NULL,
	[vft_age] [int] NULL,
	[vft_manual] [bit] NULL
) ON [PRIMARY]
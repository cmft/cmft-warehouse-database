﻿CREATE TABLE [dbo].[communitywebsiteobjectlog](
	[cwo_id] [uniqueidentifier] NOT NULL,
	[cwo_objecttype] [int] NULL,
	[cwo_fks_id] [uniqueidentifier] NULL,
	[cwo_insertedremotely] [datetime] NULL,
	[cwo_lastupdated] [datetime] NULL
) ON [PRIMARY]
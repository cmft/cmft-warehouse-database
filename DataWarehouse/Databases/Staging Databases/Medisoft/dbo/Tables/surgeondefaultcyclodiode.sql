﻿CREATE TABLE [dbo].[surgeondefaultcyclodiode](
	[sdc_id] [uniqueidentifier] NOT NULL,
	[sdc_fks_id] [uniqueidentifier] NULL,
	[sdc_rft_id_areatreated] [uniqueidentifier] NULL,
	[sdc_dru_id_preparation] [uniqueidentifier] NULL,
	[sdc_degreestreated] [int] NULL,
	[sdc_rft_id_instrument] [uniqueidentifier] NULL
) ON [PRIMARY]
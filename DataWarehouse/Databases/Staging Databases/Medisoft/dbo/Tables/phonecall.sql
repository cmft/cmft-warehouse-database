﻿CREATE TABLE [dbo].[phonecall](
	[pca_id] [uniqueidentifier] NOT NULL,
	[pca_use_id_phonedby] [uniqueidentifier] NULL,
	[pca_use_id_consultant] [uniqueidentifier] NULL,
	[pca_time] [varchar](255) NOT NULL,
	[pca_rft_id_from] [uniqueidentifier] NULL,
	[pca_type] [int] NULL,
	[pca_other] [varchar](255) NULL,
	[pca_rft_id_conversation] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[patienttrial](
	[pti_id] [uniqueidentifier] NOT NULL,
	[pti_pat_id] [uniqueidentifier] NULL,
	[pti_rft_id_trial] [uniqueidentifier] NULL,
	[pti_trial_start] [datetime] NULL,
	[pti_trial_stop] [datetime] NULL,
	[pti_rft_id_outcome] [uniqueidentifier] NULL,
	[pti_eye_id] [uniqueidentifier] NULL,
	[pti_comment] [varchar](2000) NULL
) ON [PRIMARY]
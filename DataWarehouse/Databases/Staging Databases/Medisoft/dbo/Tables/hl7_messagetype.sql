﻿CREATE TABLE [dbo].[hl7_messagetype](
	[mtp_message] [varchar](3) NOT NULL,
	[mtp_description] [varchar](255) NOT NULL,
	[mtp_chapter] [int] NOT NULL,
	[mtp_supported] [bit] NULL
) ON [PRIMARY]
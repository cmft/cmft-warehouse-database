﻿CREATE TABLE [dbo].[MedisoftIndexList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IndexName] [varchar](255) NOT NULL,
	[TableName] [varchar](1000) NOT NULL,
	[IsUnique] [bit] NULL,
	[ColumnList] [varchar](4000) NOT NULL
) ON [PRIMARY]
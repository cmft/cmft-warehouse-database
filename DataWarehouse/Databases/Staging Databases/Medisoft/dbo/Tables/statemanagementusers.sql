﻿CREATE TABLE [dbo].[statemanagementusers](
	[stu_id] [uniqueidentifier] NULL,
	[stu_use_id] [uniqueidentifier] NULL,
	[stu_machine] [varchar](255) NULL,
	[stu_lastcontact] [datetime] NULL,
	[stu_pat_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ffaeye](
	[ffe_id] [uniqueidentifier] NOT NULL,
	[ffe_ffa_id] [uniqueidentifier] NOT NULL,
	[ffe_eye_id] [uniqueidentifier] NOT NULL,
	[ffe_lesionsize] [decimal](5, 2) NULL,
	[ffe_fibrosispercent] [int] NULL,
	[ffe_haemorragearea] [int] NULL,
	[ffe_ped] [uniqueidentifier] NULL
) ON [PRIMARY]
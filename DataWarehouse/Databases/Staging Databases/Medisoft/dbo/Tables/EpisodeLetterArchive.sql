﻿CREATE TABLE [dbo].[EpisodeLetterArchive](
	[ela_id] [uniqueidentifier] NOT NULL,
	[ela_epi_id] [uniqueidentifier] NOT NULL,
	[ela_let_id] [uniqueidentifier] NOT NULL,
	[ela_content] [ntext] NOT NULL,
	[ela_datesent] [datetime] NOT NULL DEFAULT (getdate()),
	[ela_extra] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[nursepostop](
	[npt_id] [uniqueidentifier] NOT NULL,
	[npt_use_id_dischargedby] [uniqueidentifier] NULL,
	[npt_timeward] [varchar](255) NULL,
	[npt_timedischarged] [varchar](255) NULL,
	[npt_painscoreanaesthetic] [float] NULL,
	[npt_painscoreperop] [float] NULL,
	[npt_painscorepostop] [float] NULL,
	[npt_rft_id_pulse] [uniqueidentifier] NULL,
	[npt_rft_id_dressing] [uniqueidentifier] NULL,
	[npt_ope_id] [uniqueidentifier] NULL,
	[npt_use_id_dispensedby] [uniqueidentifier] NULL
) ON [PRIMARY]
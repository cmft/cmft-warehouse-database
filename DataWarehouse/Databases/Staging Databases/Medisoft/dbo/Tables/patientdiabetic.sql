﻿CREATE TABLE [dbo].[patientdiabetic](
	[pdi_id] [uniqueidentifier] NOT NULL,
	[pdi_pat_id] [uniqueidentifier] NULL,
	[pdi_rft_id_diabetictype] [uniqueidentifier] NULL,
	[pdi_rft_id_diabeticcontrolled] [uniqueidentifier] NULL,
	[pdi_agediagnosed] [tinyint] NULL,
	[pdi_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
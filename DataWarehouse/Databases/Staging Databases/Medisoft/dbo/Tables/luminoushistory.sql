﻿CREATE TABLE [dbo].[luminoushistory](
	[luh_id] [uniqueidentifier] NOT NULL,
	[luh_lub_id] [uniqueidentifier] NOT NULL,
	[luh_epi_id] [uniqueidentifier] NOT NULL,
	[luh_uploaddate] [datetime] NULL,
	[luh_commitdate] [datetime] NULL,
	[luh_createdate] [datetime] NOT NULL,
	[luh_savedate] [datetime] NOT NULL,
	[luh_hiscnd1a] [varchar](255) NULL,
	[luh_dgnsrg1d] [datetime] NULL,
	[luh_sitjc] [int] NULL,
	[luh_actprb1c] [int] NULL
) ON [PRIMARY]
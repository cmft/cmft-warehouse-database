﻿CREATE TABLE [dbo].[referraloverride](
	[rfb_id] [uniqueidentifier] NOT NULL,
	[rfb_pat_id] [uniqueidentifier] NULL,
	[rfb_date] [datetime] NULL
) ON [PRIMARY]
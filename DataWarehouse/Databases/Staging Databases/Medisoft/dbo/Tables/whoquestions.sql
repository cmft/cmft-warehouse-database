﻿CREATE TABLE [dbo].[whoquestions](
	[whq_id] [int] IDENTITY(1,1) NOT NULL,
	[whq_whp_id] [int] NULL,
	[whq_desc] [varchar](255) NULL,
	[whq_label] [varchar](255) NULL,
	[whq_number] [int] NULL,
	[whq_type] [int] NULL,
	[whq_membership] [int] NULL,
	[whq_active] [bit] NULL
) ON [PRIMARY]
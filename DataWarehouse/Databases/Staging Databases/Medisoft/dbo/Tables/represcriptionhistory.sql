﻿CREATE TABLE [dbo].[represcriptionhistory](
	[rph_id] [uniqueidentifier] NOT NULL,
	[rph_mcn_id] [uniqueidentifier] NULL,
	[rph_epi_id] [uniqueidentifier] NULL,
	[rph_use_id] [uniqueidentifier] NULL,
	[rph_startdate] [datetime] NULL
) ON [PRIMARY]
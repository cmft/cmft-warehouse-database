﻿CREATE TABLE [dbo].[userhistory](
	[uhi_id] [uniqueidentifier] NOT NULL,
	[uhi_dt] [datetime] NULL,
	[uhi_use_id] [uniqueidentifier] NULL,
	[uhi_episode] [bit] NULL,
	[uhi_fks_id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
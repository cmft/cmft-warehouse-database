﻿CREATE TABLE [dbo].[interfacedataoctextra](
	[idx_id] [uniqueidentifier] NOT NULL,
	[idx_ido_id] [uniqueidentifier] NOT NULL,
	[idx_circle_total] [varchar](255) NULL,
	[idx_circle_superior] [varchar](255) NULL,
	[idx_circle_inferior] [varchar](255) NULL,
	[idx_disc_area] [varchar](255) NULL,
	[idx_rim_area] [varchar](255) NULL,
	[idx_linear_cdr] [varchar](255) NULL,
	[idx_rim_volume] [varchar](255) NULL,
	[idx_symmetry] [varchar](255) NULL,
	[idx_cup_area] [varchar](255) NULL,
	[idx_cd_area_ratio] [varchar](255) NULL,
	[idx_vertical_cdr] [varchar](255) NULL,
	[idx_cup_volume] [varchar](255) NULL,
	[idx_horizontal_dd] [varchar](255) NULL,
	[idx_vertical_dd] [varchar](255) NULL
) ON [PRIMARY]
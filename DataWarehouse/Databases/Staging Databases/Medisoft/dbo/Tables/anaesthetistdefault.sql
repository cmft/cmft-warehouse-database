﻿CREATE TABLE [dbo].[anaesthetistdefault](
	[anz_id] [uniqueidentifier] NOT NULL,
	[anz_use_id] [uniqueidentifier] NULL,
	[anz_venous] [bit] NULL,
	[anz_rft_id_venoussize] [uniqueidentifier] NULL,
	[anz_rft_id_venoussite] [uniqueidentifier] NULL,
	[anz_balloonpressure] [tinyint] NULL,
	[anz_dru_id_topicalanaesthetic] [uniqueidentifier] NULL,
	[anz_monitorecg] [bit] NULL,
	[anz_monitornibp] [bit] NULL,
	[anz_monitorspo2] [bit] NULL,
	[anz_rft_id_o2flow] [uniqueidentifier] NULL,
	[anz_rft_id_anaesthesiatype] [uniqueidentifier] NULL,
	[anz_pressurereductionmethod] [tinyint] NULL,
	[anz_pressurereductionduration] [tinyint] NULL
) ON [PRIMARY]
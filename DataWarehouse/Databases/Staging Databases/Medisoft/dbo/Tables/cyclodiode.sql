﻿CREATE TABLE [dbo].[cyclodiode](
	[cyd_id] [uniqueidentifier] NOT NULL,
	[cyd_rft_id_areatreated] [uniqueidentifier] NULL,
	[cyd_dru_id_preparation] [uniqueidentifier] NULL,
	[cyd_degreestreated] [int] NULL,
	[cyd_degreesfrom] [int] NULL,
	[cyd_degreesto] [int] NULL,
	[cyd_numberofshots] [int] NULL,
	[cyd_powerrangemin] [int] NULL,
	[cyd_powerrangemax] [int] NULL,
	[cyd_durationmax] [int] NULL,
	[cyd_durationmin] [int] NULL,
	[cyd_sparing3and9oclock] [bit] NULL,
	[cyd_rft_id_instrument] [uniqueidentifier] NULL
) ON [PRIMARY]
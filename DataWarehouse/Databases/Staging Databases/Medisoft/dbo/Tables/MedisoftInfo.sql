﻿CREATE TABLE [dbo].[MedisoftInfo](
	[spid] [int] NOT NULL,
	[status] [varchar](20) NOT NULL,
	[login] [varchar](255) NOT NULL,
	[blkBy] [varchar](10) NOT NULL,
	[command] [varchar](255) NOT NULL,
	[CPUTime] [int] NOT NULL,
	[DiskIO] [int] NOT NULL,
	[LastBatch] [varchar](255) NOT NULL,
	[ProgramName] [varchar](255) NOT NULL,
	[sqlHandle] [binary](20) NULL,
	[eventInfo] [nvarchar](4000) NULL,
	[sql] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
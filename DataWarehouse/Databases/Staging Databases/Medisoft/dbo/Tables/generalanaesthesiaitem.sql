﻿CREATE TABLE [dbo].[generalanaesthesiaitem](
	[git_id] [uniqueidentifier] NOT NULL,
	[git_gan_id] [uniqueidentifier] NOT NULL,
	[git_itemtype] [int] NULL,
	[git_itemorder] [int] NULL,
	[git_item_guid_id1] [uniqueidentifier] NULL,
	[git_item_guid_id2] [uniqueidentifier] NULL,
	[git_item_guid_id3] [uniqueidentifier] NULL,
	[git_itemtext] [varchar](255) NULL,
	[git_itemcode] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DMOPatientEpisodes](
	[epi_id] [uniqueidentifier] NOT NULL,
	[epi_pat_id] [uniqueidentifier] NULL,
	[epi_date] [datetime] NULL,
	[epi_daterecorded] [datetime] NULL,
	[epi_ety_id] [uniqueidentifier] NULL
) ON [PRIMARY]
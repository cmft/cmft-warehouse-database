﻿CREATE TABLE [dbo].[cliniclistschedulearchive](
	[csc_id] [int] NOT NULL,
	[csc_pat_id] [uniqueidentifier] NULL,
	[csc_clt_id] [int] NULL,
	[csc_clinicdatetime] [datetime] NULL,
	[csc_newpatient] [bit] NULL,
	[csc_manual] [bit] NULL,
	[csc_audit_userid] [int] NOT NULL,
	[csc_audit_date] [datetime] NOT NULL
) ON [PRIMARY]
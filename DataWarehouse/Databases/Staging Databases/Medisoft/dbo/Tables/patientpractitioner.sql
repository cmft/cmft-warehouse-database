﻿CREATE TABLE [dbo].[patientpractitioner](
	[ppr_pat_id] [uniqueidentifier] NOT NULL,
	[ppr_ptr_id] [uniqueidentifier] NOT NULL,
	[ppr_rft_id_practitionertype] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
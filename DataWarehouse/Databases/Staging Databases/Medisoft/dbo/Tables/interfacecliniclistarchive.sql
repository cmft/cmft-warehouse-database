﻿CREATE TABLE [dbo].[interfacecliniclistarchive](
	[ica_id] [uniqueidentifier] NOT NULL,
	[ica_use_id_consultant] [uniqueidentifier] NULL,
	[ica_pat_id] [uniqueidentifier] NULL,
	[ica_loc_id] [uniqueidentifier] NULL,
	[ica_clinicdatetime] [datetime] NULL,
	[ica_timearrived] [datetime] NULL,
	[ica_cliniccode] [varchar](255) NULL,
	[ica_clinicname] [varchar](255) NULL,
	[ica_timestamp] [datetime] NULL,
	[ica_newpatient] [bit] NULL,
	[ica_manual] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodevisualacuitylinkx](
	[evx_id] [uniqueidentifier] NOT NULL,
	[evx_epi_id] [uniqueidentifier] NULL,
	[evx_eye_id] [uniqueidentifier] NULL,
	[evx_vav_id_unaided] [uniqueidentifier] NULL,
	[evx_vav_id_pinhole] [uniqueidentifier] NULL,
	[evx_vav_id_bestcorrected] [uniqueidentifier] NULL,
	[evx_prosthesis] [bit] NULL,
	[evx_lens] [smallint] NULL,
	[evx_vav_id_unaided_g] [uniqueidentifier] NULL,
	[evx_vav_id_pinhole_g] [uniqueidentifier] NULL,
	[evx_vav_id_bestcorrected_g] [uniqueidentifier] NULL,
	[evx_rft_id_notation] [uniqueidentifier] NULL,
	[evx_readingset] [int] NULL,
	[evx_source] [int] NULL,
	[evx_rft_id_measurementtype] [uniqueidentifier] NULL,
	[evx_primary] [bit] NULL
) ON [PRIMARY]
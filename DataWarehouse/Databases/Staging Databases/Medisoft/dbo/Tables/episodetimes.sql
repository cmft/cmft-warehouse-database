﻿CREATE TABLE [dbo].[episodetimes](
	[ept_id] [uniqueidentifier] NOT NULL,
	[ept_epi_id] [uniqueidentifier] NULL,
	[ept_appointmenttime] [datetime] NULL,
	[ept_arrivaltime] [datetime] NULL,
	[ept_intime] [datetime] NULL,
	[ept_outtime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodeletteresend](
	[els_id] [uniqueidentifier] NOT NULL,
	[els_epi_id] [uniqueidentifier] NULL,
	[els_let_id] [uniqueidentifier] NULL,
	[els_content] [ntext] NULL,
	[els_datecreated] [datetime] NULL,
	[els_active] [bit] NULL,
	[els_sent] [bit] NULL,
	[els_datesent] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
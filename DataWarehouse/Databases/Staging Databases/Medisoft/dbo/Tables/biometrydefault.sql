﻿CREATE TABLE [dbo].[biometrydefault](
	[bde_id] [uniqueidentifier] NOT NULL,
	[bde_loc_id] [uniqueidentifier] NULL,
	[bde_rft_id_iolformula] [uniqueidentifier] NULL,
	[bde_rft_id_ascan] [uniqueidentifier] NULL,
	[bde_rft_id_keratometer] [uniqueidentifier] NULL,
	[bde_iol_id] [uniqueidentifier] NULL,
	[bde_dru_id_dilatingdrop] [uniqueidentifier] NULL,
	[bde_dru_id_anaestheticdrop] [uniqueidentifier] NULL,
	[bde_commport] [int] NULL,
	[bde_inbuffersize] [int] NULL,
	[bde_outbuffersize] [int] NULL,
	[bde_rthreshold] [int] NULL,
	[bde_sthreshold] [int] NULL,
	[bde_iolmastersettings] [varchar](255) NULL,
	[bde_delay] [varchar](50) NULL CONSTRAINT [DF_biometrydefault_bde_delay]  DEFAULT (0),
	[bde_timeout] [int] NULL,
	[bde_active] [bit] NULL CONSTRAINT [DF__biometryd__bde_a__0D3ECF6E]  DEFAULT (1)
) ON [PRIMARY]
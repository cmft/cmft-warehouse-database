﻿CREATE TABLE [dbo].[luminoustriggerhistory](
	[lut_id] [uniqueidentifier] NOT NULL,
	[lut_pat_id] [uniqueidentifier] NOT NULL,
	[lut_triggertype] [int] NOT NULL,
	[lut_date] [datetime] NOT NULL,
	[lut_outcome] [int] NOT NULL
) ON [PRIMARY]
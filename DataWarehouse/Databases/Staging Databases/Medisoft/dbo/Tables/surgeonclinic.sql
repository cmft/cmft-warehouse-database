﻿CREATE TABLE [dbo].[surgeonclinic](
	[sci_id] [uniqueidentifier] NOT NULL,
	[sci_use_id_seenby] [uniqueidentifier] NULL,
	[sci_use_id_consultant] [uniqueidentifier] NULL,
	[sci_use_id_dataenteredby] [uniqueidentifier] NULL,
	[sci_eye_id_dilated] [uniqueidentifier] NULL,
	[sci_pairsofglasses] [int] NULL,
	[sci_datereviewed] [datetime] NULL,
	[sci_reviewapproval] [int] NULL
) ON [PRIMARY]
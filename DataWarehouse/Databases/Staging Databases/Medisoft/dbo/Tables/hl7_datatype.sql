﻿CREATE TABLE [dbo].[hl7_datatype](
	[dty_category] [varchar](255) NULL,
	[dty_datatype] [varchar](255) NOT NULL,
	[dty_datatypename] [varchar](255) NULL,
	[dty_hl7_ref] [varchar](255) NULL,
	[dty_notes] [varchar](255) NULL,
	[dty_format] [varchar](2000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ETCUser](
	[UserID] [uniqueidentifier] NOT NULL,
	[CurrentGrade] [varchar](200) NULL,
	[UserType] [varchar](200) NULL
) ON [PRIMARY]
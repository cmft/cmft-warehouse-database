﻿CREATE TABLE [dbo].[ETCEpisodeIOP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Eye] [varchar](1) NULL,
	[IOP] [decimal](5, 1) NULL,
	[TonometerType] [varchar](255) NULL
) ON [PRIMARY]
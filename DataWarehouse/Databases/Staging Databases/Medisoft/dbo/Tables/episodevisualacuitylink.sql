﻿CREATE TABLE [dbo].[episodevisualacuitylink](
	[evl_id] [uniqueidentifier] NOT NULL,
	[evl_epi_id] [uniqueidentifier] NULL,
	[evl_eye_id] [uniqueidentifier] NULL,
	[evl_vav_id_unaided] [uniqueidentifier] NULL,
	[evl_vav_id_pinhole] [uniqueidentifier] NULL,
	[evl_vav_id_bestcorrected] [uniqueidentifier] NULL,
	[evl_prosthesis] [bit] NULL,
	[evl_lens] [smallint] NULL CONSTRAINT [DF_episodevisualacuitylink_evl_lens]  DEFAULT (1),
	[evl_vav_id_unaided_g] [uniqueidentifier] NULL,
	[evl_vav_id_pinhole_g] [uniqueidentifier] NULL,
	[evl_vav_id_bestcorrected_g] [uniqueidentifier] NULL,
	[evl_rft_id_notation] [uniqueidentifier] NULL,
	[evl_source] [int] NULL DEFAULT (0),
	[evl_rft_id_measurementtype] [uniqueidentifier] NULL
) ON [PRIMARY]
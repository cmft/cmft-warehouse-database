﻿CREATE TABLE [dbo].[diagnosticcodelist](
	[dnc_id] [uniqueidentifier] NULL,
	[dnc_codename] [nvarchar](50) NULL,
	[dnc_number] [int] NULL
) ON [PRIMARY]
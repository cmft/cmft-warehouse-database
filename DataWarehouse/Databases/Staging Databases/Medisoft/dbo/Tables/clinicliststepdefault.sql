﻿CREATE TABLE [dbo].[clinicliststepdefault](
	[hcs_id] [int] IDENTITY(1,1) NOT NULL,
	[hcs_description] [varchar](255) NOT NULL,
	[hcs_clt_id] [int] NOT NULL,
	[hcs_stp_id] [int] NOT NULL,
	[hcs_order] [int] NULL,
	[hcs_status] [int] NULL,
	[hcs_duration] [int] NULL DEFAULT ((30)),
	[hcs_ruleactivate] [int] NULL DEFAULT ((1)),
	[hcs_ruleautostart] [int] NULL DEFAULT ((1)),
	[hcs_ruleautocomplete] [int] NULL DEFAULT ((1)),
	[hcs_rulestartclockcondition] [int] NULL DEFAULT ((1)),
	[hcs_rulestopclockcondition] [int] NULL DEFAULT ((1)),
	[hcs_rulestartaction] [int] NULL DEFAULT ((1)),
	[hcs_red] [int] NULL DEFAULT ((1)),
	[hcs_amber] [int] NULL DEFAULT ((1)),
	[hcs_audit_userid] [int] NOT NULL DEFAULT ((-1)),
	[hcs_audit_date] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodejoin](
	[epj_id] [uniqueidentifier] NOT NULL,
	[epj_epi_id_src] [uniqueidentifier] NOT NULL,
	[epj_epi_id_dest] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodediabeticassesment](
	[dia_id] [uniqueidentifier] NOT NULL,
	[dia_epi_id] [uniqueidentifier] NOT NULL,
	[dia_rft_id_impairment] [uniqueidentifier] NULL,
	[dia_rft_id_appropriate] [uniqueidentifier] NULL,
	[dia_rft_id_accuracy] [uniqueidentifier] NULL,
	[dia_mode] [int] NULL,
	[dia_version] [int] NULL DEFAULT ((2))
) ON [PRIMARY]
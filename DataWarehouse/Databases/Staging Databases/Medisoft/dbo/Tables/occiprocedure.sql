﻿CREATE TABLE [dbo].[occiprocedure](
	[ocp_id] [uniqueidentifier] NOT NULL,
	[ocp_rft_id_diameter] [uniqueidentifier] NULL,
	[ocp_incisionlength] [float] NULL,
	[ocp_meridian] [int] NULL
) ON [PRIMARY]
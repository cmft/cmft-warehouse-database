﻿CREATE TABLE [dbo].[interfacedatareferraldr](
	[idd_id] [uniqueidentifier] NOT NULL,
	[idd_ipm_id] [uniqueidentifier] NULL,
	[idd_prr_id] [uniqueidentifier] NULL,
	[idd_processed] [bit] NULL,
	[idd_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
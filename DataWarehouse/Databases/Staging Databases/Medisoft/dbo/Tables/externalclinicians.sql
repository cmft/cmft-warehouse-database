﻿CREATE TABLE [dbo].[externalclinicians](
	[exc_id] [uniqueidentifier] NOT NULL,
	[exc_firstname] [nvarchar](50) NULL,
	[exc_title] [nvarchar](50) NULL,
	[exc_surname] [nvarchar](50) NULL,
	[exc_hos_id] [uniqueidentifier] NULL,
	[exc_qualification] [nvarchar](255) NULL,
	[exc_signoff] [nvarchar](255) NULL,
	[exc_active] [bit] NULL
) ON [PRIMARY]
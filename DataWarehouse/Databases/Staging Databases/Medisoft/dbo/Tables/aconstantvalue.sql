﻿CREATE TABLE [dbo].[aconstantvalue](
	[acv_id] [uniqueidentifier] NOT NULL,
	[acv_iac_id] [uniqueidentifier] NOT NULL,
	[acv_rft_id_aconstanttype] [uniqueidentifier] NOT NULL,
	[acv_rft_id_iolformula] [uniqueidentifier] NULL,
	[acv_aconstant] [float] NULL,
	[acv_number] [int] NULL,
	[acv_a0] [float] NULL,
	[acv_a1] [float] NULL,
	[acv_a2] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[auditvalue](
	[adv_id] [uniqueidentifier] NOT NULL,
	[adv_aud_id] [uniqueidentifier] NULL,
	[aud_columnname] [varchar](255) NULL,
	[aud_oldvalue] [varchar](1000) NULL,
	[aud_newvalue] [varchar](1000) NULL,
	[aud_oldvalueres] [varchar](1000) NULL,
	[aud_newvalueres] [varchar](1000) NULL,
	[aud_resolvednew] [bit] NULL DEFAULT (0),
	[aud_resolvedold] [bit] NULL DEFAULT (0)
) ON [PRIMARY]
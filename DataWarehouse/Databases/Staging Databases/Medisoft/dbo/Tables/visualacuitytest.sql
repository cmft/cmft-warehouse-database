﻿CREATE TABLE [dbo].[visualacuitytest](
	[vat_id] [uniqueidentifier] NOT NULL,
	[vat_pat_id] [uniqueidentifier] NOT NULL,
	[vat_test] [varchar](255) NOT NULL,
	[vat_eye_id] [uniqueidentifier] NOT NULL,
	[vat_use_id] [uniqueidentifier] NOT NULL,
	[vat_date] [datetime] NOT NULL,
	[vat_notation] [int] NOT NULL,
	[vat_score] [int] NOT NULL,
	[vat_chart] [uniqueidentifier] NOT NULL,
	[vat_distance] [uniqueidentifier] NOT NULL,
	[vat_chartsize] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[interfacedatavisupac](
	[idv_id] [uniqueidentifier] NOT NULL,
	[idv_ipm_id] [uniqueidentifier] NULL,
	[idv_visupac_visitid] [int] NOT NULL,
	[idv_visupac_patientid] [int] NOT NULL,
	[idv_date] [datetime] NULL,
	[idv_processed] [bit] NULL,
	[idv_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
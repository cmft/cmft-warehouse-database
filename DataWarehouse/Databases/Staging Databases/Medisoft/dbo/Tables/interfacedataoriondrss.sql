﻿CREATE TABLE [dbo].[interfacedataoriondrss](
	[idr_id] [uniqueidentifier] NOT NULL,
	[idr_ipm_id] [uniqueidentifier] NULL,
	[idr_oriondrss_id] [varchar](36) NULL,
	[idr_date] [datetime] NULL,
	[idr_processed] [bit] NULL,
	[idr_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
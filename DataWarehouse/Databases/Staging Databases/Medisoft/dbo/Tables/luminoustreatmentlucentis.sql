﻿CREATE TABLE [dbo].[luminoustreatmentlucentis](
	[lui_id] [uniqueidentifier] NOT NULL,
	[lui_fks_id] [uniqueidentifier] NULL,
	[lui_uploaddate] [datetime] NULL,
	[lui_commitdate] [datetime] NULL,
	[lui_createdate] [datetime] NULL DEFAULT (getdate()),
	[lui_savedate] [datetime] NULL,
	[lui_epi_id] [uniqueidentifier] NULL,
	[lui_site] [int] NULL,
	[lui_reason] [varchar](50) NULL,
	[lui_prstdt] [datetime] NULL,
	[lui_trtno] [int] NULL
) ON [PRIMARY]
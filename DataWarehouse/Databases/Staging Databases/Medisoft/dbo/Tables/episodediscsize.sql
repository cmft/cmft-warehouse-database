﻿CREATE TABLE [dbo].[episodediscsize](
	[vds_id] [uniqueidentifier] NOT NULL,
	[vds_epi_id] [uniqueidentifier] NULL,
	[vds_eye_id] [uniqueidentifier] NULL,
	[vds_discsize] [float] NULL,
	[vds_cdratio] [float] NULL,
	[vds_rft_id_lens] [uniqueidentifier] NULL
) ON [PRIMARY]
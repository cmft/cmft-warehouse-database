﻿CREATE TABLE [dbo].[luminousderivedterms](
	[lsi_id] [uniqueidentifier] NOT NULL,
	[lsi_fks_id] [uniqueidentifier] NOT NULL,
	[lsi_dataitem] [varchar](20) NOT NULL,
	[lsi_lmp_id] [uniqueidentifier] NULL,
	[lsi_epi_id] [uniqueidentifier] NULL,
	[lsi_eye_id] [uniqueidentifier] NULL,
	[lsi_value] [varchar](255) NULL
) ON [PRIMARY]
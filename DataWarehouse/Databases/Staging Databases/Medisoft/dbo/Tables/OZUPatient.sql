﻿CREATE TABLE [dbo].[OZUPatient](
	[ExtractID] [int] NOT NULL,
	[PatID] [uniqueidentifier] NOT NULL,
	[Gender] [int] NULL,
	[Ethnic] [varchar](10) NULL,
	[EthnicDescription] [varchar](255) NULL,
	[PatDOB] [datetime] NULL,
	[PatDOD] [datetime] NULL,
	[FirstEpisodeDate] [datetime] NULL,
	[FirstInjectionDate] [datetime] NULL
) ON [PRIMARY]
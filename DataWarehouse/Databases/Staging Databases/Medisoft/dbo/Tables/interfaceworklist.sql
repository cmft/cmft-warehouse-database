﻿CREATE TABLE [dbo].[interfaceworklist](
	[ihw_id] [uniqueidentifier] NOT NULL,
	[ihw_date] [datetime] NULL,
	[ihw_pat_id] [uniqueidentifier] NULL,
	[ihw_interfacename] [varchar](25) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[hl7_scheduling_bak](
	[sch_id] [uniqueidentifier] NOT NULL,
	[sch_Appointment_ID] [varchar](255) NULL,
	[sch_pat_number] [varchar](255) NULL,
	[sch_pat_nhsno] [varchar](255) NULL,
	[sch_pat_surname] [varchar](255) NULL,
	[sch_pat_dob] [varchar](255) NULL,
	[sch_consultant_code] [varchar](255) NULL,
	[sch_consultant_name] [varchar](255) NULL,
	[sch_appointment_date] [varchar](255) NULL,
	[sch_clinic_code] [varchar](255) NULL,
	[sch_clinic_name] [varchar](255) NULL,
	[sch_clinic_type] [varchar](255) NULL,
	[sch_timestamp] [datetime] NULL
) ON [PRIMARY]
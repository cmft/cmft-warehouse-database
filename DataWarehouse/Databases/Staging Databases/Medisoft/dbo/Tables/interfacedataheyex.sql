﻿CREATE TABLE [dbo].[interfacedataheyex](
	[ihx_id] [uniqueidentifier] NOT NULL,
	[ihx_ipm_id] [uniqueidentifier] NULL,
	[ihx_datetime] [datetime] NULL,
	[ihx_devtypelongname] [varchar](255) NULL,
	[ihx_devtypeshortname] [varchar](255) NULL,
	[ihx_studyname] [varchar](255) NULL,
	[ihx_processed] [bit] NULL,
	[ihx_epi_id] [uniqueidentifier] NULL,
	[ihx_db_path] [varchar](255) NULL
) ON [PRIMARY]
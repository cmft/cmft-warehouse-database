﻿CREATE TABLE [dbo].[OZUEpisodeTreatmentCoPathology](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[TreatmentID] [uniqueidentifier] NOT NULL,
	[CoPathology] [varchar](255) NULL
) ON [PRIMARY]
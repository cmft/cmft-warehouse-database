﻿CREATE TABLE [dbo].[outputdatastage_ivistoppedreason](
	[psr_pat_id] [uniqueidentifier] NULL,
	[psr_eye_id] [uniqueidentifier] NULL,
	[psr_date] [datetime] NULL,
	[psr_reasoncount] [int] NOT NULL,
	[psr_monthbl14] [int] NULL,
	[psr_reasongroup] [varchar](47) NULL,
	[psr_reasongroupcount] [int] NOT NULL,
	[psr_injafter] [datetime] NULL,
	[psr_year] [int] NULL,
	[psr_nhsyear] [int] NULL,
	[rpe] [int] NOT NULL,
	[vab] [int] NOT NULL
) ON [PRIMARY]
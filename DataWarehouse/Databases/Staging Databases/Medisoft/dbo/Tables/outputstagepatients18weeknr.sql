﻿CREATE TABLE [dbo].[outputstagepatients18weeknr](
	[CriteriaDate] [datetime] NULL,
	[CriteriaTime] [varchar](255) NULL,
	[CriteriaTimeNumber] [decimal](5, 2) NULL,
	[HospitalNumber] [varchar](255) NULL,
	[EpisodeType] [varchar](255) NULL,
	[EpisodeTypeID] [uniqueidentifier] NULL,
	[EpisodeName] [varchar](255) NULL,
	[SeenBy] [varchar](255) NULL,
	[SeenByID] [uniqueidentifier] NULL,
	[PatientName] [varchar](255) NULL,
	[PatientID] [uniqueidentifier] NULL,
	[HospitalID] [uniqueidentifier] NULL,
	[PatDOB] [datetime] NULL,
	[EpisodeID] [uniqueidentifier] NULL
) ON [PRIMARY]
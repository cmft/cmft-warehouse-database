﻿CREATE TABLE [dbo].[measurementepisode](
	[epm_id] [uniqueidentifier] NOT NULL,
	[epm_use_id] [uniqueidentifier] NULL,
	[epm_date] [datetime] NULL
) ON [PRIMARY]
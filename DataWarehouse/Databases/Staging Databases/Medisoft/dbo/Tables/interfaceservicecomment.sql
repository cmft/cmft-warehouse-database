﻿CREATE TABLE [dbo].[interfaceservicecomment](
	[isc_id] [uniqueidentifier] NOT NULL,
	[isc_its_id] [uniqueidentifier] NULL,
	[isc_comment] [varchar](255) NULL,
	[isc_user] [varchar](255) NULL,
	[isc_timestamp] [datetime] NULL
) ON [PRIMARY]
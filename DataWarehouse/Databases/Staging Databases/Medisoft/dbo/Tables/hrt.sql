﻿CREATE TABLE [dbo].[hrt](
	[hrt_id] [uniqueidentifier] NOT NULL,
	[hrt_use_id] [uniqueidentifier] NULL,
	[hrt_date] [datetime] NULL
) ON [PRIMARY]
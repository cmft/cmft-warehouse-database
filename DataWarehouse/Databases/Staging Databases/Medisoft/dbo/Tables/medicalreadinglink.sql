﻿CREATE TABLE [dbo].[medicalreadinglink](
	[mlk_id] [uniqueidentifier] NOT NULL,
	[mlk_med_id] [uniqueidentifier] NULL,
	[mlk_epl_id] [uniqueidentifier] NULL,
	[mlk_value] [varchar](255) NULL,
	[mlk_time] [varchar](255) NULL,
	[mlk_measure] [int] NULL
) ON [PRIMARY]
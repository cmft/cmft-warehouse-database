﻿CREATE TABLE [dbo].[clinicalertcondition](
	[cac_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[cac_cal_id] [uniqueidentifier] NOT NULL,
	[cac_lateralised] [bit] NULL DEFAULT (0),
	[cac_order] [int] NULL,
	[cac_disabled] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OZUEpisodeAnaesthesiaComplication](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[Type] [varchar](255) NULL,
	[Description] [varchar](255) NULL
) ON [PRIMARY]
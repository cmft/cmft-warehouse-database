﻿CREATE TABLE [dbo].[outputdatastage_ivipostopcomp](
	[poc_pat_id] [uniqueidentifier] NULL,
	[poc_eye_id] [uniqueidentifier] NULL,
	[poc_drug] [varchar](25) NULL,
	[poc_comp] [varchar](255) NOT NULL
) ON [PRIMARY]
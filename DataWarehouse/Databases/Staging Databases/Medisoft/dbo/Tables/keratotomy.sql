﻿CREATE TABLE [dbo].[keratotomy](
	[ker_id] [uniqueidentifier] NOT NULL,
	[ker_noofincisions] [tinyint] NULL,
	[ker_rft_id_type] [uniqueidentifier] NULL,
	[ker_meridian] [smallint] NULL,
	[ker_rft_id_diameter] [uniqueidentifier] NULL,
	[ker_depth] [int] NULL,
	[ker_arcdegrees] [float] NULL,
	[ker_arcmm] [float] NULL
) ON [PRIMARY]
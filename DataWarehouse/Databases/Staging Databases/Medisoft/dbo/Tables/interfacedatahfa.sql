﻿CREATE TABLE [dbo].[interfacedatahfa](
	[idh_id] [uniqueidentifier] NOT NULL,
	[idh_ipm_id] [uniqueidentifier] NULL,
	[idh_hfa_id] [int] NOT NULL,
	[idh_processed] [bit] NULL,
	[idh_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
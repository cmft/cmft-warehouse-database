﻿CREATE TABLE [dbo].[clinicliststatusarchive](
	[cla_id] [uniqueidentifier] NOT NULL,
	[cla_ica_id] [uniqueidentifier] NULL,
	[cla_arrivaldatetime] [datetime] NULL,
	[cla_nursedatetime] [datetime] NULL,
	[cla_doctordatetime] [datetime] NULL,
	[cla_use_id_seenby] [uniqueidentifier] NULL,
	[cla_18week] [varchar](5) NULL,
	[cla_flags] [int] NULL,
	[cla_updateddatetime] [datetime] NULL,
	[cla_rft_id_transport] [uniqueidentifier] NULL,
	[cla_booked] [bit] NULL,
	[cla_listed] [bit] NULL,
	[cla_clinicdate] [datetime] NULL,
	[cla_ety_id] [uniqueidentifier] NULL,
	[cla_use_id_followup] [uniqueidentifier] NULL,
	[cla_appointment] [varchar](255) NULL,
	[cla_rft_id_appointment] [uniqueidentifier] NULL,
	[cla_epi_id] [uniqueidentifier] NULL,
	[cla_pop_id] [uniqueidentifier] NULL,
	[cla_anaesthesia] [varchar](255) NULL,
	[cla_asagrade] [varchar](2) NULL,
	[cla_opdetails] [varchar](255) NULL,
	[cla_use_id_consultant] [uniqueidentifier] NULL,
	[cla_opname] [varchar](255) NULL,
	[cla_use_id_seenbydoctor] [uniqueidentifier] NULL,
	[cla_opdatetime] [datetime] NULL,
	[cla_comment] [varchar](500) NULL,
	[cla_cco_id] [uniqueidentifier] NULL,
	[cla_visitno] [int] NULL,
	[cla_autoupdate] [bit] NULL,
	[cla_waitdatetime] [datetime] NULL,
	[cla_comment2] [varchar](500) NULL,
	[cla_18weekpas] [varchar](5) NULL,
	[cla_pasupdated] [varchar](500) NULL,
	[cla_pasupdatedtime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[interfacelog](
	[log_id] [uniqueidentifier] NOT NULL,
	[log_date] [datetime] NULL,
	[log_thread] [varchar](255) NULL,
	[log_level] [varchar](50) NULL,
	[log_logger] [varchar](255) NULL,
	[log_user] [varchar](50) NULL,
	[log_computername] [varchar](255) NULL,
	[log_message] [varchar](4000) NULL,
	[log_exception] [varchar](2000) NULL
) ON [PRIMARY]
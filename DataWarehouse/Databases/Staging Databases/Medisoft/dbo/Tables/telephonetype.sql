﻿CREATE TABLE [dbo].[telephonetype](
	[tty_id] [uniqueidentifier] NOT NULL,
	[tty_code] [varchar](255) NOT NULL,
	[tty_desc] [varchar](255) NOT NULL
) ON [PRIMARY]
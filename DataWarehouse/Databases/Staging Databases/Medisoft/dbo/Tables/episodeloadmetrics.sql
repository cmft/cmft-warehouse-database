﻿CREATE TABLE [dbo].[episodeloadmetrics](
	[elt_id] [uniqueidentifier] NOT NULL,
	[elt_epi_id] [uniqueidentifier] NOT NULL,
	[elt_machine] [varchar](255) NULL,
	[elt_concurrencylevel1] [int] NULL,
	[elt_concurrencylevel2] [int] NULL,
	[elt_dateloaded] [datetime] NOT NULL,
	[elt_duration] [int] NULL,
	[elt_type] [int] NULL,
	[elt_bandwith] [int] NULL
) ON [PRIMARY]
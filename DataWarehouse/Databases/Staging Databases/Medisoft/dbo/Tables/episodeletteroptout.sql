﻿CREATE TABLE [dbo].[episodeletteroptout](
	[elo_id] [uniqueidentifier] NOT NULL,
	[elo_epi_id] [uniqueidentifier] NOT NULL,
	[elo_let_id] [uniqueidentifier] NOT NULL,
	[elo_fks_id] [uniqueidentifier] NOT NULL,
	[elo_batch] [bit] NULL
) ON [PRIMARY]
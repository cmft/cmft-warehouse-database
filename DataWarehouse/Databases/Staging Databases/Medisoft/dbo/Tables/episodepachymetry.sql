﻿CREATE TABLE [dbo].[episodepachymetry](
	[epp_id] [uniqueidentifier] NOT NULL,
	[epp_eye_id] [uniqueidentifier] NOT NULL,
	[epp_epi_id] [uniqueidentifier] NOT NULL,
	[epp_value] [int] NULL,
	[epp_correctionf] [decimal](5, 2) NULL,
	[epp_stdev] [decimal](5, 2) NULL
) ON [PRIMARY]
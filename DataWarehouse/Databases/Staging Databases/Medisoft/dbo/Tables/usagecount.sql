﻿CREATE TABLE [dbo].[usagecount](
	[usc_id] [uniqueidentifier] NOT NULL,
	[usc_fks_id] [uniqueidentifier] NOT NULL,
	[usc_count] [int] NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[anaesthesiamedicalreading](
	[amr_id] [uniqueidentifier] NOT NULL,
	[amr_ana_id] [uniqueidentifier] NOT NULL,
	[amr_rft_id_type] [uniqueidentifier] NOT NULL,
	[amr_time] [varchar](255) NULL
) ON [PRIMARY]
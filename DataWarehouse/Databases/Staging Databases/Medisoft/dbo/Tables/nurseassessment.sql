﻿CREATE TABLE [dbo].[nurseassessment](
	[nua_id] [uniqueidentifier] NOT NULL,
	[nua_use_id_nurse] [uniqueidentifier] NULL,
	[nua_use_id_consultant] [uniqueidentifier] NULL,
	[nua_inclinic] [bit] NOT NULL,
	[nua_bytelephone] [bit] NOT NULL,
	[nua_noliquidfrom] [varchar](255) NULL,
	[nua_rft_id_leaflet] [uniqueidentifier] NULL,
	[nua_rft_id_languageleaflet] [uniqueidentifier] NULL,
	[nua_epi_id_plannedoperation] [uniqueidentifier] NULL,
	[nua_datelisted] [datetime] NULL,
	[nua_surgerycancelled] [bit] NULL,
	[nua_eye_id_dilated] [uniqueidentifier] NULL,
	[nua_kin_id] [uniqueidentifier] NULL,
	[nua_nofoodfrom] [varchar](255) NULL,
	[nua_use_id_cancelled] [uniqueidentifier] NULL,
	[nua_virtualreview] [bit] NULL
) ON [PRIMARY]
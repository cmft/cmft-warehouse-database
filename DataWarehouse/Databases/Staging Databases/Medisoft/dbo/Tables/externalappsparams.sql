﻿CREATE TABLE [dbo].[externalappsparams](
	[xxp_id] [int] IDENTITY(1,1) NOT NULL,
	[xxp_xap_id] [int] NOT NULL,
	[xxp_param] [int] NULL,
	[xxp_order] [int] NULL,
	[xxp_active] [int] NULL
) ON [PRIMARY]
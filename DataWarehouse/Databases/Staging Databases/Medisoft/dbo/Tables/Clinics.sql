﻿CREATE TABLE [dbo].[Clinics](
	[ClinicID] [varchar](50) NOT NULL,
	[ClinicName] [varchar](50) NOT NULL,
	[ClinicAbrv] [varchar](50) NOT NULL,
	[ClinicDrName] [varchar](50) NOT NULL,
	[Selectable] [int] NOT NULL,
	[LocationId] [int] NOT NULL,
	[RedFlagCounter] [int] NOT NULL
) ON [PRIMARY]
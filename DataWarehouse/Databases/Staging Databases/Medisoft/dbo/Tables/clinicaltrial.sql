﻿CREATE TABLE [dbo].[clinicaltrial](
	[trl_id] [uniqueidentifier] NOT NULL,
	[trl_desc] [varchar](255) NOT NULL,
	[trl_type] [int] NULL,
	[trl_from] [datetime] NULL,
	[trl_to] [datetime] NULL
) ON [PRIMARY]
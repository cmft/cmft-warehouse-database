﻿CREATE TABLE [dbo].[ssrsreportaccess](
	[sra_id] [int] IDENTITY(1,1) NOT NULL,
	[sra_use_id] [int] NOT NULL,
	[sra_report_id] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[iolrange](
	[ilr_id] [uniqueidentifier] NOT NULL,
	[ilr_iol_id] [uniqueidentifier] NULL,
	[ilr_range_low] [float] NULL,
	[ilr_range_high] [float] NULL,
	[ilr_increment] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[interfacedataophthalsuitelogid](
	[ibl_id] [uniqueidentifier] NOT NULL,
	[ibl_log_id] [int] NULL,
	[ibl_exam_id] [int] NULL,
	[ibl_log_datetime] [datetime] NULL,
	[ibl_log_type] [varchar](255) NULL
) ON [PRIMARY]
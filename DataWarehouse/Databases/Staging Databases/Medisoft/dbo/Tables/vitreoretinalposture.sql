﻿CREATE TABLE [dbo].[vitreoretinalposture](
	[vtp_id] [uniqueidentifier] NOT NULL,
	[vtp_fks_id] [uniqueidentifier] NULL,
	[vtp_rft_id_posture] [uniqueidentifier] NULL,
	[vtp_rft_id_time] [uniqueidentifier] NULL,
	[vtp_days] [int] NULL,
	[vtp_hours] [int] NULL
) ON [PRIMARY]
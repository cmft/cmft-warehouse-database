﻿CREATE TABLE [dbo].[hl7_event](
	[evn_event] [varchar](10) NOT NULL,
	[evn_description] [varchar](255) NULL,
	[evn_storedprocedure] [varchar](255) NULL,
	[evn_storedprocedure_active] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[cliniclistvisitstepsteps](
	[css_id] [int] IDENTITY(1,1) NOT NULL,
	[css_cst_id] [int] NOT NULL,
	[css_previousstep] [int] NULL,
	[css_nextstep] [int] NULL,
	[css_subsequentsteps] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[anaesthetistdefaultdruglink](
	[adf_id] [uniqueidentifier] NOT NULL,
	[adf_use_id] [uniqueidentifier] NULL,
	[adf_dmi_id] [uniqueidentifier] NULL,
	[adf_rft_id_technique] [uniqueidentifier] NULL,
	[adf_hyalase] [float] NULL,
	[adf_dro_id] [uniqueidentifier] NULL,
	[adf_rft_id_site] [uniqueidentifier] NULL,
	[adf_rft_id_needle] [uniqueidentifier] NULL,
	[adf_oct_id] [uniqueidentifier] NULL,
	[adf_volume] [float] NULL
) ON [PRIMARY]
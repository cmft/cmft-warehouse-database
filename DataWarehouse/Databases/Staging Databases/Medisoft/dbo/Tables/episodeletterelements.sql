﻿CREATE TABLE [dbo].[episodeletterelements](
	[elm_id] [uniqueidentifier] NOT NULL,
	[elm_epi_id] [uniqueidentifier] NOT NULL,
	[elm_let_id] [uniqueidentifier] NOT NULL,
	[elm_rft_id_elementtype] [uniqueidentifier] NOT NULL,
	[elm_content] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
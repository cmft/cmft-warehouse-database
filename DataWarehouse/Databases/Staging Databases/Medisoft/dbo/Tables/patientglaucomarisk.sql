﻿CREATE TABLE [dbo].[patientglaucomarisk](
	[pgr_id] [uniqueidentifier] NOT NULL,
	[pgr_pat_id] [uniqueidentifier] NOT NULL,
	[pgr_eye_id] [uniqueidentifier] NOT NULL,
	[pgr_risk] [numeric](15, 2) NOT NULL,
	[pgr_risksource] [int] NOT NULL,
	[pgr_risklabel] [varchar](255) NOT NULL,
	[pgr_timestamp] [datetime] NULL
) ON [PRIMARY]
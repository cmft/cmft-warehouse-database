﻿CREATE TABLE [dbo].[episodenote](
	[epn_id] [uniqueidentifier] NOT NULL,
	[epn_epi_id] [uniqueidentifier] NULL,
	[epn_content] [ntext] NULL,
	[epn_datecreated] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
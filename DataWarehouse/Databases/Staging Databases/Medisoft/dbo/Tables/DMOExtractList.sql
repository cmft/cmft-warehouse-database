﻿CREATE TABLE [dbo].[DMOExtractList](
	[ExtractSuffix] [varchar](100) NOT NULL,
	[DataDescr] [varchar](100) NOT NULL,
	[SeqNum] [int] NOT NULL,
	[ForExport] [bit] NOT NULL,
	[NoSP] [bit] NULL,
	[ParamExtractID] [bit] NULL,
	[ParamLastExtractDate] [bit] NULL,
	[ParamStartExtractDate] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputstagewashauto](
	[CriteriaDate] [datetime] NULL,
	[use_id] [uniqueidentifier] NULL,
	[c_sec_fullname] [nvarchar](100) NULL,
	[Process] [varchar](255) NULL,
	[ProcessID] [uniqueidentifier] NULL,
	[wsh_cycle] [int] NULL,
	[Equipment] [varchar](255) NULL,
	[EquipmentID] [uniqueidentifier] NULL,
	[Location] [varchar](100) NULL,
	[LocationID] [uniqueidentifier] NULL,
	[OperationDates] [varchar](2500) NULL,
	[PatientNumbers] [varchar](2000) NULL,
	[PatientNames] [varchar](2500) NULL,
	[TableName] [varchar](40) NULL,
	[NextWashDate] [datetime] NULL,
	[DefinedId] [int] IDENTITY(0,1) NOT NULL
) ON [PRIMARY]
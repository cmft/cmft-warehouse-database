﻿CREATE TABLE [dbo].[episodediabeticassesmentgrade](
	[dag_id] [uniqueidentifier] NOT NULL,
	[dag_epi_id] [uniqueidentifier] NOT NULL,
	[dag_eye_id] [uniqueidentifier] NULL,
	[dag_rft_id_gradeclass] [uniqueidentifier] NULL,
	[dag_gradeno] [int] NULL,
	[dag_gradechar] [varchar](50) NULL
) ON [PRIMARY]
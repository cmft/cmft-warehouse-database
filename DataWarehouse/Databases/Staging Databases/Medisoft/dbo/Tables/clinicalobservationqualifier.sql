﻿CREATE TABLE [dbo].[clinicalobservationqualifier](
	[coq_cob_id] [uniqueidentifier] NOT NULL,
	[coq_ctm_id_qualifier] [uniqueidentifier] NOT NULL,
	[coq_id] [uniqueidentifier] NOT NULL DEFAULT (newid())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[patientdiabeticstore](
	[pds_id] [uniqueidentifier] NOT NULL,
	[pds_pat_id] [uniqueidentifier] NULL,
	[pds_status_date] [datetime] NULL,
	[pds_valid_to] [datetime] NULL,
	[pds_valid_from] [datetime] NULL,
	[pds_status] [int] NULL,
	[pds_source] [int] NULL,
	[pds_epi_id] [uniqueidentifier] NULL,
	[pds_timestamp] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodenrvisualacuitylink](
	[enl_id] [uniqueidentifier] NOT NULL,
	[enl_epi_id] [uniqueidentifier] NULL,
	[enl_eye_id] [uniqueidentifier] NULL,
	[enl_rft_id_unaided] [uniqueidentifier] NULL,
	[enl_rft_id_bestcorrected] [uniqueidentifier] NULL,
	[enl_lens] [smallint] NULL CONSTRAINT [DF_episodenrvisualacuitylink_enl_lens]  DEFAULT (1),
	[enl_rft_id_measurementtype] [uniqueidentifier] NULL,
	[enl_measurementdistance] [int] NULL
) ON [PRIMARY]
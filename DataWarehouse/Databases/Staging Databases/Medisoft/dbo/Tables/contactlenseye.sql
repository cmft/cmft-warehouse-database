﻿CREATE TABLE [dbo].[contactlenseye](
	[cle_id] [uniqueidentifier] NOT NULL,
	[cle_cln_id] [uniqueidentifier] NULL,
	[cle_eye_id] [uniqueidentifier] NULL,
	[cle_rft_id_type] [uniqueidentifier] NULL,
	[cle_rft_id_watercontent] [uniqueidentifier] NULL,
	[cle_oxygenpermeability] [int] NULL,
	[cle_rft_id_supplier] [uniqueidentifier] NULL,
	[cle_rft_id_name] [uniqueidentifier] NULL,
	[cle_rft_id_material] [uniqueidentifier] NULL,
	[cle_rft_id_design] [uniqueidentifier] NULL,
	[cle_size] [decimal](5, 2) NULL,
	[cle_shape] [decimal](5, 2) NULL,
	[cle_backvertexpower] [decimal](5, 2) NULL,
	[cle_rft_id_colortint] [uniqueidentifier] NULL,
	[cle_rft_id_colorshade] [uniqueidentifier] NULL
) ON [PRIMARY]
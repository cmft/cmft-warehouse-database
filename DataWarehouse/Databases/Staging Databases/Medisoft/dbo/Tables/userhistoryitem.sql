﻿CREATE TABLE [dbo].[userhistoryitem](
	[uhd_id] [uniqueidentifier] NOT NULL,
	[uhd_uhi_id] [uniqueidentifier] NOT NULL,
	[uhd_no] [int] NULL,
	[uhd_item] [varchar](255) NULL,
	[uhd_detail] [varchar](255) NULL
) ON [PRIMARY]
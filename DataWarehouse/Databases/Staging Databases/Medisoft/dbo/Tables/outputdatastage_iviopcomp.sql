﻿CREATE TABLE [dbo].[outputdatastage_iviopcomp](
	[opc_pat_id] [uniqueidentifier] NULL,
	[opc_eye_id] [uniqueidentifier] NULL,
	[opc_drug] [varchar](25) NULL,
	[opc_comp] [varchar](255) NOT NULL
) ON [PRIMARY]
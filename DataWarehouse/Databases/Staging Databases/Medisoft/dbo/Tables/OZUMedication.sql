﻿CREATE TABLE [dbo].[OZUMedication](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[EpisodeDate] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[StopDate] [datetime] NULL,
	[Drug] [varchar](100) NULL,
	[Route] [varchar](100) NULL,
	[Dose] [varchar](100) NULL,
	[Frequency] [varchar](100) NULL,
	[Duration] [varchar](100) NULL,
	[ContinueIndefinitely] [bit] NULL,
	[AddedByPrescription] [bit] NULL,
	[UserRecordedBy] [uniqueidentifier] NULL
) ON [PRIMARY]
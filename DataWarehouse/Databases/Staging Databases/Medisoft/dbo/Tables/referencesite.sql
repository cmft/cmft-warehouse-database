﻿CREATE TABLE [dbo].[referencesite](
	[rft_id] [uniqueidentifier] NOT NULL,
	[rft_code] [varchar](255) NOT NULL,
	[rft_desc] [varchar](255) NOT NULL,
	[rft_question_type] [int] NULL,
	[rft_order] [int] NOT NULL,
	[rft_available] [bit] NOT NULL,
	[rft_rfy_id] [uniqueidentifier] NULL,
	[rft_fixed] [bit] NULL,
	[rft_examination] [bit] NULL,
	[rft_biometrytype] [tinyint] NULL,
	[rft_highlight_answer] [int] NULL,
	[rft_lookup] [varchar](50) NULL,
	[rft_membershipnumber] [int] NOT NULL,
	[rft_number] [decimal](5, 2) NULL,
	[rft_desc_alt] [varchar](255) NULL
) ON [PRIMARY]
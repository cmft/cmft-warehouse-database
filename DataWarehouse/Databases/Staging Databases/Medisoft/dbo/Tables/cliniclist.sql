﻿CREATE TABLE [dbo].[cliniclist](
	[clt_id] [int] IDENTITY(1,1) NOT NULL,
	[clt_desc] [varchar](255) NULL,
	[clt_loc_id] [uniqueidentifier] NULL,
	[clt_loc_id_reception] [uniqueidentifier] NULL,
	[clt_code] [varchar](20) NULL,
	[clt_type] [int] NULL DEFAULT ((128)),
	[clt_rft_id_mode] [uniqueidentifier] NULL,
	[clt_use_id_consultant] [uniqueidentifier] NULL,
	[clt_active] [bit] NULL DEFAULT ((1)),
	[clt_instructions] [varchar](1000) NULL,
	[clt_rft_id_vanotation] [uniqueidentifier] NULL,
	[clt_audit_userid] [int] NOT NULL DEFAULT ((-1)),
	[clt_audit_date] [datetime] NOT NULL DEFAULT (getdate()),
	[clt_clerksteps] [int] NULL DEFAULT ((15)),
	[clt_id_guid] [uniqueidentifier] NULL DEFAULT (newid())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[occi](
	[occ_id] [uniqueidentifier] NOT NULL,
	[occ_lineno] [tinyint] NOT NULL,
	[occ_from] [float] NULL,
	[occ_to] [float] NULL,
	[occ_pccilength] [float] NULL,
	[occ_occilength] [float] NULL
) ON [PRIMARY]
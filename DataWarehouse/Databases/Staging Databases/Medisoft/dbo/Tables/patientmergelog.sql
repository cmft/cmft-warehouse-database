﻿CREATE TABLE [dbo].[patientmergelog](
	[mrg_id] [uniqueidentifier] NOT NULL,
	[mrg_pat_id] [uniqueidentifier] NULL,
	[mrg_pat_internalpasnumbers] [varchar](1000) NULL,
	[mrg_pat_nhsno] [varchar](255) NULL,
	[mrg_pat_patientnumbers] [varchar](1000) NULL,
	[mrg_pat_id_old] [uniqueidentifier] NULL,
	[mrg_pat_internalpasnumbers_old] [varchar](1000) NULL,
	[mrg_pat_nhsno_old] [varchar](255) NULL,
	[mrg_pat_patientnumbers_old] [varchar](1000) NULL,
	[mrg_timestamp] [datetime] NULL,
	[mrg_pat_surname] [varchar](255) NULL,
	[mrg_pat_firstname] [varchar](255) NULL,
	[mrg_pat_dob] [datetime] NULL,
	[mrg_pat_episodes] [varchar](max) NULL,
	[mrg_pat_surname_old] [varchar](255) NULL,
	[mrg_pat_firstname_old] [varchar](255) NULL,
	[mrg_pat_dob_old] [datetime] NULL,
	[mrg_pat_episodes_old] [varchar](max) NULL,
	[mrg_validated] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
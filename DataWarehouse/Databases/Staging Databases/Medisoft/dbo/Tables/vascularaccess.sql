﻿CREATE TABLE [dbo].[vascularaccess](
	[vac_id] [uniqueidentifier] NOT NULL,
	[vac_fks_id] [uniqueidentifier] NOT NULL,
	[vac_rft_id_vascularsitevenus1] [uniqueidentifier] NULL,
	[vac_vascularsizevenus1] [uniqueidentifier] NULL,
	[vac_rft_id_vascularsitevenus2] [uniqueidentifier] NULL,
	[vac_vascularsizevenus2] [uniqueidentifier] NULL,
	[vac_rft_id_vascularsitecvp] [uniqueidentifier] NULL,
	[vac_vascularsizecvp] [uniqueidentifier] NULL,
	[vac_rft_id_vascularsitearterial] [uniqueidentifier] NULL,
	[vac_vascularsizearterial] [uniqueidentifier] NULL,
	[vac_vascularaccessnone] [bit] NULL DEFAULT (0)
) ON [PRIMARY]
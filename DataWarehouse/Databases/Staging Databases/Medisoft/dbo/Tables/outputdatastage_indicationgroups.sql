﻿CREATE TABLE [dbo].[outputdatastage_indicationgroups](
	[ing_flagvalue] [int] NULL,
	[ing_desc] [varchar](50) NULL,
	[ing_order] [int] NULL,
	[ing_reportlist] [bit] NULL,
	[ing_systemonly] [bit] NULL
) ON [PRIMARY]
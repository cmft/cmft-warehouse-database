﻿CREATE TABLE [dbo].[ETCEpisodeDRAssessmentMaculopathySimple](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[Eye] [char](1) NULL,
	[UserID] [uniqueidentifier] NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[Centre-involvingCSMO] [int] NULL,
	[Non-centreInvolvingCSMO] [int] NULL,
	[RetinalThickening<1DDCentreOfFovea] [int] NULL,
	[P1Focal/GridToMacula] [int] NULL
) ON [PRIMARY]
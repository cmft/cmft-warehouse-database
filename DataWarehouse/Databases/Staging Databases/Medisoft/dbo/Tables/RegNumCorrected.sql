﻿CREATE TABLE [dbo].[RegNumCorrected](
	[rnc_id] [uniqueidentifier] NULL,
	[rnc_reg_id] [uniqueidentifier] NULL,
	[rnc_ExtractedPatNum] [varchar](100) NULL,
	[rnc_pat_id] [uniqueidentifier] NULL,
	[AlreadyInRegTable] [bit] NULL
) ON [PRIMARY]
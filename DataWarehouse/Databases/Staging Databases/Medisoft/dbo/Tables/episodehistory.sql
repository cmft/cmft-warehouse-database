﻿CREATE TABLE [dbo].[episodehistory](
	[eph_id] [uniqueidentifier] NOT NULL,
	[eph_ety_id] [uniqueidentifier] NULL,
	[eph_pat_id] [uniqueidentifier] NULL,
	[eph_use_id_operator] [uniqueidentifier] NULL,
	[eph_use_id_entered] [uniqueidentifier] NULL,
	[eph_loc_id] [uniqueidentifier] NULL,
	[eph_date] [datetime] NULL,
	[eph_dateentered] [datetime] NULL,
	[eph_dateapprox] [bit] NULL,
	[eph_detailabbrev] [varchar](30) NULL,
	[eph_detailfull] [varchar](2000) NULL,
	[eph_dna] [bit] NULL,
	[eph_rft_id_clinic] [uniqueidentifier] NULL,
	[eph_use_id_consultant] [uniqueidentifier] NULL,
	[eph_rft_id_dnaoutcome] [uniqueidentifier] NULL
) ON [PRIMARY]
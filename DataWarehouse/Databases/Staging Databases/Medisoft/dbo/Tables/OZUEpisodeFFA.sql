﻿CREATE TABLE [dbo].[OZUEpisodeFFA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Eye] [varchar](1) NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[LesionSize] [decimal](5, 2) NULL,
	[LesionUnder50PctFibrosis] [varchar](10) NULL,
	[LesionOver50PctFibrosis] [varchar](10) NULL,
	[PctLesionFibrosis] [int] NULL,
	[AreaHaem] [int] NULL,
	[SubfovealHaem] [varchar](10) NULL,
	[SubfovealAtrophy] [varchar](10) NULL,
	[CystoidMacOedema] [varchar](10) NULL,
	[PED] [varchar](255) NULL
) ON [PRIMARY]
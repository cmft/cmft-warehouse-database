﻿CREATE TABLE [dbo].[drugadministered](
	[dga_id] [uniqueidentifier] NOT NULL,
	[dga_fks_id] [uniqueidentifier] NULL,
	[dga_dru_id] [uniqueidentifier] NULL,
	[dga_dos_id] [uniqueidentifier] NULL,
	[dga_dose] [int] NULL,
	[dga_source] [int] NULL,
	[dga_dategiven] [datetime] NULL,
	[dga_timegiven] [datetime] NULL,
	[dga_use_id_given] [uniqueidentifier] NULL,
	[dga_epi_id] [uniqueidentifier] NULL,
	[dga_desc] [varchar](255) NULL,
	[dga_dro_id] [uniqueidentifier] NULL
) ON [PRIMARY]
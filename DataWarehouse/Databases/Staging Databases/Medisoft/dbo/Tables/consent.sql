﻿CREATE TABLE [dbo].[consent](
	[con_id] [uniqueidentifier] NOT NULL,
	[con_pat_id] [uniqueidentifier] NOT NULL,
	[con_fks_id] [uniqueidentifier] NOT NULL,
	[con_date] [datetime] NOT NULL,
	[con_action] [int] NOT NULL
) ON [PRIMARY]
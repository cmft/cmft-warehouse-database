﻿CREATE TABLE [dbo].[OZUEpisodeClinicalFindings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Eye] [varchar](1) NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[Type] [varchar](255) NULL,
	[Description] [varchar](255) NULL
) ON [PRIMARY]
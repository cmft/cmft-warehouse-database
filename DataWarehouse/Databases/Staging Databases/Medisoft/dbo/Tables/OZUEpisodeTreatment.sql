﻿CREATE TABLE [dbo].[OZUEpisodeTreatment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Eye] [varchar](1) NOT NULL,
	[User] [varchar](36) NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[TreatmentID] [uniqueidentifier] NULL,
	[Type] [varchar](255) NULL,
	[OPCSCode] [varchar](255) NULL,
	[Hypertension] [int] NULL,
	[UserGrade] [varchar](255) NULL,
	[ListedDate] [datetime] NULL,
	[Assistant] [varchar](36) NULL,
	[AssistantGrade] [varchar](255) NULL,
	[IsDayCase] [bit] NULL,
	[LocationType] [varchar](255) NULL
) ON [PRIMARY]
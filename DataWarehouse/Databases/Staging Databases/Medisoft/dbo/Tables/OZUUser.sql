﻿CREATE TABLE [dbo].[OZUUser](
	[UserID] [uniqueidentifier] NOT NULL,
	[CurrentGrade] [varchar](200) NULL,
	[UserType] [varchar](200) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[messagethread](
	[msg_id] [uniqueidentifier] NOT NULL,
	[msg_subject_fks_id] [uniqueidentifier] NULL,
	[msg_rft_id_category] [uniqueidentifier] NULL,
	[msg_startdate] [datetime] NULL
) ON [PRIMARY]
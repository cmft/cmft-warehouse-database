﻿CREATE TABLE [dbo].[luminousmedicationivi](
	[lue_id] [uniqueidentifier] NOT NULL,
	[lue_fks_id] [uniqueidentifier] NULL,
	[lue_uploaddate] [datetime] NULL,
	[lue_commitdate] [datetime] NULL,
	[lue_createdate] [datetime] NULL DEFAULT (getdate()),
	[lue_savedate] [datetime] NULL,
	[lue_epi_id] [uniqueidentifier] NULL,
	[lue_itvtyp] [int] NULL,
	[lue_itvmed] [varchar](50) NULL,
	[lue_itvsite] [int] NULL,
	[lue_itvresn] [varchar](50) NULL,
	[lue_itvstdt] [datetime] NULL,
	[lue_itvendt] [datetime] NULL,
	[lue_itvcont] [bit] NULL,
	[lue_itvnotr] [int] NULL
) ON [PRIMARY]
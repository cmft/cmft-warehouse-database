﻿CREATE TABLE [dbo].[question](
	[que_id] [uniqueidentifier] NOT NULL,
	[que_fks_id] [uniqueidentifier] NOT NULL,
	[que_rft_id_question] [uniqueidentifier] NULL,
	[que_answer] [tinyint] NOT NULL,
	[que_eye_id] [uniqueidentifier] NULL,
	[que_qualifier] [varchar](255) NULL
) ON [PRIMARY]
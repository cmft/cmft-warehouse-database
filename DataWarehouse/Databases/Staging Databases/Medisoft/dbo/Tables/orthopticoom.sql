﻿CREATE TABLE [dbo].[orthopticoom](
	[oom_id] [uniqueidentifier] NOT NULL,
	[oom_ort_id] [uniqueidentifier] NOT NULL,
	[oom_saccades_left] [uniqueidentifier] NULL,
	[oom_saccades_right] [uniqueidentifier] NULL,
	[oom_saccades_up] [uniqueidentifier] NULL,
	[oom_saccades_down] [uniqueidentifier] NULL,
	[oom_okn_left] [uniqueidentifier] NULL,
	[oom_okn_right] [uniqueidentifier] NULL,
	[oom_okn_up] [uniqueidentifier] NULL,
	[oom_okn_down] [uniqueidentifier] NULL,
	[oom_vor_left] [uniqueidentifier] NULL,
	[oom_vor_right] [uniqueidentifier] NULL,
	[oom_vor_up] [uniqueidentifier] NULL,
	[oom_vor_down] [uniqueidentifier] NULL
) ON [PRIMARY]
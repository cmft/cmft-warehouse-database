﻿CREATE TABLE [dbo].[userindications](
	[inh_id] [uniqueidentifier] NULL DEFAULT (newid()),
	[inh_dgn_id] [uniqueidentifier] NULL,
	[inh_use_id] [uniqueidentifier] NULL,
	[inh_desc] [varchar](300) NULL,
	[inh_count] [int] NULL
) ON [PRIMARY]
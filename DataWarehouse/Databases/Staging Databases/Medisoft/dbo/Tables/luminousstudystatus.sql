﻿CREATE TABLE [dbo].[luminousstudystatus](
	[lus_id] [uniqueidentifier] NOT NULL,
	[lus_pat_id] [uniqueidentifier] NOT NULL,
	[lus_subjectno] [varchar](16) NOT NULL,
	[lus_status] [int] NOT NULL
) ON [PRIMARY]
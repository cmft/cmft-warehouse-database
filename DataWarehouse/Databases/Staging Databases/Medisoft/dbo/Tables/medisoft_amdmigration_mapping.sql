﻿CREATE TABLE [dbo].[medisoft_amdmigration_mapping](
	[val_mappingtype] [varchar](255) NOT NULL,
	[val_tomap] [varchar](255) NULL,
	[val_medisoft] [varchar](255) NULL,
	[val_count] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[externalapps](
	[xap_id] [int] IDENTITY(1,1) NOT NULL,
	[xap_name] [varchar](255) NULL,
	[xap_location] [varchar](255) NULL,
	[xap_acl_id] [int] NULL,
	[xap_explicitparms] [bit] NULL,
	[xap_active] [bit] NULL,
	[xap_flag] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
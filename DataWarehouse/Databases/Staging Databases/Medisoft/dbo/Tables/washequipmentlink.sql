﻿CREATE TABLE [dbo].[washequipmentlink](
	[wel_id] [uniqueidentifier] NOT NULL,
	[wel_wsh_id] [uniqueidentifier] NOT NULL,
	[wel_rft_id_equipment] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
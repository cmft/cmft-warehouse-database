﻿CREATE TABLE [dbo].[statemanagementexceptions](
	[stx_id] [uniqueidentifier] NULL,
	[stx_use_id] [uniqueidentifier] NULL,
	[stx_machine] [varchar](255) NULL,
	[stx_lastcontact] [datetime] NULL
) ON [PRIMARY]
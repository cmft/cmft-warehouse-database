﻿CREATE TABLE [dbo].[interfacesetting](
	[ins_id] [uniqueidentifier] NOT NULL,
	[ins_int_id] [uniqueidentifier] NULL,
	[ins_setting] [varchar](255) NULL,
	[ins_setting_name] [varchar](255) NULL,
	[int_setting_id] [int] NULL
) ON [PRIMARY]
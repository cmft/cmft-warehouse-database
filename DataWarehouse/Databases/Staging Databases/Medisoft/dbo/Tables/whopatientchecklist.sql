﻿CREATE TABLE [dbo].[whopatientchecklist](
	[wpc_id] [int] IDENTITY(1,1) NOT NULL,
	[wpc_gid] [uniqueidentifier] NOT NULL,
	[wpc_signin_status] [int] NULL,
	[wpc_timeout_status] [int] NULL,
	[wpc_signout_status] [int] NULL,
	[wpc_epi_operation_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[recoveryinterval](
	[rci_id] [uniqueidentifier] NOT NULL,
	[rci_rcy_id] [uniqueidentifier] NOT NULL,
	[rci_intervalno] [int] NULL,
	[rci_intervalrow] [int] NULL,
	[rci_intervaltype] [int] NULL,
	[rci_units] [decimal](9, 2) NULL,
	[rci_bp1] [varchar](5) NULL,
	[rci_bp2] [varchar](5) NULL,
	[rci_pulse] [varchar](5) NULL
) ON [PRIMARY]
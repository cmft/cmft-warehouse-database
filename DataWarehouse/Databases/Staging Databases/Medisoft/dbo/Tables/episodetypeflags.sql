﻿CREATE TABLE [dbo].[episodetypeflags](
	[efl_id] [uniqueidentifier] NOT NULL,
	[efl_ety_id] [uniqueidentifier] NOT NULL,
	[efl_canflag] [int] NULL,
	[efl_flag] [int] NULL
) ON [PRIMARY]
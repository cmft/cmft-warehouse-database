﻿CREATE TABLE [dbo].[procedurelink](
	[prl_id] [uniqueidentifier] NOT NULL,
	[prl_ope_id] [uniqueidentifier] NULL,
	[prl_pro_id] [uniqueidentifier] NULL,
	[prl_eye_id] [uniqueidentifier] NULL
) ON [PRIMARY]
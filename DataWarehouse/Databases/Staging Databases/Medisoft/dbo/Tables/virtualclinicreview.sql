﻿CREATE TABLE [dbo].[virtualclinicreview](
	[vcl_id] [uniqueidentifier] NOT NULL,
	[vcl_epi_id_requested] [uniqueidentifier] NULL,
	[vcl_reviewcancelled] [bit] NULL,
	[vcl_epi_id_actioned] [uniqueidentifier] NULL,
	[vcl_action] [smallint] NULL,
	[vcl_reviewtype] [int] NULL DEFAULT ((1))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[summaryscreenelement](
	[sce_id] [uniqueidentifier] NULL DEFAULT (newid()),
	[sce_number] [int] NOT NULL,
	[sce_name] [varchar](255) NULL,
	[sce_enabled] [bit] NULL
) ON [PRIMARY]
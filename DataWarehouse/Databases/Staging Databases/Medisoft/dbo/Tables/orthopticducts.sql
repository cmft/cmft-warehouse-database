﻿CREATE TABLE [dbo].[orthopticducts](
	[ord_id] [uniqueidentifier] NOT NULL,
	[ord_ort_id] [uniqueidentifier] NOT NULL,
	[ord_no] [int] NULL,
	[ord_value] [int] NULL,
	[ord_valuerestriction] [int] NULL,
	[ord_rft_id_direction] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[practitioner](
	[ptr_id] [uniqueidentifier] NOT NULL,
	[ptr_code] [varchar](20) NULL,
	[ptr_title] [varchar](10) NULL,
	[ptr_firstname] [varchar](55) NULL,
	[ptr_surname] [varchar](55) NULL,
	[ptr_add_id] [uniqueidentifier] NULL,
	[ptr_typemembership] [int] NOT NULL,
	[ptr_signoff] [varchar](255) NULL
) ON [PRIMARY]
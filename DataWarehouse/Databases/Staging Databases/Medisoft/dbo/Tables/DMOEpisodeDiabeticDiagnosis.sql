﻿CREATE TABLE [dbo].[DMOEpisodeDiabeticDiagnosis](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[IsDiabetic] [bit] NOT NULL,
	[Type] [varchar](255) NULL,
	[ControlledBy] [varchar](255) NULL,
	[AgeDiagnosed] [int] NULL
) ON [PRIMARY]
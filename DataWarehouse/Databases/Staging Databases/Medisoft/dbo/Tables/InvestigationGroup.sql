﻿CREATE TABLE [dbo].[InvestigationGroup](
	[igp_id] [uniqueidentifier] NOT NULL,
	[igp_rft_id_group] [uniqueidentifier] NOT NULL,
	[igp_eye_id] [uniqueidentifier] NOT NULL,
	[igp_ctm_id] [uniqueidentifier] NOT NULL,
	[igp_notes] [varchar](2048) NULL,
	[igp_parsedtext] [varchar](2000) NULL,
	[igp_value] [decimal](10, 3) NULL
) ON [PRIMARY]
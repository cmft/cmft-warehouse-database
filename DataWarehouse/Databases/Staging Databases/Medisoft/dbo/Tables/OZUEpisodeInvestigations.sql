﻿CREATE TABLE [dbo].[OZUEpisodeInvestigations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Investigation] [varchar](255) NULL,
	[Timeframe] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[settingoverride](
	[sor_fks_id] [uniqueidentifier] NULL,
	[sor_set_id] [int] NULL,
	[sor_set_value] [varchar](255) NULL
) ON [PRIMARY]
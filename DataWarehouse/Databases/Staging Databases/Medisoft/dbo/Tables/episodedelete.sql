﻿CREATE TABLE [dbo].[episodedelete](
	[ede_id] [uniqueidentifier] NULL,
	[ede_epi_id] [uniqueidentifier] NULL,
	[ede_pat_id] [uniqueidentifier] NULL,
	[ede_fks_id] [uniqueidentifier] NULL,
	[deletedate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ETCPatient](
	[ExtractID] [int] NOT NULL,
	[PatID] [uniqueidentifier] NOT NULL,
	[Gender] [int] NULL,
	[Ethnic] [varchar](10) NULL,
	[EthnicDescription] [varchar](255) NULL,
	[PatDOB] [datetime] NULL,
	[FirstEpisodeDate] [datetime] NULL,
	[FirstInjectionDate] [datetime] NULL,
	[DOD<30DaysLastInjection] [int] NULL,
	[DOD30-90DaysLastInjection] [int] NULL,
	[DOD91-180DaysLastInjection] [int] NULL,
	[DOD>180DaysLastInjection] [int] NULL,
	[AliveExtractDate] [int] NULL
) ON [PRIMARY]
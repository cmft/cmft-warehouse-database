﻿CREATE TABLE [dbo].[outputstagevirtualclinic](
	[PatientID] [uniqueidentifier] NULL,
	[CriteriaDate] [datetime] NULL,
	[SaveDate] [datetime] NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[PatientDOB] [datetime] NULL,
	[Practitioner] [varchar](255) NULL,
	[PractitionerID] [uniqueidentifier] NULL,
	[PractitionerClinic] [varchar](255) NULL,
	[PractitionerActions] [varchar](2000) NULL,
	[PractitionerRequestDate] [datetime] NULL,
	[Consultant] [varchar](255) NULL,
	[ConsultantID] [uniqueidentifier] NULL,
	[ConsultantActions] [varchar](2000) NULL,
	[ManagementChange] [bit] NULL,
	[EpiIDReview] [uniqueidentifier] NULL,
	[EpiIDRequest] [uniqueidentifier] NULL
) ON [PRIMARY]
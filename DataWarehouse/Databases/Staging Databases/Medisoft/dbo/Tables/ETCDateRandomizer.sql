﻿CREATE TABLE [dbo].[ETCDateRandomizer](
	[PatientID] [uniqueidentifier] NULL,
	[DaysOffset] [int] NULL,
	[DaysOffsetDOB] [int] NULL
) ON [PRIMARY]
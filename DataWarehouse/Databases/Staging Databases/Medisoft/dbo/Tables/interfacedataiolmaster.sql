﻿CREATE TABLE [dbo].[interfacedataiolmaster](
	[idi_id] [uniqueidentifier] NOT NULL,
	[idi_ipm_id] [uniqueidentifier] NULL,
	[idi_datetime] [datetime] NULL,
	[idi_file_path] [varchar](500) NULL,
	[idi_file_type] [varchar](5) NULL,
	[idi_iolmaster] [varchar](50) NULL,
	[idi_axialLengthr] [float] NULL,
	[idi_axialLengthl] [float] NULL,
	[idi_k1dr] [float] NULL,
	[idi_k2dr] [float] NULL,
	[idi_k1mmr] [float] NULL,
	[idi_k2mmr] [float] NULL,
	[idi_k1axisr] [float] NULL,
	[idi_k2axisr] [float] NULL,
	[idi_acdr] [float] NULL,
	[idi_k1dl] [float] NULL,
	[idi_k2dl] [float] NULL,
	[idi_k1mml] [float] NULL,
	[idi_k2mml] [float] NULL,
	[idi_k1axisl] [float] NULL,
	[idi_k2axisl] [float] NULL,
	[idi_acdl] [float] NULL,
	[idi_processed] [bit] NULL,
	[idi_epi_id] [uniqueidentifier] NULL,
	[idi_wtwr] [float] NULL,
	[idi_wtwl] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodemeasurement](
	[ems_id] [uniqueidentifier] NOT NULL,
	[ems_epi_id] [uniqueidentifier] NULL,
	[ems_rft_id_machine] [uniqueidentifier] NULL,
	[ems_rft_id_measurement] [uniqueidentifier] NULL
) ON [PRIMARY]
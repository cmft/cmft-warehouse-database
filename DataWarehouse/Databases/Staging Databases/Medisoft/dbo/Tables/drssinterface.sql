﻿CREATE TABLE [dbo].[drssinterface](
	[dri_id] [uniqueidentifier] NOT NULL,
	[dri_patientid] [varchar](50) NULL,
	[dri_url] [varchar](512) NULL,
	[dri_date] [datetime] NULL,
	[dri_active] [bit] NULL
) ON [PRIMARY]
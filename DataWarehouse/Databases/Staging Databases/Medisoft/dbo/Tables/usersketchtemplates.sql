﻿CREATE TABLE [dbo].[usersketchtemplates](
	[iut_id] [uniqueidentifier] NOT NULL,
	[iut_use_id] [uniqueidentifier] NOT NULL,
	[iut_eye_id] [uniqueidentifier] NULL,
	[iut_imr_id] [uniqueidentifier] NULL
) ON [PRIMARY]
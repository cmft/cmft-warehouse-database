﻿CREATE TABLE [dbo].[telephone](
	[tel_id] [uniqueidentifier] NOT NULL,
	[tel_number] [varchar](255) NOT NULL,
	[tel_tty_id] [uniqueidentifier] NULL
) ON [PRIMARY]
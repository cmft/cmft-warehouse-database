﻿CREATE TABLE [dbo].[gps](
	[gps_id] [uniqueidentifier] NOT NULL,
	[gps_code] [varchar](255) NOT NULL,
	[gps_initials] [varchar](255) NOT NULL,
	[gps_surname] [varchar](255) NOT NULL,
	[gps_localcode] [varchar](255) NOT NULL,
	[gps_pcgcode] [varchar](255) NOT NULL,
	[gps_pasid] [int] NULL,
	[gps_title] [varchar](255) NULL,
	[gps_firstname] [varchar](255) NULL,
	[gps_typemembership] [int] NULL,
	[gps_rft_id_specialty] [uniqueidentifier] NULL
) ON [PRIMARY]
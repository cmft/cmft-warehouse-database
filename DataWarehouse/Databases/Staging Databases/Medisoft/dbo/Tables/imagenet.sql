﻿CREATE TABLE [dbo].[imagenet](
	[imn_id] [uniqueidentifier] NOT NULL,
	[imn_shotprocs] [varchar](255) NOT NULL,
	[imn_database] [varchar](25) NULL
) ON [PRIMARY]
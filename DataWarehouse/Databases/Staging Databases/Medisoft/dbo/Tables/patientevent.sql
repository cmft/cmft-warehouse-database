﻿CREATE TABLE [dbo].[patientevent](
	[pae_id] [uniqueidentifier] NOT NULL,
	[pae_pat_id] [uniqueidentifier] NULL,
	[pae_mrsadate] [datetime] NULL,
	[pae_mrsaoutcome] [int] NULL,
	[pae_epi_id] [uniqueidentifier] NULL,
	[pae_type] [int] NULL,
	[pae_cvidateblind] [datetime] NULL,
	[pae_cvidateblindapprox] [bit] NULL,
	[pae_cvidatepartial] [datetime] NULL,
	[pae_cvidatepartialapprox] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[luminousstudyaudit](
	[lua_id] [uniqueidentifier] NOT NULL,
	[lua_lub_id] [uniqueidentifier] NOT NULL,
	[lua_use_id] [uniqueidentifier] NOT NULL,
	[lua_date] [datetime] NOT NULL,
	[lua_action] [int] NOT NULL
) ON [PRIMARY]
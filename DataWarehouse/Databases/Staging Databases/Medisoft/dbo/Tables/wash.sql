﻿CREATE TABLE [dbo].[wash](
	[wsh_id] [uniqueidentifier] NOT NULL,
	[wsh_date] [datetime] NULL,
	[wsh_daterecorded] [datetime] NULL,
	[wsh_loc_id] [uniqueidentifier] NOT NULL,
	[wsh_use_id] [uniqueidentifier] NOT NULL,
	[wsh_rft_id_process] [uniqueidentifier] NOT NULL,
	[wsh_cycle] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[urlinterface](
	[url_id] [uniqueidentifier] NOT NULL,
	[url_pat_id] [uniqueidentifier] NULL,
	[url_patientid] [varchar](50) NULL,
	[url_url] [varchar](512) NULL,
	[url_date] [datetime] NULL,
	[url_active] [bit] NULL,
	[url_source] [int] NULL,
	[url_firstname] [varchar](255) NULL,
	[url_surname] [varchar](255) NULL,
	[url_dob] [datetime] NULL
) ON [PRIMARY]
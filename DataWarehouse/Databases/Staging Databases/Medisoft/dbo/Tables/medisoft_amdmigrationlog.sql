﻿CREATE TABLE [dbo].[medisoft_amdmigrationlog](
	[log_id] [uniqueidentifier] NOT NULL,
	[log_level] [varchar](20) NULL,
	[log_rowid] [int] NULL,
	[log_message] [varchar](2000) NULL,
	[log_count] [int] NULL,
	[log_timestamp] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[surgerytype](
	[sty_id] [uniqueidentifier] NOT NULL,
	[sty_fks_id] [uniqueidentifier] NULL,
	[sty_rft_id_type] [uniqueidentifier] NULL,
	[sty_typenumber] [int] NULL,
	[sty_typeother] [varchar](255) NULL
) ON [PRIMARY]
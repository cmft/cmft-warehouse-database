﻿CREATE TABLE [dbo].[anaesthesiamedicalreadinglink](
	[aml_id] [uniqueidentifier] NOT NULL,
	[aml_amr_id] [uniqueidentifier] NOT NULL,
	[aml_med_id] [uniqueidentifier] NOT NULL,
	[aml_value] [varchar](255) NULL
) ON [PRIMARY]
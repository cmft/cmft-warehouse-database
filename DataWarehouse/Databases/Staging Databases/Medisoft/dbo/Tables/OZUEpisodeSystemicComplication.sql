﻿CREATE TABLE [dbo].[OZUEpisodeSystemicComplication](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[Description] [varchar](255) NULL
) ON [PRIMARY]
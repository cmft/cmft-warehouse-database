﻿CREATE TABLE [dbo].[episodemeasurementeye](
	[eme_id] [uniqueidentifier] NOT NULL,
	[eme_ems_id] [uniqueidentifier] NOT NULL,
	[eme_eye_id] [uniqueidentifier] NOT NULL,
	[ems_measure] [float] NULL
) ON [PRIMARY]
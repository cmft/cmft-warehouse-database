﻿CREATE TABLE [dbo].[refractionprocess](
	[rpr_id] [uniqueidentifier] NOT NULL,
	[rpr_epi_id] [uniqueidentifier] NULL,
	[rpr_vav_id_binocular] [uniqueidentifier] NULL,
	[rpr_vav_id_binocular_g] [uniqueidentifier] NULL,
	[rpr_rft_id_type] [uniqueidentifier] NULL,
	[rpr_daterecorded] [datetime] NULL,
	[rpr_dateapprox] [int] NULL,
	[rpr_step] [int] NULL,
	[rpr_step_order] [int] NULL,
	[rpr_dru_id] [uniqueidentifier] NULL,
	[rpr_rft_id_binocular] [uniqueidentifier] NULL,
	[rpr_ipd] [float] NULL
) ON [PRIMARY]
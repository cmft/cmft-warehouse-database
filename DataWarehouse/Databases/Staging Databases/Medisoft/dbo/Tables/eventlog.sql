﻿CREATE TABLE [dbo].[eventlog](
	[evt_id] [uniqueidentifier] NOT NULL,
	[evt_rft_id_type] [uniqueidentifier] NOT NULL,
	[evt_use_id] [uniqueidentifier] NULL,
	[evt_pat_id] [uniqueidentifier] NULL,
	[evt_date] [datetime] NULL DEFAULT (getdate()),
	[evt_source] [varchar](50) NULL,
	[evt_data] [varchar](8000) NULL,
	[evt_exception] [bit] NULL DEFAULT ((0))
) ON [PRIMARY]
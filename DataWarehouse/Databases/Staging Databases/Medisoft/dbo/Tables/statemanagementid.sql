﻿CREATE TABLE [dbo].[statemanagementid](
	[std_id] [uniqueidentifier] NOT NULL,
	[std_deviceid] [varchar](32) NULL,
	[std_name] [varchar](32) NULL,
	[std_macaddress] [varchar](17) NULL,
	[std_os] [varchar](255) NULL,
	[std_lastlogin] [datetime] NULL,
	[std_loc_id] [uniqueidentifier] NULL,
	[std_use_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodelink](
	[epl_id] [uniqueidentifier] NOT NULL,
	[epl_epi_id] [uniqueidentifier] NULL,
	[epl_fks_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[alkeratoplasty](
	[alk_id] [uniqueidentifier] NOT NULL,
	[alk_dru_id_preparation] [uniqueidentifier] NULL,
	[alk_rft_id_donortrephine] [uniqueidentifier] NULL,
	[alk_rft_id_donorinstruement] [uniqueidentifier] NULL,
	[alk_donordiameter] [decimal](6, 2) NULL,
	[alk_transplantno] [varchar](25) NULL,
	[alk_rft_id_hosttrephine] [uniqueidentifier] NULL,
	[alk_rft_id_hostinstruement] [uniqueidentifier] NULL,
	[alk_hostdiameter] [decimal](6, 2) NULL,
	[alk_paracentesis] [bit] NULL,
	[alk_rft_id_centration] [uniqueidentifier] NULL,
	[alk_viscoelastic] [bit] NULL,
	[alk_rft_id_viscoelastic] [uniqueidentifier] NULL,
	[alk_rft_id_viscoelasticlocation] [uniqueidentifier] NULL,
	[alk_stromal] [bit] NULL,
	[alk_dalk] [bit] NULL,
	[alk_thickness] [int] NULL,
	[alk_rft_id_technique] [uniqueidentifier] NULL,
	[alk_sutureinteruptedno] [int] NULL,
	[alk_sutureinteruptedthickness] [uniqueidentifier] NULL,
	[alk_suturecontinuous1no] [int] NULL,
	[alk_suturecontinuous1thickness] [uniqueidentifier] NULL,
	[alk_suturecontinuous2no] [int] NULL,
	[alk_suturecontinuous2thickness] [uniqueidentifier] NULL,
	[alk_rft_id_sutureinteruptedmaterial] [uniqueidentifier] NULL,
	[alk_rft_id_suturecontinuous1material] [uniqueidentifier] NULL,
	[alk_rft_id_suturecontinuous2material] [uniqueidentifier] NULL
) ON [PRIMARY]
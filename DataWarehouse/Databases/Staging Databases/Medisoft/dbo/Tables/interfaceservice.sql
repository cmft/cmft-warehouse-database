﻿CREATE TABLE [dbo].[interfaceservice](
	[its_id] [uniqueidentifier] NOT NULL,
	[its_servicename] [varchar](255) NULL,
	[its_displayname] [varchar](255) NULL,
	[its_displaynamefriendly] [varchar](255) NULL,
	[its_servicestatus] [varchar](255) NULL,
	[its_serviceconfigfile] [ntext] NULL,
	[its_querycomputers] [varchar](4000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
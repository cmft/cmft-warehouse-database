﻿CREATE TABLE [dbo].[whopatientchecklistdetail](
	[wpd_id] [int] IDENTITY(1,1) NOT NULL,
	[wpd_wpc_id] [int] NOT NULL,
	[wpd_whq_id] [int] NOT NULL,
	[wpd_whr_id] [int] NOT NULL,
	[wpd_textanswer] [varchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodeorthoptic](
	[ort_id] [uniqueidentifier] NOT NULL,
	[ort_epi_id] [uniqueidentifier] NOT NULL,
	[ort_imr_id] [uniqueidentifier] NULL,
	[ort_nystagmus_imr_id] [uniqueidentifier] NULL,
	[ort_rft_id_pattern] [uniqueidentifier] NULL
) ON [PRIMARY]
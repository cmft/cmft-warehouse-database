﻿CREATE TABLE [dbo].[ETCExtract](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StartExtractDate] [datetime] NOT NULL,
	[EndExtractDate] [datetime] NULL
) ON [PRIMARY]
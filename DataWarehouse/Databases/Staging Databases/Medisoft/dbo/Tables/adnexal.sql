﻿CREATE TABLE [dbo].[adnexal](
	[adx_id] [uniqueidentifier] NOT NULL,
	[adx_rft_id_preparation] [uniqueidentifier] NULL,
	[adx_glue] [bit] NULL,
	[adx_steristrips] [bit] NULL
) ON [PRIMARY]
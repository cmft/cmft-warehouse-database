﻿CREATE TABLE [dbo].[episodeiop](
	[eil_id] [uniqueidentifier] NOT NULL,
	[eil_epi_id] [uniqueidentifier] NOT NULL,
	[eil_rft_id_ioptype] [uniqueidentifier] NULL,
	[eil_target] [bit] NOT NULL,
	[eil_historicdate] [datetime] NULL,
	[eil_rft_id_status] [uniqueidentifier] NULL,
	[eil_historical]  AS (case when ([eil_historicdate] is null) then 0 else 1 end),
	[eil_currenttarget] [bit] NOT NULL DEFAULT (0),
	[eil_time] [varchar](5) NULL,
	[eil_phasedreading] [bit] NOT NULL DEFAULT (0),
	[eil_historicdateapprox] [bit] NULL CONSTRAINT [DF__episodeio__eil_h__2AB13106]  DEFAULT (0),
	[eil_comment] [varchar](2000) NULL
) ON [PRIMARY]
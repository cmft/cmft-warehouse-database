﻿CREATE TABLE [dbo].[comment](
	[cmt_id] [uniqueidentifier] NOT NULL,
	[cmt_comment] [varchar](2000) NOT NULL,
	[cmt_cty_id] [uniqueidentifier] NULL
) ON [PRIMARY]
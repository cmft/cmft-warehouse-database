﻿CREATE TABLE [dbo].[vteassessmentoutcome](
	[vto_id] [uniqueidentifier] NOT NULL,
	[vto_vte_id] [uniqueidentifier] NULL,
	[vto_done] [int] NULL,
	[vto_bleedingriskoutweighed] [int] NULL,
	[vto_nothromoprovided] [int] NULL,
	[vto_antiembolicstockings] [int] NULL,
	[vto_prophylaxis] [int] NULL,
	[vto_ipcdevice] [int] NULL,
	[vto_extendedthrombo] [int] NULL
) ON [PRIMARY]
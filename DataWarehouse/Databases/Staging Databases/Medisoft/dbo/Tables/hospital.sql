﻿CREATE TABLE [dbo].[hospital](
	[hos_id] [uniqueidentifier] NOT NULL,
	[hos_code] [varchar](255) NOT NULL,
	[hos_trustname] [varchar](255) NOT NULL,
	[hos_trustsub] [varchar](255) NOT NULL,
	[hos_hospname] [varchar](255) NOT NULL,
	[hos_add_id] [uniqueidentifier] NULL,
	[hos_pas_id] [uniqueidentifier] NULL,
	[hos_referralonly] [bit] NULL,
	[hos_type] [int] NULL CONSTRAINT [DF__hospital__hos_ty__13F3F02D]  DEFAULT (1),
	[hos_sharedcarescheme] [varchar](255) NULL,
	[hos_available] [bit] NULL DEFAULT ((1)),
	[hos_attributes] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DMOEpisodeMRProtocol](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Eye] [varchar](1) NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[RetreatmentDecision] [varchar](1000) NULL,
	[StopReasons] [varchar](1000) NULL,
	[StableFibrosedScar] [varchar](10) NULL,
	[RPERipInvolvingFovea] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[spectacleprescription](
	[spr_id] [uniqueidentifier] NOT NULL,
	[spr_epi_id] [uniqueidentifier] NULL,
	[spr_valid] [int] NULL,
	[spr_bifocalpairs] [int] NULL,
	[spr_rft_id_bifocalvoucher] [uniqueidentifier] NULL,
	[spr_varifocalpairs] [int] NULL,
	[spr_rft_id_varifocalvoucher] [uniqueidentifier] NULL,
	[spr_svdistancepairs] [int] NULL,
	[spr_rft_id_svdistancevoucher] [uniqueidentifier] NULL,
	[spr_svnearpairs] [int] NULL,
	[spr_rft_id_svnearvoucher] [uniqueidentifier] NULL,
	[spr_intermediatepairs] [int] NULL,
	[spr_rft_id_intermediatevoucher] [uniqueidentifier] NULL
) ON [PRIMARY]
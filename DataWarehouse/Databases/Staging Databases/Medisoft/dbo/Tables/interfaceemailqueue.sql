﻿CREATE TABLE [dbo].[interfaceemailqueue](
	[ieq_id] [uniqueidentifier] NOT NULL,
	[ieq_iet_id] [uniqueidentifier] NOT NULL,
	[ieq_dateadded] [datetime] NOT NULL,
	[ieq_emailto] [varchar](255) NOT NULL,
	[ieq_emailfrom] [varchar](255) NOT NULL,
	[ieq_emailbody] [text] NOT NULL,
	[ieq_emailsubject] [varchar](1000) NOT NULL,
	[ieq_datetosend] [datetime] NOT NULL,
	[ieq_sent] [int] NOT NULL,
	[ieq_datesent] [datetime] NULL,
	[ieq_epi_id] [uniqueidentifier] NULL,
	[ieq_pat_id] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[interfacepatientmatch](
	[ipm_id] [uniqueidentifier] NOT NULL,
	[ipm_fullname] [varchar](255) NULL,
	[ipm_firstname] [varchar](255) NULL,
	[ipm_surname] [varchar](255) NULL,
	[ipm_patientid] [varchar](50) NULL,
	[ipm_nhsno] [varchar](50) NULL,
	[ipm_birthdate] [varchar](20) NULL,
	[ipm_pat_id] [uniqueidentifier] NULL,
	[ipm_resolved] [bit] NULL,
	[ipm_irresolvable] [bit] NOT NULL CONSTRAINT [DF__interface__ipm_i__5EA2DAC6]  DEFAULT (0),
	[ipm_resolved_timestamp] [datetime] NULL,
	[ipm_use_id_resolvedby] [uniqueidentifier] NULL
) ON [PRIMARY]
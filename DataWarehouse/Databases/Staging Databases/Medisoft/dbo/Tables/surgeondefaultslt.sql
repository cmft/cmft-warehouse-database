﻿CREATE TABLE [dbo].[surgeondefaultslt](
	[sld_id] [uniqueidentifier] NOT NULL,
	[sld_fks_id] [uniqueidentifier] NULL,
	[sld_rft_id_instrument] [uniqueidentifier] NULL,
	[sld_rft_id_goniolens] [uniqueidentifier] NULL,
	[sld_rft_id_degreestreated] [uniqueidentifier] NULL,
	[sld_degreesfrom] [int] NULL,
	[sld_degreesto] [int] NULL,
	[sld_beamdiameter] [int] NULL
) ON [PRIMARY]
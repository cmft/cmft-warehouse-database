﻿CREATE TABLE [dbo].[pachymetry](
	[pac_id] [uniqueidentifier] NOT NULL,
	[pac_rft_id_machine] [uniqueidentifier] NULL,
	[pac_use_id_operator] [uniqueidentifier] NULL
) ON [PRIMARY]
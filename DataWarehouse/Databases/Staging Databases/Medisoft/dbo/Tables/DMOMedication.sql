﻿CREATE TABLE [dbo].[DMOMedication](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[EpisodeDate] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[StopDate] [datetime] NULL,
	[Drug] [varchar](255) NULL,
	[Route] [varchar](255) NULL,
	[Dose] [varchar](255) NULL,
	[Frequency] [varchar](255) NULL,
	[Duration] [varchar](255) NULL,
	[ContinueIndefinitely] [bit] NULL,
	[AddedByPrescription] [bit] NULL,
	[UserID] [uniqueidentifier] NULL
) ON [PRIMARY]
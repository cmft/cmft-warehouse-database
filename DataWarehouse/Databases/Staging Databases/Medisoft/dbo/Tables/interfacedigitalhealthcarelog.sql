﻿CREATE TABLE [dbo].[interfacedigitalhealthcarelog](
	[idl_id] [uniqueidentifier] NOT NULL,
	[idl_log_level] [varchar](10) NULL,
	[idl_timestamp] [datetime] NULL,
	[idl_service_name] [varchar](100) NULL,
	[idl_dh_id] [int] NULL,
	[idl_log] [varchar](8000) NULL
) ON [PRIMARY]
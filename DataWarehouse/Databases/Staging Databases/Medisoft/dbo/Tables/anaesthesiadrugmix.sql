﻿CREATE TABLE [dbo].[anaesthesiadrugmix](
	[dmi_id] [uniqueidentifier] NOT NULL,
	[dmi_code] [varchar](255) NOT NULL,
	[dmi_desc] [varchar](255) NOT NULL,
	[dmi_order] [tinyint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[vitreoretinalsclerostomy](
	[vts_id] [uniqueidentifier] NOT NULL,
	[vts_fks_id] [uniqueidentifier] NULL,
	[vts_rft_id_gauge] [uniqueidentifier] NULL,
	[vta_rft_id_type] [uniqueidentifier] NULL,
	[vts_location] [int] NULL,
	[vts_limbus] [decimal](5, 2) NULL
) ON [PRIMARY]
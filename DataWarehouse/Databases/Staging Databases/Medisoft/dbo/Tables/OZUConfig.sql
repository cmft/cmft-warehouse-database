﻿CREATE TABLE [dbo].[OZUConfig](
	[ID] [int] NOT NULL,
	[Version] [varchar](50) NULL,
	[HESCode] [varchar](50) NULL,
	[OZUPassword] [varchar](50) NULL,
	[WebServiceUserName] [varchar](50) NULL,
	[WebServicePassword] [varchar](50) NULL,
	[AutoUploadActive] [bit] NOT NULL,
	[RunEveryNMonths] [int] NOT NULL,
	[RunDay] [int] NOT NULL,
	[RunTime] [datetime] NULL
) ON [PRIMARY]
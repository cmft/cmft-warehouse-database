﻿CREATE TABLE [dbo].[cliniclistresource](
	[crs_id] [int] IDENTITY(1,1) NOT NULL,
	[crs_desc] [varchar](20) NOT NULL,
	[crs_active] [bit] NOT NULL,
	[crs_sec_id] [int] NOT NULL,
	[crs_audit_userid] [int] NOT NULL,
	[crs_audit_date] [datetime] NOT NULL
) ON [PRIMARY]
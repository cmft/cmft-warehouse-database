﻿CREATE TABLE [dbo].[outputstagepatientoperationsglatmp](
	[TimePeriod] [varchar](255) NULL,
	[MedCategory] [varchar](255) NULL,
	[SubTotal] [int] NULL,
	[MedNo] [int] NULL,
	[PeriodNo] [int] NULL,
	[PC] [decimal](8, 2) NULL
) ON [PRIMARY]
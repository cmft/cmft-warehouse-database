﻿CREATE TABLE [dbo].[patientxml](
	[pxl_id] [uniqueidentifier] NOT NULL,
	[pxl_pat_id] [uniqueidentifier] NOT NULL,
	[pxl_element] [int] NOT NULL,
	[pxl_datevalid] [datetime] NOT NULL,
	[pxl_xml] [ntext] NULL,
	[pxl_hash] [varchar](1000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[gdxeye](
	[gde_id] [uniqueidentifier] NOT NULL,
	[gde_gdx_id] [uniqueidentifier] NOT NULL,
	[gde_eye_id] [uniqueidentifier] NOT NULL,
	[gde_avtsnit] [decimal](8, 2) NULL,
	[gde_avsuperior] [decimal](8, 2) NULL,
	[gde_avinferior] [decimal](8, 2) NULL,
	[gde_stdevtsnit] [decimal](8, 2) NULL,
	[gde_nfi] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputdatastage_ivipatientstart](
	[ivi_pat_id] [uniqueidentifier] NULL,
	[ivi_eye_id] [uniqueidentifier] NULL,
	[ivi_hospnum] [varchar](255) NULL,
	[ivi_nhsno] [varchar](255) NOT NULL,
	[ivi_eye] [varchar](255) NOT NULL,
	[ivi_dob] [datetime] NULL,
	[ivi_postcodearea] [varchar](255) NULL,
	[ivi_postcodeletter] [varchar](2) NULL,
	[ivi_age] [int] NULL,
	[ivi_sex] [varchar](255) NULL,
	[ivi_dod] [datetime] NULL,
	[ivi_drug] [varchar](25) NULL,
	[ivi_copathology] [varchar](2000) NULL,
	[ivi_indications] [varchar](2000) NULL,
	[ivi_private] [bit] NULL,
	[ivi_patprivate] [bit] NULL,
	[ivi_first] [datetime] NULL,
	[ivi_last] [datetime] NULL,
	[ivi_FUlast] [datetime] NULL,
	[ivi_va] [int] NULL,
	[ivi_valetter] [int] NULL,
	[ivi_baseline] [varchar](25) NULL,
	[ivi_baselineorder] [int] NULL,
	[ivi_firsteye] [bit] NULL,
	[ivi_previousrecorded] [bit] NULL,
	[ivi_patpreviousrecorded] [bit] NULL,
	[ivi_onlydrug] [bit] NULL,
	[ivi_patonlydrug] [bit] NULL,
	[ivi_otherops] [bit] NULL,
	[ivi_patotherops] [bit] NULL,
	[ivi_diaggroup] [int] NULL,
	[ivi_year] [int] NULL,
	[ivi_nhsyear] [int] NULL,
	[ivi_FUyears] [int] NULL,
	[ivi_injyears] [int] NULL,
	[ivi_FUmonths] [int] NULL,
	[ivi_injmonths] [int] NULL,
	[ivi_psr_date] [datetime] NULL,
	[ivi_psr_year] [int] NULL,
	[ivi_psr_year_pat] [int] NULL,
	[ivi_comp_year] [int] NULL,
	[ivi_comp_year_pat] [int] NULL,
	[ivi_luminous] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalObservation](
	[cob_id] [uniqueidentifier] NOT NULL,
	[cob_epi_id] [uniqueidentifier] NOT NULL,
	[cob_eye_id] [uniqueidentifier] NOT NULL,
	[cob_ctm_id] [uniqueidentifier] NOT NULL,
	[cob_notes] [varchar](2048) NULL,
	[cob_parsedtext] [varchar](2000) NULL,
	[cob_value] [decimal](10, 3) NULL
) ON [PRIMARY]
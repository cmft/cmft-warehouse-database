﻿CREATE TABLE [dbo].[interfacedatakowa](
	[idk_id] [uniqueidentifier] NOT NULL,
	[idk_ipm_id] [uniqueidentifier] NULL,
	[idk_datetime] [datetime] NULL,
	[idk_ser] [int] NULL,
	[idk_stereo] [tinyint] NULL,
	[idk_filepath] [varchar](255) NULL,
	[idk_imr_id] [uniqueidentifier] NULL,
	[idk_processed] [bit] NULL,
	[idk_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[anaestheticassessment](
	[aas_id] [uniqueidentifier] NOT NULL,
	[aas_use_id_anaesthetist] [uniqueidentifier] NULL,
	[aas_rft_id_asagrade] [uniqueidentifier] NULL,
	[aas_rft_id_ponv] [uniqueidentifier] NULL,
	[aas_mallampatiscore] [int] NULL,
	[aas_rft_id_intubation] [uniqueidentifier] NULL,
	[aas_year] [int] NULL,
	[aas_mouthopening] [int] NULL,
	[aas_thyromental] [int] NULL,
	[aas_generalexplained] [bit] NULL,
	[aas_regionalexplained] [bit] NULL,
	[aas_rectalexplained] [bit] NULL,
	[aas_sedationexplained] [bit] NULL,
	[aas_dentitionexplained] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputaudit](
	[opa_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__outputaud__opa_i__775A7B47]  DEFAULT (newid()),
	[opa_out_id] [uniqueidentifier] NULL,
	[opa_criteriaxml] [ntext] NULL,
	[opa_use_id] [uniqueidentifier] NULL,
	[opa_datetime] [datetime] NOT NULL CONSTRAINT [DF__outputaud__opa_d__784E9F80]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
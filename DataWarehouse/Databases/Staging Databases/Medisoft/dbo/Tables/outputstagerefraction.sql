﻿CREATE TABLE [dbo].[outputstagerefraction](
	[ref_id] [uniqueidentifier] NOT NULL,
	[ref_eye_id] [uniqueidentifier] NOT NULL,
	[ref_sphere] [float] NULL,
	[ref_axis] [float] NULL,
	[ref_add] [float] NULL,
	[ref_cylinder] [float] NULL,
	[ref_rty_id] [uniqueidentifier] NOT NULL,
	[ref_prismrange1] [decimal](5, 2) NULL,
	[ref_prismrange2] [decimal](5, 2) NULL,
	[ref_rft_id_prismbase1] [uniqueidentifier] NULL,
	[ref_rft_id_prismbase2] [uniqueidentifier] NULL,
	[ref_dru_id] [uniqueidentifier] NULL,
	[ref_date] [datetime] NULL,
	[ref_approx] [bit] NULL
) ON [PRIMARY]
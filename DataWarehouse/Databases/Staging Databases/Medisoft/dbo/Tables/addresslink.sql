﻿CREATE TABLE [dbo].[addresslink](
	[adl_id] [uniqueidentifier] NOT NULL,
	[adl_add_id] [uniqueidentifier] NOT NULL,
	[adl_fks_id] [uniqueidentifier] NOT NULL,
	[adl_addressno] [tinyint] NOT NULL
) ON [PRIMARY]
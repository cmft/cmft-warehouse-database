﻿CREATE TABLE [dbo].[interfacedeviceconfig](
	[idc_id] [uniqueidentifier] NOT NULL,
	[idc_rft_id_devicetype] [uniqueidentifier] NULL,
	[idc_deviceid] [varchar](255) NULL,
	[idc_loc_id] [uniqueidentifier] NULL
) ON [PRIMARY]
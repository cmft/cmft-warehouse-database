﻿CREATE TABLE [dbo].[interfaceecopydata](
	[iec_id] [uniqueidentifier] NOT NULL,
	[iec_elc_id] [uniqueidentifier] NOT NULL,
	[iec_dateadded] [datetime] NULL,
	[iec_type] [varchar](255) NULL,
	[iec_content] [ntext] NULL,
	[iec_published] [int] NULL,
	[iec_datepublished] [datetime] NULL,
	[iec_dmsdocumentid] [int] NULL,
	[iec_dmsmessage] [varchar](4000) NULL,
	[iec_imr_id] [uniqueidentifier] NULL,
	[iec_filepath] [varchar](1000) NULL,
	[iec_metadata] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
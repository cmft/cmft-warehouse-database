﻿CREATE TABLE [dbo].[registration](
	[reg_id] [uniqueidentifier] NOT NULL,
	[reg_internalpasnumber] [varchar](255) NOT NULL,
	[reg_patientnumber] [varchar](255) NOT NULL,
	[reg_hos_id] [uniqueidentifier] NULL
) ON [PRIMARY]
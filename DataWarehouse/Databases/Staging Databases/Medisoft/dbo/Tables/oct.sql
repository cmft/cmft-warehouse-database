﻿CREATE TABLE [dbo].[oct](
	[ocm_id] [uniqueidentifier] NOT NULL,
	[ocm_rft_id_machine] [uniqueidentifier] NULL,
	[ocm_use_id] [uniqueidentifier] NULL,
	[ocm_optometristdata] [bit] NULL,
	[ocm_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
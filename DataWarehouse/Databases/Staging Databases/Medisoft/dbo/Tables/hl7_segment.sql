﻿CREATE TABLE [dbo].[hl7_segment](
	[seg_segment] [varchar](3) NOT NULL,
	[seg_description] [varchar](255) NOT NULL,
	[seg_chapter] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[cht_clinics](
	[clinicid] [varchar](50) NOT NULL,
	[clinicname] [varchar](50) NOT NULL,
	[clinicabrv] [varchar](50) NOT NULL,
	[clinicdrname] [varchar](50) NOT NULL,
	[selectable] [int] NOT NULL,
	[selected] [int] NOT NULL,
	[locationId] [int] NOT NULL,
	[redflagcounter] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputdatastage_ivireportdrugs](
	[rdr_bitwise] [int] NULL,
	[rdr_drug] [varchar](25) NULL,
	[rdr_display] [bit] NULL,
	[dru_order] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[patientregistration](
	[prg_id] [uniqueidentifier] NOT NULL,
	[prg_pat_id] [uniqueidentifier] NULL,
	[prg_reg_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[clinicalertepisode](
	[cae_cal_id] [uniqueidentifier] NOT NULL,
	[cae_epi_id] [uniqueidentifier] NOT NULL,
	[cae_hash] [varchar](1000) NOT NULL,
	[cae_showcount] [int] NOT NULL DEFAULT (0),
	[cae_description] [varchar](1000) NULL,
	[cae_old_description] [varchar](1000) NULL,
	[cae_active] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[digitalhealthcareoptomize](
	[dho_id] [uniqueidentifier] NOT NULL,
	[dho_patient_id] [varchar](40) NULL,
	[dho_patient_id_type] [varchar](50) NULL,
	[dho_web_root] [varchar](50) NULL,
	[dho_url_to_encounter] [varchar](500) NULL
) ON [PRIMARY]
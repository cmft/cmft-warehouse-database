﻿CREATE TABLE [dbo].[hl7_scheduling_cancel](
	[scc_id] [uniqueidentifier] NOT NULL,
	[scc_Appointment_ID] [varchar](255) NULL,
	[scc_pat_number] [varchar](255) NULL,
	[scc_pat_nhsno] [varchar](255) NULL,
	[scc_pat_surname] [varchar](255) NULL,
	[scc_pat_dob] [varchar](255) NULL,
	[scc_consultant_code] [varchar](255) NULL,
	[scc_consultant_name] [varchar](255) NULL,
	[scc_appointment_date] [varchar](255) NULL,
	[scc_clinic_code] [varchar](255) NULL,
	[scc_clinic_name] [varchar](255) NULL,
	[scc_clinic_type] [varchar](255) NULL,
	[scc_timestamp] [datetime] NULL
) ON [PRIMARY]
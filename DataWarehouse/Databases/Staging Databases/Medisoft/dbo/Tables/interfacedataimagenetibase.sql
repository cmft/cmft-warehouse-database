﻿CREATE TABLE [dbo].[interfacedataimagenetibase](
	[iii_id] [uniqueidentifier] NOT NULL,
	[iii_ipm_id] [uniqueidentifier] NULL,
	[iii_createdate] [datetime] NULL,
	[iii_procedurename] [nchar](50) NULL,
	[iii_database] [varchar](25) NULL,
	[iii_processed] [bit] NULL,
	[iii_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
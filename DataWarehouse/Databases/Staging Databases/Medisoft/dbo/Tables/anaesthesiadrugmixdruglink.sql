﻿CREATE TABLE [dbo].[anaesthesiadrugmixdruglink](
	[dmd_id] [uniqueidentifier] NOT NULL,
	[dmd_dmi_id] [uniqueidentifier] NULL,
	[dmd_dru_id] [uniqueidentifier] NULL,
	[dmd_ratio] [float] NULL,
	[dmd_percent] [float] NULL,
	[dmd_vasoconstrictor] [bit] NULL,
	[dmd_vasoconcentration] [float] NULL,
	[dmd_order] [tinyint] NULL
) ON [PRIMARY]
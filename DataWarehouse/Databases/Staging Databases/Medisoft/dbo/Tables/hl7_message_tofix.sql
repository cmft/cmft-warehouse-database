﻿CREATE TABLE [dbo].[hl7_message_tofix](
	[hlm_id] [uniqueidentifier] NOT NULL,
	[hlm_hl7] [varchar](max) NULL,
	[hlm_xml] [xml] NULL,
	[hlm_ack] [varchar](max) NULL,
	[hlm_timestamp] [datetime] NULL,
	[hlm_processed] [int] NULL,
	[hlm_ack_sent] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
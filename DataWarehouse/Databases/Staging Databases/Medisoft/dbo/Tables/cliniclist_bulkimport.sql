﻿CREATE TABLE [dbo].[cliniclist_bulkimport](
	[clb_id] [int] IDENTITY(1,1) NOT NULL,
	[clb_Appointment_ID] [varchar](255) NULL,
	[clb_DistrictNumber] [varchar](255) NULL,
	[clb_CasenoteNumber] [varchar](255) NULL,
	[clb_nhsno] [varchar](50) NULL,
	[clb_pat_surname] [varchar](255) NULL,
	[clb_pat_dob] [datetime] NULL,
	[clb_consultant_code] [varchar](255) NULL,
	[clb_appointment_datetime] [datetime] NULL,
	[clb_clinic_code] [varchar](20) NULL,
	[clb_clinic_type] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[consultantsecretaryprinter](
	[cop_id] [uniqueidentifier] NOT NULL,
	[cop_hos_id] [uniqueidentifier] NULL,
	[cop_use_id] [uniqueidentifier] NULL,
	[cop_printer] [varchar](255) NULL
) ON [PRIMARY]
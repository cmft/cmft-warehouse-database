﻿CREATE TABLE [dbo].[proceduresuture](
	[psu_id] [uniqueidentifier] NOT NULL,
	[psu_fks_id] [uniqueidentifier] NULL,
	[psu_item] [int] NULL,
	[psu_number] [int] NULL,
	[psu_rft_id_size] [uniqueidentifier] NULL,
	[psu_rft_id_material] [uniqueidentifier] NULL,
	[psu_rft_id_site] [uniqueidentifier] NULL,
	[psu_rft_id_method] [uniqueidentifier] NULL
) ON [PRIMARY]
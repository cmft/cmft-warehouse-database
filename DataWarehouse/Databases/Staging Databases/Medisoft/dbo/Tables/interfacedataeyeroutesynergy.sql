﻿CREATE TABLE [dbo].[interfacedataeyeroutesynergy](
	[ide_id] [uniqueidentifier] NOT NULL,
	[ide_ipm_id] [uniqueidentifier] NULL,
	[ide_datetime] [datetime] NULL,
	[ide_desc] [varchar](255) NULL,
	[ide_url] [varchar](255) NULL,
	[ide_processed] [bit] NULL,
	[ide_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
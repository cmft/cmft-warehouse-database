﻿CREATE TABLE [dbo].[patientchartdata](
	[ID] [uniqueidentifier] NOT NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[EyeID] [uniqueidentifier] NULL,
	[Item] [varchar](255) NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemValue] [varchar](255) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [varchar](50) NULL,
	[MeasurementType] [varchar](255) NULL,
	[CustomItem1] [varchar](255) NULL,
	[CustomItem2] [varchar](255) NULL,
	[CustomItem3] [varchar](255) NULL,
	[CustomItem4] [varchar](255) NULL,
	[CustomItem5] [varchar](255) NULL,
	[SaveDate] [datetime] NULL
) ON [PRIMARY]
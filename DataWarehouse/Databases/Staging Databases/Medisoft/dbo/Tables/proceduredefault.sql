﻿CREATE TABLE [dbo].[proceduredefault](
	[pdf_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__procedure__pdf_i__3B70DD5D]  DEFAULT (newid()),
	[pdf_fks_id] [uniqueidentifier] NOT NULL,
	[pdf_use_id_anaesthetist] [uniqueidentifier] NULL,
	[pdf_rft_id_anaesthesiatechnique] [uniqueidentifier] NULL,
	[pdf_nvi_id_planfollowup] [uniqueidentifier] NULL,
	[pdf_hyalase] [bit] NULL CONSTRAINT [DF__procedure__pdf_h__4C9B695F]  DEFAULT (0),
	[pdf_pro_code] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[patientcategory](
	[pct_id] [uniqueidentifier] NULL,
	[pct_pat_id] [uniqueidentifier] NULL,
	[pct_category] [varchar](255) NULL,
	[pct_timestamp] [datetime] NULL,
	[pct_iscurrentdiabetic] [bit] NULL DEFAULT (0)
) ON [PRIMARY]
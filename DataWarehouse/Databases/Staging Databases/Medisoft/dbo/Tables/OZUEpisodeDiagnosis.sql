﻿CREATE TABLE [dbo].[OZUEpisodeDiagnosis](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[DateOfDiagnosis] [smalldatetime] NOT NULL,
	[Eye] [varchar](1) NOT NULL,
	[User] [varchar](36) NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[Type] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[DateRecorded] [smalldatetime] NULL,
	[Active] [bit] NULL,
	[DateDeactivated] [datetime] NULL
) ON [PRIMARY]
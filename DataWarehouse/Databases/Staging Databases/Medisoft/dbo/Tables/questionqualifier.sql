﻿CREATE TABLE [dbo].[questionqualifier](
	[qql_id] [uniqueidentifier] NOT NULL,
	[qql_que_id] [uniqueidentifier] NOT NULL,
	[qql_qqt_id] [uniqueidentifier] NULL,
	[qql_answer] [int] NOT NULL
) ON [PRIMARY]
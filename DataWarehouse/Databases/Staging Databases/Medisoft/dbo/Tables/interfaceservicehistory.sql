﻿CREATE TABLE [dbo].[interfaceservicehistory](
	[ish_id] [uniqueidentifier] NOT NULL,
	[ish_its_id] [uniqueidentifier] NULL,
	[ish_fks_id] [uniqueidentifier] NULL,
	[ish_timestamp] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[hl7_component](
	[com_id] [uniqueidentifier] NOT NULL,
	[com_name] [varchar](255) NULL,
	[com_variable] [varchar](255) NULL,
	[com_seg_segment] [varchar](3) NULL,
	[com_xpath] [varchar](2000) NULL,
	[com_attribute] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ophthalsuite](
	[ops_id] [uniqueidentifier] NOT NULL,
	[ops_bw_pat_id] [int] NULL,
	[ops_exam_id] [int] NULL,
	[ops_exam_type_id] [int] NULL,
	[ops_exam_type_name] [varchar](255) NULL,
	[ops_exam_equipment] [varchar](255) NULL,
	[ops_exam_eye] [varchar](25) NULL
) ON [PRIMARY]
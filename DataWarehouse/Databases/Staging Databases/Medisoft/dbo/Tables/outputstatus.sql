﻿CREATE TABLE [dbo].[outputstatus](
	[ous_id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_outputstatus_ous_id]  DEFAULT (newid()),
	[ous_out_id] [uniqueidentifier] NOT NULL,
	[ous_info] [varchar](1000) NULL,
	[ous_schedule] [int] NULL,
	[ous_stagetable] [varchar](100) NULL,
	[out_daterun] [datetime] NULL,
	[out_lastepisodedate] [datetime] NULL
) ON [PRIMARY]
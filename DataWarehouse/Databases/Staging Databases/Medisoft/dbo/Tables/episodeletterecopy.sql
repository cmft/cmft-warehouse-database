﻿CREATE TABLE [dbo].[episodeletterecopy](
	[elc_id] [uniqueidentifier] NOT NULL,
	[elc_epi_id] [uniqueidentifier] NULL,
	[elc_let_id] [uniqueidentifier] NULL,
	[elc_rft_id_recipienttype] [uniqueidentifier] NULL,
	[elc_rft_id_format] [uniqueidentifier] NULL,
	[elc_content] [ntext] NULL,
	[elc_imr_id] [uniqueidentifier] NULL,
	[elc_datecreated] [datetime] NULL,
	[elc_active] [bit] NULL,
	[elc_version] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
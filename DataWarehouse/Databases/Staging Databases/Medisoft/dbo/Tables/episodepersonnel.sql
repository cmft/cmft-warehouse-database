﻿CREATE TABLE [dbo].[episodepersonnel](
	[epu_id] [uniqueidentifier] NOT NULL,
	[epu_epi_id] [uniqueidentifier] NULL,
	[epu_pat_id] [uniqueidentifier] NULL,
	[epu_rft_id_accompanied] [uniqueidentifier] NULL,
	[epu_use_id_primary] [uniqueidentifier] NULL,
	[epu_use_id_consultant] [uniqueidentifier] NULL,
	[epu_use_id_secondary] [uniqueidentifier] NULL,
	[epu_use_id_additional1] [uniqueidentifier] NULL,
	[epu_use_id_additional2] [uniqueidentifier] NULL
) ON [PRIMARY]
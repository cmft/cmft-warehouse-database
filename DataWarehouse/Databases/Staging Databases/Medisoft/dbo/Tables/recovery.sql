﻿CREATE TABLE [dbo].[recovery](
	[rcy_id] [uniqueidentifier] NOT NULL,
	[rcy_waterlowscore] [int] NULL,
	[rcy_timetorecovery] [datetime] NULL,
	[rcy_timeout] [datetime] NULL,
	[rcy_airwaytime] [datetime] NULL
) ON [PRIMARY]
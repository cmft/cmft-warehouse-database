﻿CREATE TABLE [dbo].[patientamdrisk](
	[pam_id] [uniqueidentifier] NOT NULL,
	[pam_pat_id] [uniqueidentifier] NOT NULL,
	[pam_risk] [decimal](5, 2) NOT NULL,
	[pam_risklabel] [varchar](255) NOT NULL
) ON [PRIMARY]
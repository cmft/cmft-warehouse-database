﻿CREATE TABLE [dbo].[anaesthesiadrugmixlink](
	[adm_id] [uniqueidentifier] NOT NULL,
	[adm_ana_id] [uniqueidentifier] NULL,
	[adm_dmi_id] [uniqueidentifier] NULL,
	[adm_dose] [float] NULL,
	[adm_dose2] [float] NULL,
	[adm_time] [varchar](255) NULL,
	[adm_dro_id] [uniqueidentifier] NULL,
	[adm_rft_id_site] [uniqueidentifier] NULL,
	[adm_rft_id_needle] [uniqueidentifier] NULL,
	[adm_rft_id_technique] [uniqueidentifier] NULL,
	[adm_volume] [float] NULL,
	[adm_hyalase] [float] NOT NULL
) ON [PRIMARY]
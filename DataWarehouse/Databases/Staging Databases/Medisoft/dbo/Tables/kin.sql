﻿CREATE TABLE [dbo].[kin](
	[kin_id] [uniqueidentifier] NOT NULL,
	[kin_name] [varchar](255) NOT NULL,
	[kin_relation] [varchar](255) NOT NULL,
	[kin_add_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[nursetheatreconsumable](
	[ntc_id] [uniqueidentifier] NOT NULL,
	[ntc_nth_id] [uniqueidentifier] NOT NULL,
	[ntc_rft_id_consumable] [uniqueidentifier] NOT NULL,
	[ntc_serial] [varchar](255) NOT NULL,
	[ntc_expiry] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[outputsavedsearch](
	[oss_id] [uniqueidentifier] NULL,
	[oss_use_id] [uniqueidentifier] NULL,
	[oss_out_id] [uniqueidentifier] NULL,
	[oss_desc] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DMOEpisodeFollowUp](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[FollowUpDate] [datetime] NULL,
	[FollowUpDays] [int] NULL,
	[FollowUpWeeks] [int] NULL,
	[FollowUpMonths] [int] NULL
) ON [PRIMARY]
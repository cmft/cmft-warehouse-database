﻿CREATE TABLE [dbo].[episodefile](
	[epf_id] [uniqueidentifier] NOT NULL,
	[epf_use_id_attached] [uniqueidentifier] NULL,
	[epf_medisoftname] [varchar](255) NOT NULL,
	[epf_extension] [varchar](255) NULL,
	[epf_originalpath] [varchar](255) NULL,
	[epf_imr_id] [uniqueidentifier] NOT NULL,
	[epf_storedfile] [bit] NULL,
	[epf_fileclass] [uniqueidentifier] NULL,
	[epf_filedatetime] [datetime] NULL,
	[epf_eye_id] [uniqueidentifier] NULL
) ON [PRIMARY]
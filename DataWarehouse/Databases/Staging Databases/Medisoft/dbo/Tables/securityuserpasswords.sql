﻿CREATE TABLE [dbo].[securityuserpasswords](
	[upw_id] [uniqueidentifier] NOT NULL,
	[upw_use_id] [uniqueidentifier] NOT NULL,
	[upw_password] [nvarchar](50) NULL,
	[upw_dateissued] [datetime] NULL,
	[upw_dateexpired] [smalldatetime] NULL,
	[upw_salt] [nvarchar](255) NULL
) ON [PRIMARY]
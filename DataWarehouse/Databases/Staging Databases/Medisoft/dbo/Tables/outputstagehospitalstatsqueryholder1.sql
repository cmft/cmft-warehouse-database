﻿CREATE TABLE [dbo].[outputstagehospitalstatsqueryholder1](
	[Operations] [varchar](100) NULL,
	[EpisodeCount] [int] NULL,
	[Finished] [int] NULL,
	[MalePC] [decimal](8, 1) NULL,
	[WaitTimeMean] [varchar](50) NULL,
	[WaitMedian] [varchar](50) NULL,
	[StayLengthMean] [varchar](50) NULL,
	[StayLengthMedian] [varchar](50) NULL,
	[MeanAge] [decimal](8, 1) NULL,
	[BedDays] [decimal](8, 1) NULL,
	[MeanAgeMale] [decimal](8, 1) NULL,
	[MeanAgeFemale] [decimal](8, 1) NULL,
	[Age1559] [decimal](8, 1) NULL,
	[Age6074] [decimal](8, 1) NULL,
	[Age75Plus] [decimal](8, 1) NULL,
	[DayCase] [decimal](8, 1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DMOEpisodeOCT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Eye] [varchar](1) NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[OCTMachine] [varchar](255) NULL,
	[FovealThickness] [int] NULL,
	[Central1mmRetinalThickness] [int] NULL,
	[MacularVolume] [decimal](5, 2) NULL,
	[PEDThickness] [int] NULL
) ON [PRIMARY]
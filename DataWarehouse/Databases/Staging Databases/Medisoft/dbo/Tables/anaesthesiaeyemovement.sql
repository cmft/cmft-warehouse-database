﻿CREATE TABLE [dbo].[anaesthesiaeyemovement](
	[aem_id] [uniqueidentifier] NOT NULL,
	[aem_ana_id] [uniqueidentifier] NULL,
	[aem_time] [varchar](255) NULL,
	[aem_rft_id_superiorrectus] [uniqueidentifier] NULL,
	[aem_rft_id_inferiorrectus] [uniqueidentifier] NULL,
	[aem_rft_id_lateralrectus] [uniqueidentifier] NULL,
	[aem_rft_id_medialrectus] [uniqueidentifier] NULL,
	[aem_rectusscore] [tinyint] NULL,
	[aem_rft_id_superioroblique] [uniqueidentifier] NULL,
	[aem_rft_id_obicularis] [uniqueidentifier] NULL,
	[aem_rft_id_sensoryloss] [uniqueidentifier] NULL
) ON [PRIMARY]
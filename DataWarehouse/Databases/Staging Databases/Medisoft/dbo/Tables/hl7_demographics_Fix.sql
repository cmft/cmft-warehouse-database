﻿CREATE TABLE [dbo].[hl7_demographics_Fix](
	[hdf_CurrentHL7PatNum] [varchar](500) NULL,
	[hdf_CurrentHL7InternalNum] [varchar](50) NULL,
	[hdf_action] [varchar](100) NULL,
	[hdf_NewHL7PatNum] [varchar](50) NULL,
	[hdf_NewHL7InternalNum] [varchar](50) NULL
) ON [PRIMARY]
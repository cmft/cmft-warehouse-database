﻿CREATE TABLE [dbo].[nursetheatreequipment](
	[nte_id] [uniqueidentifier] NOT NULL,
	[nte_nth_id] [uniqueidentifier] NOT NULL,
	[nte_rft_id_equipment] [uniqueidentifier] NOT NULL,
	[nte_serial] [varchar](255) NULL,
	[nte_expiry] [datetime] NULL
) ON [PRIMARY]
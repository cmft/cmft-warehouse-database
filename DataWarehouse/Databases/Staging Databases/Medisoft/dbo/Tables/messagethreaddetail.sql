﻿CREATE TABLE [dbo].[messagethreaddetail](
	[msd_id] [uniqueidentifier] NOT NULL,
	[msd_msg_id] [uniqueidentifier] NOT NULL,
	[msd_date] [datetime] NULL,
	[msd_threadid] [int] NULL,
	[msd_parentthreadid] [int] NULL,
	[msd_order] [int] NULL,
	[msd_indent] [int] NULL,
	[msd_use_id_created] [uniqueidentifier] NULL,
	[msd_use_id_intendedrecipient] [uniqueidentifier] NULL,
	[msd_readbyintended] [bit] NULL,
	[msd_detail] [varchar](2000) NULL
) ON [PRIMARY]
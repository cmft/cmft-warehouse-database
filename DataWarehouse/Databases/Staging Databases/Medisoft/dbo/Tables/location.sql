﻿CREATE TABLE [dbo].[location](
	[loc_id] [uniqueidentifier] NOT NULL,
	[loc_hos_id] [uniqueidentifier] NULL,
	[loc_desc] [varchar](255) NULL,
	[loc_order] [int] NOT NULL,
	[loc_available] [bit] NOT NULL,
	[loc_notrec] [int] NULL,
	[loc_rft_id_chart_right] [uniqueidentifier] NULL,
	[loc_rft_id_chart_left] [uniqueidentifier] NULL,
	[loc_rft_id_chartsize] [uniqueidentifier] NULL DEFAULT ('EDB54413-7969-437D-A2A0-25EE95917554'),
	[loc_rft_id_distance] [uniqueidentifier] NULL
) ON [PRIMARY]
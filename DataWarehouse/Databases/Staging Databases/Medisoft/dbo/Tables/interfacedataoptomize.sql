﻿CREATE TABLE [dbo].[interfacedataoptomize](
	[idz_id] [uniqueidentifier] NOT NULL,
	[idz_ipm_id] [uniqueidentifier] NULL,
	[idz_optomize_id] [int] NULL,
	[idz_date] [datetime] NULL,
	[idz_processed] [bit] NULL,
	[idz_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodedraw](
	[edr_id] [uniqueidentifier] NOT NULL,
	[edr_epi_id] [uniqueidentifier] NULL,
	[edr_pat_id] [uniqueidentifier] NULL,
	[edr_use_id] [uniqueidentifier] NULL,
	[edr_eye_id] [uniqueidentifier] NULL,
	[edr_fks_id] [uniqueidentifier] NULL,
	[edr_imr_id] [uniqueidentifier] NULL,
	[edr_imrthumb_id] [uniqueidentifier] NULL,
	[edr_dateupdated] [datetime] NULL,
	[edr_type] [int] NULL CONSTRAINT [DF__episodeDr__edr_t__7C9AEFFC]  DEFAULT (1),
	[edr_ext] [varchar](5) NULL DEFAULT ('jpg')
) ON [PRIMARY]
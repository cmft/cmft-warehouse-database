﻿CREATE TABLE [dbo].[securityuseradditional](
	[sue_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[sue_use_id] [uniqueidentifier] NULL,
	[sue_imr_id] [uniqueidentifier] NULL,
	[sue_initials] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[nursepreop](
	[npo_id] [uniqueidentifier] NOT NULL,
	[npo_use_id_nurse] [uniqueidentifier] NULL,
	[npo_arrivaltime] [varchar](255) NOT NULL,
	[npo_use_id_consultant] [uniqueidentifier] NULL,
	[npo_eye_id] [uniqueidentifier] NULL,
	[npo_rft_id_anaesthesia] [uniqueidentifier] NULL,
	[npo_fastedfrom] [varchar](255) NOT NULL,
	[npo_rft_id_pulse] [uniqueidentifier] NULL,
	[npo_surgerycancelled] [bit] NULL,
	[npo_epi_id_plannedoperation] [uniqueidentifier] NULL,
	[npo_caution_html] [varchar](8000) NULL,
	[npo_eye_id_drug] [uniqueidentifier] NULL,
	[npo_nilbymouthfrom] [varchar](255) NULL
) ON [PRIMARY]
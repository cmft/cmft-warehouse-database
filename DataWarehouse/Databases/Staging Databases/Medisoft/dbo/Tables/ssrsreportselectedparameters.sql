﻿CREATE TABLE [dbo].[ssrsreportselectedparameters](
	[srp_report_id] [uniqueidentifier] NOT NULL,
	[srp_session_id] [uniqueidentifier] NOT NULL,
	[srp_parametername] [varchar](255) NULL,
	[srp_parametersselected] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[patientpinstore](
	[ppn_id] [int] IDENTITY(1,1) NOT NULL,
	[ppn_pin] [varchar](50) NULL,
	[ppn_pinint] [int] NULL,
	[ppn_pat_id] [uniqueidentifier] NULL,
	[ppn_status] [int] NULL,
	[ppn_startdate] [datetime] NULL,
	[ppn_stopdate] [datetime] NULL,
	[ppn_stopreason] [int] NULL,
	[ppn_flag] [int] NULL,
	[ppn_scheme] [int] NULL,
	[ppn_updated] [datetime] NULL,
	[ppn_epi_id] [uniqueidentifier] NULL,
	[ppn_epi_id_requested] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[medicalhistoryxml](
	[mhx_id] [uniqueidentifier] NOT NULL,
	[mhx_pat_id] [uniqueidentifier] NOT NULL,
	[mhx_xml] [xml] NULL,
	[mhx_datevalid] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
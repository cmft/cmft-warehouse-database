﻿CREATE TABLE [dbo].[cht_actiontypes](
	[actiontype_id] [varchar](50) NOT NULL,
	[type] [int] NOT NULL,
	[design] [varchar](50) NOT NULL,
	[responses] [varchar](50) NOT NULL
) ON [PRIMARY]
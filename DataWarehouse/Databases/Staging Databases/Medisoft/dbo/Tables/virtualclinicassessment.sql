﻿CREATE TABLE [dbo].[virtualclinicassessment](
	[vca_id] [uniqueidentifier] NOT NULL,
	[vca_vcl_id] [uniqueidentifier] NOT NULL,
	[vca_eye_id] [uniqueidentifier] NULL,
	[vca_rft_id_findingtype] [uniqueidentifier] NOT NULL,
	[vca_rft_id_finding] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[pas_audit](
	[pas_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[pas_timestamp] [datetime] NOT NULL DEFAULT (getdate()),
	[pas_queryid] [uniqueidentifier] NOT NULL,
	[pas_searchid] [varchar](255) NOT NULL,
	[pas_found] [bit] NULL,
	[pas_error] [varchar](255) NULL,
	[pas_event] [varchar](255) NULL
) ON [PRIMARY]
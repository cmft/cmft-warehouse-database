﻿CREATE TABLE [dbo].[proceduredrug](
	[prd_id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[prd_fks_id] [uniqueidentifier] NOT NULL,
	[prd_rft_id_route] [uniqueidentifier] NULL,
	[prd_dru_id_drug] [uniqueidentifier] NULL,
	[prd_rft_id_type] [uniqueidentifier] NOT NULL,
	[prd_pro_code] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[imagerepository](
	[imr_id] [uniqueidentifier] NOT NULL,
	[imr_no] [int] IDENTITY(1,1) NOT NULL,
	[imr_image] [image] NULL,
	[imr_shrunk] [bit] NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
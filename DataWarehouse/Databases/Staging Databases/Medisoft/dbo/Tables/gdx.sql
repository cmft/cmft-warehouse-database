﻿CREATE TABLE [dbo].[gdx](
	[gdx_id] [uniqueidentifier] NOT NULL,
	[gdx_use_id] [uniqueidentifier] NULL,
	[gdx_intereyeassymetry] [decimal](8, 2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ETCEpisodeDRAssessmentMacExudates](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[Eye] [char](1) NOT NULL,
	[Description] [varchar](255) NOT NULL
) ON [PRIMARY]
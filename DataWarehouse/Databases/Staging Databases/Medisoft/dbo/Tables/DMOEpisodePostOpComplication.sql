﻿CREATE TABLE [dbo].[DMOEpisodePostOpComplication](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[ClinicEpisodeDate] [datetime] NOT NULL,
	[Eye] [varchar](1) NOT NULL,
	[ClinicEpisodeID] [uniqueidentifier] NOT NULL,
	[Type] [varchar](255) NULL,
	[Complication] [varchar](255) NULL,
	[OperationDate] [datetime] NULL
) ON [PRIMARY]
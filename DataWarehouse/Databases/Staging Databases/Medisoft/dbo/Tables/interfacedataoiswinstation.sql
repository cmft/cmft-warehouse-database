﻿CREATE TABLE [dbo].[interfacedataoiswinstation](
	[idw_id] [uniqueidentifier] NOT NULL,
	[idw_ipm_id] [uniqueidentifier] NULL,
	[idw_recno] [int] NOT NULL,
	[idw_eye] [nvarchar](4) NULL,
	[idw_datetime] [datetime] NULL,
	[idw_procedures] [varchar](128) NULL,
	[idw_processed] [bit] NULL,
	[idw_epi_id] [uniqueidentifier] NULL
) ON [PRIMARY]
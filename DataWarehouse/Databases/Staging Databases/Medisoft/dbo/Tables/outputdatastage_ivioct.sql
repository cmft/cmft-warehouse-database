﻿CREATE TABLE [dbo].[outputdatastage_ivioct](
	[oct_pat_id] [uniqueidentifier] NULL,
	[oct_eye_id] [uniqueidentifier] NULL,
	[oct_date] [datetime] NULL,
	[oct_crt] [int] NULL,
	[oct_year] [int] NULL,
	[oct_nhsyear] [int] NULL
) ON [PRIMARY]
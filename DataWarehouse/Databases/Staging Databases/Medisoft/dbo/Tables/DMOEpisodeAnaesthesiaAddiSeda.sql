﻿CREATE TABLE [dbo].[DMOEpisodeAnaesthesiaAddiSeda](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[Drug] [varchar](255) NOT NULL
) ON [PRIMARY]
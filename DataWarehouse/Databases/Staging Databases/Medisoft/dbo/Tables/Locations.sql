﻿CREATE TABLE [dbo].[Locations](
	[LocationID] [int] NOT NULL,
	[LocationName] [varchar](200) NOT NULL,
	[Selectable] [int] NOT NULL,
	[Selected] [int] NOT NULL
) ON [PRIMARY]
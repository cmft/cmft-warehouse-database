﻿CREATE TABLE [dbo].[interfacelookupdatamapping](
	[ilm_interface_value_type] [varchar](255) NULL,
	[ilm_interface_value] [varchar](255) NULL,
	[ilm_medisoft_value_desc] [varchar](255) NULL,
	[ilm_medisoft_value_id] [uniqueidentifier] NULL
) ON [PRIMARY]
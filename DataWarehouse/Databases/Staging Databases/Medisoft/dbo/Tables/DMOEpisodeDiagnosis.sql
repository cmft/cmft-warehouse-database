﻿CREATE TABLE [dbo].[DMOEpisodeDiagnosis](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[DateOfDiagnosis] [smalldatetime] NOT NULL,
	[Eye] [varchar](1) NOT NULL,
	[UserID] [uniqueidentifier] NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[Type] [varchar](255) NULL,
	[Diagnosis] [varchar](255) NULL,
	[DateRecorded] [smalldatetime] NULL,
	[Active] [bit] NULL,
	[DateDeactivated] [datetime] NULL
) ON [PRIMARY]
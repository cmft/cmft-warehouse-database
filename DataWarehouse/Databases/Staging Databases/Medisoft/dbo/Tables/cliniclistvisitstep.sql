﻿CREATE TABLE [dbo].[cliniclistvisitstep](
	[cst_id] [int] IDENTITY(1,1) NOT NULL,
	[cst_clv_id] [int] NOT NULL,
	[cst_hcs_id] [int] NOT NULL,
	[cst_use_id] [int] NULL,
	[cst_order] [int] NULL,
	[cst_status] [int] NULL,
	[cst_stopclockcolor] [int] NULL,
	[cst_timestamp] [datetime] NULL,
	[cst_updated] [datetime] NULL,
	[cst_audit_userid] [int] NOT NULL DEFAULT ((-1)),
	[cst_audit_date] [datetime] NOT NULL DEFAULT (getdate()),
	[cst_stp_manual_id] [int] NULL
) ON [PRIMARY]
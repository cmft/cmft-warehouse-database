﻿CREATE TABLE [dbo].[outputstagepatientoperationsglatmp2](
	[Category] [varchar](255) NULL,
	[PatientID] [uniqueidentifier] NULL,
	[CriteriaDate] [datetime] NULL,
	[IOP] [int] NULL,
	[PeriodNo] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[episodetransactionsave](
	[ets_id] [uniqueidentifier] NOT NULL,
	[ets_epi_id] [uniqueidentifier] NOT NULL,
	[ets_datesaved] [datetime] NULL,
	[ets_datetransactionsaved] [datetime] NOT NULL DEFAULT (getdate()),
	[ets_duration] [int] NULL
) ON [PRIMARY]
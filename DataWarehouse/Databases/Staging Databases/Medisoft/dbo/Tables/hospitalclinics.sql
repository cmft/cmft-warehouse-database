﻿CREATE TABLE [dbo].[hospitalclinics](
	[cco_id] [uniqueidentifier] NOT NULL,
	[cco_loc_id] [uniqueidentifier] NULL,
	[cco_code] [varchar](20) NULL,
	[cco_desc] [varchar](255) NULL,
	[cco_booked] [bit] NULL,
	[cco_use_id_consultant] [uniqueidentifier] NULL,
	[cco_rft_id_mode] [uniqueidentifier] NULL,
	[cco_active] [bit] NULL
) ON [PRIMARY]
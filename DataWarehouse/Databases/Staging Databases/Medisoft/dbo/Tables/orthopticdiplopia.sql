﻿CREATE TABLE [dbo].[orthopticdiplopia](
	[orp_id] [uniqueidentifier] NOT NULL,
	[orp_ort_id] [uniqueidentifier] NOT NULL,
	[orp_diplopia] [int] NULL,
	[orp_rft_id_fromhorizontal] [uniqueidentifier] NULL,
	[orp_rft_id_tohorizontal] [uniqueidentifier] NULL,
	[orp_fromhorizontal] [int] NULL,
	[orp_tohorizontal] [int] NULL,
	[orp_rft_id_fromvertical] [uniqueidentifier] NULL,
	[orp_rft_id_tovertical] [uniqueidentifier] NULL,
	[orp_fromvertical] [int] NULL,
	[orp_tovertical] [int] NULL,
	[orp_diplopianr] [int] NULL,
	[orp_rft_id_fromhorizontalnr] [uniqueidentifier] NULL,
	[orp_rft_id_tohorizontalnr] [uniqueidentifier] NULL,
	[orp_fromhorizontalnr] [int] NULL,
	[orp_tohorizontalnr] [int] NULL,
	[orp_rft_id_fromverticalnr] [uniqueidentifier] NULL,
	[orp_rft_id_toverticalnr] [uniqueidentifier] NULL,
	[orp_fromverticalnr] [int] NULL,
	[orp_toverticalnr] [int] NULL,
	[orp_rft_id_sbisabar] [uniqueidentifier] NULL,
	[orp_sbisabar] [int] NULL
) ON [PRIMARY]
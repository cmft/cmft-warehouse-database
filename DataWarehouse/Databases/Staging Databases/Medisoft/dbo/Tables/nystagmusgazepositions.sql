﻿CREATE TABLE [dbo].[nystagmusgazepositions](
	[ngp_id] [uniqueidentifier] NOT NULL,
	[ngp_ort_id] [uniqueidentifier] NOT NULL,
	[ngp_positionno] [int] NULL,
	[ngp_rft_id_amplitude_r] [uniqueidentifier] NULL,
	[ngp_rft_id_amplitude_l] [uniqueidentifier] NULL,
	[ngp_rft_id_frequency_r] [uniqueidentifier] NULL,
	[ngp_rft_id_frequency_l] [uniqueidentifier] NULL,
	[ngp_rft_id_direction_r] [uniqueidentifier] NULL,
	[ngp_rft_id_direction_l] [uniqueidentifier] NULL,
	[ngp_seesaw] [bit] NULL,
	[ngp_null] [bit] NULL
) ON [PRIMARY]
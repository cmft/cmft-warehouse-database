﻿CREATE TABLE [dbo].[episodedrug](
	[epd_id] [uniqueidentifier] NOT NULL,
	[epd_epi_id] [uniqueidentifier] NULL,
	[epd_eye_id] [uniqueidentifier] NULL,
	[epd_dru_id] [uniqueidentifier] NULL,
	[epd_time] [varchar](255) NULL,
	[epd_dos_id] [uniqueidentifier] NULL,
	[epd_dro_id] [uniqueidentifier] NULL,
	[epd_dfr_id] [uniqueidentifier] NULL,
	[epd_ddu_id] [uniqueidentifier] NULL,
	[epd_dat_id] [uniqueidentifier] NULL,
	[epd_use_id_by] [uniqueidentifier] NULL,
	[epd_hyalase] [bit] NULL,
	[epd_order_id] [int] NULL,
	[epd_qualify] [varchar](255) NULL
) ON [PRIMARY]
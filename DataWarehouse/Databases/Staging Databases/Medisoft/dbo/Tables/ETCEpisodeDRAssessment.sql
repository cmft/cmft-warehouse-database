﻿CREATE TABLE [dbo].[ETCEpisodeDRAssessment](
	[ID] [bigint] NULL,
	[ExtractID] [int] NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[Eye] [varchar](255) NULL,
	[UserID] [uniqueidentifier] NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[ETDRSRetinopathy] [varchar](255) NULL,
	[ETDRSMaculopathy] [varchar](255) NULL,
	[ETDRSNumber] [int] NULL,
	[InternationalRetinopathy] [varchar](255) NULL,
	[InternationalMaculopathy] [varchar](255) NULL,
	[NDESPRetinopathy] [varchar](52) NULL,
	[NDESPMaculopathy] [varchar](52) NULL,
	[NDESPPhotoagulation] [varchar](52) NULL
) ON [PRIMARY]
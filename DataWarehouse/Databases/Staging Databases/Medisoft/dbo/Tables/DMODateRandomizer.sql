﻿CREATE TABLE [dbo].[DMODateRandomizer](
	[PatientID] [uniqueidentifier] NULL,
	[DaysOffset] [int] NULL
) ON [PRIMARY]
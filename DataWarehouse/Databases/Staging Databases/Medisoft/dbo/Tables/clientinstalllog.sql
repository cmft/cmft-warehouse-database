﻿CREATE TABLE [dbo].[clientinstalllog](
	[clo_id] [uniqueidentifier] NOT NULL,
	[clo_computername] [varchar](50) NULL,
	[clo_installedversion] [varchar](50) NULL,
	[clo_uninstalledversion] [varchar](50) NULL,
	[clo_timestamp] [datetime] NULL
) ON [PRIMARY]
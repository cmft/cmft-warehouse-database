﻿CREATE TABLE [dbo].[PAS_ManchesterCasenoteNumbers_Archive](
	[mca_CasenoteNumber] [nvarchar](50) NOT NULL,
	[mca_DistrictNumber] [nvarchar](50) NOT NULL,
	[mca_InternalPASNumber] [nvarchar](50) NULL
) ON [PRIMARY]
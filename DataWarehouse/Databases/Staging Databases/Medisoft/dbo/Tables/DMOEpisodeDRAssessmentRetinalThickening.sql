﻿CREATE TABLE [dbo].[DMOEpisodeDRAssessmentRetinalThickening](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[Eye] [char](1) NULL,
	[UserID] [uniqueidentifier] NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[None] [bit] NULL,
	[CystoidMacularOedema] [bit] NULL,
	[AtCentreOfFovea] [bit] NULL,
	[<500MicronsCentreOfFovea] [bit] NULL,
	[AdjacentToExudates<500MicronsFovealCentre] [bit] NULL,
	[Zone(s)>1DiscAreaAnyPart<1DDFovealCentre] [bit] NULL,
	[MacularOedemaButNoCSMO] [bit] NULL,
	[NoMacularLaserScarsVisible] [bit] NULL,
	[MacularLaserScarsVisible] [bit] NULL
) ON [PRIMARY]
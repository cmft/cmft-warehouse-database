﻿CREATE TABLE [dbo].[clientcontroller](
	[icm_id] [int] IDENTITY(1,1) NOT NULL,
	[icm_master_id] [int] NULL,
	[icm_instruction_id] [int] NULL,
	[icm_fks_id] [uniqueidentifier] NULL,
	[icm_transaction_id] [uniqueidentifier] NULL,
	[icm_machine] [varchar](1000) NULL,
	[icm_timestamp] [datetime] NULL,
	[icm_action] [int] NULL,
	[icm_audit_userid] [int] NOT NULL,
	[icm_audit_date] [datetime] NOT NULL
) ON [PRIMARY]
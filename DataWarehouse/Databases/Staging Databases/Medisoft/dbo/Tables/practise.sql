﻿CREATE TABLE [dbo].[practise](
	[pra_id] [uniqueidentifier] NOT NULL,
	[pra_code] [varchar](255) NOT NULL,
	[pra_desc] [varchar](255) NOT NULL,
	[pra_type] [int] NULL
) ON [PRIMARY]
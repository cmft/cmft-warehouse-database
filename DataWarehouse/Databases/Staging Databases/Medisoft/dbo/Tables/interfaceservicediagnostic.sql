﻿CREATE TABLE [dbo].[interfaceservicediagnostic](
	[isd_id] [uniqueidentifier] NOT NULL,
	[isd_its_id] [uniqueidentifier] NULL,
	[isd_computername] [varchar](255) NULL,
	[isd_username] [varchar](255) NULL,
	[isd_diagnostic] [varchar](255) NULL,
	[isd_result] [varchar](255) NULL,
	[isd_timestamp] [datetime] NULL
) ON [PRIMARY]
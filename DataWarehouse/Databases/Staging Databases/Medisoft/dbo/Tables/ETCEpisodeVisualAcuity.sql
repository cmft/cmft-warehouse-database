﻿CREATE TABLE [dbo].[ETCEpisodeVisualAcuity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[WeeksSinceFirstInjection] [int] NULL,
	[Eye] [varchar](1) NOT NULL,
	[EpisodeID] [uniqueidentifier] NOT NULL,
	[Notation] [varchar](255) NULL,
	[BestMeasure] [varchar](255) NULL,
	[Unaided] [varchar](50) NULL,
	[Pinhole] [varchar](50) NULL,
	[BestCorrected] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[proceduredetailplanned](
	[pri_id] [uniqueidentifier] NOT NULL,
	[pri_prl_id] [uniqueidentifier] NOT NULL,
	[pri_rft_id_drugsinjected1] [uniqueidentifier] NULL,
	[pri_rft_id_drugsinjected2] [uniqueidentifier] NULL,
	[pri_opeyenumber] [int] NULL,
	[pri_predpostref] [float] NULL,
	[pri_rft_id_planfollowup] [uniqueidentifier] NULL,
	[pri_iol_model] [uniqueidentifier] NULL,
	[pri_model_status] [int] NULL
) ON [PRIMARY]
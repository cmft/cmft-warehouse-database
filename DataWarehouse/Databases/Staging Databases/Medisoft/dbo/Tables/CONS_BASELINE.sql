﻿CREATE TABLE [dbo].[CONS_BASELINE](
	[con_gps_code] [varchar](255) NOT NULL,
	[con_gps_initials] [varchar](255) NOT NULL,
	[con_gps_surname] [varchar](255) NOT NULL,
	[con_pra_code] [varchar](255) NOT NULL,
	[con_add_address1] [varchar](255) NULL,
	[con_add_address2] [varchar](255) NULL,
	[con_add_address3] [varchar](255) NULL,
	[con_add_address4] [varchar](255) NULL,
	[con_add_postcode] [varchar](255) NULL
) ON [PRIMARY]
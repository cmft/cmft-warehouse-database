﻿CREATE TABLE [dbo].[medisoft_checkpatientagainsthl7demogs_results](
	[pat_id] [uniqueidentifier] NOT NULL,
	[pat_details] [varchar](542) NULL,
	[hl7_details] [varchar](255) NULL
) ON [PRIMARY]
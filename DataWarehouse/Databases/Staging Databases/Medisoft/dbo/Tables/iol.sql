﻿CREATE TABLE [dbo].[iol](
	[iol_id] [uniqueidentifier] NOT NULL,
	[iol_model] [varchar](255) NOT NULL,
	[iol_available] [bit] NOT NULL DEFAULT (1),
	[iol_multiplerange] [bit] NULL DEFAULT (0),
	[iol_stockitem] [bit] NULL
) ON [PRIMARY]
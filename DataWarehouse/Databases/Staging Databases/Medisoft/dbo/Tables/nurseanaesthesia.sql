﻿CREATE TABLE [dbo].[nurseanaesthesia](
	[naz_id] [uniqueidentifier] NOT NULL,
	[naz_use_id_nurse] [uniqueidentifier] NULL,
	[naz_use_id_consultant] [uniqueidentifier] NULL,
	[naz_timein] [varchar](255) NOT NULL,
	[naz_timestart] [varchar](255) NOT NULL,
	[naz_timeready] [varchar](255) NOT NULL,
	[naz_timeout] [varchar](255) NOT NULL,
	[naz_rft_id_pulse] [uniqueidentifier] NULL,
	[naz_surgerycancelled] [bit] NULL,
	[naz_caution_html] [varchar](8000) NULL,
	[naz_epi_id_plannedoperation] [uniqueidentifier] NULL,
	[naz_eye_id_drug] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[cataractiollink](
	[cil_id] [uniqueidentifier] NOT NULL,
	[cil_cat_id] [uniqueidentifier] NULL,
	[cil_iol_id] [uniqueidentifier] NULL,
	[cil_power] [float] NULL,
	[cil_order] [tinyint] NULL
) ON [PRIMARY]
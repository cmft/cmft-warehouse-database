﻿CREATE TABLE [dbo].[interfacecliniclist](
	[icl_id] [uniqueidentifier] NOT NULL,
	[icl_use_id_consultant] [uniqueidentifier] NULL,
	[icl_pat_id] [uniqueidentifier] NULL,
	[icl_loc_id] [uniqueidentifier] NULL,
	[icl_clinicdatetime] [datetime] NULL,
	[icl_timearrived] [datetime] NULL,
	[icl_cliniccode] [varchar](255) NULL,
	[icl_clinicname] [varchar](255) NULL,
	[icl_timestamp] [datetime] NULL,
	[icl_newpatient] [bit] NULL,
	[icl_manual] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[interfacedigitalhealthcaresettings](
	[ids_id] [uniqueidentifier] NOT NULL,
	[ids_desc] [varchar](255) NULL,
	[ids_value] [varchar](255) NULL
) ON [PRIMARY]
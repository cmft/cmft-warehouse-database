﻿CREATE TABLE [dbo].[iolaconstantlink](
	[iac_id] [uniqueidentifier] NOT NULL,
	[iac_iol_id] [uniqueidentifier] NOT NULL,
	[iac_date] [datetime] NULL,
	[iac_customised] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[interfacedataophthalsuitepatientmatch](
	[osm_ipm_id] [uniqueidentifier] NULL,
	[osm_bw_patient_id] [int] NULL,
	[osm_pat_id] [uniqueidentifier] NULL
) ON [PRIMARY]
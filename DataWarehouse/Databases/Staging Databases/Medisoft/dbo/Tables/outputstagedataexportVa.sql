﻿CREATE TABLE [dbo].[outputstagedataexportVa](
	[OperationID] [uniqueidentifier] NULL,
	[VAPREBestcorrected] [decimal](7, 2) NULL,
	[VAPostBestcorrected] [decimal](7, 2) NULL,
	[VAPREUnaided] [decimal](7, 2) NULL,
	[VAPostUnaided] [decimal](7, 2) NULL,
	[VAPREPinhole] [decimal](7, 2) NULL,
	[VAPostPinhole] [decimal](7, 2) NULL
) ON [PRIMARY]
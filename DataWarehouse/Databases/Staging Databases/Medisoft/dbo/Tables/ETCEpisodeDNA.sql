﻿CREATE TABLE [dbo].[ETCEpisodeDNA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[PatientID] [uniqueidentifier] NULL,
	[Date] [datetime] NULL,
	[EpisodeID] [uniqueidentifier] NULL,
	[EpisodeType] [varchar](max) NULL,
	[DNAOutcome] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
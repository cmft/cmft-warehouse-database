﻿CREATE TABLE [dbo].[PAS_MedisoftJune2013](
	[HospitalNumbers] [varchar](max) NULL,
	[DistrictNumber] [varchar](50) NOT NULL,
	[InternalPASID] [varchar](50) NOT NULL,
	[NHSNumber] [char](12) NULL,
	[NHSNumberStatus] [varchar](2) NULL,
	[Surname] [varchar](255) NULL,
	[FirstName] [varchar](20) NULL,
	[Title] [varchar](5) NULL,
	[WishesToBeCalled] [varchar](255) NULL,
	[SurnameAtBirth] [varchar](100) NULL,
	[Occupation] [varchar](255) NULL,
	[DateOfBirth] [date] NULL,
	[DateOfDeath] [date] NULL,
	[Sex] [varchar](1) NULL,
	[EthnicType] [varchar](4) NULL,
	[MaritalStatus] [varchar](1) NULL,
	[Religion] [varchar](4) NULL,
	[Address1] [varchar](255) NULL,
	[Address2] [varchar](255) NULL,
	[Address3] [varchar](255) NULL,
	[Address4] [varchar](255) NULL,
	[PostCode] [varchar](10) NULL,
	[TelDay] [varchar](50) NULL,
	[TelEvening] [varchar](50) NULL,
	[Mobile] [varchar](50) NULL,
	[NoKName] [varchar](255) NULL,
	[NoKRelationship] [varchar](255) NULL,
	[NoKAddrLine1] [varchar](255) NULL,
	[NoKAddrLine2] [varchar](255) NULL,
	[NoKAddrLine3] [varchar](255) NULL,
	[NoKAddrLine4] [varchar](255) NULL,
	[NoKPostCode] [varchar](255) NULL,
	[NoKHomePhone] [varchar](50) NULL,
	[NoKWorkPhone] [varchar](50) NULL,
	[GPNationalCode] [nvarchar](8) NULL,
	[GPPracticeCode] [nvarchar](6) NULL,
	[GpSurname] [nvarchar](100) NULL,
	[GpInitials] [nvarchar](50) NULL,
	[LastUpdated] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
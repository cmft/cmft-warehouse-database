﻿CREATE TABLE [dbo].[usersite](
	[usi_id] [uniqueidentifier] NOT NULL,
	[usi_hos_id] [uniqueidentifier] NOT NULL,
	[usi_use_id] [uniqueidentifier] NOT NULL,
	[usi_status] [int] NOT NULL
) ON [PRIMARY]
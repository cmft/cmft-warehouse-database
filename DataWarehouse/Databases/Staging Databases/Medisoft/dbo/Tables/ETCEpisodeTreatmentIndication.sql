﻿CREATE TABLE [dbo].[ETCEpisodeTreatmentIndication](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExtractID] [int] NOT NULL,
	[TreatmentID] [uniqueidentifier] NOT NULL,
	[Indication] [varchar](255) NULL
) ON [PRIMARY]
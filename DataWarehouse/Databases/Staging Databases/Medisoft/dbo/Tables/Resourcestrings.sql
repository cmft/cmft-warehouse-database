﻿CREATE TABLE [dbo].[Resourcestrings](
	[resourcestrings_id] [int] IDENTITY(1,1) NOT NULL,
	[lang] [varchar](50) NOT NULL,
	[key] [varchar](50) NOT NULL,
	[value] [varchar](50) NOT NULL
) ON [PRIMARY]
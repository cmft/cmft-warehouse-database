﻿CREATE TABLE [dbo].[paslookup](
	[plu_id] [uniqueidentifier] NOT NULL,
	[plu_type] [varchar](255) NULL,
	[plu_code] [varchar](255) NULL,
	[plu_lookup] [varchar](255) NULL
) ON [PRIMARY]
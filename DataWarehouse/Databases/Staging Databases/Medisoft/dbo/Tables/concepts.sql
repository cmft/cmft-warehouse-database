﻿CREATE TABLE [dbo].[concepts](
	[cnc_id] [uniqueidentifier] NOT NULL,
	[cnc_no] [int] NOT NULL,
	[cnc_code] [varchar](4) NOT NULL,
	[cnc_default] [varchar](255) NOT NULL,
	[cnc_sitevalue] [varchar](255) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[drscreening](
	[drr_id] [uniqueidentifier] NOT NULL,
	[drr_datereferral] [datetime] NOT NULL,
	[drr_datenotification] [datetime] NULL,
	[drr_rft_id_priority] [uniqueidentifier] NULL,
	[drr_rft_id_screeningprogram] [uniqueidentifier] NULL,
	[drr_rft_id_diabetestype] [uniqueidentifier] NULL,
	[drr_datediagnosis] [datetime] NULL,
	[drr_rft_id_referredfor] [uniqueidentifier] NULL,
	[drr_use_id_recordedby] [uniqueidentifier] NULL,
	[drr_datescreened] [datetime] NULL,
	[drr_rft_id_referredfrom] [uniqueidentifier] NULL,
	[drr_gradeversion] [int] NULL
) ON [PRIMARY]
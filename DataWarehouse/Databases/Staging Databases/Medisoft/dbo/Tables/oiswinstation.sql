﻿CREATE TABLE [dbo].[oiswinstation](
	[ois_id] [uniqueidentifier] NOT NULL,
	[ois_eye] [nvarchar](4) NULL,
	[ois_procedures] [varchar](128) NULL
) ON [PRIMARY]
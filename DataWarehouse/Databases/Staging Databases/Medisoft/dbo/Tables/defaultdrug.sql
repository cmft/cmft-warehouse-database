﻿CREATE TABLE [dbo].[defaultdrug](
	[ddg_id] [uniqueidentifier] NOT NULL,
	[ddg_dru_id] [uniqueidentifier] NULL,
	[ddg_dat_id] [uniqueidentifier] NULL,
	[ddg_dos_id] [uniqueidentifier] NULL,
	[ddg_dro_id] [uniqueidentifier] NULL,
	[ddg_dfr_id] [uniqueidentifier] NULL,
	[ddg_ddu_id] [uniqueidentifier] NULL,
	[ddg_fks_id] [uniqueidentifier] NULL,
	[ddg_order] [int] NULL,
	[ddg_indef] [bit] NULL,
	[ddg_continueontto] [int] NULL,
	[ddg_rft_id_caution] [uniqueidentifier] NULL,
	[ddg_asrequired] [bit] NULL DEFAULT (0)
) ON [PRIMARY]
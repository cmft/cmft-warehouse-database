﻿CREATE TABLE [dbo].[episodeinvestigation](
	[ein_id] [uniqueidentifier] NOT NULL,
	[ein_epi_id] [uniqueidentifier] NULL,
	[ein_rft_id_investigation] [uniqueidentifier] NULL,
	[ein_rft_id_investigationstatus] [uniqueidentifier] NULL,
	[ein_result] [varchar](255) NULL
) ON [PRIMARY]
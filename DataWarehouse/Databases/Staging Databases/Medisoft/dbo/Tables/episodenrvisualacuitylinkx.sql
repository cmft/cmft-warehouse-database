﻿CREATE TABLE [dbo].[episodenrvisualacuitylinkx](
	[vnx_id] [uniqueidentifier] NOT NULL,
	[vnx_epi_id] [uniqueidentifier] NULL,
	[vnx_eye_id] [uniqueidentifier] NULL,
	[vnx_rft_id_unaided] [uniqueidentifier] NULL,
	[vnx_rft_id_bestcorrected] [uniqueidentifier] NULL,
	[vnx_lens] [smallint] NULL,
	[vnx_readingset] [int] NULL,
	[vnx_rft_id_measurementtype] [uniqueidentifier] NULL,
	[vnx_measurementdistance] [int] NULL,
	[vnx_primary] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[nurseclinic](
	[ncv_id] [uniqueidentifier] NOT NULL,
	[ncv_use_id_seenby] [uniqueidentifier] NULL,
	[ncv_use_id_consultant] [uniqueidentifier] NULL,
	[ncv_time] [varchar](255) NOT NULL,
	[ncv_use_id_dataenteredby] [uniqueidentifier] NULL,
	[ncv_eye_id_dilated] [uniqueidentifier] NULL,
	[ncv_pairsofglasses] [int] NULL,
	[ncv_rft_id_hfamachine] [uniqueidentifier] NULL
) ON [PRIMARY]
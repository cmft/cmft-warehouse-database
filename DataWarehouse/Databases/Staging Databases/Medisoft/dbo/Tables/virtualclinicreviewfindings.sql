﻿CREATE TABLE [dbo].[virtualclinicreviewfindings](
	[vrf_id] [uniqueidentifier] NOT NULL,
	[vrf_epi_id] [uniqueidentifier] NOT NULL,
	[vrf_desc] [varchar](255) NULL,
	[vrf_findingtype] [int] NULL,
	[vrf_numeyes] [int] NULL,
	[vrf_significant_id] [uniqueidentifier] NULL,
	[vrf_significant_id_2] [uniqueidentifier] NULL
) ON [PRIMARY]
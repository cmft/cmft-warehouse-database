﻿CREATE TABLE [dbo].[Note](
	[NoteId] [int] NOT NULL,
	[Note] [text] NULL,
	[NoteType] [char](1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[SecureActionPermission](
	[ID] [int] NOT NULL,
	[SecureActionID] [int] NOT NULL,
	[ADGroupName] [varchar](250) NOT NULL
) ON [PRIMARY]
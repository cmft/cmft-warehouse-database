﻿CREATE TABLE [dbo].[ActionMenuItem](
	[ItemCode] [varchar](10) NOT NULL,
	[ItemText] [varchar](200) NULL,
	[ItemParentCode] [varchar](10) NULL,
	[JSFunctionText] [varchar](8000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Consultant](
	[Code] [varchar](40) NOT NULL,
	[Title] [varchar](20) NULL,
	[GivenName] [varchar](250) NOT NULL,
	[FamilyName] [varchar](250) NOT NULL,
	[FullName] [varchar](250) NULL,
	[Email] [varchar](250) NULL,
	[NationalCode] [varchar](50) NULL,
	[Abbrev] [varchar](250) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Bed](
	[BedId] [uniqueidentifier] NOT NULL,
	[BedCode] [varchar](20) NOT NULL,
	[BedAbbrev] [varchar](50) NULL,
	[BedName] [varchar](80) NULL,
	[RoomCode] [varchar](20) NOT NULL,
	[RoomAbbrev] [varchar](50) NULL,
	[RoomName] [varchar](80) NULL,
	[WardCode] [varchar](20) NOT NULL,
	[WardAbbrev] [varchar](50) NULL,
	[WardName] [varchar](80) NULL,
	[Status] [varchar](20) NOT NULL CONSTRAINT [DF__Bed__Status__59FA5E80]  DEFAULT ('Available')
) ON [PRIMARY]
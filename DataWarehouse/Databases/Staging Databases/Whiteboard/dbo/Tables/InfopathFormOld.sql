﻿CREATE TABLE [dbo].[InfopathFormOld](
	[FormId] [int] NOT NULL,
	[UserName] [varchar](255) NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[FormType] [varchar](250) NOT NULL,
	[FormStatus] [varchar](255) NULL,
	[FormXML] [xml] NOT NULL,
	[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_InfopathForm_DateAdded]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
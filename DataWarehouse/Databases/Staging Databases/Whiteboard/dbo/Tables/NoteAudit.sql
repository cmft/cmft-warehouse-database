﻿CREATE TABLE [dbo].[NoteAudit](
	[AuditId] [int] NOT NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[Timestamp] [datetime] NOT NULL CONSTRAINT [DF_NoteAudit_Timestamp]  DEFAULT (getdate()),
	[Note] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
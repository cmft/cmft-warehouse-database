﻿CREATE TABLE [dbo].[FCETrackingLog](
	[ID] [int] NOT NULL,
	[MessageType] [varchar](20) NULL,
	[AdmissionNumber] [varchar](20) NULL,
	[PatientNumber1] [varchar](50) NULL,
	[PatientNumber2] [varchar](50) NULL,
	[ConsultantCode] [varchar](20) NULL,
	[SpecialtyCode] [varchar](20) NULL,
	[WardCode] [varchar](20) NULL,
	[RoomCode] [varchar](20) NULL,
	[BedCode] [varchar](20) NULL,
	[MessageID] [varchar](255) NULL,
	[MessageDateTime] [varchar](50) NULL,
	[LogDateTime] [datetime] NULL CONSTRAINT [DF_FCETrackingLog_LogDateTime]  DEFAULT (getdate())
) ON [PRIMARY]
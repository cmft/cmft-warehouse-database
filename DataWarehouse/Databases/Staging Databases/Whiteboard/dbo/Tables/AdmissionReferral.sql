﻿CREATE TABLE [dbo].[AdmissionReferral](
	[AdmissionId] [varchar](10) NOT NULL,
	[ReferralType] [varchar](3) NOT NULL,
	[Status] [varchar](20) NOT NULL CONSTRAINT [DF_AdmissionReferral_Status]  DEFAULT ('Referred')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SecureAction](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[PageName] [varchar](250) NOT NULL
) ON [PRIMARY]
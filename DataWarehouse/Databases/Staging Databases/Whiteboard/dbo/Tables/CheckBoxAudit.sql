﻿CREATE TABLE [dbo].[CheckBoxAudit](
	[AuditId] [int] NOT NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_CheckBoxAudit_ModifiedDate]  DEFAULT (getdate()),
	[FieldName] [varchar](250) NOT NULL,
	[NewValue] [bit] NOT NULL
) ON [PRIMARY]
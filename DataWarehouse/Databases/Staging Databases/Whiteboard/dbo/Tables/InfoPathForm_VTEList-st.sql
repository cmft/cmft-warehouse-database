﻿CREATE TABLE [dbo].[InfoPathForm_VTEList-st](
	[FormId] [int] NOT NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[UserName] [varchar](255) NULL,
	[FormType] [varchar](250) NOT NULL,
	[FormStatus] [varchar](255) NULL,
	[DateAdded] [datetime] NOT NULL,
	[EPMINumber] [varchar](20) NULL,
	[OriginalCreationDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Specialty](
	[Code] [varchar](50) NOT NULL,
	[Name] [varchar](250) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ConsultantSpecialty](
	[ID] [int] NOT NULL,
	[ConsultantCode] [varchar](50) NOT NULL,
	[SpecialtyCode] [varchar](50) NOT NULL,
	[DepartmentCode] [varchar](50) NOT NULL
) ON [PRIMARY]
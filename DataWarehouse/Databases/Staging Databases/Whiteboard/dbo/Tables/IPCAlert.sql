﻿CREATE TABLE [dbo].[IPCAlert](
	[AlertId] [int] NOT NULL,
	[AdmissionNumber] [varchar](50) NOT NULL,
	[AlertType] [varchar](100) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[AdditionalText] [varchar](1000) NULL,
	[AlertDescription] [varchar](1000) NULL,
	[AlertActive] [bit] NOT NULL,
	[TypeActive] [bit] NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
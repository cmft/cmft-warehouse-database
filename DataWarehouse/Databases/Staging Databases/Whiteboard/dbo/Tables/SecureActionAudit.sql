﻿CREATE TABLE [dbo].[SecureActionAudit](
	[UpdateID] [int] NOT NULL,
	[AdmissionNumber] [varchar](20) NOT NULL,
	[Username] [varchar](255) NOT NULL,
	[UpdatedField] [varchar](250) NOT NULL,
	[NewDateValue] [datetime] NULL,
	[NewBitValue] [bit] NULL,
	[UpdateTime] [datetime] NOT NULL CONSTRAINT [DF_SecureActionAudit_UpdateTime]  DEFAULT (getdate())
) ON [PRIMARY]
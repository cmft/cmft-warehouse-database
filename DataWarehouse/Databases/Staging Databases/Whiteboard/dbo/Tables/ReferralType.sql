﻿CREATE TABLE [dbo].[ReferralType](
	[Code] [varchar](3) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[ParentCode] [varchar](3) NULL
) ON [PRIMARY]
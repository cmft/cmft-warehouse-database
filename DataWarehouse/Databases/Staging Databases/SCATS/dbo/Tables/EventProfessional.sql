﻿CREATE TABLE [dbo].[EventProfessional](
	[EventProfNo] [int] IDENTITY(1,1) NOT NULL,
	[EventTypeCode] [smallint] NOT NULL,
	[ActualEventNo] [int] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
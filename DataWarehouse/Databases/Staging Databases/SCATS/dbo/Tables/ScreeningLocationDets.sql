﻿CREATE TABLE [dbo].[ScreeningLocationDets](
	[ScreeningNo] [int] NOT NULL,
	[ScreenLocDetsNo] [int] IDENTITY(1,1) NOT NULL,
	[OrganNo] [int] NULL,
	[WhereInd] [char](1) NOT NULL CONSTRAINT [DF_ScreeningLocationDets_WhereInd]  DEFAULT ('Y'),
	[OrganTypeCode] [smallint] NULL,
	[NonUkLocation] [varchar](200) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ScreeningLocationDets_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Risk](
	[RiskCode] [smallint] IDENTITY(1,1) NOT NULL,
	[RiskDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RiskType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
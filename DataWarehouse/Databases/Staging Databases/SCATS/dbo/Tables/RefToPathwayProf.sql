﻿CREATE TABLE [dbo].[RefToPathwayProf](
	[RefToPathActivityProfNo] [int] IDENTITY(1,1) NOT NULL,
	[RefPathActivityNo] [int] NULL,
	[EventTypeCode] [smallint] NOT NULL,
	[ReferredToNo] [int] NULL,
	[AuthPersonNo] [int] NOT NULL,
	[ReferredToDate] [datetime] NOT NULL,
	[ReferToLocInd] [char](1) NOT NULL CONSTRAINT [DF_RefToPathwayProf_ReferToLocInd]  DEFAULT ('N'),
	[ActualSeenDate] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RefToPathwayProf_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
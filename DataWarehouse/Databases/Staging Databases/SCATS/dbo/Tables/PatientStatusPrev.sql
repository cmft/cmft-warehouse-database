﻿CREATE TABLE [dbo].[PatientStatusPrev](
	[PatientStatusCode] [smallint] NOT NULL,
	[PatientStatusDesc] [varchar](50) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WaitingListType](
	[WLTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[WLTypeDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_WaitingListType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
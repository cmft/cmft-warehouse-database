﻿CREATE TABLE [dbo].[InvestigationTest](
	[InvestigTestCode] [smallint] IDENTITY(1,1) NOT NULL,
	[InvestigTestDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_InvestigationTest_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
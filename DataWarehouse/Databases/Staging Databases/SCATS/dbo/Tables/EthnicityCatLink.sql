﻿CREATE TABLE [dbo].[EthnicityCatLink](
	[EthnicityCatCode] [smallint] NOT NULL,
	[EthnicityCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EthnicityCatLink_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MaritalStatus](
	[MaritalStatusCode] [smallint] IDENTITY(1,1) NOT NULL,
	[MaritalStatusDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_MaritalStatus_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
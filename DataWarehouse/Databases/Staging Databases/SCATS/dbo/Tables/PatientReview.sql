﻿CREATE TABLE [dbo].[PatientReview](
	[PatientReviewNo] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[ReviewTypeCode] [smallint] NOT NULL,
	[ReviewDate] [datetime] NOT NULL CONSTRAINT [DF_PatientReview_ReviewDate]  DEFAULT (getdate()),
	[ReferralNo] [int] NULL,
	[AuthPersonNo] [int] NOT NULL,
	[MemoPad] [varchar](max) NULL,
	[ContactTypeCode] [smallint] NOT NULL,
	[IssueTypeCode] [smallint] NULL,
	[ANCPartnerPersonId] [int] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientReview_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
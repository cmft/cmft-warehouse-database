﻿CREATE TABLE [dbo].[Ethnicity](
	[EthnicityCode] [smallint] IDENTITY(1,1) NOT NULL,
	[EthnicityDesc] [varchar](50) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Ethnicity_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
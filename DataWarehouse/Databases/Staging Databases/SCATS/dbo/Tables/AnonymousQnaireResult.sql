﻿CREATE TABLE [dbo].[AnonymousQnaireResult](
	[AnonymAnswerNo] [int] NOT NULL,
	[QnaireHeaderNo] [int] NOT NULL,
	[QnaireTypeCode] [smallint] NOT NULL,
	[QuestionGroupCode] [smallint] NOT NULL,
	[QuestionNo] [int] NOT NULL,
	[AnsOptionCode] [smallint] NOT NULL,
	[PositionalOrderCode] [smallint] NOT NULL,
	[MultiChoiceOptionCode] [smallint] NULL,
	[MultiChoiceHdrOrdCode] [smallint] NULL,
	[AnswerInd] [char](1) NULL,
	[ResultEntry] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
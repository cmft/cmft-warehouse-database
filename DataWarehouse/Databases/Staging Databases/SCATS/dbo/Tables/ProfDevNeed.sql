﻿CREATE TABLE [dbo].[ProfDevNeed](
	[ProfDevNeedCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ProfDevNeedDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ProfDevNeed_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ParentAwareAttendance](
	[CounsellingNo] [int] NOT NULL,
	[ParentAwareCode] [smallint] NOT NULL,
	[AnswerInd] [char](1) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ParentAwareAttendance_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
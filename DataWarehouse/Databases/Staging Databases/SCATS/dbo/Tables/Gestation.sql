﻿CREATE TABLE [dbo].[Gestation](
	[GestationNo] [int] IDENTITY(1,1) NOT NULL,
	[PregnancyNo] [int] NOT NULL,
	[GestationTypeCode] [smallint] NOT NULL,
	[ReviewDate] [datetime] NOT NULL,
	[GestationResult] [varchar](100) NOT NULL,
	[MemoPad] [varchar](500) NULL CONSTRAINT [DF_Gestation_MemoPad]  DEFAULT ('gestation outcome'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Gestation_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
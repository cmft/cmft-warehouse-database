﻿CREATE TABLE [dbo].[PatientDiagnosis](
	[PatientId] [int] NOT NULL,
	[DiagnosisTypeCode] [smallint] NOT NULL,
	[DiagnosisCode] [smallint] NOT NULL,
	[DiagnosedDate] [datetime] NOT NULL CONSTRAINT [DF_PatientDiagnosis_DiagnosedDate]  DEFAULT (getdate()),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientDiagnosis_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
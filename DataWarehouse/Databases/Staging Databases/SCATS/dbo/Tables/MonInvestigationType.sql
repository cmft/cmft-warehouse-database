﻿CREATE TABLE [dbo].[MonInvestigationType](
	[MonInvestigTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[MonInvestigTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_InvestigationType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
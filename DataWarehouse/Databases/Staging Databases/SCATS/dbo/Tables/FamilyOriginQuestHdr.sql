﻿CREATE TABLE [dbo].[FamilyOriginQuestHdr](
	[FamilyOrigQuestNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[EstimatedDeliveryDate] [datetime] NULL,
	[ScreeningTestDeclinedInd] [char](1) NOT NULL,
	[ScreeningDeclinedReason] [varchar](500) NULL,
	[MemoPAD] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
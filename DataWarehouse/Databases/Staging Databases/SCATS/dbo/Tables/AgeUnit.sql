﻿CREATE TABLE [dbo].[AgeUnit](
	[AgeUnitCode] [smallint] IDENTITY(1,1) NOT NULL,
	[AgeUnitDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AgeUnit_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
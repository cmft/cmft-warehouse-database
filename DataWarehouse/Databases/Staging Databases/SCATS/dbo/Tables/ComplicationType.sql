﻿CREATE TABLE [dbo].[ComplicationType](
	[ComplicationTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ComplicationTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Complication_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
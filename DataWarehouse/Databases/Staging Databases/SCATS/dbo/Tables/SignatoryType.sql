﻿CREATE TABLE [dbo].[SignatoryType](
	[SignatoryTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[SignatoryTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SignatoryType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
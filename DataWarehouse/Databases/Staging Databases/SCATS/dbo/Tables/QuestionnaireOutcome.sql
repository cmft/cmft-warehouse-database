﻿CREATE TABLE [dbo].[QuestionnaireOutcome](
	[QnaireHeaderNo] [int] NOT NULL,
	[QnaireOutItemCode] [smallint] NOT NULL,
	[AnsOptionCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
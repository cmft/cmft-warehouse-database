﻿CREATE TABLE [dbo].[Vaccine](
	[VaccineCode] [smallint] IDENTITY(1,1) NOT NULL,
	[VaccineDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Vaccine_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ProcessStage](
	[ProcessStageCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ProcessStageDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ProcessStage_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
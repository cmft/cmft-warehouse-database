﻿CREATE TABLE [dbo].[IllnessWarningSign](
	[TreatmentNo] [int] NOT NULL,
	[TreatmentOfferedCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[IllnessCode] [smallint] NOT NULL,
	[DetectionDate] [datetime] NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
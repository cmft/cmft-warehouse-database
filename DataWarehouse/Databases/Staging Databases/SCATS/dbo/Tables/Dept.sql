﻿CREATE TABLE [dbo].[Dept](
	[DeptCode] [smallint] IDENTITY(1,1) NOT NULL,
	[DeptName] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Dept_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
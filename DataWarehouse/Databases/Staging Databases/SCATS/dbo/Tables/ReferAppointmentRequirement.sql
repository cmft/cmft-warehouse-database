﻿CREATE TABLE [dbo].[ReferAppointmentRequirement](
	[ReferralNo] [int] NOT NULL,
	[RequirementTypeCode] [smallint] NOT NULL,
	[NoFDays] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AppointmentRequirement_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[StaffCaseloadPeriod](
	[CaseLoadPeriodNo] [int] IDENTITY(1,1) NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInactive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_StaffCaseload_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
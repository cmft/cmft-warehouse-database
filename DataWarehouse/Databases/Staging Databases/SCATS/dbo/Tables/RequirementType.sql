﻿CREATE TABLE [dbo].[RequirementType](
	[RequirementTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[RequirementTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RequirementType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ProcessStageOutcome](
	[ProcessStageCode] [smallint] IDENTITY(1,1) NOT NULL,
	[OutcomeCode] [smallint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [datetime] NOT NULL
) ON [PRIMARY]
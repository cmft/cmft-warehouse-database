﻿CREATE TABLE [dbo].[AppointmentType](
	[AppointTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[AppointTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AppointmentType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MonExpectedResult](
	[MonExpectedResultCode] [smallint] IDENTITY(1,1) NOT NULL,
	[MonExpectedResultDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ExpectedResult_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ReasonType](
	[ReasonTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReasonTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReasonType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClassifiedDiagnosis](
	[DiagnosisTypeCode] [smallint] NOT NULL,
	[DiagnosisCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClassifiedDiagnosis_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
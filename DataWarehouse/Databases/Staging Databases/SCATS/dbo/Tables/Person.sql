﻿CREATE TABLE [dbo].[Person](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,
	[Surname] [varchar](30) NOT NULL,
	[Forename] [varchar](20) NOT NULL,
	[MidInits] [varchar](30) NULL,
	[Gender] [char](1) NULL,
	[TitleCode] [smallint] NULL,
	[PersonTypeCode] [smallint] NOT NULL,
	[JobTitleCode] [smallint] NULL,
	[DateRegistered] [datetime] NOT NULL CONSTRAINT [DF_Person_DateRegistered]  DEFAULT (getdate()),
	[DateDecommissioned] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Person_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PreviousPartner](
	[MainPersonId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_PreviousPartner_DateEffective]  DEFAULT (getdate()),
	[DateInActive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PreviousPartner_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
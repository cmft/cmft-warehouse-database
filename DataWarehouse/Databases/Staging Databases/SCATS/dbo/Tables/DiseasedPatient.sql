﻿CREATE TABLE [dbo].[DiseasedPatient](
	[PatientId] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_DiseasedPatient_DateCreated]  DEFAULT (getdate()),
	[DateDischarged] [datetime] NULL,
	[AuthPersonNo] [int] NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_DiseasedPatient_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
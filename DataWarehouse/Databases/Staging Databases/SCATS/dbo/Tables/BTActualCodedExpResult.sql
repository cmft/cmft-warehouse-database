﻿CREATE TABLE [dbo].[BTActualCodedExpResult](
	[BTResultTypeOPCode] [smallint] NOT NULL,
	[BloodTestCode] [smallint] NOT NULL,
	[BloodTestSubCode] [smallint] NOT NULL,
	[BTResultRatioTypeCode] [smallint] NOT NULL,
	[ExpBTResOpRangeCode] [smallint] NOT NULL,
	[BTestCodedResultTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_BTActualCodedExpResult_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
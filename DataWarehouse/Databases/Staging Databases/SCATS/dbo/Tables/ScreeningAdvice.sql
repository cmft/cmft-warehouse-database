﻿CREATE TABLE [dbo].[ScreeningAdvice](
	[ScreeningNo] [int] NOT NULL,
	[ReasonSpecificCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ScreeningAdvice_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
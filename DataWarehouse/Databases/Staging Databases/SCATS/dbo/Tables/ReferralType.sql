﻿CREATE TABLE [dbo].[ReferralType](
	[ReferTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReferTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
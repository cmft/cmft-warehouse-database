﻿CREATE TABLE [dbo].[VaccinationDueAge](
	[VaccAgeDueCode] [smallint] IDENTITY(1,1) NOT NULL,
	[VaccAgeDueDesc] [varchar](50) NOT NULL,
	[AgeUnitCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_VaccinationAge_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
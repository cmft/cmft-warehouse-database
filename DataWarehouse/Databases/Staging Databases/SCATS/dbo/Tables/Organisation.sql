﻿CREATE TABLE [dbo].[Organisation](
	[OrganNo] [int] IDENTITY(1,1) NOT NULL,
	[OrganName] [varchar](50) NOT NULL,
	[OrganTypeCode] [smallint] NOT NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_AdmissionProvider_DateCreated]  DEFAULT (getdate()),
	[GeogAreaCode] [smallint] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AdmissionProvider_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
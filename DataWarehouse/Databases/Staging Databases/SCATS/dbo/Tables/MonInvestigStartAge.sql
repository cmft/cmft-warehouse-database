﻿CREATE TABLE [dbo].[MonInvestigStartAge](
	[VaccAgeDueCode] [smallint] NOT NULL,
	[MonInvestigTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_InvestMonitorinStartAge_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
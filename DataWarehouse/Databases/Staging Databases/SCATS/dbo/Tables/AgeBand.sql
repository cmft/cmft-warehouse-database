﻿CREATE TABLE [dbo].[AgeBand](
	[AgeBandCode] [smallint] IDENTITY(1,1) NOT NULL,
	[LowerAge] [smallint] NOT NULL,
	[UpperAge] [smallint] NOT NULL,
	[AgeBandRange] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AgeBand_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientPersonRelation](
	[MainPersonId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[RelationTypeCode] [smallint] NOT NULL,
	[FamilyNo] [int] NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[PregnancyYear] [smallint] NULL,
	[OrganNo] [int] NULL,
	[BornAtComments] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientPersonRelation_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
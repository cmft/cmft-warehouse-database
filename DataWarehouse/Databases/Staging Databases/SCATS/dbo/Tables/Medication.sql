﻿CREATE TABLE [dbo].[Medication](
	[MedicationCode] [smallint] IDENTITY(1,1) NOT NULL,
	[MedicationDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Medication_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
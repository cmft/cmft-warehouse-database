﻿CREATE TABLE [dbo].[DiagnosedPatientStatus](
	[PatientStatusCode] [smallint] NOT NULL,
	[PatientId] [int] NOT NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_DiseasedPatientStatus_DateEffective]  DEFAULT (getdate()),
	[DateInActive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_DiseasedPatientStatus_TstampLastUpdated_1]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
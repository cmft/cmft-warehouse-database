﻿CREATE TABLE [dbo].[SubApplicationTab](
	[AppTabCode] [smallint] NOT NULL,
	[SubAppTabCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SubApplicationTab_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
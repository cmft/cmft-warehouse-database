﻿CREATE TABLE [dbo].[AppTabAccessAuthority](
	[AccessAuthCode] [smallint] NOT NULL,
	[AppTabCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AppTabAccessAuthority_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
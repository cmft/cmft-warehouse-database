﻿CREATE TABLE [dbo].[LetterCategory](
	[CommCategoryCode] [smallint] NOT NULL,
	[LetterCategoryCode] [smallint] IDENTITY(1,1) NOT NULL,
	[LetterCategoryDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_LetterCategory_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
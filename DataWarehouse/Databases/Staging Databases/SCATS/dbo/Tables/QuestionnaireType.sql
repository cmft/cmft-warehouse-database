﻿CREATE TABLE [dbo].[QuestionnaireType](
	[QnaireTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[QnaireTypeDesc] [varchar](100) NOT NULL,
	[FrequencyCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_QuestionnaireType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[StaffWorkRegion](
	[CaseLoadPeriodNo] [int] NOT NULL,
	[CityNo] [int] NOT NULL,
	[PostCodeBaseNo] [int] NOT NULL,
	[PersonSizeGrpCode] [smallint] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PathwayActivity](
	[PathwayCode] [smallint] NOT NULL,
	[ActivityCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PathwayActivity_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
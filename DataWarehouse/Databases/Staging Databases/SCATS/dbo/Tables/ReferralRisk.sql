﻿CREATE TABLE [dbo].[ReferralRisk](
	[ReferralNo] [int] NOT NULL,
	[RiskSpecifiedCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_ReferralRisk_DateEffective]  DEFAULT (getdate()),
	[DateInActive] [datetime] NULL,
	[CurrentPartnerPersonId] [int] NULL,
	[PartnerRiskInd] [char](1) NOT NULL CONSTRAINT [DF_ReferralRisk_PartnerRiskInd]  DEFAULT ('N'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralRisk_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
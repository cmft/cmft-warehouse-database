﻿CREATE TABLE [dbo].[NeonateType](
	[NeonateTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[NeonateTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_NeonateType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
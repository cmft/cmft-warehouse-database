﻿CREATE TABLE [dbo].[WaitingList](
	[ReferralActionNo] [int] NOT NULL,
	[WLTypeCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_WaitingList_DateEffective]  DEFAULT (getdate()),
	[DateInActive] [datetime] NULL,
	[AppointmentNo] [int] NULL,
	[DeadlineDate] [datetime] NOT NULL,
	[DateSent] [datetime] NULL,
	[ResponseTypeCode] [smallint] NOT NULL,
	[InfoGiven] [varchar](500) NULL,
	[WLOutcome] [varchar](500) NULL,
	[MemoPad] [varchar](max) NULL,
	[ReasonSpecificCode] [smallint] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_WaitingList_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
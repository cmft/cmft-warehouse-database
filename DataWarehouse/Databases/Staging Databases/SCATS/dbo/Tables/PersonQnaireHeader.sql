﻿CREATE TABLE [dbo].[PersonQnaireHeader](
	[PersonId] [int] NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[QnaireHeaderNo] [int] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[RecordedDated] [datetime] NOT NULL,
	[OutcomeComents] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientInfoIdIssuer](
	[PatientId] [int] NOT NULL,
	[MiscInfoTypeCode] [smallint] NOT NULL,
	[IssuedInfoId] [varchar](20) NOT NULL,
	[DateIssued] [datetime] NULL,
	[OrganNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientHospIdentifier_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
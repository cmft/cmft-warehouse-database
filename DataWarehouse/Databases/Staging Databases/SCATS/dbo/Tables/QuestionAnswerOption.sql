﻿CREATE TABLE [dbo].[QuestionAnswerOption](
	[QnaireTypeCode] [smallint] NOT NULL,
	[QuestionGroupCode] [smallint] NOT NULL,
	[QuestionNo] [int] NOT NULL,
	[AnsOptionCode] [smallint] NOT NULL,
	[PositionalOrderCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
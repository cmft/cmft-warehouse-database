﻿CREATE TABLE [dbo].[Disability](
	[DisabilityCode] [smallint] IDENTITY(1,1) NOT NULL,
	[DisabilityDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Disability_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
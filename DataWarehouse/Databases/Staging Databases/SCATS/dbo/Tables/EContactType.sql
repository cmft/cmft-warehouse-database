﻿CREATE TABLE [dbo].[EContactType](
	[EContactTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[EContactTypeDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EContactType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
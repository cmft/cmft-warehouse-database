﻿CREATE TABLE [dbo].[Country](
	[CountryCode] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [varchar](50) NOT NULL,
	[TstamplastUpdated] [datetime] NOT NULL,
	[UseridLasUpdated] [smallint] NOT NULL
) ON [PRIMARY]
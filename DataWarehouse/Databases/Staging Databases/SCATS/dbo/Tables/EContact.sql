﻿CREATE TABLE [dbo].[EContact](
	[EContactNo] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NULL,
	[OrganDeptNo] [int] NULL,
	[EContactTypeCode] [smallint] NOT NULL,
	[TelContactNo] [varchar](255) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EContact_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
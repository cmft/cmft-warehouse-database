﻿CREATE TABLE [dbo].[RemoveFamilyRelation](
	[MainPersonId] [int] NOT NULL,
	[FamilyNo] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[OldParentId] [int] NULL,
	[OldMainPersonId] [int] NULL,
	[RelationTypeCode] [smallint] NOT NULL,
	[OldMainSurname] [varchar](30) NULL,
	[OldMainForename] [varchar](20) NULL,
	[OldMainMidInits] [varchar](20) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RemoveFamilyRelation_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
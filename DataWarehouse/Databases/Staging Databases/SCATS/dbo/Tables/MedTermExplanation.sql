﻿CREATE TABLE [dbo].[MedTermExplanation](
	[MedTermExplanCode] [smallint] IDENTITY(1,1) NOT NULL,
	[MedTermExplanDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_MedTermExplanation_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
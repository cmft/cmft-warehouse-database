﻿CREATE TABLE [dbo].[ReceipientType](
	[ReceipientTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReceipientTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_DestinationTypeCode_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
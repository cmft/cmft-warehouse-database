﻿CREATE TABLE [dbo].[QuestionStructure](
	[QnaireTypeCode] [smallint] NOT NULL,
	[QuestionGroupCode] [smallint] NOT NULL,
	[QuestionNo] [int] NOT NULL,
	[PositionalOrderCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
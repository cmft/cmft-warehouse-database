﻿CREATE TABLE [dbo].[FamilyEthnicityOriginType](
	[FamEthOrigTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[FamEthOrigTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_FamilyOriginType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[InvestigationItem](
	[InvestigItemCode] [smallint] IDENTITY(1,1) NOT NULL,
	[InvestigItemDesc] [varchar](500) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_InvestigationItem_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PersonLanguage](
	[PersonId] [int] NOT NULL,
	[LangCode] [smallint] NOT NULL,
	[FirstLangInd] [char](1) NOT NULL CONSTRAINT [DF_PersonLanguage_FirstLangInd]  DEFAULT ('N'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PersonLanguage_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[JobTitle](
	[JobTitleCode] [smallint] IDENTITY(1,1) NOT NULL,
	[JobTitleDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_JobTitle_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
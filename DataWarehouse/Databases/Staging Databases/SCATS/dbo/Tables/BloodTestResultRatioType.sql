﻿CREATE TABLE [dbo].[BloodTestResultRatioType](
	[BTResultRatioTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[BTResultRatioTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_BloodTestResultRatioType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
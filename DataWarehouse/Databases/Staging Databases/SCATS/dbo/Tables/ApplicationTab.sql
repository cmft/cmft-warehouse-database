﻿CREATE TABLE [dbo].[ApplicationTab](
	[AppTabCode] [smallint] IDENTITY(1,1) NOT NULL,
	[AppTabDesc] [varchar](100) NOT NULL,
	[ApptabSystemDesc] [varchar](100) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ApplicationTab_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
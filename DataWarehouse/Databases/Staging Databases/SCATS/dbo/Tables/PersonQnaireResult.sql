﻿CREATE TABLE [dbo].[PersonQnaireResult](
	[PersonId] [int] NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[QnaireHeaderNo] [int] NOT NULL,
	[QnaireTypeCode] [smallint] NOT NULL,
	[QuestionGroupCode] [smallint] NOT NULL,
	[QuestionNo] [int] NOT NULL,
	[AnsOptionCode] [smallint] NOT NULL,
	[AnswerInd] [char](1) NOT NULL,
	[ResultEntry] [varchar](100) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
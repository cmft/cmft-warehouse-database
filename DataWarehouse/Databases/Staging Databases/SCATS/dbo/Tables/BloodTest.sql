﻿CREATE TABLE [dbo].[BloodTest](
	[BloodTestCode] [smallint] NOT NULL,
	[BloodTestDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_BloodTestType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
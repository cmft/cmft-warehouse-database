﻿CREATE TABLE [dbo].[PatientDocumentSent](
	[DocNo] [int] NOT NULL,
	[SentDate] [datetime] NOT NULL,
	[StaffNo] [int] NOT NULL,
	[DocReSendInd] [char](1) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
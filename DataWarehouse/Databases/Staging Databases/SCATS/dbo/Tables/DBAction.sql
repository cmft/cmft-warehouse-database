﻿CREATE TABLE [dbo].[DBAction](
	[DBActionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[DBActionDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_DBAction_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
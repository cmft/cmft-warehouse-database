﻿CREATE TABLE [dbo].[EpisodicCategoryRequirement](
	[EpisodicCatReqdNo] [int] IDENTITY(1,1) NOT NULL,
	[EpisodicCatCode] [smallint] NOT NULL,
	[EpisodicReqdCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingEpisodicIssueReqment_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
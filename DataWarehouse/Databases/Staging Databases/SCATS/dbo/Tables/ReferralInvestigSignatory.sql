﻿CREATE TABLE [dbo].[ReferralInvestigSignatory](
	[ReferInvestigHDRNo] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[SignatoryTypeCode] [smallint] NOT NULL,
	[SignedDate] [datetime] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
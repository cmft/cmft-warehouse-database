﻿CREATE TABLE [dbo].[UserName](
	[UsernameId] [varchar](20) NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[UserId] [smallint] IDENTITY(1,1) NOT NULL,
	[GeogAreaCode] [smallint] NOT NULL,
	[UserPassword] [varchar](255) NULL,
	[InitialPassword] [varchar](255) NULL,
	[AuthenticationFailedInd] [char](1) NOT NULL CONSTRAINT [DF_UserName_AuthenticationFailedInd]  DEFAULT ('N'),
	[PasswordHint] [varchar](255) NULL,
	[PasswordHintAnswer] [varchar](255) NULL,
	[EULAAcceptInd] [char](1) NOT NULL CONSTRAINT [DF_UserName_EULAAcceptInd]  DEFAULT ('N'),
	[UserPasswordChange] [varchar](255) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_UserName_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
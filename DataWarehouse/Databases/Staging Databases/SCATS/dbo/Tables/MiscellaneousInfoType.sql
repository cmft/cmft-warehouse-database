﻿CREATE TABLE [dbo].[MiscellaneousInfoType](
	[MiscInfoTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[MiscInfoTypeDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_MiscellaneousInfoType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
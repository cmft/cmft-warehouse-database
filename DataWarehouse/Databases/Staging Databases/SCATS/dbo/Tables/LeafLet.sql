﻿CREATE TABLE [dbo].[LeafLet](
	[LeafletCode] [smallint] IDENTITY(1,1) NOT NULL,
	[LeafletDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [varchar](50) NOT NULL CONSTRAINT [DF_LeafLet_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
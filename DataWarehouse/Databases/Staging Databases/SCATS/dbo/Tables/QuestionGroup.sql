﻿CREATE TABLE [dbo].[QuestionGroup](
	[QuestionGroupCode] [smallint] IDENTITY(1,1) NOT NULL,
	[QuestionGroupDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_QuestionGroup_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[QnaireOutcomeItem](
	[QnaireOutItemCode] [smallint] IDENTITY(1,1) NOT NULL,
	[QnaireOutItemDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_QnaireOutcomeItem_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Calendar](
	[MonthNo] [smallint] NOT NULL,
	[MonthName] [varchar](20) NOT NULL,
	[MonthAbbrev] [char](3) NULL,
	[NoOfDays] [smallint] NOT NULL,
	[TstamplastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
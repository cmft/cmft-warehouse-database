﻿CREATE TABLE [dbo].[PersonReferRejectReason](
	[ReferralNo] [int] NOT NULL,
	[ReasonCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ExternalLetterReceipient](
	[ExternalLetReceipientNo] [int] IDENTITY(1,1) NOT NULL,
	[DocNo] [int] NOT NULL,
	[DocPageNo] [smallint] NOT NULL,
	[PersonId] [int] NULL,
	[AuthPersonNo] [int] NULL,
	[FHSGPId] [int] NULL,
	[FHSSPId] [money] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ExternalLetterReceipient_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
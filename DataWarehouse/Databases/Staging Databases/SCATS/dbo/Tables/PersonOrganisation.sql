﻿CREATE TABLE [dbo].[PersonOrganisation](
	[OrganDeptNo] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_PersonOrganisation_DateEffective]  DEFAULT (getdate()),
	[DateInActive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PersonOrganisation_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
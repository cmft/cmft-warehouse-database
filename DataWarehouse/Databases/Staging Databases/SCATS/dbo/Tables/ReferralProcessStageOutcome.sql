﻿CREATE TABLE [dbo].[ReferralProcessStageOutcome](
	[RPStageOutcomeNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[ProcessStageCode] [smallint] NOT NULL,
	[OutcomeCode] [smallint] NOT NULL,
	[OutcomeValue] [varbinary](500) NOT NULL,
	[OutcomeDate] [datetime] NOT NULL,
	[CounsellingNo] [int] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [datetime] NOT NULL
) ON [PRIMARY]
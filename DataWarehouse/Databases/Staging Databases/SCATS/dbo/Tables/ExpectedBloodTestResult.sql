﻿CREATE TABLE [dbo].[ExpectedBloodTestResult](
	[BTResultTypeOPCode] [smallint] NOT NULL,
	[BloodTestCode] [smallint] NOT NULL,
	[BloodTestSubCode] [smallint] NOT NULL,
	[BTResultRatioTypeCode] [smallint] NOT NULL,
	[ExpBTResOpRangeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ExpectedBloodTestResult_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientGP](
	[GPId] [int] NOT NULL,
	[SPId] [int] NULL,
	[PatientId] [int] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInActive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientGP_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
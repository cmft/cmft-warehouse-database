﻿CREATE TABLE [dbo].[Profession](
	[ProfessionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ProfessionDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Profession_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
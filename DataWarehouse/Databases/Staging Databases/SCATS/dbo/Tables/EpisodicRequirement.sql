﻿CREATE TABLE [dbo].[EpisodicRequirement](
	[EpisodicReqdCode] [smallint] IDENTITY(1,1) NOT NULL,
	[EpisodicReqdDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EpisodicRequirement_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
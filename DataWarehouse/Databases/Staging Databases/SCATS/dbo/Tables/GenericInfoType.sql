﻿CREATE TABLE [dbo].[GenericInfoType](
	[GenInfoTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[GenInfoTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_GenericInfoType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
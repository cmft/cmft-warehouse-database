﻿CREATE TABLE [dbo].[ReferralReason](
	[ReferralNo] [int] NOT NULL,
	[ReasonSpecificCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_ReferralReason_DateEffective]  DEFAULT (getdate()),
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastupdated] [smallint] NOT NULL
) ON [PRIMARY]
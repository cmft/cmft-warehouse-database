﻿CREATE TABLE [dbo].[ReferralBloodTestResult](
	[RefBloodTestNo] [int] NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[BTResultTypeCode] [smallint] NOT NULL,
	[BloodTestCode] [smallint] NOT NULL,
	[BloodTestSubCode] [smallint] NOT NULL,
	[BTResultRatioTypeCode] [smallint] NOT NULL,
	[ExpBTResOpRangeCode] [smallint] NOT NULL,
	[BloodTestResult] [varchar](20) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralBloodTestResult_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
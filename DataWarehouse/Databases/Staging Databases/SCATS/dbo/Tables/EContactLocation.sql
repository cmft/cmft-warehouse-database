﻿CREATE TABLE [dbo].[EContactLocation](
	[AddressUsageId] [int] NOT NULL,
	[EContactNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EContactLocation_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
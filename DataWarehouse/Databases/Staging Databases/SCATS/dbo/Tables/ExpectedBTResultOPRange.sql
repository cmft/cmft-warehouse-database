﻿CREATE TABLE [dbo].[ExpectedBTResultOPRange](
	[ExpBTResOpRangeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ExpBTResOpRangeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Table_1_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
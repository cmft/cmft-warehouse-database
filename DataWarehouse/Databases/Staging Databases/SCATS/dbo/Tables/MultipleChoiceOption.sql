﻿CREATE TABLE [dbo].[MultipleChoiceOption](
	[MultiChoiceOptionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[MultiChoiceOptionDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_MultipleChoiceOption_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NULL
) ON [PRIMARY]
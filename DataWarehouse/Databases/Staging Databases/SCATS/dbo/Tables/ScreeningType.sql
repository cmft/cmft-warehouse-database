﻿CREATE TABLE [dbo].[ScreeningType](
	[ScreeningTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ScreeningTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ScreeningType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
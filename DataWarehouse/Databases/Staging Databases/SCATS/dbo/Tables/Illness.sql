﻿CREATE TABLE [dbo].[Illness](
	[IllnessCode] [smallint] IDENTITY(1,1) NOT NULL,
	[IllnessDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Illness_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
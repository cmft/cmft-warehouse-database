﻿CREATE TABLE [dbo].[CounsellingType](
	[CounsellingTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[CounsellingTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ReferralSourceOrganOUT](
	[ReferSourceCode] [smallint] NOT NULL,
	[OrganNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralSourceOrgan_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
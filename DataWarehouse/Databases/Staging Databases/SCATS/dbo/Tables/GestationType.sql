﻿CREATE TABLE [dbo].[GestationType](
	[GestationTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[GestationTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_GestationType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
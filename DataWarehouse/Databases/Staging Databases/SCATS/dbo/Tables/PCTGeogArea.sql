﻿CREATE TABLE [dbo].[PCTGeogArea](
	[PCTGeogAreaNo] [int] IDENTITY(1,1) NOT NULL,
	[PCTCode] [char](3) NOT NULL,
	[GeogAreaCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PCTGeogArea_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientReferralSource](
	[PatReferSourceNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferalNo] [int] NOT NULL,
	[ReferSourceCode] [smallint] NULL,
	[OrganDeptNo] [int] NULL,
	[AddressUsageId] [int] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientReferralSource_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
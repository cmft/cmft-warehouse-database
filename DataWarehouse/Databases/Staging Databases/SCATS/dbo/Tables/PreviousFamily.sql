﻿CREATE TABLE [dbo].[PreviousFamily](
	[FamilyNo] [int] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInActive] [datetime] NULL,
	[FamilyName] [varchar](50) NOT NULL,
	[MemoPAd] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PreviousFamily_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
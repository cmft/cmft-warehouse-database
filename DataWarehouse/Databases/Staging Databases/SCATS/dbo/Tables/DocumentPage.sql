﻿CREATE TABLE [dbo].[DocumentPage](
	[DocNo] [int] NOT NULL,
	[DocPageNo] [smallint] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[ActualdocName] [varchar](100) NOT NULL,
	[ActualDocument] [image] NULL,
	[ActualDocExt] [varchar](5) NULL,
	[LetterSentDate] [datetime] NULL,
	[LetterResendInd] [char](1) NOT NULL CONSTRAINT [DF_DocumentPage_LetterResendInd]  DEFAULT ('N'),
	[ReceipientTypeCode] [smallint] NULL,
	[CopySentInd] [char](1) NOT NULL CONSTRAINT [DF_DocumentPage_CopySentind]  DEFAULT ('N'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_DocumentPage_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
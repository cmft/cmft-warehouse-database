﻿CREATE TABLE [dbo].[RequiredAction](
	[RequiredActionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[RequiredActionDesc] [varchar](200) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RequiredAction_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
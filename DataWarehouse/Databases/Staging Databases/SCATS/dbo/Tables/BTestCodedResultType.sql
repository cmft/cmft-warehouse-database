﻿CREATE TABLE [dbo].[BTestCodedResultType](
	[BTCodedResultTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[BTResultId] [varchar](10) NOT NULL,
	[BTCodedResultTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_BTestCodedResultType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
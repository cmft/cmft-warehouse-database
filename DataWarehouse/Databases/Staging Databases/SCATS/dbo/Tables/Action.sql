﻿CREATE TABLE [dbo].[Action](
	[ActionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ActionDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Action_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NULL
) ON [PRIMARY]
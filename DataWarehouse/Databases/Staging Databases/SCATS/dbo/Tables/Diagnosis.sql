﻿CREATE TABLE [dbo].[Diagnosis](
	[DiagnosisCode] [smallint] IDENTITY(1,1) NOT NULL,
	[DiagnosisDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Diagnosis_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
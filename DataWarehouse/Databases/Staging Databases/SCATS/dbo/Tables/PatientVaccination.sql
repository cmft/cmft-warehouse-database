﻿CREATE TABLE [dbo].[PatientVaccination](
	[PatientVaccNo] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[VaccAgeDueCode] [smallint] NOT NULL,
	[VaccineCode] [smallint] NOT NULL,
	[ChildInd] [char](1) NOT NULL CONSTRAINT [DF_PatientVaccination_ChildInd]  DEFAULT ('Y'),
	[DateReceived] [datetime] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
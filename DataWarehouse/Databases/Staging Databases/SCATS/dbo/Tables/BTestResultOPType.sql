﻿CREATE TABLE [dbo].[BTestResultOPType](
	[BTResultTypeOPCode] [smallint] IDENTITY(1,1) NOT NULL,
	[BTResultTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_BTestResultOPType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
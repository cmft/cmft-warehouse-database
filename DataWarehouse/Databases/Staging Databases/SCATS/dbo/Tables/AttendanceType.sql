﻿CREATE TABLE [dbo].[AttendanceType](
	[AttendanceTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[AttendanceTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AttendanceType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
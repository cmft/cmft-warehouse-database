﻿CREATE TABLE [dbo].[Screening](
	[ScreeningNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[ScreeingTypeCode] [smallint] NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[DateScreened] [datetime] NOT NULL CONSTRAINT [DF_Screening_DateCreated]  DEFAULT (getdate()),
	[DateRequested] [datetime] NOT NULL CONSTRAINT [DF_Screening_RiskTypeCode]  DEFAULT (getdate()),
	[ScreeningResult] [varchar](200) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Screening_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
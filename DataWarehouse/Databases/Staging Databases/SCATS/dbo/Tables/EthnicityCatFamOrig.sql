﻿CREATE TABLE [dbo].[EthnicityCatFamOrig](
	[EthnicityCatCode] [smallint] NOT NULL,
	[EthnicityCode] [smallint] NOT NULL,
	[FamEthOrigTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EthnicityCatFamOrigType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
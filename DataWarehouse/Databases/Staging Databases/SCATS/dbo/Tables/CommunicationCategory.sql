﻿CREATE TABLE [dbo].[CommunicationCategory](
	[CommCategoryCode] [smallint] IDENTITY(1,1) NOT NULL,
	[CommCategoryDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CommunicationCategory_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientSCDetectionAction](
	[PatientId] [int] NOT NULL,
	[SCActionCode] [smallint] NOT NULL,
	[ActionDate] [datetime] NOT NULL CONSTRAINT [DF_PatientSCDetectionAction_ActionDate]  DEFAULT (getdate()),
	[MemoPad] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientSCDetectionAction_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
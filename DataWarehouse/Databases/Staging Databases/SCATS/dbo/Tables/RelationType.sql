﻿CREATE TABLE [dbo].[RelationType](
	[RelationTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[RelationTypeDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RelationType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
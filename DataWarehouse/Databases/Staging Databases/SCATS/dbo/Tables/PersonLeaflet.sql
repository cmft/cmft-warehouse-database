﻿CREATE TABLE [dbo].[PersonLeaflet](
	[PersonId] [int] NOT NULL,
	[LeafletCode] [smallint] NOT NULL,
	[DateIssued] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
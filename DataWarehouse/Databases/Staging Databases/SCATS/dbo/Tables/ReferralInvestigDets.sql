﻿CREATE TABLE [dbo].[ReferralInvestigDets](
	[ReferInvestigHDRNo] [int] NOT NULL,
	[InvestigGroupCode] [smallint] NOT NULL,
	[InvestigItemCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
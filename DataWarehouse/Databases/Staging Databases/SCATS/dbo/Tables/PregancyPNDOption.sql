﻿CREATE TABLE [dbo].[PregancyPNDOption](
	[PregnancyNo] [int] NOT NULL,
	[PNDOptionCode] [smallint] NOT NULL,
	[AnswerInd] [char](1) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PregancyPNDOption_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
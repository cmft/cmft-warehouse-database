﻿CREATE TABLE [dbo].[AddressUsagePrev](
	[PersonId] [int] NULL,
	[OrganDeptNo] [int] NULL,
	[AddressId] [int] NOT NULL,
	[AddrUsageTypeCode] [smallint] NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInactive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
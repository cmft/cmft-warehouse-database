﻿CREATE TABLE [dbo].[VaccinationPeriodicReqd](
	[VaccAgeDueCode] [smallint] NOT NULL,
	[VaccineCode] [smallint] NOT NULL,
	[ChildInd] [char](1) NOT NULL CONSTRAINT [DF_VaccinationPeriodicReqd_ChildInd]  DEFAULT ('Y'),
	[FrequencyCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_VaccinationPeriodicReqd_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
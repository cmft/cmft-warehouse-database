﻿CREATE TABLE [dbo].[WLResponseWeek](
	[ReferralActionNo] [int] NOT NULL,
	[WLTypeCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_WLResponseWeek_DateEffective]  DEFAULT (getdate()),
	[ResponseWeekDate] [datetime] NOT NULL CONSTRAINT [DF_WLResponseWeek_ResponseWeekDate]  DEFAULT (getdate()),
	[Response] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_WLResponseWeek_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
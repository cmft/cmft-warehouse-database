﻿CREATE TABLE [dbo].[AddressUnit](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[HouseNo] [varchar](10) NULL,
	[HouseName] [varchar](30) NULL,
	[StreetName] [varchar](30) NULL,
	[AddrQualif] [varchar](30) NULL,
	[SuburbCode] [smallint] NULL,
	[Town] [varchar](30) NULL,
	[CityNo] [int] NULL,
	[PostCode] [varchar](8) NULL,
	[County] [varchar](30) NULL,
	[FlatNo] [varchar](20) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AddressUnit_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CityPostcodeBase](
	[CityNo] [int] NOT NULL,
	[PostCodeBaseNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CityPostcode_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
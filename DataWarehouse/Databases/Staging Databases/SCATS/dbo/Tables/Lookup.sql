﻿CREATE TABLE [dbo].[Lookup](
	[LookupId] [smallint] IDENTITY(1,1) NOT NULL,
	[LookupDesc] [varchar](50) NOT NULL,
	[LookupName] [varchar](50) NOT NULL,
	[ReadOnlyInd] [char](1) NULL,
	[AppNo] [smallint] NULL,
	[TableStructTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Lookup_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
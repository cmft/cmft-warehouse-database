﻿CREATE TABLE [dbo].[PatientParent](
	[MainPersonId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientParent_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
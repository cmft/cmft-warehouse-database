﻿CREATE TABLE [dbo].[ReferralNeonate](
	[ReferralNo] [int] NOT NULL,
	[NeonateType] [smallint] NOT NULL,
	[TransfusedDate] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralNeonate_TstampLastUpdated_1]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
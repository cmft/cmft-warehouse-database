﻿CREATE TABLE [dbo].[Referral](
	[ReferralNo] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[ReferTypeCode] [smallint] NOT NULL,
	[ReferredDate] [datetime] NOT NULL,
	[AuthPersonNo] [int] NULL,
	[DecisionDate] [datetime] NULL,
	[DischargedDate] [datetime] NULL,
	[AllowHomeVisitInd] [char](1) NOT NULL CONSTRAINT [DF_Referral_HomeVisitInd]  DEFAULT ('Y'),
	[RiskSpecifiedCode] [smallint] NULL,
	[MemoPad] [varchar](max) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Referral_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[CIssueNoTickReason](
	[CounsellingNo] [int] NOT NULL,
	[ReasonSpecificCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientMiscInfoDets](
	[PatMiscInfoDetsNo] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[MiscInfoTypeCode] [smallint] NOT NULL,
	[ReasonSpecificCode] [smallint] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralNeonate_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
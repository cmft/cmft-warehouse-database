﻿CREATE TABLE [dbo].[LetterType](
	[LetterTypeCode] [int] IDENTITY(1,1) NOT NULL,
	[ReceipientTypeCode] [smallint] NOT NULL,
	[CommCategoryCode] [smallint] NOT NULL,
	[LetterCategoryCode] [smallint] NOT NULL,
	[LetterTypeDesc] [varchar](100) NOT NULL,
	[LetterTypeContent] [image] NULL,
	[ActualDocExt] [varchar](5) NULL,
	[AttachedDocInd] [char](1) NOT NULL CONSTRAINT [DF_LetterType_AttachedDocInd]  DEFAULT ('N'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_LetterType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
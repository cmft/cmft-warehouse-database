﻿CREATE TABLE [dbo].[CounsellingReason](
	[CounsellingNo] [int] NOT NULL,
	[ReasonSpecificCode] [smallint] NOT NULL,
	[CIssueNoTickInd] [char](1) NULL CONSTRAINT [DF_CounsellingReason_CIssueNotickInd]  DEFAULT ('N'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingReason_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
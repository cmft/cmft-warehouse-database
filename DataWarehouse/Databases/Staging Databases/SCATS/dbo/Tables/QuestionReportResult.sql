﻿CREATE TABLE [dbo].[QuestionReportResult](
	[QnaireHeaderNo] [int] NOT NULL,
	[QuestionGroupCode] [smallint] NOT NULL,
	[QuestionNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
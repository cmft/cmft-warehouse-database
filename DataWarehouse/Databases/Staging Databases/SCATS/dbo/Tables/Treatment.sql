﻿CREATE TABLE [dbo].[Treatment](
	[TreatmentNo] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[DiagnosisCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInActive] [datetime] NULL,
	[MemoPad] [varchar](500) NULL,
	[AuthPersonNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
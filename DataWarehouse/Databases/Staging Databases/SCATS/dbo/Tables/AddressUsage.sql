﻿CREATE TABLE [dbo].[AddressUsage](
	[AddressUsageId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NULL,
	[OrganDeptNo] [int] NULL,
	[AddressId] [int] NOT NULL,
	[AddrUsageTypeCode] [smallint] NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_AddressUsage_DateEffective]  DEFAULT (getdate()),
	[DateInActive] [datetime] NULL,
	[ForwardingAddrGivenInd] [char](1) NOT NULL CONSTRAINT [DF_AddressUsage_FowardingAddressGivenInd]  DEFAULT ('Y'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AddressUsage_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
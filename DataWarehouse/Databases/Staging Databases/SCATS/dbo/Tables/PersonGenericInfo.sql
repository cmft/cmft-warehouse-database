﻿CREATE TABLE [dbo].[PersonGenericInfo](
	[PatientReviewNo] [int] NOT NULL,
	[GenInfoTypeCode] [smallint] NOT NULL,
	[MemoPad] [varchar](max) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PersonMiscInfo_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[ReviewType](
	[ReviewTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReviewTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReviewType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
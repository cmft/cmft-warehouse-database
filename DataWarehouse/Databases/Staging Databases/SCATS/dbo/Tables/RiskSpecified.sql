﻿CREATE TABLE [dbo].[RiskSpecified](
	[RiskSpecifiedCode] [smallint] NOT NULL,
	[RiskTypeCode] [smallint] NOT NULL,
	[RiskCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RiskSpecified_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
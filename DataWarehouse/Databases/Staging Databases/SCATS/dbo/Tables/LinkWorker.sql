﻿CREATE TABLE [dbo].[LinkWorker](
	[AuthPersonNo] [int] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInActive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
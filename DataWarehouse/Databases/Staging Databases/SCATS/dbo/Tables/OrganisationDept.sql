﻿CREATE TABLE [dbo].[OrganisationDept](
	[OrganDeptNo] [int] IDENTITY(1,1) NOT NULL,
	[OrganNo] [int] NOT NULL,
	[DeptCode] [smallint] NOT NULL,
	[GeogAreaCode] [smallint] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_OrganisationDept_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
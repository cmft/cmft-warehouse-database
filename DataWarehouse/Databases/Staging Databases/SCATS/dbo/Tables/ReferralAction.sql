﻿CREATE TABLE [dbo].[ReferralAction](
	[ReferralActionNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[ActionCode] [smallint] NOT NULL,
	[MemoPAD] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralAction_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
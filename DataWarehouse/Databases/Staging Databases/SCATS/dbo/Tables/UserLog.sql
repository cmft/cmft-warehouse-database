﻿CREATE TABLE [dbo].[UserLog](
	[UsernameId] [varchar](20) NOT NULL,
	[LogInDate] [datetime] NOT NULL,
	[DBActionCode] [smallint] NOT NULL,
	[AppNo] [smallint] NULL,
	[LogOutDate] [datetime] NULL,
	[PersonId] [int] NULL,
	[PersonTypeCode] [smallint] NULL,
	[DBActionActivity] [varchar](max) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_UserLog_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PNDOption](
	[PNDOptionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[PNDOptionDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PNDOption_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
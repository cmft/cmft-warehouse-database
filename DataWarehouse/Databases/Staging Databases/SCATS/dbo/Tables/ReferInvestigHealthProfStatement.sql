﻿CREATE TABLE [dbo].[ReferInvestigHealthProfStatement](
	[ReferInvestigHDRNo] [int] NOT NULL,
	[InvestigTestCode] [smallint] NOT NULL,
	[MedTermExplanCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
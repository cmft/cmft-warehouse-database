﻿CREATE TABLE [dbo].[Title](
	[TitleCode] [smallint] IDENTITY(1,1) NOT NULL,
	[TitleDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Title_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
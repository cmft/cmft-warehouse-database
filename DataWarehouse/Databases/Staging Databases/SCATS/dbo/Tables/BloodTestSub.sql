﻿CREATE TABLE [dbo].[BloodTestSub](
	[BloodTestSubCode] [smallint] IDENTITY(1,1) NOT NULL,
	[BloodTestSubDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_BloodTest_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[FamilyOriginQuestDets](
	[FamilyOrigQuestNo] [int] NOT NULL,
	[EthnicityCatCode] [smallint] NOT NULL,
	[FamEtnOrigTypeCode] [smallint] NOT NULL,
	[EthnicityRegionCode] [smallint] NOT NULL,
	[FamOrigRespTypeCode] [smallint] NOT NULL,
	[FamOrigResponseTick] [char](1) NOT NULL,
	[FamOrigResponseGen] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
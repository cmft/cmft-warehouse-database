﻿CREATE TABLE [dbo].[AppointmentProfessional](
	[AppointmentNo] [int] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MonExpectedInvestigResult](
	[VaccAgeDueCode] [smallint] NOT NULL,
	[MonInvestigTypeCode] [smallint] NOT NULL,
	[MonExpectedResultCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ExpectedInvestigationResult_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
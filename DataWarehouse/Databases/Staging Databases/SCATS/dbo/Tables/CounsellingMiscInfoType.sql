﻿CREATE TABLE [dbo].[CounsellingMiscInfoType](
	[CounsellingMiscInfoTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[CounsellingMiscInfoTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingMiscInfoType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
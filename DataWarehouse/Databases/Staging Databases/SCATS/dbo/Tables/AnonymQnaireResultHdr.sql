﻿CREATE TABLE [dbo].[AnonymQnaireResultHdr](
	[AnonymAnswerNo] [int] IDENTITY(1,1) NOT NULL,
	[DatefilledIn] [datetime] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[TstamplastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Document](
	[DocNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferralNo] [int] NULL,
	[PersonId] [int] NULL,
	[AuthPersonNo] [int] NOT NULL,
	[DocTypeCode] [smallint] NOT NULL,
	[GenerationDate] [datetime] NOT NULL CONSTRAINT [DF_Document_GenerationDate]  DEFAULT (getdate()),
	[LetterTypeCode] [int] NULL,
	[EventRefLocNo] [int] NULL,
	[MemoPAD] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Document_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserAccessAuthority](
	[UsernameId] [varchar](20) NOT NULL,
	[AccessAuthCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_UserAccessAuthority_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RequiredBloodTest](
	[BTResultTypeOPCode] [smallint] NOT NULL CONSTRAINT [DF_RequiredBloodTest_BTResultTypeOPCode]  DEFAULT ((1)),
	[BloodTestCode] [smallint] NOT NULL,
	[BloodTestSubCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RequiredBloodTest_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientStatus](
	[PatientStatusCode] [smallint] IDENTITY(1,1) NOT NULL,
	[PatientStatusDesc] [varchar](50) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientStatus_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
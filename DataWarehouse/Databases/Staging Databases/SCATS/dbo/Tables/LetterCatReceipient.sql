﻿CREATE TABLE [dbo].[LetterCatReceipient](
	[ReceipientTypeCode] [smallint] NOT NULL,
	[CommCategoryCode] [smallint] NOT NULL,
	[LetterCategoryCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_LetterCatDestination_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
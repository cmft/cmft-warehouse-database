﻿CREATE TABLE [dbo].[Pathway](
	[PathwayCode] [smallint] IDENTITY(1,1) NOT NULL,
	[PathwayDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Pathway_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CounsellingDets](
	[CounsellingNo] [int] NOT NULL,
	[CounsellingIssueCode] [smallint] NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[CounselledDate] [datetime] NOT NULL CONSTRAINT [DF_CounsellingDets_CounselledDate]  DEFAULT (getdate()),
	[CIssueTickInd] [char](1) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingDets_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
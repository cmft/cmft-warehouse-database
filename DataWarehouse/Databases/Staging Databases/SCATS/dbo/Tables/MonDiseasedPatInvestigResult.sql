﻿CREATE TABLE [dbo].[MonDiseasedPatInvestigResult](
	[MonDiseasedPatHdrNo] [int] NOT NULL,
	[VaccAgeDueCode] [smallint] NOT NULL,
	[MonInvestigTypeCode] [smallint] NOT NULL,
	[MonExpectedResultCode] [smallint] NOT NULL,
	[InvestigResultInd] [char](1) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ProcedureType](
	[ProcedureTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ProcedureTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ProcedureType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AnswerOption](
	[AnsOptionCode] [smallint] NOT NULL,
	[AnsOptionDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_QuestionType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
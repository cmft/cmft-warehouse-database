﻿CREATE TABLE [dbo].[ReferralSource](
	[ReferSourceCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReferSourceDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralSource_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
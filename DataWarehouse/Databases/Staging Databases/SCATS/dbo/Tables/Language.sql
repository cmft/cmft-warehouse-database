﻿CREATE TABLE [dbo].[Language](
	[LangCode] [smallint] IDENTITY(1,1) NOT NULL,
	[LangName] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Language_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EventType](
	[EventTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[EventTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferredToType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [nchar](10) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MonDiseasedPatHeader](
	[MonDiseasedPatHdrNo] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[VaccAgeDueCode] [smallint] NOT NULL,
	[MonInvestigTypeCode] [smallint] NOT NULL,
	[InvestigationDate] [datetime] NOT NULL,
	[OrganNo] [int] NULL,
	[MonClinic] [varchar](50) NULL,
	[OutcomeComments] [varchar](max) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
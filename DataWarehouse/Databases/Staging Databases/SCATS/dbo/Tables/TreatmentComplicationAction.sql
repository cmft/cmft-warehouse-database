﻿CREATE TABLE [dbo].[TreatmentComplicationAction](
	[TreatmentNo] [int] NOT NULL,
	[TreatmentOfferedCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[ComplicationTypeCode] [smallint] NOT NULL,
	[DetectionDate] [datetime] NOT NULL,
	[RequiredActionCode] [smallint] NOT NULL,
	[RequiredActionDate] [datetime] NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
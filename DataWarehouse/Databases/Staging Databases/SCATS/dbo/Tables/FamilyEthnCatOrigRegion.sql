﻿CREATE TABLE [dbo].[FamilyEthnCatOrigRegion](
	[FamilyOriginNo] [int] IDENTITY(1,1) NOT NULL,
	[EthnicityCatCode] [smallint] NOT NULL,
	[EthnicityCode] [smallint] NULL,
	[FamEthOrigTypeCode] [smallint] NOT NULL,
	[EthnicityRegionCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_FamilyEthnCatOrigRegion_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
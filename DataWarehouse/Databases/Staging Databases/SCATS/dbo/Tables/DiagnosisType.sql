﻿CREATE TABLE [dbo].[DiagnosisType](
	[DiagnosisTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[DiagnosisTypeDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_DiagnosisType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CounsellingEpisodicCatReq](
	[CounsellingNo] [int] NOT NULL,
	[EpisodicCatReqdNo] [int] NOT NULL,
	[EpisodicCatReqdInd] [char](1) NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingEpisodicIssueReq_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
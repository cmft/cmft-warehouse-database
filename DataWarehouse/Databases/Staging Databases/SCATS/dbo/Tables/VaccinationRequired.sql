﻿CREATE TABLE [dbo].[VaccinationRequired](
	[VaccAgeDueCode] [smallint] NOT NULL,
	[VaccineCode] [smallint] NOT NULL,
	[ChildInd] [char](1) NOT NULL CONSTRAINT [DF_VaccinationRequired_ChildInd]  DEFAULT ('Y'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_VaccinationRequired_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
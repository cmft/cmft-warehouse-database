﻿CREATE TABLE [dbo].[ReviewReason](
	[PatientReviewNo] [int] NOT NULL,
	[ReasonSpecificCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OutCome](
	[OutComeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[OutComeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_OutCome_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
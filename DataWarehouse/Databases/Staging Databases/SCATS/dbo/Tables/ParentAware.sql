﻿CREATE TABLE [dbo].[ParentAware](
	[ParentAwareCode] [smallint] IDENTITY(1,1) NOT NULL,
	[AwareDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ParentAware_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
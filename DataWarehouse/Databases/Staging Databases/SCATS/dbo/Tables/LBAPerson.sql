﻿CREATE TABLE [dbo].[LBAPerson](
	[PersonId] [int] NOT NULL,
	[Surname] [varchar](30) NOT NULL,
	[Forename] [varchar](20) NOT NULL,
	[MidInits] [varchar](20) NULL,
	[Gender] [char](1) NULL,
	[TitleCode] [smallint] NULL,
	[PersonTypeCode] [smallint] NOT NULL,
	[JobTitleCode] [smallint] NULL,
	[DateRegistered] [datetime] NOT NULL,
	[DateDecommissioned] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
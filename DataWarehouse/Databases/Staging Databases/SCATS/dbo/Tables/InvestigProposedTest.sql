﻿CREATE TABLE [dbo].[InvestigProposedTest](
	[InvestigTestCode] [smallint] NOT NULL,
	[MedTermExplanCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_InvestigProposedTest_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ReferralBloodTest](
	[RefBloodTestNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[BloodTestCode] [smallint] NOT NULL,
	[BloodTestDate] [datetime] NOT NULL,
	[BloodTestTime] [varchar](20) NULL,
	[LabReferenceId] [varchar](50) NULL,
	[ScreeningTypeCode] [smallint] NOT NULL,
	[AuthPersonNo] [int] NULL,
	[BloodTestResultDetail] [varchar](200) NOT NULL,
	[BloodTestReason] [varchar](200) NULL,
	[LabUseRefId] [varchar](10) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralBloodTest_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[QuestionnaireHeader](
	[QnaireHeaderNo] [int] IDENTITY(1,1) NOT NULL,
	[QnaireTypeCode] [smallint] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[MiscInstructionPad] [varchar](500) NULL,
	[ReleaseDate] [datetime] NULL,
	[AnonymousInd] [char](1) NULL,
	[NoOfQnairesSent] [smallint] NULL,
	[ResultsTalliedByDate] [datetime] NULL,
	[QnaireTitle] [varchar](100) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
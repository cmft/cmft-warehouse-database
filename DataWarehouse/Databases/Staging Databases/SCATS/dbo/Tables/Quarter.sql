﻿CREATE TABLE [dbo].[Quarter](
	[QuarterId] [char](2) NOT NULL,
	[QuarterNo] [smallint] NOT NULL,
	[QuarterDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Quarter_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PersonType](
	[PersonTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[PersonTypeDesc] [varchar](30) NOT NULL,
	[TstamPlastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PersonType_TstamPlastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EthnicityCategory](
	[EthnicityCatCode] [smallint] IDENTITY(1,1) NOT NULL,
	[EthnicityCatDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EthnicityCategory_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[BloodTestBaseline](
	[RefBloodTestNo] [int] NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[BTResultTypeCode] [smallint] NOT NULL,
	[BloodTestCode] [smallint] NOT NULL,
	[BloodTestSubCode] [smallint] NOT NULL,
	[BTResultRatioTypeCode] [smallint] NOT NULL,
	[ExpBTResOpRangeCode] [smallint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[BloodTestResult] [varchar](20) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
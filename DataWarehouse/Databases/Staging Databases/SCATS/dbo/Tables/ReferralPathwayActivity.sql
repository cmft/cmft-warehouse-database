﻿CREATE TABLE [dbo].[ReferralPathwayActivity](
	[RefPathActivityNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[PathwayCode] [smallint] NOT NULL,
	[ActivityCode] [smallint] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[MemoPad] [varchar](50) NOT NULL,
	[ActivityDate] [datetime] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[County](
	[CountyNo] [int] IDENTITY(1,1) NOT NULL,
	[CountyName] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_County_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
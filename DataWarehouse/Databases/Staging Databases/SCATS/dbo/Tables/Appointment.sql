﻿CREATE TABLE [dbo].[Appointment](
	[AppointmentNo] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[AppointTypeCode] [smallint] NOT NULL,
	[ReasonSpecificCode] [smallint] NOT NULL,
	[AppointmentDate] [datetime] NOT NULL,
	[AppointmentTime] [varchar](10) NULL,
	[AttendanceTypeCode] [smallint] NOT NULL,
	[AppointmentOutcome] [varchar](500) NULL,
	[MemoPad] [varchar](500) NULL,
	[AuthPersonNo] [int] NOT NULL,
	[CancelledDate] [datetime] NULL,
	[AddressUsageId] [int] NULL,
	[ReferralNo] [int] NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Appointment_CreatedDate]  DEFAULT (getdate()),
	[ANCPartnerPersonId] [int] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Appointment_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
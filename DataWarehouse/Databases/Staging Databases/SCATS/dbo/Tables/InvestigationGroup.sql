﻿CREATE TABLE [dbo].[InvestigationGroup](
	[InvestigGroupCode] [smallint] IDENTITY(1,1) NOT NULL,
	[InvestigGroupDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_InvestigationGroup_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
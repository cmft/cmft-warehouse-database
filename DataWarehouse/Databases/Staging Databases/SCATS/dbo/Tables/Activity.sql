﻿CREATE TABLE [dbo].[Activity](
	[ActivityCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ActivityDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Activity_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
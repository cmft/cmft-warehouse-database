﻿CREATE TABLE [dbo].[PostCounsellingType](
	[PostCounselTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[PostCounselTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PostCounsellingType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
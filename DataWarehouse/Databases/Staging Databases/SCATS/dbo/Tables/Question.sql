﻿CREATE TABLE [dbo].[Question](
	[QuestionNo] [int] IDENTITY(1,1) NOT NULL,
	[QuestionDesc] [varchar](200) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Question_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
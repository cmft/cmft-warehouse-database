﻿CREATE TABLE [dbo].[Family](
	[FamilyNo] [int] IDENTITY(1,1) NOT NULL,
	[FamilyName] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Family_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
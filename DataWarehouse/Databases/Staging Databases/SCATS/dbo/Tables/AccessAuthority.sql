﻿CREATE TABLE [dbo].[AccessAuthority](
	[AccessAuthCode] [smallint] IDENTITY(1,1) NOT NULL,
	[AccessAuthDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AccessAuthority_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
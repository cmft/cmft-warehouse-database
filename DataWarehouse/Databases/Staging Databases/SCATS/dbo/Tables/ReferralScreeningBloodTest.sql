﻿CREATE TABLE [dbo].[ReferralScreeningBloodTest](
	[ScreeningNo] [int] NOT NULL,
	[RefBloodTestNo] [int] NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReferralScreeningBloodTest_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
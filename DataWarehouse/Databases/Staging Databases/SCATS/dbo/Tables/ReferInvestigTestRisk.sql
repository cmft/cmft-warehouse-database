﻿CREATE TABLE [dbo].[ReferInvestigTestRisk](
	[ReferInvestigHDRNo] [int] NOT NULL,
	[RiskSpecifiedCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
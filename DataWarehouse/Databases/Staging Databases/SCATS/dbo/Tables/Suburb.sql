﻿CREATE TABLE [dbo].[Suburb](
	[SuburbCode] [smallint] IDENTITY(1,1) NOT NULL,
	[SuburbName] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Suburb_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
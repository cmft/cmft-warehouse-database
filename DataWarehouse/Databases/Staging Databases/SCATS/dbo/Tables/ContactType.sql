﻿CREATE TABLE [dbo].[ContactType](
	[ContactTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ContactTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ContactType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
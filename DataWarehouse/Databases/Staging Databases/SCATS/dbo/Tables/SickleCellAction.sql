﻿CREATE TABLE [dbo].[SickleCellAction](
	[SCActionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[SCActionDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SickleCellAction_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
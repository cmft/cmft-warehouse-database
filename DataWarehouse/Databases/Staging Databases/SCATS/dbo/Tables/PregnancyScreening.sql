﻿CREATE TABLE [dbo].[PregnancyScreening](
	[ScreeningNo] [int] NOT NULL,
	[PregnancyNo] [int] NOT NULL,
	[ProcedureTypeCode] [smallint] NOT NULL,
	[ProcedureDate] [datetime] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[MemoPad] [nvarchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
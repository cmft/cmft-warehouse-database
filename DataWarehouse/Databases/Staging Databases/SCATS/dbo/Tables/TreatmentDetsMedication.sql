﻿CREATE TABLE [dbo].[TreatmentDetsMedication](
	[TreatmentNo] [int] NOT NULL,
	[TreatmentOfferedCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[MedicationCode] [smallint] NOT NULL,
	[DateMedicationStart] [datetime] NOT NULL,
	[DateMedicationEnd] [datetime] NULL,
	[Dosage] [varchar](100) NULL,
	[Formulation] [varchar](100) NULL,
	[MemoPad] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
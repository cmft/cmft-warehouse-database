﻿CREATE TABLE [dbo].[FamOrigQuestResponseType](
	[FamOrigRespTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[FamOrigRespTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_FamOrigQuestResponseType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
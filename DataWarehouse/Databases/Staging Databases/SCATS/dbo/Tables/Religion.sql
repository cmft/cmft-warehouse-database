﻿CREATE TABLE [dbo].[Religion](
	[ReligionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReligionDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Religion_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NULL,
	[PASCode] [varchar](10) NULL
) ON [PRIMARY]
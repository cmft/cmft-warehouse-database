﻿CREATE TABLE [dbo].[City](
	[CityNo] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_City_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
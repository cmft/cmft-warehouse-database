﻿CREATE TABLE [dbo].[PersonReferralStatus](
	[ReferralNo] [int] NOT NULL,
	[ReferralStatusCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInActive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PersonReferralStatus_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
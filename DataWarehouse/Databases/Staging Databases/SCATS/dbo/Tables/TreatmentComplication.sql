﻿CREATE TABLE [dbo].[TreatmentComplication](
	[TreatmentNo] [int] NOT NULL,
	[TreatmentOfferedCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[ComplicationTypeCode] [smallint] NOT NULL,
	[DetectionDate] [datetime] NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
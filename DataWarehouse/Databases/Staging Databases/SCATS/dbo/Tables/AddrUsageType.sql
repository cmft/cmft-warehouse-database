﻿CREATE TABLE [dbo].[AddrUsageType](
	[AddrUsageTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[AddrUsageTypeDesc] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AddrUsageType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
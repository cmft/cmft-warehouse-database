﻿CREATE TABLE [dbo].[EthnicityRegion](
	[EthnicityRegionCode] [smallint] IDENTITY(1,1) NOT NULL,
	[EthnicityRegionDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EthnicityRegion_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DocumentType](
	[DocTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[DocTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_DocumentType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
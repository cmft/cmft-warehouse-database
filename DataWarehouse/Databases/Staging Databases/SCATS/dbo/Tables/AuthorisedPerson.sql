﻿CREATE TABLE [dbo].[AuthorisedPerson](
	[AuthPersonNo] [int] NOT NULL,
	[OrganId] [int] NULL,
	[PersonId] [int] NOT NULL,
	[PersonTypeCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL CONSTRAINT [DF_AuthorisedPerson_DateEffective]  DEFAULT (getdate()),
	[DateInActive] [datetime] NULL,
	[InternalStaffInd] [char](1) NOT NULL CONSTRAINT [DF_AuthorisedPerson_InternalStaffInd]  DEFAULT ('N'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AuthorisedPerson_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
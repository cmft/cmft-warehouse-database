﻿CREATE TABLE [dbo].[ReferralInvestigHdr](
	[ReferInvestigHDRNo] [int] IDENTITY(1,1) NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[InvestigStartDate] [datetime] NOT NULL,
	[MemoPad] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
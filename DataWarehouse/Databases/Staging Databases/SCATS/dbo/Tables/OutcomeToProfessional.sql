﻿CREATE TABLE [dbo].[OutcomeToProfessional](
	[PersonId] [int] NOT NULL,
	[ReferralNo] [int] NOT NULL,
	[QnaireHeaderNo] [int] NOT NULL,
	[QnaireOutItemCode] [smallint] NOT NULL,
	[AnsOptionCode] [smallint] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[NotifiedDate] [datetime] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
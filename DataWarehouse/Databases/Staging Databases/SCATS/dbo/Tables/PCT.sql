﻿CREATE TABLE [dbo].[PCT](
	[PctCode] [char](3) NOT NULL,
	[PctName] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PCT_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
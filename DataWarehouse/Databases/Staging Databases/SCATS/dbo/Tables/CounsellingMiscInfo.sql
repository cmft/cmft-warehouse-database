﻿CREATE TABLE [dbo].[CounsellingMiscInfo](
	[CounsellingNo] [int] NOT NULL,
	[CounsellingMiscInfoTypeCode] [smallint] NOT NULL,
	[MemoPad] [varchar](max) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingMiscInfo_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
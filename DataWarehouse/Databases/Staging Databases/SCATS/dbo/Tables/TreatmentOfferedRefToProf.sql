﻿CREATE TABLE [dbo].[TreatmentOfferedRefToProf](
	[TreatmentNo] [int] NOT NULL,
	[TreatmentOfferedCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[RefToPathActivityProfNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
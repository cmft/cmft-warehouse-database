﻿CREATE TABLE [dbo].[ReferralStatus](
	[ReferralStatusCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReferralStatusDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NULL CONSTRAINT [DF_ReferralStatus_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ResponseType](
	[ResponseTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ResponseTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ResponseType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
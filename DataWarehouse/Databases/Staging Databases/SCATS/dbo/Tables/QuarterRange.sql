﻿CREATE TABLE [dbo].[QuarterRange](
	[QuarterId] [char](2) NOT NULL,
	[MonthstartNo] [smallint] NOT NULL,
	[DayStartNo] [smallint] NOT NULL,
	[MonthEndNo] [smallint] NOT NULL,
	[DayEndNo] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_QuarterRange_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ComplicationRequiredAction](
	[ComplicationTypeCode] [smallint] NOT NULL,
	[RequiredActionCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ComplicationRequiredAction_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
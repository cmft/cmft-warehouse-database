﻿CREATE TABLE [dbo].[PatientMiscInfo](
	[PatientId] [int] NOT NULL,
	[MiscInfoTypeCode] [smallint] NOT NULL,
	[MemoPad] [varchar](max) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PatientMiscInfo_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
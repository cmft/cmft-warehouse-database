﻿CREATE TABLE [dbo].[EpisodicCategory](
	[EpisodicCatCode] [smallint] IDENTITY(1,1) NOT NULL,
	[EpisodicIssueDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingEpisodicIssue_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PostCodeBase](
	[PostCodeBaseNo] [int] IDENTITY(1,1) NOT NULL,
	[PostcodeBase] [varchar](4) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PostCode_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ProfCareProvision](
	[PatientId] [int] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UserIdLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
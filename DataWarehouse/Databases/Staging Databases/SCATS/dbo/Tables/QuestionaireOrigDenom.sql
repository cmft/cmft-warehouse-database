﻿CREATE TABLE [dbo].[QuestionaireOrigDenom](
	[QnaireHeaderNo] [int] NOT NULL,
	[QnaireTypeCode] [smallint] NOT NULL,
	[QuestionGroupCode] [smallint] NOT NULL,
	[QuestionNo] [int] NOT NULL,
	[AnsOptionCode] [smallint] NOT NULL,
	[PositionalOrderCode] [smallint] NULL,
	[MultiChoiceOptionCode] [smallint] NULL,
	[MultiChoiceHdrOrdCode] [smallint] NULL,
	[QnaireTypeDesc] [varchar](100) NULL,
	[QuestionGroupDesc] [varchar](100) NULL,
	[QuestionDesc] [varchar](100) NULL,
	[AnsOptionDesc] [varchar](100) NULL,
	[MultiChoiceOptionDesc] [varchar](100) NULL,
	[MultiChoiceHdrOrdering] [smallint] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OrganisationType](
	[OrganTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[OrganTypeDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ProviderType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
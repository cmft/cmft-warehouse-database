﻿CREATE TABLE [dbo].[PatientLinkWorker](
	[PatientId] [int] NOT NULL,
	[AuthPersonNo] [int] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInActive] [datetime] NULL,
	[TstamplastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ReasonSpecific](
	[ReasonSpecificCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReasonTypeCode] [smallint] NOT NULL,
	[ReasonCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ReasonSpecificCode_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
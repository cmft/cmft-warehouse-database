﻿CREATE TABLE [dbo].[Reason](
	[ReasonCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ReasonDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Reason_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientPeriodicVaccination](
	[PatientVaccNo] [int] NOT NULL,
	[VaccAgeDueCode] [smallint] NOT NULL,
	[VaccineCode] [smallint] NOT NULL,
	[ChildInd] [char](1) NOT NULL,
	[FrequencyCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
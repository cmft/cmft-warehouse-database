﻿CREATE TABLE [dbo].[RiskType](
	[RiskTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[RiskTypeDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_RiskType_TstampLastUpdated_1]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
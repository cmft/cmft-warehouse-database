﻿CREATE TABLE [dbo].[ReferralPathProfDevNeed](
	[RefPathActivityNo] [int] NOT NULL,
	[ProfDevNeedCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
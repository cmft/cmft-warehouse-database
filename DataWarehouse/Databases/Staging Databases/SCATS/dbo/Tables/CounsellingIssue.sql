﻿CREATE TABLE [dbo].[CounsellingIssue](
	[CounsellingIssueCode] [smallint] IDENTITY(1,1) NOT NULL,
	[CounsellingIssueDesc] [varchar](100) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_CounsellingIssue_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[InvestigationGroupItem](
	[InvestigGroupCode] [smallint] NOT NULL,
	[InvestigItemCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_InvestigationGroupItem_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
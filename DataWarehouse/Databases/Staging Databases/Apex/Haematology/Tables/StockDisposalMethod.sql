﻿CREATE TABLE [Haematology].[StockDisposalMethod](
	[StockDisposalMethodCode] [varchar](10) NOT NULL,
	[StockDisposalMethod] [varchar](100) NOT NULL
) ON [PRIMARY]
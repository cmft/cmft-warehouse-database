﻿CREATE TABLE [Renal].[Results](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RequestDate] [date] NOT NULL,
	[TimeOfOrder] [time](7) NOT NULL,
	[DateTimeOfOrder] [datetime] NOT NULL,
	[NhsNumber] [varchar](12) NULL,
	[HospitalNumber] [varchar](20) NOT NULL,
	[PatientSurname] [varchar](50) NOT NULL,
	[PatientForename] [varchar](50) NOT NULL,
	[TestCodeDesc] [varchar](50) NOT NULL,
	[TestCode] [varchar](6) NOT NULL,
	[Result] [varchar](20) NULL,
	[OrderCode] [char](2) NOT NULL,
	[SpecimenNo] [char](9) NOT NULL,
	[HospitalCode] [char](3) NOT NULL
) ON [PRIMARY]
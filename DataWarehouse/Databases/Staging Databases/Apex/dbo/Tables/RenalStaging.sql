﻿CREATE TABLE [dbo].[RenalStaging](
	["Request Date"] [varchar](50) NULL,
	["Time Of Order"] [varchar](50) NULL,
	["Nhs Number"] [varchar](50) NULL,
	["Hospital Number"] [varchar](50) NULL,
	["Patient Surname"] [varchar](50) NULL,
	["Patient Forename"] [varchar](50) NULL,
	["Test Code Desc"] [varchar](50) NULL,
	["Test Code"] [varchar](50) NULL,
	["Result"] [varchar](50) NULL,
	["Order Code"] [varchar](50) NULL,
	["Specimen No"] [varchar](50) NULL,
	["Hospital Code"] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Microsoft.ApplicationServer.DurableInstancing].[AbandonedInstanceControlCommandsCleanupTable](
	[LastCleanupTime] [datetime] NOT NULL,
	[LastCleanupCount] [bigint] NOT NULL,
	[TotalCleanupCount] [bigint] NOT NULL
) ON [PRIMARY]
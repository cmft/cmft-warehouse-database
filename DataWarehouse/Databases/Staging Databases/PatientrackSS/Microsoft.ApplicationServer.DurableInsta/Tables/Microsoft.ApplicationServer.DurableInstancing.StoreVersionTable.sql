﻿CREATE TABLE [Microsoft.ApplicationServer.DurableInstancing].[StoreVersionTable](
	[Major] [bigint] NULL,
	[Minor] [bigint] NULL,
	[Build] [bigint] NULL,
	[Revision] [bigint] NULL,
	[LastUpdated] [datetime] NULL,
	[StoreIdentifier] [uniqueidentifier] NULL
) ON [PRIMARY]
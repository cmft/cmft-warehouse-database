﻿CREATE TABLE [dbo].[MEASURE_SET_ITEM](
	[MSITM_PK] [int] IDENTITY(1,1) NOT NULL,
	[MSSET_PK] [int] NOT NULL,
	[OBMSR_PK] [int] NOT NULL,
	[DISPLAY_ORDER] [int] NOT NULL,
	[MANDATORY_FLAG] [bit] NOT NULL,
	[EWS_CONTRIBUTOR_FLAG] [bit] NOT NULL,
	[DELETED_FLAG] [bit] NOT NULL,
	[CREATED_BY_USERR_PK] [int] NOT NULL,
	[CREATED_DTTM] [datetime] NOT NULL,
	[LAST_MODIFIED_BY_USERR_PK] [int] NOT NULL,
	[LAST_MODIFIED_DTTM] [datetime] NOT NULL
) ON [PRIMARY]
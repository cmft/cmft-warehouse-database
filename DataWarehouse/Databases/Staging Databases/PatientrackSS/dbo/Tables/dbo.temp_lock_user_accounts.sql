﻿CREATE TABLE [dbo].[temp_lock_user_accounts](
	[USERR_PK] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [varchar](20) NOT NULL,
	[LOCKED_FLAG] [bit] NOT NULL
) ON [PRIMARY]
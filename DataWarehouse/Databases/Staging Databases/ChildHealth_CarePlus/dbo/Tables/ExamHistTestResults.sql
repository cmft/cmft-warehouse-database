﻿CREATE TABLE [dbo].[ExamHistTestResults](
	[EntSys] [int] NOT NULL,
	[EXAM_DATE] [date] NOT NULL,
	[EXAM_CODE] [varchar](6) NOT NULL,
	[EXAM_DESC] [varchar](30) NULL,
	[TEST_CODE] [varchar](6) NOT NULL,
	[TEST_DESC] [varchar](255) NULL,
	[RESULT] [varchar](50) NULL,
	[RESULT_DESC] [varchar](50) NULL,
	[REFERRED_TO_CODE] [varchar](6) NULL,
	[REFERRED_TO_DESC] [varchar](30) NULL,
	[CLINIC_CODE] [varchar](50) NULL,
	[CLINIC_DESC] [varchar](50) NULL
) ON [PRIMARY]
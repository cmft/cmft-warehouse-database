﻿CREATE TABLE [SQLUser].[PRAC_HISTORY](
	[EntSys] [int] NOT NULL,
	[HDATE] [varchar](50) NOT NULL,
	[PRAC_CODE] [varchar](50) NULL,
	[PRAC_NAME] [varchar](50) NULL,
	[START_DATE] [date] NULL,
	[END_DATE] [date] NULL
) ON [PRIMARY]
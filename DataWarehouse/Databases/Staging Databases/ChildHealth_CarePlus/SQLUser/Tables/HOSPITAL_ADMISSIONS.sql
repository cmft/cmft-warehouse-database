﻿CREATE TABLE [SQLUser].[HOSPITAL_ADMISSIONS](
	[SYSTEM_NUMBER] [int] NOT NULL,
	[ADMISSION_DATE] [date] NOT NULL,
	[VENUE_TYPE] [varchar](50) NULL,
	[VENUE_TYPE_DESCRIPTION] [varchar](250) NULL,
	[VENUE_CODE] [varchar](50) NULL,
	[VENUE_DESCRIPTION] [varchar](250) NULL,
	[DIAGNOSIS_CODE_1] [varchar](50) NULL,
	[DIAGNOSIS_CODE_1_DESCRIPTION] [varchar](250) NULL,
	[DIAGNOSIS_CODE_2] [varchar](50) NULL,
	[DIAGNOSIS_CODE_2_DESCRIPTION] [varchar](250) NULL,
	[WHERE_DISCHARGED] [varchar](50) NULL,
	[REASON_FOR_ADMISSION] [varchar](250) NULL,
	[ADMISSION_TIME] [time](7) NULL,
	[DISCHARGE_DATE] [date] NULL,
	[FOLLOW_UP] [varchar](50) NULL,
	[FOLLOW_UP_BY_WHOM_CODE] [varchar](50) NULL,
	[FOLLOW_UP_BY_WHOM_DESCRIPTION] [varchar](250) NULL,
	[FOLLOW_UP_BY_WHOM_TYPE] [varchar](50) NULL,
	[FOLLOW_UP_BY_WHOM_TYPE_DESCRIPTION] [varchar](250) NULL
) ON [PRIMARY]
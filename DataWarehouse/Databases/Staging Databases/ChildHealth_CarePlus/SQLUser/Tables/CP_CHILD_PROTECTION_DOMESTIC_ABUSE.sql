﻿CREATE TABLE [SQLUser].[CP_CHILD_PROTECTION_DOMESTIC_ABUSE](
	[CAF_DECLINED] [varchar](2) NULL,
	[CAF_RECOMMENDED] [varchar](2) NULL,
	[CRIME_NUMBER] [varchar](50) NULL,
	[LEVEL_CODE] [varchar](50) NULL,
	[LEVEL_DESCRIPTION] [varchar](250) NULL,
	[OUTCOME_CODE] [varchar](10) NULL,
	[OUTCOME_DESCRIPTION] [varchar](250) NULL,
	[DATE_OF_INCIDENT] [date] NOT NULL,
	[SYSTEM_NUMBER] [varchar](50) NOT NULL,
	[REASON_DECLINED] [varchar](250) NULL,
	[SEQUENCE_NUMBER] [varchar](50) NOT NULL,
	[HEALTH_PROFESSIONAL_CODE] [varchar](50) NULL,
	[HEALTH_PROFESSIONAL_DESCRIPTION] [varchar](250) NULL,
	[HEALTH_PROFESSIONAL_TYPE] [varchar](50) NULL,
	[HEALTH_PROFESSIONAL_TYPE_DESCRIPTION] [varchar](250) NULL
) ON [PRIMARY]
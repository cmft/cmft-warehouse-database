﻿CREATE TABLE [SQLUser].[chsREG_CMF_ROUTINE_TESTS](
	[EntSys] [int] NOT NULL,
	[SPECIMEN_DATE] [date] NOT NULL,
	[SEQ_NO] [int] NOT NULL,
	[TEST_CODE] [varchar](6) NOT NULL,
	[TEST_DESC] [varchar](30) NULL,
	[TEST_RES_CODE] [varchar](14) NULL,
	[TEST_RESULT_DESC] [varchar](36) NULL,
	[ON_PCMMTP_PRINT] [date] NULL,
	[SPEC_RESULT_DATE] [date] NULL,
	[RESULT_REC_DATE] [date] NULL,
	[DATE_RESULT_ENTERED] [date] NULL,
	[NK_DATE_FLAG] [varchar](1) NULL,
	[DATE_ACKNOWLEDGED] [date] NULL,
	[DATE_SENT] [date] NULL,
	[TEST_GIVEN] [varchar](50) NULL,
	[BAD_SPOT_IND] [varchar](50) NULL,
	[REASON_CODE] [varchar](60) NULL,
	[REASON_CODE_DESCRIPTION] [varchar](61) NULL,
	[LAB_NUMBER] [varchar](50) NULL,
	[SPECIMEN_COMMENT] [varchar](116) NULL,
	[TEST_FREE_TEXT_COMMENT] [varchar](120) NULL
) ON [PRIMARY]
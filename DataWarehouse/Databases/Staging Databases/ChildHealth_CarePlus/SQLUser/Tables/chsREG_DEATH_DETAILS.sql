﻿CREATE TABLE [SQLUser].[chsREG_DEATH_DETAILS](
	[EntSys] [int] NOT NULL,
	[DATE_OF_DEATH] [varchar](50) NULL,
	[TIME_OF_DEATH] [time](7) NULL,
	[PLACE_OF_DEATH] [varchar](50) NULL,
	[CAUSE_OF_DEATH_TEXT] [varchar](60) NULL,
	[CAUSE_OF_DEATH_CODE1] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC1] [varchar](80) NULL,
	[CAUSE_OF_DEATH_CODE2] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC2] [varchar](50) NULL,
	[CAUSE_OF_DEATH_CODE3] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC3] [varchar](50) NULL,
	[CAUSE_OF_DEATH_CODE4] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC4] [varchar](50) NULL,
	[CAUSE_OF_DEATH_CODE5] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC5] [varchar](50) NULL,
	[CAUSE_OF_DEATH_CODE6] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC6] [varchar](50) NULL,
	[CAUSE_OF_DEATH_CODE7] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC7] [varchar](50) NULL,
	[CAUSE_OF_DEATH_CODE8] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC8] [varchar](50) NULL,
	[NOTIFIED_BY_CODE] [varchar](50) NULL,
	[NOTIFIED_BY_DESC] [varchar](50) NULL,
	[NOTIFIED_DATE] [date] NULL,
	[RAPID_RESPONSE_CLINICIAN] [varchar](250) NULL,
	[RAPID_RESPONSE_TEAM_INVOLVED] [varchar](5) NULL,
	[TYPD] [varchar](50) NULL,
	[TYPE_OF_DEATH] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SQLUser].[chsSH_SCHOOL_HEALTH_PRO](
	[SCHOOL_CODE] [varchar](10) NOT NULL,
	[SCHOOL_NAME] [varchar](60) NULL,
	[HP_TYPE] [varchar](6) NOT NULL,
	[HP_DESC] [varchar](30) NULL,
	[HP_CODE] [varchar](20) NOT NULL,
	[HP_NAME] [varchar](50) NULL
) ON [PRIMARY]
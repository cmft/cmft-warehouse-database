﻿CREATE TABLE [SQLUser].[EntDem](
	[EntSys] [int] NOT NULL,
	[SecLng] [varchar](12) NULL,
	[PrmLng] [varchar](12) NULL,
	[Ssn] [varchar](17) NULL,
	[DoB] [datetime] NULL,
	[Sex] [varchar](1) NULL,
	[Eth] [varchar](4) NULL,
	[MarSts] [varchar](4) NULL,
	[Rlg] [varchar](12) NULL,
	[Edu] [varchar](12) NULL,
	[Inc] [varchar](12) NULL,
	[MomMdnNam] [varchar](4) NULL,
	[DobEst] [varchar](4) NULL
) ON [PRIMARY]
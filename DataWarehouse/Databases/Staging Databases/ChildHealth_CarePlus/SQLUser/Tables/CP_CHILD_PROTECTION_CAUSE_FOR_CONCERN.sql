﻿CREATE TABLE [SQLUser].[CP_CHILD_PROTECTION_CAUSE_FOR_CONCERN](
	[CAUSE_FOR_CONCERN_CODE] [varchar](50) NULL,
	[CAUSE_FOR_CONCERN_DESCRIPTION] [varchar](250) NULL,
	[DATE_IDENTIFIED] [date] NOT NULL,
	[END_DATE] [date] NULL,
	[SYSTEM_NUMBER] [varchar](50) NOT NULL,
	[SEQUENCE_NUMBER] [varchar](50) NOT NULL
) ON [PRIMARY]
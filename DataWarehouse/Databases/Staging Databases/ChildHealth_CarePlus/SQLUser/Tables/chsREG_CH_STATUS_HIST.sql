﻿CREATE TABLE [SQLUser].[chsREG_CH_STATUS_HIST](
	[EntSys] [int] NOT NULL,
	[DATE_CHANGED] [date] NOT NULL,
	[REG_STATUS_CODE] [varchar](1) NULL,
	[VI_STATUS_CODE] [varchar](1) NULL,
	[PRE_SCHOOL_STATUS] [varchar](1) NULL,
	[SCHOOL_STATUS] [varchar](1) NULL,
	[LOST_CONTACT_REASON] [varchar](6) NULL,
	[LOST_CONTACT_COMMENTS] [varchar](30) NULL,
	[LOST_CONTACT_DATE] [date] NULL,
	[USER_ID] [varchar](30) NULL,
	[STATUS_START_DATE] [date] NULL,
	[REG_STATUS_DESC] [varchar](30) NULL,
	[VI_STATUS_DESC] [varchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SQLUser].[chsPS_EXAM_HIST_TEST_RESULTS_VISION](
	[EntSys] [int] NOT NULL,
	[EXAM_DATE] [date] NOT NULL,
	[EXAM_CODE] [varchar](6) NOT NULL,
	[EXAM_DESC] [varchar](30) NULL,
	[TEST_CODE] [varchar](6) NOT NULL,
	[TEST_DESC] [varchar](30) NULL,
	[RIGHT_6_WITHOUT_GLASSES] [varchar](50) NULL,
	[LEFT_6_WITHOUT_GLASSES] [varchar](50) NULL,
	[RIGHT_NEAR_WITHOUT_GLASSES] [varchar](50) NULL,
	[LEFT_NEAR_WITHOUT_GLASSES] [varchar](50) NULL,
	[RIGHT_FAR_WITHOUT_GLASSES] [varchar](50) NULL,
	[LEFT_FAR_WITHOUT_GLASSES] [varchar](50) NULL,
	[RIGHT_6_WITH_GLASSES] [varchar](50) NULL,
	[LEFT_6_WITH_GLASSES] [varchar](50) NULL,
	[RIGHT_NEAR_WITH_GLASSES] [varchar](50) NULL,
	[LEFT_NEAR_WITH_GLASSES] [varchar](50) NULL,
	[RIGHT_FAR_WITH_GLASSES] [varchar](50) NULL,
	[LEFT_FAR_WITH_GLASSES] [varchar](50) NULL,
	[RIGHT_NEAR] [varchar](50) NULL,
	[LEFT_NEAR] [varchar](50) NULL,
	[RIGHT_FAR] [varchar](50) NULL,
	[LEFT_FAR] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SQLUser].[CP_CHILD_PROTECTION_MAPPA](
	[DATE_REFERRED] [date] NOT NULL,
	[END_DATE] [date] NULL,
	[SYSTEM_NUMBER] [varchar](50) NOT NULL,
	[MAPPA_CATEGORY_CODE] [varchar](50) NULL,
	[MAPPA_CATEGORY_DESCRIPTION] [varchar](250) NULL,
	[SEQUENCE_NUMBER] [varchar](50) NOT NULL
) ON [PRIMARY]
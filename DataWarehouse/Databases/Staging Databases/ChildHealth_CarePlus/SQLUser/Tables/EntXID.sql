﻿CREATE TABLE [SQLUser].[EntXID](
	[EntSys] [int] NOT NULL,
	[IdSeq] [int] NOT NULL,
	[IDTyp] [varchar](12) NULL,
	[IdVal] [varchar](20) NOT NULL,
	[EffBegDat] [datetime] NULL,
	[EffEndDat] [datetime] NULL
) ON [PRIMARY]
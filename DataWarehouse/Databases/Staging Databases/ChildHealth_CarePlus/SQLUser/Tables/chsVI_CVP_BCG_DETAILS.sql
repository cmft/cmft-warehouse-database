﻿CREATE TABLE [SQLUser].[chsVI_CVP_BCG_DETAILS](
	[EntSys] [int] NOT NULL,
	[EVENT_DATE] [date] NOT NULL,
	[CONSENT_DATE] [date] NULL,
	[REFUSAL] [varchar](1) NULL,
	[BCG_COURSE] [varchar](1) NULL,
	[HEAF_SESSION_DATE] [date] NULL,
	[ABSENT_FOR_HEAF] [varchar](1) NULL,
	[DEFER_HEAF_CODE] [varchar](1) NULL,
	[PERM_CONTRA_IND_CODE] [varchar](1) NULL,
	[HEAF_ADMINISTERED_BY_CODE] [varchar](22) NULL,
	[HEAF_TEST_LOCATION] [varchar](6) NULL,
	[HEAF_READING_DATE] [date] NULL,
	[ABSENT_FOR_READING] [varchar](1) NULL,
	[HEAF_RESULT] [varchar](5) NULL,
	[CONTRA_IND_TO_BCG_CODE] [varchar](1) NULL,
	[APPT_NEXT_TIME] [varchar](1) NULL,
	[HEAF_READ_BY_TYPE] [varchar](10) NULL,
	[HEAF_READ_BY_CODE] [varchar](22) NULL,
	[READING_LOCATION] [varchar](6) NULL,
	[BCG_SESSION_DATE] [date] NULL,
	[PREVIOUS_SCAR] [varchar](1) NULL,
	[REFUSED_VACCINATION] [varchar](1) NULL,
	[BCG_SITE] [varchar](1) NULL,
	[BCG_BATCH_NUMBER] [varchar](25) NULL,
	[BCG_GIVEN_BY_CODE] [varchar](22) NULL,
	[BCG_LOCATION] [varchar](20) NULL,
	[X_RAY_REFERRAL_DATE] [date] NULL,
	[HEAF_ADMINISTERED_BY_TYPE] [varchar](6) NULL,
	[PLANNED_SESSION] [varchar](1) NULL,
	[BCG_GIVEN_BY_TYPE] [varchar](10) NULL,
	[BCG_COMMENT] [varchar](128) NULL,
	[HEAF_SITE] [varchar](50) NULL,
	[HEAF_SITE_DESCRIPTION] [varchar](50) NULL,
	[HEAF_BATCH_NUMBER] [varchar](16) NULL,
	[SKIN_TEST_ADMIN_VENUE_TYPE] [varchar](2) NULL,
	[SKIN_TEST_ADMIN_VENUE_TYPE_DESCRIPTION] [varchar](50) NULL,
	[SKIN_TEST_READ_VENUE_TYPE] [varchar](2) NULL,
	[SKIN_TEST_VENUE_TYPE_DESCRIPTION] [varchar](50) NULL,
	[BCG_GIVEN_VENUE_TYPE] [varchar](2) NULL,
	[BCG_GIVEN_VENUE_TYPE_DESCRIPTION] [varchar](50) NULL,
	[SKIN_TEST_TYPE] [varchar](1) NULL,
	[SKIN_TEST_TYPE_DESCRIPTION] [varchar](50) NULL,
	[ABSENT_FOR_HEAF_DESC] [varchar](1) NULL,
	[ABSENT_FOR_READING_YN] [varchar](1) NULL,
	[APPT_NEXT_TIME_YN] [varchar](1) NULL,
	[BCG_GIVEN_BY_NAME] [varchar](50) NULL,
	[CONTRA_IND_TO_BCG_DESC] [varchar](30) NULL,
	[DEFER_HEAF_DESC] [varchar](30) NULL,
	[HEAF_ADMINISTERED_BY_NAME] [varchar](50) NULL,
	[HEAF_READ_BY_NAME] [varchar](50) NULL,
	[HEAF_TEST_LOCATION_DESC] [varchar](100) NULL,
	[PERM_CONTRA_IND_DESC] [varchar](30) NULL,
	[PLANNED_SESSION_YN] [varchar](1) NULL,
	[PREVIOUS_SCAR_YN] [varchar](1) NULL,
	[READING_LOCATION_DESC] [varchar](100) NULL,
	[REFUSAL_DESC] [varchar](25) NULL,
	[REFUSED_VACCINATION_YN] [varchar](1) NULL
) ON [PRIMARY]
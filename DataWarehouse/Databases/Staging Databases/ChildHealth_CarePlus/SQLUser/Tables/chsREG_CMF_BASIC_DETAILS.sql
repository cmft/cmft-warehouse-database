﻿CREATE TABLE [SQLUser].[chsREG_CMF_BASIC_DETAILS](
	[EntSys] [int] NOT NULL,
	[DATE_OF_BIRTH] [date] NULL,
	[BIRTH_STATE] [varchar](1) NULL,
	[PLACE_OF_BIRTH_CODE] [varchar](6) NULL,
	[TIME_OF_BIRTH] [time](7) NULL,
	[BIRTH_WEIGHT] [varchar](5) NULL,
	[BIRTH_ORDER] [varchar](1) NULL,
	[ONSET_OF_RESP] [varchar](4) NULL,
	[APGAR_SCORES] [varchar](20) NULL,
	[TRANSFERRED_TO_HOSPITAL] [varchar](1) NULL,
	[DATE_ON_REGISTRARS_LIST] [date] NULL,
	[FATHERS_OCC_CODE] [varchar](40) NULL,
	[MOTHERS_OCC_CODE] [varchar](40) NULL,
	[LEGITIMACY] [varchar](1) NULL,
	[REGISTRATION_STATUS_CODE] [varchar](1) NULL,
	[VI_STATUS_CODE] [varchar](1) NULL,
	[PRE_SCHOOL_STATUS] [varchar](1) NULL,
	[SCHOOL_STATUS] [varchar](1) NULL,
	[LOST_CONTACT_REASON] [varchar](6) NULL,
	[LOST_CONTACT_DATE] [date] NULL,
	[LOST_CONTACT_COMMENT] [varchar](30) NULL,
	[FIP_REG_STATUS] [varchar](1) NULL,
	[SPECIAL_REG_STATUS] [varchar](1) NULL,
	[HEALTH_VISITOR_CODE] [varchar](30) NULL,
	[SCHOOL_CODE] [varchar](6) NULL,
	[CARE_NOTES] [varchar](30) NULL,
	[SCHOOL_NURSE_CODE] [varchar](6) NULL,
	[DISCHARGE_ADDRESS] [varchar](105) NULL,
	[CONG_MALFORMATIONS] [varchar](1) NULL,
	[ALIVE_AT_28_DAYS_CODE] [varchar](1) NULL,
	[REGISTRAR_CODE] [varchar](6) NULL,
	[AGE_AT_DEATH_CODE] [varchar](1) NULL,
	[SCBU_HOSPITAL_CODE] [varchar](6) NULL,
	[STILL_BIRTH_STATUS] [varchar](1) NULL,
	[CONG_MALS_FREE_TEXT] [varchar](40) NULL,
	[DELIVERY_COMPLICATIONS] [varchar](40) NULL,
	[INTENDED_BIRTH_PLACE] [varchar](6) NULL,
	[REASON_FOR_CHANGE_CODE] [varchar](6) NULL,
	[LENGTH_NK] [float] NULL,
	[HEAD_CIRCUM] [float] NULL,
	[HIPS_CODE] [varchar](6) NULL,
	[SCBU_DAYS] [float] NULL,
	[VENTILATED_DAYS] [float] NULL,
	[FEEDING_INTENTION_CODE] [varchar](6) NULL,
	[JAUNDICE_CODE] [varchar](6) NULL,
	[BIRTH_COMMENT] [varchar](20) NULL,
	[SPECIAL_REFERRAL_CODE] [varchar](6) NULL,
	[SEEN_BY_PAED] [varchar](1) NULL,
	[PHYSICAL_PROBS] [varchar](1) NULL,
	[OTHER_DIAGNOSIS] [varchar](1) NULL,
	[SP_NEEDS_REGISTER] [varchar](1) NULL,
	[GESTATIONAL_AGE] [float] NULL,
	[FEEDING_INTENTION_TEXT] [varchar](20) NULL,
	[ONSET_OF_LABOUR_CODE] [varchar](6) NULL,
	[MODE_OF_DELIVERY_CODE] [varchar](6) NULL,
	[ANAESTHETIC_DURING_LABOUR] [varchar](1) NULL,
	[ANAESTHETIC_DURING_DELIVERY] [varchar](1) NULL,
	[DELIVERED_BY] [varchar](20) NULL,
	[DELIVERER_STATUS_CODE] [varchar](6) NULL,
	[INTENDED_PLACE_OF_BIRTH_CODE] [varchar](6) NULL,
	[PRESENT_OF_FOETUS_CODE] [varchar](6) NULL,
	[EDUCATION_DISTRICT_CODE] [varchar](6) NULL,
	[ULTRA_SOUND] [varchar](1) NULL,
	[DATE_OF_DEATH] [date] NULL,
	[PLACE_OF_DEATH] [varchar](40) NULL,
	[CAUSE_OF_DEATH_TEXT] [varchar](58) NULL,
	[TIME_OF_DEATH] [time](7) NULL,
	[CAUSE_OF_DEATH_CODE1] [varchar](10) NULL,
	[CAUSE_OF_DEATH_CODE2] [varchar](10) NULL,
	[CAUSE_OF_DEATH_CODE3] [varchar](10) NULL,
	[CAUSE_OF_DEATH_CODE4] [varchar](10) NULL,
	[CAUSE_OF_DEATH_CODE5] [varchar](10) NULL,
	[CAUSE_OF_DEATH_CODE6] [varchar](10) NULL,
	[REG_DISABLED] [varchar](1) NULL,
	[PLACE_OF_BIRTH_TYPE_CODE] [varchar](6) NULL,
	[BASE_CODE] [varchar](20) NULL,
	[ANAESTHETIC_POST_DELIVERY] [varchar](1) NULL,
	[MOVEMENT_IN_DATE] [date] NULL,
	[SCHOOL_HEALTH_REG] [varchar](1) NULL,
	[AGE_AT_DEATH_DESC] [varchar](20) NULL,
	[AGE_AT_MOVEMENT_IN] [float] NULL,
	[AGE_IN_WHOLE_MONTHS] [int] NULL,
	[AGE_IN_YEARS] [int] NULL,
	[AGE_IN_YEARS_AND_MONTHS] [varchar](20) NULL,
	[ALIVE_AT_28_DAYS_DESC] [varchar](10) NULL,
	[ANAESTHETIC_DURING_DELIVERY_YN] [varchar](1) NULL,
	[ANAESTHETIC_DURING_LABOUR_YN] [varchar](1) NULL,
	[ANAESTHETIC_POST_DELIVERY_YN] [varchar](1) NULL,
	[APGAR1] [varchar](7) NULL,
	[APGAR2] [varchar](7) NULL,
	[APGAR3] [varchar](7) NULL,
	[APGARSCORE1] [varchar](2) NULL,
	[APGARSCORE2] [varchar](2) NULL,
	[APGARSCORE3] [varchar](2) NULL,
	[APGARSCORETIME1] [varchar](5) NULL,
	[APGARSCORETIME2] [varchar](5) NULL,
	[APGARSCORETIME3] [varchar](5) NULL,
	[BASE_DESC] [varchar](50) NULL,
	[CAUSE_OF_DEATH_DESC1] [varchar](max) NULL,
	[CAUSE_OF_DEATH_DESC2] [varchar](max) NULL,
	[CAUSE_OF_DEATH_DESC3] [varchar](max) NULL,
	[CAUSE_OF_DEATH_DESC4] [varchar](max) NULL,
	[CAUSE_OF_DEATH_DESC5] [varchar](max) NULL,
	[CAUSE_OF_DEATH_DESC6] [varchar](max) NULL,
	[CONG_MALFORMATIONS_YN] [varchar](1) NULL,
	[DELIVERER_STATUS_DESC] [varchar](30) NULL,
	[EDUCATION_DISTRICT_DESC] [varchar](30) NULL,
	[FATHERS_OCC_DESC] [varchar](30) NULL,
	[FEEDING_INTENTION_DESC] [varchar](30) NULL,
	[GP_START_DATE] [date] NULL,
	[HEALTH_VISITOR_NAME] [varchar](64) NULL,
	[HIPS_DESCRIPTION] [varchar](30) NULL,
	[INTENDED_PLACE_OF_BIRTH_DESC] [varchar](30) NULL,
	[JAUNDICE_DESC] [varchar](30) NULL,
	[LEGITIMACY_YN] [varchar](1) NULL,
	[MODE_OF_DELIVERY_DESC] [varchar](30) NULL,
	[MOTHERS_OCC_DESC] [varchar](50) NULL,
	[ONSET_OF_LABOUR_DESC] [varchar](30) NULL,
	[OTHER_DIAGNOSIS_YN] [varchar](1) NULL,
	[PHYSICAL_PROBS_YN] [varchar](1) NULL,
	[PLACE_OF_BIRTH_DESC] [varchar](40) NULL,
	[PLACE_OF_BIRTH_TYPE_DESC] [varchar](30) NULL,
	[PRESENT_OF_FOETUS_DESC] [varchar](30) NULL,
	[REASON_FOR_CHANGE_DESC] [varchar](30) NULL,
	[REGISTRAR_NAME] [varchar](40) NULL,
	[REGISTRATION_STATUS_DESC] [varchar](50) NULL,
	[SCHOOL_NAME] [varchar](60) NULL,
	[SCHOOL_NURSE_NAME] [varchar](30) NULL,
	[SCHOOL_YEAR] [int] NULL,
	[SEEN_BY_PAED_YN] [varchar](1) NULL,
	[SP_NEEDS_REGISTER_YN] [varchar](1) NULL,
	[TRANSFERRED_TO_HOSPITAL_YN] [varchar](1) NULL,
	[VI_STATUS_DESC] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
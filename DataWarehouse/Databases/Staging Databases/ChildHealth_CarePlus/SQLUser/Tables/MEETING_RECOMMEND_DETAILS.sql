﻿CREATE TABLE [SQLUser].[MEETING_RECOMMEND_DETAILS](
	[EntSys] [int] NOT NULL,
	[MEETING_TYPE_CODE] [varchar](50) NOT NULL,
	[MEETING_TYPE_DESC] [varchar](50) NULL,
	[MEETING_DATE] [date] NOT NULL,
	[RECOMMEND_DETAILS] [varchar](1250) NULL
) ON [PRIMARY]
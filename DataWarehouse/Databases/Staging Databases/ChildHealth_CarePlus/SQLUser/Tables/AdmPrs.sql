﻿CREATE TABLE [SQLUser].[AdmPrs](
	[AdmSys] [int] NOT NULL,
	[PrsSeq] [int] NOT NULL,
	[TEMSYS] [int] NULL,
	[PrsTyp] [varchar](12) NULL,
	[PrsSys] [int] NULL,
	[Dsc] [varchar](12) NULL,
	[BegDat] [datetime] NULL,
	[EndDat] [datetime] NULL,
	[PrsOrd] [int] NULL,
	[StaffTyp] [varchar](12) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SQLUser].[GPPrac](
	[EntSys] [int] NOT NULL,
	[GPCode] [varchar](50) NULL,
	[LocalID] [varchar](50) NULL,
	[PCTBegDat] [datetime] NULL,
	[PCTCode] [varchar](50) NULL,
	[PCTName] [varchar](50) NULL,
	[PracCode] [varchar](50) NULL,
	[PracName] [varchar](50) NULL,
	[SurgCode] [varchar](50) NULL,
	[SurgName] [varchar](50) NULL
) ON [PRIMARY]
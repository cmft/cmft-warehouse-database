﻿CREATE TABLE [SQLUser].[chsVI_VACCINE_MASTER](
	[VACCINE_CODE] [varchar](6) NOT NULL,
	[VACCINE_NAME] [varchar](30) NULL,
	[VACCINE_VALUE] [varchar](50) NULL,
	[IN_USE] [varchar](1) NULL,
	[IN_USE_YN] [varchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SQLUser].[RelEnt](
	[EntSys] [int] NOT NULL,
	[RelEntSys] [int] NOT NULL,
	[Rel] [varchar](12) NOT NULL,
	[Typ] [varchar](12) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL
) ON [PRIMARY]
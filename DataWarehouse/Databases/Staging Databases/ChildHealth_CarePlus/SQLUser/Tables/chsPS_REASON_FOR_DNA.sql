﻿CREATE TABLE [SQLUser].[chsPS_REASON_FOR_DNA](
	[DNA_CANCEL_REASON_CODE] [varchar](6) NOT NULL,
	[DESCRIPTION] [varchar](64) NULL,
	[REFUSAL_CODE] [varchar](6) NULL,
	[IN_USE] [varchar](1) NULL,
	[IN_USE_YN] [varchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [SQLUser].[GENERAL_ALERTS](
	[EntSys] [int] NOT NULL,
	[ACTION] [varchar](50) NOT NULL,
	[ACTION_DESCRIPTION] [varchar](50) NULL,
	[CODE] [varchar](50) NOT NULL,
	[CODE_DESCRIPTION] [varchar](50) NULL,
	[LAC] [varchar](50) NULL,
	[MESSAGE] [varchar](1250) NULL,
	[OPERATION] [varchar](50) NOT NULL,
	[OPERATION_DESCRIPTION] [varchar](50) NULL,
	[SERVICE] [varchar](50) NULL,
	[SERVICE_DESCRIPTION] [varchar](50) NULL,
	[SAFEGUARDING] [varchar](50) NULL,
	[TYPE] [varchar](50) NOT NULL,
	[TYPE_DESCRIPTION] [varchar](50) NULL,
	[VALUE] [varchar](50) NULL,
	[VALUE_TYPE] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [SQLUser].[MCR](
	[McrTyp] [varchar](3) NOT NULL,
	[Mcr] [varchar](12) NOT NULL,
	[Des] [varchar](40) NULL,
	[RsvCod] [varchar](1) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Switch] [varchar](1) NULL,
	[Alert] [varchar](1) NULL,
	[EpisodeGrp] [varchar](50) NULL
) ON [PRIMARY]
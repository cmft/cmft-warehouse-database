﻿CREATE TABLE [SQLUser].[chsSH_EXAM_CONSENTS](
	[EntSys] [varchar](50) NOT NULL,
	[EXAM_CODE] [varchar](50) NOT NULL,
	[EXAM_DESC] [varchar](50) NULL,
	[TYPE] [varchar](50) NULL,
	[CONSENT] [varchar](50) NULL,
	[DATE_OF_CONSENT] [date] NULL,
	[RESFUSAL_CODE] [varchar](50) NULL,
	[RESFUSAL_DESC] [varchar](50) NULL
) ON [PRIMARY]
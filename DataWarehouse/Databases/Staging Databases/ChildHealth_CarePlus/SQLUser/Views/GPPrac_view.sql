﻿
create view [SQLUser].[GPPrac_view](EntSys,GPCode,LocalID,PCTBegDat,PCTCode,PCTName,PracCode,PracName,SurgCode,SurgName )
with schemabinding
as
with s as(
select ROW_NUMBER() over (partition by entsys order by pctbegdat desc) rn,EntSys,GPCode,LocalID,PCTBegDat,PCTCode,PCTName,PracCode,PracName,SurgCode,SurgName
from sqluser.GPPrac)

select EntSys,GPCode,LocalID,PCTBegDat,PCTCode,PCTName,PracCode,PracName,SurgCode,SurgName 
from s where rn = 1

GO


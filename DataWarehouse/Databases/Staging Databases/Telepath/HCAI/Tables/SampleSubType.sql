﻿CREATE TABLE [HCAI].[SampleSubType](
	[Code] [varchar](15) NULL,
	[Description] [varchar](200) NULL,
	[created] [datetime2](7) NULL,
	[updated] [datetime2](7) NULL,
	[byWhom] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [HCAI].[referenceData](
	[Code] [varchar](10) NULL,
	[Description] [varchar](300) NULL,
	[subject] [varchar](20) NULL,
	[created] [datetime2](7) NULL
) ON [PRIMARY]
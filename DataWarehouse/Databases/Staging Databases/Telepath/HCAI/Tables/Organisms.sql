﻿CREATE TABLE [HCAI].[Organisms](
	[Code] [varchar](10) NULL,
	[Description] [varchar](200) NULL,
	[created] [datetime2](7) NULL,
	[updated] [datetime2](7) NULL,
	[byWhom] [varchar](50) NULL
) ON [PRIMARY]
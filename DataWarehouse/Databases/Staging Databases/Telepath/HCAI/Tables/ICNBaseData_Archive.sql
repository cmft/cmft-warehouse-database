﻿CREATE TABLE [HCAI].[ICNBaseData_Archive](
	[AlertType] [varchar](20) NULL,
	[SpecimenNumber] [varchar](30) NULL,
	[PatientName] [varchar](200) NULL,
	[HospitalNumber] [varchar](16) NULL,
	[NHSNumber] [varchar](20) NULL,
	[LDN] [varchar](15) NULL,
	[Sex] [varchar](1) NULL,
	[DateOfBirth] [date] NULL,
	[CurrentLocation] [varchar](200) NULL,
	[DateSpecimenCollected] [varchar](20) NULL,
	[SpecimenCollectedLocation] [varchar](100) NULL,
	[SpecimenSite] [varchar](200) NULL,
	[DateOfPreviousPositive] [date] NULL,
	[Organism] [varchar](20) NULL,
	[ExtractDate] [varchar](30) NULL,
	[dateImported] [datetime2](7) NULL DEFAULT (getdate())
) ON [PRIMARY]
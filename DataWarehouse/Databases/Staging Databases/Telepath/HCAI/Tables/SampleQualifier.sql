﻿CREATE TABLE [HCAI].[SampleQualifier](
	[Code] [varchar](15) NULL,
	[Description] [varchar](50) NULL,
	[created] [datetime2](7) NULL,
	[updated] [datetime2](7) NULL,
	[byWhom] [varchar](50) NULL
) ON [PRIMARY]
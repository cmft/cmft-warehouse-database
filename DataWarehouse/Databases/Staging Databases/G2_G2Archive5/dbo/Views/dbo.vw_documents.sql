﻿CREATE VIEW [dbo].[vw_documents]
/****** Object:  View dbo.vw_documents *********************************************
@Description: 
	Return archived documents.

@Author: 	
	rdo

@History:	
	14-12-2009  ewe SCNL1007402 Addition of datefields from new production table VisitInfo
	19-12-2006	rdo	Support CorrectionRate.
	15-05-2006	rdo	Syntax.
	17-01-2005	rdo	Adjustment RecognitionRate.	
	09-01-2004	rdo	Support dictation time. 
	2002		rdo	Created first version.
************************************************************************************/
AS
SELECT	[document].Title, 
		[document].Length, 
		documenttype.[name] AS [Type], 
		[document].Priority, 
        [context].[Name] AS [Context], 
		worktype.[Name] AS Worktype, 
		[document].BatchNr AS Batchnummer, 
        workstation.[Name] AS Workplace, 
		interface.[Name] AS Interface, 
		[group].[Name] AS [Group], 
		author.[Name] AS AuthorName, 
        transcriptionist.[Name] AS TranscriptionistName, 
		authorizer.[Name] AS AuthorizerName, 
		[document].CreationDate, 
        [document].DictationTime, 
		CAST((80+(ROUND(((CAST([document].RecognitionRate AS float)/100)*20),0))) AS int) AS RecognitionRate, 
		[document].WaitingTimeCorrection, 
		[document].CorrectionDate, 
		[document].CorrectionTime, 
		[document].WaitingTimeAuthorization, 
		[document].AuthorizationDate, 
		[document].PassThroughTime, 
		[document].UserIDAuthor AS AuthorId, 
		[document].UserIDTranscriptionist AS TranscriptionistId, 
		[document].UserIDAuthorizer AS AuthorizerId, 
		[document].WorktypeID, 
		[document].WorkPlace AS WorkPlaceId, 
		[document].InterfaceID, 
		[document].GroupID, 
		[document].documenttype AS DocumentTypeId, 
		[document].ContextID,
		[document].CorrectionRate,
/*#SCNL1007402.sn*/
		[document].AdmissionDate,
		[document].VisitDate,
		[document].DischargeDate
/*#SCNL1007402.en*/
FROM	
		Archived_G2Document [document] LEFT OUTER JOIN
		Archived_G2User author ON [document].UserIDAuthor = author.ID LEFT OUTER JOIN
        Archived_G2User transcriptionist ON [document].UserIDTranscriptionist = transcriptionist.ID LEFT OUTER JOIN
        Archived_G2User authorizer ON [document].UserIDAuthorizer = authorizer.ID LEFT OUTER JOIN
        Archived_G2Worktype worktype ON [document].WorktypeID = worktype.ID LEFT OUTER JOIN
        Archived_G2Workstation workstation ON [document].WorkPlace = workstation.ID LEFT OUTER JOIN
        Archived_G2Interface interface ON [document].InterfaceID = interface.ID LEFT OUTER JOIN
        Archived_G2Group [group] ON [document].GroupID = [group].ID LEFT OUTER JOIN
    	Archived_G2DocumentType documenttype ON [document].documenttype = documenttype.ID LEFT OUTER JOIN
		Archived_G2Context [context] ON [document].ContextID = [context].ID
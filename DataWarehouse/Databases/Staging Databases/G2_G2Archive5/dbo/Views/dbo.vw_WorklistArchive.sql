﻿CREATE VIEW [dbo].[vw_WorklistArchive]
/****** Object:  View dbo.vw_WorklistArchive   ***********************************
@Description: 	Archive worklist, used in viewer
@Remarks:	View must contain the field 'DocumentID'!
@Author: 	rdo
@History:	
		rdo	15-05-2006	Syntax.
		rdo	12-05-2006	Use Case statement instead of Replace statement for column Authorized.
		rdo	27-10-2004	Added column Authorized (Reserved04)
		rdo	17-02-2004	Created first version
************************************************************************************/
AS
SELECT 
	[Document].ID AS DocumentID,
    [Document].Title,
    Author.[Name] AS Author,
   	[Group].[Name] AS [Group],
   	CONVERT(CHAR(8),DATEADD(s,COALESCE([Document].Length,0),'0:00:00'),108) AS 'Length', 
   	Interface.[Name] AS Interface,
   	Transcriptionist.[Name] AS Transcriptionist,
   	Authorizer.[Name] AS Authoriser, 
   	Worktype.[Name] AS Worktype,
   	[Document].BatchNr AS BatchNumber,
   	[Document].Priority,
   	[Document].CreationDate,
   	Documenttype.[Name] AS DocumentType,	
	CASE WHEN [Document].Reserved02 = '1' THEN 'v' ELSE '' END AS Authorised
FROM   
	Archived_G2Document [Document]
	LEFT OUTER JOIN Archived_G2User Author ON [Document].UserIDAuthor = Author.ID
	LEFT OUTER JOIN Archived_G2User Transcriptionist ON [Document].UserIDTranscriptionist = Transcriptionist.ID
	LEFT OUTER JOIN Archived_G2User Authorizer ON [Document].UserIDAuthorizer = Authorizer.ID
	LEFT OUTER JOIN Archived_G2Worktype Worktype ON [Document].WorktypeID = Worktype.ID
	LEFT OUTER JOIN Archived_G2Interface Interface ON [document].InterfaceID = Interface.ID
	LEFT OUTER JOIN Archived_G2Group [Group] ON [Document].GroupID = [Group].ID
	LEFT OUTER JOIN Archived_G2DocumentType Documenttype ON [Document].DocumentType = Documenttype.ID
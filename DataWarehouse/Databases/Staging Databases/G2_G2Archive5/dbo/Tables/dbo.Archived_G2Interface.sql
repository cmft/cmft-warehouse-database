﻿CREATE TABLE [dbo].[Archived_G2Interface](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Reserved01] [varchar](50) NULL,
	[Reserved02] [varchar](50) NULL,
	[Reserved03] [varchar](50) NULL,
	[Reserved04] [varchar](50) NULL,
	[Reserved05] [varchar](50) NULL,
	[Reserved06] [int] NULL,
	[Reserved07] [int] NULL,
	[Reserved08] [int] NULL,
	[Reserved09] [int] NULL,
	[Reserved10] [int] NULL
) ON [PRIMARY]
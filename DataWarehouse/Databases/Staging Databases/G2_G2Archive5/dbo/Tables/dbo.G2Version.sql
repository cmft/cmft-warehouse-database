﻿CREATE TABLE [dbo].[G2Version](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](50) NULL
) ON [PRIMARY]
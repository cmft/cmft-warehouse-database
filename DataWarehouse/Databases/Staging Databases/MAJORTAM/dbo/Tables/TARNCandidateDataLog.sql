﻿CREATE TABLE [dbo].[TARNCandidateDataLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CandidateDataID] [int] NOT NULL,
	[Details] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
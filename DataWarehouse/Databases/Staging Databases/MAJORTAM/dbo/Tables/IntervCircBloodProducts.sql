﻿CREATE TABLE [dbo].[IntervCircBloodProducts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TARNID] [int] NOT NULL,
	[Description] [varchar](60) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NULL
) ON [PRIMARY]
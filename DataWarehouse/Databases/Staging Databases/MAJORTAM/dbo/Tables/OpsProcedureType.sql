﻿CREATE TABLE [dbo].[OpsProcedureType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProcedureType] [varchar](50) NOT NULL
) ON [PRIMARY]
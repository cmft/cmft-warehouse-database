﻿CREATE TABLE [dbo].[IncidentAddInjuryInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TarnID] [int] NOT NULL,
	[Description] [nvarchar](60) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_IncidentAddInjuryInfo_Active]  DEFAULT ((1)),
	[Order] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[OCCANCEL20150511](
	[OCCANCELID] [varchar](254) NOT NULL,
	[Comment1] [varchar](60) NULL,
	[Comment2] [varchar](60) NULL,
	[Comment3] [varchar](60) NULL,
	[Comment4] [varchar](60) NULL,
	[Comment5] [varchar](60) NULL,
	[Comment6] [varchar](60) NULL,
	[DateCancelled] [varchar](10) NULL,
	[EntereBy] [varchar](4) NULL,
	[OcmTransKey] [varchar](9) NOT NULL,
	[SessionNumber] [varchar](10) NOT NULL,
	[SessionNumberExt] [varchar](8) NULL,
	[Status] [varchar](30) NULL,
	[StatusInt] [varchar](2) NULL,
	[TimeCancelled] [varchar](10) NULL,
	[TransactionNumber] [varchar](5) NULL
) ON [PRIMARY]
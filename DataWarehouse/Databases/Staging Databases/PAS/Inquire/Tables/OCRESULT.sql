﻿CREATE TABLE [Inquire].[OCRESULT](
	[OcmTransKey] [int] NOT NULL,
	[SessionNumber] [int] NOT NULL,
	[ResultCode] [char](4) NOT NULL,
	[Status] [smallint] NULL,
	[Result] [varchar](9) NULL,
	[ResultDate] [date] NULL,
	[ResultTime] [varchar](10) NULL,
	[Comment1] [varchar](60) NULL,
	[Comment2] [varchar](60) NULL,
	[Comment3] [varchar](60) NULL,
	[Comment4] [varchar](60) NULL,
	[Comment5] [varchar](60) NULL,
	[Comment6] [varchar](60) NULL,
	[EnteredBy] [varchar](4) NULL,
	[RangeIndicator] [smallint] NULL,
	[OCRESULTID]  AS ((((CONVERT([varchar](10),[OcmTransKey],(0))+'||')+CONVERT([varchar](10),[SessionNumber],(0)))+'||')+CONVERT([varchar](10),[ResultCode],(0))),
 CONSTRAINT [pk_OCRESULT] PRIMARY KEY CLUSTERED 
(
	[OcmTransKey] ASC,
	[SessionNumber] ASC,
	[ResultCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
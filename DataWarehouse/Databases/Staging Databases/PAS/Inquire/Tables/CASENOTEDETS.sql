﻿CREATE TABLE [Inquire].[CASENOTEDETS](
	[CASENOTEDETSID] [nvarchar](255) NULL,
	[Allocated] [nvarchar](255) NULL,
	[Comment] [nvarchar](30) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[Location] [nvarchar](4) NULL,
	[PtCasenoteIdPhyskey] [nvarchar](14) NULL,
	[PtCsNtLocDat] [nvarchar](255) NULL,
	[Status] [nvarchar](10) NULL,
	[Withdrawn] [nvarchar](255) NULL
)
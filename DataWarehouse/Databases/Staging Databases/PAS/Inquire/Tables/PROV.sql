﻿CREATE TABLE [Inquire].[PROV](
	[PROVID] [nvarchar](4) NOT NULL,
	[CmmCommissioningMainCommissionerDescription] [nvarchar](30) NULL,
	[CmmCommissioningMainCommissionerPcgDesc] [nvarchar](30) NULL,
	[CmmTypeOfProviderDescription] [nvarchar](30) NULL,
	[Description] [nvarchar](30) NULL,
	[DohCode] [nvarchar](5) NULL,
	[MainCommissionerHa] [nvarchar](5) NULL,
	[MainCommissionerPcg] [nvarchar](5) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[OrganisationCode] [nvarchar](3) NULL,
	[ProvType] [nvarchar](2) NULL,
	[ProviderCode] [nvarchar](4) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[OCMFDEPT](
	[HospitalCode] [smallint] NOT NULL,
	[Department] [varchar](4) NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Category] [varchar](1) NULL,
	[OCMFDEPTID]  AS ((CONVERT([varchar](4),[HospitalCode],0)+'||')+[Department]),
 CONSTRAINT [PK_OCMFDEPT] PRIMARY KEY CLUSTERED 
(
	[HospitalCode] ASC,
	[Department] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
﻿CREATE TABLE [Inquire].[GDP](
	[GDPID] [nvarchar](6) NOT NULL,
	[Address] [nvarchar](20) NULL,
	[DpbContractCode] [nvarchar](8) NULL,
	[GdpAddLine2] [nvarchar](20) NULL,
	[GdpAddrLine3] [nvarchar](20) NULL,
	[GdpAddrLine4] [nvarchar](20) NULL,
	[GdpAddrOnOneLine] [nvarchar](70) NULL,
	[GdpCode] [nvarchar](6) NOT NULL,
	[Initials] [nvarchar](5) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[Name] [nvarchar](35) NULL,
	[Phone] [nvarchar](23) NULL,
	[Postcode] [nvarchar](10) NULL,
	[Surname] [nvarchar](24) NULL,
	[Title] [nvarchar](4) NULL
) ON [PRIMARY]
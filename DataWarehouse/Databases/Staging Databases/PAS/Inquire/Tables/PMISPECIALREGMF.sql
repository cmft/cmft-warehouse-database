﻿CREATE TABLE [Inquire].[PMISPECIALREGMF](
	[PMISPECIALREGMFID] [varchar](4) NOT NULL,
	[SpecialRegister] [varchar](4) NOT NULL,
	[SpecialRegisterDescription] [varchar](30) NULL,
	[PmiSpecialRegisterCodeFormOfWarningInt] [varchar](1) NULL,
	[FormOfWarning] [varchar](9) NULL,
	[MfRecStsInd] [bit] NULL
) ON [PRIMARY]
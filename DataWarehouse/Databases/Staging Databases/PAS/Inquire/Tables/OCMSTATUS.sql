﻿CREATE TABLE [Inquire].[OCMSTATUS](
	[StatusSeqCd] [tinyint] NOT NULL,
	[StatusDescription] [varchar](30) NULL,
	[MfRecStsInd] [bit] NOT NULL,
	[CollectDt] [varchar](3) SPARSE  NULL,
	[OcmPromptForDatetimeOfCollection] [varchar](1) SPARSE  NULL
)
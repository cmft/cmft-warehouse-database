﻿CREATE TABLE [Inquire].[RTTPERSTSMF](
	[RTTPERSTSMFID] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[RttStatusCode] [nvarchar](4) NOT NULL,
	[RttStsDefault] [nvarchar](3) NULL,
	[RttStsDescription] [nvarchar](30) NULL,
	[RttStsInternalValue] [nvarchar](2) NULL,
 CONSTRAINT [pk_RTTPERSTSMF] PRIMARY KEY CLUSTERED 
(
	[RttStatusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
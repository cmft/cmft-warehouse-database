﻿CREATE TABLE [Inquire].[CONTR](
	[CONTRID] [nvarchar](255) NULL,
	[AgreementCDS] [nvarchar](6) NULL,
	[CidGrp] [nvarchar](1) NULL,
	[CidType] [nvarchar](1) NULL,
	[Cm2ContractMfSuitableToAutoAllocateInt] [nvarchar](1) NULL,
	[ContractDesc] [nvarchar](30) NULL,
	[ContractGroup] [nvarchar](4) NULL,
	[ContractGroupCodeDescription] [nvarchar](30) NULL,
	[ContractId] [nvarchar](6) NULL,
	[ContractId_1] [nvarchar](16) NULL,
	[ContractManagementEndDateInternal] [nvarchar](8) NULL,
	[ContractManagementStartDateInternal] [nvarchar](8) NULL,
	[Costsum] [nvarchar](10) NULL,
	[Description] [nvarchar](30) NULL,
	[Description_1] [nvarchar](30) NULL,
	[EndDate] [nvarchar](10) NULL,
	[InterimBilling] [nvarchar](3) NULL,
	[IntrmBilling] [nvarchar](3) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[OutpatientPrice1st] [nvarchar](7) NULL,
	[PaymentTerms] [nvarchar](2) NULL,
	[PriceProfile] [nvarchar](10) NULL,
	[PriceProfileDescription] [nvarchar](30) NULL,
	[ProcedureProfileCode] [nvarchar](4) NULL,
	[ProviderCode] [nvarchar](4) NULL,
	[PurchaserCode] [nvarchar](5) NULL,
	[Reatt] [nvarchar](7) NULL,
	[StartDate] [nvarchar](10) NULL,
	[SuitableToAutoallocate] [nvarchar](3) NULL,
	[Thresholdceiling] [nvarchar](4) NULL,
	[Type] [nvarchar](16) NULL,
	[TypeOfCont] [nvarchar](2) NULL,
	[WardAttPrice] [nvarchar](7) NULL
) ON [PRIMARY]
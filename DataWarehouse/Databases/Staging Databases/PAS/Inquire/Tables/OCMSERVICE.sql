﻿CREATE TABLE [Inquire].[OCMSERVICE](
	[HospitalCode] [smallint] NOT NULL,
	[Service] [int] NOT NULL,
	[Description] [varchar](30) NULL,
	[DescriptionLabels] [varchar](6) NULL,
	[DescriptionShort] [varchar](13) NULL,
	[CategoryOfService] [varchar](12) NULL,
	[CategoryOfServiceInt] [tinyint] NULL,
	[ScreenType] [varchar](4) NULL,
	[Mnemonic] [varchar](13) NULL,
	[Deleted] [bit] NOT NULL,
	[OCMSERVICEID]  AS ((CONVERT([varchar](20),[HospitalCode],0)+'||')+CONVERT([varchar](20),[Service],0)),
 CONSTRAINT [PK_SERVICE] PRIMARY KEY CLUSTERED 
(
	[HospitalCode] ASC,
	[Service] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
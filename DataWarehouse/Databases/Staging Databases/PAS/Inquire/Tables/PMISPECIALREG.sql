﻿CREATE TABLE [Inquire].[PMISPECIALREG](
	[ActivityDateTime] [datetime] NULL,
	[DateEntered] [date] NULL,
	[EpisodeNumber] [nvarchar](9) NULL,
	[InternalPatientNumber] [int] NULL,
	[SpecialRegister] [nvarchar](50) NOT NULL
) ON [PRIMARY]
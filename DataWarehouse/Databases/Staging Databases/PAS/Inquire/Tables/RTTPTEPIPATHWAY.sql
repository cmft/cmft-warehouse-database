﻿CREATE TABLE [Inquire].[RTTPTEPIPATHWAY](
	[RTTPTEPIPATHWAYID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](9) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[PathwayNumber] [nvarchar](25) NULL,
	[PathwayOrgProv] [nvarchar](5) NULL,
 CONSTRAINT [IX_RTTPTEPIPATHWAY] UNIQUE CLUSTERED 
(
	[InternalPatientNumber] ASC,
	[EpisodeNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
﻿CREATE TABLE [Inquire].[STAFFGP](
	[STAFFGPID] [nvarchar](255) NULL,
	[GradeOfStaffCode] [nvarchar](4) NULL,
	[GradeOfStaffDescription] [nvarchar](40) NULL,
	[MedicalStaffType] [nvarchar](2) NULL,
	[MedicalStaffTypeDescription] [nvarchar](25) NULL,
	[MfRecStsInd] [nvarchar](255) NULL
) ON [PRIMARY]
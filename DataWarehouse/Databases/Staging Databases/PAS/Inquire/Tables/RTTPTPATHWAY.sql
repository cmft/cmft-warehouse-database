﻿CREATE TABLE [Inquire].[RTTPTPATHWAY](
	[RTTPTPATHWAYID] [nvarchar](255) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[PathwayCondition] [nvarchar](20) NULL,
	[PathwayNumber] [nvarchar](25) NOT NULL,
	[PathwayOrgProv] [nvarchar](5) NULL,
	[RttCurProv] [nvarchar](4) NULL,
	[RttCurrentStatus] [nvarchar](4) NULL,
	[RttCurrentStatusDateTime] [nvarchar](255) NULL,
	[RttOsvStatus] [nvarchar](1) NULL,
	[RttPrivatePat] [nvarchar](1) NULL,
	[RttSpeciality] [nvarchar](4) NULL,
 CONSTRAINT [pk_RTTPTPATHWAY] PRIMARY KEY CLUSTERED 
(
	[InternalPatientNumber] ASC,
	[PathwayNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
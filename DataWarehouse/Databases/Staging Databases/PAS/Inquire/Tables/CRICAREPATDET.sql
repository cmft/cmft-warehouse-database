﻿CREATE TABLE [Inquire].[CRICAREPATDET](
	[CRICAREPATDETID] [nvarchar](255) NULL,
	[CcpAdvCarLevDays] [nvarchar](3) NULL,
	[CcpAdvResLevDays] [nvarchar](3) NULL,
	[CcpBasCarLevDays] [nvarchar](3) NULL,
	[CcpBasResLevDays] [nvarchar](3) NULL,
	[CcpCcLevel2Days] [nvarchar](3) NULL,
	[CcpCcLevel3Days] [nvarchar](3) NULL,
	[CcpCreaterevByuser] [nvarchar](3) NULL,
	[CcpCreaterevDtInt] [nvarchar](12) NULL,
	[CcpDerSupportDays] [nvarchar](3) NULL,
	[CcpEndDate] [nvarchar](16) NULL,
	[CcpEndDtInt] [nvarchar](12) NULL,
	[CcpLivSupportDays] [nvarchar](3) NULL,
	[CcpLocalIdentifier] [nvarchar](8) NULL,
	[CcpLocation] [nvarchar](4) NULL,
	[CcpNeuSupportDays] [nvarchar](3) NULL,
	[CcpRenSupportDays] [nvarchar](3) NULL,
	[CcpStartDate] [nvarchar](16) NULL,
	[CcpStartDtInt] [nvarchar](12) NOT NULL,
	[CcpStartTime] [nvarchar](10) NULL,
	[CcpStatus] [nvarchar](11) NULL,
	[CcpTreatmentFunc] [nvarchar](4) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[NoofSupportSystems] [nvarchar](1) NULL,
	[PlannedAcpPeriod] [nvarchar](3) NULL,
 CONSTRAINT [PK_CRICAREPATDET] PRIMARY KEY CLUSTERED 
(
	[CcpStartDtInt] ASC,
	[EpisodeNumber] ASC,
	[InternalPatientNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
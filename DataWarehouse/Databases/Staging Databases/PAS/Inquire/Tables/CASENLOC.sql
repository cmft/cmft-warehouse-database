﻿CREATE TABLE [Inquire].[CASENLOC](
	[CASENLOCID] [nvarchar](255) NULL,
	[Location] [nvarchar](4) NULL,
	[LocationName] [nvarchar](15) NULL,
	[MfRecStsInd] [nvarchar](255) NULL
) ON [PRIMARY]
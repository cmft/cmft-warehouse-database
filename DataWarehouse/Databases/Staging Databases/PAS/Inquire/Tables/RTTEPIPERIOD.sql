﻿CREATE TABLE [Inquire].[RTTEPIPERIOD](
	[RTTEPIPERIODID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](9) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[PathwayNumber] [nvarchar](25) NULL,
	[PPeriodId] [nvarchar](4) NULL,
	[RttStartDateTimeInt] [nvarchar](12) NULL
)
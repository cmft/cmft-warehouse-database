﻿CREATE TABLE [Inquire].[OCORDERRSONEW](
	[InternalPatientNumber] [int] NOT NULL,
	[EpisodeNumber] [int] NOT NULL,
	[ResultDtInt] [bigint] NOT NULL,
	[OcmTransKey] [int] NOT NULL,
	[SessionNumber] [int] NOT NULL,
	[ResultDt] [date] not null , -- AS (CONVERT([date],CONVERT([varchar](8),[resultdtint],0),(112))),
	[ResultTime] [time] not null , -- AS (CONVERT([time],(left(CONVERT([varchar](12),[resultdtint],0),(2))+':')+right(CONVERT([varchar](12),[resultdtint],0),(2)),0)),
	[OCORDERRSONEWID] [varchar](50) not null -- AS (((((((((CONVERT([varchar](50),[InternalPatientNumber],0)+'||')+CONVERT([varchar](50),[episodenumber],0))+'||')+CONVERT([varchar](50),[ResultDtInt],0))+'||')+CONVERT([varchar](50),[ocmtranskey],0))+'||')+CONVERT([varchar](50),[sessionNumber],0))+'||')
)
﻿CREATE TABLE [Inquire].[HOSPDET](
	[HOSPDETID] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](20) NULL,
	[DeliveryFacilities] [nvarchar](1) NULL,
	[HaaCode] [nvarchar](5) NULL,
	[HospAddrLine2] [nvarchar](20) NULL,
	[HospAddrLine3] [nvarchar](20) NULL,
	[HospAddrLine4] [nvarchar](20) NULL,
	[HospitalCode] [nvarchar](4) NULL,
	[HospitalName] [nvarchar](30) NULL,
	[KornerSiteType] [nvarchar](1) NULL,
	[Location] [nvarchar](5) NULL,
	[MainSpecialty1] [nvarchar](4) NULL,
	[MainSpecialty2] [nvarchar](4) NULL,
	[MainSpecialty3] [nvarchar](4) NULL,
	[MainSpecialty4] [nvarchar](4) NULL,
	[MaternityTransactionsRequired] [nvarchar](1) NULL,
	[PhoneNo] [nvarchar](23) NULL,
	[Postcode] [nvarchar](10) NULL,
	[ProviderCode] [nvarchar](4) NULL,
	[TreatmentSiteCd] [nvarchar](5) NULL,
 CONSTRAINT [PK_HOSPDET] PRIMARY KEY CLUSTERED 
(
	[HOSPDETID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[OPDIAGNOSIS](
	[OPDIAGNOSISID] [nvarchar](255) NULL,
	[CurrentOpRegDate] [nvarchar](12) NULL,
	[Diagnosis] [nvarchar](8) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[OpEpisodeSecondaryDiagnosisSequenceNumber] [nvarchar](2) NOT NULL,
	[ReadCodeRepeatGroupSecondaryDiagnosisCode] [nvarchar](8) NULL,
 CONSTRAINT [PK_OPDIAGNOSIS] PRIMARY KEY CLUSTERED 
(
	[EpisodeNumber] ASC,
	[InternalPatientNumber] ASC,
	[OpEpisodeSecondaryDiagnosisSequenceNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[AEATTENDANCE](
	[AEATTENDANCEID] [varchar](255) NOT NULL,
	[AeAttendanceDatetimeneutral] [varchar](12) NOT NULL,
	[DepartmentCode] [varchar](8) NOT NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[EpisodeNumber] [varchar](9) NOT NULL,
	[HospitalCode] [varchar](4) NOT NULL
) ON [PRIMARY]
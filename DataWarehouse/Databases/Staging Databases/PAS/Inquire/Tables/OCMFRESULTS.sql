﻿CREATE TABLE [Inquire].[OCMFRESULTS](
	[ResultCd] [varchar](4) NOT NULL,
	[ResultDescription] [varchar](30) NULL,
	[ResultName] [varchar](15) NULL,
	[ResultType] [varchar](14) NULL,
	[UnitOfMeasurement] [varchar](8) NULL,
	[Deleted] [bit] NOT NULL,
	[OCMFRESULTSID]  AS ([ResultCd]),
 CONSTRAINT [PK_OCMFRESULTS] PRIMARY KEY CLUSTERED 
(
	[ResultCd] ASC,
	[Deleted] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
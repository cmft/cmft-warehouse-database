﻿CREATE TABLE [Inquire].[OCMTEXTRESULTS](
	[OcmTransKey] [int] NOT NULL,
	[SessionNumber] [int] NOT NULL,
	[ResultCode] [char](4) NOT NULL,
	[AgpSeqNo] [smallint] NOT NULL,
	[Comment] [varchar](max) NULL,
	[SubResultName]  [varchar](max) null , --AS (ltrim(rtrim(case when charindex(':',[comment])>(0) AND right([comment],(1))<>':' then left([comment],charindex(':',[comment])-(1))  end))),
	[SubResultValue]  [varchar](max) null , --AS (ltrim(rtrim(case when charindex(':',[comment])>(0) AND right([comment],(1))<>':' then right([comment],len([comment])-charindex(':',[comment]))  end))),
 CONSTRAINT [PK_Inq_OCMTEXTRESULTS] PRIMARY KEY CLUSTERED 
(
	[OcmTransKey] ASC,
	[SessionNumber] ASC,
	[ResultCode] ASC,
	[AgpSeqNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
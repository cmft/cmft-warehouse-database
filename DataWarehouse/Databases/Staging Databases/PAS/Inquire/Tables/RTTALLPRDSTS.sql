﻿CREATE TABLE [Inquire].[RTTALLPRDSTS](
	[RTTALLPRDSTSID] [nvarchar](255) NULL,
	[ActivityDateTime] [nvarchar](16) NULL,
	[ActivityDateTimeInt] [nvarchar](12) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[PathwayNumber] [nvarchar](25) NULL,
	[PRDStsType] [nvarchar](5) NULL,
	[RttPRDSts] [nvarchar](4) NULL,
	[RttPRDStsInt] [nvarchar](2) NULL
)
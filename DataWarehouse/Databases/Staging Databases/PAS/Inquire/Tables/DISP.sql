﻿CREATE TABLE [Inquire].[DISP](
	[Applications] [varchar](7) NULL,
	[Description] [varchar](30) NULL,
	[DisposalCode] [varchar](4) NOT NULL,
	[Doh] [varchar](2) NULL,
	[MfRecStsInd] [varchar](255) NULL,
	[ScottishIntValue] [varchar](1) NULL
) ON [PRIMARY]
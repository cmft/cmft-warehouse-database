﻿CREATE TABLE [Inquire].[OPATTTRACK](
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[OpCurrentRegDate] [varchar](255) NULL,
	[OpCurrentRegTime] [varchar](10) NULL,
	[PtApptStartDate] [varchar](14) NULL,
	[PtApptStartDtimeInt] [varchar](12) NOT NULL,
	[PtApptStartTime] [varchar](10) NULL,
	[PtTrackingStepDate] [varchar](12) NULL,
	[PtTrackingStepTime] [varchar](10) NULL,
	[ResCode] [varchar](8) NOT NULL,
	[SchPatientTrackingStepDatetimeNeutral] [varchar](12) NULL,
	[TrackingStep] [varchar](4) NOT NULL
) ON [PRIMARY]
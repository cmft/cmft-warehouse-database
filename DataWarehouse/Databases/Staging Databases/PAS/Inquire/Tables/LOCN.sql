﻿CREATE TABLE [Inquire].[LOCN](
	[LOCNID] [varchar](4) NOT NULL,
	[LocationCode] [varchar](4) NOT NULL,
	[LocationDesc] [varchar](20) NULL,
	[LocationDesc_1] [varchar](30) NULL,
	[LocationType] [varchar](2) NULL,
	[MfRecStsInd] [varchar](255) NULL,
	[SgLocnLink] [varchar](6) NULL
) ON [PRIMARY]
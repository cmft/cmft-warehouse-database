﻿CREATE TABLE [Inquire].[OPWLREMENTRY](
	[OPWLREMENTRYID] [nvarchar](254) NULL,
	[AppointmentType] [nvarchar](3) NULL,
	[CancelReasonInt] [nvarchar](1) NULL,
	[Category] [nvarchar](3) NULL,
	[Comment] [nvarchar](55) NULL,
	[Date1stLetterSent] [nvarchar](10) NULL,
	[Date1stLetterSentInt] [nvarchar](12) NULL,
	[Date1stReminder] [nvarchar](10) NULL,
	[Date1stReminderInt] [nvarchar](8) NULL,
	[Date2ndReminder] [nvarchar](10) NULL,
	[Date2ndReminderInt] [nvarchar](8) NULL,
	[DateOnList] [nvarchar](10) NULL,
	[DateOnListInt] [nvarchar](12) NULL,
	[EpisodeNumber] [nvarchar](9) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[LastReviewDate] [nvarchar](12) NULL,
	[LastReviewedUserId] [nvarchar](4) NULL,
	[OPRegDate] [nvarchar](10) NULL,
	[OPRegDateInt] [nvarchar](12) NULL,
	[OpWlPatientCons] [nvarchar](6) NULL,
	[OpWlPatientSpecialty] [nvarchar](4) NULL,
	[OpWlRemDatetimeExt] [nvarchar](16) NULL,
	[OpWlRemDatetimeInt] [nvarchar](12) NULL,
	[OpWlRemovalCode] [nvarchar](4) NULL,
	[OpWlRemovalComment] [nvarchar](25) NULL,
	[OpWlRemovalDesc] [nvarchar](30) NULL,
	[PatientResponse] [nvarchar](10) NULL,
	[PatientResponseInt] [nvarchar](8) NULL,
	[ProcedureType] [nvarchar](55) NULL,
	[ResponseComments] [nvarchar](40) NULL,
	[ShortNotice] [nvarchar](3) NULL,
	[TransportCode] [nvarchar](2) NULL,
	[TreatmentCode] [nvarchar](8) NULL,
	[WLStatus] [nvarchar](255) NULL,
	[WLStatusDesc] [nvarchar](30) NULL,
	[WlDateAppReq] [nvarchar](7) NULL,
	[WlDateAppReqInt] [nvarchar](6) NULL
) ON [PRIMARY]
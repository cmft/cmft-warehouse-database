﻿CREATE TABLE [Inquire].[PREADMISSION](
	[Activity] [varchar](255) NULL,
	[ActivityDate] [varchar](14) NOT NULL,
	[ActivityTime] [varchar](10) NOT NULL,
	[EpsActvDtimeInt] [varchar](12) NOT NULL,
	[InternalPatientNumber] [int] NOT NULL,
	[EpisodeNumber] [int] NOT NULL,
	[CaseNoteNumber] [varchar](14) NULL,
	[OriginalDateOnList] [varchar](10) NULL,
	[DateOnWList] [varchar](10) NULL,
	[CancelDeferInd] [varchar](3) NULL,
	[CancelledBy] [varchar](3) NULL,
	[CancelReasonCd] [varchar](30) NULL,
	[Category] [varchar](3) NULL,
	[DistrictNumber] [varchar](14) NULL,
	[TCIDate] [varchar](14) NULL,
	[HospCode] [varchar](4) NULL,
	[Consultant] [varchar](6) NULL,
	[Specialty] [varchar](4) NULL,
	[ExpectedWard] [varchar](4) NULL,
	[IntendedProc] [varchar](7) NULL,
	[LatestAdmDate] [varchar](10) NULL,
	[MethodOfAdm] [varchar](2) NULL,
 CONSTRAINT [PK_Inq_PREADMISSION] PRIMARY KEY CLUSTERED 
(
	[InternalPatientNumber] ASC,
	[EpisodeNumber] ASC,
	[ActivityDate] ASC,
	[ActivityTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
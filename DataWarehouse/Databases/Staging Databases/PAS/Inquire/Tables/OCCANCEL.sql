﻿CREATE TABLE [Inquire].[OCCANCEL](
	[OcmTransKey] [int] NOT NULL,
	[SessionNumber] [int] NOT NULL,
	[TransactionNumber] [int] NULL,
	[DateCancelled] [date] NULL,
	[TimeCancelled] [time](7) NULL,
	[EnteredBy] [varchar](3) NULL,
	[Status] [tinyint] NULL,
	[Comment1] [varchar](60) SPARSE  NULL,
	[Comment2] [varchar](60) SPARSE  NULL,
	[Comment3] [varchar](60) SPARSE  NULL,
	[Comment4] [varchar](60) SPARSE  NULL,
	[Comment5] [varchar](60) SPARSE  NULL,
	[Comment6] [varchar](60) SPARSE  NULL,
	[OCCANCELID] [varchar](12) -- AS ((CONVERT([varchar](12),[ocmtranskey],0)+'||')+CONVERT([varchar](12),[sessionnumber],0)),
 CONSTRAINT [pk_OCCANCEL] PRIMARY KEY CLUSTERED 
(
	[OcmTransKey] ASC,
	[SessionNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
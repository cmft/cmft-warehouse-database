﻿CREATE TABLE [Inquire].[CONSEPISDIAG](
	[CONSEPISDIAGID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[FCEEndDate] [nvarchar](10) NULL,
	[FCEEndDateTime] [nvarchar](12) NULL,
	[FCEEndTime] [nvarchar](10) NULL,
	[FCENumber] [nvarchar](4) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[ReadCode] [nvarchar](8) NULL,
	[SecondaryDiagnosis] [nvarchar](8) NULL,
	[SecondaryDiagSeqNo] [nvarchar](3) NOT NULL,
	[SeqNo] [nvarchar](30) NULL,
 CONSTRAINT [PK_CONSEPISDIAG] PRIMARY KEY CLUSTERED 
(
	[EpisodeNumber] ASC,
	[FCENumber] ASC,
	[InternalPatientNumber] ASC,
	[SecondaryDiagSeqNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
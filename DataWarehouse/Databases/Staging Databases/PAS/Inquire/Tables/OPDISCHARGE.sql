﻿CREATE TABLE [Inquire].[OPDISCHARGE](
	[OPDISCHARGEID] [nvarchar](255) NULL,
	[DischargeCode] [nvarchar](255) NULL,
	[DischargeDatetime] [nvarchar](16) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[OpDischargeDTimeInt] [nvarchar](12) NULL,
	[OpRegDtimeInt] [nvarchar](12) NOT NULL,
	[Reason] [nvarchar](30) NULL,
	[ReasonCode] [nvarchar](4) NULL,
 CONSTRAINT [PK_OPDISCHARGE] PRIMARY KEY CLUSTERED 
(
	[EpisodeNumber] ASC,
	[InternalPatientNumber] ASC,
	[OpRegDtimeInt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
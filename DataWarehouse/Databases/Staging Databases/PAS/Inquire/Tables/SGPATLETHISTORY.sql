﻿CREATE TABLE [Inquire].[SGPATLETHISTORY](
	[SGPATLETHISTORYID] [varchar](254) NOT NULL,
	[EpisodeNumber] [varchar](9) NOT NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[Clinic] [varchar](8) NULL,
	[Clinician] [varchar](8) NULL,
	[ApptDTInt] [varchar](16) NULL,
	[ApptDate] [varchar](10) NULL,
	[ApptTime] [varchar](10) NULL,
	[ClassificationInt] [varchar](2) NULL,
	[DocID] [varchar](10) NULL,
	[DocIDDesc] [varchar](30) NULL,
	[RequestDTDate] [varchar](10) NULL,
	[RequestDTTime] [varchar](10) NULL,
	[PatLetterHistoryReqDatetimeRevChronNeutral] [varchar](12) NOT NULL,
	[PatLetterHistoryRequestDatetimeSeqNumber] [varchar](1) NOT NULL,
	[Reviseddatetimeint] [varchar](12) NULL,
	[Withdrawnint] [varchar](1) NULL
) ON [PRIMARY]
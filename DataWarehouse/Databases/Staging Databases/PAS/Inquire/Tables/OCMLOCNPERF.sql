﻿CREATE TABLE [Inquire].[OCMLOCNPERF](
	[OCMLOCNPERFID] [varchar](254) NOT NULL,
	[DefaultPerfLocn] [varchar](4) NULL,
	[FixedCostDefPerfLocn] [varchar](9) NULL,
	[FixedCostPerfLocn] [varchar](9) NULL,
	[Hospital] [varchar](4) NOT NULL,
	[HospitalCode] [varchar](4) NOT NULL,
	[PerformingLocation] [varchar](4) NOT NULL,
	[Service] [varchar](7) NOT NULL,
	[VariableCostDefPerfLocn] [varchar](9) NULL,
	[VariableCostPerfLocn] [varchar](9) NULL
)
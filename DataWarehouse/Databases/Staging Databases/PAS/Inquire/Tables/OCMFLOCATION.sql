﻿CREATE TABLE [Inquire].[OCMFLOCATION](
	[PerformingLoc] [varchar](4) NOT NULL,
	[Description] [varchar](20) NULL,
	[MfRecStsInd] [bit] NULL,
 CONSTRAINT [pk_ocmflocation] PRIMARY KEY CLUSTERED 
(
	[PerformingLoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
﻿CREATE TABLE [Inquire].[CASENOTELINK](
	[CASENOTELINKID] [nvarchar](254) NULL,
	[EntityType] [nvarchar](25) NULL,
	[EntityTypeKey] [nvarchar](2) NULL,
	[ExternalNumber] [nvarchar](20) NULL,
	[ExternalNumberKey] [nvarchar](20) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[CONSEPISPROC](
	[CONSEPISPROCID] [nvarchar](255) NULL,
	[DoctorPerfOp] [nvarchar](8) NULL,
	[DrAdmAnaes] [nvarchar](8) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[FCEEndDate] [nvarchar](10) NULL,
	[FCEEndDateTimeInt] [nvarchar](12) NULL,
	[FCEEndTime] [nvarchar](10) NULL,
	[FCENumber] [nvarchar](4) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[OPCS] [nvarchar](6) NULL,
	[ProcedureDate] [nvarchar](10) NULL,
	[ProcedureDateInt] [nvarchar](8) NULL,
	[ReadCode] [nvarchar](8) NULL,
	[SecondaryProcSeqNo] [nvarchar](3) NULL,
	[SeqNo] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_CONSEPISPROC] PRIMARY KEY CLUSTERED 
(
	[EpisodeNumber] ASC,
	[FCENumber] ASC,
	[InternalPatientNumber] ASC,
	[SeqNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
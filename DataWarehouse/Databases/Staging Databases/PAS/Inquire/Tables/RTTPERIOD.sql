﻿CREATE TABLE [Inquire].[RTTPERIOD](
	[RTTPERIODID] [nvarchar](255) NULL,
	[Condition] [nvarchar](20) NULL,
	[CurrCategory] [nvarchar](1) NULL,
	[CurrCons] [nvarchar](10) NULL,
	[CurrentStatus] [nvarchar](4) NULL,
	[CurrentStatusDt] [nvarchar](10) NULL,
	[CurrentStatusDtInt] [nvarchar](12) NULL,
	[CurrentStatusInt] [nvarchar](2) NULL,
	[CurrOSV] [nvarchar](1) NULL,
	[CurrProv] [nvarchar](10) NULL,
	[CurrSpec] [nvarchar](10) NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[PathwayNumber] [nvarchar](25) NOT NULL,
	[PauseDate] [nvarchar](10) NULL,
	[PauseDtInt] [nvarchar](12) NULL,
	[PPeriodId] [nvarchar](4) NOT NULL,
	[RttEndDate] [nvarchar](10) NULL,
	[RttEndDateTimeInt] [nvarchar](12) NULL,
	[RttStartDate] [nvarchar](10) NULL,
	[RttStartDateTimeInt] [nvarchar](12) NOT NULL,
 CONSTRAINT [pk_RTTPERIOD] PRIMARY KEY NONCLUSTERED 
(
	[InternalPatientNumber] ASC,
	[PathwayNumber] ASC,
	[PPeriodId] ASC,
	[RttStartDateTimeInt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY]
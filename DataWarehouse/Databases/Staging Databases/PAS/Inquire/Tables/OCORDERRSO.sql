﻿CREATE TABLE [Inquire].[OCORDERRSO](
	[OcmTransKey] [int] NOT NULL,
	[SessionNumber] [int] NOT NULL,
	[InternalPatientNumber] [int] NOT NULL,
	[EpisodeNumber] [int] NOT NULL,
	[ResultSignOffDateTime] [datetime] SPARSE  NULL,
	[SignOffDateTimeInt] [bigint] SPARSE  NULL,
	[SignOffUID] [varchar](20) SPARSE  NULL,
	[OCORDERRSOID] [varchar](50) -- AS ((CONVERT([varchar](50),[ocmtranskey],0)+'||')+CONVERT([varchar](50),[sessionnumber],0)),
 CONSTRAINT [pk_OCORDERRSO] PRIMARY KEY CLUSTERED 
(
	[OcmTransKey] ASC,
	[SessionNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
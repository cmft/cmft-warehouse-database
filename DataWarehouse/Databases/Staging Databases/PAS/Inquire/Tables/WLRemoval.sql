﻿CREATE TABLE [Inquire].[WLRemoval](
	[WLREMOVALID] [varchar](254) NOT NULL,
	[ActivityDate] [varchar](14) NULL,
	[ActivityTime] [varchar](10) NULL,
	[Comment] [varchar](255) NULL,
	[EpisodeNumber] [varchar](9) NOT NULL,
	[EpsActvDtimeInt] [varchar](12) NOT NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[OpCancelled] [varchar](1) NULL,
	[OpCancelledDesc] [varchar](10) NULL,
	[PatSelfDeferral] [varchar](3) NULL,
	[PatSelfDeferralInt] [varchar](1) NULL,
	[PatientChoice] [varchar](1) NULL,
	[PatientChoiceDesc] [varchar](20) NULL,
	[Reason] [varchar](4) NULL,
 CONSTRAINT [pk_WLRemoval] PRIMARY KEY CLUSTERED 
(
	[InternalPatientNumber] ASC,
	[EpisodeNumber] ASC,
	[EpsActvDtimeInt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
﻿CREATE TABLE [Inquire].[ALLOCATEDCONTRACT](
	[ALLOCATEDCONTRACTID] [nvarchar](255) NULL,
	[CmNeutralDttime] [nvarchar](12) NULL,
	[CmReverseDttime] [nvarchar](12) NOT NULL,
	[ContractId] [nvarchar](6) NULL,
	[ContractStartDate] [nvarchar](10) NULL,
	[ContractStartTime] [nvarchar](10) NULL,
	[EcrType] [nvarchar](1) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[LineNumber] [nvarchar](10) NULL,
	[PurchaserRef] [nvarchar](20) NULL,
 CONSTRAINT [PK_ALLOCATEDCONTRACT] PRIMARY KEY CLUSTERED 
(
	[CmReverseDttime] ASC,
	[EpisodeNumber] ASC,
	[InternalPatientNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [Inquire].[CPFDISCHREASON](
	[DischargeReason] [varchar](4) NOT NULL,
	[Description] [varchar](255) NULL,
	[MfRecStsInd] [bit] NULL
) ON [PRIMARY]
﻿
USE [PAS]
GO

/****** Object:  Table [dbo].[Casenote__DistrictNumber]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[CASENOTELINK_ENTITYTYPES]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[CASENOTEtoIPN]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CompressedHospitalToIPN]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[derived_Appointments]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[opwlrementry]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [derived].[CurrentCaseNoteIDs]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [derived].[NonCurrentCaseNoteIDs]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [derived].[PatDataIDs]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [derived].[Patient_casenotes]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [derived].[TraffordDemFuzzy]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [derived].[TraffordDemIDs]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ETL].[PatientExclusions]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ETL].[tableInfo]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[ADMITDISCH]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[AEATTENDANCE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[ALLOCATEDCONTRACT]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[AMSUID]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[ANANINIT]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[APPTYP]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[AUGCAREPATDET]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[AUGPATCARELOC]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[BIRTH]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[BORROWER]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CASENLOC]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CASENOTEDETS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CASENOTELINK]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[casenotelinkentitytypes]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CASENOTELOAN]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CONSEPISDIAG]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CONSEPISODE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CONSEPISPROC]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CONSMAST]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CONTR]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CPFDISCHREASON]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[CRICAREPATDET]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[CRITICALCARENEONATE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[CURRENTPREADMISSIONS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[DELIVERY]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[DIARY]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[DISP]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[DOD]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[EPIREGISTRATION]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[EPIREGISTRATION_AfterLoad]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[EPIREGISTRATION_PreModel]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[ETHN]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[FCEEXT]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[FCEEXT_GC]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[FCEEXT_TEST]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[GDP]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[GP]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[GPHISTORY]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[HOSPDET]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[ICD10]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[INMAN]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[IPPATLETHISTORY]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[LEGALSTATUSDETS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[LOCN]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[MATCLINICDETS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[MIDNIGHTBEDSTATE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[MOA]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[MOD]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[NEONATE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OCCANCEL]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCCANCEL20150511]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMDUPREASONMF]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMFDEPT]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMFLOCATION]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMFRESULTS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMLOCNPERF]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMPTLINK]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMRESULTSOPT]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMSCREENTYPE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMSERVICE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMSTATUS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCMTEXTRESULTS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCORDER]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCORDERRSO]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCORDERRSOCOM]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCORDERRSONEW]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OCRESULT]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OPA]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPA1]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPAPPOINTATTEND]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPATTTRACK]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OPBOOKTYPEMF]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPCANCRSN]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPCS4]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPDIAGNOSIS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPDISCHARGE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPDOCTOR]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPPATLETHISTORY]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[OPPROCEDURE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPREFERRAL]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPREFSOURCE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPWLENTRY]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OPWLREMENTRY]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[OUTCLNC]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[PATDATA]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[PATDATAUPDATE]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[PMISPECIALREG]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[PMISPECIALREGMF]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[PREADMISSION]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[PREVADDRESS]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[PREVSURNAME]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[PROV]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[PURCHASER]    Script Date: 11/08/2015 15:59:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RELIGION]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RTTALLPRDSTS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RTTEPIPERIOD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RTTPERIOD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RTTPERIODCURR]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RTTPERSTSMF]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RTTPTEPIPATHWAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RTTPTPATHWAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[RTTPTPATHWAYCURRENT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[SGPATLETHISTORY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[SOAD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[SPEC]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[STAFFGP]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WAITCD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WARD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WARDATTENDER]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WARDATTSECPROC]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WARDSTAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WLACTIVITY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WLCURRENT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WLENTRY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Inquire].[WLRemoval]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Inquire].[WLSUSPENSION]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[ADMITDISCH]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[ALLOCATEDCONTRACT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[ANANINIT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[AUGCAREPATDET]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[BIRTH]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CASENLOC]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CASENOTEDETS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CASENOTELINK]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CASENOTELOAN]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CONSEPISDIAG]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CONSEPISODE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CONSEPISPROC]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CONSMAST]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CONTR]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CRICAREPATDET]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[CURRENTPREADMISSIONS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[DELIVERY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[DIARY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[DOD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[EPIREGISTRATION]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[ETHN]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[FCEEXT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[GP]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[GPHISTORY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[HOSPDET]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[ICD10]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[INMAN]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[LEGALSTATUSDETS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[MATCLINICDETS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[MIDNIGHTBEDSTATE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[MOA]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[MOD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[NEONATE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPA]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPAPPOINTATTEND]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPBOOKTYPEMF]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPCANCRSN]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPCS4]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPDIAGNOSIS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPDISCHARGE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPDOCTOR]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPPROCEDURE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPREFERRAL]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPREFSOURCE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPWLENTRY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OPWLREMENTRY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[OUTCLNC]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[PATDATA]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[PMISPECIALREG]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[PREVADDRESS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[PREVSURNAME]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[PURCHASER]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[RTTALLPRDSTS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[RTTEPIPERIOD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[RTTPERIOD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[RTTPERIODCURR]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[RTTPERSTSMF]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[RTTPTEPIPATHWAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[RTTPTPATHWAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[RTTPTPATHWAYCURRENT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[SOAD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[SPEC]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[STAFFGP]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[WARD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[WARDATTENDER]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[WARDATTSECPROC]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[WARDSTAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[WLACTIVITY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[WLCURRENT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[WLENTRY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireError].[WLSUSPENSION]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireUpdate].[ADMITDISCH]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [InquireUpdate].[AEATTENDANCE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [InquireUpdate].[CONSEPISODE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireUpdate].[OCMPTLINK]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [InquireUpdate].[OCRESULT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [InquireUpdate].[PATDATA]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [InquireUpdate].[PMISPECIALREG]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [InquireUpdate].[Spell]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [InquireUpdate].[WARDSTAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OCM].[OCRESULTGROUP]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OCM].[OCRESULTGROUPMEMBER]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [OCM].[OCSERVICEGROUP]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [OCM].[OCSERVICEGROUPMEMBERS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [TestPatient].[ADMITDISCH]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[ALLOCATEDCONTRACT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[ANANINIT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[AUGCAREPATDET]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[BIRTH]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[CASENOTEDETS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[CASENOTELINK]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[CASENOTELOAN]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[CONSEPISDIAG]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[CONSEPISODE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[CONSEPISPROC]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[CRICAREPATDET]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[CURRENTPREADMISSIONS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[DELIVERY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[EPIREGISTRATION]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[FCEEXT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[GPHISTORY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[LEGALSTATUSDETS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[MATCLINICDETS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[NEONATE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[OPA]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[OPAPPOINTATTEND]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[OPDIAGNOSIS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[OPDISCHARGE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[OPPROCEDURE]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[OPREFERRAL]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[OPWLENTRY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[OPWLREMENTRY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[PATDATA]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[PREVADDRESS]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[PREVSURNAME]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[RTTPERIOD]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[RTTPERIODCURR]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[RTTPTEPIPATHWAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[RTTPTPATHWAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[RTTPTPATHWAYCURRENT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[WARDATTENDER]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[WARDSTAY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[WLACTIVITY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[WLCURRENT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[WLENTRY]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [TestPatient].[WLSUSPENSION]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Utility].[AuditLog]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Utility].[AuditProcess]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Utility].[AuditProcessSource]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Utility].[Datafile]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Utility].[InquireStatistics]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Utility].[LogOPA]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Utility].[LogOPA1]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Utility].[Masterfile]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Utility].[Rowstats]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Utility].[SnapshotFCEEXT]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Utility].[SnapshotFCEEXT20140523]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Utility].[SnapshotFCEEXT20140523a]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Utility].[snapshotfceextBAK20141021]    Script Date: 11/08/2015 15:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

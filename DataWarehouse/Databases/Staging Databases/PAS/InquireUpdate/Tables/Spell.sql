﻿CREATE TABLE [InquireUpdate].[Spell](
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[Created] [datetime] NOT NULL,
	[ByWhom] [varchar](50) NULL,
 CONSTRAINT [PK_SPELL] PRIMARY KEY CLUSTERED 
(
	[InternalPatientNumber] ASC,
	[EpisodeNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [InquireUpdate].[Spell] ADD  DEFAULT (getdate()) FOR [Created]
GO
ALTER TABLE [InquireUpdate].[Spell] ADD  DEFAULT (suser_sname()) FOR [ByWhom]
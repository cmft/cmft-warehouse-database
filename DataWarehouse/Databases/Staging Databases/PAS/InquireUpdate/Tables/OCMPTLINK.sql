﻿CREATE TABLE [InquireUpdate].[OCMPTLINK](
	[InternalPatientNumber] [int] NOT NULL,
	[EpisodeNumber] [int] NOT NULL,
	[TransNo] [int] NOT NULL,
	[OcmTransKey] [int] NOT NULL,
	[OCMPTLINKID]  AS ((((CONVERT([varchar](10),[InternalPatientNumber],(0))+'||')+CONVERT([varchar](10),[EpisodeNumber],(0)))+'||')+CONVERT([varchar](10),[TransNo],(0))),
 CONSTRAINT [PK_Inquire_OCMPTLINK_InqKey] PRIMARY KEY CLUSTERED 
(
	[InternalPatientNumber] ASC,
	[EpisodeNumber] ASC,
	[TransNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
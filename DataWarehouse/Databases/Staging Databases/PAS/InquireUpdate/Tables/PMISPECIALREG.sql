﻿CREATE TABLE [InquireUpdate].[PMISPECIALREG](
	[PMISPECIALREGID] [varchar](254) NOT NULL,
	[ActivityDateTime] [varchar](12) NULL,
	[DateEntered] [varchar](10) NULL,
	[EpisodeNumber] [varchar](9) NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[SpecialRegister] [varchar](4) NOT NULL,
	[SpecialRegisterDescription] [varchar](30) NULL,
	[created] [datetime2](7) NULL,
	[updated] [datetime2](7) NULL,
	[byWhom] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[derived_Appointments](
	[In_Or_Out] [varchar](1) NOT NULL,
	[Yr] [int] NULL,
	[Mon] [int] NULL,
	[InternalPatientNumber] [varchar](20) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [TestPatient].[RTTPTPATHWAYCURRENT](
	[RTTPTPATHWAYCURRENTID] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[PathwayCondition] [nvarchar](20) NULL,
	[PathwayNumber] [nvarchar](25) NOT NULL,
	[PathwayOrgProv] [nvarchar](5) NULL,
	[RttCurProv] [nvarchar](4) NULL,
	[RttCurrentStartDate] [nvarchar](10) NULL,
	[RttCurrentStartDateInt] [nvarchar](12) NULL,
	[RttCurrentStatus] [nvarchar](4) NULL,
	[RttCurrentStatusDateTime] [nvarchar](255) NULL,
	[RttOsvStatus] [nvarchar](1) NULL,
	[RttPrivatePat] [nvarchar](1) NULL,
	[RttSpeciality] [nvarchar](4) NULL
) ON [PRIMARY]
﻿CREATE TABLE [TestPatient].[WLSUSPENSION](
	[WLSUSPENSIONID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[EpsActvDtimeInt] [nvarchar](12) NOT NULL,
	[Extended] [nvarchar](10) NULL,
	[Extension] [nvarchar](60) NULL,
	[ExtensionBy] [nvarchar](3) NULL,
	[InitiatedBy] [nvarchar](3) NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[LastRevisionDT] [nvarchar](255) NULL,
	[LastRevisionDTInt] [nvarchar](12) NULL,
	[LastRevisionUID] [nvarchar](3) NULL,
	[Reason] [nvarchar](30) NULL,
	[SuspendedUntil] [nvarchar](10) NULL,
	[SuspendedUntil_1] [nvarchar](10) NULL,
	[SuspensionStartDate] [nvarchar](12) NULL,
	[SuspReason] [nvarchar](4) NULL,
	[WlSuspensionExtensionRepeatGroupKey] [nvarchar](1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [TestPatient].[OPPROCEDURE](
	[OPPROCEDUREID] [nvarchar](255) NULL,
	[AppointmentStartDate] [nvarchar](14) NULL,
	[AppointmentStartTime] [nvarchar](10) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[OpAppointmentProcedureSequenceNumber] [nvarchar](255) NOT NULL,
	[Operation] [nvarchar](6) NULL,
	[PtApptStartDtimeInt] [nvarchar](12) NOT NULL,
	[ReadCodeRepeatGroupSecondaryProcedureCode] [nvarchar](8) NULL,
	[ResCode] [nvarchar](8) NOT NULL,
	[SecondaryProcGroup] [nvarchar](4) NULL
) ON [PRIMARY]
﻿CREATE TABLE [TestPatient].[NEONATE](
	[NEONATEID] [nvarchar](254) NULL,
	[ApgarScoreAt1Min] [nvarchar](2) NULL,
	[ApgarScoreAt5Mins] [nvarchar](2) NULL,
	[BirthOrder] [nvarchar](1) NULL,
	[DiagnosisCode1] [nvarchar](8) NULL,
	[DiagnosisCode2] [nvarchar](6) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[HeadCircumference] [nvarchar](2) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[KeMatBcgAdminInt] [nvarchar](1) NULL,
	[KeMatFeedingInt] [nvarchar](1) NULL,
	[KeMatFollUpCareInt] [nvarchar](1) NULL,
	[KeMatHipExamInt] [nvarchar](1) NULL,
	[KeMatJaundiceInt] [nvarchar](1) NULL,
	[KeMatMetabolicScInt] [nvarchar](1) NULL,
	[Length] [nvarchar](2) NULL,
	[PaedLenGestation] [nvarchar](2) NULL
) ON [PRIMARY]
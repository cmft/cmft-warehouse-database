﻿CREATE TABLE [TestPatient].[OPDISCHARGE](
	[OPDISCHARGEID] [nvarchar](255) NULL,
	[DischargeCode] [nvarchar](255) NULL,
	[DischargeDatetime] [nvarchar](16) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[OpDischargeDTimeInt] [nvarchar](12) NULL,
	[OpRegDtimeInt] [nvarchar](12) NOT NULL,
	[Reason] [nvarchar](30) NULL,
	[ReasonCode] [nvarchar](4) NULL
) ON [PRIMARY]
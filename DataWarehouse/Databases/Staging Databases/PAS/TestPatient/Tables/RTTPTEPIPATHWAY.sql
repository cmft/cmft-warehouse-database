﻿CREATE TABLE [TestPatient].[RTTPTEPIPATHWAY](
	[RTTPTEPIPATHWAYID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](9) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[PathwayNumber] [nvarchar](25) NULL,
	[PathwayOrgProv] [nvarchar](5) NULL
) ON [PRIMARY]
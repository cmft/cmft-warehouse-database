﻿CREATE TABLE [TestPatient].[OPDIAGNOSIS](
	[OPDIAGNOSISID] [nvarchar](255) NULL,
	[CurrentOpRegDate] [nvarchar](12) NULL,
	[Diagnosis] [nvarchar](8) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[OpEpisodeSecondaryDiagnosisSequenceNumber] [nvarchar](2) NOT NULL,
	[ReadCodeRepeatGroupSecondaryDiagnosisCode] [nvarchar](8) NULL
) ON [PRIMARY]
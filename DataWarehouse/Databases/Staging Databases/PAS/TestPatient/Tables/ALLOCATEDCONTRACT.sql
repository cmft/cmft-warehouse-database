﻿CREATE TABLE [TestPatient].[ALLOCATEDCONTRACT](
	[ALLOCATEDCONTRACTID] [nvarchar](255) NULL,
	[CmNeutralDttime] [nvarchar](12) NULL,
	[CmReverseDttime] [nvarchar](12) NOT NULL,
	[ContractId] [nvarchar](6) NULL,
	[ContractStartDate] [nvarchar](10) NULL,
	[ContractStartTime] [nvarchar](10) NULL,
	[EcrType] [nvarchar](1) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[LineNumber] [nvarchar](10) NULL,
	[PurchaserRef] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [InquireError].[SPEC](
	[SPECID] [nvarchar](255) NULL,
	[CmtGpfhExemptSpecialtyInt] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[DohCode] [nvarchar](255) NULL,
	[GpfhExempt] [nvarchar](255) NULL,
	[HaaCode] [nvarchar](255) NULL,
	[KarsIdCode] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[ScottishIntValue] [nvarchar](255) NULL,
	[ScottishLocalCode] [nvarchar](255) NULL,
	[SpecGroup] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[SpecialtyType] [nvarchar](255) NULL,
	[SupraregCode] [nvarchar](255) NULL,
	[TreatmentFunction] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[SPEC] ADD  CONSTRAINT [DF__SPEC__ErrorRaise__336AA144]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
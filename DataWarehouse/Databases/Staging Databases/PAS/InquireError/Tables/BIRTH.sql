﻿CREATE TABLE [InquireError].[BIRTH](
	[BIRTHID] [nvarchar](254) NULL,
	[BabyPatientNumber] [nvarchar](9) NULL,
	[BirthOrder] [nvarchar](1) NULL,
	[BirthWeight] [nvarchar](4) NULL,
	[CaseNoteNumber] [nvarchar](14) NULL,
	[Comments] [nvarchar](30) NULL,
	[DeliveryDate] [nvarchar](8) NULL,
	[DeliveryTime] [nvarchar](5) NULL,
	[DrugsUsed] [nvarchar](30) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[KeMatDrugsUsed2] [nvarchar](30) NULL,
	[KeMatDtimeDelivNeut] [nvarchar](12) NULL,
	[KeMatLiveStillIndInt] [nvarchar](1) NULL,
	[KeMatMethDelivInt] [nvarchar](2) NULL,
	[KeMatMorDrugsInt] [nvarchar](1) NULL,
	[KeMatMorPpInt] [nvarchar](1) NULL,
	[KeMatPresFetusInt] [nvarchar](1) NULL,
	[SuspCongAnomaly] [nvarchar](7) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[BIRTH] ADD  CONSTRAINT [DF__BIRTH__ErrorRais__4C0144E4]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
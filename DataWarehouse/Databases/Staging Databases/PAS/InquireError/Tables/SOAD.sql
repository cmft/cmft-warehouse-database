﻿CREATE TABLE [InquireError].[SOAD](
	[SOADID] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[FullDescription] [nvarchar](255) NULL,
	[HaaCode] [nvarchar](255) NULL,
	[InternalValue] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[ScottishIntValue] [nvarchar](255) NULL,
	[SourceOfAdm] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[SOAD] ADD  CONSTRAINT [DF__SOAD__ErrorRaise__345EC57D]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
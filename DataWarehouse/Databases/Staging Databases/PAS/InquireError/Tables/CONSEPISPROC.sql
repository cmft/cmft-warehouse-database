﻿CREATE TABLE [InquireError].[CONSEPISPROC](
	[CONSEPISPROCID] [nvarchar](255) NULL,
	[DoctorPerfOp] [nvarchar](255) NULL,
	[DrAdmAnaes] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[FCEEndDate] [nvarchar](255) NULL,
	[FCEEndDateTimeInt] [nvarchar](255) NULL,
	[FCEEndTime] [nvarchar](255) NULL,
	[FCENumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[OPCS] [nvarchar](255) NULL,
	[ProcedureDate] [nvarchar](255) NULL,
	[ProcedureDateInt] [nvarchar](255) NULL,
	[ReadCode] [nvarchar](255) NULL,
	[SecondaryProcSeqNo] [nvarchar](255) NULL,
	[SeqNo] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[CONSEPISPROC] ADD  CONSTRAINT [DF__CONSEPISP__Error__2704CA5F]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
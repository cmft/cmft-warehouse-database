﻿CREATE TABLE [InquireError].[RTTEPIPERIOD](
	[RTTEPIPERIODID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[PathwayNumber] [nvarchar](255) NULL,
	[PPeriodId] [nvarchar](255) NULL,
	[RttStartDateTimeInt] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[RTTEPIPERIOD] ADD  CONSTRAINT [DF__RTTEPIPER__Error__3B0BC30C]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
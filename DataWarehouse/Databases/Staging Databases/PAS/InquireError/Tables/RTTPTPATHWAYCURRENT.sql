﻿CREATE TABLE [InquireError].[RTTPTPATHWAYCURRENT](
	[RTTPTPATHWAYCURRENTID] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[PathwayCondition] [nvarchar](255) NULL,
	[PathwayNumber] [nvarchar](255) NULL,
	[PathwayOrgProv] [nvarchar](255) NULL,
	[RttCurProv] [nvarchar](255) NULL,
	[RttCurrentStartDate] [nvarchar](255) NULL,
	[RttCurrentStartDateInt] [nvarchar](255) NULL,
	[RttCurrentStatus] [nvarchar](255) NULL,
	[RttCurrentStatusDateTime] [nvarchar](255) NULL,
	[RttOsvStatus] [nvarchar](255) NULL,
	[RttPrivatePat] [nvarchar](255) NULL,
	[RttSpeciality] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[RTTPTPATHWAYCURRENT] ADD  CONSTRAINT [DF__RTTPTPATH__Error__3552E9B6]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
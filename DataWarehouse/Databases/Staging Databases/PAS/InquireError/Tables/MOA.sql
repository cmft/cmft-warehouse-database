﻿CREATE TABLE [InquireError].[MOA](
	[MOAID] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[FullDescription] [nvarchar](255) NULL,
	[HaaCode] [nvarchar](255) NULL,
	[InternalValue] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[ScottishIntValue] [nvarchar](255) NULL,
	[MethodOfAdmission] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[MOA] ADD  CONSTRAINT [DF__MOA__ErrorRaised__19AACF41]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
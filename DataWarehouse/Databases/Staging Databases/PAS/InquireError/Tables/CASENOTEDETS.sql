﻿CREATE TABLE [InquireError].[CASENOTEDETS](
	[CASENOTEDETSID] [nvarchar](255) NULL,
	[Allocated] [nvarchar](255) NULL,
	[Comment] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[Location] [nvarchar](255) NULL,
	[PtCasenoteIdPhyskey] [nvarchar](255) NULL,
	[PtCsNtLocDat] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL,
	[Withdrawn] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[CASENOTEDETS] ADD  CONSTRAINT [DF__CASENOTED__Error__4B422AD5]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[EPIREGISTRATION](
	[EPIREGISTRATIONID] [nvarchar](254) NULL,
	[Consultant] [nvarchar](6) NULL,
	[Epiadd1] [nvarchar](20) NULL,
	[Epiadd2] [nvarchar](20) NULL,
	[Epiadd3] [nvarchar](20) NULL,
	[Epiadd4] [nvarchar](20) NULL,
	[Epidor] [nvarchar](3) NULL,
	[EpiGDP] [nvarchar](8) NULL,
	[EpiGP] [nvarchar](8) NULL,
	[Epipc] [nvarchar](8) NULL,
	[EpisodeNumber] [nvarchar](9) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[Reason] [nvarchar](33) NULL,
	[ReferredBy] [nvarchar](3) NULL,
	[Tempadd1] [nvarchar](20) NULL,
	[Tmpadd2] [nvarchar](20) NULL,
	[Tmpadd3] [nvarchar](20) NULL,
	[Tmpadd4] [nvarchar](20) NULL,
	[Tmpdor] [nvarchar](3) NULL,
	[Tmppostcode] [nvarchar](8) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[EPIREGISTRATION] ADD  CONSTRAINT [DF__EPIREGIST__Error__46486B8E]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
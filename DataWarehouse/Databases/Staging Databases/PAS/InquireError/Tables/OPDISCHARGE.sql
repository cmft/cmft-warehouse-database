﻿CREATE TABLE [InquireError].[OPDISCHARGE](
	[OPDISCHARGEID] [nvarchar](255) NULL,
	[DischargeCode] [nvarchar](255) NULL,
	[DischargeDatetime] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[OpDischargeDTimeInt] [nvarchar](255) NULL,
	[OpRegDtimeInt] [nvarchar](255) NULL,
	[Reason] [nvarchar](255) NULL,
	[ReasonCode] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPDISCHARGE] ADD  CONSTRAINT [DF__OPDISCHAR__Error__43A1090D]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[HOSPDET](
	[HOSPDETID] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[DeliveryFacilities] [nvarchar](255) NULL,
	[HaaCode] [nvarchar](255) NULL,
	[HospAddrLine2] [nvarchar](255) NULL,
	[HospAddrLine3] [nvarchar](255) NULL,
	[HospAddrLine4] [nvarchar](255) NULL,
	[HospitalCode] [nvarchar](255) NULL,
	[HospitalName] [nvarchar](255) NULL,
	[KornerSiteType] [nvarchar](255) NULL,
	[Location] [nvarchar](255) NULL,
	[MainSpecialty1] [nvarchar](255) NULL,
	[MainSpecialty2] [nvarchar](255) NULL,
	[MainSpecialty3] [nvarchar](255) NULL,
	[MainSpecialty4] [nvarchar](255) NULL,
	[MaternityTransactionsRequired] [nvarchar](255) NULL,
	[PhoneNo] [nvarchar](255) NULL,
	[Postcode] [nvarchar](255) NULL,
	[ProviderCode] [nvarchar](255) NULL,
	[TreatmentSiteCd] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[HOSPDET] ADD  CONSTRAINT [DF__HOSPDET__ErrorRa__1D7B6025]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
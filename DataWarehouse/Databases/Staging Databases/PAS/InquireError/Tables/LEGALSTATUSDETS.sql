﻿CREATE TABLE [InquireError].[LEGALSTATUSDETS](
	[LEGALSTATUSDETSID] [nvarchar](254) NULL,
	[EpisodeNumber] [nvarchar](9) NOT NULL,
	[InternalPatientNumber] [nvarchar](9) NOT NULL,
	[KeLegalStatChangeDtimeInt] [nvarchar](12) NOT NULL,
	[LegalStatus] [nvarchar](2) NULL,
	[MentalCategory] [nvarchar](1) NULL,
	[PsycPtSts] [nvarchar](1) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorRaised] [datetime2](7) NULL,
 CONSTRAINT [PK_LEGALSTATUSDETS] PRIMARY KEY CLUSTERED 
(
	[EpisodeNumber] ASC,
	[InternalPatientNumber] ASC,
	[KeLegalStatChangeDtimeInt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[LEGALSTATUSDETS] ADD  CONSTRAINT [DF_LEGALSTATUSDETS_ErrorRaised]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
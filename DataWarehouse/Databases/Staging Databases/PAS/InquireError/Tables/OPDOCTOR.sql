﻿CREATE TABLE [InquireError].[OPDOCTOR](
	[OPDOCTORID] [nvarchar](255) NULL,
	[AllowOverbooking] [nvarchar](255) NULL,
	[ClinicHospCode] [nvarchar](255) NULL,
	[Comment] [nvarchar](255) NULL,
	[GmcGdcHcp] [nvarchar](255) NULL,
	[GradeOfStaff] [nvarchar](255) NULL,
	[LocCode] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[SchDays] [nvarchar](255) NULL,
	[SchedResMfCd] [nvarchar](255) NULL,
	[ServiceGroup] [nvarchar](255) NULL,
	[TypeOfDoctor] [nvarchar](255) NULL,
	[ValidApptTypes] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPDOCTOR] ADD  CONSTRAINT [DF__OPDOCTOR__ErrorR__42ACE4D4]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
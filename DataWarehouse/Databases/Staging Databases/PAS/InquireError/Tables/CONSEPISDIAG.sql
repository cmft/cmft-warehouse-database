﻿CREATE TABLE [InquireError].[CONSEPISDIAG](
	[CONSEPISDIAGID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[FCEEndDate] [nvarchar](255) NULL,
	[FCEEndDateTime] [nvarchar](255) NULL,
	[FCEEndTime] [nvarchar](255) NULL,
	[FCENumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[ReadCode] [nvarchar](255) NULL,
	[SecondaryDiagnosis] [nvarchar](255) NULL,
	[SecondaryDiagSeqNo] [nvarchar](255) NULL,
	[SeqNo] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[CONSEPISDIAG] ADD  CONSTRAINT [DF__CONSEPISD__Error__28ED12D1]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
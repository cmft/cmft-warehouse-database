﻿CREATE TABLE [InquireError].[CASENOTELINK](
	[CASENOTELINKID] [nvarchar](254) NULL,
	[EntityType] [nvarchar](25) NULL,
	[EntityTypeKey] [nvarchar](2) NULL,
	[ExternalNumber] [nvarchar](20) NULL,
	[ExternalNumberKey] [nvarchar](20) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[CASENOTELINK] ADD  CONSTRAINT [DF__CASENOTEL__Error__59904A2C]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
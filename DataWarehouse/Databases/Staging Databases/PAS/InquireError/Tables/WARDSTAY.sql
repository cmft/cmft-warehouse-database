﻿CREATE TABLE [InquireError].[WARDSTAY](
	[WARDSTAYID] [nvarchar](255) NULL,
	[AdmitDate] [nvarchar](255) NULL,
	[AdmitTime] [nvarchar](255) NULL,
	[ConsultantCode] [nvarchar](255) NULL,
	[EndActivity] [nvarchar](255) NULL,
	[EndDate] [nvarchar](255) NULL,
	[EndDtTimeInt] [nvarchar](255) NULL,
	[EndTime] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[EpsActvDtimeInt] [nvarchar](255) NULL,
	[HospitalCode] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[IpAdmDtimeInt] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[StartActivity] [nvarchar](255) NULL,
	[StartDate] [nvarchar](255) NULL,
	[StartTime] [nvarchar](255) NULL,
	[WardCode] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[WARDSTAY] ADD  CONSTRAINT [DF__WARDSTAY__ErrorR__2EA5EC27]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
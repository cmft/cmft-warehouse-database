﻿CREATE TABLE [InquireError].[OPA](
	[OPAID] [nvarchar](255) NULL,
	[A2ndOfferDate] [nvarchar](255) NULL,
	[A2ndOfferDateInt] [nvarchar](255) NULL,
	[ApplicationIndicator] [nvarchar](255) NULL,
	[ApptBookedDate] [nvarchar](255) NULL,
	[ApptBookedTime] [nvarchar](255) NULL,
	[ApptCancDate] [nvarchar](255) NULL,
	[ApptCancDTimeInt] [nvarchar](255) NULL,
	[ApptCancTime] [nvarchar](255) NULL,
	[ApptCategory] [nvarchar](255) NULL,
	[ApptClass] [nvarchar](255) NULL,
	[ApptComment] [nvarchar](255) NULL,
	[ApptConractId] [nvarchar](255) NULL,
	[ApptDate] [nvarchar](255) NULL,
	[ApptEndTime] [nvarchar](255) NULL,
	[ApptEnteredDtime] [nvarchar](255) NULL,
	[ApptEnteredUID] [nvarchar](255) NULL,
	[ApptPrimaryProcedureCode] [nvarchar](255) NULL,
	[ApptPurchaser] [nvarchar](255) NULL,
	[ApptPurchRef] [nvarchar](255) NULL,
	[ApptStatus] [nvarchar](255) NULL,
	[ApptTime] [nvarchar](255) NULL,
	[ApptType] [nvarchar](255) NULL,
	[AttPriDiagCode] [nvarchar](255) NULL,
	[AttSubDiagCode] [nvarchar](255) NULL,
	[BookFromWL] [nvarchar](255) NULL,
	[BookingType] [nvarchar](255) NULL,
	[BreachDate] [nvarchar](255) NULL,
	[BreachDateInt] [nvarchar](255) NULL,
	[BreachReasonCmnt] [nvarchar](255) NULL,
	[BreachReasonCode] [nvarchar](255) NULL,
	[BreachReasonDesc] [nvarchar](255) NULL,
	[BreachRecDtime] [nvarchar](255) NULL,
	[BreachRecDtimeInt] [nvarchar](255) NULL,
	[CABServiceCode] [nvarchar](255) NULL,
	[CABServiceDesc] [nvarchar](255) NULL,
	[CancelBy] [nvarchar](255) NULL,
	[CancelComment] [nvarchar](255) NULL,
	[CaseNoteNumber] [nvarchar](255) NULL,
	[ClinicCode] [nvarchar](255) NULL,
	[ClinicConsultant] [nvarchar](255) NULL,
	[ClinicSpecialty] [nvarchar](255) NULL,
	[DateApptBookedInt] [nvarchar](255) NULL,
	[DischDate] [nvarchar](255) NULL,
	[DischTime] [nvarchar](255) NULL,
	[Disposal] [nvarchar](255) NULL,
	[DistrictNumber] [nvarchar](255) NULL,
	[Dob] [nvarchar](255) NULL,
	[EbookingDNAReason] [nvarchar](255) NULL,
	[EbookingDNAReasonDesc] [nvarchar](255) NULL,
	[EbookingReferenceNumber] [nvarchar](255) NULL,
	[EpiGp] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[Forenames] [nvarchar](255) NULL,
	[GdpCode] [nvarchar](255) NULL,
	[HaCode] [nvarchar](255) NULL,
	[HRGCode] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[Interpreter] [nvarchar](255) NULL,
	[InterpreterLanguage] [nvarchar](255) NULL,
	[LastRevisionDtime] [nvarchar](255) NULL,
	[LastRevisionUID] [nvarchar](255) NULL,
	[NhsNumber] [nvarchar](255) NULL,
	[OfferAccepted] [nvarchar](255) NULL,
	[OpDischargeDTimeInt] [nvarchar](255) NULL,
	[OpEROD] [nvarchar](255) NULL,
	[OpERODInt] [nvarchar](255) NULL,
	[OsvStatus] [nvarchar](255) NULL,
	[PatChoice] [nvarchar](255) NULL,
	[Postcode] [nvarchar](255) NULL,
	[PracticeCode] [nvarchar](255) NULL,
	[PrimaryProcGroup] [nvarchar](255) NULL,
	[PtApptStartDtimeInt] [nvarchar](255) NULL,
	[PtCategory] [nvarchar](255) NULL,
	[PtVerticalApptInd] [nvarchar](255) NULL,
	[ReadPrimary] [nvarchar](255) NULL,
	[ReasonableOffer] [nvarchar](255) NULL,
	[ReasonForCanc] [nvarchar](255) NULL,
	[ReasonForRef] [nvarchar](255) NULL,
	[RefBy] [nvarchar](255) NULL,
	[RefConsultant] [nvarchar](255) NULL,
	[ReferralContractId] [nvarchar](255) NULL,
	[ReferralDate] [nvarchar](255) NULL,
	[ReferralDateInt] [nvarchar](255) NULL,
	[ReferralPurchaser] [nvarchar](255) NULL,
	[ReferralTime] [nvarchar](255) NULL,
	[RefPrimaryDiagnosisCode] [nvarchar](255) NULL,
	[RefPriority] [nvarchar](255) NULL,
	[RefSpecialty] [nvarchar](255) NULL,
	[RefSubsidDiag] [nvarchar](255) NULL,
	[RegGpCode] [nvarchar](255) NULL,
	[ResCode] [nvarchar](255) NULL,
	[RttPeriodStatus] [nvarchar](255) NULL,
	[Sex] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[Transport] [nvarchar](255) NULL,
	[UniqueServiceID] [nvarchar](255) NULL,
	[WaitingGuaranteeException] [nvarchar](255) NULL,
	[WaitingGuaranteeExceptionDesc] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPA] ADD  CONSTRAINT [DF__OPA__ErrorRaised__17C286CF]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
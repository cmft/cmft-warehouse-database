﻿CREATE TABLE [InquireError].[STAFFGP](
	[STAFFGPID] [nvarchar](255) NULL,
	[GradeOfStaffCode] [nvarchar](255) NULL,
	[GradeOfStaffDescription] [nvarchar](255) NULL,
	[MedicalStaffType] [nvarchar](255) NULL,
	[MedicalStaffTypeDescription] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL
) ON [PRIMARY]
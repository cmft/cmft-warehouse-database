﻿CREATE TABLE [InquireError].[OPCS4](
	[OPCS4ID] [nvarchar](255) NULL,
	[Opcs4Dx3] [nvarchar](255) NULL,
	[Opcs4Dx4] [nvarchar](255) NULL,
	[Opcs4GpfhChargeable] [nvarchar](255) NULL,
	[Opcs4GpfhEffDate] [nvarchar](255) NULL,
	[Opcs4Groupcode] [nvarchar](255) NULL,
	[Opcs4Ldf] [nvarchar](255) NULL,
	[Opcs4Procpricegroup] [nvarchar](255) NULL,
	[RevisionFourOperationCode] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPCS4] ADD  CONSTRAINT [DF__OPCS4__ErrorRais__4589517F]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
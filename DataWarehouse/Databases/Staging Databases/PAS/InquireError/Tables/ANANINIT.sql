﻿CREATE TABLE [InquireError].[ANANINIT](
	[ANANINITID] [nvarchar](254) NULL,
	[AnaesanalgDuring] [nvarchar](2) NULL,
	[Aneadurlab] [nvarchar](2) NULL,
	[Aneadurlabreas] [nvarchar](2) NULL,
	[Aneapostlab] [nvarchar](1) NULL,
	[Aneapostlabreas] [nvarchar](1) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[KeMatPerAnaesAdminInt] [nvarchar](1) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[ANANINIT] ADD  CONSTRAINT [DF__ANANINIT__ErrorR__4B0D20AB]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
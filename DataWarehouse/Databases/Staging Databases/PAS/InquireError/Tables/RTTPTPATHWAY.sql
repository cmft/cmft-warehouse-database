﻿CREATE TABLE [InquireError].[RTTPTPATHWAY](
	[RTTPTPATHWAYID] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[PathwayCondition] [nvarchar](255) NULL,
	[PathwayNumber] [nvarchar](255) NULL,
	[PathwayOrgProv] [nvarchar](255) NULL,
	[RttCurProv] [nvarchar](255) NULL,
	[RttCurrentStatus] [nvarchar](255) NULL,
	[RttCurrentStatusDateTime] [nvarchar](255) NULL,
	[RttOsvStatus] [nvarchar](255) NULL,
	[RttPrivatePat] [nvarchar](255) NULL,
	[RttSpeciality] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[RTTPTPATHWAY] ADD  CONSTRAINT [DF__RTTPTPATH__Error__36470DEF]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
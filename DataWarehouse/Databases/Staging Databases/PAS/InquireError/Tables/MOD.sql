﻿CREATE TABLE [InquireError].[MOD](
	[MODID] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[FullDescription] [nvarchar](255) NULL,
	[HaaCode] [nvarchar](255) NULL,
	[InternalValue] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[ScottishIntValue] [nvarchar](255) NULL,
	[MethodOfDischarge] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[MOD] ADD  CONSTRAINT [DF__MOD__ErrorRaised__18B6AB08]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
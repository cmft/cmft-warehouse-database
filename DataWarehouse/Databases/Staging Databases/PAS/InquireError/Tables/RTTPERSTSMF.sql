﻿CREATE TABLE [InquireError].[RTTPERSTSMF](
	[RTTPERSTSMFID] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[RttStatusCode] [nvarchar](255) NULL,
	[RttStsDefault] [nvarchar](255) NULL,
	[RttStsDescription] [nvarchar](255) NULL,
	[RttStsInternalValue] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[RTTPERSTSMF] ADD  CONSTRAINT [DF__RTTPERSTS__Error__382F5661]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[OPDIAGNOSIS](
	[OPDIAGNOSISID] [nvarchar](255) NULL,
	[CurrentOpRegDate] [nvarchar](255) NULL,
	[Diagnosis] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[OpEpisodeSecondaryDiagnosisSequenceNumber] [nvarchar](255) NULL,
	[ReadCodeRepeatGroupSecondaryDiagnosisCode] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPDIAGNOSIS] ADD  CONSTRAINT [DF__OPDIAGNOS__Error__44952D46]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
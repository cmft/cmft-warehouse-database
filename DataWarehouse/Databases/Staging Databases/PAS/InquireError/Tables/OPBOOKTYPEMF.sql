﻿CREATE TABLE [InquireError].[OPBOOKTYPEMF](
	[OPBOOKTYPEMFID] [nvarchar](254) NULL,
	[Code] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[InternalValue] [nvarchar](255) NULL,
	[InternalValueDescription] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[SchedulingApplicationAreaExternal] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPBOOKTYPEMF] ADD  CONSTRAINT [DF__OPBOOKTYP__Error__477199F1]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
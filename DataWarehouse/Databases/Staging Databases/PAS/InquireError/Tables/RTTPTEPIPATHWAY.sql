﻿CREATE TABLE [InquireError].[RTTPTEPIPATHWAY](
	[RTTPTEPIPATHWAYID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[PathwayNumber] [nvarchar](255) NULL,
	[PathwayOrgProv] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[RTTPTEPIPATHWAY] ADD  CONSTRAINT [DF__RTTPTEPIP__Error__373B3228]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
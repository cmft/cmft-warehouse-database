﻿CREATE TABLE [InquireError].[PREVADDRESS](
	[PREVADDRESSID] [nvarchar](254) NULL,
	[DistrictNumber] [nvarchar](14) NULL,
	[EffectiveFromDate] [nvarchar](10) NULL,
	[EffectiveFromDateInt] [nvarchar](10) NULL,
	[EffectiveToDate] [nvarchar](10) NULL,
	[EffectiveToDateInt] [nvarchar](8) NULL,
	[HaCode] [nvarchar](3) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[Postcode] [nvarchar](8) NULL,
	[PseudoPostCode] [nvarchar](8) NULL,
	[PtAddLn1] [nvarchar](20) NULL,
	[PtAddLn2] [nvarchar](20) NULL,
	[PtAddLn3] [nvarchar](20) NULL,
	[PtAddLn4] [nvarchar](20) NULL,
	[SeqNo] [nvarchar](3) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[PREVADDRESS] ADD  CONSTRAINT [DF__PREVADDRE__Error__4830B400]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[WLACTIVITY](
	[WLACTIVITYID] [nvarchar](255) NULL,
	[Activity] [nvarchar](255) NULL,
	[ActivityDate] [nvarchar](255) NULL,
	[ActivityTime] [nvarchar](255) NULL,
	[AdmEROD] [nvarchar](255) NULL,
	[AdmERODInt] [nvarchar](255) NULL,
	[BookingType] [nvarchar](255) NULL,
	[CancelledBy] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[CharterCancelCode] [nvarchar](255) NULL,
	[CharterCancelDeferInd] [nvarchar](255) NULL,
	[CharterCancelDescription] [nvarchar](255) NULL,
	[Consultant] [nvarchar](255) NULL,
	[DefEndDate] [nvarchar](255) NULL,
	[DefEndDateInt] [nvarchar](255) NULL,
	[DefRevisedEndDate] [nvarchar](255) NULL,
	[DefRevisedEndDateInt] [nvarchar](255) NULL,
	[DiagGroup] [nvarchar](255) NULL,
	[EfgCode] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[EpsActvDtimeInt] [nvarchar](255) NULL,
	[EpsActvTypeInt] [nvarchar](255) NULL,
	[FirstPbLetterSent] [nvarchar](255) NULL,
	[FirstPbReminderSent] [nvarchar](255) NULL,
	[Hospital] [nvarchar](255) NULL,
	[HrgRankedProcedureCode] [nvarchar](255) NULL,
	[HrgResourceGroup] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[LastRevisionDtime] [nvarchar](255) NULL,
	[LastRevisionUID] [nvarchar](255) NULL,
	[OpCancelled] [nvarchar](255) NULL,
	[OpCancelledDesc] [nvarchar](255) NULL,
	[PatientChoice] [nvarchar](255) NULL,
	[PatientChoiceDesc] [nvarchar](255) NULL,
	[PatSelfDeferral] [nvarchar](255) NULL,
	[PatSelfDeferralInt] [nvarchar](255) NULL,
	[PbResponseDate] [nvarchar](255) NULL,
	[PrevActivity] [nvarchar](255) NULL,
	[PrevActivityDate] [nvarchar](255) NULL,
	[PrevActivityDTimeInt] [nvarchar](255) NULL,
	[PrevActivityInt] [nvarchar](255) NULL,
	[PrevActivityTime] [nvarchar](255) NULL,
	[PrevConsultant] [nvarchar](255) NULL,
	[PrevDiagGroup] [nvarchar](255) NULL,
	[PrevHospital] [nvarchar](255) NULL,
	[PrevSpecialty] [nvarchar](255) NULL,
	[PrevWaitingList] [nvarchar](255) NULL,
	[Reason] [nvarchar](255) NULL,
	[Remark] [nvarchar](255) NULL,
	[RemovalComment] [nvarchar](255) NULL,
	[RemovalReason] [nvarchar](255) NULL,
	[SecondPbReminderSent] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[SuspReason] [nvarchar](255) NULL,
	[TCIAcceptDate] [nvarchar](255) NULL,
	[TCIAcceptDateInt] [nvarchar](255) NULL,
	[TCIDate] [nvarchar](255) NULL,
	[TCIDTimeInt] [nvarchar](255) NULL,
	[TCILetterComment] [nvarchar](255) NULL,
	[TCILetterDate] [nvarchar](255) NULL,
	[TCIOfferDateInt] [nvarchar](255) NULL,
	[TCITime] [nvarchar](255) NULL,
	[Urgency] [nvarchar](255) NULL,
	[WaitingList] [nvarchar](255) NULL,
	[Ward] [nvarchar](255) NULL,
	[WlSuspensionInitiator] [nvarchar](255) NULL,
	[WLSuspensionInitiatorDesc] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[WLACTIVITY] ADD  CONSTRAINT [DF__WLACTIVIT__Error__2DB1C7EE]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
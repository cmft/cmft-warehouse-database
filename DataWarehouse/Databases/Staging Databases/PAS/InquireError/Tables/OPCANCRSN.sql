﻿CREATE TABLE [InquireError].[OPCANCRSN](
	[OPCANCRSNID] [nvarchar](255) NULL,
	[CancellationCode] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[EbsInboundCancReason] [nvarchar](255) NULL,
	[EbsInboundCancReasonInt] [nvarchar](255) NULL,
	[EbsReasonCode] [nvarchar](255) NULL,
	[EbsReasonDesc] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPCANCRSN] ADD  CONSTRAINT [DF__OPCANCRSN__Error__467D75B8]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[RTTPERIODCURR](
	[RTTPERIODCURRID] [nvarchar](255) NULL,
	[Condition] [nvarchar](255) NULL,
	[CurrCategory] [nvarchar](255) NULL,
	[CurrCons] [nvarchar](255) NULL,
	[CurrentStatus] [nvarchar](255) NULL,
	[CurrentStatusDt] [nvarchar](255) NULL,
	[CurrentStatusDtInt] [nvarchar](255) NULL,
	[CurrentStatusInt] [nvarchar](255) NULL,
	[CurrOSV] [nvarchar](255) NULL,
	[CurrProv] [nvarchar](255) NULL,
	[CurrSpec] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[PathwayNumber] [nvarchar](255) NULL,
	[PauseDate] [nvarchar](255) NULL,
	[PauseDtInt] [nvarchar](255) NULL,
	[PPeriodId] [nvarchar](255) NULL,
	[RttStartDate] [nvarchar](255) NULL,
	[RttStartDateTimeInt] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[RTTPERIODCURR] ADD  CONSTRAINT [DF__RTTPERIOD__Error__39237A9A]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
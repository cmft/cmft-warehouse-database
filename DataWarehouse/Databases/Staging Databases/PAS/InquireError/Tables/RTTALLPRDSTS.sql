﻿CREATE TABLE [InquireError].[RTTALLPRDSTS](
	[RTTALLPRDSTSID] [nvarchar](255) NULL,
	[ActivityDateTime] [nvarchar](255) NULL,
	[ActivityDateTimeInt] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[PathwayNumber] [nvarchar](255) NULL,
	[PRDStsType] [nvarchar](255) NULL,
	[RttPRDSts] [nvarchar](255) NULL,
	[RttPRDStsInt] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[RTTALLPRDSTS] ADD  CONSTRAINT [DF__RTTALLPRD__Error__3BFFE745]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[CASENLOC](
	[CASENLOCID] [nvarchar](255) NULL,
	[Location] [nvarchar](4) NULL,
	[LocationName] [nvarchar](15) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[CASENLOC] ADD  CONSTRAINT [DF__CASENLOC__ErrorR__4E1E9780]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
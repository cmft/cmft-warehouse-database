﻿CREATE TABLE [InquireError].[OPREFSOURCE](
	[OPREFSOURCEID] [nvarchar](255) NULL,
	[Applications] [nvarchar](255) NULL,
	[CmdsValue] [nvarchar](255) NULL,
	[CollectGpRefStatsInternal] [nvarchar](255) NULL,
	[CollectGpReferralStatisticsWithThisCode] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[InternalValue] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[Modules] [nvarchar](255) NULL,
	[ReferredBy] [nvarchar](255) NULL,
	[ScottishIntValue] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPREFSOURCE] ADD  CONSTRAINT [DF__OPREFSOUR__Error__3FD07829]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[PREVSURNAME](
	[PREVSURNAMEID] [nvarchar](254) NULL,
	[DistrictNumber] [nvarchar](14) NULL,
	[Forenames] [nvarchar](20) NULL,
	[InternalDateOfBirth] [nvarchar](8) NULL,
	[InternalPatientNumber] [nvarchar](9) NULL,
	[PtPrevSexInt] [nvarchar](1) NULL,
	[SeqNo] [nvarchar](30) NULL,
	[Surname] [nvarchar](24) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[PREVSURNAME] ADD  CONSTRAINT [DF__PREVSURNA__Error__473C8FC7]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
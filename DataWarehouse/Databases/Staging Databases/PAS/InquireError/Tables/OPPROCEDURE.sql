﻿CREATE TABLE [InquireError].[OPPROCEDURE](
	[OPPROCEDUREID] [nvarchar](255) NULL,
	[AppointmentStartDate] [nvarchar](255) NULL,
	[AppointmentStartTime] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[OpAppointmentProcedureSequenceNumber] [nvarchar](255) NULL,
	[Operation] [nvarchar](255) NULL,
	[PtApptStartDtimeInt] [nvarchar](255) NULL,
	[ReadCodeRepeatGroupSecondaryProcedureCode] [nvarchar](255) NULL,
	[ResCode] [nvarchar](255) NULL,
	[SecondaryProcGroup] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPPROCEDURE] ADD  CONSTRAINT [DF__OPPROCEDU__Error__41B8C09B]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
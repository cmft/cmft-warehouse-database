﻿CREATE TABLE [InquireError].[MIDNIGHTBEDSTATE](
	[MIDNIGHTBEDSTATEID] [nvarchar](255) NULL,
	[ActivityIn] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Consultant] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[HospitalCode] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[IpAdmDtimeInt] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[StatisticsDate] [nvarchar](255) NULL,
	[Ward] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[MIDNIGHTBEDSTATE] ADD  CONSTRAINT [DF__MIDNIGHTB__Error__1A9EF37A]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[WARDATTSECPROC](
	[WARDATTSECPROCID] [nvarchar](255) NULL,
	[AttendanceDate] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[ReadCodeRepeatGroupSecondaryProcedureCode] [nvarchar](255) NULL,
	[SecondaryProcCode] [nvarchar](255) NULL,
	[SecondaryProcGroup] [nvarchar](255) NULL,
	[SeqNo] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[WARDATTSECPROC] ADD  CONSTRAINT [DF__WARDATTSE__Error__2F9A1060]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
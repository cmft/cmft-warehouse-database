﻿CREATE TABLE [InquireError].[MATCLINICDETS](
	[MATCLINICDETSID] [nvarchar](254) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[IsThePatientKnownToBeImmune] [nvarchar](3) NULL,
	[KeMatRubellaImmInt] [nvarchar](1) NULL,
	[LeadProfessionalType] [nvarchar](12) NULL,
	[MaternalHeight] [nvarchar](3) NULL,
	[NoOfNeonatalDeaths] [nvarchar](2) NULL,
	[NoOfNoninducedAbortions] [nvarchar](2) NULL,
	[NoOfPreviousCaesareanSections] [nvarchar](2) NULL,
	[NoOfPreviousInducedAbortions] [nvarchar](2) NULL,
	[NoOfRegistrableLiveBirths] [nvarchar](2) NULL,
	[NoOfRegistrableStillBirths] [nvarchar](2) NULL,
	[PatientImmunised] [nvarchar](3) NULL,
	[PreviousTransfusion] [nvarchar](3) NULL,
	[WasAntibodyStatusTested] [nvarchar](3) NULL,
	[WasTheResultPositive] [nvarchar](3) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[MATCLINICDETS] ADD  CONSTRAINT [DF__MATCLINIC__Error__4A18FC72]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
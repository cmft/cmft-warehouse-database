﻿CREATE TABLE [InquireError].[INMAN](
	[INMANID] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[HaaCode] [nvarchar](255) NULL,
	[IntdMgmt] [nvarchar](255) NULL,
	[InternalValue] [nvarchar](255) NULL,
	[IntMgmtCode] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[ScottishIntValue] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[INMAN] ADD  CONSTRAINT [DF__INMAN__ErrorRais__1B9317B3]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
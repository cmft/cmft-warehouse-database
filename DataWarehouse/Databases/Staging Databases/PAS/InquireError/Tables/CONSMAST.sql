﻿CREATE TABLE [InquireError].[CONSMAST](
	[CONSMASTID] [nvarchar](255) NULL,
	[Consultant] [nvarchar](255) NULL,
	[ConsultantCode2] [nvarchar](255) NULL,
	[ContractedSpecialty] [nvarchar](255) NULL,
	[Deleted] [nvarchar](255) NULL,
	[ExternalCons] [nvarchar](255) NULL,
	[ForenamePfx] [nvarchar](255) NULL,
	[GMCCode] [nvarchar](255) NULL,
	[Inits] [nvarchar](255) NULL,
	[KornerCode] [nvarchar](255) NULL,
	[Midwife] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[PrimarySpecialty] [nvarchar](255) NULL,
	[ProviderCode] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[CONSMAST] ADD  CONSTRAINT [DF__CONSMAST__ErrorR__2610A626]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
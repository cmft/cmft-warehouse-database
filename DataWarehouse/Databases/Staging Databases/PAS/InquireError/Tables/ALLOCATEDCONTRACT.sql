﻿CREATE TABLE [InquireError].[ALLOCATEDCONTRACT](
	[ALLOCATEDCONTRACTID] [nvarchar](255) NULL,
	[CmNeutralDttime] [nvarchar](255) NULL,
	[CmReverseDttime] [nvarchar](255) NULL,
	[ContractId] [nvarchar](255) NULL,
	[ContractStartDate] [nvarchar](255) NULL,
	[ContractStartTime] [nvarchar](255) NULL,
	[EcrType] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[LineNumber] [nvarchar](255) NULL,
	[PurchaserRef] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[ALLOCATEDCONTRACT] ADD  CONSTRAINT [DF__ALLOCATED__Error__29E1370A]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
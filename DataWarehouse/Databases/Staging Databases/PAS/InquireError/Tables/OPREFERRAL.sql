﻿CREATE TABLE [InquireError].[OPREFERRAL](
	[OPREFERRALID] [nvarchar](255) NULL,
	[AppointmentWithinTargetDays] [nvarchar](255) NULL,
	[CaseNoteNumber] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[Comment] [nvarchar](255) NULL,
	[ConsCode] [nvarchar](255) NULL,
	[CurrentStatus] [nvarchar](255) NULL,
	[CurrentStatusDescription] [nvarchar](255) NULL,
	[DateOfPrimaryDiagnosis] [nvarchar](255) NULL,
	[DecisionToRefer] [nvarchar](255) NULL,
	[DiagnosisStatus] [nvarchar](255) NULL,
	[DischargeDt] [nvarchar](255) NULL,
	[DischargeTm] [nvarchar](255) NULL,
	[DistrictNumber] [nvarchar](255) NULL,
	[EpiGDPCode] [nvarchar](255) NULL,
	[EpiGPCode] [nvarchar](255) NULL,
	[EpiGPPracticeCode] [nvarchar](255) NULL,
	[EpiReferringCon] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[GpDiagnosis] [nvarchar](255) NULL,
	[GPReferralLetterNumber] [nvarchar](255) NULL,
	[HospitalCode] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[KornerEpisodePrimaryDiagnosisCode] [nvarchar](255) NULL,
	[OpRegDtimeInt] [nvarchar](255) NULL,
	[OsvExempt] [nvarchar](255) NULL,
	[OsvStatus] [nvarchar](255) NULL,
	[PriorityType] [nvarchar](255) NULL,
	[QM08EndWtDate] [nvarchar](255) NULL,
	[QM08StartWaitDate] [nvarchar](255) NULL,
	[ReadPrimary] [nvarchar](255) NULL,
	[ReadSubsid] [nvarchar](255) NULL,
	[ReasonForRef] [nvarchar](255) NULL,
	[RefBy] [nvarchar](255) NULL,
	[RefComment] [nvarchar](255) NULL,
	[ReferralDate] [nvarchar](255) NULL,
	[RiIcd10OpEpisodeDiagnosisCodeType] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[StatusDT] [nvarchar](255) NULL,
	[StatusDTInt] [nvarchar](255) NULL,
	[Subsid] [nvarchar](255) NULL,
	[SuspectedCancerCode] [nvarchar](255) NULL,
	[Verbal] [nvarchar](255) NULL,
	[WalkinReferral] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[OPREFERRAL] ADD  CONSTRAINT [DF__OPREFERRA__Error__40C49C62]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
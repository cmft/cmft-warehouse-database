﻿CREATE TABLE [InquireError].[WLSUSPENSION](
	[WLSUSPENSIONID] [nvarchar](255) NULL,
	[EpisodeNumber] [nvarchar](255) NULL,
	[EpsActvDtimeInt] [nvarchar](255) NULL,
	[Extended] [nvarchar](255) NULL,
	[Extension] [nvarchar](255) NULL,
	[ExtensionBy] [nvarchar](255) NULL,
	[InitiatedBy] [nvarchar](255) NULL,
	[InternalPatientNumber] [nvarchar](255) NULL,
	[LastRevisionDT] [nvarchar](255) NULL,
	[LastRevisionDTInt] [nvarchar](255) NULL,
	[LastRevisionUID] [nvarchar](255) NULL,
	[Reason] [nvarchar](255) NULL,
	[SuspendedUntil] [nvarchar](255) NULL,
	[SuspendedUntil_1] [nvarchar](255) NULL,
	[SuspensionStartDate] [nvarchar](255) NULL,
	[SuspReason] [nvarchar](255) NULL,
	[WlSuspensionExtensionRepeatGroupKey] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[WLSUSPENSION] ADD  CONSTRAINT [DF__WLSUSPENS__Error__2AD55B43]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [InquireError].[DOD](
	[DODID] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[FullDescription] [nvarchar](255) NULL,
	[InternalValue] [nvarchar](255) NULL,
	[MfRecStsInd] [nvarchar](255) NULL,
	[ScottishIntValue] [nvarchar](255) NULL,
	[DestinationCode] [nvarchar](255) NULL,
	[ErrorCode] [nvarchar](255) NULL,
	[ErrorColumn] [nvarchar](255) NULL,
	[ErrorRaised] [datetime2](7) NULL
) ON [PRIMARY]
GO
ALTER TABLE [InquireError].[DOD] ADD  CONSTRAINT [DF__DOD__ErrorRaised__214BF109]  DEFAULT (sysdatetime()) FOR [ErrorRaised]
﻿CREATE TABLE [derived].[CurrentCaseNoteIDs](
	[InternalPatientNumber] [int] NOT NULL,
	[Casenote] [varchar](14) NOT NULL,
 CONSTRAINT [pk_ccnid] PRIMARY KEY CLUSTERED 
(
	[InternalPatientNumber] ASC,
	[Casenote] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
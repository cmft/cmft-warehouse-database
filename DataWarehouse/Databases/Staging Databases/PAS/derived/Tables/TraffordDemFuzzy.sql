﻿CREATE TABLE [derived].[TraffordDemFuzzy](
	[pat_id] [int] NOT NULL,
	[pat_familyname] [varchar](80) NULL,
	[pat_givenname] [varchar](80) NULL,
	[PAT_BIRTHDATE] [datetime] NULL,
	[PAT_DEATHDATE] [datetime] NULL
) ON [PRIMARY]
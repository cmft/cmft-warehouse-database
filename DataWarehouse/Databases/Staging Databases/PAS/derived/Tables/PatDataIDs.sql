﻿CREATE TABLE [derived].[PatDataIDs](
	[InternalPatientNumber] [int] NOT NULL,
	[DistrictNumber] [int] NULL,
	[DNTxt] [varchar](14) NULL,
	[NHSNumber] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[InternalPatientNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
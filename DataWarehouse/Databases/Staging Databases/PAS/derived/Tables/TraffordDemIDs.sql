﻿CREATE TABLE [derived].[TraffordDemIDs](
	[pat_id] [int] NOT NULL,
	[pat_number] [varchar](25) NULL,
	[NHSNumber] [varchar](40) NULL,
	[EPMINumber] [varchar](40) NULL
) ON [PRIMARY]
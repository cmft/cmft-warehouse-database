﻿CREATE TABLE [Utility].[AuditProcessSource](
	[ProcessSourceCode] [varchar](50) NOT NULL,
	[ProcessSource] [varchar](255) NULL,
 CONSTRAINT [PK_ProcessSource] PRIMARY KEY CLUSTERED 
(
	[ProcessSourceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [Utility].[Rowstats](
	[fulltable_name] [nvarchar](261) NULL,
	[schema_name] [nvarchar](128) NULL,
	[table_name] [sysname] NOT NULL,
	[rows] [int] NULL,
	[sampleTime] [datetime2](7) NOT NULL,
	[idx] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idx] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
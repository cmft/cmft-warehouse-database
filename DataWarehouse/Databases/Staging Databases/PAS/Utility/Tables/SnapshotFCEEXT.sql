﻿CREATE TABLE [Utility].[SnapshotFCEEXT](
	[CensusDate] [date] NULL,
	[EpisodeStartDate] [date] NULL,
	[Cases] [int] NULL
) ON [PRIMARY]
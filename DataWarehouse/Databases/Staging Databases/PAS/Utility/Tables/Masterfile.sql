﻿CREATE TABLE [Utility].[Masterfile](
	[InquireTable] [sysname] NOT NULL,
	[Batch] [tinyint] NULL,
	[truncateTable] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Utility].[Masterfile] ADD  DEFAULT ((1)) FOR [truncateTable]
﻿CREATE TABLE [Utility].[Datafile](
	[InquireTable] [sysname] NOT NULL,
	[Batch] [tinyint] NULL,
	[truncateTable] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [Utility].[Datafile] ADD  DEFAULT ((1)) FOR [truncateTable]
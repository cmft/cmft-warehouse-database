﻿CREATE TABLE [Utility].[AuditProcess](
	[ProcessCode] [varchar](255) NOT NULL,
	[Process] [varchar](255) NULL,
	[ParentProcessCode] [varchar](255) NULL,
	[ProcessSourceCode] [varchar](50) NOT NULL,
	[Deleted] [bit] NULL,
 CONSTRAINT [PK_Process] PRIMARY KEY CLUSTERED 
(
	[ProcessCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--ALTER TABLE [Utility].[AuditProcess]  WITH CHECK ADD  CONSTRAINT [FK_AuditProcess_AuditProcess] FOREIGN KEY([ProcessSourceCode])
--REFERENCES [Utility].[AuditProcessSource] ([ProcessSourceCode])
--GO

--ALTER TABLE [Utility].[AuditProcess] CHECK CONSTRAINT [FK_AuditProcess_AuditProcess]
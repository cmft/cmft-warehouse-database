﻿CREATE TABLE [Utility].[InquireStatistics](
	[CurrentCensusDate] [date] NULL,
	[TableName] [varchar](100) NULL,
	[EpisodeStartDate] [date] NULL,
	[DayOfWeek] [varchar](10) NULL,
	[DayOfWeekKey] [tinyint] NULL,
	[CurrentCases] [int] NULL,
	[PreviousCensusDate] [date] NULL,
	[PreviousCases] [int] NULL,
	[CurrentCasesMinusPreviousCases] [int] NULL,
	[AbsoluteCurrentCasesMinusPreviousCases] [int] NULL,
	[StatisticsCensusDate] [date] NULL,
	[CasesMean] [int] NULL,
	[CasesSD] [float] NULL,
	[CurrentCasesMinusMeanCases] [int] NULL,
	[AbsoluteCurrentCasesMinusMeanCases] [int] NULL,
	[CurrentCasesWithin1SD] [bit] NULL,
	[CurrentCasesWithin2SD] [bit] NULL,
	[CurrentCasesWithin3SD] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NHS_Trust_EAddr](
	[NHSTE_ID] [int] IDENTITY(1,1) NOT NULL,
	[NHST_ID] [int] NOT NULL,
	[NHSTE_EAddr] [nvarchar](50) NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[Lastmodified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
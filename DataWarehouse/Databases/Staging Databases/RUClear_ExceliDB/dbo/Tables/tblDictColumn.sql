﻿CREATE TABLE [dbo].[tblDictColumn](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Dictionary_Id] [int] NULL,
	[ColumnHeader] [varchar](50) NULL,
	[IsActive] [tinyint] NULL CONSTRAINT [DF_tblDictColumn_IsActive]  DEFAULT (1),
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_tblDictColumn_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
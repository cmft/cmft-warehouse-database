﻿CREATE TABLE [dbo].[tblApptClinicEAddr](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinic_ID] [int] NOT NULL,
	[EmailAddress] [nvarchar](50) NOT NULL,
	[IsPreferred] [bit] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
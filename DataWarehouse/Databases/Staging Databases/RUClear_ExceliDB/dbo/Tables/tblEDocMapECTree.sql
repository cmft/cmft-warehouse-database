﻿CREATE TABLE [dbo].[tblEDocMapECTree](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EDocMap_ID] [int] NOT NULL,
	[DisplayOrder] [int] NULL,
	[EDocMapSourceRecType_SLU] [int] NOT NULL,
	[SourceRecord_ID] [int] NOT NULL,
	[Parent_EDocMapECTree_ID] [int] NOT NULL,
	[FormLibItem_ID] [int] NOT NULL CONSTRAINT [DF_tblEDocMapECTree_FormLibItem_ID]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
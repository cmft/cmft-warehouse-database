﻿CREATE TABLE [dbo].[tblEDocRefObj](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](256) NULL,
	[File_Name] [varchar](256) NULL,
	[EDocRefObjType_SLU] [int] NOT NULL,
	[ObjectFile] [image] NOT NULL,
	[Comments] [varchar](200) NULL,
	[KeyWords] [varchar](100) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
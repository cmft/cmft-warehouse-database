﻿CREATE TABLE [dbo].[tblCustomDictionary](
	[MedicalDictionary] [image] NULL,
	[BackupDict] [image] NULL,
	[UpDatedDict] [image] NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
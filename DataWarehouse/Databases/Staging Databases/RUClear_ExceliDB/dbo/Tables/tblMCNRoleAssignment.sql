﻿CREATE TABLE [dbo].[tblMCNRoleAssignment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MCNRole_ID] [int] NOT NULL,
	[MCN_ID] [int] NOT NULL,
	[Staff_ID] [int] NOT NULL,
	[StaffType] [int] NOT NULL,
	[ConfUserType] [int] NULL,
	[MCNCatchment_ID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[StopDate] [datetime] NULL,
	[IsActive] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
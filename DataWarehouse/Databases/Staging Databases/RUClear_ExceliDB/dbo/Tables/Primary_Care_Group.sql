﻿CREATE TABLE [dbo].[Primary_Care_Group](
	[PCG_ID] [int] IDENTITY(1,1) NOT NULL,
	[PCG_Name] [nvarchar](50) NULL,
	[PCG_ID_Number] [nvarchar](50) NULL,
	[PCG_Contact_Name] [nvarchar](50) NULL,
	[PCG_Contact_Designation] [nvarchar](50) NULL,
	[PCG_Addr1] [varchar](35) NULL,
	[PCG_Addr2] [varchar](35) NULL,
	[PCG_County] [nvarchar](30) NULL,
	[PCG_City] [varchar](35) NULL,
	[PCG_PCode] [nvarchar](50) NULL,
	[PCG_Country_Code] [int] NULL,
	[HB_ID] [int] NULL,
	[NHST_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModIFied_User_ID] [int] NOT NULL,
	[LastModIFiedDate] [datetime] NOT NULL
) ON [PRIMARY]
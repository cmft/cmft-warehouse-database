﻿CREATE TABLE [dbo].[tblCAMAuditData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SysFSGroup_ID] [int] NOT NULL,
	[ModuleType] [tinyint] NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[DataID] [int] NOT NULL,
	[TypeofChange] [tinyint] NOT NULL,
	[Status_LU] [int] NOT NULL,
	[StartProcessTime] [datetime] NULL,
	[EndProcessTime] [datetime] NULL
) ON [PRIMARY]
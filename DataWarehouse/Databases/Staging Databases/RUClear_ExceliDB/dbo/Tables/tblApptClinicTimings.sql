﻿CREATE TABLE [dbo].[tblApptClinicTimings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinic_ID] [int] NOT NULL,
	[ClinicDate] [datetime] NOT NULL,
	[WeekDay] [int] NULL,
	[Starttime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[RecurrencePattern] [varchar](8000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
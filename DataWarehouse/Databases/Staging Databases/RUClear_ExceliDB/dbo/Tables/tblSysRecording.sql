﻿CREATE TABLE [dbo].[tblSysRecording](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](1000) NOT NULL,
	[IsRequired] [bit] NOT NULL CONSTRAINT [df_tblsysrecording_isrequired]  DEFAULT (1),
	[IsActive] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
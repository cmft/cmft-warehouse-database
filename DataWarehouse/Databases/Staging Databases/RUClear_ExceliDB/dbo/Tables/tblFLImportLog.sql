﻿CREATE TABLE [dbo].[tblFLImportLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](128) NOT NULL,
	[SourceSiteID] [int] NOT NULL,
	[SourceItemID] [int] NOT NULL,
	[SourceItemName] [nvarchar](256) NULL,
	[TargetSiteID] [int] NOT NULL,
	[TargetItemID] [int] NOT NULL,
	[TargetItemName] [nvarchar](256) NULL,
	[SelectedOption] [nvarchar](15) NULL,
	[ImportedDate] [datetime] NOT NULL,
	[ImportedBy_User_ID] [int] NOT NULL
) ON [PRIMARY]
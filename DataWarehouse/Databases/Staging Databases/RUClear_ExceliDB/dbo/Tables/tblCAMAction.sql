﻿CREATE TABLE [dbo].[tblCAMAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Rule_ID] [int] NOT NULL,
	[ActionType_ID] [int] NOT NULL,
	[ActionValue] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
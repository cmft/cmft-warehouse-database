﻿CREATE TABLE [dbo].[Ax_CAR_TotalUserActivity](
	[CustomerCode] [varchar](50) NULL,
	[Month] [varchar](255) NULL,
	[LoggedInMonth] [int] NULL,
	[LoggedInYear] [int] NULL,
	[LoggedFrom] [nvarchar](255) NULL,
	[NumberOfLogins] [int] NULL
) ON [PRIMARY]
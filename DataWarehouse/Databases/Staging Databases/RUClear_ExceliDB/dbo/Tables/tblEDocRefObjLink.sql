﻿CREATE TABLE [dbo].[tblEDocRefObjLink](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Parent_EDocRefObj_ID] [int] NOT NULL,
	[Child_EDocRefObj_ID] [int] NOT NULL,
	[EDocRefObjLinkType_SLU] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[GP_Branch_Phone](
	[RGPBPH_ID] [int] IDENTITY(1,1) NOT NULL,
	[RGPBP_ID] [int] NOT NULL,
	[RGPBPH_Phone_No] [nvarchar](100) NOT NULL,
	[RGPBPH_Phone_Type_Code] [int] NOT NULL,
	[RGPBPH_Provider_Code] [int] NULL,
	[IsISDN_Bonded] [bit] NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
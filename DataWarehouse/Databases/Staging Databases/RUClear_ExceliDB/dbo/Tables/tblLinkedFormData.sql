﻿CREATE TABLE [dbo].[tblLinkedFormData](
	[MasterID] [int] NOT NULL,
	[MasterType] [tinyint] NOT NULL,
	[MasterDataID] [int] NOT NULL,
	[SlaveID] [int] NOT NULL,
	[SlaveType] [tinyint] NOT NULL,
	[SlaveDataID] [int] NOT NULL
) ON [PRIMARY]
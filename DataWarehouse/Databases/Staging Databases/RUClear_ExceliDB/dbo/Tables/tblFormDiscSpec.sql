﻿CREATE TABLE [dbo].[tblFormDiscSpec](
	[FormLibItem_ID] [int] NOT NULL,
	[Discipline_LU] [int] NOT NULL,
	[Speciality_LU] [int] NOT NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
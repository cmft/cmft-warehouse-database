﻿CREATE TABLE [dbo].[tblUserPreferences](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User_ID] [int] NOT NULL,
	[SysECForm_ID] [int] NOT NULL,
	[ControlType_SLU] [int] NOT NULL,
	[Settings] [varchar](7000) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblApptGroupPatient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Appt_ID] [int] NOT NULL,
	[Pat_ID] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
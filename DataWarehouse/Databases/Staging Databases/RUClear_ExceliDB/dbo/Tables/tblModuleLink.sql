﻿CREATE TABLE [dbo].[tblModuleLink](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Source_SysModule_ID] [int] NOT NULL,
	[ButtonCaption] [varchar](50) NULL,
	[ButtonImageFile] [varchar](50) NULL,
	[ButtonShortDesc] [varchar](256) NULL,
	[ButtonColour] [varchar](50) NULL,
	[FontColour] [varchar](50) NULL,
	[DisplayPosition] [int] NULL,
	[LinkModuleType_SLU] [int] NULL,
	[Link_SysModule_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[GP_Branch_EAddr](
	[RGPBE_ID] [int] IDENTITY(1,1) NOT NULL,
	[RGPBP_ID] [int] NULL,
	[RGPBE_EAddr] [nvarchar](50) NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
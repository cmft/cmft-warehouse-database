﻿CREATE TABLE [dbo].[tblChartDesignerSeries](
	[ChartDesigner_ID] [int] NOT NULL,
	[SeriesID] [int] NOT NULL,
	[ChartDesigner_GroupID] [smallint] NOT NULL,
	[SeriesHeadings] [varchar](20) NOT NULL,
	[SeriesXValues] [ntext] NOT NULL,
	[SeriesYValues] [ntext] NULL,
	[SeriesDataPoints] [int] NOT NULL,
	[SeriesLineColor] [varchar](20) NOT NULL,
	[SeriesLineWidth] [smallint] NOT NULL,
	[SeriesLinePattern] [smallint] NOT NULL,
	[SeriesInteriorForeColor] [varchar](20) NOT NULL,
	[SeriesInteriorBackColor] [varchar](20) NOT NULL,
	[SeriesMarker] [smallint] NOT NULL,
	[SeriesMarkerSize] [smallint] NOT NULL,
	[SeriesMarkerColor] [varchar](20) NOT NULL,
	[SeriesDisplay] [smallint] NOT NULL,
	[ChartMode] [char](1) NOT NULL,
	[IsActive] [int] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSysClientConfig](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[ConfigValue] [nvarchar](100) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
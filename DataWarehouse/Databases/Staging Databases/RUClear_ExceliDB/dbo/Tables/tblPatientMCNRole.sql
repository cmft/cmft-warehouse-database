﻿CREATE TABLE [dbo].[tblPatientMCNRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PATMCN_ID] [int] NOT NULL,
	[MCNRole_ID] [int] NOT NULL,
	[MCN_ID] [int] NOT NULL,
	[Pat_ID] [int] NOT NULL,
	[Staff_ID] [int] NOT NULL,
	[StaffType] [int] NOT NULL,
	[IsPrimary] [tinyint] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
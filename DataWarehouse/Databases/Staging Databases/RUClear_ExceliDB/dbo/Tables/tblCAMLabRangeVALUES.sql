﻿CREATE TABLE [dbo].[tblCAMLabRangeVALUES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Labtest_ID] [int] NOT NULL,
	[Gender_LU] [int] NULL,
	[FromAge] [int] NULL,
	[ToAge] [int] NULL,
	[Minvalue] [int] NOT NULL,
	[Maxvalue] [int] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
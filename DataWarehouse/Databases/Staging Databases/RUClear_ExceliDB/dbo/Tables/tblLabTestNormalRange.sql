﻿CREATE TABLE [dbo].[tblLabTestNormalRange](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LabTest_ID] [int] NOT NULL,
	[Sex] [int] NULL,
	[FromAge] [int] NULL,
	[FromTo] [int] NULL,
	[HighValue] [float] NULL,
	[LowValue] [float] NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
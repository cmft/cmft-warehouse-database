﻿CREATE TABLE [dbo].[tblPatientProblemMCN](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient_ID] [int] NOT NULL,
	[PRD_ID] [int] NOT NULL,
	[MCN_ID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[StopDate] [datetime] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
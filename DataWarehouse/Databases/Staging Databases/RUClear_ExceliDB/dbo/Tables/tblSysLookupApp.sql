﻿CREATE TABLE [dbo].[tblSysLookupApp](
	[LookupCategory_SLU] [int] NOT NULL,
	[AppType] [int] NOT NULL,
	[IsCoded] [tinyint] NOT NULL CONSTRAINT [DF_tblSysLookupApp_IsCoded]  DEFAULT (0)
) ON [PRIMARY]
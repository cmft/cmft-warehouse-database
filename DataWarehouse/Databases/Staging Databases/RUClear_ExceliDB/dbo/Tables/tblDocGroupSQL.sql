﻿CREATE TABLE [dbo].[tblDocGroupSQL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupView] [varchar](2000) NULL,
	[GroupCriteria] [varchar](2000) NULL,
	[GroupFields] [varchar](4000) NULL,
	[Document_ID] [int] NULL
) ON [PRIMARY]
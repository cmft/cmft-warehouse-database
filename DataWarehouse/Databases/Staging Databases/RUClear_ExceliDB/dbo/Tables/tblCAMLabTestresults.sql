﻿CREATE TABLE [dbo].[tblCAMLabTestresults](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LabTest_ID] [int] NOT NULL,
	[Usr_ID] [int] NOT NULL,
	[IsArrival] [tinyint] NOT NULL,
	[Abn_Result] [tinyint] NOT NULL,
	[LabResult_Min] [int] NOT NULL,
	[LabResult_Max] [int] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
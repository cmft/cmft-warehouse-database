﻿CREATE TABLE [dbo].[tblACLStaff](
	[Usr_ID] [int] NOT NULL,
	[ACLGroup_ID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[StopDate] [datetime] NULL,
	[SetBy_User_ID] [int] NOT NULL,
	[StoppedBy_User_ID] [int] NULL,
	[RestrictionLevel] [tinyint] NOT NULL CONSTRAINT [DF_tblACLStaff_RestrictionLevel]  DEFAULT (0),
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
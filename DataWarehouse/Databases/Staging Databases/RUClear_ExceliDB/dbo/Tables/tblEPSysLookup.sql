﻿CREATE TABLE [dbo].[tblEPSysLookup](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LookupValue] [nvarchar](50) NOT NULL,
	[ListPosition] [smallint] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
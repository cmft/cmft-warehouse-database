﻿CREATE TABLE [dbo].[tblApptClinicClinician](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinic_ID] [int] NULL,
	[Clinician_ID] [int] NULL,
	[IsActive] [tinyint] NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
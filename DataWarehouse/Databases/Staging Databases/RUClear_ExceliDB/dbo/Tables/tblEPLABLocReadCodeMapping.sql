﻿CREATE TABLE [dbo].[tblEPLABLocReadCodeMapping](
	[ECLabTest_ID] [int] NOT NULL,
	[SCIStoreLoc_SLU] [int] NOT NULL,
	[LabStoreLocation] [varchar](50) NOT NULL,
	[ReadCode] [varchar](25) NOT NULL,
	[ReadCodeDesc] [varchar](100) NULL,
	[ShortName] [varchar](25) NULL,
	[LongName] [varchar](100) NULL,
	[DateCreated] [datetime] NULL,
	[Department_LU] [int] NULL,
	[ResultStore_SLU] [int] NULL,
	[SpecimenType_LU] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
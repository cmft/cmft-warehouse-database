﻿CREATE TABLE [dbo].[tblEPImportWarning](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PortConfig_Id] [int] NULL,
	[ImportWarning_LU] [int] NOT NULL,
	[Description] [varchar](4000) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsMailSent] [tinyint] NOT NULL,
	[Severity] [tinyint] NULL,
	[IsActive] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_Id] [int] NOT NULL
) ON [PRIMARY]
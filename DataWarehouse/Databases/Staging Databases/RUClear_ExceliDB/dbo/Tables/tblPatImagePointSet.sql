﻿CREATE TABLE [dbo].[tblPatImagePointSet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[COB_ID] [int] NOT NULL,
	[SetName] [varchar](20) NOT NULL,
	[SetDate] [datetime] NOT NULL,
	[AnnPoints] [image] NULL,
	[Sig1_User_ID] [int] NULL,
	[Sig2_user_ID] [int] NULL,
	[Sig1_Discipline_LU] [int] NULL,
	[Sig1_Speciality_LU] [int] NOT NULL,
	[Sig1_Department_LU] [int] NULL,
	[Entered_User_ID] [int] NOT NULL,
	[IsRecordSigned] [tinyint] NOT NULL,
	[IsHistoryAvailable] [tinyint] NULL,
	[IsAutoSigned] [tinyint] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
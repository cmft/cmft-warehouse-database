﻿CREATE TABLE [dbo].[tblWebCompilerLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WebServer] [varchar](50) NULL,
	[WebVersion] [varchar](50) NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[CompiledForms] [varchar](2000) NULL,
	[Total_Forms_Generated] [int] NULL,
	[Total_Forms_SuccessfullyGenerated] [int] NULL,
	[Total_Forms_GeneratedWithErrors] [int] NULL,
	[IsActive] [nvarchar](1) NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]
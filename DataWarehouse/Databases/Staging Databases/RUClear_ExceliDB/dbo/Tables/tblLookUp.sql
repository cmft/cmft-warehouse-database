﻿CREATE TABLE [dbo].[tblLookUp](
	[ID] [int] IDENTITY(100000,1) NOT NULL,
	[LookupCategory_SLU] [int] NOT NULL,
	[Code] [varchar](30) NULL,
	[AddCode] [varchar](30) NULL,
	[LookupValue] [nvarchar](125) NOT NULL,
	[ListPosition] [smallint] NOT NULL,
	[IsProtected] [bit] NULL CONSTRAINT [DF_tblLookUp_IsProtected]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblLookUp_LastModifiedDate]  DEFAULT (getdate()),
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblScriptQuestion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Script_ID] [int] NOT NULL,
	[ScriptSection_SectionNo] [smallint] NOT NULL,
	[Question_ID] [int] NOT NULL,
	[IsReportedByPhone] [bit] NOT NULL CONSTRAINT [df_tblscriptquestion_isreportedbyphone]  DEFAULT (0),
	[IsReportedByGraph] [bit] NOT NULL CONSTRAINT [df_tblscriptquestion_isreportedbygraph]  DEFAULT (0),
	[IsReportedByAlertReport] [bit] NOT NULL CONSTRAINT [df_tblscriptquestion_isreportedbyalertreport]  DEFAULT (0),
	[ResponseFilter] [nvarchar](50) NULL,
	[ListPosition] [smallint] NOT NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_tblscriptquestion_isactive]  DEFAULT (1),
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblMCNRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](80) NOT NULL,
	[MCN_ID] [int] NOT NULL,
	[VisibleToPatient] [tinyint] NOT NULL,
	[IsActive] [tinyint] NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCAMFailedNotification](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Notification_ID] [int] NOT NULL,
	[NotificationDetails_ID] [int] NOT NULL,
	[NotificationTemplateRecipient_ID] [int] NULL,
	[NotificationMode_LU] [int] NOT NULL,
	[RecipientID] [int] NOT NULL,
	[ErrNumber] [int] NOT NULL,
	[ErrDescription] [varchar](255) NULL,
	[NotificationFailedDate] [datetime] NULL
) ON [PRIMARY]
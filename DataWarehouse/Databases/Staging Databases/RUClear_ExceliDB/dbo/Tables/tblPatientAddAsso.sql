﻿CREATE TABLE [dbo].[tblPatientAddAsso](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NULL,
	[PPA_ID] [int] NOT NULL,
	[AddAssCode_LU] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
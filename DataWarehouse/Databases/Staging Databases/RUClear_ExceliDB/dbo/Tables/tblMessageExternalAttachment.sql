﻿CREATE TABLE [dbo].[tblMessageExternalAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Message_ID] [int] NOT NULL,
	[AttachmentName] [nvarchar](500) NULL,
	[Attachment] [image] NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
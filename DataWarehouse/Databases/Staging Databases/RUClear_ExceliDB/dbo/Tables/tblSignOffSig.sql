﻿CREATE TABLE [dbo].[tblSignOffSig](
	[OrigRec_ID] [int] NOT NULL,
	[TableName] [nvarchar](100) NOT NULL,
	[ActualSig] [image] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
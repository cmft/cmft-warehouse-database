﻿CREATE TABLE [dbo].[tblNotificationTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationName] [varchar](100) NOT NULL,
	[NotificationUrgency_LU] [int] NOT NULL,
	[NotificationSchedule] [nvarchar](4000) NULL,
	[UrgencyInterval] [int] NULL,
	[UrgencyInterval_LU] [int] NULL,
	[RepeatNotification] [bit] NOT NULL,
	[Repetition_LU] [int] NULL,
	[RepetitionInterval] [int] NULL,
	[RepetitionInterval_LU] [int] NULL,
	[RepetitionTimes] [int] NULL,
	[RecurringText] [varchar](4000) NULL,
	[MaxRepetitionTime] [smallint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
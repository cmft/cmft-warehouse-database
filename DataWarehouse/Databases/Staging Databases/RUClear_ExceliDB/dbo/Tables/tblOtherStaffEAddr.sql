﻿CREATE TABLE [dbo].[tblOtherStaffEAddr](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OtherStaff_ID] [int] NOT NULL,
	[EAddr] [nvarchar](50) NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
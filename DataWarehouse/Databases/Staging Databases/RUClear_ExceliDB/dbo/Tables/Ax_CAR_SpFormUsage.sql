﻿CREATE TABLE [dbo].[Ax_CAR_SpFormUsage](
	[CustomerCode] [varchar](50) NULL,
	[FormName] [nvarchar](255) NULL,
	[Month] [varchar](255) NULL,
	[ModifiedMonth] [int] NULL,
	[ModifiedYear] [int] NULL,
	[TotalModifications] [int] NULL
) ON [PRIMARY]
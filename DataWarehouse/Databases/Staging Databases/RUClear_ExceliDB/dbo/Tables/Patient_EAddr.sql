﻿CREATE TABLE [dbo].[Patient_EAddr](
	[PEA_ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[PEA_EAddr] [nvarchar](50) NOT NULL,
	[PEA_EAddr2] [nvarchar](50) NULL,
	[PEA_EAddr3] [nvarchar](50) NULL,
	[IsPreferred] [bit] NULL,
	[ListPosition] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
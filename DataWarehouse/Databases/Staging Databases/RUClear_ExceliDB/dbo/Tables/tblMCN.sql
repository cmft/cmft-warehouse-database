﻿CREATE TABLE [dbo].[tblMCN](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](80) NOT NULL,
	[Description] [varchar](500) NULL,
	[Manager_Staff_ID] [int] NOT NULL,
	[Manager_StaffType] [int] NOT NULL,
	[Lead_Clinician_ID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[MCNEndReason_SLU] [int] NULL,
	[MCNEndInformation] [varchar](500) NULL,
	[MCNCatchmentType_SLU] [int] NULL,
	[UseInConference] [tinyint] NULL,
	[ACLBehaviour] [tinyint] NULL,
	[IsActive] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
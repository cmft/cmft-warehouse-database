﻿CREATE TABLE [dbo].[tblPatientClinicalNote](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NULL,
	[PatClinicalCode_ID] [int] NULL,
	[Encounter_ID] [int] NULL,
	[DateRecorded] [datetime] NULL,
	[TimeRecorded] [datetime] NULL,
	[NoteHeader] [nvarchar](140) NULL,
	[Sig1_User_ID] [int] NULL,
	[Sig2_User_ID] [int] NULL,
	[Sig1_Discipline_LU] [int] NULL,
	[Sig1_Speciality_LU] [int] NULL,
	[Sig1_Department_LU] [int] NULL,
	[Entered_User_ID] [int] NOT NULL,
	[IsRecordSigned] [tinyint] NOT NULL,
	[IsHistoryAvailable] [tinyint] NOT NULL CONSTRAINT [DF_tblPatientClinicalNote_IsHistoryAvailable]  DEFAULT (0),
	[IsAutoSigned] [tinyint] NOT NULL CONSTRAINT [DF_tblPatientClinicalNote_IsAutoSigned]  DEFAULT (0),
	[PatClinicalNote_GroupID] [int] NULL,
	[Facility_LU] [int] NULL,
	[AmendmentReason_LU] [int] NULL,
	[AmendmentComments] [nvarchar](512) NULL,
	[InActivationReason_LU] [int] NULL,
	[EditCount] [smallint] NOT NULL CONSTRAINT [DF_tblPatientClinicalNote_EditCount]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
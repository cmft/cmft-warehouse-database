﻿CREATE TABLE [dbo].[tblSecurityModule](
	[SysModule_ID] [int] NOT NULL,
	[SecurityLevel_ID] [int] NOT NULL,
	[AccessType] [tinyint] NULL,
	[IsSystemModule] [bit] NULL CONSTRAINT [df_tblsecuritymodule_issystemmodule]  DEFAULT (1),
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [df_tblsecuritymodule_lastmodifieddate]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblAppLogin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LoginDate] [datetime] NULL,
	[LogoffDate] [datetime] NULL,
	[UserID] [int] NULL
) ON [PRIMARY]
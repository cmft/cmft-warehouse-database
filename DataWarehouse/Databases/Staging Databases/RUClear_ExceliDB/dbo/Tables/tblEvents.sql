﻿CREATE TABLE [dbo].[tblEvents](
	[ID] [int] NOT NULL,
	[SourceID] [int] NOT NULL,
	[FieldID] [int] NOT NULL,
	[Event_SLU] [int] NOT NULL,
	[EventName] [varchar](50) NOT NULL,
	[EventDescription] [varchar](100) NULL,
	[IsActive] [smallint] NOT NULL
) ON [PRIMARY]
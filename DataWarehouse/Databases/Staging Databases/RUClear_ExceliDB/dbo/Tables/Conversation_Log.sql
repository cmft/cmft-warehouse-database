﻿CREATE TABLE [dbo].[Conversation_Log](
	[CNV_ID] [int] IDENTITY(1,1) NOT NULL,
	[CNV_User_ID] [int] NULL,
	[CON_ID] [int] NULL,
	[RCL_ID] [int] NULL,
	[CNV_Start_Time] [datetime] NULL,
	[CNV_End_Time] [datetime] NULL,
	[CLG_ID] [int] NULL,
	[CNV_Consult_Type_Code] [int] NULL,
	[CNV_Phone_No] [nvarchar](100) NULL,
	[Comments] [nvarchar](1000) NULL,
	[Direction] [smallint] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
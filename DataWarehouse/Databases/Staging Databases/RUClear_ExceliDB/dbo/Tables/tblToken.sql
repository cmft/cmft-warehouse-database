﻿CREATE TABLE [dbo].[tblToken](
	[ID] [int] IDENTITY(1000,1) NOT NULL,
	[Name] [nvarchar](75) NOT NULL,
	[ObjectName] [nvarchar](512) NOT NULL,
	[TokenType_LU] [int] NOT NULL,
	[NoInParams] [int] NULL,
	[DBUserName] [nvarchar](75) NULL,
	[DBUserPassword] [nvarchar](75) NULL,
	[IsActive] [char](10) NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
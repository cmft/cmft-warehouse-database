﻿CREATE TABLE [dbo].[tblSysDefault](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](4000) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[ListValues] [nvarchar](1200) NULL,
	[ListPosition] [int] NOT NULL,
	[ParentNode] [nvarchar](50) NOT NULL,
	[Type] [tinyint] NOT NULL,
	[ControlType] [tinyint] NOT NULL,
	[ImageName] [nvarchar](20) NOT NULL,
	[Settings] [ntext] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
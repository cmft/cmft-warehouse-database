﻿CREATE TABLE [dbo].[tblFormLibSetItems](
	[FormLibSet_ID] [int] NOT NULL,
	[FormLibItem_ID] [int] NOT NULL,
	[LibraryStatus_SLU] [int] NULL,
	[DisplayOrder] [int] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
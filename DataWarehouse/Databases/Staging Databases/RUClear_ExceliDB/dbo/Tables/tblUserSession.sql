﻿CREATE TABLE [dbo].[tblUserSession](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User_ID] [int] NOT NULL,
	[TypeOfSession_SLU] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[IsComplete] [bit] NULL,
	[CallerIDNo] [nvarchar](50) NULL,
	[IPAddress] [nvarchar](50) NULL
) ON [PRIMARY]
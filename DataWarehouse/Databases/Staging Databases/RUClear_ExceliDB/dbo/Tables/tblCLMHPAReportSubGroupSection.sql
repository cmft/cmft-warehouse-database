﻿CREATE TABLE [dbo].[tblCLMHPAReportSubGroupSection](
	[GroupSectionNo] [varchar](10) NULL,
	[SubGroupSectionNo] [varchar](10) NULL,
	[SubGroupSectionDescription] [varchar](256) NULL,
	[SubGroupCountDescription] [varchar](256) NULL,
	[SubGroupSectionCount] [int] NULL
) ON [PRIMARY]
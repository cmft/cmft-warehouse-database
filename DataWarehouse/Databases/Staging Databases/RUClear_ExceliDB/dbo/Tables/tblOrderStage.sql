﻿CREATE TABLE [dbo].[tblOrderStage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Order_ID] [int] NOT NULL,
	[Description] [ntext] NULL,
	[ListPosition] [int] NULL,
	[CollectionScreen_ID] [int] NULL,
	[UDFScreen_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatLabTestNotes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Notes] [varchar](2000) NULL,
	[ResultStore_ID] [int] NULL,
	[ResultStore_SLU] [int] NULL,
	[IsActive] [bit] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
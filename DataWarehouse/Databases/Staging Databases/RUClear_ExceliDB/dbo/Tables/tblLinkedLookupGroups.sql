﻿CREATE TABLE [dbo].[tblLinkedLookupGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LookupGroupID] [int] NOT NULL,
	[LinkGroupID] [int] NOT NULL,
	[IsActive] [int] NOT NULL,
	[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_tblLinkedLookupGroups_LastModifieddate]  DEFAULT (getdate()),
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
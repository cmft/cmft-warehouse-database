﻿CREATE TABLE [dbo].[tblFileRecordInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileID] [int] NULL,
	[RecordNumber] [int] NULL,
	[AdditionalInfo] [varchar](256) NOT NULL,
	[Status] [varchar](64) NULL,
	[SourceData] [nvarchar](3000) NULL
) ON [PRIMARY]
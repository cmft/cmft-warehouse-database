﻿CREATE TABLE [dbo].[tblStaffFacility](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StaffID] [int] NOT NULL,
	[Facility_LU] [int] NOT NULL,
	[UserType_LU] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [DF_tblUserFacility_IsActive]  DEFAULT (1),
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
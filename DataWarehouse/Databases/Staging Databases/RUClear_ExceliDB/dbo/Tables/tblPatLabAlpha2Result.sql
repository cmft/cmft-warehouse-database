﻿CREATE TABLE [dbo].[tblPatLabAlpha2Result](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LabTest_ID] [int] NULL,
	[Sample_ID] [int] NULL,
	[InterSample_ID] [int] NULL,
	[DateSpecimenAuthorised] [datetime] NULL,
	[IsLatestRecord] [bit] NOT NULL,
	[ResultValue] [varchar](250) NULL,
	[LabLocation_ID] [int] NULL,
	[IsNotes] [bit] NULL,
	[RefRange] [varchar](50) NULL,
	[Units] [varchar](100) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
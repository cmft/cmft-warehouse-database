﻿CREATE TABLE [dbo].[tblEPPatMatchField](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Fieldname] [varchar](256) NOT NULL,
	[FieldDataType_LU] [int] NOT NULL,
	[FieldLength] [int] NOT NULL,
	[ECTablename] [varchar](256) NOT NULL,
	[DisplayOrder] [int] NULL,
	[SysFsField_ID] [int] NULL,
	[LookUpID] [int] NULL CONSTRAINT [DF_tblEPPatMatchField_LookUpID]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
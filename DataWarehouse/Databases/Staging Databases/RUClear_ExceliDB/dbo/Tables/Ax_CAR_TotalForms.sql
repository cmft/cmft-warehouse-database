﻿CREATE TABLE [dbo].[Ax_CAR_TotalForms](
	[CustomerCode] [varchar](255) NULL,
	[CustomForm_ID] [int] NULL,
	[FormName] [varchar](255) NULL,
	[Total] [int] NULL
) ON [PRIMARY]
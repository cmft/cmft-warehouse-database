﻿CREATE TABLE [dbo].[tblGCVAdditionalDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GCV_ID] [int] NOT NULL,
	[OnCode] [ntext] NULL,
	[OffCode] [ntext] NULL,
	[Onscore] [int] NULL,
	[Offscore] [int] NULL,
	[DateDisplayFormat_SLU] [int] NULL,
	[TimeDisplayFormat_SLU] [int] NULL,
	[LookupGroupID] [int] NULL,
	[LookupScores] [ntext] NULL,
	[GroupName] [nvarchar](200) NULL,
	[NoOfOptionButtons] [int] NULL,
	[OptionButtonCaption] [ntext] NULL,
	[IsCoded] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
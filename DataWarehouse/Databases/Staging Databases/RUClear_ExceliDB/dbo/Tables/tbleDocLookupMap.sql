﻿CREATE TABLE [dbo].[tbleDocLookupMap](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[eDocLookupRef_ID] [int] NOT NULL,
	[LookupCode] [varchar](256) NOT NULL,
	[Lookup_ID] [int] NULL,
	[ShortDesc] [char](10) NULL,
	[IsNewLookup] [tinyint] NOT NULL,
	[XSDMappedElement_ID] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
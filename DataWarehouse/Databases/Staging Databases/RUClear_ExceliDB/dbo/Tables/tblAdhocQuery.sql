﻿CREATE TABLE [dbo].[tblAdhocQuery](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QueryFile] [ntext] NULL,
	[QueryName] [nvarchar](75) NULL,
	[QueryType] [char](1) NULL,
	[Usr_ID] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblMCNConfPatient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Conf_ID] [int] NOT NULL,
	[Instance_ID] [int] NOT NULL,
	[Pat_ID] [int] NOT NULL,
	[MCN_Id] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL
) ON [PRIMARY]
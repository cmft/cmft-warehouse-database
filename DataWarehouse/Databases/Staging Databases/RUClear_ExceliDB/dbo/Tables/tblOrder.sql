﻿CREATE TABLE [dbo].[tblOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Description] [ntext] NULL,
	[ScheduleSetting_ID] [int] NULL,
	[NoOfSignatures] [int] NULL,
	[NoOfStages] [int] NOT NULL,
	[SecurityLevel] [int] NULL,
	[AuditRecordExists] [bit] NULL,
	[NettingType_SLU] [int] NULL,
	[RequiresPrep] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
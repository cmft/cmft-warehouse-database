﻿CREATE TABLE [dbo].[tblConfInstancePerson](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ConfInstance_ID] [int] NOT NULL,
	[Person_Id] [int] NULL,
	[PersonType] [int] NULL,
	[PersonCategory] [int] NULL,
	[ListPosition] [int] NULL,
	[GuestList] [varchar](1000) NULL,
	[isAttended] [bit] NOT NULL,
	[ReviewSplForm_ID] [int] NULL,
	[ReferralSplForm_ID] [int] NULL,
	[OptField1Value] [varchar](100) NULL,
	[OptField2Value] [varchar](100) NULL,
	[OptField3Value] [varchar](100) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
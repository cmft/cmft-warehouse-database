﻿CREATE TABLE [dbo].[tblApptClinicAddress](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinic_ID] [int] NOT NULL,
	[Address1] [nvarchar](50) NOT NULL,
	[Address2] [nvarchar](50) NOT NULL,
	[Address3] [nvarchar](50) NOT NULL,
	[Address4] [nvarchar](50) NOT NULL,
	[Address5] [nvarchar](50) NOT NULL,
	[PostCode] [nvarchar](50) NOT NULL,
	[Country_LU] [int] NOT NULL,
	[IsPreferred] [bit] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
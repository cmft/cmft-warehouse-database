﻿CREATE TABLE [dbo].[tblGetDataFunction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[NoInParams] [int] NULL,
	[NoOutParams] [int] NULL,
	[Category] [tinyint] NOT NULL CONSTRAINT [DF_tblGetDataFunction_Category]  DEFAULT (0),
	[ReturnType] [char](15) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
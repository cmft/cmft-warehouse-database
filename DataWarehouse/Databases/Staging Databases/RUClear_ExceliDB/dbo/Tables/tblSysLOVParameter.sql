﻿CREATE TABLE [dbo].[tblSysLOVParameter](
	[ID] [int] IDENTITY(500000,1) NOT NULL,
	[LOVID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[DefaultValue] [varchar](100) NULL,
	[DataType] [varchar](50) NULL,
	[DataSize] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblSysLOVParameter_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
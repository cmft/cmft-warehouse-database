﻿CREATE TABLE [dbo].[tblScriptFailedAction](
	[UserSession_ID] [int] NOT NULL,
	[Action_ID] [int] NOT NULL,
	[ScriptRuleLog_SequenceNo] [int] NOT NULL,
	[ErrNumber] [int] NOT NULL,
	[ErrDescription] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCamExpressionField](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Rule_ID] [int] NOT NULL,
	[SysFSField_ID] [int] NOT NULL,
	[FieldName] [varchar](100) NOT NULL,
	[Module_ID] [int] NOT NULL,
	[FieldLookupValue] [int] NOT NULL,
	[FieldType] [tinyint] NOT NULL,
	[ParentID] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
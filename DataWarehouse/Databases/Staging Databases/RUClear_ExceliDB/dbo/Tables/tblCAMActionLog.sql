﻿CREATE TABLE [dbo].[tblCAMActionLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExpressionPool_ID] [int] NOT NULL,
	[Rule_ID] [int] NOT NULL,
	[Action_ID] [int] NOT NULL,
	[Notification_ID] [int] NOT NULL,
	[ActionStatus_LU] [int] NOT NULL,
	[ActionStartDate] [datetime] NOT NULL,
	[ActionEndDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
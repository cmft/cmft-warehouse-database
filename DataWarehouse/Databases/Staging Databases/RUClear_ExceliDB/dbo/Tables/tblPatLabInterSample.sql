﻿CREATE TABLE [dbo].[tblPatLabInterSample](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatLabSample_ID] [int] NULL,
	[DateTimeSampleUsed] [datetime] NULL,
	[Pat_ID] [int] NULL,
	[IsLatestRecord] [bit] NOT NULL CONSTRAINT [DF_tblPatLabInterSample_IsLatestRecord]  DEFAULT (0),
	[Comment] [varchar](2500) NULL,
	[Bucket_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatientClinicalNoteFile](
	[PatClinicalNote_ID] [int] NOT NULL,
	[NoteFile] [image] NULL,
	[PlainText] [ntext] NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblOrderSynonym](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Order_ID] [int] NOT NULL,
	[Synonym] [nvarchar](100) NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
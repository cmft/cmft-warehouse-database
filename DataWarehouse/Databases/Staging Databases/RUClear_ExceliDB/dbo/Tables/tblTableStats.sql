﻿CREATE TABLE [dbo].[tblTableStats](
	[TableName] [varchar](75) NULL,
	[NoOfRows] [int] NULL,
	[ReservedSize] [float] NULL,
	[DataSize] [float] NULL,
	[IndexSize] [float] NULL,
	[UnusedSize] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEventLog](
	[EventType_SLU] [int] NOT NULL,
	[User_ID] [int] NULL,
	[SourceModule] [char](40) NULL,
	[SourceProcedure] [char](40) NULL,
	[Line] [smallint] NULL,
	[ErrorNumber] [int] NULL,
	[ErrorDescription] [varchar](1000) NULL,
	[DateLogged] [datetime] NOT NULL
) ON [PRIMARY]
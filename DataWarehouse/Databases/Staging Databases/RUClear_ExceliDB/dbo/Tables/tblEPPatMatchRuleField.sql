﻿CREATE TABLE [dbo].[tblEPPatMatchRuleField](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatchType_ID] [int] NOT NULL,
	[MatchRule_ID] [int] NOT NULL,
	[MatchField_ID] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
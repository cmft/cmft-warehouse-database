﻿CREATE TABLE [dbo].[tblEDocParkMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient_ID] [int] NOT NULL,
	[EDocMap_ID] [int] NOT NULL,
	[Parent_EDocMap_ID] [int] NOT NULL,
	[ParentMatchKey] [nvarchar](128) NULL,
	[MessagePath] [nvarchar](512) NOT NULL,
	[ParkStatus_LU] [int] NOT NULL,
	[MatchFieldData] [nvarchar](2000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSysECV2HelpMapping](
	[ID] [int] NOT NULL,
	[SysModule_ID] [int] NOT NULL,
	[FormName] [nvarchar](300) NOT NULL,
	[TargetHelpURL] [nvarchar](1000) NOT NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [DF_tblSysECV2HelpMapping_IsActive]  DEFAULT (1),
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblSysECV2HelpMapping_LastModifiedDate]  DEFAULT (getdate()),
	[LastModified_User_ID] [int] NOT NULL CONSTRAINT [DF_tblSysECV2HelpMapping_LastModified_User_ID]  DEFAULT (10)
) ON [PRIMARY]
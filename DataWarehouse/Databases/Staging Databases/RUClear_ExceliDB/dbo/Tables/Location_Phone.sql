﻿CREATE TABLE [dbo].[Location_Phone](
	[LOCPH_ID] [int] IDENTITY(1,1) NOT NULL,
	[LOC_ID] [int] NOT NULL,
	[LOCPH_Phone_No] [nvarchar](100) NOT NULL,
	[LOCPH_Phone_Type_Code] [int] NOT NULL,
	[LOCPH_Provider_Code] [int] NULL,
	[IsISDN_Bonded] [bit] NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
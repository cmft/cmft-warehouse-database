﻿CREATE TABLE [dbo].[Patient_Address](
	[PPA_ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NULL,
	[PPA_Addr1] [nvarchar](35) NULL,
	[PPA_Addr2] [nvarchar](35) NULL,
	[PPA_Addr3] [nvarchar](35) NULL,
	[PPA_County] [nvarchar](30) NULL,
	[PPA_City] [nvarchar](35) NULL,
	[PPA_PCode] [nvarchar](50) NULL,
	[PPA_Country_Code] [int] NULL,
	[PPA_IsCurrent] [bit] NULL,
	[PPA_Entered_Date] [datetime] NULL,
	[PPA_HB_ID] [int] NULL,
	[PPA_ENDDate] [datetime] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModIFied_User_ID] [int] NOT NULL,
	[LastModIFiedDate] [datetime] NOT NULL
) ON [PRIMARY]
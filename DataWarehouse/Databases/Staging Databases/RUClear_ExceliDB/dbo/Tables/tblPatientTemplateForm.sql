﻿CREATE TABLE [dbo].[tblPatientTemplateForm](
	[Patient_ID] [int] NOT NULL,
	[CustomForm_ID] [int] NOT NULL,
	[Condition_ID] [int] NOT NULL,
	[ID] [int] NOT NULL,
	[DateTimeCreated] [datetime] NULL,
	[IsHistoryAvailable] [bit] NULL,
	[DateLastModified] [datetime] NULL,
	[UserCreated] [int] NULL,
	[UserLastModified] [int] NULL,
	[PCN_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblLocWard](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Facility_Id] [int] NOT NULL,
	[Name] [nvarchar](30) NULL,
	[RestrictSex] [char](1) NOT NULL,
	[RestrictAge] [char](1) NOT NULL,
	[NumberBeds] [int] NOT NULL
) ON [PRIMARY]
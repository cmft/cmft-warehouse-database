﻿CREATE TABLE [dbo].[tblWebUDFControlPropertyMap](
	[ID] [int] NOT NULL,
	[ControlType_ID] [int] NULL,
	[PropertyNumber] [tinyint] NULL,
	[PropertyName] [varchar](100) NULL,
	[WebPropertyName] [varchar](100) NULL,
	[HTMLAttributeName] [varchar](100) NULL,
	[ParentProperty] [varchar](100) NULL
) ON [PRIMARY]
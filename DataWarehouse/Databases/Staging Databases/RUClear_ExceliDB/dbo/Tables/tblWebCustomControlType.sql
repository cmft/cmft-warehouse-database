﻿CREATE TABLE [dbo].[tblWebCustomControlType](
	[ID] [int] NOT NULL,
	[PropertyName] [varchar](100) NULL,
	[HTMLAttributeName] [varchar](100) NULL,
	[ParentProperty] [varchar](100) NULL,
	[SourceValue] [varchar](100) NULL,
	[TargetValue] [varchar](100) NULL,
	[DefaultValue] [varchar](100) NULL
) ON [PRIMARY]
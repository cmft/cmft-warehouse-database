﻿CREATE TABLE [dbo].[tblFormUserType](
	[FormLibItem_ID] [int] NOT NULL,
	[UserType_LU] [int] NOT NULL,
	[ReadAccess_SLU] [int] NOT NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSysLOV](
	[ID] [int] IDENTITY(100000,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
	[DBSource] [nvarchar](200) NOT NULL,
	[WhereClause] [varchar](450) NULL,
	[Description] [varchar](450) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblSysLOV_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblApptDefaultAttendance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Attendance_SLU] [int] NOT NULL,
	[Back_Color] [varchar](50) NULL,
	[Border_Color] [varchar](50) NULL,
	[Text_Color] [varchar](50) NULL,
	[Font_Bold] [tinyint] NULL,
	[Font_Italic] [tinyint] NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
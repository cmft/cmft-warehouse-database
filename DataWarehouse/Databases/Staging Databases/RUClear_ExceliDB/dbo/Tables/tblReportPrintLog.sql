﻿CREATE TABLE [dbo].[tblReportPrintLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient_ID] [int] NOT NULL,
	[RecordID] [int] NOT NULL,
	[RecordType_SLU] [int] NOT NULL,
	[PrintDate] [datetime] NOT NULL,
	[PrintedBy_User_ID] [int] NOT NULL
) ON [PRIMARY]
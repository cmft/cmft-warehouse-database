﻿CREATE TABLE [dbo].[tblApptClinicPhone](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinic_ID] [int] NOT NULL,
	[PhoneNumber] [nvarchar](100) NOT NULL,
	[PhoneType_LU] [int] NOT NULL,
	[Provider_LU] [int] NULL,
	[IsISDN_Bonded] [bit] NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
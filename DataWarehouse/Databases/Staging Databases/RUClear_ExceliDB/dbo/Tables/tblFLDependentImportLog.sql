﻿CREATE TABLE [dbo].[tblFLDependentImportLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](128) NOT NULL,
	[SourceSiteID] [int] NOT NULL,
	[SourceItemID] [int] NOT NULL,
	[SourceItemName] [nvarchar](256) NULL,
	[Parent_SourceItemID] [int] NOT NULL,
	[TargetSiteID] [int] NOT NULL,
	[TargetItemID] [int] NOT NULL,
	[TargetItemName] [nvarchar](256) NULL,
	[Parent_TargetItemID] [int] NOT NULL,
	[ImportedDate] [datetime] NOT NULL,
	[ImportedBy_User_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblRunScript](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventType_SLU] [int] NULL,
	[SysModule_ID] [int] NULL,
	[SourceForm_ID] [int] NULL,
	[RunScript_ID] [int] NULL,
	[SequenceNo] [tinyint] NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [DF_tblRunScript_IsActive]  DEFAULT (1),
	[LastModified_User_ID] [int] NOT NULL CONSTRAINT [DF_tblRunScript_LastModified_User_ID]  DEFAULT (10),
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblRunScript_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblUDLinkedLookup](
	[Parent_LU] [int] NOT NULL,
	[Child_LU] [int] NOT NULL,
	[IsProtected] [bit] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
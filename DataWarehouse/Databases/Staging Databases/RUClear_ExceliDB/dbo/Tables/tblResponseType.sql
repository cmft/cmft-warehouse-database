﻿CREATE TABLE [dbo].[tblResponseType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[IsTerminated] [bit] NOT NULL CONSTRAINT [df_tblresponsetype_isterminated]  DEFAULT (0),
	[ResponseTypeCategory_SLU] [int] NOT NULL,
	[MinValidValue] [float] NULL,
	[MaxValidValue] [float] NULL,
	[FloatPrecision] [smallint] NULL,
	[RangeValuesString] [nvarchar](255) NULL,
	[SysTable] [nvarchar](50) NULL,
	[SysField] [nvarchar](50) NULL,
	[RespTypeCalcField_SLU] [int] NULL,
	[GetDataFunction_ID] [int] NULL,
	[IsSilentResponse] [bit] NOT NULL CONSTRAINT [df_tblresponsetype_issilentresponse]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_tblresponsetype_isactive]  DEFAULT (1),
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
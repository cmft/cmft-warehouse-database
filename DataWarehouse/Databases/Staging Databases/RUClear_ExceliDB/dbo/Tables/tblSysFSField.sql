﻿CREATE TABLE [dbo].[tblSysFSField](
	[ID] [int] NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[DisplayOrder] [smallint] NOT NULL,
	[SysFSGroup_ID] [int] NOT NULL,
	[SeeAlso] [tinyint] NOT NULL,
	[IsMultipleRecord] [bit] NOT NULL,
	[IsUDFField] [bit] NOT NULL,
	[IsDocField] [bit] NOT NULL,
	[IsScriptField] [bit] NOT NULL,
	[IsMapField] [bit] NOT NULL CONSTRAINT [DF_tblSysFSField_IsMapField]  DEFAULT (0),
	[IsCAMField] [bit] NOT NULL CONSTRAINT [DF_tblSysFSField_IsCAMField]  DEFAULT (0),
	[SysModule_ID] [int] NULL,
	[SysControl_ID] [int] NULL,
	[IsMandatory] [bit] NOT NULL CONSTRAINT [DF_tblSysFSField_IsMandatory]  DEFAULT (0),
	[IsActive] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblGCVAssociations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GCV_ID] [int] NOT NULL,
	[Associations_SLU] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
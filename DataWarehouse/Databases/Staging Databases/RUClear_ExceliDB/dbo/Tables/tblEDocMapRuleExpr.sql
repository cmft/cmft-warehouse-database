﻿CREATE TABLE [dbo].[tblEDocMapRuleExpr](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EDocMapRule_ID] [int] NOT NULL,
	[DisplayOrder] [int] NULL,
	[Expression] [varchar](2000) NULL,
	[Comments] [varchar](200) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
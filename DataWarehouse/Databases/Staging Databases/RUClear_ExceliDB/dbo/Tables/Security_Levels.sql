﻿CREATE TABLE [dbo].[Security_Levels](
	[CODE_ID] [int] NOT NULL,
	[CODE_Text] [nvarchar](50) NOT NULL,
	[Updt_Dt_Tm] [datetime] NOT NULL,
	[Updt_User] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_security_levels_isactive]  DEFAULT (1)
) ON [PRIMARY]
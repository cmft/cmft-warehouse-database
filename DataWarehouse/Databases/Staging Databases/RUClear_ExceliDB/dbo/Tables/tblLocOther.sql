﻿CREATE TABLE [dbo].[tblLocOther](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Facility_Id] [int] NOT NULL,
	[Name] [nvarchar](30) NOT NULL
) ON [PRIMARY]
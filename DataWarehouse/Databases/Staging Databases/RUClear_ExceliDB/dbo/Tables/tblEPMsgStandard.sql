﻿CREATE TABLE [dbo].[tblEPMsgStandard](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[Description] [nvarchar](500) NULL,
	[ModelType] [char](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Ax_CAR_TotalPatientsModified](
	[CustomerCode] [varchar](50) NULL,
	[TotalChanges] [int] NULL,
	[ModifiedYear] [int] NULL,
	[ModifiedMonth] [int] NULL,
	[Month] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblLocFacility](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](70) NOT NULL,
	[Location_Id] [int] NOT NULL,
	[FacilityType_SLU] [int] NOT NULL,
	[FacilityCategory_SLU] [int] NOT NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
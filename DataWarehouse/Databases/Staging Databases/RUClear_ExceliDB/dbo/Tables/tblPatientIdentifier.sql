﻿CREATE TABLE [dbo].[tblPatientIdentifier](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NULL,
	[Identifier_LU] [int] NULL,
	[Value] [varchar](30) NULL,
	[Identifier_Type] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
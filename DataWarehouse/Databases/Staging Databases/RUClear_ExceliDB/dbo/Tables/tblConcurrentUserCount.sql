﻿CREATE TABLE [dbo].[tblConcurrentUserCount](
	[Count] [numeric](19, 0) NULL,
	[TimeRecorded] [datetime] NULL
) ON [PRIMARY]
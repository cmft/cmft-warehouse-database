﻿CREATE TABLE [dbo].[tblUDFGridPreFilter](
	[ID] [int] NOT NULL,
	[CustomForm_ID] [int] NULL,
	[CustomFormControl_ID] [int] NULL,
	[CustomControlType_ID] [int] NULL,
	[MappedControl_ID] [int] NULL,
	[MappedControl_Name] [varchar](128) NULL,
	[DisplayCondition] [varchar](2048) NULL,
	[SQLCondition] [varchar](2048) NULL,
	[FirstCondition] [varchar](1856) NULL,
	[LogicalOperator] [varchar](4) NULL,
	[SecondCondition] [varchar](1856) NULL,
	[ControlDataType] [int] NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
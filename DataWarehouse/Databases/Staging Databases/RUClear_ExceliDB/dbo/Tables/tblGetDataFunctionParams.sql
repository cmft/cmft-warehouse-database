﻿CREATE TABLE [dbo].[tblGetDataFunctionParams](
	[GetDataFunction_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Direction] [nvarchar](50) NULL,
	[DataType] [nvarchar](50) NULL,
	[ParamPosition] [tinyint] NOT NULL CONSTRAINT [DF_tblGetDataFunctionParams_ParamPosition]  DEFAULT (1),
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblDictionary](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DictName] [varchar](50) NULL,
	[NoOfColumns] [int] NULL,
	[NoOfRows] [int] NULL,
	[DefaultSearch] [varchar](50) NULL,
	[ColumnDelimiter] [char](1) NULL,
	[IsDataImported] [bit] NULL,
	[IsFirstRowHeader] [bit] NULL,
	[IsActive] [tinyint] NULL CONSTRAINT [DF_tblDictionary_IsActive]  DEFAULT (1),
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_tblDictionary_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
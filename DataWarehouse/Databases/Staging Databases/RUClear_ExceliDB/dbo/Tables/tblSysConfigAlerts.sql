﻿CREATE TABLE [dbo].[tblSysConfigAlerts](
	[LoggedDate] [datetime] NOT NULL,
	[RunTimeFacts] [varchar](256) NOT NULL,
	[ClientIP] [varchar](20) NOT NULL,
	[ClientName] [varchar](100) NOT NULL,
	[Usr_id] [int] NOT NULL
) ON [PRIMARY]
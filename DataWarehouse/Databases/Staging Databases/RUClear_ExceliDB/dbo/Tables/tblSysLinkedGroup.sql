﻿CREATE TABLE [dbo].[tblSysLinkedGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentGroup_SLU] [int] NOT NULL,
	[ChildGroup_SLU] [int] NOT NULL,
	[IsProtected] [tinyint] NOT NULL,
	[Isactive] [tinyint] NOT NULL,
	[LastModified_USer_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
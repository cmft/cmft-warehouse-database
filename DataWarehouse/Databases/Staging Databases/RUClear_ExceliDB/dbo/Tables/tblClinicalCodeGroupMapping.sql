﻿CREATE TABLE [dbo].[tblClinicalCodeGroupMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClinicalCodeGroup_ID] [int] NOT NULL,
	[Code] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[Ranking] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
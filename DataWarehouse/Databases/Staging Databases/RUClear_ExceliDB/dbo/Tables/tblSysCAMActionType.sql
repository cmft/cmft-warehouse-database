﻿CREATE TABLE [dbo].[tblSysCAMActionType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActionType] [varchar](200) NOT NULL,
	[ActionClassification] [int] NOT NULL CONSTRAINT [DF_tblSysCAMActionType_ActionClassification]  DEFAULT (3),
	[LibFormMode] [int] NOT NULL,
	[Lookup_ID] [int] NOT NULL CONSTRAINT [DF_tblSysCAMActionType_Lookup_ID]  DEFAULT (0),
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
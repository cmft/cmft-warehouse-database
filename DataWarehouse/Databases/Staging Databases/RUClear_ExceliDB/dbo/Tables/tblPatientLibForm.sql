﻿CREATE TABLE [dbo].[tblPatientLibForm](
	[FormLibItem_ID] [int] NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[AddedToPatient_Date] [datetime] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
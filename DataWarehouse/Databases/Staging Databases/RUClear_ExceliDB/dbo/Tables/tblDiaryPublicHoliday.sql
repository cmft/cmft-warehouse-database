﻿CREATE TABLE [dbo].[tblDiaryPublicHoliday](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[holidaydate] [datetime] NULL,
	[IsActive] [tinyint] NULL,
	[Description] [char](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblReadCodeMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Group_LU] [int] NOT NULL,
	[SubGroup_LU] [int] NOT NULL,
	[ReadCode] [nvarchar](32) NOT NULL,
	[Description] [nvarchar](128) NULL,
	[ListPosition] [int] NULL,
	[Isactive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
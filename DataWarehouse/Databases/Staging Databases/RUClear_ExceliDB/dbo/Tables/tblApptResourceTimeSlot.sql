﻿CREATE TABLE [dbo].[tblApptResourceTimeSlot](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Appt_ID] [int] NOT NULL,
	[Resource_ID] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[IsActive] [tinyint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSysSupportQuery](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateofQuery] [datetime] NOT NULL,
	[Name] [varchar](300) NOT NULL,
	[Patient_IDs] [varchar](1000) NOT NULL,
	[Comments] [varchar](1000) NULL
) ON [PRIMARY]
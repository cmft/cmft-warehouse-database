﻿CREATE TABLE [dbo].[Ax_CAR_TotalModuleActivity](
	[CustomerCode] [varchar](50) NULL,
	[Caption] [varchar](255) NULL,
	[SessionYear] [int] NULL,
	[SessionMonth] [int] NULL,
	[Month] [varchar](50) NULL,
	[Total] [int] NULL
) ON [PRIMARY]
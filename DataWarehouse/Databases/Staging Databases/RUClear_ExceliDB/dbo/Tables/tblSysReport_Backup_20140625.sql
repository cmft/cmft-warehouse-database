﻿CREATE TABLE [dbo].[tblSysReport_Backup_20140625](
	[ID] [int] IDENTITY(100000,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[DisplayName] [varchar](150) NULL,
	[ShowGraph] [bit] NULL,
	[ListPosition] [smallint] NULL,
	[GroupName] [varchar](50) NULL,
	[DBSource] [varchar](50) NULL,
	[SQLQuery] [text] NULL,
	[DBSourceType] [varchar](50) NULL,
	[DBPostPrint] [varchar](50) NULL,
	[IsExport] [varchar](10) NULL,
	[DBExport] [varchar](50) NULL,
	[MDBOption] [varchar](50) NULL,
	[TableDefs] [text] NULL,
	[IsShowReport] [tinyint] NULL,
	[PrinterOrientation] [tinyint] NULL,
	[PrinterPaperSize] [int] NULL,
	[AllowDeceasedPatients] [int] NULL,
	[IsAxsysReport] [bit] NULL,
	[IsSMSEnabled] [bit] NOT NULL,
	[ReportDB_SLU] [int] NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[GP_Branch](
	[RGPBP_ID] [int] IDENTITY(1,1) NOT NULL,
	[LOC_ID] [int] NULL,
	[Surgery_ID] [varchar](10) NULL,
	[RGPBP_Addr1] [varchar](35) NULL,
	[RGPBP_Addr2] [varchar](35) NULL,
	[RGPBP_County] [nvarchar](30) NULL,
	[RGPBP_City] [varchar](35) NULL,
	[RGPBP_PCode] [nvarchar](50) NULL,
	[RGPBP_Country_Code] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModIFied_User_ID] [int] NOT NULL,
	[LastModIFiedDate] [datetime] NOT NULL
) ON [PRIMARY]
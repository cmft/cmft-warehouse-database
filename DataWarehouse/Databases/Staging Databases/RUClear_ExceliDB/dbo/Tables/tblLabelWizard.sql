﻿CREATE TABLE [dbo].[tblLabelWizard](
	[ID] [int] IDENTITY(10000,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[TemplateType] [int] NOT NULL,
	[LabelObject] [image] NOT NULL,
	[PrintOptionsType] [int] NOT NULL,
	[LabelPrint] [tinyint] NOT NULL,
	[NoOfRows] [int] NOT NULL,
	[NoOfColumns] [int] NOT NULL,
	[RemoveBlankLines] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
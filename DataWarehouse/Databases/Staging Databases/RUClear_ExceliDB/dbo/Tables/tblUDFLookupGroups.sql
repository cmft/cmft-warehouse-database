﻿CREATE TABLE [dbo].[tblUDFLookupGroups](
	[ID] [int] IDENTITY(500000,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[LookupValue] [nvarchar](100) NOT NULL,
	[ListPosition] [smallint] NOT NULL,
	[BrowserType_SLU] [int] NULL,
	[CTCodeSet_SLU] [int] NULL,
	[IsCoded] [bit] NOT NULL CONSTRAINT [DF_tblUDFLookupGroups_IsCoded]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblUDFMedia](
	[UDF_ID] [int] NULL,
	[Control_ID] [int] NULL,
	[UseMode] [smallint] NULL,
	[COB_ID] [int] NULL,
	[FormLibItem_ID] [int] NULL,
	[Picture] [image] NULL,
	[Annotations] [image] NULL,
	[Preview] [image] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
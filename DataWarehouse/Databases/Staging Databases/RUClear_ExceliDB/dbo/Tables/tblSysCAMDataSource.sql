﻿CREATE TABLE [dbo].[tblSysCAMDataSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RecipientType_LU] [int] NOT NULL,
	[TableName] [varchar](200) NOT NULL,
	[FieldName] [varchar](200) NOT NULL,
	[SourceFilter] [varchar](200) NOT NULL,
	[CategoryTableName] [varchar](200) NOT NULL,
	[CategoryFieldName] [varchar](200) NOT NULL,
	[Criteria] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_tblCAMActionDataSource_IsActive]  DEFAULT (1),
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblFormDeptFac](
	[FormLibItem_ID] [int] NOT NULL,
	[Department_LU] [int] NOT NULL,
	[Facility_LU] [int] NOT NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSysScriptRunner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient_ID] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[Script_ID] [int] NOT NULL,
	[Status] [tinyint] NOT NULL
) ON [PRIMARY]
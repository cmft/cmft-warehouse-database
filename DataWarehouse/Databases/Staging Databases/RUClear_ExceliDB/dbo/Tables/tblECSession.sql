﻿CREATE TABLE [dbo].[tblECSession](
	[ID] [varchar](70) NOT NULL,
	[USR_ID] [int] NULL,
	[MachineName] [varchar](50) NULL,
	[MachineIP] [varchar](50) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[TimeLastSeenConnected] [datetime] NULL,
	[SystemLogout] [bit] NULL,
	[LoggedFrom] [nvarchar](10) NULL
) ON [PRIMARY]
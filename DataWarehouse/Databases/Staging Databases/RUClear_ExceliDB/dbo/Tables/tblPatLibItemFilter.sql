﻿CREATE TABLE [dbo].[tblPatLibItemFilter](
	[PAT_ID] [int] NOT NULL,
	[FLIPositionIdentifier] [nvarchar](2000) NOT NULL,
	[FLI_ID] [int] NOT NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
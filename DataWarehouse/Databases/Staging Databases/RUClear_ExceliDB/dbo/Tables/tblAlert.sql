﻿CREATE TABLE [dbo].[tblAlert](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserSession_ID] [int] NOT NULL,
	[Action_ID] [int] NOT NULL,
	[DateTimeCreated] [datetime] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ClearedText] [nvarchar](255) NULL,
	[IsActive] [tinyint] NOT NULL,
	[ClearedBy_User_ID] [int] NULL,
	[DateTimeCleared] [datetime] NULL
) ON [PRIMARY]
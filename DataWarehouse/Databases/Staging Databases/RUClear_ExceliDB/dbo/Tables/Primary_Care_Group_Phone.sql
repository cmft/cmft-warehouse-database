﻿CREATE TABLE [dbo].[Primary_Care_Group_Phone](
	[PCGP_ID] [int] IDENTITY(1,1) NOT NULL,
	[PCG_ID] [int] NOT NULL,
	[PCGP_Phone_No] [nvarchar](100) NOT NULL,
	[PCGP_Phone_Type_Code] [int] NOT NULL,
	[PCGP_Provider_Code] [int] NULL,
	[IsISDN_Bonded] [bit] NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatientMergeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Record_ID] [int] NOT NULL,
	[TableName] [char](256) NOT NULL,
	[PrePat_ID] [int] NOT NULL,
	[PostPat_ID] [int] NOT NULL,
	[MergeLog_ID] [int] NULL,
	[IsMerge] [tinyint] NULL,
	[Reason_LU] [int] NULL,
	[Comments] [varchar](500) NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCAMExpressionPool](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RulePool_ID] [int] NOT NULL,
	[Rule_Id] [char](10) NOT NULL,
	[Pat_ID] [int] NOT NULL,
	[Status_LU] [int] NOT NULL,
	[StartProcessTime] [datetime] NULL,
	[EndProcessTime] [datetime] NULL
) ON [PRIMARY]
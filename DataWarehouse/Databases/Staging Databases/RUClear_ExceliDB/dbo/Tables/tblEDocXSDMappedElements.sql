﻿CREATE TABLE [dbo].[tblEDocXSDMappedElements](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EDocMapRule_ID] [int] NOT NULL,
	[MapType] [int] NOT NULL,
	[Targetpath] [varchar](1000) NOT NULL,
	[MaxOccurs] [int] NULL CONSTRAINT [DF_tblEDocXSDMappedElements_MaxOccurs]  DEFAULT (0),
	[ControlType] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatientMedia](
	[Cob_ID] [int] NOT NULL,
	[Media] [image] NOT NULL,
	[Preview] [image] NULL,
	[Compression] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
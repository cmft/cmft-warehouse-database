﻿CREATE TABLE [dbo].[tblDictationMatch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SysControl_ID] [int] NULL,
	[LevelNo] [int] NULL,
	[SysModule_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
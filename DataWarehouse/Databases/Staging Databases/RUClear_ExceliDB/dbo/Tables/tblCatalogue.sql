﻿CREATE TABLE [dbo].[tblCatalogue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[ParentCatalogue_ID] [int] NULL,
	[IsSet] [bit] NOT NULL,
	[User_ID] [int] NOT NULL,
	[ListPosition] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[IsComplete] [bit] NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
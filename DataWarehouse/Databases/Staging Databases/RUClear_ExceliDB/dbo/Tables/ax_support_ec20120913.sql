﻿CREATE TABLE [dbo].[ax_support_ec20120913](
	[SequenceNumber] [numeric](18, 0) NULL,
	[ModuleId] [int] NULL,
	[ModuleName] [nvarchar](50) NULL,
	[ECSession_ID] [varchar](70) NOT NULL,
	[Pat_ID] [int] NULL,
	[EventType_SLU] [varchar](50) NOT NULL,
	[EventDetails] [ntext] NULL,
	[RecordId] [int] NULL,
	[TimeLogged] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
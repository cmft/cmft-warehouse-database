﻿CREATE TABLE [dbo].[tblScriptSection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Script_ID] [int] NOT NULL,
	[SectionNo] [smallint] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_tblscriptsection_isactive]  DEFAULT (1),
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [df_tblscriptsection_lastmodifieddate]  DEFAULT (getdate()),
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
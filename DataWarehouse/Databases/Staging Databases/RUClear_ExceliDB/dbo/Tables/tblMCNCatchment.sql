﻿CREATE TABLE [dbo].[tblMCNCatchment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MCN_ID] [int] NOT NULL,
	[EntryID] [int] NOT NULL,
	[MCNCatchmentType_SLU] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
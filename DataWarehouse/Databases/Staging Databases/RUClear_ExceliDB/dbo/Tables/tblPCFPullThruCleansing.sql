﻿CREATE TABLE [dbo].[tblPCFPullThruCleansing](
	[SurNameID] [int] NULL,
	[ForeNameID] [int] NULL,
	[ID] [int] NULL,
	[Patient_ID] [int] NULL,
	[SurName] [varchar](4000) NULL,
	[ForeName] [varchar](4000) NULL
) ON [PRIMARY]
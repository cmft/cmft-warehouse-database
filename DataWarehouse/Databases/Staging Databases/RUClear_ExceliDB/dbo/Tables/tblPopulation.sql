﻿CREATE TABLE [dbo].[tblPopulation](
	[HB_ID] [int] NULL,
	[HB_Name] [nvarchar](255) NULL,
	[Year] [int] NULL,
	[Male_Population] [int] NULL,
	[Female_Population] [int] NULL,
	[Total_Population] [int] NULL
) ON [PRIMARY]
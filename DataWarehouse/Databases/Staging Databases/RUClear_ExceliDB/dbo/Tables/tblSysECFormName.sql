﻿CREATE TABLE [dbo].[tblSysECFormName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SysModule_ID] [int] NOT NULL,
	[ClientType_SLU] [int] NOT NULL,
	[ECFormName] [nvarchar](256) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
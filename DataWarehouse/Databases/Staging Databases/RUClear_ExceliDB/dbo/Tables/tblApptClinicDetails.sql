﻿CREATE TABLE [dbo].[tblApptClinicDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinic_ID] [int] NOT NULL,
	[HealthBoard_ID] [int] NULL,
	[Location_ID] [int] NULL,
	[ServiceType_LU] [int] NULL,
	[ClinicType_LU] [int] NULL,
	[ClinicGroup_LU] [int] NULL,
	[StaffRoles] [ntext] NULL,
	[ClinicSignatory1] [nvarchar](256) NULL,
	[ClinicSignatory2] [nvarchar](256) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
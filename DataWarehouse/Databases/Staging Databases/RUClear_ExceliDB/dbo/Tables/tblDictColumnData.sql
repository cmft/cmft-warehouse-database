﻿CREATE TABLE [dbo].[tblDictColumnData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DictColumn_ID] [int] NULL,
	[RowID] [int] NULL,
	[ResultValue] [varchar](256) NULL,
	[Dictionary_Id] [int] NOT NULL CONSTRAINT [DF_tblDictColumnData_Dictionary_Id]  DEFAULT (0),
	[IsActive] [tinyint] NULL CONSTRAINT [DF_tblDictColumnData_IsActive]  DEFAULT (1),
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_tblDictColumnData_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
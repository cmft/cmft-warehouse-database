﻿CREATE TABLE [dbo].[tblConfPatAudit](
	[ConfInstance_ID] [int] NOT NULL,
	[ConfInstPerson_ID] [int] NOT NULL,
	[ConfPatSurname] [nvarchar](100) NOT NULL,
	[ConfPatForename] [nvarchar](100) NOT NULL,
	[ConfPatLocation] [nvarchar](200) NULL,
	[ConfPatDOB] [datetime] NULL,
	[ConfPatSex] [varchar](125) NULL,
	[ConfPatPrimaryID] [varchar](30) NULL,
	[ConfPatSecondaryID] [varchar](30) NULL,
	[ConfPatListPosition] [int] NOT NULL,
	[OptField1value] [varchar](100) NULL,
	[OptField2value] [varchar](100) NULL,
	[OptField3value] [varchar](100) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
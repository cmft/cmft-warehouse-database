﻿CREATE TABLE [dbo].[tblTokenParameter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Token_ID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SysFSField_ID] [int] NULL,
	[Direction] [tinyint] NOT NULL,
	[DataType] [nvarchar](128) NOT NULL,
	[ParamPosition] [tinyint] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
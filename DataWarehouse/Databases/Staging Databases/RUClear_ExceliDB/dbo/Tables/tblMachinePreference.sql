﻿CREATE TABLE [dbo].[tblMachinePreference](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MachineName] [nvarchar](100) NOT NULL,
	[OtherStoreAccessible] [bit] NOT NULL CONSTRAINT [DF_tblMachinePreference_OtherStoreAccessible]  DEFAULT (0),
	[EPDBServerLocation] [nvarchar](250) NULL,
	[EPDBVersion] [nvarchar](50) NULL,
	[EPWebserviceLocation] [nvarchar](250) NULL,
	[EPLogTrace] [smallint] NULL CONSTRAINT [DF_tblMachinePreference_EPLogTrace]  DEFAULT (0),
	[EPDefaultPASServer] [int] NULL,
	[EPWebServiceDebug] [nvarchar](500) NULL,
	[EPDBTrainingServerName] [nvarchar](250) NULL,
	[AdhocExePath] [nvarchar](500) NULL,
	[IsActive] [tinyint] NOT NULL,
	[Lastmodified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
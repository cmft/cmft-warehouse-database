﻿CREATE TABLE [dbo].[tblEDocMapRuleCompiledQueries](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EDocMapRule_ID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[GroupFields] [varchar](2000) NOT NULL,
	[FieldType_SLU] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
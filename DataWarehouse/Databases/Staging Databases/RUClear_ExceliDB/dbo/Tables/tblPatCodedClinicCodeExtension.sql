﻿CREATE TABLE [dbo].[tblPatCodedClinicCodeExtension](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClinicalCode_ID] [int] NULL,
	[PatClinicalCode_ID] [int] NULL,
	[Extension] [nvarchar](256) NULL,
	[ExtensionType] [int] NULL,
	[Sequence] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
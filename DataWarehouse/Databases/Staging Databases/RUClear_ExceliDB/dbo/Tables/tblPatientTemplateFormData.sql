﻿CREATE TABLE [dbo].[tblPatientTemplateFormData](
	[ID] [int] NOT NULL,
	[PatientCustomForm_ID] [int] NOT NULL,
	[CustomFormControl_ID] [int] NOT NULL,
	[ResultValue] [nvarchar](4000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblDocPrintLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PrintLogID] [int] NULL,
	[Rec_Type] [int] NULL,
	[Rec_ID] [int] NULL
) ON [PRIMARY]
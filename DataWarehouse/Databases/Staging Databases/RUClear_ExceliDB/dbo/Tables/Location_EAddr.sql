﻿CREATE TABLE [dbo].[Location_EAddr](
	[LOCE_ID] [int] IDENTITY(1,1) NOT NULL,
	[LOC_ID] [int] NULL,
	[LOCE_EAddr] [nvarchar](50) NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
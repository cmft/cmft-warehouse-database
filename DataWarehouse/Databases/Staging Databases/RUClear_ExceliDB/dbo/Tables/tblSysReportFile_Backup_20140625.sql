﻿CREATE TABLE [dbo].[tblSysReportFile_Backup_20140625](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[SysReport_ID] [int] NOT NULL,
	[ReportFile] [image] NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatientClinicalNoteMedia](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatClinicalNote_ID] [int] NOT NULL,
	[Media] [image] NULL,
	[FormLibItem_ID] [int] NULL,
	[FileName] [nvarchar](200) NULL,
	[PreviewImage] [image] NULL,
	[Annotation] [image] NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[Lastmodified_User_ID] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblDocuments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[ShortDesc] [nvarchar](150) NULL,
	[DocType_SLU] [int] NULL,
	[DocObject] [image] NULL,
	[ShortCode] [char](10) NULL,
	[IsActive] [tinyint] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModified_User_ID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
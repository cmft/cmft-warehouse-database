﻿CREATE TABLE [dbo].[tblNotificationTemplateRecipient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateDetails_ID] [int] NOT NULL,
	[GroupType_LU] [int] NOT NULL,
	[GroupRecordID] [int] NULL,
	[RecipientID] [int] NOT NULL,
	[Acknowledge] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
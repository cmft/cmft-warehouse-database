﻿CREATE TABLE [dbo].[tblUDFControlLinks](
	[RemoteFormLibID] [int] NULL,
	[LocalFormLibID] [int] NULL,
	[RemoteControlID] [int] NULL,
	[LocalControlID] [int] NULL,
	[RemoteUDLID] [int] NULL,
	[LocalUDLID] [int] NULL
) ON [PRIMARY]
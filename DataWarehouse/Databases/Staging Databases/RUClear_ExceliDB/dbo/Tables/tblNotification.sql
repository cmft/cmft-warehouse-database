﻿CREATE TABLE [dbo].[tblNotification](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Rule_ID] [int] NULL,
	[NotificationDate] [datetime] NOT NULL,
	[NotificationTemplate_ID] [int] NOT NULL,
	[NotificationStatus_LU] [int] NOT NULL,
	[RepetitionTimes] [int] NOT NULL,
	[Nextrundatetime] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEDocMap](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[EDocMapDirection_SLU] [int] NOT NULL,
	[EDocCategory_SLU] [int] NOT NULL,
	[EDocMapStatus_SLU] [int] NOT NULL,
	[RootXSD_EDocRefObject_ID] [int] NOT NULL,
	[XSL_EDocRefObject_ID] [int] NULL,
	[TemplateXMLFile] [image] NOT NULL,
	[Comments] [varchar](200) NULL,
	[KeyWords] [varchar](100) NULL,
	[Propertystring] [varchar](1000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
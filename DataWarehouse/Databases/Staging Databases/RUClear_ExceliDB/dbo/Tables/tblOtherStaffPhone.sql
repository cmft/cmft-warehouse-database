﻿CREATE TABLE [dbo].[tblOtherStaffPhone](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OtherStaff_ID] [int] NOT NULL,
	[PhoneNo] [nvarchar](100) NOT NULL,
	[PhoneType_LU] [int] NOT NULL,
	[Provider_Code] [int] NULL,
	[IsISDNBonded] [bit] NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient_GP](
	[PGP_ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NULL,
	[LOC_ID] [int] NULL,
	[Branch_ID] [int] NULL,
	[CLN_ID] [int] NULL,
	[PGP_GP_Status_Code] [int] NULL,
	[PGP_Date_of_Removal] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[IsUsuallySeenGP] [bit] NULL CONSTRAINT [DF_Patient_GP_IsUsuallySeenGP]  DEFAULT (0),
	[IsCC] [bit] NULL CONSTRAINT [DF_Patient_GP_IsCC]  DEFAULT (0),
	[IsAddresse] [bit] NULL CONSTRAINT [DF_Patient_GP_IsAddresse]  DEFAULT (0),
	[Optional1] [bit] NULL CONSTRAINT [DF_Patient_GP_Optional1]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
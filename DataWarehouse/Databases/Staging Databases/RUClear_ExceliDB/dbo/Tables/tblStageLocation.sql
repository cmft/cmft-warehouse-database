﻿CREATE TABLE [dbo].[tblStageLocation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Stage_ID] [int] NOT NULL,
	[Location_SLU] [int] NULL,
	[Location_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
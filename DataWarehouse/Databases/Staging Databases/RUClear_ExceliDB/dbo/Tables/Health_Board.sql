﻿CREATE TABLE [dbo].[Health_Board](
	[HB_ID] [int] IDENTITY(1,1) NOT NULL,
	[HB_Name] [nvarchar](50) NULL,
	[HB_ID_Number] [nvarchar](50) NULL,
	[HB_Contact_Name] [nvarchar](50) NULL,
	[HB_Contact_Designation] [nvarchar](50) NULL,
	[HB_Addr1] [varchar](35) NULL,
	[HB_Addr2] [varchar](35) NULL,
	[HB_County] [nvarchar](30) NULL,
	[HB_City] [varchar](35) NULL,
	[HB_PCode] [nvarchar](50) NULL,
	[HB_Country_Code] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModIFied_User_ID] [int] NOT NULL,
	[LastModIFiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblReferralProtocol](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Caption] [nvarchar](100) NULL,
	[RefProtocol_ID] [int] NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatientOrganDonar](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NULL,
	[PATOrganDonorDate] [datetime] NOT NULL,
	[PATOrganDonor_LU] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
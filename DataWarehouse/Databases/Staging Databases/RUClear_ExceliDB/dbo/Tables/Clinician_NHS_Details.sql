﻿CREATE TABLE [dbo].[Clinician_NHS_Details](
	[NHSC_ID] [int] IDENTITY(1,1) NOT NULL,
	[CLN_ID] [int] NULL,
	[NHSC_GMC_Temp_Registration_No] [nvarchar](50) NULL,
	[NHSC_GMC_Temp_Registration_Date] [datetime] NULL,
	[NHSC_GMC_Perm_Registration_No] [nvarchar](50) NULL,
	[NHSC_GMC_Perm_Registration_Date] [datetime] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
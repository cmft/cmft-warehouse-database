﻿CREATE TABLE [dbo].[tblLabTest](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DisplayPriority] [int] NULL,
	[AccessSecurityLevel] [int] NULL,
	[ShortName] [varchar](25) NULL,
	[LongName] [varchar](100) NULL,
	[Department_LU] [int] NULL,
	[ResultStore_SLU] [int] NULL,
	[Units] [varchar](100) NULL,
	[SpecimenType_LU] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
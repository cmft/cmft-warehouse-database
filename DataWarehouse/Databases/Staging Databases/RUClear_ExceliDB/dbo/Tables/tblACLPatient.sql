﻿CREATE TABLE [dbo].[tblACLPatient](
	[Pat_ID] [int] NOT NULL,
	[ACLGroup_ID] [int] NOT NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
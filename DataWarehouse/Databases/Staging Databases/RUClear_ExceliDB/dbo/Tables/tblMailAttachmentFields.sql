﻿CREATE TABLE [dbo].[tblMailAttachmentFields](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MailAttachments_Id] [int] NOT NULL,
	[FieldName] [nvarchar](250) NOT NULL,
	[FieldType] [nvarchar](250) NOT NULL,
	[FieldValue] [nvarchar](250) NULL
) ON [PRIMARY]
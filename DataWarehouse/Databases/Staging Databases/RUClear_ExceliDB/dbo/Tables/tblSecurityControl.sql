﻿CREATE TABLE [dbo].[tblSecurityControl](
	[SysModule_ID] [int] NOT NULL,
	[SysControl_ID] [int] NOT NULL,
	[SecurityLevel_ID] [int] NOT NULL,
	[IsSystemControl] [bit] NULL CONSTRAINT [df_tblsecuritycontrol_issystemcontrol]  DEFAULT (1),
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [df_tblsecuritycontrol_lastmodifieddate]  DEFAULT (getdate())
) ON [PRIMARY]
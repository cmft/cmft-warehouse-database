﻿CREATE TABLE [dbo].[tbl_cust_regions](
	[Area] [nvarchar](255) NULL,
	[Postcode1] [nvarchar](255) NULL,
	[Postcode2] [float] NULL,
	[Postcode] [varchar](10) NULL,
	[City] [varchar](100) NULL
) ON [PRIMARY]
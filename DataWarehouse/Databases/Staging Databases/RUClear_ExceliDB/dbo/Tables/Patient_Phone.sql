﻿CREATE TABLE [dbo].[Patient_Phone](
	[PPH_ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[PPH_Phone_No] [nvarchar](100) NOT NULL,
	[PPH_Phone_Type_Code] [int] NOT NULL,
	[PPH_Provider_Code] [int] NULL,
	[IsISDN_Bonded1] [bit] NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
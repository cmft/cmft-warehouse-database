﻿CREATE TABLE [dbo].[Clinician_Phone](
	[CLNP_ID] [int] IDENTITY(1,1) NOT NULL,
	[CLN_ID] [int] NULL,
	[CLNP_Phone_No] [nvarchar](100) NULL,
	[CLNP_Phone_Type_Code] [int] NULL,
	[CLNP_Provider_Code] [int] NULL,
	[IsISDN_Bonded] [bit] NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEPErrorData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [char](256) NOT NULL,
	[ImportModule_LU] [int] NOT NULL,
	[ErrorData] [varchar](1000) NULL,
	[IsImportedAgain] [tinyint] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
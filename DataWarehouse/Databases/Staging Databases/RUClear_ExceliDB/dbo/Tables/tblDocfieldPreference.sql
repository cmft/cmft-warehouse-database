﻿CREATE TABLE [dbo].[tblDocfieldPreference](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Template_ID] [int] NOT NULL,
	[PreferenceType] [tinyint] NOT NULL CONSTRAINT [DF_tblDocfieldPreference_PreferenceType]  DEFAULT (0),
	[Preference] [varchar](50) NOT NULL,
	[Result] [varchar](2000) NOT NULL
) ON [PRIMARY]
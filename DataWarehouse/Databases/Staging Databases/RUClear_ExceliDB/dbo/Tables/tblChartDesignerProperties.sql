﻿CREATE TABLE [dbo].[tblChartDesignerProperties](
	[ChartDesigner_ID] [int] NOT NULL,
	[PropertyID] [smallint] NOT NULL,
	[ChartDesigner_GroupID] [smallint] NOT NULL,
	[FontColor] [varchar](20) NOT NULL,
	[FontName] [varchar](20) NOT NULL,
	[FontSize] [smallint] NOT NULL,
	[FontStyle] [smallint] NOT NULL,
	[PropertyText] [varchar](40) NOT NULL,
	[TitleRotation] [smallint] NULL,
	[ChartMode] [char](1) NOT NULL,
	[IsActive] [int] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
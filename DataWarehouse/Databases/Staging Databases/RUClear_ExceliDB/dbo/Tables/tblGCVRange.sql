﻿CREATE TABLE [dbo].[tblGCVRange](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GCV_ID] [int] NOT NULL,
	[RangeType] [tinyint] NOT NULL,
	[Gender] [tinyint] NOT NULL,
	[AgeFrom] [tinyint] NULL,
	[AgeTo] [tinyint] NULL,
	[MinValue] [float] NOT NULL,
	[MaxValue] [float] NOT NULL,
	[LowValue] [float] NULL,
	[HighValue] [float] NULL,
	[LowAlert] [float] NULL,
	[HighAlert] [float] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
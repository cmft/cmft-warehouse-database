﻿CREATE TABLE [dbo].[tblExternalData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormLibItem_ID] [int] NOT NULL,
	[FileName] [nvarchar](250) NULL,
	[FileObject] [image] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
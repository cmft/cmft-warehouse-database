﻿CREATE TABLE [dbo].[tblWorkList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduledOrder_ID] [int] NOT NULL,
	[Order_ID] [int] NOT NULL,
	[StartDateTime] [datetime] NULL,
	[State] [int] NULL,
	[StageNumber] [int] NULL,
	[CurrentStageInfo] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
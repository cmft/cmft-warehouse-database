﻿CREATE TABLE [dbo].[tblDictations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[COB_ID] [int] NULL,
	[REF_ID] [int] NULL,
	[TransStatus] [tinyint] NULL,
	[TransLocation] [tinyint] NULL,
	[IsExistingRecord] [tinyint] NULL,
	[TransRecID] [int] NULL
) ON [PRIMARY]
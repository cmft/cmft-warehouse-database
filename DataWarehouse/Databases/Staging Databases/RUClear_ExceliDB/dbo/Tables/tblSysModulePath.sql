﻿CREATE TABLE [dbo].[tblSysModulePath](
	[Id] [int] NOT NULL,
	[SysModule_Id] [int] NOT NULL,
	[TargetSysModule_Id] [int] NOT NULL,
	[Source] [varchar](200) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
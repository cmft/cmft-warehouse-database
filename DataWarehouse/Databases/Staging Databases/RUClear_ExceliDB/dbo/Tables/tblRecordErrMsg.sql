﻿CREATE TABLE [dbo].[tblRecordErrMsg](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RecordInfoID] [int] NOT NULL,
	[Description] [varchar](1024) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEventConditions](
	[ID] [int] NOT NULL,
	[Event_ID] [int] NOT NULL,
	[Condition] [varchar](2000) NULL,
	[EventAction] [varchar](50) NOT NULL,
	[ConditionOrder] [smallint] NOT NULL,
	[ParentConditionID] [int] NULL,
	[ConditionLevel] [smallint] NULL,
	[ConditionDescription] [nvarchar](100) NULL,
	[IsActive] [smallint] NOT NULL
) ON [PRIMARY]
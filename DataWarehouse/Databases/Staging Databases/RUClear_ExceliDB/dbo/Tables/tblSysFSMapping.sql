﻿CREATE TABLE [dbo].[tblSysFSMapping](
	[SysFSField_ID] [int] NOT NULL,
	[SysControl_ID] [int] NOT NULL,
	[IsStatic] [bit] NULL,
	[Description] [varchar](100) NULL
) ON [PRIMARY]
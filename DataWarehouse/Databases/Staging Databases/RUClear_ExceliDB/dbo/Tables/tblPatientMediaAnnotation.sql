﻿CREATE TABLE [dbo].[tblPatientMediaAnnotation](
	[COB_ID] [int] NULL,
	[MediaAnnotation] [image] NULL,
	[PreviewAnnotation] [image] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
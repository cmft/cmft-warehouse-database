﻿CREATE TABLE [dbo].[tblDocTranscription](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [int] NULL,
	[TransDoc_ID] [int] NULL,
	[FieldName] [char](100) NULL
) ON [PRIMARY]
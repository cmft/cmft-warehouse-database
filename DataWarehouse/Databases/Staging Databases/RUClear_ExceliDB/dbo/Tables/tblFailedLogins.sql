﻿CREATE TABLE [dbo].[tblFailedLogins](
	[MachineName] [varchar](50) NULL,
	[MachineIP] [varchar](50) NULL,
	[LoginNameGiven] [varchar](50) NULL,
	[PasswordGiven] [varchar](100) NULL,
	[IsActive] [tinyint] NULL,
	[DateTime] [datetime] NULL
) ON [PRIMARY]
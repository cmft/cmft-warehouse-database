﻿CREATE TABLE [dbo].[tblApptDefaultTimeSlots](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Appt_Type_SLU] [int] NULL,
	[TimeSlot] [int] NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
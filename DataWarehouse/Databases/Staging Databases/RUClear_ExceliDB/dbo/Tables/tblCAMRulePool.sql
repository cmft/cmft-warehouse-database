﻿CREATE TABLE [dbo].[tblCAMRulePool](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Rule_Id] [int] NOT NULL,
	[AuditData_ID] [int] NOT NULL,
	[Status_LU] [int] NOT NULL,
	[StartProcessTime] [datetime] NULL,
	[EndProcessTime] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatientPractice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[LOC_ID] [int] NOT NULL,
	[Branch_ID] [int] NULL,
	[PAT_Prac_FrameID] [tinyint] NOT NULL,
	[CLN_ID] [int] NULL,
	[GP_Status_LU] [int] NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateRemoved] [datetime] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblScriptResponse](
	[UserSession_ID] [int] NOT NULL,
	[SequenceNo] [smallint] NOT NULL,
	[Script_ID] [int] NOT NULL,
	[ScriptSection_SectionNo] [smallint] NOT NULL,
	[Question_ID] [int] NOT NULL,
	[ResponseValue] [varchar](20) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblScriptResponse_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
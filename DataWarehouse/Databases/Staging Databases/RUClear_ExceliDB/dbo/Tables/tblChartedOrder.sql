﻿CREATE TABLE [dbo].[tblChartedOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WorkList_ID] [int] NOT NULL,
	[Stage_ID] [int] NOT NULL,
	[Order_ID] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[CurrentStage] [int] NULL,
	[DateTimeCharted] [datetime] NULL,
	[DateTimeCreated] [datetime] NULL,
	[ChartType] [int] NULL,
	[Comment] [nvarchar](250) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
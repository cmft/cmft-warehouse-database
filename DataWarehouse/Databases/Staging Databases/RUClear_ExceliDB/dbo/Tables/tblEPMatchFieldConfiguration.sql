﻿CREATE TABLE [dbo].[tblEPMatchFieldConfiguration](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EPPatMatchType_ID] [int] NOT NULL,
	[EPPatMatchField_ID] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
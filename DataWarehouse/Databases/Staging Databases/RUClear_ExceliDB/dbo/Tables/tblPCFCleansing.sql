﻿CREATE TABLE [dbo].[tblPCFCleansing](
	[Patient_ID] [int] NULL,
	[ID] [int] NULL,
	[DateTimeCreated] [datetime] NULL,
	[CustomForm_ID] [int] NULL,
	[KeyFieldValue] [nvarchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSysPagerQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SessionID] [int] NOT NULL,
	[PatientID] [int] NOT NULL,
	[StaffID] [int] NOT NULL,
	[StaffType] [tinyint] NOT NULL,
	[IsActive] [tinyint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEPDictionaryRef](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EPstore_SLU] [int] NOT NULL,
	[ItemName] [char](256) NOT NULL,
	[ECLookupCategory_SLU] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
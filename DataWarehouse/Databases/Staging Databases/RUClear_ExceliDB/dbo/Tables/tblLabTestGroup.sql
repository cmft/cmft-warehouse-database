﻿CREATE TABLE [dbo].[tblLabTestGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ShortName] [varchar](25) NULL,
	[LongName] [varchar](100) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
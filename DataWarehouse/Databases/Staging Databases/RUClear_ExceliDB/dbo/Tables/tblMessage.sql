﻿CREATE TABLE [dbo].[tblMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Header] [varchar](500) NULL,
	[Message] [varchar](4000) NULL,
	[LinkRec_ID] [int] NULL,
	[LinkRecType_SLU] [int] NULL,
	[LinkRecParent_ID] [int] NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_tblMessage_DateCreated]  DEFAULT (getdate()),
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
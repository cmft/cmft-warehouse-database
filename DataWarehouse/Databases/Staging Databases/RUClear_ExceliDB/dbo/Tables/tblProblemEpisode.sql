﻿CREATE TABLE [dbo].[tblProblemEpisode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient_ID] [int] NOT NULL,
	[PRD_ID] [int] NULL,
	[EpisodeType_LU] [int] NOT NULL,
	[PatientType_LU] [int] NOT NULL,
	[ReferralDateTime] [datetime] NOT NULL,
	[DischargeDateTime] [datetime] NULL,
	[Referring_Clinician_ID] [int] NULL,
	[Consultant_Clinician_ID] [int] NULL,
	[ClinicalFacility_LU] [int] NULL,
	[SignificantFacility_LU] [int] NULL,
	[PatientCategory_LU] [int] NULL,
	[ReferralReason] [nvarchar](1000) NULL,
	[OverseasVisitorStatus] [int] NULL,
	[DirectAccessPatient] [bit] NULL,
	[DateReferralSent] [datetime] NULL,
	[ReferralRequestedBy_LU] [int] NULL,
	[GPReferralLetterNo] [int] NULL,
	[ReferralSource_LU] [int] NULL,
	[SelfOrOpenAccess] [bit] NULL,
	[ReferralOrg_LU] [int] NULL,
	[ReferralType_LU] [int] NULL,
	[PriorityType_LU] [int] NULL,
	[RefMultiDisciplinaryTeam] [bit] NULL,
	[DecisionToAdmit] [datetime] NULL,
	[ReAdmission] [bit] NULL,
	[AdmissionTypeMain_LU] [int] NULL,
	[AdmissionTypeDetail_LU] [int] NULL,
	[AdmissionTypeReasonMain_LU] [int] NULL,
	[AdmissionTypeReasonDetail_LU] [int] NULL,
	[LegalStatusOnAdmission] [int] NULL,
	[ChronicSickOrDisabled] [bit] NULL,
	[MainCondOnAdmissionText] [nvarchar](150) NULL,
	[MainCondOnAdmissionCode] [nvarchar](50) NULL,
	[DischargeTypeMain_LU] [int] NULL,
	[DischargeTypeDetail_LU] [int] NULL,
	[DischargeToMain_LU] [int] NULL,
	[DischargeToDetail_LU] [int] NULL,
	[PrimaryDischargeDiagCode] [nvarchar](50) NULL,
	[PrimaryDischargeDiagText] [nvarchar](150) NULL,
	[SecondaryDischargeDiagCode1] [nvarchar](50) NULL,
	[SecondaryDischargeDiagText1] [nvarchar](150) NULL,
	[SecondaryDischargeDiagCode2] [nvarchar](50) NULL,
	[SecondaryDischargeDiagText2] [nvarchar](150) NULL,
	[SecondaryDischargeDiagCode3] [nvarchar](50) NULL,
	[SecondaryDischargeDiagText3] [nvarchar](150) NULL,
	[SecondaryDischargeDiagCode4] [nvarchar](50) NULL,
	[SecondaryDischargeDiagText4] [nvarchar](150) NULL,
	[InActivationReason_LU] [int] NULL,
	[IsHistoryAvailable] [tinyint] NULL,
	[ClinicalCodemain] [ntext] NULL,
	[ClinicalCodePrimaryDialog] [ntext] NULL,
	[ClinicalCodeSecondaryDialog1] [ntext] NULL,
	[ClinicalCodeSecondaryDialog2] [ntext] NULL,
	[ClinicalCodeSecondaryDialog3] [ntext] NULL,
	[ClinicalCodeSecondaryDialog4] [ntext] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
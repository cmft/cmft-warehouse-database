﻿CREATE TABLE [dbo].[tblPatientCustomFormData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientCustomForm_ID] [int] NOT NULL,
	[CustomFormControl_ID] [int] NOT NULL,
	[ResultValue] [nvarchar](4000) NOT NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_tblpatientcustomformdata_isactive]  DEFAULT (1),
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblPatientCustomFormData_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
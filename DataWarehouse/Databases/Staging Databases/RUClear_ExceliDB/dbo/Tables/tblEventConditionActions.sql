﻿CREATE TABLE [dbo].[tblEventConditionActions](
	[ID] [int] NOT NULL,
	[EventID] [int] NOT NULL,
	[ConditionID] [int] NOT NULL,
	[ActionFieldID] [int] NOT NULL,
	[ActionProperty] [varchar](50) NOT NULL,
	[ActionPropertyValue] [varchar](2000) NULL,
	[ActionOrder] [smallint] NOT NULL,
	[IsActive] [smallint] NOT NULL
) ON [PRIMARY]
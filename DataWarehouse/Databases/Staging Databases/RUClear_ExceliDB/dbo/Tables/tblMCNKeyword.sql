﻿CREATE TABLE [dbo].[tblMCNKeyword](
	[MCN_ID] [int] NOT NULL,
	[KeyWord] [varchar](80) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
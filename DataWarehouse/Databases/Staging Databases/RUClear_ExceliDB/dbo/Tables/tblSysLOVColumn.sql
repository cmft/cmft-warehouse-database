﻿CREATE TABLE [dbo].[tblSysLOVColumn](
	[ID] [int] IDENTITY(500000,1) NOT NULL,
	[LOV_ID] [int] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[DisplayName] [nvarchar](50) NOT NULL,
	[DisplayPosition] [int] NOT NULL,
	[IsKeyColumn] [bit] NOT NULL,
	[IsDisplayColumn] [tinyint] NOT NULL,
	[ControlType_SLU] [int] NOT NULL,
	[ColumnWidth] [nvarchar](50) NOT NULL,
	[SourceQuery] [nvarchar](200) NULL,
	[ListValues] [nvarchar](200) NULL,
	[DateFormat_SLU] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblSysLOVColumn_LastModifedDate]  DEFAULT (getdate())
) ON [PRIMARY]
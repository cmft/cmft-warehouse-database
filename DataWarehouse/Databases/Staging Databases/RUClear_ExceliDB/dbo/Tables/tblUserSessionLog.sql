﻿CREATE TABLE [dbo].[tblUserSessionLog](
	[UserSession_ID] [int] NOT NULL,
	[SequenceNo] [smallint] NOT NULL,
	[EventType] [nvarchar](50) NOT NULL,
	[EventDesc] [nvarchar](255) NULL,
	[TimeLogged] [datetime] NOT NULL
) ON [PRIMARY]
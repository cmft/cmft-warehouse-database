﻿CREATE TABLE [dbo].[tblLabUserSecurity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Usr_ID] [int] NOT NULL,
	[LabsDepartment_LU] [int] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
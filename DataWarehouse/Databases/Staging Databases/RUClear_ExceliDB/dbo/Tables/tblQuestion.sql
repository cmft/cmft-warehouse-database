﻿CREATE TABLE [dbo].[tblQuestion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseType_ID] [int] NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[Name] [nvarchar](80) NOT NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_tblquestion_isactive]  DEFAULT (1),
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
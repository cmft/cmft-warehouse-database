﻿CREATE TABLE [dbo].[tblScriptRecipient](
	[Script_ID] [int] NOT NULL,
	[Recip_User_ID] [int] NOT NULL,
	[Clinician_EAddr_ID] [int] NULL,
	[Clinician_Phone_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_tblactionrecepient_isactive]  DEFAULT (1),
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblNoticeBoardFields](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Usr_ID] [int] NOT NULL,
	[FieldIDs] [varchar](50) NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
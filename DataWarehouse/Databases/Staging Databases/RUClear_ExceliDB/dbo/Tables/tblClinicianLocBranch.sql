﻿CREATE TABLE [dbo].[tblClinicianLocBranch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CLN_ID] [int] NOT NULL,
	[LOC_ID] [int] NULL,
	[Branch_ID] [int] NULL
) ON [PRIMARY]
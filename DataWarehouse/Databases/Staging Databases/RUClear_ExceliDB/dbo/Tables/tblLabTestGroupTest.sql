﻿CREATE TABLE [dbo].[tblLabTestGroupTest](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LabTest_ID] [int] NOT NULL,
	[LabTestGroup_ID] [int] NULL,
	[DisplayPriority] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
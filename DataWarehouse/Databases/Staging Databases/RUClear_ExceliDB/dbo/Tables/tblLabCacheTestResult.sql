﻿CREATE TABLE [dbo].[tblLabCacheTestResult](
	[ID] [int] NOT NULL,
	[LabCacheInterSample_ID] [int] NOT NULL,
	[TestCode] [varchar](25) NULL,
	[TestName] [char](256) NULL,
	[ResultType] [char](2) NULL,
	[TestResult] [char](256) NULL,
	[TestUnits] [varchar](50) NULL,
	[AbnormalFlag] [tinyint] NULL,
	[NormalRange] [varchar](50) NULL,
	[AbnormalRange] [varchar](50) NULL,
	[TestComments] [varchar](2000) NULL,
	[ReadCode] [char](10) NULL
) ON [PRIMARY]
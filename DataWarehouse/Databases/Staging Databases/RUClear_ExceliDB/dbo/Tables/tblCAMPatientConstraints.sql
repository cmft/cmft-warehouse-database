﻿CREATE TABLE [dbo].[tblCAMPatientConstraints](
	[Rule_ID] [int] NOT NULL,
	[PatientType_LU] [int] NULL,
	[PatientGroup_LU] [int] NULL,
	[PatientCategory] [int] NULL,
	[PatientIDs] [char](50) NULL,
	[PatientSex] [char](20) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEPDictionaryMap](
	[DictionaryRef_ID] [int] NOT NULL,
	[LookupCode] [varchar](256) NOT NULL,
	[Lookup_ID] [int] NULL,
	[ShortDesc] [char](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblFileInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](256) NOT NULL,
	[LocationName] [varchar](256) NULL,
	[Summary] [varchar](256) NULL,
	[DateProcessed] [datetime] NOT NULL
) ON [PRIMARY]
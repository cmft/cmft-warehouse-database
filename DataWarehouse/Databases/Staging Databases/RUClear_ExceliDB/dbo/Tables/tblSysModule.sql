﻿CREATE TABLE [dbo].[tblSysModule](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Caption] [nvarchar](50) NULL,
	[Type] [tinyint] NULL,
	[DisplayOrder] [smallint] NOT NULL,
	[ParentModule_ID] [int] NULL,
	[ParentForm_ID] [int] NULL,
	[ImageName] [nvarchar](50) NULL,
	[IsReportable] [bit] NOT NULL,
	[IsLinkedModule] [bit] NOT NULL,
	[DomainName] [nvarchar](50) NULL,
	[TargetURL] [nvarchar](150) NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_tblsysmodule_isactive]  DEFAULT (1),
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblInPatientLocation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[LOC_ID] [int] NULL,
	[Ward_ID] [int] NULL,
	[Consultant_ID] [int] NULL,
	[AdmissionDate] [datetime] NULL,
	[DischargeDate] [datetime] NULL,
	[AdmissionStatus] [int] NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
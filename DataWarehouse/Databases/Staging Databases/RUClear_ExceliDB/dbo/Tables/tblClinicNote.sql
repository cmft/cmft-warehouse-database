﻿CREATE TABLE [dbo].[tblClinicNote](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinic_LU] [int] NULL,
	[ClinicNoteDate] [datetime] NULL,
	[ClinicNote] [ntext] NULL,
	[IsActive] [tinyint] NULL,
	[LastModifiedUserID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
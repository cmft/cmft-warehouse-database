﻿CREATE TABLE [dbo].[tblMergedMails](
	[ID] [int] NOT NULL,
	[Mail_ID] [int] NOT NULL,
	[TableName] [nvarchar](255) NOT NULL,
	[ColumnName] [nvarchar](255) NOT NULL,
	[RecordID] [int] NOT NULL,
	[SiteID] [int] NOT NULL,
	[DependCount] [int] NOT NULL
) ON [PRIMARY]
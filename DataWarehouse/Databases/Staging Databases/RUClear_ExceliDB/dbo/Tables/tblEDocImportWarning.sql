﻿CREATE TABLE [dbo].[tblEDocImportWarning](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EdocMap_ID] [int] NOT NULL DEFAULT (0),
	[MessageID] [int] NOT NULL DEFAULT (0),
	[MessageSource] [varchar](100) NOT NULL DEFAULT (''),
	[MappingEventType_LU] [int] NULL,
	[MappingEventID] [int] NULL,
	[ImportWarning_LU] [int] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsMailSent] [tinyint] NOT NULL,
	[Severity] [tinyint] NULL,
	[IsActive] [int] NOT NULL,
	[LastModified_User_Id] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSessTempData](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NOT NULL,
	[DataType] [nvarchar](150) NOT NULL,
	[SessionID] [nvarchar](100) NOT NULL,
	[Data] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
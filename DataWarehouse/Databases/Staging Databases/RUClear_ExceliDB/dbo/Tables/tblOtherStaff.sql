﻿CREATE TABLE [dbo].[tblOtherStaff](
	[ID] [int] IDENTITY(100,1) NOT NULL,
	[Title_LU] [int] NULL,
	[Forename] [nvarchar](20) NULL,
	[Surname] [nvarchar](30) NULL,
	[DOB] [datetime] NULL,
	[Designation] [nvarchar](100) NULL,
	[Department_LU] [int] NOT NULL,
	[JobTitle] [nvarchar](100) NULL,
	[LOC_ID] [int] NULL,
	[CallNotes] [varchar](2150) NULL,
	[Nationality] [nvarchar](50) NULL,
	[SpProfessionalBodyNo] [nvarchar](50) NULL,
	[Comments] [nvarchar](300) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
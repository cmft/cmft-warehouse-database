﻿CREATE TABLE [dbo].[tblCAMAlertSubscription](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Rule_ID] [int] NOT NULL,
	[Usr_ID] [int] NOT NULL,
	[IsSubscribed] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
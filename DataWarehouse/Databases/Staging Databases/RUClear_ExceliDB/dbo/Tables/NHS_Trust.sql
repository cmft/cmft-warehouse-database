﻿CREATE TABLE [dbo].[NHS_Trust](
	[NHST_ID] [int] IDENTITY(1,1) NOT NULL,
	[NHST_Name] [nvarchar](50) NULL,
	[NHST_ID_Number] [nvarchar](50) NULL,
	[NHST_Contact_Name] [nvarchar](50) NULL,
	[NHST_Contact_Designation] [nvarchar](50) NULL,
	[NHST_Addr1] [varchar](35) NULL,
	[NHST_Addr2] [varchar](35) NULL,
	[NHST_County] [nvarchar](30) NULL,
	[NHST_City] [varchar](35) NULL,
	[NHST_PCode] [nvarchar](50) NULL,
	[NHST_Country_Code] [int] NULL,
	[HB_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModIFied_User_ID] [int] NOT NULL,
	[LastModIFiedDate] [datetime] NOT NULL
) ON [PRIMARY]
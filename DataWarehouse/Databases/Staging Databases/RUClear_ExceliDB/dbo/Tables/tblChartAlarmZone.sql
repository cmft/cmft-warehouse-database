﻿CREATE TABLE [dbo].[tblChartAlarmZone](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChartDesigner_ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[LowerExtent] [float] NULL,
	[UpperExtent] [float] NULL,
	[Pattern] [smallint] NULL,
	[Backcolor] [varchar](20) NULL,
	[Forecolor] [varchar](20) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_Id] [int] NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
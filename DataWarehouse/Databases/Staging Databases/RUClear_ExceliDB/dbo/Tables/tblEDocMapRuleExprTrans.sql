﻿CREATE TABLE [dbo].[tblEDocMapRuleExprTrans](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EDocMapRuleExpr_ID] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[TargetNode] [varchar](1000) NULL,
	[Expression] [varchar](2000) NULL,
	[IsAddToEC] [bit] NOT NULL,
	[PropertyString] [nvarchar](2000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
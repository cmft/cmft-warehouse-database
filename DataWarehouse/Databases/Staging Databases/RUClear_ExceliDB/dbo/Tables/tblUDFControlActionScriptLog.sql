﻿CREATE TABLE [dbo].[tblUDFControlActionScriptLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Action_ID] [int] NOT NULL,
	[ActionScriptLog] [text] NULL,
	[Status] [varchar](20) NULL,
	[ActionScript] [text] NULL,
	[IsActive] [nvarchar](1) NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
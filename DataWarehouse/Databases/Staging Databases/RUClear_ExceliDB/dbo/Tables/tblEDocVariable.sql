﻿CREATE TABLE [dbo].[tblEDocVariable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EdocMap_ID] [int] NULL,
	[Name] [varchar](30) NOT NULL,
	[EDocMapVarBaseType_SLU] [int] NOT NULL,
	[DisplayOrder] [int] NULL,
	[EDocMapVarDataType_SLU] [int] NOT NULL,
	[Expression] [varchar](2000) NULL,
	[PropertyString] [varchar](500) NULL,
	[DisplayPrompt] [varchar](300) NULL,
	[IsMandatory] [int] NULL,
	[Comments] [varchar](200) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCLMPrintPatientInfo](
	[ReportPrintLog_ID] [int] NOT NULL,
	[CRN] [varchar](30) NULL,
	[LabNo] [varchar](30) NULL,
	[DateofSpecimen] [datetime] NULL,
	[PatientCustomForm_ID] [int] NULL,
	[LetterType] [int] NULL,
	[RowID] [int] NULL
) ON [PRIMARY]
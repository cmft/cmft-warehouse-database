﻿CREATE TABLE [dbo].[tblEPPatMatchType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatchTypename] [varchar](256) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NHS_Trust_Phone](
	[NHSTP_ID] [int] IDENTITY(1,1) NOT NULL,
	[NHST_ID] [int] NOT NULL,
	[NHSTP_Phone_No] [nvarchar](100) NOT NULL,
	[NHSTP_Phone_Type_Code] [int] NOT NULL,
	[NHSTP_Provider_Code] [int] NULL,
	[IsISDN_Bonded] [bit] NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
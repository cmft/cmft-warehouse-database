﻿CREATE TABLE [dbo].[My_Patient_List](
	[USR_ID] [int] NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[ClinicWard_LU] [int] NULL,
	[ClinicWardType] [tinyint] NULL,
	[ListPosition] [int] NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df__my_patien__isact__0eae1de1]  DEFAULT (1),
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
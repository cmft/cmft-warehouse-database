﻿CREATE TABLE [dbo].[tblSysSearchModule](
	[SysModule_ID] [int] NULL,
	[TableName] [nvarchar](50) NULL,
	[KeyColumn] [nvarchar](20) NULL,
	[OrderColumn] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblVoice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Language_LU] [int] NOT NULL,
	[IsSystemVoice] [bit] NOT NULL CONSTRAINT [df_tblvoice_issystemvoice]  DEFAULT (0),
	[IsMenuComplete] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
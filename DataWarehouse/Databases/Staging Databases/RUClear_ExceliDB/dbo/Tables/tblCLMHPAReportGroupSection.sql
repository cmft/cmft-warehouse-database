﻿CREATE TABLE [dbo].[tblCLMHPAReportGroupSection](
	[GroupSectionNo] [varchar](10) NULL,
	[ReportSectionID] [int] NULL,
	[GroupSectionDescription] [varchar](175) NULL,
	[GroupSummary] [varchar](256) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEPLookUp](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LookupCategory_SLU] [int] NOT NULL,
	[LookupValue] [nvarchar](125) NOT NULL,
	[ListPosition] [smallint] NOT NULL,
	[IsProtected] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
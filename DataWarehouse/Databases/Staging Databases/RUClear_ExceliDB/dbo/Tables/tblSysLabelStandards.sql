﻿CREATE TABLE [dbo].[tblSysLabelStandards](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Brand] [varchar](50) NULL,
	[TopMargin] [float] NULL,
	[SideMargin] [float] NULL,
	[VerticalPitch] [float] NULL,
	[HorizontalPitch] [float] NULL,
	[Height] [float] NULL,
	[Width] [float] NULL,
	[NoAcross] [int] NULL,
	[NoDown] [int] NULL,
	[PageSize_SLU] [int] NULL,
	[LabelType] [int] NULL,
	[IsActive] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Health_Board_EAddr](
	[HBE_ID] [int] IDENTITY(1,1) NOT NULL,
	[HB_ID] [int] NULL,
	[HBE_EAddr] [nvarchar](50) NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblDocParameter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Doc_ID] [int] NOT NULL,
	[ParamName] [nvarchar](100) NOT NULL,
	[DefaultValue] [nvarchar](3000) NULL,
	[ParamField_ID] [int] NOT NULL,
	[ActionType_SLU] [int] NOT NULL,
	[DataType_SLU] [int] NOT NULL,
	[PromptAtRuntime] [tinyint] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[SourceID] [int] NULL,
	[SourceType] [tinyint] NULL,
	[LastModifed_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblControlExpectedInput](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Control_ID] [int] NOT NULL,
	[Control_Property] [varchar](20) NULL,
	[ExpectedInput] [varchar](30) NULL,
	[IsActive] [nvarchar](1) NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblLinkedLookup](
	[Parent_LU] [int] NOT NULL,
	[Child_LU] [int] NOT NULL,
	[IsProtected] [bit] NULL,
	[IsActive] [tinyint] NULL CONSTRAINT [DF_tblLinkedLookup_IsActive]  DEFAULT (1),
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_tblLinkedLookup_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
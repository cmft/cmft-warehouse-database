﻿CREATE TABLE [dbo].[tblPatientSiteRef](
	[CurrentID] [int] NOT NULL,
	[OriginalID] [int] NOT NULL,
	[TableName] [nvarchar](50) NOT NULL,
	[SiteID] [numeric](18, 0) NOT NULL,
	[IsCurrPackItem] [bit] NOT NULL,
	[LinkedRecord_ID] [int] NOT NULL
) ON [PRIMARY]
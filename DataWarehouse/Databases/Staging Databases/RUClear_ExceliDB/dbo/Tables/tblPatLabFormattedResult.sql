﻿CREATE TABLE [dbo].[tblPatLabFormattedResult](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LabTest_ID] [int] NOT NULL,
	[Sample_ID] [int] NOT NULL,
	[InterSample_ID] [int] NOT NULL,
	[DateSpecimenAuthorised] [datetime] NULL,
	[IsLatestRecord] [bit] NOT NULL,
	[ResultValue] [ntext] NULL,
	[ResultType] [tinyint] NULL,
	[LabLocation_ID] [int] NULL,
	[IsNotes] [bit] NULL,
	[RefRange] [varchar](50) NULL,
	[Units] [varchar](100) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSessionData](
	[ID] [int] NOT NULL,
	[SessionID] [varchar](70) NOT NULL,
	[ObjectName] [varchar](50) NULL,
	[SessionData] [varchar](7850) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
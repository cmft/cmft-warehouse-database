﻿CREATE TABLE [dbo].[tblsignoffqueue20150122](
	[OrigRec_ID] [int] NOT NULL,
	[TableName] [nvarchar](100) NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[Signer_CLN_ID] [int] NOT NULL,
	[Entered_User_ID] [int] NOT NULL,
	[Entered_Date_Time] [datetime] NOT NULL,
	[MsgType] [int] NULL,
	[IsRead] [bit] NULL,
	[FormLibItem_ID] [int] NULL,
	[IsLinkedRecord] [bit] NULL,
	[Signer_Group_ID] [int] NULL
) ON [PRIMARY]
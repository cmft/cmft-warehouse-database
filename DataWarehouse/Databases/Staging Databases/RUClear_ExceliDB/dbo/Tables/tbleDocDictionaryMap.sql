﻿CREATE TABLE [dbo].[tbleDocDictionaryMap](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EdocMapRuleExprTrans_ID] [int] NULL,
	[DictColumn_ID] [int] NOT NULL,
	[CustomFormControl_ID] [int] NULL,
	[DefaultValue] [varchar](255) NULL,
	[EdocXSDMappedElement_ID] [int] NULL,
	[IsDefaultSearchColumn] [bit] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
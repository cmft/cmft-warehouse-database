﻿CREATE TABLE [dbo].[tblApptResourceTimings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type_SLU] [int] NULL,
	[Code] [nvarchar](125) NULL,
	[Description] [nvarchar](1000) NULL,
	[Duration] [int] NULL,
	[Duration_SLU] [int] NULL,
	[DurationInMin] [int] NULL,
	[Status] [tinyint] NULL,
	[IsActive] [tinyint] NULL,
	[LastModifiedUser_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
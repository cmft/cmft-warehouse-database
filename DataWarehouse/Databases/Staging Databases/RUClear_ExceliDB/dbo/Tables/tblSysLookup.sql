﻿CREATE TABLE [dbo].[tblSysLookup](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LookupValue] [nvarchar](125) NOT NULL,
	[ListPosition] [smallint] NOT NULL,
	[BrowserType_SLU] [int] NULL,
	[CTCodeSet_SLU] [int] NULL,
	[IsCoded] [bit] NOT NULL CONSTRAINT [DF_tblSysLookup_IsCoded]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
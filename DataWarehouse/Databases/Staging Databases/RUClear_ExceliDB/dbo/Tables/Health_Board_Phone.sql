﻿CREATE TABLE [dbo].[Health_Board_Phone](
	[HBP_ID] [int] IDENTITY(1,1) NOT NULL,
	[HB_ID] [int] NOT NULL,
	[HBP_Phone_No] [nvarchar](100) NOT NULL,
	[HBP_Phone_Type_Code] [int] NOT NULL,
	[HBP_Provider_Code] [int] NULL,
	[IsISDN_Bonded] [bit] NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
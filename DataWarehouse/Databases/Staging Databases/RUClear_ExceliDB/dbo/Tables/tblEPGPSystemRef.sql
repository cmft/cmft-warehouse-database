﻿CREATE TABLE [dbo].[tblEPGPSystemRef](
	[MsgStandard_ID] [int] NOT NULL,
	[LocalID] [int] NOT NULL,
	[ExternalID] [nvarchar](25) NOT NULL,
	[TableName] [nvarchar](256) NOT NULL,
	[RecordCode] [char](100) NULL
) ON [PRIMARY]
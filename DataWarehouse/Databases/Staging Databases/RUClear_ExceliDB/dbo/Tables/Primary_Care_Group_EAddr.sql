﻿CREATE TABLE [dbo].[Primary_Care_Group_EAddr](
	[PCGE_ID] [int] IDENTITY(1,1) NOT NULL,
	[PCG_ID] [int] NOT NULL,
	[PCGE_EAddr] [nvarchar](50) NOT NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
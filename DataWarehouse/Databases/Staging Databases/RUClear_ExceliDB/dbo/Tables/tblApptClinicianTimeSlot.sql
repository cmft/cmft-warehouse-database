﻿CREATE TABLE [dbo].[tblApptClinicianTimeSlot](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinician_ID] [int] NOT NULL,
	[Appt_SLU] [int] NOT NULL,
	[TimeSlot] [int] NOT NULL,
	[ShowAppt] [bit] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
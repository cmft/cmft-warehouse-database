﻿CREATE TABLE [dbo].[tblLabDeptPreferences](
	[Department_LU] [int] NOT NULL,
	[GroupBySpecimen] [bit] NOT NULL,
	[AdditionalFields] [nvarchar](4000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
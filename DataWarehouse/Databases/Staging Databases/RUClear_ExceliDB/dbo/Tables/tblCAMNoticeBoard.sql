﻿CREATE TABLE [dbo].[tblCAMNoticeBoard](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Rule_Id] [int] NOT NULL,
	[Pat_Id] [int] NOT NULL,
	[LookUp_Id] [int] NULL,
	[LibFormMode] [int] NULL,
	[ActionValue] [int] NULL,
	[Status] [bit] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
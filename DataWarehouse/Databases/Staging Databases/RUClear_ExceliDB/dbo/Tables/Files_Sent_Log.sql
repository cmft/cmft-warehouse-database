﻿CREATE TABLE [dbo].[Files_Sent_Log](
	[FS_ID] [int] IDENTITY(1,1) NOT NULL,
	[FS_DateTime] [datetime] NULL,
	[USR_ID] [int] NULL,
	[FS_File_Object] [image] NULL,
	[FS_File_Type] [tinyint] NULL,
	[FS_Size] [int] NULL,
	[FS_CallerID] [nvarchar](100) NULL,
	[IsMerged] [bit] NULL,
	[FS_Merged_PatID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
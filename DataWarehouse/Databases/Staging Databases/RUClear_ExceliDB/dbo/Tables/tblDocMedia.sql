﻿CREATE TABLE [dbo].[tblDocMedia](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Doc_ID] [int] NULL,
	[DocMedia] [image] NULL,
	[FormLibItem_ID] [int] NULL,
	[IsDesignerMedia] [bit] NOT NULL,
	[Filename] [nvarchar](500) NOT NULL,
	[FilePath] [nvarchar](500) NOT NULL,
	[OldLibItem_ID] [int] NULL,
	[PreviewImage] [image] NULL,
	[Annotation] [image] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblSysDBMaintenanceLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DBVersionMain] [varchar](256) NOT NULL,
	[DBVersionInternal] [varchar](256) NOT NULL,
	[DateOperated] [smalldatetime] NOT NULL,
	[ResourceName] [varchar](64) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Comments] [varchar](4000) NULL
) ON [PRIMARY]
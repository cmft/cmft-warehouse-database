﻿CREATE TABLE [dbo].[tblWebActiveXControl](
	[ID] [int] NOT NULL,
	[ControlType_ID] [int] NULL,
	[PropertyName] [varchar](100) NULL,
	[PropertyNumber] [tinyint] NULL,
	[DefaultValue] [varchar](100) NULL
) ON [PRIMARY]
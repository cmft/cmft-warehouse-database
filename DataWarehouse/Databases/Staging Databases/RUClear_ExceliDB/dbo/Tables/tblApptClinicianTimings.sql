﻿CREATE TABLE [dbo].[tblApptClinicianTimings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Clinician_ID] [int] NOT NULL,
	[ClinicianDate] [datetime] NOT NULL,
	[WeekDay] [int] NULL,
	[Starttime] [datetime] NOT NULL,
	[Endtime] [datetime] NOT NULL,
	[RecurrencePattern] [varchar](8000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
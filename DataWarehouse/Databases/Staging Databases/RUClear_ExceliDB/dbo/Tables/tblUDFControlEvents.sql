﻿CREATE TABLE [dbo].[tblUDFControlEvents](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomForm_ID] [int] NOT NULL,
	[CustomControl_ID] [int] NOT NULL,
	[MacroID] [int] NULL,
	[MacroName] [varchar](100) NULL,
	[CustomControl_Name] [varchar](100) NULL,
	[Condition] [varchar](2000) NOT NULL,
	[EventAction] [varchar](20) NOT NULL,
	[FiringOrder] [int] NULL,
	[EventDescription] [varchar](100) NOT NULL,
	[LookupType] [char](1) NULL,
	[LookupID] [int] NULL,
	[LookupConditions] [varchar](2000) NULL,
	[IsActive] [int] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
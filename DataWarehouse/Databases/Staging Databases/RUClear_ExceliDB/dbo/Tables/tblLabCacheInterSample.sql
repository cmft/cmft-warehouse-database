﻿CREATE TABLE [dbo].[tblLabCacheInterSample](
	[ID] [int] NOT NULL,
	[LabCacheSpecimenHeader_ID] [int] NOT NULL,
	[TestGroupCode] [char](10) NULL,
	[TestGroupText] [char](256) NULL,
	[DateAuthorised] [datetime] NULL,
	[TestGroupComments] [varchar](1000) NULL
) ON [PRIMARY]
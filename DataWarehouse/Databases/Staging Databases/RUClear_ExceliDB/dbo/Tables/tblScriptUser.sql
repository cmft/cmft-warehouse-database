﻿CREATE TABLE [dbo].[tblScriptUser](
	[Script_ID] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[ListPosition] [int] NULL,
	[DateAssigned] [datetime] NOT NULL,
	[DateStopped] [datetime] NULL,
	[DateLastCompleted] [datetime] NULL,
	[NoOfTimesUsed] [int] NOT NULL,
	[AssignedBy_User_ID] [int] NOT NULL,
	[Voice_ID] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
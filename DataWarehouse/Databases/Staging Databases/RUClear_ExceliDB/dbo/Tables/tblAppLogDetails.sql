﻿CREATE TABLE [dbo].[tblAppLogDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LogType_SLU] [int] NOT NULL,
	[SourceID] [int] NOT NULL,
	[SourceType_SLU] [int] NOT NULL,
	[SourceName] [varchar](100) NULL,
	[PrinterName] [varchar](100) NULL,
	[No.ofCopies] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[DatePrinted] [datetime] NOT NULL
) ON [PRIMARY]
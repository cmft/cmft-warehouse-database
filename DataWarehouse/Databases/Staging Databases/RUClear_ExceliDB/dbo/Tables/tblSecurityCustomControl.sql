﻿CREATE TABLE [dbo].[tblSecurityCustomControl](
	[CustomForm_ID] [int] NOT NULL,
	[CustomControl_ID] [int] NOT NULL,
	[SecurityLevel_ID] [int] NOT NULL,
	[AccessLevel] [tinyint] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCustomControlActionProperties](
	[ID] [int] NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[PropertyCount] [smallint] NOT NULL,
	[Property1] [nvarchar](50) NULL,
	[Property2] [nvarchar](50) NULL,
	[Property3] [nvarchar](50) NULL,
	[Property4] [nvarchar](50) NULL,
	[Property5] [nvarchar](50) NULL,
	[Property6] [nvarchar](50) NULL,
	[Property7] [nvarchar](50) NULL,
	[Property8] [nvarchar](50) NULL,
	[Property9] [nvarchar](50) NULL,
	[Property10] [nvarchar](50) NULL,
	[IsDataEntryField] [bit] NOT NULL,
	[IsActive] [nvarchar](1) NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]
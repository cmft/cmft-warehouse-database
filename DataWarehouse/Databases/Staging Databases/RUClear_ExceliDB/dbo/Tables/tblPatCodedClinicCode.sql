﻿CREATE TABLE [dbo].[tblPatCodedClinicCode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatClinicalCode_ID] [int] NULL,
	[CodeType_SLU] [int] NULL,
	[BasedCodeType_SLU] [int] NULL,
	[CodeID] [nvarchar](128) NULL,
	[Description] [nvarchar](256) NULL,
	[SnomedConceptXML] [ntext] NULL,
	[Sequence] [int] NULL,
	[IsPrimary] [bit] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
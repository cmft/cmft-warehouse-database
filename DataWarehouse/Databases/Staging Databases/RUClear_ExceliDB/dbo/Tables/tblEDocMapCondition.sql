﻿CREATE TABLE [dbo].[tblEDocMapCondition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EdocMapRule_ID] [int] NOT NULL,
	[Condition] [varchar](2000) NULL,
	[Action] [int] NOT NULL,
	[ActionValue] [nvarchar](200) NULL,
	[DisplayOrder] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TblLabTestUnitChangeHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LabTest_ID] [int] NULL,
	[Units] [varchar](100) NULL,
	[UnitChangedDate] [datetime] NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
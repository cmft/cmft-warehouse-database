﻿CREATE TABLE [dbo].[tblMacroFields](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Macro_ID] [int] NOT NULL,
	[SysFSField_ID] [int] NOT NULL,
	[FieldName] [varchar](50) NOT NULL,
	[ListPosition] [int] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEDocLookupRef](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[eDocMap_ID] [int] NOT NULL,
	[eDocRef_Id] [int] NOT NULL,
	[LookupName] [char](256) NOT NULL,
	[ECLookupCategory_SLU] [int] NOT NULL,
	[ECLookupCategoryType_ID] [int] NOT NULL,
	[EdocXSDMappedElements_ID] [int] NOT NULL,
	[EDocECMappedFields_ID] [int] NOT NULL,
	[IsDataTrans] [bit] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
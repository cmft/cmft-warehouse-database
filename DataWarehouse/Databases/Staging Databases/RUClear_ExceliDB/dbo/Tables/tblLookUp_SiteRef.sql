﻿CREATE TABLE [dbo].[tblLookUp_SiteRef](
	[CurrentID] [int] NOT NULL,
	[OriginalID] [int] NOT NULL,
	[LookUPCategory_SLU] [int] NOT NULL,
	[OriginalValue] [nvarchar](50) NOT NULL,
	[SiteID] [numeric](10, 0) NOT NULL,
	[IsCurrPackItem] [bit] NOT NULL
) ON [PRIMARY]
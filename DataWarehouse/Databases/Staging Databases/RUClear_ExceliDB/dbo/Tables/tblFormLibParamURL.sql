﻿CREATE TABLE [dbo].[tblFormLibParamURL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormLibItem_ID] [int] NOT NULL,
	[ParamID] [int] NULL,
	[ParamName] [varchar](256) NULL,
	[FieldType] [tinyint] NULL,
	[FieldID] [varchar](256) NULL,
	[DataSource] [varchar](8000) NULL,
	[MaskedURL] [varchar](2083) NULL,
	[URLObject] [image] NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
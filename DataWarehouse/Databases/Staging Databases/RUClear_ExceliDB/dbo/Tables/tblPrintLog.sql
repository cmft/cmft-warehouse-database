﻿CREATE TABLE [dbo].[tblPrintLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemType_SLU] [int] NOT NULL,
	[ItemName] [varchar](256) NULL,
	[PrinterName] [varchar](50) NULL,
	[User_ID] [int] NOT NULL,
	[DatePrinted] [datetime] NOT NULL
) ON [PRIMARY]
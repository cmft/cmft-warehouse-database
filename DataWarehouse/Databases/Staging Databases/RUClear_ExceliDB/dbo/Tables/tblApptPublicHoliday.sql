﻿CREATE TABLE [dbo].[tblApptPublicHoliday](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[holidaydate] [datetime] NOT NULL,
	[Description] [char](100) NULL,
	[ApplytoAllYears] [bit] NULL CONSTRAINT [DF_tblApptPublicHoliday_ApplytoAllYears]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblDiaryClinicTimings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Location_ID] [int] NULL,
	[Weekday] [int] NULL,
	[Starttime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[SlotTime] [nvarchar](50) NULL,
	[LastModifiedUser_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[DiarySlotInterval] [int] NULL,
	[TotalSlots] [int] NULL,
	[IsActive] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblNotificationLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Notification_ID] [int] NOT NULL,
	[NotificationDetails_ID] [int] NOT NULL,
	[NotificationTemplateRecipient_ID] [int] NULL,
	[NotificationMode_LU] [int] NOT NULL,
	[RecipientID] [int] NOT NULL,
	[NotificationSentDate] [datetime] NOT NULL,
	[AcknowledgmentReceived] [bit] NOT NULL,
	[AcknowledgmentDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
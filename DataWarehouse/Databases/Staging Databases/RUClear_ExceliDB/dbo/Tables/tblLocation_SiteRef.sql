﻿CREATE TABLE [dbo].[tblLocation_SiteRef](
	[CurrentID] [int] NOT NULL,
	[OriginalID] [int] NOT NULL,
	[TableName] [nvarchar](50) NOT NULL,
	[SiteID] [int] NOT NULL,
	[IsCurrPackItem] [tinyint] NOT NULL,
	[RecordType] [tinyint] NOT NULL,
	[LinkedRecord_ID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblConfInstance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ConfTemplate_Id] [int] NOT NULL,
	[ScheduledDate] [datetime] NULL,
	[ActualStartDate] [datetime] NULL,
	[Stopdate] [datetime] NULL,
	[ConfStartedUser_ID] [int] NULL,
	[IsClosed] [tinyint] NULL,
	[Comments] [varchar](1000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedUser_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
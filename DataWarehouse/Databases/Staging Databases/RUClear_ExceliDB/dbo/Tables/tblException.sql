﻿CREATE TABLE [dbo].[tblException](
	[SessionErrorID] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[SessionID] [varchar](70) NOT NULL,
	[RequestMethod] [varchar](5) NULL,
	[LocalAddr] [varchar](15) NULL,
	[RemoteAddr] [varchar](15) NULL,
	[UserAgent] [varchar](255) NULL,
	[URL] [varchar](400) NULL,
	[FormData] [text] NULL,
	[AllHTTP] [varchar](2000) NULL,
	[InsertDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
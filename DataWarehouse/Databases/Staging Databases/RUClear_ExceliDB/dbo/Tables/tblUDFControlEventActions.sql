﻿CREATE TABLE [dbo].[tblUDFControlEventActions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UDFEvent_ID] [int] NOT NULL,
	[ParentMacroName] [varchar](100) NOT NULL,
	[ActionID] [smallint] NOT NULL,
	[ActionCustomForm_ID] [int] NOT NULL,
	[ActionCustomForm_Name] [varchar](100) NULL,
	[ActionCustomControl_ID] [int] NOT NULL,
	[ActionCustomControl_Name] [varchar](100) NULL,
	[ActionProperty] [varchar](30) NOT NULL,
	[ActionPropertyValue] [varchar](2000) NULL,
	[IsActive] [int] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
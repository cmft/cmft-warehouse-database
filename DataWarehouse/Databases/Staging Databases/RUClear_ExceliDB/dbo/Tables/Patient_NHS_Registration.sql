﻿CREATE TABLE [dbo].[Patient_NHS_Registration](
	[PNHS_ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[HB_ID] [int] NULL,
	[PNHS_Registration_Type_Code] [int] NULL,
	[PNHS_Short_Term_Code] [int] NULL,
	[PNHS_Stand_Alone_Code] [int] NULL,
	[PNHS_RP_Mileage] [int] NULL,
	[PNHS_RP_Walking_Units] [int] NULL,
	[PNHS_RP_Water_Miles] [int] NULL,
	[PNHS_Dispensing_Status] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
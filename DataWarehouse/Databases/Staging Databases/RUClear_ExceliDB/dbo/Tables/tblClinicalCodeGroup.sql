﻿CREATE TABLE [dbo].[tblClinicalCodeGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[Priority_LU] [int] NULL,
	[Status_LU] [int] NULL,
	[IsTimeLine] [bit] NOT NULL,
	[IsProblemGroup] [bit] NULL CONSTRAINT [DF_tblClinicalCodeGroup_IsProblemGroup]  DEFAULT (0),
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
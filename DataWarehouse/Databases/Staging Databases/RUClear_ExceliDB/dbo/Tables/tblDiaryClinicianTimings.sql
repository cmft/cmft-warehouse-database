﻿CREATE TABLE [dbo].[tblDiaryClinicianTimings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClinicTimings_ID] [int] NULL,
	[Dept_Lu] [int] NULL,
	[Location_ID] [int] NULL,
	[Clinician_ID] [int] NULL,
	[LastModifiedUser_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[Isactive] [tinyint] NULL
) ON [PRIMARY]
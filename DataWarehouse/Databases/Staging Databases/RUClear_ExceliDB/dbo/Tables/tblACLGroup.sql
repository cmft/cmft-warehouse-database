﻿CREATE TABLE [dbo].[tblACLGroup](
	[ID] [int] IDENTITY(100,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ACLGroupType_SLU] [int] NOT NULL,
	[PAT_ID] [int] NULL,
	[MCN_ID] [int] NULL,
	[MCNCatchmentType_SLU] [int] NULL,
	[LOC_ID] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
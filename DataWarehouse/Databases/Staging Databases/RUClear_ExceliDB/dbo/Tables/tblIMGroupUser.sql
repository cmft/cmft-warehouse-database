﻿CREATE TABLE [dbo].[tblIMGroupUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IMGroup_ID] [int] NOT NULL,
	[Usr_ID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
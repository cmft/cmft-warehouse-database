﻿CREATE TABLE [dbo].[tblPatImagePointSetPoint](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ImagePointSet_Id] [int] NOT NULL,
	[PointName] [varchar](10) NULL,
	[Xpos] [int] NOT NULL,
	[Ypos] [int] NOT NULL,
	[AnnTag] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblAllStaff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StaffID] [int] NOT NULL,
	[StaffType] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]
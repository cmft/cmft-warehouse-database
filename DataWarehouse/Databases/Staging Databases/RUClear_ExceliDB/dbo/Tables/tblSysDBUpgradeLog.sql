﻿CREATE TABLE [dbo].[tblSysDBUpgradeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DBVersion] [varchar](256) NOT NULL,
	[DateOperated] [smalldatetime] NOT NULL,
	[ResourceName] [varchar](50) NOT NULL,
	[Description] [varchar](256) NOT NULL,
	[Comments] [varchar](4000) NULL
) ON [PRIMARY]
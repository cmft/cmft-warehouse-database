﻿CREATE TABLE [dbo].[tblScriptRule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Script_ID] [int] NOT NULL,
	[ScriptSection_SectionNo] [smallint] NOT NULL,
	[CriteriaString] [nvarchar](2000) NULL,
	[ListPosition] [smallint] NOT NULL,
	[NextAction] [nvarchar](50) NOT NULL,
	[IsActive] [tinyint] NOT NULL CONSTRAINT [df_tblscriptrule_isactive]  DEFAULT (1),
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
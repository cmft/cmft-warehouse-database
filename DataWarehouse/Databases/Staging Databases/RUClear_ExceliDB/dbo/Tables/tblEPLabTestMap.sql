﻿CREATE TABLE [dbo].[tblEPLabTestMap](
	[EPStore_SLU] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ECLabTest_Id] [int] NULL,
	[ShortName] [varchar](25) NULL,
	[LongName] [varchar](100) NULL,
	[Department_LU] [int] NULL,
	[ResultStore_SLU] [int] NULL,
	[SpecimenType_LU] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatientMCN](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NOT NULL,
	[MCN_ID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[StopDate] [datetime] NULL,
	[MCNStopReason_SLU] [int] NULL,
	[StopReasonDesc] [varchar](500) NULL,
	[FirstContact_Clinician_ID] [int] NULL,
	[StaffType] [int] NULL,
	[CurrentMCN_Catchment_ID] [int] NULL,
	[CurrentMCN_Location] [int] NULL,
	[IsMultipleCurrentCatchment] [bit] NOT NULL,
	[IsHistoryAvailable] [tinyint] NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
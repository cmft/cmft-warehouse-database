﻿CREATE TABLE [dbo].[tblScriptRuleLog](
	[UserSession_ID] [int] NOT NULL,
	[SequenceNo] [smallint] NOT NULL,
	[ScriptRule_ID] [int] NOT NULL
) ON [PRIMARY]
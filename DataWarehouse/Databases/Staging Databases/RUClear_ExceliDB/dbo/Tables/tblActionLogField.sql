﻿CREATE TABLE [dbo].[tblActionLogField](
	[Action_ID] [int] NOT NULL,
	[CustomFormControl_ID] [int] NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[LogType] [tinyint] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
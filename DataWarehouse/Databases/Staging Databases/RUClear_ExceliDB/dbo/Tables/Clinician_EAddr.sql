﻿CREATE TABLE [dbo].[Clinician_EAddr](
	[CLNE_ID] [int] IDENTITY(1,1) NOT NULL,
	[CLN_ID] [int] NULL,
	[CLNE_EAddr] [nvarchar](50) NULL,
	[IsPreferred] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblEPLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LogType_LU] [int] NOT NULL,
	[ImportModule_LU] [int] NULL,
	[Description] [varchar](256) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[EventTime] [datetime] NULL,
	[Duration] [float] NULL,
	[Status] [int] NULL,
	[EventResult_LU] [int] NULL,
	[FileName] [varchar](256) NULL,
	[ErrorData_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
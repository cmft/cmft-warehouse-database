﻿CREATE TABLE [dbo].[tblSysLookupLink](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormName] [varchar](100) NOT NULL,
	[ControlName] [varchar](100) NOT NULL,
	[LookupGroup_ID] [int] NOT NULL,
	[LookupCategoryTypeID] [tinyint] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_USer_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblPatientName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_ID] [int] NULL,
	[Title_LU] [int] NULL,
	[Initials] [nvarchar](16) NULL,
	[Forename1] [nvarchar](30) NOT NULL,
	[Forename2] [nvarchar](30) NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[NameType_LU] [int] NULL,
	[IsCurrentName] [bit] NULL,
	[DateEntered] [datetime] NULL,
	[KnownAs] [nvarchar](50) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
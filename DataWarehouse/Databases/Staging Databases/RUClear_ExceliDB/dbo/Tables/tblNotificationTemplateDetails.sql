﻿CREATE TABLE [dbo].[tblNotificationTemplateDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Template_ID] [int] NOT NULL,
	[Type_LU] [int] NULL,
	[NotifyBy_LU] [int] NULL,
	[MessageText] [varchar](500) NULL,
	[MessageMode] [int] NULL,
	[MessageSubject] [varchar](100) NULL,
	[FormLibItem_ID] [int] NULL,
	[DueInterval] [int] NOT NULL,
	[DueInterval_LU] [int] NOT NULL,
	[Acknowledge_LU] [int] NOT NULL,
	[SelectedGroup] [varchar](4000) NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
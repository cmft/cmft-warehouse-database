﻿CREATE TABLE [dbo].[tblLinkedForm](
	[FormLibItem_ID] [int] NOT NULL,
	[Slave_FormLibItem_ID] [int] NOT NULL,
	[LinkType] [tinyint] NOT NULL,
	[ListPosition] [int] NULL
) ON [PRIMARY]
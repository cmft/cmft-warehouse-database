﻿CREATE TABLE [dbo].[tblLocDiagnostic](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Facility_Id] [int] NOT NULL,
	[Name] [nvarchar](30) NULL
) ON [PRIMARY]
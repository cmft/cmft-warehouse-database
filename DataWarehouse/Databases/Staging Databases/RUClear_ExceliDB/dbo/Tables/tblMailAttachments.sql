﻿CREATE TABLE [dbo].[tblMailAttachments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Mail_ID] [int] NOT NULL,
	[AttachmentName] [varchar](4000) NOT NULL,
	[AttachmentType] [varchar](50) NOT NULL,
	[FileObject] [image] NULL,
	[FileObjectSize] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
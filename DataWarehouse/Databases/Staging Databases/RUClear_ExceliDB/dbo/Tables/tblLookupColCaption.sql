﻿CREATE TABLE [dbo].[tblLookupColCaption](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LookupGroup_ID] [int] NOT NULL,
	[LookupCategoryType] [tinyint] NOT NULL,
	[CodeCaption] [varchar](125) NOT NULL,
	[AddCodeCaption] [varchar](125) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_USer_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCustomFormControlPTSql](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PCFMode_SLU] [int] NOT NULL,
	[CustomFormControl_ID] [int] NOT NULL,
	[CustomForm_Id] [int] NOT NULL,
	[FormLibitem_ID] [int] NOT NULL,
	[SourceCustomForm_ID] [int] NOT NULL,
	[SourceCustomFormControl_ID] [int] NOT NULL,
	[SourceFormLibItem_Id] [int] NOT NULL,
	[SelectionMode] [tinyint] NULL,
	[SelectionType] [tinyint] NULL,
	[DataSource] [ntext] NOT NULL,
	[ParamList] [varchar](400) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
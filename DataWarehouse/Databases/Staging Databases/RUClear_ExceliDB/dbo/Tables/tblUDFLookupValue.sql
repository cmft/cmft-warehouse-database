﻿CREATE TABLE [dbo].[tblUDFLookupValue](
	[ID] [int] IDENTITY(500000,1) NOT NULL,
	[tblUDFLookupGroup_ID] [int] NOT NULL,
	[ShortDescription] [nvarchar](125) NOT NULL,
	[ShortDescription2] [nvarchar](125) NULL,
	[Description] [nvarchar](1000) NOT NULL,
	[ListPosition] [smallint] NOT NULL,
	[IsProtected] [bit] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
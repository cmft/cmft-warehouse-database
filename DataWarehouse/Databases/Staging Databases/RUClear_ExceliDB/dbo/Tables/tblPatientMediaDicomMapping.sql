﻿CREATE TABLE [dbo].[tblPatientMediaDicomMapping](
	[COB_ID] [int] NOT NULL,
	[IsDicomImage] [bit] NOT NULL
) ON [PRIMARY]
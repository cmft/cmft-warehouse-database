﻿CREATE TABLE [dbo].[tblFormLibItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](2083) NULL,
	[FLClassificationType_SLU] [int] NULL,
	[BaseFormType] [int] NULL,
	[BaseFormID] [int] NULL,
	[UDFType_SLU] [int] NULL,
	[Keywords] [nvarchar](200) NULL,
	[Description] [nvarchar](2000) NULL,
	[LibraryStatus_SLU] [int] NULL,
	[IsAxsys] [bit] NULL,
	[IsSet] [bit] NULL,
	[EnabledDate] [datetime] NULL,
	[DisabledDate] [datetime] NULL,
	[MadeLocalDate] [datetime] NULL,
	[InheritSetClassification] [bit] NOT NULL CONSTRAINT [DF_tblFormLibItem_InheritSetClassification]  DEFAULT (1),
	[RestrictAccess] [bit] NULL CONSTRAINT [DF_tblFormLibItem_RestrictAccess]  DEFAULT (0),
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
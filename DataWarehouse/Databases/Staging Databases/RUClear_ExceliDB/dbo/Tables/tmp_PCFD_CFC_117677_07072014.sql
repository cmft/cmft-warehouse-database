﻿CREATE TABLE [dbo].[tmp_PCFD_CFC_117677_07072014](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientCustomForm_ID] [int] NOT NULL,
	[CustomFormControl_ID] [int] NOT NULL,
	[ResultValue] [nvarchar](4000) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
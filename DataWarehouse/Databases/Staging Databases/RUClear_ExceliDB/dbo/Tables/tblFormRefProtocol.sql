﻿CREATE TABLE [dbo].[tblFormRefProtocol](
	[FormLibItem_ID] [int] NOT NULL,
	[RefProtocol_ID] [int] NOT NULL,
	[LastModified_User_ID] [char](10) NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
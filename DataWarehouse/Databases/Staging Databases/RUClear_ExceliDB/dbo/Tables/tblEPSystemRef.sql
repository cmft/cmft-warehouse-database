﻿CREATE TABLE [dbo].[tblEPSystemRef](
	[MsgStandard_ID] [int] NOT NULL,
	[LocalID] [int] NOT NULL,
	[ExternalID] [nvarchar](25) NOT NULL,
	[TableName] [nvarchar](256) NOT NULL,
	[RecordCode] [char](100) NULL,
	[IsActive] [int] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifieddate] [datetime] NULL
) ON [PRIMARY]
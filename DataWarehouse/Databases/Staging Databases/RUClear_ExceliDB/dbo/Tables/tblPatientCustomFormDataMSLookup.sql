﻿CREATE TABLE [dbo].[tblPatientCustomFormDataMSLookup](
	[PatientCustomForm_ID] [int] NULL,
	[Control_ID] [int] NULL,
	[ResultValue] [nvarchar](4000) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblHPAPCDTestResultData](
	[STDPCFID] [int] NULL,
	[PCDPCFID] [int] NULL,
	[PCDRowID] [int] NULL,
	[PCDCTResult] [int] NULL,
	[PCDIsResultRow] [int] NULL
) ON [PRIMARY]
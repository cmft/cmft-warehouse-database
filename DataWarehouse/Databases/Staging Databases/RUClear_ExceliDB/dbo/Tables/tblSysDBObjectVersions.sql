﻿CREATE TABLE [dbo].[tblSysDBObjectVersions](
	[ID] [int] NOT NULL,
	[ObjectName] [nvarchar](50) NULL,
	[CurrentVersionNo] [nvarchar](50) NULL
) ON [PRIMARY]
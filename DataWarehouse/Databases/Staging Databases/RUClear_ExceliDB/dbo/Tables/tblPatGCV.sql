﻿CREATE TABLE [dbo].[tblPatGCV](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pat_Id] [int] NOT NULL,
	[GCV_Id] [int] NOT NULL,
	[ModuleType_SLU] [int] NOT NULL,
	[Module_Id] [int] NOT NULL,
	[Instance_Id] [int] NOT NULL,
	[DateTimeCreated] [datetime] NOT NULL,
	[Control_Id] [int] NOT NULL,
	[ResultValue] [varchar](4000) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
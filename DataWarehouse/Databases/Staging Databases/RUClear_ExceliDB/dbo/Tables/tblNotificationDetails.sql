﻿CREATE TABLE [dbo].[tblNotificationDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Notification_ID] [int] NOT NULL,
	[LinkModuleID] [int] NULL,
	[LinkRecordID] [int] NULL,
	[Patient_ID] [int] NULL,
	[NotificationDetails_ID] [int] NOT NULL,
	[NotificationStatus_LU] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
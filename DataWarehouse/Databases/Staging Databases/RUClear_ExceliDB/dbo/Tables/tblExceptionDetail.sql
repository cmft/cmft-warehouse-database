﻿CREATE TABLE [dbo].[tblExceptionDetail](
	[SessionErrorID] [numeric](18, 0) NOT NULL,
	[ExceptionLevel] [int] NOT NULL,
	[Message] [varchar](1000) NULL,
	[Source] [varchar](1000) NULL,
	[StackTrace] [varchar](4500) NULL,
	[TargetSite] [varchar](1000) NULL
) ON [PRIMARY]
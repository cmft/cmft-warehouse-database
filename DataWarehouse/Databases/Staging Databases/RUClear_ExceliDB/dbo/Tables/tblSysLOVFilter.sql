﻿CREATE TABLE [dbo].[tblSysLOVFilter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LOVType] [varchar](30) NOT NULL,
	[FilterColumnName] [varchar](50) NOT NULL,
	[DisplayName] [varchar](80) NOT NULL,
	[DisplayOrder] [smallint] NOT NULL,
	[SQLFilterValues] [varchar](500) NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_tblUDFLOVFilter_LastModifiedDate]  DEFAULT (getdate())
) ON [PRIMARY]
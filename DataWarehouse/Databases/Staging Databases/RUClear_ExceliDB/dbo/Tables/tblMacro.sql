﻿CREATE TABLE [dbo].[tblMacro](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Param_ID] [int] NULL,
	[Source_ID] [int] NOT NULL,
	[SourceType] [tinyint] NOT NULL,
	[MacroObject] [image] NULL,
	[DefaultView] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
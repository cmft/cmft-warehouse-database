﻿CREATE TABLE [dbo].[tblECSessionRecordInfo](
	[ID] [int] NOT NULL,
	[SysControl_ID] [int] NOT NULL,
	[SysModule_ID] [int] NOT NULL,
	[IsActive] [tinyint] NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCatalogueOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Catalogue_ID] [int] NULL,
	[Order_ID] [int] NOT NULL,
	[ListPosition] [int] NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
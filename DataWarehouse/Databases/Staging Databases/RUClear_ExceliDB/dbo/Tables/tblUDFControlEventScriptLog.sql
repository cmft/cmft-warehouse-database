﻿CREATE TABLE [dbo].[tblUDFControlEventScriptLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UDFEvent_ID] [int] NOT NULL,
	[EventScriptlog] [text] NULL,
	[Status] [varchar](20) NULL,
	[EventScript] [text] NULL,
	[IsActive] [nvarchar](1) NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
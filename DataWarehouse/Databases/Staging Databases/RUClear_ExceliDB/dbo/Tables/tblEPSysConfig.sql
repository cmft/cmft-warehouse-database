﻿CREATE TABLE [dbo].[tblEPSysConfig](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItemName] [nvarchar](50) NOT NULL,
	[ItemValue] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[ListValues] [nvarchar](300) NULL,
	[ParentNode] [nvarchar](50) NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL
) ON [PRIMARY]
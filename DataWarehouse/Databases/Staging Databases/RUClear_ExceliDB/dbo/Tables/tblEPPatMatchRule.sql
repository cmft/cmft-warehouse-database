﻿CREATE TABLE [dbo].[tblEPPatMatchRule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MatchRuleName] [varchar](256) NULL,
	[MatchType_ID] [int] NOT NULL,
	[Precedence] [int] NOT NULL,
	[IsActive] [tinyint] NOT NULL,
	[LastModified_User_ID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY]
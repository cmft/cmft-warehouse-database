﻿CREATE TABLE [dbo].[tblDocResult](
	[FieldID] [int] NULL,
	[RecordID] [int] NULL,
	[ResultValue] [varchar](4000) NULL,
	[TableView] [varchar](3) NULL,
	[DocFieldName] [varchar](100) NULL,
	[BinaryResultValue] [ntext] NULL,
	[FormID] [int] NULL,
	[ControlType] [int] NULL,
	[PropertyString] [varchar](1000) NULL,
	[SysControl_ID] [int] NULL,
	[Proc_ID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
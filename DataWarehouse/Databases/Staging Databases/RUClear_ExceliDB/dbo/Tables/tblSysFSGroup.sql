﻿CREATE TABLE [dbo].[tblSysFSGroup](
	[ID] [int] NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Description] [varchar](200) NULL,
	[DisplayHierarchy_SLU] [tinyint] NOT NULL,
	[DisplayOrder] [smallint] NOT NULL,
	[Parent_SysFSGroup_ID] [int] NULL,
	[IsUDFGroup] [bit] NOT NULL,
	[IsDocGroup] [bit] NOT NULL,
	[IsScriptGroup] [bit] NOT NULL,
	[IsMapGroup] [bit] NOT NULL CONSTRAINT [DF_tblSysFSGroup_IsMapGroup]  DEFAULT (0),
	[IsCAMGroup] [bit] NOT NULL CONSTRAINT [DF_tblSysFSGroup_IsCAMGroup]  DEFAULT (0),
	[SeeAlso] [tinyint] NOT NULL,
	[IsDynamic] [bit] NOT NULL,
	[IsSource] [bit] NOT NULL,
	[SysModule_ID] [int] NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]
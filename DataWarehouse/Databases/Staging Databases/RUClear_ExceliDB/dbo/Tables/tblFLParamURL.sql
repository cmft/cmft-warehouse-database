﻿CREATE TABLE [dbo].[tblFLParamURL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormLibItemID] [int] NOT NULL,
	[ParamID] [int] NOT NULL,
	[ParamName] [varchar](250) NOT NULL,
	[FieldType] [bit] NOT NULL,
	[FieldID] [varchar](250) NOT NULL,
	[DataSource] [nvarchar](2000) NULL,
	[MaskedURL] [varchar](250) NOT NULL,
	[URLObject] [image] NULL,
	[IsActive] [bit] NOT NULL,
	[LastModifiedUserID] [int] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
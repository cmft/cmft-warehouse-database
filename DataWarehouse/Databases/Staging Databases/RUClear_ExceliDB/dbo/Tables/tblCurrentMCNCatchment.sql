﻿CREATE TABLE [dbo].[tblCurrentMCNCatchment](
	[Pat_ID] [int] NOT NULL,
	[MCN_ID] [int] NOT NULL,
	[CurrentMCN_Catchment_ID] [int] NOT NULL,
	[CurrentMCN_Location] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModified_User_ID] [int] NULL,
	[LastModifiedDeletedDate] [datetime] NULL,
	[LastModifiedDeleted_User_ID] [int] NULL,
	[TypeofChange] [char](1) NULL
) ON [PRIMARY]
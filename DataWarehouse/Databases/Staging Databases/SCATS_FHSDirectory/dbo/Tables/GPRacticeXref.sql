﻿CREATE TABLE [dbo].[GPRacticeXref](
	[SPid] [int] NOT NULL,
	[spcode] [varchar](10) NOT NULL,
	[personid] [int] NOT NULL,
	[gpid] [varchar](10) NOT NULL,
	[surname] [varchar](30) NOT NULL,
	[forename] [varchar](20) NOT NULL,
	[Midinit] [char](3) NULL,
	[gendercode] [char](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SPServiceAvailability](
	[SPId] [int] NOT NULL,
	[ServiceCode] [smallint] NOT NULL,
	[ServiceTypeCode] [smallint] NOT NULL,
	[ServiceAvailCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPServiceAvailability_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
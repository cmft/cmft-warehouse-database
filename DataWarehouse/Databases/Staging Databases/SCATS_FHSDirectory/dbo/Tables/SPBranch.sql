﻿CREATE TABLE [dbo].[SPBranch](
	[SPId] [int] NOT NULL,
	[SPBranchId] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UserIdLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
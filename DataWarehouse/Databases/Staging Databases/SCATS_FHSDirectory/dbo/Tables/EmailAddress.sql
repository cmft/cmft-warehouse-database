﻿CREATE TABLE [dbo].[EmailAddress](
	[SPId] [int] NOT NULL,
	[EmailAddr] [varchar](255) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
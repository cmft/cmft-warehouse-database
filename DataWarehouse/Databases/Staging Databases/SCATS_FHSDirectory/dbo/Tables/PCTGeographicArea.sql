﻿CREATE TABLE [dbo].[PCTGeographicArea](
	[PCTCode] [char](3) NOT NULL,
	[GeogAreaCode] [char](3) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PCTGeographicArea_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
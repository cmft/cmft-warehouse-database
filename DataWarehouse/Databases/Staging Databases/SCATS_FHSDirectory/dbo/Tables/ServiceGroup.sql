﻿CREATE TABLE [dbo].[ServiceGroup](
	[ServiceCode] [smallint] NOT NULL,
	[ServiceTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ServiceGroup_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
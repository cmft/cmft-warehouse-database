﻿CREATE TABLE [dbo].[SPPerson](
	[SPId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[RemoveInd] [char](1) NULL CONSTRAINT [DF_SPPerson_RemoveInd]  DEFAULT ('N'),
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPPerson_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PrevServiceProvider](
	[SPId] [int] NOT NULL,
	[ArchiveDate] [datetime] NOT NULL,
	[SPTypeCode] [smallint] NULL,
	[SPCode] [varchar](10) NOT NULL,
	[PCTCode] [char](3) NULL,
	[GeogAreaCode] [char](3) NULL,
	[SPName] [varchar](50) NULL,
	[AddInfo] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SPTypeService](
	[ServiceCode] [smallint] NOT NULL,
	[SPTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPTypeService_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
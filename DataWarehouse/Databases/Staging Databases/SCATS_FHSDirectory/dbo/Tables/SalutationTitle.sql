﻿CREATE TABLE [dbo].[SalutationTitle](
	[SalutTitleCode] [smallint] IDENTITY(1,1) NOT NULL,
	[SalutTitleDesc] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PersonTitle_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SPClinicSession](
	[SPClinicSessId] [int] IDENTITY(1,1) NOT NULL,
	[SPId] [int] NOT NULL,
	[ClinicCode] [smallint] NULL,
	[DayWeekId] [char](3) NULL,
	[OpenTimeNo] [smallint] NULL,
	[TimeBandNo] [smallint] NULL,
	[StartFinishCode] [smallint] NULL,
	[ClinicActivityCode] [smallint] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPClinicSession_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
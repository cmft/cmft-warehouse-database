﻿CREATE TABLE [dbo].[FNCGPExtract](
	[practicecode] [varchar](10) NOT NULL,
	[GPid] [varchar](10) NOT NULL,
	[personid] [int] NOT NULL,
	[surname] [varchar](30) NULL,
	[forename] [varchar](20) NULL,
	[Midinit] [char](3) NULL,
	[gendercode] [char](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicActivity](
	[ClinicActivityCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ClinicActivityDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ClinicActivity_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
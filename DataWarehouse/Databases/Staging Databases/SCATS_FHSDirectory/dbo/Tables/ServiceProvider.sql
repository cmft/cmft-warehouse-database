﻿CREATE TABLE [dbo].[ServiceProvider](
	[SPId] [int] IDENTITY(1,1) NOT NULL,
	[SPTypeCode] [smallint] NULL,
	[SPCode] [varchar](10) NULL,
	[PCTCode] [char](3) NULL,
	[GeogAreaCode] [char](3) NULL,
	[SPName] [varchar](100) NULL,
	[AddInfo] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ServiceProvider_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
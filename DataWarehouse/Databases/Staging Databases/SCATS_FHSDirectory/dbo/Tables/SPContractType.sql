﻿CREATE TABLE [dbo].[SPContractType](
	[SPCTNo] [int] IDENTITY(1,1) NOT NULL,
	[SPTypeCode] [smallint] NOT NULL,
	[ContractTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPContractType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastupdated] [smallint] NOT NULL
) ON [PRIMARY]
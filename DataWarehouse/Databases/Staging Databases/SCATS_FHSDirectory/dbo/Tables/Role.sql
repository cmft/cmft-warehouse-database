﻿CREATE TABLE [dbo].[Role](
	[RoleCode] [smallint] IDENTITY(1,1) NOT NULL,
	[RoleDesc] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Role_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
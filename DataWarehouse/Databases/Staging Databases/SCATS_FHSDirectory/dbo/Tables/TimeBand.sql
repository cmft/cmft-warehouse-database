﻿CREATE TABLE [dbo].[TimeBand](
	[TimeBandNo] [smallint] IDENTITY(1,1) NOT NULL,
	[TimeBandDesc] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_TimeBand_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
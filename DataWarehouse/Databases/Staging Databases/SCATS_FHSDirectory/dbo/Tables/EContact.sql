﻿CREATE TABLE [dbo].[EContact](
	[EContactNo] [int] IDENTITY(1,1) NOT NULL,
	[SPId] [int] NOT NULL,
	[TelTypeCode] [smallint] NOT NULL,
	[STDCode] [char](5) NULL,
	[TelContactNo] [varchar](12) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_EContact_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
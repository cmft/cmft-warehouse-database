﻿CREATE TABLE [dbo].[ServiceAvailability](
	[ServiceAvailCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ServiceAvailDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ServiceAvailability_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
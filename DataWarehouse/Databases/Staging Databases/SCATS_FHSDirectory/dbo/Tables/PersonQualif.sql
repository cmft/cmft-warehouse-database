﻿CREATE TABLE [dbo].[PersonQualif](
	[SPTPQNo] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PersonQualif_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
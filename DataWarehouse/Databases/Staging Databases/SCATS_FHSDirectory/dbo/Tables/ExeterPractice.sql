﻿CREATE TABLE [dbo].[ExeterPractice](
	[Practice_Code] [varchar](6) NULL,
	[PCG_Code] [varchar](5) NULL,
	[Practice_Name] [varchar](50) NULL,
	[Practice_Start_Date] [datetime] NULL
) ON [PRIMARY]
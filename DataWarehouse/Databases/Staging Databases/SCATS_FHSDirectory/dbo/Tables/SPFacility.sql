﻿CREATE TABLE [dbo].[SPFacility](
	[SPId] [int] NOT NULL,
	[FacilityCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
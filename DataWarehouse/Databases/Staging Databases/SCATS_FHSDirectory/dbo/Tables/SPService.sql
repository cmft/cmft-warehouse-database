﻿CREATE TABLE [dbo].[SPService](
	[SPId] [int] NOT NULL,
	[ServiceCode] [smallint] NOT NULL,
	[ServiceTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPService_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
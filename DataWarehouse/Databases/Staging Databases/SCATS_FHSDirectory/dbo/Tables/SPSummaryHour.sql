﻿CREATE TABLE [dbo].[SPSummaryHour](
	[SPId] [int] NOT NULL,
	[SPTypeCode] [smallint] NOT NULL,
	[ContractedHours] [decimal](5, 2) NOT NULL CONSTRAINT [DF_SPSummaryHour_ContractedHours]  DEFAULT (0),
	[ActualOpenHours] [decimal](5, 2) NOT NULL CONSTRAINT [DF_SPSummaryHour_ActualOpenHours]  DEFAULT (0),
	[SuppHours] [decimal](5, 2) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPSummaryHour_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
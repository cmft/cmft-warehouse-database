﻿CREATE TABLE [dbo].[StartFinish](
	[StartFinishCode] [smallint] IDENTITY(1,1) NOT NULL,
	[StartFinishDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_StartFinish_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AddressUnit](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[HouseNo] [varchar](10) NULL,
	[HouseName] [varchar](100) NULL,
	[StreetName] [varchar](100) NULL,
	[AddrQualif] [varchar](100) NULL,
	[SuburbCode] [int] NULL,
	[Town] [varchar](30) NULL,
	[PostCode] [varchar](8) NULL,
	[County] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AddressUnit_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
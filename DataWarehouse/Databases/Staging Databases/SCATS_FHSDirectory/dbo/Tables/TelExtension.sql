﻿CREATE TABLE [dbo].[TelExtension](
	[EContactNo] [int] NOT NULL,
	[TelExtension] [char](5) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
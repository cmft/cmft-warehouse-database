﻿CREATE TABLE [dbo].[PCT](
	[PCTCode] [char](3) NOT NULL,
	[PCTName] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PCT_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PrevServiceProviderOpenHour](
	[SPId] [int] NULL,
	[Archivedate] [datetime] NULL,
	[SPODayWeekId] [char](3) NULL,
	[SPOOpenTimeNo] [smallint] NULL,
	[SPOTimeBandNo] [smallint] NULL,
	[SPOStartFinishCode] [smallint] NULL,
	[SPClinicSessId] [int] NULL,
	[ClinicCode] [smallint] NULL,
	[SPCWeekDayId] [char](3) NULL,
	[SPCOpentimeNo] [smallint] NULL,
	[SPCTimeBandNo] [smallint] NULL,
	[SPCStartFinishCode] [smallint] NULL,
	[SPCClinicActivityCode] [smallint] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
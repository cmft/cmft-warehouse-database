﻿CREATE TABLE [dbo].[WeekDay](
	[DayWeekId] [char](3) NOT NULL,
	[DayNo] [smallint] NOT NULL,
	[DayWeekName] [char](9) NOT NULL,
	[TstampLastupdated] [datetime] NOT NULL CONSTRAINT [DF_WeekDay_TstampLastupdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[suburb](
	[SuburbCode] [int] IDENTITY(1,1) NOT NULL,
	[Suburbname] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_suburb_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ContractType](
	[ContractTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ContractTypeDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ContractType_TimestampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
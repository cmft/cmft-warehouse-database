﻿CREATE TABLE [dbo].[SPClinic](
	[SPId] [int] NOT NULL,
	[ClinicCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPClinic_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service](
	[ServiceCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ServiceDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Service_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
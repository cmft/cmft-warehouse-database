﻿CREATE TABLE [dbo].[TelType](
	[TelTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[TelephoneTypeDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_TelephoneType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
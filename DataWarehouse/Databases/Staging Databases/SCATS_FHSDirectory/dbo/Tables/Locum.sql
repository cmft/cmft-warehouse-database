﻿CREATE TABLE [dbo].[Locum](
	[PersonId] [int] NOT NULL,
	[PCTCode] [char](3) NOT NULL,
	[GeogAreaCode] [char](3) NOT NULL,
	[LocumStartDate] [datetime] NOT NULL,
	[LocumEndDate] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Locum_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
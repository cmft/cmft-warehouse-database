﻿CREATE TABLE [dbo].[TestSPPractitioner](
	[spid] [int] NULL,
	[personid] [int] NULL,
	[surname] [varchar](30) NULL,
	[forename] [varchar](20) NULL,
	[spcode] [varchar](10) NULL,
	[sptypecode] [smallint] NULL
) ON [PRIMARY]
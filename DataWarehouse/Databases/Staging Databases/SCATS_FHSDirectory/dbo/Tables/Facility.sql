﻿CREATE TABLE [dbo].[Facility](
	[FacilityCode] [smallint] IDENTITY(1,1) NOT NULL,
	[FacilityDesc] [varchar](30) NOT NULL,
	[TimestampLastUpDated] [datetime] NOT NULL CONSTRAINT [DF_Facility_TimestampLastUpDated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Clinic](
	[ClinicCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ClinicDesc] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Clinic_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
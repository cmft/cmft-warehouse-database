﻿CREATE TABLE [dbo].[WTEquivalent](
	[WTECode] [smallint] IDENTITY(1,1) NOT NULL,
	[WTEAltCode] [char](1) NOT NULL,
	[WTEAltDesc] [varchar](30) NOT NULL,
	[WorkHours] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_WTEquivalent_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PractitionerSpeciality](
	[SPId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[ServiceCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PractisingReg_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
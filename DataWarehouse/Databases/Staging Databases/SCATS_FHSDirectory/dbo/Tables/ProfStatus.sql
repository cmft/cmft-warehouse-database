﻿CREATE TABLE [dbo].[ProfStatus](
	[ProfStatusCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ProfStatusDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ProfStatus_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
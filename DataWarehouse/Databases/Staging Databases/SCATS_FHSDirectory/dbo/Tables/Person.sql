﻿CREATE TABLE [dbo].[Person](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,
	[Surname] [varchar](30) NOT NULL,
	[ForeName] [varchar](30) NOT NULL,
	[MidInit] [char](50) NULL,
	[SalutTitleCode] [smallint] NULL,
	[GenderCode] [char](1) NULL,
	[RoleCode] [smallint] NOT NULL,
	[RegId] [varchar](20) NULL,
	[RegistrationDate] [datetime] NULL,
	[DOB] [datetime] NULL,
	[LocumInd] [char](1) NOT NULL CONSTRAINT [DF_Person_LocumInd]  DEFAULT ('N'),
	[ExeterGPLocalCode] [varchar](6) NULL,
	[ExeterForename] [varchar](100) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Person_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
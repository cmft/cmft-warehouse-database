﻿CREATE TABLE [dbo].[SPOpening](
	[SPId] [int] NOT NULL,
	[DayWeekId] [char](3) NOT NULL,
	[OpenTimeNo] [smallint] NOT NULL,
	[TimeBandNo] [smallint] NOT NULL,
	[StartFinishCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPOpening_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
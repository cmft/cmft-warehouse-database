﻿CREATE TABLE [dbo].[SPTProfQualif](
	[SPTPQNo] [int] IDENTITY(1,1) NOT NULL,
	[SPTypeCode] [smallint] NOT NULL,
	[ProfQualifCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPTProfQualif_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
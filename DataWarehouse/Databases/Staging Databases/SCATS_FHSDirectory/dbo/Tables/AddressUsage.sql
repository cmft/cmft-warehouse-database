﻿CREATE TABLE [dbo].[AddressUsage](
	[AddressId] [int] NOT NULL,
	[SPId] [int] NOT NULL,
	[AddrUsageTypeCode] [smallint] NOT NULL,
	[DateEffective] [datetime] NOT NULL,
	[DateInactive] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_AddressUsage_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
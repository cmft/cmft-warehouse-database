﻿CREATE TABLE [dbo].[LangWithSP](
	[SPId] [int] NOT NULL,
	[LangCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_LangWithSP_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
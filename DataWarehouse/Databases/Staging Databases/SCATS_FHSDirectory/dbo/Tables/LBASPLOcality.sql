﻿CREATE TABLE [dbo].[LBASPLOcality](
	[spid] [int] NOT NULL,
	[spname] [varchar](50) NULL,
	[spcode] [varchar](10) NOT NULL,
	[AddressId] [int] NOT NULL,
	[HouseNo] [varchar](10) NULL,
	[HouseName] [varchar](30) NULL,
	[StreetName] [varchar](30) NULL,
	[AddrQualif] [varchar](30) NULL,
	[SuburbCode] [int] NULL,
	[Town] [varchar](30) NULL,
	[PostCode] [varchar](8) NULL,
	[County] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
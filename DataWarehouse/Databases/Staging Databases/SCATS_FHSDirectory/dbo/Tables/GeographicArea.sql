﻿CREATE TABLE [dbo].[GeographicArea](
	[GeogAreaCode] [char](3) NOT NULL,
	[GeogAreaDesc] [varchar](50) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_GeographicArea_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
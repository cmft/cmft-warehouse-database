﻿CREATE TABLE [dbo].[PrevPerson](
	[PersonId] [int] NOT NULL,
	[ArchiveDate] [datetime] NOT NULL,
	[Surname] [varchar](30) NOT NULL,
	[Forename] [varchar](20) NOT NULL,
	[MidInit] [varchar](50) NULL,
	[SalutTitleCode] [smallint] NULL,
	[GenderCode] [char](1) NULL,
	[RoleCode] [smallint] NULL,
	[LocumInd] [char](1) NOT NULL,
	[RegId] [varchar](20) NULL,
	[RegistrationDate] [datetime] NULL,
	[DOB] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UserIdLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
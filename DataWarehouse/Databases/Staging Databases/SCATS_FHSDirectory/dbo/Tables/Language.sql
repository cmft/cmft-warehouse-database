﻿CREATE TABLE [dbo].[Language](
	[LangCode] [smallint] IDENTITY(1,1) NOT NULL,
	[LangDesc] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Language_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
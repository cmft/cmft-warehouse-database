﻿CREATE TABLE [dbo].[OpeningTime](
	[OpenTimeNo] [smallint] IDENTITY(1,1) NOT NULL,
	[OpenTimeDesc] [varchar](5) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_OpeningTime_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
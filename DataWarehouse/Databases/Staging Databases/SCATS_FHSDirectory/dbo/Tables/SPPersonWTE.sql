﻿CREATE TABLE [dbo].[SPPersonWTE](
	[SPCTNo] [int] NOT NULL,
	[WTENo] [int] NOT NULL,
	[SPId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[WTEStartDate] [datetime] NOT NULL,
	[WTEEndDate] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPPersonWTE_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ServiceType](
	[ServiceTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ServiceTypeDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ServiceType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [char](10) NOT NULL
) ON [PRIMARY]
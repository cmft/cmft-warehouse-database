﻿CREATE TABLE [dbo].[PractitionerContract](
	[SPCTNo] [int] NOT NULL,
	[SPId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[ActiveInd] [char](1) NOT NULL CONSTRAINT [DF_PractitionerContract_ActiveInd]  DEFAULT ('N'),
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[TerminationReason] [varchar](100) NULL,
	[ProfStatusCode] [smallint] NULL,
	[ContractNo] [varchar](20) NULL,
	[ActualTerminationDate] [datetime] NULL,
	[ContractNotes] [varchar](500) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PractitionerContract_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
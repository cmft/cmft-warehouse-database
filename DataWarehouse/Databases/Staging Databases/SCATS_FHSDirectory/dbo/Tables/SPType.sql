﻿CREATE TABLE [dbo].[SPType](
	[SPTypeCode] [smallint] IDENTITY(1,1) NOT NULL,
	[SPTypeDesc] [varchar](30) NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SPType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
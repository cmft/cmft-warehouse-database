﻿CREATE TABLE [dbo].[PrevPersonDets](
	[PersonId] [int] NOT NULL,
	[ArchiveDate] [datetime] NOT NULL,
	[SPCTNo] [int] NULL,
	[SPId] [int] NULL,
	[ServiceCode] [smallint] NULL,
	[ProfQualifCode] [smallint] NULL,
	[RemoveInd] [char](1) NULL,
	[LocumStartDate] [datetime] NULL,
	[LocumEndDate] [datetime] NULL,
	[TstampLastUpdated] [datetime] NOT NULL,
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
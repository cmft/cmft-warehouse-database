﻿CREATE TABLE [dbo].[WTESPType](
	[WTENo] [int] IDENTITY(1,1) NOT NULL,
	[WTECode] [smallint] NOT NULL,
	[SPTypeCode] [smallint] NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_WTESPType_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
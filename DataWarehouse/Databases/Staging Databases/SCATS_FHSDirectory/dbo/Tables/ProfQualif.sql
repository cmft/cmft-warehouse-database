﻿CREATE TABLE [dbo].[ProfQualif](
	[ProfQualifCode] [smallint] IDENTITY(1,1) NOT NULL,
	[ProfQualifDesc] [varchar](30) NOT NULL,
	[TstampLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_ProfQualif_TstampLastUpdated]  DEFAULT (getdate()),
	[UseridLastUpdated] [smallint] NOT NULL
) ON [PRIMARY]
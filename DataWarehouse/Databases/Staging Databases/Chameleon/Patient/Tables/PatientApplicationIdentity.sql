﻿CREATE TABLE [Patient].[PatientApplicationIdentity](
	[PatientId] [int] NOT NULL,
	[SourceApplicationIdentifierId] [int] NOT NULL,
	[PatientApplicationIdentityValue] [nvarchar](50) NOT NULL CONSTRAINT [DF_PatientApplicationIdentity_Name]  DEFAULT (N'Unknown')
) ON [PRIMARY]
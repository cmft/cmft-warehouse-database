﻿CREATE TABLE [TSE].[QueueArchive](
	[QueueId] [bigint] NOT NULL,
	[WorkStreamId] [tinyint] NOT NULL,
	[SourceApplicationContextId] [int] NULL,
	[QueueType] [varchar](20) NULL,
	[PatientId] [int] NULL,
	[PatientLocationIndex] [varchar](100) NULL,
	[isProcessed] [tinyint] NOT NULL,
	[InitialDTTM] [datetime2](7) NOT NULL,
	[ReceivedDTTM] [datetime2](7) NOT NULL,
	[QueuePriority] [int] NULL,
	[Outcome] [varchar](100) NULL,
	[HL7MessageObjectReferenceId] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [TSE].[PatientSynchronisation](
	[PatientId] [int] NOT NULL,
	[Postcode] [varchar](10) NULL,
	[GPPCode] [varchar](10) NULL,
	[PostcodeHistoryList] [varchar](200) NULL,
	[GPPCodeHistoryList] [varchar](200) NULL,
	[OriginalForenames] [varchar](100) NULL,
	[OriginalSurname] [varchar](100) NULL,
	[OriginalPostcode] [varchar](100) NULL,
	[ForenameSoundex] [varchar](100) NULL,
	[SurnameSoundex] [varchar](100) NULL,
	[PostcodeSoundex] [varchar](100) NULL
) ON [PRIMARY]
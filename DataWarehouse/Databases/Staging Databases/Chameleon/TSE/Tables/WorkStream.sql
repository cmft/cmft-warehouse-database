﻿CREATE TABLE [TSE].[WorkStream](
	[WorkStreamId] [tinyint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](50) NOT NULL CONSTRAINT [DF_WorkStream_Name]  DEFAULT (N'Unknown'),
	[Description] [Type].[Description] NOT NULL CONSTRAINT [DF_WorkStream_Description]  DEFAULT ('')
) ON [PRIMARY]
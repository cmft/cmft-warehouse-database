﻿CREATE TABLE [TSE].[TSEConfiguration](
	[ID] [tinyint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Identifier] [varchar](50) NOT NULL,
	[Value] [varchar](2) NULL
) ON [PRIMARY]
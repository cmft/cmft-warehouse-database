﻿CREATE TABLE [TSE].[Queue](
	[QueueId] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[WorkStreamId] [tinyint] NOT NULL CONSTRAINT [DF_Queue_WorkStreamId]  DEFAULT ((1)),
	[SourceApplicationContextId] [int] NULL,
	[QueueType] [varchar](20) NULL,
	[PatientId] [int] NULL,
	[PatientLocationIndex] [varchar](100) NULL,
	[isProcessed] [tinyint] NOT NULL CONSTRAINT [DF_Queue_isProcessed]  DEFAULT ((0)),
	[InitialDTTM] [datetime2](7) NOT NULL CONSTRAINT [DF_Queue_InitialDTTM]  DEFAULT (sysutcdatetime()),
	[ReceivedDTTM] [datetime2](7) NOT NULL CONSTRAINT [DF_Queue_ReceivedDTTM]  DEFAULT (sysutcdatetime()),
	[QueuePriority] [int] NULL,
	[Outcome] [varchar](100) NULL,
	[HL7MessageObjectReferenceId] [bigint] NULL
) ON [PRIMARY]
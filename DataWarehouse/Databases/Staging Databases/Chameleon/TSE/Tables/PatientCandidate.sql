﻿CREATE TABLE [TSE].[PatientCandidate](
	[PatientCadidateId] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[QueueId] [bigint] NOT NULL,
	[SourceApplicationContextId] [int] NULL,
	[PatientLocationIndex] [varchar](100) NULL,
	[PatientId] [int] NULL,
	[SurnameOriginal] [varchar](200) NULL,
	[ForenameOriginal] [varchar](200) NULL,
	[DOB] [varchar](500) NULL,
	[Gender] [varchar](500) NULL,
	[Address] [varchar](500) NULL,
	[PostCodeOriginal] [varchar](500) NULL,
	[NHS] [varchar](500) NULL,
	[GPP] [varchar](500) NULL,
	[Outcome] [varchar](5) NULL,
	[MatrixRule] [bigint] NULL
) ON [PRIMARY]
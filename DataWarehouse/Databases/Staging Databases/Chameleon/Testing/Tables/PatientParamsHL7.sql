﻿CREATE TABLE [Testing].[PatientParamsHL7](
	[PatientId] [int] NOT NULL,
	[PatientAddressHL7] [varchar](4000) NULL,
	[PatientContactHL7] [varchar](4000) NULL,
	[NextOfKinHL7] [varchar](8000) NULL,
	[HL7toXML] [varchar](8000) NULL,
	[InsertDTTM] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [PAS].[OCServiceResultDisplayOrder](
	[ServiceCode] [int] NOT NULL,
	[ResultCode] [varchar](4) NOT NULL,
	[DisplayOrder] [int] NOT NULL
) ON [PRIMARY]
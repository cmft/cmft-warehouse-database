﻿CREATE TABLE [PAS].[OCMSERVICE](
	[Service] [int] NOT NULL,
	[Description] [varchar](30) NULL,
	[ScreenType] [varchar](4) NULL,
	[CategoryOfServiceInt] [tinyint] NULL
) ON [PRIMARY]
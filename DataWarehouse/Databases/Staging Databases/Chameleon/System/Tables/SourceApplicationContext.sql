﻿CREATE TABLE [System].[SourceApplicationContext](
	[SourceApplicationContextId] [int] IDENTITY(0,1) NOT FOR REPLICATION NOT NULL,
	[SourceApplicationId] [int] NOT NULL,
	[SiteId] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [System].[Site](
	[SiteId] [int] IDENTITY(0,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](50) NOT NULL CONSTRAINT [DF_Site_Name]  DEFAULT (N'Unknown'),
	[Description] [Type].[Description] NOT NULL CONSTRAINT [DF_Site_Description]  DEFAULT (''),
	[HL7SendingFacility] [varchar](50) NOT NULL CONSTRAINT [DF_Site_HL7SendingFacility]  DEFAULT (N'Unknown'),
	[FacilityCode] [varchar](10) NOT NULL CONSTRAINT [DF_Site_FacilityCode]  DEFAULT (N'Unknown')
) ON [PRIMARY]
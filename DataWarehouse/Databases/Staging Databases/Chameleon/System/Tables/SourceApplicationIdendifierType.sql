﻿CREATE TABLE [System].[SourceApplicationIdendifierType](
	[SourceApplicationIdentifierTypeId] [tinyint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL CONSTRAINT [DF_SourceApplicationIdendifierType_Name1]  DEFAULT (N'Unknown'),
	[Description] [Type].[Description] NOT NULL CONSTRAINT [DF_SourceApplicationIdendifierType_Description]  DEFAULT ('')
) ON [PRIMARY]
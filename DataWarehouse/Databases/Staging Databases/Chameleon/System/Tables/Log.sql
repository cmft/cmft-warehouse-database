﻿CREATE TABLE [System].[Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventDTTM] [datetime2](7) NULL,
	[Type] [varchar](50) NULL,
	[Event] [varchar](100) NULL,
	[Description] [Type].[Description] NULL,
	[Username] [varchar](50) NULL,
	[Keys] [varchar](max) NULL,
	[MessageID] [varchar](50) NULL,
	[ReceiveID] [varchar](50) NULL,
	[SendID] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
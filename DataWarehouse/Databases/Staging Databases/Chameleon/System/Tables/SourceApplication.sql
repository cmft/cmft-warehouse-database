﻿CREATE TABLE [System].[SourceApplication](
	[SourceApplicationId] [int] IDENTITY(0,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](50) NOT NULL CONSTRAINT [DF_SourceApplication_Name]  DEFAULT (N'Unknown'),
	[Description] [Type].[Description] NOT NULL CONSTRAINT [DF_SourceApplication_Description]  DEFAULT (''),
	[HL7SendingApplication] [varchar](50) NOT NULL CONSTRAINT [DF_SourceApplication_SendingApplication]  DEFAULT (N'Unknown')
) ON [PRIMARY]
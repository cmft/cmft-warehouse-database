﻿CREATE TABLE [dbo].[STAFF_SECURITY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Staff_Number] [int] NOT NULL,
	[Password] [varchar](255) NULL,
	[Site] [varchar](25) NULL,
	[User_Function] [nvarchar](5) NULL,
	[Performer] [bit] NULL,
	[Pacing] [bit] NULL,
	[Active] [bit] NULL,
	[LastPatient] [int] NULL,
	[AllowForms] [int] NULL CONSTRAINT [DEFAllowForms]  DEFAULT ((0)),
	[LastAccessed] [datetime] NULL,
	[LastMachine] [varchar](50) NULL,
	[MY_DIARY] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AUTHORS](
	[AU_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[AU_CODE] [varchar](10) NULL,
	[AU_NAME] [varchar](50) NULL CONSTRAINT [DF_AUTHORS_AU_NAME]  DEFAULT (N'fred'),
	[AU_GRADE] [varchar](75) NULL,
	[AU_SITE] [varchar](10) NULL,
	[AU_ORDER] [varchar](10) NULL,
	[AU_STATUS] [bit] NOT NULL CONSTRAINT [DF_AUTHORS_AU_STATUS]  DEFAULT ((1))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SERIALISED_STOCK](
	[STOCK_NO] [int] IDENTITY(1,1) NOT NULL,
	[REF_NO] [int] NULL,
	[STOCK_TYPE] [nvarchar](5) NULL,
	[DATE_ADDED] [datetime] NULL,
	[USER_ADDED] [nvarchar](50) NULL,
	[DATE_ISSUED] [datetime] NULL,
	[USER_ISSUED] [nvarchar](50) NULL,
	[DATE_RETURNED] [datetime] NULL,
	[USER_RETURNED] [nvarchar](50) NULL,
	[DATE_DESTROYED] [datetime] NULL,
	[USER_DESTROYED] [nvarchar](50) NULL,
	[NO_ADDED] [int] NULL,
	[SERIAL_NO] [nvarchar](50) NULL,
	[HX_NUMBER] [int] NULL,
	[COST] [money] NULL,
	[CODE_NUMBER] [nvarchar](50) NULL,
	[STATUS] [bit] NOT NULL
) ON [PRIMARY]
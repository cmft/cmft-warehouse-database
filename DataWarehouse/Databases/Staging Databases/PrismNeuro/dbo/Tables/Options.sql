﻿CREATE TABLE [dbo].[Options](
	[SecurityID] [char](2) NULL,
	[UserOption] [varchar](255) NULL,
	[Userfunction] [varchar](255) NULL,
	[title] [varchar](50) NULL,
	[role] [varchar](50) NULL,
	[Optorder] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[STAFF_ABSCENCE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[STAFF_NUMBER] [int] NULL,
	[SDATE] [datetime] NULL,
	[TYPE] [varchar](20) NULL,
	[AM] [bit] NULL,
	[PM] [bit] NULL,
	[COMMENT] [varchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ETHNIC_GROUPS](
	[E_ID] [int] IDENTITY(1,1) NOT NULL,
	[E_CODE] [char](10) NULL,
	[E_DETAILS] [varchar](100) NULL,
	[E_EXT] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ReportLookup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LookupGroup] [varchar](20) NULL,
	[LookupData] [varchar](70) NULL
) ON [PRIMARY]
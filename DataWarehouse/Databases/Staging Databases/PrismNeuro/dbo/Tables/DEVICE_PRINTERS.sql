﻿CREATE TABLE [dbo].[DEVICE_PRINTERS](
	[ID] [int] NULL,
	[Machine] [varchar](50) NULL,
	[PrinterType] [varchar](50) NULL,
	[PrinterName] [varchar](50) NULL,
	[PrinterDriver] [varchar](50) NULL,
	[PrinterPort] [varchar](50) NULL
) ON [PRIMARY]
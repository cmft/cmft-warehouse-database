﻿CREATE TABLE [dbo].[WARD_DEPT](
	[WD_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[WD_CODE] [nvarchar](6) NULL,
	[WD_SOURCE_TYPE] [nvarchar](5) NULL,
	[WD_TITLE] [nvarchar](40) NULL,
	[WD_SITE] [nvarchar](6) NULL,
	[WD_EXT_CODE] [nvarchar](10) NULL,
	[WD_MALE] [bit] NULL CONSTRAINT [DF_WARD_DEPT_WD_MALE]  DEFAULT (0),
	[WD_FEMALE] [bit] NULL CONSTRAINT [DF_WARD_DEPT_WD_FEMALE]  DEFAULT (0),
	[WD_DATE_FROM] [datetime] NULL,
	[WD_DATE_TO] [datetime] NULL,
	[WD_LOW_AGE] [smallint] NULL,
	[WD_UPR_AGE] [smallint] NULL,
	[WD_STATUS] [bit] NULL,
	[WD_DOC_ONLY] [bit] NULL,
	[WD_ORDER] [nvarchar](50) NULL
) ON [PRIMARY]
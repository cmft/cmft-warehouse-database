﻿CREATE TABLE [dbo].[DaySheetSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](50) NULL,
	[UserNum] [int] NULL,
	[Site] [varchar](50) NULL,
	[Device] [varchar](50) NULL,
	[Source] [varchar](50) NULL,
	[Referrer] [int] NULL,
	[Activity] [int] NULL,
	[ActGroup] [int] NULL,
	[Staff] [varchar](50) NULL,
	[GP] [int] NULL,
	[WardClinic] [int] NULL,
	[Clinician] [int] NULL,
	[PMT] [varchar](50) NULL,
	[SelectedSite] [varchar](50) NULL,
	[ReportStatus] [int] NULL,
	[DefaultRange] [int] NULL,
	[AllProcedures] [int] NULL,
	[DisplayOrder] [varchar](50) NULL,
	[Show] [int] NULL,
	[DisplayHno] [int] NULL,
	[DisplayNHS] [int] NULL,
	[DisplaySource] [int] NULL,
	[DisplayWard] [int] NULL,
	[DisplayRef] [int] NULL,
	[DisplaySite] [int] NULL,
	[DisplayStaff] [int] NULL,
	[DisplayReport] [int] NULL,
	[DisplayPrismNo] [int] NULL,
	[SelectMode] [int] NULL,
	[DisplayTimes] [int] NULL CONSTRAINT [DT_checkd]  DEFAULT ((0)),
	[ReferringSite] [varchar](50) NULL
) ON [PRIMARY]
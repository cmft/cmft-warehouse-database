﻿CREATE TABLE [dbo].[groupFIELDS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataSetId] [nvarchar](10) NULL,
	[GroupId] [nvarchar](10) NULL,
	[FieldType] [nvarchar](50) NULL,
	[FieldName] [nvarchar](100) NULL,
	[FieldValue] [nvarchar](50) NULL,
	[ExtraParameter1] [nvarchar](255) NULL,
	[ExtraParameter2] [nvarchar](50) NULL,
	[TextLabel] [nvarchar](100) NULL,
	[DefaultDate] [nvarchar](50) NULL,
	[Position] [int] NULL,
	[Qualifier] [nvarchar](50) NULL,
	[HelpText] [nvarchar](50) NULL
) ON [PRIMARY]
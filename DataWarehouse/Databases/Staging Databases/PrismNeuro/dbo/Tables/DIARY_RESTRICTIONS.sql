﻿CREATE TABLE [dbo].[DIARY_RESTRICTIONS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RES_ID] [int] NULL,
	[RType] [nvarchar](5) NULL,
	[code] [nvarchar](500) NULL,
	[allow] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DataSet_User_Names](
	[Field_Name] [varchar](50) NULL,
	[User_Name] [varchar](100) NULL,
	[DataSet] [varchar](50) NULL,
	[Mandatory] [bit] NOT NULL,
	[Mandatory_Discharge] [bit] NOT NULL
) ON [PRIMARY]
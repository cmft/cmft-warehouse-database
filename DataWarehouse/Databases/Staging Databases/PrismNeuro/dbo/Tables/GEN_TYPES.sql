﻿CREATE TABLE [dbo].[GEN_TYPES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GEN_TYPE] [varchar](10) NULL,
	[GEN_TITLE] [varchar](50) NULL,
	[GEN_EXT] [varchar](50) NULL
) ON [PRIMARY]
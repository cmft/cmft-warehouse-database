﻿CREATE TABLE [dbo].[ATTEND_TYPES](
	[ATT_ID] [int] IDENTITY(1,1) NOT NULL,
	[ATT_DETAILS] [varchar](50) NULL,
	[ATT_STATUS] [varchar](50) NULL,
	[deleted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_VIEWS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ViewTitle] [varchar](50) NULL,
	[ViewDescription] [varchar](50) NULL,
	[Documents] [int] NULL,
	[Olap] [int] NULL,
	[RM] [int] NULL
) ON [PRIMARY]
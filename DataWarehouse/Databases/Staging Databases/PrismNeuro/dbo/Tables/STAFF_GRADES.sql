﻿CREATE TABLE [dbo].[STAFF_GRADES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CODE] [nvarchar](5) NULL,
	[NATIONAL_CODE] [nvarchar](10) NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
	[PAY_SCALE] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ds_datasets](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DATASET] [varchar](50) NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[Prism] [bit] NULL,
	[PrismNet] [bit] NULL,
	[DS_Extract] [varchar](50) NULL
) ON [PRIMARY]
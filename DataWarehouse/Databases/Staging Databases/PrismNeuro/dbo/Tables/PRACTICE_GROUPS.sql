﻿CREATE TABLE [dbo].[PRACTICE_GROUPS](
	[GRP_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[GRP_CODE] [nvarchar](10) NULL,
	[GRP_NAME] [nvarchar](100) NULL,
	[GRP_ADDRESS1] [nvarchar](50) NULL,
	[GRP_ADDRESS2] [nvarchar](50) NULL,
	[GRP_ADDRESS3] [nvarchar](50) NULL,
	[GRP_ADDRESS4] [nvarchar](50) NULL,
	[GRP_ADDRESS5] [nvarchar](50) NULL,
	[GRP_POSTCODE] [nvarchar](9) NULL,
	[GRP_TEL] [nvarchar](20) NULL,
	[GRP_FAX] [nvarchar](20) NULL,
	[GRP_EMAIL] [nvarchar](50) NULL,
	[GRP_EXT_CODE] [nvarchar](10) NULL,
	[GRP_LOCAL] [int] NULL,
	[GRP_STATUS] [int] NULL
) ON [PRIMARY]
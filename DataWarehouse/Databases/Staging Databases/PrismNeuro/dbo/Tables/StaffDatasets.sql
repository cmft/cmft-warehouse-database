﻿CREATE TABLE [dbo].[StaffDatasets](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UsedID] [varchar](50) NULL,
	[DataSet] [varchar](50) NULL,
	[AllowView] [int] NULL,
	[AllowAmend] [int] NULL
) ON [PRIMARY]
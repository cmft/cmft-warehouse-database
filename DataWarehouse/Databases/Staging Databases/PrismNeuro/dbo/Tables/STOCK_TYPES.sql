﻿CREATE TABLE [dbo].[STOCK_TYPES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CODE] [nvarchar](10) NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
	[TABLE_] [nvarchar](50) NULL,
	[PROG_MODE] [smallint] NULL,
	[SERIALISED] [bit] NOT NULL
) ON [PRIMARY]
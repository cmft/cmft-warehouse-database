﻿CREATE TABLE [dbo].[DIARY_RESTRICTION_NAME](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[Colour] [int] NULL,
	[short_desc] [varchar](50) NULL
) ON [PRIMARY]
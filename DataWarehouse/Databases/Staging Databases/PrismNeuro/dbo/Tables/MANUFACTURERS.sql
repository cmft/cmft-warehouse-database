﻿CREATE TABLE [dbo].[MANUFACTURERS](
	[MFG_NO] [int] IDENTITY(1,1) NOT NULL,
	[MFG] [varchar](255) NULL,
	[NAME] [varchar](50) NULL,
	[EPS_NAME] [varchar](50) NULL,
	[ACTIVE] [bit] NOT NULL,
	[ADDRESS1] [varchar](50) NULL,
	[ADDRESS2] [varchar](50) NULL,
	[ADDRESS3] [varchar](50) NULL,
	[ADDRESS4] [varchar](50) NULL,
	[POST_CODE] [varchar](50) NULL,
	[WEB] [varchar](50) NULL,
	[TEL] [varchar](50) NULL,
	[FAX] [varchar](50) NULL,
	[CONTACT] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[comment_] [varchar](255) NULL
) ON [PRIMARY]
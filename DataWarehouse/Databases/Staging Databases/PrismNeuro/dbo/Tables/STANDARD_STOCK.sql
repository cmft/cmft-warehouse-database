﻿CREATE TABLE [dbo].[STANDARD_STOCK](
	[REF_NO] [int] IDENTITY(1,1) NOT NULL,
	[MFG_NO] [int] NULL,
	[MODEL] [nvarchar](50) NULL,
	[COMMENT_] [nvarchar](255) NULL,
	[RECORD_ADDED] [datetime] NULL,
	[CREATED_BY] [nvarchar](50) NULL,
	[UNIT_COST] [money] NULL,
	[MIN_LEVEL] [int] NULL,
	[NORMAL_LEVEL] [int] NULL,
	[RE_ORDER_NO] [int] NULL,
	[LEAD_TIME_DAYS] [smallint] NULL,
	[UNIT_QTY] [int] NULL,
	[STOCK_LEVEL] [int] NULL
) ON [PRIMARY]
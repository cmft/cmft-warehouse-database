﻿CREATE TABLE [dbo].[SITE_GROUPS](
	[GROUP_CODE] [nvarchar](5) NULL,
	[GROUP_TITLE] [nvarchar](50) NULL,
	[GROUP_EXT_CODE] [nvarchar](20) NULL
) ON [PRIMARY]
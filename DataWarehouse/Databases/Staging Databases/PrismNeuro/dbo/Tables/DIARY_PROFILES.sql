﻿CREATE TABLE [dbo].[DIARY_PROFILES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DR_NUMBER] [int] NULL,
	[DATE_CREATED] [smalldatetime] NULL,
	[DATE_START] [smalldatetime] NULL,
	[DATE_END] [smalldatetime] NULL,
	[DOW] [int] NULL,
	[ACTIVE] [int] NULL,
	[PROF_ID] [int] NULL,
	[PROFDESC] [varchar](50) NULL
) ON [PRIMARY]
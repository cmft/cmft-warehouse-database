﻿CREATE TABLE [dbo].[GROUPS](
	[GROUP_ID] [nvarchar](25) NULL,
	[DATA_SET_ID] [nvarchar](10) NULL,
	[GROUP_NAME] [nvarchar](50) NULL,
	[GROUP_LABEL] [nvarchar](50) NULL,
	[GROUP_POSITION] [int] NULL
) ON [PRIMARY]
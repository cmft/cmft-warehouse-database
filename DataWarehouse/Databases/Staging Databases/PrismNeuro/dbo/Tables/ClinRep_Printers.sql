﻿CREATE TABLE [dbo].[ClinRep_Printers](
	[Machine] [int] NULL,
	[Printer] [smallint] NULL,
	[Name] [varchar](50) NULL,
	[Driver] [varchar](50) NULL,
	[Port] [varchar](50) NULL
) ON [PRIMARY]
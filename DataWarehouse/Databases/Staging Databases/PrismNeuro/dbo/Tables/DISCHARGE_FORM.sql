﻿CREATE TABLE [dbo].[DISCHARGE_FORM](
	[DISID] [int] NOT NULL,
	[DATASET] [nvarchar](50) NULL,
	[TEMPLATE] [nvarchar](50) NULL,
	[PATH] [nvarchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EPISODES](
	[EP_ID] [int] NOT NULL,
	[EP_MS_ID] [int] NULL,
	[EP_CLIN] [int] NULL,
	[EP_START] [datetime] NULL,
	[EP_END] [datetime] NULL,
	[EP_GP] [int] NULL,
	[EP_DIAG_1] [int] NULL,
	[EP_DIAG_2] [int] NULL,
	[EP_DIAG_3] [int] NULL,
	[EP_COMMENT] [nvarchar](255) NULL
) ON [PRIMARY]
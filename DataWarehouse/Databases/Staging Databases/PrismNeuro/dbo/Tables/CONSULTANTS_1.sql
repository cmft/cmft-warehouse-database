﻿CREATE TABLE [dbo].[CONSULTANTS_1](
	[CWS Code] [nvarchar](255) NULL,
	[ExternalCons] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[Inits] [nvarchar](255) NULL,
	[ForenamePfx] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[GMCCode] [nvarchar](255) NULL,
	[PrimarySpecialty] [nvarchar](255) NULL,
	[ProviderCode] [nvarchar](255) NULL,
	[NationalCode] [nvarchar](max) NULL,
	[NationalSpecialty] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
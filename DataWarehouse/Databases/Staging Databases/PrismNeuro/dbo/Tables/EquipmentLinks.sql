﻿CREATE TABLE [dbo].[EquipmentLinks](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProdCode] [char](10) NULL,
	[Supplier] [varchar](50) NULL,
	[System] [varchar](50) NULL
) ON [PRIMARY]
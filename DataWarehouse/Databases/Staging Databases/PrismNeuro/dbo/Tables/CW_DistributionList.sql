﻿CREATE TABLE [dbo].[CW_DistributionList](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[Address3] [varchar](30) NULL,
	[Address4] [varchar](30) NULL
) ON [PRIMARY]
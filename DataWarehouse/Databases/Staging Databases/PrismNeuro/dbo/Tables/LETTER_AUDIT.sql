﻿CREATE TABLE [dbo].[LETTER_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AEV_NUMBER] [int] NOT NULL,
	[LETTER_TYPE] [varchar](50) NULL,
	[LDATE] [datetime] NULL,
	[USERID] [varchar](50) NULL,
	[TEMPLATE] [varchar](50) NULL,
	[POSTED] [varchar](50) NULL,
	[LetterLocation] [varchar](255) NULL,
	[APTDATE] [varchar](255) NULL,
	[UniqueID] [varchar](36) NOT NULL DEFAULT (newid())
) ON [PRIMARY]
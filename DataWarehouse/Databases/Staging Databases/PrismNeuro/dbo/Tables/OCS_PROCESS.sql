﻿CREATE TABLE [dbo].[OCS_PROCESS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Process] [varchar](1000) NOT NULL,
	[Instance] [varchar](10) NULL,
	[Status] [varchar](5) NULL CONSTRAINT [DF_OCS_PROCESS_Status]  DEFAULT ((0))
) ON [PRIMARY]
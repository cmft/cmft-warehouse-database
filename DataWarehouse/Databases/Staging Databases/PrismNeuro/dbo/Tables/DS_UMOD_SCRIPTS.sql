﻿CREATE TABLE [dbo].[DS_UMOD_SCRIPTS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[D_FUNCTION] [varchar](50) NOT NULL,
	[AUTOEXECSCRIPT] [varchar](50) NULL,
	[TERMINATESCRIPT] [varchar](50) NULL,
	[SAVESCRIPT] [varchar](50) NULL
) ON [PRIMARY]
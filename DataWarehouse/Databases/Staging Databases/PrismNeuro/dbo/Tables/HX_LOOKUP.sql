﻿CREATE TABLE [dbo].[HX_LOOKUP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](120) NULL,
	[Fieldname] [varchar](150) NULL,
	[Field_Description] [varchar](150) NULL,
	[Code] [varchar](150) NULL,
	[Text] [varchar](150) NULL,
	[Phrase] [nvarchar](1000) NULL,
	[Ext_Code] [nvarchar](50) NULL,
	[SortOrder] [int] NULL CONSTRAINT [DF_Hx_Lookup_SortOrder]  DEFAULT (0),
	[IsDefault] [bit] NULL CONSTRAINT [DF_Hx_Lookup_isdefault]  DEFAULT (0),
	[sys] [bit] NULL CONSTRAINT [DF_HX_LOOKUP_sys]  DEFAULT (1)
) ON [PRIMARY]
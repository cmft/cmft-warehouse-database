﻿CREATE TABLE [dbo].[CWS_TYPES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DOC_TYPE] [varchar](50) NULL,
	[ATTACHMENT] [int] NULL,
	[ATTACHMENT_ID] [int] NULL,
	[LOCATION] [varchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[mWardsclinics$](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CODE] [nvarchar](255) NULL,
	[TITLE] [nvarchar](255) NULL,
	[TYPE] [nvarchar](255) NULL,
	[SITE] [nvarchar](255) NULL,
	[F5] [nvarchar](255) NULL
) ON [PRIMARY]
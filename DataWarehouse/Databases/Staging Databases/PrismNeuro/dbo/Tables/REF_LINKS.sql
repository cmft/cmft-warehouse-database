﻿CREATE TABLE [dbo].[REF_LINKS](
	[RF_NUMBER] [int] NULL,
	[RG_CODE] [nvarchar](5) NULL,
	[RT_CODE] [nvarchar](5) NULL,
	[RL_PRIMARY] [bit] NULL,
	[RL_STATUS] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DOR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DOR] [char](10) NULL,
	[TITLE] [varchar](50) NULL
) ON [PRIMARY]
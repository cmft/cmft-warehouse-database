﻿CREATE TABLE [dbo].[Data_Forms](
	[FormID] [int] NOT NULL,
	[DataSetId] [char](10) NULL,
	[GroupId] [char](15) NULL,
	[FormName] [varchar](100) NULL,
	[FormInstructions] [ntext] NULL,
	[SubmitTo] [varchar](100) NULL,
	[DataTableName] [varchar](100) NULL,
	[RedirectType] [int] NULL,
	[RedirectMessage] [ntext] NULL,
	[SubmitName] [varchar](50) NULL,
	[SubmitValue] [varchar](50) NULL,
	[SubmitMethod] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
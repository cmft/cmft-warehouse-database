﻿CREATE TABLE [dbo].[DS_OPTIONS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TYPE_] [varchar](50) NULL,
	[DATASET] [varchar](50) NULL,
	[LEGEND] [varchar](50) NULL,
	[DETAIL] [varchar](50) NULL,
	[BUTTON_ID] [int] NULL
) ON [PRIMARY]
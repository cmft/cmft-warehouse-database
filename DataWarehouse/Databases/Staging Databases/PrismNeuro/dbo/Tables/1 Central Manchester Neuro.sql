﻿CREATE TABLE [dbo].[1 Central Manchester Neuro](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HDC] [varchar](100) NULL,
	[Version] [char](10) NULL,
	[VDate] [datetime] NULL,
	[Comment] [varchar](4000) NULL
) ON [PRIMARY]
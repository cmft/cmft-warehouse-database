﻿CREATE TABLE [dbo].[PAT_TABLES_LINK](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PTable] [nvarchar](50) NULL,
	[HX_number] [nvarchar](50) NULL,
	[Status] [bit] NULL
) ON [PRIMARY]
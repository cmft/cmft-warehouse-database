﻿CREATE TABLE [dbo].[REF_TEAMS](
	[RT_CODE] [nvarchar](5) NULL,
	[RT_TITLE] [nvarchar](30) NULL,
	[RT_EXT_CODE] [nvarchar](10) NULL,
	[RT_LOCAL_CODE] [nvarchar](10) NULL
) ON [PRIMARY]
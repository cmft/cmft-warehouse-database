﻿CREATE TABLE [dbo].[MProcedures$](
	[CODE ] [nvarchar](255) NULL,
	[TITLE] [nvarchar](255) NULL,
	[FUNCTION] [nvarchar](255) NULL,
	[SUB GROUP *] [nvarchar](255) NULL,
	[CWS CODE] [float] NULL,
	[F6] [nvarchar](255) NULL,
	[F7] [nvarchar](255) NULL,
	[F8] [nvarchar](255) NULL,
	[F9] [nvarchar](255) NULL,
	[F10] [nvarchar](255) NULL
) ON [PRIMARY]
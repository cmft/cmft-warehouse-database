﻿CREATE TABLE [dbo].[CLIN_REPORTS](
	[RP_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[EVENT_NUMBER] [int] NULL,
	[RP_BATCH_NO] [smallint] NULL,
	[RP_DATE] [datetime] NULL,
	[RP_AUTHOR] [varchar](75) NULL,
	[AUTHOR_GRADE] [varchar](75) NULL,
	[RP_TYPIST] [varchar](50) NULL,
	[RP_AUTHORISED] [bit] NULL,
	[RP_AUTHORISED_DATE] [datetime] NULL,
	[RP_AUTHORISED_BY] [varchar](50) NULL,
	[RP_REPORT] [text] NULL,
	[RP_ABNORMAL] [bit] NULL,
	[RP_LEFTM] [real] NULL,
	[RP_RIGHTM] [real] NULL,
	[RP_TOPM] [real] NULL,
	[RP_BOTTOMM] [real] NULL,
	[RP_HX_NUMBER] [int] NULL,
	[RP_ACTIVITY_NUMBER] [int] NULL,
	[RP_ISSUE_DATE] [datetime] NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[RP] [char](1) NULL CONSTRAINT [DF_CLIN_REPORTS_RP]  DEFAULT ('R'),
	[ATTACHMENT] [int] NULL CONSTRAINT [DF_CLIN_REPORTS_ATTACHMENT]  DEFAULT (0),
	[RP_TYPE] [int] NULL CONSTRAINT [DF_CLIN_REPORTS_RP_TYPE]  DEFAULT (0),
	[attachment_number] [int] NULL,
	[Location] [varchar](100) NULL,
	[Orig_Location] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[CONFIG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Element] [varchar](150) NULL,
	[Detail] [varchar](150) NULL,
	[Site] [varchar](10) NULL,
	[PrismFunction] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[FOLLOWUP](
	[F_ID] [int] NOT NULL,
	[F_TITLE] [nvarchar](50) NULL,
	[F_FINAL] [bit] NOT NULL,
	[deleted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DS_GRID_CONFIG](
	[ColumnID] [int] NOT NULL,
	[GridName] [nvarchar](20) NULL,
	[Table_] [nvarchar](50) NULL,
	[ColumnName] [nvarchar](50) NULL,
	[ColumnCriteria] [nvarchar](50) NULL,
	[OptionSQL] [varchar](4000) NULL
) ON [PRIMARY]
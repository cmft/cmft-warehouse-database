﻿CREATE TABLE [dbo].[ATTATCHMENT](
	[Attatchment_no] [int] IDENTITY(1,1) NOT NULL,
	[event_number] [int] NULL,
	[hilex_number] [int] NULL,
	[Attatchment_Description] [varchar](250) NULL,
	[Attatchment_location] [varchar](255) NULL,
	[Dicom] [bit] NULL,
	[ImageText] [varchar](4000) NULL,
	[CreateDate] [datetime] NULL,
	[AEVENT_NUMBER] [int] NULL,
	[ATT_TYPE] [int] NULL,
	[attachment_number] [int] NULL
) ON [PRIMARY]
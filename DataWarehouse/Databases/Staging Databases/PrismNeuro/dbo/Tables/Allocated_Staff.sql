﻿CREATE TABLE [dbo].[Allocated_Staff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AL_Diary] [int] NULL,
	[AL_Date] [datetime] NULL,
	[AL_Session] [smallint] NULL,
	[AL_Staff] [varchar](50) NULL
) ON [PRIMARY]
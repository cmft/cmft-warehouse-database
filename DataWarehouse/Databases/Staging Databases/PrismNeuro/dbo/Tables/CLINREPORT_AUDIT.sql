﻿CREATE TABLE [dbo].[CLINREPORT_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EVENT_NUMBER] [int] NOT NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[RPDATE] [datetime] NULL,
	[EVDATE] [datetime] NULL,
	[USERID] [varchar](50) NULL,
	[STATUS] [varchar](20) NULL,
	[PREV_RPNUM] [int] NULL,
	[ReportLocation] [varchar](255) NULL,
	[UniqueID] [varchar](36) NOT NULL,
	[AUTHORISED] [int] NOT NULL,
	[SEQUENCE] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[POSTCODE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PCODE] [nvarchar](10) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[ADDRESS3] [nvarchar](255) NULL,
	[ADDRESS4] [nvarchar](255) NULL,
	[DHA] [nvarchar](6) NULL
) ON [PRIMARY]
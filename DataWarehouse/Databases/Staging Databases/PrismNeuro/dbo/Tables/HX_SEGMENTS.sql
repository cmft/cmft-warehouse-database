﻿CREATE TABLE [dbo].[HX_SEGMENTS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DATASET] [varchar](50) NULL,
	[COLOUR] [varchar](50) NULL,
	[SEGMENT] [varchar](50) NULL,
	[PHRASE] [varchar](500) NULL
) ON [PRIMARY]
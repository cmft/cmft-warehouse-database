﻿CREATE TABLE [dbo].[PRICES](
	[price_band] [int] NOT NULL,
	[sr_code] [nvarchar](5) NULL,
	[Price_1] [money] NULL,
	[Price_2] [money] NULL,
	[Price_3] [money] NULL,
	[Price_4] [money] NULL,
	[Price_5] [money] NULL,
	[X] [char](10) NULL
) ON [PRIMARY]
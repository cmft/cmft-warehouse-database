﻿CREATE TABLE [dbo].[Phrases](
	[Code] [char](10) NULL,
	[Description] [varchar](50) NULL,
	[Phrase] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[ACTIVITIES](
	[AC_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[AC_CODE] [varchar](10) NULL,
	[AC_TITLE] [varchar](50) NULL,
	[AC_SUB_GROUP] [varchar](10) NULL,
	[AC_FUNCTION] [varchar](10) NULL,
	[AC_EXT_CODE_1] [varchar](50) NULL,
	[AC_EQPT_LINK] [varchar](50) NULL,
	[AC_SERVICE_MASTER] [bit] NULL,
	[AC_METHOD] [varchar](50) NULL,
	[ac_ext_code_2] [varchar](50) NULL,
	[AC_PATIENT] [varchar](50) NULL
) ON [PRIMARY]
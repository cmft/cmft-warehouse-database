﻿CREATE TABLE [dbo].[CATHETERS](
	[REF_NO] [int] NOT NULL,
	[MFG_NO] [int] NULL,
	[MODEL] [varchar](50) NULL,
	[COMMENT_] [varchar](255) NULL,
	[RECORD_ADDED] [datetime] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[COST] [money] NULL,
	[STOCK_LEVEL] [int] NULL,
	[MIN_LEVEL] [int] NULL,
	[RE_ORDER_NO] [int] NULL,
	[LEAD_TIME_DAYS] [int] NULL,
	[NORMAL_LEVEL] [int] NULL,
	[UNIT_QTY] [int] NULL
) ON [PRIMARY]
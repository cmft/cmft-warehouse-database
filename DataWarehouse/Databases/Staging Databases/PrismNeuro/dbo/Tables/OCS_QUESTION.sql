﻿CREATE TABLE [dbo].[OCS_QUESTION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OCS_NUMBER] [int] NULL,
	[HX_NUMBER] [int] NULL,
	[SEQUENCE] [int] NULL,
	[HEADER] [varchar](255) NULL,
	[QUESTION] [varchar](1000) NULL,
	[RESPONSE] [varchar](4000) NULL,
	[Type] [char](10) NULL CONSTRAINT [DF_OCS_QUESTION_Type]  DEFAULT ((0)),
	[UniqueID] [varchar](36) NULL,
	[ORDER_ID] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PROCEDURES](
	[P_ID] [int] IDENTITY(1,1) NOT NULL,
	[P_CODE] [nvarchar](10) NULL,
	[P_TITLE] [nvarchar](50) NULL
) ON [PRIMARY]
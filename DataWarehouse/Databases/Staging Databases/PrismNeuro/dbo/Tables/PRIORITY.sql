﻿CREATE TABLE [dbo].[PRIORITY](
	[ID] [int] NULL,
	[PRISM_PRIORITY] [varchar](50) NULL,
	[PRIORITY_DESCRIPTION] [varchar](100) NULL,
	[PRIORITY_HL7CODE] [varchar](20) NULL,
	[Instance] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MergeJournal](
	[MergeID] [int] IDENTITY(1,1) NOT NULL,
	[MasterHX] [int] NULL,
	[SlaveHX] [int] NULL,
	[MasterExt] [varchar](50) NULL,
	[SlaveExt] [varchar](50) NULL,
	[DateReq] [datetime] NULL CONSTRAINT [DF_MergeJournal_DateReq]  DEFAULT (getdate()),
	[DateProcessed] [datetime] NULL,
	[Processed] [int] NULL CONSTRAINT [DF__MergeJour__Proce__2BC97F7C]  DEFAULT ((0))
) ON [PRIMARY]
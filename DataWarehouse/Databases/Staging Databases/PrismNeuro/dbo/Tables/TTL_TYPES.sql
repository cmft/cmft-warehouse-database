﻿CREATE TABLE [dbo].[TTL_TYPES](
	[TTL_TYPE] [nvarchar](5) NULL,
	[TTL_TITLE] [nvarchar](10) NULL,
	[TTL_GENDER] [nvarchar](1) NULL
) ON [PRIMARY]
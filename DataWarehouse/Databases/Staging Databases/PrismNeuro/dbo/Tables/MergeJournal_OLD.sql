﻿CREATE TABLE [dbo].[MergeJournal_OLD](
	[MergeID] [int] IDENTITY(1,1) NOT NULL,
	[MasterHX] [int] NULL,
	[SlaveHX] [int] NULL,
	[DateReq] [datetime] NULL,
	[DateProcessed] [datetime] NULL
) ON [PRIMARY]
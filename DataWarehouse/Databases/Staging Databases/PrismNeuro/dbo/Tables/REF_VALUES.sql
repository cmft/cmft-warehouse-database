﻿CREATE TABLE [dbo].[REF_VALUES](
	[RV_ID] [int] IDENTITY(1,1) NOT NULL,
	[RV_GROUP] [nvarchar](20) NULL,
	[RV_DOMAIN] [nvarchar](50) NULL,
	[RV_CODE] [nvarchar](25) NULL,
	[RV_DESCRIPTION] [nvarchar](255) NULL,
	[RV_PHRASE] [nvarchar](255) NULL,
	[SITE] [nvarchar](10) NULL,
	[EXT_CODE] [nvarchar](255) NULL
) ON [PRIMARY]
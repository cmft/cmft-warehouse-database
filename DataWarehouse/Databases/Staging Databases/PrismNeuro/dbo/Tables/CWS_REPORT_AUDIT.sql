﻿CREATE TABLE [dbo].[CWS_REPORT_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RP_NUMBER] [int] NOT NULL,
	[DESCRIPTION] [varchar](50) NULL,
	[DATE] [datetime] NULL,
	[USERID] [varchar](50) NULL,
	[STATUS] [varchar](20) NULL,
	[ReportLocation] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EVENT_CODING](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EV_NUMBER] [int] NULL,
	[CODE] [varchar](50) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[C_ORDER] [int] NULL,
	[CODESET] [varchar](50) NULL
) ON [PRIMARY]
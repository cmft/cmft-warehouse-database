﻿CREATE TABLE [dbo].[DS_MASTERLIST](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataSet] [nvarchar](10) NULL,
	[DataSetName] [nvarchar](50) NULL,
	[DataSetDesription] [nvarchar](50) NULL,
	[DataSetTable] [nvarchar](255) NULL,
	[DisplayCol1] [nvarchar](50) NULL,
	[DisplayCol1Wd] [smallint] NULL,
	[DisplayCol2] [nvarchar](50) NULL,
	[DisplayCol2Wd] [smallint] NULL,
	[DisplayCol3] [nvarchar](50) NULL,
	[DisplayCol3Wd] [smallint] NULL,
	[DisplayCol4] [nvarchar](50) NULL,
	[DisplayCol4Wd] [smallint] NULL,
	[EXTRACT_] [nvarchar](50) NULL,
	[dlibrary] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AlertCodes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Class] [varchar](50) NULL,
	[ClassCode] [varchar](50) NULL,
	[Description] [varchar](50) NULL,
	[Severity] [int] NULL
) ON [PRIMARY]
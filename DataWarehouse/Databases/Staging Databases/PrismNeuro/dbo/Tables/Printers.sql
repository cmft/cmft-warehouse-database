﻿CREATE TABLE [dbo].[Printers](
	[Printer] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL
) ON [PRIMARY]
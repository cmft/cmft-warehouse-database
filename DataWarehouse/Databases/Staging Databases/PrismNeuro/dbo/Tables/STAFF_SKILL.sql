﻿CREATE TABLE [dbo].[STAFF_SKILL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[STAFF_NUMBER] [int] NULL,
	[SKILL] [int] NULL,
	[SITE] [varchar](50) NULL
) ON [PRIMARY]
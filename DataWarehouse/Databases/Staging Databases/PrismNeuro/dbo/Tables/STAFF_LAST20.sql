﻿CREATE TABLE [dbo].[STAFF_LAST20](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Staff_ID] [int] NULL,
	[Last_idx] [int] NULL,
	[Last_Pat] [int] NULL,
	[Last_Date] [datetime] NULL
) ON [PRIMARY]
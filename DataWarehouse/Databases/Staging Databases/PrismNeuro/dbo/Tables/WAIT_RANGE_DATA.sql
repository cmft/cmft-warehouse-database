﻿CREATE TABLE [dbo].[WAIT_RANGE_DATA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AC_NUMBER] [int] NULL,
	[AC_CODE] [varchar](50) NULL,
	[AC_TITLE] [varchar](50) NULL,
	[AC_COUNT] [int] NULL,
	[AC_RANGE] [int] NULL,
	[RANGE_TEXT] [varchar](50) NULL,
	[SITE] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MARITAL_STATUS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](50) NULL,
	[morder] [smallint] NULL,
	[MS_EXT] [varchar](50) NULL
) ON [PRIMARY]
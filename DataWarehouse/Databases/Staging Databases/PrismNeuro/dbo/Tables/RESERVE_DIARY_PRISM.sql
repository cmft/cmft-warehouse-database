﻿CREATE TABLE [dbo].[RESERVE_DIARY_PRISM](
	[RD_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[DR_NUMBER] [int] NULL,
	[RD_DATE] [datetime] NULL,
	[RD_START] [nvarchar](5) NULL,
	[RD_END] [nvarchar](5) NULL,
	[RT_NUMBER] [int] NULL,
	[RD_COMMENT] [nvarchar](120) NULL,
	[RD_INHIBIT] [int] NULL,
	[RD_PARENT] [int] NULL,
	[RD_DATEWRITTEN] [datetime] NULL,
	[RD_MACHINE] [nvarchar](50) NULL,
	[RD_GROUP] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NUMBER_TYPES](
	[NUM_ID] [int] IDENTITY(1,1) NOT NULL,
	[NUM_TYPE] [varchar](10) NULL,
	[NUM_DESC] [varchar](50) NULL,
	[NUM_LENGTH] [smallint] NULL,
	[NUM_FORMAT] [varchar](50) NULL
) ON [PRIMARY]
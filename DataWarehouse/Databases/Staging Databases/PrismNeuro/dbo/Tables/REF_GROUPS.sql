﻿CREATE TABLE [dbo].[REF_GROUPS](
	[RG_CODE] [nvarchar](5) NULL,
	[RG_TITLE] [nvarchar](50) NULL,
	[RG_EXT_CODE] [nvarchar](10) NULL,
	[RG_LOCAL_CODE] [nvarchar](10) NULL,
	[STYPE] [varchar](50) NULL,
	[SGROUP] [varchar](50) NULL
) ON [PRIMARY]
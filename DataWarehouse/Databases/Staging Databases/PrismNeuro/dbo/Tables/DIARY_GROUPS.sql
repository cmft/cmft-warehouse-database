﻿CREATE TABLE [dbo].[DIARY_GROUPS](
	[DR_NUMBER] [int] NULL,
	[DG_DOW] [smallint] NULL,
	[DG_SESSION] [smallint] NULL,
	[DG_GROUP] [smallint] NULL,
	[DG_VOLUME] [smallint] NULL
) ON [PRIMARY]
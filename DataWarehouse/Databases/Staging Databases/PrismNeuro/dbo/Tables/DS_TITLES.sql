﻿CREATE TABLE [dbo].[DS_TITLES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Dataset] [varchar](50) NULL,
	[Title] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DS_GRID](
	[ColumnID] [int] NOT NULL,
	[GridName] [nvarchar](20) NULL,
	[ColumnTable] [nvarchar](50) NULL,
	[ColumnName] [nvarchar](50) NULL,
	[ColumnCaption] [nvarchar](50) NULL,
	[ColumnSequence] [smallint] NULL,
	[ColumnLookup] [nvarchar](50) NULL,
	[ColumnWidth] [smallint] NULL,
	[ColumnVisible] [bit] NOT NULL,
	[ColumnLevel] [smallint] NULL,
	[DataType] [nvarchar](50) NULL,
	[ColumnMask] [nvarchar](10) NULL
) ON [PRIMARY]
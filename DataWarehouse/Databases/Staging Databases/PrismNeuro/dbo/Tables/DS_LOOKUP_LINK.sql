﻿CREATE TABLE [dbo].[DS_LOOKUP_LINK](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LookupName] [nvarchar](255) NULL,
	[Dataset] [nvarchar](20) NULL,
	[Dataset_Title] [nvarchar](60) NULL
) ON [PRIMARY]
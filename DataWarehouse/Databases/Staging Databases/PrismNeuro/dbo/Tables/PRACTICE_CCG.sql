﻿CREATE TABLE [dbo].[PRACTICE_CCG](
	[CCG_NUMBER] [int] IDENTITY(1,1) NOT NULL,
	[CCG_CODE] [varchar](50) NULL,
	[CCG_NAME] [varchar](500) NULL,
	[CCG_ADDRESS1] [varchar](500) NULL,
	[CCG_ADDRESS2] [varchar](500) NULL,
	[CCG_ADDRESS3] [varchar](500) NULL,
	[CCG_ADDRESS4] [varchar](500) NULL,
	[CCG_ADDRESS5] [varchar](500) NULL,
	[CCG_POSTCODE] [varchar](50) NULL,
	[CCG_TEL] [varchar](50) NULL,
	[CCG_FAX] [varchar](50) NULL,
	[CCG_EMAIL] [varchar](50) NULL,
	[CCG_EXT_CODE] [varchar](50) NULL,
	[CCG_LOCAL] [bit] NULL,
	[CCG_STATUS] [bit] NULL,
	[CCG_START_DATE] [datetime] NULL
) ON [PRIMARY]
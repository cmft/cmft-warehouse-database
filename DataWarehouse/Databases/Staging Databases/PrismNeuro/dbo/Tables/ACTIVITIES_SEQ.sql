﻿CREATE TABLE [dbo].[ACTIVITIES_SEQ](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SiteCode] [varchar](50) NULL,
	[ProcedureID] [int] NULL,
	[SeqNo] [int] NULL,
	[Format] [varchar](50) NULL
) ON [PRIMARY]
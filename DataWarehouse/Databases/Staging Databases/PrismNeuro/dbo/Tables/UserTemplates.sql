﻿CREATE TABLE [dbo].[UserTemplates](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserNum] [int] NULL,
	[Template] [varchar](50) NULL
) ON [PRIMARY]
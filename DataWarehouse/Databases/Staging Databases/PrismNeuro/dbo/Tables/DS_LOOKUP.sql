﻿CREATE TABLE [dbo].[DS_LOOKUP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LookupName] [nvarchar](40) NULL,
	[LookupDetail] [nvarchar](510) NULL,
	[LookupCode] [nvarchar](100) NULL,
	[FullName] [nvarchar](100) NULL,
	[LookupPhrase] [nvarchar](510) NULL,
	[ORDER_] [smallint] NULL,
	[LookupHelp] [varchar](4000) NULL,
	[LookupHelpColour] [varchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[HX_LOOKUP_U](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](120) NULL,
	[Fieldname] [varchar](150) NULL,
	[Field_Description] [varchar](150) NULL,
	[Code] [varchar](150) NULL,
	[Text] [varchar](150) NULL,
	[Phrase] [nvarchar](1000) NULL,
	[Ext_Code] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[isDefault] [bit] NULL,
	[Site] [varchar](50) NULL,
	[Fn] [varchar](50) NULL,
	[Active] [bit] NULL,
	[sys] [bit] NULL CONSTRAINT [DF_HX_LOOKUP_U_sys]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ICD10](
	[ICD10] [varchar](50) NULL,
	[ICD_Text] [varchar](255) NULL,
	[PBC02] [varchar](50) NULL,
	[PBcode] [varchar](50) NULL,
	[PBCat] [varchar](255) NULL
) ON [PRIMARY]
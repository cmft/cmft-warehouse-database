﻿CREATE TABLE [dbo].[0_VERSION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VER] [varchar](10) NULL,
	[DATE_UPDATED] [datetime] NULL,
	[Description] [varchar](50) NULL
) ON [PRIMARY]
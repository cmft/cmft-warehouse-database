﻿CREATE TABLE [dbo].[Contracts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Contract] [varchar](50) NULL,
	[Contact] [varchar](50) NULL,
	[Ad1] [varchar](50) NULL,
	[Ad2] [varchar](50) NULL,
	[Ad3] [varchar](50) NULL,
	[Ad4] [varchar](50) NULL,
	[Pcode] [varchar](10) NULL,
	[TelNo] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Active] [int] NULL
) ON [PRIMARY]
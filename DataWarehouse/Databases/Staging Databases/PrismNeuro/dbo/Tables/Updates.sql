﻿CREATE TABLE [dbo].[Updates](
	[UpdateNumber] [int] NOT NULL,
	[MachineName] [nvarchar](10) NULL,
	[UpdateName] [nvarchar](50) NULL
) ON [PRIMARY]
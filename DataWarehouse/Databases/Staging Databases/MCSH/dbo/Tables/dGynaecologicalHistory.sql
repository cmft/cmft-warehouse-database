﻿CREATE TABLE [dbo].[dGynaecologicalHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[HistoryDate] [smalldatetime] NULL,
	[GynaecologicalTypeID] [bigint] NULL,
	[Details] [varchar](1024) NULL,
	[ReviewDate] [smalldatetime] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [char](10) NULL
) ON [PRIMARY]
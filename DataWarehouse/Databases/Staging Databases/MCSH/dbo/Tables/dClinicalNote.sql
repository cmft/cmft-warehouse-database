﻿CREATE TABLE [dbo].[dClinicalNote](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[ClinicalNoteTypeID] [bigint] NULL,
	[Comments] [varchar](1024) NULL,
	[ClinicalNoteOptionID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sDispensaries](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Enabled] [bit] NULL,
	[LocationID] [bigint] NULL
) ON [PRIMARY]
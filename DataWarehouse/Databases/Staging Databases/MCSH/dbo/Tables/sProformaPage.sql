﻿CREATE TABLE [dbo].[sProformaPage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProformaID] [bigint] NULL,
	[Description] [varchar](1024) NOT NULL,
	[Position] [bigint] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
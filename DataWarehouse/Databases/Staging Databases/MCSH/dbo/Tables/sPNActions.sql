﻿CREATE TABLE [dbo].[sPNActions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [int] NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
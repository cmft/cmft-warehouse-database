﻿CREATE TABLE [dbo].[sDrugInteraction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugAID] [bigint] NOT NULL,
	[DrugBID] [bigint] NOT NULL,
	[Comment] [varchar](1024) NULL
) ON [PRIMARY]
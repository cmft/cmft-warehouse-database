﻿CREATE TABLE [dbo].[sTestGroupToUser](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TestGroupID] [bigint] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[AbnormalOnly] [bit] NULL,
	[Enabled] [bit] NULL,
	[CanOrder] [bit] NULL
) ON [PRIMARY]
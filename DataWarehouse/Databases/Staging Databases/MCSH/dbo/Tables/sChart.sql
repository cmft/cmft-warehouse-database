﻿CREATE TABLE [dbo].[sChart](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](80) NULL,
	[Enabled] [bit] NOT NULL,
	[ChartTypeID] [bigint] NOT NULL,
	[XMin] [float] NULL,
	[XMax] [float] NULL,
	[YMin] [float] NULL,
	[YMax] [float] NULL,
	[Y2Min] [float] NULL,
	[Y2Max] [float] NULL,
	[Legend] [bit] NOT NULL,
	[Title] [varchar](80) NULL,
	[CrossHair] [bit] NULL
) ON [PRIMARY]
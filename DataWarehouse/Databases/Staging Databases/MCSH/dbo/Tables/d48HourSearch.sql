﻿CREATE TABLE [dbo].[d48HourSearch](
	[TempID] [bigint] IDENTITY(1,1) NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[OutOf48HourReasonID] [int] NULL,
	[AppFound] [bit] NULL,
	[WorkingDaysToApt] [smallint] NULL,
	[EarliestDateOffered] [datetime] NULL,
	[PatientWaitID] [bigint] NULL,
	[Symptomatic] [bit] NULL,
	[PreferredClinic] [bit] NULL,
	[WorkingDaysToOffer] [bigint] NULL,
	[UnScheduled] [bit] NULL
) ON [PRIMARY]
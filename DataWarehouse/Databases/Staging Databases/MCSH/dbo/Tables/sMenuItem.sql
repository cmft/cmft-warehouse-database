﻿CREATE TABLE [dbo].[sMenuItem](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MenuGroupID] [bigint] NULL,
	[Description] [varchar](1024) NOT NULL,
	[href] [varchar](1024) NULL,
	[ProfileObjectID] [bigint] NULL,
	[Position] [float] NULL,
	[EffectiveFrom] [smalldatetime] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
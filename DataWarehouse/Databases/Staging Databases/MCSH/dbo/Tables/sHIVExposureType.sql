﻿CREATE TABLE [dbo].[sHIVExposureType](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[SophidCode] [varchar](1024) NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
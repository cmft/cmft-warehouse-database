﻿CREATE TABLE [dbo].[sPrescriptionDrugDays](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Duration] [int] NOT NULL,
	[Position] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
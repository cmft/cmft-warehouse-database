﻿CREATE TABLE [dbo].[dLineDrawingAnnotation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ToolBoxItemID] [bigint] NULL,
	[LineDrawingID] [bigint] NULL,
	[XOffSet] [float] NULL,
	[YOffSet] [float] NULL,
	[CollectionDateTime] [datetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
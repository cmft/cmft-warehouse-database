﻿CREATE TABLE [dbo].[sUserSecurity](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NOT NULL,
	[SecurityFlag1] [varchar](8) NULL,
	[SecurityFlag2] [varchar](16) NULL
) ON [PRIMARY]
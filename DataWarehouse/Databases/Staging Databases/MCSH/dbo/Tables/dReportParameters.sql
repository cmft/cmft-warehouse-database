﻿CREATE TABLE [dbo].[dReportParameters](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ReportID] [bigint] NULL,
	[FieldName] [varchar](50) NULL,
	[Prompt] [varchar](50) NULL,
	[FieldTypeID] [bigint] NULL,
	[LookupTable] [varchar](50) NULL,
	[MinimumValue] [float] NULL,
	[MaximumValue] [float] NULL
) ON [PRIMARY]
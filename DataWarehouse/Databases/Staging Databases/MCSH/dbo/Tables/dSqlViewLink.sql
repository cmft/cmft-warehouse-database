﻿CREATE TABLE [dbo].[dSqlViewLink](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[SqlStatementID] [bigint] NOT NULL,
	[ViewDefID] [bigint] NOT NULL,
	[UserID] [bigint] NULL,
	[ResourceID] [bigint] NULL CONSTRAINT [DF_dSqlViewLink_ResourceID]  DEFAULT (2)
) ON [PRIMARY]
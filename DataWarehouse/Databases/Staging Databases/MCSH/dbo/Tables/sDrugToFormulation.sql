﻿CREATE TABLE [dbo].[sDrugToFormulation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugID] [bigint] NOT NULL,
	[FormulationID] [bigint] NOT NULL
) ON [PRIMARY]
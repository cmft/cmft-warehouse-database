﻿CREATE TABLE [dbo].[dComplications](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[ComplicationID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[ComplicationDate] [smalldatetime] NOT NULL,
	[Notes] [text] NULL,
	[TypeID] [bigint] NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
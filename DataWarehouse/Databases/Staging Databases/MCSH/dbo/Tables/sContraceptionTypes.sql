﻿CREATE TABLE [dbo].[sContraceptionTypes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[RequiresFitting] [bit] NULL,
	[SexTypeID] [bigint] NULL,
	[Code] [int] NULL
) ON [PRIMARY]
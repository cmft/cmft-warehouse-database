﻿CREATE TABLE [dbo].[sClinicSiteLocation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClinicTemplateID] [bigint] NULL,
	[SiteLocationID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
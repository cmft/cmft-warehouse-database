﻿CREATE TABLE [dbo].[sKT31Clinics](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClinicTemplateVersionID] [bigint] NULL,
	[Domiciliary] [bit] NULL,
	[Under25s] [bit] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
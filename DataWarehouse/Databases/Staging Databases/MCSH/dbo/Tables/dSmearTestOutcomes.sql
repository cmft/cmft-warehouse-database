﻿CREATE TABLE [dbo].[dSmearTestOutcomes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SmearTestID] [bigint] NOT NULL,
	[OutcomeSmearTestID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
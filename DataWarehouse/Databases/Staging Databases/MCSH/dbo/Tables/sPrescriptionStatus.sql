﻿CREATE TABLE [dbo].[sPrescriptionStatus](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Position] [int] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
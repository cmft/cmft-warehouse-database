﻿CREATE TABLE [dbo].[dPathTest](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[PathProfileID] [bigint] NULL,
	[TestID] [bigint] NULL,
	[Value] [varchar](1024) NULL,
	[Comment] [text] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
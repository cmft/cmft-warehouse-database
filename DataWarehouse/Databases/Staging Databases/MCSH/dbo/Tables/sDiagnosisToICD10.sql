﻿CREATE TABLE [dbo].[sDiagnosisToICD10](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DiagnosisID] [bigint] NULL,
	[ICD10ID] [bigint] NULL
) ON [PRIMARY]
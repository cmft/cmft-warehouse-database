﻿CREATE TABLE [dbo].[sSiteLocationSettings](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[SettingValue] [varchar](50) NULL,
	[SiteLocationID] [bigint] NOT NULL,
	[HelpText] [varchar](1024) NULL,
	[DataType] [varchar](50) NULL,
	[AssociatedTable] [varchar](50) NULL,
	[InternalOnly] [bit] NULL
) ON [PRIMARY]
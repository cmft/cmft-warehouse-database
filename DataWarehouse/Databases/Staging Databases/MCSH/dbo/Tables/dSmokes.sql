﻿CREATE TABLE [dbo].[dSmokes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[IntakeID] [bigint] NOT NULL,
	[AssessmentDate] [smalldatetime] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sChlamydiaReasonForTest](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[Code] [varchar](6) NULL,
	[Position] [float] NULL
) ON [PRIMARY]
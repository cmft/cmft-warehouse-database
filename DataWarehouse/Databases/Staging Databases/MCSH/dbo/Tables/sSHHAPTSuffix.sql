﻿CREATE TABLE [dbo].[sSHHAPTSuffix](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Suffix] [varchar](10) NULL,
	[Description] [varchar](255) NULL,
	[LevelFlag] [int] NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
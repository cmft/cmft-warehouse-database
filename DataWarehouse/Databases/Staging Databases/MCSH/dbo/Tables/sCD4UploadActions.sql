﻿CREATE TABLE [dbo].[sCD4UploadActions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[CompletedDateTime] [smalldatetime] NULL
) ON [PRIMARY]
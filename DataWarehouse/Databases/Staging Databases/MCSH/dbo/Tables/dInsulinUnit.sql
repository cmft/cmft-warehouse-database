﻿CREATE TABLE [dbo].[dInsulinUnit](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[InsulinRegimeID] [bigint] NULL,
	[TimeToBeTaken] [int] NULL,
	[Units] [float] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
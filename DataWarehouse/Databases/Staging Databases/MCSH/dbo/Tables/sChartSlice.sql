﻿CREATE TABLE [dbo].[sChartSlice](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](80) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[Thickness] [smallint] NOT NULL,
	[ChartID] [bigint] NOT NULL,
	[SliceItem] [varchar](10) NOT NULL
) ON [PRIMARY]
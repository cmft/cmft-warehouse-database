﻿CREATE TABLE [dbo].[sLineDrawingToolboxItem](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Caption] [varchar](50) NULL,
	[ToolboxID] [bigint] NULL,
	[Position] [float] NULL,
	[Image] [image] NULL,
	[ContentType] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
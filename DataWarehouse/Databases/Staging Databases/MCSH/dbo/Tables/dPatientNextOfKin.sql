﻿CREATE TABLE [dbo].[dPatientNextOfKin](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[Name] [varchar](1024) NULL,
	[ContactNumber] [varchar](50) NULL,
	[Address] [varchar](1024) NULL,
	[PostCode] [varchar](10) NULL,
	[Relationship] [bigint] NULL,
	[AwareOfHIVStatusID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dPatientAddressHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[Address] [varchar](1024) NULL,
	[PostCode] [varchar](10) NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[DisabledReason] [varchar](1024) NULL,
	[Processed] [bit] NULL,
	[SubBuilding] [varchar](255) NULL,
	[BuildingName] [varchar](255) NULL,
	[Address1] [varchar](255) NULL,
	[Address2] [varchar](255) NULL,
	[Address3] [varchar](255) NULL,
	[Address4] [varchar](255) NULL,
	[PostTown] [varchar](255) NULL,
	[County] [varchar](255) NULL,
	[HouseNumber] [varchar](255) NULL,
	[AddressType] [int] NULL,
	[WardName] [varchar](255) NULL,
	[WardCode] [varchar](50) NULL,
	[PrimaryCareName] [varchar](255) NULL,
	[PrimaryCareCode] [varchar](50) NULL,
	[NHSAuthorityName] [varchar](255) NULL,
	[NHSAuthorityCode] [varchar](50) NULL,
	[Overseasvisitor] [bit] NULL
) ON [PRIMARY]
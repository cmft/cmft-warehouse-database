﻿CREATE TABLE [dbo].[dTargetWeight](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[TargetWeight] [varchar](50) NOT NULL,
	[TargetBMI] [varchar](50) NULL,
	[TargetDate] [smalldatetime] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
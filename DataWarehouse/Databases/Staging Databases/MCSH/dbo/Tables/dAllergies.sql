﻿CREATE TABLE [dbo].[dAllergies](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[AllergyID] [bigint] NOT NULL,
	[ReactionID] [bigint] NULL,
	[SeverityID] [bigint] NULL,
	[Comments] [varchar](1024) NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sBookmarks](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[BookmarkGroupID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
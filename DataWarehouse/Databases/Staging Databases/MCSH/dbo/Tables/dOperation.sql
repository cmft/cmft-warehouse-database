﻿CREATE TABLE [dbo].[dOperation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[OPCSID] [bigint] NOT NULL,
	[OperationDate] [smalldatetime] NOT NULL,
	[Comments] [varchar](1024) NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
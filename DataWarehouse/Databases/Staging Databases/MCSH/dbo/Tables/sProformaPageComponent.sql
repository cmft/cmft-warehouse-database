﻿CREATE TABLE [dbo].[sProformaPageComponent](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[ProformaComponentTypeID] [bigint] NULL,
	[ProfileObjectID] [bigint] NULL,
	[Position] [float] NULL,
	[ProformaPageID] [bigint] NULL,
	[Caption] [varchar](1024) NULL,
	[Enabled] [bit] NULL,
	[DisplayPartial] [bit] NULL
) ON [PRIMARY]
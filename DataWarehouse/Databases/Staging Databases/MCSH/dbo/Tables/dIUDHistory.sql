﻿CREATE TABLE [dbo].[dIUDHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[IUDTypeID] [bigint] NULL,
	[FitDate] [smalldatetime] NULL,
	[ResourceID] [bigint] NULL,
	[ChangeDueDate] [smalldatetime] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
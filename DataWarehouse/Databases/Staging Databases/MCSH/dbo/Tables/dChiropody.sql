﻿CREATE TABLE [dbo].[dChiropody](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DateOfAttendance] [smalldatetime] NOT NULL,
	[ReasonForAttendanceID] [bigint] NULL,
	[OutcomeID] [bigint] NULL,
	[TreatmentID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
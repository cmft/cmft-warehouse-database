﻿CREATE TABLE [dbo].[dAntenatalAttendingProfessionals](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AntenatalID] [bigint] NOT NULL,
	[ProfessionalID] [bigint] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
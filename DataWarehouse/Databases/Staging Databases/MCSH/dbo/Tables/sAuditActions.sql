﻿CREATE TABLE [dbo].[sAuditActions](
	[ID] [bigint] NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Display] [varchar](1024) NULL,
	[AuditLevelID] [bigint] NULL,
	[AuditDetail] [bit] NULL,
	[AuditSQL] [varchar](50) NULL,
	[IdentityColumn] [varchar](50) NULL,
	[AffectedTableName] [varchar](50) NULL
) ON [PRIMARY]
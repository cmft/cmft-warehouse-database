﻿CREATE TABLE [dbo].[sHAConsultationType](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[ConsultationGroupID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[FieldType] [int] NULL,
	[LookupTable] [int] NULL
) ON [PRIMARY]
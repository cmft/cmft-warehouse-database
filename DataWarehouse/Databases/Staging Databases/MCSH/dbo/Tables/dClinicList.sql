﻿CREATE TABLE [dbo].[dClinicList](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClinicID] [bigint] NULL,
	[ResourceID] [bigint] NULL,
	[AllowOverbooking] [bit] NULL,
	[SIA] [bit] NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Comments] [varchar](1024) NULL,
	[LocationTypeID] [int] NULL
) ON [PRIMARY]
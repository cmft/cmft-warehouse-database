﻿CREATE TABLE [dbo].[sTests](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NULL,
	[FullDescription] [varchar](100) NULL,
	[ReadCode] [varchar](50) NULL,
	[UnitsOfMeasurementID] [bigint] NULL,
	[RefRangeLow] [float] NULL,
	[RefRangeHigh] [float] NULL,
	[Enabled] [bit] NULL,
	[LocalCode] [varchar](50) NULL,
	[FreeTextResult] [bit] NULL
) ON [PRIMARY]
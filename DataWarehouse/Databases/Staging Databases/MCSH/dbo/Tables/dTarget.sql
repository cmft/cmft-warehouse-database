﻿CREATE TABLE [dbo].[dTarget](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[TargetID] [bigint] NOT NULL,
	[TargetValue] [varchar](50) NOT NULL,
	[DateSet] [smalldatetime] NOT NULL,
	[Comments] [varchar](1024) NULL,
	[UserID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
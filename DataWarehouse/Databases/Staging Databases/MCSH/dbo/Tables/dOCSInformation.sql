﻿CREATE TABLE [dbo].[dOCSInformation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderNumber] [varchar](50) NULL,
	[OrderDateTime] [smalldatetime] NULL,
	[Ready] [int] NULL,
	[EpisodeID] [bigint] NULL,
	[SiteLocationID] [bigint] NULL,
	[Edit] [bit] NULL,
	[AlertUser] [bit] NULL,
	[AlertConsultantID] [bigint] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
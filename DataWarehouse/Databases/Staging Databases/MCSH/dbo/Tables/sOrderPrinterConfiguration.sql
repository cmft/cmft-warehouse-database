﻿CREATE TABLE [dbo].[sOrderPrinterConfiguration](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Host] [varchar](255) NULL,
	[LabelPrinter] [varchar](255) NULL,
	[OrderRequestPrinter] [varchar](255) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dUserResultAlert](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NOT NULL,
	[RRTestInformationID] [bigint] NOT NULL,
	[Status] [bigint] NULL
) ON [PRIMARY]
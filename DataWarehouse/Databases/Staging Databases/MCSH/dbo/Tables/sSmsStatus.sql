﻿CREATE TABLE [dbo].[sSmsStatus](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Status] [bigint] NULL,
	[Description] [varchar](255) NOT NULL
) ON [PRIMARY]
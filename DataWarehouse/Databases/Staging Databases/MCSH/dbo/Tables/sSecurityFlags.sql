﻿CREATE TABLE [dbo].[sSecurityFlags](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[ConstantName] [varchar](50) NULL,
	[ConstantValue] [int] NULL,
	[SecurityTypeID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
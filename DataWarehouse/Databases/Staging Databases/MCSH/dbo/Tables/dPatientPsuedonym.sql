﻿CREATE TABLE [dbo].[dPatientPsuedonym](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[FirstName] [varchar](1024) NULL,
	[LastName] [varchar](1024) NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
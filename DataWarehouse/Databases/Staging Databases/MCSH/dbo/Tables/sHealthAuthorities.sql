﻿CREATE TABLE [dbo].[sHealthAuthorities](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Enabled] [bit] NULL,
	[Description] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sKT31Line](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PartID] [bigint] NULL,
	[Line] [smallint] NULL,
	[Description] [char](80) NULL,
	[Mappable] [bit] NOT NULL,
	[ForOPCS] [bit] NOT NULL
) ON [PRIMARY]
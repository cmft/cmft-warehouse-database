﻿CREATE TABLE [dbo].[dPatientHIVStage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[HIVStageID] [bigint] NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[CollectionDateTime] [datetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[DisabledReason] [varchar](1024) NULL
) ON [PRIMARY]
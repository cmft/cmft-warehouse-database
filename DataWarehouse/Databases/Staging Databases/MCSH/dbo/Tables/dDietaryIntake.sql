﻿CREATE TABLE [dbo].[dDietaryIntake](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DietAssessmentID] [bigint] NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DietaryIntakeID] [bigint] NOT NULL,
	[DietaryIntakeLevelsID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
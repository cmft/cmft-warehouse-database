﻿CREATE TABLE [dbo].[dPatientPartners](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[PartnerPatientID] [bigint] NOT NULL,
	[StartDate] [bigint] NOT NULL,
	[EndDate] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NULL,
	[ParentID] [bigint] NULL
) ON [PRIMARY]
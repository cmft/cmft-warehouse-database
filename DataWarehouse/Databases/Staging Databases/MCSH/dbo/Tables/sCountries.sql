﻿CREATE TABLE [dbo].[sCountries](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Enabled] [bit] NULL,
	[CountryCode] [varchar](3) NULL
) ON [PRIMARY]
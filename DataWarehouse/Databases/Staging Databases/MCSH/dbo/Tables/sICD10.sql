﻿CREATE TABLE [dbo].[sICD10](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Code] [varchar](5) NOT NULL,
	[ClinicalSignificance] [bit] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dReportDefResourceLink](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[vDefID] [bigint] NOT NULL,
	[ResourceId] [bigint] NULL
) ON [PRIMARY]
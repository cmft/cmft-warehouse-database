﻿CREATE TABLE [dbo].[dProformaDraft](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[UserID] [bigint] NULL,
	[SessionID] [varchar](50) NULL,
	[ProformaID] [bigint] NULL,
	[ProformaHTML] [text] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
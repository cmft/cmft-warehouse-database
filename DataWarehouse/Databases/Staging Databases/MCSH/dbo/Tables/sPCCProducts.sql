﻿CREATE TABLE [dbo].[sPCCProducts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PCCID] [bigint] NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dAssociatedObjects](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ParentObjectID] [bigint] NULL,
	[ChildObjectID] [bigint] NULL,
	[ProfileObjectAssociationID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sRRWorkGroup](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[WorkGroup] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[Description] [varchar](50) NULL
) ON [PRIMARY]
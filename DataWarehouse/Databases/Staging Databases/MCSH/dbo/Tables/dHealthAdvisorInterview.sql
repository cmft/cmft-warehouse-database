﻿CREATE TABLE [dbo].[dHealthAdvisorInterview](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[HealthAdvisorID] [bigint] NOT NULL,
	[ReasonForInterviewID] [bigint] NOT NULL,
	[VisitDate] [smalldatetime] NOT NULL,
	[StartTime] [bigint] NOT NULL,
	[OutcomeID] [bigint] NULL,
	[ConsultationDurationID] [bigint] NULL,
	[EndTime] [bigint] NOT NULL,
	[Notes] [text] NULL,
	[CollectionDateTime] [smalldatetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[sInvestigationPanelType](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dOCRRErrorLog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ErrorType] [varchar](10) NULL,
	[ErrorDescription] [varchar](1024) NULL,
	[OrderNumber] [varchar](50) NULL,
	[Message] [image] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[PatientDetails] [varchar](200) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[dPresentations](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[PresentationID] [bigint] NOT NULL,
	[PresentationDate] [smalldatetime] NOT NULL,
	[Notes] [text] NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
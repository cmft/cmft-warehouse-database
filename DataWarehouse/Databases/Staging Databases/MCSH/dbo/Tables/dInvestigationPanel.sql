﻿CREATE TABLE [dbo].[dInvestigationPanel](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PanelTemplateID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[InvestigationRequestID] [bigint] NULL
) ON [PRIMARY]
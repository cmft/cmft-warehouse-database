﻿CREATE TABLE [dbo].[sOutputSelection](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OutputGroupID] [bigint] NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[SQLStatement] [varchar](1024) NULL,
	[Multiple] [bit] NULL
) ON [PRIMARY]
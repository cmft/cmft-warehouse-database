﻿CREATE TABLE [dbo].[sPrescription](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugID] [bigint] NULL,
	[DosageID] [bigint] NULL,
	[RouteID] [bigint] NULL,
	[FormulationID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
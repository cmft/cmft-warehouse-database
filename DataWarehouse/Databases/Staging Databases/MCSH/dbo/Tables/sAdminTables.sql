﻿CREATE TABLE [dbo].[sAdminTables](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [char](128) NOT NULL,
	[TableDescription] [char](50) NOT NULL
) ON [PRIMARY]
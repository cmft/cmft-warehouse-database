﻿CREATE TABLE [dbo].[sDOGS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[StoredProcedureName] [varchar](1024) NOT NULL,
	[MustBeScheduled] [bit] NULL,
	[DefaultOutputTypeID] [bigint] NULL,
	[CanChangeDefaultOutputType] [bit] NULL,
	[DefaultPrinterID] [bigint] NULL,
	[CanChangeDefaultPrinter] [bit] NULL,
	[Enabled] [bit] NULL,
	[ActionTable] [varchar](255) NULL,
	[ActionColumn] [varchar](255) NULL,
	[DetailColumns] [varchar](1024) NULL
) ON [PRIMARY]
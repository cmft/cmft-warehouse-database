﻿CREATE TABLE [dbo].[dUserProfiles](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[ProfileObjectID] [bigint] NULL,
	[Position] [int] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
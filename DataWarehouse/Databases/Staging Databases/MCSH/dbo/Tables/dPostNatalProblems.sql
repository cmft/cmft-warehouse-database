﻿CREATE TABLE [dbo].[dPostNatalProblems](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[ReproductiveHistoryID] [bigint] NULL,
	[PostNatalProblemID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sRRCancelReason](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
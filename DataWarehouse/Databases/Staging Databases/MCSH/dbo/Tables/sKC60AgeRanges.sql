﻿CREATE TABLE [dbo].[sKC60AgeRanges](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[IndexNumber] [int] NULL,
	[UpperAgeRange] [int] NULL,
	[LowerAgeRange] [int] NULL
) ON [PRIMARY]
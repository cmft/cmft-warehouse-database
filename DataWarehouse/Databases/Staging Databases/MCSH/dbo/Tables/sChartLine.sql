﻿CREATE TABLE [dbo].[sChartLine](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](80) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[Y2Axis] [bit] NOT NULL,
	[ChartLineStyleID] [bigint] NOT NULL,
	[Thickness] [smallint] NOT NULL,
	[ChartID] [bigint] NOT NULL,
	[LineItem] [bigint] NULL
) ON [PRIMARY]
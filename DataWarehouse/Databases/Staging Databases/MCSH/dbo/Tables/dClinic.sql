﻿CREATE TABLE [dbo].[dClinic](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClinicTemplateVersionID] [bigint] NULL,
	[DateTime] [smalldatetime] NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Comments] [varchar](1024) NULL
) ON [PRIMARY]
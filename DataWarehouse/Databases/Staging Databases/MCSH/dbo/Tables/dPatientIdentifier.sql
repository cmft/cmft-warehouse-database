﻿CREATE TABLE [dbo].[dPatientIdentifier](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[Identifier] [varchar](50) NULL,
	[PITypeID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
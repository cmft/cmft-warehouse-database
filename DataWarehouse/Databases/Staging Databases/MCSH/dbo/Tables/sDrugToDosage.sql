﻿CREATE TABLE [dbo].[sDrugToDosage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugID] [bigint] NOT NULL,
	[DosageID] [bigint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dCHEpisode](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dChiropodyABPIndices](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[AssessmentDate] [smalldatetime] NOT NULL,
	[Brachial] [int] NULL,
	[RightAnkle] [int] NULL,
	[LeftAnkle] [int] NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sClinicalNoteTypeOptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClinicalNoteTypeID] [bigint] NULL,
	[ClinicalNoteOptionID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
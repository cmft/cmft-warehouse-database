﻿CREATE TABLE [dbo].[dClinicWaitStack](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClinicID] [bigint] NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[AppointmentTypeID] [bigint] NULL,
	[AppointmentSessionTypeID] [bigint] NULL,
	[Note] [varchar](1024) NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Active] [bit] NULL,
	[Enabled] [bit] NULL,
	[ApptFirstTryID] [bigint] NULL,
	[ApptWaitingReasonID] [bigint] NULL,
	[ApptFirstTryElseWhereID] [bigint] NULL,
	[TempID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sProfileObjectLookupTable](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProfileObjectID] [bigint] NULL,
	[Table] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
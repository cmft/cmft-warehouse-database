﻿CREATE TABLE [dbo].[dDOGSParameters](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOGSID] [bigint] NOT NULL,
	[PARAMETERID] [bigint] NOT NULL,
	[VALUE] [varchar](1024) NULL,
	[COLLECTIONDATETIME] [smalldatetime] NULL,
	[USERID] [bigint] NULL,
	[ENABLED] [bit] NULL
) ON [PRIMARY]
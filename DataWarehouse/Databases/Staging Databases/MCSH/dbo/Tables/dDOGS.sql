﻿CREATE TABLE [dbo].[dDOGS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DogsID] [bigint] NULL,
	[Description] [varchar](1024) NULL,
	[CLINICID] [bigint] NULL,
	[LETTERID] [bigint] NULL,
	[SCHEDULEID] [bigint] NULL,
	[LASTRUNDATETIME] [smalldatetime] NULL,
	[Enabled] [bit] NULL,
	[GreenDays] [bigint] NULL,
	[AmberDays] [bigint] NULL,
	[RedDays] [bigint] NULL,
	[LightsFromDOL] [bit] NULL
) ON [PRIMARY]
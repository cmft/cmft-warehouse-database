﻿CREATE TABLE [dbo].[dSRHCareActivity](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SRHCareActivityID] [bigint] NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[AppointmentID] [bigint] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
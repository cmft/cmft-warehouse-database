﻿CREATE TABLE [dbo].[dInjectionSite](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[InjectionSiteID] [bigint] NOT NULL,
	[Comments] [varchar](1024) NULL,
	[DateTaken] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sKC60Config](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PartASectionID] [bigint] NULL,
	[PartAMaleCount] [bit] NULL,
	[PartAHomoCount] [bit] NULL,
	[PartAFemaleCount] [bit] NULL,
	[PartBHomoCountOnly] [bit] NULL,
	[LineNumber] [int] NULL,
	[Description] [varchar](255) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
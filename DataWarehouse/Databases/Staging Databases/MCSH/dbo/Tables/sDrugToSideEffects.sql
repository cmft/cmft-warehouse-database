﻿CREATE TABLE [dbo].[sDrugToSideEffects](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugID] [bigint] NOT NULL,
	[SideEffectID] [bigint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dCarePathwayEvent](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CarepathwayID] [bigint] NOT NULL,
	[CarepathwayTemplateEventID] [bigint] NOT NULL,
	[OffSetDay] [int] NULL,
	[Actioned] [bit] NOT NULL,
	[ActionUserID] [bigint] NULL
) ON [PRIMARY]
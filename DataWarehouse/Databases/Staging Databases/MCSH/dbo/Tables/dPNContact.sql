﻿CREATE TABLE [dbo].[dPNContact](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PNEpisodeID] [bigint] NOT NULL,
	[IndexPatientID] [bigint] NOT NULL,
	[ContactPatientID] [bigint] NULL,
	[PNContactReferenceNo] [varchar](50) NOT NULL,
	[FirstName] [varchar](1024) NULL,
	[LastName] [varchar](1024) NULL,
	[SexID] [bigint] NULL,
	[TitleID] [bigint] NULL,
	[DateOfBirth] [smalldatetime] NULL,
	[Age] [varchar](3) NULL,
	[ContactInfo] [varchar](1024) NULL,
	[PostCode] [varchar](10) NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NULL,
	[ParentID] [bigint] NULL
) ON [PRIMARY]
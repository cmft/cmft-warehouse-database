﻿CREATE TABLE [dbo].[sKT31Parts](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](8) NULL,
	[Title] [varchar](120) NULL,
	[StoredProcedure] [varchar](30) NULL,
	[SexID] [bigint] NULL
) ON [PRIMARY]
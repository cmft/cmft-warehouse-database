﻿CREATE TABLE [dbo].[sSmsProviderStatus](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProviderId] [bigint] NOT NULL,
	[PvStatusId] [bigint] NOT NULL,
	[ProviderStatusId] [bigint] NOT NULL,
	[ProviderDescription] [varchar](255) NOT NULL
) ON [PRIMARY]
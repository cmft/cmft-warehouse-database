﻿CREATE TABLE [dbo].[dAuditDetail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AuditID] [bigint] NULL,
	[ColumnName] [varchar](50) NULL,
	[OldValue] [varchar](1024) NULL,
	[NewValue] [varchar](1024) NULL
) ON [PRIMARY]
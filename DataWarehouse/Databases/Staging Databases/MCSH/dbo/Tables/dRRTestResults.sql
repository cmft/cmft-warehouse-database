﻿CREATE TABLE [dbo].[dRRTestResults](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TestInformationID] [bigint] NOT NULL,
	[ValueType] [varchar](50) NULL,
	[TestID] [bigint] NULL,
	[ResultValue] [varchar](5000) NULL,
	[Units] [varchar](50) NULL,
	[RefRangeLow] [varchar](50) NULL,
	[RefRangeHigh] [varchar](50) NULL,
	[AbnormalFlags] [varchar](50) NULL,
	[AbnormalResult] [bit] NULL,
	[Comments] [text] NULL,
	[ObservationResultStatus] [varchar](50) NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[ResultReceivedDateTime] [smalldatetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
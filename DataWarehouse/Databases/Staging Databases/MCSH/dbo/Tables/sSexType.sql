﻿CREATE TABLE [dbo].[sSexType](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[SophidCode] [varchar](1024) NULL,
	[Enabled] [bit] NOT NULL,
	[Code] [int] NULL
) ON [PRIMARY]
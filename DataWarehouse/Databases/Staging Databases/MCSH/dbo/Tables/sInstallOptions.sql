﻿CREATE TABLE [dbo].[sInstallOptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[ConstName] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
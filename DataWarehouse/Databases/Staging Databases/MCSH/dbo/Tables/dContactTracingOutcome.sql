﻿CREATE TABLE [dbo].[dContactTracingOutcome](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactTracingActionID] [bigint] NOT NULL,
	[ContactTracingOutcomeID] [bigint] NOT NULL,
	[OutcomeDate] [smalldatetime] NOT NULL,
	[Comments] [varchar](1024) NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dUnmetDemand](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[ClinicID] [bigint] NOT NULL,
	[Unmet] [bigint] NULL
) ON [PRIMARY]
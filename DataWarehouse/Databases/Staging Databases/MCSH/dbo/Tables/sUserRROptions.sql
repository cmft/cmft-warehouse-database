﻿CREATE TABLE [dbo].[sUserRROptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NOT NULL,
	[ShowActioned] [bit] NOT NULL,
	[UserOrdersOnly] [bit] NOT NULL
) ON [PRIMARY]
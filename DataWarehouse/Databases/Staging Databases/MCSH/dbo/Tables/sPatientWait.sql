﻿CREATE TABLE [dbo].[sPatientWait](
	[ID] [bigint] NOT NULL,
	[DESCRIPTION] [varchar](1024) NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
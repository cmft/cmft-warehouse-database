﻿CREATE TABLE [dbo].[sChiropodyImages](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ImageName] [varchar](1024) NULL,
	[FileName] [varchar](1024) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
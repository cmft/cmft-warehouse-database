﻿CREATE TABLE [dbo].[dRisk](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[ExposureTypeID] [bigint] NULL,
	[CountryWhereContractedID] [bigint] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dDietaryObjective](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DietAssessmentID] [bigint] NOT NULL,
	[ObjectiveID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
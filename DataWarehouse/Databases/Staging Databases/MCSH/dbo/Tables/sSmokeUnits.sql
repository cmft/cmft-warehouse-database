﻿CREATE TABLE [dbo].[sSmokeUnits](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[NumericValue] [int] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
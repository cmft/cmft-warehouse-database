﻿CREATE TABLE [dbo].[sSiteSettings](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[SettingValue] [varchar](1024) NULL,
	[SiteID] [bigint] NOT NULL,
	[DataType] [varchar](50) NULL,
	[AssociatedTable] [varchar](50) NULL,
	[InternalOnly] [bit] NULL,
	[HelpText] [varchar](1024) NULL
) ON [PRIMARY]
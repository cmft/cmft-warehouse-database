﻿CREATE TABLE [dbo].[dPseudonymPatientIndentifier](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientPseudonymID] [bigint] NULL,
	[Identifier] [varchar](50) NULL,
	[PITypeID] [bigint] NULL
) ON [PRIMARY]
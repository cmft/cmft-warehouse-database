﻿CREATE TABLE [dbo].[sInvestigationPanel](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[PanelTypeID] [bigint] NOT NULL
) ON [PRIMARY]
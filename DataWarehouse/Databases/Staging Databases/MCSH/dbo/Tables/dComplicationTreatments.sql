﻿CREATE TABLE [dbo].[dComplicationTreatments](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ComplicationID] [bigint] NOT NULL,
	[ComplicationTreatmentID] [bigint] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
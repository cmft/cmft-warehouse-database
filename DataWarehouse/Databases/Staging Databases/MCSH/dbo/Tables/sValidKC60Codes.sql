﻿CREATE TABLE [dbo].[sValidKC60Codes](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[KC60Code] [varchar](5) NOT NULL,
	[Condition] [varchar](120) NOT NULL
) ON [PRIMARY]
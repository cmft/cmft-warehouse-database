﻿CREATE TABLE [dbo].[sFamilyRelationship](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Degree] [int] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
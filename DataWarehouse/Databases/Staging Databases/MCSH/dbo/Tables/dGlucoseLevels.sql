﻿CREATE TABLE [dbo].[dGlucoseLevels](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[GlucoseTypeID] [bigint] NOT NULL,
	[GlucoseValue] [varchar](50) NOT NULL,
	[UnitsOfMeasurementID] [bigint] NOT NULL,
	[MeasurementDate] [smalldatetime] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
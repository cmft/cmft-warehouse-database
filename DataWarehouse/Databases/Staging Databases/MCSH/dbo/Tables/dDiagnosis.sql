﻿CREATE TABLE [dbo].[dDiagnosis](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DiagnosisCodeID] [bigint] NULL,
	[ICD10ID] [bigint] NULL,
	[MethodOfAcquisitionID] [bigint] NULL,
	[SuffixID] [bigint] NULL,
	[Comments] [char](1024) NULL,
	[DiagnosisProvisionalDateTime] [smalldatetime] NULL,
	[DiagnosisFinalDateTime] [smalldatetime] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[ProvisionalDiagnosisCodeID] [bigint] NULL,
	[ProvisionalSuffixID] [bigint] NULL,
	[ProvisionalICD10ID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[Reason] [varchar](1024) NULL,
	[dPatientHIVStageID] [bigint] NULL
) ON [PRIMARY]
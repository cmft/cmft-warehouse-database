﻿CREATE TABLE [dbo].[sOutputDefinition](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OutputCategoryID] [bigint] NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Path] [varchar](1024) NOT NULL,
	[PrinterID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
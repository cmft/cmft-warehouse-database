﻿CREATE TABLE [dbo].[dRRDrugResults](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugInformationID] [bigint] NOT NULL,
	[DrugID] [bigint] NULL,
	[DatabaseMatches] [varchar](50) NULL,
	[FoldChange] [varchar](50) NULL,
	[CutOffLow] [float] NULL,
	[CutOffHigh] [float] NULL,
	[ResistanceAnalysis] [varchar](50) NULL,
	[ClinicalNote] [varchar](5000) NULL,
	[CollectionDateTime] [smalldatetime] NULL
) ON [PRIMARY]
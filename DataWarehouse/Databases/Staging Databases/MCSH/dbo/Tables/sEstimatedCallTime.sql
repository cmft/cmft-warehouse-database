﻿CREATE TABLE [dbo].[sEstimatedCallTime](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[CallValue] [int] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
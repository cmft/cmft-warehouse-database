﻿CREATE TABLE [dbo].[dPNDiagnosis](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PNContactInformationID] [bigint] NOT NULL,
	[DiagnosisCodeID] [bigint] NOT NULL,
	[DiagnosisDateTime] [bigint] NULL,
	[Final] [bit] NULL,
	[Diagnosis] [varchar](1024) NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NULL,
	[ParentID] [bigint] NULL
) ON [PRIMARY]
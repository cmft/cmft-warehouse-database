﻿CREATE TABLE [dbo].[sLocationType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Code] [varchar](3) NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
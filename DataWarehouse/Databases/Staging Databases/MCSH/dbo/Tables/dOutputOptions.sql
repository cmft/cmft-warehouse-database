﻿CREATE TABLE [dbo].[dOutputOptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OutputID] [bigint] NULL,
	[OutputOptionID] [bigint] NULL,
	[SelectedID] [bigint] NULL
) ON [PRIMARY]
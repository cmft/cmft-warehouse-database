﻿CREATE TABLE [dbo].[dPersonalProfilePanes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dPPID] [bigint] NOT NULL,
	[sPaneID] [bigint] NOT NULL,
	[FrameNumber] [int] NOT NULL
) ON [PRIMARY]
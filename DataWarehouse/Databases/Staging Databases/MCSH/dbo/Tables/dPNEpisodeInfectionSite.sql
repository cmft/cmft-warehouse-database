﻿CREATE TABLE [dbo].[dPNEpisodeInfectionSite](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [bigint] NOT NULL,
	[InfectionSiteID] [bigint] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
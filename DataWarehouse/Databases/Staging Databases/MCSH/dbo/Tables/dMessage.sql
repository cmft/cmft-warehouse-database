﻿CREATE TABLE [dbo].[dMessage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SenderID] [bigint] NOT NULL,
	[RecipientID] [bigint] NOT NULL,
	[MessageText] [text] NULL,
	[ReadByRecipient] [bit] NULL,
	[Priority] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
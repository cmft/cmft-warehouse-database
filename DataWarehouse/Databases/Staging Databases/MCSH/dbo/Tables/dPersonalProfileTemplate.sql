﻿CREATE TABLE [dbo].[dPersonalProfileTemplate](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[sPPTID] [bigint] NOT NULL
) ON [PRIMARY]
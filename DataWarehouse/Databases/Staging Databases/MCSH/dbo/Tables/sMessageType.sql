﻿CREATE TABLE [dbo].[sMessageType](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[FieldSeparator] [varchar](50) NULL,
	[EncodingChars] [varchar](50) NULL,
	[SendingApplication] [varchar](50) NULL,
	[ReceivingApplication] [varchar](50) NULL,
	[MessageType] [varchar](50) NULL,
	[SendingFacility] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sChartType](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](80) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[LinesSP] [varchar](50) NULL,
	[SlicesSP] [varchar](20) NULL,
	[Page] [varchar](50) NULL,
	[ChartType] [smallint] NOT NULL,
	[XTime] [bit] NOT NULL,
	[Y2Axis] [bit] NOT NULL,
	[YLogarithmic] [bit] NOT NULL,
	[Y2Logarithmic] [bit] NOT NULL
) ON [PRIMARY]
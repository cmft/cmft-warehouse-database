﻿CREATE TABLE [dbo].[dContactTracingContact](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactTracingID] [bigint] NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[ContactTypeID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
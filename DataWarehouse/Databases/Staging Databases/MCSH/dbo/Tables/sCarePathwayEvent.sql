﻿CREATE TABLE [dbo].[sCarePathwayEvent](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CarePathwayID] [bigint] NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[OffSetDay] [int] NOT NULL,
	[ResourceTypeID] [bigint] NULL,
	[EventTypeID] [bigint] NOT NULL,
	[EventText] [text] NULL,
	[EventLetterTypeID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
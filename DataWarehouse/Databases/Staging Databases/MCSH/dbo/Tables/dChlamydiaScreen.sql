﻿CREATE TABLE [dbo].[dChlamydiaScreen](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[SiteCodeID] [bigint] NOT NULL,
	[ReasonForTestID] [bigint] NOT NULL,
	[SpecimenTypeID] [bigint] NOT NULL,
	[TypeOfTestID] [bigint] NOT NULL,
	[NewSexualPartnerID] [bigint] NOT NULL,
	[MultipleSexualPartnersID] [bigint] NOT NULL,
	[TestReference] [varchar](50) NOT NULL,
	[Notes] [text] NULL,
	[CollectionDateTime] [datetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[OutcomeID] [bigint] NULL,
	[Enabled] [bit] NOT NULL,
	[DateOfTest] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
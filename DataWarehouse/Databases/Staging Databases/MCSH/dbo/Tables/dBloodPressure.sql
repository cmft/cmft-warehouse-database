﻿CREATE TABLE [dbo].[dBloodPressure](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[Systolic] [float] NOT NULL,
	[Diastolic] [float] NOT NULL,
	[BloodPressurePositionID] [bigint] NULL,
	[DateTaken] [smalldatetime] NOT NULL,
	[UserID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
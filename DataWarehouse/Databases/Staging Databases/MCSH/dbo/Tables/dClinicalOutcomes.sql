﻿CREATE TABLE [dbo].[dClinicalOutcomes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[ClinicalOutcomeID] [bigint] NOT NULL,
	[Comment] [varchar](1024) NULL,
	[DateSet] [smalldatetime] NULL,
	[UserID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
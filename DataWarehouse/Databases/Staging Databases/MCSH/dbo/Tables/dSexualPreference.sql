﻿CREATE TABLE [dbo].[dSexualPreference](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[SexualPreferenceID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
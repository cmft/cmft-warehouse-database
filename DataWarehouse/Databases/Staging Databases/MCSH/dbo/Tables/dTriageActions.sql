﻿CREATE TABLE [dbo].[dTriageActions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TriageID] [bigint] NULL,
	[TriageActionID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dCarePathway](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[CarePathwayTemplateID] [bigint] NOT NULL
) ON [PRIMARY]
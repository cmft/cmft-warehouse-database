﻿CREATE TABLE [dbo].[dRREpisode](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ActionID] [bigint] NULL,
	[UserID] [bigint] NULL,
	[ActionDateTime] [datetime] NULL
) ON [PRIMARY]
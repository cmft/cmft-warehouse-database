﻿CREATE TABLE [dbo].[dOutputBookmarks](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OutputID] [bigint] NULL,
	[BookmarkName] [varchar](50) NULL,
	[BookmarkValue] [varchar](8000) NULL,
	[OptionValue] [bigint] NULL
) ON [PRIMARY]
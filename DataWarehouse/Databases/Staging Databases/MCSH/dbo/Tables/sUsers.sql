﻿CREATE TABLE [dbo].[sUsers](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [char](50) NOT NULL,
	[Pwd] [char](50) NOT NULL,
	[TitleID] [bigint] NULL,
	[FirstName] [varchar](1024) NOT NULL,
	[LastName] [varchar](1024) NOT NULL,
	[PwdStartDate] [smalldatetime] NOT NULL,
	[PwdDuration] [int] NOT NULL,
	[Enabled] [bit] NULL,
	[SessionTimeout] [bigint] NULL,
	[ResourceTypeID] [bigint] NULL,
	[LastURL] [varchar](1024) NULL,
	[CollectionDatetime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[JobTitle] [varchar](50) NULL,
	[ChangePassword] [bit] NULL
) ON [PRIMARY]
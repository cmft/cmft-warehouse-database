﻿CREATE TABLE [dbo].[sDefaultFieldValues](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TableName] [varchar](50) NOT NULL,
	[ValueID] [bigint] NOT NULL
) ON [PRIMARY]
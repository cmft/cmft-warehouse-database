﻿CREATE TABLE [dbo].[dActiveSessions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SessionID] [varchar](255) NULL,
	[DateTime] [datetime] NOT NULL,
	[UserID] [bigint] NULL,
	[IPAddress] [varchar](19) NULL
) ON [PRIMARY]
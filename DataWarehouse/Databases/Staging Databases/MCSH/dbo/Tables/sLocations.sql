﻿CREATE TABLE [dbo].[sLocations](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[Address] [varchar](1024) NULL,
	[Postcode] [varchar](50) NULL,
	[ContactNo] [varchar](50) NULL
) ON [PRIMARY]
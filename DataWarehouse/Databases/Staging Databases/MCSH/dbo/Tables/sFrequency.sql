﻿CREATE TABLE [dbo].[sFrequency](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[FrequencyValue] [float] NULL,
	[Enabled] [bit] NULL,
	[Position] [bigint] NULL
) ON [PRIMARY]
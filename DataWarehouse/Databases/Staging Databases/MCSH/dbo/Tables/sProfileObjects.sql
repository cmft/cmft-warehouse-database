﻿CREATE TABLE [dbo].[sProfileObjects](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[COMObject] [varchar](1024) NULL,
	[Enabled] [bit] NULL,
	[TypeID] [bigint] NULL,
	[SourcePath] [varchar](1024) NULL,
	[PatientMenuItem] [bit] NULL,
	[PatientMenuItemURL] [varchar](1024) NULL,
	[InstallOptionID] [bigint] NULL,
	[SexTypeID] [bigint] NULL,
	[SecurityFlagID] [bigint] NULL,
	[AssociatedSQL] [varchar](1024) NULL,
	[AssociatedTableName] [varchar](50) NULL,
	[LetterOption] [bigint] NULL
) ON [PRIMARY]
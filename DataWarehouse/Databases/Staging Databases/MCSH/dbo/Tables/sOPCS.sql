﻿CREATE TABLE [dbo].[sOPCS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](3) NULL,
	[Description] [varchar](1024) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
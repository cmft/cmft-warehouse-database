﻿CREATE TABLE [dbo].[dPatientRecall](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[RecallDate] [smalldatetime] NULL,
	[ResourceTypeID] [bigint] NULL,
	[RecallReasonID] [bigint] NULL,
	[Comment] [varchar](1024) NULL,
	[Actioned] [bit] NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
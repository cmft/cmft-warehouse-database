﻿CREATE TABLE [dbo].[sDrugToMetabolicProblems](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugID] [bigint] NOT NULL,
	[MetabolicProblemID] [bigint] NOT NULL
) ON [PRIMARY]
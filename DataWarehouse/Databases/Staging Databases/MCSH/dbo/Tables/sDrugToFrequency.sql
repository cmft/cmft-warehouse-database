﻿CREATE TABLE [dbo].[sDrugToFrequency](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugID] [bigint] NOT NULL,
	[FrequencyID] [bigint] NOT NULL
) ON [PRIMARY]
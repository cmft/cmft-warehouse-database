﻿CREATE TABLE [dbo].[dMenstruation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[MenstrualCycleDuration] [varchar](10) NULL,
	[MenstrualPeriodTypeID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[LMP] [smalldatetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sConditions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Diabetes] [bit] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
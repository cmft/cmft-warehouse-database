﻿CREATE TABLE [dbo].[sClinicalSignificance](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[SignificanceValue] [int] NULL
) ON [PRIMARY]
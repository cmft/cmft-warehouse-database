﻿CREATE TABLE [dbo].[sColourItem](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](12) NOT NULL,
	[ColourCategoryID] [bigint] NOT NULL,
	[Position] [bigint] NULL,
	[Colour] [char](6) NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
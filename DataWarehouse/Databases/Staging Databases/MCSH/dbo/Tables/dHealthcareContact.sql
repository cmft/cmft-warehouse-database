﻿CREATE TABLE [dbo].[dHealthcareContact](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[ContactID] [bigint] NULL,
	[ContactStartDate] [smalldatetime] NULL,
	[ContactEndDate] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
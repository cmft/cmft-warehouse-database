﻿CREATE TABLE [dbo].[sClinicFrequency](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[FrequencyValue] [int] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
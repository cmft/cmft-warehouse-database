﻿CREATE TABLE [dbo].[dCombination](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[CombinationNumber] [int] NOT NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NULL,
	[Comments] [varchar](1024) NULL,
	[Enabled] [bit] NULL,
	[UserID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL
) ON [PRIMARY]
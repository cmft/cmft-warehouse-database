﻿CREATE TABLE [dbo].[sChiropodyLineDrawings](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[HTML_Name] [varchar](1024) NULL,
	[ImagePath] [varchar](1024) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
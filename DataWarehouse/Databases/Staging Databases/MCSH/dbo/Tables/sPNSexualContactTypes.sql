﻿CREATE TABLE [dbo].[sPNSexualContactTypes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
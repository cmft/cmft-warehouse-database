﻿CREATE TABLE [dbo].[dIVDU](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserOfNeedleExchange] [bit] NULL,
	[IVDUSinceLastVisit] [bit] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
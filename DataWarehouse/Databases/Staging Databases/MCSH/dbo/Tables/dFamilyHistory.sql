﻿CREATE TABLE [dbo].[dFamilyHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[ConditionID] [bigint] NOT NULL,
	[RelationshipID] [bigint] NOT NULL,
	[Notes] [text] NULL,
	[FamilyHistoryDate] [smalldatetime] NULL,
	[DateOfBirth] [smalldatetime] NULL,
	[DateOfDeath] [smalldatetime] NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[UserID] [bigint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[sOrderProfile](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sQueries](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[HelpText] [text] NULL,
	[ProcedureName] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
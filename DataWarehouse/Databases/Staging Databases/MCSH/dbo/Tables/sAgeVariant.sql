﻿CREATE TABLE [dbo].[sAgeVariant](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Value] [int] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
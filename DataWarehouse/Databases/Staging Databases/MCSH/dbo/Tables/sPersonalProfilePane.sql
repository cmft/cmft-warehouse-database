﻿CREATE TABLE [dbo].[sPersonalProfilePane](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClassName] [varchar](1024) NULL,
	[FunctionName] [varchar](1024) NULL,
	[Parameters] [varchar](1024) NULL,
	[url] [varchar](1024) NULL
) ON [PRIMARY]
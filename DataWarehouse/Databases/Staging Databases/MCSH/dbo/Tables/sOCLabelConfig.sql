﻿CREATE TABLE [dbo].[sOCLabelConfig](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) NULL,
	[FormatLevel] [bigint] NULL,
	[X] [int] NULL,
	[Y] [int] NULL,
	[LabelID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
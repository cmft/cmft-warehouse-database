﻿CREATE TABLE [dbo].[sInstallOptionLookupTable](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AdminTableID] [int] NULL,
	[InstallOptionID] [bigint] NULL
) ON [PRIMARY]
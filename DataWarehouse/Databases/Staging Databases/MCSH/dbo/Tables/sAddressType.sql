﻿CREATE TABLE [dbo].[sAddressType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
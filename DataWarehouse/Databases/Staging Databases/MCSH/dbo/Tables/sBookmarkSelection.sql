﻿CREATE TABLE [dbo].[sBookmarkSelection](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[BookmarkGroupID] [bigint] NULL,
	[SelectSQL] [varchar](1024) NULL,
	[Multiple] [bit] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sEducationPublication](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EducationCategoryID] [bigint] NULL,
	[Description] [varchar](1024) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
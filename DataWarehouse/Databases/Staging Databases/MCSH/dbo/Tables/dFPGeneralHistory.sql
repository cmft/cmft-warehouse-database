﻿CREATE TABLE [dbo].[dFPGeneralHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[HistoryDate] [smalldatetime] NULL,
	[HistoryTypeID] [bigint] NULL,
	[Details] [varchar](1024) NULL,
	[ReviewDate] [smalldatetime] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
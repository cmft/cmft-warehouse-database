﻿CREATE TABLE [dbo].[sCD4Upload](
	[CD4UploadActionID] [bigint] IDENTITY(1,1) NOT NULL,
	[HospitalNo] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[DOB] [varchar](50) NULL,
	[LabNo] [varchar](50) NULL,
	[SpecDate] [varchar](50) NULL,
	[CD4TC] [varchar](50) NULL,
	[HIVVL] [varchar](50) NULL
) ON [PRIMARY]
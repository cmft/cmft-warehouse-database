﻿CREATE TABLE [dbo].[dPNIntercourseTypes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PNContactInformationID] [bigint] NOT NULL,
	[IntercourseTypeID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[ParentID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dLoginInfo](
	[UserId] [bigint] NOT NULL,
	[DateTime] [smalldatetime] NOT NULL,
	[LastLogin] [smalldatetime] NULL,
	[SessionID] [varchar](255) NOT NULL
) ON [PRIMARY]
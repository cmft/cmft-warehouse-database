﻿CREATE TABLE [dbo].[dProvisionalBooking](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[TempID] [bigint] NULL,
	[ClinicID] [bigint] NULL,
	[DateTime] [datetime] NULL,
	[Referenced] [bit] NOT NULL
) ON [PRIMARY]
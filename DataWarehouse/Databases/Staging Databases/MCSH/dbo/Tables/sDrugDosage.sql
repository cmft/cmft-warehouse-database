﻿CREATE TABLE [dbo].[sDrugDosage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugID] [bigint] NOT NULL,
	[Dosage] [varchar](50) NOT NULL,
	[DrugMeasureID] [bigint] NULL,
	[DrugCount] [int] NULL,
	[Enabled] [bit] NOT NULL,
	[DrugFormulationID] [bigint] NULL
) ON [PRIMARY]
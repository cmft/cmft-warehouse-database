﻿CREATE TABLE [dbo].[dAlcohol](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[UnitsPerWeek] [varchar](50) NOT NULL,
	[UnitsID] [bigint] NOT NULL,
	[Assessmentdate] [smalldatetime] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
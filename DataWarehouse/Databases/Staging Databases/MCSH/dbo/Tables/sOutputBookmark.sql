﻿CREATE TABLE [dbo].[sOutputBookmark](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OutputGroupID] [bigint] NULL,
	[Description] [varchar](100) NULL,
	[Enabled] [bit] NULL,
	[UserField] [bit] NULL,
	[OutputFieldTypeID] [bigint] NULL,
	[KeyDescription] [varchar](100) NULL
) ON [PRIMARY]
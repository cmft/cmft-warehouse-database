﻿CREATE TABLE [dbo].[dAntenatal](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[CountryID] [bigint] NOT NULL,
	[PregnantAtDiagnosisID] [bigint] NOT NULL,
	[DateOfConception] [smalldatetime] NULL,
	[ModeOfConceptionID] [bigint] NULL,
	[BirthPlanDiscussedID] [bigint] NULL,
	[StatusID] [bigint] NULL,
	[ExpectedDateOfDelivery] [smalldatetime] NULL,
	[ModeOfDeliveryID] [bigint] NULL,
	[DateOfCaesarean] [smalldatetime] NULL,
	[PlaceOfDeliveryID] [bigint] NULL,
	[WardID] [bigint] NULL,
	[OutcomeOfPregnancyID] [bigint] NULL,
	[Notes] [text] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[dDOGSHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DogsID] [bigint] NULL,
	[CollectionDateTime] [datetime] NULL,
	[OutputDefinitionID] [bigint] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sCarePathwayProperty](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CarePathwayID] [bigint] NULL,
	[PropertyTypeID] [bigint] NULL,
	[Description] [varchar](1024) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sTabs](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[DrugID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
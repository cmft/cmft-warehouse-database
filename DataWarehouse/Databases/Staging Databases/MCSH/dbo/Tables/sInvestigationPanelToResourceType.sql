﻿CREATE TABLE [dbo].[sInvestigationPanelToResourceType](
	[ResourceTypeID] [bigint] NOT NULL,
	[InvestigationPanelTypeID] [bigint] NOT NULL
) ON [PRIMARY]
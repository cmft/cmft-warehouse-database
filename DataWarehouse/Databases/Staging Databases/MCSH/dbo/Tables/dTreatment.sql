﻿CREATE TABLE [dbo].[dTreatment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[TreatmentID] [bigint] NOT NULL,
	[Notes] [text] NULL,
	[TreatmentCommencedDate] [smalldatetime] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
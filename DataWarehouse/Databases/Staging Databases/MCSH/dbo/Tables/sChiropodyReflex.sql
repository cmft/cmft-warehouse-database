﻿CREATE TABLE [dbo].[sChiropodyReflex](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
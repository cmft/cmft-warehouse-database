﻿CREATE TABLE [dbo].[sWorkstations](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[IPAddress] [varchar](19) NOT NULL,
	[Timeout] [bigint] NOT NULL,
	[Description] [varchar](1024) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sResponseCategories](
	[ID] [int] NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dCondomDispensary](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GivenDate] [smalldatetime] NOT NULL,
	[Postcode] [varchar](50) NOT NULL,
	[AgeRangeID] [bigint] NOT NULL,
	[SexID] [bigint] NOT NULL,
	[CondomTypeID] [bigint] NOT NULL,
	[FirstVisit] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sChlamydiaEthnicity](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Code] [bigint] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
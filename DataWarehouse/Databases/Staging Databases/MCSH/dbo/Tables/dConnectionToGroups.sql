﻿CREATE TABLE [dbo].[dConnectionToGroups](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ConnectionGroupID] [bigint] NULL,
	[ConnectionID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sOutputFieldType](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Enabled] [char](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dDietAssessment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [bigint] NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[MealsPreparedID] [bigint] NULL,
	[MealPatternID] [bigint] NULL,
	[SnacksID] [bigint] NULL,
	[ExcessKCals] [int] NULL,
	[TargetWeightReduction] [int] NULL,
	[Notes] [text] NULL,
	[AssessmentDate] [smalldatetime] NOT NULL,
	[CollectiondateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[MotivationID] [bigint] NULL,
	[UnderstandingID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
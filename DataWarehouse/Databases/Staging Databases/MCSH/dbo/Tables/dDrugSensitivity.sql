﻿CREATE TABLE [dbo].[dDrugSensitivity](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DrugID] [bigint] NOT NULL,
	[ReactionID] [bigint] NOT NULL,
	[SeverityID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sEducationCategory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sClinicList](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClinicTemplateVersionID] [bigint] NULL,
	[ResourceID] [bigint] NULL,
	[AllowOverbooking] [bit] NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[ParentID] [bigint] NULL,
	[LocationTypeID] [int] NULL
) ON [PRIMARY]
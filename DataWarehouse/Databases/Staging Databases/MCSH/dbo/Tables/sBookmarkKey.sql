﻿CREATE TABLE [dbo].[sBookmarkKey](
	[KeyDescription] [varchar](100) NOT NULL,
	[BookmarkName] [varchar](50) NULL,
	[IncLetter] [bit] NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dPID](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MSHID] [bigint] NULL,
	[PatientID] [bigint] NULL,
	[PatientNumber] [varchar](50) NULL,
	[PatientDOB] [varchar](50) NULL,
	[PatientSex] [varchar](50) NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[Enabled] [bit] NULL,
	[UserID] [bigint] NULL,
	[ExceptionReason] [varchar](1024) NULL,
	[PatientName] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sRequestingGuidelines](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TestGroupID] [bigint] NULL,
	[FileName] [varchar](100) NULL,
	[FileImage] [image] NULL,
	[FileType] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
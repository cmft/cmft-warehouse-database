﻿CREATE TABLE [dbo].[dPNEpisodeInfection](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [bigint] NOT NULL,
	[InfectionID] [bigint] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
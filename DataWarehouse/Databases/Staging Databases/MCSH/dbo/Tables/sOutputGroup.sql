﻿CREATE TABLE [dbo].[sOutputGroup](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[Prefix] [varchar](50) NULL,
	[StoredProcedure] [varchar](1024) NULL,
	[CanChangePrefix] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dAppointmentResources](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AppointmentID] [bigint] NULL,
	[ResourceTypeID] [bigint] NULL
) ON [PRIMARY]
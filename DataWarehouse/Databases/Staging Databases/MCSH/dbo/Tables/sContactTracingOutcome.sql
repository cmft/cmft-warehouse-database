﻿CREATE TABLE [dbo].[sContactTracingOutcome](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Description] [varchar](1024) NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sInvestigationTemplate](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Enabled] [bit] NULL,
	[HPACode] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sLineDrawing](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Image] [image] NULL,
	[ContentType] [varchar](50) NULL,
	[Height] [float] NULL,
	[Width] [float] NULL,
	[ToolboxID] [bigint] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
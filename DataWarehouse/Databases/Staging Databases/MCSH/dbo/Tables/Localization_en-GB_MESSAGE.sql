﻿CREATE TABLE [dbo].[Localization_en-GB_MESSAGE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CultureName] [nvarchar](5) NULL,
	[StrGroup] [nvarchar](10) NULL,
	[LookupID] [nvarchar](50) NOT NULL,
	[LocalizedText] [nvarchar](4000) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sSiteLocationLookupData](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Object] [varchar](50) NULL,
	[ObjectID] [bigint] NULL,
	[SiteLocationID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sClinicalNoteOption](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[FieldTypeId] [bigint] NULL
) ON [PRIMARY]
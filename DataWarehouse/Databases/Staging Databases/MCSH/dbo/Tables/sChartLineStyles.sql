﻿CREATE TABLE [dbo].[sChartLineStyles](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
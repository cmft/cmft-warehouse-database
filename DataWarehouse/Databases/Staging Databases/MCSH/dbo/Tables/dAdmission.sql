﻿CREATE TABLE [dbo].[dAdmission](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DateOfAdmission] [smalldatetime] NOT NULL,
	[ReasonForAdmissionID] [bigint] NOT NULL,
	[AdmissionLocationID] [bigint] NOT NULL,
	[Notes] [text] NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[sChlamydiaEthnicityLookup](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EthnicCodeID] [bigint] NOT NULL,
	[ChlamydiaEthnicityID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sClinicTemplate](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [datetime] NULL,
	[UserID] [bigint] NULL,
	[ModuleFlags] [bigint] NULL
) ON [PRIMARY]
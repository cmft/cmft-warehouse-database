﻿CREATE TABLE [dbo].[sMenuToGroups](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MenuID] [bigint] NULL,
	[MenuGroupID] [bigint] NULL,
	[Position] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
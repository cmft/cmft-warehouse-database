﻿CREATE TABLE [dbo].[dReproductiveHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[YearofPregnancy] [int] NOT NULL,
	[Duration] [int] NULL,
	[AliveAndWell] [bit] NULL,
	[ModeOfDeliveryID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[NeoNatalDeath] [bit] NULL,
	[NonInducedAbortion] [bit] NULL,
	[StillBirth] [bit] NULL,
	[Ectopic] [bit] NULL,
	[RiskOfUnplannedPregnancy] [tinyint] NULL,
	[InducedAbortion] [bit] NULL
) ON [PRIMARY]
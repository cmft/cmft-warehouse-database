﻿CREATE TABLE [dbo].[sSiteLocation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[SiteID] [bigint] NOT NULL,
	[Enabled] [bit] NULL,
	[PCTCode] [varchar](10) NULL,
	[PCTName] [varchar](80) NULL,
	[ClinicCode] [varchar](10) NULL,
	[ClinicName] [varchar](80) NULL,
	[LocationCode] [varchar](10) NULL
) ON [PRIMARY]
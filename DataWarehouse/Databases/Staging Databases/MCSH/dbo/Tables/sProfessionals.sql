﻿CREATE TABLE [dbo].[sProfessionals](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TitleID] [bigint] NULL,
	[FirstName] [varchar](1024) NULL,
	[LastName] [varchar](1024) NULL,
	[ContactNo] [varchar](1024) NULL,
	[LocationID] [bigint] NULL,
	[ProfessionalTypeID] [bigint] NULL,
	[Email] [varchar](1024) NULL
) ON [PRIMARY]
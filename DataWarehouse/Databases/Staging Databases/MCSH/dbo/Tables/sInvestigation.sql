﻿CREATE TABLE [dbo].[sInvestigation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [char](1024) NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
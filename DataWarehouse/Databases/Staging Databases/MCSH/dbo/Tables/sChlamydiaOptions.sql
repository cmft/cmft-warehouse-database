﻿CREATE TABLE [dbo].[sChlamydiaOptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Code] [varchar](10) NULL,
	[Enabled] [bit] NULL,
	[Position] [float] NULL
) ON [PRIMARY]
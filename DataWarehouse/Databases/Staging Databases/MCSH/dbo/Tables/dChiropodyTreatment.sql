﻿CREATE TABLE [dbo].[dChiropodyTreatment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ChiropodyID] [bigint] NULL,
	[TreatmentID] [bigint] NULL
) ON [PRIMARY]
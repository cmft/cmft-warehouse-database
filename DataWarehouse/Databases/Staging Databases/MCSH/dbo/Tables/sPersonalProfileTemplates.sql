﻿CREATE TABLE [dbo].[sPersonalProfileTemplates](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[framesetcode] [varchar](1024) NOT NULL
) ON [PRIMARY]
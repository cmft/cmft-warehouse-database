﻿CREATE TABLE [dbo].[sBookmarkDefinition](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[BookmarkName] [varchar](50) NULL,
	[RelatedSPName] [varchar](50) NULL,
	[ClinicalSignificant] [bit] NULL,
	[SelectSQL] [varchar](1000) NULL,
	[Multiple] [bit] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dOutput](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[OutputDefinitionID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
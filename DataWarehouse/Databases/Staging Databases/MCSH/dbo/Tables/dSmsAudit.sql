﻿CREATE TABLE [dbo].[dSmsAudit](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](255) NOT NULL,
	[ScheduledDateTime] [datetime] NULL,
	[Scheduled] [bit] NOT NULL,
	[Ticket] [varchar](255) NULL,
	[PatientId] [bigint] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[StatusID] [bigint] NOT NULL,
	[MessageReturn] [varchar](2048) NOT NULL,
	[TelePhoneNumber] [varchar](50) NOT NULL
) ON [PRIMARY]
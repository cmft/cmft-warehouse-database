﻿CREATE TABLE [dbo].[sysvalues](
	[name] [nvarchar](35) NULL,
	[number] [int] NOT NULL,
	[type] [nchar](3) NOT NULL,
	[low] [int] NULL,
	[high] [int] NULL,
	[status] [int] NULL CONSTRAINT [DF__sysvalues__statu__060DEAE8]  DEFAULT (0)
) ON [PRIMARY]
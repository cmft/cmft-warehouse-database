﻿CREATE TABLE [dbo].[sHAConsultationDuration](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[TimeValue] [int] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dReportDefinition](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ViewDef] [varchar](1024) NOT NULL,
	[LocalDescriptionID] [bigint] NULL,
	[Description] [varchar](1024) NULL,
	[Header] [varchar](255) NULL,
	[LocalHeaderID] [varchar](255) NULL
) ON [PRIMARY]
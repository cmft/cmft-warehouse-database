﻿CREATE TABLE [dbo].[dContraceptionSideEffects](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[ContraceptionHistoryID] [bigint] NULL,
	[SideEffectID] [bigint] NULL
) ON [PRIMARY]
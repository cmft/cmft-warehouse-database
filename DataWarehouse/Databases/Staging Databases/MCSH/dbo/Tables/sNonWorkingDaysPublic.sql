﻿CREATE TABLE [dbo].[sNonWorkingDaysPublic](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[NonWorkingDayTypeID] [bigint] NULL,
	[SessionTypeID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
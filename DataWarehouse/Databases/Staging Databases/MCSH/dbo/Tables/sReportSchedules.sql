﻿CREATE TABLE [dbo].[sReportSchedules](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[ReportType] [varchar](10) NULL,
	[JobName] [varchar](200) NULL,
	[ScheduleDate] [smalldatetime] NULL,
	[ScheduleTime] [varchar](10) NULL,
	[FinishDateTime] [smalldatetime] NULL,
	[SearchFilter] [varchar](50) NULL,
	[ExportDetails] [varchar](5000) NULL,
	[CollectionDatetime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[SPParameters] [varchar](1024) NULL,
	[StoredProcedure] [varchar](1024) NULL,
	[JOB_ID] [uniqueidentifier] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sSmsProviders](
	[Password] [varchar](255) NOT NULL,
	[UserName] [varchar](255) NOT NULL,
	[SmsServer] [varchar](255) NULL,
	[SSlSmsServer] [varchar](255) NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_sSmsProviders_Enabled]  DEFAULT (0),
	[Description] [varchar](50) NULL,
	[WebProxyPort] [varchar](50) NULL,
	[WebProxyIP] [varchar](50) NULL,
	[sms_Location] [varchar](50) NULL,
	[sms_department] [varchar](50) NULL,
	[WebProxyUser] [varchar](255) NULL,
	[WebProxyPassword] [varchar](255) NULL,
	[WebProxyDomain] [varchar](255) NULL
) ON [PRIMARY]
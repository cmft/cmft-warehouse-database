﻿CREATE TABLE [dbo].[dImmunisationHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DateTime] [smalldatetime] NOT NULL,
	[ImmunisationID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
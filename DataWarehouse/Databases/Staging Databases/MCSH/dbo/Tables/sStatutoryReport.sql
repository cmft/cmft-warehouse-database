﻿CREATE TABLE [dbo].[sStatutoryReport](
	[ID] [bigint] NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[ModuleFlags] [bigint] NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sGPPractise](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[PostCode] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
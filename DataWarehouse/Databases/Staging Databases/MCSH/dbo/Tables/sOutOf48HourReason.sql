﻿CREATE TABLE [dbo].[sOutOf48HourReason](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
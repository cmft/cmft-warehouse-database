﻿CREATE TABLE [dbo].[sInvestigationPanelTemplate](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[HPACode] [varchar](10) NULL
) ON [PRIMARY]
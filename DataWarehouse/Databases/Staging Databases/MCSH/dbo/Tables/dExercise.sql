﻿CREATE TABLE [dbo].[dExercise](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[ExerciseLevelID] [bigint] NULL,
	[ExerciseTypeID] [bigint] NULL,
	[ExerciseDate] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dDOGSHistoryItem](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOGSHistoryID] [bigint] NULL,
	[DOGSEntryID] [bigint] NULL,
	[LetterID] [bigint] NULL,
	[Complete] [bit] NULL
) ON [PRIMARY]
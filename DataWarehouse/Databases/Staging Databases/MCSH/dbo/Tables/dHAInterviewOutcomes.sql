﻿CREATE TABLE [dbo].[dHAInterviewOutcomes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[HAInterviewID] [bigint] NULL,
	[OutcomeID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sChlamydiaTypeOfTest](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[Code] [bigint] NULL,
	[Position] [float] NULL
) ON [PRIMARY]
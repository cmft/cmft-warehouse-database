﻿CREATE TABLE [dbo].[dPsychiatricHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DateOfReferral] [smalldatetime] NOT NULL,
	[LocationID] [bigint] NOT NULL,
	[ReasonForReferralID] [bigint] NULL,
	[SourceOfReferralID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
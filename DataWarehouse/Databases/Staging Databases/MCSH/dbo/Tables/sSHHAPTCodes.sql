﻿CREATE TABLE [dbo].[sSHHAPTCodes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SHHAPTCode] [varchar](50) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Category] [varchar](50) NULL,
	[LevelFlag] [int] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
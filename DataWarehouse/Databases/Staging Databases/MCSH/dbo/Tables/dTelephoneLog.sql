﻿CREATE TABLE [dbo].[dTelephoneLog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CallDirectionID] [bigint] NULL,
	[CallDate] [datetime] NULL,
	[StartTime] [int] NULL,
	[EndTime] [int] NULL,
	[EstimatedCallTimeID] [bigint] NULL,
	[PatientID] [bigint] NULL,
	[OriginalName] [varchar](50) NULL,
	[OriginalDOB] [datetime] NULL,
	[CallTypeID] [bigint] NULL,
	[CollectionDateTime] [datetime] NULL,
	[UserID] [bigint] NULL,
	[Comments] [text] NULL,
	[TempID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[sGPDetails](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](1024) NULL,
	[LastName] [varchar](1024) NULL,
	[Title] [bigint] NOT NULL,
	[PractiseID] [bigint] NULL,
	[GPNumber] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
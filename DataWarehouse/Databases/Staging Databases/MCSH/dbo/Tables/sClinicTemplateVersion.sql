﻿CREATE TABLE [dbo].[sClinicTemplateVersion](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[StartTime] [int] NULL,
	[EndTime] [int] NULL,
	[SexID] [bigint] NULL,
	[ClinicTemplateID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[Archived] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
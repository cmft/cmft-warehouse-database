﻿CREATE TABLE [dbo].[sAppointment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ClinicListID] [bigint] NULL,
	[StartTime] [bigint] NULL,
	[Searchable] [bit] NULL,
	[AppointmentSessionTypeID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
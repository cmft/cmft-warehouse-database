﻿CREATE TABLE [dbo].[dHIVTests](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[TestDate] [smalldatetime] NULL,
	[OutcomeID] [bigint] NULL,
	[LocationID] [bigint] NULL,
	[SourceOfReferralID] [bigint] NULL,
	[ReasonForReferralID] [bigint] NULL,
	[UserID] [bigint] NULL,
	[ReturnedForResult] [bit] NULL
) ON [PRIMARY]
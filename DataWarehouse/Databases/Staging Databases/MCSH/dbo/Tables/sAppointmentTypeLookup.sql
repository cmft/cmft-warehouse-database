﻿CREATE TABLE [dbo].[sAppointmentTypeLookup](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SiteAptTypeID] [bigint] NULL,
	[SystemAptTypeID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
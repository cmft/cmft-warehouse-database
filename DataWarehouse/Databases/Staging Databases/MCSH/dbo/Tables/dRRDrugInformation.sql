﻿CREATE TABLE [dbo].[dRRDrugInformation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[VircoID] [varchar](50) NULL,
	[SampleID] [varchar](50) NULL,
	[SpecimenDateTime] [smalldatetime] NULL,
	[ReceivedDateTime] [smalldatetime] NULL,
	[ReportDateTime] [smalldatetime] NULL,
	[ClinicalInformation] [varchar](255) NULL,
	[SpecimenType] [varchar](50) NULL,
	[OrderingProviderID] [bigint] NULL,
	[AnalysedSequenceRegionPR] [varchar](50) NULL,
	[AnalysedSequenceRegionRT] [varchar](50) NULL,
	[Clade] [varchar](50) NULL,
	[PRMutations] [varchar](1024) NULL,
	[RTMutations] [varchar](1024) NULL,
	[CollectionDateTime] [smalldatetime] NULL
) ON [PRIMARY]
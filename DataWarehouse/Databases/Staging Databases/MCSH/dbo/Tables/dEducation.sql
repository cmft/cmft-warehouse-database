﻿CREATE TABLE [dbo].[dEducation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DietAssessmentID] [bigint] NULL,
	[EducationPublicationID] [bigint] NOT NULL,
	[EducationDate] [smalldatetime] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sComplications](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[ComplicationTypeID] [bigint] NULL,
	[SingleOccurance] [bit] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
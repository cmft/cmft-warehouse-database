﻿CREATE TABLE [dbo].[dReproductiveHistoryProblems](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[ReproductiveHistoryID] [bigint] NULL,
	[ReproductiveHistoryProblemID] [bigint] NULL
) ON [PRIMARY]
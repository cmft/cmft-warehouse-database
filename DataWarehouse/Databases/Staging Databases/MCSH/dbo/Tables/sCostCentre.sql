﻿CREATE TABLE [dbo].[sCostCentre](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Code] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
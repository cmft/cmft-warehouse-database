﻿CREATE TABLE [dbo].[dReferral](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[DateTime] [smalldatetime] NOT NULL,
	[ReferralTypeID] [bigint] NULL,
	[ClassificationID] [bigint] NULL,
	[StatusID] [bigint] NULL,
	[ReferredByID] [bigint] NULL,
	[ReasonForReferralID] [bigint] NULL,
	[ReferredToID] [bigint] NULL,
	[Notes] [text] NULL,
	[CollectionDatetime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Actioned] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[dDrugSideEffects](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[dDrugID] [bigint] NULL,
	[DrugSideEffectsID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
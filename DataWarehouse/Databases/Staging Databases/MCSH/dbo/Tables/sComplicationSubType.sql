﻿CREATE TABLE [dbo].[sComplicationSubType](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[ComplicationID] [bigint] NULL,
	[ICD10Code] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sReleaseVersion](
	[ID] [tinyint] NOT NULL,
	[Description] [varchar](100) NULL,
	[Major] [varchar](50) NULL,
	[Minor] [varchar](50) NULL,
	[Release] [varchar](50) NULL,
	[Revision] [varchar](50) NULL,
	[RelTimeStamp] [timestamp] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dLineDrawing](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[LineDrawingID] [bigint] NULL,
	[CollectionDateTime] [datetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dAudit](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[IPAddress] [varchar](19) NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[ActionID] [bigint] NULL,
	[PatientID] [bigint] NULL,
	[Message] [varchar](1024) NULL,
	[IdentityKey] [bigint] NULL
) ON [PRIMARY]
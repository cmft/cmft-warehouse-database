﻿CREATE TABLE [dbo].[sPrinters](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Port] [varchar](1024) NULL,
	[Driver] [varchar](1024) NULL,
	[DeviceName] [varchar](1024) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
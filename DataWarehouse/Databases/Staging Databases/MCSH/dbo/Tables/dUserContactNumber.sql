﻿CREATE TABLE [dbo].[dUserContactNumber](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactTypeID] [bigint] NOT NULL,
	[Number] [varchar](50) NOT NULL,
	[NonUrgentContact] [bit] NULL,
	[ContactUserID] [bigint] NOT NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
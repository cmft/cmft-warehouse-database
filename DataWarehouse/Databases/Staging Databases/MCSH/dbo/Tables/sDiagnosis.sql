﻿CREATE TABLE [dbo].[sDiagnosis](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NULL,
	[Enabled] [bit] NULL,
	[Code] [varchar](1024) NULL,
	[KC60Map] [varchar](1024) NULL,
	[HIVDiagnosis] [bit] NULL,
	[SingleOccurance] [bit] NULL,
	[HIVStageID] [bigint] NULL,
	[RecallDays] [int] NULL,
	[RecallResourceTypeID] [bigint] NULL,
	[ClinicalSignificance] [int] NULL
) ON [PRIMARY]
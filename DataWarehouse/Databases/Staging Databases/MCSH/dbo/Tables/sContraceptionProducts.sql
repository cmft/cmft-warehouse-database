﻿CREATE TABLE [dbo].[sContraceptionProducts](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[ContraceptionTypeID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
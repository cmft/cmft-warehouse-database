﻿CREATE TABLE [dbo].[dClinicInvestigations](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [bigint] NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[InvestigationTemplateID] [bigint] NULL,
	[PanelTemplateID] [bigint] NULL,
	[UserID] [bigint] NOT NULL,
	[Complete] [bit] NULL,
	[Cancelled] [bit] NULL,
	[DeclinedByClient] [bit] NULL,
	[Comments] [varchar](1024) NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sUserSiteLocation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[SiteLocationID] [bigint] NULL,
	[Position] [bigint] NULL,
	[Checked] [bit] NULL,
	[Enabled] [bit] NULL,
	[RRAlerts] [bit] NULL
) ON [PRIMARY]
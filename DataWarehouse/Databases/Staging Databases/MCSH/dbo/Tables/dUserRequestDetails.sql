﻿CREATE TABLE [dbo].[dUserRequestDetails](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[ConnectionGroupID] [bigint] NULL,
	[ConsultantCode] [varchar](20) NULL,
	[Speciality] [varchar](20) NULL,
	[SpecialityDescription] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dCarePlan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[CarePlanID] [bigint] NOT NULL,
	[Comment] [varchar](50) NULL,
	[DateSet] [smalldatetime] NULL,
	[UserID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
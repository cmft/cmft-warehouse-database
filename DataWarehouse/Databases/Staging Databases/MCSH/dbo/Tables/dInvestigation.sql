﻿CREATE TABLE [dbo].[dInvestigation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[InvestigationPanelID] [bigint] NOT NULL,
	[ProviderLocationID] [bigint] NULL,
	[ProviderResourceID] [bigint] NULL,
	[ProviderComment] [text] NULL,
	[InvestigationTemplateID] [bigint] NOT NULL,
	[Complete] [bit] NULL,
	[Cancelled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[DeclinedByClient] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
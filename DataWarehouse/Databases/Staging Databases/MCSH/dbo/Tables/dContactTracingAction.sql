﻿CREATE TABLE [dbo].[dContactTracingAction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactTracingID] [bigint] NOT NULL,
	[ContactTracingActionID] [bigint] NOT NULL,
	[ActionDate] [smalldatetime] NOT NULL,
	[Comments] [varchar](1024) NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
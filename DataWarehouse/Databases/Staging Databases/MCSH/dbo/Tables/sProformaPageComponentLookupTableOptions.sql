﻿CREATE TABLE [dbo].[sProformaPageComponentLookupTableOptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProformaPageComponentLookupTableID] [bigint] NULL,
	[OptionID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
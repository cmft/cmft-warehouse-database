﻿CREATE TABLE [dbo].[dDOGSEntry](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOGSID] [bigint] NULL,
	[PATIENTID] [bigint] NULL,
	[APPTID] [bigint] NULL,
	[LETTERID] [bigint] NULL,
	[ENABLED] [bit] NULL,
	[COLLECTIONDATETIME] [smalldatetime] NULL,
	[USERID] [bigint] NULL,
	[ITEMID] [bigint] NULL,
	[ITEMDUEDATE] [datetime] NULL,
	[ItemDetails] [varchar](1024) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dChiropodyUlcerationHistory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[AssessmentDate] [smalldatetime] NOT NULL,
	[SiteID] [bigint] NOT NULL,
	[Current] [bit] NOT NULL,
	[EndDate] [char](10) NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
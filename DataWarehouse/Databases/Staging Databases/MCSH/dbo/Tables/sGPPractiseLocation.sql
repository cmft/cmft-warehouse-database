﻿CREATE TABLE [dbo].[sGPPractiseLocation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Address] [varchar](1024) NOT NULL,
	[PostCode] [varchar](50) NOT NULL,
	[ContactNo] [varchar](50) NOT NULL,
	[PractiseID] [bigint] NOT NULL
) ON [PRIMARY]
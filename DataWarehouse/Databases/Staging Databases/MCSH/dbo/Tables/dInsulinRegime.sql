﻿CREATE TABLE [dbo].[dInsulinRegime](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[InsulinTypeID] [bigint] NOT NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NULL,
	[ReasonForStopID] [bigint] NULL,
	[Enabled] [bit] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL
) ON [PRIMARY]
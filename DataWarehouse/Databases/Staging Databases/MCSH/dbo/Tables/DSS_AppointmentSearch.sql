﻿CREATE TABLE [dbo].[DSS_AppointmentSearch](
	[AptIds] [bigint] NOT NULL,
	[ClinicDescription] [varchar](100) NULL,
	[ClinicTemplateID] [bigint] NULL,
	[ClinicListID] [bigint] NULL,
	[ResourceID] [bigint] NULL,
	[SexID] [bigint] NULL,
	[ClinicListDescription] [varchar](100) NULL,
	[DateTime] [smalldatetime] NULL,
	[StartTime] [int] NULL,
	[EndTime] [int] NULL,
	[SessionTypeId] [bigint] NULL,
	[SessionType] [varchar](50) NULL
) ON [PRIMARY]
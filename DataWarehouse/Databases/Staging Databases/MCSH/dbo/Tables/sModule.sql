﻿CREATE TABLE [dbo].[sModule](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Module] [varchar](12) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[BitMask] [int] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
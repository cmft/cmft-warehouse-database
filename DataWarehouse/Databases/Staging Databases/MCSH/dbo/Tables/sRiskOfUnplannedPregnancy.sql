﻿CREATE TABLE [dbo].[sRiskOfUnplannedPregnancy](
	[ID] [tinyint] NOT NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
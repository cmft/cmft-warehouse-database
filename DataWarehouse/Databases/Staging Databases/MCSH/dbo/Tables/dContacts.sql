﻿CREATE TABLE [dbo].[dContacts](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[ContactID] [bigint] NULL,
	[Name] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[Postcode] [varchar](50) NULL,
	[AwareOfHIVStatusID] [bigint] NULL,
	[Comment] [text] NULL,
	[CollectionDateTime] [datetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[sOutputOptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OutputDefinitionID] [bigint] NULL,
	[OutputGroupID] [bigint] NULL,
	[OutputSelectionID] [bigint] NULL,
	[SelectionUserPrompt] [varchar](50) NULL,
	[Prefix] [varchar](50) NULL
) ON [PRIMARY]
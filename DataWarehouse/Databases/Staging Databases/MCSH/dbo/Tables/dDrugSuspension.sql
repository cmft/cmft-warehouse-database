﻿CREATE TABLE [dbo].[dDrugSuspension](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dDrugID] [bigint] NOT NULL,
	[SuspensionStartDate] [smalldatetime] NOT NULL,
	[SuspensionStopDate] [smalldatetime] NULL,
	[ReasonForSuspensionID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sUserOptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[UserOptions] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dHeight](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[Height] [varchar](50) NULL,
	[MeasurementDate] [smalldatetime] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
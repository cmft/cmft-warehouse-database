﻿CREATE TABLE [dbo].[sDrugToRoute](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DrugID] [bigint] NOT NULL,
	[RouteID] [bigint] NOT NULL
) ON [PRIMARY]
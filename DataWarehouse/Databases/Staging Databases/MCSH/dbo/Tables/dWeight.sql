﻿CREATE TABLE [dbo].[dWeight](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[weight] [varchar](50) NOT NULL,
	[MeasurementDate] [smalldatetime] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
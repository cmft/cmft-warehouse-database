﻿CREATE TABLE [dbo].[sNationalCountryCodes](
	[ID] [smallint] IDENTITY(1,1) NOT NULL,
	[CountryName] [varchar](50) NOT NULL,
	[CountryCode2] [char](2) NOT NULL,
	[CountryCode3] [char](3) NOT NULL
) ON [PRIMARY]
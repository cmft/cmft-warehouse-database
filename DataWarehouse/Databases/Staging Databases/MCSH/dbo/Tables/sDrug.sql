﻿CREATE TABLE [dbo].[sDrug](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[DrugTypeID] [bigint] NULL,
	[ClinicalSignificance] [bit] NULL,
	[Enabled] [bit] NULL,
	[DrugGroup] [varchar](50) NULL,
	[DrugTradeName] [varchar](50) NULL,
	[GenericDrugName] [varchar](50) NULL,
	[DrugAcronym] [varchar](50) NULL,
	[DrugCount] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dUserMenuGroup](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserMenuID] [bigint] NULL,
	[Description] [varchar](1024) NOT NULL,
	[Position] [bigint] NOT NULL,
	[Enabled] [bit] NULL,
	[Expanded] [bit] NULL
) ON [PRIMARY]
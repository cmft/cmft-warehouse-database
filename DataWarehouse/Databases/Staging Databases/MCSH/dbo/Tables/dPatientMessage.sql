﻿CREATE TABLE [dbo].[dPatientMessage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[DisplayDate] [smalldatetime] NOT NULL,
	[ResourceTypeID] [bigint] NOT NULL,
	[Message] [varchar](1024) NOT NULL,
	[Actioned] [bit] NULL,
	[ActionDate] [smalldatetime] NULL,
	[Enabled] [bit] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dSmearTest](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[TestDate] [smalldatetime] NOT NULL,
	[ResourceID] [bigint] NULL,
	[OutcomeSmearTestID] [bigint] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL,
	[SmearTestReasonID] [bigint] NULL,
	[Comment] [varchar](1024) NULL
) ON [PRIMARY]
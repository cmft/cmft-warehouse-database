﻿CREATE TABLE [dbo].[sComplicationTreatment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ComplicationID] [bigint] NULL,
	[Description] [varchar](1024) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
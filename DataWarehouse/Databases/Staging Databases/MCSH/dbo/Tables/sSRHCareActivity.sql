﻿CREATE TABLE [dbo].[sSRHCareActivity](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[GroupID] [bigint] NULL,
	[Code] [int] NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
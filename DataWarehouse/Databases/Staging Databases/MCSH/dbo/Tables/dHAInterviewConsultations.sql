﻿CREATE TABLE [dbo].[dHAInterviewConsultations](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[HAInterviewID] [bigint] NOT NULL,
	[ConsultationID] [bigint] NOT NULL,
	[Response] [varchar](50) NULL
) ON [PRIMARY]
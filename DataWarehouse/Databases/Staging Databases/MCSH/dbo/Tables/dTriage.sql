﻿CREATE TABLE [dbo].[dTriage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TriageNumber] [bigint] NULL,
	[TriageDate] [datetime] NULL,
	[TriageTime] [int] NULL,
	[OriginalName] [varchar](1024) NULL,
	[OriginalDOB] [datetime] NULL,
	[PatientID] [bigint] NULL,
	[DetailsOfAttendance] [text] NULL,
	[ContactSlip] [bit] NULL,
	[DiagnosisID] [bigint] NULL,
	[ReferralLetter] [bit] NULL,
	[ReferralTypeID] [bigint] NULL,
	[FurtherAdvice] [text] NULL,
	[CollectionDateTime] [datetime] NULL,
	[UserID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
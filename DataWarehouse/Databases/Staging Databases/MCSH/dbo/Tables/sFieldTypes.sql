﻿CREATE TABLE [dbo].[sFieldTypes](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[FIELDTYPE] [varchar](255) NULL,
	[ENABLED] [bit] NULL
) ON [PRIMARY]
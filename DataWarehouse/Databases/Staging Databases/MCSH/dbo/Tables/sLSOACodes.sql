﻿CREATE TABLE [dbo].[sLSOACodes](
	[PostCode] [char](8) NOT NULL,
	[LSOA] [char](9) NOT NULL,
	[PCT] [char](3) NOT NULL,
	[PCNoSpace] [varchar](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dChiropodyUlceration](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ChiropodyFootExamID] [bigint] NULL,
	[ChiropodyUlcerationID] [bigint] NULL
) ON [PRIMARY]
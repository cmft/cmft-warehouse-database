﻿CREATE TABLE [dbo].[dSqlFilters](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[SqlFilter] [varchar](3072) NOT NULL,
	[SqlHeader] [varchar](100) NOT NULL,
	[SqlDescription] [varchar](1024) NOT NULL,
	[AdminCreated] [bit] NOT NULL CONSTRAINT [DF_dSqlFilters_AdminCreated]  DEFAULT (0),
	[ProcParams] [varchar](3072) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dChiropodyFootExamAnnotation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FootExamID] [bigint] NOT NULL,
	[DivID] [bigint] NOT NULL,
	[ImageID] [bigint] NULL,
	[XOffSet] [int] NULL,
	[YOffSet] [int] NULL,
	[CollectiondateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sTarget](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[IdealValue] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
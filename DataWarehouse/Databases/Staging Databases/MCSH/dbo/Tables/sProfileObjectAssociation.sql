﻿CREATE TABLE [dbo].[sProfileObjectAssociation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ParentProfileObjectID] [bigint] NULL,
	[ChildProfileObjectID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dChiropodyFootExamAmputation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ChiropodyFootExamID] [bigint] NULL,
	[ChiropodyAmputationID] [bigint] NULL
) ON [PRIMARY]
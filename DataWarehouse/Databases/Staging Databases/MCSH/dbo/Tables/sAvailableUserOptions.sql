﻿CREATE TABLE [dbo].[sAvailableUserOptions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[ConstantValue] [bigint] NULL
) ON [PRIMARY]
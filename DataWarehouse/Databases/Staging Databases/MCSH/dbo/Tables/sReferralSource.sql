﻿CREATE TABLE [dbo].[sReferralSource](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[HPACode] [varchar](10) NULL
) ON [PRIMARY]
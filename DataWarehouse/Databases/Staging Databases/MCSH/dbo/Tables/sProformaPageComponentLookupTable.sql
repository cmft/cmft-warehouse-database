﻿CREATE TABLE [dbo].[sProformaPageComponentLookupTable](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProfileObjectLookupTableID] [bigint] NULL,
	[ProformaPageComponentID] [bigint] NULL
) ON [PRIMARY]
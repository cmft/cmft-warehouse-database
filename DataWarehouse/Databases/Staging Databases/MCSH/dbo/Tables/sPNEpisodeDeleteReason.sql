﻿CREATE TABLE [dbo].[sPNEpisodeDeleteReason](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
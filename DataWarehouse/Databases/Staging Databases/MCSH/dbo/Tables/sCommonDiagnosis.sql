﻿CREATE TABLE [dbo].[sCommonDiagnosis](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DiagnosisID] [bigint] NULL,
	[ICD10ID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
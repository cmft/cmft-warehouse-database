﻿CREATE TABLE [dbo].[dInvestigationRequest](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NOT NULL,
	[RequestorResourceID] [bigint] NULL,
	[RequestorLocationID] [bigint] NULL,
	[RequestorComment] [text] NULL,
	[RequestDate] [smalldatetime] NOT NULL,
	[PatientToReturn] [bit] NOT NULL,
	[ProviderLocationID] [bigint] NULL,
	[ProviderResourceID] [bigint] NULL,
	[Enabled] [bit] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[UserID] [bigint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
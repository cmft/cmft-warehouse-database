﻿CREATE TABLE [dbo].[sOCLabelFormatLevels](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) NULL,
	[FontName] [varchar](50) NULL,
	[FontSize] [tinyint] NULL,
	[FontColour] [varchar](50) NULL,
	[FontStyle] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sContactTracingAction](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
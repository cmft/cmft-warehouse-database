﻿CREATE TABLE [dbo].[dPathProfile](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[ProfileID] [bigint] NULL,
	[RequestingConsultantID] [bigint] NULL,
	[LocationID] [bigint] NULL,
	[SpecimenReceivedDateTime] [smalldatetime] NULL,
	[SpecimenCollectionDateTime] [smalldatetime] NULL,
	[LabReference] [varchar](255) NULL,
	[Comment] [text] NULL,
	[CollectionDateTime] [smalldatetime] NULL,
	[UserID] [bigint] NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
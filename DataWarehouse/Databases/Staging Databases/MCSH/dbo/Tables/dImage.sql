﻿CREATE TABLE [dbo].[dImage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ImageTypeID] [bigint] NULL,
	[PatientID] [bigint] NULL,
	[ImageSetID] [bigint] NULL,
	[Title] [varchar](1024) NULL,
	[Comment] [varchar](1024) NULL,
	[SourceName] [varchar](1024) NULL,
	[ContentType] [varchar](1024) NULL,
	[ImageBLOB] [image] NULL,
	[UserID] [bigint] NOT NULL,
	[CollectionDateTime] [smalldatetime] NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
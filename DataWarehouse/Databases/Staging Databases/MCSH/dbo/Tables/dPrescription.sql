﻿CREATE TABLE [dbo].[dPrescription](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientID] [bigint] NULL,
	[ConsultantID] [bigint] NULL,
	[Weight] [varchar](50) NULL,
	[SurfaceArea] [varchar](50) NULL,
	[PrescriptionDate] [smalldatetime] NULL,
	[CostCentreID] [bigint] NULL,
	[StatusID] [bigint] NULL,
	[CheckedByID] [bigint] NULL,
	[CounselledByID] [bigint] NULL,
	[ContactID] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[sChlamydiaSiteCode](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Description] [varchar](255) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
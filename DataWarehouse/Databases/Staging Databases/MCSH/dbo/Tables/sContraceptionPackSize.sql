﻿CREATE TABLE [dbo].[sContraceptionPackSize](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ContraceptionProductID] [bigint] NULL,
	[Description] [varchar](50) NULL,
	[Enabled] [bit] NULL
) ON [PRIMARY]
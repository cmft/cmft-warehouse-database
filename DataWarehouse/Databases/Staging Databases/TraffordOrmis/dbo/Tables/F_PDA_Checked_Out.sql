﻿CREATE TABLE [dbo].[F_PDA_Checked_Out](
	[PDCO_BOOKING_SEQU] [numeric](9, 0) NOT NULL,
	[PDCO_USER_SEQU] [numeric](9, 0) NOT NULL,
	[PDCO_CHECK_OUT_TIME] [datetime] NULL,
	[PDCO_PDA_NAME] [varchar](50) NULL
) ON [PRIMARY]
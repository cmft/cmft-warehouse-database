﻿CREATE TABLE [dbo].[F_RecOrdTrans](
	[ROT_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[ROT_ROI_SEQU] [numeric](9, 0) NOT NULL,
	[ROT_OI_SEQU] [numeric](9, 0) NOT NULL,
	[ROT_SEQU] [numeric](9, 0) NOT NULL,
	[ROT_QTY] [numeric](9, 0) NOT NULL,
	[ROT_BATCH_NUM] [varchar](50) NULL,
	[ROT_EXPIRY_DATE] [datetime] NULL,
	[ROT_LOG_DATE] [datetime] NULL,
	[ROT_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
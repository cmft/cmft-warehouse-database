﻿CREATE TABLE [dbo].[F_Contact_Details](
	[CDE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CDE_CO_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CDE_PRAC_CODE] [varchar](20) NULL,
	[CDE_STREET_1] [varchar](60) NULL,
	[CDE_STREET_2] [varchar](60) NULL,
	[CDE_SUBURB] [varchar](60) NULL,
	[CDE_CITY] [varchar](60) NULL,
	[CDE_STATE] [varchar](12) NULL,
	[CDE_PHONE] [varchar](25) NULL,
	[CDE_FAX] [varchar](25) NULL,
	[CDE_EMAIL] [varchar](80) NULL,
	[CDE_HA] [varchar](20) NULL,
	[CDE_POSTCODE] [varchar](12) NULL,
	[CDE_FINISH_DATE] [datetime] NULL
) ON [PRIMARY]
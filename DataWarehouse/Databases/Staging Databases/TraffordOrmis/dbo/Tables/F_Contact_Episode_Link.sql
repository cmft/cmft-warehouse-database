﻿CREATE TABLE [dbo].[F_Contact_Episode_Link](
	[CEL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CEL_EPS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CEL_CO_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CEL_CDE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CEL_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CEL_NAME] [varchar](100) NULL,
	[CEL_STREET_1] [varchar](60) NULL,
	[CEL_STREET_2] [varchar](60) NULL,
	[CEL_SUBURB] [varchar](60) NULL,
	[CEL_CITY] [varchar](60) NULL,
	[CEL_STATE] [varchar](12) NULL,
	[CEL_POSTCODE] [varchar](12) NULL,
	[CEL_PHONE] [varchar](25) NULL,
	[CEL_FAX] [varchar](25) NULL,
	[CEL_EMAIL] [varchar](80) NULL,
	[CEL_HA] [varchar](20) NULL
) ON [PRIMARY]
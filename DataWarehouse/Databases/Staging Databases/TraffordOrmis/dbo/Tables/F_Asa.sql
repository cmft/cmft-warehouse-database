﻿CREATE TABLE [dbo].[F_Asa](
	[ASA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ASA_CODE] [varchar](2) NULL,
	[ASA_DESCRIPTION] [varchar](255) NULL,
	[ASA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ASA_LOG_DATE] [datetime] NULL,
	[ASA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ASA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
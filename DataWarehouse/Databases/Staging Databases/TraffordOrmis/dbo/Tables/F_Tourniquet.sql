﻿CREATE TABLE [dbo].[F_Tourniquet](
	[TO_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TO_SEQU] [numeric](9, 0) NOT NULL,
	[TO_OP_SEQU] [numeric](9, 0) NOT NULL,
	[TO_DATE_TIME_ON] [datetime] NULL,
	[TO_DATE_TIME_OFF] [datetime] NULL,
	[TO_LOG_DATE] [datetime] NULL,
	[TO_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[TO_BS_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
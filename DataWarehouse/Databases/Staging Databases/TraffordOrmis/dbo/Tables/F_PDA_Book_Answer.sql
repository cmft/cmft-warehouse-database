﻿CREATE TABLE [dbo].[F_PDA_Book_Answer](
	[PBA_SEQU] [numeric](9, 0) NOT NULL,
	[PBA_PBQ_SEQU] [numeric](9, 0) NOT NULL,
	[PBA_PA_SEQU] [numeric](9, 0) NOT NULL,
	[PBA_PDQ_SEQU] [numeric](9, 0) NOT NULL,
	[PBA_SET_NO] [numeric](3, 0) NOT NULL,
	[PBA_ANSWER_1] [varchar](255) NULL,
	[PBA_ANSWER_2] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Religion](
	[RL_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RL_CODE] [varchar](5) NULL,
	[RL_DESCRIPTION] [varchar](60) NULL,
	[RL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RL_LOG_DATE] [datetime] NULL,
	[RL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[RL_EXT_CODE] [varchar](5) NULL,
	[RL_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RL_EXTRACT_CODE] [varchar](5) NULL,
	[RL_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
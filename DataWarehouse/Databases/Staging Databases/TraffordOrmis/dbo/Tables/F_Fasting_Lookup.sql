﻿CREATE TABLE [dbo].[F_Fasting_Lookup](
	[FL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[FL_CODE] [varchar](5) NULL,
	[FL_DESCRIPTION] [varchar](60) NULL,
	[FL_INACTIVE] [numeric](3, 0) NOT NULL,
	[FL_DETAILS] [text] NULL,
	[FL_LOG_DATE] [datetime] NULL,
	[FL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[FL_SEQU] [numeric](9, 0) NOT NULL,
	[FL_DEFAULT] [numeric](3, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
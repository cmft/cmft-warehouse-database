﻿CREATE TABLE [dbo].[F_Relation](
	[RS_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RS_CODE] [varchar](5) NULL,
	[RS_DESCRIPTION] [varchar](60) NULL,
	[RS_LOG_DATE] [datetime] NULL,
	[RS_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[RS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RS_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RS_INC_ACTIVITY] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RS_EXTRACT_CODE] [varchar](5) NULL,
	[RS_EXT_CODE] [varchar](5) NULL,
	[RS_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RS_PRIMARY_NOK] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Equip_Conflict](
	[EQC_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_CONFLICT1_TYPE] [numeric](3, 0) NOT NULL,
	[EQC_CONFLICT1_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_CONFLICT2_TYPE] [numeric](3, 0) NOT NULL,
	[EQC_CONFLICT2_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_REASON_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_STAFF_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_REASON_TEXT] [varchar](255) NULL
) ON [PRIMARY]
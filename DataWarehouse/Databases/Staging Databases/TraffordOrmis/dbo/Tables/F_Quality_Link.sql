﻿CREATE TABLE [dbo].[F_Quality_Link](
	[QLYL_SEQU] [numeric](9, 0) NOT NULL,
	[QLYL_PAD_SEQU] [numeric](9, 0) NOT NULL,
	[QLYL_QLY_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
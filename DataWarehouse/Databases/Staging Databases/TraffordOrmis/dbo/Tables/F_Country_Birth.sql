﻿CREATE TABLE [dbo].[F_Country_Birth](
	[CB_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CB_CODE] [varchar](5) NULL,
	[CB_DESCRIPTION] [varchar](60) NULL,
	[CB_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CB_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CB_LOG_DATE] [datetime] NULL,
	[CB_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[CB_EXT_CODE] [varchar](5) NULL,
	[CB_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CB_EXTRACT_CODE] [varchar](5) NULL,
	[CB_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
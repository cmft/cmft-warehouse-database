﻿CREATE TABLE [dbo].[F_Airway_Link](
	[AIRL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AIRL_PAD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AIRL_AIR_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
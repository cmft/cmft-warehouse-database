﻿CREATE TABLE [dbo].[F_Gauge](
	[GA_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[GA_SEQU] [numeric](9, 0) NOT NULL,
	[GA_CODE] [varchar](5) NULL,
	[GA_DESCRIPTION] [varchar](60) NULL,
	[GA_INACTIVE] [numeric](3, 0) NOT NULL,
	[GA_LOG_DATE] [datetime] NULL,
	[GA_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
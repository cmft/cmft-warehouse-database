﻿CREATE TABLE [dbo].[F_Op_Equipment](
	[OPEQ_SEQU] [numeric](9, 0) NOT NULL,
	[OPEQ_EQI_SEQU] [numeric](9, 0) NOT NULL,
	[OPEQ_OP_SEQU] [numeric](9, 0) NOT NULL,
	[OPEQ_IMS_PRODUCTION_ID] [numeric](11, 0) NOT NULL,
	[OPEQ_NON_CONF_FLAG] [numeric](3, 0) NOT NULL,
	[OPEQ_NON_CONF_REASON] [varchar](75) NULL,
	[OPEQ_EQP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
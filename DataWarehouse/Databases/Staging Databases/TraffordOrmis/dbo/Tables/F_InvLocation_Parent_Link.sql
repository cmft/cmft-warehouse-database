﻿CREATE TABLE [dbo].[F_InvLocation_Parent_Link](
	[ILPL_IL_CHILD_SEQU] [numeric](9, 0) NOT NULL,
	[ILPL_IL_PARENT_SEQU] [numeric](9, 0) NOT NULL,
	[ILPL_DISTANCE] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
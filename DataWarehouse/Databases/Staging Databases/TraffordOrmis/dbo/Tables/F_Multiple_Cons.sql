﻿CREATE TABLE [dbo].[F_Multiple_Cons](
	[MC_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MC_SS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MC_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MC_PRIMARY] [numeric](3, 0) NOT NULL DEFAULT (0),
	[MC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Preadmission_Template](
	[PAT_SEQU] [numeric](9, 0) NOT NULL,
	[PAT_NAME] [varchar](60) NULL,
	[PAT_TEXT] [text] NULL,
	[PAT_INACTIVE] [numeric](3, 2) NOT NULL,
	[PAT_TEXT_OPTION] [numeric](9, 2) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
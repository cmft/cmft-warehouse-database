﻿CREATE TABLE [dbo].[F_Monitor_Link](
	[MONL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MONL_PAD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MONL_MON_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
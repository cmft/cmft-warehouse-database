﻿CREATE TABLE [dbo].[F_Spec_Tech](
	[SPT_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SPT_CODE] [varchar](5) NULL,
	[SPT_DESCRIPTION] [varchar](60) NULL,
	[SPT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SPT_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SPT_LOG_DATE] [datetime] NULL,
	[SPT_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
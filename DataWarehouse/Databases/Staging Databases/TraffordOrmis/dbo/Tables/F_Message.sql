﻿CREATE TABLE [dbo].[F_Message](
	[MSG_SEQU] [numeric](9, 0) NOT NULL,
	[MSG_MESSAGE] [varchar](1000) NULL,
	[MSG_RECEIPIENT_SEQU] [varchar](1000) NULL,
	[MSG_READ_BY] [varchar](1000) NULL,
	[MSG_TYPE] [numeric](3, 0) NOT NULL,
	[MSG_GEN_DATE_TIME] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Proc_Inv_Items](
	[PRI_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[PRI_II_SEQU] [numeric](9, 0) NOT NULL,
	[PRI_QTY] [numeric](9, 0) NOT NULL,
	[PRI_CD_SEQU] [numeric](9, 0) NOT NULL,
	[PRI_LOG_DATE] [datetime] NULL,
	[PRI_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PRI_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
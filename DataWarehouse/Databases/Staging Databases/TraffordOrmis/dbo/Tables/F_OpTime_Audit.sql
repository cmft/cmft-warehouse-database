﻿CREATE TABLE [dbo].[F_OpTime_Audit](
	[OTA_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[OTA_SU_SEQU] [numeric](9, 0) NOT NULL,
	[OTA_OP_SEQU] [numeric](9, 0) NOT NULL,
	[OTA_DATE_TIME] [datetime] NULL,
	[OTA_TYPE] [numeric](3, 0) NOT NULL,
	[OTA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
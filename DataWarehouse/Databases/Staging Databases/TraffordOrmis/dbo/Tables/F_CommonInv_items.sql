﻿CREATE TABLE [dbo].[F_CommonInv_items](
	[CI_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[CI_II_SEQU] [numeric](9, 0) NOT NULL,
	[CI_S1_SEQU] [numeric](9, 0) NOT NULL,
	[CI_UNIQ_ID] [varchar](15) NOT NULL
) ON [PRIMARY]
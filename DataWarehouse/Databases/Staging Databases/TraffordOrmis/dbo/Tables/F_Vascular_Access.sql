﻿CREATE TABLE [dbo].[F_Vascular_Access](
	[VAS_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[VAS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[VAS_CODE] [varchar](5) NULL,
	[VAS_DESCRIPTION] [varchar](60) NULL,
	[VAS_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[VAS_LOG_DATE] [datetime] NULL,
	[VAS_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
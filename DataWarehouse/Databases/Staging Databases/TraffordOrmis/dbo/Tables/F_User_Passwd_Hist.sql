﻿CREATE TABLE [dbo].[F_User_Passwd_Hist](
	[UPH_SEQN] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UPH_US_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UPH_PASSWORD] [varchar](50) NULL,
	[UPH_DATE_RECORD] [datetime] NULL,
	[UPH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
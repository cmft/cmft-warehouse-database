﻿CREATE TABLE [dbo].[F_Consult_Pros](
	[COP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[COP_SU_SEQU] [numeric](9, 0) NOT NULL,
	[COP_II_SEQU] [numeric](9, 0) NOT NULL,
	[COP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
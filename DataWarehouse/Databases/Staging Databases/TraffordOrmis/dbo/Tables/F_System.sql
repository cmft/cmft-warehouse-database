﻿CREATE TABLE [dbo].[F_System](
	[SYS_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SYS_NAME] [varchar](255) NOT NULL,
	[SYS_VALUE] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[FTHEAT](
	[TH_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TH_CODE] [varchar](8) NULL,
	[TH_DESC] [varchar](60) NULL,
	[TH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TH_SORT] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TH_LOG_DATE] [datetime] NULL,
	[TH_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[TH_UN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TH_LIST_CLOSURE] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TH_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[TH_UNCOD] [varchar](5) NULL,
	[TH_IL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
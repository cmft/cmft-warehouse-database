﻿CREATE TABLE [dbo].[F_Maintenance_Codes](
	[MAC_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MAC_CODE] [varchar](5) NULL,
	[MAC_DESCRIPTION] [varchar](30) NULL,
	[MAC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MAC_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
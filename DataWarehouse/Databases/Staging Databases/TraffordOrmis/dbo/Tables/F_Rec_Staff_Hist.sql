﻿CREATE TABLE [dbo].[F_Rec_Staff_Hist](
	[RSH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RSH_REC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RSH_PRIM_NURSE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RSH_SEC_NURSE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RSH_DATE_TIME] [datetime] NULL
) ON [PRIMARY]
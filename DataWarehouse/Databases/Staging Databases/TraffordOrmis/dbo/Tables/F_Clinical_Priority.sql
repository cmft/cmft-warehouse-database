﻿CREATE TABLE [dbo].[F_Clinical_Priority](
	[CLP_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CLP_CODE] [varchar](3) NULL,
	[CLP_DESCRIPTION] [varchar](60) NULL,
	[CLP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CLP_MAX_TIME] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CLP_LOG_DATE] [datetime] NULL,
	[CLP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[CLP_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_LettersGP](
	[LGP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[LGP_NAME] [varchar](50) NULL,
	[LGP_TEXT] [text] NULL,
	[LGP_LOG_DATE] [datetime] NULL,
	[LGP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[LGP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
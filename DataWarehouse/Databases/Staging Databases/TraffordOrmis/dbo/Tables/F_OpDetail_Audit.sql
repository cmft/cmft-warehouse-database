﻿CREATE TABLE [dbo].[F_OpDetail_Audit](
	[OA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[OA_LOG_DATE_TIME] [datetime] NULL,
	[OA_FILE_NAME] [varchar](25) NULL,
	[OA_RECORD_DETAIL] [text] NULL,
	[OA_USER] [varchar](25) NULL,
	[OA_MACHINE] [varchar](30) NULL,
	[OA_ACTION] [varchar](15) NULL,
	[OA_FIELD_NUMBER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[OA_WINDOW] [varchar](40) NULL,
	[OA_FILLER_01] [numeric](3, 0) NOT NULL DEFAULT (0),
	[OA_UPDATE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[OA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
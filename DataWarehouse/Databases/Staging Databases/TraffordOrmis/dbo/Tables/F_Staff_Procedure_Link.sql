﻿CREATE TABLE [dbo].[F_Staff_Procedure_Link](
	[SPL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SPL_STAFF_PROCEDURE] [varchar](10) NULL,
	[SPL_SU_SEQU] [numeric](9, 0) NOT NULL,
	[SPL_CD_SEQU] [numeric](9, 0) NOT NULL,
	[SPL_DATE] [datetime] NULL,
	[SPL_LOG_DATE] [datetime] NULL,
	[SPL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SPL_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
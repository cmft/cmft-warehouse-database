﻿CREATE TABLE [dbo].[F_StaffSuperv](
	[SSU_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SSU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SSU_CODE] [varchar](5) NULL,
	[SSU_DESC] [varchar](255) NULL,
	[SSU_SURGEON] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SSU_LOG_DATE] [datetime] NULL,
	[SSU_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SSU_ANAESTHETIST] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SSU_NURSING] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SSU_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SSU_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SSU_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SSU_TECH_1] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SSU_TECH_2] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
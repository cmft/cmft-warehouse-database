﻿CREATE TABLE [dbo].[F_Induction](
	[IND_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[IND_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[IND_CODE] [varchar](5) NULL,
	[IND_DESCRIPTION] [varchar](60) NULL,
	[IND_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[IND_LOG_DATE] [datetime] NULL,
	[IND_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
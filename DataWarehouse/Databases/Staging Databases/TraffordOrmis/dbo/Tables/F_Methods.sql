﻿CREATE TABLE [dbo].[F_Methods](
	[ME_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ME_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ME_CODE] [varchar](5) NULL,
	[ME_DESC] [varchar](60) NULL,
	[ME_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ME_LOG_DATE] [datetime] NULL,
	[ME_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
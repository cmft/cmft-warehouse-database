﻿CREATE TABLE [dbo].[F_Equipment](
	[EQP_SEQU] [numeric](9, 0) NOT NULL,
	[EQP_CODE] [varchar](30) NULL,
	[EQP_DESCRIPTION] [varchar](60) NULL,
	[EQP_SUP_SEQU] [numeric](9, 0) NOT NULL,
	[EQP_MODEL] [varchar](30) NULL,
	[EQP_INACTIVE] [numeric](3, 0) NOT NULL,
	[EQP_TURNAROUND_TIME] [numeric](3, 0) NOT NULL,
	[EQP_IMS_ITEM] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
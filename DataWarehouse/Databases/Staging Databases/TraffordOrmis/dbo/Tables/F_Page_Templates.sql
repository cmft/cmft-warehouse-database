﻿CREATE TABLE [dbo].[F_Page_Templates](
	[PT_SEQU] [numeric](9, 0) NOT NULL,
	[PT_FORMAT_NAME] [varchar](60) NULL,
	[PT_FORMAT_DATA] [image] NULL,
	[PT_FORMAT_DESC] [varchar](60) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
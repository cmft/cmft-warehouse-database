﻿CREATE TABLE [dbo].[F_Message_Queue](
	[MSQ_SEQU] [numeric](9, 0) NOT NULL,
	[MSQ_DATE_TIME] [datetime] NULL,
	[MSQ_INTERNAL_VISIT_SEQU] [numeric](9, 0) NOT NULL,
	[MSQ_PATIENT_SEQU] [numeric](9, 0) NOT NULL,
	[MSQ_MQH_NAME] [varchar](64) NULL,
	[MSQ_EVENT] [varchar](30) NULL,
	[MSQ_STATUS] [numeric](3, 0) NOT NULL,
	[MSQ_MESSAGE] [text] NULL,
	[MSQ_ERROR_MESSAGE] [varchar](256) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
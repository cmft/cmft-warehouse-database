﻿CREATE TABLE [dbo].[F_Allocation_Header](
	[AH_SEQU] [numeric](9, 0) NOT NULL,
	[AH_SHD_SEQU] [numeric](9, 0) NOT NULL,
	[AH_DATE] [datetime] NULL,
	[AH_PATTERN] [text] NULL,
	[AH_AVAILABLE] [numeric](3, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
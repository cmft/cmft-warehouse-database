﻿CREATE TABLE [dbo].[FHEALTHFUND](
	[FHF_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[FHF_CODE] [varchar](5) NULL,
	[FHF_NAME] [varchar](60) NULL,
	[FHF_SEQU] [numeric](9, 0) NOT NULL,
	[FHF_LOG_DATE] [datetime] NULL,
	[FHF_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[FHF_MEBERSHIPNO] [numeric](3, 0) NOT NULL,
	[FHF_INACTIVE] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
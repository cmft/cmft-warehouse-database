﻿CREATE TABLE [dbo].[F_Letters](
	[LE_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[LE_NAME] [varchar](50) NULL,
	[LE_TEXT] [text] NULL,
	[LE_LOG_DATE] [datetime] NULL,
	[LE_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[LE_AUDIT_LETTER] [numeric](3, 0) NOT NULL,
	[LE_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_RetRecTrans](
	[RRT_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[RRT_RET_SEQU] [numeric](9, 0) NOT NULL,
	[RRT_ROL_SEQU] [numeric](9, 0) NOT NULL,
	[RRT_SEQU] [numeric](9, 0) NOT NULL,
	[RRT_QTY] [numeric](9, 0) NOT NULL,
	[RRT_LOG_DATE] [datetime] NULL,
	[RRT_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Data_Item_Definition](
	[DID_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DID_CODE] [varchar](10) NULL,
	[DID_NAME] [varchar](30) NULL,
	[DID_DESCRIPTION] [varchar](255) NULL,
	[DID_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[DID_DEFINITION] [text] NULL,
	[DID_SORT_ORDER] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_BUPA_Score](
	[BUPA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[BUPA_CODE] [varchar](10) NULL,
	[BUPA_DESCRIPTION] [varchar](255) NULL,
	[BUPA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[BUPA_EXTRACT_CODE] [varchar](10) NULL,
	[BUPA_EXT_DB] [varchar](10) NULL
) ON [PRIMARY]
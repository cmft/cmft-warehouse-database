﻿CREATE TABLE [dbo].[F_Equip_Timetable](
	[ET_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ET_II_SEQU] [numeric](9, 0) NOT NULL,
	[ET_DATE] [datetime] NULL,
	[ET_SHARED] [numeric](3, 0) NOT NULL,
	[ET_SU_SEQU] [numeric](9, 0) NOT NULL,
	[ET_PA_SEQU] [numeric](9, 0) NOT NULL,
	[ET_SEQU] [numeric](9, 0) NOT NULL,
	[ET_LOG_DATE] [datetime] NULL,
	[ET_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ET_SS_SEQU] [numeric](9, 0) NOT NULL,
	[ET_REASON] [varchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Wait_Proc_Items](
	[WP_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WP_WD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WP_CD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WP_PRIMARY_CD] [numeric](3, 0) NOT NULL DEFAULT (0),
	[WP_TIME] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WP_TIME2] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WP_TIME3] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WP_EST_TIME] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WP_LOG_DATE] [datetime] NULL,
	[WP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[WP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WP_DESC] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Transfer_Log](
	[TL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TL_DATE_TIME_STARTED] [datetime] NULL,
	[TL_FILLER_01] [numeric](3, 0) NOT NULL,
	[TL_DATE_TIME_FINISHED] [datetime] NULL,
	[TL_STATUS] [numeric](3, 0) NOT NULL,
	[TL_REQUEST_NO] [varchar](6) NULL,
	[TL_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Body_Position_Op_Link](
	[BPO_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[BPO_SEQU] [numeric](9, 0) NOT NULL,
	[BPO_BP_SEQU] [numeric](9, 0) NOT NULL,
	[BPO_OP_SEQU] [numeric](9, 0) NOT NULL,
	[BPO_DATE_TIME] [datetime] NULL
) ON [PRIMARY]
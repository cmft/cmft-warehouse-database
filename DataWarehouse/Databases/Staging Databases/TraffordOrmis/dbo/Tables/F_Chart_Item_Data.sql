﻿CREATE TABLE [dbo].[F_Chart_Item_Data](
	[CID_SEQU] [numeric](9, 0) NOT NULL,
	[CID_CIH_SEQU] [numeric](9, 0) NOT NULL,
	[CID_DATETIME] [datetime] NULL,
	[CID_DATA] [numeric](11, 2) NOT NULL,
	[CID_NOTES] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_OnHold_Details](
	[BH_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[BH_SEQU] [numeric](9, 0) NOT NULL,
	[BH_PA_SEQU] [numeric](9, 0) NOT NULL,
	[BH_FILLER_01] [numeric](3, 0) NOT NULL,
	[BH_HOLD_START_DATE_TIME] [datetime] NULL,
	[BH_PR_SEQU] [numeric](9, 0) NOT NULL,
	[BH_HOLD_FINISH_DATE_TIME] [datetime] NULL,
	[BH_FILLER_02] [numeric](3, 0) NOT NULL,
	[BH_HOLD_DETAILS] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
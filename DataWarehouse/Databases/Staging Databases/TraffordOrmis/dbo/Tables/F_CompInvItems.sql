﻿CREATE TABLE [dbo].[F_CompInvItems](
	[CII_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[CII_COMP_II_SEQU] [numeric](9, 0) NOT NULL,
	[CII_II_SEQU] [numeric](9, 0) NOT NULL,
	[CII_AMOUNT] [numeric](9, 0) NOT NULL,
	[CII_SEQU] [numeric](9, 0) NOT NULL,
	[CII_LOG_DATE] [datetime] NULL,
	[CII_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
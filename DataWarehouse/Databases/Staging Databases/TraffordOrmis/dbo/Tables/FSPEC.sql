﻿CREATE TABLE [dbo].[FSPEC](
	[SP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SP_SEQU] [numeric](9, 0) NOT NULL,
	[SP_S1_SEQU] [numeric](9, 0) NOT NULL,
	[SP_CODE] [varchar](5) NULL,
	[SP_DESC] [varchar](150) NULL,
	[SP_LOG_DATE] [datetime] NULL,
	[SP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SP_INACTIVE] [numeric](3, 0) NOT NULL,
	[SP_S1COD] [varchar](5) NULL
) ON [PRIMARY]
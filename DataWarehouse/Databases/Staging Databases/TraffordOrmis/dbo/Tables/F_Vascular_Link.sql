﻿CREATE TABLE [dbo].[F_Vascular_Link](
	[VASL_SEQU] [numeric](9, 0) NOT NULL,
	[VASL_PAD_SEQU] [numeric](9, 0) NOT NULL,
	[VASL_VAS_SEQU] [numeric](9, 0) NOT NULL,
	[VASL_GA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
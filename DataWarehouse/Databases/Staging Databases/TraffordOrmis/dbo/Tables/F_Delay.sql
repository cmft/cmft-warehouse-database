﻿CREATE TABLE [dbo].[F_Delay](
	[DE_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DE_CODE] [varchar](5) NULL,
	[DE_DESCRIPTION] [varchar](60) NULL,
	[DE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DE_LOG_DATE] [datetime] NULL,
	[DE_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[DE_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
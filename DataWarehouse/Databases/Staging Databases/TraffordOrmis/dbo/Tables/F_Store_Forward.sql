﻿CREATE TABLE [dbo].[F_Store_Forward](
	[STNF_SEQU] [numeric](9, 0) NOT NULL,
	[STNF_DATE] [datetime] NULL,
	[STNF_USER] [varchar](20) NULL,
	[STNF_MESSAGE] [text] NULL,
	[STNF_REASON] [numeric](3, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
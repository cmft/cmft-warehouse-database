﻿CREATE TABLE [dbo].[F_Common_Proc](
	[CS_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CS_UNIQ_ID] [varchar](15) NOT NULL,
	[CS_CD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CS_S1_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
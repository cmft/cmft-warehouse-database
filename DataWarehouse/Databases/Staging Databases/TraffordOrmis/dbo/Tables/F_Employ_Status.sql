﻿CREATE TABLE [dbo].[F_Employ_Status](
	[ES_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ES_CODE] [varchar](5) NULL,
	[ES_DESCRIPTION] [varchar](60) NULL,
	[ES_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ES_LOG_DATE] [datetime] NULL,
	[ES_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ES_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ES_EXTRACT_CODE] [varchar](5) NULL,
	[ES_EXT_CODE] [varchar](5) NULL,
	[ES_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ES_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
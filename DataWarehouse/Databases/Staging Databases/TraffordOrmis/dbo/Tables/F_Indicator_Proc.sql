﻿CREATE TABLE [dbo].[F_Indicator_Proc](
	[IP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[IP_CODE] [varchar](5) NULL,
	[IP_DESCRIPTION] [varchar](170) NULL,
	[IP_SEQU] [numeric](9, 0) NOT NULL,
	[IP_LOG_DATE] [datetime] NULL,
	[IP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[IP_INACTIVE] [numeric](3, 0) NOT NULL,
	[IP_EXTRACT_CODE] [varchar](5) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Audit_Trail](
	[AT_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AT_DATE_TIME] [datetime] NULL,
	[AT_FILLER_01] [numeric](3, 0) NOT NULL DEFAULT (0),
	[AT_MACHINE_ID] [varchar](30) NULL,
	[AT_USER_NAME] [varchar](25) NULL,
	[AT_USER_ACTION] [varchar](15) NULL,
	[AT_USER_FILE] [varchar](25) NULL,
	[AT_USER_LIST] [varchar](15) NULL,
	[AT_AUDIT_CHAR] [text] NULL,
	[AT_REC_SEQU] [varchar](50) NULL,
	[AT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
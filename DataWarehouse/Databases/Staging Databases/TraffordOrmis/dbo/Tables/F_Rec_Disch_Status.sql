﻿CREATE TABLE [dbo].[F_Rec_Disch_Status](
	[RDS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RDS_CODE] [varchar](20) NULL,
	[RDS_DESCRIPTION] [varchar](60) NULL,
	[RDS_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RDS_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
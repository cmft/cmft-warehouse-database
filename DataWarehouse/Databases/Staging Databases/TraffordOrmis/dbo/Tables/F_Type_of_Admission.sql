﻿CREATE TABLE [dbo].[F_Type_of_Admission](
	[TA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TA_CODE] [varchar](5) NULL,
	[TA_DESCRIPTION] [varchar](60) NULL,
	[TA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TA_LOG_DATE] [datetime] NULL,
	[TA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[TA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[TA_ORDER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TA_THEATRE_LIST] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Closure_type](
	[CT_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CT_CODE] [varchar](5) NULL,
	[CT_DESCRIPTION] [varchar](60) NULL,
	[CT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CT_COMPLETE_CLOSE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CT_LOG_DATE] [datetime] NULL,
	[CT_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[CT_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CT_ORDER] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Mode_Of_Discharge](
	[MOD_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[MOD_SEQU] [numeric](9, 0) NOT NULL,
	[MOD_CODE] [varchar](5) NULL,
	[MOD_DESCRIPTION] [varchar](60) NULL,
	[MOD_LOG_DATE] [datetime] NULL,
	[MOD_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[MOD_INACTIVE] [numeric](3, 0) NOT NULL,
	[MOD_DEATH_INDICATOR] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
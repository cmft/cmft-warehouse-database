﻿CREATE TABLE [dbo].[F_Associated_Book](
	[AB_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[AB_PA_SEQU] [numeric](9, 0) NOT NULL,
	[AB_ASSOC_PA_SEQU] [numeric](9, 0) NOT NULL,
	[AB_CANCELLED] [numeric](3, 0) NOT NULL,
	[AB_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
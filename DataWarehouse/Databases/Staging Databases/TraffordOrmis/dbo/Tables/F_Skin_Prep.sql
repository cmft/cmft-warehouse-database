﻿CREATE TABLE [dbo].[F_Skin_Prep](
	[SKP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SKP_SEQU] [numeric](9, 0) NOT NULL,
	[SKP_OP_SEQU] [numeric](9, 0) NOT NULL,
	[SKP_AG_SEQU] [numeric](9, 0) NOT NULL,
	[SKP_ME_SEQU] [numeric](9, 0) NOT NULL,
	[SKP_LOG_DATE] [datetime] NULL,
	[SKP_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
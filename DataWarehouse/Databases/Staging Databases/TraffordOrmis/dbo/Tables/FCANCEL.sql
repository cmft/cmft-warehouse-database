﻿CREATE TABLE [dbo].[FCANCEL](
	[CN_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CN_CODE] [varchar](5) NULL,
	[CN_DESC] [varchar](60) NULL,
	[CN_LOG_DATE] [datetime] NULL,
	[CN_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[CN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CN_CG_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CN_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CN_DES_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Anaes_Maintenance](
	[MAN_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MAN_CODE] [varchar](5) NULL,
	[MAN_DESCRIPTION] [varchar](60) NULL,
	[MAN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MAN_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[MAN_LOG_DATE] [datetime] NULL,
	[MAN_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
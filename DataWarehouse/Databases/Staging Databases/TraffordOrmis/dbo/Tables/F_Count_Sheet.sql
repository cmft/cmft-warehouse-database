﻿CREATE TABLE [dbo].[F_Count_Sheet](
	[CSH_SEQU] [numeric](9, 0) NOT NULL,
	[CSH_ITEMS] [text] NULL,
	[CSH_PRE_SU_SEQU] [numeric](9, 0) NOT NULL,
	[CSH_1ST_SU_SEQU] [numeric](9, 0) NOT NULL,
	[CSH_2ND_SU_SEQU] [numeric](9, 0) NOT NULL,
	[CSH_FINAL_SU_SEQU] [numeric](9, 0) NOT NULL,
	[CSH_OP_SEQU] [numeric](9, 0) NOT NULL,
	[CSH_PRE_REASON] [varchar](500) NULL,
	[CSH_1ST_REASON] [varchar](500) NULL,
	[CSH_2ND_REASON] [varchar](500) NULL,
	[CSH_FINAL_REASON] [varchar](500) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
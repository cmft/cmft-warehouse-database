﻿CREATE TABLE [dbo].[FSURGN](
	[SU_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SU_CODE] [varchar](10) NULL,
	[SU_LNAME] [varchar](60) NULL,
	[SU_FNAME] [varchar](60) NULL,
	[SU_MRB_NUM] [varchar](10) NULL,
	[SU_RECORD] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_ACTUN] [numeric](9, 2) NOT NULL DEFAULT (0),
	[SU_LOG_DATE] [datetime] NULL,
	[SU_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SU_SC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SU_S1_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SU_EMP_NUM] [varchar](10) NULL,
	[SU_INITIAL] [varchar](3) NULL,
	[SU_PRIV_VERIFY] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_PIN_NO] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SU_AMO_NUM] [varchar](25) NULL,
	[SU_SPECIALTY] [varchar](5) NULL,
	[SU_UNUSED_1] [varchar](1) NULL,
	[SU_CURRENT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SU_PAGER1] [varchar](5) NULL,
	[SU_PAGER2] [varchar](5) NULL,
	[SU_PASSWORD] [varchar](50) NULL,
	[SU_PRIV_OVERBOOK] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_EDIT_ALERTS] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_EDIT_TEMPLATES] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_OVERRIDE_EQUIP_CONFLICT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_CANCEL_REINSTATE_SESS] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SU_EDIT_STAFF_HISTORY] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_VIEW_ANONYMOUS] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_VALIDATE_SURG_NOTE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[SU_SIGN_PDA] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
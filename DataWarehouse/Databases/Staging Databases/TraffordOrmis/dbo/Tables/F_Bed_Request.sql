﻿CREATE TABLE [dbo].[F_Bed_Request](
	[BRQ_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[BRQ_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[BRQ_CODE] [varchar](5) NULL,
	[BRQ_DESCRIPTION] [varchar](60) NULL,
	[BRQ_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[BRQ_LOG_DATE] [datetime] NULL,
	[BRQ_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[BRQ_NOT_THEATRE_LIST] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Appointment_Hist](
	[PAH_SEQU] [numeric](9, 0) NOT NULL,
	[PAH_EPS_SEQU] [numeric](9, 0) NOT NULL,
	[PAH_DATE_TIME] [datetime] NULL,
	[PAH_STATUS] [numeric](3, 0) NOT NULL,
	[PAH_APPOINT_DATE_TIME] [datetime] NULL
) ON [PRIMARY]
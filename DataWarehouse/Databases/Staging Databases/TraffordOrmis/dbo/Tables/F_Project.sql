﻿CREATE TABLE [dbo].[F_Project](
	[PRJ_SEQU] [numeric](9, 0) NOT NULL,
	[PRJ_CODE] [varchar](12) NULL,
	[PRJ_DESCRIPTION] [varchar](255) NULL,
	[PRJ_START_DATE] [datetime] NULL,
	[PRJ_END_DATE] [datetime] NULL,
	[PRJ_INFORMATION] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
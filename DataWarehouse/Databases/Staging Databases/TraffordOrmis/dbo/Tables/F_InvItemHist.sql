﻿CREATE TABLE [dbo].[F_InvItemHist](
	[IIH_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[IIH_DATE] [datetime] NULL,
	[IIH_COST] [numeric](9, 2) NOT NULL,
	[IIH_II_SEQU] [numeric](9, 0) NOT NULL,
	[IIH_SEQU] [numeric](9, 0) NOT NULL,
	[IIH_LOG_DATE] [datetime] NULL,
	[IIH_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[IIH_PAT_COST] [numeric](9, 2) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Transfer_Log2](
	[TL2_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TL2_DATE_TIME_STARTED] [datetime] NULL,
	[TL2_FILLER_01] [numeric](3, 0) NOT NULL,
	[TL2_DATE_TIME_FINISHED] [datetime] NULL,
	[TL2_STATUS] [numeric](3, 0) NOT NULL,
	[TL2_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
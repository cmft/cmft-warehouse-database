﻿CREATE TABLE [dbo].[F_Removal](
	[RE_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RE_CODE] [varchar](3) NULL,
	[RE_DESCRIPTION] [varchar](60) NULL,
	[RE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RE_LOG_DATE] [datetime] NULL,
	[RE_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[RE_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RE_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RE_EXTRACT_CODE] [varchar](5) NULL
) ON [PRIMARY]
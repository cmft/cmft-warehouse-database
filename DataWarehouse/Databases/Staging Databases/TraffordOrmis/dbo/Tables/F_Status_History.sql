﻿CREATE TABLE [dbo].[F_Status_History](
	[SH_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SH_DATE_START] [datetime] NULL,
	[SH_WD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SH_STATUS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SH_STATUS_FLAG] [varchar](2) NULL,
	[SH_LOG_DATE] [datetime] NULL,
	[SH_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SH_DATE_FINISH] [datetime] NULL,
	[SH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Delay_Items](
	[DIT_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIT_OP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIT_PR_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIT_SOURCE] [varchar](100) NULL,
	[DIT_MINUTES] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIT_OP_DELAY_ORDER] [numeric](3, 0) NOT NULL DEFAULT (0),
	[DIT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIT_FREE_TEXT] [varchar](200) NULL
) ON [PRIMARY]
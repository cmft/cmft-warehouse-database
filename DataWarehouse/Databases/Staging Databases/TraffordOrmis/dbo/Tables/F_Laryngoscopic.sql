﻿CREATE TABLE [dbo].[F_Laryngoscopic](
	[LAR_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LAR_CODE] [varchar](5) NULL,
	[LAR_DESC] [varchar](60) NULL,
	[LAR_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LAR_LOG_DATE] [datetime] NULL,
	[LAR_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[LAR_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
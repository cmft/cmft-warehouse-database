﻿CREATE TABLE [dbo].[FDIARY](
	[DI_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DI_DATE] [datetime] NULL,
	[DI_COMMENT] [text] NULL,
	[DI_WHO] [varchar](5) NULL,
	[DI_OR] [varchar](8) NULL,
	[DI_TOPIC] [varchar](45) NULL,
	[DI_TH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DI_LOG_DATE] [datetime] NULL,
	[DI_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[DI_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
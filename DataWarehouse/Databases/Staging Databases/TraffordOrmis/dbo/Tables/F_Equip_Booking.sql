﻿CREATE TABLE [dbo].[F_Equip_Booking](
	[EQB_SEQU] [numeric](9, 0) NOT NULL,
	[EQB_PA_SEQU] [numeric](9, 0) NOT NULL,
	[EQB_EQP_SEQU] [numeric](9, 0) NOT NULL,
	[EQB_EQI_SEQU] [numeric](9, 0) NOT NULL,
	[EQB_EP_SEQU] [numeric](9, 0) NOT NULL,
	[EQB_START_TIME] [datetime] NULL,
	[EQB_FINISH_TIME] [datetime] NULL,
	[EQB_TRANSPORT_TIME] [numeric](9, 0) NOT NULL,
	[EQB_CONFLICT_SEQU] [numeric](9, 0) NOT NULL,
	[EQB_CONFLICT_STAFF] [numeric](9, 0) NOT NULL,
	[EQB_CONFLICT_TEXT] [varchar](255) NULL
) ON [PRIMARY]
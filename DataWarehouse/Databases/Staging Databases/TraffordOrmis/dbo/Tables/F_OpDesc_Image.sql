﻿CREATE TABLE [dbo].[F_OpDesc_Image](
	[ODI_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ODI_SEQU] [numeric](9, 0) NOT NULL,
	[ODI_OP_SEQU] [numeric](9, 0) NOT NULL,
	[ODI_OIM_SEQU] [numeric](9, 0) NOT NULL,
	[ODI_LOG_DATE] [datetime] NULL,
	[ODI_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
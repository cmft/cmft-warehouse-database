﻿CREATE TABLE [dbo].[FPROCDY](
	[PR_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PR_CODE] [varchar](5) NULL,
	[PR_DESC] [varchar](60) NULL,
	[PR_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PR_LOG_DATE] [datetime] NULL,
	[PR_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PR_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[PR_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[PR_FREE_TEXT] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_SuppInvItems](
	[SII_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[SII_SUP_SEQU] [numeric](9, 0) NOT NULL,
	[SII_II_SEQU] [numeric](9, 0) NOT NULL,
	[SII_PREFERRED] [numeric](3, 0) NOT NULL,
	[SII_ORDER_UNIT] [varchar](30) NULL,
	[SII_UNITS_IN_UNIT] [numeric](9, 0) NOT NULL,
	[SII_CAT_NUM] [varchar](15) NULL,
	[SII_BARCODE_NUM] [varchar](30) NULL,
	[SII_SEQU] [numeric](9, 0) NOT NULL,
	[SII_COST] [numeric](9, 2) NOT NULL,
	[SII_LOG_DATE] [datetime] NULL,
	[SII_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SII_ORDER_TYPE_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_PDA_Checklist](
	[PDC_SEQU] [numeric](9, 0) NOT NULL,
	[PDC_CODE] [varchar](12) NULL,
	[PDC_NAME] [varchar](50) NULL,
	[PDC_INACTIVE] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
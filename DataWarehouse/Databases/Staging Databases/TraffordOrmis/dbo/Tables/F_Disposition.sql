﻿CREATE TABLE [dbo].[F_Disposition](
	[DS_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[DS_CODE] [varchar](5) NULL,
	[DS_DESCRIPTION] [varchar](60) NULL,
	[DS_SEQU] [numeric](9, 0) NOT NULL,
	[DS_LOG_DATE] [datetime] NULL,
	[DS_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[DS_REP_NO] [numeric](3, 0) NOT NULL,
	[DS_INACTIVE] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
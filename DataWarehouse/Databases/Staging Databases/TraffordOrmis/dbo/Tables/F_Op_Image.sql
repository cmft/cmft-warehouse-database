﻿CREATE TABLE [dbo].[F_Op_Image](
	[OIM_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[OIM_NAME] [varchar](25) NULL,
	[OIM_DESCRIPTION] [varchar](60) NULL,
	[OIM_LOG_DATE] [datetime] NULL,
	[OIM_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[OIM_IMAGE] [image] NULL,
	[OIM_INACTIVE] [numeric](3, 0) NOT NULL,
	[OIM_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
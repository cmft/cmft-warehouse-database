﻿CREATE TABLE [dbo].[F_Session_Template](
	[SSTT_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_IT_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_DAY_NO] [numeric](3, 0) NOT NULL,
	[SSTT_SESSION] [numeric](3, 0) NOT NULL,
	[SSTT_SURG1_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_SURG2_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_SURG3_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_ANAES2_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_ANAES3_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_SCOUT_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_INST_SEQU] [numeric](9, 0) NOT NULL,
	[SSTT_ANAESN_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_SurgTeamExtra](
	[STE_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[STE_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[STE_OP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[STE_OI_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[STE_SC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[STE_LOG_DATE] [datetime] NULL,
	[STE_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[STE_NAME] [varchar](52) NULL,
	[STE_SSU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[STE_ROLE] [varchar](60) NULL,
	[STE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
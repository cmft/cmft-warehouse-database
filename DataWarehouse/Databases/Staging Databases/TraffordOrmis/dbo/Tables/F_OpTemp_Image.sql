﻿CREATE TABLE [dbo].[F_OpTemp_Image](
	[OTI_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[OTI_SEQU] [numeric](9, 0) NOT NULL,
	[OTI_ODT_SEQU] [numeric](9, 0) NOT NULL,
	[OTI_OIM_SEQU] [numeric](9, 0) NOT NULL,
	[OTI_LOG_DATE] [datetime] NULL,
	[OTI_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
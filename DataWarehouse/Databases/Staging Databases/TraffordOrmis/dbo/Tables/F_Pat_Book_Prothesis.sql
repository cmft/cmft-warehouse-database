﻿CREATE TABLE [dbo].[F_Pat_Book_Prothesis](
	[PBP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[PBP_DATE] [datetime] NULL,
	[PBP_QTY] [numeric](9, 0) NOT NULL,
	[PBP_II_SEQU] [numeric](9, 0) NOT NULL,
	[PBP_PA_SEQU] [numeric](9, 0) NOT NULL,
	[PBP_INV_DESCRIPTION] [varchar](100) NULL,
	[PBP_LOCATION] [varchar](25) NULL,
	[PBP_SU_SEQU] [numeric](9, 0) NOT NULL,
	[PBP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_BUPA_Op_Link](
	[BOL_SEQU] [numeric](9, 0) NOT NULL,
	[BOL_OP_SEQU] [numeric](9, 0) NOT NULL,
	[BOL_BUPA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
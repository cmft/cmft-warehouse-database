﻿CREATE TABLE [dbo].[F_Setup_Link](
	[SUL_SEQU] [numeric](9, 0) NOT NULL,
	[SUL_SU_SEQU] [numeric](9, 0) NOT NULL,
	[SUL_CD_SEQU] [numeric](9, 0) NOT NULL,
	[SUL_INFORMATION] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
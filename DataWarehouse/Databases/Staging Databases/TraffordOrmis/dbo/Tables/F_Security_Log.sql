﻿CREATE TABLE [dbo].[F_Security_Log](
	[SL_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SL_US_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SL_FAIL_DATE] [datetime] NULL,
	[SL_PASSWORD] [varchar](15) NULL,
	[SL_MACHINE_ID] [varchar](20) NULL,
	[SL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
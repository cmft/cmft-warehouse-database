﻿CREATE TABLE [dbo].[F_Language](
	[LA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LA_CODE] [varchar](5) NULL,
	[LA_DESCRIPTION] [varchar](60) NULL,
	[LA_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[LA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LA_LOG_DATE] [datetime] NULL,
	[LA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[LA_EXT_CODE] [varchar](5) NULL,
	[LA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[LA_EXTRACT_CODE] [varchar](5) NULL,
	[LA_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
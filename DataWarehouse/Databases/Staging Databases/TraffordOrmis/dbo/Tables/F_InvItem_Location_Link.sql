﻿CREATE TABLE [dbo].[F_InvItem_Location_Link](
	[IILL_II_SEQU] [numeric](9, 0) NOT NULL,
	[IILL_IL_SEQU] [numeric](9, 0) NOT NULL,
	[IILL_CURRENT_STOCK] [numeric](9, 0) NOT NULL,
	[IILL_REORDER_LEVEL] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Drugs](
	[DR_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DR_BUTTON_1] [varchar](20) NULL,
	[DR_BUTTON_2] [varchar](20) NULL,
	[DR_BUTTON_3] [varchar](20) NULL,
	[DR_BUTTON_4] [varchar](20) NULL,
	[DR_BUTTON_5] [varchar](20) NULL,
	[DR_BUTTON_6] [varchar](20) NULL,
	[DR_BUTTON_7] [varchar](20) NULL,
	[DR_BUTTON_8] [varchar](20) NULL,
	[DR_BUTTON_9] [varchar](20) NULL,
	[DR_BUTTON_10] [varchar](20) NULL,
	[DR_BUTTON_11] [varchar](20) NULL,
	[DR_BUTTON_12] [varchar](20) NULL,
	[DR_BUTTON_13] [varchar](20) NULL,
	[DR_BUTTON_14] [varchar](20) NULL,
	[DR_BUTTON_15] [varchar](20) NULL,
	[DR_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DR_LOG_DATE] [datetime] NULL,
	[DR_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_PainLink](
	[PAL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[PAL_DESCRIPTION] [varchar](30) NULL,
	[PAL_DETAILS] [text] NULL,
	[PAL_OP_SEQU] [numeric](9, 0) NOT NULL,
	[PAL_LOG_DATE] [datetime] NULL,
	[PAL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PAL_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
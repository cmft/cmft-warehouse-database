﻿CREATE TABLE [dbo].[F_State](
	[STA_SEQU] [numeric](9, 0) NOT NULL,
	[STA_CODE] [varchar](12) NULL,
	[STA_DESC] [varchar](60) NULL,
	[STA_MAP_DB] [varchar](12) NULL,
	[STA_EXTRACT_VALUE] [varchar](12) NULL,
	[STA_LOCKED] [numeric](3, 0) NOT NULL,
	[STA_INACTIVE] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
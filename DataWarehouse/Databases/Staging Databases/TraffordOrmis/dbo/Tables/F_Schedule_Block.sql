﻿CREATE TABLE [dbo].[F_Schedule_Block](
	[SB_SEQU] [numeric](9, 0) NOT NULL,
	[SB_SHD_SEQU] [numeric](9, 0) NOT NULL,
	[SB_START_TIME] [datetime] NULL,
	[SB_NO_OF_TIME_BLOCKS] [numeric](3, 0) NOT NULL,
	[SB_BLOCK_REASON] [varchar](40) NULL
) ON [PRIMARY]
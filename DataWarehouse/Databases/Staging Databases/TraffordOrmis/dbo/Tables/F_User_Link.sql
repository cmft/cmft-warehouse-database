﻿CREATE TABLE [dbo].[F_User_Link](
	[UL_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UL_INSERT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UL_EDIT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UL_DELETE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UL_PRINT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UL_UNIQUE] [varchar](50) NOT NULL,
	[UL_ACCESS] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UL_FO_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UL_US_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UL_LOG_DATE] [datetime] NULL,
	[UL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[UL_EXCEPTION] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UL_EDIT_PERIOD] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UL_CANCEL] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
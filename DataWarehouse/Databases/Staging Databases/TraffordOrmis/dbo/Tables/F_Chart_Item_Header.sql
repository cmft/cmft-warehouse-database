﻿CREATE TABLE [dbo].[F_Chart_Item_Header](
	[CIH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CIH_CHT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CIH_ITEM_ID] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CIH_NAME] [varchar](50) NULL,
	[CIH_TYPE] [varchar](30) NULL,
	[CIH_SUBTYPE] [varchar](30) NULL,
	[CIH_GRAPH] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CIH_LIMIT] [varchar](1000) NULL,
	[CIH_ORDER] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Accom_Status](
	[AS_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AS_CODE] [varchar](5) NULL,
	[AS_DESCRIPTION] [varchar](60) NULL,
	[AS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AS_EX_VALUE] [varchar](1) NULL,
	[AS_LOG_DATE] [datetime] NULL,
	[AS_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[AS_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[AS_DAYCASE] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
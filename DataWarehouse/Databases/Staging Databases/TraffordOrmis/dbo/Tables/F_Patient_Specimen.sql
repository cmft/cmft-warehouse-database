﻿CREATE TABLE [dbo].[F_Patient_Specimen](
	[PS_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PS_OP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PS_NAME] [varchar](10) NULL,
	[PS_DESCRIPTION] [varchar](100) NULL,
	[PS_WA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PS_QTY] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PS_REPORT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[PS_PD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
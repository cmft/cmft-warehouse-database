﻿CREATE TABLE [dbo].[F_Pre_Op_Assess_Link](
	[POAL_SEQU] [numeric](9, 0) NOT NULL,
	[POAL_PAD_SEQU] [numeric](9, 0) NOT NULL,
	[POAL_POA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
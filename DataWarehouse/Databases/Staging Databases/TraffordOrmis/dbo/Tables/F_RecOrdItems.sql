﻿CREATE TABLE [dbo].[F_RecOrdItems](
	[ROI_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[ROI_DATE] [datetime] NULL,
	[ROI_OI_SEQU] [numeric](9, 0) NOT NULL,
	[ROI_II_SEQU] [numeric](9, 0) NOT NULL,
	[ROI_LOG_DATE] [datetime] NULL,
	[ROI_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ROI_QTY] [numeric](9, 0) NOT NULL,
	[ROI_SEQU] [numeric](9, 0) NOT NULL,
	[ROI_SD_SEQU] [numeric](9, 0) NOT NULL,
	[ROI_INVOICE_NO] [varchar](12) NULL,
	[ROI_UNIT_PRICE] [numeric](9, 2) NOT NULL,
	[ROI_SUP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Data_Item_Careplan](
	[DIC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIC_DID_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIC_CRITERIA] [text] NULL,
	[DIC_LEVEL] [numeric](3, 0) NOT NULL DEFAULT (0),
	[DIC_MANDATORY] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
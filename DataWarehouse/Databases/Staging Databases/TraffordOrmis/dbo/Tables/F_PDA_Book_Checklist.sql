﻿CREATE TABLE [dbo].[F_PDA_Book_Checklist](
	[PBC_SEQU] [numeric](9, 0) NOT NULL,
	[PBC_PA_SEQU] [numeric](9, 0) NOT NULL,
	[PBC_PDC_SEQU] [numeric](9, 0) NOT NULL,
	[PBC_PDC_CODE] [varchar](20) NULL,
	[PBC_PDC_NAME] [varchar](50) NULL,
	[PBC_SET_1_CHECKIN] [datetime] NULL,
	[PBC_SET_1_SU_SEQU] [numeric](9, 0) NOT NULL,
	[PBC_SET_1_STAFF] [varchar](255) NULL,
	[PBC_SET_2_CHECKIN] [datetime] NULL,
	[PBC_SET_2_SU_SEQU] [numeric](9, 0) NOT NULL,
	[PBC_SET_2_STAFF] [varchar](255) NULL,
	[PBC_SET_3_CHECKIN] [datetime] NULL,
	[PBC_SET_3_SU_SEQU] [numeric](9, 0) NOT NULL,
	[PBC_SET_3_STAFF] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_InvKey](
	[IK_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[IK_KEY_WORD] [varchar](30) NULL,
	[IK_II_SEQU] [numeric](9, 0) NOT NULL,
	[IK_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
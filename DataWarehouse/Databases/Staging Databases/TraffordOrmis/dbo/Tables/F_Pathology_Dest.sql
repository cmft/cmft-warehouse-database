﻿CREATE TABLE [dbo].[F_Pathology_Dest](
	[PD_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PD_CODE] [varchar](10) NULL,
	[PD_DESCRIPTION] [varchar](120) NULL,
	[PD_EXT_CODE] [varchar](10) NULL,
	[PD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PD_LOG_DATE] [datetime] NULL,
	[PD_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PD_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[PD_EXTRACT_CODE] [varchar](10) NULL,
	[PD_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
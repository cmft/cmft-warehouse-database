﻿CREATE TABLE [dbo].[F_Anaesthetic](
	[ANA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ANA_CODE] [varchar](6) NULL,
	[ANA_DESCRIPTION] [varchar](60) NULL,
	[ANA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ANA_LOG_DATE] [datetime] NULL,
	[ANA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ANA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ANA_MINIMAL_TIMINGS] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
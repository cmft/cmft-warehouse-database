﻿CREATE TABLE [dbo].[F_Occupation](
	[OCC_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[OCC_CODE] [varchar](5) NULL,
	[OCC_DESCRIPTION] [varchar](60) NULL,
	[OCC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[OCC_LOG_DATE] [datetime] NULL,
	[OCC_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[OCC_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[OCC_EXTRACT_CODE] [varchar](5) NULL,
	[OCC_EXT_CODE] [varchar](5) NULL,
	[OCC_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[OCC_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
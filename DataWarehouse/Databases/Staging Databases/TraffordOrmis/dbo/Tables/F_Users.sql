﻿CREATE TABLE [dbo].[F_Users](
	[US_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[US_NAME] [varchar](20) NULL,
	[US_PASSWORD] [varchar](50) NULL,
	[US_ADMIN] [numeric](3, 0) NOT NULL DEFAULT (0),
	[US_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[US_LOG_DATE] [datetime] NULL,
	[US_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[US_PASS_DATE] [datetime] NULL,
	[US_LAST_LOG_DATE] [datetime] NULL,
	[US_GROUP] [numeric](3, 0) NOT NULL DEFAULT (0),
	[US_US_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[US_VERSION] [numeric](3, 0) NOT NULL DEFAULT (0),
	[US_EXPIRE_DATE] [datetime] NULL,
	[US_OVERRIDE_RES] [numeric](3, 0) NOT NULL DEFAULT (0),
	[US_CURRENT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[US_WEB_ACCESS] [numeric](3, 0) NOT NULL DEFAULT (0),
	[US_PDA_ACCESS] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Body_Site](
	[BS_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[BS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[BS_CODE] [varchar](5) NULL,
	[BS_DESC] [varchar](60) NULL,
	[BS_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[BS_LOG_DATE] [datetime] NULL,
	[BS_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
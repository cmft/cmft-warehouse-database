﻿CREATE TABLE [dbo].[SpecialityPerRoomBlock](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[SpecialityId] [uniqueidentifier] NOT NULL,
	[RoomBlockId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
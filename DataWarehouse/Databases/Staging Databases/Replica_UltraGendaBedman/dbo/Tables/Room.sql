﻿CREATE TABLE [dbo].[Room](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Abbreviation] [nvarchar](20) NOT NULL,
	[Code] [nvarchar](40) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[WardId] [uniqueidentifier] NOT NULL,
	[RoomTypeId] [uniqueidentifier] NOT NULL,
	[AdmittedSex] [nchar](1) NOT NULL,
	[AgeCriterionId] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]
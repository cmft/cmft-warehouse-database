﻿CREATE TABLE [dbo].[RoomCharacteristicPerAdmissionType](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[AdmissionTypeId] [uniqueidentifier] NOT NULL,
	[RoomCharacteristicId] [uniqueidentifier] NOT NULL,
	[IsMandatory] [bit] NOT NULL
) ON [PRIMARY]
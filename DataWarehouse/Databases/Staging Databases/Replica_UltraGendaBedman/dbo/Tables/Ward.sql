﻿CREATE TABLE [dbo].[Ward](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Abbreviation] [nvarchar](20) NOT NULL,
	[Code] [nvarchar](40) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[OrganisationalUnitId] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[AdministrativeResponsible] [nvarchar](80) NULL,
	[MaximumTreatmentLoad] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]
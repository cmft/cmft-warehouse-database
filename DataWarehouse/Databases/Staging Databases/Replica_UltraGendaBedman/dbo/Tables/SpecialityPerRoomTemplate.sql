﻿CREATE TABLE [dbo].[SpecialityPerRoomTemplate](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[SpecialityId] [uniqueidentifier] NOT NULL,
	[RoomTemplateId] [uniqueidentifier] NOT NULL,
	[WeekDays] [tinyint] NOT NULL
) ON [PRIMARY]
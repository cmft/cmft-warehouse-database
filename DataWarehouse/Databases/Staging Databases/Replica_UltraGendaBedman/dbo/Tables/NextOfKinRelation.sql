﻿CREATE TABLE [dbo].[NextOfKinRelation](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Code] [nvarchar](40) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Instruction](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Text] [nvarchar](1000) NOT NULL,
	[Receiver] [tinyint] NOT NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[InstructionPerAdmissionType](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[AdmissionTypeId] [uniqueidentifier] NOT NULL,
	[InstructionId] [uniqueidentifier] NOT NULL,
	[Sequence] [smallint] NOT NULL
) ON [PRIMARY]
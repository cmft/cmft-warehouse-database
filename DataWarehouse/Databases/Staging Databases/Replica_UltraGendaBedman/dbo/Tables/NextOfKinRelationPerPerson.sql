﻿CREATE TABLE [dbo].[NextOfKinRelationPerPerson](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[PersonId] [uniqueidentifier] NULL,
	[RelatedPersonId] [uniqueidentifier] NOT NULL,
	[NextOfKinRelationId] [uniqueidentifier] NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
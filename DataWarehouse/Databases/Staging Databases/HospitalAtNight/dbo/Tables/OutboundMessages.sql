﻿CREATE TABLE [dbo].[OutboundMessages](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PayLoad] [xml] NOT NULL,
	[InsertedTimeStamp] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
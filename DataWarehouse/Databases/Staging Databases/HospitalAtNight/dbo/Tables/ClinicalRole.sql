﻿CREATE TABLE [dbo].[ClinicalRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Role] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[ClinicalCategoryID] [int] NOT NULL,
	[Order] [int] NOT NULL CONSTRAINT [DF_ClinicalRole_Order]  DEFAULT ((0))
) ON [PRIMARY]
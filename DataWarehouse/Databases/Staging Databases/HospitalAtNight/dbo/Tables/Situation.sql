﻿CREATE TABLE [dbo].[Situation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[SituationDescription] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL CONSTRAINT [DF_Situation_Order]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserType](
	[UserTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TypeDescription] [varchar](50) NULL
) ON [PRIMARY]
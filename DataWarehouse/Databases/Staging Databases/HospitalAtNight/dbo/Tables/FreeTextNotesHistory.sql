﻿CREATE TABLE [dbo].[FreeTextNotesHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FreeTextNotes] [nvarchar](max) NOT NULL,
	[ServiceRequestID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
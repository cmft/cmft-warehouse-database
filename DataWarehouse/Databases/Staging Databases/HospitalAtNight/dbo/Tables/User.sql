﻿CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
	[LastName] [varchar](20) NOT NULL,
	[UserLogin] [varchar](50) NULL,
	[UserTypeID] [int] NOT NULL,
	[ClinicalRoleID] [int] NULL,
	[EmailAddress] [varchar](50) NOT NULL,
	[MobileTelephone] [varchar](20) NULL,
	[Bleep] [varchar](20) NULL,
	[ADUserID] [varchar](50) NULL,
	[Active] [bit] NOT NULL,
	[Password] [varbinary](128) NULL
) ON [PRIMARY]
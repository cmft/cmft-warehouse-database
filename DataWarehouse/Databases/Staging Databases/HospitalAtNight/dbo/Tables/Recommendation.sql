﻿CREATE TABLE [dbo].[Recommendation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RecommendationName] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL CONSTRAINT [DF_Recommendation_Order]  DEFAULT ((0))
) ON [PRIMARY]
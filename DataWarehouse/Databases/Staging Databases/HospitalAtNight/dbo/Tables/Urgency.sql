﻿CREATE TABLE [dbo].[Urgency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Threshold] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Ward](
	[ID] [int] NOT NULL,
	[WardName] [varchar](50) NOT NULL,
	[PASCode] [varchar](4) NOT NULL,
	[Hospital] [varchar](4) NULL
) ON [PRIMARY]
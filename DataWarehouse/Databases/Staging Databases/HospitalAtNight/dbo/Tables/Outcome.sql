﻿CREATE TABLE [dbo].[Outcome](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](60) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NULL
) ON [PRIMARY]
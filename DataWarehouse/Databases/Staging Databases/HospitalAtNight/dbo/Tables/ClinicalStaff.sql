﻿CREATE TABLE [dbo].[ClinicalStaff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](30) NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[Telephone] [varchar](20) NULL,
	[Bleep] [varchar](20) NULL,
	[EmailAddress] [varchar](50) NULL,
	[CinicalRoleID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ErrorLog](
	[ErrorID] [bigint] IDENTITY(1,1) NOT NULL,
	[FormID] [bigint] NULL,
	[FormTypeID] [int] NULL,
	[InnerException] [varchar](max) NULL,
	[MethodName] [varchar](200) NULL,
	[AttemptedBy] [varchar](200) NULL,
	[AttemptedOn] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
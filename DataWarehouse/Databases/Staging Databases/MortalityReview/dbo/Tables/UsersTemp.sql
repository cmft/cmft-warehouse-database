﻿CREATE TABLE [dbo].[UsersTemp](
	[UserID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[DomainLogin] [varchar](500) NULL,
	[Email] [varchar](500) NULL,
	[Active] [bit] NULL
) ON [PRIMARY]
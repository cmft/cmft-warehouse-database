﻿CREATE TABLE [dbo].[UserFormType](
	[UserFormTypeID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[FormTypeID] [int] NULL,
	[IsReviewer] [bit] NULL
) ON [PRIMARY]
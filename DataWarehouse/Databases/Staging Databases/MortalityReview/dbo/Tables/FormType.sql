﻿CREATE TABLE [dbo].[FormType](
	[FormTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
	[MultipleReviewers] [bit] NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
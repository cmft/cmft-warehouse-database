﻿CREATE TABLE [dbo].[FormTypeSection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormTypeID] [int] NOT NULL,
	[SectionID] [int] NULL,
	[Ordering] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Audit](
	[AuditID] [bigint] IDENTITY(1,1) NOT NULL,
	[FormID] [bigint] NULL,
	[UpdatedBy] [varchar](500) NULL,
	[UpdatedOn] [datetime] NULL,
	[Status] [varchar](200) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Section](
	[SectionID] [int] IDENTITY(1,1) NOT NULL,
	[SectionName] [varchar](200) NULL,
	[PartialName] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
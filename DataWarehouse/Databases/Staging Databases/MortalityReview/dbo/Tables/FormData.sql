﻿CREATE TABLE [dbo].[FormData](
	[FormID] [bigint] IDENTITY(1,1) NOT NULL,
	[FormTypeID] [int] NULL,
	[XMLData] [xml] NULL,
	[MortalityReviewBaseID] [bigint] NULL,
	[Status] [varchar](200) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
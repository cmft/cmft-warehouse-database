﻿CREATE TABLE [dbo].[Clinic_Slot_Bookings_Status_Codes](
	[Status_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Code] [varchar](3) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Default] [bit] NOT NULL,
	[Default_When_Cancelled] [bit] NOT NULL,
	[Default_When_DNA] [bit] NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
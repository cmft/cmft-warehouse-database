﻿CREATE TABLE [dbo].[Interop_Login](
	[id] [uniqueidentifier] NOT NULL,
	[Interop_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[session_id] [uniqueidentifier] NOT NULL,
	[enc_key] [varchar](32) NOT NULL,
	[patient_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[service_id] [smallint] NOT NULL,
	[url] [varchar](150) NOT NULL,
	[data] [varchar](1000) NULL,
	[xml] [varchar](7000) NOT NULL,
	[xml_response] [text] NULL,
	[create_dt] [datetime] NOT NULL CONSTRAINT [DF_Interop_Login_create_dt]  DEFAULT (getdate()),
	[URN] [varchar](100) NOT NULL,
	[Location_Index] [int] NOT NULL CONSTRAINT [DF_Interop_Login_Location_Index]  DEFAULT ((-1)),
	[Authenticated] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Print_Profile_Conditions_Data](
	[Condition_Data_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Condition_Index] [int] NOT NULL,
	[Value] [varchar](100) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_User_Roles](
	[Role_Index] [tinyint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Role_Name] [varchar](20) NOT NULL,
	[Role_Type] [char](1) NOT NULL,
	[AD_Security_Group] [varchar](255) NULL,
	[Demand_LR] [bit] NOT NULL DEFAULT (0),
	[NumRestrictedLocsToDisplay] [tinyint] NOT NULL CONSTRAINT [DF_NumRestrictedLocsToDisplay_Service_User_Roles]  DEFAULT ('0'),
	[ApplyLocationRestrictions] [bit] NOT NULL CONSTRAINT [DF_ApplyLocationRestrictions_Service_User_Roles]  DEFAULT ((0)),
	[ApplyLocationRestrictionsToSearch] [bit] NOT NULL CONSTRAINT [DF_ApplyLocationRestrictionsToSearch_Service_User_Roles]  DEFAULT ((0)),
	[MaximumLoginAttempts] [tinyint] NULL,
	[Active] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Request_Custom_Status_Codes](
	[Custom_Code_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Custom_Code] [varchar](6) NOT NULL,
	[Custom_Display_Code] [varchar](3) NOT NULL,
	[Custom_Code_Description] [varchar](50) NOT NULL,
	[Deny_Edit_Request] [bit] NOT NULL,
	[Deny_Cancel_Request] [bit] NOT NULL,
	[Deny_Reprint_Request] [bit] NOT NULL,
	[Deny_Write_Report] [bit] NOT NULL,
	[Organisation] [varchar](6) NOT NULL
) ON [PRIMARY]
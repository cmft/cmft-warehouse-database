﻿CREATE TABLE [dbo].[Clinician_Local_Id](
	[Clinician_Local_code] [varchar](35) NOT NULL,
	[Date_Added] [datetime] NULL,
	[EDI_LTS_Index] [int] NOT NULL,
	[Clinician_Index] [int] NOT NULL DEFAULT (0),
	[Inbound] [bit] NOT NULL DEFAULT (1),
	[Outbound] [bit] NOT NULL DEFAULT (1)
) ON [PRIMARY]
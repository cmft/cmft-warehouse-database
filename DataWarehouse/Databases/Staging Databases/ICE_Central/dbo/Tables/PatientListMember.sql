﻿CREATE TABLE [dbo].[PatientListMember](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ListIndex] [int] NOT NULL,
	[TeamIndex] [int] NULL,
	[UserIndex] [int] NULL,
	[CanWrite] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ICD9_CPT_Map](
	[Mapping_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Organisation] [varchar](6) NOT NULL,
	[ICD9_Code] [varchar](10) NOT NULL,
	[CPT_Code] [varchar](5) NOT NULL,
	[Active_Date] [smalldatetime] NOT NULL,
	[Inactive_Date] [smalldatetime] NULL,
	[Active_Marker]  AS (case when [Inactive_Date] IS NULL then (1) when [Inactive_Date]>getdate() then (1) else (0) end)
) ON [PRIMARY]
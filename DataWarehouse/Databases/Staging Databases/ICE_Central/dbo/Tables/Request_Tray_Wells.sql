﻿CREATE TABLE [dbo].[Request_Tray_Wells](
	[Well_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Tray_Index] [int] NULL,
	[Tray_x_Coord] [int] NULL,
	[Tray_y_Coord] [int] NULL,
	[Well_Content] [varchar](30) NULL
) ON [PRIMARY]
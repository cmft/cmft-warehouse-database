﻿CREATE TABLE [dbo].[WBP_Wristband_Template](
	[Template_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Print_Control_Commands] [text] NOT NULL,
	[Active] [bit] NOT NULL,
	[Gender_Restriction] [tinyint] NOT NULL,
	[Gender_Restriction_Level] [tinyint] NOT NULL,
	[Age_Restriction_Min] [tinyint] NULL,
	[Age_Restriction_Max] [tinyint] NULL,
	[Age_Restriction_Level] [tinyint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
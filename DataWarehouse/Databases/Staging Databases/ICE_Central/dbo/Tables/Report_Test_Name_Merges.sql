﻿CREATE TABLE [dbo].[Report_Test_Name_Merges](
	[Merge_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[User_Index] [int] NOT NULL,
	[Test_Name_Index] [int] NOT NULL,
	[Lower_Range] [varchar](35) NOT NULL,
	[Upper_Range] [varchar](35) NOT NULL,
	[UOM_Code] [varchar](35) NOT NULL,
	[Master] [bit] NOT NULL,
	[Merged_To] [int] NULL
) ON [PRIMARY]
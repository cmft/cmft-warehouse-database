﻿CREATE TABLE [dbo].[gp_local_id_redundant_keep_for_a_while](
	[Gp_National_Code] [varchar](8) NOT NULL,
	[Gp_Local_id] [varchar](16) NOT NULL,
	[Org_Code] [varchar](6) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Signatory](
	[Signatory] [varchar](30) NULL,
	[Job_Title] [varchar](50) NULL,
	[Discharge_Workgroup] [int] NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
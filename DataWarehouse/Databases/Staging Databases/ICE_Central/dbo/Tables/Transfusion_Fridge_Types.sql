﻿CREATE TABLE [dbo].[Transfusion_Fridge_Types](
	[Type_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](50) NOT NULL
) ON [PRIMARY]
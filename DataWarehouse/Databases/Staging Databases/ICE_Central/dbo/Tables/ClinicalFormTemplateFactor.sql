﻿CREATE TABLE [dbo].[ClinicalFormTemplateFactor](
	[TemplateId] [int] NOT NULL,
	[HelpText] [varchar](255) NULL,
	[MinValue] [decimal](19, 4) NULL,
	[MaxValue] [decimal](19, 4) NULL,
	[Mandatory] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[FactorId] [int] NOT NULL,
	[GroupId] [int] NULL,
	[PrefillWithValue] [varchar](255) NULL,
	[HelpURL] [varchar](255) NULL,
	[RangeWarningText] [varchar](255) NULL,
	[ShowRangeWarningConfirmation] [tinyint] NOT NULL DEFAULT ((0)),
	[AlertToRaiseIfActionNotTaken] [int] NULL
) ON [PRIMARY]
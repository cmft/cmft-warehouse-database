﻿CREATE TABLE [dbo].[EDI_Interchange_No](
	[Ref_Index] [int] NOT NULL,
	[EDI_Msg_Format] [varchar](41) NOT NULL CONSTRAINT [DF_EDI_Interchange_No_EDI_Msg_Format]  DEFAULT ('None'),
	[EDI_Last_Interchange] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Request_Histology_Sites_Level2](
	[Site2_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Site_Index] [int] NOT NULL,
	[Site] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Language_Lookup](
	[Language_Index] [smallint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Language] [varchar](100) NOT NULL
) ON [PRIMARY]
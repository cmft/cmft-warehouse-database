﻿CREATE TABLE [dbo].[Application](
	[ApplicationID] [int] NOT NULL,
	[ApplicationName] [varchar](50) NOT NULL,
	[BriefName] [varchar](10) NULL,
	[Notes] [text] NULL,
	[Installed] [bit] NOT NULL CONSTRAINT [DF_Application_Installed]  DEFAULT (0),
	[Colour] [int] NULL,
	[Mobile_Installed] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
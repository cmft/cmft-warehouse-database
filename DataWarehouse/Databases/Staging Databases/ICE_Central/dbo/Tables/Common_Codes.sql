﻿CREATE TABLE [dbo].[Common_Codes](
	[Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CommonCode] [char](5) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Type] [char](1) NOT NULL
) ON [PRIMARY]
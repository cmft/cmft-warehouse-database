﻿CREATE TABLE [dbo].[User_Configurable_DataSets_Keys](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dataset_id] [int] NOT NULL,
	[male] [bit] NOT NULL,
	[key_name] [varchar](100) NOT NULL,
	[lookup_values] [bit] NOT NULL,
	[lookup_table] [varchar](50) NULL,
	[lookup_column] [varchar](50) NULL,
	[description] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
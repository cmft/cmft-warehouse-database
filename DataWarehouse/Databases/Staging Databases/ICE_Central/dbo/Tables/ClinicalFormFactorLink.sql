﻿CREATE TABLE [dbo].[ClinicalFormFactorLink](
	[MultiFieldId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FactorId1] [int] NOT NULL,
	[FactorId2] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CombineChars] [varchar](5) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ConditionFlag](
	[ConditionFlagIndex] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ConditionActionIndex] [int] NOT NULL,
	[FlagID] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Subject] [nvarchar](255) NULL,
	[Message] [nvarchar](max) NULL,
	[EmailToRequestingClinician] [bit] NULL,
	[ICEMailToRequestingClinician] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
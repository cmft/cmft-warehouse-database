﻿CREATE TABLE [dbo].[OpenNet_Remote_Registrations](
	[Registration_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Remote_Server] [varchar](255) NOT NULL,
	[Secure] [bit] NOT NULL,
	[Host_Organisation_Code] [varchar](6) NOT NULL,
	[Remote_Organisation_Code] [varchar](6) NOT NULL,
	[Remote_Organisation_Name] [varchar](100) NULL,
	[GUID] [uniqueidentifier] NULL,
	[Contact_User_Index] [int] NOT NULL,
	[Approved] [bit] NULL,
	[Certificate_Serial] [varchar](40) NULL,
	[Service_Type] [varchar](20) NOT NULL,
	[Alias] [varchar](10) NULL
) ON [PRIMARY]
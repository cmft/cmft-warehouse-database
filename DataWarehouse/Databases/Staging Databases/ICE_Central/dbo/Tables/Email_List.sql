﻿CREATE TABLE [dbo].[Email_List](
	[Email_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SMTP_Address] [varchar](255) NULL,
	[Subject] [varchar](500) NULL,
	[Body] [text] NULL,
	[Date_Added] [datetime] NULL,
	[SMTP_From_Address] [varchar](255) NULL,
	[SMTP_Server] [varchar](100) NULL,
	[Date_Processed] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_ImpExp_Comments](
	[Service_ImpExp_Comment_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Service_ImpExp_ID] [int] NULL,
	[Service_ImpExp_Comment] [varchar](350) NULL,
	[Service_ImpExp_Process] [varchar](10) NULL,
	[Code] [int] NULL,
	[Date_Added] [datetime] NULL,
	[Service_ImpExp_Msg_Id] [int] NULL
) ON [PRIMARY]
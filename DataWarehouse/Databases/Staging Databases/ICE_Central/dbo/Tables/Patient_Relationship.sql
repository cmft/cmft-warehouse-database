﻿CREATE TABLE [dbo].[Patient_Relationship](
	[Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NULL CONSTRAINT [DF_Active_Patient_Relationship]  DEFAULT ((1))
) ON [PRIMARY]
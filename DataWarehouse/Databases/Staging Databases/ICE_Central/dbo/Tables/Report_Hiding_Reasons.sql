﻿CREATE TABLE [dbo].[Report_Hiding_Reasons](
	[Reason_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Reason_Code] [varchar](5) NOT NULL,
	[Reason] [varchar](50) NOT NULL,
	[User_Selectable] [bit] NOT NULL,
	[Interim_Replaced_Reason] [bit] NOT NULL,
	[Final_Replaced_Reason] [bit] NOT NULL,
	[VIP_Hide_Reason] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Default] [bit] NOT NULL,
	[Display_On_Linked_Reports] [bit] NOT NULL CONSTRAINT [DF_Report_Hiding_Reasons_Display_On_Linked_Reports]  DEFAULT (1)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Discharge_Checklist_Descriptions](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[description] [varchar](100) NOT NULL
) ON [PRIMARY]
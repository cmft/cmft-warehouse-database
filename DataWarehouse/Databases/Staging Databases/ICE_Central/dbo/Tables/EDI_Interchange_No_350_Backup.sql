﻿CREATE TABLE [dbo].[EDI_Interchange_No_350_Backup](
	[Ref_Index] [int] NOT NULL,
	[EDI_Msg_Format] [varchar](41) NOT NULL,
	[EDI_Last_Interchange] [int] NULL
) ON [PRIMARY]
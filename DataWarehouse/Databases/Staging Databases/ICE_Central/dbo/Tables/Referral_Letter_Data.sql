﻿CREATE TABLE [dbo].[Referral_Letter_Data](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[specialty_id] [int] NOT NULL,
	[clinic_id] [varchar](10) NULL,
	[header] [varchar](4000) NULL,
	[footer] [varchar](4000) NULL
) ON [PRIMARY]
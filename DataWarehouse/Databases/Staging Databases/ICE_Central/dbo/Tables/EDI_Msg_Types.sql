﻿CREATE TABLE [dbo].[EDI_Msg_Types](
	[Organisation] [varchar](10) NOT NULL,
	[EDI_Org_NatCode] [varchar](10) NOT NULL,
	[EDI_Msg_IO] [char](1) NULL,
	[EDI_Msg_Format] [varchar](41) NOT NULL,
	[EDI_Delivery_Method] [char](1) NULL,
	[EDI_Last_Interchange] [char](10) NULL,
	[EDI_Encrypt_Enabled] [bit] NOT NULL,
	[EDI_Acks_Active] [bit] NOT NULL,
	[EDI_Msg_Active] [bit] NOT NULL,
	[EDI_Msg_Test] [bit] NOT NULL,
	[EDI_Batch] [varchar](2) NULL,
	[EDI_Anonymize_Data] [bit] NOT NULL,
	[EDI_Redirect_To] [varchar](10) NULL,
	[EDI_Copy_To] [varchar](10) NULL,
	[FormatIndex] [int] NULL
) ON [PRIMARY]
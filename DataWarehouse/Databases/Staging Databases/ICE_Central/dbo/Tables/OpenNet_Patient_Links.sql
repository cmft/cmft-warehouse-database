﻿CREATE TABLE [dbo].[OpenNet_Patient_Links](
	[Patient_Id_Key] [int] NOT NULL,
	[Remote_Registration] [int] NOT NULL,
	[Remote_Patient_Id_Key] [int] NOT NULL,
	[Match_Type] [smallint] NOT NULL,
	[Chosen_From_List] [bit] NOT NULL
) ON [PRIMARY]
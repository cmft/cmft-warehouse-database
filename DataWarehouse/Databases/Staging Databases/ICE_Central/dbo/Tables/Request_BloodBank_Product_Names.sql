﻿CREATE TABLE [dbo].[Request_BloodBank_Product_Names](
	[Product_Name_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Product_Code] [varchar](20) NOT NULL,
	[Product_Name] [varchar](50) NULL,
	[organisation] [varchar](6) NOT NULL,
	[picklist_index] [int] NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
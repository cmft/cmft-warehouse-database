﻿CREATE TABLE [dbo].[Reports_For_Download](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Clinician_National_Code] [varchar](10) NOT NULL,
	[Report_Index] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL,
	[Process_indicator] [bit] NOT NULL CONSTRAINT [DF_Reports_For_Download_Process_indicator]  DEFAULT (0),
	[Location_National_Code] [varchar](20) NULL
) ON [PRIMARY]
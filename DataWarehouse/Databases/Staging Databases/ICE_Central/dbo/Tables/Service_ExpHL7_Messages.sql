﻿CREATE TABLE [dbo].[Service_ExpHL7_Messages](
	[Service_ExpHL7_Message_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Service_ExpHL7_ID] [int] NULL,
	[Service_Message_ID] [varchar](50) NULL,
	[Patient_Local_ID] [varchar](30) NULL,
	[Service_ID] [varchar](40) NULL,
	[Patient_Name] [varchar](70) NULL,
	[Patient_Sex] [int] NULL,
	[Patient_DOB] [datetime] NULL,
	[Service_Date_Of_Service] [datetime] NULL,
	[Date_Added] [datetime] NULL,
	[Destination] [varchar](30) NULL,
	[Service_Report_Index] [int] NULL,
	[Discipline] [int] NULL,
	[Report_Identifier] [varchar](35) NULL
) ON [PRIMARY]
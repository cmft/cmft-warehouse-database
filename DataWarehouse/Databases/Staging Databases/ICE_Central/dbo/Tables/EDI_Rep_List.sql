﻿CREATE TABLE [dbo].[EDI_Rep_List](
	[EDI_Report_Index] [int] NOT NULL,
	[EDI_Provider_Org] [varchar](6) NULL,
	[EDI_Loc_Nat_Code_To] [varchar](10) NULL,
	[EDI_Individual_Index_To] [int] NULL,
	[EDI_Service_ID] [varchar](35) NULL,
	[Date_Added] [datetime] NULL,
	[EDI_LTS_Index] [int] NULL,
	[EDI_Rep_Specialty] [int] NULL,
	[Extra_Copy_To] [varchar](10) NULL,
	[Report_Class] [tinyint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EDI_UOM_Nat_Codes](
	[EDIUOM_Index] [int] NOT NULL,
	[EDIUOM_NatCode] [varchar](35) NULL,
	[EDIUOM_Rubric] [varchar](50) NULL
) ON [PRIMARY]
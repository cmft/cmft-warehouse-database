﻿CREATE TABLE [dbo].[User_Configurable_DataSets](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupName] [varchar](100) NOT NULL,
	[description] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
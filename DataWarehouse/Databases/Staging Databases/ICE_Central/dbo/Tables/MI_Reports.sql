﻿CREATE TABLE [dbo].[MI_Reports](
	[Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Code] [varchar](30) NOT NULL,
	[CategoryCode] [varchar](10) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[Enabled] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ConditionTrigger](
	[ConditionTriggerIndex] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ConditionFlagIndex] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[UserIndex] [int] NULL,
	[AlertTypeIndex] [int] NULL,
	[PatientListIndex] [int] NULL,
	[TargetQueue] [nvarchar](255) NULL,
	[SubjectRubric] [nvarchar](255) NULL,
	[MessageRubric] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
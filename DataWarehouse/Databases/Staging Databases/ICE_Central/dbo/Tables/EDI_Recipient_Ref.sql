﻿CREATE TABLE [dbo].[EDI_Recipient_Ref](
	[Ref_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[EDI_Trader_Account] [varchar](10) NULL,
	[EDI_Free_Part] [varchar](5) NULL,
	[Link_Interchange_Nos] [bit] NOT NULL DEFAULT (1)
) ON [PRIMARY]
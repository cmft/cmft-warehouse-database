﻿CREATE TABLE [dbo].[PAS_History](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Organisation] [varchar](6) NOT NULL,
	[Source_Identifier] [varchar](20) NOT NULL,
	[Trx_Identifier] [varchar](20) NOT NULL,
	[Trx_DateTime] [datetime] NOT NULL,
	[Trx_Type] [varchar](8) NOT NULL,
	[Patient_Identifier] [varchar](24) NOT NULL,
	[Trx_Data] [text] NULL,
	[Information] [varchar](50) NULL,
	[Date_Added] [datetime] NOT NULL,
	[Errored] [bit] NOT NULL,
	[Error_Message] [varchar](50) NULL,
	[System_Provider_Index] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Request_Panels](
	[PanelID] [smallint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PanelName] [varchar](20) NULL,
	[PanelType] [smallint] NULL,
	[Sequence] [smallint] NULL,
	[organisation] [varchar](6) NOT NULL,
	[Restricted] [bit] NOT NULL
) ON [PRIMARY]
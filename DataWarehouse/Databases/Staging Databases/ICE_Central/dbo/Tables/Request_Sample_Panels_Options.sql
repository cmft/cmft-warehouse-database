﻿CREATE TABLE [dbo].[Request_Sample_Panels_Options](
	[Sample_Panel_ID] [int] NOT NULL,
	[Option_ID] [int] NOT NULL,
	[Sequence] [smallint] NOT NULL,
	[Default] [bit] NOT NULL,
	[GP] [bit] NOT NULL
) ON [PRIMARY]
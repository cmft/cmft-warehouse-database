﻿CREATE TABLE [dbo].[Patient_Local_Id_Type](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Type_Code] [varchar](10) NOT NULL,
	[Active] [bit] NULL
) ON [PRIMARY]
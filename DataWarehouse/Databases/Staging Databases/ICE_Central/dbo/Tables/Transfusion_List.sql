﻿CREATE TABLE [dbo].[Transfusion_List](
	[Transfusion_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Patient_Index] [int] NOT NULL,
	[Order_Index] [int] NULL,
	[Date_Added] [datetime] NOT NULL,
	[Date_Fated] [datetime] NULL,
	[Transfuser_Id] [int] NULL
) ON [PRIMARY]
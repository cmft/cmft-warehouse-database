﻿CREATE TABLE [dbo].[Blood_Products](
	[Product_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Code] [varchar](5) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Pack_Division] [varchar](20) NOT NULL,
	[Lifespan_ToFridge_RT] [int] NULL,
	[Lifespan_ToFridge_CB] [int] NULL,
	[Group_Index] [int] NULL,
	[Active] [bit] NOT NULL,
	[Lifespan_ToPatient_RT] [int] NULL,
	[Lifespan_ToPatient_CB] [int] NULL,
	[Transport_at_RT] [bit] NOT NULL,
	[Transport_in_CB] [bit] NOT NULL
) ON [PRIMARY]
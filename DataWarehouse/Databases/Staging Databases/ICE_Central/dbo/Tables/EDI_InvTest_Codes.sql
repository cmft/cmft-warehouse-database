﻿CREATE TABLE [dbo].[EDI_InvTest_Codes](
	[Organisation] [varchar](6) NOT NULL,
	[EDI_Index] [int] IDENTITY(1,1) NOT NULL,
	[EDI_LTS_Index] [int] NOT NULL,
	[EDI_Local_Test_Code] [varchar](10) NOT NULL,
	[EDI_Local_Rubric] [varchar](35) NOT NULL,
	[EDI_READ_Code] [varchar](6) NULL,
	[EDI_Sample_TypeCode] [varchar](5) NOT NULL,
	[EDI_Read_Status] [smallint] NULL,
	[EDI_OP_UOM] [varchar](15) NULL,
	[EDI_OP_Active] [bit] NOT NULL,
	[EDI_OP_Suppress] [bit] NOT NULL,
	[Review_Level] [smallint] NULL,
	[EDI_Loc_Code_Binary] [binary](10) NOT NULL
) ON [PRIMARY]
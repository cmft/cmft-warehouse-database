﻿CREATE TABLE [dbo].[PA_Patient_Alert](
	[Alert_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Type_Index] [int] NOT NULL,
	[Patient_Id_Key] [int] NOT NULL,
	[Expiry_Date] [smalldatetime] NULL,
	[Lives_Remaining] [tinyint] NOT NULL,
	[User_Entered_Text] [text] NULL,
	[Is_Active]  AS (case when ([Lives_Remaining] > 0 and (([Expiry_Date] is null or [Expiry_Date] > getutcdate()))) then 1 else 0 end),
	[Last_Updated] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
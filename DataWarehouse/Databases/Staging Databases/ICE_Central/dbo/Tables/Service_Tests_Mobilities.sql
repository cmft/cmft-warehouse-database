﻿CREATE TABLE [dbo].[Service_Tests_Mobilities](
	[Mobility_Index] [int] NOT NULL,
	[Mobility_Caption] [varchar](30) NULL,
	[Mobility_Order] [int] NULL,
	[Mobility_OrgCode] [varchar](6) NULL
) ON [PRIMARY]
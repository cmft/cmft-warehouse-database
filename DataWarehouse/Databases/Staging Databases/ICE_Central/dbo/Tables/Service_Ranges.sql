﻿CREATE TABLE [dbo].[Service_Ranges](
	[Range_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Result_Index] [int] NOT NULL,
	[Lower_Range] [varchar](35) NULL,
	[Upper_Range] [varchar](35) NULL,
	[Type] [varchar](3) NULL,
	[Unit_Code] [varchar](14) NULL,
	[Unit_Text] [varchar](35) NULL,
	[Comment_Marker] [bit] NOT NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
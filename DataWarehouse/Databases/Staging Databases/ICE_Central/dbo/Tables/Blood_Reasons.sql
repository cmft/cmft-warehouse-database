﻿CREATE TABLE [dbo].[Blood_Reasons](
	[Reason_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Type] [char](1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Default] [bit] NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
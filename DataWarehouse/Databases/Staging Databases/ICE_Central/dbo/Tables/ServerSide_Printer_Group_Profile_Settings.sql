﻿CREATE TABLE [dbo].[ServerSide_Printer_Group_Profile_Settings](
	[Setting_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Group_Index] [int] NOT NULL,
	[Profile_Index] [int] NOT NULL,
	[Margin_Top] [real] NULL,
	[Margin_Bottom] [real] NULL,
	[Margin_Left] [real] NULL,
	[Margin_Right] [real] NULL,
	[Portrait] [bit] NOT NULL,
	[PatientDetailsHeader] [bit] NOT NULL,
	[PagesFooter] [bit] NOT NULL,
	[Collate] [bit] NOT NULL,
	[Duplex] [tinyint] NULL,
	[PaperSize] [varchar](50) NULL,
	[PaperSource] [varchar](50) NULL
) ON [PRIMARY]
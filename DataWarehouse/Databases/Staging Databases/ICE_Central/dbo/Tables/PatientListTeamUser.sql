﻿CREATE TABLE [dbo].[PatientListTeamUser](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TeamIndex] [int] NOT NULL,
	[UserIndex] [int] NOT NULL,
	[Manager] [bit] NOT NULL,
	[ExpiryDate] [datetime] NULL
) ON [PRIMARY]
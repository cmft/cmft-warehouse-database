﻿CREATE TABLE [dbo].[Patient_Notepad](
	[Note_ID] [int] IDENTITY(1,1) NOT NULL,
	[Patient_ID] [int] NOT NULL,
	[User_Type] [tinyint] NOT NULL,
	[Note] [varchar](8000) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Private] [bit] NOT NULL,
	[organisation] [varchar](6) NOT NULL,
	[User_Index] [int] NOT NULL,
	[Date_Created] [datetime] NULL,
	[Last_Updated] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Spine_Actions_Required](
	[MessageGUID] [uniqueidentifier] NOT NULL,
	[ResponseGUID] [uniqueidentifier] NULL,
	[MessageType] [varchar](15) NOT NULL,
	[ProcessingCode] [char](1) NOT NULL,
	[ProcessingModeCode] [char](1) NOT NULL,
	[Status] [char](1) NOT NULL,
	[Retries] [tinyint] NOT NULL,
	[Priority] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
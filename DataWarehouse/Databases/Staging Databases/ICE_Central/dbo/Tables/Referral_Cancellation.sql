﻿CREATE TABLE [dbo].[Referral_Cancellation](
	[id] [int] NOT NULL,
	[user_id] [varchar](10) NOT NULL,
	[role] [tinyint] NOT NULL,
	[reason] [smallint] NOT NULL,
	[create_dt] [datetime] NOT NULL
) ON [PRIMARY]
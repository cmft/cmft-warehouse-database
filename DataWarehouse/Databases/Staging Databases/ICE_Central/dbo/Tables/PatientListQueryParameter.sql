﻿CREATE TABLE [dbo].[PatientListQueryParameter](
	[ParameterIndex] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ListIndex] [int] NOT NULL,
	[ParameterType] [int] NOT NULL,
	[Value] [varchar](100) NULL
) ON [PRIMARY]
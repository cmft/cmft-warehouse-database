﻿CREATE TABLE [dbo].[Request_Test_Collections](
	[Collection_Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[BackColour] [int] NULL,
	[Help_Text] [varchar](140) NULL,
	[Help_Colour] [int] NULL,
	[Help_BackColour] [int] NULL,
	[Organisation] [varchar](6) NULL,
	[Exclude_Personal_Panel] [bit] NOT NULL CONSTRAINT [DF_Request_Test_Collections_Exclude_Personal_Panel]  DEFAULT (0),
	[Exclude_Select_All] [bit] NOT NULL CONSTRAINT [DF_Request_Test_Collections_Exclude_Select_All]  DEFAULT (0),
	[Exclude_DeSelect_All] [bit] NOT NULL CONSTRAINT [DF_Request_Test_Collections_Exclude_DeSelect_All]  DEFAULT (0),
	[Select_All_By_Default] [bit] NOT NULL CONSTRAINT [DF_Request_Test_Collections_Select_All_By_Default]  DEFAULT (0)
) ON [PRIMARY]
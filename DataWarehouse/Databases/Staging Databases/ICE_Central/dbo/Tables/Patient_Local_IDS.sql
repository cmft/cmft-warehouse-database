﻿CREATE TABLE [dbo].[Patient_Local_IDS](
	[Patient_Id_key] [int] NOT NULL,
	[Patient_Local_id] [varchar](30) NOT NULL,
	[Org_Code] [varchar](6) NULL,
	[Main_Identifier] [bit] NULL CONSTRAINT [main_identifier_default]  DEFAULT (0),
	[Active_Casenote] [bit] NOT NULL DEFAULT (0),
	[Hidden] [bit] NOT NULL DEFAULT (0),
	[HospitalNumber] [varchar](24) NULL,
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Retired] [bit] NOT NULL CONSTRAINT [DF_Patient_Local_IDS_Retired]  DEFAULT (0),
	[Type_Id] [int] NULL
) ON [PRIMARY]
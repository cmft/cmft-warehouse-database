﻿CREATE TABLE [dbo].[ClinicalFormTemplateImage](
	[TemplateId] [int] NOT NULL,
	[ImageId] [int] NOT NULL,
	[Default] [bit] NOT NULL CONSTRAINT [DF_ClinicalFormTemplateImage_Default]  DEFAULT ((0))
) ON [PRIMARY]
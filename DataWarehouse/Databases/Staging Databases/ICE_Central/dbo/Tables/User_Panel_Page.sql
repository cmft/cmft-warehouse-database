﻿CREATE TABLE [dbo].[User_Panel_Page](
	[Page_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[User_Index] [int] NOT NULL,
	[Panel_ID] [smallint] NOT NULL,
	[Page_Name] [varchar](15) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Interop_Data_For_Download](
	[Clinician_NatCode] [varchar](10) NOT NULL,
	[Clinician_LocalCode] [varchar](16) NULL,
	[Service_Type] [varchar](10) NOT NULL,
	[Service_Index] [int] NOT NULL,
	[User_Code] [int] NOT NULL,
	[Tracker_GUID] [uniqueidentifier] NOT NULL,
	[DateTime_Added] [datetime] NOT NULL,
	[DateTime_Downloaded] [datetime] NULL,
	[MessageString] [text] NOT NULL,
	[Location_Index] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
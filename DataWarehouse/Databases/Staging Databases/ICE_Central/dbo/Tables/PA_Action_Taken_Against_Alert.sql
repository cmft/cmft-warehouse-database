﻿CREATE TABLE [dbo].[PA_Action_Taken_Against_Alert](
	[Action_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Alert_Index] [int] NOT NULL,
	[Date_Taken] [datetime] NOT NULL,
	[Taken_By] [int] NOT NULL,
	[User_Entered_Text] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
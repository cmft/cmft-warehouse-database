﻿CREATE TABLE [dbo].[ClinicalFormEntryData](
	[DataId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[EntryId] [int] NOT NULL,
	[FactorId] [int] NOT NULL,
	[Score] [smallint] NULL,
	[OutOfRange] [bit] NOT NULL,
	[Value] [varchar](2000) NULL
) ON [PRIMARY]
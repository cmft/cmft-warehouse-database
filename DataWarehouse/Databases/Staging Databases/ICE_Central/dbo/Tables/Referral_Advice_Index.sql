﻿CREATE TABLE [dbo].[Referral_Advice_Index](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[referral_id] [int] NOT NULL,
	[status] [smallint] NOT NULL
) ON [PRIMARY]
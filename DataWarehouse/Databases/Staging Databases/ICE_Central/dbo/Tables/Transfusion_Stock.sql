﻿CREATE TABLE [dbo].[Transfusion_Stock](
	[Stock_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Unit_Code] [varchar](20) NOT NULL,
	[Product_Index] [int] NOT NULL,
	[Blood_Group_Index] [smallint] NOT NULL,
	[Fridge_Index] [smallint] NULL,
	[Expiry_DateTime] [datetime] NULL,
	[Transfusion_Index] [int] NULL
) ON [PRIMARY]
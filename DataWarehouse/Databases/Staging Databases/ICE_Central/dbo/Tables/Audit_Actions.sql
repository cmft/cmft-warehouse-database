﻿CREATE TABLE [dbo].[Audit_Actions](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[application_id] [int] NOT NULL,
	[description] [varchar](100) NOT NULL
) ON [PRIMARY]
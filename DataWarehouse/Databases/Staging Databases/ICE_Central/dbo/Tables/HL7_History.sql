﻿CREATE TABLE [dbo].[HL7_History](
	[HL7_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Source_App] [varchar](20) NULL,
	[Source_ID] [varchar](20) NULL,
	[Message_No] [varchar](20) NULL,
	[Transaction_Type] [varchar](7) NULL,
	[Patient_Id] [varchar](20) NULL,
	[Organisation] [varchar](6) NULL,
	[Date_added] [datetime] NULL,
	[Process_Indicator] [bit] NOT NULL,
	[Message] [varchar](5000) NULL,
	[Date_Processed] [datetime] NULL
) ON [PRIMARY]
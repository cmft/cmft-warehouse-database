﻿CREATE TABLE [dbo].[Request_Forms](
	[Abstract_Name] [varchar](10) NOT NULL,
	[Form] [varchar](50) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[LabelForm] [bit] NOT NULL CONSTRAINT [DF_LabelForm_Request_Forms]  DEFAULT ((0))
) ON [PRIMARY]
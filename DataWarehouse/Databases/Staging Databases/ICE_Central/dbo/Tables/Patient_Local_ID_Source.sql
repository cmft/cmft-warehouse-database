﻿CREATE TABLE [dbo].[Patient_Local_ID_Source](
	[Patient_Local_ID_Key] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Source_ID] [int] NOT NULL,
	[System_Index] [int] NULL,
	[Assigning_Authority] [varchar](50) NULL,
	[Type_Id] [int] NULL,
	[User_Index] [int] NULL,
	[Date_Added] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Transaction](
	[TransactionID] [uniqueidentifier] NOT NULL,
	[ParentID] [uniqueidentifier] NULL,
	[ApplicationID] [int] NOT NULL,
	[StartServer] [varchar](20) NULL,
	[StartTimestamp] [datetime] NULL,
	[EndServer] [varchar](20) NULL,
	[EndTimestamp] [datetime] NULL,
	[UserId] [int] NOT NULL CONSTRAINT [DF_Transaction_UserId]  DEFAULT (0),
	[HasArchived_Data] [bit] NOT NULL CONSTRAINT [DF_Transaction_HasArchived_Data]  DEFAULT (0)
) ON [PRIMARY]
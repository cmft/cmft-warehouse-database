﻿CREATE TABLE [dbo].[ConditionAction](
	[ConditionActionIndex] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[ConditionActive] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
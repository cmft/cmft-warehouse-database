﻿CREATE TABLE [dbo].[Connect_Modules](
	[Module_Name] [varchar](20) NULL,
	[Connection_Name] [varchar](30) NULL,
	[Module_Launch_Folder] [varchar](128) NULL,
	[Monitor_Source] [varchar](128) NULL,
	[Module_Active] [bit] NOT NULL
) ON [PRIMARY]
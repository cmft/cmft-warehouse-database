﻿CREATE TABLE [dbo].[Service_Requests_Status](
	[Service_Request_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Status_Index] [tinyint] NOT NULL,
	[Date_Added] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Referral_Protocol_Response](
	[id] [int] NOT NULL,
	[protocol_id] [int] NOT NULL,
	[response_uid] [int] NOT NULL,
	[response] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
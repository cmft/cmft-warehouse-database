﻿CREATE TABLE [dbo].[BloodCollectionNoteConfirmation](
	[ConfirmationIndex] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Clinical_Letter_Paragraph_Templates](
	[Paragraph_Template_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Letter_Template_Index] [int] NOT NULL,
	[Paragraph_Title] [varchar](255) NOT NULL,
	[Order] [tinyint] NOT NULL,
	[Mode] [char](1) NOT NULL,
	[Predefined_Options] [bit] NOT NULL,
	[inherit_content] [bit] NOT NULL DEFAULT (0),
	[Max_Length] [int] NULL,
	[Code] [varchar](20) NULL,
	[Include_In_TTO_Print] [bit] NOT NULL CONSTRAINT [DF_Clinical_Letter_Paragraph_Templates_Include_In_TTO_Print]  DEFAULT (0),
	[SuppressTTOParagraph] [bit] NOT NULL CONSTRAINT [DF_SuppressTTOParagraph_Clinical_Letter_Paragraph_Templates]  DEFAULT ((0))
) ON [PRIMARY]
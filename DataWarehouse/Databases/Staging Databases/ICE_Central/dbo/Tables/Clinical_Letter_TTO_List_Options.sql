﻿CREATE TABLE [dbo].[Clinical_Letter_TTO_List_Options](
	[Option_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Option_Type] [char](1) NOT NULL,
	[Option_Code] [varchar](50) NOT NULL,
	[Option_Text] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
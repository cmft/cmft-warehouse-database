﻿CREATE TABLE [dbo].[ICD9](
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](250) NOT NULL,
	[Active_Date] [smalldatetime] NOT NULL,
	[Last_Modified] [smalldatetime] NULL,
	[Active_Marker] [bit] NOT NULL,
	[Inactive_Date] [smalldatetime] NULL
) ON [PRIMARY]
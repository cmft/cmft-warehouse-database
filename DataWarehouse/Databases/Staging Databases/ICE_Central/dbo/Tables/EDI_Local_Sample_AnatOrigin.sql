﻿CREATE TABLE [dbo].[EDI_Local_Sample_AnatOrigin](
	[National_Code] [char](5) NOT NULL,
	[Local_Text] [varchar](50) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
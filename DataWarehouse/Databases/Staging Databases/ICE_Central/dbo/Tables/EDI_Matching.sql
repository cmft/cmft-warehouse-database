﻿CREATE TABLE [dbo].[EDI_Matching](
	[MatchIndex] [int] IDENTITY(1,1) NOT NULL,
	[Individual_Index] [int] NULL,
	[EDI_Local_Key1] [varchar](30) NULL,
	[EDI_Local_Key2] [int] NULL,
	[EDI_Local_Key3] [varchar](30) NULL,
	[EDI_Op_Name] [varchar](35) NULL,
	[Matching_Active] [bit] NULL,
	[EDI_SMTP_Active] [bit] NULL
) ON [PRIMARY]
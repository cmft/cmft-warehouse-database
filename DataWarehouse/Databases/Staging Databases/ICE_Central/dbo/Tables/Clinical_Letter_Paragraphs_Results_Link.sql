﻿CREATE TABLE [dbo].[Clinical_Letter_Paragraphs_Results_Link](
	[Results_Link_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Paragraph_Index] [int] NOT NULL,
	[Investigation_Index] [int] NOT NULL,
	[Result_Index] [int] NULL,
	[Results] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
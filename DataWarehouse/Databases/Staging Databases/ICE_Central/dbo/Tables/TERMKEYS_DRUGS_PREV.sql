﻿CREATE TABLE [dbo].[TERMKEYS_DRUGS_PREV](
	[TERM_KEY_V2] [varchar](10) NULL,
	[TERM_V2] [varchar](30) NULL,
	[TERM_CODE_V2] [varchar](2) NULL,
	[READ_CODE_V2] [varchar](8) NULL,
	[TTO] [bit] NOT NULL,
	[SPEC_FLAGS_V2] [varchar](10) NULL,
	[OPCS] [varchar](20) NULL,
	[ICD10] [varchar](20) NULL,
	[Date_Added] [datetime] NULL,
	[Security_Level] [int] NOT NULL,
	[Restricted] [bit] NULL,
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL
) ON [PRIMARY]
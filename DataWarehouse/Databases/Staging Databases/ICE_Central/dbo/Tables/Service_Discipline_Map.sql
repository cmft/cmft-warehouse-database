﻿CREATE TABLE [dbo].[Service_Discipline_Map](
	[Discipline_Index] [int] IDENTITY(1,1) NOT NULL,
	[Discipline_Text] [varchar](5) NULL,
	[Specialty_Code] [varchar](5) NULL,
	[Discipline_Expansion] [varchar](50) NULL,
	[Display] [bit] NULL,
	[Is_Numeric] [bit] NOT NULL CONSTRAINT [DF_Service_Discipline_Map_Is_Numeric]  DEFAULT (1),
	[Specialty_Text] [varchar](5) NULL,
	[Report_Header_Text] [varchar](100) NULL,
	[Use_Report_Filing_Message] [bit] NOT NULL CONSTRAINT [DF_Service_Discipline_Map_Use_Report_Filing_Message]  DEFAULT (1),
	[Report_Filing_Message] [varchar](500) NULL,
	[Auto_File_Popup] [bit] NULL
) ON [PRIMARY]
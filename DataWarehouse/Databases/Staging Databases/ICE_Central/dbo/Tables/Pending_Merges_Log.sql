﻿CREATE TABLE [dbo].[Pending_Merges_Log](
	[index] [int] IDENTITY(1,1) NOT NULL,
	[Pending_Merge_Index] [int] NOT NULL,
	[success] [bit] NOT NULL,
	[error_number] [bigint] NOT NULL,
	[error_text] [varchar](300) NOT NULL,
	[Date_Added] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalFormGroup](
	[GroupId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
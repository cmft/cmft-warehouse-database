﻿CREATE TABLE [dbo].[HL7_Failure](
	[Fail_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Report_Index] [int] NULL,
	[Output_Feed] [int] NULL,
	[Ward] [int] NULL,
	[Specialty] [varchar](4) NULL,
	[Clinic_Name] [varchar](35) NULL,
	[New_NHS_No] [varchar](10) NULL,
	[HospitalNumber] [varchar](20) NULL,
	[Surname] [varchar](35) NULL,
	[Forename] [varchar](35) NULL,
	[Midname] [varchar](35) NULL,
	[Date_of_Birth] [datetime] NULL,
	[Mail_Active] [bit] NULL,
	[Mail_Address] [varchar](100) NULL,
	[HL7_Index] [int] NULL,
	[Patient_Id] [varchar](20) NULL,
	[NHS_No] [varchar](10) NULL,
	[Failing_Process] [varchar](50) NULL,
	[Comments] [text] NULL,
	[Date_of_Failure] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
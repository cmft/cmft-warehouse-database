﻿CREATE TABLE [dbo].[MSHV_App_Registration](
	[index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ApplicationName] [varchar](100) NOT NULL,
	[ApplicationID] [varchar](50) NOT NULL
) ON [PRIMARY]
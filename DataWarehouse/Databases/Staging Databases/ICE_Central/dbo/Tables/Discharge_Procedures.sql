﻿CREATE TABLE [dbo].[Discharge_Procedures](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[procedure_code] [varchar](6) NOT NULL,
	[specialty_id] [smallint] NOT NULL,
	[codetype] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[US_States](
	[State_Index] [smallint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[State_Code] [varchar](2) NOT NULL,
	[State] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Blood_Product_Group_Type_Mappings](
	[Product_Group_Index] [int] NOT NULL,
	[Stock_Blood_Type_Index] [int] NOT NULL,
	[Patient_Blood_Type_Index] [int] NOT NULL,
	[Match_Type] [char](1) NOT NULL
) ON [PRIMARY]
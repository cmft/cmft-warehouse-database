﻿CREATE TABLE [dbo].[OpenNet_Registrations](
	[Registration_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Host_Organisation_Code] [varchar](6) NOT NULL,
	[Remote_Organisation_Code] [varchar](6) NOT NULL,
	[Name] [varchar](100) NULL,
	[Remote_Server] [varchar](255) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[Approved] [bit] NULL,
	[Certificate_Serial] [varchar](40) NULL,
	[User_Index] [int] NULL,
	[Service_Type] [varchar](20) NOT NULL
) ON [PRIMARY]
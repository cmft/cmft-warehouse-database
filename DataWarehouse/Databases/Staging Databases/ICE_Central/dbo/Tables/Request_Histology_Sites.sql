﻿CREATE TABLE [dbo].[Request_Histology_Sites](
	[Site_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Site] [varchar](30) NOT NULL
) ON [PRIMARY]
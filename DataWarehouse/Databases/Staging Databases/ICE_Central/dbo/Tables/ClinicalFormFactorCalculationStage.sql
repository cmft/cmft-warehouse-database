﻿CREATE TABLE [dbo].[ClinicalFormFactorCalculationStage](
	[StageIndex] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FactorId] [int] NOT NULL,
	[Order] [smallint] NOT NULL,
	[LhsType] [tinyint] NOT NULL,
	[Lhs] [decimal](13, 3) NOT NULL,
	[RhsType] [tinyint] NOT NULL,
	[Rhs] [decimal](13, 3) NOT NULL,
	[Operator] [tinyint] NOT NULL
) ON [PRIMARY]
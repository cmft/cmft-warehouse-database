﻿CREATE TABLE [dbo].[Request_Test_Help_URLs](
	[Url_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Test_Name] [varchar](50) NOT NULL,
	[Test_Url] [varchar](255) NOT NULL
) ON [PRIMARY]
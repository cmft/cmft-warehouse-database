﻿CREATE TABLE [dbo].[Clinical_Letter_Paragraphs](
	[Paragraph_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Letter_Index] [int] NOT NULL,
	[Paragraph_Template_Index] [int] NULL,
	[Paragraph_Title] [varchar](255) NOT NULL,
	[Paragraph_Text] [text] NULL,
	[Paragraph_Order] [tinyint] NOT NULL,
	[Alignment] [tinyint] NOT NULL,
	[unconfirmed_content] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
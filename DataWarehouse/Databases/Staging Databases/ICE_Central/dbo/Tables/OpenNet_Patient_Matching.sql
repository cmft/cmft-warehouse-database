﻿CREATE TABLE [dbo].[OpenNet_Patient_Matching](
	[Match_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Registration] [int] NOT NULL,
	[Match_Type] [smallint] NOT NULL,
	[Order] [tinyint] NOT NULL,
	[Patient_Identifier_Type_Id] [int] NULL
) ON [PRIMARY]
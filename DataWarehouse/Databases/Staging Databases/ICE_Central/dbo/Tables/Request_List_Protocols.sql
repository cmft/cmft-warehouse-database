﻿CREATE TABLE [dbo].[Request_List_Protocols](
	[Protocol_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Protocol_Text] [varchar](100) NOT NULL,
	[Protocol_Type] [char](1) NOT NULL,
	[Protocol_Active] [bit] NOT NULL,
	[Service_Provider_Index] [int] NOT NULL
) ON [PRIMARY]
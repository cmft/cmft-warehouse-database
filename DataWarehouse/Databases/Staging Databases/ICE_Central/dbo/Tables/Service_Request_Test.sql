﻿CREATE TABLE [dbo].[Service_Request_Test](
	[Service_Request_Id] [int] NOT NULL,
	[Test_Id] [int] NOT NULL,
	[Test_Status] [tinyint] NOT NULL,
	[Service_Request_Test_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ABN_Reqd] [bit] NULL,
	[Last_Updated] [datetime] NULL DEFAULT (getdate()),
	[Repeat_Reason] [varchar](1000) NULL,
	[PriceCode] [money] NULL
) ON [PRIMARY]
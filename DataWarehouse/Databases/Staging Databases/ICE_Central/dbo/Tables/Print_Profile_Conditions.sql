﻿CREATE TABLE [dbo].[Print_Profile_Conditions](
	[Condition_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Profile_Index] [int] NOT NULL,
	[Condition_Type] [varchar](10) NOT NULL,
	[Positivity] [bit] NOT NULL
) ON [PRIMARY]
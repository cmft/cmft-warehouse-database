﻿CREATE TABLE [dbo].[Hosp_Procedure](
	[Proc_key] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Procedure_Number] [int] NULL,
	[Procedure_type] [varchar](1) NULL,
	[Procedure_code] [varchar](7) NULL,
	[Procedure_date] [datetime] NULL,
	[Date_added] [datetime] NULL,
	[Comment_Text] [varchar](5000) NULL,
	[SiteID] [int] NULL,
	[Laterality] [varchar](1) NULL,
	[Episode_Index] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Referral_Pending](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[referrer_id] [varchar](10) NOT NULL,
	[patient_id] [int] NOT NULL,
	[comment] [varchar](2000) NULL,
	[create_dt] [datetime] NOT NULL
) ON [PRIMARY]
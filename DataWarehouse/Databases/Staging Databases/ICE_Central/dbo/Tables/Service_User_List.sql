﻿CREATE TABLE [dbo].[Service_User_List](
	[UserList_Index] [int] NOT NULL,
	[Username] [varchar](25) NULL,
	[Patient_ID] [varchar](30) NULL,
	[Patient_id_key] [int] NULL,
	[Hospital_Number] [varchar](24) NULL,
	[Procedure_Notes] [varchar](255) NULL,
	[General_Notes] [varchar](255) NULL,
	[Active] [bit] NOT NULL,
	[Date_Last_Amended] [datetime] NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientListTab](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UserIndex] [int] NOT NULL,
	[ListIndex] [int] NULL,
	[Order] [int] NOT NULL
) ON [PRIMARY]
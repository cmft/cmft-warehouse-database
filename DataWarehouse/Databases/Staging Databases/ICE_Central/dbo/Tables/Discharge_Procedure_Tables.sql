﻿CREATE TABLE [dbo].[Discharge_Procedure_Tables](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[description] [varchar](100) NOT NULL,
	[table_name] [varchar](100) NOT NULL
) ON [PRIMARY]
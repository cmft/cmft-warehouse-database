﻿CREATE TABLE [dbo].[Investigation_Test_Combination](
	[Combination_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Is_Numeric] [bit] NOT NULL,
	[EDI_LTS_Index] [int] NOT NULL,
	[Investigation_Name_Index] [int] NOT NULL,
	[Test_Name_Index] [int] NOT NULL,
	[Is_Unrestricted] [bit] NOT NULL,
	[Test_Order] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Request_Category](
	[Category_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Category_Default] [bit] NOT NULL,
	[Category_Active] [bit] NOT NULL,
	[Category_Value] [varchar](8) NULL,
	[Category_Desc] [varchar](30) NULL,
	[Organisation] [varchar](6) NOT NULL
) ON [PRIMARY]
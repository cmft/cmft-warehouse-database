﻿CREATE TABLE [dbo].[Patient_Groups](
	[GroupID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupName] [varchar](50) NOT NULL,
	[Organisation] [varchar](6) NOT NULL
) ON [PRIMARY]
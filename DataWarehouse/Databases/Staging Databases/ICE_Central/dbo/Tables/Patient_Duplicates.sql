﻿CREATE TABLE [dbo].[Patient_Duplicates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[GroupKey] [varchar](50) NOT NULL,
	[Patient_ID_Key] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL CONSTRAINT [DF_Patient_Duplicates_Date_Added]  DEFAULT (getdate())
) ON [PRIMARY]
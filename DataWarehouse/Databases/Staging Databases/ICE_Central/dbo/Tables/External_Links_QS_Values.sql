﻿CREATE TABLE [dbo].[External_Links_QS_Values](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Link_ID] [int] NOT NULL,
	[QS_Item] [int] NULL,
	[Name] [varchar](100) NOT NULL,
	[Value] [varchar](100) NULL,
	[RequiresPatient] [bit] NULL
) ON [PRIMARY]
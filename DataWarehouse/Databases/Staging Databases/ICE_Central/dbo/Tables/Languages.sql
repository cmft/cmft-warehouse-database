﻿CREATE TABLE [dbo].[Languages](
	[Language_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Language_Name] [varchar](50) NOT NULL
) ON [PRIMARY]
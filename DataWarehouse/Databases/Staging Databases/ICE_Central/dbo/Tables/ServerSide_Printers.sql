﻿CREATE TABLE [dbo].[ServerSide_Printers](
	[Printer_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Printer_Name] [varchar](100) NOT NULL,
	[Group_Index] [int] NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
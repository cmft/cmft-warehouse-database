﻿CREATE TABLE [dbo].[AssessmentMapItems](
	[MapItemId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MapId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[HelpText] [varchar](255) NULL,
	[TextColour] [int] NULL,
	[BackColour] [int] NULL,
	[XCoord] [int] NOT NULL,
	[YCoord] [int] NOT NULL,
	[SnomedCode] [varchar](12) NULL,
	[Specialty] [varchar](5) NOT NULL,
	[LinkMap] [int] NULL,
	[Active] [bit] NOT NULL,
	[TemplateId] [int] NULL
) ON [PRIMARY]
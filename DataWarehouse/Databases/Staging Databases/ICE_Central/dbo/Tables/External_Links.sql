﻿CREATE TABLE [dbo].[External_Links](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Base_URL] [varchar](255) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL DEFAULT (1)
) ON [PRIMARY]
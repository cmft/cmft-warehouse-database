﻿CREATE TABLE [dbo].[Request_Sample_Panel_Collection_Day_Options](
	[Option_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Requires_Date] [bit] NOT NULL,
	[Day_Adjustment] [smallint] NULL,
	[Show_Calendar] [bit] NOT NULL,
	[Show_Time] [bit] NOT NULL,
	[Use_Current_Time] [bit] NOT NULL,
	[organisation] [varchar](6) NOT NULL,
	[BookIntoClinic] [bit] NOT NULL,
	[BookIntoNextSlot] [bit] NOT NULL,
	[BookPending] [bit] NOT NULL,
	[RestrictByLocation] [bit] NOT NULL DEFAULT (0),
	[TimeMinuteSteps] [tinyint] NULL,
	[Allow_Date_In_Past] [bit] NOT NULL
) ON [PRIMARY]
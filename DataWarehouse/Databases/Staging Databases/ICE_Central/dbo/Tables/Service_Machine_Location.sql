﻿CREATE TABLE [dbo].[Service_Machine_Location](
	[ComputerName] [varchar](50) NOT NULL,
	[Location_Index] [int] NULL,
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL
) ON [PRIMARY]
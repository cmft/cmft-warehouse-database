﻿CREATE TABLE [dbo].[Blood_Product_Groups_Fridge_Permissions](
	[Permission_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Group_Index] [int] NOT NULL,
	[Fridge_Type_Index] [int] NOT NULL,
	[Permitted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[mfGP](
	[GPCode] [varchar](6) NOT NULL,
	[Surname] [varchar](24) NOT NULL,
	[Initials] [varchar](4) NULL,
	[Title] [varchar](4) NULL,
	[HAACode] [varchar](6) NULL,
	[NationalCode] [varchar](8) NULL,
	[Address1] [varchar](20) NULL,
	[Address2] [varchar](20) NULL,
	[Address3] [varchar](20) NULL,
	[Address4] [varchar](20) NULL,
	[PostCode] [varchar](10) NULL,
	[Telephone] [varchar](23) NULL,
	[DOHCode] [varchar](8) NULL,
	[PracticeCode] [varchar](6) NOT NULL,
	[OrgCode] [varchar](5) NULL,
	[ChangeDate] [datetime] NULL
) ON [PRIMARY]
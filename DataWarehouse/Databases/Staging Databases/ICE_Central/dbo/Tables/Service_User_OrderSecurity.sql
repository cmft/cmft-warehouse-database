﻿CREATE TABLE [dbo].[Service_User_OrderSecurity](
	[Order_Code] [varchar](50) NULL,
	[Request_Level] [varchar](50) NULL,
	[Review_Level] [varchar](50) NULL,
	[Review_String] [varchar](255) NULL
) ON [PRIMARY]
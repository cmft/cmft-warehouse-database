﻿CREATE TABLE [dbo].[Service_Tests_Tubes](
	[Tube_Index] [int] NOT NULL,
	[Tube_Code] [varchar](6) NOT NULL,
	[Tube_Name] [varchar](25) NULL,
	[Tube_Description] [varchar](50) NULL,
	[Tube_Vol] [int] NULL,
	[Tube_Vol_Units] [varchar](8) NULL,
	[Tube_Colour] [varchar](25) NULL,
	[Tube_EDI_SampleCode] [varchar](8) NULL,
	[Tube_EDI_SampleText] [varchar](65) NULL,
	[Tube_Stock_Code] [varchar](25) NULL,
	[Tube_Unit_Of_Issue] [varchar](25) NULL,
	[Tube_ReOrder_Qty] [varchar](25) NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
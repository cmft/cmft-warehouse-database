﻿CREATE TABLE [dbo].[ICD10_CPT_Map](
	[Mapping_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Organisation] [varchar](6) NOT NULL,
	[ICD10_Code] [varchar](4) NOT NULL,
	[CPT_Code] [varchar](5) NOT NULL,
	[Active_Date] [smalldatetime] NOT NULL,
	[Active_Marker] [bit] NOT NULL,
	[Inactive_Date] [smalldatetime] NULL
) ON [PRIMARY]
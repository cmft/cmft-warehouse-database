﻿CREATE TABLE [dbo].[Service_User_Roles_Permissions](
	[Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RoleIndex] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[DateAdded] [datetime] NULL,
	[AddedBy] [int] NOT NULL,
	[Class] [nchar](40) NULL
) ON [PRIMARY]
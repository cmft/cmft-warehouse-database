﻿CREATE TABLE [dbo].[Request_Histology_Types](
	[Type_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Type] [varchar](30) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Request_Labels](
	[Label_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Label] [text] NOT NULL,
	[LabelForm] [bit] NOT NULL CONSTRAINT [DF_LabelForm_Request_Labels]  DEFAULT ((0)),
	[Form] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
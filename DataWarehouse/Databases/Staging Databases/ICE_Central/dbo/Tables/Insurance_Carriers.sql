﻿CREATE TABLE [dbo].[Insurance_Carriers](
	[Carrier_Index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Code] [varchar](8) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Address_Line1] [varchar](100) NOT NULL,
	[Address_Line2] [varchar](100) NOT NULL,
	[Address_Line3] [varchar](100) NOT NULL,
	[Address_Line4] [varchar](100) NOT NULL,
	[PostCode] [varchar](10) NOT NULL,
	[Telephone] [varchar](20) NOT NULL,
	[Contact_Name] [varchar](50) NOT NULL,
	[Web_Address] [varchar](50) NOT NULL,
	[Main_Lab] [varchar](6) NOT NULL,
	[Active] [bit] NOT NULL,
	[Date_Added] [smalldatetime] NOT NULL
) ON [PRIMARY]
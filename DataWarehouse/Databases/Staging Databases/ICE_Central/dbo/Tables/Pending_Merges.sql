﻿CREATE TABLE [dbo].[Pending_Merges](
	[index] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CorrectIDKey] [int] NOT NULL,
	[IncorrectIDKey] [int] NOT NULL,
	[Delete_Hosp_Nos] [varchar](500) NOT NULL CONSTRAINT [DF_Pending_Merges_Delete_Hosp_Nos]  DEFAULT (''),
	[User_ID] [int] NOT NULL,
	[App_ID] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL CONSTRAINT [DF_Pending_Merges_Date_Added]  DEFAULT (getdate()),
	[Date_Complete] [datetime] NULL CONSTRAINT [DF_Pending_Merges_Complete]  DEFAULT (null)
) ON [PRIMARY]
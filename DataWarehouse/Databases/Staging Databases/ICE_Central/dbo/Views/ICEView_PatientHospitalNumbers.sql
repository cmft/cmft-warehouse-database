﻿CREATE VIEW [dbo].[ICEView_PatientHospitalNumbers]
as
	select
		p.patient_id_key AS Patient_Id_Key,
		pl.HospitalNumber AS HospitalNumber,
		pl.Org_Code AS Org_Code,
		pl.Main_Identifier AS Main_Identifier,
		pl.Active_Casenote AS Active_Casenote,
		pl.Hidden AS Hidden,
		pl.Retired AS Retired
	from
		Patient p
		inner join Patient_Local_IDS pl on p.patient_id_key = pl.Patient_Id_key
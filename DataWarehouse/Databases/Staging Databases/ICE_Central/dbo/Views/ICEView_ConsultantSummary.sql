﻿

CREATE VIEW [dbo].[ICEView_ConsultantSummary]
as
	select
		c.Clinician_Index AS Clinician_Index, 
		c.Clinician_National_Code AS National_Code, 
		c.Clinician_Org_ID AS Org_Code, 
		c.Clinician_Surname AS Surname, 
		c.Clinician_Forename AS Forename, 
		c.Clinician_Middlenames AS MiddleNames, 
		c.Clinician_Title AS Title, 
		c.Clinician_Active AS Active, 
		c.Clinician_Speciality_Code AS Specialty_Code, 
		s.Specialty AS Specialty_Description, 
		c.Date_Added AS Date_Added, 
		c.GP_Indicator AS GP_Indicator, 
		c.Master_Clinician_Index AS Master_Clinician_Index, 
		c.Master_Record AS Master_Record
	from
		Clinician c
		left outer join Specialty s on c.Clinician_Speciality_Code = s.Specialty_Code

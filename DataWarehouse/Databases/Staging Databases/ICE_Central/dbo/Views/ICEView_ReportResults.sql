﻿CREATE VIEW [dbo].[ICEView_ReportResults]
as
	select
		r.Investigation_Index AS Investigation_Index, 
		r.Sample_Index AS Sample_Index, 
		r.Result_Index AS Result_Index, 
		r.Result_Code AS Result_Code, 
		r.Result_Rubric AS Result_Name, 
		r.Abnormal_Flag AS Abnormal_Flag, 
		r.UOM_Text AS Unit_Of_Measure,
		r.UOM_Code AS Unit_Of_Measure_Code, 
		r.Result AS Result
	from
		Service_Investigations i
		inner join Service_Results r on i.Investigation_Index = r.Investigation_Index
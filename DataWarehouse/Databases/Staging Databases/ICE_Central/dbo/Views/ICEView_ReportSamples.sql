﻿CREATE VIEW [dbo].[ICEView_ReportSamples]
as
	select
		sr.Service_Report_Index AS Service_Report_Index, 
		ss.Sample_Index AS Sample_Index, 
		ss.Sample_ID AS Sample_Number, 
		ss.Sample_Code AS Sample_Code, 
		ss.Sample_Text AS Sample_Text, 
		ss.Collection_DateTime AS Collection_DateTime
	from
		Service_Reports sr
		inner join Service_Samples ss on sr.Service_Request_Index = ss.Service_Request_Index
﻿CREATE VIEW [dbo].[ICEView_ReportInvestigations]
as
	select
		sr.Service_Report_Index AS Service_Report_Index, 
		i.Sample_Index AS Sample_Index, 
		i.Investigation_Index AS Investigation_Index, 
		i.Investigation_Code AS Investigation_Code, 
		i.Investigation_Requested AS Investigation_Name
	from
		Service_Reports sr
		inner join Service_Investigations i on sr.Service_Report_Index = i.Service_Report_Index
﻿CREATE TABLE [dbo].[tblCanCWTUploadRules](
	[RuleNbr] [int] NOT NULL,
	[RuleDescription] [varchar](400) NOT NULL,
	[XFPhase] [int] NOT NULL,
	[ColumnsChecked] [varchar](80) NULL,
	[HelpInfo] [varchar](100) NULL
) ON [PRIMARY]
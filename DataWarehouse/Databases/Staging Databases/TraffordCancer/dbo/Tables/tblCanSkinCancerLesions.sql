﻿CREATE TABLE [dbo].[tblCanSkinCancerLesions](
	[PatientNoID] [varchar](max) NULL,
	[PatientNO] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[RegistrationDateTime] [varchar](30) NULL,
	[Version] [int] NULL,
	[LesionsID] [int] NULL,
	[DiagnosisDate] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[BodySite] [varchar](max) NULL,
	[ClinicalDiameter] [varchar](max) NULL,
	[ExcisionMargin] [varchar](max) NULL,
	[Distribution] [varchar](max) NULL,
	[morphology] [varchar](max) NULL,
	[RecurrenceIndicator] [varchar](max) NULL,
	[WorkflowID] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
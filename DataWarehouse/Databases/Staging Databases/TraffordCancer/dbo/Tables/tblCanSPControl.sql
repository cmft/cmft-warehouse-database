﻿CREATE TABLE [dbo].[tblCanSPControl](
	[ProcName] [varchar](128) NOT NULL,
	[PrimarySource] [varchar](128) NOT NULL,
	[PrimaryTarget] [varchar](128) NULL,
	[SecondaryTarget] [varchar](128) NULL,
	[TertiaryTarget] [varchar](128) NULL
) ON [PRIMARY]
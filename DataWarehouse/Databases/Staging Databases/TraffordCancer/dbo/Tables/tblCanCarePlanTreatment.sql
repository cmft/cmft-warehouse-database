﻿CREATE TABLE [dbo].[tblCanCarePlanTreatment](
	[PatientNoID] [int] NULL,
	[PatientNO] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[RegistrationDateTime] [varchar](25) NULL,
	[Version] [int] NULL,
	[Consultant] [varchar](max) NULL,
	[Location] [varchar](max) NULL,
	[TreatmentsTreatmentValue] [varchar](max) NULL,
	[TreatmentsTreatmentCode] [varchar](max) NULL,
	[TreatmentsTreatmentSequence] [varchar](max) NULL,
	[WorkflowID] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
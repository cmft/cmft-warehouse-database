﻿CREATE TABLE [dbo].[tblCanCWTTriggerBreachHistory_AsAt_20111214](
	[DateTo] [datetime] NULL,
	[TriggerCat] [varchar](1) NOT NULL,
	[Breach] [float] NULL,
	[WorkFlowId] [varchar](20) NULL,
	[Surname] [varchar](50) NULL,
	[NHSNumber] [varchar](10) NULL,
	[DelayReasonCode] [int] NOT NULL,
	[ClockStart] [datetime] NULL,
	[TriggerPoint] [datetime] NULL,
	[Duration] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCanCarePlan](
	[PatientNoID] [int] NULL,
	[PatientNO] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[Forenames] [varchar](max) NULL,
	[Surname] [varchar](max) NULL,
	[DateOfBirth] [varchar](max) NULL,
	[Sex] [varchar](max) NULL,
	[Address1] [varchar](max) NULL,
	[Address2] [varchar](max) NULL,
	[Address3] [varchar](max) NULL,
	[Address4] [varchar](max) NULL,
	[PostCode] [varchar](max) NULL,
	[HomeTelephone] [varchar](max) NULL,
	[GPID] [varchar](max) NULL,
	[PracticeID] [varchar](max) NULL,
	[RegistrationDateTime] [varchar](25) NULL,
	[Version] [int] NULL,
	[Consultant] [varchar](max) NULL,
	[Location] [varchar](max) NULL,
	[PerformanceStatusValue] [varchar](max) NULL,
	[PerformanceStatusCode] [varchar](max) NULL,
	[ReasonForNoSpecificTreatmentValue] [varchar](max) NULL,
	[ReasonForNoSpecificTreatmentCode] [varchar](max) NULL,
	[CancerCareModalityValue] [varchar](max) NULL,
	[CancerCareModalityCode] [varchar](max) NULL,
	[CancerStatusValue] [varchar](max) NULL,
	[CancerStatusCode] [varchar](max) NULL,
	[CancerStatusChanged] [varchar](max) NULL,
	[CarePlanIntentValue] [varchar](max) NULL,
	[CarePlanIntentCode] [varchar](max) NULL,
	[TreatmentsTreatmentValue] [varchar](max) NULL,
	[TreatmentsTreatmentCode] [varchar](max) NULL,
	[TreatmentsTreatmentSequence] [varchar](max) NULL,
	[TreatmentsForRecurrence] [varchar](max) NULL,
	[TreatmentsParticipant] [varchar](max) NULL,
	[CarePlanAgreedDate] [varchar](25) NULL,
	[MDTCancerTypeValue] [varchar](max) NULL,
	[MDTCarePlanDiscussed] [varchar](max) NULL,
	[MDTDiscussionDate] [varchar](25) NULL,
	[WorkflowID] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
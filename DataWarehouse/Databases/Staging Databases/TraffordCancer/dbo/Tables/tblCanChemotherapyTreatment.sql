﻿CREATE TABLE [dbo].[tblCanChemotherapyTreatment](
	[PatientNoID] [int] NULL,
	[PatientNO] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[RegistrationDateTime] [varchar](25) NULL,
	[Version] [int] NULL,
	[Consultant] [varchar](max) NULL,
	[Location] [varchar](max) NULL,
	[CancerTreatmentDrugsDrugID] [int] NULL,
	[CancerTreatmentDrugsDrug] [varchar](max) NULL,
	[CancerTreatmentDrugsActualDose] [varchar](max) NULL,
	[CancerTreatmentDrugsDosePerDay] [varchar](max) NULL,
	[CancerTreatmentDrugsDurationInDays] [varchar](max) NULL,
	[WorkflowID] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCanCWTErrors](
	[Unifier] [int] NOT NULL,
	[ErrType] [varchar](2) NOT NULL,
	[ErrCode] [varchar](2) NOT NULL,
	[ErrDescription] [varchar](400) NULL,
	[ErrColsChecked] [varchar](80) NULL,
	[ErrHelpInfo] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCanCWTTriggerPoints](
	[CatATriggerPoint] [int] NOT NULL,
	[CatBTriggerPoint] [int] NOT NULL,
	[CatCTriggerPoint] [int] NOT NULL,
	[CatDTriggerPoint] [int] NOT NULL,
	[CatETriggerPoint] [int] NOT NULL,
	[CatFTriggerPoint] [int] NOT NULL,
	[CatGTriggerPoint] [int] NOT NULL,
	[CatHTriggerPoint] [int] NOT NULL,
	[CatITriggerPoint] [int] NOT NULL,
	[CatJTriggerPoint] [int] NOT NULL,
	[CatKTriggerPoint] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblTempTarget](
	[c1] [int] NULL,
	[c2] [int] NULL,
	[RecordID] [int] NOT NULL,
	[BatchType] [varchar](25) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[RecordsRead] [int] NULL,
	[RecordsInserted] [int] NULL,
	[RecordsUpdated] [int] NULL,
	[RecordsErrored] [int] NULL,
	[Status] [varchar](20) NULL,
	[UserId] [varchar](30) NULL,
	[Comments] [varchar](200) NULL
) ON [PRIMARY]
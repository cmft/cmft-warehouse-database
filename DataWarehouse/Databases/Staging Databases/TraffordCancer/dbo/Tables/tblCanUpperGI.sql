﻿CREATE TABLE [dbo].[tblCanUpperGI](
	[PatientNoID] [int] NULL,
	[PatientNO] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[Forenames] [varchar](max) NULL,
	[Surname] [varchar](max) NULL,
	[DateOfBirth] [varchar](max) NULL,
	[Sex] [varchar](max) NULL,
	[Address1] [varchar](max) NULL,
	[Address2] [varchar](max) NULL,
	[Address3] [varchar](max) NULL,
	[Address4] [varchar](max) NULL,
	[PostCode] [varchar](max) NULL,
	[HomeTelephone] [varchar](max) NULL,
	[GPID] [varchar](max) NULL,
	[PracticeID] [varchar](max) NULL,
	[RegistrationDateTime] [varchar](30) NULL,
	[Version] [int] NULL,
	[Consultant] [varchar](max) NULL,
	[Location] [varchar](max) NULL,
	[CoMorbiditiesStatus] [varchar](max) NULL,
	[AlcoholStatus] [varchar](max) NULL,
	[EstimatedPackYears] [varchar](max) NULL,
	[YearStoppedSmoking] [varchar](max) NULL,
	[SmokingStatus] [varchar](max) NULL,
	[FamilyHistoryCancer] [varchar](max) NULL,
	[FamilyHistoryRelationshipToPatient] [varchar](max) NULL,
	[FamilyHistoryPrimaryDiagnosisOfRelation] [varchar](max) NULL,
	[PossumScoreAtDiagnosis] [varchar](max) NULL,
	[PossumScoreAfterSurgery] [varchar](max) NULL,
	[WorkflowID] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCanPathology](
	[PatientNoID] [int] NULL,
	[PatientNO] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[Forenames] [varchar](max) NULL,
	[Surname] [varchar](max) NULL,
	[DateOfBirth] [varchar](max) NULL,
	[Sex] [varchar](max) NULL,
	[Address1] [varchar](max) NULL,
	[Address2] [varchar](max) NULL,
	[Address3] [varchar](max) NULL,
	[Address4] [varchar](max) NULL,
	[PostCode] [varchar](max) NULL,
	[HomeTelephone] [varchar](max) NULL,
	[GPID] [varchar](max) NULL,
	[PracticeID] [varchar](max) NULL,
	[RegistrationDateTime] [varchar](30) NULL,
	[Version] [int] NULL,
	[Consultant] [varchar](max) NULL,
	[Location] [varchar](max) NULL,
	[ReferralRequestingOrganisationCodeValue] [varchar](max) NULL,
	[ReferralRequestingOrganisationCodeCode] [varchar](max) NULL,
	[ReferralReferringCareProfessional] [varchar](max) NULL,
	[SpecimenReportNumber] [varchar](max) NULL,
	[SpecimenReportStatusValue] [varchar](max) NULL,
	[SpecimenReportStatusCode] [varchar](max) NULL,
	[SpecimenNatureValue] [varchar](max) NULL,
	[SpecimenNatureCode] [varchar](max) NULL,
	[TNMStageGrouping] [varchar](max) NULL,
	[MCATCategory] [varchar](max) NULL,
	[MCATDistantMetastases] [varchar](max) NULL,
	[NCATCategory] [varchar](max) NULL,
	[NCATLocalInvasionTumourExtent] [varchar](max) NULL,
	[TCATCategory] [varchar](max) NULL,
	[TCATLocalInvasionTumourExtent] [varchar](max) NULL,
	[ExcisionMarginValue] [varchar](max) NULL,
	[ExcisionMarginCode] [varchar](max) NULL,
	[CancerVascularOrLymphaticInvasionValue] [varchar](max) NULL,
	[CancerVascularOrLymphaticInvasionCode] [varchar](max) NULL,
	[GradeOfDifferentiationValue] [varchar](max) NULL,
	[GradeOfDifferentiationCode] [varchar](max) NULL,
	[Histology] [varchar](max) NULL,
	[SynchronousTumourIndicatorCode] [varchar](max) NULL,
	[SynchronousTumourIndicatorValue] [varchar](max) NULL,
	[InvasiveLesionSize] [varchar](max) NULL,
	[TumourLateralityCode] [varchar](max) NULL,
	[TumourLateralityValue] [varchar](max) NULL,
	[NumNodesPositive] [varchar](max) NULL,
	[NumNodesExamined] [varchar](max) NULL,
	[PrimaryDiagnosis] [varchar](max) NULL,
	[OrganisationCodeCode] [varchar](max) NULL,
	[OrganisationCodeValue] [varchar](max) NULL,
	[ConsultantCode] [varchar](max) NULL,
	[InvestigationResultDate] [varchar](max) NULL,
	[SampleReceiptDate] [varchar](max) NULL,
	[InvestigationTypeCode] [varchar](max) NULL,
	[InvestigationTypeValue] [varchar](max) NULL,
	[WorkflowID] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblCanSurgeonsBRT](
	[PatientNoID] [int] NULL,
	[PatientNO] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[RegistrationDateTime] [varchar](30) NULL,
	[Version] [int] NULL,
	[SurgeonID] [int] NULL,
	[SurgeonName] [varchar](max) NULL,
	[SurgeonCode] [varchar](max) NULL,
	[CancerType] [varchar](20) NULL,
	[WorkflowID] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
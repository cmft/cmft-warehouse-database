﻿CREATE TABLE [dbo].[project_organisation_roles](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nchar](30) NOT NULL,
	[archive] [bit] NOT NULL CONSTRAINT [DF_project_organisation_types_archive]  DEFAULT ((0))
) ON [PRIMARY]
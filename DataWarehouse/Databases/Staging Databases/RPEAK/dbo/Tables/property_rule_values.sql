﻿CREATE TABLE [dbo].[property_rule_values](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RuleId] [int] NOT NULL,
	[Name] [nvarchar](128) NULL,
	[DisplayName] [nvarchar](128) NULL,
	[RawValue] [nvarchar](max) NULL,
	[IsDefault] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
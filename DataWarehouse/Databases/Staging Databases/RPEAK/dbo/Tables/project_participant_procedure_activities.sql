﻿CREATE TABLE [dbo].[project_participant_procedure_activities](
	[ProjectParticipantProcedureActivities_Id] [int] NOT NULL,
	[ProjectParticipantVisits_Id] [int] NULL,
	[ProjectPerPatientBudgetProcedureActivity_Id] [int] NULL,
	[Times] [int] NULL
) ON [PRIMARY]
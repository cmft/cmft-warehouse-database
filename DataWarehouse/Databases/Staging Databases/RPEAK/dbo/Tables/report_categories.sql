﻿CREATE TABLE [dbo].[report_categories](
	[category_id] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [nvarchar](20) NOT NULL
) ON [PRIMARY]
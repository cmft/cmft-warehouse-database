﻿CREATE TABLE [dbo].[user_trusts](
	[user_id] [int] NOT NULL,
	[trust_id] [int] NOT NULL,
	[is_default] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
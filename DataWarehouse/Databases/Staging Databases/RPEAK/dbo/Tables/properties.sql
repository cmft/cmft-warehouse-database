﻿CREATE TABLE [dbo].[properties](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](128) NOT NULL,
	[IsSystem] [bit] NOT NULL,
	[IsMultiple] [bit] NOT NULL
) ON [PRIMARY]
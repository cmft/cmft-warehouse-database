﻿CREATE TABLE [dbo].[report_query_conditions](
	[query_condition_id] [int] IDENTITY(1,1) NOT NULL,
	[query_id] [int] NOT NULL,
	[field_id] [int] NOT NULL,
	[condition_type_id] [int] NOT NULL,
	[operator_id] [int] NOT NULL,
	[value] [nvarchar](max) NOT NULL,
	[condition_order] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
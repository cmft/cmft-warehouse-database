﻿CREATE TABLE [dbo].[countries](
	[country_code] [nvarchar](3) NOT NULL,
	[country] [nvarchar](80) NULL,
	[archived] [datetime] NULL CONSTRAINT [DF_countries_active]  DEFAULT ((1))
) ON [PRIMARY]
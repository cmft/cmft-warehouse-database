﻿CREATE TABLE [dbo].[project_participant_visits](
	[ppv_id] [int] IDENTITY(1,1) NOT NULL,
	[pp_id] [int] NOT NULL,
	[visit_id] [int] NOT NULL,
	[attended] [bit] NULL,
	[date_attended] [date] NULL,
	[notes] [nvarchar](500) NULL,
	[created_date] [datetime] NULL,
	[created_by] [int] NULL,
	[updated_date] [datetime] NULL,
	[updated_by] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[study_types](
	[code] [nvarchar](12) NOT NULL,
	[description] [nchar](100) NOT NULL,
	[archive] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[project_organisations](
	[po_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[org_id] [int] NOT NULL,
	[counter] [int] NULL,
	[sponsor] [bit] NOT NULL,
	[funder] [bit] NOT NULL,
	[comments] [nvarchar](2000) NULL,
	[created_date] [datetime] NULL,
	[created_by] [int] NULL,
	[updated_date] [datetime] NULL,
	[updated_by] [int] NULL,
	[role_id] [int] NULL
) ON [PRIMARY]
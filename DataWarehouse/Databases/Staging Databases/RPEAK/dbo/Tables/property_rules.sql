﻿CREATE TABLE [dbo].[property_rules](
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](128) NOT NULL,
	[IsSystem] [bit] NOT NULL,
	[IsMultiple] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[property_rule_categories](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](128) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[project_participant_research_activity_type](
	[pprat_id] [int] IDENTITY(1,1) NOT NULL,
	[research_activity_type_code] [nvarchar](50) NOT NULL,
	[short_description] [nvarchar](50) NULL,
	[long_description] [nvarchar](2500) NULL
) ON [PRIMARY]
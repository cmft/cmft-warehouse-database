﻿CREATE TABLE [dbo].[task_dependencies](
	[td_id] [int] IDENTITY(1,1) NOT NULL,
	[task_id] [int] NULL,
	[dependent_on] [int] NULL,
	[active] [bit] NULL
) ON [PRIMARY]
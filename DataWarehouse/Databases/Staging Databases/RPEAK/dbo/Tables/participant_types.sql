﻿CREATE TABLE [dbo].[participant_types](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](50) NOT NULL,
	[short_description] [nvarchar](50) NULL,
	[long_description] [nvarchar](255) NULL
) ON [PRIMARY]
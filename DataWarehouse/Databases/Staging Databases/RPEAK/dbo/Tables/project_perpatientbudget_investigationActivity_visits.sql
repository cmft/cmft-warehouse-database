﻿CREATE TABLE [dbo].[project_perpatientbudget_investigationActivity_visits](
	[pppav_id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectPerPatientBudgetInvestigationActivity_Id] [int] NOT NULL,
	[ProjectPerPatientBudgetVisit_Id] [int] NOT NULL,
	[Times] [int] NULL
) ON [PRIMARY]
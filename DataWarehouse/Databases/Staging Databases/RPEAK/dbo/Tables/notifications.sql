﻿CREATE TABLE [dbo].[notifications](
	[nid] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[task_id] [int] NOT NULL,
	[details] [nvarchar](1000) NOT NULL,
	[log_date] [datetime] NOT NULL
) ON [PRIMARY]
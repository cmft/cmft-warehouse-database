﻿CREATE TABLE [dbo].[workflows](
	[wf_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](120) NOT NULL,
	[description] [nvarchar](2000) NULL,
	[biz_rule] [nvarchar](200) NULL,
	[archive] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[person_titles](
	[title] [nvarchar](10) NOT NULL,
	[full_title] [nvarchar](50) NOT NULL,
	[archive] [bit] NOT NULL CONSTRAINT [DF_person_titles_archive]  DEFAULT ((0))
) ON [PRIMARY]
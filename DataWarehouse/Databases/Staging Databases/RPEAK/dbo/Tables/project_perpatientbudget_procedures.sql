﻿CREATE TABLE [dbo].[project_perpatientbudget_procedures](
	[ppp_id] [int] IDENTITY(1,1) NOT NULL,
	[procedure_name] [varchar](250) NULL,
	[clinical_time] [int] NULL,
	[nurse_time] [int] NULL,
	[administrative_time] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[report_query_fields](
	[field_id] [int] NOT NULL,
	[query_id] [int] NOT NULL,
	[field_order] [int] NOT NULL,
	[sort_order] [bit] NULL
) ON [PRIMARY]
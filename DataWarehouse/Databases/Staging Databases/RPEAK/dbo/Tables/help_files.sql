﻿CREATE TABLE [dbo].[help_files](
	[controller_action] [nvarchar](100) NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[help_text] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
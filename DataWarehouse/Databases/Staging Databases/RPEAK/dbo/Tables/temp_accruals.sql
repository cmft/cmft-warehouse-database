﻿CREATE TABLE [dbo].[temp_accruals](
	[project_code] [nvarchar](50) NULL,
	[participant_ref] [nvarchar](50) NULL,
	[entry_date] [date] NULL
) ON [PRIMARY]
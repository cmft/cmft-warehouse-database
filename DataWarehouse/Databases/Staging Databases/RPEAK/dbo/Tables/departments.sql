﻿CREATE TABLE [dbo].[departments](
	[dept_id] [int] IDENTITY(1,1) NOT NULL,
	[dept_code] [nvarchar](20) NULL,
	[description] [nvarchar](80) NULL,
	[parent_id] [int] NULL,
	[type] [int] NULL,
	[active] [bit] NULL,
	[trust_id] [int] NULL
) ON [PRIMARY]
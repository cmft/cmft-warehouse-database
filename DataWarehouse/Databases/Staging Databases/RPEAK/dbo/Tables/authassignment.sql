﻿CREATE TABLE [dbo].[authassignment](
	[aa_id] [int] IDENTITY(1,1) NOT NULL,
	[itemname] [nvarchar](64) NULL,
	[userid] [int] NULL,
	[bizrule] [nvarchar](2000) NULL,
	[data] [nvarchar](2000) NULL
) ON [PRIMARY]
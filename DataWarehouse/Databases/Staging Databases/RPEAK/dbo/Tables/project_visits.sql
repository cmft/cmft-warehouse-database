﻿CREATE TABLE [dbo].[project_visits](
	[visit_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[study_arm_id] [int] NULL,
	[visit_number] [int] NOT NULL,
	[description] [nvarchar](60) NULL,
	[mandatory] [bit] NOT NULL CONSTRAINT [DF_project_visits_non_standard]  DEFAULT ((1)),
	[timeline] [int] NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[start_date] [datetime2](7) NULL,
	[end_date] [datetime2](7) NULL
) ON [PRIMARY]
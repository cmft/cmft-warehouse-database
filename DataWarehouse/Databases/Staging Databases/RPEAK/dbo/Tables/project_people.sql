﻿CREATE TABLE [dbo].[project_people](
	[pp_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NULL,
	[person_id] [int] NULL,
	[role] [nvarchar](100) NULL,
	[lead] [bit] NULL,
	[chief] [bit] NULL,
	[contact] [bit] NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL,
	[created_by] [int] NULL,
	[created_date] [datetime] NULL,
	[updated_by] [int] NULL,
	[updated_date] [datetime] NULL,
	[gcp_required] [bit] NULL,
	[trust_id] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[review_types](
	[type_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nchar](20) NULL,
	[description] [nchar](60) NULL
) ON [PRIMARY]
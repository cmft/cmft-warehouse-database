﻿CREATE TABLE [dbo].[workflow_roles](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](80) NOT NULL,
	[description] [nvarchar](255) NULL
) ON [PRIMARY]
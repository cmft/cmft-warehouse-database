﻿CREATE TABLE [dbo].[organisation_mffRate](
	[org_id] [int] NOT NULL,
	[mff_rate] [decimal](18, 6) NULL
) ON [PRIMARY]
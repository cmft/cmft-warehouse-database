﻿CREATE TABLE [dbo].[project_costs_invoiced](
	[pci_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NULL,
	[requisition_ref] [nvarchar](40) NULL,
	[description] [nvarchar](120) NULL,
	[raised] [date] NULL,
	[processed] [date] NULL,
	[recieved] [date] NULL,
	[income_type] [nvarchar](20) NULL,
	[org_id] [int] NULL,
	[amount] [decimal](18, 2) NULL,
	[created_date] [datetime] NULL,
	[created_by] [int] NULL,
	[updated_date] [datetime] NULL,
	[updated_by] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[labs](
	[lab_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nchar](30) NULL,
	[archive] [bit] NOT NULL
) ON [PRIMARY]
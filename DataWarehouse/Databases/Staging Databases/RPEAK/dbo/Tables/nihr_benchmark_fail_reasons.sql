﻿CREATE TABLE [dbo].[nihr_benchmark_fail_reasons](
	[reason_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](80) NOT NULL,
	[map_to] [nvarchar](2) NULL,
	[archive] [bit] NULL CONSTRAINT [DF_nihr_benchmark_fail_reasons_archive]  DEFAULT ((0))
) ON [PRIMARY]
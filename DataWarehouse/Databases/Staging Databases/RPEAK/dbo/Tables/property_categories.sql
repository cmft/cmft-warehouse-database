﻿CREATE TABLE [dbo].[property_categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](128) NOT NULL,
	[IsSystem] [bit] NOT NULL
) ON [PRIMARY]
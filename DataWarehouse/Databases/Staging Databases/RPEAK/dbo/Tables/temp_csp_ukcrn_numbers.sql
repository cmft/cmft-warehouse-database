﻿CREATE TABLE [dbo].[temp_csp_ukcrn_numbers](
	[PORTFOLIO ID] [nvarchar](50) NULL,
	[IRAS ID] [int] NULL,
	[Short Title] [nvarchar](106) NULL,
	[Trust Name] [nvarchar](90) NULL,
	[Trust Code] [nvarchar](50) NULL,
	[PIC] [nvarchar](50) NULL,
	[Topic] [nvarchar](70) NULL,
	[Date NHS Permission Granted] [nvarchar](50) NULL,
	[Site Target] [nvarchar](50) NULL,
	[Date FPV] [nvarchar](50) NULL
) ON [PRIMARY]
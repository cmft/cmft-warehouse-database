﻿CREATE TABLE [dbo].[people_organisations](
	[person_id] [int] NOT NULL,
	[org_id] [int] NOT NULL,
	[address_street] [nvarchar](80) NULL,
	[address_city] [nvarchar](80) NULL,
	[address_state] [nvarchar](80) NULL,
	[postcode] [nvarchar](20) NULL,
	[country] [nvarchar](50) NULL,
	[tel_1] [nvarchar](50) NULL,
	[tel_2] [nvarchar](50) NULL,
	[post_title] [nvarchar](100) NULL,
	[status] [nvarchar](20) NULL,
	[status_date] [date] NULL,
	[person_category] [nvarchar](20) NULL,
	[dept_id] [int] NULL,
	[contract_type] [nvarchar](10) NULL,
	[staff_type] [nvarchar](40) NULL
) ON [PRIMARY]
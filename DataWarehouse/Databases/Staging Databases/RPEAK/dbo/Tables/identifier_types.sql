﻿CREATE TABLE [dbo].[identifier_types](
	[type_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [varchar](20) NOT NULL,
	[description] [varchar](500) NULL,
	[website] [varchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[local_clinical_research_network](
	[lcrn_id] [int] IDENTITY(1,1) NOT NULL,
	[lcrn_name] [nvarchar](255) NOT NULL
) ON [PRIMARY]
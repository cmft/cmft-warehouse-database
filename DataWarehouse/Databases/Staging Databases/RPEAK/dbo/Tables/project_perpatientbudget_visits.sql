﻿CREATE TABLE [dbo].[project_perpatientbudget_visits](
	[ppv_id] [int] IDENTITY(1,1) NOT NULL,
	[StudyArm_Id] [int] NOT NULL,
	[ProjectPerPatiebtBudget_Id] [int] NULL,
	[visit_name] [varchar](50) NOT NULL,
	[days_afterEntry] [int] NULL
) ON [PRIMARY]
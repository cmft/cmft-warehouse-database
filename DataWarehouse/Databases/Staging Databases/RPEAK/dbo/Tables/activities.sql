﻿CREATE TABLE [dbo].[activities](
	[activity_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [int] NOT NULL,
	[description] [nvarchar](180) NOT NULL,
	[lab_id] [int] NULL,
	[clinical_time] [int] NULL,
	[nurse_time] [int] NULL,
	[admin_time] [int] NULL,
	[default_cost] [decimal](18, 2) NULL,
	[cost_centre] [nvarchar](20) NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[task_type](
	[tc_id] [int] IDENTITY(1,1) NOT NULL,
	[task_category] [nvarchar](40) NULL,
	[description] [nvarchar](255) NULL,
	[archive] [bit] NULL
) ON [PRIMARY]
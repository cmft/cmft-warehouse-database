﻿CREATE TABLE [dbo].[temp_delivery_comments](
	[rec] [nvarchar](50) NULL,
	[comments] [nvarchar](500) NULL,
	[project_id] [int] NULL
) ON [PRIMARY]
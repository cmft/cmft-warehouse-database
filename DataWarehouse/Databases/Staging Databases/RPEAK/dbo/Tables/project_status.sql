﻿CREATE TABLE [dbo].[project_status](
	[status_code] [nvarchar](20) NOT NULL,
	[description] [nvarchar](80) NOT NULL,
	[archive] [bit] NULL
) ON [PRIMARY]
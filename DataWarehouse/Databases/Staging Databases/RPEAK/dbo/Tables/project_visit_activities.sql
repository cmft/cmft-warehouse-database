﻿CREATE TABLE [dbo].[project_visit_activities](
	[pva_id] [int] IDENTITY(1,1) NOT NULL,
	[visit_id] [int] NOT NULL,
	[project_activity_id] [int] NOT NULL,
	[occurs] [int] NOT NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [int] NULL,
	[updated_date] [datetime] NULL,
	[start_date] [datetime2](7) NULL,
	[end_date] [datetime2](7) NULL
) ON [PRIMARY]
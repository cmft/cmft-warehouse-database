﻿CREATE TABLE [dbo].[cost_centres](
	[code] [nvarchar](20) NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[archive] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[workflow_dependencies](
	[wfd_id] [int] IDENTITY(1,1) NOT NULL,
	[wft_id] [int] NULL,
	[dependent_on] [int] NULL,
	[active] [bit] NULL
) ON [PRIMARY]
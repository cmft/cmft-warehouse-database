﻿CREATE TABLE [dbo].[specific_property_rules](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [int] NOT NULL,
	[RuleId] [int] NOT NULL,
	[RawValue] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
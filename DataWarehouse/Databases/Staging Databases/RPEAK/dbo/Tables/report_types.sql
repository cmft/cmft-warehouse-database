﻿CREATE TABLE [dbo].[report_types](
	[report_type_id] [int] IDENTITY(1,1) NOT NULL,
	[report_type_name] [nvarchar](50) NOT NULL
) ON [PRIMARY]
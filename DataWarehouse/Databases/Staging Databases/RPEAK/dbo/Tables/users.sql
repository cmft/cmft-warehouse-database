﻿CREATE TABLE [dbo].[users](
	[user_id] [int] NOT NULL,
	[username] [nvarchar](40) NULL,
	[user_password] [nvarchar](41) NULL,
	[salt] [nvarchar](20) NULL,
	[question] [nvarchar](120) NULL,
	[answer] [nvarchar](120) NULL,
	[active] [bit] NULL,
	[password_hash] [nvarchar](100) NULL,
	[password_change_date] [date] NOT NULL DEFAULT (getdate()),
	[attempts_login] [int] NOT NULL DEFAULT ((0)),
	[is_locked] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[project_workflows](
	[pw_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[wf_id] [int] NOT NULL
) ON [PRIMARY]
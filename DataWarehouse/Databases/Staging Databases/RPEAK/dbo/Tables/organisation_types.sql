﻿CREATE TABLE [dbo].[organisation_types](
	[type_code] [nvarchar](12) NOT NULL,
	[type_desc] [nvarchar](60) NOT NULL
) ON [PRIMARY]
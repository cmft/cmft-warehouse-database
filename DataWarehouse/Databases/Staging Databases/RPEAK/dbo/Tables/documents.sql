﻿CREATE TABLE [dbo].[documents](
	[doc_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NULL,
	[doc_name] [nvarchar](200) NOT NULL,
	[doc_type] [nvarchar](20) NULL,
	[author_id] [int] NULL,
	[owner_id] [int] NULL,
	[updated_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[trust_id] [int] NULL,
	[lcrn_id] [int] NULL,
	[project_wide_information_id] [int] NULL,
	[person_id] [int] NULL,
	[is_project_shared_by_lcrn] [bit] NULL,
	[upload_type] [nvarchar](10) NULL
) ON [PRIMARY]
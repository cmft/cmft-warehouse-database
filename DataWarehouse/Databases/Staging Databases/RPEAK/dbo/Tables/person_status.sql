﻿CREATE TABLE [dbo].[person_status](
	[code] [nvarchar](20) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[archive] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[doc_types](
	[doc_type] [nvarchar](20) NOT NULL,
	[doc_description] [nvarchar](120) NULL,
	[archived] [bit] NULL
) ON [PRIMARY]
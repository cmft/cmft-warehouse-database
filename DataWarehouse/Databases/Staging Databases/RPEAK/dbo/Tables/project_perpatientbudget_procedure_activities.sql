﻿CREATE TABLE [dbo].[project_perpatientbudget_procedure_activities](
	[pppa_id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectPerPatientBudgetProcedureName] [varchar](250) NOT NULL,
	[StudyArm_Id] [int] NOT NULL,
	[ProjectPerPatientBudget_Id] [int] NOT NULL,
	[Clinical_Time] [int] NULL,
	[Nurse_Time] [int] NULL,
	[administrative_time] [int] NULL,
	[Costs] [money] NULL
) ON [PRIMARY]
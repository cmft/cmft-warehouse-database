﻿CREATE TABLE [dbo].[project_identifiers](
	[pi_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[identifier_type] [int] NOT NULL,
	[identifier] [varchar](40) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL,
	[issue_date] [date] NULL
) ON [PRIMARY]
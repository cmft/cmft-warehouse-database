﻿CREATE TABLE [dbo].[participant_status](
	[code] [nvarchar](12) NOT NULL,
	[description] [nvarchar](80) NOT NULL,
	[archive] [bit] NULL
) ON [PRIMARY]
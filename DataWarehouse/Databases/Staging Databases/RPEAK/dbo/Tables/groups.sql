﻿CREATE TABLE [dbo].[groups](
	[group_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](80) NOT NULL,
	[description] [nvarchar](500) NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL
) ON [PRIMARY]
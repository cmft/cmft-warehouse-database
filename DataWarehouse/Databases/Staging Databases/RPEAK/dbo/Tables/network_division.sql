﻿CREATE TABLE [dbo].[network_division](
	[short_desc] [nvarchar](20) NOT NULL,
	[long_desc] [nchar](200) NOT NULL,
	[active] [bit] NOT NULL
) ON [PRIMARY]
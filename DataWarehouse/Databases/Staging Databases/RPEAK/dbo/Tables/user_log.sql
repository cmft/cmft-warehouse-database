﻿CREATE TABLE [dbo].[user_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[action] [nvarchar](100) NULL,
	[action_date] [datetime] NULL,
	[additional_data] [nvarchar](2000) NULL
) ON [PRIMARY]
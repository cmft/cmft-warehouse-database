﻿CREATE TABLE [dbo].[study_arms](
	[study_arm_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NULL,
	[short_description] [nvarchar](60) NOT NULL,
	[long_description] [nvarchar](2000) NULL,
	[proposed_start_date] [date] NULL,
	[proposed_end_date] [date] NULL,
	[arm_start_date] [date] NULL,
	[arm_end_date] [date] NULL,
	[updated_by] [int] NULL,
	[updated_date] [datetime] NULL,
	[created_by] [int] NULL,
	[created_date] [datetime] NULL,
	[default_arm] [bit] NULL,
	[research_site_id] [int] NULL
) ON [PRIMARY]
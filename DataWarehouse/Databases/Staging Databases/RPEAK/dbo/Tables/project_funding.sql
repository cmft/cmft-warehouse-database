﻿CREATE TABLE [dbo].[project_funding](
	[pf_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[funder_id] [int] NOT NULL,
	[funder_ref] [nvarchar](50) NULL,
	[recipient_id] [int] NULL,
	[finance_ref] [nvarchar](50) NULL,
	[total_amount] [decimal](18, 2) NULL,
	[fund_start] [date] NULL,
	[fund_end] [date] NULL,
	[notes] [nvarchar](max) NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL,
	[trust_income] [decimal](18, 2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
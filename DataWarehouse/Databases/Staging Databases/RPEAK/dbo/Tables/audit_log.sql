﻿CREATE TABLE [dbo].[audit_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[user_name] [nvarchar](255) NOT NULL,
	[action_id] [int] NOT NULL,
	[section_id] [int] NOT NULL,
	[parent_entity] [nvarchar](255) NULL,
	[entity] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[date] [datetime] NOT NULL
) ON [PRIMARY]
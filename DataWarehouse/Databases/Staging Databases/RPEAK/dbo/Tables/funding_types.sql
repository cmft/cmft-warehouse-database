﻿CREATE TABLE [dbo].[funding_types](
	[type_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nchar](40) NOT NULL,
	[archive] [bit] NOT NULL CONSTRAINT [DF_funding_types_active]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[project_perpatientbudget_procedureActivity_visits](
	[ppiav_id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectPerPatientBudgetProcedureActivity_Id] [int] NOT NULL,
	[ProjectPerPatientBudgetVisit_Id] [int] NOT NULL,
	[Times] [int] NULL
) ON [PRIMARY]
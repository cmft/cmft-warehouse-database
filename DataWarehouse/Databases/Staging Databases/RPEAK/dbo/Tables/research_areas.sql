﻿CREATE TABLE [dbo].[research_areas](
	[area_code] [nvarchar](3) NOT NULL,
	[research_area] [nvarchar](40) NULL,
	[description] [nvarchar](2000) NULL,
	[archive] [bit] NOT NULL
) ON [PRIMARY]
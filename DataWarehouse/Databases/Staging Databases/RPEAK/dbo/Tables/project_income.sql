﻿CREATE TABLE [dbo].[project_income](
	[income_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NULL,
	[description] [nvarchar](120) NULL,
	[income_date] [date] NULL,
	[income_type] [nvarchar](20) NULL,
	[org_id] [int] NULL,
	[amount] [decimal](18, 2) NULL,
	[updated_date] [datetime] NULL,
	[updated_by] [int] NULL,
	[created_date] [datetime] NULL,
	[created_by] [int] NULL
) ON [PRIMARY]
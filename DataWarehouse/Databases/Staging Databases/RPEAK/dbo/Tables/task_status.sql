﻿CREATE TABLE [dbo].[task_status](
	[status_code] [nvarchar](20) NOT NULL,
	[description] [nvarchar](60) NULL,
	[active] [bit] NULL
) ON [PRIMARY]
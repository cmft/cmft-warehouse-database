﻿CREATE TABLE [dbo].[workflow_tasks](
	[wft_id] [int] IDENTITY(1,1) NOT NULL,
	[wf_id] [int] NOT NULL,
	[task] [nvarchar](120) NOT NULL,
	[description] [nvarchar](1000) NULL,
	[owner_role] [int] NULL,
	[assigned_role] [int] NULL,
	[due_date] [date] NULL,
	[completion_date] [date] NULL,
	[task_type] [nvarchar](20) NULL,
	[review_type] [int] NULL,
	[wf_related_task] [int] NULL,
	[offset_days] [int] NULL,
	[private] [bit] NULL,
	[task_order] [float] NULL
) ON [PRIMARY]
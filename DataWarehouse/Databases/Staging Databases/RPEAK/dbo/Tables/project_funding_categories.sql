﻿CREATE TABLE [dbo].[project_funding_categories](
	[pfc_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](60) NULL,
	[archive] [bit] NULL
) ON [PRIMARY]
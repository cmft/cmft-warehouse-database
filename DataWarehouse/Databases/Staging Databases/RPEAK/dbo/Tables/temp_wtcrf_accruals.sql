﻿CREATE TABLE [dbo].[temp_wtcrf_accruals](
	[project_code] [nvarchar](50) NULL,
	[entry_date] [datetime] NULL,
	[participant_ref] [nvarchar](54) NULL
) ON [PRIMARY]
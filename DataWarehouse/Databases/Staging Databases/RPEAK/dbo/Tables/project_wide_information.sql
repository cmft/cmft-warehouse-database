﻿CREATE TABLE [dbo].[project_wide_information](
	[project_wide_info_id] [int] IDENTITY(1,1) NOT NULL,
	[title_short] [nvarchar](200) NULL,
	[title_full] [nvarchar](2000) NULL,
	[research_area] [nvarchar](3) NULL,
	[study_type] [nvarchar](12) NULL,
	[national_theme_id] [int] NULL,
	[clinical_trial] [int] NULL,
	[multi_centre] [bit] NOT NULL,
	[private] [bit] NOT NULL,
	[project_reference] [nvarchar](255) NULL,
	[cpms_id] [nvarchar](50) NULL,
	[acronym] [nvarchar](50) NULL,
	[lcrn_created] [bit] NOT NULL,
	[updated_date] [datetime] NULL,
	[updated_by] [int] NULL,
	[created_date] [datetime] NULL,
	[created_by] [int] NULL,
	[lcrn_id] [int] NOT NULL
) ON [PRIMARY]
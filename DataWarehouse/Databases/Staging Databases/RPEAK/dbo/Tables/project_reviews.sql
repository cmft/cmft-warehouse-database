﻿CREATE TABLE [dbo].[project_reviews](
	[pt_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[type_id] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL
) ON [PRIMARY]
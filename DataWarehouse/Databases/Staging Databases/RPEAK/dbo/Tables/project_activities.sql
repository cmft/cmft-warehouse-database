﻿CREATE TABLE [dbo].[project_activities](
	[pa_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NULL,
	[study_arm_id] [int] NOT NULL,
	[activity_id] [int] NOT NULL,
	[description] [nvarchar](180) NULL,
	[type] [int] NOT NULL,
	[clinical_time] [int] NULL,
	[nurse_time] [int] NULL,
	[admin_time] [int] NULL,
	[activity_cost] [decimal](18, 2) NULL,
	[cost_centre] [nvarchar](20) NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [int] NULL,
	[updated_date] [datetime] NULL
) ON [PRIMARY]
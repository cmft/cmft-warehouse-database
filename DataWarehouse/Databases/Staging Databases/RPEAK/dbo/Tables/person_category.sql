﻿CREATE TABLE [dbo].[person_category](
	[category_code] [nvarchar](20) NOT NULL,
	[category_description] [nvarchar](80) NULL,
	[archive] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[report_condition_types](
	[condition_type_id] [int] IDENTITY(1,1) NOT NULL,
	[condition_type_name] [nvarchar](10) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[recently_viewed](
	[view_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[project_id] [int] NOT NULL,
	[view_date] [datetime] NOT NULL
) ON [PRIMARY]
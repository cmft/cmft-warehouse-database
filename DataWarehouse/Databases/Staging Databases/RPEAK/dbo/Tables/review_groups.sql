﻿CREATE TABLE [dbo].[review_groups](
	[rg_id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL,
	[review_type_id] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL
) ON [PRIMARY]
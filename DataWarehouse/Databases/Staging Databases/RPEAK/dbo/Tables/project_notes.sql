﻿CREATE TABLE [dbo].[project_notes](
	[pn_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[note] [nvarchar](3000) NOT NULL,
	[tags] [nvarchar](250) NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL,
	[lcrn_shared] [bit] NOT NULL DEFAULT ((0)),
	[lcrn_created] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
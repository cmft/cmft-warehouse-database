﻿CREATE TABLE [dbo].[doc_store](
	[ds_id] [int] IDENTITY(1,1) NOT NULL,
	[doc_id] [int] NOT NULL,
	[version_no] [varchar](20) NULL,
	[version_date] [date] NULL,
	[approved_date] [date] NULL,
	[doc_name] [nvarchar](200) NOT NULL,
	[type] [nvarchar](200) NULL,
	[size] [int] NOT NULL,
	[file_path] [nchar](200) NULL,
	[archived] [datetime] NULL,
	[updated_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[description] [nvarchar](255) NULL
) ON [PRIMARY]
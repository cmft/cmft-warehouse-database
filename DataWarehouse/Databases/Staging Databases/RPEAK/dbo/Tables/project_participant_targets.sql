﻿CREATE TABLE [dbo].[project_participant_targets](
	[target_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[target_accruals] [int] NOT NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL,
	[legacy_total] [int] NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[ref_required] [bit] NULL
) ON [PRIMARY]
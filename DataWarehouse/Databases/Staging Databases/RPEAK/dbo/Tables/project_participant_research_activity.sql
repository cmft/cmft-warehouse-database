﻿CREATE TABLE [dbo].[project_participant_research_activity](
	[prat_id] [int] IDENTITY(1,1) NOT NULL,
	[project_participant_id] [int] NOT NULL,
	[project_participant_research_acttivity_type_id] [int] NOT NULL,
	[activity_datetime] [datetime] NOT NULL
) ON [PRIMARY]
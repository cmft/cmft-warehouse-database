﻿CREATE TABLE [dbo].[cost_categories](
	[category_id] [int] IDENTITY(1,1) NOT NULL,
	[category] [nvarchar](20) NULL,
	[description] [nvarchar](120) NULL,
	[archive] [bit] NOT NULL CONSTRAINT [DF_cost_categories_archived]  DEFAULT ((0))
) ON [PRIMARY]
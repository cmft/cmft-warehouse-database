﻿CREATE TABLE [dbo].[document_task_links](
	[dtl_id] [int] IDENTITY(1,1) NOT NULL,
	[task_id] [int] NULL,
	[document_id] [int] NULL
) ON [PRIMARY]
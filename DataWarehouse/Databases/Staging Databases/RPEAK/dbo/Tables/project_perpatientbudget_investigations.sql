﻿CREATE TABLE [dbo].[project_perpatientbudget_investigations](
	[ppi_id] [int] IDENTITY(1,1) NOT NULL,
	[investigation_name] [varchar](500) NULL,
	[price_of_investigation] [decimal](18, 8) NULL
) ON [PRIMARY]
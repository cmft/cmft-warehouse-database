﻿CREATE TABLE [dbo].[report_queries](
	[query_id] [int] IDENTITY(1,1) NOT NULL,
	[query_name] [nvarchar](50) NOT NULL,
	[report_type] [int] NOT NULL,
	[shared] [bit] NOT NULL,
	[user_id] [int] NOT NULL,
	[report_category_id] [int] NOT NULL DEFAULT ((1)),
	[lcrn_created] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
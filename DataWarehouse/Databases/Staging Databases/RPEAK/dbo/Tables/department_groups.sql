﻿CREATE TABLE [dbo].[department_groups](
	[pd_id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL,
	[dept_id] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_date] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[specific_properties](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[PropertyId] [int] NOT NULL,
	[DisplayName] [nvarchar](128) NULL
) ON [PRIMARY]
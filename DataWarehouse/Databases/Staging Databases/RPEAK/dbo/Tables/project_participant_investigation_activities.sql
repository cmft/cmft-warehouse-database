﻿CREATE TABLE [dbo].[project_participant_investigation_activities](
	[ProjectParticipantInvestigationActivities_Id] [int] NOT NULL,
	[ProjectParticipantVisit_Id] [int] NULL,
	[ProjectPerPatientBudgetInvestigationActivity_Id] [int] NULL,
	[Times] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[logon_audit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[date] [datetime] NOT NULL,
	[user_name] [nvarchar](150) NOT NULL,
	[ip] [nvarchar](50) NOT NULL,
	[logon_status_id] [int] NOT NULL
) ON [PRIMARY]
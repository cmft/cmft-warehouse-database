﻿CREATE TABLE [dbo].[project_participant_activities](
	[ppa_id] [int] IDENTITY(1,1) NOT NULL,
	[pva_id] [int] NOT NULL,
	[ppv_id] [int] NOT NULL,
	[occurs] [int] NOT NULL,
	[confirmed] [bit] NULL,
	[created_by] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [int] NULL,
	[updated_date] [datetime] NULL
) ON [PRIMARY]
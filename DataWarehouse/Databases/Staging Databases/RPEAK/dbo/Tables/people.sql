﻿CREATE TABLE [dbo].[people](
	[person_id] [int] IDENTITY(1,1) NOT NULL,
	[forename] [nvarchar](60) NULL,
	[surname] [nvarchar](60) NULL,
	[title] [nvarchar](14) NULL,
	[gender] [nvarchar](1) NULL,
	[research_area] [nvarchar](300) NULL,
	[internal] [bit] NULL,
	[last_gcp] [date] NULL,
	[email] [nvarchar](80) NULL,
	[mobile] [nvarchar](50) NULL,
	[created_date] [datetime] NULL,
	[created_by] [int] NULL,
	[updated_date] [datetime] NULL,
	[updated_by] [int] NULL,
	[gcp_required] [bit] NULL
) ON [PRIMARY]
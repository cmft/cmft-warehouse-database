﻿CREATE TABLE [dbo].[report_fields](
	[field_id] [int] IDENTITY(1,1) NOT NULL,
	[field_name] [nvarchar](50) NOT NULL,
	[field_type] [nvarchar](10) NULL
) ON [PRIMARY]
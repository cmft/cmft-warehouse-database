﻿CREATE TABLE [dbo].[property_types](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](128) NOT NULL,
	[TemplateName] [nvarchar](128) NOT NULL,
	[IsEnabled] [bit] NOT NULL
) ON [PRIMARY]
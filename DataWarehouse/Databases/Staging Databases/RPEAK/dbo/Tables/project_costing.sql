﻿CREATE TABLE [dbo].[project_costing](
	[pc_id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NULL,
	[description] [nvarchar](120) NULL,
	[cost_category] [nvarchar](20) NULL,
	[cost] [decimal](18, 2) NULL,
	[created_date] [datetime] NULL,
	[created_by] [int] NULL,
	[updated_date] [datetime] NULL,
	[updated_by] [int] NULL,
	[cost_centre] [nvarchar](20) NULL,
	[total_number_of_units] [int] NULL,
	[cost_date] [datetime] NULL,
	[applicable] [bit] NOT NULL DEFAULT ((0)),
	[total_price] [decimal](18, 2) NULL,
	[pbr_applicable] [bit] NOT NULL DEFAULT ((0)),
	[total_price_with_pbr_mff] [decimal](18, 2) NULL
) ON [PRIMARY]
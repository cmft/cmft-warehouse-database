﻿CREATE TABLE [dbo].[project_perpatientbudget_staffRate](
	[ppsr_id] [int] IDENTITY(1,1) NOT NULL,
	[Staff_Category] [varchar](50) NOT NULL,
	[staff_hourly_rate] [decimal](18, 2) NOT NULL
) ON [PRIMARY]
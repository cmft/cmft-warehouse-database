﻿CREATE TABLE [dbo].[authitem](
	[name] [nvarchar](64) NOT NULL,
	[type] [int] NULL,
	[description] [nvarchar](2000) NULL,
	[bizrule] [nvarchar](2000) NULL,
	[data] [nvarchar](2000) NULL,
	[authitem_id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
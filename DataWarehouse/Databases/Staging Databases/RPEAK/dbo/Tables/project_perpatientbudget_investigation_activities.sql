﻿CREATE TABLE [dbo].[project_perpatientbudget_investigation_activities](
	[ppia_id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectPerPatientBudgetInvestigationName] [varchar](250) NOT NULL,
	[StudyArm_Id] [int] NOT NULL,
	[price_of_investigation] [money] NULL
) ON [PRIMARY]
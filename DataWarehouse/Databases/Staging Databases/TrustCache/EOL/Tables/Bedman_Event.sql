﻿CREATE TABLE [EOL].[Bedman_Event](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HospSpellID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[EventTypeID] [int] NOT NULL,
	[LastUpdated_TS] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[Details] [xml] NOT NULL,
	[Patient] [varchar](8) NULL,
	[IPEpisode] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Dementia].[Bedman_DataSet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProviderspellNo] [varchar](50) NOT NULL,
	[LastUpdated_TS] [datetime] NULL,
	[KnownToHaveDementia] [int] NULL,
	[ForgetfulnessQuestionAnswered] [int] NULL,
	[AMTScore] [int] NULL,
	[DementiaAssessmentAnswerDate] [datetime] NULL,
	[AMTRecordedDate] [datetime] NULL,
	[AMTTestNotPerformedReason] [varchar](100) NULL,
	[InvestigationRecordedDate] [datetime] NULL,
	[InvestigationStarted] [varchar](5) NULL,
	[DementiaReferralSentDate] [datetime] NULL,
	[CurrentWard] [varchar](50) NULL,
	[ContextCode] [varchar](50) NULL,
	[CurrentForgetfulnessQuestionAnswered] [int] NULL,
	[ClinicalIndicationsPresent] [int] NULL
) ON [PRIMARY]
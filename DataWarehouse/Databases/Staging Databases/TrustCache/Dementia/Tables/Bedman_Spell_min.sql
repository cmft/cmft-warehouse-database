﻿CREATE TABLE [Dementia].[Bedman_Spell_min](
	[SpellID] [int] NOT NULL,
	[Patient] [varchar](8) NULL,
	[IPEpisode] [varchar](5) NULL,
	[CurrWard] [char](4) NULL,
	[ProviderSpellNo] [varchar](50) NULL
) ON [PRIMARY]
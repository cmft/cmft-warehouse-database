﻿CREATE TABLE [IDS].[EDTHub](
	[EncounterRecno] [int] IDENTITY(1,1) NOT NULL,
	[DocumentID] [int] NOT NULL,
	[Documentdate] [datetime] NULL,
	[Patientnoid] [int] NULL,
	[Documentversion] [int] NULL,
	[Stage] [varchar](50) NULL,
	[SendToGP] [bit] NULL,
	[SpecialtyCode] [varchar](10) NULL,
	[Specialty] [varchar](100) NULL,
	[Documentxml] [xml] NULL,
	[DischargeXML] [xml] NULL,
	[PracticeCode] [varchar](20) NULL,
	[DateSent] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
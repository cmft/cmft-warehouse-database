﻿CREATE TABLE [dbo].[BiochemicalScreenings](
	[BabyNHSNumber] [nvarchar](30) NULL,
	[Episode] [int] NULL,
	[bloodspot1_parental_consent] [nvarchar](250) NULL,
	[bloodspot1_date] [datetime] NULL,
	[bloodspot1_taken] [nvarchar](10) NULL,
	[bloodspot2_parental_consent] [int] NULL,
	[bloodspot2_date] [datetime] NULL,
	[bloodspot2_taken] [nvarchar](10) NULL,
	[bloodspot3_date] [datetime] NULL,
	[bloodspot3_reason] [ntext] NULL,
	[bloodspot4_date] [datetime] NULL,
	[bloodspot4_reason] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[drugs](
	[BabyNHSNumber] [nvarchar](30) NULL,
	[Episode] [int] NULL,
	[BadgerCode] [int] NULL,
	[Drugs] [ntext] NULL,
	[NumberOfDays] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
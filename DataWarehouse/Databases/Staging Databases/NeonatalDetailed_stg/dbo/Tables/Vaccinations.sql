﻿CREATE TABLE [dbo].[Vaccinations](
	[BabyNHSNumber] [nvarchar](30) NULL,
	[rectime] [datetime] NULL,
	[episode] [int] NULL,
	[UnitName] [nvarchar](150) NULL,
	[given] [ntext] NULL,
	[vaccination_notes] [ntext] NULL,
	[bcg_vaccination_given] [nvarchar](50) NULL,
	[bcg_date_given] [datetime] NULL,
	[bcg_batch_number] [nvarchar](25) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
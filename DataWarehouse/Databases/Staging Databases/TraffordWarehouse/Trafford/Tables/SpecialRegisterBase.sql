﻿CREATE TABLE [Trafford].[SpecialRegisterBase](
	[SpecialRegisterCode] [varchar](4) NOT NULL,
	[SpecialRegister] [varchar](50) NULL,
	[Active] [bit] NULL,
	[ParentRegisterCode] [varchar](4) NULL,
	[ParentRegister] [varchar](50) NULL,
	[ParentActive] [bit] NULL
) ON [PRIMARY]
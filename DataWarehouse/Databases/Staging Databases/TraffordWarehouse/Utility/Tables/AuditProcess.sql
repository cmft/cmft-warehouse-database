﻿CREATE TABLE [Utility].[AuditProcess](
	[ProcessCode] [varchar](255) NOT NULL,
	[Process] [varchar](255) NULL,
	[ParentProcessCode] [varchar](255) NULL,
	[ProcessSourceCode] [varchar](50) NOT NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY]
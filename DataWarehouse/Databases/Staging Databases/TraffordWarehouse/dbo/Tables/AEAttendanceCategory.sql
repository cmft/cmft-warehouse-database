﻿CREATE TABLE [dbo].[AEAttendanceCategory](
	[AEAttendanceCategoryCode] [varchar](50) NOT NULL,
	[AEAttendanceCategory] [varchar](255) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CaseNoteData](
	[HospitalNumber] [varchar](255) NULL,
	[EpmiCode] [varchar](255) NULL,
	[surname] [varchar](255) NULL,
	[SerialNumber] [varchar](255) NOT NULL,
	[Name] [varchar](255) NULL,
	[createddate] [datetime] NULL,
	[NhsNumber] [varchar](50) NULL,
	[Comment] [varchar](255) NULL
) ON [PRIMARY]
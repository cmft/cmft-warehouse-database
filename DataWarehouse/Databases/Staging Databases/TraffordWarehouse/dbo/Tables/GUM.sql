﻿CREATE TABLE [dbo].[GUM](
	[EncounterRecno] [int] NOT NULL,
	[PCTCode] [varchar](10) NOT NULL,
	[SiteCode] [varchar](10) NOT NULL,
	[TreatmentDate] [datetime] NOT NULL,
	[Patients] [int] NOT NULL
) ON [PRIMARY]
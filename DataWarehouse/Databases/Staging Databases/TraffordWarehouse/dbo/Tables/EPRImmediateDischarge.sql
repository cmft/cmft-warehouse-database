﻿CREATE TABLE [dbo].[EPRImmediateDischarge](
	[DocumentID] [int] NOT NULL,
	[PatientNoID] [int] NOT NULL,
	[DocumentDate] [datetime] NOT NULL,
	[UniqueID] [varchar](50) NOT NULL,
	[DocumentXML] [xml] NOT NULL,
	[DocumentVersion] [smallint] NOT NULL,
	[Stage] [smallint] NOT NULL,
	[SearchFlag] [bit] NOT NULL,
	[FormType] [varchar](50) NOT NULL,
	[EPMINumber] [varchar](50) NULL,
	[NHSNumber] [varchar](50) NULL,
	[DistrictNumber] [varchar](50) NULL,
	[ProviderSpellNo] [varchar](50) NULL,
	[APCSourceuniqueid] [varchar](50) NULL,
	[PrintDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[RTT_Weekly_Reporting](
	[Hospital] [varchar](50) NULL,
	[InternalNo] [varchar](50) NULL,
	[ReferralDate] [datetime] NULL,
	[EpisodeNo] [varchar](50) NULL,
	[RefSource] [varchar](50) NULL,
	[Consultant] [varchar](255) NULL,
	[Specialty] [varchar](10) NULL,
	[doh_spec] [varchar](10) NULL,
	[DistrictNo] [varchar](50) NULL,
	[pathway_start_date_original] [datetime] NULL,
	[pathway_start_date_current] [datetime] NULL,
	[pathway_treat_by_date] [datetime] NULL,
	[pathway_end_date] [datetime] NULL,
	[pathway_ID] [varchar](50) NULL,
	[WL_WLDate] [smalldatetime] NULL,
	[WL_IntMan] [int] NULL,
	[WL_cons] [varchar](50) NULL,
	[WL_Spec] [int] NULL,
	[WL_doh_spec] [int] NULL,
	[WL_PrimProc] [int] NULL,
	[tci_date] [datetime] NULL,
	[DecidedToAdmitDate] [datetime] NULL,
	[path_division] [varchar](50) NULL,
	[path_directorate] [varchar](50) NULL,
	[path_cons] [varchar](255) NULL,
	[path_spec] [varchar](255) NULL,
	[path_doh_spec] [varchar](50) NULL,
	[path_doh_desc] [varchar](255) NULL,
	[path_closed_wks_DNA_adjs] [int] NULL,
	[path_closed_days_DNA_adjs] [int] NULL,
	[path_open_wks_DNA_adjs] [int] NULL,
	[path_open_days_DNA_adjs] [int] NULL,
	[Surname] [varchar](255) NULL,
	[Forename] [varchar](255) NULL,
	[type] [varchar](6) NOT NULL,
	[pct_new] [varchar](50) NULL,
	[treattype] [varchar](10) NOT NULL,
	[Rundate] [datetime] NULL,
	[Audit] [text] NULL,
	[Censusdate] [datetime] NULL,
	[PCTCode] [varchar](10) NULL,
	[TWID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
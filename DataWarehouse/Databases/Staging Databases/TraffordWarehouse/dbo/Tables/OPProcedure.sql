﻿CREATE TABLE [dbo].[OPProcedure](
	[ProcedureRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[ProcedureDate] [datetime] NULL,
	[SequenceNo] [tinyint] NULL,
	[ProcedureCode] [varchar](50) NULL,
	[ProcedureReadCode] [varchar](10) NULL,
	[ProcedureReadTermCode] [varchar](50) NULL,
	[OPSourceUniqueID] [varchar](50) NULL,
	[SourceProcedureCode] [varchar](50) NULL,
	[SourceSequenceNo] [int] NULL,
	[InterfaceCode] [varchar](5) NULL
) ON [PRIMARY]
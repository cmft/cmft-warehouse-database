﻿CREATE TABLE [dbo].[HRG4APCSpell](
	[EncounterRecno] [int] NOT NULL,
	[HRGCode] [varchar](10) NULL,
	[DominantProcedureCode] [varchar](10) NULL,
	[PrimaryDiagnosisCode] [varchar](10) NULL,
	[SecondaryDiagnosisCode] [varchar](10) NULL,
	[EpisodeCount] [smallint] NULL,
	[LOS] [smallint] NULL,
	[ReportingSpellLOS] [smallint] NULL,
	[Trimpoint] [smallint] NULL,
	[ExcessBeddays] [smallint] NULL,
	[CCDays] [smallint] NULL
) ON [PRIMARY]
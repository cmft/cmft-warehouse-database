﻿CREATE TABLE [dbo].[AESpecialty](
	[SpecialtyCode] [int] NOT NULL,
	[Specialty] [varchar](80) NOT NULL,
	[IsMentalHealth] [bit] NULL
) ON [PRIMARY]
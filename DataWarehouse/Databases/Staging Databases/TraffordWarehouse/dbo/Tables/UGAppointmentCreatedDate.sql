﻿CREATE TABLE [dbo].[UGAppointmentCreatedDate](
	[SourceUniqueID] [varchar](50) NOT NULL,
	[AppointmentCreateDate] [datetime] NOT NULL
) ON [PRIMARY]
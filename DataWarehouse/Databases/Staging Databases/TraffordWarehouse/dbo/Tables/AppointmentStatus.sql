﻿CREATE TABLE [dbo].[AppointmentStatus](
	[AppointmentStatusCode] [varchar](50) NOT NULL,
	[AppointmentStatus] [varchar](255) NOT NULL
) ON [PRIMARY]
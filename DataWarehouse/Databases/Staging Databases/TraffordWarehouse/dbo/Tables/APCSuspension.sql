﻿CREATE TABLE [dbo].[APCSuspension](
	[SourceUniqueID] [varchar](50) NOT NULL,
	[IPSourceUniqueID] [varchar](50) NULL,
	[SuspensionStartDate] [datetime] NULL,
	[SuspensionEndDate] [datetime] NULL,
	[SuspendedByCode] [varchar](10) NULL,
	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NULL
) ON [PRIMARY]
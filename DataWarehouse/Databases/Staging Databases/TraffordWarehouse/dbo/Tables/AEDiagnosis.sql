﻿CREATE TABLE [dbo].[AEDiagnosis](
	[DiagnosisRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NOT NULL,
	[SequenceNo] [smallint] NOT NULL,
	[DiagnosticSchemeInUse] [varchar](10) NULL,
	[DiagnosisCode] [varchar](50) NULL,
	[AESourceUniqueID] [varchar](50) NULL,
	[SourceDiagnosisCode] [varchar](50) NULL,
	[SourceSequenceNo] [smallint] NULL,
	[DiagnosisSiteCode] [varchar](10) NULL,
	[DiagnosisSideCode] [varchar](10) NULL,
	[DiagnosisName] [varchar](255) NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF__AEDiagnos__Creat__45BE5BA9]  DEFAULT (getdate()),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL CONSTRAINT [DF__AEDiagnos__ByWho__46B27FE2]  DEFAULT (suser_name())
) ON [PRIMARY]
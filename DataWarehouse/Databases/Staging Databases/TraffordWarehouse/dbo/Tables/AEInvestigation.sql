﻿CREATE TABLE [dbo].[AEInvestigation](
	[InvestigationRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[InvestigationDate] [datetime] NULL,
	[SequenceNo] [tinyint] NULL,
	[InvestigationSchemeInUse] [varchar](10) NULL,
	[InvestigationCode] [varchar](50) NULL,
	[AESourceUniqueID] [varchar](50) NULL,
	[SourceInvestigationCode] [varchar](50) NULL,
	[SourceSequenceNo] [tinyint] NULL,
	[ResultDate] [datetime] NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF__AEInvesti__Creat__498EEC8D]  DEFAULT (getdate()),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL CONSTRAINT [DF__AEInvesti__ByWho__4A8310C6]  DEFAULT (suser_name())
) ON [PRIMARY]



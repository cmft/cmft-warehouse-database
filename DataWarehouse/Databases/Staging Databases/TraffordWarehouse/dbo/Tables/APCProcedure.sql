﻿CREATE TABLE [dbo].[APCProcedure](
	[ProcedureRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[ProcedureDate] [datetime] NULL,
	[SequenceNo] [tinyint] NOT NULL,
	[ProcedureCode] [varchar](50) NULL,
	[ProcedureReadCode] [varchar](10) NULL,
	[ProcedureReadTermCode] [varchar](50) NULL,
	[IPSourceUniqueID] [varchar](50) NOT NULL,
	[SourceProcedureCode] [varchar](50) NULL,
	[SourceSequenceNo] [int] NULL,
	[InterfaceCode] [varchar](5) NULL,
	[SequenceNoCheck] [smallint] NULL
) ON [PRIMARY]
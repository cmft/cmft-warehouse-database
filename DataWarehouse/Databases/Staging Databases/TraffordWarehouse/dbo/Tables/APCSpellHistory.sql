﻿CREATE TABLE [dbo].[APCSpellHistory](
	[ProviderSpellNo] [varchar](50) NOT NULL,
	[Status] [varchar](2) NOT NULL,
	[RegisteredGpCode] [varchar](10) NULL,
	[RegisteredGpPracticeCode] [varchar](10) NULL,
	[PatientAddress1] [varchar](50) NULL,
	[PatientAddress2] [varchar](50) NULL,
	[PatientAddress3] [varchar](50) NULL,
	[PatientAddress4] [varchar](50) NULL,
	[Postcode] [varchar](50) NULL,
	[Created] [datetime] NOT NULL
) ON [PRIMARY]
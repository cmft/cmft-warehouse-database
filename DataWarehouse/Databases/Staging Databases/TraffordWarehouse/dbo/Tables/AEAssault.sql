﻿CREATE TABLE [dbo].[AEAssault](
	[AssaultRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[AESourceUniqueID] [varchar](50) NOT NULL,
	[AssaultDate] [datetime] NULL,
	[AssaultTime] [smalldatetime] NULL,
	[AssaultWeapon] [varchar](80) NULL,
	[AssaultWeaponDetails] [varchar](4000) NULL,
	[AssaultLocation] [varchar](80) NULL,
	[AssaultLocationDetails] [varchar](4000) NULL,
	[AlcoholConsumed3Hour] [varchar](80) NULL,
	[AssaultRelationship] [varchar](80) NULL,
	[AssaultRelationshipDetails] [varchar](4000) NULL,
	[Safeguarding] [varchar](4000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WardBase](
	[WardCode] [nvarchar](40) NOT NULL,
	[WardName] [nvarchar](80) NOT NULL,
	[WardAbbrev] [nvarchar](20) NOT NULL,
	[WardDescription] [nvarchar](255) NULL,
	[WardIsDeleted] [bit] NOT NULL,
	[SiteCode] [nvarchar](40) NOT NULL
) ON [PRIMARY]
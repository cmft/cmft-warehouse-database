﻿CREATE TABLE [dbo].[OPMissingFromMerg](
	[EncounterRecno] [int] NOT NULL,
	[Action] [varchar](6) NOT NULL,
	[RunDate] [datetime] NOT NULL
) ON [PRIMARY]
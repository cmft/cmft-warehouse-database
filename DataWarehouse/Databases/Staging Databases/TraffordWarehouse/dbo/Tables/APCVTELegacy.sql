﻿CREATE TABLE [dbo].[APCVTELegacy](
	[AdmissionNumber] [varchar](20) NOT NULL,
	[EPMINumber] [varchar](20) NULL,
	[AdmittedOn] [datetime] NULL,
	[DischargedOn] [datetime] NULL,
	[ConSurname] [varchar](100) NULL,
	[admittingward] [varchar](10) NULL,
	[VTECompleted] [varchar](20) NULL
) ON [PRIMARY]
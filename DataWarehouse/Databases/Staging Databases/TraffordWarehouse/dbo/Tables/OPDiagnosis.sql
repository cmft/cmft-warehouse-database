﻿CREATE TABLE [dbo].[OPDiagnosis](
	[DiagnosisRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NOT NULL,
	[SequenceNo] [smallint] NOT NULL,
	[DiagnosisCode] [varchar](50) NULL,
	[DiagnosisReadCode] [varchar](50) NULL,
	[DiagnosisReadTermCode] [varchar](50) NULL,
	[OPSourceUniqueID] [varchar](50) NULL,
	[SourceDiagnosisCode] [varchar](50) NULL,
	[SourceSequenceNo] [smallint] NULL,
	[InterfaceCode] [varchar](5) NULL
) ON [PRIMARY]
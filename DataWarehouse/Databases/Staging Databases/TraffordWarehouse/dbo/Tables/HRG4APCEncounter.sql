﻿CREATE TABLE [dbo].[HRG4APCEncounter](
	[EncounterRecno] [int] NOT NULL,
	[HRGCode] [varchar](10) NULL,
	[GroupingMethodFlag] [varchar](10) NULL,
	[DominantProcedureCode] [varchar](10) NULL,
	[PBCCode] [varchar](10) NULL,
	[CalculatedEpisodeDuration] [smallint] NULL,
	[ReportingEpisodeDuration] [smallint] NULL,
	[Trimpoint] [smallint] NULL,
	[ExcessBeddays] [smallint] NULL,
	[SpellReportFlag] [varchar](10) NULL
) ON [PRIMARY]
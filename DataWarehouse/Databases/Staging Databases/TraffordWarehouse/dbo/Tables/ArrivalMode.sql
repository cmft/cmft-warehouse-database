﻿CREATE TABLE [dbo].[ArrivalMode](
	[ArrivalModeCode] [int] NOT NULL,
	[ArrivalMode] [varchar](255) NULL,
	[ArrivalModeNationalCode] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WardStay](
	[WardStayRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[ProviderSpellNo] [varchar](50) NULL,
	[WardCode] [varchar](10) NULL,
	[SequenceNo] [tinyint] NULL,
	[SiteOfTreatmentCode] [varchar](9) NULL
) ON [PRIMARY]
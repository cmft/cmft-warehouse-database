﻿CREATE TABLE [dbo].[HRG4OPEncounter](
	[EncounterRecno] [int] NOT NULL,
	[HRGCode] [varchar](10) NULL,
	[GroupingMethodFlag] [varchar](10) NULL,
	[DominantProcedureCode] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Specialty](
	[SpecialtyCode] [varchar](10) NOT NULL,
	[Specialty] [varchar](80) NOT NULL,
	[DivisionCode] [varchar](10) NULL
) ON [PRIMARY]
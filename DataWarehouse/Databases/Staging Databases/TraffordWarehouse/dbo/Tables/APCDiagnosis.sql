﻿CREATE TABLE [dbo].[APCDiagnosis](
	[DiagnosisRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NOT NULL,
	[SequenceNo] [smallint] NOT NULL,
	[DiagnosisCode] [varchar](50) NULL,
	[DiagnosisReadCode] [varchar](50) NULL,
	[DiagnosisReadTermCode] [varchar](50) NULL,
	[IPSourceUniqueID] [varchar](50) NOT NULL,
	[SourceDiagnosisCode] [varchar](50) NULL,
	[SourceSequenceNo] [smallint] NULL,
	[InterfaceCode] [varchar](5) NULL,
	[SequenceNoCheck] [smallint] NULL
) ON [PRIMARY]
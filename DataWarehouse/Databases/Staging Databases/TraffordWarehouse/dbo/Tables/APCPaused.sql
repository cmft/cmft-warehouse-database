﻿CREATE TABLE [dbo].[APCPaused](
	[SuspensionStartDate] [datetime] NOT NULL,
	[SuspensionEndDate] [datetime] NULL,
	[ContactReasonCode] [varchar](50) NULL,
	[ContactStatusCode] [varchar](5) NULL,
	[ContactSourceUniqueID] [varchar](50) NULL,
	[SourceUniqueID] [varchar](50) NOT NULL,
	[PathwayID] [varchar](50) NULL,
	[MAXStatus] [varchar](5) NULL
) ON [PRIMARY]
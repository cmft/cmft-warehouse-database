﻿CREATE TABLE [dbo].[AEProcedure](
	[ProcedureRecno] [int] NOT NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[ProcedureDate] [datetime] NULL,
	[SequenceNo] [tinyint] NULL,
	[ProcedureSchemeInUse] [varchar](10) NULL,
	[ProcedureCode] [varchar](50) NULL,
	[AESourceUniqueID] [varchar](50) NULL,
	[SourceProcedureCode] [varchar](50) NULL,
	[SourceSequenceNo] [tinyint] NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF__AEProcedu__Creat__47A6A41B]  DEFAULT (getdate()),
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NOT NULL CONSTRAINT [DF__AEProcedu__ByWho__489AC854]  DEFAULT (suser_name())
) ON [PRIMARY]
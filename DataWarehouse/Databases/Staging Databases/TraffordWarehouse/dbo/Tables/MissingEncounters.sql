﻿CREATE TABLE [dbo].[MissingEncounters](
	[EncounterRecno] [int] IDENTITY(1,1) NOT NULL,
	[Action] [varchar](6) NOT NULL,
	[appointmentdate] [datetime] NULL
) ON [PRIMARY]
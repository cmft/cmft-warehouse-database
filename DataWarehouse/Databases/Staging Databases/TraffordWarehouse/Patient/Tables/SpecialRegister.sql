﻿CREATE TABLE [Patient].[SpecialRegister](
	[SpecialRegisterRecNo] [int] IDENTITY(1,1) NOT NULL,
	[SourcePatientNo] [int] NOT NULL,
	[SpecialRegisterCode] [varchar](4) NOT NULL,
	[Alert] [varchar](100) NOT NULL,
	[DistrictNo] [varchar](20) NULL,
	[NHSNumber] [varchar](17) NULL,
	[EnteredTime] [datetime] NOT NULL,
	[LastModifiedTime] [datetime] NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
	[Active] [bit] NULL,
	[Created] [datetime] NULL CONSTRAINT [df_Patient_SpecialRegister_CreatedTime]  DEFAULT (getdate()),
	[CreatedByWhom] [varchar](255) NULL CONSTRAINT [df_Patient_SpecialRegister_CreatedByWhom]  DEFAULT (suser_name()),
	[Updated] [datetime] NULL CONSTRAINT [df_Patient_SpecialRegister_UpdatedTime]  DEFAULT (getdate()),
	[UpdatedByWhom] [varchar](255) NULL CONSTRAINT [df_Patient_SpecialRegister_UpdatedByWhom]  DEFAULT (suser_name()),
	[ID] [int] NULL
) ON [PRIMARY]
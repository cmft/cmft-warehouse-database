﻿CREATE view  [ETL].[TimportEPRIDS] as

select 
	IDSForm.DocumentID
	--PatientNumbers.PatientNoID
	,DischargeDocumentID  = Discharge.DocumentID
	,Sourceuniqueid =  Discharge.DocumentID 
		
	,EPMI = PatientNumbers.CurrentNo
	,ProviderspellNO = DischargeMessage.DocumentXml.value('data(//AdmissionNumber)[1]', 'VARCHAR(100)')
	
	,IDSDocumentDate = ImmediateDischarge.DocumentDate
	,DischargeDate = ImmediateDischarge.DocumentXml.value('data(//DischargeDate)[1]', 'datetime')
	,AdmissionDate = DischargeMessage.DocumentXml.value('data(//AdmissionDate)[1]', 'datetime')
	,DischargeDateTime = Discharge.DocumentDate
	,IDSStatus = IDSStatus.status
	,DraftIDS
	,IDSXML = ImmediateDischarge.DocumentXml 
	,DischargeXML = DischargeMessage.DocumentXml
	,IDSForm.DocumentVersion
		
	,SendToGP =
		
		case
			when ImmediateDischarge.DocumentDate is not null and IDSStatus.status = 'Complete' then 1 
			when ImmediateDischarge.DocumentDate is not null and DraftIDS is null and IDSStatus.status is null  then 1
				
		else 0
		end 
	,IDSForm.TypeID
	,IDSForm.CreationDate
from
	EPR.PatientNumbers
		
inner join EPR.DocumentRegistry IDSForm
on	IDSForm.PatientNoID = PatientNumbers.PatientNoID
and TypeID = 209 --IDS Form	
	
			
left join EPR.Docs_ImmediateDischarge_Support IDSStatus
on	IDSStatus.documentID = IDSForm.DocumentID
and IDSStatus.patientNoID = IDSForm.PatientNoID
		
inner join EPR.Docs_ImmediateDischarge ImmediateDischarge
on	ImmediateDischarge.DocumentID = IDSForm.DocumentID

left join EPR.DocumentRegistry Discharge
on	Discharge.PatientNoID = PatientNumbers.PatientNoID
and Discharge.TypeID =  301 --UG Discharge
and Discharge.InactiveDocument = 0
and 
--Immediatedischarge.DocumentXML.value('data(//DischargeDate)[1]', 'datetime') 
	IDSForm.CreationDate between 
			DATEADD(dd, -1, DATEDIFF(dd, 0, Discharge.DocumentDate))
		and DATEADD(dd, 1, DATEDIFF(dd, 0, Discharge.DocumentDate))
				
left join EPR.Docs_DischargeUG DischargeMessage
on	DischargeMessage.DocumentID = Discharge.DocumentID
		
where 
--PatientNumbers.PatientNoID = 73568 and
	Discharge.TypeID = 301 --Discharge message from UG
and	IDSForm.InactiveDocument = 0 --Current Version only
			
	--and
	--	(
	--DATEADD(dd, 0, DATEDIFF(dd, 0, Discharge.DocumentDate)) between '01 april 2013' and DATEADD(dd, 0, DATEDIFF(dd, 0, getdate ())) ---10
	--or
	--	Discharge.DocumentDate is null
	--	and
	--	DATEADD(dd, 0, DATEDIFF(dd, 0, IDSForm.DocumentDate)) between '01 april 2013' and DATEADD(dd, 0, DATEDIFF(dd, 0, getdate ())) ---10
			
		--)
				


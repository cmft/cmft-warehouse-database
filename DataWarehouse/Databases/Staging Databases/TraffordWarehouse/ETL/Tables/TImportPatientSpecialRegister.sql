﻿CREATE TABLE [ETL].[TImportPatientSpecialRegister](
	[SourcePatientNo] [int] NOT NULL,
	[SpecialRegisterCode] [varchar](4) NOT NULL,
	[Alert] [varchar](100) NULL,
	[DistrictNo] [varchar](20) NULL,
	[NHSNumber] [varchar](17) NULL,
	[EnteredTime] [datetime] NULL,
	[LastModifiedTime] [datetime] NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[Active] [bit] NULL,
	[Created] [datetime] NULL,
	[ByWhom] [varchar](255) NULL,
	[ID] [int] NULL
) ON [PRIMARY]
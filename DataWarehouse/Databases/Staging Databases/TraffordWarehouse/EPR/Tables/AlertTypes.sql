﻿CREATE TABLE [EPR].[AlertTypes](
	[ID] [int] NOT NULL,
	[ParentID] [int] NULL,
	[Description] [varchar](100) NOT NULL,
	[AdditionalText] [varchar](2000) NULL,
	[Active] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [EPR].[PatientNumbers](
	[PatientNoID] [int] NOT NULL,
	[CurrentNo] [varchar](20) NOT NULL,
	[PreviousNo] [varchar](20) NULL
) ON [PRIMARY]
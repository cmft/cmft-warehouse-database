﻿CREATE TABLE [EPR].[Docs_ImmediateDischarge_Support](
	[id] [int] NOT NULL,
	[documentID] [int] NULL,
	[patientNoID] [int] NULL,
	[status] [nvarchar](50) NULL,
	[adminRemoved] [bit] NULL,
	[DraftIDS] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [EPR].[Docs_ImmediateDischarge](
	[DocumentID] [int] NOT NULL,
	[PatientNoID] [int] NOT NULL,
	[DocumentDate] [datetime] NOT NULL,
	[UniqueID] [varchar](50) NOT NULL,
	[DocumentXml] [xml] NULL,
	[DocumentVersion] [smallint] NOT NULL,
	[Stage] [smallint] NOT NULL,
	[SearchFlag] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
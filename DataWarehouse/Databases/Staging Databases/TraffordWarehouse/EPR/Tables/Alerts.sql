﻿CREATE TABLE [EPR].[Alerts](
	[ID] [int] NOT NULL,
	[AlertTypeID] [int] NOT NULL,
	[PatientNoID] [int] NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Description] [varchar](1000) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [EPR].[DocumentStatus](
	[DocumentStatusID] [int] IDENTITY(1,1) NOT NULL,
	[DocumentStatus] [varchar](50) NULL
) ON [PRIMARY]
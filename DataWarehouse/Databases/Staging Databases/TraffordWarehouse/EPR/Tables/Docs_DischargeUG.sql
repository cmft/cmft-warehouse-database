﻿CREATE TABLE [EPR].[Docs_DischargeUG](
	[DocumentID] [int] NOT NULL,
	[PatientNoID] [int] NOT NULL,
	[UniqueID] [varchar](50) NOT NULL,
	[Consultant] [varchar](15) NULL,
	[Location] [varchar](15) NULL,
	[DocumentDate] [datetime] NOT NULL,
	[DocumentXml] [xml] NULL,
	[Reason] [varchar](50) NULL,
	[DocumentVersion] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
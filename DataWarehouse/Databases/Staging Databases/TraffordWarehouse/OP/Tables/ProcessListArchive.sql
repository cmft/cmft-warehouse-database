﻿CREATE TABLE [OP].[ProcessListArchive](
	[EncounterRecno] [int] NOT NULL,
	[Action] [nvarchar](10) NULL,
	[ArchiveTime] [datetime] NOT NULL
) ON [PRIMARY]
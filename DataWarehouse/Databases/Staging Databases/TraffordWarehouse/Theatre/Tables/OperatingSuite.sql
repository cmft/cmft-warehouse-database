﻿CREATE TABLE [Theatre].[OperatingSuite](
	[OperatingSuiteCode] [int] NOT NULL,
	[OperatingSuite] [varchar](255) NULL,
	[OperatingSuiteCode1] [varchar](5) NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
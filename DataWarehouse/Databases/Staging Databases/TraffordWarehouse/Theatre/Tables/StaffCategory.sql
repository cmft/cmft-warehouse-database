﻿CREATE TABLE [Theatre].[StaffCategory](
	[StaffCategoryCode] [numeric](9, 0) NOT NULL,
	[StaffCategoryCode1] [varchar](5) NULL,
	[StaffCategory] [varchar](60) NULL,
	[SurgeonFlag] [bit] NOT NULL,
	[InactiveFlag] [bit] NOT NULL,
	[ConsultantFlag] [bit] NOT NULL,
	[SupervisionFlag] [bit] NOT NULL,
	[AnaesthetistFlag] [bit] NOT NULL,
	[OtherFlag] [bit] NOT NULL,
	[NurseFlag] [bit] NOT NULL,
	[PorterFlag] [bit] NOT NULL,
	[Technician1Flag] [bit] NOT NULL,
	[Technician2Flag] [bit] NOT NULL,
	[ClerkFlag] [bit] NOT NULL
) ON [PRIMARY]
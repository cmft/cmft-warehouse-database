﻿CREATE TABLE [Theatre].[TimportTheatre](
	[TheatreCode] [int] NULL,
	[Theatre] [varchar](255) NULL,
	[OperatingSuiteCode] [int] NULL,
	[TheatreCode1] [varchar](8) NULL,
	[InactiveFlag] [bit] NULL,
	[InterfaceCode] [varchar](10) NULL
) ON [PRIMARY]
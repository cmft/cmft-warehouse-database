﻿CREATE TABLE [Theatre].[TimportSpecialty](
	[SpecialtyCode] [int] NULL,
	[Specialty] [varchar](255) NULL,
	[SpecialtyCode1] [varchar](8) NULL,
	[InactiveFlag] [bit] NULL,
	[InterfaceCode] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Theatre].[Ward](
	[WardCode] [int] NOT NULL,
	[Ward] [varchar](255) NULL,
	[WardCode1] [varchar](20) NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
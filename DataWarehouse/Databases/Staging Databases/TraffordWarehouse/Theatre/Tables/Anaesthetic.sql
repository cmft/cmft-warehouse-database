﻿CREATE TABLE [Theatre].[Anaesthetic](
	[AnaestheticCode] [int] NOT NULL,
	[Anaesthetic] [varchar](255) NULL,
	[AnaestheticCode1] [varchar](6) NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
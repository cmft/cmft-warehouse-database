﻿CREATE TABLE [Theatre].[Specialty](
	[SpecialtyCode] [int] NOT NULL,
	[Specialty] [varchar](255) NULL,
	[SpecialtyCode1] [varchar](8) NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Theatre].[TimportStaffCategory](
	[StaffCategoryCode] [numeric](9, 0) NULL,
	[StaffCategoryCode1] [varchar](60) NULL,
	[StaffCategory] [varchar](60) NULL,
	[SurgeonFlag] [bit] NULL,
	[InactiveFlag] [bit] NULL,
	[ConsultantFlag] [bit] NULL,
	[SupervisionFlag] [bit] NULL,
	[AnaesthetistFlag] [bit] NULL,
	[OtherFlag] [bit] NULL,
	[NurseFlag] [bit] NULL,
	[PorterFlag] [bit] NULL,
	[Technician1Flag] [bit] NULL,
	[Technician2Flag] [bit] NULL,
	[ClerkFlag] [bit] NULL,
	[InterfaceCode] [varchar](10) NULL
) ON [PRIMARY]
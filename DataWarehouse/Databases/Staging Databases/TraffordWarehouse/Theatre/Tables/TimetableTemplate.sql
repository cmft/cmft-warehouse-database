﻿CREATE TABLE [Theatre].[TimetableTemplate](
	[TimetableTemplateCode] [int] NOT NULL,
	[SessionNumber] [tinyint] NOT NULL,
	[DayNumber] [tinyint] NOT NULL,
	[TheatreCode] [int] NULL,
	[SessionStartTime] [datetime] NULL,
	[SessionEndTime] [datetime] NULL,
	[ConsultantCode] [varchar](10) NULL,
	[AnaesthetistCode] [varchar](10) NULL,
	[SpecialtyCode] [varchar](10) NULL,
	[LogLastUpdated] [datetime] NULL,
	[RecordLogTemplates] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [msec].[Comorbidities](
	[CoRecNo] [int] IDENTITY(1,1) NOT NULL,
	[CasenoteNumber] [varchar](20) NULL,
	[PatientNo] [varchar](8) NOT NULL,
	[SpellNo] [varchar](5) NULL,
	[AdmissionDate] [datetime] NULL,
	[Type] [varchar](30) NOT NULL,
	[System] [varchar](40) NOT NULL,
	[CodeFlag] [varchar](40) NULL,
	[FullDiagnosis] [varchar](310) NOT NULL,
	[Status] [varchar](80) NULL,
	[EventDate] [datetime] NOT NULL,
	[RetirementDate] [datetime] NULL
) ON [PRIMARY]
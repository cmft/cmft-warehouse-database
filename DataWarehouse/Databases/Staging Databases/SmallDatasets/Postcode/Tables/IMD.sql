﻿CREATE TABLE [Postcode].[IMD](
	[LSOA CODE] [nvarchar](255) NULL,
	[LA CODE] [nvarchar](255) NULL,
	[LA NAME] [nvarchar](255) NULL,
	[GOR CODE] [nvarchar](255) NULL,
	[GOR NAME] [nvarchar](255) NULL,
	[IMD SCORE] [float] NULL,
	[RANK] [float] NULL
) ON [PRIMARY]
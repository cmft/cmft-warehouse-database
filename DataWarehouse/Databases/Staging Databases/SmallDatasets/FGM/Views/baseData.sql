﻿
CREATE view [FGM].[baseData]

as

  
WITH XMLNAMESPACES('http://schemas.microsoft.com/office/infopath/2003/myXSD/2012-08-30T10:18:22' as my)

SELECT	
	  
	  XML.value('/my:FGMValues[1]/my:UN[1]', 'varchar(200)') UserName,
	  XML.value('/my:FGMValues[1]/my:dpFGMIdentified[1]', 'varchar(200)') FGMIdentified,
	  XML.value('/my:FGMValues[1]/my:txtEmail[1]', 'varchar(200)') txtEmail,
	  XML.value('/my:FGMValues[1]/my:txtCasenote[1]', 'varchar(200)') txtCasenote,
	  XML.value('/my:FGMValues[1]/my:txtDOB[1]', 'varchar(200)') txtDOB,
	  XML.value('/my:FGMValues[1]/my:txtDepartment[1]', 'varchar(200)') txtDepartment,
	  XML.value('/my:FGMValues[1]/my:FGMType[1]', 'varchar(200)') FGMType,
	    
		  XML.value('/my:FGMValues[1]/my:ddlDeinfibulationPerformed[1]', 'varchar(200)') ddlDeinfibulationPerformed,
		   XML.value('/my:FGMValues[1]/my:RepeatDeinfibulation[1]', 'varchar(200)') RepeatDeinfibulation,
		    XML.value('/my:FGMValues[1]/my:ddlRepeatDeinfibulation[1]', 'varchar(200)') ddlRepeatDeinfibulation,
			 XML.value('/my:FGMValues[1]/my:Email[1]', 'varchar(200)') Email,
			  XML.value('/my:FGMValues[1]/my:ddlFamilyHistory[1]', 'varchar(200)') ddlFamilyHistory , XML
      
from 
	[FGM].[baseExtract]



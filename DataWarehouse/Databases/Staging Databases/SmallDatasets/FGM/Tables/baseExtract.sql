﻿CREATE TABLE [FGM].[baseExtract](
	[ID] [bigint] NOT NULL,
	[FormID] [bigint] NULL,
	[XML] [xml] NOT NULL,
	[DateAdded] [datetime2](3) NULL,
	[UserConfirm] [bit] NULL,
	[DDConfirm] [bit] NULL,
	[CommsConfirm] [bit] NULL,
	[CommsAuthoriser] [varchar](200) NULL,
	[DocConfirm] [bit] NULL,
	[DocAuthoriser] [varchar](200) NULL,
	[IPAddress] [varchar](200) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
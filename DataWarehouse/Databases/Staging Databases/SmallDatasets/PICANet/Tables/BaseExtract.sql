﻿CREATE TABLE [PICANet].[BaseExtract](
	[ExtractRecno] [int] IDENTITY(1,1) NOT NULL,
	[xmlData] [xml] NULL,
	[ExtractFilename] [varchar](255) NULL,
	[ExtractFileLastModified] [datetime] NULL,
	[ImportedTime] [datetime] NULL,
	[ImportedBy] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
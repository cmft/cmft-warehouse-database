﻿CREATE TABLE [ref].[dates](
	[month] [nvarchar](20) NULL,
	[date] [varchar](20) NULL,
	[finYear] [varchar](5) NULL
) ON [PRIMARY]
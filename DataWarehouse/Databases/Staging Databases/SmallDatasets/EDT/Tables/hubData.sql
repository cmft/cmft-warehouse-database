﻿CREATE TABLE [EDT].[hubData](
	[CasenoteNumber] [varchar](20) NULL,
	[DischargeDateTime] [datetime2](7) NULL,
	[DateTimeSentToHub] [datetime2](7) NULL,
	[Hospital] [varchar](20) NULL,
	[firstName] [varchar](50) NULL,
	[surName] [varchar](50) NULL,
	[datetimeimported] [datetime2](7) NULL DEFAULT (getdate()),
	[Interface] [varchar](20) NULL,
	[InternalPatientNumber] [varchar](20) NULL,
	[DocumentID] [varchar](40) NULL,
	[ContextCode] [varchar](20) NULL
) ON [PRIMARY]
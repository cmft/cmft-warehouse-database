﻿CREATE TABLE [EDT].[ActivePractice](
	[EncounterRecno] [int] IDENTITY(1,1) NOT NULL,
	[PracticeName] [varchar](max) NULL,
	[PracticeCode] [varchar](10) NOT NULL,
	[Activated] [int] NULL,
	[DateActivated] [datetime] NULL,
	[DateDeActiviated] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
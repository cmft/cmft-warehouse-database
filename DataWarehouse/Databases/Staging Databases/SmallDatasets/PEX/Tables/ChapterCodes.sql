﻿CREATE TABLE [PEX].[ChapterCodes](
	[ICD10 Code] [nvarchar](10) NULL,
	[ICD10 Chapter (roman numeric code)] [nvarchar](10) NULL
) ON [PRIMARY]
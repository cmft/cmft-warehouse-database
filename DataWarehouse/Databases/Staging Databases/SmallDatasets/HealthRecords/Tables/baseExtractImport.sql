﻿CREATE TABLE [HealthRecords].[baseExtractImport](
	[xml_data] [xml] NULL,
	[DateImported] [datetime] NOT NULL,
	[filename] [varchar](300) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
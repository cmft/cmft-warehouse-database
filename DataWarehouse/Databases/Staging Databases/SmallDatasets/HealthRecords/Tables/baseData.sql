﻿CREATE TABLE [HealthRecords].[baseData](
	[TransportId] [varchar](100) NULL,
	[BatchIdentifier] [varchar](100) NULL,
	[JobName] [varchar](100) NULL,
	[StartTime] [varchar](100) NULL,
	[OperatorName] [varchar](100) NULL,
	[Response] [varchar](100) NULL,
	[EndTime] [varchar](100) NULL,
	[NumPages] [varchar](100) NULL,
	[DateImported] [datetime] NOT NULL,
	[filename] [varchar](300) NULL
) ON [PRIMARY]
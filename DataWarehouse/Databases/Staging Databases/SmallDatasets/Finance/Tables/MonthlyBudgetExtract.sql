﻿CREATE TABLE [Finance].[MonthlyBudgetExtract](
	[Date] [nvarchar](20) NULL,
	[Type] [nvarchar](50) NULL,
	[Division] [nvarchar](100) NULL,
	[Value] [int] NULL,
	[month] [varchar](13) NULL
) ON [PRIMARY]
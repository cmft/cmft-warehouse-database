﻿CREATE TABLE [Finance].[RiskRating](
	[Census date] [date] NULL,
	[Month] [date] NULL,
	[Surgery] [varchar](1) NULL,
	[Medicine & Community] [varchar](1) NULL,
	[CSS] [varchar](1) NULL,
	[Specialist Medical Services] [varchar](1) NULL,
	[REH] [varchar](1) NULL,
	[UDH] [varchar](1) NULL,
	[SMH] [varchar](1) NULL,
	[RMCH] [varchar](1) NULL,
	[Trafford ] [varchar](1) NULL,
	[Trust Overall] [varchar](1) NULL
) ON [PRIMARY]
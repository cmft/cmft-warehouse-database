﻿CREATE TABLE [Finance].[RiskRatingByDivision](
	[ID] [bigint] NULL,
	[Census] [date] NULL,
	[Month] [date] NULL,
	[Division] [varchar](21) NOT NULL,
	[RiskRating] [varchar](1) NULL
) ON [PRIMARY]
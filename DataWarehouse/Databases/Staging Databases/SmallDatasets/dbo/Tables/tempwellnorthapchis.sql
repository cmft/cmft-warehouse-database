﻿CREATE TABLE [dbo].[tempwellnorthapchis](
	[MergeEncounterRecno] [int] NOT NULL,
	[PATID] [varchar](50) NULL,
	[SPELLID] [varchar](50) NULL,
	[NHSnumber] [varchar](17) NULL,
	[Casenotenumber] [varchar](20) NULL,
	[EPI_ID] [int] NULL,
	[EPI_NUM] [int] NULL,
	[POSTCD] [varchar](4) NULL,
	[SEX] [varchar](1) NULL,
	[AGE_DISCH] [numeric](18, 6) NULL,
	[DEATH_DT] [smalldatetime] NULL,
	[DEATH_CSE] [int] NULL,
	[ETHNIC] [varchar](50) NULL,
	[CARER] [int] NULL,
	[PATCLASS] [varchar](50) NULL,
	[ADMIMETH] [varchar](50) NULL,
	[ADMISORC] [varchar](50) NULL,
	[ADMIDATE] [smalldatetime] NULL,
	[DISCDATE] [smalldatetime] NULL,
	[EPISTART] [smalldatetime] NULL,
	[EPIEND] [smalldatetime] NULL,
	[DISDEST] [varchar](50) NULL,
	[DISMETH] [varchar](50) NULL,
	[SPELL_LOS] [int] NULL,
	[EPI_LOS] [smallint] NULL,
	[SPELLEND] [int] NOT NULL,
	[DIAG01] [varchar](10) NULL,
	[DIAG02] [varchar](10) NULL,
	[DIAG03] [varchar](10) NULL,
	[DIAG04] [varchar](10) NULL,
	[DIAG05] [varchar](10) NULL,
	[DIAG06] [varchar](10) NULL,
	[DIAG07] [varchar](10) NULL,
	[DIAG08] [varchar](10) NULL,
	[DIAG09] [varchar](10) NULL,
	[DIAG10] [varchar](10) NULL,
	[DIAG11] [varchar](10) NULL,
	[DIAG12] [varchar](10) NULL,
	[DIAG13] [varchar](10) NULL,
	[DIAG14] [varchar](10) NULL,
	[DIAG15] [varchar](10) NULL,
	[DIAG16] [varchar](10) NULL,
	[DIAG17] [varchar](10) NULL,
	[DIAG18] [varchar](10) NULL,
	[DIAG19] [varchar](10) NULL,
	[DIAG20] [varchar](10) NULL,
	[PROC01] [varchar](10) NULL,
	[PROC02] [varchar](10) NULL,
	[PROC03] [varchar](10) NULL,
	[PROC04] [varchar](10) NULL,
	[PROC05] [varchar](10) NULL,
	[PROC06] [varchar](10) NULL,
	[PROC07] [varchar](10) NULL,
	[PROC08] [varchar](10) NULL,
	[PROC09] [varchar](10) NULL,
	[PROC10] [varchar](10) NULL,
	[PROC11] [varchar](10) NULL,
	[PROC12] [varchar](10) NULL,
	[PROC13] [varchar](10) NULL,
	[PROC14] [varchar](10) NULL,
	[PROC15] [varchar](10) NULL,
	[PROC16] [varchar](10) NULL,
	[PROC17] [varchar](10) NULL,
	[PROC18] [varchar](10) NULL,
	[PROC19] [varchar](10) NULL,
	[PROC20] [varchar](10) NULL,
	[PROCDT1] [smalldatetime] NULL,
	[PROCDT2] [smalldatetime] NULL,
	[PROCDT3] [smalldatetime] NULL,
	[PROCDT4] [smalldatetime] NULL,
	[PROCDT5] [smalldatetime] NULL,
	[PROCDT6] [smalldatetime] NULL,
	[PROCDT7] [smalldatetime] NULL,
	[PROCDT8] [smalldatetime] NULL,
	[PROCDT9] [smalldatetime] NULL,
	[PROCDT10] [smalldatetime] NULL,
	[PROCDT11] [smalldatetime] NULL,
	[PROCDT12] [smalldatetime] NULL,
	[PROCDT13] [smalldatetime] NULL,
	[PROCDT14] [smalldatetime] NULL,
	[PROCDT15] [smalldatetime] NULL,
	[PROCDT16] [smalldatetime] NULL,
	[PROCDT17] [smalldatetime] NULL,
	[PROCDT18] [smalldatetime] NULL,
	[PROCDT19] [smalldatetime] NULL,
	[PROCDT20] [smalldatetime] NULL,
	[PROCODE] [varchar](10) NULL,
	[PROSITE] [varchar](50) NULL,
	[REFGP] [varchar](8) NULL,
	[REGGP] [varchar](10) NULL,
	[GPPRAC] [varchar](10) NULL,
	[COMMISSIONER CODE] [varchar](10) NULL,
	[SHA] [varchar](3) NULL,
	[DEPRIV_SC] [int] NULL,
	[DEPRIV_RK] [int] NULL,
	[LSOA] [int] NULL,
	[MSOA] [int] NULL,
	[CONSULTANT CODE] [varchar](50) NULL,
	[MAIN SPECIALTY CODE] [varchar](20) NULL,
	[TREATMENT FUNCTION CODE] [varchar](50) NULL
) ON [PRIMARY]
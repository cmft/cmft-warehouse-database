﻿CREATE TABLE [dbo].[BvsA](
	[Month] [datetime] NULL,
	[Budget/Actual] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[Value £000's] [float] NULL,
	[Monthly Variance £000's] [float] NULL,
	[F6] [nvarchar](255) NULL,
	[F7] [float] NULL
) ON [PRIMARY]
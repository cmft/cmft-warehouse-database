﻿CREATE TABLE [dbo].[ResultCodeMapping](
	[ResultContextCode] [varchar](255) NOT NULL,
	[ResultContext] [varchar](255) NOT NULL,
	[ResultCode] [varchar](255) NOT NULL,
	[Result] [varchar](600) NULL,
	[CWSResultCode] [varchar](255) NULL,
	[CWSResult] [varchar](600) NULL,
	[GPICEResultCode] [varchar](255) NULL,
	[GPICEResult] [varchar](600) NULL,
	[CentralICEResultCode] [varchar](255) NULL,
	[CentralICEResult] [varchar](600) NULL
) ON [PRIMARY]
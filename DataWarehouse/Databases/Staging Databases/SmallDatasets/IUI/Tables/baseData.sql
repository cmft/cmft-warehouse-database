﻿CREATE TABLE [IUI].[baseData](
	[DOB] [datetime] NULL,
	[NHSNumber] [nvarchar](255) NULL,
	[Attempt] [float] NULL,
	[Hospnum] [nvarchar](255) NULL,
	[Source] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[FollStart] [nvarchar](255) NULL,
	[TrDate] [datetime] NULL,
	[Outcome] [nvarchar](255) NULL,
	[TreatType] [nvarchar](255) NULL,
	[Type] [nvarchar](255) NULL,
	[importDate] [datetime2](7) NULL,
	[filename] [varchar](30) NULL,
	[created] [datetime2](7) NULL,
	[updated] [datetime2](7) NULL,
	[byWhom] [varchar](50) NULL
) ON [PRIMARY]
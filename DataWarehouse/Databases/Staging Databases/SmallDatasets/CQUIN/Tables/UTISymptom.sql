﻿CREATE TABLE [CQUIN].[UTISymptom](
	[Bid] [int] NOT NULL,
	[UTISymptonsObservedTS] [datetime] NULL,
	[UTISymptonsClosedTS] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[WardCapturedInfo] [varchar](50) NULL
) ON [PRIMARY]
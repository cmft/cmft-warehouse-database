﻿CREATE TABLE [CQUIN].[VTEAssessment](
	[Bid] [int] NOT NULL,
	[NoAssessmentReasonID] [int] NULL,
	[AssessmentCompletedTS] [datetime] NULL,
	[ConsideredAtRisk] [varchar](10) NULL,
	[MedicationStartedTS] [datetime] NULL,
	[NursingActionsStartedTS] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[WardCapturedInfo] [varchar](50) NULL
) ON [PRIMARY]
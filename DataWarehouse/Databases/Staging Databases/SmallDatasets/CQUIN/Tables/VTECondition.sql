﻿CREATE TABLE [CQUIN].[VTECondition](
	[Bid] [int] NOT NULL,
	[DiagnosisTS] [datetime] NULL,
	[VTETypeID] [int] NULL,
	[MedicationRequired] [varchar](10) NULL,
	[MedicationStartedTS] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[WardCapturedInfo] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [CQUIN].[APCFriendsFamilyTestReturn](
	[Return] [nvarchar](255) NULL,
	[ReturnMonth] [datetime] NULL,
	[HospitalSiteCode] [nvarchar](255) NULL,
	[HospitalSite] [nvarchar](255) NULL,
	[Ward] [nvarchar](255) NULL,
	[1ExtremelyLikely] [float] NULL,
	[2Likely] [float] NULL,
	[3NeitherLikelyNorUnlikely] [float] NULL,
	[4Unlikely] [float] NULL,
	[5ExtremelyUnlikely] [float] NULL,
	[6DontKnow] [float] NULL,
	[EligibleResponders] [float] NULL,
	[WardCode] [nvarchar](255) NULL,
	[Created] [datetime] NULL CONSTRAINT [DF_APCFriendsFamilyTestReturn_Created]  DEFAULT (getdate()),
	[ByWhom] [varchar](50) NULL CONSTRAINT [DF_APCFriendsFamilyTestReturn_ByWhom]  DEFAULT (suser_name())
) ON [PRIMARY]
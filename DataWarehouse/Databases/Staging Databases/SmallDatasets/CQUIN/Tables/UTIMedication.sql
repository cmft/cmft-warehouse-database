﻿CREATE TABLE [CQUIN].[UTIMedication](
	[Bid] [int] NOT NULL,
	[UrinaryTestTreatmentStartedTS] [datetime] NULL,
	[TreatmentFinishedTS] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[WardCapturedInfo] [varchar](50) NULL
) ON [PRIMARY]
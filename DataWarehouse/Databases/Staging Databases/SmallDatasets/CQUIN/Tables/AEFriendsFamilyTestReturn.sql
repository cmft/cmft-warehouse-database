﻿CREATE TABLE [CQUIN].[AEFriendsFamilyTestReturn](
	[Return] [nvarchar](255) NULL,
	[ReturnMonth] [datetime] NULL,
	[HospitalSiteCode] [nvarchar](255) NULL,
	[HospitalSite] [nvarchar](255) NULL,
	[1ExtremelyLikely] [float] NULL,
	[2Likely] [float] NULL,
	[3NeitherLikelyNorUnlikely] [float] NULL,
	[4Unlikely] [float] NULL,
	[5ExtremelyUnlikely] [float] NULL,
	[6DontKnow] [float] NULL,
	[EligibleResponders] [float] NULL,
	[Created] [datetime] NULL CONSTRAINT [DF_AEFriendsFamilyTestReturn_Created]  DEFAULT (getdate()),
	[ByWhom] [varchar](50) NULL CONSTRAINT [DF_AEFriendsFamilyTestReturn_ByWhom]  DEFAULT (suser_name())
) ON [PRIMARY]
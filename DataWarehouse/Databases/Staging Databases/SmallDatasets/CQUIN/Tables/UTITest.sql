﻿CREATE TABLE [CQUIN].[UTITest](
	[Bid] [int] NOT NULL,
	[UrinaryTestResultsSentTS] [datetime] NULL,
	[UrinaryTestResultsReceivedTS] [datetime] NULL,
	[TestResultsPositive] [varchar](10) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[WardCapturedInfo] [varchar](50) NULL
) ON [PRIMARY]
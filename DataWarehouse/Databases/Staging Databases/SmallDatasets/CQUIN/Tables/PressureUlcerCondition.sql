﻿CREATE TABLE [CQUIN].[PressureUlcerCondition](
	[Bid] [int] NOT NULL,
	[PressureUlcerLocationID] [int] NULL,
	[IdentifiedTS] [datetime] NULL,
	[Category] [int] NULL,
	[Validated] [varchar](10) NULL,
	[HealedTS] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[WardCapturedInfo] [varchar](50) NULL
) ON [PRIMARY]
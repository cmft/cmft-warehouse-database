﻿CREATE TABLE [CQUIN].[CatheterIntervention](
	[Bid] [int] NOT NULL,
	[InsertedTS] [datetime] NULL,
	[RemovedTS] [datetime] NULL,
	[CatheterType] [int] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[WardCapturedInfo] [varchar](50) NULL
) ON [PRIMARY]
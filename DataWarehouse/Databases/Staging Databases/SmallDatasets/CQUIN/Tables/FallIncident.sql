﻿CREATE TABLE [CQUIN].[FallIncident](
	[Bid] [int] NOT NULL,
	[FallTS] [datetime] NULL,
	[InitialSeverity] [varchar](10) NULL,
	[Validated] [varchar](10) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[WardCapturedInfo] [varchar](50) NULL
) ON [PRIMARY]
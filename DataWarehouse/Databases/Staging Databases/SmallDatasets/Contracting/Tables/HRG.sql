﻿CREATE TABLE [Contracting].[HRG](
	[ID] [int] NOT NULL,
	[Base_HRG_Code] [varchar](50) NOT NULL,
	[HRG_Code] [varchar](50) NOT NULL,
	[HRG_Description] [varchar](100) NOT NULL,
	[Special_Service_ID] [varchar](10) NOT NULL,
	[Special_Service_ID_2] [varchar](10) NOT NULL,
	[HRG_Chapter] [char](1) NOT NULL,
	[HRG_Subchapter] [char](2) NOT NULL,
	[Currency] [int] NULL,
	[High_Cost] [bit] NULL
) ON [PRIMARY]
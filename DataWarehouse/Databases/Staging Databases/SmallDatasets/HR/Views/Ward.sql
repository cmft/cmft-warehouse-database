﻿CREATE view [HR].[Ward]

as

/*
	The cost centre code is used as a code to describe level 5 organisations. Problem is that for District Nursing
	there are multiple level 5 organisations that share the same cost centre. There is only one other example of this
	so rather than create codes we will just remain at the cost centre level and use the first organisation alphabetically 
	associated with the cost centre

*/

select
	WardCode = OrganisationCostCentreCode
	--,Division	
	,Ward = min(Organisation)
	--,CensusDate
from
	(
	select 
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Division
		,Organisation = substring(Organisation, 8, len(Organisation))
		,CensusDate
	from
		HR.AppraisalStaging

	union 

	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Division	
		,Organisation = substring(Organisation, 8, len(Organisation))
		,CensusDate
	from
		HR.ClinicalMandatoryTrainingStaging

	union 

	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Division	
		,Organisation = substring(Organisation, 8, len(Organisation))
		,CensusDate
	from
		HR.CorporateMandatoryTrainingStaging

	union 

	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Division	
		,Organisation = substring(Organisation, 8, len(Organisation))
		,CensusDate
	from
		HR.RetentionStaging

	union 

	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Division = cast(Division as varchar)		
		,Organisation = substring(Organisation, 8, len(Organisation))
		,CensusDate
	from
		HR.SicknessStaging

	union 

	select
		OrganisationCostCentreCode = '349' + [Cost Centre] -- to match the format of the other HR datasets
		,Division		
		,Organisation = [Cost Centre Desc]
		,CensusDate = [Census Date]
	from
		HR.WTEStaging
	
	)	Organisation
	
--where
--	Organisation like '%Ward%'


where
	OrganisationCostCentreCode in (	'349623697',
									'349623698',
									'349621508',
									'349621925',
									'349621507',
									'349621203',
									'349623810',
									'349623809',
									'349623808',
									'349621228',
									'349621622',
									'349621621',
									'349621540',
									'349621515',
									'349621518',
									'349622220',
									'349621506',
									'349622817',
									'349622812',
									'349621224',
									'349622132',
									'349622245',
									'349622225',
									'349622050',
									'349623695',
									'349625453',
									'349625650',
									'349625653',
									'349625350',
									'349629835',
									'349625351',
									'349625352',
									'349623693',
									'349625551',
									'349625520',
									'349625550',
									'349625552',
									'349625553',
									'349625451',
									'349625452',
									'349625463',
									'349625450',
									'349625767',
									'349623696',
									'349621520',
									'349621521',
									'349621514',
									'349621725',
									'349621505',
									'349621517',
									'349622219',
									'349621619',
									'349621226',
									'349621222',
									'349621054',
									'349622612',
									'349621730',
									'349621182',
									'349621541',
									'349621011',
									'349621610',
									'349621625',
									'349621630',
									'349621614',
									'349621609',
									'349621114',
									'349621831',
									'349622612',
									'349621726',
									'349621920',
									'349621940',
									'349621850',
									'349621912',
									'349621847',
									'349623826',
									'349621966',
									'349621847',
									'349621971',
									'349621924',
									'349621968',
									'349621926',
									'349621973',
									'349621927',
									'349621928',
									'349621929',
									'349621930'		)

group by
	OrganisationCostCentreCode
	--,CensusDate




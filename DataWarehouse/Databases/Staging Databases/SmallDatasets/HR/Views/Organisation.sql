﻿CREATE view [HR].[Organisation]

as

/*
	The cost centre code is used as a code to describe level 5 organisations. Problem is that for District Nursing
	there are multiple level 5 organisations that share the same cost centre. There is only one other example of this
	so rather than create codes we will just remain at the cost centre level and use the first organisation alphabetically 
	associated with the cost centre

	We also seem to have organisations belonging to more than one direcrorate. We take the first direcrorate associated 
	alphabetically
	
	20151116	RR	Bringing in HR.Qualified Nursing & Midwifery vacancies.  Information cannot be provided at cost centre level at present
					Therefore setting a default cost centre code for the higher level Division data they send.
					To enable us to still use the same ETL process.
					
*/

select 
	OrganisationCode = coalesce(OrganisationCostCentreCode,-999)
	,Organisation = upper(min(Organisation))
	,Directorate = upper(min(Directorate))
	,CensusDate
from
	(
	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Organisation = substring(Organisation, 8, len(Organisation))
		,Directorate = substring(Directorate, 8, len(Directorate))
		,CensusDate
	from
		HR.AppraisalStaging

	union 

	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Organisation = substring(Organisation, 8, len(Organisation))
		,Directorate = substring(Directorate, 8, len(Directorate))
		,CensusDate
	from
		HR.ClinicalMandatoryTrainingStaging

	union 

	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Organisation = substring(Organisation, 8, len(Organisation))
		,Directorate = substring(Directorate, 8, len(Directorate))
		,CensusDate
	from
		HR.CorporateMandatoryTrainingStaging

	union 

	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Organisation = substring(Organisation, 8, len(Organisation))
		,Directorate = substring(Directorate, 8, len(Directorate))
		,CensusDate
	from
		HR.RetentionStaging

	union 

	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Organisation = substring(Organisation, 8, len(Organisation))
		,Directorate = substring(Directorate, 8, len(Directorate))
		,CensusDate
	from
		HR.SicknessStaging

	union 

	select
		OrganisationCostCentreCode = '349' + [Cost Centre] -- to match the format of the other HR datasets
		,Organisation = [Cost Centre Desc]
		,Directorate 
		,CensusDate = [Census Date]
	from
		HR.WTEStaging
	
	union 
	-- 20141022 RR added
	select
		OrganisationCostCentreCode = [Organisation Cost Centre]
		,Organisation = substring(Organisation, 8, len(Organisation))
		,Directorate = substring(Directorate, 8, len(Directorate))
		,CensusDate
	from
		HR.TurnoverStaging
	
	union 
	-- 20150109 RR added (new indicator A&C Agency spend)
	select
		OrganisationCostCentreCode = '349' + [Organisation Cost Centre] -- to match the format of the other HR datasets
		,Organisation 
		,Directorate 
		,CensusDate = [Census Date]
	from
		HR.AdminClericalAgencySpendStaging
	union
	-- 20150909 RR added (new indicator BME Retention)
	select
		OrganisationCostCentreCode = [Organisation Cost Centre] 
		,Organisation 
		,Directorate 
		,CensusDate 
	from 
		HR.BMERetentionStaging
		
	union	
	
	select
		OrganisationCostCentre 
		,Organisation 
		,Division 
		,CensusDate 
	from 
		HR.QualifiedNursingAndMidwiferyStaging
		
		
		
	)	Organisation

where
	CensusDate is not null
--OrganisationCostCentreCode is not null  --20141022 RR removed where criteria, as we need to include activity counts where the Organisation Cost Centre is null -- make it unnasigned
--and OrganisationCostCentreCode = '349620011'
group by
	OrganisationCostCentreCode
	,CensusDate

-- Original 5045 rows
-- RR added Turnover Staging = 5154
-- Changed the where criteria to CensusDate not null rather than CostCentre Code is not null = 5156



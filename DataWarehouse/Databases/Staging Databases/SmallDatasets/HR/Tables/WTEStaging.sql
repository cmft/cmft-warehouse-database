﻿CREATE TABLE [HR].[WTEStaging](
	[Census Date] [datetime] NULL,
	[Cost Centre Desc] [nvarchar](255) NULL,
	[Cost Centre] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Subjective Code Desc] [nvarchar](255) NULL,
	[Wte Budget] [float] NULL,
	[Wte Contracted] [float] NULL,
	[WTE Variance (Under)/Over] [float] NULL,
	[Staff Group] [nvarchar](50) NULL,
	[Created] [datetime] NULL CONSTRAINT [DF__WTEStagin__Creat__3864608B]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [HR].[SicknessStaging](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [ntext] NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[Abs (FTE)] [float] NULL,
	[Avail (FTE)] [float] NULL,
	[% Abs Rate (FTE)] [float] NULL,
	[% Abs Rate (FTE)1] [float] NULL,
	[% Abs Rate (FTE)2] [float] NULL,
	[Sickness Occasions] [float] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL CONSTRAINT [DF__SicknessS__Creat__72C60C4A]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [HR].[TimeToFillExtract](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[AVERAGE WORKING DAYS TIME TO FILL] [float] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL
) ON [PRIMARY]
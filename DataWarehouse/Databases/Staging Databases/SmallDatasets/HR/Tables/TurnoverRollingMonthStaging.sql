﻿CREATE TABLE [HR].[TurnoverRollingMonthStaging](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[Headcount 3mthrolling] [float] NULL,
	[FTE 3mthrolling] [float] NULL,
	[Starters Headcount 3mthrolling] [float] NULL,
	[Starters FTE 3mthrolling] [float] NULL,
	[Leavers Headcount 3mthrolling] [float] NULL,
	[Leavers FTE 3mthrolling] [float] NULL,
	[LTR Headcount % 3mthrolling] [float] NULL,
	[LTR FTE % 3mthrolling] [float] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL
) ON [PRIMARY]
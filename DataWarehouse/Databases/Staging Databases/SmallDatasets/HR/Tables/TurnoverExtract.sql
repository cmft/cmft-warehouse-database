﻿CREATE TABLE [HR].[TurnoverExtract](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[Headcount] [float] NULL,
	[FTE] [float] NULL,
	[Starters Headcount] [float] NULL,
	[Starters FTE] [float] NULL,
	[Leavers Headcount] [float] NULL,
	[Leavers FTE] [float] NULL,
	[LTR Headcount %] [float] NULL,
	[LTR FTE %] [float] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]
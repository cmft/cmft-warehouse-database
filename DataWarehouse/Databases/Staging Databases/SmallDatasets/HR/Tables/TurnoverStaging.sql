﻿CREATE TABLE [HR].[TurnoverStaging](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL CONSTRAINT [DF__TurnoverS__Creat__5165187F]  DEFAULT (getdate()),
	[Headcount Monthly] [float] NULL,
	[FTE Monthly] [float] NULL,
	[Starters Headcount Monthly] [float] NULL,
	[Starters FTE Monthly] [float] NULL,
	[Leavers Headcount Monthly] [float] NULL,
	[Leavers FTE Monthly] [float] NULL,
	[LTR Headcount % Monthly] [float] NULL,
	[LTR FTE % Monthly] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [HR].[CorporateMandatoryTrainingExtract](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[NUMBER OF STAFF THAT DO NOT MEET THE REQUIREMENT] [float] NULL,
	[NUMBER OF STAFF THAT MEET REQUIREMENT] [float] NULL,
	[NUMBER OF STAFF WHO REQUIRE CORPORATE MANDATORY] [float] NULL,
	[%  Compliant] [float] NULL,
	[% Non Compliant] [float] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL
) ON [PRIMARY]
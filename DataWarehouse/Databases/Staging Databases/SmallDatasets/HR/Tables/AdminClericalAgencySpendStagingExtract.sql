﻿CREATE TABLE [HR].[AdminClericalAgencySpendStagingExtract](
	[Census Date] [datetime] NULL,
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[subjective code] [int] NULL,
	[Current Month Actual SUM] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [HR].[RetentionExtract](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[recent] [float] NULL,
	[long term] [float] NULL,
	[Grand Total] [float] NULL,
	[Stability %] [float] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL
) ON [PRIMARY]
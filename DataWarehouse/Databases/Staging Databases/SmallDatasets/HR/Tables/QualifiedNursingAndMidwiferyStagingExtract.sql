﻿CREATE TABLE [HR].[QualifiedNursingAndMidwiferyStagingExtract](
	[Census Date] [datetime] NULL,
	[Division] [nvarchar](255) NULL,
	[ESRSIP] [float] NULL,
	[GeneralLedgerEst] [float] NULL,
	[ESRSIPBand5] [float] NULL,
	[GeneralLedgerEstBand5] [float] NULL
) ON [PRIMARY]
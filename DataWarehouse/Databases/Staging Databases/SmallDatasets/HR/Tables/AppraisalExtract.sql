﻿CREATE TABLE [HR].[AppraisalExtract](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[NUMBER OF STAFF THAT DO NOT MEET REQUIREMENT] [float] NULL,
	[NUMBER OF STAFF THAT MEET REQUIREMENT] [float] NULL,
	[NUMBER OF STAFF WHO REQUIRE APPRAISAL] [float] NULL,
	[% STAFF COMPLIANT] [float] NULL,
	[% STAFF NON-COMPLIANT] [float] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]
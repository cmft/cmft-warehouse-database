﻿CREATE TABLE [HR].[BMERetentionStaging](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[BMERecent] [float] NULL,
	[BMELongTerm] [float] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]
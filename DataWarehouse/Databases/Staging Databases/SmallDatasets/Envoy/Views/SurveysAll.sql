﻿


CREATE view [Envoy].[SurveysAll]
as


SELECT       '1' + convert(varchar(30),[Discharge ID]) 'ResponseID', [Discharge ID] , [Discharge Date], Location, [Contact Initiated], [Last Rating], [Last Response Date], Status, 
		[Survey Date 1] [Survey Date], [Survey Question 1] [Survey Question], [Response 1] Response, [Response Date 1] [Response Date], 'R1' rNo
FROM            Envoy.baseSurveys
where [Survey Date 1] is not null and [Response Date 1] is not null

union  

SELECT        '2' + convert(varchar(30),[Discharge ID]), [Discharge ID], [Discharge Date], Location, [Contact Initiated], [Last Rating], [Last Response Date], Status, 
		[Survey Date 2], [Survey Question 2], [Response 2], [Response Date 2], 'R2' rNo
FROM            Envoy.baseSurveys
where [Survey Date 2] is not null and [Response Date 2] is not null

union
 
SELECT        '3'  + convert(varchar(30),[Discharge ID]),[Discharge ID], [Discharge Date], Location, [Contact Initiated], [Last Rating], [Last Response Date], Status, 
		[Survey Date 3], [Survey Question 3], [Response 3], [Response Date 3] , 'R3' rNo
FROM            Envoy.baseSurveys
where [Survey Date 3] is not null and [Response Date 3] is not null




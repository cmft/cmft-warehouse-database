﻿CREATE TABLE [Envoy].[SurveyAnswers](
	[questionIdentifier] [varchar](8000) NULL,
	[answerIdentifier] [varchar](8000) NULL,
	[response] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
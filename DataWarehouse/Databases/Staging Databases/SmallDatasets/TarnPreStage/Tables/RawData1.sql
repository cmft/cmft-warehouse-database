﻿CREATE TABLE [TarnPreStage].[RawData1](
	[SubmissionID] [bigint] NOT NULL,
	[SubmissionSectionID] [int] NOT NULL,
	[QuestionID] [nvarchar](255) NULL,
	[AnswerText] [nvarchar](255) NULL,
	[QuestionnaireSectionID] [int] NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[loc] [nvarchar](255) NULL
) ON [PRIMARY]
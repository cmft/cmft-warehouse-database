﻿CREATE TABLE [HRError].[WTEStaging](
	[Census Date] [date] NULL,
	[Cost Centre Desc] [nvarchar](255) NULL,
	[Cost Centre] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Subjective Code Desc] [nvarchar](255) NULL,
	[Wte Budget] [float] NULL,
	[Wte Contracted] [float] NULL,
	[WTE Variance (Under)/Over] [float] NULL,
	[Staff Group] [nvarchar](50) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[Created] [datetime] NULL
) ON [PRIMARY]
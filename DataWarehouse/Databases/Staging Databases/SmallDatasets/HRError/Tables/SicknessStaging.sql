﻿CREATE TABLE [HRError].[SicknessStaging](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [ntext] NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[Abs (FTE)] [float] NULL,
	[Avail (FTE)] [float] NULL,
	[% Abs Rate (FTE)] [float] NULL,
	[% Abs Rate (FTE)1] [float] NULL,
	[% Abs Rate (FTE)2] [float] NULL,
	[Sickness Occasions] [float] NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[CensusDate] [datetime] NULL,
	[Created] [datetime] NULL CONSTRAINT [DF__OLE DB De__Creat__74AE54BC]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
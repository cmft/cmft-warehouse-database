﻿CREATE TABLE [HRError].[BMERetentionStaging](
	[Organisation] [nvarchar](255) NULL,
	[Organisation Cost Centre] [float] NULL,
	[Division] [nvarchar](255) NULL,
	[Directorate] [nvarchar](255) NULL,
	[Area of Work] [nvarchar](255) NULL,
	[BMERecent] [float] NULL,
	[BMELongTerm] [float] NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[CensusDate] [date] NULL,
	[Created] [datetime] NULL
) ON [PRIMARY]
﻿create View [Governance].[RiskRatingByDivision]

As

Select
	ID = row_number() over (order by [Month],Division)
	,Census
	,[Month]
	,Division
	,RiskRating
from
	(
	select
		Census = [Census Date]
		,[Month]
		,Division = 'Surgical'
		,RiskRating = Surgery
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Medicine & Community'
		,RiskRating = [Medicine & Community]
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Clinical & Scientific'
		,RiskRating = CSS
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Specialist Medicine'
		,RiskRating = [Specialist Medical Services]
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Ophthalmology'
		,RiskRating = REH
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Dental'
		,RiskRating = UDH
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Saint Mary''s'
		,RiskRating = SMH
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Childrens'
		,RiskRating = RMCH
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Trafford'
		,RiskRating = Trafford
	from
		Governance.RiskRating	

	Union	

	select
		Census = [Census Date]
		,[Month]
		,Division = 'Trust'
		,RiskRating = [Trust Overall]
	from
		Governance.RiskRating	
	)Risk
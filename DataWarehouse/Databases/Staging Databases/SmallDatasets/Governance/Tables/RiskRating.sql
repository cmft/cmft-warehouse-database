﻿CREATE TABLE [Governance].[RiskRating](
	[Census date] [datetime] NULL,
	[Month] [datetime] NULL,
	[Surgery] [varchar](2) NULL,
	[Medicine & Community] [varchar](2) NULL,
	[CSS] [varchar](2) NULL,
	[Specialist Medical Services] [varchar](2) NULL,
	[REH] [varchar](2) NULL,
	[UDH] [varchar](2) NULL,
	[SMH] [varchar](2) NULL,
	[RMCH] [varchar](2) NULL,
	[Trafford] [varchar](2) NULL,
	[Trust Overall] [varchar](2) NULL
) ON [PRIMARY]
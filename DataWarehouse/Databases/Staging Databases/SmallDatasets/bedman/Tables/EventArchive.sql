﻿CREATE TABLE [bedman].[EventArchive](
	[Bid] [int] IDENTITY(1,1) NOT NULL,
	[ID] [int] NOT NULL,
	[HospSpellID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[EventTypeID] [int] NOT NULL,
	[LastUpdated_TS] [datetime] NOT NULL,
	[LastUpdatedBy] [int] NOT NULL,
	[Details] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
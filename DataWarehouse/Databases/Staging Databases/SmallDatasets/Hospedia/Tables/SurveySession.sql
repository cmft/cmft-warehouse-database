﻿CREATE TABLE [Hospedia].[SurveySession](
	[SessionID] [nvarchar](255) NULL,
	[wardIdentifier] [varchar](8000) NULL,
	[surveyIdentifier] [varchar](8000) NULL,
	[SurveyDate] [datetime] NULL
) ON [PRIMARY]
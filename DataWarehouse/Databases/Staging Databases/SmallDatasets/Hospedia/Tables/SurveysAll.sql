﻿CREATE TABLE [Hospedia].[SurveysAll](
	[surveyIdentifier] [varchar](8000) NULL,
	[questionIdentifier] [bigint] NULL,
	[answerIdentifier] [bigint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Hospedia].[HospitalWards](
	[WardIdentifier] [varchar](20) NULL,
	[WardName] [varchar](50) NULL,
	[HospitalIdentifier] [int] NULL,
	[HospitalName] [varchar](80) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Hospedia].[HospitalSurveyBase](
	[timePeriod] [varchar](max) NULL,
	[recordDate] [varchar](max) NULL,
	[siteId] [varchar](max) NULL,
	[sitePrefix] [varchar](max) NULL,
	[hospitalName] [varchar](max) NULL,
	[wardID] [varchar](max) NULL,
	[wardName] [varchar](max) NULL,
	[wardTypeName] [varchar](max) NULL,
	[surveyName] [varchar](max) NULL,
	[questionNumber] [varchar](max) NULL,
	[question] [varchar](max) NULL,
	[answerNumber] [varchar](max) NULL,
	[answer] [varchar](max) NULL,
	[personCount] [varchar](max) NULL,
	[responseCount] [varchar](max) NULL,
	[SurveyIdentifier] [varchar](max) NULL,
	[QuestionIdentifier] [varchar](max) NULL,
	[OptionIdentifier] [varchar](max) NULL,
	[DateImported] [datetime2](7) NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
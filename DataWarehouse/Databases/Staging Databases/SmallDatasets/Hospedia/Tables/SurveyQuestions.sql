﻿CREATE TABLE [Hospedia].[SurveyQuestions](
	[SessionID] [nvarchar](255) NULL,
	[questionIdentifier] [bigint] NULL,
	[QuestionNo] [int] NULL,
	[Question] [varchar](512) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Hospedia].[surveyanswers](
	[questionIdentifier] [bigint] NULL,
	[answerIdentifier] [bigint] NULL,
	[optionno] [varchar](350) NULL,
	[answer] [varchar](512) NULL
) ON [PRIMARY]
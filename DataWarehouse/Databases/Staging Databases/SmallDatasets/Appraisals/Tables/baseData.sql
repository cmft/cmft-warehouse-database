﻿CREATE TABLE [Appraisals].[baseData](
	[Status] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[GMC Number] [varchar](50) NULL,
	[Department] [varchar](50) NULL,
	[Specialty] [varchar](50) NULL,
	[Next Appraisal Date] [varchar](50) NULL,
	[Last Appraisal Meeting Date] [varchar](50) NULL,
	[Next Appraisal Scheduled Date] [varchar](50) NULL,
	[Last Appraisal Date] [varchar](50) NULL,
	[Last Revalidation Date] [varchar](50) NULL,
	[importDate] [datetime2](7) NULL DEFAULT (getdate()),
	[filename] [varchar](50) NULL
) ON [PRIMARY]
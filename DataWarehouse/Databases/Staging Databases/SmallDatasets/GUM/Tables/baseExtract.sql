﻿CREATE TABLE [GUM].[baseExtract](
	[Appointment Date] [datetime] NULL,
	[PatientNumber] [nvarchar](255) NULL,
	[PostCode] [nvarchar](255) NULL,
	[Clinic] [nvarchar](255) NULL,
	[Appointment Type] [nvarchar](255) NULL,
	[Attended or Did Not Attend] [nvarchar](255) NULL,
	[importDate] [datetime2](7) NULL,
	[filename] [varchar](30) NULL
) ON [PRIMARY]
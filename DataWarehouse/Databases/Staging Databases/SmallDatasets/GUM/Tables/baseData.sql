﻿CREATE TABLE [GUM].[baseData](
	[Appointment Date] [datetime] NULL,
	[PatientNumber] [nvarchar](255) NULL,
	[PostCode] [nvarchar](255) NULL,
	[Clinic] [nvarchar](255) NULL,
	[Appointment Type] [nvarchar](255) NULL,
	[Attended or Did Not Attend] [nvarchar](255) NULL,
	[importDate] [datetime2](7) NULL,
	[filename] [varchar](30) NULL,
	[created] [datetime2](7) NULL,
	[updated] [datetime2](7) NULL,
	[byWhom] [varchar](50) NULL
) ON [PRIMARY]
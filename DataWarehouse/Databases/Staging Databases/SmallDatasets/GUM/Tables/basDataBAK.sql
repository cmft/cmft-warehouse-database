﻿CREATE TABLE [GUM].[basDataBAK](
	[Appointment Date] [datetime] NULL,
	[PatientNumber] [nvarchar](255) NULL,
	[PostCode] [nvarchar](255) NULL,
	[Clinic] [nvarchar](255) NULL,
	[Appointment Type] [nvarchar](255) NULL,
	[Attended or Did Not Attend] [nvarchar](255) NULL
) ON [PRIMARY]
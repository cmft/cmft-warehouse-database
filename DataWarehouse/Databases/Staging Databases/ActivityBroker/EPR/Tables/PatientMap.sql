﻿CREATE TABLE [EPR].[PatientMap](
	[PatientMapID] [bigint] IDENTITY(1,1) NOT NULL,
	[ContextCode] [varchar](10) NULL,
	[SourcePatientNo] [int] NULL,
	[SourceDistrictNo] [int] NULL,
	[XrefContextCode] [varchar](10) NULL,
	[XrefSourcePatientNo] [int] NULL,
	[XrefDistrictNo] [int] NULL
) ON [PRIMARY]
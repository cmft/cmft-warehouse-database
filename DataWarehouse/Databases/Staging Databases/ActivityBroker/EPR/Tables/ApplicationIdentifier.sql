﻿CREATE TABLE [EPR].[ApplicationIdentifier](
	[SourceApplicationIdentifierID] [int] NULL,
	[ContextCode] [nchar](10) NULL,
	[SourceApplicationContextID] [int] NULL
) ON [PRIMARY]
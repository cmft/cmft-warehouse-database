﻿CREATE TABLE [PAS].[Patient](
	[SourcePatientNo] [varchar](9) NOT NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[DistrictNumber] [varchar](14) NULL,
	[Surname] [varchar](24) NULL,
	[SurnameSoundex] [varchar](5) NULL,
	[Forename] [varchar](20) NULL,
	[ForenameSoundex] [varchar](5) NULL,
	[DateOfBirthInteger] [varchar](8) NULL,
	[NHSNumber] [varchar](10) NULL,
	[Postcode] [varchar](10) NULL
) ON [PRIMARY]
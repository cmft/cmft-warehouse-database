﻿CREATE TABLE [AB].[ContactReason](
	[ContactReasonCode] [varchar](20) NULL,
	[ContactReason] [varchar](100) NULL,
	[ContactType] [varchar](20) NULL
) ON [PRIMARY]
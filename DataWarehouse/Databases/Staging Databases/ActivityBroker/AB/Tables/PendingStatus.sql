﻿CREATE TABLE [AB].[PendingStatus](
	[PendingStatusId] [int] IDENTITY(1,1) NOT NULL,
	[PendingStatusGroupId] [int] NOT NULL,
	[PendingStatusCode] [nvarchar](100) NOT NULL,
	[PendingStatus] [nvarchar](500) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [nchar](10) NOT NULL,
	[CreatedBy] [nchar](10) NOT NULL,
	[UpdatedBy] [nchar](10) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [AB].[UserAction](
	[UserActionID] [int] IDENTITY(1,1) NOT NULL,
	[UserActionCode] [varchar](50) NULL,
	[UserAction] [varchar](100) NULL,
	[BrokerDescription] [varchar](50) NULL,
	[ActionTargetID] [int] NULL,
	[BrokerLegend] [varchar](100) NULL,
	[ActionGroupID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [AB].[PathwayStatus](
	[PathwayStatusID] [tinyint] IDENTITY(1,1) NOT NULL,
	[PathwayStatusCode] [varchar](50) NULL,
	[PathwayStatus] [varchar](50) NULL
) ON [PRIMARY]
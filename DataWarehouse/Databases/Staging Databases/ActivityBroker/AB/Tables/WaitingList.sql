﻿CREATE TABLE [AB].[WaitingList](
	[WaitingListID] [int] IDENTITY(1,1) NOT NULL,
	[WaitingListCode] [varchar](10) NOT NULL,
	[OriginEPRApplicationContextID] [int] NOT NULL,
	[PathwayTypeID] [int] NULL,
	[ScheduleAtOrigin] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [AB].[AdmissionSource](
	[AdmissionSourceID] [int] IDENTITY(1,1) NOT NULL,
	[AdmissionSourceCode] [varchar](10) NOT NULL,
	[DestinationEPRApplicationContextID] [int] NOT NULL,
	[PathwayTypeID] [int] NULL
) ON [PRIMARY]
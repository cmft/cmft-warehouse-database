﻿CREATE TABLE [AB].[PathwayType](
	[PathwayTypeID] [int] IDENTITY(1,1) NOT NULL,
	[PathwayType] [varchar](20) NULL
) ON [PRIMARY]
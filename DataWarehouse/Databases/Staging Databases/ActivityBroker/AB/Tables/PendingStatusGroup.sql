﻿CREATE TABLE [AB].[PendingStatusGroup](
	[PendingStatusGroupId] [int] IDENTITY(1,1) NOT NULL,
	[PendingStatusGroupCode] [nvarchar](100) NOT NULL,
	[PendingStatusGroup] [nvarchar](500) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [nchar](10) NOT NULL,
	[CreatedBy] [nchar](10) NOT NULL,
	[UpdatedBy] [nchar](10) NOT NULL
) ON [PRIMARY]
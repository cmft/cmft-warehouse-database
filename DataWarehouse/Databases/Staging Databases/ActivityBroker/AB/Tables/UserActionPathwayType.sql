﻿CREATE TABLE [AB].[UserActionPathwayType](
	[UserActionPathwayTypeID] [int] IDENTITY(1,1) NOT NULL,
	[UserActionID] [int] NULL,
	[PathwayTypeID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [AB].[DischargeDestination](
	[DischargeDestinationID] [int] IDENTITY(1,1) NOT NULL,
	[DischargeDestinationCode] [varchar](10) NOT NULL,
	[OriginEPRApplicationContextID] [int] NOT NULL,
	[PathwayTypeID] [int] NULL
) ON [PRIMARY]
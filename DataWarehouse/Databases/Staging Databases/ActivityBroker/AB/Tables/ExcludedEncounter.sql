﻿CREATE TABLE [AB].[ExcludedEncounter](
	[EncounterTypeCode] [varchar](16) NOT NULL,
	[SourceUniqueID] [varchar](254) NOT NULL,
	[SourcePatientNo] [int] NULL,
	[SourceEntityRecno] [int] NULL,
	[EPRApplicationContextID] [int] NULL,
	[ExcludedTime] [datetime] NOT NULL
) ON [PRIMARY]
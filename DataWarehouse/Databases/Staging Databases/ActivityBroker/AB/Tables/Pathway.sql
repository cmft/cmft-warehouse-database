﻿CREATE TABLE [AB].[Pathway](
	[PathwayID] [int] IDENTITY(1,1) NOT NULL,
	[PatientIsMatched] [bit] NULL,
	[PathwayTypeID] [int] NOT NULL,
	[OriginAPCWaitID] [int] NULL,
	[OriginAPCSpellID] [int] NULL,
	[OriginOPWaitID] [int] NULL,
	[DestinationAPCWaitID] [int] NULL,
	[DestinationAPCSpellID] [int] NULL,
	[DestinationOPWaitID] [int] NULL
) ON [PRIMARY]
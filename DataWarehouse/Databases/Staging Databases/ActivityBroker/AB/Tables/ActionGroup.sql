﻿CREATE TABLE [AB].[ActionGroup](
	[ActionGroupID] [int] IDENTITY(1,1) NOT NULL,
	[ActionGroupCode] [varchar](20) NULL,
	[ActionGroup] [varchar](100) NULL
) ON [PRIMARY]
﻿CREATE TABLE [ETL].[TImportPASOPWLPatient](
	[SourceUniqueID] [varchar](9) NOT NULL,
	[SourcePatientNo] [varchar](9) NOT NULL,
	[DistrictNo] [varchar](14) NULL,
	[Surname] [varchar](24) NULL,
	[Forename] [varchar](20) NULL,
	[DateOfBirth] [date] NULL,
	[SexCode] [varchar](1) NULL,
	[NHSNumber] [varchar](20) NULL
) ON [PRIMARY]
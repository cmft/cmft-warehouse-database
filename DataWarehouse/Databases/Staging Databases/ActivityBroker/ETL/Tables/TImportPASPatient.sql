﻿CREATE TABLE [ETL].[TImportPASPatient](
	[PATDATAID] [varchar](9) NOT NULL,
	[DistrictNumber] [varchar](14) NULL,
	[Forenames] [varchar](20) NULL,
	[InternalDateOfBirth] [varchar](8) NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[NHSNumber] [varchar](17) NULL,
	[PtAddrPostCode] [varchar](13) NULL,
	[PtDoB] [varchar](10) NULL,
	[Surname] [varchar](24) NULL
) ON [PRIMARY]
﻿CREATE TABLE [ETL].[TImportPASAPCWLCancel](
	[SourceUniqueID] [varchar](254) NOT NULL,
	[ContextCode] [varchar](8) NOT NULL,
	[SourcePatientNo] [varchar](9) NOT NULL,
	[SourceEntityRecno] [varchar](9) NOT NULL,
	[WLActivityTimeInt] [varchar](12) NOT NULL,
	[ActivityCode] [varchar](2) NULL,
	[Activity] [varchar](99) NULL,
	[CancelledByCode] [varchar](3) NULL,
	[CancellationReason] [varchar](30) NULL
) ON [PRIMARY]
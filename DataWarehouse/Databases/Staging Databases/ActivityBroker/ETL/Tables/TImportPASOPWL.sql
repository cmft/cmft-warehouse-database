﻿CREATE TABLE [ETL].[TImportPASOPWL](
	[SourceUniqueID] [varchar](254) NOT NULL,
	[ContextCode] [varchar](8) NOT NULL,
	[SourcePatientNo] [varchar](9) NOT NULL,
	[SourceEntityRecno] [varchar](9) NOT NULL,
	[DistrictNo] [varchar](14) NULL,
	[Surname] [varchar](24) NULL,
	[Forename] [varchar](20) NULL,
	[DateOfBirth] [date] NULL,
	[SexCode] [varchar](1) NULL,
	[WaitingListCode] [varchar](8) NOT NULL,
	[AppointmentTypeCode] [varchar](3) NULL,
	[DateOnWaitingList] [date] NULL,
	[OPRegistrationDate] [date] NULL,
	[AppointmentRequiredBy] [int] NULL,
	[Comment] [varchar](55) NULL
) ON [PRIMARY]
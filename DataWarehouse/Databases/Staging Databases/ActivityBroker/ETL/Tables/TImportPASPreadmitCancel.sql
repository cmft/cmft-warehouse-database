﻿CREATE TABLE [ETL].[TImportPASPreadmitCancel](
	[SourceUniqueID] [varchar](254) NOT NULL,
	[ContextCode] [varchar](8) NOT NULL,
	[SourcePatientNo] [varchar](9) NOT NULL,
	[SourceEntityRecno] [varchar](9) NOT NULL,
	[WLActivityTimeInt] [varchar](12) NOT NULL,
	[ActivityCode] [varchar](2) NULL,
	[Activity] [varchar](99) NULL,
	[PreviousActivityTimeInt] [varchar](12) NULL,
	[PreviousActivityCode] [varchar](2) NULL,
	[PreviousActivity] [varchar](20) NULL,
	[CancelledByCode] [varchar](3) NULL,
	[CancellationReason] [varchar](30) NULL,
	[PreviousActivityTCIDate] [date] NULL,
	[PreviousActivityTCITime] [smalldatetime] NULL
) ON [PRIMARY]
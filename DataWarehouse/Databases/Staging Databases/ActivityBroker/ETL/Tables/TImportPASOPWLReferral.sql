﻿CREATE TABLE [ETL].[TImportPASOPWLReferral](
	[SourceUniqueID] [varchar](254) NOT NULL,
	[SourcePatientNo] [varchar](9) NOT NULL,
	[SourceEntityRecno] [varchar](9) NOT NULL,
	[SpecialtyCode] [varchar](4) NULL,
	[ConsultantCode] [varchar](6) NULL,
	[ReferralDate] [date] NULL,
	[SiteCode] [varchar](4) NULL,
	[SourceOfReferralCode] [varchar](10) NULL
) ON [PRIMARY]
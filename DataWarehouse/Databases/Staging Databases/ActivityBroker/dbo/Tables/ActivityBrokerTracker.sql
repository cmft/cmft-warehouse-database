﻿CREATE TABLE [dbo].[ActivityBrokerTracker](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PathwayID] [int] NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[ActivityType] [varchar](50) NULL,
	[PathwayAction] [varchar](50) NULL,
	[UserName] [varchar](50) NULL,
	[Status] [varchar](20) NOT NULL,
	[CreatedTimestamp] [datetime] NOT NULL,
	[ProcessedTimestamp] [datetime] NULL
) ON [PRIMARY]
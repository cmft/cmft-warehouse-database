﻿CREATE TABLE [dbo].[ActivityBrokerTrackerErrors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](20) NULL,
	[Source] [varchar](200) NULL,
	[Description] [varchar](max) NULL,
	[StackTrace] [varchar](max) NULL,
	[UserName] [varchar](100) NULL,
	[Timestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
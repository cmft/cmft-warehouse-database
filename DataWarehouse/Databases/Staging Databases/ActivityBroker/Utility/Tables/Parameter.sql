﻿CREATE TABLE [Utility].[Parameter](
	[Parameter] [varchar](128) NOT NULL,
	[TextValue] [varchar](255) NULL,
	[NumericValue] [decimal](18, 5) NULL,
	[DateValue] [datetime] NULL,
	[BooleanValue] [bit] NULL
) ON [PRIMARY]
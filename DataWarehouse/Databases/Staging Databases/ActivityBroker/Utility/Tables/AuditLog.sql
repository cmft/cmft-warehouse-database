﻿CREATE TABLE [Utility].[AuditLog](
	[AuditLogRecno] [int] IDENTITY(1,1) NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[UserId] [varchar](30) NOT NULL,
	[ProcessCode] [varchar](255) NOT NULL,
	[Event] [varchar](255) NOT NULL,
	[StartTime] [datetime] NULL
) ON [PRIMARY]
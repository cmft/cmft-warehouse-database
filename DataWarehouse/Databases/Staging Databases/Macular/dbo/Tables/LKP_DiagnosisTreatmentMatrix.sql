﻿CREATE TABLE [dbo].[LKP_DiagnosisTreatmentMatrix](
	[IntersectionID] [int] IDENTITY(1,1) NOT NULL,
	[DiagnosisID] [int] NULL,
	[TreatmentID] [int] NULL,
	[ExpectedDuration] [nvarchar](255) NULL,
	[ExpectedAnnualCost] [nvarchar](255) NULL,
	[ExitCriteria] [nvarchar](255) NULL
) ON [PRIMARY]
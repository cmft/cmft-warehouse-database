﻿CREATE TABLE [dbo].[DAT_Notification_AdminComment](
	[AdminCommentID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationID] [int] NULL,
	[AdminCommentDateTime] [datetime] NULL CONSTRAINT [DF_DAT_Notification_AdminComment_AdminCommentDateTime]  DEFAULT (getdate()),
	[AdminComment] [ntext] NULL,
	[AdminCommentBy] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
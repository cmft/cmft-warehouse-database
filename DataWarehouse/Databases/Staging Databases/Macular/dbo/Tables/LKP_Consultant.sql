﻿CREATE TABLE [dbo].[LKP_Consultant](
	[ConsultantID] [nvarchar](50) NOT NULL,
	[ConsultantTitle] [nchar](5) NULL,
	[ConsultantInitials] [nchar](5) NULL,
	[ConsultantSurname] [nchar](50) NULL,
	[Email] [nvarchar](255) NULL,
	[ConsultantCode] [nvarchar](20) NULL,
	[HasMTCSessions] [int] NULL,
	[HasOwnpatients] [int] NULL,
	[MedisoftName] [nvarchar](255) NULL
) ON [PRIMARY]
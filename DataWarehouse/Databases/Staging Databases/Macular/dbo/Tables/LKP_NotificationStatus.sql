﻿CREATE TABLE [dbo].[LKP_NotificationStatus](
	[NotificationStatusID] [int] NOT NULL,
	[NotificationStatusCaption] [nvarchar](255) NULL,
	[NICECompliantFlag] [bit] NULL,
	[CommissionerPreApprovedFlag] [bit] NULL,
	[AllowIFRFlag] [bit] NULL,
	[IFRCompleteFlag] [bit] NULL
) ON [PRIMARY]
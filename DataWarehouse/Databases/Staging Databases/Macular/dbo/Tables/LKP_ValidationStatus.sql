﻿CREATE TABLE [dbo].[LKP_ValidationStatus](
	[ValidationStatusID] [int] IDENTITY(1,1) NOT NULL,
	[ValidationStatusCaption] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SYS_ReportUserMatrix](
	[PermissionID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NOT NULL,
	[ADLogin] [nvarchar](100) NOT NULL
) ON [PRIMARY]
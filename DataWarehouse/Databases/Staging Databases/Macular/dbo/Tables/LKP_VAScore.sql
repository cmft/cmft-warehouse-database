﻿CREATE TABLE [dbo].[LKP_VAScore](
	[VASCoreID] [int] NOT NULL,
	[VAScore] [nchar](11) NULL,
	[WithinNICEApprovedRange] [bit] NULL,
	[Ordering] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SYS_Computer](
	[ComputerID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](25) NULL,
	[PCName] [nvarchar](50) NULL,
	[GeographicalLocation] [nvarchar](50) NULL
) ON [PRIMARY]
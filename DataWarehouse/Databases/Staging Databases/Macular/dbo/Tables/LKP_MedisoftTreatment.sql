﻿CREATE TABLE [dbo].[LKP_MedisoftTreatment](
	[MedisoftDrugCode] [nvarchar](10) NOT NULL,
	[HRGCode] [nvarchar](55) NULL,
	[TreatmentID] [int] NULL
) ON [PRIMARY]
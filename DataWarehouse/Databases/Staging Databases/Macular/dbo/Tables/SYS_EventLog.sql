﻿CREATE TABLE [dbo].[SYS_EventLog](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[ADLogin] [nvarchar](100) NOT NULL,
	[EventName] [nvarchar](50) NOT NULL,
	[EventDateTime] [datetime] NOT NULL,
	[EventArguments] [nvarchar](255) NULL,
	[PCName] [nvarchar](50) NULL,
	[MINARVersion] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LKP_Diagnosis](
	[DiagnosisID] [int] IDENTITY(1,1) NOT NULL,
	[DiagnosisCaption] [nvarchar](255) NULL,
	[SortOrder] [int] NULL
) ON [PRIMARY]
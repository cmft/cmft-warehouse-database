﻿CREATE TABLE [dbo].[SYS_Default](
	[ADLogin] [nvarchar](255) NOT NULL,
	[DefaultForm] [nvarchar](255) NULL,
	[FriendlyFormName] [nvarchar](255) NULL,
	[TagPermissions] [nvarchar](255) NULL,
	[IsGeneric] [bit] NULL
) ON [PRIMARY]
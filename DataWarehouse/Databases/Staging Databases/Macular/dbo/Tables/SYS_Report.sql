﻿CREATE TABLE [dbo].[SYS_Report](
	[ReportID] [int] IDENTITY(1,1) NOT NULL,
	[ReportName] [nvarchar](60) NULL,
	[ReportNotes] [nvarchar](255) NULL,
	[ReportColumns] [int] NULL,
	[ShowDateFrom] [bit] NULL,
	[ShowDateTo] [bit] NULL,
	[OrderByClause] [nvarchar](255) NULL
) ON [PRIMARY]
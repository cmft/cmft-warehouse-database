﻿CREATE TABLE [dbo].[DAT_HistoricalNotification](
	[NotificationID] [int] IDENTITY(1000000,1) NOT NULL,
	[PatientForename] [nvarchar](255) NULL,
	[PatientSurname] [nvarchar](255) NULL,
	[PatientDOB] [datetime] NULL,
	[NHSNumber] [nvarchar](255) NULL,
	[ApplicationDate] [datetime] NULL,
	[DiagnosisID] [int] NULL,
	[LateralityID] [int] NULL,
	[TreatmentID] [int] NULL,
	[CasenoteNumber] [nvarchar](255) NULL,
	[CompletedBy] [nvarchar](50) NULL
) ON [PRIMARY]
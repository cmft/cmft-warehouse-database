﻿CREATE TABLE [dbo].[LKP_ReasonForTreatment](
	[ReasonForTreamentID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonForTreatment] [nvarchar](255) NULL
) ON [PRIMARY]
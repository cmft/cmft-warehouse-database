﻿CREATE TABLE [dbo].[LKP_Treatment](
	[TreatmentID] [int] NOT NULL,
	[TreatmentCaption] [nvarchar](100) NULL,
	[DefinitiveDrugFlag] [bit] NULL
) ON [PRIMARY]
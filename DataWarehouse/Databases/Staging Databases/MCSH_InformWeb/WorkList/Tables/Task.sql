﻿CREATE TABLE [WorkList].[Task](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[TaskGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_WorkList_Task_TaskGuid]  DEFAULT (newid()),
	[TaskDate] [datetime] NOT NULL,
	[Name] [varchar](max) NULL,
	[SourceColumnId] [int] NULL,
	[PatientId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[ListId] [int] NOT NULL,
	[StageId] [int] NULL,
	[Status] [varchar](100) NULL,
	[Notes] [varchar](255) NULL,
	[Flag] [varchar](3) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

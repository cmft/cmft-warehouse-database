﻿CREATE TABLE [WorkList].[Action](
	[ActionId] [int] IDENTITY(1,1) NOT NULL,
	[ActionGuid] [uniqueidentifier] NOT NULL,
	[ListId] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Interval] [varchar](10) NOT NULL,
	[OrderIndex] [int] NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [WorkList].[List](
	[ListId] [int] IDENTITY(1,1) NOT NULL,
	[ListGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_List_ListGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](10) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_WorkList_IsActive]  DEFAULT ((0)),
	[Gender] [tinyint] NOT NULL DEFAULT ((0)),
	[GroupId] [int] NULL
) ON [PRIMARY]

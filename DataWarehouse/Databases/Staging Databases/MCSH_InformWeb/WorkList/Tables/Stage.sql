﻿CREATE TABLE [WorkList].[Stage](
	[StageId] [int] IDENTITY(1,1) NOT NULL,
	[StageGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_WorkList_Stage_StageGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](6) NULL,
	[GroupId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_WorkList_Stage_IsActive]  DEFAULT ((1)),
	[IsTrigger] [bit] NOT NULL CONSTRAINT [DF_WorkList_Stage_IsTrigger]  DEFAULT ((0))
) ON [PRIMARY]

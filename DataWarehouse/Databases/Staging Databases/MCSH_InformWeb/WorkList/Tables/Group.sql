﻿CREATE TABLE [WorkList].[Group](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[GroupGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_WorkList_Group_GroupGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_WorkList_Group_IsActive]  DEFAULT ((0)),
	[ConfigXml] [varchar](1000) NULL
) ON [PRIMARY]

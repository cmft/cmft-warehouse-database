﻿CREATE TABLE [GeneralMedical].[NHSTrustSite](
	[NHSTrustSiteId] [int] NOT NULL,
	[OrganisationCode] [nvarchar](9) NULL,
	[OrganisationName] [nvarchar](100) NULL,
	[NGCode] [nvarchar](3) NULL,
	[HACode] [nvarchar](3) NULL,
	[AddressLine1] [varchar](35) NULL,
	[AddressLine2] [varchar](35) NULL,
	[AddressLine3] [varchar](35) NULL,
	[AddressLine4] [varchar](35) NULL,
	[AddressLine5] [varchar](35) NULL,
	[Postcode] [varchar](8) NULL,
	[OpenDate] [nvarchar](8) NULL,
	[CloseDate] [nvarchar](8) NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

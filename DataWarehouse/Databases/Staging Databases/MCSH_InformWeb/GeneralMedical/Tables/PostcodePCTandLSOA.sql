﻿CREATE TABLE [GeneralMedical].[PostcodePCTandLSOA](
	[PostcodePCTandLSOAId] [int] IDENTITY(1,1) NOT NULL,
	[Postcode] [varchar](8) NOT NULL,
	[LSOA] [varchar](9) NOT NULL,
	[PCT] [varchar](3) NOT NULL
) ON [PRIMARY]

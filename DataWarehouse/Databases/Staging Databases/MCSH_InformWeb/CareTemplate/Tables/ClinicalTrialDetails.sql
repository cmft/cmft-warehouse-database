﻿CREATE TABLE [CareTemplate].[ClinicalTrialDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[ClinicalTrialDetailsId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NOT NULL,
	[LastReviewedByUserRoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]
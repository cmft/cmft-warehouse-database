﻿CREATE TABLE [CareTemplate].[Attachment](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[AttachmentId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NULL,
	[LastReviewedByUserRoleId] [int] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]

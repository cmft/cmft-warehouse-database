﻿CREATE TABLE [CareTemplate].[Occupation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[OccupationId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NOT NULL,
	[LastReviewedByUserRoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL DEFAULT ((0)),
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]
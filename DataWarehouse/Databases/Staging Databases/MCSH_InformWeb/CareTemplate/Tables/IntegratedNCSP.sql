﻿CREATE TABLE [CareTemplate].[IntegratedNCSP](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[IntegratedNCSPId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NULL,
	[LastReviewedByUserRoleId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]

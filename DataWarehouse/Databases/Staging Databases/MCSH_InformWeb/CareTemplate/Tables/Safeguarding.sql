﻿CREATE TABLE [CareTemplate].[Safeguarding](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[SafeguardingId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NULL,
	[LastReviewedByUserRoleId] [int] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_Safeguarding_IsActive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL DEFAULT ((0)),
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]

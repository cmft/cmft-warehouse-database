﻿CREATE TABLE [CareTemplate].[RiskScore](
	[CareTemplateId] [int] NOT NULL,
	[RiskScoreId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NOT NULL,
	[LastReviewedByUserRoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[AssociatedCareContactId] [int] NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

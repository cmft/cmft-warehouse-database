﻿CREATE TABLE [CareTemplate].[TemplateDataItem](
	[TemplateDataItemId] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Value] [int] NULL,
	[TextValue] [varchar](max) NULL,
	[IsNumericValue] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_TemplateDataItem_IsNumericValue]  DEFAULT ((1)),
	[DateValue] [datetime] NULL,
	[IsDateValue] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_TemplateDataItem_IsDateValue]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

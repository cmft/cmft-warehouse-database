﻿CREATE TABLE [CareTemplate].[ClerkIssueofCondoms](
	[Id] [int] NOT NULL,
	[ClerkIssueofCondomsId] [int] NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NOT NULL,
	[LastReviewedByUserRoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

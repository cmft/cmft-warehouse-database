﻿CREATE TABLE [CareTemplate].[Diagnosis](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[DiagnosisId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NOT NULL,
	[LastReviewedByUserRoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_Diagnosis_IsActive]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_Diagnosis_IsDeleted]  DEFAULT ((0)),
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]

﻿CREATE TABLE [CareTemplate].[Prescription](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[PrescriptionId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NULL,
	[LastReviewedByUserRoleId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [Caretemplate_Prescription_IsDeleted]  DEFAULT ((0)),
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]

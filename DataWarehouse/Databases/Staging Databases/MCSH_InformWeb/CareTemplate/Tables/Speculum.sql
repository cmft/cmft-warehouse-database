﻿CREATE TABLE [CareTemplate].[Speculum](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[SpeculumId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NOT NULL,
	[LastReviewedByUserRoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]

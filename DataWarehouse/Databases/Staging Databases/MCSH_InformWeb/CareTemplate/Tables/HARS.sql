﻿CREATE TABLE [CareTemplate].[HARS](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[HARSId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NULL,
	[LastReviewedByUserRoleId] [int] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_HARS_IsActive]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_HARS_IsDeleted]  DEFAULT ((0)),
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]

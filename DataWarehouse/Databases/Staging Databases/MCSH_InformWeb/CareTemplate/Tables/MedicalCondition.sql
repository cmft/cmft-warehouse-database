﻿CREATE TABLE [CareTemplate].[MedicalCondition](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CareTemplateId] [int] NOT NULL,
	[MedicalConditionId] [int] NOT NULL,
	[LastReviewedDateTime] [datetime] NULL,
	[LastReviewedByUserRoleId] [int] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_MedicalCondition_IsActive]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_MedicalCondition_IsDeleted]  DEFAULT ((0)),
	[AssociatedCareContactId] [int] NULL
) ON [PRIMARY]

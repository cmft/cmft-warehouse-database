﻿CREATE TABLE [Reporting].[Report](
	[ReportId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Reporting_Report_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

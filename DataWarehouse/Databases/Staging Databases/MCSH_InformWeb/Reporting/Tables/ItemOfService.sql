﻿CREATE TABLE [Reporting].[ItemOfService](
	[ReportItemOfServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ReportId] [int] NOT NULL,
	[ItemOfServiceId] [int] NOT NULL
) ON [PRIMARY]

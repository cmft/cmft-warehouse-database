﻿CREATE TABLE [Reporting].[Query](
	[QueryId] [int] IDENTITY(1,1) NOT NULL,
	[ParentQueryId] [int] NULL,
	[Name] [varchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Reporting_Query_Guid]  DEFAULT (newid()),
	[Version] [float] NOT NULL CONSTRAINT [DF_Reporting_Query_Version]  DEFAULT ((1)),
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Reporting_Query_CreatedDateTime]  DEFAULT (getdate()),
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[IsFolder] [bit] NOT NULL CONSTRAINT [DF_Reporting_Query_IsFolder]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Reporting_Query_IsActive]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [Reporting].[Configuration](
	[ConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[ReportName] [varchar](100) NOT NULL,
	[StoredProcedureName] [nvarchar](100) NOT NULL,
	[ReportKey] [varchar](20) NULL,
	[PermissionKey] [varchar](10) NULL,
	[OrderIndex] [tinyint] NULL,
	[ReportMode] [char](1) NULL,
	[Query] [nvarchar](max) NULL,
	[FileFormat] [varchar](5) NULL,
	[Delimiter] [char](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

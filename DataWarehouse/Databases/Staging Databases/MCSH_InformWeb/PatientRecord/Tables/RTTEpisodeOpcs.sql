﻿CREATE TABLE [PatientRecord].[RTTEpisodeOpcs](
	[RTTEpisodeOpcsId] [int] IDENTITY(1,1) NOT NULL,
	[RTTEpisodeId] [int] NOT NULL,
	[RTTOpcsId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[IsPrimary] [bit] NOT NULL,
	[RTTEpisodeOpcsDate] [datetime] NOT NULL
) ON [PRIMARY]

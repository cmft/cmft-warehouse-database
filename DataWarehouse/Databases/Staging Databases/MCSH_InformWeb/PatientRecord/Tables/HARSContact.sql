﻿CREATE TABLE [PatientRecord].[HARSContact](
	[HARSContactId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[HARSContactDate] [datetime] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_HARSContact_Guid]  DEFAULT (newid()),
	[NumberOfContacts] [int] NOT NULL,
	[NumberContactable] [int] NULL,
	[NumberTestedForHIV] [int] NULL,
	[Notes] [varchar](max) NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_HARSContact_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatientRecord_HARSContact_Version]  DEFAULT ((1)),
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientRecord_HARSContact_CreatedDateTime]  DEFAULT (getdate()),
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_HARSContact_IsActive]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_HARSContact_IsDeleted]  DEFAULT ((0)),
	[RemovedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[CareContactId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

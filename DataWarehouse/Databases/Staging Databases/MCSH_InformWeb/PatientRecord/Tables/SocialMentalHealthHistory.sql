﻿CREATE TABLE [PatientRecord].[SocialMentalHealthHistory](
	[SocialMentalHealthHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[SocialMentalHealthHistoryDate] [datetime] NULL,
	[Notes] [varchar](max) NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsNotRequired] [bit] NOT NULL,
	[Version] [float] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

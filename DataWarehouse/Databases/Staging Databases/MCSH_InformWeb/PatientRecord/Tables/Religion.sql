﻿CREATE TABLE [PatientRecord].[Religion](
	[ReligionId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_Religion_Guid]  DEFAULT (newid()),
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[ReligionDate] [datetime] NOT NULL,
	[ReligionTypeId] [tinyint] NULL,
	[ReligionOtherText] [varchar](255) NULL,
	[Notes] [varchar](max) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Religion_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatientRecord_Religion_Version]  DEFAULT ((1)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Religion_IsActive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Religion_IsDeleted]  DEFAULT ((0)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[FamilyHistoryConditionDetailItem](
	[FamilyHistoryConditionDetailItemId] [int] IDENTITY(1,1) NOT NULL,
	[FamilyHistoryConditionDetailId] [int] NOT NULL,
	[FamilyHistoryConditionItemId] [int] NOT NULL
) ON [PRIMARY]

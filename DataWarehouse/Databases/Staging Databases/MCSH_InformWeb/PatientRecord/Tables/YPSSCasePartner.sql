﻿CREATE TABLE [PatientRecord].[YPSSCasePartner](
	[YPSSCasePartnerId] [int] IDENTITY(1,1) NOT NULL,
	[YPSSCaseId] [int] NOT NULL,
	[GivenName] [varchar](35) NULL,
	[FamilyName] [varchar](35) NULL,
	[Aliases] [varchar](255) NULL,
	[Postcode] [varchar](8) NULL,
	[BirthDate] [datetime] NULL,
	[Age] [tinyint] NULL,
	[EducationEmploymentTrainingDetails] [varchar](255) NULL,
	[EthnicityId] [smallint] NULL,
	[ParentalResponsibilityLookUpId] [int] NULL,
	[Notes] [varchar](255) NULL,
	[PatientId] [int] NULL
) ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[DrugInteractionContraception](
	[DrugInteractionContraceptionId] [int] IDENTITY(1,1) NOT NULL,
	[DrugInteractionId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Interaction] [bit] NOT NULL,
	[Suitability] [tinyint] NOT NULL,
	[Notes] [varchar](255) NULL,
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL
) ON [PRIMARY]

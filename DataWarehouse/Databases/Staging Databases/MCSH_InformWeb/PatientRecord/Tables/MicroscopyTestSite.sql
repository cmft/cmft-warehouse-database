﻿CREATE TABLE [PatientRecord].[MicroscopyTestSite](
	[MicroscopyTestSiteId] [int] IDENTITY(1,1) NOT NULL,
	[MicroscopyTestId] [int] NOT NULL,
	[SiteName] [varchar](20) NOT NULL,
	[Lactobacilli] [varchar](20) NULL,
	[PusCell] [varchar](20) NULL,
	[PusCellNotes] [varchar](255) NULL,
	[EpithelialCells] [varchar](20) NULL,
	[OtherOrganisms] [varchar](20) NULL,
	[OtherOrganismsNotes] [varchar](255) NULL,
	[ClueCells] [varchar](20) NULL,
	[ClueCellsNotes] [varchar](255) NULL,
	[Gnid] [varchar](20) NULL,
	[GnidNotes] [varchar](255) NULL,
	[OtherFindings] [varchar](255) NULL,
	[OtherSiteNotes] [varchar](255) NULL,
	[MotileBodies] [varchar](20) NULL,
	[Tv] [varchar](20) NULL,
	[Spirochaetes] [varchar](20) NULL,
	[GveIcdc] [varchar](20) NULL,
	[Candida] [varchar](255) NULL
) ON [PRIMARY]

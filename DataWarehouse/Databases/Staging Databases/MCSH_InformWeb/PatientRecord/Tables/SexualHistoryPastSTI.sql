﻿CREATE TABLE [PatientRecord].[SexualHistoryPastSTI](
	[SexualHistoryPastSTIId] [int] IDENTITY(1,1) NOT NULL,
	[SexualHistoryId] [int] NOT NULL,
	[SexualHistorySTIId] [int] NOT NULL
) ON [PRIMARY]

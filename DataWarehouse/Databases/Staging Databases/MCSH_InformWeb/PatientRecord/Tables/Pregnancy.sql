﻿CREATE TABLE [PatientRecord].[Pregnancy](
	[PregnancyId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_Pregnancy_Guid]  DEFAULT (newid()),
	[PregnancyDate] [datetime] NOT NULL,
	[PregnancyDateFormat] [tinyint] NOT NULL CONSTRAINT [DF_PatientRecord_Pregnancy_PregnancyDateFormat]  DEFAULT ((0)),
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[Term] [tinyint] NULL,
	[AliveWellStatus] [tinyint] NULL,
	[PregnancyDeliveryTypeId] [int] NULL,
	[PregnancyOutcomeId] [int] NULL,
	[WeeksPostpartum] [tinyint] NULL,
	[Notes] [varchar](max) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Pregnancy_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatienRecordt_Pregnancy_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Pregnancy_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Pregnancy_IsActive]  DEFAULT ((1)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[IsNotesOnly] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Pregnancy_IsNotesOnly]  DEFAULT ((0)),
	[OwnerCareTemplateId] [int] NULL,
	[VaginalDelivery] [tinyint] NULL,
	[Caesarean] [tinyint] NULL,
	[Miscarriage] [tinyint] NULL,
	[Ectopic] [tinyint] NULL,
	[Termination] [tinyint] NULL,
	[Breastfeeding] [bit] NULL,
	[FullyBreastfeeding] [bit] NULL,
	[BabyLessThan6Months] [bit] NULL,
	[Menstruating] [bit] NULL,
	[BreastfeedingNotes] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[SRHADActivity](
	[SRHADActivityId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_SRHADActivity_Guid]  DEFAULT (newid()),
	[SRHADActivityDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_SRHADActivity_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatientRecord_SRHADActivity_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_SRHADActivity_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_SRHADActivity_IsActive]  DEFAULT ((1)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY]

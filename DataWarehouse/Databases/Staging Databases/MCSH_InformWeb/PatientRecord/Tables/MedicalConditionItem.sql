﻿CREATE TABLE [PatientRecord].[MedicalConditionItem](
	[MedicalConditionItemId] [int] IDENTITY(1,1) NOT NULL,
	[MedicalConditionId] [int] NOT NULL,
	[DiagnosedDate] [datetime] NULL,
	[DiagnosedDateFormat] [tinyint] NULL,
	[ClosedDate] [datetime] NULL,
	[ClosedDateFormat] [tinyint] NULL,
	[MedicalCondition] [varchar](255) NULL,
	[MedicalConditionTypeId] [int] NULL,
	[MedicalConditionGroupId] [int] NULL,
	[Notes] [varchar](max) NULL,
	[IsNotesOnly] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_MedicalConditionItem_IsNotesOnly]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[ConsultationHistory](
	[ConsultationHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ConsultationId] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[UserId] [int] NULL,
	[RoomId] [int] NULL,
	[Notes] [varchar](255) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_ConsultationHistory_Guid]  DEFAULT (newid()),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatientRecord_ConsultationHistory_Version]  DEFAULT ((1)),
	[IsComplete] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_ConsultationHistory_IsComplete]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_ConsultationHistory_IsDeleted]  DEFAULT ((0)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL
) ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[PIDHistory](
	[PIDHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[PatientId] [int] NULL,
	[PIDHistoryDate] [datetime] NOT NULL,
	[Notes] [varchar](max) NULL,
	[IsNotRequired] [bit] NOT NULL,
	[Version] [float] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[RemovedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[CareContactId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

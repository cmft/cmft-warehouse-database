﻿CREATE TABLE [PatientRecord].[Consultation](
	[ConsultationId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_Consultation_Guid]  DEFAULT (newid()),
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[Version] [tinyint] NOT NULL CONSTRAINT [DF_PatientRecord_Consultation_Version]  DEFAULT ((1)),
	[IsComplete] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Consultation_IsComplete]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Consultation_IsDeleted]  DEFAULT ((0)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY]

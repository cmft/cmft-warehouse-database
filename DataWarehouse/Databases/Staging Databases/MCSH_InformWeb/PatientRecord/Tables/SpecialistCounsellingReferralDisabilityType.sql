﻿CREATE TABLE [PatientRecord].[SpecialistCounsellingReferralDisabilityType](
	[SpecialistCounsellingReferralId] [int] NOT NULL,
	[DisabilityTypeId] [int] NOT NULL
) ON [PRIMARY]

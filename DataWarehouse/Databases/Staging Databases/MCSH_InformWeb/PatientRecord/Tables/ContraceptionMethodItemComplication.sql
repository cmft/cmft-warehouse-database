﻿CREATE TABLE [PatientRecord].[ContraceptionMethodItemComplication](
	[ContraceptionMethodItemComplicationId] [int] IDENTITY(1,1) NOT NULL,
	[ContraceptionMethodItemId] [int] NOT NULL,
	[ComplicationId] [tinyint] NULL,
	[Notes] [varchar](255) NULL
) ON [PRIMARY]

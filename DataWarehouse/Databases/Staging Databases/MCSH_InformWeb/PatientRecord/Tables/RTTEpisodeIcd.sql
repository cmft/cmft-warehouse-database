﻿CREATE TABLE [PatientRecord].[RTTEpisodeIcd](
	[RTTEpisodeIcdId] [int] IDENTITY(1,1) NOT NULL,
	[RTTEpisodeId] [int] NOT NULL,
	[RTTIcdId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[IsPrimary] [bit] NOT NULL,
	[RTTEpisodeIcdDate] [datetime] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[YPSSCasePartnerLink](
	[YPSSCasePartnerLinkId] [int] IDENTITY(1,1) NOT NULL,
	[YPSSCasePartnerId] [int] NOT NULL,
	[PatientId] [int] NULL,
	[Type] [tinyint] NOT NULL,
	[Name] [varchar](20) NOT NULL
) ON [PRIMARY]

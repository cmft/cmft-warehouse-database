﻿CREATE TABLE [PatientRecord].[SHHAPTActivity](
	[SHHAPTActivityId] [int] IDENTITY(1,1) NOT NULL,
	[SHHAPTActivityDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[EpisodeId] [int] NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_SHHAPTActivity_IsNotRequired]  DEFAULT ((0)),
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_SHHAPTActivity_Guid]  DEFAULT (newid()),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_SHHAPTActivity_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_SHHAPTActivity_IsActive]  DEFAULT ((1)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatientRecord_SHHAPTActivity_Version]  DEFAULT ((1)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY]

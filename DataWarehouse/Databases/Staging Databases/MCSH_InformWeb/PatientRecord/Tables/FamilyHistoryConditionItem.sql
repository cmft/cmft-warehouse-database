﻿CREATE TABLE [PatientRecord].[FamilyHistoryConditionItem](
	[FamilyHistoryConditionItemId] [int] IDENTITY(1,1) NOT NULL,
	[FamilyHistoryConditionId] [int] NOT NULL,
	[Notes] [varchar](255) NULL,
	[FamilyHistoryId] [int] NOT NULL
) ON [PRIMARY]

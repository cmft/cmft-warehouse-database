﻿CREATE TABLE [PatientRecord].[TreatmentCervixType](
	[TreatmentCervixTypeId] [int] NOT NULL,
	[TreatmentCervixId] [int] NOT NULL,
	[Notes] [varchar](255) NULL
) ON [PRIMARY]

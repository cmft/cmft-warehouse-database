﻿CREATE TABLE [PatientRecord].[TestRequest](
	[TestRequestId] [int] IDENTITY(1,1) NOT NULL,
	[RequestedDate] [datetime] NOT NULL,
	[RequestedDateFormat] [tinyint] NOT NULL CONSTRAINT [DF_PatientRecord_TestRequest_RequestedDateFormat]  DEFAULT ((0)),
	[PatientId] [int] NULL,
	[CareContactId] [int] NULL,
	[InvestigationGroupId] [smallint] NULL,
	[SampleNo] [varchar](75) NULL,
	[Notes] [varchar](max) NULL,
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientRecord_TestRequest_CreatedDateTime]  DEFAULT (getdate()),
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_TestRequest_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatientRecord_TestRequest_Version]  DEFAULT ((1)),
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_TestRequest_Guid]  DEFAULT (newid()),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_TestRequest_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_TestRequest_IsActive]  DEFAULT ((1)),
	[GroupInvestigationCodeId] [int] NULL,
	[IsGroupRequest] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_TestRequest_IsGroupRequest]  DEFAULT ((0)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL,
	[ProcessedMessageId] [int] NULL,
	[RequestDay]  AS (dateadd(day,(0),datediff(day,(0),[RequestedDate])))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

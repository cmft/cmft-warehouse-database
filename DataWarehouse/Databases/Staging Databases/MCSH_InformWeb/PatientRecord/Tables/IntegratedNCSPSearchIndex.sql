﻿CREATE TABLE [PatientRecord].[IntegratedNCSPSearchIndex](
	[IntegratedNCSPId] [int] NOT NULL,
	[SearchIndex] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

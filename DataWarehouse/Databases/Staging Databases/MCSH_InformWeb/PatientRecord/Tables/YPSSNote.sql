﻿CREATE TABLE [PatientRecord].[YPSSNote](
	[YPSSNoteId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[YPSSNoteDate] [datetime] NOT NULL,
	[YPSSCaseId] [int] NULL,
	[PatientId] [int] NULL,
	[ActivityType] [int] NOT NULL,
	[Outcome] [int] NOT NULL,
	[TimeSpent] [tinyint] NULL,
	[PerformedByUserId] [int] NULL,
	[PerformedByUserName] [varchar](100) NULL,
	[Notes] [varchar](max) NULL,
	[IsNotRequired] [bit] NOT NULL,
	[Version] [float] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[RemovedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[CareContactId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[MenstruationBleedingProblem](
	[MenstruationId] [int] NOT NULL,
	[MenstruationBleedingProblemTypeId] [int] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[OutboundReferral](
	[OutboundReferralId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_OutboundReferral_Guid]  DEFAULT (newid()),
	[OutboundReferralDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[ReferralId] [int] NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_OutboundReferral_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatientRecord_OutboundReferral_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_OutboundReferral_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_OutboundReferral_IsActive]  DEFAULT ((1)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY]

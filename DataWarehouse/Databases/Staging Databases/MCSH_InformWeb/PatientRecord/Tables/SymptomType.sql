﻿CREATE TABLE [PatientRecord].[SymptomType](
	[SymptomTypeId] [int] NOT NULL,
	[SymptomId] [int] NOT NULL,
	[Notes] [varchar](255) NULL
) ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[ContraceptionHistoryItem](
	[ContraceptionHistoryItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContraceptionHistoryId] [int] NOT NULL,
	[ContraceptionTypeId] [int] NOT NULL,
	[IsMain] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_ContraceptionHistoryItem_IsMain]  DEFAULT ((0)),
	[IsAdditional] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_ContraceptionHistoryItem_IsAdditional]  DEFAULT ((0)),
	[IsProvidedByService] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_ContraceptionHistoryItem_IsProvidedByService]  DEFAULT ((0)),
	[IsProblem] [bit] NOT NULL CONSTRAINT [DF_CareTemplate_ContraceptionHistoryItem_IsProblem]  DEFAULT ((0)),
	[Notes] [varchar](255) NULL
) ON [PRIMARY]

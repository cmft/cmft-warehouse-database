﻿CREATE TABLE [PatientRecord].[CCardPack](
	[CCardPackId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[CCardRegistrationId] [int] NOT NULL,
	[PackNo] [tinyint] NOT NULL,
	[DateIssued] [datetime] NOT NULL,
	[CCardVenueId] [smallint] NOT NULL,
	[IssuedBy] [varchar](50) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL
) ON [PRIMARY]

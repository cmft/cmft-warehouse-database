﻿CREATE TABLE [PatientRecord].[ExaminationType](
	[ExaminationTypeId] [int] NOT NULL,
	[ExaminationId] [int] NOT NULL,
	[Notes] [varchar](255) NULL
) ON [PRIMARY]

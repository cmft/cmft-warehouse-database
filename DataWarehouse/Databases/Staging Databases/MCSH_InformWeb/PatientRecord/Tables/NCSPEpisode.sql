﻿CREATE TABLE [PatientRecord].[NCSPEpisode](
	[NCSPEpisodeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[NCSPEpisodeDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[IsOpen] [bit] NULL,
	[ClosedDate] [datetime] NULL,
	[NotificationStatus] [int] NULL,
	[NotifiedBy] [int] NULL,
	[NotifiedDate] [datetime] NULL,
	[NotifiedNotes] [varchar](255) NULL,
	[PartnerManagementStatus] [int] NULL,
	[MalePartners] [tinyint] NULL,
	[FemalePartners] [tinyint] NULL,
	[UnknownPartners] [tinyint] NULL,
	[NCSPScreenResultId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[IsNotRequired] [bit] NOT NULL,
	[Version] [float] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TreatmentStatus] [int] NULL,
	[TreatmentDate] [datetime] NULL,
	[TreatmentSiteId] [int] NULL,
	[TreatmentSiteName] [varchar](255) NULL,
	[TreatmentServiceTypeId] [int] NULL,
	[TreatmentUsed] [varchar](255) NULL,
	[TreatmentNotes] [varchar](max) NULL,
	[ConfirmedDate] [datetime] NULL,
	[ConfirmedBy] [varchar](255) NULL,
	[CourseCompleted] [bit] NULL,
	[ComplianceChecked] [bit] NULL,
	[PatientVomited] [bit] NULL,
	[PatientAbstained] [bit] NULL,
	[PatientCompliant] [bit] NULL,
	[RetreatmentRequired] [bit] NULL,
	[RetreatmentNotes] [varchar](max) NULL,
	[RetreatmentDetails] [varchar](max) NULL,
	[SaferSexDiscussed] [bit] NULL,
	[AppointmentOffered] [varchar](1) NULL,
	[PostalKitSentForRetest] [bit] NULL,
	[TestOfCure] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

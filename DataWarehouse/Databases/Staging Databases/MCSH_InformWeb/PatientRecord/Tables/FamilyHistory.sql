﻿CREATE TABLE [PatientRecord].[FamilyHistory](
	[FamilyHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_FamilyHistory_Guid]  DEFAULT (newid()),
	[FamilyHistoryDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[Notes] [varchar](max) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_FamilyHistory_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatienRecordt_FamilyHistory_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_FamilyHistory_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_FamilyHistory_IsActive]  DEFAULT ((1)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

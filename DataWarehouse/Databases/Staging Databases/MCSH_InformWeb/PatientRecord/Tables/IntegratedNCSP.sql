﻿CREATE TABLE [PatientRecord].[IntegratedNCSP](
	[IntegratedNCSPId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[DateOfScreen] [datetime] NULL,
	[ScreenTypeId] [int] NULL,
	[NHSNumber] [varchar](10) NULL,
	[GivenName] [varchar](35) NULL,
	[FamilyName] [varchar](35) NULL,
	[BirthDate] [datetime] NULL,
	[AgeAtScreen] [tinyint] NULL,
	[Gender] [tinyint] NULL,
	[EthnicityId] [smallint] NULL,
	[AddressLine1] [varchar](35) NULL,
	[AddressLine2] [varchar](35) NULL,
	[AddressLine3] [varchar](35) NULL,
	[AddressLine4] [varchar](35) NULL,
	[AddressLine5] [varchar](35) NULL,
	[Postcode] [varchar](8) NULL,
	[PostcodeOfResidence] [varchar](8) NULL,
	[HasWritePermission] [bit] NULL,
	[Landline] [varchar](35) NULL,
	[LandlineHasCallPermission] [bit] NULL,
	[LandlineHasVMPermission] [bit] NULL,
	[Mobile] [varchar](35) NULL,
	[MobileHasCallPermission] [bit] NULL,
	[MobileHasVMPermission] [bit] NULL,
	[MobileHasSMSPermission] [bit] NULL,
	[Email] [varchar](255) NULL,
	[HasEmailPermission] [bit] NULL,
	[GPCode] [varchar](8) NULL,
	[GPName] [varchar](100) NULL,
	[GPPracticeCode] [varchar](8) NULL,
	[GPPracticeName] [varchar](100) NULL,
	[GPAddressLine1] [varchar](35) NULL,
	[GPAddressLine2] [varchar](35) NULL,
	[GPAddressLine3] [varchar](35) NULL,
	[GPAddressLine4] [varchar](35) NULL,
	[GPAddressLine5] [varchar](35) NULL,
	[GPTelephone] [varchar](35) NULL,
	[GPPostcode] [varchar](8) NULL,
	[GPContactTelephoneNumber] [varchar](12) NULL,
	[GPContactPermission] [bit] NULL,
	[LSOA] [varchar](35) NULL,
	[LSOACode] [varchar](9) NULL,
	[CCGResidenceName] [varchar](35) NULL,
	[CCGResidenceCode] [varchar](9) NULL,
	[CCGResponsibleName] [varchar](35) NULL,
	[CCGResponsibleCode] [varchar](9) NULL,
	[ReferenceNumber] [varchar](100) NULL,
	[ResultsReportedDate] [datetime] NULL,
	[ResultsReceivedByServiceDate] [datetime] NULL,
	[ReasonForTestId] [int] NULL,
	[Symptomatic] [tinyint] NULL,
	[SpecimenTypeId] [int] NULL,
	[ChlamydiaResultId] [int] NULL,
	[GonorrhoeaResultId] [int] NULL,
	[FraserAssessmentId] [int] NULL,
	[FirstPreferredMethodOfContactId] [int] NULL,
	[SecondPreferredMethodOfContactId] [int] NULL,
	[PermissionToSendReTestReminder] [bit] NULL,
	[TreatmentFirstOfferedDate] [datetime] NULL,
	[ScreeningSiteId] [int] NULL,
	[ScreeningServiceTypeId] [int] NULL,
	[TreatmentSiteId] [int] NULL,
	[TreatmentServiceTypeId] [int] NULL,
	[ComplianceCheckedId] [int] NULL,
	[CourseCompleted] [bit] NULL,
	[DidThePatientVomit] [bit] NULL,
	[DidThePatientAbstain] [bit] NULL,
	[PatientCompliant] [bit] NULL,
	[ReTreatmentRequired] [bit] NULL,
	[PlannedDateAndLocationForReTreatment] [varchar](255) NULL,
	[ConfirmationOfReTreatmentDetails] [varchar](255) NULL,
	[PartnerNotificationOffered] [bit] NULL,
	[TestRequestItemId] [int] NULL,
	[Notes] [varchar](max) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[Version] [float] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsNotRequired] [bit] NOT NULL,
	[OwnerCareTemplateId] [int] NULL,
	[LADistrictCode] [varchar](9) NULL,
	[OfferAndUptakeId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

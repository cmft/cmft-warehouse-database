﻿CREATE TABLE [PatientRecord].[YPSSCaseOutboundReferral](
	[YPSSCaseId] [int] NOT NULL,
	[OutboundReferralId] [int] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[AttachmentData](
	[AttachmentId] [int] NOT NULL,
	[SequenceNo] [smallint] NULL,
	[Filename] [varchar](255) NULL,
	[FileType] [varchar](255) NULL,
	[DocumentData] [varbinary](max) NULL,
	[AttachmentDataId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
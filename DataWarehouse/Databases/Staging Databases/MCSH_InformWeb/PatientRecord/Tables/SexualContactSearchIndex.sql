﻿CREATE TABLE [PatientRecord].[SexualContactSearchIndex](
	[SexualContactId] [int] NOT NULL,
	[Patient] [varchar](max) NOT NULL,
	[Address] [varchar](max) NOT NULL,
	[Notes] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

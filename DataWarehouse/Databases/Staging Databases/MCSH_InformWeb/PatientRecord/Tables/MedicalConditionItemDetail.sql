﻿CREATE TABLE [PatientRecord].[MedicalConditionItemDetail](
	[MedicalConditionItemDetailId] [int] IDENTITY(1,1) NOT NULL,
	[MedicalConditionItemId] [int] NOT NULL,
	[MedicalConditionDetailTypeId] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [PatientRecord].[HIVConsentToInformGPLetterDate](
	[HIVConsentToInformGPLetterDateId] [int] IDENTITY(1,1) NOT NULL,
	[HIVConsentToInformGPId] [int] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_HIVConsentToInformGPLetterDate_Guid]  DEFAULT (newid()),
	[LetterDate] [datetime] NOT NULL,
	[PatientCopied] [bit] NOT NULL,
	[Notes] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

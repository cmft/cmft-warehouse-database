﻿CREATE TABLE [PatientRecord].[ContraceptionMethodItem](
	[ContraceptionMethodItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContraceptionMethodId] [int] NOT NULL,
	[AdminContraceptionMethodId] [tinyint] NOT NULL,
	[IsCurrent] [bit] NOT NULL,
	[IsPast] [bit] NOT NULL,
	[Notes] [varchar](255) NULL
) ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[UrinalysisTestConfiguration](
	[UrinalysisTestConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[TestName] [varchar](max) NULL,
	[ControlType] [varchar](max) NULL,
	[LookupKey] [varchar](max) NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_UrinalysisTestConfiguration_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_UrinalysisTestConfiguration_IsActive]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

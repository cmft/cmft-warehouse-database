﻿CREATE TABLE [PatientRecord].[Measurement](
	[MeasurementId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_Measurement_Guid]  DEFAULT (newid()),
	[MeasurementDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[Height] [float] NOT NULL CONSTRAINT [DF_PatientRecord_Measurement_Height]  DEFAULT ((0)),
	[Weight] [float] NOT NULL CONSTRAINT [DF_PatientRecord_Measurement_Weight]  DEFAULT ((0)),
	[BMI] [float] NOT NULL CONSTRAINT [DF_PatientRecord_Measurement_BMI]  DEFAULT ((0)),
	[Notes] [varchar](255) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Measurement_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatienRecord_Measurement_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Measurement_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_Measurement_IsActive]  DEFAULT ((1)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[PreviousPregnancy](
	[PreviousPregnancyId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NOT NULL,
	[PreviousPregnancyDate] [datetime] NOT NULL,
	[PregnancyStatus] [tinyint] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[IsNotRequired] [bit] NOT NULL,
	[Version] [float] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY]

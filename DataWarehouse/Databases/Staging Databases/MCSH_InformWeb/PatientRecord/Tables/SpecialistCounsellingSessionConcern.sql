﻿CREATE TABLE [PatientRecord].[SpecialistCounsellingSessionConcern](
	[SpecialistCounsellingSessionId] [int] NOT NULL,
	[ConcernId] [int] NOT NULL
) ON [PRIMARY]

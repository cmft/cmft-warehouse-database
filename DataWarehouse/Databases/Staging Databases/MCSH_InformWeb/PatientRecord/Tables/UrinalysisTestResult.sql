﻿CREATE TABLE [PatientRecord].[UrinalysisTestResult](
	[UrinalysisTestResultId] [int] IDENTITY(1,1) NOT NULL,
	[TestName] [varchar](max) NULL,
	[Value] [varchar](max) NULL,
	[UrinalysisId] [int] NOT NULL,
	[UrinalysisTestConfigurationId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_UrinalysisTestResult_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_UrinalysisTestResult_IsActive]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [PatientRecord].[MedicalCondition](
	[MedicalConditionId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_MedicalCondition_Guid]  DEFAULT (newid()),
	[MedicalConditionDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[CareContactId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL,
	[MedicalConditionGroupId] [int] NULL,
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientRecord_MedicalCondition_CreatedDateTime]  DEFAULT (getdate()),
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_MedicalCondition_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatienRecord_MedicalCondition_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_MedicalCondition_IsDeleted]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_MedicalCondition_IsActive]  DEFAULT ((1)),
	[IsNotesOnly] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_MedicalConditions_IsNotesOnly]  DEFAULT ((0)),
	[Notes] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [PatientRecord].[IssueAndSupplyProvision](
	[IssueAndSupplyProvisionId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_IssueAndSupplyProvision_Guid]  DEFAULT (newid()),
	[IssueAndSupplyId] [int] NULL,
	[IssueAndSupplyProvisionDate] [datetime] NOT NULL,
	[ProvidedByStaffName] [varchar](255) NOT NULL,
	[Quantity] [varchar](255) NULL,
	[BatchNumber] [varchar](255) NULL,
	[ExpiryDate] [varchar](35) NULL,
	[Site] [varchar](max) NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_IssueAndSupplyProvision_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_PatientRecord_IssueAndSupplyProvision_Version]  DEFAULT ((1)),
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_PatientRecord_IssueAndSupplyProvision_CreatedDateTime]  DEFAULT (getdate()),
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_IssueAndSupplyProvision_IsActive]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_IssueAndSupplyProvision_IsDeleted]  DEFAULT ((0)),
	[RemovedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[CareContactId] [int] NULL,
	[OwnerCareTemplateId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

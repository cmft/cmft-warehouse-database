﻿CREATE TABLE [Requesting].[NotificationStatus](
	[NotificationStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsTrigger] [bit] NOT NULL,
	[IsStandardForNewInvestigationCodes] [bit] NOT NULL
) ON [PRIMARY]

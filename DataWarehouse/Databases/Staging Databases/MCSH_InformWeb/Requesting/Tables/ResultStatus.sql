﻿CREATE TABLE [Requesting].[ResultStatus](
	[ResultStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Requesting_ResultStatus_Guid]  DEFAULT (newid()),
	[Name] [varchar](75) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Requesting_ResultStatus_IsActive]  DEFAULT ((1)),
	[IsStandardForNewInvestigationCodes] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

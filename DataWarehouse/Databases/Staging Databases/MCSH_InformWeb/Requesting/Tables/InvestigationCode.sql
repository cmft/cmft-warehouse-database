﻿CREATE TABLE [Requesting].[InvestigationCode](
	[InvestigationCodeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Requesting_InvestigationCode_Guid]  DEFAULT (newid()),
	[Name] [varchar](75) NOT NULL,
	[Code] [varchar](26) NULL,
	[AssociatedCodeIds] [varchar](255) NULL,
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_Requesting_InvestigationCode_Gender]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Requesting_InvestigationCode_IsActive]  DEFAULT ((1)),
	[SpecialtyCode] [char](1) NULL,
	[IsGroupCode] [bit] NOT NULL CONSTRAINT [DF_Requesting_InvestigationCode_IsGroupCode]  DEFAULT ((0)),
	[IsReviewable] [bit] NOT NULL CONSTRAINT [DF_Requesting_InvestigationCode_IsReviewable]  DEFAULT ((0)),
	[ShowInSelections] [bit] NOT NULL CONSTRAINT [DF_Requesting_InvestigationCode_ShowInSelections]  DEFAULT ((1)),
	[AutomaticallyCreated] [bit] NOT NULL CONSTRAINT [DF_Requesting_InvestigationCode_AutomaticallyCreated]  DEFAULT ((0)),
	[OverwrittenBy] [xml] NULL,
	[CanAttachToNCSP] [bit] NOT NULL CONSTRAINT [DF_Requesting_InvestigationCode]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

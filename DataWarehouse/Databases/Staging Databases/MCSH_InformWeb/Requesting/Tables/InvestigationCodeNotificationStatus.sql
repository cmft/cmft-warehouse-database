﻿CREATE TABLE [Requesting].[InvestigationCodeNotificationStatus](
	[NotificationStatusId] [int] NOT NULL,
	[InvestigationCodeId] [int] NOT NULL
) ON [PRIMARY]

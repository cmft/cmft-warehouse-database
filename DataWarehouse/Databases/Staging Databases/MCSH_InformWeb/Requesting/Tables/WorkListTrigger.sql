﻿CREATE TABLE [Requesting].[WorkListTrigger](
	[WorkListTriggerId] [int] IDENTITY(1,1) NOT NULL,
	[InvestigationCodeId] [smallint] NOT NULL,
	[WorkListId] [int] NOT NULL,
	[WorkGroupId] [int] NOT NULL,
	[WorkListStageId] [smallint] NOT NULL,
	[IsAbnormal] [bit] NULL,
	[NotificationStatusId] [tinyint] NULL,
	[ActionStatus] [tinyint] NULL
) ON [PRIMARY]

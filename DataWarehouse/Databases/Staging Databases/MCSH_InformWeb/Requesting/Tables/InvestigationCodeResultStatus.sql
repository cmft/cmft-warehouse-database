﻿CREATE TABLE [Requesting].[InvestigationCodeResultStatus](
	[ResultStatusId] [int] NOT NULL,
	[InvestigationCodeId] [int] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [Requesting].[InvestigationStatus](
	[InvestigationStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsStandardForNewInvestigationCodes] [bit] NOT NULL,
	[IsTestCarriedOut] [bit] NOT NULL
) ON [PRIMARY]

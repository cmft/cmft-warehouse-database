﻿CREATE TABLE [Requesting].[ActionStatus](
	[ActionStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Requesting_ActionStatus_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
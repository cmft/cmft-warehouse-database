﻿CREATE TABLE [Requesting].[SampleSite](
	[SampleSiteId] [smallint] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Gender] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

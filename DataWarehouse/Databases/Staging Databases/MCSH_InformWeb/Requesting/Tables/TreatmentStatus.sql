﻿CREATE TABLE [Requesting].[TreatmentStatus](
	[TreatmentStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Requesting_TreatmentStatus_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [Requesting].[Reason](
	[ReasonId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL,
	[IsStandardForNewInvestigationCodes] [bit] NOT NULL
) ON [PRIMARY]

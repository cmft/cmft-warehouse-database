﻿CREATE TABLE [Requesting].[InvestigationCodeGroup](
	[InvestigationGroupId] [smallint] NOT NULL,
	[InvestigationCodeId] [int] NOT NULL
) ON [PRIMARY]

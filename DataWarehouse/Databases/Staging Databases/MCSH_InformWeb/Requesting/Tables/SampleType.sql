﻿CREATE TABLE [Requesting].[SampleType](
	[SampleTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [PK_Requesting_SampleType_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](10) NULL,
	[IsActive] [bit] NOT NULL,
	[Gender] [tinyint] NULL
) ON [PRIMARY]

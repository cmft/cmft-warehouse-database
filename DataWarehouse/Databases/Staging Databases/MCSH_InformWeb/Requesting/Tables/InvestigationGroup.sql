﻿CREATE TABLE [Requesting].[InvestigationGroup](
	[InvestigationGroupId] [smallint] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](75) NOT NULL,
	[Code] [varchar](6) NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

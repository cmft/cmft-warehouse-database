﻿CREATE TABLE [Requesting].[InvestigationCodeResult](
	[InvestigationCodeResultId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[InvestigationCodeId] [int] NOT NULL,
	[Name] [varchar](75) NOT NULL,
	[Code] [varchar](26) NULL,
	[RangeMinimum] [decimal](19, 3) NULL,
	[RangeMaximum] [decimal](19, 3) NULL,
	[Units] [varchar](20) NULL,
	[IsNumericValue] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ShowInSelections] [bit] NOT NULL,
	[AutomaticallyCreated] [bit] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [Requesting].[ResultStatusRule](
	[ResultStatusRuleId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Requesting_ResultStatusRule_Guid]  DEFAULT (newid()),
	[InvestigationCodeId] [int] NULL,
	[ResultStatusId] [int] NOT NULL,
	[Rule] [varchar](1000) NULL,
	[IsAbnormal] [bit] NULL,
	[Priority] [int] NOT NULL,
	[IgnoreCase] [bit] NOT NULL,
	[AnywhereInString] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Requesting_ResultStatusRule_IsActive]  DEFAULT ((1)),
	[OnlyApplyToFirstVersion] [bit] NOT NULL DEFAULT ((0)),
	[AllResultTextValuesMustMatch] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

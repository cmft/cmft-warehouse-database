﻿CREATE TABLE [Messaging].[ProcessedMessage](
	[ProcessedMessageId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Messaging_ProcessedMessage_Guid]  DEFAULT (newid()),
	[Body] [varchar](max) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[Format] [varchar](50) NOT NULL,
	[ReceivedDate] [datetime] NULL,
	[ProcessedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Messaging_ProcessedMessage_IsActive]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

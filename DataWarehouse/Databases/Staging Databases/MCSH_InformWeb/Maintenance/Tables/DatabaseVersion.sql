﻿CREATE TABLE [Maintenance].[DatabaseVersion](
	[DatabaseVersionId] [int] IDENTITY(1,1) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[VersionNo] [varchar](100) NULL
) ON [PRIMARY]

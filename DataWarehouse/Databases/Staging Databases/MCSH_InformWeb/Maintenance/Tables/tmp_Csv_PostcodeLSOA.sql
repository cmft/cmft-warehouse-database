﻿CREATE TABLE [Maintenance].[tmp_Csv_PostcodeLSOA](
	[Postcode] [nvarchar](10) NULL,
	[LSOA] [nvarchar](12) NULL,
	[PCT] [nvarchar](5) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Maintenance].[ReleaseInfo](
	[ReleaseInfoId] [int] IDENTITY(1,1) NOT NULL,
	[VersionNo] [varchar](100) NOT NULL,
	[Notes] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

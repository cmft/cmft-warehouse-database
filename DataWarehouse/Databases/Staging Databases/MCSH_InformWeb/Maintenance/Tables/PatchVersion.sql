﻿CREATE TABLE [Maintenance].[PatchVersion](
	[PatchVersionId] [int] IDENTITY(1,1) NOT NULL,
	[PatchDate] [datetime] NOT NULL CONSTRAINT [DF_Maintenance_PatchVersion_PatchDate]  DEFAULT (getdate()),
	[VersionNo] [decimal](9, 4) NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [Document].[MailMerge](
	[MailMergeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Url] [varchar](255) NOT NULL,
	[IconUrl] [varchar](255) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Document_MailMerge_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

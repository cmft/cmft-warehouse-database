﻿CREATE TABLE [Document].[LetterTemplate](
	[LetterTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[ParentLetterTemplateId] [int] NULL,
	[Name] [varchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LocationId] [int] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Document_LetterTemplate_Guid]  DEFAULT (newid()),
	[Version] [float] NOT NULL CONSTRAINT [DF_Document_LetterTemplate_Version]  DEFAULT ((1)),
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Document_LetterTemplate_CreatedDateTime]  DEFAULT (getdate()),
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL,
	[RemovedDateTime] [datetime] NULL,
	[IsFolder] [bit] NOT NULL CONSTRAINT [DF_Document_LetterTemplate_IsFolder]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Document_LetterTemplate_IsActive]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

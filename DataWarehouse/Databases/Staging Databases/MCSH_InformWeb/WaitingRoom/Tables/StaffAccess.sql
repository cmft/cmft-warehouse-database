﻿CREATE TABLE [WaitingRoom].[StaffAccess](
	[StaffAccessId] [int] IDENTITY(1,1) NOT NULL,
	[UserRoleId] [int] NOT NULL,
	[ClinicId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[JoinTime] [datetime] NOT NULL CONSTRAINT [DF_WaitingRoom_StaffingAccess_JoinTime]  DEFAULT (getdate()),
	[LeaveTime] [datetime] NULL,
	[TakenByStaffAccessId] [int] NULL
) ON [PRIMARY]

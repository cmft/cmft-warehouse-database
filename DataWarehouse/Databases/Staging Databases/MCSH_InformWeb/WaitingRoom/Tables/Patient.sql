﻿CREATE TABLE [WaitingRoom].[Patient](
	[WaitingRoomId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[AppointmentId] [int] NOT NULL,
	[ClinicId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[WaitingForRoleId] [int] NULL,
	[WaitingForUserId] [int] NULL,
	[WaitStartTime] [datetime] NOT NULL CONSTRAINT [DF_WaitingRoom_Patient_WaitStartTime]  DEFAULT (getdate()),
	[ArrivedDateTime] [datetime] NOT NULL,
	[IsPatientCalled] [bit] NOT NULL CONSTRAINT [DF_WaitingRoom_Patient_IsPatientCalled]  DEFAULT ((0)),
	[CallingUserRoleId] [int] NULL,
	[IsInConsultation] [bit] NOT NULL CONSTRAINT [DF_WaitingRoom_Patient_IsInConsultation]  DEFAULT ((0)),
	[ConsultationUserRoleId] [int] NULL,
	[ConsultationId] [int] NULL,
	[Notes] [varchar](max) NULL,
	[IsPriority] [bit] NOT NULL CONSTRAINT [DF_WaitingRoom_Patient_IsPriority]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

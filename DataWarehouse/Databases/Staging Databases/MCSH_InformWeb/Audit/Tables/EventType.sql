﻿CREATE TABLE [Audit].[EventType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventTypeId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL
) ON [PRIMARY]

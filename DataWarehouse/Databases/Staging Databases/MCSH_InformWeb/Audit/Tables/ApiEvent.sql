﻿CREATE TABLE [Audit].[ApiEvent](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[Url] [varchar](1000) NULL,
	[Username] [varchar](200) NULL,
	[Details] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

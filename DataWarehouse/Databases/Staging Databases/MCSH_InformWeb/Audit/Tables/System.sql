﻿CREATE TABLE [Audit].[System](
	[SystemAuditId] [int] IDENTITY(1,1) NOT NULL,
	[SystemAuditDate] [datetime] NOT NULL,
	[SystemEventTypeId] [int] NOT NULL,
	[UserId] [int] NULL,
	[UserRoleId] [int] NULL,
	[LocationId] [int] NULL,
	[OrganisationId] [int] NULL,
	[Details] [varchar](2000) NULL
) ON [PRIMARY]

﻿CREATE TABLE [Audit].[SystemEventType](
	[SystemEventTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL
) ON [PRIMARY]

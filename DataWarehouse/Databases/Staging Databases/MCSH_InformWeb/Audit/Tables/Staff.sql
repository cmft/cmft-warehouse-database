﻿CREATE TABLE [Audit].[Staff](
	[StaffAuditId] [int] IDENTITY(1,1) NOT NULL,
	[StaffAuditDate] [datetime] NOT NULL,
	[StaffEventTypeId] [smallint] NOT NULL,
	[UserId] [int] NULL,
	[UserRoleId] [int] NULL,
	[LocationId] [int] NULL,
	[OrganisationId] [int] NULL,
	[Details] [varchar](255) NULL
) ON [PRIMARY]

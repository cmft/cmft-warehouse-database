﻿CREATE TABLE [Audit].[StaffEventType](
	[StaffEventTypeId] [smallint] NOT NULL,
	[Name] [varchar](50) NOT NULL
) ON [PRIMARY]

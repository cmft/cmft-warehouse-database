﻿CREATE TABLE [Audit].[Patient](
	[PatientAuditId] [int] IDENTITY(1,1) NOT NULL,
	[PatientAuditDate] [datetime] NOT NULL,
	[PatientEventTypeId] [smallint] NOT NULL,
	[PatientId] [int] NOT NULL,
	[UserId] [int] NULL,
	[UserRoleId] [int] NULL,
	[LocationId] [int] NULL,
	[OrganisationId] [int] NULL,
	[Details] [varchar](255) NULL
) ON [PRIMARY]

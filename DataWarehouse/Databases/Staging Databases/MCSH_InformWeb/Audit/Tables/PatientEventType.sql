﻿CREATE TABLE [Audit].[PatientEventType](
	[PatientEventTypeId] [smallint] NOT NULL,
	[Name] [varchar](50) NOT NULL
) ON [PRIMARY]

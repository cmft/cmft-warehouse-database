﻿CREATE TABLE [Audit].[SchedulingEngine](
	[SchedulingEngineAuditId] [int] IDENTITY(1,1) NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[EventType] [varchar](max) NULL,
	[Component] [varchar](max) NULL,
	[Details] [varchar](max) NULL,
	[UserId] [int] NULL,
	[UserRoleId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
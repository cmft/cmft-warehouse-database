﻿CREATE TABLE [Audit].[AppointmentEvent](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[EventTypeId] [int] NOT NULL,
	[EventSourceId] [int] NOT NULL,
	[UserRoleId] [int] NOT NULL,
	[LocationId] [int] NOT NULL,
	[LocationName] [varchar](100) NULL,
	[ClinicId] [int] NULL,
	[ClinicName] [varchar](100) NULL,
	[ListId] [int] NULL,
	[ListName] [varchar](50) NULL,
	[AppointmentSlotId] [int] NULL,
	[EventDetails] [varchar](255) NULL,
	[PatientId] [int] NULL
) ON [PRIMARY]

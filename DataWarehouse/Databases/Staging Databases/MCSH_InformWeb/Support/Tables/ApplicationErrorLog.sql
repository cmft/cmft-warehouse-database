﻿CREATE TABLE [Support].[ApplicationErrorLog](
	[ApplicationErrorLogId] [int] IDENTITY(1,1) NOT NULL,
	[ExceptionDate] [datetime] NOT NULL,
	[ExceptionMessage] [varchar](max) NULL,
	[ExceptionDetails] [varchar](max) NULL,
	[SourceUrl] [varchar](255) NULL,
	[UserRoleId] [int] NULL,
	[PatientId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

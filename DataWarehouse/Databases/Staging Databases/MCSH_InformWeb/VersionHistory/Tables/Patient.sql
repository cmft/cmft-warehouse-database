﻿CREATE TABLE [VersionHistory].[Patient](
	[VersionHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[SourceTypeId] [smallint] NOT NULL,
	[SourceXml] [varchar](max) NOT NULL,
	[SourceColumnId] [int] NOT NULL,
	[Version] [tinyint] NOT NULL,
	[UserRoleId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

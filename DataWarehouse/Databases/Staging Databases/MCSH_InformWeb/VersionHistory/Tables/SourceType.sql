﻿CREATE TABLE [VersionHistory].[SourceType](
	[SourceTypeId] [smallint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[SourceTable] [varchar](255) NOT NULL
) ON [PRIMARY]

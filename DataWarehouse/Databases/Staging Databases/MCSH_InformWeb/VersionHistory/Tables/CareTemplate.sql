﻿CREATE TABLE [VersionHistory].[CareTemplate](
	[VersionHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[UserRoleId] [int] NOT NULL,
	[SourceXml] [varchar](max) NOT NULL,
	[SourceColumnId] [int] NOT NULL,
	[Version] [float] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

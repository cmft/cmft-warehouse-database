﻿CREATE TABLE [Formulary].[DrugType](
	[DrugTypeId] [smallint] IDENTITY(1,1) NOT NULL,
	[DrugTypeGuid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Formulary].[DrugGroup](
	[DrugGroupId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Formulary_DrugGroup_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Formulary_DrugGroup_IsActive]  DEFAULT ((1)),
	[CompetencyId] [int] NULL
) ON [PRIMARY]

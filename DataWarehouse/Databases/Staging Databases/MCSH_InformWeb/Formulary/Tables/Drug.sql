﻿CREATE TABLE [Formulary].[Drug](
	[DrugId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Formulary_Drug_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[DrugGroupId] [int] NOT NULL,
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_Formulary_Drug_Gender]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Formulary_Drug_IsActive]  DEFAULT ((1)),
	[IsTreatment] [bit] NOT NULL DEFAULT ((1)),
	[IsPrescription] [bit] NOT NULL DEFAULT ((0)),
	[DrugTypeId] [smallint] NULL
) ON [PRIMARY]

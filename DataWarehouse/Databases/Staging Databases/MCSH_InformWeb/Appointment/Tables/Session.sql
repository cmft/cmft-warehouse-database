﻿CREATE TABLE [Appointment].[Session](
	[SessionId] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleId] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[DayOfWeek] [tinyint] NOT NULL,
	[ClinicId] [int] NOT NULL,
	[IsCancelled] [bit] NOT NULL CONSTRAINT [DF_Appointment_Session_IsCancelled]  DEFAULT ((0))
) ON [PRIMARY]

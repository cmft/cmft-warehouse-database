﻿CREATE TABLE [Appointment].[AppointmentSlot](
	[AppointmentSlotId] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[IsBreak] [bit] NOT NULL CONSTRAINT [DF_Appointment_AppointmentSlot_IsBreak]  DEFAULT ((0)),
	[Description] [varchar](50) NULL,
	[SlotStatusId] [tinyint] NOT NULL,
	[IsCancelled] [bit] NOT NULL CONSTRAINT [DF_Appointment_AppointmentSlot_IsCancelled]  DEFAULT ((0)),
	[UserRoleId] [int] NULL,
	[OriginalEndTime] [datetime] NULL,
	[ParentAppointmentSlotId] [int] NULL,
	[TimePeriod] [tinyint] NULL
) ON [PRIMARY]

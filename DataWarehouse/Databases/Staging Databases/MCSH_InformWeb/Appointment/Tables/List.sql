﻿CREATE TABLE [Appointment].[List](
	[ListId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](100) NULL,
	[ClinicId] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[RecurrenceRule] [varchar](max) NULL,
	[CreatedByUserRoleId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Appointment_List_IsDeleted]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

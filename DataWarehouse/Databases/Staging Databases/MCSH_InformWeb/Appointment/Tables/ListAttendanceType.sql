﻿CREATE TABLE [Appointment].[ListAttendanceType](
	[ListId] [int] NOT NULL,
	[AttendanceTypeId] [tinyint] NOT NULL,
	[MaximumAllowed] [tinyint] NOT NULL CONSTRAINT [DF_Appointment_ListAttendanceType_MaximumAllowed]  DEFAULT ((0))
) ON [PRIMARY]

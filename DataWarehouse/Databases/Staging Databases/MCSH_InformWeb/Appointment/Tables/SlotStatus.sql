﻿CREATE TABLE [Appointment].[SlotStatus](
	[SlotStatusId] [tinyint] NOT NULL,
	[Code] [char](2) NOT NULL,
	[Description] [varchar](255) NOT NULL
) ON [PRIMARY]

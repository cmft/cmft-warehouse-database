﻿CREATE TABLE [Appointment].[ScheduleIntendedService](
	[ScheduleId] [int] NOT NULL,
	[ItemOfServiceId] [int] NOT NULL,
	[MaximumAllowed] [tinyint] NOT NULL CONSTRAINT [DF_Appointment_ScheduleIntendedService_MaximumAllowed]  DEFAULT ((0))
) ON [PRIMARY]

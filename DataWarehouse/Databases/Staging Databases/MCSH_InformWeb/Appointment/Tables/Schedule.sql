﻿CREATE TABLE [Appointment].[Schedule](
	[ScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[ListId] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[RecurrenceRule] [varchar](max) NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

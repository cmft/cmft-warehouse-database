﻿CREATE TABLE [Appointment].[AppointmentSlotAttendanceType](
	[AppointmentSlotId] [int] NOT NULL,
	[AttendanceTypeId] [tinyint] NOT NULL
) ON [PRIMARY]

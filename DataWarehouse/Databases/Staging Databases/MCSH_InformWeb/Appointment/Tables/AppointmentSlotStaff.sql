﻿CREATE TABLE [Appointment].[AppointmentSlotStaff](
	[AppointmentSlotStaffId] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentSlotId] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [int] NULL
) ON [PRIMARY]

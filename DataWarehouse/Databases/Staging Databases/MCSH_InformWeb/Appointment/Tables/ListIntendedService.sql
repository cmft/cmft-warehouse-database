﻿CREATE TABLE [Appointment].[ListIntendedService](
	[ListId] [int] NOT NULL,
	[ItemOfServiceId] [int] NOT NULL,
	[MaximumAllowed] [tinyint] NOT NULL CONSTRAINT [DF_Appointment_ListIntendedService_MaximumAllowed]  DEFAULT ((0))
) ON [PRIMARY]

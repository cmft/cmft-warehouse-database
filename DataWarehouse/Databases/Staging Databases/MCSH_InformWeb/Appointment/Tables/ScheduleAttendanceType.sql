﻿CREATE TABLE [Appointment].[ScheduleAttendanceType](
	[ScheduleId] [int] NOT NULL,
	[AttendanceTypeId] [tinyint] NOT NULL,
	[MaximumAllowed] [tinyint] NOT NULL CONSTRAINT [DF_Appointment_ScheduleAttendanceType_MaximumAllowed]  DEFAULT ((0))
) ON [PRIMARY]

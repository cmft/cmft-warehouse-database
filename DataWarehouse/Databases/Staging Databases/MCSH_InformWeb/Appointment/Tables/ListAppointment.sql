﻿CREATE TABLE [Appointment].[ListAppointment](
	[ListAppointmentId] [int] IDENTITY(1,1) NOT NULL,
	[ListId] [int] NOT NULL,
	[Description] [varchar](50) NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[IsBreak] [bit] NOT NULL CONSTRAINT [DF_Appointment_ListAppointment_IsBreak]  DEFAULT ((0)),
	[SlotStatusId] [tinyint] NOT NULL,
	[StaffId] [int] NULL
) ON [PRIMARY]

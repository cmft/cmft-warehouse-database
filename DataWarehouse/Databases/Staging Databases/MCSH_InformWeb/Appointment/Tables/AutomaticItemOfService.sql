﻿CREATE TABLE [Appointment].[AutomaticItemOfService](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[ItemOfServiceId] [int] NOT NULL,
	[Rules] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [Admin].[MedicalConditionType](
	[MedicalConditionTypeId] [int] IDENTITY(1,1) NOT NULL,
	[MedicalConditionTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_MedicalConditionType_MedicalConditionTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Gender] [tinyint] NOT NULL,
	[MedicalConditionGroupId] [int] NOT NULL,
	[UKMECId] [int] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_MedicalConditionType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[Title](
	[TitleId] [tinyint] IDENTITY(1,1) NOT NULL,
	[TitleGuid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](35) NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

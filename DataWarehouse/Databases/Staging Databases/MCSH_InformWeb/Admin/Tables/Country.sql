﻿CREATE TABLE [Admin].[Country](
	[CountryId] [smallint] IDENTITY(1,1) NOT NULL,
	[CountryGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Country_CountryGuid]  DEFAULT (newid()),
	[Code] [char](3) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[OrderIndex] [tinyint] NULL CONSTRAINT [DF_Admin_Country_OrderIndex]  DEFAULT ((0)),
	[CodeISO] [char](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Country_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[Ethnicity](
	[EthnicityId] [smallint] IDENTITY(1,1) NOT NULL,
	[EthnicityGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Ethnicity_EthnicityGuid]  DEFAULT (newid()),
	[Code] [char](1) NOT NULL,
	[Name] [varchar](50) NOT NULL
) ON [PRIMARY]

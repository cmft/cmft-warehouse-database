﻿CREATE TABLE [Admin].[Room](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[RoomGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Room_RoomGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](255) NULL,
	[Code] [varchar](10) NULL,
	[LocationId] [int] NULL,
	[RoomTypeId] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Room_IsActive]  DEFAULT ((1)),
	[IsDefault] [bit] NOT NULL CONSTRAINT [DF_Admin_Room_IsDefault]  DEFAULT ((0))
) ON [PRIMARY]

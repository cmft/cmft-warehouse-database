﻿CREATE TABLE [Admin].[ReferralOrganisation](
	[ReferralOrganisationId] [smallint] IDENTITY(1,1) NOT NULL,
	[ReferralOrganisationGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ReferralOrganisation_ReferralOrganisationGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ReferralOrganisation_IsActive]  DEFAULT ((1)),
	[SRHADId] [int] NULL,
	[ReferralDirection] [tinyint] NOT NULL CONSTRAINT [DF_Admin_ReferralOrganisation_ReferralDirection]  DEFAULT ((0))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[NCSPServiceType](
	[NCSPServiceTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PatientRecord_NCSPTreatmentDetails_Guid]  DEFAULT (newid()),
	[Name] [varchar](255) NULL,
	[Code] [varchar](5) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_PatientRecord_NCSPTreatmentDetails_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

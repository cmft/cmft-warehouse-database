﻿CREATE TABLE [Admin].[Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Role_RoleGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](200) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Role_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

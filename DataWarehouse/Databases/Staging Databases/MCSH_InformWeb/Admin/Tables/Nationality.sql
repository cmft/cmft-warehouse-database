﻿CREATE TABLE [Admin].[Nationality](
	[NationalityId] [smallint] IDENTITY(1,1) NOT NULL,
	[NationalityGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Nationality_NationalityGuid]  DEFAULT (newid()),
	[Code] [char](1) NULL,
	[Name] [varchar](50) NOT NULL,
	[OrderIndex] [tinyint] NULL CONSTRAINT [DF_Admin_Nationality_OrderIndex]  DEFAULT ((0))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[ClinicAttendanceType](
	[ClinicId] [int] NOT NULL,
	[AttendanceTypeId] [tinyint] NOT NULL,
	[MaximumAllowed] [tinyint] NOT NULL CONSTRAINT [DF_Admin_ClinicAttendanceType_MaximumAllowed]  DEFAULT ((0))
) ON [PRIMARY]

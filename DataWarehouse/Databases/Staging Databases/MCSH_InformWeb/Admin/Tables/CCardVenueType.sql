﻿CREATE TABLE [Admin].[CCardVenueType](
	[CCardVenueTypeId] [smallint] IDENTITY(1,1) NOT NULL,
	[CCardVenueTypeGuid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

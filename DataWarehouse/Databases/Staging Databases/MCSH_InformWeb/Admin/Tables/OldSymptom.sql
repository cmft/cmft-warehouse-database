﻿CREATE TABLE [Admin].[OldSymptom](
	[SymptomId] [tinyint] IDENTITY(1,1) NOT NULL,
	[SymptomGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Symptom_SymptomGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Symptom_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

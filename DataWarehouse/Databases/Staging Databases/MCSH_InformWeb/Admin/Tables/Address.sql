﻿CREATE TABLE [Admin].[Address](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[BuildingNameNumber] [varchar](35) NULL,
	[NumberStreetRoad] [varchar](35) NULL,
	[Village] [varchar](35) NULL,
	[Town] [varchar](35) NULL,
	[County] [varchar](35) NULL,
	[Postcode] [varchar](8) NULL,
	[Telephone] [varchar](35) NULL
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[ExaminationType](
	[ExaminationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ExaminationTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ExaminationType_ExaminationTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Gender] [tinyint] NOT NULL,
	[ExaminationGroupId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ExaminationType_IsActive]  DEFAULT ((1)),
	[OrderIndex] [smallint] NULL
) ON [PRIMARY]

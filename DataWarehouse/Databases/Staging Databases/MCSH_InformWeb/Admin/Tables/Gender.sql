﻿CREATE TABLE [Admin].[Gender](
	[GenderId] [tinyint] NOT NULL,
	[GenderGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Gender_GenderGuid]  DEFAULT (newid()),
	[Name] [varchar](15) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Gender_IsActive]  DEFAULT ((1)),
	[PatientFriendlyName] [varchar](200) NULL
) ON [PRIMARY]

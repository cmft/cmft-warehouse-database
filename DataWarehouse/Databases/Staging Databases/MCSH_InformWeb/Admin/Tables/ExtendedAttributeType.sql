﻿CREATE TABLE [Admin].[ExtendedAttributeType](
	[ExtendedAttributeTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Entity] [varchar](100) NOT NULL,
	[Attribute] [varchar](100) NOT NULL,
	[ReferenceEntity] [varchar](100) NULL
) ON [PRIMARY]

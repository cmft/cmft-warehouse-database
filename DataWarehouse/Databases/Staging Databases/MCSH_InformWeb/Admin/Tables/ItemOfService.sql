﻿CREATE TABLE [Admin].[ItemOfService](
	[ItemOfServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ItemOfServiceGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ItemOfService_ItemOfServiceGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[SlotLength] [tinyint] NOT NULL,
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_Admin_ItemOfService_Gender]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ItemsOfService_IsActive]  DEFAULT ((1)),
	[CareTemplateUrl] [varchar](255) NULL,
	[OrderIndex] [tinyint] NOT NULL CONSTRAINT [DF_Admin_ItemOfService_OrderIndex]  DEFAULT ((1)),
	[PermissionKey] [varchar](10) NULL
) ON [PRIMARY]

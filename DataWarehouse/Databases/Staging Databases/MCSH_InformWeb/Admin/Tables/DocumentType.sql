﻿CREATE TABLE [Admin].[DocumentType](
	[DocumentTypeId] [tinyint] NOT NULL,
	[DocumentTypeGuid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](6) NULL,
	[Gender] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

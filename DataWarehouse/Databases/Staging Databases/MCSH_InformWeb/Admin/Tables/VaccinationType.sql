﻿CREATE TABLE [Admin].[VaccinationType](
	[VaccinationTypeId] [smallint] IDENTITY(1,1) NOT NULL,
	[VaccinationGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Vaccination_VaccinationGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Vaccination_IsActive]  DEFAULT ((1)),
	[Gender] [tinyint] NULL
) ON [PRIMARY]

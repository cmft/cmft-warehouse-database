﻿CREATE TABLE [Admin].[ServiceDefinedRuleCode](
	[ServiceDefinedRuleCodeId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceDefinedRuleId] [int] NOT NULL,
	[SourceId] [int] NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[IsDiagnosis] [bit] NOT NULL,
	[IsSHHAPT] [bit] NOT NULL,
	[IsSRHAD] [bit] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[SexualOrientationType](
	[SexualOrientationTypeId] [smallint] IDENTITY(1,1) NOT NULL,
	[SexualOrientationTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_SexualOrientationType_SexualOrientationTypeGuid]  DEFAULT (newid()),
	[Code] [char](3) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_SexualOrientationType_IsActive]  DEFAULT ((1)),
	[PatientFriendlyName] [varchar](200) NULL
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[EducationType](
	[EducationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[EducationTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Template_EducationType_EducationTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](50) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Template_EducationType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[DrugSupplyType](
	[DrugSupplyTypeId] [smallint] IDENTITY(1,1) NOT NULL,
	[DrugSupplyTypeGuid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT ((1)),
	[IsTreatment] [bit] NOT NULL,
	[IsPrescription] [bit] NOT NULL
) ON [PRIMARY]
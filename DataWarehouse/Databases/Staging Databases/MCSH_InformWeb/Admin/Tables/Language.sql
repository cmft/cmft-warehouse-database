﻿CREATE TABLE [Admin].[Language](
	[LanguageId] [smallint] IDENTITY(1,1) NOT NULL,
	[LanguageGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Language_LanguageGuid]  DEFAULT (newid()),
	[Code] [char](3) NOT NULL,
	[Name] [varchar](35) NOT NULL
) ON [PRIMARY]
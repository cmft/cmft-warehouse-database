﻿CREATE TABLE [Admin].[ReligionType](
	[ReligionTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[ReligionTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Template_ReligionType_ReligionTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Template_ReligionType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[MedicalConditionDetailType](
	[MedicalConditionDetailTypeId] [int] IDENTITY(1,1) NOT NULL,
	[MedicalConditionTypeId] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT ((1)),
	[MedicalConditionDetailTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_MedicalConditionDetailType_MedicalConditionDetailTypeGuid]  DEFAULT (newid()),
	[UkmecChc] [varchar](100) NULL,
	[UkmecPop] [varchar](100) NULL,
	[UkmecCu] [varchar](100) NULL,
	[UkmecDmpa] [varchar](100) NULL,
	[UkmecImp] [varchar](100) NULL,
	[UkmecPo] [varchar](100) NULL,
	[UkmecLng] [varchar](100) NULL,
	[UkmecCoil] [varchar](100) NULL
) ON [PRIMARY]

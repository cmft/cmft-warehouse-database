﻿CREATE TABLE [Admin].[ContraceptionMethod](
	[ContraceptionMethodId] [tinyint] IDENTITY(1,1) NOT NULL,
	[ContraceptionMethodGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ContraceptionMethod_ContraceptionMethodGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[CanBeCurrent] [bit] NOT NULL,
	[CanBePast] [bit] NOT NULL,
	[CanHaveComplications] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ContraceptionMethod_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

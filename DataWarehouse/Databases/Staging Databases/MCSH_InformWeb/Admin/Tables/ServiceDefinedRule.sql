﻿CREATE TABLE [Admin].[ServiceDefinedRule](
	[ServiceDefinedRuleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[RuleType] [varchar](100) NOT NULL,
	[Rules] [varchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [Admin].[OldContraceptionType](
	[ContraceptionTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[ContraceptionTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ContraceptionType_ContraceptionTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ContraceptionType_IsActive]  DEFAULT ((1)),
	[Gender] [tinyint] NULL
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[Occupation](
	[OccupationId] [int] IDENTITY(1,1) NOT NULL,
	[OccupationGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Occupation_OccupationGuid]  DEFAULT (newid()),
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Occupation_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

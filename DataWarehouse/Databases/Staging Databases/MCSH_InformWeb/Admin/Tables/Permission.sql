﻿CREATE TABLE [Admin].[Permission](
	[PermissionId] [int] IDENTITY(1,1) NOT NULL,
	[PermissionGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Permission_PermissionGuid]  DEFAULT (newid()),
	[PermissionKey] [varchar](10) NOT NULL,
	[ParentPermissionId] [int] NULL,
	[Name] [varchar](100) NOT NULL,
	[IsParent] [bit] NOT NULL CONSTRAINT [DF_Admin_Permission]  DEFAULT ((0))
) ON [PRIMARY]

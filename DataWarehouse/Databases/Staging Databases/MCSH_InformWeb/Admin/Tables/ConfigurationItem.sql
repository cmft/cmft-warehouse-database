﻿CREATE TABLE [Admin].[ConfigurationItem](
	[ConfigurationItemId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Group] [varchar](200) NOT NULL,
	[SubGroup] [varchar](200) NULL,
	[LookupKey] [varchar](200) NULL,
	[NameToDisplay] [varchar](200) NOT NULL,
	[IsValueEditable] [bit] NOT NULL DEFAULT ((1)),
	[IsCodeEditable] [bit] NOT NULL DEFAULT ((1)),
	[IsHighlightable] [bit] NOT NULL DEFAULT ((1)),
	[Controller] [varchar](200) NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
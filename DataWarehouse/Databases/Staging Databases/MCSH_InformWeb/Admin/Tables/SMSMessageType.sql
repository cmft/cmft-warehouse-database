﻿CREATE TABLE [Admin].[SMSMessageType](
	[SMSMessageTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Message] [varchar](300) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_SMSMessageType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

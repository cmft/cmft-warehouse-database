﻿CREATE TABLE [Admin].[OldPartnerContactStatus](
	[PartnerContactStatusId] [tinyint] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_PartnerContactStatus_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](6) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_PartnerContactStatus_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

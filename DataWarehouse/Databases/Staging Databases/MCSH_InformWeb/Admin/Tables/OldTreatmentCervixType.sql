﻿CREATE TABLE [Admin].[OldTreatmentCervixType](
	[TreatmentCervixTypeId] [tinyint] NOT NULL,
	[TreatmentCervixGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_TreatmentCervixType_TreatmentCervixTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_TreatmentCervixType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[DrugType](
	[DrugTypeId] [tinyint] NOT NULL,
	[DrugTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_TestGroup_DrugTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](6) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_DrugType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

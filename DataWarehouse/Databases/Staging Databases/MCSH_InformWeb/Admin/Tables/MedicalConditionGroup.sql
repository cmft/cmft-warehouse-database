﻿CREATE TABLE [Admin].[MedicalConditionGroup](
	[MedicalConditionGroupId] [int] NOT NULL,
	[MedicalConditionGroupGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_MedicalConditionGroup_MedicalConditionGroupGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](6) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_MedicalConditionGroup_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

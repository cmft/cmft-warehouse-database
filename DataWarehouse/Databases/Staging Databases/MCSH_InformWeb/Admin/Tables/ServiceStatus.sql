﻿CREATE TABLE [Admin].[ServiceStatus](
	[ServiceStatusId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceStatusGuid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[ServiceName] [varchar](50) NOT NULL,
	[HeartbeatTime] [datetime] NOT NULL
) ON [PRIMARY]
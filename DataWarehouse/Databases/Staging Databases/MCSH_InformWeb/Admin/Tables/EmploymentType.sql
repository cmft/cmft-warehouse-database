﻿CREATE TABLE [Admin].[EmploymentType](
	[EmploymentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_EmploymentType_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_EmploymentType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

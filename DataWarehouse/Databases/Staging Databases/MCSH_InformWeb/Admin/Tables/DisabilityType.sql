﻿CREATE TABLE [Admin].[DisabilityType](
	[DisabilityTypeId] [int] IDENTITY(1,1) NOT NULL,
	[DisabilityTypeGuid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [varchar](255) NULL,
	[Code] [varchar](10) NULL,
	[IsActive] [bit] NULL DEFAULT ((1))
) ON [PRIMARY]
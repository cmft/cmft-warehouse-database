﻿CREATE TABLE [Admin].[LocationType](
	[LocationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[LocationTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_LocationType_LocationTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [char](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_LocationType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

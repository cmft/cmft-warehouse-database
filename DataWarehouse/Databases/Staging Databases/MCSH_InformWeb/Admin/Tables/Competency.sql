﻿CREATE TABLE [Admin].[Competency](
	[CompetencyId] [int] IDENTITY(1,1) NOT NULL,
	[CompetencyGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Competency_CompetencyGuid]  DEFAULT (newid()),
	[CompetencyKey] [char](6) NOT NULL,
	[ParentCompetencyId] [int] NULL,
	[Name] [varchar](100) NOT NULL,
	[IsParent] [bit] NOT NULL CONSTRAINT [DF_Admin_Competency]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[SessionType](
	[SessionTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[SessionTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_SessionType_SessionTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_SessionType_IsActive]  DEFAULT ((1)),
	[CareContactUrl] [varchar](255) NULL,
	[SRHAD] [bit] NOT NULL CONSTRAINT [DF_Admin_SessionType_SRHAD]  DEFAULT ((0)),
	[GUMCAD] [bit] NOT NULL CONSTRAINT [DF_Admin_SessionType_GUMCAD]  DEFAULT ((0))
) ON [PRIMARY]

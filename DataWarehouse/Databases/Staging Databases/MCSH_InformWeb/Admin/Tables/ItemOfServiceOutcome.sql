﻿CREATE TABLE [Admin].[ItemOfServiceOutcome](
	[ItemOfServiceOutcomeId] [int] IDENTITY(1,1) NOT NULL,
	[ItemOfServiceOutcomeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ItemOfServiceOutcome_ItemOfServiceOutcomeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ItemOfServiceOutcome_IsActive]  DEFAULT ((1)),
	[ItemOfServiceId] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL CONSTRAINT [DF_Admin_ItemOfServiceOutcome_IsDefault]  DEFAULT ((0))
) ON [PRIMARY]

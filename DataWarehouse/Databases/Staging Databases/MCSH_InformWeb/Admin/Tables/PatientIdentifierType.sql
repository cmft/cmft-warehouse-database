﻿CREATE TABLE [Admin].[PatientIdentifierType](
	[PatientIdentifierTypeId] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[Education](
	[EducationId] [int] IDENTITY(1,1) NOT NULL,
	[EducationGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Education_EducationGuid]  DEFAULT (newid()),
	[Name] [varchar](150) NOT NULL,
	[Code] [varchar](3) NULL,
	[EducationTypeId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Education_IsActive]  DEFAULT ((1)),
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_Admin_Education_Gender]  DEFAULT ((0))
) ON [PRIMARY]

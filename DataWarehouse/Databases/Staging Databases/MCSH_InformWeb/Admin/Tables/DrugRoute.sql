﻿CREATE TABLE [Admin].[DrugRoute](
	[DrugRouteId] [smallint] IDENTITY(1,1) NOT NULL,
	[DrugRouteGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_DrugRoute_DrugRouteGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_DrugRoute_IsActive]  DEFAULT ((1)),
	[IsTreatment] [bit] NOT NULL DEFAULT ((1)),
	[IsPrescription] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

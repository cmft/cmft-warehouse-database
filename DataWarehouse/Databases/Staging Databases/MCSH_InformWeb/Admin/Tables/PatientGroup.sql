﻿CREATE TABLE [Admin].[PatientGroup](
	[PatientGroupId] [tinyint] IDENTITY(1,1) NOT NULL,
	[PatientGroupGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_PatientGroup_PatientGroupGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](10) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_PatientGroup_IsActive]  DEFAULT ((1)),
	[IsMale] [bit] NOT NULL CONSTRAINT [DF_Admin_PatientGroup_IsMale]  DEFAULT ((0)),
	[IsFemale] [bit] NOT NULL CONSTRAINT [DF_Admin_PatientGroup_IsFemale]  DEFAULT ((0)),
	[IsYoungPerson] [bit] NOT NULL CONSTRAINT [DF_Admin_PatientGroup_IsYoungPerson]  DEFAULT ((0))
) ON [PRIMARY]

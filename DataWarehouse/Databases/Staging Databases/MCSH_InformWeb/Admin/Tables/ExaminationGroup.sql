﻿CREATE TABLE [Admin].[ExaminationGroup](
	[ExaminationGroupId] [int] NOT NULL,
	[ExaminationGroupGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ExaminationGroup_ExaminationGroupGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](6) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ExaminationGroup_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[FamilyHistoryConditionDetail](
	[FamilyHistoryConditionDetailId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_FamilyHistoryConditionDetail_Guid]  DEFAULT (newid()),
	[FamilyHistoryConditionId] [int] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UkmecChc] [varchar](100) NULL,
	[UkmecPop] [varchar](100) NULL,
	[UkmecCu] [varchar](100) NULL,
	[UkmecDmpa] [varchar](100) NULL,
	[UkmecImp] [varchar](100) NULL,
	[UkmecPo] [varchar](100) NULL,
	[UkmecLng] [varchar](100) NULL,
	[UkmecCoil] [varchar](100) NULL
) ON [PRIMARY]

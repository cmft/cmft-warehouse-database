﻿CREATE TABLE [Admin].[DrugFrequency](
	[DrugFrequencyId] [smallint] IDENTITY(1,1) NOT NULL,
	[DrugFrequencyGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_DrugFrequency_DrugFrequencyGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_DrugFrequency_IsActive]  DEFAULT ((1)),
	[IsTreatment] [bit] NOT NULL DEFAULT ((1)),
	[IsPrescription] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

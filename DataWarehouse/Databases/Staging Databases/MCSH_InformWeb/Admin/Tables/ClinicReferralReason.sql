﻿CREATE TABLE [Admin].[ClinicReferralReason](
	[ClinicReferralReasonId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ClinicTriageOption_Guid]  DEFAULT (newid()),
	[ClinicId] [int] NOT NULL,
	[IsAvailable] [bit] NOT NULL,
	[IsHighRisk] [bit] NOT NULL,
	[ReferralReasonId] [smallint] NULL
) ON [PRIMARY]

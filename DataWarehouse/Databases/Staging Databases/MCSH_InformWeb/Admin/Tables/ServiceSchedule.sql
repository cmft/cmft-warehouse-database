﻿CREATE TABLE [Admin].[ServiceSchedule](
	[ServiceScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceScheduleGuid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[ServiceName] [varchar](50) NOT NULL,
	[ServiceType] [varchar](50) NOT NULL,
	[ScheduledTime] [int] NULL,
	[LastScheduledRun] [datetime] NULL,
	[LastManualRun] [datetime] NULL,
	[AllowManualTrigger] [bit] NOT NULL,
	[ManualTrigger] [bit] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]
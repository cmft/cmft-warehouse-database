﻿CREATE TABLE [Admin].[DirtyCache](
	[DirtyCacheId] [int] IDENTITY(1,1) NOT NULL,
	[CacheKey] [varchar](100) NOT NULL,
	[CacheName] [varchar](50) NOT NULL
) ON [PRIMARY]

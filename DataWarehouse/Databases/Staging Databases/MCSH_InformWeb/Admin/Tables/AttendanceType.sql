﻿CREATE TABLE [Admin].[AttendanceType](
	[AttendanceTypeId] [tinyint] NOT NULL,
	[AttendanceTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_AttendanceType_AttendanceTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](10) NULL,
	[SlotLength] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_AttendanceType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

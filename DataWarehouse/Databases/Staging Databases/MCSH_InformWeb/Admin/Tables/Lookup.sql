﻿CREATE TABLE [Admin].[Lookup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Lookup_Guid]  DEFAULT (newid()),
	[Type] [varchar](255) NOT NULL,
	[ParentLookupId] [int] NULL,
	[Value] [varchar](255) NULL,
	[Code] [varchar](255) NULL,
	[AdditionalMetaData] [xml] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Lookup_IsActive]  DEFAULT ((1)),
	[OrderIndex] [int] NULL,
	[IsDefault] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

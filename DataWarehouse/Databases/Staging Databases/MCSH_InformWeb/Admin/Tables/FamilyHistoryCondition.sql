﻿CREATE TABLE [Admin].[FamilyHistoryCondition](
	[FamilyHistoryConditionId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_FamilyHistoryCondition_Guid]  DEFAULT (newid()),
	[Name] [varchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

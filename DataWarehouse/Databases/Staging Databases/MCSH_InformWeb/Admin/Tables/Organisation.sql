﻿CREATE TABLE [Admin].[Organisation](
	[OrganisationId] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Organisation_OrganisationGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](200) NULL,
	[OrganisationCode] [varchar](8) NULL,
	[ROCode] [char](3) NULL,
	[HACode] [char](3) NULL,
	[ParentOrganisationId] [int] NULL,
	[AddressId] [int] NULL,
	[OpenDate] [datetime] NOT NULL,
	[CloseDate] [datetime] NULL,
	[IsOpen] [bit] NOT NULL CONSTRAINT [DF_Admin_Organisation_IsOpen]  DEFAULT ((1)),
	[OrganisationTypeId] [int] NULL,
	[PCTCode] [varchar](8) NOT NULL CONSTRAINT [DF_Admin_Organisation_PCTCode]  DEFAULT ('UNK'),
	[CCGCode] [varchar](3) NULL
) ON [PRIMARY]

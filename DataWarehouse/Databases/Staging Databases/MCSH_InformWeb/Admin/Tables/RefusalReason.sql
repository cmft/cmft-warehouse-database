﻿CREATE TABLE [Admin].[RefusalReason](
	[RefusalReasonId] [smallint] IDENTITY(1,1) NOT NULL,
	[RefusalReasonGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_RefusalReason_RefusalReasonGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_RefusalReason_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

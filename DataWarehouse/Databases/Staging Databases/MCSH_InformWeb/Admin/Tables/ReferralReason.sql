﻿CREATE TABLE [Admin].[ReferralReason](
	[ReferralReasonId] [smallint] IDENTITY(1,1) NOT NULL,
	[ReferralReasonGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ReferralReason_ReferralReasonGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_Admin_ReferralReason_Gender]  DEFAULT ((0)),
	[ReferralDirection] [tinyint] NOT NULL CONSTRAINT [DF_Admin_ReferralReason_ReferralDirection]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ReferralReason_IsActive]  DEFAULT ((1)),
	[ReferralOrganisationId] [smallint] NULL,
	[SRHADId] [int] NULL
) ON [PRIMARY]

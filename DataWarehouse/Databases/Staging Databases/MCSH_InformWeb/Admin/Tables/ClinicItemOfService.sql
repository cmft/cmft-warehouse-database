﻿CREATE TABLE [Admin].[ClinicItemOfService](
	[ClinicId] [int] NOT NULL,
	[ItemOfServiceId] [int] NOT NULL,
	[MaximumAllowed] [tinyint] NOT NULL CONSTRAINT [DF_Admin_ClinicItemOfService_MaximumAllowed]  DEFAULT ((0))
) ON [PRIMARY]

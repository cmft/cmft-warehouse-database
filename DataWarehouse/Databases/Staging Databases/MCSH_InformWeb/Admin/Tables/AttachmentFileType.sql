﻿CREATE TABLE [Admin].[AttachmentFileType](
	[AttachmentFileTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [varchar](255) NOT NULL,
	[Extension] [varchar](6) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_AttachmentFileType_IsActive]  DEFAULT ((0))
) ON [PRIMARY]

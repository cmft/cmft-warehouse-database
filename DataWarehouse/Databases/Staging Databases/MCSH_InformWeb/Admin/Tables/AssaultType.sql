﻿CREATE TABLE [Admin].[AssaultType](
	[AssaultTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_AssaultType_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_AssaultType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[SexualOrientation](
	[SexualOrientationId] [tinyint] NOT NULL,
	[SexualOrientationGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_SexualOrientation_SexualOrientationGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](6) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_SexualOrientation_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

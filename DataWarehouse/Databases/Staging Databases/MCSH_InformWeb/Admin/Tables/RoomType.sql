﻿CREATE TABLE [Admin].[RoomType](
	[RoomTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[RoomTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_RoomType_RoomGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_RoomType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

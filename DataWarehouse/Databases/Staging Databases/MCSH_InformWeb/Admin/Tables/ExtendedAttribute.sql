﻿CREATE TABLE [Admin].[ExtendedAttribute](
	[ExtendedAttributeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[EntityId] [int] NOT NULL,
	[ExtendedAttributeTypeId] [int] NOT NULL,
	[Value] [varchar](100) NOT NULL,
	[ReferenceEntityId] [int] NULL
) ON [PRIMARY]

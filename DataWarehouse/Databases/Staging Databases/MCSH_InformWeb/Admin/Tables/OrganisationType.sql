﻿CREATE TABLE [Admin].[OrganisationType](
	[OrganisationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_OrganisationType_OrganisationTypeGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [char](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_OrganisationType_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

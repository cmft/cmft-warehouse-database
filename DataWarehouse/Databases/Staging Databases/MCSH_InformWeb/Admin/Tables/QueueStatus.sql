﻿CREATE TABLE [Admin].[QueueStatus](
	[QueueStatusId] [int] IDENTITY(1,1) NOT NULL,
	[QueueStatusGuid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Name] [varchar](50) NOT NULL,
	[Status] [bit] NOT NULL,
	[MessageCount] [int] NOT NULL DEFAULT ((0)),
	[LastMessageReceived] [datetime] NULL,
	[NumberOfUnprocessedMessages] [int] NOT NULL DEFAULT ((0)),
	[TotalNumberOfFailures] [int] NOT NULL DEFAULT ((0)),
	[NumberOfFailuresInLast24H] [int] NOT NULL DEFAULT ((0)),
	[TotalNumberOfSuccesses] [int] NOT NULL DEFAULT ((0)),
	[NumberOfSuccessesInLast24H] [int] NOT NULL DEFAULT ((0)),
	[Details] [varchar](1000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Admin].[ClinicFormatType](
	[ClinicFormatTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[ClinicFormatTypeGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ClinicFormatType_ClinicFormatGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ClinicFormatType_IsActive]  DEFAULT ((1)),
	[Code] [varchar](3) NULL
) ON [PRIMARY]
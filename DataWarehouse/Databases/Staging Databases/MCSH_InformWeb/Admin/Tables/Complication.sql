﻿CREATE TABLE [Admin].[Complication](
	[ComplicationId] [tinyint] IDENTITY(1,1) NOT NULL,
	[ComplicationGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Complication_ComplicationGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Complication_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

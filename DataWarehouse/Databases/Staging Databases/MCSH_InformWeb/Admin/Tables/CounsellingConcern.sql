﻿CREATE TABLE [Admin].[CounsellingConcern](
	[ConcernId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Admin].[NCSPSite](
	[NCSPSiteId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NULL,
	[Code] [varchar](10) NULL,
	[NCSPServiceTypeId] [int] NOT NULL,
	[Postcode] [varchar](9) NULL,
	[ManagementOffice] [varchar](255) NULL,
	[ScreeningSite] [bit] NOT NULL,
	[TreatmentSite] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

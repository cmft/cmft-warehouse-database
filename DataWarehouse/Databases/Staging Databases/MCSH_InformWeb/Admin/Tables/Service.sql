﻿CREATE TABLE [Admin].[Service](
	[ServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Service_ServiceGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](10) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_Service_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

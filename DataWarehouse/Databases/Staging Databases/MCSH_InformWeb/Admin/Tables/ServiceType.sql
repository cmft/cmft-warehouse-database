﻿CREATE TABLE [Admin].[ServiceType](
	[ServiceTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ServiceType_Guid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Code] [varchar](3) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_ServiceType_IsActive]  DEFAULT ((1)),
	[OrderIndex] [int] NULL
) ON [PRIMARY]

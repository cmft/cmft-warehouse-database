﻿CREATE TABLE [Admin].[SMSMessageStatus](
	[SMSMessageStatusId] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Admin_SMSMessageStatus_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

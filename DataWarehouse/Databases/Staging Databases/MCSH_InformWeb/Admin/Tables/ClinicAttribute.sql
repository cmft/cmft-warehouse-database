﻿CREATE TABLE [Admin].[ClinicAttribute](
	[ClinicAttributeId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_ClinicAttribute_Guid]  DEFAULT (newid()),
	[ClinicId] [int] NOT NULL,
	[AllowKioskWalkIn] [bit] NOT NULL,
	[AllowMaleGender] [bit] NOT NULL,
	[AllowFemaleGender] [bit] NOT NULL,
	[AllowUnknownGender] [bit] NOT NULL,
	[MinimumAge] [int] NULL,
	[MaximumAge] [int] NULL,
	[AllowNewAttendance] [bit] NOT NULL,
	[AllowRebookAttendance] [bit] NOT NULL,
	[AllowFollowUpAttendance] [bit] NOT NULL
) ON [PRIMARY]

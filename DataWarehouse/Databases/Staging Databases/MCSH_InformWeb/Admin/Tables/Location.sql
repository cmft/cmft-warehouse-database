﻿CREATE TABLE [Admin].[Location](
	[LocationGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Admin_Location_LocationGuid]  DEFAULT (newid()),
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](200) NULL,
	[Code] [varchar](10) NULL,
	[OrganisationId] [int] NOT NULL,
	[LocationTypeId] [int] NULL,
	[AddressId] [int] NULL,
	[OpenDate] [datetime] NOT NULL,
	[CloseDate] [datetime] NULL,
	[IsOpen] [bit] NOT NULL CONSTRAINT [DF_Admin_Location_IsOpen]  DEFAULT ((1)),
	[ParentLocationId] [int] NULL,
	[SiteCode] [varchar](10) NULL,
	[LocationId] [int] IDENTITY(1,1) NOT NULL,
	[OrderIndex] [tinyint] NOT NULL CONSTRAINT [DF_Admin_Location_OrderIndex]  DEFAULT ((1))
) ON [PRIMARY]

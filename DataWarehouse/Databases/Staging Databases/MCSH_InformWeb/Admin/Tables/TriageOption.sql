﻿CREATE TABLE [Admin].[TriageOption](
	[TriageOptionId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](250) NOT NULL
) ON [PRIMARY]

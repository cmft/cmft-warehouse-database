﻿CREATE TABLE [Admin].[CCardVenue](
	[CCardVenueId] [smallint] IDENTITY(1,1) NOT NULL,
	[CCardVenueGuid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[CCardVenueTypeId] [smallint] NOT NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]

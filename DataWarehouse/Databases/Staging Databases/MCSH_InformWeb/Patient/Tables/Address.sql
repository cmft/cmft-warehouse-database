﻿CREATE TABLE [Patient].[Address](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[AddressGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Patient_Address_AddressGuid]  DEFAULT (newid()),
	[PatientId] [int] NOT NULL,
	[AddressType] [tinyint] NOT NULL,
	[AddressLine1] [varchar](35) NULL,
	[AddressLine2] [varchar](35) NULL,
	[AddressLine3] [varchar](35) NULL,
	[AddressLine4] [varchar](35) NULL,
	[AddressLine5] [varchar](35) NULL,
	[Postcode] [varchar](8) NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[HasWritePermission] [bit] NOT NULL CONSTRAINT [DF_Patient_Address_HasWritePermission]  DEFAULT ((0)),
	[UseForCorrespondence] [bit] NOT NULL CONSTRAINT [DF_Patient_Address_UseForCorrespondence]  DEFAULT ((0)),
	[Notes] [varchar](255) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Patient_Address_IsActive]  DEFAULT ((0))
) ON [PRIMARY]

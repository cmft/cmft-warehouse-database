﻿CREATE TABLE [Patient].[CareContact](
	[CareContactId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[AppointmentId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[CreatedByUserRoleId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[ContactRecordType] [tinyint] NOT NULL CONSTRAINT [DF_Patient_CareContact_ContactRecordType]  DEFAULT ((0)),
	[CareTemplateId] [int] NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Patient_CareContact_IsDeleted]  DEFAULT ((0)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL
) ON [PRIMARY]

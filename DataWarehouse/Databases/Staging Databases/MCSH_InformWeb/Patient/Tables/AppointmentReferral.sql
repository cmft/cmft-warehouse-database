﻿CREATE TABLE [Patient].[AppointmentReferral](
	[AppointmentReferralId] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentId] [int] NOT NULL,
	[IsNewReferral] [bit] NOT NULL CONSTRAINT [DF_Patient_AppointmentReferral_IsNewReferral]  DEFAULT ((0)),
	[ReferralId] [int] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [Patient].[ContactPreference](
	[ContactPreferenceId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL
) ON [PRIMARY]

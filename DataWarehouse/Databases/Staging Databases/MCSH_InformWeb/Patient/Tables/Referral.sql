﻿CREATE TABLE [Patient].[Referral](
	[ReferralId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[ReferralDate] [datetime] NULL,
	[ReceivedDate] [datetime] NULL,
	[Notes] [varchar](max) NULL,
	[ReferrerUserId] [int] NULL,
	[ReferralDirection] [tinyint] NOT NULL CONSTRAINT [DF_Patient_Referral_ReferralDirection]  DEFAULT ((0)),
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Patient_Referral_CreatedDateTime]  DEFAULT (getdate()),
	[CreatedByUserRoleId] [int] NOT NULL,
	[UpdatedDateTime] [datetime] NULL CONSTRAINT [DF_Patient_Referral_UpdatedDateTime]  DEFAULT (getdate()),
	[UpdatedByUserRoleId] [int] NULL,
	[ReferralOrganisationId] [smallint] NOT NULL,
	[Version] [tinyint] NOT NULL CONSTRAINT [DF_Patient_Referral_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Patient_Referral_IsDeleted]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

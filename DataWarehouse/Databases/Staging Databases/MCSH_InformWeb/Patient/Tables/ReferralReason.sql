﻿CREATE TABLE [Patient].[ReferralReason](
	[ReferralReasonId] [smallint] NOT NULL,
	[ReferralId] [int] NOT NULL
) ON [PRIMARY]

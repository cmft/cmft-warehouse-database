﻿CREATE TABLE [Patient].[ItemOfService](
	[PatientItemOfServiceId] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Patient_ItemOfService_Guid]  DEFAULT (newid()),
	[ItemOfServiceDate] [datetime] NULL,
	[PatientId] [int] NOT NULL,
	[AppointmentId] [int] NOT NULL,
	[ItemOfServiceId] [int] NULL,
	[ItemOfServiceOutcomeId] [int] NULL,
	[Progress] [tinyint] NULL,
	[Notes] [varchar](255) NULL,
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Patient_ItemOfService_CreatedDate]  DEFAULT (getdate()),
	[CreatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[IsNotRequired] [bit] NOT NULL CONSTRAINT [DF_Patient_ItemOfService_IsNotRequired]  DEFAULT ((0)),
	[Version] [float] NOT NULL CONSTRAINT [DF_Patient_ItemOfService_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Patient_ItemOfService_IsDeleted]  DEFAULT ((0)),
	[RemovedDateTime] [datetime] NULL,
	[RemovedByUserRoleId] [int] NULL
) ON [PRIMARY]

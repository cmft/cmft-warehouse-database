﻿CREATE TABLE [Patient].[Contact](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[ContactGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Patient_Contact_ContactGuid]  DEFAULT (newid()),
	[PatientId] [int] NOT NULL,
	[Details] [varchar](255) NOT NULL,
	[ContactType] [tinyint] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[HasCallPermission] [bit] NOT NULL CONSTRAINT [DF_Patient_Contact_HasCallPermission]  DEFAULT ((0)),
	[HasSMSPermission] [bit] NOT NULL CONSTRAINT [DF_Patient_Contact_HasSMSPermission]  DEFAULT ((0)),
	[HasVMPermission] [bit] NOT NULL CONSTRAINT [DF_Patient_Contact_HasVMPermission]  DEFAULT ((0)),
	[HasEmailPermission] [bit] NOT NULL CONSTRAINT [DF_Patient_Contact_HasEmailPermission]  DEFAULT ((0)),
	[Notes] [varchar](255) NULL,
	[NormalizedDetails] [varchar](max) -- AS (case when [ContactType]<(2) then [Admin].[fnRemoveNonNumericCharacters]([Details]) else [Details] end) PERSISTED
) ON [PRIMARY]

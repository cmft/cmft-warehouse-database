﻿CREATE TABLE [Patient].[Requirement](
	[RequirementId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[IsVulnerable] [tinyint] NOT NULL CONSTRAINT [DF_Patient_Requirement]  DEFAULT ((9)),
	[IsHCPGenderRequired] [tinyint] NOT NULL CONSTRAINT [DF_Person_PatientRequirement_HCPGenderRequired]  DEFAULT ((9)),
	[IsViolent] [bit] NOT NULL CONSTRAINT [DF_Person_PatientRequirement_Violent]  DEFAULT ((0)),
	[IsInterpreterRequired] [bit] NOT NULL CONSTRAINT [DF_Person_PatientRequirement_InterpreterRequired]  DEFAULT ((0)),
	[HCPGenderNotes] [varchar](255) NULL,
	[IsDeaf] [bit] NOT NULL CONSTRAINT [DF_Patient_Requirement_IsDeaf]  DEFAULT ((0)),
	[IsBlind] [bit] NOT NULL CONSTRAINT [DF_Patient_Requirement_IsBlind]  DEFAULT ((0)),
	[IsWheelChairUser] [bit] NOT NULL CONSTRAINT [DF_Patient_Requirement_IsWheelChairUser]  DEFAULT ((0)),
	[HasLearningDisability] [bit] NOT NULL CONSTRAINT [DF_Patient_Requirement_HasLearningDisablity]  DEFAULT ((0))
) ON [PRIMARY]

﻿CREATE TABLE [Patient].[AppointmentContact](
	[AppointmentContactId] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentId] [int] NOT NULL,
	[Details] [varchar](255) NOT NULL,
	[ContactType] [tinyint] NOT NULL,
	[HasCallPermission] [bit] NOT NULL CONSTRAINT [DF_Patient_AppointmentContact_HasCallPermission]  DEFAULT ((0)),
	[HasSMSPermission] [bit] NOT NULL CONSTRAINT [DF_Patient_AppointmentContact_HasSMSPermission]  DEFAULT ((0)),
	[HasVMPermission] [bit] NOT NULL CONSTRAINT [DF_Patient_AppointmentContact_HasVMPermission]  DEFAULT ((0)),
	[HasEmailPermission] [bit] NOT NULL CONSTRAINT [DF_Patient_AppointmentContact_HasEmailPermission]  DEFAULT ((0))
) ON [PRIMARY]

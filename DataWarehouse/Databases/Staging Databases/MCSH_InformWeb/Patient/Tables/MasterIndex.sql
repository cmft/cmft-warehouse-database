﻿CREATE TABLE [Patient].[MasterIndex](
	[PatientId] [int] NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[SearchIndex] [varchar](max) NULL,
	[MasterPatientId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

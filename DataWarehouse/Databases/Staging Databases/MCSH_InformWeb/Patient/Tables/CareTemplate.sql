﻿CREATE TABLE [Patient].[CareTemplate](
	[CareTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[PatientItemOfServiceId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[CreatedByUserRoleId] [int] NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[UpdatedByUserRoleId] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[CareContactId] [int] NULL,
	[Version] [float] NOT NULL CONSTRAINT [DF_Patient_CareTemplate_Version]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Patient_CareTemplate_IsDeleted]  DEFAULT ((0))
) ON [PRIMARY]

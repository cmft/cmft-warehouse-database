﻿CREATE TABLE [Patient].[GP](
	[GPId] [int] IDENTITY(1,1) NOT NULL,
	[GPGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Person_PatientGP_PatientGPGuid]  DEFAULT (newid()),
	[PatientId] [int] NOT NULL,
	[GPCode] [varchar](8) NULL,
	[PracticeCode] [varchar](8) NULL,
	[HasContactPermission] [bit] NOT NULL CONSTRAINT [DF_Person_PatientGP_HasContactPermission]  DEFAULT ((0)),
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[AddressLine1] [varchar](35) NULL,
	[AddressLine2] [varchar](35) NULL,
	[AddressLine3] [varchar](35) NULL,
	[AddressLine4] [varchar](35) NULL,
	[AddressLine5] [varchar](35) NULL,
	[Postcode] [varchar](8) NULL,
	[Telephone] [varchar](12) NULL,
	[GPName] [varchar](100) NULL,
	[PracticeName] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Patient_GP_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

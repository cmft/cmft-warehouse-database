﻿CREATE TABLE [Patient].[Configuration](
	[ConfigurationId] [smallint] IDENTITY(1,1) NOT NULL,
	[PatientNumberMask] [varchar](255) NOT NULL
) ON [PRIMARY]

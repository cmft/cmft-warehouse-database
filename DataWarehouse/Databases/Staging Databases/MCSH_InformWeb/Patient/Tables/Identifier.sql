﻿CREATE TABLE [Patient].[Identifier](
	[IdentifierId] [int] IDENTITY(1,1) NOT NULL,
	[IdentifierGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Person_Identifier_IdentifierGuid]  DEFAULT (newid()),
	[PatientId] [int] NOT NULL,
	[Identifier] [varchar](20) NOT NULL,
	[Description] [varchar](50) NULL,
	[IdentifierTypeId] [tinyint] NOT NULL
) ON [PRIMARY]

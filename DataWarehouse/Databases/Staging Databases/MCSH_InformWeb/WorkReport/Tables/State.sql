﻿CREATE TABLE [WorkReport].[State](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[StateGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_WorkReport_State_StateGuid]  DEFAULT (newid()),
	[ConfigurationId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IsShared] [bit] NOT NULL,
	[OwnerId] [int] NOT NULL,
	[StateXml] [xml] NOT NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [WorkReport].[Configuration](
	[ConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[ConfigurationGuid] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[ReportName] [varchar](100) NOT NULL,
	[GroupName] [varchar](100) NOT NULL,
	[ShowOnHomePage] [bit] NOT NULL,
	[Configuration] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

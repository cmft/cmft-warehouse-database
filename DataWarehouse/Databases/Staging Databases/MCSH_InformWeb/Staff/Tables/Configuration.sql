﻿CREATE TABLE [Staff].[Configuration](
	[ConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[PasswordExpiryAge] [tinyint] NOT NULL,
	[PasswordRetries] [tinyint] NOT NULL,
	[PasswordMinimumLength] [tinyint] NOT NULL,
	[PasswordHistoryLength] [tinyint] NOT NULL
) ON [PRIMARY]

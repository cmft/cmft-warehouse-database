﻿CREATE TABLE [Staff].[PasswordHistory](
	[PasswordHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Password] [varbinary](30) NOT NULL,
	[PasswordChangeDate] [datetime] NULL
) ON [PRIMARY]

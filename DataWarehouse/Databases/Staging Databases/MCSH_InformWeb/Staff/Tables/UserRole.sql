﻿CREATE TABLE [Staff].[UserRole](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[UserRoleGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Staff_UserRole_UserRoleGuid]  DEFAULT (newid()),
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL CONSTRAINT [DF_Staff_UserRole_IsDefault]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Staff_UserRole_IsDeleted]  DEFAULT ((0))
) ON [PRIMARY]

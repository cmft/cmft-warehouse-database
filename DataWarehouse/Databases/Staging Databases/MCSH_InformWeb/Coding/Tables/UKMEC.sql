﻿CREATE TABLE [Coding].[UKMEC](
	[UKMECId] [int] NOT NULL,
	[UKMECGuid] [uniqueidentifier] NOT NULL,
	[DataGroupName] [varchar](100) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[COC] [tinyint] NOT NULL,
	[CHC_I] [tinyint] NOT NULL,
	[CHC_C] [tinyint] NOT NULL,
	[POP_I] [tinyint] NOT NULL,
	[POP_C] [tinyint] NOT NULL,
	[DMPA_NETEN_I] [tinyint] NOT NULL,
	[DMPA_NETEN_C] [tinyint] NOT NULL,
	[IMP_I] [tinyint] NOT NULL,
	[IMP_C] [tinyint] NOT NULL,
	[CUIUD_I] [tinyint] NOT NULL,
	[CUIUD_C] [tinyint] NOT NULL,
	[LNGIUD_I] [tinyint] NOT NULL,
	[LNGIUD_C] [tinyint] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [Coding].[AIDS](
	[AIDSId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](3) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Coding_AIDS_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

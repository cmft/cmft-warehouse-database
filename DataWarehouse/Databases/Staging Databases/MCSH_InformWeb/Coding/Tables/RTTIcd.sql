﻿CREATE TABLE [Coding].[RTTIcd](
	[RTTIcdId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](5) NOT NULL,
	[Name] [varchar](75) NOT NULL,
	[EpisodeTypeLookUpId] [int] NOT NULL,
	[SRHADCode] [varchar](5) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Coding_RTTIcd_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

﻿CREATE TABLE [Coding].[ARV](
	[ARVId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](3) NOT NULL,
	[Name] [varchar](75) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Coding_ARV_IsActive]  DEFAULT ((1))
) ON [PRIMARY]

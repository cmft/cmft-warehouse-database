﻿CREATE TABLE [Coding].[SRHAD](
	[SRHADId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [char](5) NOT NULL,
	[Name] [varchar](70) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Coding_SRHAD_IsActive]  DEFAULT ((0)),
	[IsLocal] [bit] NOT NULL CONSTRAINT [DF_Coding_SHRAD_IsLocal]  DEFAULT ((0)),
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_Coding_SRHAD_Gender]  DEFAULT ((0)),
	[OrderIndex] [smallint] NOT NULL CONSTRAINT [DF_Coding_SRHAD_OrderIndex]  DEFAULT ((0)),
	[LocalCode] [char](5) NULL
) ON [PRIMARY]

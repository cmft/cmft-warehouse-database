﻿CREATE TABLE [Coding].[SHHAPT](
	[SHHAPTId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [char](7) NOT NULL,
	[Name] [varchar](70) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Coding_SHHAPT_IsActive]  DEFAULT ((0)),
	[IsLocal] [bit] NOT NULL CONSTRAINT [DF_Coding_SHHAPT_IsLocal]  DEFAULT ((0)),
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_Coding_SHHAPT_Gender]  DEFAULT ((0)),
	[OrderIndex] [smallint] NOT NULL CONSTRAINT [DF_Coding_SHHAPT_OrderIndex]  DEFAULT ((0)),
	[DefinitionAndGuidance] [varchar](2000) NULL,
	[LocalCode] [char](5) NULL
) ON [PRIMARY]

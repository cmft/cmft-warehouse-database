﻿CREATE TABLE [Coding].[Diagnosis](
	[DiagnosisId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](5) NOT NULL,
	[Name] [varchar](75) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Coding_Diagnosis_IsActive]  DEFAULT ((0)),
	[IsLocal] [bit] NOT NULL CONSTRAINT [DF_Coding_Diagnosis_IsLocal]  DEFAULT ((0)),
	[Gender] [tinyint] NOT NULL CONSTRAINT [DF_Coding_Diagnosis_Gender]  DEFAULT ((0)),
	[OrderIndex] [smallint] NOT NULL CONSTRAINT [DF_Coding_Diagnosis_OrderIndex]  DEFAULT ((0)),
	[DefinitionAndGuidance] [varchar](2000) NULL,
	[LocalCode] [char](5) NULL
) ON [PRIMARY]

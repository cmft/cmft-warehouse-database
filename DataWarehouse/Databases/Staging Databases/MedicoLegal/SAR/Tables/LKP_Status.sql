﻿CREATE TABLE [SAR].[LKP_Status](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusCaption] [nvarchar](30) NULL,
	[NextStatusChangeTrigger] [nvarchar](50) NULL
) ON [PRIMARY]
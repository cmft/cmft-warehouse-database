﻿CREATE TABLE [SAR].[SYS_User](
	[UserID] [int] IDENTITY(200,10) NOT NULL,
	[ADLogin] [nvarchar](100) NULL,
	[CleanName] [nvarchar](150) NULL,
	[ColourPalette] [nvarchar](20) NULL
) ON [PRIMARY]
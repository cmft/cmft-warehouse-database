﻿CREATE TABLE [SAR].[Encounter](
	[SubjectAccessRequestID] [int] IDENTITY(1000,1) NOT NULL,
	[RequestorID] [int] NULL,
	[SubjectID] [int] NULL,
	[RequestDate] [datetime] NULL,
	[StatusID] [int] NULL,
	[RequestorReference] [nvarchar](50) NULL,
	[CategoryID] [int] NULL,
	[TargetDueDate] [date] NULL,
	[RequestMethodID] [int] NULL,
	[RequestPeriodStartDate] [date] NULL,
	[RequestPeriodEndDate] [date] NULL,
	[InvoiceNumber] [nvarchar](50) NULL
) ON [PRIMARY]
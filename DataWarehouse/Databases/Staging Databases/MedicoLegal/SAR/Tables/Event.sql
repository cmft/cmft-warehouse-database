﻿CREATE TABLE [SAR].[Event](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[SubjectAccessRequestID] [int] NULL,
	[EventTime] [datetime] NULL CONSTRAINT [DF_Event_EventTime]  DEFAULT (getdate()),
	[EventName] [nvarchar](50) NULL,
	[ExternallyApplicableFlag] [bit] NOT NULL CONSTRAINT [DF_Event2_ExternallyApplicableFlag]  DEFAULT ((0)),
	[EventUser] [nvarchar](50) NULL,
	[EventArgument] [nvarchar](250) NULL,
	[EventDescription] [nvarchar](250) NULL
) ON [PRIMARY]
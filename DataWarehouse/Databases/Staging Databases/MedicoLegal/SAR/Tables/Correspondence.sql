﻿CREATE TABLE [SAR].[Correspondence](
	[CorrespondenceID] [int] IDENTITY(1,1) NOT NULL,
	[SubjectLine] [nvarchar](999) NULL,
	[SubjectAccessRequestID] [int] NULL,
	[Method] [nvarchar](20) NULL,
	[GenerationTime] [datetime] NULL,
	[DistributionTime] [datetime] NULL,
	[DistributionByID] [int] NULL,
	[Body] [text] NULL,
	[SendingToID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
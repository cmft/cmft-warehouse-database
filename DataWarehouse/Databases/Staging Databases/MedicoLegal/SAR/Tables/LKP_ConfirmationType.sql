﻿CREATE TABLE [SAR].[LKP_ConfirmationType](
	[ConfirmationTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ConfirmationTypeCaption] [nvarchar](255) NULL,
	[CheckTimeIsPreSearchFlag] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [SAR].[Search](
	[SubjectAccessRequestID] [int] NULL,
	[SearchID] [int] IDENTITY(1,1) NOT NULL,
	[SearchTypeID] [int] NULL,
	[SearchInitiatedTime] [datetime] NULL,
	[SearchConcludedTime] [datetime] NULL,
	[SearchOutcomeID] [int] NULL,
	[CopiedTime] [datetime] NULL,
	[PreparedTime] [datetime] NULL,
	[QualityAssuranceTime] [datetime] NULL,
	[DistributionTime] [datetime] NULL,
	[RoyalMailReference] [nvarchar](100) NULL,
	[CurrentHolderID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [SAR].[LKP_SearchOutcome](
	[SearchOutcomeID] [int] IDENTITY(1,1) NOT NULL,
	[SearchOutcomeCaption] [nvarchar](50) NULL,
	[ExternalCaption] [nvarchar](100) NULL
) ON [PRIMARY]
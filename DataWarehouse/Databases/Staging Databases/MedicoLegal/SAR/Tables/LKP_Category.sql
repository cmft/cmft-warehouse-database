﻿CREATE TABLE [SAR].[LKP_Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryCaption] [nvarchar](100) NULL,
	[Priority] [int] NULL,
	[TargetDays] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [SAR].[Confirmation](
	[SubjectAccessRequestID] [int] NULL,
	[ConfirmationID] [int] IDENTITY(1,1) NOT NULL,
	[ConfirmationTypeID] [int] NULL,
	[SoughtDate] [datetime] NULL,
	[ConfirmedDate] [datetime] NULL,
	[ConfirmedByID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [SAR].[LKP_ConfirmationTypeMatrix](
	[InterceptID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[ConfirmationTypeID] [int] NULL
) ON [PRIMARY]
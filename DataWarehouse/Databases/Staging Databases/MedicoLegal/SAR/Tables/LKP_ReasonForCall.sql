﻿CREATE TABLE [SAR].[LKP_ReasonForCall](
	[ReasonForCallID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonForCallCaption] [nvarchar](50) NULL
) ON [PRIMARY]
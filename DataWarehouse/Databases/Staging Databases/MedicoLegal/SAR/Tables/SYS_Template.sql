﻿CREATE TABLE [SAR].[SYS_Template](
	[TemplateID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[TemplateContent] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
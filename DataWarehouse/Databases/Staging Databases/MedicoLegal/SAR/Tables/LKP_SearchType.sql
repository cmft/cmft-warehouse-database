﻿CREATE TABLE [SAR].[LKP_SearchType](
	[SearchTypeID] [int] IDENTITY(1,1) NOT NULL,
	[SearchTypeCaption] [nvarchar](50) NULL,
	[RequiresCopyingFlag] [bit] NULL,
	[DefaultInitialHolderID] [int] NULL
) ON [PRIMARY]
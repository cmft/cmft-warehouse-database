﻿CREATE TABLE [Microsoft.ApplicationServer.DurableInstancing].[AbandonedInstanceControlCommandsTable](
	[Id] [bigint] NOT NULL,
	[InstanceId] [uniqueidentifier] NOT NULL,
	[ServiceIdentifier] [varchar](max) NULL,
	[Type] [tinyint] NOT NULL,
	[ExecutionAttempts] [tinyint] NOT NULL,
	[LastExecutionAttemptAt] [datetime] NULL,
	[LastExecutedOn] [nvarchar](128) NULL,
	[Exception] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Microsoft.ApplicationServer.DurableInstancing].[InstanceControlCommandsTable](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[InstanceId] [uniqueidentifier] NOT NULL,
	[ServiceIdentifier] [varchar](max) NULL,
	[Type] [tinyint] NOT NULL,
	[ExecutionAttempts] [tinyint] NOT NULL,
	[LastExecutionAttemptAt] [datetime] NULL,
	[CurrentMachine] [nvarchar](128) NULL,
	[LockExpiration] [datetime] NULL,
	[Exception] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
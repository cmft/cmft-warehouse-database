﻿CREATE TABLE [System.Activities.DurableInstancing].[LockOwnersTable](
	[Id] [uniqueidentifier] NOT NULL,
	[SurrogateLockOwnerId] [bigint] IDENTITY(1,1) NOT NULL,
	[LockExpiration] [datetime] NOT NULL,
	[WorkflowHostType] [uniqueidentifier] NULL,
	[MachineName] [nvarchar](128) NOT NULL,
	[EnqueueCommand] [bit] NOT NULL,
	[DeletesInstanceOnCompletion] [bit] NOT NULL,
	[PrimitiveLockOwnerData] [varbinary](max) NULL DEFAULT (NULL),
	[ComplexLockOwnerData] [varbinary](max) NULL DEFAULT (NULL),
	[WriteOnlyPrimitiveLockOwnerData] [varbinary](max) NULL DEFAULT (NULL),
	[WriteOnlyComplexLockOwnerData] [varbinary](max) NULL DEFAULT (NULL),
	[EncodingOption] [tinyint] NULL DEFAULT ((0)),
	[WorkflowIdentityFilter] [tinyint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
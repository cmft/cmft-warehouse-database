﻿CREATE TABLE [dbo].[USER_SESSION_ARCHIVE](
	[USESN_PK] [int] NOT NULL,
	[USERR_PK] [int] NOT NULL,
	[SESSION_IDENTIFIER] [varchar](50) NOT NULL,
	[DEVTY_RFVAL] [int] NOT NULL,
	[DEVICE_NAME] [varchar](50) NULL,
	[IP_ADDRESS] [varchar](50) NULL,
	[LOGIN_DTTM] [datetime] NOT NULL,
	[LOGOUT_DTTM] [datetime] NULL,
	[LOTYP_RFVAL] [int] NOT NULL,
	[CREATED_BY_USERR_PK] [int] NOT NULL,
	[CREATED_DTTM] [datetime] NOT NULL,
	[LAST_MODIFIED_BY_USERR_PK] [int] NOT NULL,
	[LAST_MODIFIED_DTTM] [datetime] NOT NULL
) ON [PRIMARY]
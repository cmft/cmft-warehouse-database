﻿CREATE TABLE [dbo].[AGE_RANGE](
	[AGRNG_PK] [int] IDENTITY(1,1) NOT NULL,
	[START_AGE] [int] NULL,
	[END_AGE] [int] NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[START_DTTM] [datetime] NULL,
	[END_DTTM] [datetime] NULL,
	[CREATED_BY_USERR_PK] [int] NULL,
	[CREATED_DTTM] [datetime] NULL,
	[LAST_MODIFIED_USERR_PK] [int] NULL,
	[LAST_MODIFIED_DTTM] [datetime] NULL
) ON [PRIMARY]
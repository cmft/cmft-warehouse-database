﻿CREATE TABLE [dbo].[MYVIEWS_DATASET_INFO](
	[dataIndex] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](100) NOT NULL,
	[dataType] [varchar](10) NOT NULL,
	[filterType] [varchar](10) NOT NULL,
	[filterData] [varchar](100) NULL,
	[parent] [varchar](100) NULL,
	[displayType] [varchar](10) NOT NULL,
	[displayFormat] [int] NOT NULL DEFAULT ((0)),
	[columnFormat] [varchar](100) NULL,
	[width] [int] NULL,
	[useRenderer] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
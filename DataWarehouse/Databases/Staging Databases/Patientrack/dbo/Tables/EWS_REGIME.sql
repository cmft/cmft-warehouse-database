﻿CREATE TABLE [dbo].[EWS_REGIME](
	[EWSRG_PK] [int] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](50) NOT NULL,
	[DESCRIPTION] [varchar](255) NOT NULL,
	[TEMPLATE_FLAG] [bit] NOT NULL,
	[START_DTTM] [datetime] NOT NULL,
	[END_DTTM] [datetime] NULL,
	[DURATION] [int] NULL,
	[DELETED_FLAG] [bit] NOT NULL,
	[CREATED_BY_USERR_PK] [int] NOT NULL,
	[CREATED_DTTM] [datetime] NOT NULL,
	[LAST_MODIFIED_BY_USERR_PK] [int] NOT NULL,
	[LAST_MODIFIED_DTTM] [datetime] NOT NULL,
	[MSSET_PK] [int] NOT NULL,
	[PARENT_EWSRG_PK] [int] NULL
) ON [PRIMARY]
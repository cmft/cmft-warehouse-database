﻿CREATE TABLE [dbo].[IP11](
	[Trust Code] [nvarchar](255) NULL,
	[Q01] [float] NULL,
	[Q02] [nvarchar](255) NULL,
	[Q03] [nvarchar](255) NULL,
	[Q04] [nvarchar](255) NULL,
	[Q05] [nvarchar](255) NULL,
	[Q06] [nvarchar](255) NULL,
	[Q07] [nvarchar](255) NULL,
	[Q08] [nvarchar](255) NULL,
	[Q09] [nvarchar](255) NULL,
	[Q10] [nvarchar](255) NULL,
	[Q11] [nvarchar](255) NULL,
	[Q12] [float] NULL,
	[Q13] [float] NULL,
	[Q14] [float] NULL,
	[Q15] [nvarchar](255) NULL,
	[Q16] [float] NULL,
	[Q17] [nvarchar](255) NULL,
	[Q18] [nvarchar](255) NULL,
	[Q19] [float] NULL,
	[Q20] [nvarchar](255) NULL,
	[Q21] [nvarchar](255) NULL,
	[Q22] [float] NULL,
	[Q23] [float] NULL,
	[Q24] [float] NULL,
	[Q25] [float] NULL,
	[Q26] [float] NULL,
	[Q27] [float] NULL,
	[Q28] [float] NULL,
	[Q29] [float] NULL,
	[Q30] [float] NULL,
	[Q31] [float] NULL,
	[Q32] [float] NULL,
	[Q33] [float] NULL,
	[Q34] [float] NULL,
	[Q35] [float] NULL,
	[Q36] [float] NULL,
	[Q37] [float] NULL,
	[Q38] [float] NULL,
	[Q39] [float] NULL,
	[Q40] [float] NULL,
	[Q41] [float] NULL,
	[Q42] [float] NULL,
	[Q43] [float] NULL,
	[Q44] [float] NULL,
	[Q45] [float] NULL,
	[Q46] [float] NULL,
	[Q47] [float] NULL,
	[Q48] [float] NULL,
	[Q49] [float] NULL,
	[Q50] [nvarchar](255) NULL,
	[Q51] [float] NULL,
	[Q52] [nvarchar](255) NULL,
	[Q53] [nvarchar](255) NULL,
	[Q54] [nvarchar](255) NULL,
	[Q55] [nvarchar](255) NULL,
	[Q56] [nvarchar](255) NULL,
	[Q57] [nvarchar](255) NULL,
	[Q58] [nvarchar](255) NULL,
	[Q59] [float] NULL,
	[Q60] [float] NULL,
	[Q61] [nvarchar](255) NULL,
	[Q62] [nvarchar](255) NULL,
	[Q63] [float] NULL,
	[Q64] [float] NULL,
	[Q65] [float] NULL,
	[Q66] [float] NULL,
	[Q67] [float] NULL,
	[Q68] [float] NULL,
	[Q69] [float] NULL,
	[Q70] [float] NULL,
	[Q71] [float] NULL,
	[Q72] [nvarchar](255) NULL,
	[Q73] [float] NULL,
	[Q74] [float] NULL,
	[Q75] [float] NULL,
	[Q76] [float] NULL,
	[Q77] [float] NULL,
	[Q78] [float] NULL,
	[Q79_1] [float] NULL,
	[Q79_2] [float] NULL,
	[Q79_3] [float] NULL,
	[Q79_4] [float] NULL,
	[Q79_5] [float] NULL,
	[Q79_6] [float] NULL,
	[Q79_7] [float] NULL,
	[Q80_1] [float] NULL,
	[Q80_2] [float] NULL,
	[Q80_3] [float] NULL,
	[Q80_4] [float] NULL,
	[Q80_5] [float] NULL,
	[Q80_6] [float] NULL,
	[Q80_7] [float] NULL,
	[Q80_8] [float] NULL,
	[Q81] [float] NULL,
	[Q82] [float] NULL,
	[Q83] [float] NULL,
	[Q84] [float] NULL,
	[Q85] [float] NULL,
	[Other comments] [nvarchar](255) NULL,
	[Other comments1] [nvarchar](255) NULL,
	[Other comments2] [nvarchar](255) NULL
) ON [PRIMARY]
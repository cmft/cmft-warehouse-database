﻿CREATE TABLE [Clarity].[tmp_AntiBase_AutomatedDataDump](
	[CK1_ExtractDate] [nvarchar](100) NOT NULL,
	[CK2_PatientEncounter] [bigint] NOT NULL,
	[CK5_DischargePeriod] [nvarchar](6) NOT NULL,
	[antibioticDrugID] [int] NOT NULL,
	[antibioticDateTime] [datetime] NOT NULL,
	[DrugName] [nvarchar](500) NOT NULL,
	[ROUTE] [nvarchar](500) NOT NULL,
	[AdministrationType] [nvarchar](500) NOT NULL
) ON [PRIMARY]
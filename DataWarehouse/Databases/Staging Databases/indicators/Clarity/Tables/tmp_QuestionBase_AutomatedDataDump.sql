﻿CREATE TABLE [Clarity].[tmp_QuestionBase_AutomatedDataDump](
	[CK6_QuestionID] [bigint] NOT NULL,
	[CK1_ExtractDate] [nvarchar](100) NOT NULL,
	[CK4_CFAID] [bigint] NOT NULL,
	[CK5_DischargePeriod] [nvarchar](6) NOT NULL,
	[Question] [nvarchar](max) NOT NULL,
	[AssureMDEIdentifier] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
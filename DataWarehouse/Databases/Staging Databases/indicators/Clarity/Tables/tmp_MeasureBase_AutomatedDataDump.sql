﻿CREATE TABLE [Clarity].[tmp_MeasureBase_AutomatedDataDump](
	[CK1_ExtractDate] [nvarchar](100) NOT NULL,
	[CK2_PatientEncounter] [bigint] NOT NULL,
	[CK3_PatientEncounterCFAID] [bigint] NOT NULL,
	[CK4_CFAID] [bigint] NOT NULL,
	[CK5_DischargePeriod] [nvarchar](6) NOT NULL,
	[siteCode] [nvarchar](50) NOT NULL,
	[MeasureShortTitle] [nvarchar](max) NOT NULL,
	[MeasureDescription] [nvarchar](max) NOT NULL,
	[MeasurePassStatus] [nvarchar](max) NOT NULL,
	[MeasureOutcomeReason] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Clarity].[tmp_QandABase_AutomatedDataDump](
	[CK1_ExtractDate] [nvarchar](100) NOT NULL,
	[CK2_PatientEncounter] [bigint] NOT NULL,
	[CK3_PatientEncounterCFAID] [bigint] NOT NULL,
	[CK4_CFAID] [bigint] NOT NULL,
	[CK5_DischargePeriod] [nvarchar](6) NOT NULL,
	[Answer] [nvarchar](max) NULL,
	[CK6_QuestionID] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [IPSurvey].[IP13_Trust_Section_Scores](
	[Trustcode] [nvarchar](255) NULL,
	[trustnam] [nvarchar](255) NULL,
	[meanS01] [float] NULL,
	[ll95S01] [float] NULL,
	[ul95S01] [float] NULL,
	[bandS01] [float] NULL,
	[meanS02] [float] NULL,
	[ll95S02] [float] NULL,
	[ul95S02] [float] NULL,
	[bandS02] [float] NULL,
	[meanS03] [float] NULL,
	[ll95S03] [float] NULL,
	[ul95S03] [float] NULL,
	[bandS03] [float] NULL,
	[meanS04] [float] NULL,
	[ll95S04] [float] NULL,
	[ul95S04] [float] NULL,
	[bandS04] [float] NULL,
	[meanS05] [float] NULL,
	[ll95S05] [float] NULL,
	[ul95S05] [float] NULL,
	[bandS05] [float] NULL,
	[meanS06] [float] NULL,
	[ll95S06] [float] NULL,
	[ul95S06] [float] NULL,
	[bandS06] [float] NULL,
	[meanS07] [float] NULL,
	[ll95S07] [float] NULL,
	[ul95S07] [float] NULL,
	[bandS07] [float] NULL,
	[meanS08] [float] NULL,
	[ll95S08] [float] NULL,
	[ul95S08] [float] NULL,
	[bandS08] [float] NULL,
	[meanS09] [float] NULL,
	[ll95S09] [float] NULL,
	[ul95S09] [float] NULL,
	[bandS09] [float] NULL,
	[meanS10] [float] NULL,
	[ll95S10] [float] NULL,
	[ul95S10] [float] NULL,
	[bandS10] [float] NULL
) ON [PRIMARY]
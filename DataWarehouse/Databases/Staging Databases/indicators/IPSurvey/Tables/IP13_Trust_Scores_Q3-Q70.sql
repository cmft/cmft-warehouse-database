﻿CREATE TABLE [IPSurvey].[IP13_Trust_Scores_Q3-Q70](
	[Trustcode] [nvarchar](255) NULL,
	[trustnam] [nvarchar](255) NULL,
	[n_tpat] [float] NULL,
	[meanQ3] [float] NULL,
	[ll95Q3] [float] NULL,
	[ul95Q3] [float] NULL,
	[bandQ3] [float] NULL,
	[meanQ4] [float] NULL,
	[ll95Q4] [float] NULL,
	[ul95Q4] [float] NULL,
	[bandQ4] [float] NULL,
	[meanQ6] [float] NULL,
	[ll95Q6] [float] NULL,
	[ul95Q6] [float] NULL,
	[bandQ6] [float] NULL,
	[meanQ7] [float] NULL,
	[ll95Q7] [float] NULL,
	[ul95Q7] [float] NULL,
	[bandQ7] [float] NULL,
	[meanQ8] [float] NULL,
	[ll95Q8] [float] NULL,
	[ul95Q8] [float] NULL,
	[bandQ8] [float] NULL,
	[meanQ9] [float] NULL,
	[ll95Q9] [float] NULL,
	[ul95Q9] [float] NULL,
	[bandQ9] [float] NULL,
	[meanQ11] [float] NULL,
	[ll95Q11] [float] NULL,
	[ul95Q11] [float] NULL,
	[bandQ11] [float] NULL,
	[meanQ14] [float] NULL,
	[ll95Q14] [float] NULL,
	[ul95Q14] [float] NULL,
	[bandQ14] [float] NULL,
	[meanQ15] [float] NULL,
	[ll95Q15] [float] NULL,
	[ul95Q15] [float] NULL,
	[bandQ15] [float] NULL,
	[meanQ16] [float] NULL,
	[ll95Q16] [float] NULL,
	[ul95Q16] [float] NULL,
	[bandQ16] [float] NULL,
	[meanQ17] [float] NULL,
	[ll95Q17] [float] NULL,
	[ul95Q17] [float] NULL,
	[bandQ17] [float] NULL,
	[meanQ18] [float] NULL,
	[ll95Q18] [float] NULL,
	[ul95Q18] [float] NULL,
	[bandQ18] [float] NULL,
	[meanQ19] [float] NULL,
	[ll95Q19] [float] NULL,
	[ul95Q19] [float] NULL,
	[bandQ19] [float] NULL,
	[meanQ20] [float] NULL,
	[ll95Q20] [float] NULL,
	[ul95Q20] [float] NULL,
	[bandQ20] [float] NULL,
	[meanQ21] [float] NULL,
	[ll95Q21] [float] NULL,
	[ul95Q21] [float] NULL,
	[bandQ21] [float] NULL,
	[meanQ22] [float] NULL,
	[ll95Q22] [float] NULL,
	[ul95Q22] [float] NULL,
	[bandQ22] [float] NULL,
	[meanQ23] [float] NULL,
	[ll95Q23] [float] NULL,
	[ul95Q23] [float] NULL,
	[bandQ23] [float] NULL,
	[meanQ24] [float] NULL,
	[ll95Q24] [float] NULL,
	[ul95Q24] [float] NULL,
	[bandQ24] [float] NULL,
	[meanQ25] [float] NULL,
	[ll95Q25] [float] NULL,
	[ul95Q25] [float] NULL,
	[bandQ25] [float] NULL,
	[meanQ26] [float] NULL,
	[ll95Q26] [float] NULL,
	[ul95Q26] [float] NULL,
	[bandQ26] [float] NULL,
	[meanQ27] [float] NULL,
	[ll95Q27] [float] NULL,
	[ul95Q27] [float] NULL,
	[bandQ27] [float] NULL,
	[meanQ28] [float] NULL,
	[ll95Q28] [float] NULL,
	[ul95Q28] [float] NULL,
	[bandQ28] [float] NULL,
	[meanQ29] [float] NULL,
	[ll95Q29] [float] NULL,
	[ul95Q29] [float] NULL,
	[bandQ29] [float] NULL,
	[meanQ30] [float] NULL,
	[ll95Q30] [float] NULL,
	[ul95Q30] [float] NULL,
	[bandQ30] [float] NULL,
	[meanQ31] [float] NULL,
	[ll95Q31] [float] NULL,
	[ul95Q31] [float] NULL,
	[bandQ31] [float] NULL,
	[meanQ32] [float] NULL,
	[ll95Q32] [float] NULL,
	[ul95Q32] [float] NULL,
	[bandQ32] [float] NULL,
	[meanQ33] [float] NULL,
	[ll95Q33] [float] NULL,
	[ul95Q33] [float] NULL,
	[bandQ33] [float] NULL,
	[meanQ34] [float] NULL,
	[ll95Q34] [float] NULL,
	[ul95Q34] [float] NULL,
	[bandQ34] [float] NULL,
	[meanQ35] [float] NULL,
	[ll95Q35] [float] NULL,
	[ul95Q35] [float] NULL,
	[bandQ35] [float] NULL,
	[meanQ36] [float] NULL,
	[ll95Q36] [float] NULL,
	[ul95Q36] [float] NULL,
	[bandQ36] [float] NULL,
	[meanQ37] [float] NULL,
	[ll95Q37] [float] NULL,
	[ul95Q37] [float] NULL,
	[bandQ37] [float] NULL,
	[meanQ39] [float] NULL,
	[ll95Q39] [float] NULL,
	[ul95Q39] [float] NULL,
	[bandQ39] [float] NULL,
	[meanQ40] [float] NULL,
	[ll95Q40] [float] NULL,
	[ul95Q40] [float] NULL,
	[bandQ40] [float] NULL,
	[meanQ42] [float] NULL,
	[ll95Q42] [float] NULL,
	[ul95Q42] [float] NULL,
	[bandQ42] [float] NULL,
	[meanQ43] [float] NULL,
	[ll95Q43] [float] NULL,
	[ul95Q43] [float] NULL,
	[bandQ43] [float] NULL,
	[meanQ44] [float] NULL,
	[ll95Q44] [float] NULL,
	[ul95Q44] [float] NULL,
	[bandQ44] [float] NULL,
	[meanQ45] [float] NULL,
	[ll95Q45] [float] NULL,
	[ul95Q45] [float] NULL,
	[bandQ45] [float] NULL,
	[meanQ47] [float] NULL,
	[ll95Q47] [float] NULL,
	[ul95Q47] [float] NULL,
	[bandQ47] [float] NULL,
	[meanQ48] [float] NULL,
	[ll95Q48] [float] NULL,
	[ul95Q48] [float] NULL,
	[bandQ48] [float] NULL,
	[meanQ49] [float] NULL,
	[ll95Q49] [float] NULL,
	[ul95Q49] [float] NULL,
	[bandQ49] [float] NULL,
	[meanQ50] [float] NULL,
	[ll95Q50] [float] NULL,
	[ul95Q50] [float] NULL,
	[bandQ50] [float] NULL,
	[meanQ52] [float] NULL,
	[ll95Q52] [float] NULL,
	[ul95Q52] [float] NULL,
	[bandQ52] [float] NULL,
	[meanQ53] [float] NULL,
	[ll95Q53] [float] NULL,
	[ul95Q53] [float] NULL,
	[bandQ53] [float] NULL,
	[meanQ54] [float] NULL,
	[ll95Q54] [float] NULL,
	[ul95Q54] [float] NULL,
	[bandQ54] [float] NULL,
	[meanQ55] [float] NULL,
	[ll95Q55] [float] NULL,
	[ul95Q55] [float] NULL,
	[bandQ55] [float] NULL,
	[meanQ56] [float] NULL,
	[ll95Q56] [float] NULL,
	[ul95Q56] [float] NULL,
	[bandQ56] [float] NULL,
	[meanQ57] [float] NULL,
	[ll95Q57] [float] NULL,
	[ul95Q57] [float] NULL,
	[bandQ57] [float] NULL,
	[meanQ58] [float] NULL,
	[ll95Q58] [float] NULL,
	[ul95Q58] [float] NULL,
	[bandQ58] [float] NULL,
	[meanQ59] [float] NULL,
	[ll95Q59] [float] NULL,
	[ul95Q59] [float] NULL,
	[bandQ59] [float] NULL,
	[meanQ60] [float] NULL,
	[ll95Q60] [float] NULL,
	[ul95Q60] [float] NULL,
	[bandQ60] [float] NULL,
	[meanQ61] [float] NULL,
	[ll95Q61] [float] NULL,
	[ul95Q61] [float] NULL,
	[bandQ61] [float] NULL,
	[meanQ62] [float] NULL,
	[ll95Q62] [float] NULL,
	[ul95Q62] [float] NULL,
	[bandQ62] [float] NULL,
	[meanQ63] [float] NULL,
	[ll95Q63] [float] NULL,
	[ul95Q63] [float] NULL,
	[bandQ63] [float] NULL,
	[meanQ64] [float] NULL,
	[ll95Q64] [float] NULL,
	[ul95Q64] [float] NULL,
	[bandQ64] [float] NULL,
	[meanQ65] [float] NULL,
	[ll95Q65] [float] NULL,
	[ul95Q65] [float] NULL,
	[bandQ65] [float] NULL,
	[meanQ66] [float] NULL,
	[ll95Q66] [float] NULL,
	[ul95Q66] [float] NULL,
	[bandQ66] [float] NULL,
	[meanQ67] [float] NULL,
	[ll95Q67] [float] NULL,
	[ul95Q67] [float] NULL,
	[bandQ67] [float] NULL,
	[meanQ68] [float] NULL,
	[ll95Q68] [float] NULL,
	[ul95Q68] [float] NULL,
	[bandQ68] [float] NULL,
	[meanQ69] [float] NULL,
	[ll95Q69] [float] NULL,
	[ul95Q69] [float] NULL,
	[bandQ69] [float] NULL,
	[meanQ70] [float] NULL,
	[ll95Q70] [float] NULL,
	[ul95Q70] [float] NULL,
	[bandQ70] [float] NULL
) ON [PRIMARY]
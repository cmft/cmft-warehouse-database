﻿CREATE TABLE [dbo].[SmChannel](
	[channelId] [int] IDENTITY(1,1) NOT NULL,
	[channelType] [int] NOT NULL DEFAULT ((-1)),
	[languageType] [int] NOT NULL DEFAULT ((-1)),
	[arfType] [int] NOT NULL DEFAULT ((-1)),
	[channelName] [nvarchar](127) NULL DEFAULT (''),
	[dataVersion] [int] NOT NULL DEFAULT ((1)),
	[bandwidth] [nvarchar](127) NULL DEFAULT (''),
	[resolution] [nvarchar](127) NULL DEFAULT (''),
	[description] [nvarchar](1023) NULL DEFAULT (''),
	[guid] [nvarchar](40) NOT NULL
) ON [PRIMARY]
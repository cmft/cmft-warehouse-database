﻿CREATE TABLE [dbo].[SmUser](
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[userName] [nvarchar](50) NOT NULL,
	[adminRights] [bit] NULL DEFAULT ((0)),
	[recognitionRights] [bit] NULL DEFAULT ((0)),
	[excludeFromATR] [bit] NULL DEFAULT ((0)),
	[userPassword] [nvarchar](24) NULL DEFAULT (''),
	[userFullName] [nvarchar](255) NULL DEFAULT (''),
	[gender] [int] NULL DEFAULT ((-1)),
	[dataVersion] [int] NOT NULL DEFAULT ((1)),
	[deleteVersion] [int] NOT NULL DEFAULT ((0)),
	[hashValue] [int] NOT NULL DEFAULT ((-1)),
	[checkOutTimeStamp] [int] NULL DEFAULT ((-1)),
	[checkOutTaskId] [int] NULL DEFAULT ((-1))
) ON [PRIMARY]
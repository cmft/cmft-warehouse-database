﻿CREATE TABLE [dbo].[SmLanguage](
	[languageId] [int] IDENTITY(1,1) NOT NULL,
	[languageType] [int] NOT NULL DEFAULT ((-1)),
	[languageName] [nvarchar](127) NOT NULL,
	[dataVersion] [int] NOT NULL DEFAULT ((1)),
	[guid] [nvarchar](40) NOT NULL
) ON [PRIMARY]
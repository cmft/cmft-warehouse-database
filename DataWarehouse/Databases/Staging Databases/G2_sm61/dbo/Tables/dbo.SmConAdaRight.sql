﻿CREATE TABLE [dbo].[SmConAdaRight](
	[userId] [int] NOT NULL DEFAULT ((-1)),
	[conTextId] [int] NOT NULL DEFAULT ((-1)),
	[conAdaAllRights] [bit] NULL DEFAULT ((0)),
	[dataVersion] [int] NOT NULL DEFAULT ((1)),
	[deleteVersion] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SmWorkstation](
	[workstationId] [int] IDENTITY(1,1) NOT NULL,
	[workstationName] [nvarchar](50) NOT NULL,
	[description] [nvarchar](255) NULL DEFAULT (''),
	[recognitionServiceState] [int] NULL DEFAULT ((-1)),
	[purgeDaemonServiceState] [int] NULL DEFAULT ((-1)),
	[dispatcherServiceState] [int] NULL DEFAULT ((-1)),
	[conAdaServiceState] [int] NULL DEFAULT ((-1)),
	[osNameOfPC] [nvarchar](255) NULL DEFAULT (''),
	[smVersionInfo] [nvarchar](50) NULL DEFAULT (''),
	[dataVersion] [int] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
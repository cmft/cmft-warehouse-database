﻿CREATE TABLE [dbo].[SmSystem](
	[databaseVersion] [smallint] NOT NULL,
	[updateVersion] [smallint] NOT NULL DEFAULT ((0)),
	[enableUserInstallation] [bit] NULL DEFAULT ((1)),
	[enablePasswordChange] [bit] NULL DEFAULT ((1)),
	[systemRoot] [nvarchar](1023) NULL DEFAULT (''),
	[systemId] [nvarchar](39) NULL DEFAULT ('000000000000000000000000000000000000000'),
	[systemName] [nvarchar](50) NULL DEFAULT (''),
	[databasePatch] [smallint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
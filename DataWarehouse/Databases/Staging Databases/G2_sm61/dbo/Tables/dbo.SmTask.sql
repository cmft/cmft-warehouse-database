﻿CREATE TABLE [dbo].[SmTask](
	[taskDBId] [int] IDENTITY(1,1) NOT NULL,
	[taskType] [int] NOT NULL DEFAULT ((-1)),
	[appType] [int] NOT NULL DEFAULT ((-1)),
	[workstationId] [int] NOT NULL DEFAULT ((-1)),
	[conTextId] [int] NOT NULL DEFAULT ((-1)),
	[dictationId] [int] NOT NULL DEFAULT ((-1)),
	[userId] [int] NOT NULL DEFAULT ((-1)),
	[languageType] [int] NOT NULL DEFAULT ((-1)),
	[channelId] [int] NOT NULL DEFAULT ((-1)),
	[processId] [int] NOT NULL DEFAULT ((-1)),
	[appPath] [nvarchar](260) NULL DEFAULT (''),
	[description] [nvarchar](64) NULL DEFAULT (''),
	[startTime] [int] NOT NULL DEFAULT ((0)),
	[lastHbTime] [int] NOT NULL DEFAULT ((0)),
	[lastBCDataFlush] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
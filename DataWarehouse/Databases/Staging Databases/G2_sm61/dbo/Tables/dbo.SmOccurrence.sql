﻿CREATE TABLE [dbo].[SmOccurrence](
	[unknownWordId] [int] NOT NULL DEFAULT ((-1)),
	[dictationId] [int] NOT NULL DEFAULT ((-1)),
	[unknownWordPosition] [int] NOT NULL DEFAULT ((-1)),
	[conTextId] [int] NOT NULL DEFAULT ((-1)),
	[userId] [int] NOT NULL DEFAULT ((-1)),
	[dictationName] [nvarchar](255) NULL DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SmUnknownGrammarItem](
	[unknownGrammarItemId] [int] IDENTITY(1,1) NOT NULL,
	[writtenAs] [nvarchar](255) NOT NULL,
	[phraseSegmentType] [int] NOT NULL DEFAULT ((-1)),
	[className] [nvarchar](50) NOT NULL,
	[conTextId] [int] NOT NULL DEFAULT ((-1)),
	[originalWrittenAs] [nvarchar](255) NOT NULL,
	[fromContextMerge] [bit] NOT NULL DEFAULT ((0)),
	[existingGrammarItem] [bit] NOT NULL DEFAULT ((0)),
	[creationTime] [int] NULL DEFAULT ((-1))
) ON [PRIMARY]
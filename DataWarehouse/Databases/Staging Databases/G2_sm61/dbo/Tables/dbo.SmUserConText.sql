﻿CREATE TABLE [dbo].[SmUserConText](
	[userConTextId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL DEFAULT ((-1)),
	[conTextId] [int] NOT NULL DEFAULT ((-1)),
	[uwDetectMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[ugiDetectMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[lmAdaptationMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[uwDroppedMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[ugiDroppedMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[lmDroppedAdaptationMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[lmImportTime] [int] NULL DEFAULT ((-1)),
	[calculateRcgStatistics] [bit] NULL DEFAULT ((0)),
	[dataVersion] [int] NOT NULL DEFAULT ((1)),
	[deleteVersion] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SmUnknownWord](
	[unknownWordId] [int] IDENTITY(1,1) NOT NULL,
	[grapheme] [nvarchar](50) NOT NULL,
	[specialWord] [bit] NULL DEFAULT ((0)),
	[phoneticDescription] [nvarchar](255) NULL DEFAULT (''),
	[conTextId] [int] NOT NULL DEFAULT ((-1)),
	[bglexWord] [bit] NULL DEFAULT ((0)),
	[unknownWordStatus] [int] NULL DEFAULT ((-1)),
	[creationTime] [int] NULL DEFAULT ((-1)),
	[cacheLexWord] [bit] NULL DEFAULT ((0)),
	[guiltySegmentType] [int] NULL DEFAULT ((0)),
	[originalGrapheme] [nvarchar](50) NULL DEFAULT (''),
	[fromContextMerge] [bit] NULL DEFAULT ((0)),
	[similarWordCount] [int] NULL DEFAULT ((0)),
	[favoriteSimilarWord] [nvarchar](255) NULL DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SmDataVersion](
	[tableType] [int] NOT NULL DEFAULT ((0)),
	[updateVersion] [int] NULL DEFAULT ((0)),
	[deleteVersion] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[BCFileNames](
	[FileNameClass] [nvarchar](128) NOT NULL,
	[FileNameUsage] [nvarchar](128) NOT NULL,
	[FileNameData] [nvarchar](128) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FileVersion] [nvarchar](40) NOT NULL DEFAULT (''),
	[ChangeIndex] [nvarchar](20) NOT NULL DEFAULT (''),
	[SyncInfo] [nvarchar](128) NULL DEFAULT (''),
	[CreationTime] [int] NULL DEFAULT ((-1))
) ON [PRIMARY]
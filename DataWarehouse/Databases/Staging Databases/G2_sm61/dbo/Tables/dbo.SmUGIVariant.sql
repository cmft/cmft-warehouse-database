﻿CREATE TABLE [dbo].[SmUGIVariant](
	[ugiVariantId] [int] IDENTITY(1,1) NOT NULL,
	[unknownGrammarItemId] [int] NOT NULL DEFAULT ((-1)),
	[spokenAs] [nvarchar](511) NOT NULL,
	[ugiVariantStatus] [int] NOT NULL DEFAULT ((-1)),
	[containsUnknownWords] [bit] NOT NULL DEFAULT ((0)),
	[fromContextMerge] [bit] NOT NULL DEFAULT ((0)),
	[creationTime] [int] NULL DEFAULT ((-1)),
	[originalSpokenAs] [nvarchar](255) NOT NULL DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SmConText](
	[conTextId] [int] IDENTITY(1,1) NOT NULL,
	[conTextName] [nvarchar](127) NOT NULL,
	[languageType] [int] NOT NULL DEFAULT ((-1)),
	[doReorganisation] [bit] NULL DEFAULT ((1)),
	[cacheLexiconId] [int] NULL DEFAULT ((-1)),
	[conTextImportTime] [int] NULL DEFAULT ((-1)),
	[productDomainId] [int] NULL DEFAULT ((-1)),
	[productId] [int] NULL DEFAULT ((-1)),
	[conTextVersion] [int] NULL DEFAULT ((-1)),
	[guid] [nvarchar](40) NOT NULL,
	[dataVersion] [int] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
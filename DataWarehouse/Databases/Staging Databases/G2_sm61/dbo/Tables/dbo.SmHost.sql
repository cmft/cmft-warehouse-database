﻿CREATE TABLE [dbo].[SmHost](
	[hostId] [int] IDENTITY(1,1) NOT NULL,
	[hostName] [nvarchar](63) NOT NULL,
	[dllDriver] [nvarchar](255) NOT NULL,
	[hostType] [int] NULL DEFAULT ((-1)),
	[dataVersion] [int] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
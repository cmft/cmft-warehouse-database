﻿CREATE TABLE [dbo].[SmCacheLexicon](
	[cacheLexiconId] [int] IDENTITY(1,1) NOT NULL,
	[cacheLexiconName] [nvarchar](127) NOT NULL,
	[languageType] [int] NOT NULL DEFAULT ((-1)),
	[guid] [nvarchar](40) NOT NULL,
	[defaultCacheLexicon] [bit] NULL DEFAULT ((1)),
	[dataVersion] [int] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SmUGIVariantOccurrence](
	[ugiVariantId] [int] NOT NULL DEFAULT ((-1)),
	[unknownGrammarItemId] [int] NOT NULL DEFAULT ((-1)),
	[dictationId] [int] NOT NULL DEFAULT ((-1)),
	[ugiVariantPosition] [int] NOT NULL DEFAULT ((-1)),
	[conTextId] [int] NOT NULL DEFAULT ((-1)),
	[userId] [int] NOT NULL DEFAULT ((-1)),
	[dictationName] [nvarchar](255) NULL DEFAULT ('')
) ON [PRIMARY]
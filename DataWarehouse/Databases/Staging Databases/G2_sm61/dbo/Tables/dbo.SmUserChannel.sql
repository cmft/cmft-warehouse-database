﻿CREATE TABLE [dbo].[SmUserChannel](
	[userChannelId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL DEFAULT ((-1)),
	[channelId] [int] NOT NULL DEFAULT ((-1)),
	[acousticAdaptationMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[iTrainAcoustAdaptMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[unsupervisedAAMaterial] [decimal](19, 0) NULL DEFAULT ((0)),
	[arfImportStartTime] [int] NULL DEFAULT ((-1)),
	[arfImportEndTime] [int] NULL DEFAULT ((-1)),
	[checkOutTimeStamp] [int] NULL DEFAULT ((-1)),
	[checkOutTaskId] [int] NULL DEFAULT ((-1)),
	[dataVersion] [int] NOT NULL DEFAULT ((1)),
	[deleteVersion] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
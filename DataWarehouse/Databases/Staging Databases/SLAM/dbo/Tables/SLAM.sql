﻿CREATE TABLE [dbo].[SLAM](
	[Extract_Year] [nvarchar](255) NULL,
	[Extract_Month] [nvarchar](255) NULL,
	[Extract_Type] [nvarchar](255) NULL,
	[DatasetCode] [varchar](30) NOT NULL,
	[MergeEncounterRecno] [varchar](50) NOT NULL,
	[Pod_Code] [nvarchar](50) NULL,
	[Activity_Actual] [float] NOT NULL,
	[Price_Actual] [float] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ExtractDates](
	[Month] [float] NULL,
	[Type] [nvarchar](255) NULL,
	[Date] [datetime] NULL
) ON [PRIMARY]
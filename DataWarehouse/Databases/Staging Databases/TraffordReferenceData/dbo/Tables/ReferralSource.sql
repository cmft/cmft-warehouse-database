﻿CREATE TABLE [dbo].[ReferralSource](
	[refsource_id] [uniqueidentifier] NOT NULL,
	[refsource_code] [varchar](8) NULL,
	[refsource_name] [varchar](40) NOT NULL,
	[org_id] [uniqueidentifier] NOT NULL,
	[refsource_identity] [int] NOT NULL
) ON [PRIMARY]
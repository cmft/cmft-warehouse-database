﻿CREATE TABLE [dbo].[AEDiagnoses](
	[DiagnosesCode] [int] NOT NULL,
	[Diagnoses] [varchar](80) NOT NULL,
	[DiagnosesNationalCode] [varchar](2) NULL
) ON [PRIMARY]
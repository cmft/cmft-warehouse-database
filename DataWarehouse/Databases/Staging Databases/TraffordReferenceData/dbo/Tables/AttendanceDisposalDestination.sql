﻿CREATE TABLE [dbo].[AttendanceDisposalDestination](
	[AttendanceDisposalDestinationCode] [int] NOT NULL,
	[AttendanceDisposalDestination] [varchar](255) NULL,
	[AttendanceDisposalDestinationNationalCode] [varchar](10) NULL
) ON [PRIMARY]
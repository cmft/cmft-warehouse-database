﻿CREATE TABLE [dbo].[AEStaffList](
	[StaffMemberCode] [varchar](30) NOT NULL,
	[StaffType] [varchar](30) NULL,
	[StaffSurnameName] [varchar](35) NULL,
	[StaffForenameName] [varchar](35) NULL,
	[stf_suspended] [bit] NULL
) ON [PRIMARY]
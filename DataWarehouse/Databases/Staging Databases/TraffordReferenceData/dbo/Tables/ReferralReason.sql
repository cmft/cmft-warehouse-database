﻿CREATE TABLE [dbo].[ReferralReason](
	[answer_id] [uniqueidentifier] NOT NULL,
	[ReferralReason] [varchar](1024) NULL,
	[ServiceTypeRequestedCode] [int] NULL,
	[ReferralReasonID] [int] NULL
) ON [PRIMARY]
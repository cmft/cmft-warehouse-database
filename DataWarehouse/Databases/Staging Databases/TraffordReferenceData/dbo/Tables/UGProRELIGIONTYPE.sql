﻿CREATE TABLE [dbo].[UGProRELIGIONTYPE](
	[RELIGIONID] [int] NOT NULL,
	[RELIGIONCode] [varchar](8) NULL,
	[RELIGIONDescription] [varchar](40) NULL
) ON [PRIMARY]
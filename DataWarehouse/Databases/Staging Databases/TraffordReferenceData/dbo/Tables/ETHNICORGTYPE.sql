﻿CREATE TABLE [dbo].[ETHNICORGTYPE](
	[ETHORG_ID] [int] NOT NULL,
	[ETHORG_CODE] [varchar](8) NULL,
	[ETHORG_NAME] [varchar](40) NULL,
	[ETHORG_DELETED] [bit] NOT NULL,
	[RowId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AEPresentingProblem](
	[PresentingProblemCode] [int] NOT NULL,
	[PresentingProblem] [varchar](80) NOT NULL,
	[PresentingProblemNationalCode] [varchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[INFODEPT_cab_consultants](
	[sds_code] [varchar](50) NOT NULL,
	[cons_code] [varchar](50) NOT NULL,
	[surname] [varchar](50) NOT NULL,
	[MainSpecialtyCode] [varchar](3) NULL,
	[TreatmentFunction] [int] NULL,
	[InUse] [int] NOT NULL
) ON [PRIMARY]
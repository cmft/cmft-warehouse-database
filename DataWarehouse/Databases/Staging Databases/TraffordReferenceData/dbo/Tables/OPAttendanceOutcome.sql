﻿CREATE TABLE [dbo].[OPAttendanceOutcome](
	[AttendanceOutcomeCode] [int] NOT NULL,
	[AttendanceOutcome] [varchar](1024) NULL,
	[NationalAttendanceOutcomeCode] [varchar](20) NULL
) ON [PRIMARY]
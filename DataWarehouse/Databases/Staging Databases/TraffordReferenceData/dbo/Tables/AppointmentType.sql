﻿CREATE TABLE [dbo].[AppointmentType](
	[apptype_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[org_id] [uniqueidentifier] NOT NULL,
	[apptype_localid] [int] NOT NULL,
	[apptype_code] [varchar](8) NOT NULL,
	[apptype_name] [varchar](40) NULL,
	[apptype_relid] [varchar](20) NULL,
	[apptype_identity] [int] NOT NULL
) ON [PRIMARY]
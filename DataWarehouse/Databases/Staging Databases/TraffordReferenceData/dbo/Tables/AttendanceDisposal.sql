﻿CREATE TABLE [dbo].[AttendanceDisposal](
	[AttendanceDisposalCode] [int] NOT NULL,
	[AttendanceDisposal] [varchar](255) NULL,
	[AttendanceDisposalNationalCode] [varchar](10) NULL,
	[InUse] [int] NULL,
	[Deleted] [int] NULL
) ON [PRIMARY]
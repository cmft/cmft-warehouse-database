﻿CREATE view [dbo].[Consultant]

as

/* 
	DG 13.1.14 - View needed as we can either have the sds_code or cons_code in the activity tables. View unions 
	both coded columns if available so will not run into problems with ReferenceMapv2. Union will elimate duplicates
	if present
	
*/

select
	 ConsultantCode = sds_code
	,Consultant = surname
	,NationalConsultantCode = cons_code
	,ValidNationalConsultantCode = 
									case
										when len(cons_code) = 8
										then 'Y'
										else 'N'
									end									
	,MainSpecialtyCode
	--,InUse
from
	dbo.INFODEPT_cab_consultants Consultant

union 

select 
	 ConsultantCode = cons_code
	,Consultant = surname
	,NationalConsultantCode = cons_code
	,ValidNationalConsultantCode = 
								case
									when len(cons_code) = 8
									then 'Y'
									else 'N'
								end
	,MainSpecialtyCode
	--,InUse
from
	dbo.INFODEPT_cab_consultants Consultant	
where 
	not exists
		(
		select
			1 
		from
			dbo.INFODEPT_cab_consultants NationalConsultantCode
		where
			NationalConsultantCode.cons_code = Consultant.sds_code
		)
and not exists		
		(
		select
			1 
		from
			dbo.INFODEPT_cab_consultants SDSCode
		where
			SDSCode.sds_code = Consultant.cons_code
		)
	
	
	 
	
--order by
--	Consultant
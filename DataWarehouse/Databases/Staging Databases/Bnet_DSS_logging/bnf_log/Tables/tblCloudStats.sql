﻿CREATE TABLE [bnf_log].[tblCloudStats](
	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
	[SourceType] [nvarchar](20) NULL,
	[SourceID] [uniqueidentifier] NULL,
	[SourceName] [nvarchar](200) NULL,
	[Categories] [nvarchar](500) NULL,
	[StatsName] [nvarchar](100) NULL,
	[StatsParam] [nvarchar](200) NULL,
	[ValueString] [nvarchar](max) NULL,
	[ValueFloat] [float] NULL,
	[ValueDateTimeUTC] [datetime] NULL,
	[ValueInt] [int] NULL,
	[SampleTimeUTC] [datetime] NULL,
	[MeasurementSuccess] [char](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
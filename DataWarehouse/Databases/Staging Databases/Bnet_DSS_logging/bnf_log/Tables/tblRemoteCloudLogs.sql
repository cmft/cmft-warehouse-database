﻿CREATE TABLE [bnf_log].[tblRemoteCloudLogs](
	[RecordID] [bigint] IDENTITY(1,1) NOT NULL,
	[SourceType] [nvarchar](20) NULL,
	[SourceID] [uniqueidentifier] NULL,
	[SourceName] [nvarchar](200) NULL,
	[LogData] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
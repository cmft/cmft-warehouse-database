﻿CREATE TABLE [bnf_log].[tblActivityLog](
	[ActivityID] [bigint] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NULL,
	[UserID] [uniqueidentifier] NULL,
	[EntityID] [uniqueidentifier] NULL,
	[ActivityName] [nvarchar](200) NULL,
	[Summary] [nvarchar](2000) NULL,
	[Details] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
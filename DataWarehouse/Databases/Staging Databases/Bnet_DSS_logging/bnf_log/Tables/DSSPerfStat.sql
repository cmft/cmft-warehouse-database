﻿CREATE TABLE [bnf_log].[DSSPerfStat](
	[CloudID] [uniqueidentifier] NOT NULL,
	[CloudName] [varchar](100) NULL,
	[ClientSaveTime] [datetime] NOT NULL,
	[Operation] [varchar](32) NOT NULL,
	[DAFTaskFrequency] [int] NOT NULL,
	[Parameter] [varchar](100) NULL,
	[RunTimeMilliseconds] [int] NULL,
	[ServerSaveTime] [datetime] NOT NULL
) ON [PRIMARY]
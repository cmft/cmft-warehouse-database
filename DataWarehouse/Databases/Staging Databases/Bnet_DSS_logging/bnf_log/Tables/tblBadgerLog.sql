﻿CREATE TABLE [bnf_log].[tblBadgerLog](
	[EventLogID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_tblBadgerLog_EventLogID]  DEFAULT (newid()),
	[EventTimestamp] [datetime] NOT NULL CONSTRAINT [DF_tblBadgerLog_EventTimestamp]  DEFAULT (getutcdate()),
	[UserID] [uniqueidentifier] NULL,
	[WorkstationID] [uniqueidentifier] NULL,
	[SessionID] [uniqueidentifier] NULL,
	[EventSource] [nvarchar](50) NOT NULL,
	[ReferenceID] [uniqueidentifier] NULL,
	[LogName] [nvarchar](50) NOT NULL,
	[Summary] [nvarchar](250) NOT NULL,
	[EventXml] [nvarchar](max) NOT NULL,
	[Severity] [int] NOT NULL CONSTRAINT [DF_tblBadgerLog_Severity]  DEFAULT ((0)),
	[RowID] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
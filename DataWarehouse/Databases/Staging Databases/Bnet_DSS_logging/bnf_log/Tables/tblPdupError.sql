﻿CREATE TABLE [bnf_log].[tblPdupError](
	[PdupId] [uniqueidentifier] NOT NULL,
	[PdupGroupId] [uniqueidentifier] NOT NULL,
	[PdupXml] [xml] NOT NULL,
	[PdupPrevId] [uniqueidentifier] NOT NULL,
	[PdupEntityId] [uniqueidentifier] NOT NULL,
	[PdupWorkstationId] [uniqueidentifier] NOT NULL,
	[PdupUserId] [uniqueidentifier] NOT NULL,
	[PdupEntityType] [nvarchar](50) NULL,
	[PdupDigId] [nvarchar](250) NULL,
	[PdupFormSaveTime] [datetime] NULL,
	[PdupEventTime] [datetime] NOT NULL,
	[PdupIndex] [int] NOT NULL,
	[PdupTotal] [int] NOT NULL,
	[PdupLogicallyDelete] [bit] NOT NULL,
	[PdupCount] [int] NOT NULL,
	[PdupLastEventTime] [datetime] NOT NULL,
	[EventLogXml] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
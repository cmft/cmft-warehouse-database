﻿CREATE TABLE [bnf_log].[tblSqlLog](
	[SqlQueryId] [bigint] IDENTITY(1,1) NOT NULL,
	[Query] [nvarchar](max) NOT NULL,
	[ExecutionDateTimeUTC] [datetime] NOT NULL,
	[SqlException] [nvarchar](max) NULL,
	[Duration] [int] NOT NULL,
	[DBGenericMethodName] [nvarchar](20) NOT NULL,
	[CallingMethodName] [nvarchar](2000) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
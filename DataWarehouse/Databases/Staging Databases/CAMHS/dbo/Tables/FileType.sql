﻿CREATE TABLE [dbo].[FileType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileType] [varchar](50) NOT NULL
) ON [PRIMARY]
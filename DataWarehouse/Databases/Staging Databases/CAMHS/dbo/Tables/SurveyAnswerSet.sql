﻿CREATE TABLE [dbo].[SurveyAnswerSet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AnswerSetDesc] [varchar](150) NOT NULL,
	[Payload] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
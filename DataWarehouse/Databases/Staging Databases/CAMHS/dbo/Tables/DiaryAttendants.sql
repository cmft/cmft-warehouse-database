﻿CREATE TABLE [dbo].[DiaryAttendants](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DiaryID] [int] NOT NULL,
	[AttendantID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientAudit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[PK] [int] NOT NULL,
	[TableName] [varchar](100) NULL,
	[FieldName] [varchar](1000) NOT NULL,
	[AuditDate] [datetime] NOT NULL,
	[OldValue] [nvarchar](max) NULL,
	[NewValue] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[DiaryClinicians](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DiaryId] [int] NOT NULL,
	[ClinicianId] [int] NOT NULL,
	[IsPrimaryClinician] [bit] NOT NULL
) ON [PRIMARY]
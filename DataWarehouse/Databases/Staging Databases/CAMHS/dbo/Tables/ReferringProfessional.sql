﻿CREATE TABLE [dbo].[ReferringProfessional](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Professional] [varchar](50) NOT NULL,
	[CategoryID] [int] NULL,
	[OrderBy] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CUsersGroupsPatients](
	[UserGroupPatientsID] [int] IDENTITY(1,1) NOT NULL,
	[UserGroupID] [int] NOT NULL,
	[PsychID] [int] NOT NULL,
	[Active] [bit] NULL CONSTRAINT [DF_CUsersGroupsPatients_Active]  DEFAULT ((1))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Document](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DocTypeID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Filename] [varchar](250) NOT NULL,
	[Key] [varchar](25) NOT NULL,
	[LinkID] [int] NOT NULL,
	[Title] [varchar](100) NOT NULL
) ON [PRIMARY]
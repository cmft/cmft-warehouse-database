﻿CREATE TABLE [dbo].[DiagnosticCriteria](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Criteria] [varchar](100) NULL,
	[OrderBy] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Contact](
	[ContactID] [int] IDENTITY(1,1) NOT NULL,
	[ContactName] [varchar](255) NOT NULL,
	[Address1] [varchar](255) NOT NULL,
	[Address2] [varchar](255) NULL,
	[Address3] [varchar](255) NULL,
	[Address4] [varchar](255) NULL,
	[PostCode] [varchar](50) NULL,
	[Telephone] [varchar](50) NULL,
	[ContactDetails] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Hide] [bit] NULL
) ON [PRIMARY]
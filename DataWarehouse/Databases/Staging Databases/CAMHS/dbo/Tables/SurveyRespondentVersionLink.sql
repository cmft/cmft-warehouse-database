﻿CREATE TABLE [dbo].[SurveyRespondentVersionLink](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RespondentID] [int] NOT NULL,
	[VersionID] [int] NOT NULL
) ON [PRIMARY]
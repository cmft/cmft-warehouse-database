﻿CREATE TABLE [dbo].[IAPTDataMapper](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[CAMHSID] [int] NOT NULL,
	[IAPTValue] [varchar](50) NOT NULL
) ON [PRIMARY]
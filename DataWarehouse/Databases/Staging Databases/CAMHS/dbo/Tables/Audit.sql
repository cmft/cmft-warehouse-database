﻿CREATE TABLE [dbo].[Audit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [varchar](20) NOT NULL,
	[Title] [varchar](150) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[ClinicianID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[AdditionalInfo] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[SurveyVersionExclusion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SurveyID] [int] NOT NULL,
	[VersionID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Answers](
	[PrimaryKeyID] [int] IDENTITY(1,1) NOT NULL,
	[LinkedID] [int] NULL,
	[QuestionID] [int] NULL,
	[AnswerID] [int] NULL,
	[FreText] [varchar](255) NULL
) ON [PRIMARY]
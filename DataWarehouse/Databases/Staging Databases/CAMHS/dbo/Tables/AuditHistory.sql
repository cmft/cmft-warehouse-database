﻿CREATE TABLE [dbo].[AuditHistory](
	[AuditHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[HospitalID] [int] NULL,
	[UserID] [int] NULL,
	[StartDate] [datetime] NULL,
	[TableName] [varchar](30) NULL,
	[AuditDesc] [varchar](255) NULL
) ON [PRIMARY]
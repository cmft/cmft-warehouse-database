﻿CREATE TABLE [dbo].[FileDefinition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileTypeID] [int] NOT NULL,
	[Field] [varchar](50) NOT NULL,
	[Type] [varchar](15) NOT NULL,
	[Size] [int] NULL,
	[Mandatory] [bit] NOT NULL,
	[DataField] [varchar](50) NOT NULL,
	[MandatoryOverrideField] [varchar](50) NULL,
	[SortOrder] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PsychEnhancedRisk](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PsychID] [int] NOT NULL,
	[EnhancedRiskID] [int] NOT NULL
) ON [PRIMARY]
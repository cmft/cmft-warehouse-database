﻿CREATE TABLE [dbo].[QuestionMap](
	[PrimaryKeyID] [int] IDENTITY(1,1) NOT NULL,
	[QuestionID] [int] NOT NULL,
	[QuestionText] [varchar](50) NOT NULL
) ON [PRIMARY]
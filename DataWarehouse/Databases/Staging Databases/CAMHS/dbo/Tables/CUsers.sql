﻿CREATE TABLE [dbo].[CUsers](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[ServiceID] [int] NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_CUsers_Active]  DEFAULT ((0)),
	[DateAllocated] [datetime] NULL CONSTRAINT [DF_CUsers_DateAllocated]  DEFAULT (getdate()),
	[UserName] [varchar](20) NOT NULL,
	[Password] [varchar](20) NOT NULL,
	[ExpiryDate] [datetime] NULL,
	[SpecialityID] [int] NOT NULL,
	[ITUser] [bit] NULL,
	[SuperUser] [bit] NULL,
	[Admin] [bit] NOT NULL,
	[Reporter] [bit] NOT NULL,
	[ClinicianID] [varchar](10) NULL,
	[Firstname] [varchar](30) NULL,
	[Surname] [varchar](30) NULL,
	[Email] [varchar](100) NULL,
	[Pin] [int] NULL,
	[LastUpdated] [datetime] NULL CONSTRAINT [DF_CUsers_LastUpdated]  DEFAULT (getdate()),
	[PwdChange] [bit] NULL CONSTRAINT [DF_CUsers_PwdChange]  DEFAULT ((0)),
	[GraceLogins] [int] NULL CONSTRAINT [DF_CUsers_GraceLogins]  DEFAULT ((5)),
	[Logins] [int] NULL CONSTRAINT [DF_CUsers_Logins]  DEFAULT ((0)),
	[Diary] [bit] NULL,
	[TrainedID] [int] NOT NULL,
	[ClinicalManager] [bit] NOT NULL CONSTRAINT [DF__CUsers__Clinical__369C13AA]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CUsersGroups](
	[UserGroupID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](30) NOT NULL,
	[ClinicianID] [varchar](10) NULL,
	[WhereSeenID] [int] NOT NULL,
	[Active] [bit] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[County](
	[CountyID] [int] NOT NULL,
	[CountyDesc] [varchar](100) NULL,
	[UserID] [int] NULL,
	[LastUpdated] [datetime] NULL CONSTRAINT [DF_County_LastUpdated]  DEFAULT (getdate())
) ON [PRIMARY]
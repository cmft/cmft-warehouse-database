﻿CREATE TABLE [dbo].[SurveyVersion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [char](3) NOT NULL,
	[Version] [varchar](50) NOT NULL
) ON [PRIMARY]
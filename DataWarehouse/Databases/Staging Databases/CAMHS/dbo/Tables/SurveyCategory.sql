﻿CREATE TABLE [dbo].[SurveyCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SurveyCategory] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DropDown](
	[DropDownId] [int] IDENTITY(1,1) NOT NULL,
	[AnswerValue] [int] NOT NULL,
	[QuestionId] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[Text] [varchar](100) NOT NULL,
	[IsText] [bit] NOT NULL CONSTRAINT [DF_DropDown_IsText]  DEFAULT ((0)),
	[Code] [varchar](7) NULL
) ON [PRIMARY]
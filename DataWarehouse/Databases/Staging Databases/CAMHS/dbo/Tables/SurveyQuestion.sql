﻿CREATE TABLE [dbo].[SurveyQuestion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Question] [varchar](300) NOT NULL,
	[Order] [int] NOT NULL,
	[AnswerSetID] [int] NULL,
	[SurveyID] [int] NOT NULL,
	[RespondentExclusions] [xml] NULL,
	[QuestionRef] [varchar](100) NOT NULL,
	[AnswerTypeID] [int] NOT NULL,
	[MinVal] [int] NULL,
	[MaxVal] [int] NULL,
	[Increment] [decimal](10, 5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
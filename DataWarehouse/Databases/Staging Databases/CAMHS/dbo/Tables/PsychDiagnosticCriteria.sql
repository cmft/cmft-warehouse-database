﻿CREATE TABLE [dbo].[PsychDiagnosticCriteria](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PsychID] [int] NOT NULL,
	[DiagnosticCriteriaID] [int] NOT NULL,
	[IsPrimary] [bit] NOT NULL
) ON [PRIMARY]
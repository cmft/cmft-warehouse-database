﻿CREATE TABLE [dbo].[Report](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReportName] [varchar](50) NOT NULL,
	[ReportCatID] [int] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[ReportURL] [varchar](50) NOT NULL
) ON [PRIMARY]
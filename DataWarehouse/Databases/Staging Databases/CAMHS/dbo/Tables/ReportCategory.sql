﻿CREATE TABLE [dbo].[ReportCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[IndustryCode] [nvarchar](20) NULL,
	[Active] [bit] NOT NULL,
	[SortOrder] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TeamType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TeamType] [varchar](100) NOT NULL,
	[Code] [char](3) NOT NULL
) ON [PRIMARY]
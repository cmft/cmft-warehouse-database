﻿CREATE TABLE [dbo].[SurveyRespondent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Respondent] [varchar](50) NOT NULL,
	[Code] [varchar](10) NULL
) ON [PRIMARY]
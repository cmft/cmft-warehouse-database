﻿CREATE TABLE [dbo].[SurveyClinicalBand](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ScaleID] [int] NOT NULL,
	[Sex] [char](1) NOT NULL,
	[AgeLower] [int] NOT NULL,
	[AgeUpper] [int] NOT NULL,
	[ScoreLower] [int] NOT NULL,
	[ScoreUpper] [int] NOT NULL,
	[Version] [varchar](3) NOT NULL,
	[OutputID] [int] NOT NULL,
	[ClinicalBandType] [char](2) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SurveyScaleDefinition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Scale] [varchar](50) NOT NULL,
	[ScaleDesc] [varchar](150) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SurveySubCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SubCategory] [varchar](50) NOT NULL
) ON [PRIMARY]
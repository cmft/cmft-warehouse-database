﻿CREATE TABLE [dbo].[SurveyRespondentExclusion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SurveyID] [int] NOT NULL,
	[RespondentID] [int] NOT NULL
) ON [PRIMARY]
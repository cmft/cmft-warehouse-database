﻿CREATE TABLE [dbo].[SurveyChartType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChartType] [varchar](50) NOT NULL
) ON [PRIMARY]
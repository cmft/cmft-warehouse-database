﻿CREATE TABLE [dbo].[DiaryInterventionTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InterventionTypeID] [int] NOT NULL,
	[DiaryID] [int] NOT NULL,
	[IsPrimary] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Survey](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](150) NOT NULL,
	[WelcomeText] [varchar](300) NULL,
	[ClosingText] [varchar](300) NULL,
	[IsActive] [bit] NOT NULL,
	[Ref] [varchar](20) NOT NULL,
	[Colour] [varchar](15) NOT NULL,
	[IncludeInBuild] [bit] NOT NULL,
	[VersionFileDefinition] [varchar](50) NULL,
	[CompletedByFileDefinition] [varchar](50) NULL,
	[CategoryID] [int] NOT NULL,
	[SubCategoryID] [int] NULL,
	[ChartTypeID] [int] NULL
) ON [PRIMARY]
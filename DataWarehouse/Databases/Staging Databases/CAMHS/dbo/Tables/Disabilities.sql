﻿CREATE TABLE [dbo].[Disabilities](
	[DisabilityID] [int] IDENTITY(1,1) NOT NULL,
	[Disability] [varchar](50) NOT NULL,
	[OldID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[InterventionType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InterventionType] [varchar](50) NOT NULL,
	[OrderBy] [int] NOT NULL
) ON [PRIMARY]
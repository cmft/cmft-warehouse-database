﻿CREATE TABLE [dbo].[DocumentType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[IndustryCode] [varchar](20) NULL,
	[Active] [bit] NOT NULL,
	[SortOrder] [int] NULL
) ON [PRIMARY]
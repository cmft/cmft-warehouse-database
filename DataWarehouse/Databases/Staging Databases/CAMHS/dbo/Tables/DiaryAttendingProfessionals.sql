﻿CREATE TABLE [dbo].[DiaryAttendingProfessionals](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DiaryID] [int] NOT NULL,
	[AttendingProfessionalID] [int] NOT NULL
) ON [PRIMARY]
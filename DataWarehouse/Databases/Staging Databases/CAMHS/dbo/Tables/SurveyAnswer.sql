﻿CREATE TABLE [dbo].[SurveyAnswer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SurveyID] [int] NOT NULL,
	[KeyCode] [int] NULL,
	[EpisodeID] [int] NOT NULL,
	[EventID] [int] NULL,
	[RespondentID] [int] NULL,
	[DateCreated] [datetime] NOT NULL,
	[Answers] [xml] NULL,
	[DateCompleted] [datetime] NULL,
	[Comment] [varchar](max) NULL,
	[CurrentQuestion] [int] NULL,
	[Version] [varchar](3) NOT NULL,
	[OutcomeMeasures] [xml] NULL,
	[UserID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[SurveyAnswerType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NOT NULL
) ON [PRIMARY]
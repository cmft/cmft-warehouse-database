﻿CREATE TABLE [dbo].[AJOCC](
	[OSTK] [numeric](18, 0) NOT NULL,
	[OrderSession] [numeric](18, 0) NOT NULL,
	[Consultant] [varchar](6) NULL,
	[UID] [char](10) NULL,
	[OrderDate] [varchar](12) NULL,
	[OrderBy] [varchar](20) NULL,
	[Specialty] [varchar](4) NULL,
	[Comment1] [varchar](60) NULL,
	[Comment2] [varchar](60) NULL,
	[Comment3] [varchar](60) NULL,
	[Comment4] [varchar](60) NULL,
	[Comment5] [varchar](60) NULL,
	[Comment6] [varchar](60) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AJOCP](
	[Patient] [numeric](18, 0) NULL,
	[Surname] [varchar](24) NULL,
	[Forename] [varchar](20) NULL,
	[DoB] [varchar](8) NULL,
	[NHSNo] [varchar](17) NULL
) ON [PRIMARY]
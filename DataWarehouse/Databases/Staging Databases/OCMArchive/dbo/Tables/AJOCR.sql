﻿CREATE TABLE [dbo].[AJOCR](
	[OSTK] [numeric](18, 0) NOT NULL,
	[ResultSession] [numeric](18, 0) NOT NULL,
	[UID] [char](10) NULL,
	[Comment1] [varchar](60) NULL,
	[Comment2] [varchar](60) NULL,
	[Comment3] [varchar](60) NULL,
	[Comment4] [varchar](60) NULL,
	[Comment5] [varchar](60) NULL,
	[Comment6] [varchar](60) NULL,
	[ResultCode] [char](10) NOT NULL,
	[ResultDate] [varchar](12) NULL,
	[Indicator] [char](1) NULL,
	[ResultValue] [varchar](9) NULL,
	[ResultType] [char](1) NULL,
	[UnitofMeasure] [varchar](8) NULL
) ON [PRIMARY]
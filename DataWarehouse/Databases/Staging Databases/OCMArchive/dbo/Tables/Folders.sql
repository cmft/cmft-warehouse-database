﻿CREATE TABLE [dbo].[Folders](
	[Folder] [varchar](8) NOT NULL,
	[AJOCS] [numeric](18, 0) NULL,
	[AJOCC] [numeric](18, 0) NULL,
	[AJOCR] [numeric](18, 0) NULL,
	[AJOCT] [numeric](18, 0) NULL,
	[AJOCA] [numeric](18, 0) NULL,
	[AJOCP] [numeric](18, 0) NULL,
	[AJOCE] [numeric](18, 0) NULL
) ON [PRIMARY]
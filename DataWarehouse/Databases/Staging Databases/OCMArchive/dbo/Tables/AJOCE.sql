﻿CREATE TABLE [dbo].[AJOCE](
	[Patient] [numeric](18, 0) NULL,
	[Episode] [numeric](18, 0) NULL,
	[Casenote] [varchar](14) NULL
) ON [PRIMARY]
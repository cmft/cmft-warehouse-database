﻿CREATE TABLE [dbo].[ServiceMF](
	[ServiceCode] [numeric](18, 0) NOT NULL,
	[Description] [varchar](30) NULL,
	[ShortDescription] [varchar](13) NULL,
	[Department] [char](4) NULL,
	[ScreenType] [char](4) NULL,
	[PLOC] [char](4) NULL,
	[OMP] [varchar](6) NULL,
	[SpecimenType] [varchar](6) NULL,
	[Quantity] [varchar](6) NULL,
	[UoM] [varchar](8) NULL,
	[Container] [char](4) NULL,
	[Mnemonic] [varchar](13) NULL,
	[Orderable] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AJOCT](
	[OSTK] [numeric](18, 0) NOT NULL,
	[ResultSession] [varchar](8) NOT NULL,
	[ResultCode] [char](4) NOT NULL,
	[Sequence] [int] NOT NULL,
	[ResultText] [varchar](60) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Episodes](
	[Patient] [numeric](18, 0) NOT NULL,
	[Episode] [int] NOT NULL,
	[Casenote] [varchar](14) NOT NULL
) ON [PRIMARY]
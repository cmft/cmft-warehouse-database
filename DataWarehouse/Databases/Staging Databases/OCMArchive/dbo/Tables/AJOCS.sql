﻿CREATE TABLE [dbo].[AJOCS](
	[OSTK] [numeric](18, 0) NOT NULL,
	[Episode] [int] NULL,
	[Patient] [numeric](18, 0) NULL,
	[DistNo] [varchar](14) NULL,
	[Service] [numeric](18, 0) NULL,
	[ServiceDate] [varchar](12) NULL,
	[ResSession] [varchar](8) NULL,
	[OrderSession] [varchar](8) NULL,
	[Transaction] [int] NULL,
	[PLOC] [char](4) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patients](
	[Patient] [numeric](18, 0) NOT NULL,
	[Surname] [varchar](24) NULL,
	[Forename] [varchar](20) NULL,
	[DoB] [varchar](8) NULL,
	[NHSNo] [varchar](10) NULL,
	[DistrictNumber] [char](8) NULL
) ON [PRIMARY]
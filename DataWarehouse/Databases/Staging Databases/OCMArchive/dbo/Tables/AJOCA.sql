﻿CREATE TABLE [dbo].[AJOCA](
	[OSTK] [numeric](18, 0) NOT NULL,
	[OrderSession] [numeric](18, 0) NOT NULL,
	[StatusDate] [char](12) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
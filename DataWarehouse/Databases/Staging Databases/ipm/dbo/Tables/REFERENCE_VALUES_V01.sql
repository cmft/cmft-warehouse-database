﻿CREATE TABLE [dbo].[REFERENCE_VALUES_V01](
	[TISID] [int] NOT NULL,
	[FILE_NAME] [varchar](80) NULL,
	[DATA_START_DATE] [datetime] NULL,
	[DATA_END_DATE] [datetime] NULL,
	[TOTAL_ROW_COUNT] [numeric](18, 0) NULL,
	[ROW_IDENTIFIER] [numeric](18, 0) NULL,
	[RFVAL_REFNO] [numeric](10, 0) NULL,
	[RFVDM_CODE] [varchar](5) NULL,
	[DESCRIPTION] [varchar](80) NULL,
	[MAIN_CODE] [varchar](25) NULL,
	[PARNT_REFNO] [numeric](10, 0) NULL,
	[SMODE_VALUE] [varchar](5) NULL,
	[DEFAULT_VALUE] [char](1) NULL,
	[SORT_ORDER] [numeric](3, 0) NULL,
	[SELECT_VALUE] [char](1) NULL,
	[DISPLAY_VALUE] [char](1) NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[RFTYP_CODE] [varchar](5) NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[STRAN_REFNO] [numeric](10, 0) NULL,
	[PRIOR_POINTER] [numeric](10, 0) NULL,
	[EXTERNAL_KEY] [varchar](20) NULL,
	[START_DTTM] [datetime] NULL,
	[END_DTTM] [datetime] NULL,
	[LOW_VALUE] [varchar](30) NULL,
	[HIGH_VALUE] [varchar](30) NULL,
	[PROCESS_MODULE_ID] [varchar](20) NULL,
	[SCLVL_REFNO] [numeric](10, 0) NULL,
	[SYN_CODE] [varchar](20) NULL,
	[OWNER_HEORG_REFNO] [numeric](10, 0) NULL,
	[TIS_FLAG] [char](10) NULL,
	[load_datetime] [datetime] NULL
) ON [PRIMARY]
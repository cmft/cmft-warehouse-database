﻿CREATE TABLE [dbo].[OVERSEAS_VISITOR_STATUS_V01](
	[TISID] [int] NOT NULL,
	[FILE_NAME] [varchar](80) NULL,
	[DATA_START_DATE] [datetime] NULL,
	[DATA_END_DATE] [datetime] NULL,
	[TOTAL_ROW_COUNT] [numeric](18, 0) NULL,
	[ROW_IDENTIFIER] [numeric](18, 0) NULL,
	[OVSEA_REFNO] [numeric](10, 0) NULL,
	[OVSVS_REFNO] [numeric](10, 0) NULL,
	[OVSVS_REFNO_MAIN_CODE] [varchar](25) NULL,
	[OVSVS_REFNO_DESCRIPTION] [varchar](80) NULL,
	[PATNT_REFNO] [numeric](10, 0) NULL,
	[PATNT_REFNO_PASID] [varchar](20) NULL,
	[PATNT_REFNO_NHS_IDENTIFIER] [varchar](20) NULL,
	[START_DTTM] [datetime] NULL,
	[END_DTTM] [datetime] NULL,
	[ENTRY_DTTM] [datetime] NULL,
	[CREATE_DTTM] [datetime] NULL,
	[MODIF_DTTM] [datetime] NULL,
	[USER_CREATE] [varchar](30) NULL,
	[USER_MODIF] [varchar](30) NULL,
	[ARCHV_FLAG] [char](1) NULL,
	[TIS_FLAG] [char](10) NULL,
	[load_datetime] [datetime] NULL
) ON [PRIMARY]
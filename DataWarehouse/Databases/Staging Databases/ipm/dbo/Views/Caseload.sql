﻿CREATE VIEW [dbo].[Caseload]

AS

SELECT
	 Caseload.TISID
	,Caseload.[FILE_NAME]
	,Caseload.DATA_START_DATE
	,Caseload.DATA_END_DATE
	,Caseload.TOTAL_ROW_COUNT
	,Caseload.ROW_IDENTIFIER
	,Caseload.CLENT_REFNO
	,Caseload.REFRL_REFNO
	,Caseload.CRTYP_REFNO
	,Caseload.CLSTA_REFNO
	,Caseload.ALLOC_DTTM
	,Caseload.CLRSN_REFNO
	,Caseload.INTLV_REFNO
	,Caseload.DISCHARGE_DTTM
	,Caseload.CLOCM_REFNO
	,Caseload.PROCA_REFNO
	,Caseload.SPECT_REFNO
	,Caseload.STEAM_REFNO
	,Caseload.RESPTO_HEORG_REFNO
	,Caseload.SUSP_START_DTTM
	,Caseload.SUSP_END_DTTM
	,Caseload.CLSRS_REFNO
	,Caseload.PATNT_REFNO
	,Caseload.TRANS_FROM_REFNO
	,Caseload.CREATE_DTTM
	,Caseload.MODIF_DTTM
	,Caseload.USER_CREATE
	,Caseload.USER_MODIF
	,Caseload.ARCHV_FLAG
	,Caseload.STRAN_REFNO
	,Caseload.PRIOR_POINTER
	,Caseload.EXTERNAL_KEY
	,Caseload.CSDDR_REFNO
	,Caseload.TEMP_FLAG
	,Caseload.PRIOR_REFNO
	,Caseload.ACLEV_REFNO
	,Caseload.OWNER_HEORG_REFNO
	,Caseload.TIS_FLAG
	,Caseload.load_datetime
FROM dbo.CASELOAD_ENTRIES_V01 Caseload
INNER JOIN (
				SELECT 
					 CLENT_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.CASELOAD_ENTRIES_V01
				GROUP BY CLENT_REFNO
			)LatestCaseload
ON Caseload.CLENT_REFNO = LatestCaseload.CLENT_REFNO
AND Caseload.MODIF_DTTM = LatestCaseload.MODIF_DTTM
﻿CREATE VIEW [dbo].[ProfessionalCarer]

AS

SELECT 
	 TISID
	,ProfessionalCarer.[FILE_NAME]
	,ProfessionalCarer.DATA_START_DATE
	,ProfessionalCarer.DATA_END_DATE
	,ProfessionalCarer.TOTAL_ROW_COUNT
	,ProfessionalCarer.ROW_IDENTIFIER
	,ProfessionalCarer.PROCA_REFNO
	,ProfessionalCarer.PROCA_REFNO_MAIN_IDENT
	,ProfessionalCarer.PRTYP_REFNO
	,ProfessionalCarer.PRTYP_REFNO_MAIN_CODE
	,ProfessionalCarer.PRTYP_REFNO_DESCRIPTION
	,ProfessionalCarer.FORENAME
	,ProfessionalCarer.TITLE_REFNO
	,ProfessionalCarer.TITLE_REFNO_MAIN_CODE
	,ProfessionalCarer.TITLE_REFNO_DESCRIPTION
	,ProfessionalCarer.GRADE_REFNO
	,ProfessionalCarer.GRADE_REFNO_MAIN_CODE
	,ProfessionalCarer.GRADE_REFNO_DESCRIPTION
	,ProfessionalCarer.SURNAME
	,ProfessionalCarer.SEXXX_REFNO
	,ProfessionalCarer.SEXXX_REFNO_MAIN_CODE
	,ProfessionalCarer.SEXXX_REFNO_DESCRIPTION
	,ProfessionalCarer.DATE_OF_BIRTH
	,ProfessionalCarer.CONTR_HOURS
	,ProfessionalCarer.LEAVE_ENTITLEMENT
	,ProfessionalCarer.PLACE_OF_BIRTH
	,ProfessionalCarer.QUALIFICATIONS
	,ProfessionalCarer.GLOVE_INITM_REFNO
	,ProfessionalCarer.GLOVE_INITM_REFNO_MAIN_CODE
	,ProfessionalCarer.GLOVE_INITM_REFNO_DESCRIPTION
	,ProfessionalCarer.SKNPR_INITM_REFNO
	,ProfessionalCarer.SKNPR_INITM_REFNO_MAIN_CODE
	,ProfessionalCarer.SKNPR_INITM_REFNO_DESCRIPTION
	,ProfessionalCarer.DEF_PATH_SPONT_REFNO
	,ProfessionalCarer.DEF_PATH_SPONT_REFNO_CODE
	,ProfessionalCarer.DEF_PATH_SPONT_REFNO_NAME
	,ProfessionalCarer.START_DTTM
	,ProfessionalCarer.END_DTTM
	,ProfessionalCarer.[DESCRIPTION]
	,ProfessionalCarer.CREATE_DTTM
	,ProfessionalCarer.MODIF_DTTM
	,ProfessionalCarer.USER_CREATE
	,ProfessionalCarer.USER_MODIF
	,ProfessionalCarer.LOCAL_FLAG
	,ProfessionalCarer.MAIN_IDENT
	,ProfessionalCarer.ARCHV_FLAG
	,ProfessionalCarer.PRIMARY_STEAM_REFNO
	,ProfessionalCarer.PRIMARY_STEAM_REFNO_CODE
	,ProfessionalCarer.PRIMARY_STEAM_REFNO_NAME
	,ProfessionalCarer.OWNER_HEORG_REFNO
	,ProfessionalCarer.OWNER_HEORG_REFNO_MAIN_IDENT
	,ProfessionalCarer.TIS_FLAG
	,ProfessionalCarer.load_datetime
	
FROM 
	dbo.PROF_CARERS_V01 ProfessionalCarer
INNER JOIN (
				SELECT 
					 PROCA_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.PROF_CARERS_V01
				GROUP BY PROCA_REFNO
			)LatestProfessionalCarer
ON ProfessionalCarer.PROCA_REFNO = LatestProfessionalCarer.PROCA_REFNO
AND ProfessionalCarer.MODIF_DTTM = LatestProfessionalCarer.MODIF_DTTM
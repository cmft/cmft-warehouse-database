﻿CREATE VIEW [dbo].[PatientProfessionalCarer]

AS

SELECT 
	 PatientProfessionalCarer.TISID
	,PatientProfessionalCarer.[FILE_NAME]
	,PatientProfessionalCarer.DATA_START_DATE
	,PatientProfessionalCarer.DATA_END_DATE
	,PatientProfessionalCarer.TOTAL_ROW_COUNT
	,PatientProfessionalCarer.ROW_IDENTIFIER
	,PatientProfessionalCarer.PATPC_REFNO
	,PatientProfessionalCarer.PRTYP_REFNO
	,PatientProfessionalCarer.PRTYP_MAIN_CODE
	,PatientProfessionalCarer.PRTYP_DESCRIPTION
	,PatientProfessionalCarer.PROCA_REFNO
	,PatientProfessionalCarer.PROCA_MAIN_IDENT
	,PatientProfessionalCarer.PATNT_REFNO
	,PatientProfessionalCarer.PATNT_PASID
	,PatientProfessionalCarer.PATNT_NHS_IDENTIFIER
	,PatientProfessionalCarer.HEORG_REFNO
	,PatientProfessionalCarer.HEORG_MAIN_IDENT
	,PatientProfessionalCarer.START_DTTM
	,PatientProfessionalCarer.END_DTTM
	,PatientProfessionalCarer.CREATE_DTTM
	,PatientProfessionalCarer.MODIF_DTTM
	,PatientProfessionalCarer.USER_CREATE
	,PatientProfessionalCarer.USER_MODIF
	,PatientProfessionalCarer.ARCHV_FLAG
	,PatientProfessionalCarer.TIS_FLAG
	,PatientProfessionalCarer.load_datetime
	
FROM 
	dbo.PATIENT_PROF_CARERS_V01 PatientProfessionalCarer
INNER JOIN (
			SELECT
				 PATPC_REFNO
				,MODIF_DTTM
				,Max(load_datetime) load_datetime
			FROM
			(
				SELECT 
					 PATPC_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
					,load_datetime
				FROM 
					dbo.PATIENT_PROF_CARERS_V01
				GROUP BY 
					 PATPC_REFNO
					,load_datetime
			)LatestServicePatientProfessionalCarerModified
			GROUP BY
				 PATPC_REFNO
				,MODIF_DTTM
		)LatestServicePatientProfessionalCarer
ON PatientProfessionalCarer.PATPC_REFNO = LatestServicePatientProfessionalCarer.PATPC_REFNO
AND PatientProfessionalCarer.MODIF_DTTM = LatestServicePatientProfessionalCarer.MODIF_DTTM
AND PatientProfessionalCarer.load_datetime = LatestServicePatientProfessionalCarer.load_datetime
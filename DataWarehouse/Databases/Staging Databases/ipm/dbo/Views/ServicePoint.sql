﻿CREATE VIEW [dbo].[ServicePoint]

AS


SELECT 
	 ServicePoint.TISID
	,ServicePoint.[FILE_NAME]
	,ServicePoint.DATA_START_DATE
	,ServicePoint.DATA_END_DATE
	,ServicePoint.TOTAL_ROW_COUNT
	,ServicePoint.ROW_IDENTIFIER
	,ServicePoint.SPONT_REFNO
	,ServicePoint.SPONT_REFNO_CODE
	,ServicePoint.SPONT_REFNO_NAME
	,ServicePoint.SPECT_REFNO
	,ServicePoint.SPECT_REFNO_MAIN_IDENT
	,ServicePoint.STEAM_REFNO
	,ServicePoint.STEAM_REFNO_CODE
	,ServicePoint.STEAM_REFNO_NAME
	,ServicePoint.PROCA_REFNO
	,ServicePoint.PROCA_REFNO_MAIN_IDENT
	,ServicePoint.HEORG_REFNO
	,ServicePoint.HEORG_REFNO_MAIN_IDENT
	,ServicePoint.CODE
	,ServicePoint.NOMINAL_DEPT_CODE
	,ServicePoint.[DESCRIPTION]
	,ServicePoint.NAME
	,ServicePoint.START_DTTM
	,ServicePoint.END_DTTM
	,ServicePoint.PURPS_REFNO
	,ServicePoint.PURPS_REFNO_MAIN_CODE
	,ServicePoint.PURPS_REFNO_DESCRIPTION
	,ServicePoint.SPTYP_REFNO
	,ServicePoint.SPTYP_REFNO_MAIN_CODE
	,ServicePoint.SPTYP_REFNO_DESCRIPTION
	,ServicePoint.AE_FLAG
	,ServicePoint.PDTYP_REFNO
	,ServicePoint.PDTYP_REFNO_MAIN_CODE
	,ServicePoint.PDTYP_REFNO_DESCRIPTION
	,ServicePoint.CREATE_DTTM
	,ServicePoint.MODIF_DTTM
	,ServicePoint.USER_CREATE
	,ServicePoint.USER_MODIF
	,ServicePoint.ARCHV_FLAG
	,ServicePoint.USE_BED_MANAGEMENT
	,ServicePoint.HORIZ_VALUE
	,ServicePoint.AGE_TYPE
	,ServicePoint.AGE_QUALIFIER
	,ServicePoint.WARN_LEVEL
	,ServicePoint.MAX_LEVEL
	,ServicePoint.FCPUR_REFNO
	,ServicePoint.FCPUR_REFNO_MAIN_CODE
	,ServicePoint.FCPUR_REFNO_DESCRIPTION
	,ServicePoint.EXCLUDE_HOLS
	,ServicePoint.RESCH_MAX_DIFF
	,ServicePoint.OWNER_HEORG_REFNO
	,ServicePoint.OWNER_HEORG_REFNO_MAIN_IDENT
	,ServicePoint.INSTRUCTIONS
	,ServicePoint.CHG_AC_AUTO
	,ServicePoint.PBK_FLAG
	,ServicePoint.PBK_LEAD_TIME
	,ServicePoint.PBK_LEAD_TIME_UNITS
	,ServicePoint.CONTACT_FLAG
	,ServicePoint.LEAD_TIME_CLIN_FLAG
	,ServicePoint.TIS_FLAG
	,ServicePoint.load_datetime

FROM 
	dbo.SERVICE_POINTS_V01 ServicePoint
INNER JOIN (
				SELECT 
					 SPONT_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.SERVICE_POINTS_V01
				GROUP BY SPONT_REFNO
			)LatestServicePoint
ON ServicePoint.SPONT_REFNO = LatestServicePoint.SPONT_REFNO
AND ServicePoint.MODIF_DTTM = LatestServicePoint.MODIF_DTTM
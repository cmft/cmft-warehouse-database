﻿CREATE VIEW [dbo].[AddressRole]

AS

SELECT 
	 AddressRole.TISID
	,AddressRole.[FILE_NAME]
	,AddressRole.DATA_START_DATE
	,AddressRole.DATA_END_DATE
	,AddressRole.TOTAL_ROW_COUNT
	,AddressRole.ROW_IDENTIFIER
	,AddressRole.ROLES_REFNO
	,AddressRole.PROCA_REFNO
	,AddressRole.PATNT_REFNO
	,AddressRole.PERCA_REFNO
	,AddressRole.HEORG_REFNO
	,AddressRole.ADDSS_REFNO
	,AddressRole.ROTYP_CODE
	,AddressRole.CORRESPONDENCE
	,AddressRole.START_DTTM
	,AddressRole.END_DTTM
	,AddressRole.DEPRT_PATRN_REFNO
	,AddressRole.CURNT_FLAG
	,AddressRole.HOMEL_REFNO
	,AddressRole.PURCH_REFNO
	,AddressRole.PROVD_REFNO
	,AddressRole.SECURE_FLAG
	,AddressRole.PATPC_REFNO
	,AddressRole.PERSS_REFNO
	,AddressRole.ORDRR_REFNO
	,AddressRole.CREATE_DTTM
	,AddressRole.MODIF_DTTM
	,AddressRole.USER_CREATE
	,AddressRole.USER_MODIF
	,AddressRole.ARCHV_FLAG
	,AddressRole.STRAN_REFNO
	,AddressRole.PRIOR_POINTER
	,AddressRole.EXTERNAL_KEY
	,AddressRole.BFORM_REFNO
	,AddressRole.SYN_CODE
	,AddressRole.OWNER_HEORG_REFNO
	,AddressRole.TIS_FLAG
	,AddressRole.load_datetime

FROM 
	dbo.ADDRESS_ROLES_V01 AddressRole
INNER JOIN (
				SELECT 
					 ROLES_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.ADDRESS_ROLES_V01
				GROUP BY ROLES_REFNO
			)LatestAddressRole
ON AddressRole.ROLES_REFNO = LatestAddressRole.ROLES_REFNO
AND AddressRole.MODIF_DTTM = LatestAddressRole.MODIF_DTTM
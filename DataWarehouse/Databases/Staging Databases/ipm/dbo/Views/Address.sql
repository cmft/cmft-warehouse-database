﻿CREATE VIEW [dbo].[Address]

AS

SELECT 
	 Addresses.TISID
	,Addresses.[FILE_NAME]
	,Addresses.DATA_START_DATE
	,Addresses.DATA_END_DATE
	,Addresses.TOTAL_ROW_COUNT
	,Addresses.ROW_IDENTIFIER
	,Addresses.ADDSS_REFNO
	,Addresses.ADTYP_CODE
	,Addresses.LINE1
	,Addresses.LINE2
	,Addresses.LINE3
	,Addresses.LINE4
	,Addresses.PCODE
	,Addresses.LOCAT_CODE
	,Addresses.HDIST_CODE
	,Addresses.ELECT_CODE
	,Addresses.COUNTY
	,Addresses.CNTRY_REFNO
	,Addresses.START_DTTM
	,Addresses.END_DTTM
	,Addresses.CREATE_DTTM
	,Addresses.MODIF_DTTM
	,Addresses.USER_CREATE
	,Addresses.USER_MODIF
	,Addresses.ARCHV_FLAG
	,Addresses.STRAN_REFNO
	,Addresses.PRIOR_POINTER
	,Addresses.EXTERNAL_KEY
	,Addresses.COUNT_OTCOD_REFNO
	,Addresses.COMMU_OTCOD_REFNO
	,Addresses.PARSH_OTCOD_REFNO
	,Addresses.PUARE_OTCOD_REFNO
	,Addresses.PCARE_OTCOD_REFNO
	,Addresses.RES_REG_DTTM
	,Addresses.SUBURB
	,Addresses.STATE_CODE
	,Addresses.MPIAS_REFNO
	,Addresses.PCG_CODE
	,Addresses.TEAMA_REFNO
	,Addresses.CMMCA_REFNO
	,Addresses.CMMSC_REFNO
	,Addresses.TLAND_REFNO
	,Addresses.SYN_CODE
	,Addresses.DEL_POINT_ID
	,Addresses.QAS_BARCODE
	,Addresses.OWNER_HEORG_REFNO
	,Addresses.TIS_FLAG
	,Addresses.load_datetime

FROM 
	dbo.ADDRESSES_V01 Addresses
INNER JOIN (
				SELECT 
					 ADDSS_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.ADDRESSES_V01
				GROUP BY ADDSS_REFNO
			)LatestAddress
ON LatestAddress.ADDSS_REFNO = Addresses.ADDSS_REFNO
AND LatestAddress.MODIF_DTTM = Addresses.MODIF_DTTM
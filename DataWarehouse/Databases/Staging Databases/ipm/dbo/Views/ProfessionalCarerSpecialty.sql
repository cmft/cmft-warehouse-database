﻿create view [dbo].[ProfessionalCarerSpecialty]

as

select
	[TISID]
	,[FILE_NAME]
	,[DATA_START_DATE]
	,[DATA_END_DATE]
	,[TOTAL_ROW_COUNT]
	,[ROW_IDENTIFIER]
	,[PRCAS_REFNO]
	,[SPECT_REFNO]
	,[CSTYP_REFNO]
	,[PROCA_REFNO]
	,[START_DTTM]
	,[END_DTTM]
	,[CREATE_DTTM]
	,[MODIF_DTTM]
	,[USER_CREATE]
	,[USER_MODIF]
	,[ARCHV_FLAG]
	,[STRAN_REFNO]
	,[PRIOR_POINTER]
	,[EXTERNAL_KEY]
	,[PBK_LEAD_TIME]
	,[PBK_LEAD_TIME_UNITS]
	,[OWNER_HEORG_REFNO]
	,[TIS_FLAG]
	,[load_datetime]
from
	[dbo].[PROF_CARER_SPECIALTIES_V01] ProfCarerSpecialty
	
where
	not exists
				(
					select
						1
					from
						[dbo].[PROF_CARER_SPECIALTIES_V01] ProfCarerSpecialtyLater
					where
						ProfCarerSpecialtyLater.SPECT_REFNO = ProfCarerSpecialty.SPECT_REFNO
					and
						ProfCarerSpecialtyLater.PROCA_REFNO = ProfCarerSpecialty.PROCA_REFNO
					and
						coalesce(ProfCarerSpecialtyLater.START_DTTM, '1 jan 1900') = coalesce(ProfCarerSpecialty.START_DTTM, '1 jan 1900')
					and
						coalesce(ProfCarerSpecialtyLater.END_DTTM, getdate()) = coalesce(ProfCarerSpecialty.END_DTTM, getdate())
					and
						ProfCarerSpecialtyLater.MODIF_DTTM > ProfCarerSpecialty.MODIF_DTTM
					and
						coalesce(ProfCarerSpecialtyLater.[ARCHV_FLAG], 'N') = 'N'
				)

and
	coalesce(ProfCarerSpecialty.[ARCHV_FLAG], 'N') = 'N'
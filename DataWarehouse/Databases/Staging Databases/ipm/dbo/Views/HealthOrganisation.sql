﻿CREATE VIEW [dbo].[HealthOrganisation]

AS


SELECT 
	 TISID
	,HealthOrganisation.[FILE_NAME]
	,HealthOrganisation.DATA_START_DATE
	,HealthOrganisation.DATA_END_DATE
	,HealthOrganisation.TOTAL_ROW_COUNT
	,HealthOrganisation.ROW_IDENTIFIER
	,HealthOrganisation.HEORG_REFNO
	,HealthOrganisation.HOTYP_REFNO
	,HealthOrganisation.CASLT_REFNO
	,HealthOrganisation.MAIN_IDENT
	,HealthOrganisation.[DESCRIPTION]
	,HealthOrganisation.PARNT_REFNO
	,HealthOrganisation.START_DTTM
	,HealthOrganisation.END_DTTM
	,HealthOrganisation.PDTYP_REFNO
	,HealthOrganisation.CREATE_DTTM
	,HealthOrganisation.MODIF_DTTM
	,HealthOrganisation.USER_CREATE
	,HealthOrganisation.USER_MODIF
	,HealthOrganisation.RANKING
	,HealthOrganisation.LOCAL_FLAG
	,HealthOrganisation.ARCHV_FLAG
	,HealthOrganisation.STRAN_REFNO
	,HealthOrganisation.PRIOR_POINTER
	,HealthOrganisation.EXTERNAL_KEY
	,HealthOrganisation.BILLING_CENTRE
	,HealthOrganisation.HOLVL_REFNO
	,HealthOrganisation.COMPANY_CODE
	,HealthOrganisation.SCLVL_REFNO
	,HealthOrganisation.SYN_CODE
	,HealthOrganisation.PasId_Seed
	,HealthOrganisation.PasId_FORMAT
	,HealthOrganisation.PAS_ID_SUFFIX
	,HealthOrganisation.PAS_ID_MANUAL
	,HealthOrganisation.DUMMY_FLAG
	,HealthOrganisation.OWNER_HEORG_REFNO
	,HealthOrganisation.TIS_FLAG
	,HealthOrganisation.load_datetime

FROM 
	dbo.HEALTH_ORGANISATIONS_V02 HealthOrganisation
INNER JOIN (
				SELECT 
					 HEORG_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.HEALTH_ORGANISATIONS_V02
				GROUP BY HEORG_REFNO
			)LatestHealthOrganisation
ON HealthOrganisation.HEORG_REFNO = LatestHealthOrganisation.HEORG_REFNO
AND HealthOrganisation.MODIF_DTTM = LatestHealthOrganisation.MODIF_DTTM

/* 
	DG - 3 feb 2104 - the block of code below handles where we have data with the same modified datetime
	but it appears on two seperate extracts? We will opt for the latest extract where this occurs
*/ 


where
	 not exists
				(
					select
						1
					from
						dbo.HEALTH_ORGANISATIONS_V02 HealthOrganisationLater
					where
						HealthOrganisationLater.HEORG_REFNO = HealthOrganisation.HEORG_REFNO
					and HealthOrganisationLater.MODIF_DTTM = LatestHealthOrganisation.MODIF_DTTM
					and	HealthOrganisationLater.DATA_END_DATE > HealthOrganisation.DATA_END_DATE
					--or
					--	(
					--		HealthOrganisationLater.HEORG_REFNO = HealthOrganisation.HEORG_REFNO
					--	and	HealthOrganisationLater.DATA_END_DATE = HealthOrganisation.DATA_END_DATE
					--	and HealthOrganisationLater.TISID > HealthOrganisation.TISID
					--	)
				)
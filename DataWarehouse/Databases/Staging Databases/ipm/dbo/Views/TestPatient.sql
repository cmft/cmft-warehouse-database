﻿CREATE VIEW [dbo].[TestPatient]

AS

SELECT DISTINCT
	PATNT_REFNO

FROM 
	dbo.PATIENTS_VE01

WHERE
		LEFT(NHS_IDENTIFIER, 3) = '999'
		OR
			FORENAME  IN (
							'Ebs-Donotuse'
							,'Test'
							,'Patient'
							,'Mimium'
							,'xxtest'
							,'CSC-Donotuse'
						 )
		OR
			SURNAME  IN (
						'xxtestpatientaavp'
						,'xxtestpatientaafe'
						)
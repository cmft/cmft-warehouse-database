﻿CREATE VIEW [dbo].[ReferenceValues]

AS

SELECT
	 ReferenceValues.TISID
	,ReferenceValues.[FILE_NAME]
	,ReferenceValues.DATA_START_DATE
	,ReferenceValues.DATA_END_DATE
	,ReferenceValues.TOTAL_ROW_COUNT
	,ReferenceValues.ROW_IDENTIFIER
	,ReferenceValues.RFVAL_REFNO
	,ReferenceValues.RFVDM_CODE
	,ReferenceValues.[DESCRIPTION]
	,ReferenceValues.MAIN_CODE
	,ReferenceValues.PARNT_REFNO
	,ReferenceValues.SMODE_VALUE
	,ReferenceValues.DEFAULT_VALUE
	,ReferenceValues.SORT_ORDER
	,ReferenceValues.SELECT_VALUE
	,ReferenceValues.DISPLAY_VALUE
	,ReferenceValues.CREATE_DTTM
	,ReferenceValues.MODIF_DTTM
	,ReferenceValues.USER_CREATE
	,ReferenceValues.USER_MODIF
	,ReferenceValues.RFTYP_CODE
	,ReferenceValues.ARCHV_FLAG
	,ReferenceValues.STRAN_REFNO
	,ReferenceValues.PRIOR_POINTER
	,ReferenceValues.EXTERNAL_KEY
	,ReferenceValues.START_DTTM
	,ReferenceValues.END_DTTM
	,ReferenceValues.LOW_VALUE
	,ReferenceValues.HIGH_VALUE
	,ReferenceValues.PROCESS_MODULE_ID
	,ReferenceValues.SCLVL_REFNO
	,ReferenceValues.SYN_CODE
	,ReferenceValues.OWNER_HEORG_REFNO
	,ReferenceValues.TIS_FLAG
	,ReferenceValues.load_datetime
FROM 
	dbo.REFERENCE_VALUES_V01 ReferenceValues
INNER JOIN (
				SELECT 
					 RFVAL_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.REFERENCE_VALUES_V01
				GROUP BY RFVAL_REFNO
			)LatestReferenceValues
ON LatestReferenceValues.RFVAL_REFNO = ReferenceValues.RFVAL_REFNO
AND LatestReferenceValues.MODIF_DTTM = ReferenceValues.MODIF_DTTM
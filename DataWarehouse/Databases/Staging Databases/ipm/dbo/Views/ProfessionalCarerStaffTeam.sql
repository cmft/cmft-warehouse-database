﻿CREATE view [dbo].[ProfessionalCarerStaffTeam]

as

select
	[TISID]
	,[FILE_NAME]
	,[DATA_START_DATE]
	,[DATA_END_DATE]
	,[TOTAL_ROW_COUNT]
	,[ROW_IDENTIFIER]
	,[PCSTT_REFNO]
	,[PRROL_REFNO]
	,[STEAM_REFNO]
	,[PROCA_REFNO]
	,[START_DTTM]
	,[END_DTTM]
	,[CREATE_DTTM]
	,[MODIF_DTTM]
	,[USER_CREATE]
	,[USER_MODIF]
	,[ARCHV_FLAG]
	,[STRAN_REFNO]
	,[EXTERNAL_KEY]
	,[OWNER_HEORG_REFNO]
	,[TIS_FLAG]
	,[load_datetime]
from
	[dbo].[PROF_CARER_STAFF_TEAMS_V01] ProfCarerStaffTeam
where
	not exists
				(
					select
						1
					from
						[dbo].[PROF_CARER_STAFF_TEAMS_V01] ProfCarerStaffTeamLater
					where
						ProfCarerStaffTeamLater.STEAM_REFNO = ProfCarerStaffTeam.STEAM_REFNO
					and
						ProfCarerStaffTeamLater.PROCA_REFNO = ProfCarerStaffTeam.PROCA_REFNO
					and
						coalesce(ProfCarerStaffTeamLater.START_DTTM, '1 jan 1900') = coalesce(ProfCarerStaffTeam.START_DTTM, '1 jan 1900')
					and
						coalesce(ProfCarerStaffTeamLater.END_DTTM, getdate()) = coalesce(ProfCarerStaffTeam.END_DTTM, getdate())
					and
						ProfCarerStaffTeamLater.MODIF_DTTM > ProfCarerStaffTeam.MODIF_DTTM
					and
						coalesce(ProfCarerStaffTeamLater.[ARCHV_FLAG], 'N') = 'N'
				)

and
	coalesce(ProfCarerStaffTeam.[ARCHV_FLAG], 'N') = 'N'
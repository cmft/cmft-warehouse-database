﻿CREATE VIEW [dbo].[ReferenceValueIDs]

AS

SELECT
	 ReferenceValueIDs.TISID
	,ReferenceValueIDs.[FILE_NAME]
	,ReferenceValueIDs.DATA_START_DATE
	,ReferenceValueIDs.DATA_END_DATE
	,ReferenceValueIDs.TOTAL_ROW_COUNT
	,ReferenceValueIDs.ROW_IDENTIFIER
	,ReferenceValueIDs.RFVLI_REFNO
	,ReferenceValueIDs.RFVAL_REFNO
	,ReferenceValueIDs.IDENTIFIER
	,ReferenceValueIDs.CREATE_DTTM
	,ReferenceValueIDs.MODIF_DTTM
	,ReferenceValueIDs.USER_CREATE
	,ReferenceValueIDs.USER_MODIF
	,ReferenceValueIDs.RITYP_CODE
	,ReferenceValueIDs.ARCHV_FLAG
	,ReferenceValueIDs.STRAN_REFNO
	,ReferenceValueIDs.PRIOR_POINTER
	,ReferenceValueIDs.EXTERNAL_KEY
	,ReferenceValueIDs.OWNER_HEORG_REFNO
	,ReferenceValueIDs.TIS_FLAG
	,ReferenceValueIDs.load_datetime
FROM 
	dbo.REFERENCE_VALUE_IDS_V01 ReferenceValueIDs
INNER JOIN (
				SELECT 
					 RFVAL_REFNO
					,RITYP_CODE
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.REFERENCE_VALUE_IDS_V01
				where 
					ARCHV_FLAG = 'N'
				GROUP BY 
					 RFVAL_REFNO
					,RITYP_CODE
			)LatestReferenceValueIDs
ON LatestReferenceValueIDs.RFVAL_REFNO = ReferenceValueIDs.RFVAL_REFNO
and LatestReferenceValueIDs.RITYP_CODE = ReferenceValueIDs.RITYP_CODE
AND LatestReferenceValueIDs.MODIF_DTTM = ReferenceValueIDs.MODIF_DTTM
and ReferenceValueIDs.ARCHV_FLAG = 'N'
﻿CREATE VIEW [dbo].[ServicePointStay]

AS

SELECT
	 ServicePointStay.TISID
	,ServicePointStay.[FILE_NAME]
	,ServicePointStay.DATA_START_DATE
	,ServicePointStay.DATA_END_DATE
	,ServicePointStay.TOTAL_ROW_COUNT
	,ServicePointStay.ROW_IDENTIFIER
	,ServicePointStay.SSTAY_REFNO
	,ServicePointStay.PATNT_REFNO
	,ServicePointStay.PATNT_REFNO_PASID
	,ServicePointStay.PATNT_REFNO_NHS_IDENTIFIER
	,ServicePointStay.SPONT_REFNO
	,ServicePointStay.SPONT_REFNO_CODE
	,ServicePointStay.SPONT_REFNO_NAME
	,ServicePointStay.PRVSP_REFNO
	,ServicePointStay.START_DTTM
	,ServicePointStay.END_DTTM
	,ServicePointStay.CREATE_DTTM
	,ServicePointStay.MODIF_DTTM
	,ServicePointStay.USER_CREATE
	,ServicePointStay.USER_MODIF
	,ServicePointStay.ARCHV_FLAG
	,ServicePointStay.PRVSN_FLAG
	,ServicePointStay.PROCA_REFNO
	,ServicePointStay.PROCA_REFNO_MAIN_IDENT
	,ServicePointStay.BDCAT_REFNO
	,ServicePointStay.BDCAT_REFNO_MAIN_CODE
	,ServicePointStay.BDCAT_REFNO_DESCRIPTION
	,ServicePointStay.REQTD_BDCAT_REFNO
	,ServicePointStay.REQTD_BDCAT_REFNO_MAIN_CODE
	,ServicePointStay.REQTD_BDCAT_REFNO_DESCRIPTION
	,ServicePointStay.CHAPLAIN_VISIT_FLAG
	,ServicePointStay.VISITORS_ALLOWED_FLAG
	,ServicePointStay.OWNER_HEORG_REFNO
	,ServicePointStay.OWNER_HEORG_REFNO_MAIN_IDENT
	,ServicePointStay.WARD_START_DTTM
	,ServicePointStay.TIS_FLAG
	,ServicePointStay.Org_ID
	,ServicePointStay.load_datetime
FROM 
	dbo.SERVICE_POINT_STAYS_V01 ServicePointStay
INNER JOIN (
				SELECT 
					 SSTAY_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.SERVICE_POINT_STAYS_V01
				GROUP BY SSTAY_REFNO
			)LatestServicePointStay
ON LatestServicePointStay.SSTAY_REFNO = ServicePointStay.SSTAY_REFNO
AND LatestServicePointStay.MODIF_DTTM = ServicePointStay.MODIF_DTTM

/* 
	DG - 4 feb 2104 - the block of code below handles where we have data with the same modified datetime
	but it appears on two seperate extracts? We will opt for the latest extract where this occurs
*/ 


where
	 not exists
				(
					select
						1
					from
						dbo.SERVICE_POINT_STAYS_V01 ServicePointStayLater
					where
						ServicePointStayLater.SSTAY_REFNO = ServicePointStay.SSTAY_REFNO
					and ServicePointStayLater.MODIF_DTTM = LatestServicePointStay.MODIF_DTTM
					and	ServicePointStayLater.DATA_END_DATE > ServicePointStay.DATA_END_DATE
					--or
					--	(
					--		HealthOrganisationLater.HEORG_REFNO = HealthOrganisation.HEORG_REFNO
					--	and	HealthOrganisationLater.DATA_END_DATE = HealthOrganisation.DATA_END_DATE
					--	and HealthOrganisationLater.TISID > HealthOrganisation.TISID
					--	)
				)
﻿CREATE VIEW [dbo].[ProviderSpell]

AS

SELECT
	 Spell.TISID
	,Spell.[FILE_NAME]
	,Spell.DATA_START_DATE
	,Spell.DATA_END_DATE
	,Spell.TOTAL_ROW_COUNT
	,Spell.ROW_IDENTIFIER
	,Spell.PRVSP_REFNO
	,Spell.SPECT_REFNO
	,Spell.SPONT_REFNO
	,Spell.ADMET_REFNO
	,Spell.ADSOR_REFNO
	,Spell.PATCL_REFNO
	,Spell.READM_REFNO
	,Spell.CLEVL_REFNO
	,Spell.PROCA_REFNO
	,Spell.PMORT_REFNO
	,Spell.DISDE_REFNO
	,Spell.ADCAT_REFNO
	,Spell.DISMT_REFNO
	,Spell.PATNT_REFNO
	,Spell.SPELL_REFNO
	,Spell.REFPA_REFNO
	,Spell.FOLUP_REFNO
	,Spell.DISCH_REFNO
	,Spell.FIRST_PALL_YNUNK_REFNO
	,Spell.PREV_PALL_YNUNK_REFNO
	,Spell.ADMIT_DTTM
	,Spell.DISCH_DTTM
	,Spell.PROVD_REFNO
	,Spell.PURCH_REFNO
	,Spell.RESP_HEORG_REFNO
	,Spell.REFRL_REFNO
	,Spell.AEATT_REFNO
	,Spell.NOTAD_REFNO
	,Spell.FIRST_REGULAR
	,Spell.INMGT_REFNO
	,Spell.EXPDS_DTTM
	,Spell.[DISABLED]
	,Spell.CONFIDENTIAL
	,Spell.SILNT_REFRL
	,Spell.PRIOR_REFNO
	,Spell.ARCHV_FLAG
	,Spell.CREATE_DTTM
	,Spell.MODIF_DTTM
	,Spell.USER_CREATE
	,Spell.USER_MODIF
	,Spell.IDENTIFIER
	,Spell.STRAN_REFNO
	,Spell.PRIOR_POINTER
	,Spell.ADMDC_REFNO
	,Spell.ADMOF_REFNO
	,Spell.DTA_DTTM
	,Spell.WLIST_DTTM
	,Spell.FAST_TRACK
	,Spell.CONTR_REFNO
	,Spell.EXTERNAL_KEY
	,Spell.AGEBD_REFNO
	,Spell.ARCAR_HEORG_REFNO
	,Spell.DICAR_HEORG_REFNO
	,Spell.ARRIV_TRANS_REFNO
	,Spell.DEPRT_TRANS_REFNO
	,Spell.PACAC_REFNO
	,Spell.MATSP_REFNO
	,Spell.ADMIT_MENCT_REFNO
	,Spell.ADMIT_LEGSC_REFNO
	,Spell.DISCH_MENCT_REFNO
	,Spell.DISCH_LEGSC_REFNO
	,Spell.MHCEP_REFNO
	,Spell.LAST_PRCAE_IDENTIFIER
	,Spell.NO_FUNDS_FLAG
	,Spell.CCCCC_REFNO
	,Spell.PRVSN_START_FLAG
	,Spell.PRVSN_END_FLAG
	,Spell.CNTCT_PROCA_REFNO
	,Spell.MED_DISCH_DTTM
	,Spell.EXMCT_REFNO
	,Spell.EXMRE_REFNO
	,Spell.PMETD_REFNO
	,Spell.RFECO_REFNO
	,Spell.DSINF_REFNO
	,Spell.DSINF_TEXT
	,Spell.IDPAP_REFNO
	,Spell.READD_REFNO
	,Spell.DEPOSIT_AMOUNT
	,Spell.NILBYMOUTH_DTTM
	,Spell.THVISIT_YNUNK_REFNO
	,Spell.ADTAC_REFNO
	,Spell.FINSP_REFNO
	,Spell.EMPST_REFNO
	,Spell.PENST_REFNO
	,Spell.FIRST_PSYCH_DTTM
	,Spell.PREV_PSYCH_DTTM
	,Spell.CONSENT_FLAG
	,Spell.GP_ADVISE_FLAG
	,Spell.FORM_REF_NO
	,Spell.ADMISSION_WEIGHT
	,Spell.ICU_HOURS
	,Spell.CCU_HOURS
	,Spell.IRADM_REFNO
	,Spell.ADMIT_ADTPS_REFNO
	,Spell.DISCH_ADTPS_REFNO
	,Spell.ADMIT_SVCAT_REFNO
	,Spell.DISCH_SVCAT_REFNO
	,Spell.MECH_VENT_HRS
	,Spell.AMBULANCE_NUMBER
	,Spell.PRIOR_DAYS
	,Spell.RELATED_ADMISSION
	,Spell.ADMIT_PTCLS_REFNO
	,Spell.CFWD_DAYS
	,Spell.ACC_EXPIRY_DTTM
	,Spell.CNTRL_REFNO
	,Spell.CNTTP_REFNO
	,Spell.COMPS_REFNO
	,Spell.APPLY_VAT_TO_ALL
	,Spell.ASFSR_REFNO
	,Spell.HLINS_REFNO
	,Spell.ICLST_REFNO
	,Spell.DLNKC_REFNO
	,Spell.CARAV_REFNO
	,Spell.RFRFC_REFNO
	,Spell.ACLEV_REFNO
	,Spell.ADMIT_BARTHEL_SCORE
	,Spell.SEPAR_BARTHEL_SCORE
	,Spell.CLNSP_REFNO
	,Spell.ONSET_DATE
	,Spell.READM_REHAB_FLAG
	,Spell.SRTPC_REFNO
	,Spell.ADMIT_RUGADL_SCORE
	,Spell.SEPAR_RUGADL_SCORE
	,Spell.ICU_TIME
	,Spell.CCU_TIME
	,Spell.CROEP_REFNO
	,Spell.ADTPP_REFNO
	,Spell.ADTPP_COMMENT
	,Spell.NONINV_VENT_HRS
	,Spell.ACAST_REFNO
	,Spell.CRISIS_RES_SVC_FLAG
	,Spell.LEGST_REFNO
	,Spell.MENCT_REFNO
	,Spell.OWNER_HEORG_REFNO
	,Spell.TIS_FLAG
	,Spell.Org_ID
	,Spell.load_datetime
FROM 
	dbo.PROVIDER_SPELLS_V02 Spell
INNER JOIN (
				SELECT 
					 PRVSP_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.PROVIDER_SPELLS_V02
				GROUP BY PRVSP_REFNO
			)LatestSpell
ON LatestSpell.PRVSP_REFNO = Spell.PRVSP_REFNO
AND LatestSpell.MODIF_DTTM = Spell.MODIF_DTTM

/* 
	DG - 4 feb 2104 - the block of code below handles where we have data with the same modified datetime
	but it appears on two seperate extracts? We will opt for the latest extract where this occurs
*/ 


and
	 not exists
				(
					select
						1
					from
						dbo.PROVIDER_SPELLS_V02 SpellLater
					where
						SpellLater.PRVSP_REFNO = Spell.PRVSP_REFNO
					and SpellLater.MODIF_DTTM = LatestSpell.MODIF_DTTM
					and	SpellLater.DATA_END_DATE > Spell.DATA_END_DATE
					--or
					--	(
					--		HealthOrganisationLater.HEORG_REFNO = HealthOrganisation.HEORG_REFNO
					--	and	HealthOrganisationLater.DATA_END_DATE = HealthOrganisation.DATA_END_DATE
					--	and HealthOrganisationLater.TISID > HealthOrganisation.TISID
					--	)
				)
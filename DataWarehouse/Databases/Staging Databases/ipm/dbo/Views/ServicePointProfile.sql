﻿CREATE VIEW [dbo].[ServicePointProfile]

AS

SELECT
	 ServicePointProfile.TISID
	,ServicePointProfile.[FILE_NAME]
	,ServicePointProfile.DATA_START_DATE
	,ServicePointProfile.DATA_END_DATE
	,ServicePointProfile.TOTAL_ROW_COUNT
	,ServicePointProfile.ROW_IDENTIFIER
	,ServicePointProfile.SPPRO_REFNO
	,ServicePointProfile.SPONT_REFNO
	,ServicePointProfile.SPBAY_REFNO
	,ServicePointProfile.SPBED_REFNO
	,ServicePointProfile.SPECT_REFNO
	,ServicePointProfile.PROCA_REFNO
	,ServicePointProfile.BEDSS_REFNO
	,ServicePointProfile.RFVAL_REFNO
	,ServicePointProfile.RULES_REFNO
	,ServicePointProfile.OPERATOR
	,ServicePointProfile.VALUE
	,ServicePointProfile.BDCAT_REFNO
	,ServicePointProfile.START_DTTM
	,ServicePointProfile.END_DTTM
	,ServicePointProfile.CREATE_DTTM
	,ServicePointProfile.MODIF_DTTM
	,ServicePointProfile.USER_CREATE
	,ServicePointProfile.USER_MODIF
	,ServicePointProfile.ARCHV_FLAG
	,ServicePointProfile.STRAN_REFNO
	,ServicePointProfile.EXTERNAL_KEY
	,ServicePointProfile.SPCHR_REFNO
	,ServicePointProfile.WDPRO_REFNO
	,ServicePointProfile.PLANNED_DTTM
	,ServicePointProfile.OWNER_HEORG_REFNO
	,ServicePointProfile.TIS_FLAG
	,ServicePointProfile.load_datetime
FROM 
	dbo.SERVICE_POINT_PROFILES_V01 ServicePointProfile
INNER JOIN (
				SELECT 
					 SPPRO_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.SERVICE_POINT_PROFILES_V01
				GROUP BY SPPRO_REFNO
			)LatestServicePointProfile
ON LatestServicePointProfile.SPPRO_REFNO = ServicePointProfile.SPPRO_REFNO
AND LatestServicePointProfile.MODIF_DTTM = ServicePointProfile.MODIF_DTTM
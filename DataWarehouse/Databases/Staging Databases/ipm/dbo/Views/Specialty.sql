﻿CREATE VIEW [dbo].[Specialty]

AS

SELECT 
	   Specialty.TISID
      ,Specialty.[FILE_NAME]
      ,Specialty.DATA_START_DATE
      ,Specialty.DATA_END_DATE
      ,Specialty.TOTAL_ROW_COUNT
      ,Specialty.ROW_IDENTIFIER
      ,Specialty.SPECT_REFNO
      ,Specialty.SPECT_REFNO_MAIN_IDENT
      ,Specialty.MAIN_IDENT
      ,Specialty.[DESCRIPTION]
      ,Specialty.PARNT_REFNO
      ,Specialty.DIVSN_REFNO
      ,Specialty.DIVSN_REFNO_MAIN_CODE
      ,Specialty.DIVSN_REFNO_DESCRIPTION
      ,Specialty.CREATE_DTTM
      ,Specialty.MODIF_DTTM
      ,Specialty.USER_CREATE
      ,Specialty.USER_MODIF
      ,Specialty.ARCHV_FLAG
      ,Specialty.START_DTTM
      ,Specialty.END_DTTM
      ,Specialty.OWNER_HEORG_REFNO
      ,Specialty.OWNER_HEORG_REFNO_MAIN_IDENT
      ,Specialty.TIS_FLAG
      ,Specialty.load_datetime

FROM 
	dbo.SPECIALTIES_V01 Specialty
INNER JOIN (
				SELECT 
					 SPECT_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.SPECIALTIES_V01
				GROUP BY SPECT_REFNO
			)LatestSpecialty
ON Specialty.SPECT_REFNO = LatestSpecialty.SPECT_REFNO
AND Specialty.MODIF_DTTM = LatestSpecialty.MODIF_DTTM
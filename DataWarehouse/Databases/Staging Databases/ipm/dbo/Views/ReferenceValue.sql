﻿CREATE VIEW [dbo].[ReferenceValue]

AS


SELECT
	 TISID
	,ReferenceValue.[FILE_NAME]
	,ReferenceValue.DATA_START_DATE
	,ReferenceValue.DATA_END_DATE
	,ReferenceValue.TOTAL_ROW_COUNT
	,ReferenceValue.ROW_IDENTIFIER
	,ReferenceValue.RFVAL_REFNO
	,ReferenceValue.RFVDM_CODE
	,ReferenceValue.[DESCRIPTION]
	,ReferenceValue.MAIN_CODE
	,ReferenceValue.PARNT_REFNO
	,ReferenceValue.SMODE_VALUE
	,ReferenceValue.DEFAULT_VALUE
	,ReferenceValue.SORT_ORDER
	,ReferenceValue.SELECT_VALUE
	,ReferenceValue.DISPLAY_VALUE
	,ReferenceValue.CREATE_DTTM
	,ReferenceValue.MODIF_DTTM
	,ReferenceValue.USER_CREATE
	,ReferenceValue.USER_MODIF
	,ReferenceValue.RFTYP_CODE
	,ReferenceValue.ARCHV_FLAG
	,ReferenceValue.STRAN_REFNO
	,ReferenceValue.PRIOR_POINTER
	,ReferenceValue.EXTERNAL_KEY
	,ReferenceValue.START_DTTM
	,ReferenceValue.END_DTTM
	,ReferenceValue.LOW_VALUE
	,ReferenceValue.HIGH_VALUE
	,ReferenceValue.PROCESS_MODULE_ID
	,ReferenceValue.SCLVL_REFNO
	,ReferenceValue.SYN_CODE
	,ReferenceValue.OWNER_HEORG_REFNO
	,ReferenceValue.TIS_FLAG
	,ReferenceValue.load_datetime
	
FROM 
	dbo.REFERENCE_VALUES_V01 ReferenceValue
INNER JOIN (
				SELECT 
					 RFVAL_REFNO
					,MAX(MODIF_DTTM) MODIF_DTTM
				FROM 
					dbo.REFERENCE_VALUES_V01
				GROUP BY RFVAL_REFNO
			)LatestReferenceValue
ON ReferenceValue.RFVAL_REFNO = LatestReferenceValue.RFVAL_REFNO
AND ReferenceValue.MODIF_DTTM = LatestReferenceValue.MODIF_DTTM
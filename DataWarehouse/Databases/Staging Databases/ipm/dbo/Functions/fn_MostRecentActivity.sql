﻿CREATE FUNCTION [dbo].[fn_MostRecentActivity]
(
	@StartRange DATETIME,
	@EndRange DATETIME,
	@ScheduleType int 
	
) RETURNS @MostRecentActivity TABLE

(
	REFRL_REFNO numeric(10,0) NULL,
	MostRecentActivity datetime
)


AS 
BEGIN
    
DECLARE

	@REFRL_REFNO numeric(10,0),
	@SCHDL_REFNO numeric(10,0),
	@START_DTTM datetime

if @ScheduleType = 1 --attendance

INSERT INTO @MostRecentActivity (REFRL_REFNO, MostRecentActivity)

	SELECT   
 		fs.REFRL_REFNO,
		MAX(fs.START_DTTM) 
		
	FROM
		dbo.Schedule fs
	LEFT JOIN dbo.TestPatient tp
		ON fs.PATNT_REFNO = tp.PATNT_REFNO
	WHERE
	
			tp.PATNT_REFNO IS NULL -- exclude test patients
		AND
			fs.ARCHV_FLAG = 'N'
		AND
			(
					fs.SCTYP_REFNO = '1468'
				OR
					fs.SCTYP_REFNO = '1470'
			)
		AND
			( --extracting attendances only
					(
							fs.SCTYP_REFNO = 1470					-- Clinic Contact
						AND
							fs.ATTND_REFNO IN (
								357,								-- Attended On Time
								2868								-- Patient Late / Seen
							)
					)
				OR
					(
							fs.SCTYP_REFNO = 1468					-- Community Contact
						AND
							fs.SCOCM_REFNO NOT IN (
								1459,								-- Patient Died
								2001651,							-- Cancelled
								1457								-- Did Not Attend
							)
						AND
							fs.ARRIVED_DTTM IS NOT NULL
						AND
							fs.DEPARTED_DTTM IS NOT NULL
					)
			)
		AND
			fs.CONTY_REFNO <> 2004177
		
		AND
			fs.START_DTTM < getdate()
		
	GROUP BY
		fs.REFRL_REFNO
			
else if @ScheduleType = 2 --DNA

INSERT INTO @MostRecentActivity (REFRL_REFNO, MostRecentActivity)

	SELECT   
 		fs.REFRL_REFNO,
		MAX(fs.START_DTTM) 
		
		FROM
			dbo.Schedule fs
		LEFT JOIN dbo.TestPatient tp
			ON fs.PATNT_REFNO = tp.PATNT_REFNO
		WHERE
		
				tp.PATNT_REFNO IS NULL -- exclude test patients
			AND
				fs.ARCHV_FLAG = 'N'
			AND
				(
						fs.SCTYP_REFNO = '1468'
					OR
						fs.SCTYP_REFNO = '1470'
				)
			AND
				( --extracting attendances only
						(
								fs.SCTYP_REFNO = 1470					-- Clinic Contact
							AND
								fs.ATTND_REFNO IN (
									358,								-- Did Not Attend
									2000724								-- Patient Late / Not Seen
								)
						)
					OR
						(
								fs.SCTYP_REFNO = 1468					-- Community Contact
							AND
								fs.SCOCM_REFNO  IN (
									1457								-- Did Not Attend
								)
						)
				)
			AND
				fs.CONTY_REFNO <> 2004177
			AND
				fs.START_DTTM < getdate()	
				
			
		GROUP BY
			fs.REFRL_REFNO

else if @ScheduleType = 3 --Historical Activity with no Outcome

--In this context (Historic Activity) return schedules with n ooutcome

INSERT INTO @MostRecentActivity (REFRL_REFNO, MostRecentActivity)

	SELECT   
 		fs.REFRL_REFNO,
		MAX(fs.START_DTTM) 
		
		FROM
			dbo.Schedule fs
		LEFT JOIN dbo.TestPatient tp
			ON fs.PATNT_REFNO = tp.PATNT_REFNO
		WHERE
		
				tp.PATNT_REFNO IS NULL -- exclude test patients
			AND
				fs.ARCHV_FLAG = 'N'
			AND
				(
						fs.SCTYP_REFNO = '1468'
					OR
						fs.SCTYP_REFNO = '1470'
				)
			AND
				( 
						(
								fs.SCTYP_REFNO = 1470					-- Clinic Contact
							AND
								fs.ATTND_REFNO IN (
									45									-- not specified
								)
						)
					OR
						(
								fs.SCTYP_REFNO = 1468					-- Community Contact
							AND
								fs.ARRIVED_DTTM IS NULL
							AND
								fs.SCOCM_REFNO NOT IN (
									1457,				-- DNA
									1459,				-- Patient died
									2001651			    -- Cancellation
								)
							AND
								fs.CANCR_DTTM IS NULL
						)
				)
			AND
				fs.CONTY_REFNO <> 2004177
			AND
				fs.START_DTTM < getdate()
				
			
		GROUP BY
			fs.REFRL_REFNO

else if @ScheduleType = 4 --Clock Reset Event / Previous Month End Snapshot

--DNA or Patient Cancellation will reset clock
--called in conjunction with Previous Month End Snapshot position

INSERT INTO @MostRecentActivity (REFRL_REFNO, MostRecentActivity)

	SELECT   
 		fs.REFRL_REFNO,
		MAX(fs.START_DTTM) 
		
		FROM
			dbo.Schedule fs
		LEFT JOIN dbo.TestPatient tp
			ON fs.PATNT_REFNO = tp.PATNT_REFNO
		WHERE
		
				tp.PATNT_REFNO IS NULL -- exclude test patients
			AND
				fs.ARCHV_FLAG = 'N'
			AND
				(
						fs.SCTYP_REFNO = '1468'
					OR
						fs.SCTYP_REFNO = '1470'
				)
			
			
			AND
			
			(
				--Patient Cancellation....
				(
					( --extracting attendances only
							(
									fs.SCTYP_REFNO = 1470					-- Clinic Contact
								AND
									(
									
											ATTND_REFNO = '2004301' --schedule attendance patient cancelled

										or 
											( CANCB_REFNO	= '5477' --schedule cancelled by Patient/Carer
												and CANCR_DTTM is not null 
												--P.McNulty 1.0.1 remove
												--and CANCR_DTTM < START_DTTM
											)
											
									)
							)
						OR
							(
									fs.SCTYP_REFNO = 1468					-- Community Contact
								AND
									(
									
											SCOCM_REFNO = '2001651' --schedule outcome cancelled

										or 
											( CANCB_REFNO	= '5477' --schedule cancelled by Patient/Carer
												and CANCR_DTTM is not null 
												--P.McNulty 1.0.1 remove
												--and CANCR_DTTM < START_DTTM
											)
											
									)
							
							)
					)
				)
								
				OR

				--...DNA
				(
					( --extracting attendances only
							(
									fs.SCTYP_REFNO = 1470					-- Clinic Contact
								AND
									fs.ATTND_REFNO IN (
										358,								-- Did Not Attend
										2000724								-- Patient Late / Not Seen
									)
							)
						OR
							(
									fs.SCTYP_REFNO = 1468					-- Community Contact
								AND
									fs.SCOCM_REFNO  IN (
										1457								-- Did Not Attend
									)
							)
					)
				)
				
			)
			
			AND
				fs.CONTY_REFNO <> 2004177
			AND
				--function argument Clock Reset event' is only being called
				--currently from dbo.[sp_Monthly_Diag_Return_Waiting_List]
				--which always sends the @EndRange parm so...
				fs.START_DTTM < @EndRange	
			
			
		GROUP BY
			fs.REFRL_REFNO



	
    RETURN;
END; 





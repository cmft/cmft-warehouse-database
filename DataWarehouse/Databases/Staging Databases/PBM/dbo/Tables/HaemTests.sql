﻿CREATE TABLE [dbo].[HaemTests](
	[Consultant Specialty Code] [nvarchar](255) NULL,
	[Consultant] [nvarchar](255) NULL,
	[Consultant Code] [nvarchar](255) NULL,
	[New Nhs Number] [nvarchar](255) NULL,
	[Hospital Number] [nvarchar](255) NULL,
	[Patient Forename] [nvarchar](255) NULL,
	[Patient Surname] [nvarchar](255) NULL,
	[Date Of Birth] [datetime] NULL,
	[Hospital Code] [nvarchar](255) NULL,
	[Location Code] [nvarchar](255) NULL,
	[Request Date] [datetime] NULL,
	[Time Of Request] [datetime] NULL,
	[Specimen No] [nvarchar](255) NULL,
	[Test Code] [nvarchar](255) NULL,
	[Test Code Desc] [nvarchar](255) NULL,
	[Result] [nvarchar](255) NULL,
	[Reason For Request] [nvarchar](255) NULL
) ON [PRIMARY]
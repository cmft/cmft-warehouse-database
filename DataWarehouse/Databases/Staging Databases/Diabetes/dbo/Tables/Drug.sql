﻿CREATE TABLE [dbo].[Drug](
	[DrugCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[DrugType] [varchar](30) NULL,
	[DrugName] [varchar](70) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LabUploadLog](
	[LabUploadLogCode] [varchar](8) NOT NULL,
	[FailedReason] [varchar](200) NULL,
	[FailedTask] [varchar](50) NULL,
	[FailedDateTime] [datetime] NULL,
	[FailedInterface] [varchar](50) NULL
) ON [PRIMARY]
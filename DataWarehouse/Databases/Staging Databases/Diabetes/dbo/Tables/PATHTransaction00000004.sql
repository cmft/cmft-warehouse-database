﻿CREATE TABLE [dbo].[PATHTransaction00000004](
	[UniqueID] [uniqueidentifier] NOT NULL,
	[SequenceID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionDate] [datetime] NULL,
	[TransactionTime] [datetime] NULL,
	[TransactionRecord] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
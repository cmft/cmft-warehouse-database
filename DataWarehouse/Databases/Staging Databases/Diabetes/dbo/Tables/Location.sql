﻿CREATE TABLE [dbo].[Location](
	[LocationCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[LocationType] [varchar](20) NULL,
	[LocationNumber] [varchar](10) NULL,
	[LocationKnownAs] [varchar](30) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[Address3] [varchar](30) NULL,
	[Address4] [varchar](30) NULL,
	[PostCode] [varchar](10) NULL,
	[Phone] [varchar](20) NULL,
	[NHSOrganisationCode] [varchar](20) NULL,
	[PaediatricSite] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
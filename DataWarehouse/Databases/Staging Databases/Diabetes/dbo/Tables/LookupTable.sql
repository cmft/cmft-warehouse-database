﻿CREATE TABLE [dbo].[LookupTable](
	[LookupTableOrder] [float] NOT NULL,
	[FieldName] [varchar](100) NULL,
	[FieldSize] [float] NULL,
	[AreaUsedIn] [varchar](40) NULL,
	[AreaUsedIn1] [varchar](40) NULL,
	[AreaUsedIn2] [varchar](40) NULL,
	[Description] [varchar](30) NULL,
	[Modifiable] [bit] NOT NULL DEFAULT (0),
	[TablesWhereUsed] [text] NULL,
	[FormsWhereUsed] [text] NULL,
	[ItemValues] [text] NULL,
	[Comments] [varchar](150) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
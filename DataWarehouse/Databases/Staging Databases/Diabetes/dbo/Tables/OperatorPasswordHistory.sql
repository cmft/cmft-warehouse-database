﻿CREATE TABLE [dbo].[OperatorPasswordHistory](
	[OperatorPasswordHistoryCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[OperatorCode] [varchar](8) NULL,
	[Password] [varchar](20) NULL,
	[PasswordDate] [datetime] NULL
) ON [PRIMARY]
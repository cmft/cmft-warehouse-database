﻿CREATE TABLE [dbo].[GP](
	[GPCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[SurgeryCode] [varchar](8) NULL,
	[Surname] [varchar](30) NULL,
	[Forenames] [varchar](20) NULL,
	[Initials] [varchar](10) NULL,
	[Title] [varchar](10) NULL,
	[FullName] [varchar](100) NULL,
	[GMCNumber] [varchar](12) NULL,
	[NationalGPNumber] [varchar](10) NULL,
	[InternalPASGPCode] [varchar](6) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientDrug](
	[PatientDrugCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PatientCode] [varchar](8) NULL,
	[DrugType] [varchar](30) NULL,
	[DrugCode] [varchar](8) NULL,
	[DrugComments] [varchar](100) NULL,
	[DrugCurrent] [float] NULL,
	[DrugStartDate] [datetime] NULL,
	[DrugStartDateApprox] [bit] NULL CONSTRAINT [DF_PatientDrug_DrugStartDateApprox]  DEFAULT (0),
	[DrugStopDate] [datetime] NULL,
	[DrugStopDateApprox] [bit] NULL CONSTRAINT [DF_PatientDrug_DrugStopDateApprox]  DEFAULT (0)
) ON [PRIMARY]
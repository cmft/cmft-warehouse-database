﻿CREATE TABLE [dbo].[EpisodeEyePhoto](
	[EpisodeEyePhotoCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[EpisodeCode] [varchar](8) NULL,
	[PatientCode] [varchar](8) NULL,
	[RecordAddedDate] [datetime] NULL,
	[RecordAddedCode] [varchar](8) NULL,
	[RecordAddedName] [varchar](30) NULL,
	[RecordAmendedDate] [datetime] NULL,
	[RecordAmendedCode] [varchar](8) NULL,
	[RecordAmendedName] [varchar](30) NULL,
	[EyePhotoDate] [datetime] NULL,
	[EyeLeftPhoto] [image] NULL,
	[EyeRightPhoto] [image] NULL,
	[EyePhotoComments] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
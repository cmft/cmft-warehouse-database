﻿CREATE TABLE [dbo].[EpisodeEducation](
	[EpisodeCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PatientCode] [varchar](8) NULL,
	[EpisodeAge] [float] NULL,
	[EpisodeDuration] [float] NULL,
	[Weight] [float] NULL,
	[Height] [float] NULL,
	[BMI] [float] NULL,
	[DiabeticType] [varchar](15) NULL,
	[DietTreated] [bit] NOT NULL,
	[InsulinTreated] [bit] NOT NULL,
	[TabletTreated] [bit] NOT NULL,
	[DiabeticClassification] [varchar](25) NULL,
	[EducationType] [varchar](50) NULL,
	[Injection1] [varchar](30) NULL,
	[Injection2] [varchar](30) NULL,
	[Injection3] [varchar](30) NULL,
	[Injection1DoseA] [varchar](10) NULL,
	[Injection1DoseB] [varchar](10) NULL,
	[Injection1DoseC] [varchar](10) NULL,
	[Injection1DoseD] [varchar](10) NULL,
	[Injection2DoseA] [varchar](10) NULL,
	[Injection2DoseB] [varchar](10) NULL,
	[Injection2DoseC] [varchar](10) NULL,
	[Injection2DoseD] [varchar](10) NULL,
	[Injection3DoseA] [varchar](10) NULL,
	[Injection3DoseB] [varchar](10) NULL,
	[Injection3DoseC] [varchar](10) NULL,
	[Injection3DoseD] [varchar](10) NULL,
	[InsulinByPen] [bit] NOT NULL,
	[CSIIDose] [float] NULL,
	[InjectionComment] [varchar](255) NULL,
	[InjectionSites] [varchar](15) NULL,
	[Tablet1] [varchar](30) NULL,
	[Tablet2] [varchar](30) NULL,
	[Tablet3] [varchar](30) NULL,
	[Tablet1DoseA] [varchar](10) NULL,
	[Tablet1DoseB] [varchar](10) NULL,
	[Tablet1DoseC] [varchar](10) NULL,
	[Tablet1DoseD] [varchar](10) NULL,
	[Tablet2DoseA] [varchar](10) NULL,
	[Tablet2DoseB] [varchar](10) NULL,
	[Tablet2DoseC] [varchar](10) NULL,
	[Tablet2DoseD] [varchar](10) NULL,
	[Tablet3DoseA] [varchar](10) NULL,
	[Tablet3DoseB] [varchar](10) NULL,
	[Tablet3DoseC] [varchar](10) NULL,
	[Tablet3DoseD] [varchar](10) NULL,
	[TabletComment] [varchar](255) NULL,
	[GeneralComments] [text] NULL,
	[GPComments] [text] NULL,
	[CCName] [varchar](255) NULL,
	[Type1Symptoms] [varchar](50) NULL,
	[Type1HbA1c] [varchar](50) NULL,
	[Type1Footcare] [varchar](50) NULL,
	[Type1EyeScreening] [varchar](50) NULL,
	[Type1RenalCare] [varchar](50) NULL,
	[Type1CardiacProblems] [varchar](50) NULL,
	[Type1ErectileDysfunction] [varchar](50) NULL,
	[Type1Monitoring] [varchar](50) NULL,
	[Type1InterpretingResults] [varchar](50) NULL,
	[Type1Diet] [varchar](50) NULL,
	[Type1InsulinAction] [varchar](50) NULL,
	[Type1InsulinAdmin] [varchar](50) NULL,
	[Type1DosageAdjust] [varchar](50) NULL,
	[Type1InsulinRegimes] [varchar](50) NULL,
	[Type1PenDevices] [varchar](50) NULL,
	[Type1DisposalSharps] [varchar](50) NULL,
	[Type1RotationSites] [varchar](50) NULL,
	[Type1IDCard] [varchar](50) NULL,
	[Type1Hypoglycaemia] [varchar](50) NULL,
	[Type1Alcohol] [varchar](50) NULL,
	[Type1Exercise] [varchar](50) NULL,
	[Type1Driving] [varchar](50) NULL,
	[Type1Benefits] [varchar](50) NULL,
	[Type1Travel] [varchar](50) NULL,
	[Type1Illness] [varchar](50) NULL,
	[Type1BDA] [varchar](50) NULL,
	[Type1PregnancyCounsel] [varchar](50) NULL,
	[Type1ContactNo] [varchar](12) NULL,
	[Type1Comments] [text] NULL,
	[Type2Symptoms] [varchar](50) NULL,
	[Type2HbA1c] [varchar](50) NULL,
	[Type2Footcare] [varchar](50) NULL,
	[Type2EyeScreening] [varchar](50) NULL,
	[Type2RenalCare] [varchar](50) NULL,
	[Type2CardiacProblems] [varchar](50) NULL,
	[Type2ErectileDysfunction] [varchar](50) NULL,
	[Type2Monitoring] [varchar](50) NULL,
	[Type2InterpretingResults] [varchar](50) NULL,
	[Type2Diet] [varchar](50) NULL,
	[Type2MedicationAction] [varchar](50) NULL,
	[Type2MedicationTiming] [varchar](50) NULL,
	[Type2MedicationDosage] [varchar](50) NULL,
	[Type2MedicationSideEffects] [varchar](50) NULL,
	[Type2RXOptions] [varchar](50) NULL,
	[Type2IDCard] [varchar](50) NULL,
	[Type2ConvertToInsulinDate] [datetime] NULL,
	[Type2Alcohol] [varchar](50) NULL,
	[Type2Exercise] [varchar](50) NULL,
	[Type2Driving] [varchar](50) NULL,
	[Type2Benefits] [varchar](50) NULL,
	[Type2Travel] [varchar](50) NULL,
	[Type2Illness] [varchar](50) NULL,
	[Type2BDA] [varchar](50) NULL,
	[Type2Pregnancy] [varchar](50) NULL,
	[Type2Contraception] [varchar](50) NULL,
	[Type2ContactNo] [varchar](12) NULL,
	[Type2Comments] [text] NULL,
	[EducationReview] [bit] NOT NULL DEFAULT (0),
	[EducationReviewDate] [datetime] NULL,
	[EducationStructured] [bit] NOT NULL DEFAULT (0),
	[EducationStructuredDate] [datetime] NULL,
	[EducationAttended] [bit] NOT NULL DEFAULT (0),
	[EducationAttendedDate] [datetime] NULL,
	[Type1WeightLoss] [varchar](20) NULL,
	[Type1MIRAdvice] [varchar](20) NULL,
	[Type1CarbCounting] [varchar](20) NULL,
	[Type1LipidLowering] [varchar](20) NULL,
	[Type1SodiumMod] [varchar](20) NULL,
	[Type1LowK] [varchar](20) NULL,
	[Type1HealthyEating] [varchar](20) NULL,
	[Type1GlutenFree] [varchar](20) NULL,
	[Type1NutritionSupport] [varchar](20) NULL,
	[Type1FoodExercise] [varchar](20) NULL,
	[Type2WeightLoss] [varchar](20) NULL,
	[Type2MIRAdvice] [varchar](20) NULL,
	[Type2CarbCounting] [varchar](20) NULL,
	[Type2LipidLowering] [varchar](20) NULL,
	[Type2SodiumMod] [varchar](20) NULL,
	[Type2LowK] [varchar](20) NULL,
	[Type2HealthyEating] [varchar](20) NULL,
	[Type2GlutenFree] [varchar](20) NULL,
	[Type2NutritionSupport] [varchar](20) NULL,
	[Type2FoodExercise] [varchar](20) NULL,
	[Type1ManagingKetones] [varchar](20) NULL,
	[Type1InsulinPump] [varchar](20) NULL,
	[Type1ExerciseInsulin] [varchar](20) NULL,
	[BPSystolicSitting] [float] NULL,
	[BPDiastolicSitting] [float] NULL,
	[BPSystolicStanding] [float] NULL,
	[BPDiastolicStanding] [float] NULL,
	[SEPProgramme] [varchar](30) NULL,
	[SEPOfferResponse] [varchar](30) NULL,
	[SEPDateCompleted] [datetime] NULL,
	[SEPAttendance] [varchar](20) NULL,
	[SEPDateCompetent] [datetime] NULL,
	[SEPOperatorCode] [varchar](8) NULL,
	[SEPComments] [text] NULL,
	[Waist] [float] NULL,
	[Hip] [float] NULL,
	[WaistHipRatio] [float] NULL,
	[InsulinPumpDAFNE] [varchar](15) NULL,
	[Injection4] [varchar](30) NULL,
	[Injection4DoseA] [varchar](10) NULL,
	[Injection4DoseB] [varchar](10) NULL,
	[Injection4DoseC] [varchar](10) NULL,
	[Injection4DoseD] [varchar](10) NULL,
	[Injection5] [varchar](30) NULL,
	[Injection5DoseA] [varchar](10) NULL,
	[Injection5DoseB] [varchar](10) NULL,
	[Injection5DoseC] [varchar](10) NULL,
	[Injection5DoseD] [varchar](10) NULL,
	[Tablet4] [varchar](30) NULL,
	[Tablet4DoseA] [varchar](10) NULL,
	[Tablet4DoseB] [varchar](10) NULL,
	[Tablet4DoseC] [varchar](10) NULL,
	[Tablet4DoseD] [varchar](10) NULL,
	[Tablet5] [varchar](30) NULL,
	[Tablet5DoseA] [varchar](10) NULL,
	[Tablet5DoseB] [varchar](10) NULL,
	[Tablet5DoseC] [varchar](10) NULL,
	[Tablet5DoseD] [varchar](10) NULL,
	[Tablet6] [varchar](30) NULL,
	[Tablet6DoseA] [varchar](10) NULL,
	[Tablet6DoseB] [varchar](10) NULL,
	[Tablet6DoseC] [varchar](10) NULL,
	[Tablet6DoseD] [varchar](10) NULL,
	[Injection1Type] [bit] NOT NULL DEFAULT (0),
	[Injection2Type] [bit] NOT NULL DEFAULT (0),
	[Injection3Type] [bit] NOT NULL DEFAULT (0),
	[Injection4Type] [bit] NOT NULL DEFAULT (0),
	[Injection5Type] [bit] NOT NULL DEFAULT (0),
	[InjectionTreated] [bit] NOT NULL DEFAULT (0),
	[Type1HbA1cIFCC] [varchar](50) NULL,
	[Type2HbA1cIFCC] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
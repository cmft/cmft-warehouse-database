﻿CREATE TABLE [dbo].[UploadInterfaceDetails](
	[UploadInterfaceDetailsCode] [varchar](8) NOT NULL,
	[InterfaceStatus] [varchar](50) NULL,
	[InterfaceDetails] [text] NULL,
	[InterfaceTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
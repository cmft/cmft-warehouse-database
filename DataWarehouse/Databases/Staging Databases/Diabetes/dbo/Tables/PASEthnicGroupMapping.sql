﻿CREATE TABLE [dbo].[PASEthnicGroupMapping](
	[PASEthnicGroupMappingCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PASEthnicGroup] [varchar](50) NULL,
	[PASEthnicGroupCode] [varchar](5) NULL,
	[DiamondEthnicGroup] [varchar](40) NULL
) ON [PRIMARY]
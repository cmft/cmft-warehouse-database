﻿CREATE TABLE [dbo].[Optician](
	[OpticianCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[KnownAs] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[PostCode] [varchar](10) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[DHANumber] [varchar](10) NULL,
	[PracticeNumber] [varchar](10) NULL
) ON [PRIMARY]
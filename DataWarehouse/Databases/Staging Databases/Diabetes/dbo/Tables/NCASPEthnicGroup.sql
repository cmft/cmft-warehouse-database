﻿CREATE TABLE [dbo].[NCASPEthnicGroup](
	[NCASPEthnicGroupCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[EthnicGroupCode] [varchar](2) NULL,
	[EthnicGroupDescription] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[FileServerStorage](
	[FileServerStorageCode] [varchar](8) NOT NULL,
	[DataExport] [text] NULL,
	[OperatorCode] [varchar](8) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
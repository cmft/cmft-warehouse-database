﻿CREATE TABLE [dbo].[SMSLog](
	[SMSLogCode] [varchar](8) NOT NULL,
	[SentDate] [datetime] NULL,
	[MobilePhoneNumber] [varchar](20) NULL,
	[MobilePhoneCountry] [varchar](5) NULL,
	[OperatorCode] [varchar](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UploadFailed](
	[UploadFailedCode] [varchar](8) NOT NULL,
	[FailedCode] [float] NULL,
	[FailedReason] [varchar](50) NULL,
	[FailedText] [varchar](50) NULL,
	[RecordString] [text] NULL,
	[FileName] [varchar](12) NULL,
	[SystemDate] [datetime] NULL,
	[SystemTime] [datetime] NULL,
	[LineNumber] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
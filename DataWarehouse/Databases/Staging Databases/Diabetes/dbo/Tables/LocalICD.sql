﻿CREATE TABLE [dbo].[LocalICD](
	[LocalICDCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[ICDCode] [varchar](4) NULL,
	[ICDDescription] [varchar](67) NULL,
	[OnsetMapping1] [varchar](30) NULL,
	[OnsetMapping2] [varchar](30) NULL,
	[CardioRisk] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UserAuditTrail](
	[UserAuditTrailCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[TableCode] [varchar](8) NULL,
	[TableName] [varchar](50) NULL,
	[OperatorCode] [varchar](8) NULL,
	[OperatorName] [varchar](50) NULL,
	[ViewType] [varchar](50) NULL,
	[ViewDate] [datetime] NULL
) ON [PRIMARY]
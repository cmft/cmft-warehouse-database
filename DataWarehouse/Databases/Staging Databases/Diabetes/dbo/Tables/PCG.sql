﻿CREATE TABLE [dbo].[PCG](
	[PCGCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PCGPASId] [varchar](6) NULL,
	[PCG] [varchar](30) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[Address3] [varchar](30) NULL,
	[Address4] [varchar](30) NULL,
	[PostCode] [varchar](10) NULL,
	[Phone] [varchar](20) NULL,
	[PCGPopulation] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EpisodePEATS](
	[EpisodePeatsCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PatientCode] [varchar](8) NULL,
	[OperatorCode] [varchar](8) NULL,
	[RecordAddedDate] [datetime] NULL,
	[RecordAddedCode] [varchar](8) NULL,
	[DiabeticType] [varchar](20) NULL,
	[QuestionsAnswered] [float] NULL,
	[NumberRight] [float] NULL,
	[NumberWrong] [float] NULL,
	[PeatsScore] [float] NULL,
	[MaximumPeatsScore] [float] NULL,
	[QuestionList] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
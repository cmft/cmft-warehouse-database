﻿CREATE TABLE [dbo].[ResearchProject](
	[ResearchProjectCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[ResearchIdentifier] [varchar](30) NULL,
	[ResearchDescription] [varchar](255) NULL,
	[ResearchStartDate] [datetime] NULL,
	[ResearchEndDate] [datetime] NULL,
	[ResearchInvestigatorCode] [varchar](8) NULL,
	[ResearchDoctorCode] [varchar](8) NULL,
	[ResearchNurse1Code] [varchar](8) NULL,
	[ResearchNurse2Code] [varchar](8) NULL,
	[Other1Code] [varchar](8) NULL,
	[Other2Code] [varchar](8) NULL
) ON [PRIMARY]
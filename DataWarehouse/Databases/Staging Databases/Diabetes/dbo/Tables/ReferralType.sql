﻿CREATE TABLE [dbo].[ReferralType](
	[ReferralTypeCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[ReferralType] [varchar](25) NULL,
	[PrintEyes] [bit] NOT NULL,
	[PrintFeet] [bit] NOT NULL,
	[PrintTreatment] [bit] NOT NULL
) ON [PRIMARY]
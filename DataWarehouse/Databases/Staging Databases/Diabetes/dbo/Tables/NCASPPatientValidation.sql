﻿CREATE TABLE [dbo].[NCASPPatientValidation](
	[NCASPPatientValidationCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PatientCode] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[Forenames] [varchar](50) NULL,
	[HospitalNumber] [varchar](50) NULL,
	[NHSNumber] [float] NULL,
	[ValidationText] [text] NULL,
	[InRange] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[SMSTemplate](
	[SMSTemplateCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[SMSTemplateName] [varchar](100) NULL,
	[SMSTemplateText] [text] NULL,
	[SMSTemplateDefault] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
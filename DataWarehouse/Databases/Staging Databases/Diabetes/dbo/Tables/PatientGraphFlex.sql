﻿CREATE TABLE [dbo].[PatientGraphFlex](
	[PatientGraphCode] [varchar](8) NOT NULL,
	[PatientCode] [varchar](8) NULL,
	[OperatorCode] [varchar](8) NULL,
	[Month] [float] NULL DEFAULT (0),
	[Year] [float] NULL DEFAULT (0),
	[XLabel] [varchar](20) NULL,
	[YValue1] [varchar](50) NULL
) ON [PRIMARY]
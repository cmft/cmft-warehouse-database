﻿CREATE TABLE [dbo].[Surgery](
	[SurgeryCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PCGCode] [varchar](8) NULL,
	[KnownAs] [varchar](30) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[Address3] [varchar](30) NULL,
	[Address4] [varchar](30) NULL,
	[PostCode] [varchar](10) NULL,
	[Phone] [varchar](20) NULL,
	[FHSANumber] [varchar](10) NULL,
	[PracticeNumber] [varchar](10) NULL,
	[SurgeryListSize] [float] NULL,
	[SurgeryHA] [varchar](3) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientMergeLog](
	[PatientMergeLogCode] [varchar](8) NOT NULL,
	[KeptPatientCode] [varchar](8) NULL,
	[DeletedPatientCode] [varchar](8) NULL,
	[KeptPatientHospitalNumber] [varchar](15) NULL,
	[DeletedPatientHospitalNumber] [varchar](15) NULL,
	[MergeDate] [datetime] NULL,
	[PatientMergeLogStatus] [varchar](255) NULL
) ON [PRIMARY]
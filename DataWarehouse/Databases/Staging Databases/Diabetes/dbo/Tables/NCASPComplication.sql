﻿CREATE TABLE [dbo].[NCASPComplication](
	[NCASPComplicationCode] [varchar](8) NOT NULL,
	[PatientCode] [varchar](8) NULL,
	[NHSNumber] [float] NULL,
	[TypeOfData] [float] NULL,
	[DiagnosisScheme] [float] NULL,
	[DiagnosticCoding] [varchar](10) NULL,
	[ObservationDate] [varchar](10) NULL
) ON [PRIMARY]
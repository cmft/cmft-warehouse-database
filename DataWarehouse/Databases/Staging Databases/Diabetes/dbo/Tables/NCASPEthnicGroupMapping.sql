﻿CREATE TABLE [dbo].[NCASPEthnicGroupMapping](
	[NCASPEthnicGroupMappingCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[DiamondEthnicGroup] [varchar](50) NULL,
	[DiamondPatientRecords] [float] NULL,
	[NCASPEthnicGroupCode] [varchar](8) NULL
) ON [PRIMARY]
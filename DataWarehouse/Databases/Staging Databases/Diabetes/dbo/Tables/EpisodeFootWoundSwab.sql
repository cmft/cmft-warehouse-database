﻿CREATE TABLE [dbo].[EpisodeFootWoundSwab](
	[EpisodeFootWoundSwabCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[EpisodeCode] [varchar](8) NULL,
	[PatientCode] [varchar](8) NULL,
	[WoundSwabDate] [datetime] NULL,
	[WoundSwabResult] [varchar](255) NULL
) ON [PRIMARY]
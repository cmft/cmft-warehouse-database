﻿CREATE TABLE [dbo].[EpisodeFootXRay](
	[EpisodeFootXRayCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[EpisodeCode] [varchar](8) NULL,
	[PatientCode] [varchar](8) NULL,
	[XRayDate] [datetime] NULL,
	[XRayResult] [varchar](255) NULL
) ON [PRIMARY]
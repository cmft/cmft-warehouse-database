﻿CREATE TABLE [dbo].[Letter](
	[LetterCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[LetterType] [varchar](10) NULL,
	[LetterID] [varchar](20) NULL,
	[LetterTitle] [varchar](50) NULL,
	[ReportName] [varchar](50) NULL
) ON [PRIMARY]
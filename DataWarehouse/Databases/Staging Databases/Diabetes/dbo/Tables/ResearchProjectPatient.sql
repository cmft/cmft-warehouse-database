﻿CREATE TABLE [dbo].[ResearchProjectPatient](
	[ResearchProjectPatientCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PatientCode] [varchar](8) NULL,
	[ResearchProjectCode] [varchar](8) NULL,
	[DateRecruited] [datetime] NULL,
	[DateLeft] [datetime] NULL,
	[ReviewPeriod] [varchar](20) NULL
) ON [PRIMARY]
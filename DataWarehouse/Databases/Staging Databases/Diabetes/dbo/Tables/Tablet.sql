﻿CREATE TABLE [dbo].[Tablet](
	[TabletCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[Tablet] [varchar](30) NULL,
	[ListOrder] [float] NULL,
	[MaximumDosage] [float] NULL
) ON [PRIMARY]
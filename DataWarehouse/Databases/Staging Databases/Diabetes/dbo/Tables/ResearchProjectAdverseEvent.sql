﻿CREATE TABLE [dbo].[ResearchProjectAdverseEvent](
	[ResearchProjectAdverseEventCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[ResearchProjectCode] [varchar](8) NULL,
	[PatientCode] [varchar](8) NULL,
	[AdverseEventDescription] [varchar](50) NULL,
	[AdverseEventDate] [datetime] NULL
) ON [PRIMARY]
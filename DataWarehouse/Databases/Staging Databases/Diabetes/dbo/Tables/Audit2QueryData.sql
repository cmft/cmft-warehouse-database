﻿CREATE TABLE [dbo].[Audit2QueryData](
	[Audit2QueryDataCode] [varchar](12) NOT NULL,
	[Status] [varchar](10) NULL,
	[Audit2QueryCode] [varchar](8) NULL,
	[ControlName] [varchar](255) NULL,
	[ListField] [bit] NOT NULL,
	[NumberOfSubItems] [float] NULL,
	[ListItemSequence] [float] NULL,
	[Data] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
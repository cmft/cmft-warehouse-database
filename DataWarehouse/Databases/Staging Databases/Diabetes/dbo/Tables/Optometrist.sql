﻿CREATE TABLE [dbo].[Optometrist](
	[OptometristCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[OpticianCode] [varchar](8) NULL,
	[Surname] [varchar](20) NULL,
	[Forenames] [varchar](20) NULL,
	[Initials] [varchar](10) NULL,
	[Title] [varchar](10) NULL,
	[FullName] [varchar](42) NULL,
	[GMCNumber] [varchar](12) NULL,
	[Accredited] [float] NULL
) ON [PRIMARY]
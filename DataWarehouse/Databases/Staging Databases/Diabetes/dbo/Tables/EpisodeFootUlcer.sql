﻿CREATE TABLE [dbo].[EpisodeFootUlcer](
	[EpisodeFootUlcerCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[EpisodeCode] [varchar](8) NULL,
	[PatientCode] [varchar](8) NULL,
	[UlcerNumber] [float] NULL,
	[UlcerSide] [varchar](5) NULL,
	[UlcerDate] [datetime] NULL,
	[UlcerLocation] [varchar](50) NULL,
	[UlcerSize] [varchar](15) NULL,
	[UlcerCause] [varchar](30) NULL,
	[UlcerGrade] [varchar](50) NULL,
	[UlcerStage] [varchar](50) NULL,
	[UlcerClassification] [varchar](255) NULL,
	[UlcerPointX] [float] NULL,
	[UlcerPointY] [float] NULL,
	[Amputation] [varchar](30) NULL
) ON [PRIMARY]
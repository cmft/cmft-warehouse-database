﻿CREATE TABLE [dbo].[LabUploadMapping](
	[LabUploadMappingCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PathType] [varchar](8) NULL,
	[LabResultName] [varchar](50) NULL,
	[LabResultDescription] [varchar](50) NULL,
	[LabUploadMapping] [varchar](50) NULL,
	[Comment] [varchar](50) NULL,
	[LabUnits] [varchar](10) NULL,
	[LabConversion] [float] NULL,
	[LabConversionOperator] [varchar](2) NULL,
	[LabIncomingUnits] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LabUploadClinic](
	[LabUploadClinicCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[ClinicCode] [varchar](15) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Audit2Query](
	[Audit2QueryCode] [varchar](8) NOT NULL,
	[Status] [varchar](50) NULL,
	[QueryName] [varchar](255) NULL,
	[Description] [text] NULL,
	[CreatedBy] [varchar](10) NULL,
	[DateCreated] [datetime] NULL,
	[UserGroup] [varchar](255) NULL,
	[Audit2Output] [varchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[RecordLocks](
	[RecordLocksCode] [varchar](8) NOT NULL,
	[RecordLockStatus] [varchar](15) NULL,
	[TableName] [varchar](150) NULL,
	[TableRecordCode] [varchar](8) NULL,
	[OperatorCode] [varchar](8) NULL,
	[RecordLockDateTime] [datetime] NULL
) ON [PRIMARY]
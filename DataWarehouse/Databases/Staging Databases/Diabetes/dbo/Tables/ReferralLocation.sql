﻿CREATE TABLE [dbo].[ReferralLocation](
	[ReferralLocationCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[ReferralType] [varchar](25) NULL,
	[Surname] [varchar](20) NULL,
	[Forenames] [varchar](20) NULL,
	[Initials] [varchar](10) NULL,
	[Title] [varchar](10) NULL,
	[FullName] [varchar](42) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[Address3] [varchar](30) NULL,
	[Address4] [varchar](30) NULL,
	[PostCode] [varchar](10) NULL,
	[Phone] [varchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PASPrimaryLanguageMapping](
	[PASPrimaryLanguageMappingCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PASPrimaryLanguage] [varchar](50) NULL,
	[PASPrimaryLanguageCode] [varchar](5) NULL,
	[DiamondPrimaryLanguage] [varchar](40) NULL
) ON [PRIMARY]
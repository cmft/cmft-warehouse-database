﻿CREATE TABLE [dbo].[OperatorSurgery](
	[OperatorSurgeryCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[OperatorCode] [varchar](8) NULL,
	[SurgeryCode] [varchar](8) NULL
) ON [PRIMARY]
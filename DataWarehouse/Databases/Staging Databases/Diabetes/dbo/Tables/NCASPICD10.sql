﻿CREATE TABLE [dbo].[NCASPICD10](
	[NCASPICD10Code] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[ICD10Code] [varchar](4) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientProblem](
	[PatientProblemCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[PatientCode] [varchar](8) NULL,
	[ProblemType] [varchar](10) NULL,
	[ProblemComments] [varchar](100) NULL,
	[ProblemICDCode] [varchar](10) NULL,
	[ProblemICDDescription] [varchar](70) NULL,
	[ProblemOnset] [varchar](4) NULL
) ON [PRIMARY]
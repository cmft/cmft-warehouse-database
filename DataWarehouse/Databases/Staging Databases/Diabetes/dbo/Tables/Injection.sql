﻿CREATE TABLE [dbo].[Injection](
	[InjectionCode] [varchar](8) NOT NULL,
	[Status] [varchar](10) NULL,
	[Injection] [varchar](30) NULL,
	[InjectionType] [varchar](20) NULL,
	[Manufacturer] [varchar](20) NULL,
	[ListOrder] [float] NULL,
	[Insulin] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
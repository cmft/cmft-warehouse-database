﻿CREATE TABLE [dbo].[UploadErrors](
	[UploadErrorCode] [varchar](8) NOT NULL,
	[ErrorNumber] [float] NULL,
	[SystemDate] [datetime] NULL,
	[SystemTime] [datetime] NULL,
	[ErrorCode] [varchar](20) NULL,
	[ErrorMessage] [varchar](100) NULL,
	[LineNumber] [varchar](10) NULL,
	[FieldNumber] [varchar](10) NULL,
	[FileName] [varchar](12) NULL,
	[FieldValue] [varchar](50) NULL,
	[TransactionDate] [datetime] NULL,
	[TransactionID] [varchar](9) NULL,
	[UN1] [varchar](9) NULL
) ON [PRIMARY]
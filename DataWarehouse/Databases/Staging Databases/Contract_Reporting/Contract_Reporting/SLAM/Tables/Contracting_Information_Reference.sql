﻿CREATE TABLE [SLAM].[Contracting_Information_Reference](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[SLAM_Import_Type] [varchar](15) NOT NULL,
	[Age_Category] [char](1) NOT NULL,
	[Casenote_Number] [varchar](50) NULL,
	[Feed_key] [int] NOT NULL,
	[Spell_ID] [varchar](30) NOT NULL,
	[Internal_Specialty_Code] [varchar](50) NULL,
	[Exam_Code] [varchar](50) NOT NULL,
	[Clinic_Code] [varchar](50) NOT NULL,
	[AdHoc_Code] [varchar](50) NOT NULL,
	[AdmMethod] [int] NOT NULL,
	[Patient_Classification] [int] NOT NULL,
	[Spell_Admission_Date] [date] NOT NULL,
	[Spell_Discharge_Date] [date] NOT NULL,
	[LoS] [int] NOT NULL,
	[SUS_ID] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE view [SLAM].[ContractingInformationFull]

as


select
	Contracting_ID
	,Feed_key
	,Report_Master_ID
	,Period_ID
	,Spell_Admission_Date
	,Spell_Discharge_Date
	,NHS_Number
	,DoB
	,District_Number
	,Forename
	,Patient.Surname
	,Clinic_Code
	,Casenote_Number
	,Internal_Specialty_Code
	,LoS
	,Activity_Actual
	,Price_Actual
	,Price_Actual_Adjusted
	,Activity_Plan_Commissioner
	,Activity_Plan_Divisional
	,Price_Plan_Commissioner
	,Price_Plan_Divisional
	,Price_Forecast
	,PoD_Code
	,Summary_PoD
	,HRG_Code
	,HRG_Description
	,Feed_Name
	,Treatment_Function_Code			= TFC.Specialty_Code
	,Treatment_Function_Description		= TFC.Specialty_Description
	,Main_Specialty_Code				= MSC.Specialty_Code
	,Main_Specialty_Description			= MSC.Specialty_Description
	,Extract_Month
	,Extract_Type
	,period.Extract_Year
	,Report_Month
	,COM.Commissioner_Code
	,COM.Organisation_Type
	,COM.Commissioner_Description
	,COM.Contract_Type
	,COM.NPoC
	,COM.Contract_Stream
	,COM.Area
	,Spell_ID
	,Site_Code
	,Contract_Flag = Flag.Commissioner_Code
	,Core_Commissioner_Code = Core.Commissioner_Code
	,Core_Commissioner_Description = Core.Commissioner_Description
	,AdmMethod
	,Patient_Classification
	,Directorate_Code
	,Directorate_Name
	,Division
	,Consultant_Code  =Consultant.Practitioner_Code
	,Exam_Code
	,Age_Category
	,Procedure_Code
	,Practice.Practice_Code
	,Postcode
	,SLAM_Part
	,Income_Type.Income_Type
	,Profile_ID
	,Diagnosis.Diagnosis_Code
	,SUS_ID
	,Is_Current
	,RollingYear
	,SLAM_Import_Type
	,GP_Code
	,Referral_Source_Code
	,Latest_In_Year
	,ReferencePeriod


from	
	slam.Contracting_Information 

	inner join SLAM.Contracting_Information_Reference
	on Reference_ID = Contracting_Information_Reference.ID
	
	inner join ID_Lookup.Patient
	on patient.ID = patient_id
	
	inner join ID_Lookup.HRG
	on hrg.id = hrg_id
	
	inner join ID_Lookup.PoD 
	on pod.ID = POD_ID
	
	inner join ID_Lookup.Feed
	on Feed_ID = feed.ID
	
	inner join ID_Lookup.Specialty TFC
	on Treatment_Function_ID = TFC.id
	
	inner join ID_Lookup.Specialty MSC
	on Specialty_ID = MSC.id
	
	inner join ID_Lookup.Period
	on Period.ID = Period_ID
	
	inner join ID_Lookup.Commissioner COM
	on Commissioner_ID = COM.id
	
	inner join ID_Lookup.Commissioner Flag
	on Contract_Flag_ID = Flag.id
	
	inner join ID_Lookup.Hospital_Site
	on Site_ID = Hospital_Site.id
	
	inner join ID_Lookup.Division
	on Directorate_ID = Division.ID
	
	inner join ID_Lookup.Consultant
	on Consultant_ID = Consultant.ID

	inner join ID_Lookup.Commissioner Core
	on Core_Commissioner_ID = Core.id

	inner join ID_Lookup.[Procedure]
	on Procedure_ID = [Procedure].id
	
	inner join ID_Lookup.Practice
	on Practice.ID = GPP_ID
	
	inner join ID_Lookup.Postcode
	on Postcode.ID = Postcode_ID
	
	inner join ID_Lookup.SLAM_Part
	on HRG.Currency = SLAM_Part.Currency
	and Contracting_Information.Is_National_Tariff = SLAM_Part.Tariff
	
	inner join ID_Lookup.Diagnosis
	on Diagnosis.ID = Diagnosis_ID
	
	inner join ID_Lookup.GP
	on GP.ID = GP_ID

	inner join ID_Lookup.Referral_Source
	on Referral_Source_ID = Referral_Source.ID

	inner join ID_Lookup.Income_Type
	on HRG.Income_Type = Income_Type.Currency
	and Contracting_Information.Is_National_Tariff = Income_Type.Tariff
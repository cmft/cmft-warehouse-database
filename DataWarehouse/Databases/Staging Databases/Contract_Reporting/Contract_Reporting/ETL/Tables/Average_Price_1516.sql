﻿CREATE TABLE [ETL].[Average_Price_1516](
	[Site_Code] [varchar](10) NOT NULL,
	[Specialty_Code] [varchar](10) NOT NULL,
	[PoD_Code] [varchar](50) NULL,
	[Division] [varchar](50) NOT NULL,
	[Average_Price] [float] NULL
) ON [PRIMARY]
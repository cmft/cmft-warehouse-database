﻿CREATE TABLE [ETL].[XBDs_Ratio_1516](
	[Site_Code] [varchar](5) NOT NULL,
	[Division] [varchar](50) NULL,
	[Treatment_Function_Code] [varchar](7) NOT NULL,
	[Pod_Group] [varchar](50) NOT NULL,
	[Organisation_Type] [varchar](50) NOT NULL,
	[Activity] [float] NULL,
	[Price] [float] NULL
) ON [PRIMARY]
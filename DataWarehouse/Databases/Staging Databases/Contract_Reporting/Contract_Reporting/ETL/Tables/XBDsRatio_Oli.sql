﻿CREATE TABLE [ETL].[XBDsRatio_Oli](
	[Site_Code] [varchar](10) NOT NULL,
	[Division] [varchar](30) NOT NULL,
	[Specialty_Code] [varchar](20) NOT NULL,
	[Pod_Group] [varchar](50) NOT NULL,
	[Organisation_Type] [varchar](50) NOT NULL,
	[Activity] [float] NULL,
	[Price] [float] NULL
) ON [PRIMARY]
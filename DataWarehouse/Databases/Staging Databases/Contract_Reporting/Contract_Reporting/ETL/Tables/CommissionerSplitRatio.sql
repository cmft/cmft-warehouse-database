﻿CREATE TABLE [ETL].[CommissionerSplitRatio](
	[PoD_group] [varchar](50) NOT NULL,
	[Specialty_Code] [varchar](7) NOT NULL,
	[Site_Code] [char](5) NOT NULL,
	[Division] [varchar](30) NOT NULL,
	[Organisation_Type] [varchar](50) NOT NULL,
	[Ratio] [float] NULL
) ON [PRIMARY]
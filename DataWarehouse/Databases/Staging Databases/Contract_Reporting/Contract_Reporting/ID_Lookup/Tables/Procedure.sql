﻿CREATE TABLE [ID_Lookup].[Procedure](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Procedure_Code] [char](5) NOT NULL,
	[Procedure_Description] [varchar](255) NOT NULL
) ON [PRIMARY]
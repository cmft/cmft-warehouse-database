﻿CREATE TABLE [ID_Lookup].[Profile](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[PoD_ID] [int] NULL,
	[Spec_ID] [int] NULL,
	[Directorate_ID] [int] NULL,
	[Site_ID] [int] NULL,
	[Extract_Year] [char](9) NOT NULL
) ON [PRIMARY]
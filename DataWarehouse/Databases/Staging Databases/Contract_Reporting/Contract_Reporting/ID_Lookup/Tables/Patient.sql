﻿CREATE TABLE [ID_Lookup].[Patient](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[District_Number] [char](8) NOT NULL,
	[NHS_Number] [char](10) NULL,
	[PAS_Site] [char](3) NOT NULL,
	[Forename] [varchar](100) NULL,
	[Surname] [varchar](100) NULL,
	[DoB] [date] NULL,
	[Gender] [int] NULL,
	[Ethnicity] [char](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [ID_Lookup].[Division](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Directorate_Code] [char](3) NULL,
	[Directorate_Name] [varchar](30) NOT NULL,
	[Division] [varchar](30) NOT NULL,
	[Site_ID] [int] NOT NULL
) ON [PRIMARY]
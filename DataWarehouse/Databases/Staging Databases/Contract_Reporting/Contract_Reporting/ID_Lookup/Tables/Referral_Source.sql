﻿CREATE TABLE [ID_Lookup].[Referral_Source](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Referral_Source_Code] [char](2) NOT NULL,
	[Referral_Source_Description] [varchar](150) NOT NULL
) ON [PRIMARY]
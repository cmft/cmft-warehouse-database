﻿CREATE TABLE [ID_Lookup].[GP](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[GP_Code] [char](8) NOT NULL,
	[GP_Name] [varchar](50) NOT NULL,
	[Practice_Code] [char](6) NOT NULL,
	[Extract_Year] [char](9) NOT NULL
) ON [PRIMARY]
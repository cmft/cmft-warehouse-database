﻿CREATE TABLE [ID_Lookup].[Consultant](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Practitioner_Code] [char](10) NULL,
	[Initial] [char](1) NOT NULL,
	[Surname] [varchar](100) NOT NULL
) ON [PRIMARY]
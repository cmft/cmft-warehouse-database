﻿CREATE TABLE [ID_Lookup].[Specialty](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Specialty_Code] [varchar](7) NOT NULL,
	[Specialty_Description] [varchar](100) NOT NULL
) ON [PRIMARY]
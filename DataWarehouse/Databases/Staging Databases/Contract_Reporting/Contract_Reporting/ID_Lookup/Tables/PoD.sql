﻿CREATE TABLE [ID_Lookup].[PoD](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[PoD_Code] [varchar](50) NOT NULL,
	[PoD_Description] [varchar](100) NOT NULL,
	[Summary_PoD] [varchar](50) NOT NULL,
	[National_PoD_Code] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [ID_Lookup].[Profile_Weight](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Profile_ID] [int] NOT NULL,
	[Month_ID] [int] NOT NULL,
	[Month_Weight] [float] NOT NULL,
	[Cumulative_Weight] [float] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [ID_Lookup].[Practice](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Practice_Code] [char](6) NOT NULL,
	[Practice_Description] [varchar](150) NOT NULL,
	[Commissioner_Code] [char](6) NOT NULL,
	[Extract_Year] [char](9) NOT NULL
) ON [PRIMARY]
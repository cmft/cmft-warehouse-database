﻿CREATE TABLE [ID_Lookup].[Hospital_Site](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Site_Code] [char](5) NOT NULL,
	[Site_Description] [varchar](100) NOT NULL,
	[PAS_Site] [char](3) NULL
) ON [PRIMARY]
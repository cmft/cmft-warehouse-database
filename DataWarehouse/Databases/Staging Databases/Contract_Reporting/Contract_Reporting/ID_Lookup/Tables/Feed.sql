﻿CREATE TABLE [ID_Lookup].[Feed](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Feed_Name] [varchar](50) NOT NULL,
	[Feed_Source] [varchar](50) NOT NULL,
	[PAS_Site] [char](3) NOT NULL
) ON [PRIMARY]
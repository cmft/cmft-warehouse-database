﻿CREATE TABLE [ID_Lookup].[Diagnosis](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Diagnosis_Code] [char](5) NOT NULL,
	[Diagnosis_Description] [varchar](255) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [ID_Lookup].[Commissioner](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Commissioner_Code] [varchar](50) NOT NULL,
	[Commissioner_Description] [varchar](100) NOT NULL,
	[Contract_Stream] [varchar](50) NOT NULL,
	[Organisation_Type] [varchar](50) NOT NULL,
	[Upload_File] [varchar](50) NOT NULL,
	[Contract_Type] [varchar](50) NOT NULL,
	[Area] [varchar](50) NOT NULL,
	[Area_Description] [nvarchar](max) NOT NULL,
	[NPoC] [nvarchar](50) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [ID_Lookup].[Period](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Extract_Month] [int] NOT NULL,
	[Extract_Type] [varchar](10) NOT NULL,
	[Extract_Year] [char](9) NOT NULL,
	[Report_Month] [int] NOT NULL
) ON [PRIMARY]
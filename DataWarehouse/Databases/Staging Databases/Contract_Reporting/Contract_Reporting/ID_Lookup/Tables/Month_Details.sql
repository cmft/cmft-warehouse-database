﻿CREATE TABLE [ID_Lookup].[Month_Details](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Month_Number] [int] NOT NULL,
	[Financial_Month_Number] [int] NOT NULL,
	[Month_Name] [varchar](9) NOT NULL,
	[Month_Short_Name] [char](3) NOT NULL,
	[Days_in_Month] [int] NOT NULL,
	[Financial_Year] [char](9) NOT NULL,
	[Calender_Year] [int] NOT NULL,
	[Start_Date] [datetime] NOT NULL,
	[Last_Day_Month] [datetime] NOT NULL,
	[End_Date] [datetime] NOT NULL,
	[Working_Days_This_Month] [int] NULL,
	[Working_Days_YTD] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [ID_Lookup].[Postcode](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Postcode] [varchar](50) NULL,
	[LA_of_Residence] [varchar](10) NULL,
	[HA_of_Residence] [varchar](10) NULL,
	[CCG_of_Residence] [varchar](10) NULL,
	[Extract_Year] [char](9) NOT NULL,
	[LSOA] [char](9) NULL
) ON [PRIMARY]
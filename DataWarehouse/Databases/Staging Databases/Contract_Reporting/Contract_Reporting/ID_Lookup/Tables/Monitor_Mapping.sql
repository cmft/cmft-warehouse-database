﻿CREATE TABLE [ID_Lookup].[Monitor_Mapping](
	[Is_National_Tariff] [float] NULL,
	[POD_Code] [nvarchar](255) NULL,
	[Monitor_Tariff_Type] [nvarchar](255) NULL,
	[Monitor_POD_Summary] [nvarchar](255) NULL,
	[Monitor_POD_Detail] [nvarchar](255) NULL
) ON [PRIMARY]
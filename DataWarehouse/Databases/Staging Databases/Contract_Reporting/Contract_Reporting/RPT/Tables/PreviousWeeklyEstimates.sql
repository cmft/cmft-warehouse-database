﻿CREATE TABLE [RPT].[PreviousWeeklyEstimates](
	[ExtractID] [int] IDENTITY(1,1) NOT NULL,
	[Period] [varchar](255) NULL,
	[Division] [varchar](30) NOT NULL,
	[PoD_Code] [varchar](100) NULL,
	[Activity] [float] NULL,
	[Ucodes] [float] NULL,
	[ExtractDate] [date] NOT NULL CONSTRAINT [DF_PreviousWeeklyEstimates_ExtractDate]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [RPT].[Financial_Summary_1516_data](
	[Extract_Year] [varchar](10) NOT NULL,
	[Extract_Month] [int] NOT NULL,
	[Extract_Type] [varchar](10) NOT NULL,
	[Report_Type] [varchar](50) NOT NULL,
	[Search] [varchar](50) NOT NULL,
	[Search_Description] [varchar](255) NOT NULL,
	[CCG_Code] [varchar](50) NOT NULL,
	[CCG_Description] [varchar](255) NOT NULL,
	[Plan_Annual] [float] NULL,
	[Forecast_Outturn] [float] NULL,
	[Forecast_Variance] [float] NULL,
	[Plan_YTD] [float] NULL,
	[Actual_YTD] [float] NULL,
	[Variance_YTD] [float] NULL,
	[Price_Actual_Adjusted_YTD] [float] NULL,
	[Adjusted_Variance_YTD] [float] NULL,
	[Plan_Current] [float] NULL,
	[Actual_Current] [float] NULL,
	[Variance_Current] [float] NULL,
	[Price_Actual_Adjusted_Current] [float] NULL,
	[Adjusted_Variance_Current] [float] NULL
) ON [PRIMARY]
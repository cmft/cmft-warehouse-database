﻿CREATE TABLE [RPT].[NCA_Data](
	[Commissioner_Code] [nvarchar](255) NULL,
	[Commissioner_Description] [nvarchar](255) NULL,
	[Organisation_Type] [nvarchar](255) NULL,
	[Contract_Type] [nvarchar](255) NULL,
	[Contract_Stream] [nvarchar](255) NULL,
	[Report_Master_ID] [int] NULL,
	[Casenote_Number] [nvarchar](255) NULL,
	[Consultant_Code] [nvarchar](255) NULL,
	[DOB] [date] NULL,
	[NHS_Number] [nvarchar](255) NULL,
	[GP_Code] [nvarchar](255) NULL,
	[Practice_Code] [nvarchar](255) NULL,
	[HRG_Code] [nvarchar](255) NULL,
	[PoD_Code] [nvarchar](255) NULL,
	[Specialty_Code] [nvarchar](255) NULL,
	[Spell_Discharge_Date] [date] NULL,
	[District_Number] [nvarchar](255) NULL,
	[Referral_Date] [nvarchar](255) NULL,
	[Postcode] [nvarchar](255) NULL,
	[Activity_Actual] [float] NULL,
	[Price_actual] [float] NULL,
	[Report_Year] [nvarchar](50) NULL,
	[Report_Month] [int] NULL,
	[Report_Type] [nvarchar](50) NULL,
	[Financial_Month_Number] [int] NULL,
	[SUS_ID] [varchar](50) NULL,
	[Site_Code] [nvarchar](255) NULL,
	[Feed_Key] [nvarchar](255) NULL,
	[Feed_Site] [nvarchar](255) NULL,
	[LSOA] [char](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [RPT].[Financial_Summary_NHSE_Spec_data](
	[Extract_Year] [char](9) NOT NULL,
	[Extract_Month] [int] NOT NULL,
	[Extract_Type] [varchar](10) NOT NULL,
	[MonthNo] [int] NOT NULL,
	[Specialised/Non-Specialised] [varchar](50) NOT NULL,
	[Service_Code] [varchar](50) NOT NULL,
	[Service_Description] [varchar](100) NOT NULL,
	[PoD_Code] [varchar](50) NOT NULL,
	[Price_Forecast] [float] NULL,
	[Price_Actual_Adjusted] [float] NULL,
	[Price_Actual] [float] NULL,
	[Price_Plan] [float] NULL
) ON [PRIMARY]
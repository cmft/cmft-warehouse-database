﻿CREATE TABLE [RPT].[UndischargeLLOSandCritCarePatients_OutputToCSV](
	[Provider Code] [varchar](3) NOT NULL,
	[NHS Number] [varchar](10) NOT NULL,
	[GP Practice Code] [varchar](10) NULL,
	[Commissioner Code] [varchar](50) NOT NULL,
	[Residing Ward Code] [varchar](10) NULL,
	[Current Length of Stay] [smallint] NULL,
	[Number of Undischarged CC* Days] [int] NULL,
	[Admission Speciality Code] [varchar](20) NOT NULL,
	[Admission Method] [varchar](50) NULL,
	[PoD] [varchar](50) NULL,
	[Organisation Type] [varchar](50) NOT NULL,
	[Financial Year] [char](9) NULL,
	[Financial Month] [int] NULL,
	[MergeEncounterRecno] [int] NOT NULL,
	[DataSourceDate] [datetime] NOT NULL
) ON [PRIMARY]
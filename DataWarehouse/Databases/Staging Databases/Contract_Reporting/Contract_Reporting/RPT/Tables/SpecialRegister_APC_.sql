﻿CREATE TABLE [RPT].[SpecialRegister_APC_](
	[EncounterRecno] [float] NULL,
	[SourceUniqueID] [nvarchar](255) NULL,
	[SourcePatientNo] [float] NULL,
	[SourceSpellNo] [float] NULL,
	[SourceEncounterNo] [float] NULL,
	[ProviderSpellNo] [nvarchar](255) NULL,
	[CasenoteNumber] [nvarchar](255) NULL,
	[PatientTitle] [nvarchar](255) NULL,
	[PatientForename] [nvarchar](255) NULL,
	[PatientSurname] [nvarchar](255) NULL,
	[AdmissionDate] [datetime] NULL,
	[DischargeTime] [datetime] NULL,
	[LOS] [float] NULL,
	[SpecialRegister] [nvarchar](255) NULL,
	[SpecialRegisterDescription] [nvarchar](255) NULL,
	[PCTCode] [nvarchar](255) NULL,
	[Organisation Name] [nvarchar](255) NULL,
	[SpecCode] [float] NULL,
	[Type] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [RPT].[Undischarged_Critical_Care](
	[Month_ID] [int] NULL,
	[Version_ID] [int] NULL,
	[MergeEncounterRecno] [int] NOT NULL,
	[Contract_Type] [varchar](50) NOT NULL,
	[Commissioner_Code] [varchar](50) NOT NULL,
	[Commissioner_Description] [varchar](100) NOT NULL,
	[NHSNumber] [varchar](17) NULL,
	[RegisteredGpPracticeCode] [varchar](10) NULL,
	[StartWardTypeCode] [varchar](10) NULL,
	[AdmissionDate] [datetime] NULL,
	[PODCode] [varchar](50) NULL,
	[Bed_Days] [int] NULL
) ON [PRIMARY]
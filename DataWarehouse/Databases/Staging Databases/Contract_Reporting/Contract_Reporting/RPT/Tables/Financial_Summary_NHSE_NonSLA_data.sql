﻿CREATE TABLE [RPT].[Financial_Summary_NHSE_NonSLA_data](
	[Extract_Year] [nvarchar](255) NOT NULL,
	[Extract_Month] [int] NOT NULL,
	[Extract_Type] [nvarchar](255) NOT NULL,
	[MonthNo] [int] NOT NULL,
	[CCG_Code] [varchar](50) NULL,
	[PoD_Code] [varchar](50) NOT NULL,
	[Spec_Code] [varchar](7) NOT NULL,
	[Spec_Desc] [nvarchar](255) NOT NULL,
	[HRG_Code] [varchar](50) NOT NULL,
	[HRG_Desc] [nvarchar](255) NOT NULL,
	[Price_Forecast] [float] NULL,
	[Price_Actual_Adjusted] [float] NULL,
	[Price_Actual] [float] NULL,
	[Price_Plan] [float] NULL
) ON [PRIMARY]
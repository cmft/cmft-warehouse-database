﻿CREATE TABLE [RPT].[RTT_Commissioner_Report_Data](
	[Extract_Month] [int] NOT NULL,
	[Extract_Type] [varchar](10) NOT NULL,
	[Report_Master_ID] [int] NOT NULL,
	[PoD_Code] [varchar](50) NOT NULL,
	[Treatment_Function_Code] [varchar](7) NOT NULL,
	[Treatment_Function_Description] [varchar](100) NOT NULL,
	[HRG_Code] [varchar](50) NOT NULL,
	[Month_Number] [int] NOT NULL,
	[Commissioner_Code] [varchar](50) NOT NULL,
	[Commissioner_Description] [varchar](100) NOT NULL,
	[Core_Commissioner_Code] [varchar](50) NOT NULL,
	[Core_Commissioner_Description] [varchar](100) NOT NULL,
	[Activity_Actual] [float] NOT NULL,
	[Price_Actual] [float] NOT NULL
) ON [PRIMARY]
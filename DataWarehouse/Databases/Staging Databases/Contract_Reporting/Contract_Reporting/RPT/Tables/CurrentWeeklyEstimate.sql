﻿CREATE TABLE [RPT].[CurrentWeeklyEstimate](
	[Period] [varchar](255) NULL,
	[Division] [varchar](30) NOT NULL,
	[PoD_Code] [varchar](100) NULL,
	[Activity] [float] NULL,
	[Ucodes] [float] NULL
) ON [PRIMARY]
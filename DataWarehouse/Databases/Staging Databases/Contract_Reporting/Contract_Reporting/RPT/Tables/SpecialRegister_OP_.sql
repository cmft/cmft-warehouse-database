﻿CREATE TABLE [RPT].[SpecialRegister_OP_](
	[EncounterRecno] [float] NULL,
	[SourceUniqueID] [nvarchar](255) NULL,
	[SourcePatientNo] [float] NULL,
	[SourceEncounterNo] [float] NULL,
	[CasenoteNo] [nvarchar](255) NULL,
	[PatientTitle] [nvarchar](255) NULL,
	[PatientForename] [nvarchar](255) NULL,
	[PatientSurname] [nvarchar](255) NULL,
	[AppointmentDate] [datetime] NULL,
	[BookingTypeCode] [nvarchar](255) NULL,
	[SpecialRegister] [nvarchar](255) NULL,
	[SpecialRegisterDescription] [nvarchar](255) NULL,
	[PCTCode] [nvarchar](255) NULL,
	[Organisation Name] [nvarchar](255) NULL,
	[SpecCode] [float] NULL,
	[Type] [nvarchar](255) NULL
) ON [PRIMARY]
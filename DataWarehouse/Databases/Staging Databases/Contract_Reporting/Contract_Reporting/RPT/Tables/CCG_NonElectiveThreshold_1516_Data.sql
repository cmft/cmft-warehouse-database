﻿CREATE TABLE [RPT].[CCG_NonElectiveThreshold_1516_Data](
	[Extract_Year] [varchar](10) NOT NULL,
	[Extract_Month] [int] NOT NULL,
	[Extract_Type] [varchar](10) NOT NULL,
	[Commissioner_Code] [varchar](50) NOT NULL,
	[Commissioner_Description] [varchar](100) NOT NULL,
	[Report_Month] [int] NOT NULL,
	[Annual_Threshold_CCG] [float] NULL,
	[Monthly_Threshold_CCG] [float] NULL,
	[YTD_Threshold_CCG] [float] NULL,
	[YTD_Actuals_CCG] [float] NULL,
	[YTD_Difference_CCG] [float] NULL,
	[AnnualThresholdAssociatedToCCG] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Report].[PbRDrugs_Lookup](
	[ID] [int] NOT NULL,
	[Forename] [varchar](80) NULL,
	[Surname] [varchar](80) NULL,
	[DoB] [date] NULL,
	[NHSNumber] [varchar](12) NULL,
	[DistrictNumber] [varchar](40) NULL,
	[GPCode] [varchar](20) NULL,
	[GPP_code] [nvarchar](255) NULL,
	[CCG] [nvarchar](255) NULL,
	[CCG_Name] [nvarchar](255) NULL,
	[Casenoteno] [nvarchar](255) NULL
) ON [PRIMARY]
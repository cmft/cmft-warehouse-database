﻿CREATE TABLE [Report].[PbR_drugs_CasenoteNo](
	[Feed_Patient_Identifier] [nvarchar](255) NULL,
	[Feed_Patient_Identifier_Type] [nvarchar](255) NULL,
	[Report_Master_ID] [int] IDENTITY(1,1) NOT NULL,
	[Casenote_Adjusted] [nvarchar](14) NULL
) ON [PRIMARY]
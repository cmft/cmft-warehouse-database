﻿CREATE TABLE [STAGING].[SLAM_Extract](
	[Extract_ID] [int] IDENTITY(0,1) NOT NULL,
	[Report_Master_ID] [int] NULL,
	[SLAM_Import_Type] [nvarchar](255) NULL,
	[Feed] [nvarchar](255) NULL,
	[Age_Category] [nvarchar](255) NULL,
	[Casenote_Number] [nvarchar](255) NULL,
	[Feed_key] [int] NULL,
	[Spell_ID] [nvarchar](255) NULL,
	[Internal_Specialty_Code] [nvarchar](255) NULL,
	[Exam_Code] [nvarchar](255) NULL,
	[Clinic_Code] [nvarchar](255) NULL,
	[Core_Commissioner_Code] [nvarchar](255) NULL,
	[Contract_Flag] [nvarchar](255) NULL,
	[AdHoc_Code] [nvarchar](255) NULL,
	[AdmMethod] [int] NULL,
	[Consultant_Code] [nvarchar](255) NULL,
	[DOB] [date] NULL,
	[GP_Code] [nvarchar](255) NULL,
	[Practice_Code] [nvarchar](255) NULL,
	[Base_HRG_Code] [nvarchar](255) NULL,
	[HRG_Code] [nvarchar](255) NULL,
	[Month_Number] [int] NULL,
	[NHS_Number] [nvarchar](255) NULL,
	[Patient_Classification] [int] NULL,
	[Commissioner_Code] [nvarchar](255) NULL,
	[PoD_Code] [nvarchar](255) NULL,
	[Site_Code] [nvarchar](255) NULL,
	[Treatment_Function_Code] [nvarchar](255) NULL,
	[Special_Service_ID] [nvarchar](255) NULL,
	[Special_Service_ID_2] [nvarchar](255) NULL,
	[Spell_Admission_Date] [nvarchar](255) NULL,
	[Spell_Discharge_Date] [nvarchar](255) NULL,
	[LoS] [int] NULL,
	[Directorate_Code] [char](3) NULL,
	[District_Number] [nvarchar](255) NULL,
	[Referral_Source] [nvarchar](255) NULL,
	[Postcode] [nvarchar](255) NULL,
	[SUS_ID] [nvarchar](255) NULL,
	[Updated] [bit] NULL,
	[Main_Specialty] [nvarchar](255) NULL,
	[Dominant_Diagnosis] [nvarchar](255) NULL,
	[Dominant_Procedure] [nvarchar](255) NULL,
	[Profile_Code] [nvarchar](255) NULL,
	[Is_National_Tariff] [bit] NOT NULL,
	[Commissioner_ID] [int] NULL,
	[Core_Commissioner_ID] [int] NULL,
	[Contract_Flag_ID] [int] NULL,
	[Consultant_ID] [int] NULL,
	[Diagnosis_ID] [int] NULL,
	[Directorate_ID] [int] NULL,
	[Feed_ID] [int] NULL,
	[GP_ID] [int] NULL,
	[GPP_ID] [int] NULL,
	[HRG_ID] [int] NULL,
	[Period_ID] [int] NULL,
	[Patient_ID] [int] NULL,
	[POD_ID] [int] NULL,
	[Postcode_ID] [int] NULL,
	[Procedure_ID] [int] NULL,
	[Reference_ID] [int] NULL,
	[Site_ID] [int] NULL,
	[Referral_Source_ID] [int] NULL,
	[Main_Specialty_ID] [int] NULL,
	[Treatment_Function_ID] [int] NULL,
	[Profile_ID] [int] NULL,
	[Context_ID] [int] NULL,
	[Service_ID] [int] NULL,
	[Activity_Actual] [float] NOT NULL,
	[Price_Actual] [float] NOT NULL,
	[Price_Actual_Adjusted] [float] NOT NULL,
	[Activity_Plan_Commissioner] [float] NOT NULL,
	[Price_Plan_Commissioner] [float] NOT NULL,
	[Activity_Plan_Divisional] [float] NOT NULL,
	[Price_Plan_Divisional] [float] NOT NULL,
	[Activity_Forecast] [float] NOT NULL,
	[Price_Forecast] [float] NOT NULL
) ON [PRIMARY]
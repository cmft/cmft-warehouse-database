﻿CREATE TABLE [Internal].[Threshold Figures](
	[CCG_Code] [nvarchar](255) NULL,
	[Annual_Threshold] [float] NULL,
	[F3] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Internal].[Non_Elective_Commissioner_Thresholds](
	[CCG_Id] [int] NULL,
	[MonthId] [int] NOT NULL,
	[Monthly_Threshold] [float] NULL,
	[Threshold_YTD] [float] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Internal].[FSList](
	[Row] [bigint] NULL,
	[Commissioner_Code] [varchar](50) NOT NULL,
	[Commissioner_Description] [varchar](100) NOT NULL,
	[Area] [varchar](50) NOT NULL,
	[Area_Description] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
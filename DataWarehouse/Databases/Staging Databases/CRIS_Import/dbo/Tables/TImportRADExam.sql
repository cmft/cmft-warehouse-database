﻿CREATE TABLE [dbo].[TImportRADExam](
	[ImportDateTimeStamp] [datetime] NULL,
	[Accession] [varchar](max) NULL,
	[Appt book mode] [varchar](max) NULL,
	[Appt commit] [varchar](max) NULL,
	[Appt mins] [varchar](max) NULL,
	[Appt prevdate] [varchar](max) NULL,
	[Appt prevtime] [varchar](max) NULL,
	[Appt printed] [varchar](max) NULL,
	[Appt site] [varchar](max) NULL,
	[Batch number] [varchar](max) NULL,
	[Bone dense] [varchar](max) NULL,
	[Booked Date] [varchar](max) NULL,
	[Booked Time] [varchar](max) NULL,
	[Checked By] [varchar](max) NULL,
	[Competance 1] [varchar](max) NULL,
	[Competance 2] [varchar](max) NULL,
	[Competance 3] [varchar](max) NULL,
	[Concentration] [varchar](max) NULL,
	[Contract code] [varchar](max) NULL,
	[Contrast code] [varchar](max) NULL,
	[Creation date] [varchar](max) NULL,
	[Creation time] [varchar](max) NULL,
	[Date First Ver] [varchar](max) NULL,
	[Date Last Ver] [varchar](max) NULL,
	[Date First Add] [varchar](max) NULL,
	[Date Last Add] [varchar](max) NULL,
	[Date Reported] [varchar](max) NULL,
	[Date Typed] [varchar](max) NULL,
	[Default Length] [varchar](max) NULL,
	[Days Waiting] [varchar](max) NULL,
	[Deleted] [varchar](max) NULL,
	[Difficulty 1] [varchar](max) NULL,
	[Difficulty 2] [varchar](max) NULL,
	[Difficulty 3] [varchar](max) NULL,
	[Disk] [varchar](max) NULL,
	[End time] [varchar](max) NULL,
	[Event key] [varchar](max) NULL,
	[Exam Key] [varchar](max) NULL,
	[Exam Date] [varchar](max) NULL,
	[Exam Qual] [varchar](max) NULL,
	[Examination] [varchar](max) NULL,
	[First Add By] [varchar](max) NULL,
	[First Ver By] [varchar](max) NULL,
	[Flexi form] [varchar](max) NULL,
	[Hour Reported] [varchar](max) NULL,
	[Hour Typed] [varchar](max) NULL,
	[ID Checked] [varchar](max) NULL,
	[ID checked By] [varchar](max) NULL,
	[Is Typed] [varchar](max) NULL,
	[Is Verified] [varchar](max) NULL,
	[Ignore appt] [varchar](max) NULL,
	[Injected By] [varchar](max) NULL,
	[Interventional] [varchar](max) NULL,
	[Last Add By] [varchar](max) NULL,
	[Last Ver By] [varchar](max) NULL,
	[MPPS ID] [varchar](max) NULL,
	[MPPS Status] [varchar](max) NULL,
	[Museum code] [varchar](max) NULL,
	[Mutation date] [varchar](max) NULL,
	[Mutation time] [varchar](max) NULL,
	[Order ID] [varchar](max) NULL,
	[Order key] [varchar](max) NULL,
	[Preg checked] [varchar](max) NULL,
	[Preg checked By] [varchar](max) NULL,
	[Quantity used] [varchar](max) NULL,
	[Radiographer 1] [varchar](max) NULL,
	[Radiographer 2] [varchar](max) NULL,
	[Radiographer 3] [varchar](max) NULL,
	[Radiologist] [varchar](max) NULL,
	[Reaction] [varchar](max) NULL,
	[Red Dot] [varchar](max) NULL,
	[Reported] [varchar](max) NULL,
	[Reported By] [varchar](max) NULL,
	[Reported By 2] [varchar](max) NULL,
	[Restrain dose] [varchar](max) NULL,
	[Restrain name] [varchar](max) NULL,
	[Restrain type] [varchar](max) NULL,
	[Room] [varchar](max) NULL,
	[Scan slices] [varchar](max) NULL,
	[Screening time] [varchar](max) NULL,
	[Start tim] [varchar](max) NULL,
	[Status] [varchar](max) NULL,
	[Study ID] [varchar](max) NULL,
	[Time First Verified] [varchar](max) NULL,
	[Time Last Verified] [varchar](max) NULL,
	[Time Reported] [varchar](max) NULL,
	[Time Typed] [varchar](max) NULL,
	[Typed By] [varchar](50) NULL,
	[Wait Breach Date] [varchar](max) NULL,
	[Wait N. Pland] [varchar](max) NULL,
	[Was Planned] [varchar](max) NULL,
	[Was Scheduled] [varchar](max) NULL,
	[Was Waiting] [varchar](max) NULL,
	[Weeks Waiting] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
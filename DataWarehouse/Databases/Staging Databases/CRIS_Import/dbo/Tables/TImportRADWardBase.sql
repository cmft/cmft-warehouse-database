﻿CREATE TABLE [dbo].[TImportRADWardBase](
	[ImportDateTimeStamp] [datetime] NULL,
	[Code] [varchar](50) NULL,
	[Cost centre] [varchar](50) NULL,
	[End date] [varchar](50) NULL,
	[Group] [varchar](50) NULL,
	[HIS code] [varchar](50) NULL,
	[Hospital] [varchar](50) NULL,
	[Local copy] [varchar](50) NULL,
	[Location i hrs] [varchar](50) NULL,
	[Location o hrs] [varchar](50) NULL,
	[Location rep] [varchar](50) NULL,
	[Ward Name] [varchar](50) NULL,
	[Patient type] [varchar](50) NULL,
	[Printer] [varchar](50) NULL,
	[Request cat] [varchar](50) NULL,
	[Source] [varchar](50) NULL
) ON [PRIMARY]
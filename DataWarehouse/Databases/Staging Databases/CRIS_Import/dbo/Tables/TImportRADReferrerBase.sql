﻿CREATE TABLE [dbo].[TImportRADReferrerBase](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ImportDateTimeStamp] [datetime] NULL,
	[Code] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[End date] [varchar](100) NULL,
	[Gp link code] [varchar](100) NULL,
	[Group] [varchar](100) NULL,
	[HIS code] [varchar](100) NULL,
	[Initials] [varchar](100) NULL,
	[Lead clinician] [varchar](100) NULL,
	[Name] [varchar](100) NULL,
	[National code] [varchar](100) NULL,
	[Send EDI] [varchar](max) NULL,
	[Sort code] [varchar](100) NULL,
	[Specialities] [varchar](100) NULL,
	[Start date] [varchar](100) NULL,
	[Type] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[TImportRADRadiographerBase](
	[ImportDateTimeStamp] [datetime] NULL,
	[Code] [varchar](50) NULL,
	[End date] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Student grading] [varchar](50) NULL,
	[User ID] [varchar](50) NULL
) ON [PRIMARY]
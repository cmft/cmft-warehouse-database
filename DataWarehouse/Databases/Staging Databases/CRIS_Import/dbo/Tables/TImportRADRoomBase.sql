﻿CREATE TABLE [dbo].[TImportRADRoomBase](
	[ImportDateTimeStamp] [datetime] NULL,
	[Code] [varchar](max) NULL,
	[Dept] [varchar](max) NULL,
	[Dosage typ] [varchar](max) NULL,
	[End date] [varchar](max) NULL,
	[Hospital] [varchar](max) NULL,
	[Modality] [varchar](max) NULL,
	[Name] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
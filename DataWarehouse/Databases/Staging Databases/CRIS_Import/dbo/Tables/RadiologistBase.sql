﻿CREATE TABLE [dbo].[RadiologistBase](
	[Code] [varchar](12) NULL,
	[EndDate] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Suspended] [smallint] NULL,
	[Type] [varchar](10) NULL,
	[Unprinted] [smallint] NULL,
	[Unverified] [smallint] NULL,
	[UserID] [varchar](12) NULL,
	[rabid] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
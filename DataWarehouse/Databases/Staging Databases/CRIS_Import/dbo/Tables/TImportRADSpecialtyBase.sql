﻿CREATE TABLE [dbo].[TImportRADSpecialtyBase](
	[ImportDateTimeStamp] [datetime] NULL,
	[Code] [varchar](max) NULL,
	[End date] [varchar](max) NULL,
	[Group] [varchar](max) NULL,
	[Name] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
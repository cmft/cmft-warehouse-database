﻿CREATE TABLE [dbo].[StatusTypeBase](
	[Category] [varchar](5) NULL,
	[Code] [varchar](20) NULL,
	[Description] [text] NULL,
	[Type] [varchar](20) NULL,
	[stybid] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
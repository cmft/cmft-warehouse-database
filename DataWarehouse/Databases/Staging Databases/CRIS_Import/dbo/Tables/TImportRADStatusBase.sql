﻿CREATE TABLE [dbo].[TImportRADStatusBase](
	[ImportDateTimeStamp] [datetime] NULL,
	[Category] [varchar](60) NULL,
	[Code] [varchar](60) NULL,
	[Default] [varchar](60) NULL,
	[End date] [varchar](60) NULL,
	[Long desc] [varchar](60) NULL,
	[Order] [varchar](60) NULL,
	[Short desc] [varchar](60) NULL,
	[Type] [varchar](60) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RadiographerBase](
	[Code] [varchar](12) NULL,
	[EndDate] [varchar](12) NULL,
	[Name] [varchar](50) NULL,
	[StudentGrading] [varchar](3) NULL,
	[UserID] [varchar](12) NULL,
	[rbid] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
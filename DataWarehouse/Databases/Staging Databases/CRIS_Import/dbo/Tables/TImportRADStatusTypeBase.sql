﻿CREATE TABLE [dbo].[TImportRADStatusTypeBase](
	[ImportDateTimeStamp] [datetime] NULL,
	[Category] [varchar](4000) NULL,
	[Code] [varchar](4000) NULL,
	[Description] [varchar](4000) NULL,
	[Type] [varchar](4000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[StatusBase](
	[Category] [varchar](3) NULL,
	[Code] [varchar](9) NULL,
	[Default] [varchar](1) NULL,
	[EndDate] [varchar](50) NULL,
	[LongDesc] [varchar](50) NULL,
	[Order] [smallint] NULL,
	[ShortDesc] [varchar](50) NULL,
	[Type] [varchar](7) NULL,
	[stBid] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
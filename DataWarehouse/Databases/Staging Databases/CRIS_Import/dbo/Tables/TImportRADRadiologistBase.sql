﻿CREATE TABLE [dbo].[TImportRADRadiologistBase](
	[ImportDateTimeStamp] [datetime] NULL,
	[Code] [varchar](50) NULL,
	[End date] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Suspended] [varchar](50) NULL,
	[Type] [varchar](50) NULL,
	[Unprinted] [varchar](50) NULL,
	[Unverified] [varchar](50) NULL,
	[User ID] [varchar](50) NULL
) ON [PRIMARY]
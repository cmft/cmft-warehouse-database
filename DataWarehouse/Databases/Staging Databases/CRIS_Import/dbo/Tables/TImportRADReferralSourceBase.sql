﻿CREATE TABLE [dbo].[TImportRADReferralSourceBase](
	[ImportDateTimeStamp] [datetime] NULL,
	[Address 1] [varchar](200) NULL,
	[Address 2] [varchar](200) NULL,
	[Address 3] [varchar](200) NULL,
	[Address 4] [varchar](200) NULL,
	[Address 5] [varchar](200) NULL,
	[Cluster code] [varchar](200) NULL,
	[Code] [varchar](200) NULL,
	[Courier] [varchar](200) NULL,
	[End date] [varchar](200) NULL,
	[Fax] [varchar](200) NULL,
	[Group] [varchar](200) NULL,
	[Join parent date] [varchar](200) NULL,
	[Left parent date] [varchar](200) NULL,
	[Name] [varchar](200) NULL,
	[National code] [varchar](200) NULL,
	[PCT] [varchar](200) NULL,
	[Postcode 1] [varchar](200) NULL,
	[Postcode 2] [varchar](200) NULL,
	[Send EDI] [varchar](200) NULL,
	[Send fax] [varchar](200) NULL,
	[SHA] [varchar](200) NULL,
	[Telephone] [varchar](200) NULL,
	[Type] [varchar](200) NULL
) ON [PRIMARY]
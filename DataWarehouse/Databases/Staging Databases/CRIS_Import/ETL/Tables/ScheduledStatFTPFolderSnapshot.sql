﻿CREATE TABLE [ETL].[ScheduledStatFTPFolderSnapshot](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SnapshotIndex] [int] NULL,
	[SnapshotTime] [datetime] NULL,
	[StatFilename] [varchar](255) NULL,
	[StatFileLastModifiedTime] [datetime] NULL,
	[StatFileSizeKB] [int] NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [varchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UrgentCare_APC_S6](
	[Provider Code] [varchar](10) NULL,
	[GlobalProviderSpellNo] [varchar](50) NULL,
	[GlobalEpisodeNo] [int] NULL,
	[Provider Local ID] [int] NOT NULL,
	[NHS Number] [varchar](17) NULL,
	[Date Of Birth] [datetime] NULL,
	[GP Practice Code] [varchar](10) NULL,
	[Treatment Function Code] [varchar](50) NULL,
	[Admission Method] [varchar](50) NULL,
	[Admission Date] [smalldatetime] NULL,
	[Admission Time] [smalldatetime] NULL,
	[Discharge Date] [smalldatetime] NULL,
	[Discharge Time] [smalldatetime] NULL,
	[Discharge Destination] [varchar](50) NULL,
	[Discharge Method] [varchar](50) NULL,
	[Commissioner Code] [varchar](10) NULL,
	[NationalLastEpisodeInSpellIndicator] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[UrgentCare_AE_S6](
	[Provider Code] [varchar](10) NULL,
	[Provider Local ID] [varchar](50) NULL,
	[NHS Number] [varchar](17) NULL,
	[Date Of Birth] [datetime] NULL,
	[Arrival Date] [smalldatetime] NULL,
	[Arrival Time] [smalldatetime] NULL,
	[Source Referral] [varchar](50) NULL,
	[Disposal Method] [varchar](50) NULL,
	[Presenting Complaint] [varchar](4000) NULL,
	[Initial Triage Cat] [varchar](50) NULL,
	[Departure Date] [smalldatetime] NULL,
	[Departure Time] [smalldatetime] NULL,
	[Commissioner Code] [varchar](10) NULL,
	[PCTCode] [varchar](8) NULL,
	[WalkInType] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Security].[Department](
	[DepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentName] [varchar](128) NOT NULL,
	[created] [datetime2](7) NOT NULL CONSTRAINT [DF__Departmen__creat__22751F6C]  DEFAULT (sysutcdatetime())
) ON [PRIMARY]
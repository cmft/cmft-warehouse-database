﻿CREATE TABLE [Security].[Grade](
	[GradeID] [nvarchar](128) NOT NULL,
	[GradeCode] [varchar](10) NULL,
	[GradeDescription] [varchar](100) NOT NULL,
	[Created] [datetime2](7) NOT NULL CONSTRAINT [DF__Grade__Created__3296789C]  DEFAULT (sysutcdatetime())
) ON [PRIMARY]
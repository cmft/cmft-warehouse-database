﻿CREATE TABLE [Security].[ADSyncSegment](
	[surnameStart] [varchar](2) NOT NULL,
	[lastUpdated] [datetime2](7) NOT NULL DEFAULT (sysutcdatetime())
) ON [PRIMARY]
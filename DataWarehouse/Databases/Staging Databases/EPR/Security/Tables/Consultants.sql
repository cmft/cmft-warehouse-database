﻿CREATE TABLE [Security].[Consultants](
	[ConsultantCode] [varchar](20) NOT NULL,
	[fullLoginName] [nvarchar](128) NOT NULL,
	[shortDomain] [varchar](13) NOT NULL,
	[logonName] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[Title] [varchar](max) NULL,
	[Forename] [nvarchar](128) NULL,
	[Surname] [nvarchar](128) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
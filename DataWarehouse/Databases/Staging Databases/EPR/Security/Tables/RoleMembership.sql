﻿CREATE TABLE [Security].[RoleMembership](
	[RoleMembershipID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[LogonName] [varchar](255) NOT NULL
) ON [PRIMARY]
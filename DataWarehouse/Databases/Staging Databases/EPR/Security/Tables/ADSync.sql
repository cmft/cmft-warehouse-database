﻿CREATE TABLE [Security].[ADSync](
	[Forename] [nvarchar](128) NULL,
	[Surname] [nvarchar](128) NULL,
	[Email] [nvarchar](256) NULL,
	[name] [nvarchar](256) NULL,
	[logonName] [nvarchar](128) NOT NULL,
	[altLogonName] [nvarchar](128) NULL,
	[distinguishedName] [nvarchar](4000) NULL,
	[shortDomain] [varchar](13) NOT NULL,
	[fullLoginName] [nvarchar](128) NOT NULL
) ON [PRIMARY]
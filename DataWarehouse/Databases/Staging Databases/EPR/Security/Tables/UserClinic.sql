﻿CREATE TABLE [Security].[UserClinic](
	[UserClinicId] [int] IDENTITY(1,1) NOT NULL,
	[LogonName] [nvarchar](128) NOT NULL,
	[SourceClinicId] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Security].[DivisionMembership](
	[DivisionUserId] [int] IDENTITY(1,1) NOT NULL,
	[LogonName] [varchar](128) NULL,
	[Isfavourite] [bit] NULL,
	[DivisionId] [int] NOT NULL
) ON [PRIMARY]
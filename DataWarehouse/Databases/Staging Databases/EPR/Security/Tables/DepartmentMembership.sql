﻿CREATE TABLE [Security].[DepartmentMembership](
	[GroupMembershipID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[LogonName] [nvarchar](128) NOT NULL
) ON [PRIMARY]
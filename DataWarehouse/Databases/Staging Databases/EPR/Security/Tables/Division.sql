﻿CREATE TABLE [Security].[Division](
	[DivisionId] [int] IDENTITY(1,1) NOT NULL,
	[DivisionName] [varchar](128) NOT NULL,
	[Description] [varchar](128) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Security].[Role](
	[RoleID] [int] IDENTITY(10000001,1) NOT NULL,
	[RoleName] [varchar](128) NOT NULL,
	[created] [datetime2](7) NOT NULL DEFAULT (sysutcdatetime())
) ON [PRIMARY]
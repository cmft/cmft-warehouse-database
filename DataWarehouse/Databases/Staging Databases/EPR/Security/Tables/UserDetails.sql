﻿CREATE TABLE [Security].[UserDetails](
	[id] [int] IDENTITY(14000000,14) NOT NULL,
	[fullLoginName] [varchar](128) NOT NULL,
	[domain]  AS (left([fullLoginName],charindex('\',[fullLoginName])-(1))),
	[loginName]  AS (right([fullLoginName],len([fullLoginName])-charindex('\',[fullLoginName]))),
	[Forename] [varchar](128) NOT NULL,
	[Surname] [varchar](128) NOT NULL,
	[Email] [varchar](255) NOT NULL,
	[isSuperuser] [bit] NOT NULL CONSTRAINT [DF__UserDetai__isSup__3C69FB99]  DEFAULT ((0)),
	[isActive] [bit] NOT NULL CONSTRAINT [DF__UserDetai__isAct__3D5E1FD2]  DEFAULT ((0)),
	[OptionalData] [xml] NOT NULL CONSTRAINT [DF_UserDetails_OptionalData]  DEFAULT ('<UserData><ClinicalGrade>Other</ClinicalGrade></UserData>')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Forms].[DropDownContent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DropDownID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Value] [varchar](100) NOT NULL,
	[ModifiedOn] [datetime2](7) NOT NULL CONSTRAINT [DF__DropDownC__Creat__5708E33C]  DEFAULT (getdate()),
	[ModifiedBy] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_DropDownContent_Active]  DEFAULT ((1)),
	[DisplayOrder] [int] NOT NULL CONSTRAINT [DF_DropDownContent_DisplayOrder]  DEFAULT ((0))
) ON [PRIMARY]
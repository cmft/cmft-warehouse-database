﻿CREATE TABLE [Forms].[HospitalForms](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[formName] [varchar](255) NULL,
	[fullName] [varchar](255) NULL,
	[formDescription] [varchar](255) NULL,
	[categoryId] [int] NULL,
	[authorizeRequired] [bit] NULL
) ON [PRIMARY]
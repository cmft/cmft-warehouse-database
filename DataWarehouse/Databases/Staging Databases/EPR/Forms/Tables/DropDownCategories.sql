﻿CREATE TABLE [Forms].[DropDownCategories](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Abbreviation] [nvarchar](5) NOT NULL,
	[Name] [nvarchar](100) NOT NULL
) ON [PRIMARY]
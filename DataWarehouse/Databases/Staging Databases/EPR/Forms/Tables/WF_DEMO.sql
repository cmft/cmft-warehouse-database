﻿CREATE TABLE [Forms].[WF_DEMO](
	[PatientId] [int] NOT NULL,
	[PatientName] [varchar](50) NOT NULL,
	[AppStatus] [char](1) NOT NULL,
	[Updated] [datetime] NULL,
	[WFID] [uniqueidentifier] NULL
) ON [PRIMARY]
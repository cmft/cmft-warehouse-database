﻿CREATE TABLE [Forms].[PatientForms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[casenote] [varchar](12) NOT NULL,
	[formid] [int] NOT NULL,
	[formContent] [xml] NOT NULL,
	[createdOn] [datetime] NOT NULL CONSTRAINT [DF_PatientForms_createdOn]  DEFAULT (getdate()),
	[createdBy] [varchar](25) NOT NULL,
	[version] [int] NOT NULL CONSTRAINT [DF_PatientForms_version]  DEFAULT ((1)),
	[delta] [int] NULL,
	[patientFormId] [int] NULL,
	[isCurrent] [bit] NULL,
	[isDraft] [bit] NOT NULL CONSTRAINT [DF_PatientForms_isDraft]  DEFAULT ((0)),
	[isAuthorized] [bit] NULL,
	[DistrictNumber] [varchar](8) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
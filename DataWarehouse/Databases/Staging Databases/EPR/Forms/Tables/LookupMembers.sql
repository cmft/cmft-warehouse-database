﻿CREATE TABLE [Forms].[LookupMembers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LookupID] [int] NOT NULL,
	[LookupOrder] [int] NOT NULL,
	[Value] [nvarchar](255) NOT NULL,
	[DisplayValue] [nvarchar](255) NOT NULL,
	[AppendValue] [bit] NULL CONSTRAINT [DF_LookupMembers_AppendValue]  DEFAULT ((0)),
	[LookupFilterValue] [nvarchar](255) NULL
) ON [PRIMARY]
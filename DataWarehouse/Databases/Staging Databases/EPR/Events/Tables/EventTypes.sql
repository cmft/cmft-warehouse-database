﻿CREATE TABLE [Events].[EventTypes](
	[EventTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ParentEventTypeID] [int] NULL,
	[EventName] [Chameleon].[Name] NOT NULL,
	[Description] [Chameleon].[Description] NOT NULL,
	[EventVersionMajor] [smallint] NOT NULL,
	[EventVersionMinor] [smallint] NOT NULL
) ON [PRIMARY]
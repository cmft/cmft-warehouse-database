﻿CREATE TABLE [Events].[EventTypeEntities](
	[EventTypeEntityID] [int] IDENTITY(1,1) NOT NULL,
	[EventTypeEntityName] [Chameleon].[Name] NOT NULL,
	[EventTypeEntityDescription] [Chameleon].[Description] NOT NULL,
	[EventTypeEntitySchema] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
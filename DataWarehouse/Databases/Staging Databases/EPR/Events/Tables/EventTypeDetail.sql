﻿CREATE TABLE [Events].[EventTypeDetail](
	[EventTypeDetailID] [int] IDENTITY(1,1) NOT NULL,
	[EventTypeID] [int] NOT NULL,
	[EventTypeEntityID] [int] NOT NULL,
	[EventTypeEntitySettings] [xml] NOT NULL,
	[Sequence] [smallint] NOT NULL,
	[EventTypeDetailLabel] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
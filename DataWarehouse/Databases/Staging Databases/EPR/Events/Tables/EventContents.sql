﻿CREATE TABLE [Events].[EventContents](
	[EventContentId] [int] IDENTITY(1,1) NOT NULL,
	[EventTypeID] [int] NOT NULL,
	[Content] [xml] NOT NULL,
	[created] [datetime2](7) NOT NULL,
	[createdby] [nvarchar](50) NOT NULL,
	[Version] [smallint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
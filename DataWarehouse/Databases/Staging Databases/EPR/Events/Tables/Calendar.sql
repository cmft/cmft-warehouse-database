﻿CREATE TABLE [Events].[Calendar](
	[CalendarID] [int] IDENTITY(1,1) NOT NULL,
	[CalendarName] [Chameleon].[Name] NOT NULL,
	[CalendarDescription] [Chameleon].[Description] NOT NULL,
	[CalendarFreeColour] [nvarchar](6) NULL,
	[CalendarBookedColour] [nvarchar](6) NULL,
	[CalendarColour] [nvarchar](6) NULL,
	[CalendarFreeTextColour] [nvarchar](6) NULL,
	[CalendarBookedTextColour] [nvarchar](6) NULL
) ON [PRIMARY]
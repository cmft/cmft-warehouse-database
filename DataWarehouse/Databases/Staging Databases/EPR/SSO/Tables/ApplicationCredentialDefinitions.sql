﻿CREATE TABLE [SSO].[ApplicationCredentialDefinitions](
	[CredentialDefinitionID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [int] NULL,
	[CredentialDefinition] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [SSO].[ClientComputerDetails](
	[ClientComputerDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[ComputerName] [varchar](50) NOT NULL,
	[CwsExePath] [varchar](255) NULL
) ON [PRIMARY]
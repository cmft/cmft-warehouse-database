﻿CREATE TABLE [SSO].[ApplicationCredentials](
	[CredentialID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [int] NOT NULL,
	[fullLoginName] [varchar](128) NOT NULL,
	[Credentials] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Booking].[BookingVersions](
	[BookingId] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[Grade] [nvarchar](128) NOT NULL,
	[BookingStatusId] [int] NOT NULL,
	[FromDateTime] [datetime] NOT NULL,
	[ToDateTime] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_BookingVersions_CreatedOn]  DEFAULT (getdate()),
	[loginName] [varchar](128) NOT NULL,
	[CurrentVersion] [bit] NOT NULL CONSTRAINT [DF_BookingVersions_Current]  DEFAULT ((1)),
	[EventDetail] [nvarchar](max) NULL,
	[BookingTitle] [nvarchar](128) NULL,
	[Casenote] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
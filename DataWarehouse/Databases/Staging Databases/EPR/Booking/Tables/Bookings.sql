﻿CREATE TABLE [Booking].[Bookings](
	[BookingId] [int] IDENTITY(1,1) NOT NULL,
	[EventContentId] [int] NULL,
	[CalendarID] [int] NOT NULL,
	[Casenote] [nvarchar](50) NOT NULL,
	[DistrictNumber] [varchar](8) NULL
) ON [PRIMARY]
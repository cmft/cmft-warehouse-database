﻿CREATE TABLE [Booking].[BookingStatuses](
	[BookingStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL CONSTRAINT [DF_BookingStatuses_Name]  DEFAULT ('Unknown'),
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
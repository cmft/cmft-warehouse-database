﻿CREATE TABLE [Task].[Tasks](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[TaskStatus] [char](1) NOT NULL,
	[Result] [char](1) NULL,
	[RequiredBy] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Active] [bit] NOT NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
	[StaffId] [int] NOT NULL
) ON [PRIMARY]
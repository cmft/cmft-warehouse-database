﻿CREATE TABLE [MREH].[LvaDemonstrationOutcome](
	[LvaDemonstrationOutcomeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaDemonstrationOutcome_Name]  DEFAULT (N'Unknown'),
	[Description] [nvarchar](500) NOT NULL CONSTRAINT [DF_LvaDemonstrationOutcome_Description]  DEFAULT (''),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_LvaDemonstrationOutcome_CreatedDate]  DEFAULT (getutcdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF_LvaDemonstrationOutcome_UpdatedDate]  DEFAULT (getutcdate()),
	[CreatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaDemonstrationOutcome_CreatedBy]  DEFAULT (N'Unknown'),
	[UpdatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaDemonstrationOutcome_UpdatedBy]  DEFAULT (N'Unknown')
) ON [PRIMARY]
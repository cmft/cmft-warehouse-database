﻿CREATE TABLE [MREH].[LowVisionAidMake](
	[LowVisionAidMakeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaApparatusMake_Name]  DEFAULT (N'Unknown'),
	[Description] [nvarchar](500) NOT NULL CONSTRAINT [DF_LvaApparatusMake_Description]  DEFAULT (''),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_LvaApparatusMake_CreatedDate]  DEFAULT (getutcdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF_LvaApparatusMake_UpdatedDate]  DEFAULT (getutcdate()),
	[CreatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaApparatusMake_CreatedBy]  DEFAULT (N'Unknown'),
	[UpdatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaApparatusMake_UpdatedBy]  DEFAULT (N'Unknown')
) ON [PRIMARY]
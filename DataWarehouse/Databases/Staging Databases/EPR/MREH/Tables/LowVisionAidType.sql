﻿CREATE TABLE [MREH].[LowVisionAidType](
	[LowVisionAidTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaApparatusType_Name]  DEFAULT (N'Unknown'),
	[Description] [nvarchar](500) NOT NULL CONSTRAINT [DF_LvaApparatusType_Description]  DEFAULT (''),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_LvaApparatusType_CreatedDate]  DEFAULT (getdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF_LvaApparatusType_UpdatedDate]  DEFAULT (getutcdate()),
	[CreatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaApparatusType_CreatedBy]  DEFAULT (N'Unknown'),
	[UpdatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaApparatusType_UpdatedBy]  DEFAULT (N'Unknown')
) ON [PRIMARY]
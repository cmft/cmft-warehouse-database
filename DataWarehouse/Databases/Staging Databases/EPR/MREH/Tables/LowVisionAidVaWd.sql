﻿CREATE TABLE [MREH].[LowVisionAidVaWd](
	[LowVisionAidVaWdId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL CONSTRAINT [DF_LowVisionAidVaWd_Name]  DEFAULT (N'Unknown'),
	[Description] [nvarchar](500) NOT NULL CONSTRAINT [DF_LowVisionAidVaWd_Description]  DEFAULT (''),
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_LowVisionAidVaWd_CreatedDate]  DEFAULT (getutcdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF_LowVisionAidVaWd_UpdatedDate]  DEFAULT (getutcdate()),
	[CreatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LowVisionAidVaWd_CreatedBy]  DEFAULT (N'Unknown'),
	[UpdatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LowVisionAidVaWd_UpdatedBy]  DEFAULT (N'Unknown')
) ON [PRIMARY]
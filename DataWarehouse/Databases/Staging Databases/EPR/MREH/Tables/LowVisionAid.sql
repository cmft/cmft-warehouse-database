﻿CREATE TABLE [MREH].[LowVisionAid](
	[LowVisionAidId] [int] IDENTITY(1,1) NOT NULL,
	[LowVisionAidMakeId] [int] NOT NULL CONSTRAINT [DF_LvaApparatus_LvaApparatusMakeId]  DEFAULT ((1)),
	[LowVisionAidTypeId] [int] NOT NULL CONSTRAINT [DF_LvaApparatus_LvaApparatusTypeId]  DEFAULT ((1)),
	[LowVisionAidVaWdId] [int] NOT NULL CONSTRAINT [DF_LowVisionAid_LowVisionAidVaWdId]  DEFAULT ((1)),
	[ModelNumber] [nvarchar](100) NOT NULL CONSTRAINT [DF_LowVisionAid_ModelNumber]  DEFAULT (N'NA'),
	[Obsolescent] [bit] NOT NULL CONSTRAINT [DF_LvaApparatus_Obsolescent]  DEFAULT ((0)),
	[ObsolescentDate] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_LvaApparatus_CreatedDate]  DEFAULT (getutcdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF_LvaApparatus_UpdatedDate]  DEFAULT (getutcdate()),
	[CreatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaApparatus_CreatedBy]  DEFAULT (N'Unknown'),
	[UpdatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LvaApparatus_UpdatedBy]  DEFAULT (N'Unknown'),
	[LvaTypeId] [int] NULL,
	[LvaFunctionId] [int] NULL,
	[IlluminationId] [int] NULL,
	[Illuminated] [bit] NULL,
	[Vision] [nvarchar](20) NULL,
	[IsMag] [bit] NULL,
	[MagD] [varchar](25) NULL,
	[StatusId] [int] NULL
) ON [PRIMARY]
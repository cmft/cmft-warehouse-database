﻿CREATE TABLE [MREH].[LowVisionAidAssessment](
	[LowVisionAidAssessmentId] [int] IDENTITY(1,1) NOT NULL,
	[Casenote] [nvarchar](100) NOT NULL,
	[LowVisionAidId] [int] NOT NULL CONSTRAINT [DF_LowVisionAssessment_LvaApparatusId]  DEFAULT ((1)),
	[LvaDemonstrationOutcomeId] [int] NOT NULL CONSTRAINT [DF_LowVisionAssessment_LvaDemonstrationOutcomeId]  DEFAULT ((1)),
	[Comment] [nvarchar](max) NOT NULL CONSTRAINT [DF_LowVisionAssessment_Description]  DEFAULT (''),
	[ReturnDate] [datetime] NULL,
	[LoanDate] [datetime] NULL,
	[DemoDate] [datetime] NULL,
	[ExistingDate] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_LowVisionAssessment_CreatedDate]  DEFAULT (getutcdate()),
	[UpdatedDate] [datetime] NOT NULL CONSTRAINT [DF_LowVisionAssessment_UpdatedDate]  DEFAULT (getutcdate()),
	[CreatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LowVisionAssessment_CreatedBy]  DEFAULT (N'Unknown'),
	[UpdatedBy] [nvarchar](100) NOT NULL CONSTRAINT [DF_LowVisionAssessment_UpdatedBy]  DEFAULT (N'Unknown'),
	[DistrictNumber] [varchar](8) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
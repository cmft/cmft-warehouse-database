﻿CREATE TABLE [Patient].[PatientApplicationIdentity](
	[PatientId] [int] NOT NULL,
	[SourceApplicationIdentifierId] [int] NOT NULL,
	[PatientApplicationIdentityValue] [nvarchar](50) NOT NULL
) ON [PRIMARY]
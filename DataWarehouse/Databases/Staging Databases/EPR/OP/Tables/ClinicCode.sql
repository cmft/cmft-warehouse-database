﻿CREATE TABLE [OP].[ClinicCode](
	[ClinicCodeID] [int] IDENTITY(1,1) NOT NULL,
	[PASClinicCode] [varchar](20) NOT NULL
) ON [PRIMARY]
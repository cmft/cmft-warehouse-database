﻿CREATE TABLE [OP].[Clinic](
	[SourceContextCode] [varchar](20) NOT NULL,
	[SourceContext] [varchar](100) NOT NULL,
	[SourceClinicID] [int] NOT NULL,
	[SourceClinicCode] [varchar](100) NOT NULL,
	[SourceClinic] [varchar](900) NOT NULL,
	[LocalClinicCode] [varchar](50) NULL,
	[LocalClinic] [varchar](200) NULL,
	[NationalClinicCode] [varchar](50) NULL,
	[NationalClinic] [varchar](200) NULL,
	[ClinicDisplayColour] [nvarchar](7) NULL CONSTRAINT [DF_Clinic_ClinicDisplayColour]  DEFAULT (N'#428BCA')
) ON [PRIMARY]
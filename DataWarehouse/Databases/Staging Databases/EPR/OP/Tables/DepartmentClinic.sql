﻿CREATE TABLE [OP].[DepartmentClinic](
	[DepartmentClinicID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[SourceClinicID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [OP].[AppointmentStatus](
	[SourceContextCode] [varchar](20) NOT NULL,
	[SourceContext] [varchar](100) NOT NULL,
	[SourceAppointmentStatusID] [int] NOT NULL,
	[SourceAppointmentStatusCode] [varchar](100) NOT NULL,
	[SourceAppointmentStatus] [varchar](900) NOT NULL,
	[LocalAppointmentStatusCode] [varchar](50) NULL,
	[LocalAppointmentStatus] [varchar](200) NULL,
	[NationalAppointmentStatusCode] [varchar](50) NULL,
	[NationalAppointmentStatus] [varchar](200) NULL
) ON [PRIMARY]
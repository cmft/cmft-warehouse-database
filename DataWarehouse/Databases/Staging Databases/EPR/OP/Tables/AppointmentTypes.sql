﻿CREATE TABLE [OP].[AppointmentTypes](
	[SourceContextCode] [varchar](20) NOT NULL,
	[SourceContext] [varchar](100) NOT NULL,
	[SourceAppointmentTypeID] [int] NOT NULL,
	[SourceAppointmentTypeCode] [varchar](100) NOT NULL,
	[SourceAppointmentType] [varchar](900) NOT NULL,
	[LocalAppointmentTypeCode] [varchar](50) NULL,
	[LocalAppointmentType] [varchar](200) NULL,
	[NationalAppointmentTypeCode] [varchar](50) NULL,
	[NationalAppointmentType] [varchar](200) NULL
) ON [PRIMARY]
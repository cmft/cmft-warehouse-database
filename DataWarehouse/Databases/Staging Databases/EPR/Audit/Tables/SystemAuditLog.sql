﻿CREATE TABLE [Audit].[SystemAuditLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](50) NULL,
	[Grade] [varchar](50) NULL,
	[Roles] [varchar](250) NULL,
	[Message] [varchar](max) NULL,
	[AdditionalInfo] [varchar](max) NULL,
	[Logger] [nvarchar](255) NULL,
	[Browser] [varchar](250) NULL,
	[IPAddress] [varchar](15) NULL,
	[TimeStamp] [datetime] NULL CONSTRAINT [DF_SystemAuditLog_LogDate]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
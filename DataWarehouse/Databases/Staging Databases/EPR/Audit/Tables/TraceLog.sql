﻿CREATE TABLE [Audit].[TraceLog](
	[TraceId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Logger] [nvarchar](255) NULL,
	[TimeStamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
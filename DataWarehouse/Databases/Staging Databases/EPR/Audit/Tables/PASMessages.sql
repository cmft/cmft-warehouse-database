﻿CREATE TABLE [Audit].[PASMessages](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[PayloadHL7] [varchar](max) NOT NULL,
	[Casenote] [varchar](12) NULL,
	[ReceivedDateTime] [datetime] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
	[Notes] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
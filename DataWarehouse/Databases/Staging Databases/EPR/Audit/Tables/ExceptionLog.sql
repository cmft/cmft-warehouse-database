﻿CREATE TABLE [Audit].[ExceptionLog](
	[ExceptionId] [int] IDENTITY(1,1) NOT NULL,
	[LogLevel] [nvarchar](50) NULL,
	[Message] [nvarchar](max) NULL,
	[Logger] [nvarchar](255) NULL,
	[TimeStamp] [datetime] NULL CONSTRAINT [DF_ExceptionLog_ExceptionDate]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
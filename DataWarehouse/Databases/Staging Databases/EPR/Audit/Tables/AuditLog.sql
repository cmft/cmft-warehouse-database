﻿CREATE TABLE [Audit].[AuditLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Roles] [varchar](100) NULL,
	[ActionDescription] [varchar](250) NULL,
	[Message] [varchar](max) NULL,
	[IPaddress] [varchar](20) NULL,
	[Browser] [varchar](250) NULL,
	[Timestamp] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
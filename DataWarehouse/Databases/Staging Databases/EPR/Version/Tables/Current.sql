﻿CREATE TABLE [Version].[Current](
	[Major] [tinyint] NOT NULL,
	[Minor] [tinyint] NOT NULL,
	[Revision] [tinyint] NOT NULL,
	[Build] [tinyint] NOT NULL,
	[Version] [varchar](20) not null , -- AS ((((((CONVERT([varchar](20),[major],(0))+'.')+CONVERT([varchar](20),[minor],(0)))+'.')+CONVERT([varchar](20),[revision],(0)))+'.')+CONVERT([varchar](20),[build],(0))),
	[rolledOutDate] [datetime2](7) NOT NULL,
	[Notes] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [System].[LocationType](
	[LocationTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
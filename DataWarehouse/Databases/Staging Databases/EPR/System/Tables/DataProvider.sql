﻿CREATE TABLE [System].[DataProvider](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceSystemID] [int] NOT NULL,
	[CurrencyIndicatorID] [int] NOT NULL,
	[CurrencyInfo] [xml] NULL,
	[ProviderGroupID] [int] NOT NULL,
	[Precedence] [int] NOT NULL,
	[isActive] [bit] NOT NULL,
	[ProviderInfo] [xml] NOT NULL,
	[LastUsed] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
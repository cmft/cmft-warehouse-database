﻿CREATE TABLE [System].[Location](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[WardCode] [nvarchar](15) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Active] [bit] NOT NULL,
	[DivisionID] [int] NOT NULL,
	[LocationTypeID] [int] NOT NULL
) ON [PRIMARY]
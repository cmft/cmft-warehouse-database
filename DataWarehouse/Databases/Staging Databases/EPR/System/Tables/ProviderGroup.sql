﻿CREATE TABLE [System].[ProviderGroup](
	[ProviderGroupID] [int] IDENTITY(1,1) NOT NULL,
	[ProviderGroupName] [Chameleon].[Name] NOT NULL,
	[ProviderGroupDescription] [Chameleon].[Description] NOT NULL
) ON [PRIMARY]
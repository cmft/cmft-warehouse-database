﻿CREATE TABLE [System].[CurrencyIndicator](
	[CurrencyIndicatorID] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyIndicatorName] [Chameleon].[Name] NOT NULL,
	[CurrencyIndicatorDescription] [Chameleon].[Description] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [System].[SourceApplicationIdentifier](
	[SourceApplicationIdentifierId] [int] IDENTITY(1,1) NOT NULL,
	[SourceApplicationContextId] [int] NOT NULL,
	[SourceApplicationIdentifierTypeId] [tinyint] NOT NULL CONSTRAINT [DF_SourceApplicationIdentifier_SourceApplicationIdendifierTypeId]  DEFAULT ((1)),
	[Name] [nvarchar](50) NOT NULL CONSTRAINT [DF_SourceApplicationIdentifier_Name]  DEFAULT (N'Unknown'),
	[Description] [Chameleon].[Description] NOT NULL CONSTRAINT [DF_SourceApplicationIdentifier_Description]  DEFAULT (''),
	[HL7AssigningAuthority] [varchar](50) NOT NULL CONSTRAINT [DF_SourceApplicationIdentifier_HL7AssigningAuthority]  DEFAULT ('Unknown'),
	[HL7AssigningFacility] [varchar](50) NOT NULL CONSTRAINT [DF_SourceApplicationIdentifier_HL7AssigningFacility]  DEFAULT ('Unknown'),
	[HL7IdentifierType] [varchar](50) NOT NULL CONSTRAINT [DF_SourceApplicationIdentifier_HL7IdentifierType]  DEFAULT ('Unknown')
) ON [PRIMARY]
﻿CREATE TABLE [System].[Applications](
	[ApplicationID] [int] IDENTITY(1,1) NOT NULL,
	[SystemName] [nvarchar](50) NOT NULL,
	[IsSSOEnabled] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [System].[SourceSystem](
	[SourceSystemID] [int] IDENTITY(1,1) NOT NULL,
	[SourceSystemName] [Chameleon].[Name] NOT NULL,
	[SourceSystemDescription] [Chameleon].[Description] NOT NULL
) ON [PRIMARY]
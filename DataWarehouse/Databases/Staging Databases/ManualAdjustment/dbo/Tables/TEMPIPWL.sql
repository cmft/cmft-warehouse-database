﻿CREATE TABLE [dbo].[TEMPIPWL](
	[PATNUM] [nvarchar](9) NULL,
	[WLENUM] [nvarchar](9) NULL,
	[Distnum] [nvarchar](9) NULL,
	[MONTHNUM] [smallint] NULL,
	[WLREPCAT] [int] NULL,
	[REPTSECT] [nvarchar](255) NULL,
	[CAMRPCHR] [nvarchar](255) NULL,
	[Specialty] [nvarchar](8) NULL,
	[CAMRSPEC] [nvarchar](255) NULL,
	[SPECNAME] [nvarchar](255) NULL,
	[Division] [nvarchar](255) NULL,
	[DIR] [float] NULL,
	[Dirname] [nvarchar](255) NULL,
	[CASENOTE] [nvarchar](14) NULL,
	[PC] [nvarchar](8) NULL,
	[PTHA] [nvarchar](255) NULL,
	[GP] [nvarchar](255) NULL,
	[Practice] [nvarchar](6) NULL,
	[oldPCT] [nvarchar](255) NULL,
	[SHA] [nvarchar](255) NULL,
	[PCT] [nvarchar](255) NULL,
	[GPFH] [nvarchar](255) NULL,
	[PCHR] [nvarchar](255) NULL,
	[CONS] [nvarchar](8) NULL,
	[SPEC] [nvarchar](8) NULL,
	[WARD] [nvarchar](8) NULL,
	[INTPROC] [nvarchar](8) NULL,
	[SL] [nvarchar](255) NULL,
	[INTMAN] [nvarchar](1) NULL,
	[MOA] [nvarchar](2) NULL,
	[WLSTAT] [nvarchar](20) NULL,
	[CAT] [nvarchar](3) NULL,
	[ELECDATE] [smalldatetime] NULL,
	[EFELDATE] [smalldatetime] NULL,
	[EFMNTHS] [int] NULL,
	[EFWEEKS] [float] NULL,
	[TCI] [smalldatetime] NULL,
	[EXADDATE] [smalldatetime] NULL,
	[URG] [nvarchar](2) NULL,
	[WL] [nvarchar](6) NULL,
	[WLDG] [nvarchar](8) NULL,
	[ADMREAS] [nvarchar](60) NULL,
	[PREOCM] [nvarchar](255) NULL,
	[SUSPS] [nvarchar](255) NULL,
	[SUSPDAYS] [int] NULL,
	[SELFDEFS] [nvarchar](255) NULL,
	[SUSPDATE] [nvarchar](255) NULL,
	[SUSPREAS] [nvarchar](255) NULL,
	[SUSRDATE] [smalldatetime] NULL,
	[M00] [int] NULL,
	[M01] [int] NULL,
	[M02] [int] NULL,
	[M03] [int] NULL,
	[M04] [int] NULL,
	[M05] [int] NULL,
	[M06] [int] NULL,
	[M07] [int] NULL,
	[M08] [int] NULL,
	[M09] [int] NULL,
	[M10] [int] NULL,
	[M11] [int] NULL,
	[M12] [int] NULL,
	[M13] [int] NULL,
	[M14] [int] NULL,
	[M15] [int] NULL,
	[M16] [int] NULL,
	[M17] [int] NULL,
	[M18] [int] NULL,
	[M0002] [int] NULL,
	[M0305] [int] NULL,
	[M0608] [int] NULL,
	[M0911] [int] NULL,
	[M1214] [int] NULL,
	[M1517] [int] NULL,
	[M0005] [int] NULL,
	[M0611] [int] NULL,
	[M1217] [int] NULL,
	[HOSP] [nvarchar](8) NULL,
	[CensusDate] [smalldatetime] NULL,
	[Diag_Flag] [nvarchar](255) NULL,
	[Diag_Line] [nvarchar](255) NULL,
	[diag] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [rtt].[AdjustTreatType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[InternalNo] [nvarchar](150) NULL,
	[EpisodeNo] [nvarchar](10) NULL,
	[IP_NON_IP] [nvarchar](10) NULL,
	[Email_date_time] [datetime] NULL,
	[CreatedBy] [nvarchar](255) NULL
) ON [PRIMARY]
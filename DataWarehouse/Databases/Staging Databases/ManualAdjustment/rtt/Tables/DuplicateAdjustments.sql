﻿CREATE TABLE [rtt].[DuplicateAdjustments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[InternalNo] [nvarchar](50) NOT NULL,
	[EpisodeNo] [nvarchar](50) NOT NULL,
	[LEpisodeNo] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](200) NULL,
	[Comments] [nvarchar](255) NULL,
	[Actioned] [bit] NOT NULL,
	[UpdatedDate] [datetime] NULL
) ON [PRIMARY]
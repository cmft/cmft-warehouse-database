﻿CREATE TABLE [wl].[NamesList](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Directorate] [nvarchar](255) NULL,
	[Specialty] [nvarchar](255) NULL,
	[Contact] [nvarchar](255) NULL,
	[Report] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SupplierStandard](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TypeID] [int] NULL,
	[Number] [nvarchar](20) NULL,
	[AssessmentBodyID] [int] NULL,
	[Scope] [nvarchar](200) NULL,
	[Comment] [ntext] NULL,
	[SupplierID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
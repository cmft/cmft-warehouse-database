﻿CREATE TABLE [dbo].[MessageSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Status] [smallint] NULL,
	[Description] [nvarchar](2000) NULL,
	[TriggerOrder] [smallint] NULL,
	[Body] [image] NULL,
	[Format] [smallint] NULL,
	[Subject] [nvarchar](200) NULL,
	[AssocModule] [smallint] NULL,
	[DisplayName] [nvarchar](200) NULL,
	[ReportObjectTypeName] [nvarchar](200) NULL,
	[IncludeGoTo] [tinyint] NULL CONSTRAINT [DF_IncludeGoTo]  DEFAULT (0),
	[Version] [timestamp] NOT NULL,
	[CanIncludeICalendar] [bit] NULL CONSTRAINT [DF_CanIncludeICalendar]  DEFAULT (0),
	[ICalendarEnabled] [bit] NULL CONSTRAINT [DF_ICalendarEnabled]  DEFAULT (0),
	[ICalendarFieldMappings] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
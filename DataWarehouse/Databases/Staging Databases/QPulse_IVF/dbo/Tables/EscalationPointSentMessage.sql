﻿CREATE TABLE [dbo].[EscalationPointSentMessage](
	[EscalationPointID] [int] NULL,
	[EscalatedRecordID] [int] NULL,
	[SentDate] [datetime] NULL,
	[data] [varchar](100) NULL
) ON [PRIMARY]
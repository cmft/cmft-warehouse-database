﻿CREATE TABLE [dbo].[IncidentStageActionFolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[FolderType] [int] NULL,
	[Status] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[SearchCriteria] [ntext] NULL,
	[ViewInfo] [ntext] NULL,
	[Comment] [nvarchar](2000) NULL,
	[FolderCategoryID] [int] NULL,
	[LastModified] [datetime] NULL,
	[RegisterViewType] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[ActivityDefinition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EquipmentItemID] [int] NOT NULL,
	[ActivityTypeID] [int] NOT NULL,
	[OwnerPersonID] [int] NULL,
	[SupplierID] [int] NULL,
	[AssociatedDocumentID] [int] NULL,
	[ActivityPeriodType] [int] NULL,
	[FixedMeasure] [int] NULL,
	[FixedValue] [int] NULL,
	[FrequencyOfUsage] [int] NULL,
	[FrequencyOfUsageMeasure] [int] NULL,
	[Utilisation] [int] NULL,
	[UtilisationMeasure] [int] NULL,
	[DepartmentID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
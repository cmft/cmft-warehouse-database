﻿CREATE TABLE [dbo].[Deputy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DeputyID] [int] NULL,
	[DeputorID] [int] NULL,
	[DeputeeID] [int] NULL,
	[SecurableObjectTypeID] [int] NULL,
	[SecurableCommandID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentStage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StageTypeID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[TargetDate] [datetime] NULL,
	[Description] [nvarchar](4000) NULL,
	[ClosedDate] [datetime] NULL,
	[ClosedByPersonID] [int] NULL,
	[IncidentID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[Number] [nvarchar](200) NULL,
	[CostOfStage] [decimal](10, 2) NULL,
	[TimeForStage] [int] NULL,
	[IsRestricted] [bit] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ReportForm](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormName] [nvarchar](200) NULL,
	[FormElementList] [nvarchar](max) NULL,
	[FormLayoutXml] [xml] NULL,
	[FormPropertiesXml] [xml] NULL,
	[Description] [nvarchar](2000) NULL,
	[Height] [int] NOT NULL,
	[Width] [int] NOT NULL,
	[IsActive] [bit] NULL,
	[Version] [timestamp] NULL,
	[Archived] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
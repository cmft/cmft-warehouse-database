﻿CREATE TABLE [dbo].[EscalationPointRecipient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EscalationPointConditionID] [int] NULL,
	[PersonID] [int] NULL,
	[DynamicRecipientID] [int] NULL,
	[RecipientType] [smallint] NOT NULL,
	[NotifyType] [smallint] NOT NULL,
	[Data] [nvarchar](50) NULL,
	[GroupID] [int] NULL,
	[Version] [timestamp] NOT NULL,
	[IsManagerRecipient] [bit] NOT NULL CONSTRAINT [DF_EscalationManagerRecipient]  DEFAULT (0)
) ON [PRIMARY]
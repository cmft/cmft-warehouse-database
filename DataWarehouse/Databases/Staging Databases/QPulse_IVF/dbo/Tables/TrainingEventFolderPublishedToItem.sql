﻿CREATE TABLE [dbo].[TrainingEventFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainingEventFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
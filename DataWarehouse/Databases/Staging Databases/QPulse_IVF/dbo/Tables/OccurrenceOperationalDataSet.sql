﻿CREATE TABLE [dbo].[OccurrenceOperationalDataSet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](255) NULL,
	[AnnualDataPointXml] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
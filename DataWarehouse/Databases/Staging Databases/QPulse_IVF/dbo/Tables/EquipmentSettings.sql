﻿CREATE TABLE [dbo].[EquipmentSettings](
	[PersonID] [int] NULL,
	[Settings] [ntext] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
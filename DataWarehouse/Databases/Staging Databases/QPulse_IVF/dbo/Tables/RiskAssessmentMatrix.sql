﻿CREATE TABLE [dbo].[RiskAssessmentMatrix](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](200) NULL,
	[XAxisTitle] [nvarchar](200) NULL,
	[YAxisTitle] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
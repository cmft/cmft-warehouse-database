﻿CREATE TABLE [dbo].[yActivityOutcome](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[ItemNumberingFormat] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EquipmentItemAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StorageType] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[ParentID] [int] NULL,
	[RemoveFiles] [bit] NULL,
	[TargetFolderName] [nvarchar](200) NULL,
	[TargetLocation] [nvarchar](2000) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
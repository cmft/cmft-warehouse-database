﻿CREATE TABLE [dbo].[ApprovalCycleNote](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalCycleID] [int] NULL,
	[NoteSubject] [nvarchar](200) NULL,
	[NoteText] [ntext] NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
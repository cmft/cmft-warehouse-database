﻿CREATE TABLE [dbo].[ySupplierUserDefinedField9](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[DateCreated] [datetime] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
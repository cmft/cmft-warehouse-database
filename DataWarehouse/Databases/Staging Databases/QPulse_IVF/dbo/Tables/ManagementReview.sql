﻿CREATE TABLE [dbo].[ManagementReview](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HostPersonID] [int] NULL,
	[Description] [ntext] NULL,
	[TargetDate] [datetime] NULL,
	[PerformedDate] [datetime] NULL,
	[Summary] [ntext] NULL,
	[Conclusions] [ntext] NULL,
	[ManagementReviewLocationID] [int] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentActionFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OccurrenceIncidentActionFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CustomerFolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Comment] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[SearchCriteria] [ntext] NULL,
	[ViewInfo] [ntext] NULL,
	[FolderType] [int] NULL,
	[Status] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[FolderCategoryID] [int] NULL,
	[Version] [timestamp] NOT NULL,
	[LastModified] [datetime] NULL,
	[RegisterViewType] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
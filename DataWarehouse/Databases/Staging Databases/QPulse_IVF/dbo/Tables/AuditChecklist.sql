﻿CREATE TABLE [dbo].[AuditChecklist](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Title] [nvarchar](2000) NULL,
	[Description] [nvarchar](2000) NULL,
	[CompletedByPersonID] [int] NULL,
	[CompletedDate] [datetime] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
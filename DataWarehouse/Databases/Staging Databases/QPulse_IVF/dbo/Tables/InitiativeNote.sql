﻿CREATE TABLE [dbo].[InitiativeNote](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InitiativeID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[NoteText] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
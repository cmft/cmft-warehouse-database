﻿CREATE TABLE [dbo].[AuditLogViewerSettings](
	[PersonID] [int] NOT NULL,
	[Settings] [ntext] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
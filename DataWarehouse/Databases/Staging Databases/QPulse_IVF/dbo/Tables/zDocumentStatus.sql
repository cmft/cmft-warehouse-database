﻿CREATE TABLE [dbo].[zDocumentStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL
) ON [PRIMARY]
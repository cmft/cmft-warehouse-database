﻿CREATE TABLE [dbo].[DocumentReview](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DocumentID] [int] NULL,
	[ReviewedByPersonID] [int] NULL,
	[OutcomeID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[RevisionReference] [nvarchar](200) NULL,
	[ReviewedDate] [datetime] NULL,
	[Comment] [ntext] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
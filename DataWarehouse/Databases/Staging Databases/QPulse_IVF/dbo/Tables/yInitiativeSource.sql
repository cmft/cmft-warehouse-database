﻿CREATE TABLE [dbo].[yInitiativeSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[IsVisible] [bit] NULL,
	[SortOrder] [int] NULL,
	[CalculatedPath] [nvarchar](2000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SupplierReview](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierID] [int] NULL,
	[ResponsiblePersonID] [int] NULL,
	[PerformedByPersonID] [int] NULL,
	[TargetDate] [datetime] NULL,
	[PerformedDate] [datetime] NULL,
	[ReasonForReview] [ntext] NULL,
	[ReviewResult] [nvarchar](200) NULL,
	[SupplierApprovalStatusID] [int] NULL,
	[SupplierApprovalBasisID] [int] NULL,
	[Rating] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[yAuditCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[AuditNumberDefinitionID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
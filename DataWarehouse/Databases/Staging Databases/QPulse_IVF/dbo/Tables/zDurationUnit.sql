﻿CREATE TABLE [dbo].[zDurationUnit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[Name] [nvarchar](200) NULL
) ON [PRIMARY]
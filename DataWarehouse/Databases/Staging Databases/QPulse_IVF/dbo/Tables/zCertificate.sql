﻿CREATE TABLE [dbo].[zCertificate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[DateCreated] [datetime] NULL
) ON [PRIMARY]
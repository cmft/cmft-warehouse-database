﻿CREATE TABLE [dbo].[zIncidentStageType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
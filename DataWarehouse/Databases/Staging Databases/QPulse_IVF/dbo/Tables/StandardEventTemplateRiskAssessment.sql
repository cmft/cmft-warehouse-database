﻿CREATE TABLE [dbo].[StandardEventTemplateRiskAssessment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StandardEventTemplateID] [int] NULL,
	[Title] [nvarchar](200) NULL,
	[MatrixUsed] [nvarchar](200) NULL,
	[MatrixXValue] [nvarchar](200) NULL,
	[MatrixYValue] [nvarchar](200) NULL,
	[RiskValue] [nvarchar](200) NULL,
	[OwnerPersonID] [int] NULL,
	[CreatorIsOwner] [bit] NULL,
	[DefaultActionID] [int] NULL,
	[MatrixXAxisTitle] [nvarchar](200) NULL,
	[MatrixYAxisTitle] [nvarchar](200) NULL,
	[Comment] [nvarchar](2000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WizardTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](2000) NULL,
	[ModuleKey] [int] NULL,
	[WizardDefs] [image] NULL,
	[WizardModule] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[yProductService](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[SortOrder] [int] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[Name] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[IncidentStageActionFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentStageActionFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
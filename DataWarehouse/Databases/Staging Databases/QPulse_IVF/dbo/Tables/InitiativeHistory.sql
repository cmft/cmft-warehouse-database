﻿CREATE TABLE [dbo].[InitiativeHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InitiativeID] [int] NULL,
	[PerformedByPersonID] [int] NULL,
	[Date] [datetime] NULL,
	[EventType] [int] NULL,
	[Description] [ntext] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentCost](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentID] [int] NULL,
	[CostCategoryID] [int] NULL,
	[Cost] [decimal](10, 2) NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
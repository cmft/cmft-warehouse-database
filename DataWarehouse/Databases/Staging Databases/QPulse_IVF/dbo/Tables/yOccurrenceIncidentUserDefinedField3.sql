﻿CREATE TABLE [dbo].[yOccurrenceIncidentUserDefinedField3](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[ParentID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[IsVisible] [bit] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
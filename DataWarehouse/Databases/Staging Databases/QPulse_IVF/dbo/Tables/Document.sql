﻿CREATE TABLE [dbo].[Document](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](200) NULL,
	[UserDefinedField1ID] [int] NULL,
	[UserDefinedField2ID] [int] NULL,
	[UserDefinedField3ID] [int] NULL,
	[UserDefinedField4ID] [int] NULL,
	[UserDefinedField5PersonID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[DocumentTypeID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
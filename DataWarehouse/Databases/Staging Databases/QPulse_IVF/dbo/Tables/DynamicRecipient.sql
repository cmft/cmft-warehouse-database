﻿CREATE TABLE [dbo].[DynamicRecipient](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[ObjectPath] [nvarchar](500) NULL,
	[Properties] [nvarchar](100) NULL,
	[ApplyTo] [smallint] NULL,
	[ModuleID] [int] NULL,
	[Condition] [text] NULL,
	[IsManagerRecipient] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
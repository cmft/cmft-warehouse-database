﻿CREATE TABLE [dbo].[AnalysisFolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FolderCategoryID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[FolderType] [int] NULL,
	[Status] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[Comment] [nvarchar](200) NULL,
	[DocumentDef] [ntext] NULL,
	[ModuleKey] [int] NULL,
	[ViewDef] [ntext] NULL,
	[SearchCriteria] [ntext] NULL,
	[SimpleSearchCriteria] [ntext] NULL,
	[IsSimpleSearchActive] [bit] NULL,
	[IsTrend] [bit] NULL,
	[Version] [timestamp] NOT NULL,
	[LastModified] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[yPersonUserDefinedField2](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[ParentID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[SortOrder] [int] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[Name] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
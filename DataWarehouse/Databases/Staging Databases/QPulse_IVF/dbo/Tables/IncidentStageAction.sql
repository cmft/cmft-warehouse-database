﻿CREATE TABLE [dbo].[IncidentStageAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentStageID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[Details] [ntext] NULL,
	[TargetDate] [datetime] NULL,
	[CompletedDate] [datetime] NULL,
	[ResponseDetails] [ntext] NULL,
	[CompletedByPersonID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Number] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
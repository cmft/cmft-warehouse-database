﻿CREATE TABLE [dbo].[OccurrenceIncidentFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OccurrenceIncidentFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
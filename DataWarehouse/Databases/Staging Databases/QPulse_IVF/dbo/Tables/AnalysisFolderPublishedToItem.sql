﻿CREATE TABLE [dbo].[AnalysisFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[AnalysisFolderID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CustomFieldLabel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomFieldInfoID] [int] NULL,
	[LabelText] [nvarchar](200) NULL,
	[Culture] [varchar](10) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
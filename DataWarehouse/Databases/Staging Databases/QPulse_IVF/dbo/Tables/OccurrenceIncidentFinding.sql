﻿CREATE TABLE [dbo].[OccurrenceIncidentFinding](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StageID] [int] NULL,
	[Number] [nvarchar](200) NULL,
	[RaisedDate] [datetime] NULL,
	[RaisedByPersonID] [int] NULL,
	[Description] [nvarchar](2000) NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
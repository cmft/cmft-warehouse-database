﻿CREATE TABLE [dbo].[DocumentRevisionCheckOut](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CheckOutDate] [datetime] NULL,
	[CheckOutReason] [ntext] NULL,
	[CheckOutLocation] [nvarchar](2000) NULL,
	[RevisionID] [int] NULL,
	[CheckOutPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
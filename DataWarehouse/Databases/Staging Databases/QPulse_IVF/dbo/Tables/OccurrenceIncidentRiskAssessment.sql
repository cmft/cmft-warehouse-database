﻿CREATE TABLE [dbo].[OccurrenceIncidentRiskAssessment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentID] [int] NULL,
	[MatrixUsed] [nvarchar](200) NULL,
	[MatrixXValue] [nvarchar](200) NULL,
	[MatrixYValue] [nvarchar](200) NULL,
	[RiskValue] [nvarchar](200) NULL,
	[PerformedDate] [datetime] NULL,
	[Comment] [ntext] NULL,
	[Title] [nvarchar](200) NULL,
	[DefaultActionID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[FindingID] [int] NULL,
	[NCID] [int] NULL,
	[MatrixXAxisTitle] [nvarchar](200) NULL,
	[MatrixYAxisTitle] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentStageFolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Comment] [nvarchar](2000) NULL,
	[FolderCategoryID] [int] NULL,
	[FolderType] [int] NULL,
	[Status] [int] NULL,
	[Condition] [nvarchar](255) NULL,
	[ViewInfo] [nvarchar](255) NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[LastModified] [datetime] NULL,
	[RegisterViewType] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
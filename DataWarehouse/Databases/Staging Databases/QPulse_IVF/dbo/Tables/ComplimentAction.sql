﻿CREATE TABLE [dbo].[ComplimentAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComplimentID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[PerformedByPersonID] [int] NULL,
	[TargetDate] [datetime] NULL,
	[ClosedDate] [datetime] NULL,
	[Details] [ntext] NULL,
	[ResponseDetails] [ntext] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
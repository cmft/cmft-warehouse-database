﻿CREATE TABLE [dbo].[CheckInNotification](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RevisionID] [int] NULL,
	[PersonID] [int] NULL,
	[RequestDate] [datetime] NULL,
	[NotifiedDate] [datetime] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY]
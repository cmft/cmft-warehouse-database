﻿CREATE TABLE [dbo].[xAuditee](
	[PersonID] [int] NOT NULL,
	[AuditID] [int] NOT NULL,
	[Note] [ntext] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[JobRoleRequirement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompetencyID] [int] NULL,
	[Title] [nvarchar](2000) NOT NULL,
	[TemplateID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[OccurrenceIncidentStageWorkflow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentID] [int] NULL,
	[StageID] [int] NULL,
	[WorkflowOrder] [int] NULL,
	[SortOrder] [int] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
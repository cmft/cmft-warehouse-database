﻿CREATE TABLE [dbo].[MessageGotoCondition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Condition] [text] NOT NULL,
	[TypeName] [varchar](100) NULL,
	[Created] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
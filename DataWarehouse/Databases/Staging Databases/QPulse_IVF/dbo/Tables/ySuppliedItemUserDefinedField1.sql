﻿CREATE TABLE [dbo].[ySuppliedItemUserDefinedField1](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[ParentID] [int] NULL,
	[UserDefinedTypeID] [int] NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[Name] [nvarchar](200) NULL,
	[CalculatedPath] [nvarchar](2000) NULL
) ON [PRIMARY]
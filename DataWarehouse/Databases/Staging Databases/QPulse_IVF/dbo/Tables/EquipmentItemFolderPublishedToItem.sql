﻿CREATE TABLE [dbo].[EquipmentItemFolderPublishedToItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EquipmentItemFolderID] [int] NULL,
	[PersonID] [int] NULL,
	[GroupID] [int] NULL
) ON [PRIMARY]
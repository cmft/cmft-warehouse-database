﻿CREATE TABLE [dbo].[PersonCompetency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[Title] [nvarchar](2000) NOT NULL,
	[LevelID] [int] NULL,
	[Description] [ntext] NULL,
	[StatusID] [int] NULL,
	[TargetDate] [datetime] NULL,
	[ActualDate] [datetime] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[IncidentMandatoryFieldSet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[xSupplierCertificate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierID] [int] NOT NULL,
	[AccreditationBodyCertificateID] [int] NOT NULL,
	[CertficateNumber] [nvarchar](200) NULL
) ON [PRIMARY]
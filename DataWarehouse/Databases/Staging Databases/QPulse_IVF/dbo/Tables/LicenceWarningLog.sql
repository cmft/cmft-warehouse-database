﻿CREATE TABLE [dbo].[LicenceWarningLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [char](200) NULL,
	[LicenceTypeReq] [nvarchar](50) NULL,
	[RequestTime] [datetime] NULL,
	[DatabaseName] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
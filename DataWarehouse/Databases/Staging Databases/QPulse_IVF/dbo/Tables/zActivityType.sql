﻿CREATE TABLE [dbo].[zActivityType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[IsVisible] [bit] NULL,
	[CreatedByPersonID] [int] NULL,
	[SortOrder] [int] NULL,
	[DateCreated] [datetime] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
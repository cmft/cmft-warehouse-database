﻿CREATE TABLE [dbo].[yFaultCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[IsVisible] [bit] NULL,
	[SortOrder] [int] NULL,
	[DateCreated] [datetime] NULL,
	[ParentID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
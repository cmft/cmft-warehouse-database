﻿CREATE TABLE [dbo].[yIncidentUserDefinedField2](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
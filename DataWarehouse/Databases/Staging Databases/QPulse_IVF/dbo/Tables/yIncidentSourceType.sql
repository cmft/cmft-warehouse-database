﻿CREATE TABLE [dbo].[yIncidentSourceType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[ParentID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[IncidentTemplateID] [int] NULL,
	[IncidentNumberDefinitionID] [int] NULL,
	[Version] [timestamp] NOT NULL,
	[IncidentMandatoryFieldSetID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AuditScopeItem](
	[AuditID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[IsMain] [bit] NULL,
	[IsPlanned] [bit] NULL,
	[IsActual] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
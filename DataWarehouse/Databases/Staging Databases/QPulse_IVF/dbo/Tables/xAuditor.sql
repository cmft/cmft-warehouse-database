﻿CREATE TABLE [dbo].[xAuditor](
	[PersonID] [int] NOT NULL,
	[AuditID] [int] NOT NULL,
	[IsLeadAuditor] [bit] NULL,
	[Note] [ntext] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
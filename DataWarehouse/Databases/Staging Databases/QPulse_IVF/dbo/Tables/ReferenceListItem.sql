﻿CREATE TABLE [dbo].[ReferenceListItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ListTypeID] [int] NOT NULL,
	[ParentID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[SortOrder] [int] NULL,
	[AdditionalColumns] [xml] NULL,
	[DateCreated] [datetime] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[IsVisible] [bit] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
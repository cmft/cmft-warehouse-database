﻿CREATE TABLE [dbo].[SupplierHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierID] [int] NULL,
	[PerformedByPersonID] [int] NULL,
	[Description] [ntext] NULL,
	[Date] [datetime] NULL,
	[EventType] [int] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
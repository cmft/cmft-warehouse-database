﻿CREATE TABLE [dbo].[SuppliedItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](200) NULL,
	[SuppliedItemCategoryID] [int] NULL,
	[PreferredSupplierID] [int] NULL,
	[Description] [nvarchar](200) NULL,
	[IsApproved] [bit] NULL,
	[SuppliedItemUserDefinedField1ID] [int] NULL,
	[SuppliedItemUserDefinedField2ID] [int] NULL,
	[CreatedByPersonID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EMailAddress](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[Address] [nvarchar](200) NULL,
	[IsDefault] [bit] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ManagedListTypeDefinition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ListType] [nvarchar](200) NOT NULL,
	[UseSearch] [bit] NOT NULL
) ON [PRIMARY]
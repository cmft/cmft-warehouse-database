﻿CREATE TABLE [dbo].[Group](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[IncidentHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IncidentID] [int] NULL,
	[PerformedByPersonID] [int] NULL,
	[Date] [datetime] NULL,
	[Description] [ntext] NULL,
	[EventType] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[yWorkOrderNumber](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[Name] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
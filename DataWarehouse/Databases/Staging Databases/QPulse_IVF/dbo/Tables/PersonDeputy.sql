﻿CREATE TABLE [dbo].[PersonDeputy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [int] NULL,
	[DeputeeID] [int] NULL,
	[Description] [nvarchar](200) NULL
) ON [PRIMARY]
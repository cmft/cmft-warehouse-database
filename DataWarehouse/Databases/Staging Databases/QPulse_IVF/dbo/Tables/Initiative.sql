﻿CREATE TABLE [dbo].[Initiative](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[OriginatorPersonID] [int] NULL,
	[OwnerPersonID] [int] NULL,
	[Number] [nvarchar](200) NULL,
	[Title] [nvarchar](200) NULL,
	[Details] [ntext] NULL,
	[TargetDate] [datetime] NULL,
	[RaisedDate] [datetime] NULL,
	[ClosedDate] [datetime] NULL,
	[ClosureSummary] [ntext] NULL,
	[InitiativeStatusID] [int] NULL,
	[InitiativePriorityID] [int] NULL,
	[Consequences] [ntext] NULL,
	[Keywords] [ntext] NULL,
	[InitiativeSourceID] [int] NULL,
	[OriginatedFromDepartmentID] [int] NULL,
	[OrginatedFromSupplierID] [int] NULL,
	[OriginatedFromCustomerID] [int] NULL,
	[RelatedAreaOfStandardID] [int] NULL,
	[RelatedDocumentID] [int] NULL,
	[RelatedSuppliedItemID] [int] NULL,
	[RaisedInManagementReviewID] [int] NULL,
	[RaisedInAuditID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
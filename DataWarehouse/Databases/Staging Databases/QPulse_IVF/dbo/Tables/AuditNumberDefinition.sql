﻿CREATE TABLE [dbo].[AuditNumberDefinition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Prefix] [nvarchar](200) NULL,
	[Suffix] [nvarchar](200) NULL,
	[Number] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
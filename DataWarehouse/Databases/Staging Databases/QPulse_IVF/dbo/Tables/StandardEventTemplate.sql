﻿CREATE TABLE [dbo].[StandardEventTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[OwnerPersonID] [int] NULL,
	[CreatorIsOwner] [bit] NULL,
	[Version] [timestamp] NOT NULL,
	[PriorityID] [int] NULL,
	[DepartmentID] [int] NULL
) ON [PRIMARY]
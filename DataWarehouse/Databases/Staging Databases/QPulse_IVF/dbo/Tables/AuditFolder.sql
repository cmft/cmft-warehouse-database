﻿CREATE TABLE [dbo].[AuditFolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FolderCategoryID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Name] [nvarchar](200) NULL,
	[FolderType] [int] NULL,
	[Status] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[SearchCriteria] [ntext] NULL,
	[ViewInfo] [ntext] NULL,
	[Comment] [nvarchar](2000) NULL,
	[Version] [timestamp] NOT NULL,
	[LastModified] [datetime] NULL,
	[RegisterViewType] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
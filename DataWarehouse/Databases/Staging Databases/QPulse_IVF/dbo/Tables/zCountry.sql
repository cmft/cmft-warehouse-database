﻿CREATE TABLE [dbo].[zCountry](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[SortOrder] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CompetencyTemplateLevel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompetencyTemplateID] [int] NULL,
	[LevelID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[JobRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Responsibilities] [ntext] NULL,
	[Description] [ntext] NULL,
	[IsArchived] [bit] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
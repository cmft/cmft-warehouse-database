﻿CREATE TABLE [dbo].[MatrixResult](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](2000) NULL,
	[MatrixID] [int] NULL,
	[DefaultActionID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[yAssessmentBody](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[IsVisible] [bit] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[ParentID] [int] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
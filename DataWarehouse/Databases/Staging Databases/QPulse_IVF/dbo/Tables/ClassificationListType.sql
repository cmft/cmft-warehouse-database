﻿CREATE TABLE [dbo].[ClassificationListType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[SortType] [int] NULL,
	[IsArchived] [bit] NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NULL
) ON [PRIMARY]
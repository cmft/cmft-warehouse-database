﻿CREATE TABLE [dbo].[ClassificationItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Code] [nvarchar](200) NULL,
	[Description] [nvarchar](2000) NULL,
	[ParentID] [int] NULL,
	[SortOrder] [int] NULL,
	[ForeignID] [int] NULL,
	[CalculatedPath] [nvarchar](2000) NULL,
	[DateCreated] [datetime] NULL,
	[CreatedByPersonID] [int] NULL,
	[Version] [timestamp] NULL,
	[ListTypeID] [int] NULL,
	[IsVisible] [bit] NULL
) ON [PRIMARY]
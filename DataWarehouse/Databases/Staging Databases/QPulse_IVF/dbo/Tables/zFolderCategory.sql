﻿CREATE TABLE [dbo].[zFolderCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedByPersonID] [int] NULL,
	[DateCreated] [datetime] NULL,
	[Name] [nvarchar](200) NULL,
	[IsVisible] [bit] NULL,
	[SortOrder] [int] NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY]
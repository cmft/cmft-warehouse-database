﻿CREATE TABLE [dbo].[EscalationPointCondition](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[EscalationPointID] [int] NOT NULL,
	[Condition] [ntext] NULL,
	[Priority] [smallint] NOT NULL CONSTRAINT [DF_EscalationConditionPriority]  DEFAULT (0),
	[Name] [nvarchar](200) NULL,
	[Version] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[xSupplierAccountManager](
	[PersonID] [int] NOT NULL,
	[SupplierID] [int] NOT NULL,
	[IsMain] [bit] NULL
) ON [PRIMARY]
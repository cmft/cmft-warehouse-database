﻿CREATE TABLE [Income].[OPProcedureClinic](
	[ClinicCode] [varchar](10) NOT NULL,
	[HRGCode] [varchar](10) NOT NULL,
	[ContextCode] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Income].[ZeroPriceHRG](
	[HRGCode] [char](5) NOT NULL,
	[PODCode] [varchar](10) NULL,
	[ContextCode] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Income].[OPProcedureTariffEligibility](
	[HRGCode] [char](5) NULL,
	[DateFrom] [date] NULL,
	[DateTo] [date] NULL
) ON [PRIMARY]
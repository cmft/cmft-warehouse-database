﻿CREATE TABLE [Income].[ExcludedPatient](
	[SourcePatientNo] [varchar](50) NOT NULL,
	[ExclusionType] [varchar](50) NULL,
	[EffectiveFromDate] [date] NULL,
	[EffectiveToDate] [date] NULL,
	[ContextCode] [varchar](50) NULL,
	[DateAdded] [smalldatetime] NULL
) ON [PRIMARY]
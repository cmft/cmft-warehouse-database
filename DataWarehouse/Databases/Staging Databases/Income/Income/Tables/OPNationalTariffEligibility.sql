﻿CREATE TABLE [Income].[OPNationalTariffEligibility](
	[NationalSpecialtyCode] [char](3) NULL,
	[DateFrom] [date] NULL,
	[DateTo] [date] NULL
) ON [PRIMARY]
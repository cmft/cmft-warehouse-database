﻿CREATE TABLE [Income].[BedDayLocation](
	[WardCode] [varchar](50) NOT NULL,
	[PODCode] [varchar](50) NULL,
	[HRGCode] [varchar](10) NULL,
	[SpecialtyCode] [varchar](5) NULL,
	[DirectorateCode] [varchar](5) NULL,
	[ContextCode] [varchar](10) NULL
) ON [PRIMARY]
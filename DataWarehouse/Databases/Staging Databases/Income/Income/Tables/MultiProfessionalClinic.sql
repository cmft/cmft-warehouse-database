﻿CREATE TABLE [Income].[MultiProfessionalClinic](
	[ConsultantCode] [varchar](50) NOT NULL,
	[TreatmentFunctionCode] [varchar](50) NOT NULL,
	[ClinicCode] [varchar](50) NOT NULL,
	[ContextCode] [varchar](50) NOT NULL
) ON [PRIMARY]
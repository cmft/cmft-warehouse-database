﻿CREATE TABLE [OP].[ExclusionRuleBase](
	[ExclusionRecno] [int] IDENTITY(1,1) NOT NULL,
	[ContextCode] [varchar](10) NULL,
	[PCTCode] [char](3) NULL,
	[PASSpecialtyCode] [varchar](10) NULL,
	[NationalSpecialtyCode] [char](3) NULL,
	[ClinicCode] [varchar](10) NULL,
	[FirstAttendanceFlag] [char](2) NULL,
	[PrimaryProcedureCode] [varchar](10) NULL,
	[DiagnosisFlagCode] [char](1) NULL,
	[CommissioningSerialNo] [varchar](20) NULL,
	[LocalAdminCategoryCode] [varchar](20) NULL,
	[PASConsultantCode] [varchar](50) NULL,
	[ExclusionCode] [varchar](20) NULL,
	[EffectiveFromDate] [date] NULL CONSTRAINT [DF__Exclusion__Effec__1B88F612]  DEFAULT ('01/01/1900'),
	[EffectiveToDate] [date] NULL,
	[DateAdded] [date] NULL CONSTRAINT [DF__Exclusion__DateA__1C7D1A4B]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [ETL].[TImportAPCBedday_RC](
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[SiteCode] [varchar](12) NULL,
	[LocationCode] [varchar](50) NULL,
	[PODCode] [varchar](50) NOT NULL,
	[HRGCode] [varchar](50) NOT NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[EncounterRecno] [varchar](50) NOT NULL,
	[CensusDate] [date] NOT NULL,
	[Bedday] [int] NOT NULL,
	[ContextCode] [varchar](50) NOT NULL
) ON [PRIMARY]
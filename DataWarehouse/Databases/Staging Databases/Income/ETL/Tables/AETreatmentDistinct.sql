﻿CREATE TABLE [ETL].[AETreatmentDistinct](
	[ContextCode] [varchar](10) NOT NULL,
	[AESourceUniqueID] [varchar](50) NOT NULL,
	[TreatmentCode] [varchar](6) NULL,
	[TreatmentDate] [smalldatetime] NULL,
	[SequenceNo] [bigint] NOT NULL
) ON [PRIMARY]
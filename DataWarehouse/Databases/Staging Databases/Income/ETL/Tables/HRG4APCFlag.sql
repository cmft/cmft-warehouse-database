﻿CREATE TABLE [ETL].[HRG4APCFlag](
	[RowNo] [int] NULL,
	[ProviderSpellNo] [varchar](50) NULL,
	[SequenceNo] [int] NULL,
	[SpellFlag] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [ETL].[AEDiagnosisDistinct](
	[ContextCode] [varchar](10) NOT NULL,
	[AESourceUniqueID] [varchar](50) NOT NULL,
	[DiagnosisCode] [varchar](71) NULL,
	[SequenceNo] [bigint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [ETL].[HRG4CCQuality](
	[RowNo] [int] NULL,
	[Iteration] [int] NULL,
	[CodeType] [varchar](50) NULL,
	[Code] [varchar](50) NULL,
	[ErrorMessage] [varchar](50) NULL
) ON [PRIMARY]
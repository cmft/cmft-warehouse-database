﻿CREATE TABLE [ETL].[AEInvestigationDistinct](
	[ContextCode] [varchar](10) NOT NULL,
	[AESourceUniqueID] [varchar](50) NOT NULL,
	[InvestigationCode] [varchar](6) NULL,
	[SequenceNo] [bigint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [ETL].[HRG4OPEncounter](
	[EncounterRecno] [int] NULL,
	[RowNo] [int] NULL,
	[HRGCode] [varchar](50) NULL,
	[GroupingMethodFlag] [varchar](50) NULL,
	[DominantOperationCode] [varchar](50) NULL
) ON [PRIMARY]
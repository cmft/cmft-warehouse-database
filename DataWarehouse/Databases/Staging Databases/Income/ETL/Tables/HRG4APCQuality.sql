﻿CREATE TABLE [ETL].[HRG4APCQuality](
	[RowNo] [int] NULL,
	[SequenceNo] [int] NULL,
	[QualityTypeCode] [varchar](50) NULL,
	[QualityCode] [varchar](50) NULL,
	[QualityMessage] [varchar](255) NULL
) ON [PRIMARY]
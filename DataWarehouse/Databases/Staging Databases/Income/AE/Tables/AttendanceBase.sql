﻿CREATE TABLE [AE].[AttendanceBase](
	[ExtractMonthNumber] [int] NULL,
	[FinancialYear] [varchar](10) NULL,
	[PositionCode] [int] NULL,
	[VersionNumber] [int] NULL,
	[SourceSiteCode] [varchar](100) NULL,
	[SourcePatientNo] [varchar](10) NULL,
	[ReportMasterID] [varchar](max) NULL,
	[SourceUniqueID] [varchar](50) NOT NULL,
	[SUSIdentifier] [varchar](35) NULL,
	[PatientForename] [varchar](100) NULL,
	[PatientSurname] [varchar](100) NULL,
	[DateOfBirth] [datetime] NULL,
	[NHSNumber] [varchar](20) NULL,
	[DistrictNo] [varchar](50) NULL,
	[AttendanceNumber] [varchar](max) NULL,
	[Age] [numeric](18, 0) NULL,
	[NationalSexCode] [varchar](20) NULL,
	[PCTCode] [nvarchar](3) NULL,
	[GPPracticeCode] [varchar](10) NULL,
	[GpCode] [varchar](10) NULL,
	[Postcode] [varchar](10) NULL,
	[DirectorateCode] [varchar](2) NOT NULL,
	[ConsultantCode] [varchar](100) NULL,
	[MonthNumber] [int] NULL,
	[HRGCode] [varchar](50) NULL,
	[InterfaceCode] [varchar](5) NULL,
	[NationalAttendanceCategoryCode] [varchar](20) NULL,
	[NationalAttendanceDisposalCode] [varchar](20) NULL,
	[ArrivalDate] [smalldatetime] NULL,
	[ArrivalTime] [smalldatetime] NULL,
	[Investigation1] [varchar](6) NULL,
	[Investigation2] [varchar](6) NULL,
	[Investigation3] [varchar](6) NULL,
	[Investigation4] [varchar](6) NULL,
	[Investigation5] [varchar](6) NULL,
	[Investigation6] [varchar](6) NULL,
	[Investigation7] [varchar](6) NULL,
	[Investigation8] [varchar](6) NULL,
	[Investigation9] [varchar](6) NULL,
	[Investigation10] [varchar](6) NULL,
	[Investigation11] [varchar](6) NULL,
	[Investigation12] [varchar](6) NULL,
	[Treatment1] [varchar](6) NULL,
	[Treatment2] [varchar](6) NULL,
	[Treatment3] [varchar](6) NULL,
	[Treatment4] [varchar](6) NULL,
	[Treatment5] [varchar](6) NULL,
	[Treatment6] [varchar](6) NULL,
	[Treatment7] [varchar](6) NULL,
	[Treatment8] [varchar](6) NULL,
	[Treatment9] [varchar](6) NULL,
	[Treatment10] [varchar](6) NULL,
	[Treatment11] [varchar](6) NULL,
	[Treatment12] [varchar](6) NULL,
	[ContextCode] [varchar](10) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ByWhom] [nvarchar](128) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [APC].[BaseHRGUplift](
	[ExtractMonthNumber] [int] NULL,
	[PositionCode] [int] NULL,
	[VersionNumber] [int] NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[HRGCode] [varchar](50) NULL,
	[ContextCode] [varchar](10) NOT NULL
) ON [PRIMARY]
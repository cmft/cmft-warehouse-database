﻿CREATE TABLE [APC].[HRGUplift](
	[ExtractMonthNumber] [int] NULL,
	[PositionCode] [int] NULL,
	[VersionNumber] [int] NULL,
	[EncounterRecno] [bigint] NOT NULL,
	[SourceUniqueID] [varchar](50) NULL,
	[CasenoteNumber] [varchar](20) NULL,
	[AdmissionTime] [smalldatetime] NULL,
	[PatientAge] [numeric](18, 0) NULL,
	[Division] [varchar](50) NULL,
	[NationalSpecialty] [varchar](223) NOT NULL,
	[Consultant] [varchar](200) NOT NULL,
	[HRGCode] [varchar](50) NULL,
	[PreviousDiagnosisCode] [varchar](10) NULL,
	[PreviousAdmissionTime] [smalldatetime] NULL,
	[OpportunityFlag] [varchar](20) NULL
) ON [PRIMARY]
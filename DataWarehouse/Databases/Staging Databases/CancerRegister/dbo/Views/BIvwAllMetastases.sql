﻿CREATE VIEW [dbo].[BIvwAllMetastases]
		AS
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwBrainMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwBreastMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwColorectalMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwGynaecologyMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwHeadandNeckMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwHaematologyMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwOtherMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwLungMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwSarcomaMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwPaediatricMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwUrologyMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwUpperGIMets
		UNION ALL
		SELECT     [CARE_ID], [Cancer Site], [Bone Metastases], [Brain Metastases], [Liver Metastases], [Lung Metastases], [Other Metastases], [Other Metastases Details], 
							  [Lymph Metastases], [Adrenal Metastases], [Skin Metastases], [Unknown Metastases], [Bone Marrow Metastases], [Bone Ex-Marrow Metastases]
		FROM         BIvwSkinMets
GO


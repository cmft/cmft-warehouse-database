﻿CREATE VIEW [dbo].[BIvwUrologyMets]
		AS
		SELECT     dbo.tblMAIN_REFERRALS.CARE_ID, dbo.tblMAIN_REFERRALS.L_CANCER_SITE AS [Cancer Site], ltblCOLO_METS2.MET_DESC AS [Bone Metastases], 
							  ltblCOLO_METS4.MET_DESC AS [Brain Metastases], ltblCOLO_METS3.MET_DESC AS [Liver Metastases], ltblCOLO_METS_1.MET_DESC AS [Lung Metastases], 
							  ltblCOLO_METS5.MET_DESC AS [Other Metastases], dbo.tblREFERRAL_UROLOGY.R_OTHER_METS AS [Other Metastases Details], CONVERT(varchar, NULL) 
							  AS [Lymph Metastases], CONVERT(varchar, NULL) AS [Adrenal Metastases], CONVERT(varchar, NULL) AS [Skin Metastases], 
							  dbo.ltblCOLO_METS.MET_DESC AS [Unknown Metastases], ltblCOLO_METS_2.MET_DESC AS [Bone Marrow Metastases], 
							  ltblCOLO_METS_3.MET_DESC AS [Bone Ex-Marrow Metastases]
		FROM         dbo.ltblCOLO_METS AS ltblCOLO_METS2 RIGHT OUTER JOIN
							  dbo.ltblCOLO_METS AS ltblCOLO_METS_1 RIGHT OUTER JOIN
							  dbo.ltblCOLO_METS AS ltblCOLO_METS_2 RIGHT OUTER JOIN
							  dbo.tblMAIN_REFERRALS INNER JOIN
							  dbo.tblREFERRAL_UROLOGY ON dbo.tblMAIN_REFERRALS.CARE_ID = dbo.tblREFERRAL_UROLOGY.CARE_ID LEFT OUTER JOIN
							  dbo.ltblCOLO_METS ON dbo.tblREFERRAL_UROLOGY.R_MET_UNKNOWN = dbo.ltblCOLO_METS.MET_CODE ON 
							  ltblCOLO_METS_2.MET_CODE = dbo.tblREFERRAL_UROLOGY.R_MET_BONE_MARROW LEFT OUTER JOIN
							  dbo.ltblCOLO_METS AS ltblCOLO_METS_3 ON dbo.tblREFERRAL_UROLOGY.R_MET_BONE_EX_MARROW = ltblCOLO_METS_3.MET_CODE ON 
							  ltblCOLO_METS_1.MET_CODE = dbo.tblREFERRAL_UROLOGY.R_MET_LUNG ON 
							  ltblCOLO_METS2.MET_CODE = dbo.tblREFERRAL_UROLOGY.R_MET_BONE LEFT OUTER JOIN
							  dbo.ltblCOLO_METS AS ltblCOLO_METS3 ON dbo.tblREFERRAL_UROLOGY.R_MET_LIVER = ltblCOLO_METS3.MET_CODE LEFT OUTER JOIN
							  dbo.ltblCOLO_METS AS ltblCOLO_METS4 ON dbo.tblREFERRAL_UROLOGY.R_MET_BRAIN = ltblCOLO_METS4.MET_CODE LEFT OUTER JOIN
							  dbo.ltblCOLO_METS AS ltblCOLO_METS5 ON dbo.tblREFERRAL_UROLOGY.R_MET_OTHER = ltblCOLO_METS5.MET_CODE
		WHERE     (dbo.tblMAIN_REFERRALS.L_CANCER_SITE = 'Urology')
GO

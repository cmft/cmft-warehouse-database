﻿CREATE TABLE [dbo].[ltblRecurrenceType] (
    [RecurrenceTypeID]   INT           NOT NULL,
    [RecurrenceTypeCode] INT           NOT NULL,
    [RecurrenceTypeDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblRecurrenceType] PRIMARY KEY CLUSTERED ([RecurrenceTypeID] ASC)
);


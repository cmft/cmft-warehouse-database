﻿CREATE TABLE [dbo].[tblLinkedReferrals] (
    [OriginalCareID] INT NOT NULL,
    [LinkedCareID]   INT NOT NULL,
    [PatientID]      INT NOT NULL,
    [LinkTypeID]     INT NOT NULL,
    CONSTRAINT [PK_tblLinkedReferrals] PRIMARY KEY CLUSTERED ([OriginalCareID] ASC, [LinkedCareID] ASC),
    CONSTRAINT [FK_tblLinkedReferrals_ltblReferralLinkType] FOREIGN KEY ([LinkTypeID]) REFERENCES [dbo].[ltblReferralLinkType] ([LinkTypeID]),
    CONSTRAINT [FK_tblLinkedReferrals_tblDEMOGRAPHICS] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[tblDEMOGRAPHICS] ([PATIENT_ID]),
    CONSTRAINT [FK_tblLinkedReferrals_tblMAIN_REFERRALS] FOREIGN KEY ([OriginalCareID]) REFERENCES [dbo].[tblMAIN_REFERRALS] ([CARE_ID]),
    CONSTRAINT [FK_tblLinkedReferrals_tblMAIN_REFERRALS1] FOREIGN KEY ([LinkedCareID]) REFERENCES [dbo].[tblMAIN_REFERRALS] ([CARE_ID])
);


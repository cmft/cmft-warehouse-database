﻿CREATE TABLE [dbo].[ltblBREAST_PCODES] (
    [CODES_DESC]  VARCHAR (2)  NOT NULL,
    [DESCRIPTION] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblBREAST_PCODES] PRIMARY KEY CLUSTERED ([CODES_DESC] ASC)
);


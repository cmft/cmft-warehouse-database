﻿CREATE TABLE [dbo].[tblREFERRAL_SARCOMA] (
    [REF_ID]                      INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                     INT           NOT NULL,
    [N_SA1_TUMOUR_SITE_1]         VARCHAR (50)  NULL,
    [N_SA1_TUMOUR_SITE_2]         VARCHAR (50)  NULL,
    [N_SA2_LOCATION_1]            CHAR (1)      NULL,
    [N_SA2_LOCATION_2]            CHAR (1)      NULL,
    [N_SA3_PART_SITE]             CHAR (2)      NULL,
    [N_SA4_DIAMETER]              REAL          NULL,
    [R_MET_LUNG]                  INT           NULL,
    [R_MET_BONE]                  INT           NULL,
    [R_MET_LIVER]                 INT           NULL,
    [R_MET_BRAIN]                 INT           NULL,
    [R_MET_OTHER]                 INT           NULL,
    [R_OTHER_METS]                VARCHAR (255) NULL,
    [L_PRE_T_LETTER]              CHAR (1)      NULL,
    [L_PRE_M_LETTER]              CHAR (1)      NULL,
    [L_PATH_T_LETTER]             CHAR (1)      NULL,
    [L_PATH_M_LETTER]             CHAR (1)      NULL,
    [L_PRE_N_LETTER]              CHAR (1)      NULL,
    [L_PATH_N_LETTER]             CHAR (1)      NULL,
    [R_MET_LYMPH]                 INT           NULL,
    [R_MET_ADRENAL]               INT           NULL,
    [R_MET_SKIN]                  INT           NULL,
    [R_MET_UNKNOWN]               INT           NULL,
    [R_MET_BONE_MARROW]           INT           NULL,
    [R_MET_BONE_EX_MARROW]        INT           NULL,
    [IRS_CODE]                    INT           NULL,
    [RHAB_CYTOGENETICS_ID]        INT           NULL,
    [RHAB_SITE_PROGNOSIS_CODE_ID] INT           NULL,
    [PRIMARY_TUMOUR_SIZE]         REAL          NULL,
    [NECROSIS]                    INT           NULL,
    [ADEQUACY_ID]                 INT           NULL,
    [EWINGS_VOLUME_ID]            INT           NULL,
    [EWINGS_CYTOGENETIC_ID]       INT           NULL,
    [SUBSITE_SOFT_TISSUE]         INT           NULL,
    [MULTIFOCAL_INDICATOR]        VARCHAR (2)   NULL,
    CONSTRAINT [PK_tblREFERRAL_SARCOMA] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_SARCOMA]
    ON [dbo].[tblREFERRAL_SARCOMA]([CARE_ID] ASC);


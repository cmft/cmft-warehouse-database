﻿CREATE TABLE [dbo].[ltblRecurrenceTreatment] (
    [RecurrenceTreatID]   INT           NOT NULL,
    [RecurrenceTreatCode] INT           NOT NULL,
    [RecurrenceTreatDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblRecurrenceTreatment] PRIMARY KEY CLUSTERED ([RecurrenceTreatID] ASC)
);


﻿CREATE TABLE [dbo].[ltblFIGO_LETTER_2] (
    [FIGO] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblFIGO_LETTER_2] PRIMARY KEY CLUSTERED ([FIGO] ASC)
);


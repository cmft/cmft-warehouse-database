﻿CREATE TABLE [dbo].[ltblSTENT_TYPE] (
    [TYPE_CODE] INT          NOT NULL,
    [TYPE_DESC] VARCHAR (50) NOT NULL,
    [NEW_NOGCA] BIT          CONSTRAINT [DF_ltblSTENT_TYPE_NEW_NOGCA] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ltblSTENT_TYPE] PRIMARY KEY CLUSTERED ([TYPE_CODE] ASC)
);


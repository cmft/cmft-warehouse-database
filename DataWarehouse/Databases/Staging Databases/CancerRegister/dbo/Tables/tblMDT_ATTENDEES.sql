﻿CREATE TABLE [dbo].[tblMDT_ATTENDEES] (
    [MDT_ID]        INT           IDENTITY (1, 1) NOT NULL,
    [MEETING_ID]    INT           NULL,
    [NAME]          VARCHAR (50)  NULL,
    [TYPE]          INT           NULL,
    [LEAD]          INT           NULL,
    [DISPLAY_ORDER] INT           NULL,
    [DELETED]       INT           NULL,
    [DATEDELETED]   SMALLDATETIME NULL,
    [DATEADDED]     SMALLDATETIME NULL,
    [SPECIALTY_ID]  INT           NULL,
    CONSTRAINT [PK_tblMDT_ATTENDEES] PRIMARY KEY CLUSTERED ([MDT_ID] ASC)
);


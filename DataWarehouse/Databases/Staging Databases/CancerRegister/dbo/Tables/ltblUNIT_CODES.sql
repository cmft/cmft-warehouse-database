﻿CREATE TABLE [dbo].[ltblUNIT_CODES](
	[UnitCodeId] [int] IDENTITY(1,1) NOT NULL,
	[Trust] [varchar](50) NULL,
	[SQLRI] [varchar](50) NULL,
	[unit] [varchar](50) NULL,
	[NBOCAP_Unit] [varchar](50) NULL,
	[DAHNO_Unit] [varchar](50) NULL,
	[Centre_No] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[mdtLocation] [varchar](50) NULL
) ON [PRIMARY]
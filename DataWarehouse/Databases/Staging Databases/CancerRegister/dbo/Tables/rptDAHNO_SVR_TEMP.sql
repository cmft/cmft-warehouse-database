﻿CREATE TABLE [dbo].[rptDAHNO_SVR_TEMP] (
    [DAHNO_ID]              INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]               INT          NULL,
    [PATIENT_ID]            INT          NULL,
    [NHSNumber]             VARCHAR (10) NULL,
    [SubmittingOrg]         VARCHAR (5)  NULL,
    [ContactOrg]            VARCHAR (5)  NULL,
    [PatientID]             VARCHAR (10) NULL,
    [DateDiagnosis]         DATETIME     NULL,
    [DateCarePlanAgreed]    DATETIME     NULL,
    [DateInitialContact]    DATETIME     NULL,
    [DateContact]           DATETIME     NULL,
    [Diet]                  VARCHAR (4)  NULL,
    [Prefessional]          VARCHAR (1)  NULL,
    [ContactPurpose]        VARCHAR (2)  NULL,
    [PtStatus]              INT          NULL,
    [CommunicationProposed] VARCHAR (4)  NULL,
    [CommunicationPrimary]  VARCHAR (2)  NULL,
    [CommunicationOther]    VARCHAR (2)  NULL,
    [Voicing]               INT          NULL,
    [ValveRemoval]          INT          NULL,
    [Username]              VARCHAR (50) NULL,
    CONSTRAINT [PK_rptDAHNO_SVR_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptDAHNO_SVR_TEMP]
    ON [dbo].[rptDAHNO_SVR_TEMP]([Username] ASC);


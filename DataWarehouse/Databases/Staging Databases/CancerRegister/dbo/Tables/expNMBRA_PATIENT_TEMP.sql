﻿CREATE TABLE [dbo].[expNMBRA_PATIENT_TEMP] (
    [NMBRA_ID]           INT          IDENTITY (1, 1) NOT NULL,
    [PATIENT_ID]         INT          NULL,
    [NSTS_Status]        INT          NULL,
    [HospitalType]       INT          NOT NULL,
    [SubmittingHospital] VARCHAR (5)  NOT NULL,
    [NHSNumber]          VARCHAR (10) NULL,
    [HospitalNumber]     VARCHAR (10) NULL,
    [Forename]           VARCHAR (50) NULL,
    [Surname]            VARCHAR (60) NULL,
    [Postcode]           VARCHAR (8)  NULL,
    [DateBirth]          DATETIME     NULL,
    [Ethnicity]          VARCHAR (1)  NULL,
    [ConsentStatus]      VARCHAR (2)  NULL,
    [ReasonNotConsent]   VARCHAR (2)  NULL,
    [SiteCode]           VARCHAR (5)  NULL,
    [TypePatient]        INT          NULL,
    [Username]           VARCHAR (50) NULL,
    CONSTRAINT [PK_expNMBRA_PATIENT_TEMP] PRIMARY KEY CLUSTERED ([NMBRA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expNMBRA_PATIENT_TEMP]
    ON [dbo].[expNMBRA_PATIENT_TEMP]([Username] ASC);


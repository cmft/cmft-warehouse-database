﻿CREATE TABLE [dbo].[ltblDENTIST_INFO] (
    [DentistID]   INT          IDENTITY (1, 1) NOT NULL,
    [DentistCode] CHAR (8)     NOT NULL,
    [DentistName] VARCHAR (50) NOT NULL,
    [Address1]    VARCHAR (50) NULL,
    [Address2]    VARCHAR (50) NULL,
    [Address3]    VARCHAR (50) NULL,
    [Address4]    VARCHAR (50) NULL,
    [Address5]    VARCHAR (50) NULL,
    [PostCode]    VARCHAR (8)  NULL,
    [IsDeleted]   BIT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ltblDENTIST_INFO_1] PRIMARY KEY CLUSTERED ([DentistID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ltblDENTIST_INFO]
    ON [dbo].[ltblDENTIST_INFO]([DentistName] ASC) WITH (FILLFACTOR = 90);


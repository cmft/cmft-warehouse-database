﻿CREATE TABLE [dbo].[expSWCIS_TRIAL_TEMP] (
    [SWCIS_ID]          INT           IDENTITY (1, 1) NOT NULL,
    [NHSNumber]         VARCHAR (10)  NULL,
    [OrgCodeSubmitting] VARCHAR (5)   NULL,
    [CareSpellID]       VARCHAR (50)  NULL,
    [TrialStatus]       VARCHAR (5)   NULL,
    [StartDate]         DATETIME      NULL,
    [TrialName]         VARCHAR (255) NULL,
    [TrialTypeCode]     INT           NULL,
    [TrialType]         VARCHAR (50)  NULL,
    [Username]          VARCHAR (50)  NULL,
    CONSTRAINT [PK_expSWCIS_TRIAL_TEMP] PRIMARY KEY CLUSTERED ([SWCIS_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expSWCIS_TRIAL_TEMP]
    ON [dbo].[expSWCIS_TRIAL_TEMP]([Username] ASC);


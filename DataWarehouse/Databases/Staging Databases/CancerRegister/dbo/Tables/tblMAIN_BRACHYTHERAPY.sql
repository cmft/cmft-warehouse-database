﻿CREATE TABLE [dbo].[tblMAIN_BRACHYTHERAPY] (
    [BRACHY_ID]                INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                  INT           NOT NULL,
    [TEMP_ID]                  VARCHAR (255) NULL,
    [N11_1_SITE_CODE]          VARCHAR (5)   NULL,
    [N_SITE_CODE_DTT]          VARCHAR (5)   NULL,
    [N11_2_CONSULTANT]         VARCHAR (8)   NULL,
    [N11_3_DECISION_DATE]      SMALLDATETIME NULL,
    [N11_6_TREATMENT_INTENT]   VARCHAR (1)   NULL,
    [N11_7_TYPE]               VARCHAR (2)   NULL,
    [N11_8_TREATMENT_SITE]     VARCHAR (5)   NULL,
    [N11_9_START_DATE]         SMALLDATETIME NULL,
    [N11_10_END_DATE]          SMALLDATETIME NULL,
    [N11_11_DOSE]              REAL          NULL,
    [N11_12_DURATION]          INT           NULL,
    [N11_13_FRACTIONS]         REAL          NULL,
    [N11_14_ACTUAL_DOSE]       REAL          NULL,
    [N11_15_DOSE_RATE]         VARCHAR (1)   NULL,
    [N11_16_ACTUAL_DURATION]   INT           NULL,
    [L_TYPE_OF_DOSE]           INT           NULL,
    [N11_17_ISOTOPE_TYPE]      VARCHAR (1)   NULL,
    [N11_18_ANAESTHETIC]       VARCHAR (5)   NULL,
    [N11_19_UNSEALED]          VARCHAR (1)   NULL,
    [N11_20_DELIVERY_TYPE]     VARCHAR (1)   NULL,
    [N11_22_STATUS]            INT           NULL,
    [N_TREATMENT_EVENT]        VARCHAR (2)   NULL,
    [N_TREATMENT_SETTING]      VARCHAR (2)   NULL,
    [L_NAMED_COMP]             VARCHAR (50)  NULL,
    [L_COMP_3MTH]              VARCHAR (50)  NULL,
    [L_SHORT_LONG]             VARCHAR (5)   NULL,
    [R_INSERTIONS]             INT           NULL,
    [R_DOSE_INSERTION]         REAL          NULL,
    [R_TREAT_TO]               VARCHAR (2)   NULL,
    [L_TRIAL]                  INT           NULL,
    [ENDOSCOPIC]               BIT           NULL,
    [DEFINITIVE_TREATMENT]     INT           NULL,
    [CWT_PROFORMA]             INT           NULL,
    [L_COMMENTS]               TEXT          NULL,
    [PRE_TREAT_PSA]            REAL          NULL,
    [ROOT_START_DATE_COMMENTS] TEXT          NULL,
    [ROOT_END_DATE_COMMENTS]   TEXT          NULL,
    CONSTRAINT [PK_tblMAIN_BRACHYTHERAPY] PRIMARY KEY CLUSTERED ([BRACHY_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblMAIN_BRACHYTHERAPY]
    ON [dbo].[tblMAIN_BRACHYTHERAPY]([CARE_ID] ASC);


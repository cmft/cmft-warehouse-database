﻿CREATE TABLE [dbo].[tmpBAUS_IDs] (
    [CARE_ID]             INT          NOT NULL,
    [Username]            VARCHAR (50) NOT NULL,
    [DateAdded]           DATETIME     NULL,
    [DateAmended]         DATETIME     NULL,
    [DateReferralCreated] DATETIME     NULL,
    CONSTRAINT [PK_tmpBAUS_IDs] PRIMARY KEY CLUSTERED ([CARE_ID] ASC, [Username] ASC)
);


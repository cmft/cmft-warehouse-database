﻿CREATE TABLE [dbo].[ltblPREDISPOSING] (
    [CON_CODE] CHAR (2)     NOT NULL,
    [CON_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblPREDISPOSING] PRIMARY KEY CLUSTERED ([CON_CODE] ASC)
);


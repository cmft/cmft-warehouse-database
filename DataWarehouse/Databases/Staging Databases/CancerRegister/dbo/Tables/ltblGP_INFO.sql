﻿CREATE TABLE [dbo].[ltblGP_INFO] (
    [GP_ID]      INT          IDENTITY (1, 1) NOT NULL,
    [GP]         VARCHAR (50) NOT NULL,
    [GPNAME]     VARCHAR (50) NOT NULL,
    [ADDR1]      VARCHAR (50) NULL,
    [ADDR2]      VARCHAR (50) NULL,
    [ADDR3]      VARCHAR (50) NULL,
    [ADDR4]      VARCHAR (50) NULL,
    [POSTCODE]   VARCHAR (50) NULL,
    [PRACCODE]   VARCHAR (50) NOT NULL,
    [IS_DELETED] BIT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ltblGP_INFO] PRIMARY KEY CLUSTERED ([GP_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ltblGP_INFO_2]
    ON [dbo].[ltblGP_INFO]([GPNAME] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ltblGP_INFO_1]
    ON [dbo].[ltblGP_INFO]([PRACCODE] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ltblGP_INFO]
    ON [dbo].[ltblGP_INFO]([GP] ASC) WITH (FILLFACTOR = 90);


﻿CREATE TABLE [dbo].[tblASSESSMENT_UPPER_GI] (
    [UGI_AS_ID]            INT IDENTITY (1, 1) NOT NULL,
    [ASSESSMENT_ID]        INT NULL,
    [L_PLAN_PROCEDURES]    INT NULL,
    [L_PLAN_STENT]         INT NULL,
    [L_PLAN_LASER]         INT NULL,
    [L_PLAN_ARGON]         INT NULL,
    [L_PLAN_PHOTO]         INT NULL,
    [L_PLAN_GASTROSTOMY]   INT NULL,
    [L_PLAN_BRACHY]        INT NULL,
    [L_PLAN_DILATION]      INT NULL,
    [L_PLAN_OTHER]         INT NULL,
    [L_ASPIRATION]         INT NULL,
    [L_PERFORATION]        INT NULL,
    [L_HAEMORRHAGE]        INT NULL,
    [L_MIGATION]           INT NULL,
    [L_BOLUS]              INT NULL,
    [L_OVERGROWTH]         INT NULL,
    [L_DEATH]              INT NULL,
    [L_OTHER_COMP]         INT NULL,
    [L_UNPLAN_PROCEDURES]  INT NULL,
    [L_UNPLAN_STENT]       INT NULL,
    [L_UNPLAN_LASER]       INT NULL,
    [L_UNPLAN_ARGON]       INT NULL,
    [L_UNPLAN_PHOTO]       INT NULL,
    [L_UNPLAN_GASTROSTOMY] INT NULL,
    [L_UNPLAN_BRACHY]      INT NULL,
    [L_UNPLAN_DILATION]    INT NULL,
    [L_UNPLAN_OTHER]       INT NULL,
    CONSTRAINT [PK_tblASSESSMENT_UPPER_GI] PRIMARY KEY CLUSTERED ([UGI_AS_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblASSESSMENT_UPPER_GI]
    ON [dbo].[tblASSESSMENT_UPPER_GI]([ASSESSMENT_ID] ASC);


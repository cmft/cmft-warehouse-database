﻿CREATE TABLE [dbo].[tmpNOGCA_REFERRAL] (
    [NOGCAID]       INT           IDENTITY (1, 1) NOT NULL,
    [CareID]        INT           NULL,
    [PatientID]     INT           NULL,
    [NHSNumber]     VARCHAR (10)  NULL,
    [DateOfBirth]   SMALLDATETIME NULL,
    [SubmittingOrg] VARCHAR (5)   NULL,
    [Username]      VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpNOGCA_REFERRAL] PRIMARY KEY CLUSTERED ([NOGCAID] ASC)
);


﻿CREATE TABLE [dbo].[tblPATHOLOGY_PAEDIATRIC] (
    [PAED_PATH_ID]               INT IDENTITY (1, 1) NOT NULL,
    [PATHOLOGY_ID]               INT NOT NULL,
    [ANN_ARBOR_STAGE_ID]         INT NULL,
    [ANN_ARBOR_SYMPTOMS_ID]      INT NULL,
    [ANN_ARBOR_EXTRANODALITY_ID] INT NULL,
    [MURPHY_ID]                  INT NULL,
    [INSS_ID]                    INT NULL,
    [WILMS_RENAL_ID]             INT NULL,
    [CHANG_ID]                   INT NULL,
    [ALK_1_ID]                   INT NULL,
    CONSTRAINT [PK_tblPATHOLOGY_PAEDIATRIC] PRIMARY KEY CLUSTERED ([PAED_PATH_ID] ASC)
);


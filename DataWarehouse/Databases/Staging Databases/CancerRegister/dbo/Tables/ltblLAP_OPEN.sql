﻿CREATE TABLE [dbo].[ltblLAP_OPEN] (
    [LAP_CODE] INT          NOT NULL,
    [LAP_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblLAP_OPEN] PRIMARY KEY CLUSTERED ([LAP_CODE] ASC)
);


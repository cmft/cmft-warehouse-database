﻿CREATE TABLE [dbo].[MirthEventTypes] (
    [msgID]          INT           IDENTITY (1, 1) NOT NULL,
    [msgType]        VARCHAR (3)   NOT NULL,
    [msgDescription] VARCHAR (100) NULL,
    CONSTRAINT [PK_MirthEventTypes] PRIMARY KEY CLUSTERED ([msgID] ASC)
);


﻿CREATE TABLE [dbo].[tmpNOGCA_ONCOLOGY] (
    [NOGCAOncologyID]        INT           IDENTITY (1, 1) NOT NULL,
    [CareID]                 INT           NULL,
    [NHSNumber]              VARCHAR (10)  NULL,
    [DateOfBirth]            SMALLDATETIME NULL,
    [HospitalNumber]         VARCHAR (50)  NULL,
    [OrganisationCode]       VARCHAR (5)   NULL,
    [OncolTreatmentIntent]   VARCHAR (1)   NULL,
    [OncolTreatmentModality] VARCHAR (2)   NULL,
    [ChemoStartDate]         SMALLDATETIME NULL,
    [ChemoOutcome]           INT           NULL,
    [RadioStartDate]         SMALLDATETIME NULL,
    [RadioOutcome]           INT           NULL,
    [ProceedCurativeSurgery] VARCHAR (2)   NULL,
    [Username]               VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpNOGCA_ONCOLOGY] PRIMARY KEY CLUSTERED ([NOGCAOncologyID] ASC)
);


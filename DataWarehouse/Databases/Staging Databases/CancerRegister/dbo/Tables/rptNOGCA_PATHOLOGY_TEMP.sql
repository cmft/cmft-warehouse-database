﻿CREATE TABLE [dbo].[rptNOGCA_PATHOLOGY_TEMP] (
    [NOGCA_ID]             INT          IDENTITY (1, 1) NOT NULL,
    [PATHOLOGY_ID]         INT          NULL,
    [CARE_ID]              INT          NULL,
    [PATIENT_ID]           INT          NULL,
    [NHSNumber]            VARCHAR (10) NOT NULL,
    [SubmittingOrg]        VARCHAR (10) NOT NULL,
    [ReportingOrg]         VARCHAR (10) NULL,
    [HospitalNumber]       VARCHAR (10) NULL,
    [DateDiagnosis]        DATETIME     NULL,
    [PathologySite]        VARCHAR (2)  NULL,
    [PathHistology]        VARCHAR (10) NULL,
    [ProxMargins]          VARCHAR (2)  NULL,
    [DistalMargins]        VARCHAR (2)  NULL,
    [CircMargins]          VARCHAR (2)  NULL,
    [NodesExamined]        INT          NULL,
    [NodesPositive]        INT          NULL,
    [PostTreatmentT]       VARCHAR (2)  NULL,
    [PostTreatmentN]       VARCHAR (2)  NULL,
    [PostTreatmentM]       VARCHAR (3)  NULL,
    [HistologyNeoadjuvant] VARCHAR (1)  NULL,
    [Username]             VARCHAR (50) NULL,
    CONSTRAINT [PK_rptNOGCA_PATHOLOGY_TEMP] PRIMARY KEY CLUSTERED ([NOGCA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptNOGCA_PATHOLOGY_TEMP]
    ON [dbo].[rptNOGCA_PATHOLOGY_TEMP]([Username] ASC);


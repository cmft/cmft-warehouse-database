﻿CREATE TABLE [dbo].[expDAHNO_DIAGNOSIS_TEMP] (
    [DAHNO_ID]            INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]             INT          NULL,
    [PATIENT_ID]          INT          NULL,
    [NHSNumber]           VARCHAR (10) NULL,
    [SubmittingOrg]       VARCHAR (5)  NULL,
    [ContactOrg]          VARCHAR (5)  NULL,
    [PatientID]           VARCHAR (10) NULL,
    [DateDiagnosis]       DATETIME     NULL,
    [PrimaryDiagnosis]    VARCHAR (10) NULL,
    [TumourLaterality]    VARCHAR (1)  NULL,
    [BasisDiagnosis]      INT          NULL,
    [Histology]           VARCHAR (10) NULL,
    [FinalTStage]         VARCHAR (5)  NULL,
    [FinalTCat]           VARCHAR (2)  NULL,
    [FinalNStage]         VARCHAR (5)  NULL,
    [FinalNCat]           VARCHAR (2)  NULL,
    [FinalMStage]         VARCHAR (5)  NULL,
    [FinalMCat]           VARCHAR (2)  NULL,
    [FinalStage]          VARCHAR (10) NULL,
    [FinalCat]            VARCHAR (2)  NULL,
    [DatePathologyReport] DATETIME     NULL,
    [Username]            VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_DIAGNOSIS_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_DIAGNOSIS_TEMP]
    ON [dbo].[expDAHNO_DIAGNOSIS_TEMP]([Username] ASC);


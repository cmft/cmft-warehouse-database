﻿CREATE TABLE [dbo].[tblCHEMOTHERAPY_UROLOGY] (
    [URO_CHEMO_ID]     INT         IDENTITY (1, 1) NOT NULL,
    [CHEMO_ID]         INT         NOT NULL,
    [L_SPEARM_STORAGE] VARCHAR (3) NULL,
    [L_AUDIOGRAM]      VARCHAR (3) NULL,
    [L_PFT]            VARCHAR (3) NULL,
    [PRE_TREAT_PSA]    REAL        NULL,
    CONSTRAINT [PK_tblCHEMOTHERAPY_UROLOGY] PRIMARY KEY CLUSTERED ([URO_CHEMO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblCHEMOTHERAPY_UROLOGY]
    ON [dbo].[tblCHEMOTHERAPY_UROLOGY]([CHEMO_ID] ASC);


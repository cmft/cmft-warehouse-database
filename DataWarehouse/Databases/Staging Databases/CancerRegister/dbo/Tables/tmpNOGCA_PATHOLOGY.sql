﻿CREATE TABLE [dbo].[tmpNOGCA_PATHOLOGY] (
    [NOGCAPathologyID]              INT           IDENTITY (1, 1) NOT NULL,
    [CareID]                        INT           NOT NULL,
    [NHSNumber]                     VARCHAR (50)  NOT NULL,
    [DateOfBirth]                   SMALLDATETIME NOT NULL,
    [HospitalNumber]                VARCHAR (10)  NULL,
    [TumourLength]                  VARCHAR (3)   NULL,
    [PathologySite]                 VARCHAR (2)   NULL,
    [PathHistology]                 VARCHAR (7)   NULL,
    [HistNeoadjuvant]               VARCHAR (1)   NULL,
    [ProximalMarginInvolved]        VARCHAR (1)   NULL,
    [DistalMarginInvolved]          VARCHAR (1)   NULL,
    [CircumferentialMarginInvolved] VARCHAR (2)   NULL,
    [NodesExaminedNumber]           INT           NULL,
    [NodesPositiveNumber]           INT           NULL,
    [Path_TNMfilter]                INT           NULL,
    [PostTreatment_T]               VARCHAR (3)   NULL,
    [PostTreatment_N]               VARCHAR (3)   NULL,
    [PostTreatment_M]               VARCHAR (3)   NULL,
    [Username]                      VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpNOGCA_PATHOLOGY_1] PRIMARY KEY CLUSTERED ([NOGCAPathologyID] ASC)
);


﻿CREATE TABLE [dbo].[ltblHYDRONEPHROSIS] (
    [HydroCode] CHAR (1)     NOT NULL,
    [HydroDesc] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblHYDRONEPHROSIS] PRIMARY KEY CLUSTERED ([HydroCode] ASC)
);


﻿CREATE TABLE [dbo].[tblREFERRAL_UPPER_GI] (
    [REF_ID]               INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]              INT           NOT NULL,
    [N_UG10_SYMPTOMS]      VARCHAR (50)  NULL,
    [N_UG11_SYMPTOMS]      VARCHAR (50)  NULL,
    [L_DIAG_METHOD]        VARCHAR (50)  NULL,
    [R_REF_DOCTOR]         VARCHAR (50)  NULL,
    [R_MET_LUNG]           INT           NULL,
    [R_MET_BONE]           INT           NULL,
    [R_MET_LIVER]          INT           NULL,
    [R_MET_BRAIN]          INT           NULL,
    [R_MET_OTHER]          INT           NULL,
    [R_OTHER_METS]         VARCHAR (255) NULL,
    [L_PRE_T_LETTER]       CHAR (1)      NULL,
    [L_PRE_N_LETTER]       CHAR (1)      NULL,
    [L_PRE_M_LETTER]       CHAR (1)      NULL,
    [L_PATH_T_LETTER]      CHAR (1)      NULL,
    [L_PATH_N_LETTER]      CHAR (1)      NULL,
    [L_PATH_M_LETTER]      CHAR (1)      NULL,
    [L_SIEWERT]            INT           NULL,
    [R_MET_LYMPH]          INT           NULL,
    [R_MET_ADRENAL]        INT           NULL,
    [R_MET_SKIN]           INT           NULL,
    [R_MET_UNKNOWN]        INT           NULL,
    [R_MET_BONE_MARROW]    INT           NULL,
    [R_MET_BONE_EX_MARROW] INT           NULL,
    [L_TREATMENT_SITE]     VARCHAR (2)   NULL,
    [HGD_OVERRIDE]         VARCHAR (1)   NULL,
    [PRETEXT]              INT           NULL,
    [PRETEXT_EXT]          INT           NULL,
    [NUM_LIVER_METS]       VARCHAR (1)   NULL,
    CONSTRAINT [PK_tblREFERRAL_UPPER_GI] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_UPPER_GI]
    ON [dbo].[tblREFERRAL_UPPER_GI]([CARE_ID] ASC);


﻿CREATE TABLE [dbo].[newNBOCAP_SURGERY_PATHOLOGY](
	[CARE_ID] [int] NULL,
	[EVENT_ID] [int] NULL,
	[EVENT_DATE] [smalldatetime] NULL,
	[EVENT_TYPE] [varchar](10) NULL,
	[Username] [varchar](50) NULL,
	[OrgCode] [varchar](5) NULL
) ON [PRIMARY]
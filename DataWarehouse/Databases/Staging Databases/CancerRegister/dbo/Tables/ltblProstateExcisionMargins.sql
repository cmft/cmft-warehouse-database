﻿CREATE TABLE [dbo].[ltblProstateExcisionMargins] (
    [ProsExcisionMargID]   INT           NOT NULL,
    [ProsExcisionMargCode] INT           NOT NULL,
    [ProsExcisionMargDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateExcisionMargins] PRIMARY KEY CLUSTERED ([ProsExcisionMargID] ASC)
);


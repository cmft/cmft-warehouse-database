﻿CREATE TABLE [dbo].[tmpRPT_DAHNO_PATIENT] (
    [DAHNOPatientID] INT           IDENTITY (1, 1) NOT NULL,
    [NHSNumber]      VARCHAR (10)  NULL,
    [HospitalNumber] VARCHAR (10)  NULL,
    [DateOfBirth]    SMALLDATETIME NULL,
    [FirstName]      VARCHAR (50)  NULL,
    [LastName]       VARCHAR (50)  NULL,
    [PostCode]       VARCHAR (8)   NULL,
    [Sex]            INT           NULL,
    [DateOfDeath]    SMALLDATETIME NULL,
    [Username]       VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_DAHNO_Patient] PRIMARY KEY CLUSTERED ([DAHNOPatientID] ASC)
);


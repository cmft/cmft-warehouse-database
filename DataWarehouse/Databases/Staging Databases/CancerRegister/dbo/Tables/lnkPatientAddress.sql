﻿CREATE TABLE [dbo].[lnkPatientAddress] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [PatientID]    INT           NOT NULL,
    [AddressID]    INT           NOT NULL,
    [TypeID]       INT           NOT NULL,
    [DateOfChange] SMALLDATETIME NOT NULL,
    CONSTRAINT [PK_lnkPatientAddress] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_lnkPatientAddress_tblAddress] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[tblAddress] ([ID]),
    CONSTRAINT [FK_lnkPatientAddress_tblDEMOGRAPHICS] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[tblDEMOGRAPHICS] ([PATIENT_ID])
);


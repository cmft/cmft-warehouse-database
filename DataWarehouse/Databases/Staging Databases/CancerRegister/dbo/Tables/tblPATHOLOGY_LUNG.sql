﻿CREATE TABLE [dbo].[tblPATHOLOGY_LUNG] (
    [LUNG_PATH_ID]           INT          IDENTITY (1, 1) NOT NULL,
    [PATHOLOGY_ID]           INT          NOT NULL,
    [N_L15_R_CLASSIFICATION] VARCHAR (2)  NULL,
    [R_RESECTION_MAR]        VARCHAR (3)  NULL,
    [R_SPECIMEN_TYPE]        INT          NULL,
    [R_BRONCHUS_T3]          INT          NULL,
    [R_BRONCHUS_T2]          INT          NULL,
    [R_NON_ASSESS]           INT          NULL,
    [R_RUL]                  INT          NULL,
    [R_RML]                  INT          NULL,
    [R_RLL]                  INT          NULL,
    [R_LUL]                  INT          NULL,
    [R_LLL]                  INT          NULL,
    [R_DISTANCE]             REAL         NULL,
    [R_EXTENT]               INT          NULL,
    [R_VISCERAL]             INT          NULL,
    [R_PARIETAL]             INT          NULL,
    [R_PLEURA]               INT          NULL,
    [R_PERICARDIUM]          INT          NULL,
    [R_DIAPHRAM]             INT          NULL,
    [R_VESSEL]               INT          NULL,
    [R_ATRIUM]               INT          NULL,
    [R_EFFUSION]             INT          NULL,
    [R_SEPARATE]             INT          NULL,
    [R_N1]                   VARCHAR (10) NULL,
    [R_N2]                   VARCHAR (10) NULL,
    [R_N3]                   VARCHAR (10) NULL,
    [R_BRONCHIAL]            VARCHAR (10) NULL,
    [R_MEDIASTINAL]          VARCHAR (10) NULL,
    [R_VASCULAR]             VARCHAR (10) NULL,
    [R_CHEST_WALL]           VARCHAR (10) NULL,
    [R_OTHER_PATH]           INT          NULL,
    [EPIDERMAL_ID]           INT          NULL,
    CONSTRAINT [PK_tblPATHOLOGY_LUNG] PRIMARY KEY CLUSTERED ([LUNG_PATH_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblPATHOLOGY_LUNG]
    ON [dbo].[tblPATHOLOGY_LUNG]([PATHOLOGY_ID] ASC);


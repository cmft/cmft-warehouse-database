﻿CREATE TABLE [dbo].[expDAHNO_NUTRITIONAL_TEMP] (
    [DAHNO_ID]             INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]              INT          NULL,
    [PATIENT_ID]           INT          NULL,
    [NHSNumber]            VARCHAR (10) NULL,
    [SubmittingOrg]        VARCHAR (5)  NULL,
    [ContactOrg]           VARCHAR (5)  NULL,
    [PatientID]            VARCHAR (10) NULL,
    [DateDiagnosis]        DATETIME     NULL,
    [EstimatedWeight]      REAL         NULL,
    [Weight]               REAL         NULL,
    [DateWeight]           DATETIME     NULL,
    [Height]               REAL         NULL,
    [DateHeight]           DATETIME     NULL,
    [DateInitialContact]   DATETIME     NULL,
    [DateContact]          DATETIME     NULL,
    [DateSupport]          DATETIME     NULL,
    [SupportType]          VARCHAR (1)  NULL,
    [DateRemains]          DATETIME     NULL,
    [DateWithdrawn]        DATETIME     NULL,
    [DateProcedure]        DATETIME     NULL,
    [NutritionalProcedure] VARCHAR (5)  NULL,
    [Username]             VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_NUTRITIONAL_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_NUTRITIONAL_TEMP]
    ON [dbo].[expDAHNO_NUTRITIONAL_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[tblREFERRAL_BREAST] (
    [REF_ID]                INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]               INT           NOT NULL,
    [N_B1_DIAGNOSTIC_ROUTE] INT           NULL,
    [N_B2_NURSE_SEEN]       CHAR (1)      NULL,
    [R_CONSENT_STATUS]      VARCHAR (2)   NULL,
    [R_INCAPABLE]           VARCHAR (2)   NULL,
    [R_MET_LUNG]            INT           NULL,
    [R_MET_BONE]            INT           NULL,
    [R_MET_LIVER]           INT           NULL,
    [R_MET_BRAIN]           INT           NULL,
    [R_MET_OTHER]           INT           NULL,
    [R_OTHER_METS]          VARCHAR (255) NULL,
    [L_3M_REVIEW]           VARCHAR (3)   NULL,
    [L_QUESTIONNAIRE]       VARCHAR (3)   NULL,
    [L_PROGNOSIS]           VARCHAR (50)  NULL,
    [L_SIZE]                REAL          NULL,
    [L_LYMPH_SCORE]         VARCHAR (50)  NULL,
    [L_TUMOUR_GRADE]        VARCHAR (50)  NULL,
    [L_SYM_SCREEN]          VARCHAR (50)  NULL,
    [L_ONLINE]              INT           NULL,
    [L_ONLINE_PROGNOSIS]    INT           NULL,
    [L_PRE_T_LETTER]        VARCHAR (3)   NULL,
    [L_PRE_N_LETTER]        VARCHAR (3)   NULL,
    [L_PATH_T_LETTER]       VARCHAR (3)   NULL,
    [L_PATH_N_LETTER]       VARCHAR (3)   NULL,
    [L_PRE_M_LETTER]        VARCHAR (3)   NULL,
    [L_PATH_M_LETTER]       VARCHAR (3)   NULL,
    [R_MET_LYMPH]           INT           NULL,
    [R_MET_ADRENAL]         INT           NULL,
    [R_MET_SKIN]            INT           NULL,
    [R_MET_UNKNOWN]         INT           NULL,
    [R_MET_BONE_MARROW]     INT           NULL,
    [R_MET_BONE_EX_MARROW]  INT           NULL,
    CONSTRAINT [PK_tblREFERRAL_BREAST] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_BREAST]
    ON [dbo].[tblREFERRAL_BREAST]([CARE_ID] ASC);


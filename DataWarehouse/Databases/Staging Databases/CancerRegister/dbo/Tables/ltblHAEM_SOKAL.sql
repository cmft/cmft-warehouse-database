﻿CREATE TABLE [dbo].[ltblHAEM_SOKAL] (
    [SCORE_CODE] INT          NOT NULL,
    [SCORE_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblHAEM_SOKAL] PRIMARY KEY CLUSTERED ([SCORE_CODE] ASC)
);


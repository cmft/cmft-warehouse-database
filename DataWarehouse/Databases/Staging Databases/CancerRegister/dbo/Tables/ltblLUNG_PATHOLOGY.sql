﻿CREATE TABLE [dbo].[ltblLUNG_PATHOLOGY] (
    [PATH_CODE] INT          NOT NULL,
    [PATH_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblLUNG_PATHOLOGY] PRIMARY KEY CLUSTERED ([PATH_CODE] ASC)
);


﻿CREATE TABLE [dbo].[expDAHNO_IMAGING_TEMP] (
    [DAHNO_ID]        INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]         INT          NULL,
    [PATIENT_ID]      INT          NULL,
    [NHSNumber]       VARCHAR (10) NULL,
    [SubmittingOrg]   VARCHAR (5)  NULL,
    [ContactOrg]      VARCHAR (5)  NULL,
    [PatientID]       VARCHAR (10) NULL,
    [DateDiagnosis]   DATETIME     NULL,
    [DateImaging]     DATETIME     NULL,
    [ImagingModality] VARCHAR (3)  NULL,
    [AnatomicalSite]  VARCHAR (5)  NULL,
    [DateRequested]   DATETIME     NULL,
    [DateReported]    DATETIME     NULL,
    [Username]        VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_IMAGING_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_IMAGING_TEMP]
    ON [dbo].[expDAHNO_IMAGING_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[tmpDAHNO_STATUS] (
    [DAHNOStatusID]       INT           IDENTITY (1, 1) NOT NULL,
    [NHSNumber]           VARCHAR (10)  NULL,
    [DateOfBirth]         SMALLDATETIME NULL,
    [PrimarySite]         VARCHAR (6)   NULL,
    [StatusOrg]           VARCHAR (5)   NULL,
    [StatusDate]          SMALLDATETIME NULL,
    [PrimaryTumourStatus] INT           NULL,
    [NodalStatus]         INT           NULL,
    [MetastaticStatus]    INT           NULL,
    [Username]            VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpDAHNO_Status] PRIMARY KEY CLUSTERED ([DAHNOStatusID] ASC)
);


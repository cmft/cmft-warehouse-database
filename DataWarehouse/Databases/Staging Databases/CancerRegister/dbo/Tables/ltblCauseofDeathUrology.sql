﻿CREATE TABLE [dbo].[ltblCauseofDeathUrology] (
    [CauseID]   INT           NOT NULL,
    [CauseCode] INT           NOT NULL,
    [CauseDesc] VARCHAR (100) NULL,
    CONSTRAINT [PK_ltblCauseofDeathNeph] PRIMARY KEY CLUSTERED ([CauseID] ASC)
);


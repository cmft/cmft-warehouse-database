﻿CREATE TABLE [dbo].[tblREFERRAL_LUNG] (
    [REF_ID]               INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]              INT           NOT NULL,
    [N_L23_CLASS_SC]       VARCHAR (1)   NULL,
    [R_MET_LUNG]           INT           NULL,
    [R_MET_BONE]           INT           NULL,
    [R_MET_LIVER]          INT           NULL,
    [R_MET_BRAIN]          INT           NULL,
    [R_MET_OTHER]          INT           NULL,
    [R_OTHER_METS]         VARCHAR (255) NULL,
    [L_CLASSIFICATION_SC]  VARCHAR (1)   NULL,
    [L_CLASSIFICATION_NSC] VARCHAR (20)  NULL,
    [L_CLASS_NSC]          VARCHAR (20)  NULL,
    [L_DIAG_METHOD]        INT           NULL,
    [L_PRE_T_LETTER]       CHAR (1)      NULL,
    [L_PRE_M_LETTER]       CHAR (1)      NULL,
    [L_PATH_T_LETTER]      CHAR (1)      NULL,
    [L_PATH_M_LETTER]      CHAR (1)      NULL,
    [L_PRE_N_LETTER]       CHAR (1)      NULL,
    [L_PATH_N_LETTER]      CHAR (1)      NULL,
    [R_MET_LYMPH]          INT           NULL,
    [R_MET_ADRENAL]        INT           NULL,
    [R_MET_SKIN]           INT           NULL,
    [R_MET_UNKNOWN]        INT           NULL,
    [R_MET_BONE_MARROW]    INT           NULL,
    [R_MET_BONE_EX_MARROW] INT           NULL,
    CONSTRAINT [PK_tblREFERRAL_LUNG] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_LUNG]
    ON [dbo].[tblREFERRAL_LUNG]([CARE_ID] ASC);


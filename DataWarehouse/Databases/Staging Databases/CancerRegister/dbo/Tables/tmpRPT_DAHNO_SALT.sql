﻿CREATE TABLE [dbo].[tmpRPT_DAHNO_SALT] (
    [DAHNOSaltID]          INT           IDENTITY (1, 1) NOT NULL,
    [CareID]               INT           NULL,
    [NHSNumber]            VARCHAR (10)  NULL,
    [DateOfBirth]          SMALLDATETIME NULL,
    [PrimarySite]          VARCHAR (6)   NULL,
    [SaltOrg]              VARCHAR (5)   NULL,
    [SaltAssessmentDate]   SMALLDATETIME NULL,
    [PatientAssessedPost]  VARCHAR (3)   NULL,
    [NormalcyDietPre]      VARCHAR (2)   NULL,
    [NormalcyDiet3Months]  VARCHAR (2)   NULL,
    [NormalcyDiet12Months] VARCHAR (2)   NULL,
    [Laryngectomy]         VARCHAR (5)   NULL,
    [Laryngectomy3Months]  VARCHAR (3)   NULL,
    [Laryngectomy12Months] VARCHAR (3)   NULL,
    [Username]             VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_DAHNO_Salt] PRIMARY KEY CLUSTERED ([DAHNOSaltID] ASC)
);


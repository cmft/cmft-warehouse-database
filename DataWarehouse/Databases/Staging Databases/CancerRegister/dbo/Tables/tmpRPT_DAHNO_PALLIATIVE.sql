﻿CREATE TABLE [dbo].[tmpRPT_DAHNO_PALLIATIVE] (
    [DAHNOPalliativeID]   INT           IDENTITY (1, 1) NOT NULL,
    [CareID]              INT           NULL,
    [NHSNumber]           VARCHAR (10)  NULL,
    [DateOfBirth]         SMALLDATETIME NULL,
    [PrimarySite]         VARCHAR (6)   NULL,
    [PalliativeOrg]       VARCHAR (5)   NULL,
    [PalliativeStartDate] SMALLDATETIME NULL,
    [Username]            VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_DAHNO_Palliative] PRIMARY KEY CLUSTERED ([DAHNOPalliativeID] ASC)
);


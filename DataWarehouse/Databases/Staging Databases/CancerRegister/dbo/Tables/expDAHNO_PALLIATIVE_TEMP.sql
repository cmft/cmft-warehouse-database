﻿CREATE TABLE [dbo].[expDAHNO_PALLIATIVE_TEMP] (
    [DAHNO_ID]               INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                INT          NULL,
    [PATIENT_ID]             INT          NULL,
    [NHSNumber]              VARCHAR (10) NULL,
    [SubmittingOrg]          VARCHAR (5)  NULL,
    [ContactOrg]             VARCHAR (5)  NULL,
    [PatientID]              VARCHAR (10) NULL,
    [DateDiagnosis]          DATETIME     NULL,
    [DateCarePlanAgreed]     DATETIME     NULL,
    [DateDecisionPalliative] DATETIME     NULL,
    [DateStartPalliative]    DATETIME     NULL,
    [Username]               VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_PALLIATIVE_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_PALLIATIVE_TEMP]
    ON [dbo].[expDAHNO_PALLIATIVE_TEMP]([Username] ASC);


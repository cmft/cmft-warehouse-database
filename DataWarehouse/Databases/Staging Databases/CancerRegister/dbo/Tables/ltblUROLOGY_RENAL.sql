﻿CREATE TABLE [dbo].[ltblUROLOGY_RENAL] (
    [TUMOUR_CODE] INT          NOT NULL,
    [TUMOUR_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblUROLOGY_TUMOUR_SITE] PRIMARY KEY CLUSTERED ([TUMOUR_CODE] ASC)
);


﻿CREATE TABLE [dbo].[ltblBREAST_CONSENT] (
    [STATUS_CODE] VARCHAR (2)   NOT NULL,
    [STATUS_DESC] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblBREAST_CONSENT] PRIMARY KEY CLUSTERED ([STATUS_CODE] ASC)
);


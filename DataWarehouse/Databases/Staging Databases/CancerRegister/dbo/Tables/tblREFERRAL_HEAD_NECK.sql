﻿CREATE TABLE [dbo].[tblREFERRAL_HEAD_NECK] (
    [REF_ID]                             INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                            INT           NOT NULL,
    [N_HN10_QOL]                         INT           NULL,
    [R_TUMOUR_NO]                        VARCHAR (50)  NULL,
    [R_RECURRENCE_TYPE]                  VARCHAR (50)  NULL,
    [R_NECK_NODES]                       VARCHAR (255) NULL,
    [R_MET_LUNG]                         INT           NULL,
    [R_MET_BONE]                         INT           NULL,
    [R_MET_LIVER]                        INT           NULL,
    [R_MET_BRAIN]                        INT           NULL,
    [R_MET_OTHER]                        INT           NULL,
    [R_OTHER_METS]                       VARCHAR (255) NULL,
    [L_RECORD_GIVEN]                     CHAR (3)      NULL,
    [L_PRE_T_LETTER]                     CHAR (1)      NULL,
    [L_PRE_N_LETTER]                     CHAR (1)      NULL,
    [L_PATH_T_LETTER]                    CHAR (1)      NULL,
    [L_PATH_N_LETTER]                    CHAR (1)      NULL,
    [L_PRE_M_LETTER]                     CHAR (1)      NULL,
    [L_PATH_M_LETTER]                    CHAR (1)      NULL,
    [R_MET_LYMPH]                        INT           NULL,
    [R_MET_ADRENAL]                      INT           NULL,
    [R_MET_SKIN]                         INT           NULL,
    [R_MET_UNKNOWN]                      INT           NULL,
    [R_MET_BONE_MARROW]                  INT           NULL,
    [R_MET_BONE_EX_MARROW]               INT           NULL,
    [INSS_ID]                            INT           NULL,
    [INPC_ID]                            INT           NULL,
    [CYTOGENETIC_RISK_CLASSIFICATION_ID] INT           NULL,
    [HPV_TEST_COMPLETE]                  VARCHAR (1)   NULL,
    [HPV_TEST]                           INT           NULL,
    [HPV_STATUS]                         INT           NULL,
    [PATIENT_CONCERNS_INVENTORY]         VARCHAR (1)   NULL,
    CONSTRAINT [PK_tblREFERRAL_HEAD_NECK] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_HEAD_NECK]
    ON [dbo].[tblREFERRAL_HEAD_NECK]([CARE_ID] ASC);


﻿CREATE TABLE [dbo].[ltblReferralLinkType] (
    [LinkTypeID]     INT           NOT NULL,
    [LinkTypeDesc]   VARCHAR (100) NOT NULL,
    [LinkParentDesc] VARCHAR (20)  NOT NULL,
    [LinkChildDesc]  VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_ltblReferralLinkType] PRIMARY KEY CLUSTERED ([LinkTypeID] ASC)
);


﻿CREATE TABLE [dbo].[ltblSYMPTOMS_SARCOMA] (
    [SYM_ID]      INT           NOT NULL,
    [SYM_DESC]    VARCHAR (200) NOT NULL,
    [CURRENT_SYM] INT           NOT NULL,
    CONSTRAINT [PK_ltblSYMPTOMS_SARCOMA] PRIMARY KEY CLUSTERED ([SYM_ID] ASC)
);


﻿CREATE TABLE [dbo].[rptNMBRA_PATHOLOGY_TEMP] (
    [NMBRA_ID]             INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]              INT          NULL,
    [PATIENT_ID]           INT          NULL,
    [POS_NODES]            INT          NULL,
    [HospitalType]         INT          NOT NULL,
    [SubmittingHospital]   VARCHAR (5)  NOT NULL,
    [NHSNumber]            VARCHAR (10) NULL,
    [HospitalNumber]       VARCHAR (10) NULL,
    [DateBirth]            DATETIME     NULL,
    [Laterality]           VARCHAR (1)  NULL,
    [InvasiveStatus]       VARCHAR (2)  NULL,
    [InvasiveSize]         REAL         NULL,
    [GradeDifferentiation] INT          NULL,
    [LymphNode]            VARCHAR (10) NULL,
    [NPI]                  REAL         NULL,
    [Username]             VARCHAR (50) NULL,
    CONSTRAINT [PK_rptNMBRA_PATHOLOGY_TEMP] PRIMARY KEY CLUSTERED ([NMBRA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptNMBRA_PATHOLOGY_TEMP]
    ON [dbo].[rptNMBRA_PATHOLOGY_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[tblREFERRAL_OTHER] (
    [REF_ID]               INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]              INT           NOT NULL,
    [R_MET_LUNG]           INT           NULL,
    [R_MET_BONE]           INT           NULL,
    [R_MET_LIVER]          INT           NULL,
    [R_MET_BRAIN]          INT           NULL,
    [R_MET_OTHER]          INT           NULL,
    [R_OTHER_METS]         VARCHAR (255) NULL,
    [L_PRE_T_LETTER]       VARCHAR (5)   NULL,
    [L_PATH_T_LETTER]      VARCHAR (5)   NULL,
    [L_PRE_N_LETTER]       VARCHAR (5)   NULL,
    [L_PRE_M_LETTER]       VARCHAR (5)   NULL,
    [L_PATH_N_LETTER]      VARCHAR (5)   NULL,
    [L_PATH_M_LETTER]      VARCHAR (5)   NULL,
    [R_MET_LYMPH]          INT           NULL,
    [R_MET_ADRENAL]        INT           NULL,
    [R_MET_SKIN]           INT           NULL,
    [R_MET_UNKNOWN]        INT           NULL,
    [R_MET_BONE_MARROW]    INT           NULL,
    [R_MET_BONE_EX_MARROW] INT           NULL,
    [INSS_ID]              INT           NULL,
    CONSTRAINT [PK_tblREFERRAL_OTHER] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_OTHER]
    ON [dbo].[tblREFERRAL_OTHER]([CARE_ID] ASC);


﻿CREATE TABLE [dbo].[ltblProstateTreatmentType] (
    [TreatTypeCode]     VARCHAR (3)   NOT NULL,
    [TreatTypeDesc]     VARCHAR (255) NOT NULL,
    [TreatTypeCOSDCode] VARCHAR (3)   NOT NULL,
    [NPCADatasetFlag]   BIT           NOT NULL,
    CONSTRAINT [PK_ltblProstateTreatmentType] PRIMARY KEY CLUSTERED ([TreatTypeCode] ASC)
);


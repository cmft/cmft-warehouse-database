﻿CREATE TABLE [dbo].[tempLogInAudit] (
    [tempID]        INT           IDENTITY (1, 1) NOT NULL,
    [userNameValue] VARCHAR (50)  NOT NULL,
    [oldPassword]   VARCHAR (500) NULL,
    [newPassword]   VARCHAR (500) NOT NULL,
    [actionDate]    DATETIME      CONSTRAINT [DF_Table_1_actiondate] DEFAULT (getdate()) NOT NULL,
    [actionType]    VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_tempLogInAudit] PRIMARY KEY CLUSTERED ([tempID] ASC)
);


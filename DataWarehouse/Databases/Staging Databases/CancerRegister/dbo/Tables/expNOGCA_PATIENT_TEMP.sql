﻿CREATE TABLE [dbo].[expNOGCA_PATIENT_TEMP] (
    [NOGCA_ID]       INT          IDENTITY (1, 1) NOT NULL,
    [PATIENT_ID]     INT          NULL,
    [NHSNumber]      VARCHAR (10) NOT NULL,
    [SubmittingOrg]  VARCHAR (10) NOT NULL,
    [ContactOrg]     VARCHAR (10) NULL,
    [HospitalNumber] VARCHAR (10) NULL,
    [Surname]        VARCHAR (60) NULL,
    [Forename]       VARCHAR (50) NULL,
    [Postcode]       VARCHAR (8)  NULL,
    [Sex]            INT          NULL,
    [DateBirth]      DATETIME     NULL,
    [Username]       VARCHAR (50) NULL,
    CONSTRAINT [PK_expNOGCA_PATIENT_TEMP] PRIMARY KEY CLUSTERED ([NOGCA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expNOGCA_PATIENT_TEMP]
    ON [dbo].[expNOGCA_PATIENT_TEMP]([Username] ASC);


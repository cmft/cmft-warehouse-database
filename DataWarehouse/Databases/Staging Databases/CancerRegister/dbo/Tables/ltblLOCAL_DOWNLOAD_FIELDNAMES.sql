﻿CREATE TABLE [dbo].[ltblLOCAL_DOWNLOAD_FIELDNAMES] (
    [ID]              NUMERIC (18)  IDENTITY (1, 1) NOT NULL,
    [FLD_REPORT_NAME] VARCHAR (255) NULL,
    [FLD_LIST_NAME]   VARCHAR (255) NULL,
    CONSTRAINT [PK_ltblLOCAL_DOWNLOAD_FIELDNAMES] PRIMARY KEY CLUSTERED ([ID] ASC)
);


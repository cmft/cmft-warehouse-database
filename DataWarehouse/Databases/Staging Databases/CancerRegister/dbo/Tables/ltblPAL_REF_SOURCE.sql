﻿CREATE TABLE [dbo].[ltblPAL_REF_SOURCE] (
    [Referral_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [Referral_Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblPAL_REF_SOURCE] PRIMARY KEY CLUSTERED ([Referral_ID] ASC)
);


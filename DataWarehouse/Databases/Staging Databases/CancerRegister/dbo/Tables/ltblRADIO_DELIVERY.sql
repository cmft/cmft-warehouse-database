﻿CREATE TABLE [dbo].[ltblRADIO_DELIVERY] (
    [DEL_CODE] CHAR (1)     NOT NULL,
    [DEL_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblRADIO_DELIVERY] PRIMARY KEY CLUSTERED ([DEL_CODE] ASC)
);


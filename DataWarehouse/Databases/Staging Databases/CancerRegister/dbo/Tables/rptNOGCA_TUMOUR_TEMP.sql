﻿CREATE TABLE [dbo].[rptNOGCA_TUMOUR_TEMP] (
    [NOGCA_ID]              INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]               INT          NULL,
    [PATIENT_ID]            INT          NULL,
    [M_LET]                 VARCHAR (1)  NULL,
    [NHSNumber]             VARCHAR (10) NOT NULL,
    [SubmittingOrg]         VARCHAR (10) NOT NULL,
    [ContactOrg]            VARCHAR (10) NULL,
    [HospitalNumber]        VARCHAR (10) NULL,
    [ReferralSource]        VARCHAR (2)  NULL,
    [ReferralPriority]      VARCHAR (2)  NULL,
    [DateReferral]          DATETIME     NULL,
    [DateDiagnosis]         DATETIME     NULL,
    [PreTreatmentSite]      VARCHAR (2)  NULL,
    [PreTreatmentHistology] VARCHAR (10) NULL,
    [StagingProcedure]      VARCHAR (20) NULL,
    [PreTreatmentT]         VARCHAR (2)  NULL,
    [PreTreatmentN]         VARCHAR (2)  NULL,
    [PreTreatmentM]         VARCHAR (3)  NULL,
    [PerformanceStatus]     INT          NULL,
    [CoMorbidity]           VARCHAR (50) NULL,
    [DateCarePlan]          DATETIME     NULL,
    [CarePlanIntent]        VARCHAR (1)  NULL,
    [TreatmentModality]     VARCHAR (2)  NULL,
    [PalliativeReason]      VARCHAR (2)  NULL,
    [Username]              VARCHAR (50) NULL,
    CONSTRAINT [PK_rptNOGCA_TUMOUR_TEMP] PRIMARY KEY CLUSTERED ([NOGCA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptNOGCA_TUMOUR_TEMP]
    ON [dbo].[rptNOGCA_TUMOUR_TEMP]([Username] ASC);


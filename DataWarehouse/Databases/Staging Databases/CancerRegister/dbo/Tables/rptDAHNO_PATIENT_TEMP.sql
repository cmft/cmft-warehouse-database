﻿CREATE TABLE [dbo].[rptDAHNO_PATIENT_TEMP] (
    [DAHNO_ID]       INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]        INT          NULL,
    [PATIENT_ID]     INT          NULL,
    [NSTS_Status]    INT          NULL,
    [NHSNumber]      VARCHAR (10) NULL,
    [SubmittingOrg]  VARCHAR (5)  NULL,
    [ContactOrg]     VARCHAR (5)  NULL,
    [PatientID]      VARCHAR (10) NULL,
    [Surname]        VARCHAR (60) NULL,
    [Forename]       VARCHAR (50) NULL,
    [PostCode]       VARCHAR (10) NULL,
    [Gender]         INT          NULL,
    [DateBirth]      DATETIME     NULL,
    [GPPracticeCode] VARCHAR (6)  NULL,
    [Username]       VARCHAR (50) NULL,
    [Err1]           INT          NULL,
    [Err2]           INT          NULL,
    [Err3]           INT          NULL,
    [Err4]           INT          NULL,
    [Err5]           INT          NULL,
    [Err6]           INT          NULL,
    [Err7]           INT          NULL,
    [Err8]           INT          NULL,
    [Err9]           INT          NULL,
    [Err10]          INT          NULL,
    [Err11]          INT          NULL,
    [Err12]          INT          NULL,
    [Err13]          INT          NULL,
    [Err14]          INT          NULL,
    [Err15]          INT          NULL,
    [Err16]          INT          NULL,
    [Err17]          INT          NULL,
    [Err18]          INT          NULL,
    CONSTRAINT [PK_rptDAHNO_PATIENT_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptDAHNO_PATIENT_TEMP]
    ON [dbo].[rptDAHNO_PATIENT_TEMP]([Username] ASC);


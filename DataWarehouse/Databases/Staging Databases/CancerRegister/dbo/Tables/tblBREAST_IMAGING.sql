﻿CREATE TABLE [dbo].[tblBREAST_IMAGING] (
    [BREAST_IMAGE_ID]       INT          IDENTITY (1, 1) NOT NULL,
    [IMAGING_ID]            INT          NOT NULL,
    [N3_5_LESION_SIZE]      REAL         NULL,
    [L_T_STAGE]             VARCHAR (3)  NULL,
    [L_T_LETTER]            VARCHAR (3)  NULL,
    [L_N_STAGE]             VARCHAR (3)  NULL,
    [L_N_LETTER]            VARCHAR (3)  NULL,
    [L_M_STAGE]             VARCHAR (3)  NULL,
    [L_SIDE]                VARCHAR (50) NULL,
    [L_SITE]                VARCHAR (50) NULL,
    [L_RAD_FEATURE]         VARCHAR (50) NULL,
    [L_NO_CORES]            VARCHAR (50) NULL,
    [L_BIOPSY_TYPE]         VARCHAR (50) NULL,
    [L_FINDINGS]            VARCHAR (3)  NULL,
    [L_MICRO_CALCIFICATION] VARCHAR (3)  NULL,
    [L_RESULTS]             TEXT         NULL,
    CONSTRAINT [PK_tblBREAST_IMAGING] PRIMARY KEY CLUSTERED ([BREAST_IMAGE_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblBREAST_IMAGING]
    ON [dbo].[tblBREAST_IMAGING]([IMAGING_ID] ASC);


﻿CREATE TABLE [dbo].[expNOGCA_SURGERY_TEMP] (
    [NOGCA_ID]              INT          IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]            INT          NULL,
    [CARE_ID]               INT          NULL,
    [PATIENT_ID]            INT          NULL,
    [NHSNumber]             VARCHAR (10) NOT NULL,
    [SubmittingOrg]         VARCHAR (10) NOT NULL,
    [TreatingOrg]           VARCHAR (10) NULL,
    [HospitalNumber]        VARCHAR (10) NULL,
    [DateDiagnosis]         DATETIME     NULL,
    [Surgeon]               VARCHAR (8)  NULL,
    [DateAdmission]         DATETIME     NULL,
    [DateSurgery]           DATETIME     NULL,
    [SurgicalIntent]        VARCHAR (1)  NULL,
    [PrioritySurgery]       INT          NULL,
    [FitnessASA]            VARCHAR (3)  NULL,
    [FEV]                   REAL         NULL,
    [FVC]                   REAL         NULL,
    [MainProcedure]         VARCHAR (2)  NULL,
    [ThoracicAssess]        INT          NULL,
    [AbdominalAccess]       INT          NULL,
    [FeedingAdjunct]        INT          NULL,
    [RemovedOrgan]          VARCHAR (10) NULL,
    [NodalDissection]       INT          NULL,
    [SurgicalComplications] VARCHAR (50) NULL,
    [DeathHospital]         VARCHAR (1)  NULL,
    [ReturnTheatre]         VARCHAR (1)  NULL,
    [DateDischarge]         DATETIME     NULL,
    [Username]              VARCHAR (50) NULL,
    CONSTRAINT [PK_expNOGCA_SURGERY_TEMP] PRIMARY KEY CLUSTERED ([NOGCA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expNOGCA_SURGERY_TEMP]
    ON [dbo].[expNOGCA_SURGERY_TEMP]([Username] ASC);


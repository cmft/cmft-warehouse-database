﻿CREATE TABLE [dbo].[tmpRPT_NOGCA_DEMOGRAPHICS] (
    [NOGCADemoID] INT           IDENTITY (1, 1) NOT NULL,
    [PatientID]   INT           NULL,
    [NHSNumber]   VARCHAR (50)  NULL,
    [DateOfBirth] SMALLDATETIME NULL,
    [Surname]     VARCHAR (50)  NULL,
    [Forename]    VARCHAR (50)  NULL,
    [PostCode]    VARCHAR (10)  NULL,
    [Sex]         INT           NULL,
    [Username]    VARCHAR (50)  NULL,
    CONSTRAINT [PK_rptNOGCA_DEMOGRAPHICS] PRIMARY KEY CLUSTERED ([NOGCADemoID] ASC)
);


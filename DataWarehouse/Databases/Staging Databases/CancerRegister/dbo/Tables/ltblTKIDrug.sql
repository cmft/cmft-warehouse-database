﻿CREATE TABLE [dbo].[ltblTKIDrug] (
    [DrugID]   INT           NOT NULL,
    [DrugCode] INT           NOT NULL,
    [DrugDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblTKIDrug] PRIMARY KEY CLUSTERED ([DrugID] ASC)
);


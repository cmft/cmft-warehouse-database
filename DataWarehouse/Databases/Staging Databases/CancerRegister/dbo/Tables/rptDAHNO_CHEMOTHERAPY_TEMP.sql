﻿CREATE TABLE [dbo].[rptDAHNO_CHEMOTHERAPY_TEMP] (
    [DAHNO_ID]                 INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                  INT          NULL,
    [PATIENT_ID]               INT          NULL,
    [NHSNumber]                VARCHAR (10) NULL,
    [SubmittingOrg]            VARCHAR (5)  NULL,
    [ContactOrg]               VARCHAR (5)  NULL,
    [PatientID]                VARCHAR (10) NULL,
    [DateDiagnosis]            DATETIME     NULL,
    [DateCarePlanAgreed]       DATETIME     NULL,
    [DateDecisionChemotherapy] DATETIME     NULL,
    [DrugType]                 VARCHAR (1)  NULL,
    [DrugIntent]               VARCHAR (1)  NULL,
    [DateStartChemotherapy]    DATETIME     NULL,
    [Username]                 VARCHAR (50) NULL,
    CONSTRAINT [PK_rptDAHNO_CHEMOTHERAPY_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptDAHNO_CHEMOTHERAPY_TEMP]
    ON [dbo].[rptDAHNO_CHEMOTHERAPY_TEMP]([Username] ASC);


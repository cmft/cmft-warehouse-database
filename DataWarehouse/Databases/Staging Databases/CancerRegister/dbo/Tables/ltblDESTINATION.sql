﻿CREATE TABLE [dbo].[ltblDESTINATION] (
    [DEST_CODE] INT           NOT NULL,
    [DEST_DESC] VARCHAR (150) NOT NULL,
    CONSTRAINT [PK_ltblDESTINATION] PRIMARY KEY CLUSTERED ([DEST_CODE] ASC)
);


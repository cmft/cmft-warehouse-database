﻿CREATE TABLE [dbo].[tblREFERRAL_BRAIN] (
    [REF_ID]                    INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                   INT           NOT NULL,
    [R_MET_LUNG]                INT           NULL,
    [R_MET_BONE]                INT           NULL,
    [R_MET_LIVER]               INT           NULL,
    [R_MET_BRAIN]               INT           NULL,
    [R_MET_OTHER]               INT           NULL,
    [R_OTHER_METS]              VARCHAR (255) NULL,
    [L_PRE_T_LETTER]            CHAR (1)      NULL,
    [L_PRE_N_LETTER]            CHAR (1)      NULL,
    [L_PRE_M_LETTER]            CHAR (1)      NULL,
    [L_PATH_T_LETTER]           CHAR (1)      NULL,
    [L_PATH_N_LETTER]           CHAR (1)      NULL,
    [L_PATH_M_LETTER]           CHAR (1)      NULL,
    [R_MET_LYMPH]               INT           NULL,
    [R_MET_ADRENAL]             INT           NULL,
    [R_MET_SKIN]                INT           NULL,
    [R_MET_UNKNOWN]             INT           NULL,
    [R_MET_BONE_MARROW]         INT           NULL,
    [R_MET_BONE_EX_MARROW]      INT           NULL,
    [CHANG_ID]                  INT           NULL,
    [LESION_LOCATION_ID]        INT           NULL,
    [LESION_NO_RADIOLOGICAL]    INT           NULL,
    [LESION_SIZE_RADIOLOGICAL]  INT           NULL,
    [DIAGNOSTIC_IMAGING_TYPE]   INT           NULL,
    [DIAGNOSIS_MDT_PROVISIONAL] VARCHAR (5)   NULL,
    [DIAGNOSIS_RADIOLOGICAL]    VARCHAR (5)   NULL,
    [CONTRAST_ENHANCEMENT]      INT           NULL,
    [CALCIFICATION]             INT           NULL,
    [MASS_EFFECT]               INT           NULL,
    [HYDROCEPHALUS]             INT           NULL,
    [HAEMORRHAGE]               INT           NULL,
    [CYSTIC]                    INT           NULL,
    [DURAL_TAIL]                INT           NULL,
    [BRAIN_OEDEMA]              INT           NULL,
    [CORD_SIGNAL_CHANGE]        INT           NULL,
    [CORD_COMPRESSION]          INT           NULL,
    CONSTRAINT [PK_tblREFERRAL_BRAIN] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_BRAIN]
    ON [dbo].[tblREFERRAL_BRAIN]([CARE_ID] ASC);


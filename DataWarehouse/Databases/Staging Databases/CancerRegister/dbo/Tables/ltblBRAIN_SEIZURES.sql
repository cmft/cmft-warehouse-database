﻿CREATE TABLE [dbo].[ltblBRAIN_SEIZURES] (
    [SEIZ_CODE] INT          NOT NULL,
    [SEIZ_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblBRAIN_SEIZURES] PRIMARY KEY CLUSTERED ([SEIZ_CODE] ASC)
);


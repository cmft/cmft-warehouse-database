﻿CREATE TABLE [dbo].[ltblProstateBiopsyTechnique] (
    [TechniqueID]   INT           NOT NULL,
    [TechniqueCode] INT           NOT NULL,
    [TechniqueDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateBiopsyTechnique] PRIMARY KEY CLUSTERED ([TechniqueID] ASC)
);


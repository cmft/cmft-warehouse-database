﻿CREATE TABLE [dbo].[expDAHNO_NURSING_TEMP] (
    [DAHNO_ID]            INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]             INT          NULL,
    [PATIENT_ID]          INT          NULL,
    [CNS_ID]              INT          NULL,
    [NHSNumber]           VARCHAR (10) NULL,
    [SubmittingOrg]       VARCHAR (5)  NULL,
    [ContactOrg]          VARCHAR (5)  NULL,
    [PatientID]           VARCHAR (10) NULL,
    [DateDiagnosis]       DATETIME     NULL,
    [SourceReferral]      VARCHAR (1)  NULL,
    [DateDecisionRefer]   DATETIME     NULL,
    [ReasonReferral]      VARCHAR (1)  NULL,
    [DateInitialContact]  DATETIME     NULL,
    [DatePtInformed]      DATETIME     NULL,
    [ProfessionalPresent] VARCHAR (20) NULL,
    [DateContact]         DATETIME     NULL,
    [TypeIntervention]    VARCHAR (20) NULL,
    [DateDischarged]      DATETIME     NULL,
    [Username]            VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_NURSING_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_NURSING_TEMP]
    ON [dbo].[expDAHNO_NURSING_TEMP]([Username] ASC);


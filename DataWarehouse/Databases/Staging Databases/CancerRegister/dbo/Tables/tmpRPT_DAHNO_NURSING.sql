﻿CREATE TABLE [dbo].[tmpRPT_DAHNO_NURSING] (
    [DAHNONursingID] INT           IDENTITY (1, 1) NOT NULL,
    [CareID]         INT           NULL,
    [NHSNumber]      VARCHAR (10)  NULL,
    [DateOfBirth]    SMALLDATETIME NULL,
    [PrimarySite]    VARCHAR (6)   NULL,
    [NursingOrg]     VARCHAR (5)   NULL,
    [CNSDate]        SMALLDATETIME NULL,
    [CNSPresent]     CHAR (1)      NULL,
    [Username]       VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_DAHNO_Nursing] PRIMARY KEY CLUSTERED ([DAHNONursingID] ASC)
);


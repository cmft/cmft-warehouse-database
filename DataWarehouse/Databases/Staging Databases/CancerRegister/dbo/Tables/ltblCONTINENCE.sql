﻿CREATE TABLE [dbo].[ltblCONTINENCE] (
    [CONT_CODE] INT          NOT NULL,
    [CONT_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblCONTINENCE] PRIMARY KEY CLUSTERED ([CONT_CODE] ASC)
);


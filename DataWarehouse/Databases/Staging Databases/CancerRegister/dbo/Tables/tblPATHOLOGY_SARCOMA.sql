﻿CREATE TABLE [dbo].[tblPATHOLOGY_SARCOMA] (
    [SARC_PATH_ID]       INT      IDENTITY (1, 1) NOT NULL,
    [PATHOLOGY_ID]       INT      NOT NULL,
    [N_SA10_NECROSIS]    REAL     NULL,
    [N_SA11_MARGIN]      REAL     NULL,
    [N_SA12_DEEP_FASCIA] CHAR (1) NULL,
    CONSTRAINT [PK_tblPATHOLOGY_SARCOMA] PRIMARY KEY CLUSTERED ([SARC_PATH_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblPATHOLOGY_SARCOMA]
    ON [dbo].[tblPATHOLOGY_SARCOMA]([PATHOLOGY_ID] ASC);


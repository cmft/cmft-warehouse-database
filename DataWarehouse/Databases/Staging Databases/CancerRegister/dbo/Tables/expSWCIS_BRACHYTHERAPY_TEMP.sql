﻿CREATE TABLE [dbo].[expSWCIS_BRACHYTHERAPY_TEMP] (
    [SWCIS_ID]          INT          IDENTITY (1, 1) NOT NULL,
    [NHSNumber]         VARCHAR (10) NULL,
    [OrgCodeSubmitting] VARCHAR (5)  NULL,
    [CareSpellID]       VARCHAR (50) NULL,
    [SiteCode]          VARCHAR (5)  NULL,
    [Consultant]        VARCHAR (8)  NULL,
    [DecisionTreatDate] DATETIME     NULL,
    [TreatmentIntent]   VARCHAR (1)  NULL,
    [BrachytherapyType] VARCHAR (2)  NULL,
    [TreatmentSite]     VARCHAR (5)  NULL,
    [StartDate]         DATETIME     NULL,
    [NumberInsertions]  INT          NULL,
    [DoseRateCode]      VARCHAR (1)  NULL,
    [DoseRate]          VARCHAR (50) NULL,
    [DoseInsertion]     REAL         NULL,
    [TotalGy]           REAL         NULL,
    [TreatmentTime]     INT          NULL,
    [Username]          VARCHAR (50) NULL,
    CONSTRAINT [PK_expSWCIS_BRACHYTHERAPY_TEMP] PRIMARY KEY CLUSTERED ([SWCIS_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expSWCIS_BRACHYTHERAPY_TEMP]
    ON [dbo].[expSWCIS_BRACHYTHERAPY_TEMP]([Username] ASC);


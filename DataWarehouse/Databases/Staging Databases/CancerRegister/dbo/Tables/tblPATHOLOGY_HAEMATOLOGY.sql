﻿CREATE TABLE [dbo].[tblPATHOLOGY_HAEMATOLOGY] (
    [HAEM_PATH_ID]               INT IDENTITY (1, 1) NOT NULL,
    [PATHOLOGY_ID]               INT NOT NULL,
    [RAI_ID]                     INT NULL,
    [BINET_ID]                   INT NULL,
    [ISS_ID]                     INT NULL,
    [ANN_ARBOR_STAGE_ID]         INT NULL,
    [ANN_ARBOR_SYMPTOMS_ID]      INT NULL,
    [ANN_ARBOR_EXTRANODALITY_ID] INT NULL,
    [ANN_ARBOR_BULK_ID]          INT NULL,
    [MURPHY_ID]                  INT NULL,
    [HASFORD_ID]                 INT NULL,
    [SOKAL_SCORE_CODE]           INT NULL,
    [ALK_1_ID]                   INT NULL,
    CONSTRAINT [PK_tblPATHOLOGY_HAEMATOLOGY] PRIMARY KEY CLUSTERED ([HAEM_PATH_ID] ASC)
);


﻿CREATE TABLE [dbo].[expNOGCA_ONCOLOGY_TEMP] (
    [NOGCA_ID]                 INT          IDENTITY (1, 1) NOT NULL,
    [TREATMENT_ID]             INT          NULL,
    [CARE_ID]                  INT          NULL,
    [PATIENT_ID]               INT          NULL,
    [NHSNumber]                VARCHAR (10) NOT NULL,
    [SubmittingOrg]            VARCHAR (10) NOT NULL,
    [HospitalNumber]           VARCHAR (10) NULL,
    [TreatingOrg]              VARCHAR (10) NULL,
    [DateDiagnosis]            DATETIME     NULL,
    [TreatmentIntent]          VARCHAR (1)  NULL,
    [TreatmentModality]        INT          NULL,
    [DateStartChemo]           DATETIME     NULL,
    [ChemoCyclesPrescribed]    INT          NULL,
    [ChemoCyclesReceived]      INT          NULL,
    [ChemoProtocol]            INT          NULL,
    [ChemoOutcome]             INT          NULL,
    [DateStartRadio]           DATETIME     NULL,
    [RadioDosePrescribed]      INT          NULL,
    [RadioFractionsPrescribed] INT          NULL,
    [RadioDoseReceived]        INT          NULL,
    [RadioFractionsReceived]   INT          NULL,
    [RadioOutcome]             INT          NULL,
    [Username]                 VARCHAR (50) NULL,
    CONSTRAINT [PK_expNOGCA_ONCOLOGY_TEMP] PRIMARY KEY CLUSTERED ([NOGCA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expNOGCA_ONCOLOGY_TEMP]
    ON [dbo].[expNOGCA_ONCOLOGY_TEMP]([Username] ASC);


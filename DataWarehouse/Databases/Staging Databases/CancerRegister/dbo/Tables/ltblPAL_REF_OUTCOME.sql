﻿CREATE TABLE [dbo].[ltblPAL_REF_OUTCOME] (
    [Outcome_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [Outcome_Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblPAL_REF_OUTCOME] PRIMARY KEY CLUSTERED ([Outcome_ID] ASC)
);


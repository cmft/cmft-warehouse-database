﻿CREATE TABLE [dbo].[ltblBREAST_RAD_FEATURES] (
    [RAD_CODE] INT          NOT NULL,
    [RAD_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblBREAST_RAD_FEATURES] PRIMARY KEY CLUSTERED ([RAD_CODE] ASC)
);


﻿CREATE TABLE [dbo].[ltblPostOpHistology] (
    [PostOpHistoID]   INT           NOT NULL,
    [PostOpHistoCode] INT           NOT NULL,
    [PostOpHistoDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblPostOpHistology] PRIMARY KEY CLUSTERED ([PostOpHistoID] ASC)
);


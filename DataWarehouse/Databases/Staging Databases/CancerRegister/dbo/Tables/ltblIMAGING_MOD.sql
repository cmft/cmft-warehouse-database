﻿CREATE TABLE [dbo].[ltblIMAGING_MOD] (
    [MOD_CODE]    VARCHAR (4)  NOT NULL,
    [MOD_DESC]    VARCHAR (50) NOT NULL,
    [BRAIN]       INT          NULL,
    [BREAST]      INT          NULL,
    [COLORECTAL]  INT          NULL,
    [GYNAECOLOGY] INT          NULL,
    [HEAD_NECK]   INT          NULL,
    [HAEMATOLOGY] INT          NULL,
    [LUNG]        INT          NULL,
    [PAEDIATRICS] INT          NULL,
    [SARCOMA]     INT          NULL,
    [SKIN]        INT          NULL,
    [UPPER_GI]    INT          NULL,
    [UROLOGY]     INT          NULL,
    [IS_IMAGE]    BIT          CONSTRAINT [DF_ltblIMAGING_MOD_IS_IMAGE] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ltblIMAGING_MOD] PRIMARY KEY CLUSTERED ([MOD_CODE] ASC)
);


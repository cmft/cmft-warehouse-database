﻿CREATE TABLE [dbo].[expNETWORK_MDT_TEMP] (
    [NETWORK_ID]                     INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                        INT           NULL,
    [PATIENT_ID]                     INT           NULL,
    [NSTS_Status]                    INT           NULL,
    [CancerSite]                     VARCHAR (50)  NULL,
    [DateFirstSeen]                  DATETIME      NULL,
    [DateDNA]                        DATETIME      NULL,
    [DateDiagnosis]                  DATETIME      NULL,
    [AgeAtDiagnosis]                 INT           NULL,
    [Histology]                      VARCHAR (10)  NULL,
    [DateDTT]                        DATETIME      NULL,
    [DateTreatment]                  DATETIME      NULL,
    [NHSNumber]                      VARCHAR (10)  NULL,
    [HospitalNumber]                 VARCHAR (10)  NULL,
    [Forename]                       VARCHAR (50)  NULL,
    [Surname]                        VARCHAR (60)  NULL,
    [DateBirth]                      DATETIME      NULL,
    [Gender]                         VARCHAR (1)   NULL,
    [PtAddress1]                     VARCHAR (150) NULL,
    [PtAddress2]                     VARCHAR (150) NULL,
    [PtAddress3]                     VARCHAR (150) NULL,
    [PtAddress4]                     VARCHAR (150) NULL,
    [PtPostCode]                     VARCHAR (10)  NULL,
    [RegisteredGP]                   VARCHAR (50)  NULL,
    [RegisteredGPCode]               VARCHAR (8)   NULL,
    [RegisteredPracticeCode]         VARCHAR (15)  NULL,
    [GPAddress1]                     VARCHAR (50)  NULL,
    [GPAddress2]                     VARCHAR (50)  NULL,
    [GPAddress3]                     VARCHAR (50)  NULL,
    [GPAddress4]                     VARCHAR (50)  NULL,
    [GPPostCode]                     VARCHAR (10)  NULL,
    [PCT]                            VARCHAR (50)  NULL,
    [PCTCode]                        VARCHAR (5)   NULL,
    [DateDeath]                      DATETIME      NULL,
    [PatientPathwayID]               VARCHAR (20)  NULL,
    [PathwayIDOrgCode]               VARCHAR (5)   NULL,
    [ReferralDecisionDate]           VARCHAR (10)  NULL,
    [SourceReferral]                 VARCHAR (2)   NULL,
    [PriorityType]                   VARCHAR (2)   NULL,
    [ReferralReceivedDate]           VARCHAR (10)  NULL,
    [ReferralType]                   VARCHAR (2)   NULL,
    [UpgradeDate]                    VARCHAR (10)  NULL,
    [UpgradeOrgCode]                 VARCHAR (5)   NULL,
    [FirstSeenDate]                  VARCHAR (10)  NULL,
    [FirstSeenOrgCode]               VARCHAR (5)   NULL,
    [FirstSeenAdj]                   INT           NULL,
    [FirstSeenAdjReason]             INT           NULL,
    [FirstSeenDelayComment]          VARCHAR (500) NULL,
    [FirstSeenDelayReason]           VARCHAR (2)   NULL,
    [MDTDiscussionIndicator]         VARCHAR (1)   NULL,
    [MDTDiscussionDate]              VARCHAR (10)  NULL,
    [PatientStatus]                  VARCHAR (2)   NULL,
    [DiagnosisDate]                  SMALLDATETIME NULL,
    [PrimaryDiagnosis]               VARCHAR (5)   NULL,
    [TumourLaterality]               VARCHAR (1)   NULL,
    [PtInformedDiagnosis]            VARCHAR (3)   NULL,
    [RelativeCarerInformedDiagnosis] VARCHAR (3)   NULL,
    [TreatmentEventType]             VARCHAR (2)   NULL,
    [MetastaticSite]                 VARCHAR (2)   NULL,
    [DecisionTreatOrgCode]           VARCHAR (5)   NULL,
    [DecisionTreatDate]              VARCHAR (10)  NULL,
    [TreatmentStartDate]             VARCHAR (10)  NULL,
    [TreatmentModality]              VARCHAR (2)   NULL,
    [CareSetting]                    VARCHAR (2)   NULL,
    [ClinicalTrial]                  VARCHAR (2)   NULL,
    [TreatmentOrgCode]               VARCHAR (5)   NULL,
    [RadiotherapyPriority]           VARCHAR (2)   NULL,
    [RadiotherapyIntent]             VARCHAR (2)   NULL,
    [DecisionTreatmentDelayComment]  VARCHAR (500) NULL,
    [DecisionTreatmentDelayReason]   INT           NULL,
    [TreatmentAdj]                   INT           NULL,
    [TreatmentAdjReason]             INT           NULL,
    [RefTreatmentDelayComment]       VARCHAR (500) NULL,
    [RefTreatmentDelayReason]        INT           NULL,
    [DecisionTreatAdj]               INT           NULL,
    [DecisionTreatAdjReason]         INT           NULL,
    [LocalMDTDate]                   SMALLDATETIME NULL,
    [MDTReferral]                    VARCHAR (5)   NULL,
    [MDTComments]                    VARCHAR (500) NULL,
    [Username]                       VARCHAR (50)  NULL,
    CONSTRAINT [PK_expNETWORK_MDT_TEMP] PRIMARY KEY CLUSTERED ([NETWORK_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expNETWORK_MDT_TEMP]
    ON [dbo].[expNETWORK_MDT_TEMP]([Username] ASC);


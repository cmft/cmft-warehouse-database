﻿CREATE TABLE [dbo].[ltblHGD_BARRETTS_SEGMENT] (
    [BARRETTS_SEG_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [BARRETTS_SEG_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblHGD_BARRETS_SEGMENT] PRIMARY KEY CLUSTERED ([BARRETTS_SEG_ID] ASC)
);


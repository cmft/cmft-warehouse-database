﻿CREATE TABLE [dbo].[newNBOCAP_CHEMORADIOTHERAPIES](
	[CARE_ID] [int] NULL,
	[EVENT_ID] [int] NULL,
	[EVENT_DATE] [datetime] NULL,
	[EVENT_TYPE] [varchar](10) NULL,
	[Username] [varchar](50) NULL,
	[OrgCodeTreatment] [varchar](5) NULL
) ON [PRIMARY]
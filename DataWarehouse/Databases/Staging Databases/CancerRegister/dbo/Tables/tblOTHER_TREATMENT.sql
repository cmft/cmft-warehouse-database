﻿CREATE TABLE [dbo].[tblOTHER_TREATMENT] (
    [OTHER_ID]                INT            IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                 INT            NOT NULL,
    [N16_9_DECISION_ACTIVE]   DATETIME       NULL,
    [N16_10_START_ACTIVE]     DATETIME       NULL,
    [N1_3_ORG_CODE_TREATMENT] VARCHAR (5)    NULL,
    [N_SITE_CODE_DTT]         VARCHAR (5)    NULL,
    [N_TREATMENT_EVENT]       VARCHAR (2)    NULL,
    [N_TREATMENT_SETTING]     VARCHAR (2)    NULL,
    [L_TRIAL]                 INT            NULL,
    [DEFINITIVE_TREATMENT]    INT            NULL,
    [CWT_PROFORMA]            INT            NULL,
    [L_COMMENTS]              VARCHAR (8000) NULL,
    [N7_2_CONSULTANT]         VARCHAR (8)    NULL,
    [PRE_TREAT_PSA]           REAL           NULL,
    [ROOT_CAUSE_COMMENTS]     TEXT           NULL,
    CONSTRAINT [PK_tblOTHER_TREATMENT] PRIMARY KEY CLUSTERED ([OTHER_ID] ASC)
);


﻿CREATE TABLE [dbo].[expDAHNO_SURGERY_TEMP] (
    [DAHNO_ID]             INT          IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]           INT          NULL,
    [CARE_ID]              INT          NULL,
    [PATIENT_ID]           INT          NULL,
    [NHSNumber]            VARCHAR (10) NULL,
    [SubmittingOrg]        VARCHAR (5)  NULL,
    [ContactOrg]           VARCHAR (5)  NULL,
    [PatientID]            VARCHAR (10) NULL,
    [DateDiagnosis]        DATETIME     NULL,
    [DateCarePlanAgreed]   DATETIME     NULL,
    [SurgeryIntent]        VARCHAR (1)  NULL,
    [DateDecisionSurgery]  DATETIME     NULL,
    [DateSurgery]          DATETIME     NULL,
    [PrimaryProcedure]     VARCHAR (30) NULL,
    [Procedure2]           VARCHAR (10) NULL,
    [Procedure3]           VARCHAR (10) NULL,
    [Procedure4]           VARCHAR (10) NULL,
    [Procedure5]           VARCHAR (10) NULL,
    [Procedure6]           VARCHAR (12) NULL,
    [DischargeDestination] INT          NULL,
    [PrimaryDiagnosis]     VARCHAR (10) NULL,
    [PathologyTNM]         VARCHAR (10) NULL,
    [PathologyT]           VARCHAR (5)  NULL,
    [PathologyN]           VARCHAR (5)  NULL,
    [PathologyM]           VARCHAR (5)  NULL,
    [DatePathologyReport]  DATETIME     NULL,
    [Histology]            VARCHAR (8)  NULL,
    [ExcisionMargins]      VARCHAR (1)  NULL,
    [Username]             VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_SURGERY_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_SURGERY_TEMP]
    ON [dbo].[expDAHNO_SURGERY_TEMP]([Username] ASC);


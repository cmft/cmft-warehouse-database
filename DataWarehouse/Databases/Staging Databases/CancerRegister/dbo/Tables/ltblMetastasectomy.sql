﻿CREATE TABLE [dbo].[ltblMetastasectomy] (
    [MetaID]   INT          NOT NULL,
    [MetaCode] INT          NOT NULL,
    [MetaDesc] VARCHAR (30) NOT NULL,
    CONSTRAINT [PK_ltblMetastasectomy] PRIMARY KEY CLUSTERED ([MetaID] ASC)
);


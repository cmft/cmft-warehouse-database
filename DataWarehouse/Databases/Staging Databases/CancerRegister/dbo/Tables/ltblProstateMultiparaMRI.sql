﻿CREATE TABLE [dbo].[ltblProstateMultiparaMRI] (
    [MultiparametricID]   INT           NOT NULL,
    [MultiparametricCode] INT           NOT NULL,
    [MultiparametricDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateMultiparaMRI] PRIMARY KEY CLUSTERED ([MultiparametricID] ASC)
);


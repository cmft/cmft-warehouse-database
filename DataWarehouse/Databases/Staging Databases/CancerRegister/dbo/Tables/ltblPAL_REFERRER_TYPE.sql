﻿CREATE TABLE [dbo].[ltblPAL_REFERRER_TYPE] (
    [ReferrerType_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [ReferrerType_Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblPAL_REFERRER_TYPE] PRIMARY KEY CLUSTERED ([ReferrerType_ID] ASC)
);


﻿CREATE TABLE [dbo].[expDAHNO_CAREPLAN_TEMP] (
    [DAHNO_ID]              INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]               INT          NULL,
    [PATIENT_ID]            INT          NULL,
    [NHSNumber]             VARCHAR (10) NULL,
    [SubmittingOrg]         VARCHAR (5)  NULL,
    [ContactOrg]            VARCHAR (5)  NULL,
    [PatientID]             VARCHAR (10) NULL,
    [DateDiagnosis]         DATETIME     NULL,
    [MDTDiscussion]         VARCHAR (1)  NULL,
    [DateMDT]               DATETIME     NULL,
    [DateCarePlan]          DATETIME     NULL,
    [CarePlanIntent]        VARCHAR (1)  NULL,
    [TreatmentType1]        VARCHAR (5)  NULL,
    [TreatmentType2]        VARCHAR (5)  NULL,
    [TreatmentType3]        VARCHAR (5)  NULL,
    [TreatmentType4]        VARCHAR (5)  NULL,
    [ComorbidityIndex]      INT          NULL,
    [PerformanceStatus]     INT          NULL,
    [DatePrimaryCareReport] DATETIME     NULL,
    [DateDentalAssessment]  DATETIME     NULL,
    [DateSALTAssessment]    DATETIME     NULL,
    [ClinicalTrialStatus]   VARCHAR (2)  NULL,
    [RecurrenceIndicator]   VARCHAR (1)  NULL,
    [PrimaryDiagnosis]      VARCHAR (10) NULL,
    [PreTreatmentTStage]    VARCHAR (3)  NULL,
    [PreTreatmentTCat]      VARCHAR (2)  NULL,
    [PreTreatmentNStage]    VARCHAR (3)  NULL,
    [PreTreatmentNCat]      VARCHAR (2)  NULL,
    [PreTreatmentMStage]    VARCHAR (3)  NULL,
    [PreTreatmentMCat]      VARCHAR (2)  NULL,
    [PreTreatmentStage]     VARCHAR (50) NULL,
    [PreTreatmentCat]       VARCHAR (2)  NULL,
    [ProfessionalPresent]   VARCHAR (20) NULL,
    [DatePatientInformed]   DATETIME     NULL,
    [Username]              VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_CAREPLAN_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_CAREPLAN_TEMP]
    ON [dbo].[expDAHNO_CAREPLAN_TEMP]([Username] ASC);


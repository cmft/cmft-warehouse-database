﻿CREATE TABLE [dbo].[expDAHNO_STATUS_TEMP] (
    [DAHNO_ID]              INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]               INT          NULL,
    [PATIENT_ID]            INT          NULL,
    [NHSNumber]             VARCHAR (10) NULL,
    [SubmittingOrg]         VARCHAR (5)  NULL,
    [ContactOrg]            VARCHAR (5)  NULL,
    [PatientID]             VARCHAR (10) NULL,
    [DateDiagnosis]         DATETIME     NULL,
    [DateAssessment]        DATETIME     NULL,
    [TumourStatus]          INT          NULL,
    [NodalStatus]           INT          NULL,
    [MetastaticStatus]      INT          NULL,
    [MorbidityChemotherapy] INT          NULL,
    [MorbidityRadiotherapy] INT          NULL,
    [MorbidityCombination]  INT          NULL,
    [Username]              VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_STATUS_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_STATUS_TEMP]
    ON [dbo].[expDAHNO_STATUS_TEMP]([Username] ASC);


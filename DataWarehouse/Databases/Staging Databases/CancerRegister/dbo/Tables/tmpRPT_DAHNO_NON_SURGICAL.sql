﻿CREATE TABLE [dbo].[tmpRPT_DAHNO_NON_SURGICAL] (
    [DAHNONonSurgID]          INT           IDENTITY (1, 1) NOT NULL,
    [CareID]                  INT           NULL,
    [NHSNumber]               VARCHAR (10)  NULL,
    [DateOfBirth]             SMALLDATETIME NULL,
    [PrimarySite]             VARCHAR (6)   NULL,
    [TreatID]                 INT           NULL,
    [TreatmentOrg]            VARCHAR (5)   NULL,
    [TreatmentType]           CHAR (2)      NULL,
    [TreatmentStartDate]      SMALLDATETIME NULL,
    [CancerTreatmentIntent]   CHAR (1)      NULL,
    [RadiotherapyTreatmentTo] VARCHAR (2)   NULL,
    [ChemotherapyDrugType]    CHAR (1)      NULL,
    [Username]                VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_DAHNO_NonSurgical] PRIMARY KEY CLUSTERED ([DAHNONonSurgID] ASC)
);


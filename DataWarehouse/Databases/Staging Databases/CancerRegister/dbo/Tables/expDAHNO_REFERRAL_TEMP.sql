﻿CREATE TABLE [dbo].[expDAHNO_REFERRAL_TEMP] (
    [DAHNO_ID]             INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]              INT          NULL,
    [PATIENT_ID]           INT          NULL,
    [NHSNumber]            VARCHAR (10) NULL,
    [SubmittingOrg]        VARCHAR (5)  NULL,
    [ContactOrg]           VARCHAR (5)  NULL,
    [PatientID]            VARCHAR (10) NULL,
    [DateDiagnosis]        DATETIME     NULL,
    [SourceReferral]       VARCHAR (2)  NULL,
    [PriorityReferral]     VARCHAR (2)  NULL,
    [DateReferralDecision] DATETIME     NULL,
    [DateReferralReceipt]  DATETIME     NULL,
    [DateFirstSeen]        DATETIME     NULL,
    [DateSymptomsNoted]    DATETIME     NULL,
    [Username]             VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_REFERRAL_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_REFERRAL_TEMP]
    ON [dbo].[expDAHNO_REFERRAL_TEMP]([Username] ASC);


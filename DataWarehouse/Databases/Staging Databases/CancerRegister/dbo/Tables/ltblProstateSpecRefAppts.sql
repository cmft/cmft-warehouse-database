﻿CREATE TABLE [dbo].[ltblProstateSpecRefAppts] (
    [SpecAppointmentID]   INT           NOT NULL,
    [SpecAppointmentCode] INT           NOT NULL,
    [SpecAppointmentDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateSpecRefAppts] PRIMARY KEY CLUSTERED ([SpecAppointmentID] ASC)
);


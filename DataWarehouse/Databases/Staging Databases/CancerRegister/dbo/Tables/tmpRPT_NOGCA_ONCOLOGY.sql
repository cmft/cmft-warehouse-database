﻿CREATE TABLE [dbo].[tmpRPT_NOGCA_ONCOLOGY] (
    [NOGCAONCOLOGYID]        INT           IDENTITY (1, 1) NOT NULL,
    [CareID]                 INT           NULL,
    [NHSNumber]              VARCHAR (50)  NULL,
    [DateOfBirth]            SMALLDATETIME NULL,
    [HospitalNumber]         VARCHAR (50)  NULL,
    [OrganisationCode]       VARCHAR (5)   NULL,
    [OncolTreatmentIntent]   VARCHAR (1)   NULL,
    [OncolTreatmentModality] INT           NULL,
    [ChemoStartDate]         SMALLDATETIME NULL,
    [ChemoOutcome]           INT           NULL,
    [RadioStartDate]         SMALLDATETIME NULL,
    [RadioOutcome]           INT           NULL,
    [ProceedCurativeSurgery] VARCHAR (2)   NULL,
    [Username]               VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_NOGCA_ONCOLOGY] PRIMARY KEY CLUSTERED ([NOGCAONCOLOGYID] ASC)
);


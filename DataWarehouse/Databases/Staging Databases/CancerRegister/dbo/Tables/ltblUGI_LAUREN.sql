﻿CREATE TABLE [dbo].[ltblUGI_LAUREN] (
    [CLASS_CODE] INT          NOT NULL,
    [CLASS_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblUGI_LAUREN] PRIMARY KEY CLUSTERED ([CLASS_CODE] ASC)
);


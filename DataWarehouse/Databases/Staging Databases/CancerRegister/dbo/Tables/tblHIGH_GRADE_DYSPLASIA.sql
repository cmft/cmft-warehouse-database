﻿CREATE TABLE [dbo].[tblHIGH_GRADE_DYSPLASIA] (
    [HGD_ID]              INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]             INT           NOT NULL,
    [HGD_FINAL]           BIT           CONSTRAINT [DF_tblHIGH_GRADE_DYSPLASIA_HGD_FINAL] DEFAULT ((0)) NOT NULL,
    [REFERRAL_ROUTE]      INT           NULL,
    [PATIENT_STATUS]      INT           NULL,
    [TUMOUR_STATUS]       INT           NULL,
    [DIAGNOSIS_DATE]      SMALLDATETIME NULL,
    [ASSESSED_BY]         INT           NULL,
    [DIAGNOSIS_GROUP]     VARCHAR (5)   NULL,
    [DIAGNOSIS_DETAILED]  VARCHAR (5)   NULL,
    [DIAGNOSIS_BASIS]     INT           NULL,
    [MORPHOLOGY]          VARCHAR (10)  NULL,
    [ORGANISATION]        VARCHAR (5)   NULL,
    [DIAGNOSIS_METHOD]    VARCHAR (50)  NULL,
    [SPECIALIST_REFERRAL] VARCHAR (1)   NULL,
    [TREATMENT_MODALITY]  INT           NULL,
    [EMR_ESD_USE]         INT           NULL,
    [EMR_DATE]            SMALLDATETIME NULL,
    [EMR_ESD_RESULT]      INT           NULL,
    [EMR_PATHOLOGY]       INT           NULL,
    CONSTRAINT [PK_tblHIGH_GRADE_DYSPLASIA] PRIMARY KEY CLUSTERED ([HGD_ID] ASC)
);


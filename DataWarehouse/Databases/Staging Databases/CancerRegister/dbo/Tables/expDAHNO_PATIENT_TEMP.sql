﻿CREATE TABLE [dbo].[expDAHNO_PATIENT_TEMP] (
    [DAHNO_ID]       INT          IDENTITY (1, 1) NOT NULL,
    [PATIENT_ID]     INT          NULL,
    [NHSNumber]      VARCHAR (10) NULL,
    [SubmittingOrg]  VARCHAR (5)  NULL,
    [ContactOrg]     VARCHAR (5)  NULL,
    [PatientID]      VARCHAR (10) NULL,
    [Surname]        VARCHAR (60) NULL,
    [Forename]       VARCHAR (50) NULL,
    [PostCode]       VARCHAR (8)  NULL,
    [Gender]         INT          NULL,
    [DateBirth]      DATETIME     NULL,
    [GPPracticeCode] VARCHAR (6)  NULL,
    [Username]       VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_PATIENT_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_PATIENT_TEMP]
    ON [dbo].[expDAHNO_PATIENT_TEMP]([Username] ASC);


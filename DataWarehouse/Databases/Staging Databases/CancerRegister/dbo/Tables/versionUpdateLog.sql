﻿CREATE TABLE [dbo].[versionUpdateLog] (
    [versionUpdateID]   INT           IDENTITY (1, 1) NOT NULL,
    [versionScriptName] VARCHAR (100) NOT NULL,
    [updatedDate]       DATETIME      CONSTRAINT [DF_versionUpdateLog_updatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_versionUpdateLog] PRIMARY KEY CLUSTERED ([versionUpdateID] ASC)
);


﻿CREATE TABLE [dbo].[rptDAHNO_DIAGNOSTIC_TEMP] (
    [DAHNO_ID]                INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                 INT          NULL,
    [PATIENT_ID]              INT          NULL,
    [NHSNumber]               VARCHAR (10) NULL,
    [SubmittingOrg]           VARCHAR (5)  NULL,
    [ContactOrg]              VARCHAR (5)  NULL,
    [PatientID]               VARCHAR (10) NULL,
    [DateDiagnosis]           DATETIME     NULL,
    [DiagnosticProcedureDate] DATETIME     NULL,
    [DiagnosticProcedure]     VARCHAR (5)  NULL,
    [ProcedureIntent]         VARCHAR (5)  NULL,
    [Username]                VARCHAR (50) NULL,
    CONSTRAINT [PK_rptDAHNO_DIAGNOSTIC_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptDAHNO_DIAGNOSTIC_TEMP]
    ON [dbo].[rptDAHNO_DIAGNOSTIC_TEMP]([Username] ASC);


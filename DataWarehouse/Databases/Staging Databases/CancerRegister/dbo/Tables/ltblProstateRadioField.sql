﻿CREATE TABLE [dbo].[ltblProstateRadioField] (
    [RadioFieldID]   INT           NOT NULL,
    [RadioFieldCode] INT           NOT NULL,
    [RadioFieldDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateRadioField] PRIMARY KEY CLUSTERED ([RadioFieldID] ASC)
);


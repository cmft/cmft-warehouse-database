﻿CREATE TABLE [dbo].[ltblProstateImageGuidanceType] (
    [GuidanceTypeID]   INT           NOT NULL,
    [GuidanceTypeCode] INT           NOT NULL,
    [GuidanceTypeDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateImageGuidanceType] PRIMARY KEY CLUSTERED ([GuidanceTypeID] ASC)
);


﻿CREATE TABLE [dbo].[tmpNOGCA_DEMOGRAPHICS] (
    [NOGCADemoID] INT           IDENTITY (1, 1) NOT NULL,
    [NHSNumber]   VARCHAR (10)  NULL,
    [DateOfBirth] SMALLDATETIME NULL,
    [Surname]     VARCHAR (50)  NULL,
    [Forename]    VARCHAR (50)  NULL,
    [PostCode]    VARCHAR (8)   NULL,
    [Sex]         INT           NULL,
    [Username]    VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpNOGCA_DEMOGRAPHICS] PRIMARY KEY CLUSTERED ([NOGCADemoID] ASC)
);


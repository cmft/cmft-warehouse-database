﻿CREATE TABLE [dbo].[expSWCIS_IMAGING_TEMP] (
    [SWCIS_ID]          INT           IDENTITY (1, 1) NOT NULL,
    [NHSNumber]         VARCHAR (10)  NULL,
    [OrgCodeSubmitting] VARCHAR (5)   NULL,
    [CareSpellID]       VARCHAR (50)  NULL,
    [SiteCode]          VARCHAR (5)   NULL,
    [InterventionDate]  DATETIME      NULL,
    [Modality]          VARCHAR (5)   NULL,
    [ModalityDesc]      VARCHAR (50)  NULL,
    [ExamSite]          VARCHAR (5)   NULL,
    [LesionSize]        REAL          NULL,
    [FIGO]              VARCHAR (50)  NULL,
    [CA125]             VARCHAR (255) NULL,
    [Username]          VARCHAR (50)  NULL,
    CONSTRAINT [PK_expSWCIS_IMAGING_TEMP] PRIMARY KEY CLUSTERED ([SWCIS_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expSWCIS_IMAGING_TEMP]
    ON [dbo].[expSWCIS_IMAGING_TEMP]([Username] ASC);


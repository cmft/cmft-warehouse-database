﻿CREATE TABLE [dbo].[ltblNATIONAL_CCG_INFO] (
    [CCG]                      VARCHAR (3)   NOT NULL,
    [Name]                     VARCHAR (100) NULL,
    [NationalGrouping]         VARCHAR (3)   NULL,
    [HighLevelHealthAuthority] VARCHAR (3)   NULL,
    [Address1]                 VARCHAR (35)  NULL,
    [Address2]                 VARCHAR (35)  NULL,
    [Address3]                 VARCHAR (35)  NULL,
    [Address4]                 VARCHAR (35)  NULL,
    [Address5]                 VARCHAR (35)  NULL,
    [Postcode]                 VARCHAR (8)   NULL,
    CONSTRAINT [PK_ltblNATIONAL_CCG_INFO] PRIMARY KEY CLUSTERED ([CCG] ASC)
);


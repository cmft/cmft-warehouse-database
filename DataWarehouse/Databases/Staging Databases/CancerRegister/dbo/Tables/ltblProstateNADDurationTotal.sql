﻿CREATE TABLE [dbo].[ltblProstateNADDurationTotal] (
    [TotalNADDurationID]   INT           NOT NULL,
    [TotalNADDurationCode] INT           NOT NULL,
    [TotalNADDurationDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateNADDurationTotal] PRIMARY KEY CLUSTERED ([TotalNADDurationID] ASC)
);


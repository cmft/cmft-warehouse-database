﻿CREATE TABLE [dbo].[expNMBRA_PREV_TREATMENT_TEMP] (
    [NMBRA_ID]           INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]            INT          NULL,
    [PATIENT_ID]         INT          NULL,
    [PATIENT_TYPE]       VARCHAR (3)  NULL,
    [ProcedureDate]      DATETIME     NULL,
    [HospitalType]       INT          NOT NULL,
    [SubmittingHospital] VARCHAR (5)  NOT NULL,
    [NHSNumber]          VARCHAR (10) NULL,
    [HospitalNumber]     VARCHAR (10) NULL,
    [DateBirth]          DATETIME     NULL,
    [DateDiagnosis]      DATETIME     NULL,
    [DateDTT]            DATETIME     NULL,
    [PreviousTreatment]  VARCHAR (20) NULL,
    [DateMastectomy]     DATETIME     NULL,
    [SmokingStatus]      INT          NULL,
    [BMI]                REAL         NULL,
    [Diabetes]           VARCHAR (2)  NULL,
    [ASAGrade]           INT          NULL,
    [WHOStatus]          INT          NULL,
    [Username]           VARCHAR (50) NULL,
    CONSTRAINT [PK_expNMBRA_PREV_TREATMENT_TEMP] PRIMARY KEY CLUSTERED ([NMBRA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expNMBRA_PREV_TREATMENT_TEMP]
    ON [dbo].[expNMBRA_PREV_TREATMENT_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[rptNOGCA_ENDOSCOPIC_TEMP] (
    [NOGCA_ID]            INT          IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]          INT          NULL,
    [CARE_ID]             INT          NULL,
    [PATIENT_ID]          INT          NULL,
    [NHSNumber]           VARCHAR (10) NOT NULL,
    [SubmittingOrg]       VARCHAR (10) NOT NULL,
    [HospitalNumber]      VARCHAR (10) NULL,
    [TreatingOrg]         VARCHAR (10) NULL,
    [DateDiagnosis]       DATETIME     NULL,
    [Consultant]          VARCHAR (8)  NULL,
    [DateProcedure]       DATETIME     NULL,
    [DysphagiaScore]      INT          NULL,
    [EndoscopicProcedure] VARCHAR (20) NULL,
    [MultipleProcedures]  VARCHAR (1)  NULL,
    [Anaesthetic]         INT          NULL,
    [EndoscopistGrade]    INT          NULL,
    [StentType]           INT          NULL,
    [StentPlacement]      INT          NULL,
    [StentJunction]       VARCHAR (1)  NULL,
    [StentDeployed]       VARCHAR (1)  NULL,
    [PlannedNumber]       INT          NULL,
    [PlannedProcedures]   VARCHAR (20) NULL,
    [Complications]       VARCHAR (20) NULL,
    [UnplannedNumber]     INT          NULL,
    [UnplannedProcedures] VARCHAR (20) NULL,
    [Username]            VARCHAR (50) NULL,
    CONSTRAINT [PK_rptNOGCA_ENDOSCOPIC_TEMP] PRIMARY KEY CLUSTERED ([NOGCA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptNOGCA_ENDOSCOPIC_TEMP]
    ON [dbo].[rptNOGCA_ENDOSCOPIC_TEMP]([Username] ASC);


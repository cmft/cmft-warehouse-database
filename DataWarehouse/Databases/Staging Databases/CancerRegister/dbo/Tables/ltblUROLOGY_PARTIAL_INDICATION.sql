﻿CREATE TABLE [dbo].[ltblUROLOGY_PARTIAL_INDICATION] (
    [PartialID]   INT          NOT NULL,
    [PartialDesc] VARCHAR (50) NOT NULL,
    [IsDeleted]   BIT          CONSTRAINT [DF_ltblUROLOGY_PARTIAL_INDICATION_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ltblUROLOGY_PARTIAL_INDICATION] PRIMARY KEY CLUSTERED ([PartialID] ASC)
);


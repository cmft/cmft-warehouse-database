﻿CREATE TABLE [dbo].[ltblUROLOGY_ADJ_REASON] (
    [AdjReasonID]   INT           NOT NULL,
    [AdjReasonCode] INT           NOT NULL,
    [AdjReasonDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblUROLOGY_ADJ_REASON] PRIMARY KEY CLUSTERED ([AdjReasonID] ASC)
);


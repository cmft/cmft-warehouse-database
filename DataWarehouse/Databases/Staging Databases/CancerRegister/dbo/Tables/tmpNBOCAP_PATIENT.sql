﻿CREATE TABLE [dbo].[tmpNBOCAP_PATIENT] (
    [NBOCAPPatientID] INT           IDENTITY (1, 1) NOT NULL,
    [NHSNumber]       NCHAR (10)    NULL,
    [DateOfBirth]     SMALLDATETIME NULL,
    [Surname]         VARCHAR (50)  NULL,
    [Forename]        VARCHAR (50)  NULL,
    [Postcode]        VARCHAR (8)   NULL,
    [Gender]          INT           NULL,
    [Username]        VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpNBOCAP_PATIENT] PRIMARY KEY CLUSTERED ([NBOCAPPatientID] ASC)
);


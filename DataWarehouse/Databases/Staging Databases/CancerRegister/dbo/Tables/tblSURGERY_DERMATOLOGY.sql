﻿CREATE TABLE [dbo].[tblSURGERY_DERMATOLOGY] (
    [DERM_SUR_ID]        INT           IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]         INT           NOT NULL,
    [N_SK14_NEW_LESIONS] INT           NULL,
    [N_SK15_REC_LESIONS] INT           NULL,
    [L_ASA]              INT           NULL,
    [L_NOT_OP]           VARCHAR (3)   NULL,
    [L_NO_OP_REASON]     INT           NULL,
    [L_START_TIME]       CHAR (5)      NULL,
    [L_END_TIME]         CHAR (5)      NULL,
    [L_INDICATION]       VARCHAR (255) NULL,
    [L_INCISION]         VARCHAR (255) NULL,
    [L_CLOSURE]          VARCHAR (255) NULL,
    [L_LATERALITY]       VARCHAR (5)   NULL,
    [L_LATERALITY2]      VARCHAR (5)   NULL,
    [L_LATERALITY3]      VARCHAR (5)   NULL,
    [L_TX_REASON]        INT           NULL,
    [L_SAMPLE]           VARCHAR (3)   NULL,
    [L_NO_SAMPLES]       INT           NULL,
    [L_FINDINGS]         TEXT          NULL,
    [L_DRAINS]           TEXT          NULL,
    [L_POST_OP]          TEXT          NULL,
    [L_COMMENTS]         TEXT          NULL,
    CONSTRAINT [PK_tblSURGERY_DERMATOLOGY] PRIMARY KEY CLUSTERED ([DERM_SUR_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblSURGERY_DERMATOLOGY]
    ON [dbo].[tblSURGERY_DERMATOLOGY]([SURGERY_ID] ASC);


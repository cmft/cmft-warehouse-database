﻿CREATE TABLE [dbo].[rptNMBRA_OPERATION_TEMP] (
    [NMBRA_ID]                  INT          IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]                INT          NULL,
    [CARE_ID]                   INT          NULL,
    [PATIENT_ID]                INT          NULL,
    [ASA]                       INT          NULL,
    [DTT_DATE]                  DATETIME     NULL,
    [LATERALITY]                VARCHAR (1)  NULL,
    [HospitalType]              INT          NOT NULL,
    [SubmittingHospital]        VARCHAR (5)  NOT NULL,
    [TypePatient]               INT          NULL,
    [NHSNumber]                 VARCHAR (10) NULL,
    [HospitalNumber]            VARCHAR (10) NULL,
    [DateBirth]                 DATETIME     NULL,
    [DateAdmission]             DATETIME     NULL,
    [DateProcedure]             DATETIME     NULL,
    [Mastectomy]                VARCHAR (2)  NULL,
    [Axillary]                  VARCHAR (2)  NULL,
    [Reconstruction]            VARCHAR (5)  NULL,
    [Symmetrisation]            VARCHAR (2)  NULL,
    [PlannedAdjuvant]           VARCHAR (20) NULL,
    [SecondaryReconstruction]   VARCHAR (2)  NULL,
    [ImmediateOffered]          VARCHAR (2)  NULL,
    [ReasonImmediateNotOffered] VARCHAR (2)  NULL,
    [DelayedOffered]            VARCHAR (2)  NULL,
    [DelayedAccepted]           VARCHAR (2)  NULL,
    [ReasonDelayedNotOffered]   VARCHAR (2)  NULL,
    [SiteCode]                  VARCHAR (5)  NULL,
    [DateDischarged]            DATETIME     NULL,
    [ReturnTheatre]             VARCHAR (2)  NULL,
    [TransferITU]               VARCHAR (2)  NULL,
    [AdmissionDeath]            VARCHAR (2)  NULL,
    [CompDonorSite]             VARCHAR (2)  NULL,
    [CompFlap]                  VARCHAR (2)  NULL,
    [CompImplant]               VARCHAR (2)  NULL,
    [CompDistant]               VARCHAR (2)  NULL,
    [Username]                  VARCHAR (50) NULL,
    CONSTRAINT [PK_rptNMBRA_OPERATION_TEMP] PRIMARY KEY CLUSTERED ([NMBRA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptNMBRA_OPERATION_TEMP]
    ON [dbo].[rptNMBRA_OPERATION_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[MirthInterfaceMessages] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [PatientNo]           VARCHAR (11)     NOT NULL,
    [IDType]              VARCHAR (5)      NOT NULL,
    [SiteCode]            VARCHAR (10)     NOT NULL,
    [MessageType]         VARCHAR (3)      NOT NULL,
    [Message]             XML              NOT NULL,
    [ReceivedOn]          DATETIME         CONSTRAINT [DF_MirthInterfaceMessages_dateReceived] DEFAULT (getdate()) NOT NULL,
    [ProcessedOn]         DATETIME         NULL,
    [CompletedOn]         DATETIME         NULL,
    [IsDirty]             BIT              CONSTRAINT [DF_MirthInterfaceMessages_IsDirty] DEFAULT ((0)) NULL,
    [CareID]              INT              NULL,
    [DelayReason]         VARCHAR (2)      NULL,
    [DelayReasonComments] VARCHAR (255)    NULL,
    CONSTRAINT [PK_MirthInterfaceMessages] PRIMARY KEY CLUSTERED ([Id] ASC)
);


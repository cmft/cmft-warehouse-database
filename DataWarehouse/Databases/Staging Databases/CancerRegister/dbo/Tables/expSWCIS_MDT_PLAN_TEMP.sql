﻿CREATE TABLE [dbo].[expSWCIS_MDT_PLAN_TEMP] (
    [SWCIS_ID]           INT          IDENTITY (1, 1) NOT NULL,
    [NHSNumber]          VARCHAR (10) NULL,
    [OrgCodeSubmitting]  VARCHAR (5)  NULL,
    [CareSpellID]        VARCHAR (50) NULL,
    [MDTIndicator]       VARCHAR (1)  NULL,
    [MDTDate]            DATETIME     NULL,
    [MDTMeetingType]     VARCHAR (50) NULL,
    [NetworkMDTDate]     DATETIME     NULL,
    [CarePlanAgreedDate] DATETIME     NULL,
    [Recurrence]         VARCHAR (1)  NULL,
    [CarePlanIntent]     VARCHAR (1)  NULL,
    [TreatmentType]      VARCHAR (50) NULL,
    [TreatmentSequence]  VARCHAR (50) NULL,
    [NoTreatmentReason]  VARCHAR (2)  NULL,
    [ComorbidityIndex]   INT          NULL,
    [PerformanceStatus]  INT          NULL,
    [MDTComments]        TEXT         NULL,
    [NetworkComments]    TEXT         NULL,
    [Username]           VARCHAR (50) NULL,
    CONSTRAINT [PK_expSWCIS_MDT_PLAN_TEMP] PRIMARY KEY CLUSTERED ([SWCIS_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expSWCIS_MDT_PLAN_TEMP]
    ON [dbo].[expSWCIS_MDT_PLAN_TEMP]([Username] ASC);


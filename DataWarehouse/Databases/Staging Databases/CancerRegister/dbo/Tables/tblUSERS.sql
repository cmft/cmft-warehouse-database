﻿CREATE TABLE [dbo].[tblUSERS] (
    [USER_ID]         INT           IDENTITY (1, 1) NOT NULL,
    [USER_NAME]       VARCHAR (50)  NOT NULL,
    [PASSWORD]        VARCHAR (50)  NOT NULL,
    [NAME]            VARCHAR (50)  NULL,
    [LOCATION]        VARCHAR (50)  NULL,
    [TRUST]           VARCHAR (5)   NULL,
    [CREATED_DATE]    SMALLDATETIME NULL,
    [ADMIN]           INT           NULL,
    [BRAIN]           INT           NULL,
    [BREAST]          INT           NULL,
    [COLORECTAL]      INT           NULL,
    [DERMATOLOGY]     INT           NULL,
    [GYNAECOLOGY]     INT           NULL,
    [HAEMATOLOGY]     INT           NULL,
    [HEAD_NECK]       INT           NULL,
    [LUNG]            INT           NULL,
    [OTHER]           INT           NULL,
    [PAEDIATRIC]      INT           NULL,
    [PALLIATIVE_CARE] INT           NULL,
    [SARCOMA]         INT           NULL,
    [UPPER_GI]        INT           NULL,
    [UROLOGY]         INT           NULL,
    [MDT_ACCESS]      INT           NULL,
    [CREATED_BY]      VARCHAR (50)  NULL,
    [LAST_DATE]       SMALLDATETIME NULL,
    [EXP_DATE]        SMALLDATETIME NULL,
    [DELETED]         INT           NULL,
    CONSTRAINT [PK_tblUSERS] PRIMARY KEY CLUSTERED ([USER_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblUSERS]
    ON [dbo].[tblUSERS]([USER_NAME] ASC, [PASSWORD] ASC);


﻿CREATE TABLE [dbo].[ltblMORBIDITY_SURGERY](
	[DIAG_CODE] [varchar](50) NOT NULL,
	[AllSites] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_AllSites]  DEFAULT ((0)),
	[Brain] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Brain]  DEFAULT ((0)),
	[Breast] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Breast]  DEFAULT ((0)),
	[Colorectal] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Colorectal]  DEFAULT ((0)),
	[Gynaecology] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Gynaecology]  DEFAULT ((0)),
	[Haematology] [bit] NULL,
	[HeadNeck] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_HeadNeck]  DEFAULT ((0)),
	[Lung] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Lung]  DEFAULT ((0)),
	[Other] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Other]  DEFAULT ((0)),
	[Paediatric] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Paediatric]  DEFAULT ((0)),
	[Sarcoma] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Sarcoma]  DEFAULT ((0)),
	[Skin] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Skin]  DEFAULT ((0)),
	[UpperGi] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_UpperGi]  DEFAULT ((0)),
	[Urology] [bit] NULL CONSTRAINT [DF_ltblMorbidity_Surgery_Urology]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ltblYNU] (
    [YN_ID]   INT          NOT NULL,
    [YN_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblYNU] PRIMARY KEY CLUSTERED ([YN_ID] ASC)
);


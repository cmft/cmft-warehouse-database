﻿CREATE TABLE [dbo].[ltblPAL_REASON_REFERRAL_2] (
    [RefReason2_ID]  INT          IDENTITY (1, 1) NOT NULL,
    [RefReason1_ID]  INT          NULL,
    [RefReason2Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltbl_PAL_REASON_REFERRAL_2] PRIMARY KEY CLUSTERED ([RefReason2_ID] ASC)
);


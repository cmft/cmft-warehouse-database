﻿CREATE TABLE [dbo].[tblMDT_LIST] (
    [MDT_ID]              INT           IDENTITY (1, 1) NOT NULL,
    [MEETING_ID]          INT           NULL,
    [MDT_DATE]            SMALLDATETIME NULL,
    [SUB_SITE]            VARCHAR (50)  NULL,
    [OTHER_SITE]          VARCHAR (50)  NULL,
    [PT_ORDER]            INT           NULL,
    [PATIENT_ID]          INT           NULL,
    [CARE_ID]             INT           NULL,
    [PRE_POST]            VARCHAR (4)   NULL,
    [DONE]                INT           NULL,
    [PREP]                INT           NULL,
    [COMMENTS]            VARCHAR (255) NULL,
    [RADIOLOGY_COMMENTS]  VARCHAR (255) NULL,
    [HISTOLOGY_COMMENTS]  VARCHAR (255) NULL,
    [MDT_COMMENTS]        TEXT          NULL,
    [LAST_SAVED]          DATETIME      NULL,
    [ROOT_CAUSE_COMMENTS] TEXT          NULL,
    [NEXT_OPA]            DATETIME      NULL,
    [TO_BE_SEEN_BY]       VARCHAR (50)  NULL,
    CONSTRAINT [PK_tblMDT_LIST] PRIMARY KEY CLUSTERED ([MDT_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblMDT_LIST]
    ON [dbo].[tblMDT_LIST]([MEETING_ID] ASC, [MDT_DATE] ASC);


﻿CREATE TABLE [dbo].[ltblProstateBrachyType] (
    [BrachytherapyTypeID]   INT           NOT NULL,
    [BrachytherapyTypeCode] INT           NOT NULL,
    [BrachytherapyTypeDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateBrachyType] PRIMARY KEY CLUSTERED ([BrachytherapyTypeID] ASC)
);


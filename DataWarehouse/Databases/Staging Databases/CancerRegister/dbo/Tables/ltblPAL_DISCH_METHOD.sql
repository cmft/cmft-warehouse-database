﻿CREATE TABLE [dbo].[ltblPAL_DISCH_METHOD] (
    [DischargeMethod_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [DischargeMethod_Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblPAL_DISCH_METHOD] PRIMARY KEY CLUSTERED ([DischargeMethod_ID] ASC)
);


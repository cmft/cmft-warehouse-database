﻿CREATE TABLE [dbo].[tblPATIENTS] (
    [PAT_ID]     INT           IDENTITY (1, 1) NOT NULL,
    [USER_ID]    VARCHAR (50)  NOT NULL,
    [PATIENT_ID] INT           NOT NULL,
    [LAST_DATE]  SMALLDATETIME NOT NULL,
    CONSTRAINT [PK_tblPATIENTS] PRIMARY KEY CLUSTERED ([PAT_ID] ASC)
);


GO
-- =============================================
-- Author:		Jim Shine	
-- Create date: 02/03/2009	
-- Description: Keeps no. of patients saved per user to last 15
-- =============================================
CREATE TRIGGER [dbo].[tblPATIENTS_limit]
   ON  [dbo].[tblPATIENTS]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Declare @USERID as varchar(50)
	Declare @Count as integer 
	Select @USERID=USER_ID from inserted
	
	Select @Count=count(PAT_ID) from tblPATIENTS where [USER_ID] = @USERID

	while @Count > 15 
	BEGIN
		DECLARE @MINDATE as datetime
		SELECT @MINDATE=MIN(LAST_DATE) FROM tblPATIENTS WHERE [USER_ID] = @USERID
		DELETE FROM tblPATIENTS WHERE [USER_ID] = @USERID AND LAST_DATE=@MINDATE
		Select @Count=count(PAT_ID) from tblPATIENTS where [USER_ID] = @USERID
	END

END
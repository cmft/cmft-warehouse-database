﻿CREATE TABLE [dbo].[expNBOCAP_PATIENT_TEMP] (
    [NBOCAP_ID]          INT          IDENTITY (1, 1) NOT NULL,
    [PATIENT_ID]         INT          NULL,
    [RecordType]         INT          NOT NULL,
    [NHSNumber]          VARCHAR (10) NOT NULL,
    [OriginatingOrgCode] VARCHAR (10) NOT NULL,
    [HospitalNumber]     VARCHAR (10) NULL,
    [Forename]           VARCHAR (22) NULL,
    [Surname]            VARCHAR (30) NULL,
    [Postcode]           VARCHAR (8)  NULL,
    [DateBirth]          DATETIME     NULL,
    [Sex]                INT          NULL,
    [Height]             REAL         NULL,
    [Weight]             REAL         NULL,
    [ConsultantCode]     VARCHAR (8)  NULL,
    [DateDeath]          DATETIME     NULL,
    [CauseDeath]         INT          NULL,
    [PostMortem]         VARCHAR (1)  NULL,
    [Username]           VARCHAR (50) NULL,
    CONSTRAINT [PK_expNBOCAP_PATIENT_TEMP] PRIMARY KEY CLUSTERED ([NBOCAP_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expNBOCAP_PATIENT_TEMP]
    ON [dbo].[expNBOCAP_PATIENT_TEMP]([Username] ASC);


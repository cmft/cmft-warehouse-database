﻿CREATE TABLE [dbo].[rptNMBRA_PATIENT_TEMP] (
    [NMBRA_ID]           INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]            INT          NULL,
    [PATIENT_ID]         INT          NULL,
    [NSTS_Status]        INT          NULL,
    [HospitalType]       INT          NOT NULL,
    [SubmittingHospital] VARCHAR (5)  NOT NULL,
    [NHSNumber]          VARCHAR (10) NULL,
    [HospitalNumber]     VARCHAR (10) NULL,
    [Forename]           VARCHAR (50) NULL,
    [Surname]            VARCHAR (60) NULL,
    [Postcode]           VARCHAR (10) NULL,
    [DateBirth]          DATETIME     NULL,
    [Ethnicity]          VARCHAR (1)  NULL,
    [ConsentStatus]      VARCHAR (2)  NULL,
    [ReasonNotConsent]   VARCHAR (2)  NULL,
    [SiteCode]           VARCHAR (5)  NULL,
    [TypePatient]        INT          NULL,
    [Username]           VARCHAR (50) NULL,
    [Err1]               INT          NULL,
    [Err2]               INT          NULL,
    [Err3]               INT          NULL,
    [Err4]               INT          NULL,
    [Err5]               INT          NULL,
    [Err6]               INT          NULL,
    [Err7]               INT          NULL,
    [Err8]               INT          NULL,
    [Err9]               INT          NULL,
    [Err10]              INT          NULL,
    [Err11]              INT          NULL,
    [Err12]              INT          NULL,
    [Err13]              INT          NULL,
    [Err14]              INT          NULL,
    [Err15]              INT          NULL,
    [Err16]              INT          NULL,
    [Err17]              INT          NULL,
    [Err18]              INT          NULL,
    CONSTRAINT [PK_rptNMBRA_PATIENT_TEMP] PRIMARY KEY CLUSTERED ([NMBRA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptNMBRA_PATIENT_TEMP]
    ON [dbo].[rptNMBRA_PATIENT_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[ltblCOLO_REC_BY] (
    [REC_CODE] INT          NOT NULL,
    [REC_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblCOLO_REC_BY] PRIMARY KEY CLUSTERED ([REC_CODE] ASC)
);


﻿CREATE TABLE [dbo].[expDAHNO_PATHOLOGY_TEMP] (
    [DAHNO_ID]            INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]             INT          NULL,
    [PATIENT_ID]          INT          NULL,
    [NHSNumber]           VARCHAR (10) NULL,
    [SubmittingOrg]       VARCHAR (5)  NULL,
    [ContactOrg]          VARCHAR (5)  NULL,
    [PatientID]           VARCHAR (10) NULL,
    [DateDiagnosis]       DATETIME     NULL,
    [SpecimenType]        VARCHAR (2)  NULL,
    [DatePathologyReport] DATETIME     NULL,
    [Histology]           VARCHAR (10) NULL,
    [ExcisionMargins]     VARCHAR (1)  NULL,
    [SpecimenNature]      INT          NULL,
    [Username]            VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_PATHOLOGY_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_PATHOLOGY_TEMP]
    ON [dbo].[expDAHNO_PATHOLOGY_TEMP]([Username] ASC);


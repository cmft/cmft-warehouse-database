﻿CREATE TABLE [dbo].[ltblBRAIN_CULTURES] (
    [CULTURE_CODE] INT          NOT NULL,
    [CULTURE_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblBRAIN_CULTURES] PRIMARY KEY CLUSTERED ([CULTURE_CODE] ASC)
);


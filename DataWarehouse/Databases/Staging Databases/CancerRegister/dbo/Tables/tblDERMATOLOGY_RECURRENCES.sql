﻿CREATE TABLE [dbo].[tblDERMATOLOGY_RECURRENCES] (
    [DIAGNOSIS_ID]         INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]              INT           NULL,
    [TEMP_ID]              VARCHAR (255) NULL,
    [N_SK1_DIAGNOSIS_DATE] SMALLDATETIME NULL,
    [N_SK2_BODY_SITE]      VARCHAR (2)   NULL,
    [N_SK3_DIAMETER]       REAL          NULL,
    [N_SK4_BCC_MORPHOLOGY] CHAR (1)      NULL,
    [N_SK5_MARGINS]        REAL          NULL,
    [N_SK6_RECURRENCE]     CHAR (1)      NULL,
    [L_SKIN_TYPE]          INT           NULL,
    [L_MET_LUNG]           INT           NULL,
    [L_MET_BONE]           INT           NULL,
    [L_MET_LIVER]          INT           NULL,
    [L_MET_OTHER]          INT           NULL,
    [L_OTHER_METS]         VARCHAR (255) NULL,
    CONSTRAINT [PK_tblDERMATOLOGY_RECURRENCES] PRIMARY KEY CLUSTERED ([DIAGNOSIS_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblDERMATOLOGY_RECURRENCES]
    ON [dbo].[tblDERMATOLOGY_RECURRENCES]([CARE_ID] ASC);


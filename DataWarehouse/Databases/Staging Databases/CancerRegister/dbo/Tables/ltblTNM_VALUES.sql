﻿CREATE TABLE [dbo].[ltblTNM_VALUES] (
    [TNM_ID]         INT           IDENTITY (1, 1) NOT NULL,
    [CANCER_SITE]    INT           NOT NULL,
    [VERSION_NUMBER] INT           NOT NULL,
    [IS_CLINICAL]    BIT           NOT NULL,
    [T_STAGE]        VARCHAR (50)  NULL,
    [T_LETTER]       VARCHAR (50)  NULL,
    [N_STAGE]        VARCHAR (50)  NULL,
    [N_LETTER]       VARCHAR (50)  NULL,
    [M_STAGE]        VARCHAR (50)  NULL,
    [M_LETTER]       VARCHAR (50)  NULL,
    [STAGE_GROUP]    VARCHAR (200) NULL,
    CONSTRAINT [PK_ltblTNM_VALUES] PRIMARY KEY CLUSTERED ([TNM_ID] ASC)
);


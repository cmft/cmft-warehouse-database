﻿CREATE TABLE [dbo].[ltblUGI_PROCEDURE_SITE] (
    [OP_SITE] INT          NOT NULL,
    [OP_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblUGI_PROC_SITE] PRIMARY KEY CLUSTERED ([OP_SITE] ASC)
);


﻿CREATE TABLE [dbo].[tblAddress] (
    [ID]       INT           IDENTITY (1, 1) NOT NULL,
    [Address1] VARCHAR (150) NOT NULL,
    [Address2] VARCHAR (150) NULL,
    [Address3] VARCHAR (150) NULL,
    [Address4] VARCHAR (150) NULL,
    [Address5] VARCHAR (150) NULL,
    [Postcode] VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK_tblAddress] PRIMARY KEY CLUSTERED ([ID] ASC)
);


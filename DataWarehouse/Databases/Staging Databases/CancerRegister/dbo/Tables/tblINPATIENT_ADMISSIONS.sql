﻿CREATE TABLE [dbo].[tblINPATIENT_ADMISSIONS] (
    [IP_ID]            INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]          INT           NOT NULL,
    [TEMP_ID]          VARCHAR (255) NULL,
    [L_ADMISSION_DATE] SMALLDATETIME NULL,
    [L_URGENCY]        INT           NULL,
    [L_DISCHARGE_DATE] SMALLDATETIME NULL,
    [L_REASON]         VARCHAR (255) NULL,
    [L_COMPLICATIONS]  INT           NULL,
    [L_COMPLICATIONS2] INT           NULL,
    [L_COMPLICATIONS3] INT           NULL,
    [L_COMPS]          VARCHAR (255) NULL,
    [L_PLAN]           VARCHAR (255) NULL,
    [L_DRUGS]          VARCHAR (255) NULL,
    [L_DESTINATION]    INT           NULL,
    [L_COMMENTS]       TEXT          NULL
);


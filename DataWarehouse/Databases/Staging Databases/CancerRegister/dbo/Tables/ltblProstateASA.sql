﻿CREATE TABLE [dbo].[ltblProstateASA] (
    [ASAID]   INT           NOT NULL,
    [ASACode] INT           NOT NULL,
    [ASADesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateASA] PRIMARY KEY CLUSTERED ([ASAID] ASC)
);


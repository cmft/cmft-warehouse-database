﻿CREATE TABLE [dbo].[tmpRPT_DAHNO_NUTRITION] (
    [DAHNONutritionID] INT           IDENTITY (1, 1) NOT NULL,
    [CareID]           INT           NULL,
    [NHSNumber]        VARCHAR (10)  NULL,
    [DateOfBirth]      SMALLDATETIME NULL,
    [PrimarySite]      VARCHAR (6)   NULL,
    [NutritionalOrg]   VARCHAR (5)   NULL,
    [ContactDate]      SMALLDATETIME NULL,
    [AssessedMonth]    CHAR (1)      NULL,
    [SupportMethod]    INT           NULL,
    [TubeType]         INT           NULL,
    [Assessed6Weeks]   CHAR (1)      NULL,
    [TreatType]        VARCHAR (3)   NULL,
    [Username]         VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_DAHNO_Nutrition] PRIMARY KEY CLUSTERED ([DAHNONutritionID] ASC)
);


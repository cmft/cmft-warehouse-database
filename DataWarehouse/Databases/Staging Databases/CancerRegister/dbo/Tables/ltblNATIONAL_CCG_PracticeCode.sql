﻿CREATE TABLE [dbo].[ltblNATIONAL_CCG_PracticeCode] (
    [CCG]          VARCHAR (3) NOT NULL,
    [PracticeCode] VARCHAR (6) NOT NULL,
    CONSTRAINT [PK_ltblNATIONAL_CCG_1] PRIMARY KEY CLUSTERED ([CCG] ASC, [PracticeCode] ASC)
);


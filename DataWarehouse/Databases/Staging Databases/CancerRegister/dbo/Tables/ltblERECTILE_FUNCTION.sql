﻿CREATE TABLE [dbo].[ltblERECTILE_FUNCTION] (
    [FUNCTION_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [M_F]           VARCHAR (1)  NOT NULL,
    [ERECTILE_DESC] VARCHAR (30) NOT NULL,
    CONSTRAINT [PK_ltblERECTILE_FUNCTION] PRIMARY KEY CLUSTERED ([FUNCTION_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ltblERECTILE_FUNCTION]
    ON [dbo].[ltblERECTILE_FUNCTION]([M_F] ASC);


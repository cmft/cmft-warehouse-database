﻿CREATE TABLE [dbo].[tblDEFINITIVE_TREATMENT] (
    [TREATMENT_ID]           INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                INT           NULL,
    [PATHWAY_ID]             VARCHAR (20)  NULL,
    [DECISION_DATE]          SMALLDATETIME NULL,
    [ORG_CODE_DTT]           VARCHAR (5)   NULL,
    [START_DATE]             SMALLDATETIME NULL,
    [TREATMENT]              CHAR (2)      NULL,
    [ORG_CODE]               VARCHAR (5)   NULL,
    [TREATMENT_EVENT]        VARCHAR (2)   NULL,
    [TREATMENT_SETTING]      VARCHAR (2)   NULL,
    [RT_PRIORITY]            VARCHAR (1)   NULL,
    [RT_INTENT]              VARCHAR (2)   NULL,
    [SPECIALIST]             VARCHAR (2)   NULL,
    [TRIAL]                  INT           NULL,
    [ADJ_DAYS]               INT           NULL,
    [ADJ_CODE]               INT           NULL,
    [DELAY_CODE]             INT           NULL,
    [TREAT_NO]               INT           NULL,
    [TREAT_ID]               INT           NULL,
    [CHEMO_RT]               VARCHAR (1)   NULL,
    [VALIDATED]              INT           NULL,
    [DELAY_COMMENTS]         TEXT          NULL,
    [COMMENTS]               TEXT          NULL,
    [ALL_COMMENTS]           TEXT          NULL,
    [ROOT_TCI_COMMENTS]      TEXT          NULL,
    [ROOT_DTT_DATE_COMMENTS] TEXT          NULL,
    CONSTRAINT [PK_tblDEFINITIVE_TREATMENT] PRIMARY KEY CLUSTERED ([TREATMENT_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblDEFINITIVE_TREATMENT]
    ON [dbo].[tblDEFINITIVE_TREATMENT]([CARE_ID] ASC);


﻿CREATE TABLE [dbo].[tmpDAHNO_PATIENT] (
    [DAHNOPatientID] INT           IDENTITY (1, 1) NOT NULL,
    [NHSNumber]      NCHAR (10)    NULL,
    [DateOfBirth]    SMALLDATETIME NULL,
    [FirstName]      VARCHAR (50)  NULL,
    [LastName]       VARCHAR (50)  NULL,
    [PostCode]       VARCHAR (8)   NULL,
    [Sex]            INT           NULL,
    [DateOfDeath]    SMALLDATETIME NULL,
    [Username]       VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpDAHNO_Patient] PRIMARY KEY CLUSTERED ([DAHNOPatientID] ASC)
);


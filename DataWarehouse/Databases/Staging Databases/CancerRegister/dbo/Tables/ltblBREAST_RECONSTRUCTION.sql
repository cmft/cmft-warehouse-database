﻿CREATE TABLE [dbo].[ltblBREAST_RECONSTRUCTION] (
    [RECON_CODE] INT          NOT NULL,
    [RECON_DESC] VARCHAR (50) NOT NULL,
    [OPCS_CODE]  VARCHAR (5)  NULL,
    [OPCS_CODE2] VARCHAR (5)  NULL,
    [OPCS_CODE3] VARCHAR (5)  NULL,
    CONSTRAINT [PK_ltblBREAST_RECONSTRUCTION] PRIMARY KEY CLUSTERED ([RECON_CODE] ASC)
);


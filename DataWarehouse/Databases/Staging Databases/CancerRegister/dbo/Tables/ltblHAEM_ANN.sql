﻿CREATE TABLE [dbo].[ltblHAEM_ANN] (
    [STAGE_CODE] INT          NOT NULL,
    [STAGE_DESC] VARCHAR (50) NOT NULL,
    [COSD_VALUE] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblHAEM_ANN] PRIMARY KEY CLUSTERED ([STAGE_CODE] ASC)
);


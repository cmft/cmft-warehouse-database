﻿CREATE TABLE [dbo].[tblCancerSiteTotals](
	[Cancer Site] [varchar](30) NOT NULL,
	[Total Referrals] [int] NOT NULL
) ON [PRIMARY]
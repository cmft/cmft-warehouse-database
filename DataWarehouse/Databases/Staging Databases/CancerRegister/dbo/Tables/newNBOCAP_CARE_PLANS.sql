﻿CREATE TABLE [dbo].[newNBOCAP_CARE_PLANS](
	[CARE_ID] [int] NULL,
	[EVENT_ID] [int] NULL,
	[EVENT_DATE] [smalldatetime] NULL,
	[Username] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tmpDAHNO_SALT] (
    [DAHNOSaltID]          INT           IDENTITY (1, 1) NOT NULL,
    [CareID]               INT           NULL,
    [NHSNumber]            VARCHAR (10)  NULL,
    [DateOfBirth]          SMALLDATETIME NULL,
    [PrimarySite]          VARCHAR (6)   NULL,
    [SaltOrg]              VARCHAR (5)   NULL,
    [SaltAssessmentDate]   SMALLDATETIME NULL,
    [PatientAssessedPost]  VARCHAR (3)   NULL,
    [NormalcyDietPre]      VARCHAR (2)   NULL,
    [NormalcyDiet3Months]  VARCHAR (2)   NULL,
    [NormalcyDiet12Months] VARCHAR (2)   NULL,
    [Laryngectomy]         VARCHAR (5)   NULL,
    [Laryngectomy3Months]  VARCHAR (5)   NULL,
    [Laryngectomy12Months] VARCHAR (5)   NULL,
    [Username]             VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpDAHNO_Salt] PRIMARY KEY CLUSTERED ([DAHNOSaltID] ASC)
);


﻿CREATE TABLE [dbo].[newNBOCAP_TUMOURS](
	[CARE_ID] [int] NOT NULL,
	[PATIENT_ID] [int] NULL,
	[N1_10_DATE_BIRTH] [datetime] NULL,
	[N1_3_ORG_CODE_SEEN] [varchar](10) NULL,
	[N4_1_DIAGNOSIS_DATE] [datetime] NULL,
	[N2_16_OP_REFERRAL] [varchar](3) NULL,
	[L_INDICATOR_CODE] [varchar](3) NULL,
	[N4_2_DIAGNOSIS_CODE] [varchar](10) NULL,
	[R_SYNC_TUMOUR] [varchar](3) NULL,
	[R_SYNC_APPENDIX] [varchar](3) NULL,
	[R_SYNC_CAECUM] [varchar](3) NULL,
	[R_SYNC_ASCENDING] [varchar](3) NULL,
	[R_SYNC_HEPATIC] [varchar](3) NULL,
	[R_SYNC_TRANSVERSE] [varchar](3) NULL,
	[R_SYNC_SPLENIC] [varchar](3) NULL,
	[R_SYNC_DESCENDING] [varchar](3) NULL,
	[R_SYNC_SIGMOID] [varchar](3) NULL,
	[R_SYNC_RECTUM] [varchar](3) NULL,
	[N6_1_PRE_T_STAGE] [varchar](5) NULL,
	[L_PRE_T_LETTER] [varchar](5) NULL,
	[N6_3_PRE_N_STAGE] [varchar](5) NULL,
	[N6_5_PRE_M_STAGE] [varchar](5) NULL,
	[Username] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ltblUGI_SIEWERT] (
    [SIEWERT_CODE] INT          NOT NULL,
    [SIEWERT_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblUGI_SIEWERT] PRIMARY KEY CLUSTERED ([SIEWERT_CODE] ASC)
);


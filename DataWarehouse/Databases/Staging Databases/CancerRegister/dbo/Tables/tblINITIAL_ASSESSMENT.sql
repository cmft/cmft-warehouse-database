﻿CREATE TABLE [dbo].[tblINITIAL_ASSESSMENT] (
    [ASSESS_ID]                            INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                              INT           NOT NULL,
    [TEMP_ID]                              VARCHAR (255) NOT NULL,
    [N_B4_MENSTRUAL_STATUS]                INT           NULL,
    [N_B5_LMP_DATE]                        SMALLDATETIME NULL,
    [N_HN1_CANCER_HISTORY]                 VARCHAR (50)  NULL,
    [N_HN2_CANCER_YEAR]                    INT           NULL,
    [N_HN3_PREVIOUS_TREATMENT]             VARCHAR (10)  NULL,
    [N_HN4_TOBACCO]                        VARCHAR (50)  NULL,
    [N_HN5_SMOKER]                         VARCHAR (2)   NULL,
    [N_HN6_CHEWING_TOBACCO]                INT           NULL,
    [N_HN7_YEAR_STOPPED]                   INT           NULL,
    [N_HN8_PACK_YEARS]                     INT           NULL,
    [N_HN9_ALCOHOL]                        INT           NULL,
    [N_HN11_SYMPTOM_DATE]                  SMALLDATETIME NULL,
    [N_SK13_DETERMINED]                    VARCHAR (1)   NULL,
    [N_L4_COPD]                            VARCHAR (1)   NULL,
    [N_L10_DEMENTIA]                       VARCHAR (1)   NULL,
    [N_L11_RENAL]                          VARCHAR (1)   NULL,
    [N_L12_OTHER]                          VARCHAR (1)   NULL,
    [N_L13_SEVERE]                         VARCHAR (1)   NULL,
    [N_L9_CARDIOVASCULAR]                  VARCHAR (1)   NULL,
    [N_H7_PREDISPOSING]                    VARCHAR (2)   NULL,
    [N_H12_SPLEEN]                         REAL          NULL,
    [N_H13_HEPATOMEGALY]                   REAL          NULL,
    [N_H14_LYMPHODENOPATHY]                VARCHAR (2)   NULL,
    [N_H15_SKIN]                           INT           NULL,
    [N_H16_TESTICULAR]                     INT           NULL,
    [N_H17_NEUROLOGICAL]                   INT           NULL,
    [N_H19_PRURITIS]                       INT           NULL,
    [L_HOSPITAL]                           VARCHAR (5)   NULL,
    [L_ASSESS_DATE]                        SMALLDATETIME NULL,
    [L_AS_BY]                              INT           NULL,
    [L_HEIGHT]                             REAL          NULL,
    [L_WEIGHT]                             REAL          NULL,
    [L_SOCIAL]                             INT           NULL,
    [L_SMOKING_AMOUNT]                     INT           NULL,
    [L_ALCOHOL_AMOUNT]                     INT           NULL,
    [L_SMOKING_CESSATION]                  CHAR (3)      NULL,
    [R_SPECIFY_OTHER_CA]                   VARCHAR (255) NULL,
    [R_DISEASE_HISTORY]                    VARCHAR (255) NULL,
    [R_CARDIAC_RISK]                       INT           NULL,
    [R_PERFORMANCE]                        INT           NULL,
    [R_ADENOMA]                            INT           NULL,
    [R_COLITIS]                            INT           NULL,
    [R_CROHNS]                             INT           NULL,
    [R_POLYPOSIS]                          INT           NULL,
    [R_FINDINGS]                           VARCHAR (2)   NULL,
    [L_FINDINGS]                           VARCHAR (2)   NULL,
    [L_CHILDREN]                           INT           NULL,
    [L_ON_HRT]                             VARCHAR (50)  NULL,
    [L_HRT]                                VARCHAR (255) NULL,
    [L_COPD_DETAILS]                       VARCHAR (255) NULL,
    [L_ABDO_PAIN]                          INT           NULL,
    [L_CHANGE_BOWELS]                      INT           NULL,
    [L_DIARRHOEA]                          INT           NULL,
    [L_CONSTIPATION]                       INT           NULL,
    [L_ALTERNATING]                        INT           NULL,
    [L_NOCTURNAL]                          INT           NULL,
    [L_BLOOD_STOOLS]                       INT           NULL,
    [L_FRESH]                              INT           NULL,
    [L_DARK]                               INT           NULL,
    [L_SEPARATE]                           INT           NULL,
    [L_MIXED]                              INT           NULL,
    [L_PAINFUL]                            INT           NULL,
    [L_PAINLESS]                           INT           NULL,
    [L_TENESMUS]                           INT           NULL,
    [L_IRON_DEFICIENCY]                    INT           NULL,
    [L_ASYMPTOMATIC]                       INT           NULL,
    [L_WEIGHT_LOSS]                        INT           NULL,
    [L_LOST_WEIGHT]                        REAL          NULL,
    [L_PALPABLE_MASS]                      INT           NULL,
    [L_ABDO_SURGERY]                       INT           NULL,
    [L_APPENDICECTOMY]                     INT           NULL,
    [L_CHOLECYSTECTOMY]                    INT           NULL,
    [L_GASTRODUODENAL]                     INT           NULL,
    [L_GYNAE_PELVIC]                       INT           NULL,
    [L_OTHER_PELVIC]                       INT           NULL,
    [L_ERECTILE_FUNCTION]                  VARCHAR (30)  NULL,
    [L_BLADDER_FUNCTION]                   VARCHAR (15)  NULL,
    [L_OTHER_BLADDER]                      VARCHAR (255) NULL,
    [L_OTHER_LUNG]                         INT           NULL,
    [L_LUMP_NECK]                          INT           NULL,
    [L_LUMP_PARTOID]                       INT           NULL,
    [L_OTOLOGIA]                           INT           NULL,
    [L_SWELLING]                           INT           NULL,
    [L_VOICE]                              INT           NULL,
    [L_LESION]                             INT           NULL,
    [L_THROAT]                             INT           NULL,
    [L_DYSPHAGIA]                          INT           NULL,
    [L_MOUTH]                              INT           NULL,
    [L_PALSY]                              INT           NULL,
    [L_LYMPHODENOPATHY]                    INT           NULL,
    [L_DYSPHONIA]                          INT           NULL,
    [L_PHARYNX]                            INT           NULL,
    [L_COMPLAINT_A]                        INT           NULL,
    [L_COMPLAINT_B]                        INT           NULL,
    [L_SAME_SKIN]                          VARCHAR (5)   NULL,
    [L_OTHER_SKIN]                         VARCHAR (5)   NULL,
    [L_TRANSPLANT]                         VARCHAR (3)   NULL,
    [L_IMMUNO_DISORDER]                    VARCHAR (3)   NULL,
    [L_PHOTOTHERAPY]                       VARCHAR (20)  NULL,
    [L_RADIOTHERAPY]                       VARCHAR (3)   NULL,
    [L_UN_LMP]                             INT           NULL,
    [L_CYCLE]                              INT           NULL,
    [L_AGE_MEN]                            INT           NULL,
    [L_AGE_MENO]                           INT           NULL,
    [L_MEDICAL_MEN]                        INT           NULL,
    [L_CHILDREN_YN]                        INT           NULL,
    [L_AGE_FIRST]                          INT           NULL,
    [L_BREAST_FED]                         INT           NULL,
    [L_FED_MONTHS]                         INT           NULL,
    [L_OCP]                                VARCHAR (50)  NULL,
    [L_OCP_PRE]                            INT           NULL,
    [L_OCP_TOTAL]                          INT           NULL,
    [L_OCP_COMMENTS]                       VARCHAR (255) NULL,
    [L_COIL]                               VARCHAR (10)  NULL,
    [L_STERILISED]                         INT           NULL,
    [L_HYSTERECTOMY]                       INT           NULL,
    [L_HYST_AGE]                           INT           NULL,
    [L_HYST_TYPE]                          INT           NULL,
    [L_OVARIES]                            INT           NULL,
    [L_MASS_L]                             INT           NULL,
    [L_MASS_R]                             INT           NULL,
    [L_CLOCK_L]                            INT           NULL,
    [L_CLOCK_R]                            INT           NULL,
    [L_DIS_L]                              REAL          NULL,
    [L_DIS_R]                              REAL          NULL,
    [L_SIZE_L]                             REAL          NULL,
    [L_SIZE_R]                             REAL          NULL,
    [L_CYSTIC_L]                           INT           NULL,
    [L_CYSTIC_R]                           INT           NULL,
    [L_SOLID_L]                            INT           NULL,
    [L_SOLID_R]                            INT           NULL,
    [L_FIXED_SKIN_L]                       INT           NULL,
    [L_FIXED_SKIN_R]                       INT           NULL,
    [L_FIXED_MUS_L]                        INT           NULL,
    [L_FIXED_MUS_R]                        INT           NULL,
    [L_LUMP_L]                             INT           NULL,
    [L_LUMP_R]                             INT           NULL,
    [L_NODULARITY_L]                       INT           NULL,
    [L_NODULARITY_R]                       INT           NULL,
    [L_NIPPLE_CHANGE_L]                    INT           NULL,
    [L_NIPPLE_CHANGE_R]                    INT           NULL,
    [L_NIPPLE_DISCHARGE_L]                 INT           NULL,
    [L_NIPPLE_DISCHARGE_R]                 INT           NULL,
    [L_NIPPLE_DISTORTION_L]                INT           NULL,
    [L_NIPPLE_DISTORTION_R]                INT           NULL,
    [L_ABSCESS_L]                          INT           NULL,
    [L_ABSCESS_R]                          INT           NULL,
    [L_ORANGE_L]                           INT           NULL,
    [L_ORANGE_R]                           INT           NULL,
    [L_ULCERATION_L]                       INT           NULL,
    [L_ULCERATION_R]                       INT           NULL,
    [L_LYMPH_L]                            INT           NULL,
    [L_LYMPH_R]                            INT           NULL,
    [L_OTHER_L]                            INT           NULL,
    [L_OTHER_R]                            INT           NULL,
    [L_AUX_L]                              INT           NULL,
    [L_AUX_R]                              INT           NULL,
    [L_MAMMOGRAM]                          INT           NULL,
    [L_MAM_INFO]                           VARCHAR (255) NULL,
    [L_PREGNANT]                           INT           NULL,
    [L_POSTPARTUM]                         INT           NULL,
    [L_FEEDING]                            INT           NULL,
    [L_DUE_DATE]                           SMALLDATETIME NULL,
    [L_DELIVERY_DATE]                      SMALLDATETIME NULL,
    [L_TRIMESTER]                          INT           NULL,
    [L_LACTATING]                          INT           NULL,
    [L_CO_M]                               INT           NULL,
    [L_ODYNOPHAGIA]                        INT           NULL,
    [L_EPIGASTRIC]                         INT           NULL,
    [L_BLEEDING]                           INT           NULL,
    [L_JAUNDICE]                           INT           NULL,
    [L_VOMITING]                           INT           NULL,
    [L_REFLUX]                             INT           NULL,
    [L_OESOPHAGITIS]                       INT           NULL,
    [L_BARRETTS]                           INT           NULL,
    [L_OTHER_RISKS]                        VARCHAR (255) NULL,
    [L_OESOPHAGUS]                         INT           NULL,
    [L_STOMACH]                            INT           NULL,
    [L_SMALL_BOWEL]                        INT           NULL,
    [L_COLON]                              INT           NULL,
    [L_RECTUM]                             INT           NULL,
    [L_PHYSICAL]                           INT           NULL,
    [L_ANAEMIC]                            INT           NULL,
    [L_MASS]                               INT           NULL,
    [L_NODES]                              INT           NULL,
    [L_HOARSE_VOICE]                       INT           NULL,
    [L_DISSEMINATED]                       INT           NULL,
    [L_LYMPH_SIZE]                         REAL          NULL,
    [L_FAMILY]                             VARCHAR (255) NULL,
    [L_PARITY]                             VARCHAR (255) NULL,
    [L_CESAREAN]                           VARCHAR (2)   NULL,
    [L_SMEAR]                              INT           NULL,
    [L_SMEAR_DATE]                         SMALLDATETIME NULL,
    [L_SMEAR_RESULT]                       VARCHAR (10)  NULL,
    [L_SMEAR_DETAILS]                      VARCHAR (255) NULL,
    [L_INFERTILITY]                        INT           NULL,
    [L_INFERTILITY_TX]                     VARCHAR (255) NULL,
    [L_PELVIC_EXAM]                        INT           NULL,
    [L_EXAM_VULVA]                         VARCHAR (255) NULL,
    [L_EXAM_CERVIX]                        VARCHAR (255) NULL,
    [L_EXAM_UTERUS]                        VARCHAR (255) NULL,
    [L_EXAM_ADNEXA]                        VARCHAR (255) NULL,
    [L_EXAM_RECTUM]                        VARCHAR (255) NULL,
    [L_COMORBIDITY_INDEX]                  INT           NULL,
    [L_MOLE]                               INT           NULL,
    [L_MOLE_DETAILS]                       VARCHAR (255) NULL,
    [L_LUTS]                               INT           NULL,
    [L_TURP]                               INT           NULL,
    [L_DRE]                                INT           NULL,
    [L_CM_ACUTE]                           INT           NULL,
    [L_CM_ALCOHOL]                         INT           NULL,
    [L_CM_ANAEMIA]                         INT           NULL,
    [L_CM_ANGINA]                          INT           NULL,
    [L_CM_ASPRIN]                          INT           NULL,
    [L_CM_BOWEL]                           INT           NULL,
    [L_CM_CARDIAC]                         INT           NULL,
    [L_CM_CEREBAL]                         INT           NULL,
    [L_CM_CHRONIC]                         INT           NULL,
    [L_CM_CIRRHOSIS]                       INT           NULL,
    [L_CM_CLOTTING]                        INT           NULL,
    [L_CM_COAD]                            INT           NULL,
    [L_CM_CVA]                             INT           NULL,
    [L_CM_DIABETES1]                       INT           NULL,
    [L_CM_DIABETES2]                       INT           NULL,
    [L_CM_FAILURE]                         INT           NULL,
    [L_CM_HEART]                           INT           NULL,
    [L_CM_HYPERTENSION]                    INT           NULL,
    [L_CM_LIVER]                           INT           NULL,
    [L_CM_MENTAL]                          INT           NULL,
    [L_CM_MI]                              INT           NULL,
    [L_CM_NEURO]                           INT           NULL,
    [L_CM_OTHER]                           INT           NULL,
    [L_CM_PERIPHERAL]                      INT           NULL,
    [L_CM_RENAL]                           INT           NULL,
    [L_CM_RESPIRATORY]                     INT           NULL,
    [L_CM_SERUM]                           INT           NULL,
    [L_CM_STEROIDS]                        INT           NULL,
    [L_CM_VASCULAR]                        INT           NULL,
    [L_CM_WARFARIN]                        INT           NULL,
    [L_CM_DIP]                             INT           NULL,
    [L_CM_CLOP]                            INT           NULL,
    [L_CM_PACE]                            INT           NULL,
    [L_CM_BARRETTS_OES]                    BIT           NULL,
    [L_OTHER_MORBIDITIES]                  VARCHAR (255) NULL,
    [L_FAMILY_TAKEN]                       INT           NULL,
    [L_PRE_TREAT_DENTAL]                   INT           NULL,
    [L_PRE_TREAT_DENTAL_DATE]              SMALLDATETIME NULL,
    [L_PRE_TREAT_NUTRITION]                INT           NULL,
    [L_PRE_TREAT_NUTRITION_DATE]           SMALLDATETIME NULL,
    [L_COMMENTS]                           TEXT          NULL,
    [L_OTHER_INFO]                         TEXT          NULL,
    [L_OTHER_PMH]                          TEXT          NULL,
    [L_MEDICATION]                         TEXT          NULL,
    [L_ALLERGIES]                          TEXT          NULL,
    [N_HN11_SYMPTOM_DAY]                   VARCHAR (2)   NULL,
    [N_HN11_SYMPTOM_MONTH]                 VARCHAR (2)   NULL,
    [N_HN11_SYMPTOM_YEAR]                  VARCHAR (4)   NULL,
    [OTHER_DIAGNOSIS_SUB_COMMENT]          VARCHAR (50)  NULL,
    [FAMILIAL_CANCER_SYNDROME_ID]          INT           NULL,
    [FAMILIAL_CANCER_SYNDROME_SUB_COMMENT] VARCHAR (50)  NULL,
    [ASSESS_SITE_CODE]                     VARCHAR (5)   NULL,
    [SKIN_CLINICAL_DIAGNOSIS]              INT           NULL,
    [CTYA_SECONDARY_DIAGNOSIS_1]           VARCHAR (6)   NULL,
    [CTYA_SECONDARY_DIAGNOSIS_2]           VARCHAR (6)   NULL,
    [CTYA_SECONDARY_DIAGNOSIS_3]           VARCHAR (6)   NULL,
    [CTYA_SECONDARY_DIAGNOSIS_4]           VARCHAR (6)   NULL,
    [RECTAL_BLEEDING]                      BIT           NULL,
    [PROST_COMP_SYMP_METS]                 BIT           NULL,
    [PROST_COMP_GENERAL]                   BIT           NULL,
    [PROST_COMP_NOT_KNOWN]                 BIT           NULL,
    [PROST_COMP_NONE]                      BIT           NULL,
    CONSTRAINT [PK_tblINITIAL_ASSESSMENT] PRIMARY KEY CLUSTERED ([ASSESS_ID] ASC)
);


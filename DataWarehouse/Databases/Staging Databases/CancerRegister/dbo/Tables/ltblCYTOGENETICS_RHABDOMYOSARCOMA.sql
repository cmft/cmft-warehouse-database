﻿CREATE TABLE [dbo].[ltblCYTOGENETICS_RHABDOMYOSARCOMA] (
    [CYTOGENETICS_ID]    INT          IDENTITY (1, 1) NOT NULL,
    [CYTOGENETICS_VALUE] VARCHAR (1)  NOT NULL,
    [CYTOGENETICS_DESC]  VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblCYTOGENETICS_RHABDOMYOSARCOMA] PRIMARY KEY CLUSTERED ([CYTOGENETICS_ID] ASC)
);


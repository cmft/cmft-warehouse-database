﻿CREATE TABLE [dbo].[ltblUrologyBiopsyResult] (
    [BiopsyResultID]   INT           NOT NULL,
    [BiopsyResultCode] INT           NOT NULL,
    [BiopsyResultDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblUrologyBiopsyResult] PRIMARY KEY CLUSTERED ([BiopsyResultID] ASC)
);


﻿CREATE TABLE [dbo].[MirthErrorMessages] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_MirthErrorMessages_Id] DEFAULT (newid()) NOT NULL,
    [MessageId]        UNIQUEIDENTIFIER NOT NULL,
    [ScreenId]         INT              NOT NULL,
    [FailingField]     VARCHAR (50)     NOT NULL,
    [ErrorDescription] VARCHAR (500)    NOT NULL,
    [Priority]         INT              NOT NULL,
    [UpdatedBy]        VARCHAR (50)     NULL,
    [UpdatedOn]        DATETIME         NULL,
    CONSTRAINT [PK_MirthErrorMessages] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__MirthErro__Messa__7D15B6A2] FOREIGN KEY ([MessageId]) REFERENCES [dbo].[MirthInterfaceMessages] ([Id]) ON DELETE CASCADE
);


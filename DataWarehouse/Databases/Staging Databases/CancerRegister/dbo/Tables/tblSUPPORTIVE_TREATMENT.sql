﻿CREATE TABLE [dbo].[tblSUPPORTIVE_TREATMENT] (
    [SUPPORT_ID]    INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]       INT           NULL,
    [TEMP_ID]       VARCHAR (255) NULL,
    [L_DATE]        SMALLDATETIME NULL,
    [L_RCT]         INT           NULL,
    [L_ANTIBIOTICS] INT           NULL,
    [L_ANTIFUNGALS] INT           NULL,
    [L_TPN]         INT           NULL,
    [L_PLATELETS]   INT           NULL,
    [L_OPIATE]      INT           NULL,
    [L_GCSF]        INT           NULL,
    [L_PCARE]       INT           NULL,
    [L_OTHER]       INT           NULL,
    [L_COMMENTS]    TEXT          NULL,
    CONSTRAINT [PK_tblSUPPORTIVE_TREATMENT] PRIMARY KEY CLUSTERED ([SUPPORT_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblSUPPORTIVE_TREATMENT]
    ON [dbo].[tblSUPPORTIVE_TREATMENT]([CARE_ID] ASC);


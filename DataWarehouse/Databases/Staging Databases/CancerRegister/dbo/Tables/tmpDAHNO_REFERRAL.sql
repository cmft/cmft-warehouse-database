﻿CREATE TABLE [dbo].[tmpDAHNO_REFERRAL] (
    [DAHNOID]         INT           IDENTITY (1, 1) NOT NULL,
    [CareID]          INT           NULL,
    [PatientID]       NCHAR (10)    NULL,
    [NHSNumber]       NCHAR (12)    NULL,
    [DateOfBirth]     SMALLDATETIME NULL,
    [SubmittingOrg]   VARCHAR (5)   NULL,
    [DateOfDiagnosis] SMALLDATETIME NULL,
    [PrimarySite]     VARCHAR (6)   NULL,
    [Username]        VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpDAHNO_REFERRAL] PRIMARY KEY CLUSTERED ([DAHNOID] ASC)
);


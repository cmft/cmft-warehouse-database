﻿CREATE TABLE [dbo].[ltblUSER_DEPARTMENT] (
    [DEPARTMENT_ID]          INT          IDENTITY (1, 1) NOT NULL,
    [DEPARTMENT_DESCRIPTION] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([DEPARTMENT_ID] ASC)
);


﻿CREATE TABLE [dbo].[rptNOGCA_PATIENT_TEMP] (
    [NOGCA_ID]       INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]        INT          NULL,
    [PATIENT_ID]     INT          NULL,
    [NSTS_Status]    INT          NULL,
    [NHSNumber]      VARCHAR (10) NOT NULL,
    [SubmittingOrg]  VARCHAR (10) NOT NULL,
    [ContactOrg]     VARCHAR (10) NULL,
    [HospitalNumber] VARCHAR (10) NULL,
    [Surname]        VARCHAR (60) NULL,
    [Forename]       VARCHAR (50) NULL,
    [Postcode]       VARCHAR (10) NULL,
    [Sex]            INT          NULL,
    [DateBirth]      DATETIME     NULL,
    [Username]       VARCHAR (50) NULL,
    [Err1]           INT          NULL,
    [Err2]           INT          NULL,
    [Err3]           INT          NULL,
    [Err4]           INT          NULL,
    [Err5]           INT          NULL,
    [Err6]           INT          NULL,
    [Err7]           INT          NULL,
    [Err8]           INT          NULL,
    [Err9]           INT          NULL,
    [Err10]          INT          NULL,
    [Err11]          INT          NULL,
    [Err12]          INT          NULL,
    [Err13]          INT          NULL,
    [Err14]          INT          NULL,
    [Err15]          INT          NULL,
    [Err16]          INT          NULL,
    [Err17]          INT          NULL,
    [Err18]          INT          NULL,
    [Err19]          INT          NULL,
    [Err20]          INT          NULL,
    [Err21]          INT          NULL,
    [Err22]          INT          NULL,
    [Err23]          INT          NULL,
    [Err24]          INT          NULL,
    [Err25]          INT          NULL,
    [Err26]          INT          NULL,
    CONSTRAINT [PK_rptNOGCA_PATIENT_TEMP] PRIMARY KEY CLUSTERED ([NOGCA_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptNOGCA_PATIENT_TEMP]
    ON [dbo].[rptNOGCA_PATIENT_TEMP]([Username] ASC);


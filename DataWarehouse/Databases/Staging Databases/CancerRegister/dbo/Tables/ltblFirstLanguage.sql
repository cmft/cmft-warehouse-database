﻿CREATE TABLE [dbo].[ltblFirstLanguage] (
    [langID]   INT        IDENTITY (1, 1) NOT NULL,
    [langCode] NCHAR (10) NULL,
    [langDesc] NCHAR (50) NULL,
    CONSTRAINT [PK_ltblFirstLanguage] PRIMARY KEY CLUSTERED ([langID] ASC)
);


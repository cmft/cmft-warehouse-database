﻿CREATE TABLE [dbo].[tblINITIAL_PAEDIATRICS] (
    [PAED_ID]         INT           IDENTITY (1, 1) NOT NULL,
    [ASSESS_ID]       INT           NOT NULL,
    [L_SCHOOL]        VARCHAR (255) NULL,
    [L_CONGENITAL]    VARCHAR (255) NULL,
    [L_DPT]           INT           NULL,
    [L_POLIO]         INT           NULL,
    [L_HIB]           INT           NULL,
    [L_MMR]           INT           NULL,
    [L_MENC]          INT           NULL,
    [L_BCG]           INT           NULL,
    [L_OTHER_IMM]     INT           NULL,
    [L_IMM_DETAILS]   VARCHAR (255) NULL,
    [L_CHICKENPOX]    INT           NULL,
    [L_SHINGLES]      INT           NULL,
    [L_OTHER_INFEC]   INT           NULL,
    [L_INFEC_DETAILS] VARCHAR (255) NULL,
    [LANSKY_ID]       INT           NULL,
    CONSTRAINT [PK_tblINITIAL_PAEDIATRICS] PRIMARY KEY CLUSTERED ([PAED_ID] ASC)
);


﻿CREATE TABLE [dbo].[tblPOSSUM_SCORE] (
    [POSSUM_ID]  INT           IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID] INT           NOT NULL,
    [TEMP_ID]    VARCHAR (255) NULL,
    [P1]         VARCHAR (100) NULL,
    [P2]         VARCHAR (100) NULL,
    [P3]         VARCHAR (100) NULL,
    [P4]         VARCHAR (100) NULL,
    [P5]         VARCHAR (100) NULL,
    [P6]         VARCHAR (100) NULL,
    [P7]         VARCHAR (100) NULL,
    [P8]         VARCHAR (100) NULL,
    [P9]         VARCHAR (100) NULL,
    [P10]        VARCHAR (100) NULL,
    [P11]        VARCHAR (100) NULL,
    [P12]        VARCHAR (100) NULL,
    [P13]        VARCHAR (100) NULL,
    [P14]        VARCHAR (100) NULL,
    [P15]        VARCHAR (100) NULL,
    [P16]        VARCHAR (100) NULL,
    [P17]        VARCHAR (100) NULL,
    [P18]        VARCHAR (100) NULL,
    [P19]        VARCHAR (100) NULL,
    [P20]        VARCHAR (100) NULL,
    [P21]        VARCHAR (100) NULL,
    CONSTRAINT [PK_tblPOSSUM_SCORE] PRIMARY KEY CLUSTERED ([POSSUM_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblPOSSUM_SCORE]
    ON [dbo].[tblPOSSUM_SCORE]([SURGERY_ID] ASC);


﻿CREATE TABLE [dbo].[tblPALLIATIVE_INPATIENTS] (
    [INPAT_ID]        INT           IDENTITY (1, 1) NOT NULL,
    [PALLIATIVE_ID]   INT           NULL,
    [TEMP_ID]         VARCHAR (255) NULL,
    [L_ADMIT_DATE]    SMALLDATETIME NULL,
    [L_DC_ADMIT]      INT           NULL,
    [L_LOC_PRE_ADMIT] INT           NULL,
    [L_CONSULTANT]    VARCHAR (8)   NULL,
    [L_HOSPITAL]      VARCHAR (5)   NULL,
    [L_WARD]          INT           NULL,
    [L_REASON]        VARCHAR (255) NULL,
    [L_INT_BLOOD]     INT           NULL,
    [L_INT_PARA]      INT           NULL,
    [L_INT_NEURAL]    INT           NULL,
    [L_INT_INFUSE]    INT           NULL,
    [L_DISCH_DATE]    SMALLDATETIME NULL,
    [L_DISCH_DEST]    INT           NULL,
    CONSTRAINT [PK_tblPALLIATIVE_INPATIENTS] PRIMARY KEY CLUSTERED ([INPAT_ID] ASC)
);


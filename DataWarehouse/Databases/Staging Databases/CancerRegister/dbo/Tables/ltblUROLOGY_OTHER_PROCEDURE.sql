﻿CREATE TABLE [dbo].[ltblUROLOGY_OTHER_PROCEDURE] (
    [ProcID]    INT          NOT NULL,
    [ProcDesc]  VARCHAR (50) NULL,
    [IsDeleted] BIT          CONSTRAINT [DF_ltblUROLOGY_OTHER_PROCEDURE_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ltblUROLOGY_OTHER_PROCEDURE] PRIMARY KEY CLUSTERED ([ProcID] ASC)
);


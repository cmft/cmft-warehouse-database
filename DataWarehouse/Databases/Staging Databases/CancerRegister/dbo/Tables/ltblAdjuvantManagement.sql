﻿CREATE TABLE [dbo].[ltblAdjuvantManagement] (
    [AdjManageID]   INT          NOT NULL,
    [AdjManageCode] INT          NOT NULL,
    [AdjManageDesc] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblAdjuvantManagement] PRIMARY KEY CLUSTERED ([AdjManageID] ASC)
);


﻿CREATE TABLE [dbo].[ltblContactType] (
    [Contact_Type_ID] INT          NOT NULL,
    [Contact_Type]    VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Contact_Type_ID] ASC)
);


﻿CREATE TABLE [dbo].[ltblMM_STAGE_TYPE] (
    [TYPE_CODE] INT          NOT NULL,
    [TYPE_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblMM_STAGE_TYPE] PRIMARY KEY CLUSTERED ([TYPE_CODE] ASC)
);


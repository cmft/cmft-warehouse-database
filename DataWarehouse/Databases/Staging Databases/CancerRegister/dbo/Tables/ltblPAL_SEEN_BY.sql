﻿CREATE TABLE [dbo].[ltblPAL_SEEN_BY] (
    [SeenBy_ID]   INT          NOT NULL,
    [SeenBy_Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblPAL_SEEN_BY] PRIMARY KEY CLUSTERED ([SeenBy_ID] ASC)
);


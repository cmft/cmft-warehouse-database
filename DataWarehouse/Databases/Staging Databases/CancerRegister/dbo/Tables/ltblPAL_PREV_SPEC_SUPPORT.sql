﻿CREATE TABLE [dbo].[ltblPAL_PREV_SPEC_SUPPORT] (
    [PrevSpecSupp_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [PrevSpecSupp_Desc] VARCHAR (70) NULL,
    CONSTRAINT [PK_ltblPAL_PREV_SPEC_SUPPORT] PRIMARY KEY CLUSTERED ([PrevSpecSupp_ID] ASC)
);


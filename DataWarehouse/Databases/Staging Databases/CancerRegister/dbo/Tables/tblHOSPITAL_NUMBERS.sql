﻿CREATE TABLE [dbo].[tblHOSPITAL_NUMBERS] (
    [PatientHospitalNoID] INT          IDENTITY (1, 1) NOT NULL,
    [PATIENT_ID]          INT          NOT NULL,
    [ORG_CODE]            VARCHAR (5)  NOT NULL,
    [HOSPITAL_NUMBER]     VARCHAR (50) NULL,
    [DEPARTMENT_NAME]     VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([PatientHospitalNoID] ASC)
);


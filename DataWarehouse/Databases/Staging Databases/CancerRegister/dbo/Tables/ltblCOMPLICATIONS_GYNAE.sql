﻿CREATE TABLE [dbo].[ltblCOMPLICATIONS_GYNAE] (
    [COMP_CODE] INT           IDENTITY (1, 1) NOT NULL,
    [COMP_DESC] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblCOMPLICATIONS_GYNAE] PRIMARY KEY CLUSTERED ([COMP_CODE] ASC)
);


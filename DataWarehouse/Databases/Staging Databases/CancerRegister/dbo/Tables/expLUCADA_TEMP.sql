﻿CREATE TABLE [dbo].[expLUCADA_TEMP] (
    [LUCADA_ID]                            INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                              INT          NULL,
    [PATIENT_ID]                           INT          NULL,
    [NSTS_Status]                          INT          NULL,
    [NHSNumber]                            VARCHAR (10) NULL,
    [Forename]                             VARCHAR (50) NULL,
    [Surname]                              VARCHAR (60) NULL,
    [DateBirth]                            VARCHAR (10) NULL,
    [BirthDate]                            DATETIME     NULL,
    [Sex]                                  INT          NULL,
    [AgeDiagnosis]                         INT          NULL,
    [PostCode]                             VARCHAR (8)  NULL,
    [OrganisationProvider]                 VARCHAR (5)  NULL,
    [TNMClassificationVNumber]             INT          NULL,
    [SourceReferral]                       VARCHAR (2)  NULL,
    [DateDecisionRefer]                    VARCHAR (10) NULL,
    [DateFirstSeen]                        VARCHAR (10) NULL,
    [FirstSeenDate]                        DATETIME     NULL,
    [OrganisationFirstSeen]                VARCHAR (5)  NULL,
    [DateReferredSpecialist]               VARCHAR (10) NULL,
    [DateDiagnosis]                        VARCHAR (10) NULL,
    [DiagnosisDate]                        DATETIME     NULL,
    [OrganisationDiagnosis]                VARCHAR (5)  NULL,
    [PrimaryDiagnosis]                     VARCHAR (5)  NULL,
    [Laterality]                           VARCHAR (1)  NULL,
    [BasisDiagnosis]                       INT          NULL,
    [TStage]                               VARCHAR (2)  NULL,
    [NStage]                               VARCHAR (1)  NULL,
    [MStage]                               VARCHAR (2)  NULL,
    [TCertainty]                           VARCHAR (1)  NULL,
    [NCertainty]                           VARCHAR (1)  NULL,
    [MCertainty]                           VARCHAR (1)  NULL,
    [TNMStage]                             VARCHAR (20) NULL,
    [Histology]                            VARCHAR (10) NULL,
    [StagingClassification]                VARCHAR (1)  NULL,
    [MDTDiscussion]                        VARCHAR (1)  NULL,
    [DateMDT]                              VARCHAR (10) NULL,
    [DateCarePlan]                         VARCHAR (10) NULL,
    [CarePlanIntent]                       VARCHAR (1)  NULL,
    [CoMorbidity]                          VARCHAR (1)  NULL,
    [DateCoMorbidity]                      VARCHAR (10) NULL,
    [COPD]                                 VARCHAR (1)  NULL,
    [DementiaCerebrovascular]              VARCHAR (1)  NULL,
    [Cardiovascular]                       VARCHAR (1)  NULL,
    [RenalFailure]                         VARCHAR (1)  NULL,
    [OtherMalignancy]                      VARCHAR (1)  NULL,
    [SevereWeightLoss]                     VARCHAR (1)  NULL,
    [OtherCoMorbidity]                     VARCHAR (1)  NULL,
    [FEV1Amount]                           REAL         NULL,
    [FEV1Percentage]                       REAL         NULL,
    [PerformanceStatus]                    INT          NULL,
    [OrganisationSurgery]                  VARCHAR (5)  NULL,
    [DateDecisionSurgery]                  VARCHAR (10) NULL,
    [DateSurgery]                          VARCHAR (10) NULL,
    [SurgeryDate]                          DATETIME     NULL,
    [PrimaryProcedure]                     VARCHAR (10) NULL,
    [MediastinoscopyMediastinotomy]        VARCHAR (1)  NULL,
    [ExcisionMargin]                       VARCHAR (1)  NULL,
    [pTStage]                              VARCHAR (2)  NULL,
    [pNStage]                              VARCHAR (1)  NULL,
    [pMStage]                              VARCHAR (2)  NULL,
    [pTCertainty]                          VARCHAR (1)  NULL,
    [pNCertainty]                          VARCHAR (1)  NULL,
    [pMCertainty]                          VARCHAR (1)  NULL,
    [pTNMStage]                            VARCHAR (20) NULL,
    [OrganisationDrug]                     VARCHAR (5)  NULL,
    [DateDecisionDrug]                     VARCHAR (10) NULL,
    [DateDrug]                             VARCHAR (10) NULL,
    [DrugTreatmentIntent]                  VARCHAR (1)  NULL,
    [ReceivePCI]                           VARCHAR (1)  NULL,
    [OrganisationTeletherapy]              VARCHAR (5)  NULL,
    [DateDecisionTeletherapy]              VARCHAR (10) NULL,
    [TeletherapyTreatmentIntent]           VARCHAR (1)  NULL,
    [TeletherapySite]                      VARCHAR (5)  NULL,
    [DateTeletherapy]                      VARCHAR (10) NULL,
    [OrganisationBrachytherapy]            VARCHAR (5)  NULL,
    [DateDecisionBrachytherapy]            VARCHAR (10) NULL,
    [DateBrachytherapy]                    VARCHAR (10) NULL,
    [OrganisationPalliativeCare]           VARCHAR (5)  NULL,
    [DateDecisionPalliativeCare]           VARCHAR (10) NULL,
    [DatePalliativeCare]                   VARCHAR (10) NULL,
    [OrganisationMonitoring]               VARCHAR (5)  NULL,
    [DateDecisionMonitoring]               VARCHAR (10) NULL,
    [ResultDate]                           VARCHAR (10) NULL,
    [pHistology]                           VARCHAR (10) NULL,
    [pStagingClassification]               VARCHAR (1)  NULL,
    [TrialStatus]                          VARCHAR (2)  NULL,
    [DateDeath]                            VARCHAR (10) NULL,
    [DeathRelatedTreatment]                VARCHAR (1)  NULL,
    [TreatmentMorbidity]                   INT          NULL,
    [ReceivedPCI]                          VARCHAR (1)  NULL,
    [RadiotherapyTreatmentGiven]           VARCHAR (2)  NULL,
    [ChemotherapyTreatmentGiven]           VARCHAR (2)  NULL,
    [TreatmentPlanType]                    VARCHAR (2)  NULL,
    [SuggestedTreatmentPlan]               VARCHAR (2)  NULL,
    [OriginalPlanCarriedOut]               VARCHAR (1)  NULL,
    [PlanFailureReason]                    VARCHAR (2)  NULL,
    [ReasonNotFirstChoice]                 VARCHAR (2)  NULL,
    [CTScan]                               VARCHAR (1)  NULL,
    [DateCTScan]                           VARCHAR (10) NULL,
    [PETScan]                              VARCHAR (1)  NULL,
    [DatePETScan]                          VARCHAR (10) NULL,
    [Bronchoscopy]                         VARCHAR (1)  NULL,
    [DateBronchoscopy]                     VARCHAR (10) NULL,
    [Biopsy]                               VARCHAR (1)  NULL,
    [DateBiopsy]                           VARCHAR (10) NULL,
    [CTBiopsy]                             VARCHAR (1)  NULL,
    [DateCTBiopsy]                         VARCHAR (10) NULL,
    [PalliativeProviderType]               VARCHAR (2)  NULL,
    [CommunityProviderType]                VARCHAR (2)  NULL,
    [InterventionalTxGiven]                VARCHAR (1)  NULL,
    [DateInterventionalTxGiven]            VARCHAR (10) NULL,
    [StagingProcedure]                     VARCHAR (1)  NULL,
    [StagingMediastinoscopyMediastinotomy] VARCHAR (1)  NULL,
    [StagingFNA]                           VARCHAR (1)  NULL,
    [OtherStaging]                         VARCHAR (1)  NULL,
    [UnknownStaging]                       VARCHAR (1)  NULL,
    [AssessedCNS]                          VARCHAR (2)  NULL,
    [DateAssessedCNS]                      VARCHAR (10) NULL,
    [HowAssessedCNS]                       INT          NULL,
    [WhenAssessedCNS]                      INT          NULL,
    [CNSPresentDiagnosis]                  VARCHAR (2)  NULL,
    [Username]                             VARCHAR (50) NULL,
    CONSTRAINT [PK_expLUCADA_TEMP] PRIMARY KEY CLUSTERED ([LUCADA_ID] ASC)
);


﻿CREATE TABLE [dbo].[ltblSCORE_NIH] (
    [NIH_ID]    INT           IDENTITY (1, 1) NOT NULL,
    [NIH_Q]     INT           NOT NULL,
    [NIH_SCORE] INT           NOT NULL,
    [NIH_DESC]  VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblSCORE_NIH] PRIMARY KEY CLUSTERED ([NIH_ID] ASC)
);


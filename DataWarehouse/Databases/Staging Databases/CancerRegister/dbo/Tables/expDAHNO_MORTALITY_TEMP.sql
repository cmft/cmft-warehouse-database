﻿CREATE TABLE [dbo].[expDAHNO_MORTALITY_TEMP] (
    [DAHNO_ID]      INT          IDENTITY (1, 1) NOT NULL,
    [PATIENT_ID]    INT          NULL,
    [NHSNumber]     VARCHAR (10) NULL,
    [SubmittingOrg] VARCHAR (5)  NULL,
    [ContactOrg]    VARCHAR (5)  NULL,
    [PatientID]     VARCHAR (10) NULL,
    [DateDeath]     DATETIME     NULL,
    [Username]      VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_MORTALITY_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_MORTALITY_TEMP]
    ON [dbo].[expDAHNO_MORTALITY_TEMP]([Username] ASC);


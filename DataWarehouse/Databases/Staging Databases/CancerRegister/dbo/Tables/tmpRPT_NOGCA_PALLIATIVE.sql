﻿CREATE TABLE [dbo].[tmpRPT_NOGCA_PALLIATIVE] (
    [NOGCAPalTherapyID]    INT           IDENTITY (1, 1) NOT NULL,
    [CareID]               INT           NULL,
    [NHSNumber]            VARCHAR (50)  NULL,
    [DateOfBirth]          SMALLDATETIME NULL,
    [HospitalNumber]       VARCHAR (50)  NULL,
    [Hospital_endotherapy] VARCHAR (5)   NULL,
    [EndoProcedureDate]    SMALLDATETIME NULL,
    [DysphagiaScore]       INT           NULL,
    [EndoFirstProcedure]   VARCHAR (20)  NULL,
    [EndoMultiple]         VARCHAR (2)   NULL,
    [Anaesthetic]          INT           NULL,
    [StentType]            INT           NULL,
    [StentPlacement]       INT           NULL,
    [StentDeployed]        VARCHAR (2)   NULL,
    [EndoComplications]    VARCHAR (20)  NULL,
    [Username]             VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_NOGCA_PALLIATIVE] PRIMARY KEY CLUSTERED ([NOGCAPalTherapyID] ASC)
);


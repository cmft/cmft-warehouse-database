﻿CREATE TABLE [dbo].[tblSURGERY_HEAD_NECK] (
    [HN_SUR_ID]                   INT           IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]                  INT           NOT NULL,
    [L_CANCELLED]                 INT           NULL,
    [L_CANCEL_OP]                 VARCHAR (255) NULL,
    [L_ASA]                       INT           NULL,
    [L_START_TIME]                CHAR (5)      NULL,
    [L_END_TIME]                  CHAR (5)      NULL,
    [L_INDICATION]                VARCHAR (255) NULL,
    [L_INCISION]                  VARCHAR (255) NULL,
    [L_CLOSURE]                   VARCHAR (255) NULL,
    [L_AIRWAY_PROC]               INT           NULL,
    [L_NECK_LEFT_PROC]            INT           NULL,
    [L_NECK_RIGHT_PROC]           INT           NULL,
    [L_RECONSTRUCTION]            INT           NULL,
    [L_GRAFT]                     INT           NULL,
    [L_LATERALITY]                VARCHAR (5)   NULL,
    [L_LATERALITY2]               VARCHAR (5)   NULL,
    [L_LATERALITY3]               VARCHAR (5)   NULL,
    [L_FINDINGS]                  TEXT          NULL,
    [L_SPECIMENS]                 TEXT          NULL,
    [L_DRAINS]                    TEXT          NULL,
    [L_POST_OP]                   TEXT          NULL,
    [L_COMMENTS]                  TEXT          NULL,
    [ROBOTIC_ASSISTED1]           BIT           CONSTRAINT [DF_tblSURGERY_HEAD_NECK_ROBOTIC_ASSISTED1] DEFAULT ((0)) NOT NULL,
    [ROBOTIC_ASSISTED2]           BIT           CONSTRAINT [DF_tblSURGERY_HEAD_NECK_ROBOTIC_ASSISTED2] DEFAULT ((0)) NOT NULL,
    [ROBOTIC_ASSISTED3]           BIT           CONSTRAINT [DF_tblSURGERY_HEAD_NECK_ROBOTIC_ASSISTED3] DEFAULT ((0)) NOT NULL,
    [UNPLANNED_RETURN_TO_THEATRE] VARCHAR (1)   NULL,
    CONSTRAINT [PK_tblSURGERY_HEAD_NECK] PRIMARY KEY CLUSTERED ([HN_SUR_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblSURGERY_HEAD_NECK]
    ON [dbo].[tblSURGERY_HEAD_NECK]([SURGERY_ID] ASC);


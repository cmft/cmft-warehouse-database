﻿CREATE TABLE [dbo].[newNBOCAP_CHEMORADIOTHERAPY_NO_SURGERY](
	[CARE_ID] [int] NULL,
	[EVENT_ID] [int] NULL,
	[EVENT_DATE] [datetime] NULL,
	[EVENT_TYPE] [varchar](10) NULL,
	[Username] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ltblUROLOGY_SURGICAL_TECHNIQUE_MIN_INV] (
    [TechniqueID]   INT           NOT NULL,
    [TechniqueDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblUROLOGY_SURGICAL_TECHNIQUE_MIN_INV] PRIMARY KEY CLUSTERED ([TechniqueID] ASC)
);


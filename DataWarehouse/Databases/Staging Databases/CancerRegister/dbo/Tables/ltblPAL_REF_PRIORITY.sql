﻿CREATE TABLE [dbo].[ltblPAL_REF_PRIORITY] (
    [Priority_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [Priority_Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblPAL_REF_PRIORITY] PRIMARY KEY CLUSTERED ([Priority_ID] ASC)
);


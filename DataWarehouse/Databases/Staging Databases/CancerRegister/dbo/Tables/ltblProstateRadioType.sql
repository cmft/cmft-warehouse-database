﻿CREATE TABLE [dbo].[ltblProstateRadioType] (
    [RadioTypeID]   INT           NOT NULL,
    [RadioTypeCode] INT           NOT NULL,
    [RadioTypeDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateRadioType] PRIMARY KEY CLUSTERED ([RadioTypeID] ASC)
);


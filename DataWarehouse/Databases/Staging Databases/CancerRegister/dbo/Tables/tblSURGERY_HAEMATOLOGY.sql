﻿CREATE TABLE [dbo].[tblSURGERY_HAEMATOLOGY] (
    [HAEM_SURG_ID]    INT           IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]      INT           NOT NULL,
    [L_ASA]           INT           NULL,
    [L_NOT_OP]        VARCHAR (3)   NULL,
    [L_NO_OP_REASON]  INT           NULL,
    [L_START_TIME]    CHAR (5)      NULL,
    [L_END_TIME]      CHAR (5)      NULL,
    [L_INDICATION]    VARCHAR (255) NULL,
    [L_INCISION]      VARCHAR (255) NULL,
    [L_CLOSURE]       VARCHAR (255) NULL,
    [L_GVHD]          VARCHAR (3)   NULL,
    [L_GVHD_SEVERITY] INT           NULL,
    [L_FINDINGS]      TEXT          NULL,
    [L_SPECIMENS]     TEXT          NULL,
    [L_DRAINS]        TEXT          NULL,
    [L_POST_OP]       TEXT          NULL,
    [L_COMMENTS]      TEXT          NULL,
    CONSTRAINT [PK_tblSURGERY_HAEMATOLOGY] PRIMARY KEY CLUSTERED ([HAEM_SURG_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblSURGERY_HAEMATOLOGY]
    ON [dbo].[tblSURGERY_HAEMATOLOGY]([SURGERY_ID] ASC);


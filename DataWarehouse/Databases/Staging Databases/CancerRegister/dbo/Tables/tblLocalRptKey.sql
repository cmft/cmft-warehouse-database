﻿CREATE TABLE [dbo].[tblLocalRptKey](
	[ReportName] [varchar](1000) NOT NULL,
	[User] [varchar](200) NOT NULL,
	[Date] [datetime] NOT NULL,
	[HyperDel] [varchar](200) NULL,
	[HyperView] [varchar](200) NULL
) ON [PRIMARY]
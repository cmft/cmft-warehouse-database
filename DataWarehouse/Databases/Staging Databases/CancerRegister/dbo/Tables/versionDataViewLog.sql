﻿CREATE TABLE [dbo].[versionDataViewLog] (
    [ID]              INT          NOT NULL,
    [DataViewVersion] VARCHAR (15) NOT NULL,
    [SCRVersion]      VARCHAR (15) NOT NULL,
    CONSTRAINT [PK_versionDataViewLog] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[ltblUROLOGY_SURGICAL_HISTORY] (
    [SurgicalHistoryID]   INT           NOT NULL,
    [SurgicalHistoryDesc] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblUROLOGY_SURGICAL_HISTORY] PRIMARY KEY CLUSTERED ([SurgicalHistoryID] ASC)
);


﻿CREATE TABLE [dbo].[ltblPROCEDURES] (
    [PROC_CODE] VARCHAR (5)   NOT NULL,
    [PROC_DESC] VARCHAR (255) NULL,
    [PROC_V]    VARCHAR (5)   NULL,
    CONSTRAINT [PK_ltblPROCEDURES] PRIMARY KEY CLUSTERED ([PROC_CODE] ASC)
);


﻿CREATE TABLE [dbo].[ltblANTIPLATELETS] (
    [DRUG_ID]   INT          IDENTITY (1, 1) NOT NULL,
    [DRUG_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblANTIPLATELETS] PRIMARY KEY CLUSTERED ([DRUG_ID] ASC)
);


﻿CREATE TABLE [dbo].[tblTERTIARY_REFERRAL] (
    [TERT_ID]             INT            IDENTITY (1, 1) NOT NULL,
    [CARE_ID]             INT            NOT NULL,
    [REF_CLINICIAN_CODE]  VARCHAR (10)   NULL,
    [REF_CLINICIAN]       VARCHAR (50)   NULL,
    [REF_CONTACT_NAME]    VARCHAR (50)   NULL,
    [REF_CONTACT_PHONE]   VARCHAR (20)   NULL,
    [REF_CONTACT_EMAIL]   VARCHAR (50)   NULL,
    [USE_PATIENT_DETAILS] BIT            NULL,
    [LEAD_CONTACT]        VARCHAR (50)   NULL,
    [CONTACT_HOME_TEL]    VARCHAR (20)   NULL,
    [CONTACT_MOB_TEL]     VARCHAR (20)   NULL,
    [CONTACT_WORK_TEL]    VARCHAR (20)   NULL,
    [CONTACT_EMAIL]       VARCHAR (20)   NULL,
    [ON_PATHWAY]          VARCHAR (20)   NULL,
    [RTT_STATUS]          VARCHAR (200)  NULL,
    [ALLOCATED_BY]        VARCHAR (50)   NULL,
    [CLOCK_START_DATE]    DATETIME       NULL,
    [DATE_DECISION]       DATETIME       NULL,
    [REC_ORGANISATION]    VARCHAR (5)    NULL,
    [REC_CLINICIAN]       VARCHAR (50)   NULL,
    [SPECIALITY]          INT            NULL,
    [DATE_SENT]           DATETIME       NULL,
    [NOTES]               VARCHAR (8000) NULL,
    PRIMARY KEY CLUSTERED ([TERT_ID] ASC)
);


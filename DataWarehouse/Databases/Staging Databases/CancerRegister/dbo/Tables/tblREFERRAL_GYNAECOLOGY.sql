﻿CREATE TABLE [dbo].[tblREFERRAL_GYNAECOLOGY] (
    [REF_ID]                INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]               INT           NOT NULL,
    [N_G1_GYNAE_ONCOLOGY]   CHAR (1)      NULL,
    [N_G2_DIAGNOSTIC_ROUTE] INT           NULL,
    [R_FIGO]                VARCHAR (3)   NULL,
    [R_FIGO2]               VARCHAR (3)   NULL,
    [R_FIGO3]               VARCHAR (3)   NULL,
    [R_SECONDARY_DEPOSIT]   VARCHAR (255) NULL,
    [R_MET_LUNG]            INT           NULL,
    [R_MET_BONE]            INT           NULL,
    [R_MET_LIVER]           INT           NULL,
    [R_MET_BRAIN]           INT           NULL,
    [R_MET_OTHER]           INT           NULL,
    [R_OTHER_METS]          VARCHAR (255) NULL,
    [L_QUESTIONNAIRE]       VARCHAR (3)   NULL,
    [L_PRE_T_LETTER]        CHAR (3)      NULL,
    [L_PRE_N_LETTER]        CHAR (3)      NULL,
    [L_PATH_T_LETTER]       CHAR (3)      NULL,
    [L_PATH_N_LETTER]       CHAR (3)      NULL,
    [L_PRE_M_LETTER]        CHAR (3)      NULL,
    [L_PATH_M_LETTER]       CHAR (3)      NULL,
    [R_MET_LYMPH]           INT           NULL,
    [R_MET_ADRENAL]         INT           NULL,
    [R_MET_SKIN]            INT           NULL,
    [R_MET_UNKNOWN]         INT           NULL,
    [R_MET_BONE_MARROW]     INT           NULL,
    [R_MET_BONE_EX_MARROW]  INT           NULL,
    [NODAL_STATUS_ID]       INT           NULL,
    [R_FIGO4]               VARCHAR (5)   NULL,
    [R_FIGO_STAGINGTYPE]    VARCHAR (5)   NULL,
    CONSTRAINT [PK_tblREFERRAL_GYNAECOLOGY] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_GYNAECOLOGY]
    ON [dbo].[tblREFERRAL_GYNAECOLOGY]([CARE_ID] ASC);


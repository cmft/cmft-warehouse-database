﻿CREATE TABLE [dbo].[ltblBREAST_TREATMENT] (
    [TREAT_CODE] CHAR (1)      NOT NULL,
    [TREAT_DESC] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ltblBREAST_TREATMENT] PRIMARY KEY CLUSTERED ([TREAT_CODE] ASC)
);


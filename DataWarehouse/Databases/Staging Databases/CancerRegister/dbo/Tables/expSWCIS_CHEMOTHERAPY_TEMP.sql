﻿CREATE TABLE [dbo].[expSWCIS_CHEMOTHERAPY_TEMP] (
    [SWCIS_ID]          INT           IDENTITY (1, 1) NOT NULL,
    [CHEMO_ID]          INT           NULL,
    [NHSNumber]         VARCHAR (10)  NULL,
    [OrgCodeSubmitting] VARCHAR (5)   NULL,
    [CareSpellID]       VARCHAR (50)  NULL,
    [SiteCode]          VARCHAR (5)   NULL,
    [Consultant]        VARCHAR (8)   NULL,
    [Specialty]         INT           NULL,
    [DecisionTreatDate] DATETIME      NULL,
    [TherapyType]       VARCHAR (1)   NULL,
    [TreatmentIntent]   VARCHAR (1)   NULL,
    [RegimenAcronym]    VARCHAR (255) NULL,
    [StartDate]         DATETIME      NULL,
    [Concurrent]        VARCHAR (3)   NULL,
    [Concomitant]       VARCHAR (3)   NULL,
    [Cycles]            VARCHAR (10)  NULL,
    [ResponseCode]      INT           NULL,
    [Response]          VARCHAR (50)  NULL,
    [Username]          VARCHAR (50)  NULL,
    CONSTRAINT [PK_expSWCIS_CHEMOTHERAPY_TEMP] PRIMARY KEY CLUSTERED ([SWCIS_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expSWCIS_CHEMOTHERAPY_TEMP]
    ON [dbo].[expSWCIS_CHEMOTHERAPY_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[ltblMARGIN_ADEQUACY] (
    [MARGIN_CODE] INT          NOT NULL,
    [MARGIN_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblMAGIN_ADEQUACY] PRIMARY KEY CLUSTERED ([MARGIN_CODE] ASC)
);


﻿CREATE TABLE [dbo].[ltblProstateRadioIntent] (
    [RadioIntentID]   INT           NOT NULL,
    [RadioIntentCode] INT           NOT NULL,
    [RadioIntentDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateRadioIntent] PRIMARY KEY CLUSTERED ([RadioIntentID] ASC)
);


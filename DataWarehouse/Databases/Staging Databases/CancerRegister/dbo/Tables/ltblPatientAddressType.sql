﻿CREATE TABLE [dbo].[ltblPatientAddressType] (
    [TypeID] INT           NOT NULL,
    [Type]   VARCHAR (150) NULL,
    CONSTRAINT [PK_ltblPatientAddressType] PRIMARY KEY CLUSTERED ([TypeID] ASC)
);


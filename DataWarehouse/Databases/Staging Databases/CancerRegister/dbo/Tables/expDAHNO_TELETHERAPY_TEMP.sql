﻿CREATE TABLE [dbo].[expDAHNO_TELETHERAPY_TEMP] (
    [DAHNO_ID]                INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                 INT          NULL,
    [PATIENT_ID]              INT          NULL,
    [NHSNumber]               VARCHAR (10) NULL,
    [SubmittingOrg]           VARCHAR (5)  NULL,
    [ContactOrg]              VARCHAR (5)  NULL,
    [PatientID]               VARCHAR (10) NULL,
    [DateDiagnosis]           DATETIME     NULL,
    [DateCarePlanAgreed]      DATETIME     NULL,
    [DateDecisionTeletherapy] DATETIME     NULL,
    [TreatmentIntent]         VARCHAR (1)  NULL,
    [TreatmentTo]             VARCHAR (2)  NULL,
    [AnatomicalSite]          VARCHAR (5)  NULL,
    [DateStartTeletherapy]    DATETIME     NULL,
    [Username]                VARCHAR (50) NULL,
    CONSTRAINT [PK_expDAHNO_TELETHERAPY_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_TELETHERAPY_TEMP]
    ON [dbo].[expDAHNO_TELETHERAPY_TEMP]([Username] ASC);


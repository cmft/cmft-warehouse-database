﻿CREATE TABLE [dbo].[ltblAHP_NAME] (
    [ID]                    INT          IDENTITY (1, 1) NOT NULL,
    [AHP_NAME]              VARCHAR (50) NOT NULL,
    [AHP_ROLE_ID]           INT          NULL,
    [ACTIVE]                BIT          CONSTRAINT [DF_ltblAHP_NAME_Inactive] DEFAULT ((1)) NOT NULL,
    [EDITABLE]              BIT          CONSTRAINT [DF_ltblAHP_NAME_EDITABLE] DEFAULT ((1)) NOT NULL,
    [DEFAULT_OTHER_ROLE_ID] INT          NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


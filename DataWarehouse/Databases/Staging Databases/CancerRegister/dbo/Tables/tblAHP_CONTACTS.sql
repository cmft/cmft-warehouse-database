﻿CREATE TABLE [dbo].[tblAHP_CONTACTS] (
    [AHP_ID]                       INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                      INT           NOT NULL,
    [TEMP_ID]                      VARCHAR (255) NULL,
    [FIRST_CONTACT]                INT           NULL,
    [L_REF_DATE]                   SMALLDATETIME NULL,
    [L_SEEN_DATE]                  SMALLDATETIME NULL,
    [L_AHP]                        INT           NULL,
    [L_AHP_ID]                     INT           NULL,
    [L_AHP_ROLE]                   INT           NULL,
    [L_AHP_NAME]                   VARCHAR (100) NULL,
    [L_CONTACT_PAT]                INT           CONSTRAINT [DF_tblAHP_CONTACTS_L_CONTACT_PAT] DEFAULT ((0)) NULL,
    [L_CONTACT_REL]                INT           CONSTRAINT [DF_tblAHP_CONTACTS_L_CONTACT_REL] DEFAULT ((0)) NULL,
    [L_CONTACT_CARER]              INT           CONSTRAINT [DF_tblAHP_CONTACTS_L_CONTACT_CARER] DEFAULT ((0)) NULL,
    [L_CONTACT_PROF]               INT           CONSTRAINT [DF_tblAHP_CONTACTS_L_CONTACT_PROF] DEFAULT ((0)) NULL,
    [L_HCP_ROLE]                   INT           NULL,
    [L_HCP_NAME]                   VARCHAR (255) NULL,
    [L_CONTACT_TYPE]               INT           NULL,
    [L_TOTAL_TIME]                 INT           NULL,
    [L_CONTACT_PLACE]              INT           NULL,
    [L_PRIVACY]                    INT           NULL,
    [L_ADVICE]                     TEXT          NULL,
    [L_ACTIVITY]                   TEXT          NULL,
    [L_DENTAL_TREATMENT]           INT           NULL,
    [R_SWALLOW]                    VARCHAR (3)   NULL,
    [R_DIET]                       INT           NULL,
    [R_DIET3MONTH]                 INT           NULL,
    [R_DIET12MONTH]                INT           NULL,
    [R_SVR]                        VARCHAR (3)   NULL,
    [R_WHO_CONTACT]                VARCHAR (1)   NULL,
    [R_CONTACT_PURPOSE]            VARCHAR (2)   NULL,
    [R_PT_STATUS]                  INT           NULL,
    [R_COMM]                       VARCHAR (4)   NULL,
    [R_COMM_PRIMARY]               VARCHAR (4)   NULL,
    [R_COMM_OTHER]                 VARCHAR (4)   NULL,
    [R_VOICING]                    INT           NULL,
    [R_REMOVAL_REASON]             INT           NULL,
    [R_HEIGHT]                     REAL          NULL,
    [R_WEIGHT]                     REAL          NULL,
    [N_HN_NUTRITION_ORGANISATION]  VARCHAR (5)   NULL,
    [N_HN15_NUTRITION]             VARCHAR (1)   NULL,
    [N_HN_DATE_SUPPORT_INSTIGATED] SMALLDATETIME NULL,
    [N_HN16_NUTRITION_TYPE]        VARCHAR (1)   NULL,
    [N_HN17_NUTRITION_PROCEDURE]   INT           NULL,
    [N_HN_NUTRITION_PROC_DATE]     SMALLDATETIME NULL,
    [N_HN18_NUTRITION_COMP]        VARCHAR (5)   NULL,
    [R_SUPPORT_REMAIN]             VARCHAR (1)   NULL,
    [N_HN_DATE_SUPPORT_WITHDRAWN]  SMALLDATETIME NULL,
    [R_EST_WEIGHT]                 REAL          NULL,
    [R_SALT_CONTACT_DURATION]      INT           NULL,
    [L_NOTES]                      TEXT          NULL,
    [OTHER_ROLE_ID]                INT           NULL,
    CONSTRAINT [PK_tblAHP_CONTACTS] PRIMARY KEY CLUSTERED ([AHP_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblAHP_CONTACTS]
    ON [dbo].[tblAHP_CONTACTS]([CARE_ID] ASC);


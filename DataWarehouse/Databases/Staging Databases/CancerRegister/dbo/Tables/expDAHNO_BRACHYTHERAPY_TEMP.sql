﻿CREATE TABLE [dbo].[expDAHNO_BRACHYTHERAPY_TEMP] (
    [DAHNO_ID]                  INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]                   INT          NULL,
    [PATIENT_ID]                INT          NULL,
    [NHSNumber]                 VARCHAR (10) NULL,
    [SubmittingOrg]             VARCHAR (5)  NULL,
    [ContactOrg]                VARCHAR (5)  NULL,
    [PatientID]                 VARCHAR (30) NULL,
    [DateDiagnosis]             DATETIME     NULL,
    [DateCarePlanAgreed]        DATETIME     NULL,
    [DateDecisionBrachytherapy] DATETIME     NULL,
    [TreatmentIntent]           VARCHAR (1)  NULL,
    [TreatmentTo]               VARCHAR (2)  NULL,
    [DateStartBrachytherapy]    DATETIME     NULL,
    [Username]                  VARCHAR (50) NULL,
    [RadiotherapySite]          VARCHAR (5)  NULL,
    CONSTRAINT [PK_expDAHNO_BRACHYTHERAPY_TEMP] PRIMARY KEY CLUSTERED ([DAHNO_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expDAHNO_BRACHYTHERAPY_TEMP]
    ON [dbo].[expDAHNO_BRACHYTHERAPY_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[expPALCCS3ADMITDATAEXISTING](
	[PALLIATIVE_ID] [int] NULL,
	[L_CONTACT_DATE] [smalldatetime] NULL,
	[L_CONTACT_PAT] [int] NULL,
	[L_CONTACT_REL] [int] NULL,
	[L_CONTACT_CARER] [int] NULL,
	[L_CONTACT_PROF] [int] NULL,
	[L_CONTACT_TYPE] [int] NULL,
	[L_LOCATION] [int] NULL,
	[L_SEEN_BY] [int] NULL,
	[L_DATE_DISCHARGE] [smalldatetime] NULL,
	[L_DISCH_DEST] [int] NULL
) ON [PRIMARY]
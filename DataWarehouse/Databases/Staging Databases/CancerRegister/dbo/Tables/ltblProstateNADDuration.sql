﻿CREATE TABLE [dbo].[ltblProstateNADDuration] (
    [NADDurationID]   INT           NOT NULL,
    [NADDurationCode] INT           NOT NULL,
    [NADDurationDesc] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ltblProstateNADDuration] PRIMARY KEY CLUSTERED ([NADDurationID] ASC)
);


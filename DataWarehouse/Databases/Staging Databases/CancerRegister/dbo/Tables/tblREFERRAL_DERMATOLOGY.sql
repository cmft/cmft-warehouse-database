﻿CREATE TABLE [dbo].[tblREFERRAL_DERMATOLOGY] (
    [REF_ID]               INT           IDENTITY (1, 1) NOT NULL,
    [CARE_ID]              INT           NOT NULL,
    [N_SK2_BODY_SITE]      VARCHAR (2)   NULL,
    [N_SK3_DIAMETER]       REAL          NULL,
    [N_SK4_BASAL_CELL]     CHAR (1)      NULL,
    [N_SK5_MARGIN]         REAL          NULL,
    [N_SK7_TCELL_AREA]     CHAR (2)      NULL,
    [N_SK8_LYMPHOMA]       CHAR (1)      NULL,
    [N_SK9_DISTRIBUTION]   CHAR (1)      NULL,
    [N_SK10_TCELL_VARIANT] VARCHAR (10)  NULL,
    [R_MET_LUNG]           INT           NULL,
    [R_MET_BONE]           INT           NULL,
    [R_MET_LIVER]          INT           NULL,
    [R_MET_BRAIN]          INT           NULL,
    [R_MET_OTHER]          INT           NULL,
    [R_OTHER_METS]         VARCHAR (255) NULL,
    [L_THICKNESS]          VARCHAR (20)  NULL,
    [L_CLARK_LEVEL]        INT           NULL,
    [L_AJCC]               INT           NULL,
    [L_ULCERATION]         VARCHAR (1)   NULL,
    [L_MITOTIC]            REAL          NULL,
    [L_PRE_T_LETTER]       CHAR (1)      NULL,
    [L_PRE_N_LETTER]       CHAR (1)      NULL,
    [L_PRE_M_LETTER]       CHAR (1)      NULL,
    [L_PATH_T_LETTER]      CHAR (1)      NULL,
    [L_PATH_N_LETTER]      CHAR (1)      NULL,
    [L_PATH_M_LETTER]      CHAR (1)      NULL,
    [R_MET_LYMPH]          INT           NULL,
    [R_MET_ADRENAL]        INT           NULL,
    [R_MET_SKIN]           INT           NULL,
    [R_MET_UNKNOWN]        INT           NULL,
    [R_MET_BONE_MARROW]    INT           NULL,
    [R_MET_BONE_EX_MARROW] INT           NULL,
    CONSTRAINT [PK_tblREFERRAL_DERMATOLOGY] PRIMARY KEY CLUSTERED ([REF_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblREFERRAL_DERMATOLOGY]
    ON [dbo].[tblREFERRAL_DERMATOLOGY]([CARE_ID] ASC);


﻿CREATE TABLE [dbo].[ltblUROLOGY_TESTIS_INVASION] (
    [INV_CODE] INT          NOT NULL,
    [INV_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblUROLOGY_TESTIS_INVASION] PRIMARY KEY CLUSTERED ([INV_CODE] ASC)
);


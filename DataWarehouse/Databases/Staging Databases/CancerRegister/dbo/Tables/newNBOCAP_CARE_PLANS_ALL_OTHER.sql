﻿CREATE TABLE [dbo].[newNBOCAP_CARE_PLANS_ALL_OTHER](
	[CARE_ID] [int] NULL,
	[EVENT_ID] [int] NULL,
	[EVENT_DATE] [smalldatetime] NULL,
	[Username] [varchar](50) NULL
) ON [PRIMARY]
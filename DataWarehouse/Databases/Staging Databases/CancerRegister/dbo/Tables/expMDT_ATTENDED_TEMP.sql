﻿CREATE TABLE [dbo].[expMDT_ATTENDED_TEMP] (
    [ATTENDANCE_ID]  INT           IDENTITY (1, 1) NOT NULL,
    [Name]           VARCHAR (100) NULL,
    [JobTitle]       VARCHAR (50)  NULL,
    [CoreMember]     VARCHAR (5)   NULL,
    [MDTDate]        DATETIME      NULL,
    [Present]        VARCHAR (5)   NULL,
    [MDTMeetingDesc] VARCHAR (200) NULL,
    [LeadClinician]  VARCHAR (5)   NULL,
    [DisplayOrder]   INT           NULL,
    [Comments]       VARCHAR (500) NULL,
    [Username]       VARCHAR (50)  NULL,
    CONSTRAINT [PK_expMDT_ATTENDED_TEMP] PRIMARY KEY CLUSTERED ([ATTENDANCE_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expMDT_ATTENDED_TEMP]
    ON [dbo].[expMDT_ATTENDED_TEMP]([Username] ASC);


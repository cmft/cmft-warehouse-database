﻿CREATE TABLE [dbo].[tmpRPT_NOGCA_PATHOLOGY] (
    [NOGCAPathologyID]              INT           IDENTITY (1, 1) NOT NULL,
    [CareID]                        INT           NULL,
    [NHSNumber]                     VARCHAR (50)  NULL,
    [DateOfBirth]                   SMALLDATETIME NULL,
    [HospitalNumber]                VARCHAR (50)  NULL,
    [TumourLength]                  VARCHAR (3)   NULL,
    [PathologySite]                 VARCHAR (2)   NULL,
    [PathHistology]                 VARCHAR (7)   NULL,
    [HistNeoadjuvant]               VARCHAR (1)   NULL,
    [ProximalMarginInvolved]        VARCHAR (1)   NULL,
    [DistalMarginInvolved]          VARCHAR (1)   NULL,
    [CircumferentialMarginInvolved] VARCHAR (2)   NULL,
    [NodesExaminedNumber]           INT           NULL,
    [NodesPositiveNumber]           INT           NULL,
    [Path_TNMfilter]                INT           NULL,
    [PostTreatment_T]               VARCHAR (3)   NULL,
    [PostTreatment_N]               VARCHAR (3)   NULL,
    [PostTreatment_M]               VARCHAR (3)   NULL,
    [Username]                      VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpRPT_NOGCA_PATHOLOGY] PRIMARY KEY CLUSTERED ([NOGCAPathologyID] ASC)
);


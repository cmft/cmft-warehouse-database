﻿CREATE TABLE [dbo].[ltblMM_DEGREE] (
    [DEGREE_CODE] INT          NOT NULL,
    [DEGREE_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblMM_DEGREE] PRIMARY KEY CLUSTERED ([DEGREE_CODE] ASC)
);


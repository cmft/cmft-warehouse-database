﻿CREATE TABLE [dbo].[ltblSTENT_PLACEMENT] (
    [PLACE_CODE] INT          NOT NULL,
    [PLACE_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblSTENT_PLACEMENT] PRIMARY KEY CLUSTERED ([PLACE_CODE] ASC)
);


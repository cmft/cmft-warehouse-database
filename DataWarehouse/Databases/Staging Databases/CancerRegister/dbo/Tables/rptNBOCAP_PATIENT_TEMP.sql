﻿CREATE TABLE [dbo].[rptNBOCAP_PATIENT_TEMP] (
    [NBOCAP_ID]          INT          IDENTITY (1, 1) NOT NULL,
    [CARE_ID]            INT          NULL,
    [PATIENT_ID]         INT          NULL,
    [RecordType]         INT          NOT NULL,
    [NSTS_Status]        INT          NULL,
    [NHSNumber]          VARCHAR (10) NOT NULL,
    [OriginatingOrgCode] VARCHAR (10) NOT NULL,
    [HospitalNumber]     VARCHAR (10) NULL,
    [Forename]           VARCHAR (22) NULL,
    [Surname]            VARCHAR (30) NULL,
    [Postcode]           VARCHAR (10) NULL,
    [DateBirth]          DATETIME     NULL,
    [Sex]                INT          NULL,
    [Height]             REAL         NULL,
    [Weight]             REAL         NULL,
    [ConsultantCode]     VARCHAR (8)  NULL,
    [DateDeath]          DATETIME     NULL,
    [CauseDeath]         INT          NULL,
    [PostMortem]         VARCHAR (1)  NULL,
    [Username]           VARCHAR (50) NULL,
    [Err1]               INT          NULL,
    [Err2]               INT          NULL,
    [Err3]               INT          NULL,
    [Err4]               INT          NULL,
    [Err5]               INT          NULL,
    [Err6]               INT          NULL,
    [Err7]               INT          NULL,
    [Err8]               INT          NULL,
    [Err9]               INT          NULL,
    [Err10]              INT          NULL,
    [Err11]              INT          NULL,
    [Err12]              INT          NULL,
    CONSTRAINT [PK_rptNBOCAP_PATIENT_TEMP] PRIMARY KEY CLUSTERED ([NBOCAP_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_rptNBOCAP_PATIENT_TEMP]
    ON [dbo].[rptNBOCAP_PATIENT_TEMP]([Username] ASC);


﻿CREATE TABLE [dbo].[tblSURGERY_BRAIN] (
    [BRAIN_SURG_ID]            INT           IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]               INT           NOT NULL,
    [L_ASA]                    INT           NULL,
    [L_NOT_OP]                 VARCHAR (3)   NULL,
    [L_NO_OP_REASON]           INT           NULL,
    [L_START_TIME]             CHAR (5)      NULL,
    [L_END_TIME]               CHAR (5)      NULL,
    [L_INDICATION]             VARCHAR (255) NULL,
    [L_INCISION]               VARCHAR (255) NULL,
    [L_CLOSURE]                VARCHAR (255) NULL,
    [L_PERFORMANCE]            INT           NULL,
    [L_SMEAR]                  VARCHAR (3)   NULL,
    [L_SMEAR_DETAILS]          VARCHAR (255) NULL,
    [L_SMEAR_REPORTED]         VARCHAR (8)   NULL,
    [L_IMPLANT]                VARCHAR (3)   NULL,
    [L_IMPLANT_DETAILS]        VARCHAR (255) NULL,
    [L_PLAN]                   VARCHAR (255) NULL,
    [L_FINDINGS]               TEXT          NULL,
    [L_SPECIMENS]              TEXT          NULL,
    [L_DRAINS]                 TEXT          NULL,
    [L_POST_OP]                TEXT          NULL,
    [L_COMMENTS]               TEXT          NULL,
    [TUMOUR_LOCATION_SURGICAL] INT           NULL,
    [EXCISION_TYPE_SURGICAL]   INT           NULL,
    CONSTRAINT [PK_tblSURGERY_BRAIN] PRIMARY KEY CLUSTERED ([BRAIN_SURG_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblSURGERY_BRAIN]
    ON [dbo].[tblSURGERY_BRAIN]([SURGERY_ID] ASC);


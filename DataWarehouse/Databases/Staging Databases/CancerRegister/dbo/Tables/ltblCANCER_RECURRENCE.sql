﻿CREATE TABLE [dbo].[ltblCANCER_RECURRENCE] (
    [RECURRENCE_ID]   VARCHAR (2)  NOT NULL,
    [RECURRENCE_DESC] VARCHAR (50) NULL,
    [IS_DELETED]      BIT          CONSTRAINT [DF_ltblCANCER_RECURRENCE_IS_DELETED] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ltblCANCER_RECURRENCE] PRIMARY KEY CLUSTERED ([RECURRENCE_ID] ASC)
);


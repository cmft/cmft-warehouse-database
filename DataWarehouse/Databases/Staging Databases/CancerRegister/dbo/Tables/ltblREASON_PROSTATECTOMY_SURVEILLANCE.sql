﻿CREATE TABLE [dbo].[ltblREASON_PROSTATECTOMY_SURVEILLANCE] (
    [PROST_REASON_ID] INT          IDENTITY (1, 1) NOT NULL,
    [REASON]          VARCHAR (50) NULL,
    CONSTRAINT [PK_ltblREASON_PROSTATECTOMY_SURVEILLANCE] PRIMARY KEY CLUSTERED ([PROST_REASON_ID] ASC)
);


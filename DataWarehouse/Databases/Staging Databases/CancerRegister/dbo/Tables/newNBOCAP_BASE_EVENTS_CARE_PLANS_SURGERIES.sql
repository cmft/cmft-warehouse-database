﻿CREATE TABLE [dbo].[newNBOCAP_BASE_EVENTS_CARE_PLANS_SURGERIES](
	[CARE_ID] [int] NULL,
	[EVENT_DATE] [smalldatetime] NULL,
	[EVENT_ID] [int] NULL,
	[EVENT_TYPE] [varchar](10) NULL,
	[Username] [varchar](50) NULL
) ON [PRIMARY]
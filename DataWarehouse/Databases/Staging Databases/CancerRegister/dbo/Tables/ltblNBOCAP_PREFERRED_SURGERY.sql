﻿CREATE TABLE [dbo].[ltblNBOCAP_PREFERRED_SURGERY] (
    [NBOCAP_ID]        INT          NOT NULL,
    [NBOCAP_ORDER]     INT          NOT NULL,
    [NBOCAP_DIAGNOSIS] VARCHAR (10) NULL,
    [NBOCAP_PROCEDURE] VARCHAR (10) NULL,
    [PROC_ID]          INT          NULL,
    CONSTRAINT [PK_ltblNBOCAP_PREFERRED_SURGERY] PRIMARY KEY CLUSTERED ([NBOCAP_ID] ASC) WITH (FILLFACTOR = 90)
);


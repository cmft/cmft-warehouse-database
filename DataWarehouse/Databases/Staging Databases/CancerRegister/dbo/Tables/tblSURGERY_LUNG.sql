﻿CREATE TABLE [dbo].[tblSURGERY_LUNG] (
    [LUNG_SUR_ID]   INT           IDENTITY (1, 1) NOT NULL,
    [SURGERY_ID]    INT           NOT NULL,
    [L_CANCELLED]   INT           NULL,
    [L_CANCEL_OP]   VARCHAR (255) NULL,
    [L_ASA]         INT           NULL,
    [L_START_TIME]  CHAR (5)      NULL,
    [L_END_TIME]    CHAR (5)      NULL,
    [L_INDICATION]  VARCHAR (255) NULL,
    [L_INCISION]    VARCHAR (255) NULL,
    [L_CLOSURE]     VARCHAR (255) NULL,
    [L_LATERALITY]  VARCHAR (5)   NULL,
    [L_LATERALITY2] VARCHAR (5)   NULL,
    [L_LATERALITY3] VARCHAR (5)   NULL,
    [L_FINDINGS]    TEXT          NULL,
    [L_SPECIMENS]   TEXT          NULL,
    [L_DRAINS]      TEXT          NULL,
    [L_POST_OP]     TEXT          NULL,
    CONSTRAINT [PK_tblSURGERY_LUNG] PRIMARY KEY CLUSTERED ([LUNG_SUR_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_tblSURGERY_LUNG]
    ON [dbo].[tblSURGERY_LUNG]([SURGERY_ID] ASC);


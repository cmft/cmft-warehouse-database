﻿CREATE TABLE [dbo].[ltblBRAIN_PHARYNGEAL] (
    [SCORE_CODE] INT          NOT NULL,
    [SCORE_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblBRAIN_PHARYNGEAL] PRIMARY KEY CLUSTERED ([SCORE_CODE] ASC)
);


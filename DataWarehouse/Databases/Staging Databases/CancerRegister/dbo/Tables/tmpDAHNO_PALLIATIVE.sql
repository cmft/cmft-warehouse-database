﻿CREATE TABLE [dbo].[tmpDAHNO_PALLIATIVE] (
    [DAHNOPalliativeID]   INT           IDENTITY (1, 1) NOT NULL,
    [NHSNumber]           VARCHAR (10)  NULL,
    [DateOfBirth]         SMALLDATETIME NULL,
    [PrimarySite]         VARCHAR (6)   NULL,
    [PalliativeOrg]       VARCHAR (5)   NULL,
    [PalliativeStartDate] SMALLDATETIME NULL,
    [Username]            VARCHAR (50)  NULL,
    CONSTRAINT [PK_tmpDAHNO_Palliative] PRIMARY KEY CLUSTERED ([DAHNOPalliativeID] ASC)
);


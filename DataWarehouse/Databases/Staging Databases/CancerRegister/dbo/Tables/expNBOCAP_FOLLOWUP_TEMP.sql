﻿CREATE TABLE [dbo].[expNBOCAP_FOLLOWUP_TEMP] (
    [NBOCAP_ID]          INT          IDENTITY (1, 1) NOT NULL,
    [FOLLOWUP_ID]        INT          NULL,
    [CARE_ID]            INT          NULL,
    [PATIENT_ID]         INT          NULL,
    [RecordType]         INT          NOT NULL,
    [NHSNumber]          VARCHAR (10) NOT NULL,
    [CareSpellNumber]    VARCHAR (22) NOT NULL,
    [FollowupID]         VARCHAR (38) NULL,
    [OriginatingOrgCode] VARCHAR (10) NOT NULL,
    [FollowupOrgCode]    VARCHAR (5)  NULL,
    [DateFollowup]       DATETIME     NULL,
    [ModeFollowup]       INT          NULL,
    [TumourStatus]       VARCHAR (1)  NULL,
    [LocalRecurrence]    INT          NULL,
    [WoundRecurrence]    VARCHAR (1)  NULL,
    [PortSiteRecurrence] VARCHAR (1)  NULL,
    [MetastaticStatus]   VARCHAR (1)  NULL,
    [SiteDistantSpread]  INT          NULL,
    [TreatmentMorbidity] INT          NULL,
    [Username]           VARCHAR (50) NULL,
    CONSTRAINT [PK_expNBOCAP_FOLLOWUP_TEMP] PRIMARY KEY CLUSTERED ([NBOCAP_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_expNBOCAP_FOLLOWUP_TEMP]
    ON [dbo].[expNBOCAP_FOLLOWUP_TEMP]([Username] ASC);


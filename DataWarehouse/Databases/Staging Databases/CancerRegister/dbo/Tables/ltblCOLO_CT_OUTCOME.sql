﻿CREATE TABLE [dbo].[ltblCOLO_CT_OUTCOME] (
    [OUTCOME_CODE] INT          NOT NULL,
    [OUTCOME_DESC] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblCOLO_CT_OUTCOME] PRIMARY KEY CLUSTERED ([OUTCOME_CODE] ASC)
);


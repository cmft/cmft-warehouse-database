﻿CREATE TABLE [dbo].[ltblMarginSite] (
    [MarginSiteID]   INT          NOT NULL,
    [MarginSiteCode] INT          NOT NULL,
    [MarginSiteDesc] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ltblMarginSite] PRIMARY KEY CLUSTERED ([MarginSiteID] ASC)
);


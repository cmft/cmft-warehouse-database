﻿CREATE TABLE [dbo].[Triage_ExtraFields](
	[tri_EF_extrafieldID] [int] IDENTITY(0,1) NOT NULL,
	[tri_EF_FieldType] [smallint] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__FieldType]  DEFAULT (0),
	[tri_EF_FieldID] [int] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__FieldID]  DEFAULT (0),
	[tri_EF_atdid] [int] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__atdid]  DEFAULT (0),
	[tri_EF_Value] [varchar](4000) NOT NULL CONSTRAINT [DF__Triage_ExtraFields__Value]  DEFAULT (''),
	[tri_EF_DataCategory] [smallint] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__DataCategory]  DEFAULT (0),
	[tri_EF_RecordID] [int] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__RecordID]  DEFAULT (0),
	[tri_EF_FieldCodeValues] [varchar](200) NOT NULL CONSTRAINT [DF_Triage_ExtraFields_tri_EF_FieldCodeValues]  DEFAULT (''),
	[Tri_EF_ComputationState] [tinyint] NOT NULL CONSTRAINT [DF_Triage_ExtraFields_tri_EF_ComputationState]  DEFAULT (0),
	[Tri_EF_depId] [smallint] NOT NULL CONSTRAINT [DF_Triage_ExtraFields_Tri_EF_depId]  DEFAULT ((0)),
	[Tri_EF_CPMessagesIDs] [varchar](200) NOT NULL CONSTRAINT [DF_Triage_ExtraFields_Tri_EF_CPMessagesIDs]  DEFAULT (''),
	[tri_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__tri_EF_CreatedBy]  DEFAULT ((0)),
	[tri_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__tri_EF_Created]  DEFAULT (getdate()),
	[tri_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__tri_EF_updatedby]  DEFAULT ((0)),
	[tri_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Triage_ExtraFields__tri_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
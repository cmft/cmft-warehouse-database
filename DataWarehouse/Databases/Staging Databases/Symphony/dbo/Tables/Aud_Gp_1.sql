﻿CREATE TABLE [dbo].[Aud_Gp](
	[gp_identity] [int] IDENTITY(0,1) NOT NULL,
	[gp_code] [varchar](14) NOT NULL CONSTRAINT [DF__Aud_Gp__gp_code]  DEFAULT ('G9999998'),
	[gp_practise] [varchar](14) NOT NULL CONSTRAINT [DF__Aud_Gp__gp_practise]  DEFAULT ('V81999'),
	[gp_praddid] [int] NOT NULL CONSTRAINT [DF__Aud_Gp__gp_praddid]  DEFAULT (0),
	[gp_startdate] [datetime] NOT NULL CONSTRAINT [DF__Aud_Gp__gp_startdate]  DEFAULT ('2200/01/01'),
	[gp_enddate] [datetime] NOT NULL CONSTRAINT [DF__Aud_Gp__gp_enddate]  DEFAULT ('2200/01/01'),
	[gp_surname] [varchar](35) NOT NULL CONSTRAINT [DF__Aud_Gp__gp_surname]  DEFAULT (''),
	[gp_initials] [varchar](30) NOT NULL CONSTRAINT [DF__Aud_Gp__gp_initials]  DEFAULT (''),
	[gp_title] [int] NOT NULL CONSTRAINT [DF__Aud_GP__gp_title]  DEFAULT (0),
	[gp_qualifications] [int] NOT NULL CONSTRAINT [DF__Aud_GP__gp_qualifications]  DEFAULT (0),
	[gp_telid] [int] NOT NULL CONSTRAINT [DF__Aud_GP__gp_telid]  DEFAULT (0),
	[gp_status] [int] NOT NULL CONSTRAINT [DF__Aud_Gp__gp_status]  DEFAULT (0),
	[gp_local] [varchar](14) NOT NULL CONSTRAINT [DF__Aud_Gp__gp_local]  DEFAULT (''),
	[gp_letterpref] [varchar](35) NULL,
	[gp_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Gp__gp_createdby]  DEFAULT (0),
	[gp_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Gp__gp_update]  DEFAULT (getdate()),
	[gp_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Gp__gp_deleted]  DEFAULT (0)
) ON [PRIMARY]
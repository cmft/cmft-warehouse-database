﻿CREATE TABLE [dbo].[Import_CMMCPMIExceptiON](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[District Number] [varchar](40) NULL DEFAULT (''),
	[NHS Number] [varchar](20) NULL DEFAULT (''),
	[Surname] [varchar](35) NULL DEFAULT (''),
	[Forename] [varchar](35) NULL DEFAULT (''),
	[DOB] [datetime] NULL DEFAULT ('1800-01-01')
) ON [PRIMARY]
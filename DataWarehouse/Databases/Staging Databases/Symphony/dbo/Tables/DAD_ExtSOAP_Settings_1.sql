﻿CREATE TABLE [dbo].[DAD_ExtSOAP_Settings](
	[sst_ID] [int] IDENTITY(0,1) NOT NULL,
	[sst_SOAPID] [int] NOT NULL CONSTRAINT [DF_DAD_ExtSOAP_Settings_sst_SOAPID]  DEFAULT (0),
	[sst_Name] [varchar](225) NOT NULL CONSTRAINT [DF_DAD_ExtSOAP_Settings_sst_Name]  DEFAULT (''),
	[sst_Value] [varchar](255) NOT NULL CONSTRAINT [DF_DAD_ExtSOAP_Settings_sst_Value]  DEFAULT (''),
	[sst_created] [datetime] NOT NULL CONSTRAINT [DF_DAD_ExtSOAP_Settings_sst_created]  DEFAULT (getdate()),
	[sst_createdBy] [int] NOT NULL CONSTRAINT [DF_DAD_ExtSOAP_Settings_sst_createdBy]  DEFAULT (0),
	[sst_Updated] [datetime] NOT NULL CONSTRAINT [DF_DAD_ExtSOAP_Settings_sst_Updated]  DEFAULT (getdate()),
	[sst_UpdatedBy] [int] NOT NULL CONSTRAINT [DF_DAD_ExtSOAP_Settings_sst_UpdatedBy]  DEFAULT (0),
	[sst_SOAPExtCategory] [smallint] NOT NULL CONSTRAINT [DF_DAD_ExtSOAP_Settings_sst_SOAPExtCategory]  DEFAULT (255)
) ON [PRIMARY]
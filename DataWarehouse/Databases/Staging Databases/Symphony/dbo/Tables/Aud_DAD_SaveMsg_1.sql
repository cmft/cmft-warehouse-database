﻿CREATE TABLE [dbo].[Aud_DAD_SaveMsg](
	[dsm_identity] [int] IDENTITY(0,1) NOT NULL,
	[dsm_id] [int] NOT NULL,
	[dsm_message] [varchar](4000) NOT NULL,
	[dsm_failed] [bit] NOT NULL,
	[dsm_createdby] [int] NOT NULL,
	[dsm_created] [datetime] NOT NULL
) ON [PRIMARY]
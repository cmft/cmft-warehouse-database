﻿CREATE TABLE [dbo].[Cat_Details](
	[ctd_id] [int] IDENTITY(0,1) NOT NULL,
	[ctd_catid] [int] NOT NULL CONSTRAINT [DF__cat_details__ctd_catid]  DEFAULT (0),
	[ctd_waitingfors] [varchar](2000) NOT NULL CONSTRAINT [DF__cat_details__ctd_waitingfors]  DEFAULT (''),
	[ctd_trackinggrids] [varchar](2500) NOT NULL CONSTRAINT [DF__cat_details__ctd_trackinggrids]  DEFAULT (''),
	[ctd_usedlookups] [nvarchar](3500) NOT NULL CONSTRAINT [DF__cat_details__ctd_usedlookups]  DEFAULT ('')
) ON [PRIMARY]
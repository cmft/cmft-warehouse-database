﻿CREATE TABLE [dbo].[CFG_DEPForms](
	[dpf_deprocedure] [smallint] NOT NULL CONSTRAINT [DF__CFG_DEPForms__dpf_deprocedure]  DEFAULT (0),
	[dpf_deform] [smallint] NOT NULL CONSTRAINT [DF__CFG_DEPForms__dpf_deform]  DEFAULT (0),
	[dpf_formindex] [smallint] NOT NULL CONSTRAINT [DF__CFG_DEPForms__dpf_formindex]  DEFAULT (0),
	[dpf_ispopup] [bit] NOT NULL CONSTRAINT [DF__CFG_DEPForms__dpf_ispopup]  DEFAULT (0),
	[dpf_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DEPForms__dpf_createdby]  DEFAULT (0),
	[dpf_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_DEPForms__dpf_update]  DEFAULT (getdate())
) ON [PRIMARY]
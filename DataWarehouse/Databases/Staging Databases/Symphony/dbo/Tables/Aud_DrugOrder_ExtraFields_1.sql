﻿CREATE TABLE [dbo].[Aud_DrugOrder_ExtraFields](
	[dgo_EF_ID] [int] IDENTITY(0,1) NOT NULL,
	[dgo_EF_extrafieldID] [int] NULL,
	[dgo_EF_FieldType] [smallint] NULL,
	[dgo_EF_FieldID] [int] NULL,
	[dgo_EF_atdid] [int] NULL,
	[dgo_EF_Value] [varchar](4000) NULL,
	[dgo_EF_DataCategory] [smallint] NULL,
	[dgo_EF_RecordID] [int] NULL,
	[dgo_EF_FieldCodeValues] [varchar](200) NULL,
	[dgo_EF_ComputationState] [tinyint] NULL,
	[dgo_EF_depId] [smallint] NULL,
	[dgo_EF_CPMessagesIDs] [varchar](200) NULL,
	[dgo_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Aud_DrugOrder_ExtraFields__dgo_EF_CreatedBy]  DEFAULT ((0)),
	[dgo_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DrugOrder_ExtraFields__dgo_EF_Created]  DEFAULT (getdate()),
	[dgo_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_DrugOrder_ExtraFields__dgo_EF_updatedby]  DEFAULT ((0)),
	[dgo_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_DrugOrder_ExtraFields__dgo_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient_PrevNames](
	[pvn_id] [int] IDENTITY(0,1) NOT NULL,
	[pvn_patid] [int] NOT NULL CONSTRAINT [DF__Patient_PrevNames__pvn_patid]  DEFAULT (0),
	[pvn_surname] [varchar](35) NOT NULL CONSTRAINT [DF__Patient_PrevNames__pvn_surname]  DEFAULT (''),
	[pvn_forename] [varchar](35) NOT NULL CONSTRAINT [DF__Patient_PrevNames__pvn_forename]  DEFAULT (''),
	[pvn_createdby] [int] NOT NULL CONSTRAINT [DF__Patient_PrevNames__pvn_createdby]  DEFAULT (0),
	[pvn_created] [datetime] NOT NULL CONSTRAINT [DF__Patient_PrevNames__pvn_created]  DEFAULT (getdate())
) ON [PRIMARY]
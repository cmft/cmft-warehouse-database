﻿CREATE TABLE [dbo].[RTA_Details](
	[rta_rtaid] [int] IDENTITY(0,1) NOT NULL,
	[rta_atdid] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_atdid]  DEFAULT (0),
	[rta_date] [datetime] NOT NULL CONSTRAINT [DF__RTA_Details__rta_date]  DEFAULT ('2200/01/01'),
	[rta_grid] [varchar](10) NOT NULL CONSTRAINT [DF__RTA_Details__rta_grid]  DEFAULT (''),
	[rta_patientype] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_patientype]  DEFAULT (0),
	[rta_equip] [varchar](150) NOT NULL CONSTRAINT [DF__RTA_Details__rta_equip]  DEFAULT (''),
	[rta_doctor] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_doctor]  DEFAULT (0),
	[rta_impact] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_impact]  DEFAULT (0),
	[rta_struck] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_struck]  DEFAULT (0),
	[rta_sat] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_sat]  DEFAULT (0),
	[rta_treatment] [varchar](150) NOT NULL CONSTRAINT [DF__RTA_Details__rta_treatment]  DEFAULT (''),
	[rta_thrown] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_thrown]  DEFAULT (0),
	[rta_notes] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_notes]  DEFAULT (0),
	[rta_updatedby] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_updatedby]  DEFAULT (0),
	[rta_createdby] [int] NOT NULL CONSTRAINT [DF__RTA_Details__rta_createdby]  DEFAULT (0),
	[rta_update] [datetime] NOT NULL CONSTRAINT [DF__RTA_Details__rta_update]  DEFAULT (getdate()),
	[rta_created] [datetime] NOT NULL CONSTRAINT [DF__RTA_Details__rta_created]  DEFAULT (getdate())
) ON [PRIMARY]
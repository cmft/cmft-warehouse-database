﻿CREATE TABLE [dbo].[CDSExtractV6_Table](
	[CDS_MSG_TYP] [varchar](6) NULL,
	[CDS_MSG_VER_NO] [varchar](6) NULL,
	[CDS_MSG_REF] [int] NULL,
	[Unique_Booking_Ref_No] [varchar](1) NULL,
	[Patient_Pathway_Identifier] [varchar](1) NULL,
	[OrganisationCode_PatientPathwayIdentifierIssuer] [varchar](1) NULL,
	[Referral_to_Treatment_Status] [varchar](1) NULL,
	[Referral_to_Treatment_Period_Start_Date] [varchar](1) NULL,
	[Referral_to_Treatment_Period_End_Date] [varchar](1) NULL,
	[Local_PatientID] [varchar](10) NOT NULL,
	[Org_Code_LPI] [varchar](3) NOT NULL,
	[NHS_Number] [varchar](10) NOT NULL,
	[NHS_NumberStatus] [varchar](2) NOT NULL,
	[Patient Name] [varchar](70) NOT NULL,
	[Patient Usual Address] [varchar](175) NOT NULL,
	[Postcode] [varchar](10) NOT NULL,
	[Org_Code_PCT] [varchar](3) NOT NULL,
	[Date_Of_Birth] [varchar](8000) NOT NULL,
	[Sex] [varchar](4000) NOT NULL,
	[Carer_Support_Indicator] [varchar](2) NOT NULL,
	[Ethnic_category] [varchar](4000) NOT NULL,
	[GP_Code] [varchar](8) NULL,
	[Practice_Code] [varchar](6) NOT NULL,
	[Attendance_Number] [varchar](12) NULL,
	[Arrival_Mode] [varchar](1) NOT NULL,
	[Attendance_Category] [varchar](1) NOT NULL,
	[Disposal] [varchar](2) NOT NULL,
	[Incident_Loc] [varchar](2) NOT NULL,
	[Patient_Group] [varchar](2) NOT NULL,
	[Source_Of_Referal] [varchar](2) NOT NULL,
	[A+E Department Type] [varchar](2) NOT NULL,
	[Arrival_Date] [varchar](8000) NOT NULL,
	[Arrival_Time] [varchar](8) NOT NULL,
	[Age_at_CDS_Activity_Date] [varchar](10) NOT NULL,
	[Initial_Assessment_Time] [varchar](8) NOT NULL,
	[Time_Seen_For_Treatment] [varchar](8) NOT NULL,
	[Attendance_Conclusion_Time] [varchar](8) NOT NULL,
	[Departure_Time] [varchar](8) NOT NULL,
	[Comm_SerialNo] [varchar](6) NULL,
	[NHS_ServAgreeLineNo] [varchar](1) NULL,
	[Prov_RefNo] [varchar](1) NULL,
	[Comm_RefNo] [varchar](8) NULL,
	[Org_Code_Provider] [varchar](3) NOT NULL,
	[Org_CodeComm] [varchar](3) NOT NULL,
	[StaffMember_Code] [varchar](3) NOT NULL,
	[Diag_SchemeinUse_AE] [varchar](2) NOT NULL,
	[Diagnosis_1_AE] [varchar](8000) NULL,
	[Diagnosis_2_AE] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_1] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_2] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_3] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_4] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_5] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_6] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_7] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_8] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_9] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_10] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_11] [varchar](8000) NULL,
	[SecondaryDiagnosis_AE_12] [varchar](8000) NULL,
	[Invest_SchemeinUse] [varchar](2) NOT NULL,
	[Investigation_1] [varchar](8000) NULL,
	[Investigation_2] [varchar](8000) NULL,
	[SecondaryInvestigation_1] [varchar](8000) NULL,
	[SecondaryInvestigation_2] [varchar](8000) NULL,
	[SecondaryInvestigation_3] [varchar](8000) NULL,
	[SecondaryInvestigation_4] [varchar](8000) NULL,
	[SecondaryInvestigation_5] [varchar](8000) NULL,
	[SecondaryInvestigation_6] [varchar](8000) NULL,
	[SecondaryInvestigation_7] [varchar](8000) NULL,
	[SecondaryInvestigation_8] [varchar](8000) NULL,
	[SecondaryInvestigation_9] [varchar](8000) NULL,
	[SecondaryInvestigation_10] [varchar](8000) NULL,
	[Procedure_SchemeinUseAE] [varchar](2) NOT NULL,
	[TreatmentAE_1] [varchar](8000) NULL,
	[TreatmentAE_Date_1] [varchar](8000) NULL,
	[TreatmentAE_2] [varchar](8000) NULL,
	[TreatmentAE_Date_2] [varchar](8000) NULL,
	[SecondaryTreatmentAE_1] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_1] [varchar](8000) NULL,
	[SecondaryTreatmentAE_2] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_2] [varchar](8000) NULL,
	[SecondaryTreatmentAE_3] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_3] [varchar](8000) NULL,
	[SecondaryTreatmentAE_4] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_4] [varchar](8000) NULL,
	[SecondaryTreatmentAE_5] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_5] [varchar](8000) NULL,
	[SecondaryTreatmentAE_6] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_6] [varchar](8000) NULL,
	[SecondaryTreatmentAE_7] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_7] [varchar](8000) NULL,
	[SecondaryTreatmentAE_8] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_8] [varchar](8000) NULL,
	[SecondaryTreatmentAE_9] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_9] [varchar](8000) NULL,
	[SecondaryTreatmentAE_10] [varchar](8000) NULL,
	[SecondaryTreatmentAE_Date_10] [varchar](8000) NULL,
	[HRG_Code] [varchar](3) NULL,
	[HRGCode_VerNo] [varchar](3) NOT NULL,
	[Procedure_SchemeinUse] [varchar](2) NOT NULL,
	[HRG_DomGrpVar] [varchar](1) NOT NULL,
	[CDS_MSG_REF_Trailer] [int] NULL,
	[atd_id] [int] NOT NULL
) ON [PRIMARY]
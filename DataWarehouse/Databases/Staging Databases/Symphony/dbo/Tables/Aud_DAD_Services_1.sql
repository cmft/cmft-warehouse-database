﻿CREATE TABLE [dbo].[Aud_DAD_Services](
	[ser_identity] [int] IDENTITY(0,1) NOT NULL,
	[ser_id] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_id]  DEFAULT (0),
	[ser_name] [varchar](50) NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_name]  DEFAULT (''),
	[ser_datamode] [tinyint] NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_datamode]  DEFAULT (0),
	[ser_user] [tinyint] NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_user]  DEFAULT (0),
	[ser_userid] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_userid]  DEFAULT (0),
	[ser_status] [tinyint] NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_status]  DEFAULT (0),
	[ser_hasbeenused] [bit] NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_hasbeenused]  DEFAULT (0),
	[ser_description] [varchar](2000) NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_description]  DEFAULT (''),
	[ser_deptid] [int] NOT NULL CONSTRAINT [DF__Aud_dAD_Services__ser_deptid]  DEFAULT (0),
	[ser_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_createdby]  DEFAULT (0),
	[ser_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DAD_Services__ser_created]  DEFAULT (getdate()),
	[ser_progressmsg] [varchar](2000) NOT NULL CONSTRAINT [DF__aud_DAD_Services__ser_progressmsg]  DEFAULT ('')
) ON [PRIMARY]
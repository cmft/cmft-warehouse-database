﻿CREATE TABLE [dbo].[Aud_DADSendNewReg](
	[Dsnr_PatPid] [int] NOT NULL CONSTRAINT [PK__Aud_DADSendNewReg__Dsnr_PatPid]  DEFAULT (0),
	[Dsnr_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DADSendNewReg__Dsnr_createdby]  DEFAULT (0),
	[Dsnr_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DADSendNewReg__Dsnr_created]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_DataProcessingAction](
	[dpa_id] [int] IDENTITY(0,1) NOT NULL,
	[dpa_pjbid] [int] NOT NULL CONSTRAINT [DF__CFG_DataProcessingAction__dpa_pjbid]  DEFAULT (0),
	[dpa_depid] [smallint] NOT NULL CONSTRAINT [DF__CFG_DataProcessingAction__dpa_depid]  DEFAULT (0),
	[dpa_dpa] [int] NOT NULL CONSTRAINT [DF__CFG_DataProcessingAction__dpa_dpa]  DEFAULT (0),
	[dpa_status] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DataProcessingAction__dpa_status]  DEFAULT (0),
	[dpa_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DataProcessingAction__dpa_createdby]  DEFAULT (0),
	[dpa_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_DataProcessingAction__dpa_created]  DEFAULT (getdate()),
	[dpa_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_DataProcessingAction__dpa_update]  DEFAULT (getdate())
) ON [PRIMARY]
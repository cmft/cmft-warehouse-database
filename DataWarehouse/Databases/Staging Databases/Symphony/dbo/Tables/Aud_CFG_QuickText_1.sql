﻿CREATE TABLE [dbo].[Aud_CFG_QuickText](
	[qti_identity] [int] IDENTITY(0,1) NOT NULL,
	[qti_id] [int] NOT NULL CONSTRAINT [PK__Aud_CFG_QuickText__nts_id]  DEFAULT (0),
	[qti_fieldid] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_QuickText__qti_fieldid]  DEFAULT (0),
	[qti_quicktext] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_CFG_QuickText__qti_quicktext]  DEFAULT (''),
	[qti_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_QuickText__qti_deleted]  DEFAULT (0),
	[qti_isdeleted] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_QuickText__qti_isdeleted]  DEFAULT (0),
	[qti_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_QuickText__qti_updatedby]  DEFAULT (0),
	[qti_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_QuickText__qti_update]  DEFAULT (getdate()),
	[qti_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_QuickText__qti_created]  DEFAULT (getdate()),
	[qti_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_QuickText__qti_createdby]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tmpCMMCNOK](
	[RowID] [varchar](50) NULL DEFAULT (''),
	[DataSetName] [varchar](50) NULL DEFAULT (''),
	[Health Organisation Id] [varchar](50) NULL DEFAULT (''),
	[Patient Id] [varchar](50) NULL DEFAULT (''),
	[Surname] [varchar](50) NULL DEFAULT (''),
	[Forename] [varchar](50) NULL DEFAULT (''),
	[Title] [varchar](50) NULL DEFAULT (''),
	[Relationship] [varchar](50) NULL DEFAULT (''),
	[Address Line 1] [varchar](50) NULL DEFAULT (''),
	[Address Line 2] [varchar](50) NULL DEFAULT (''),
	[Address Line 3] [varchar](50) NULL DEFAULT (''),
	[Address Line 4] [varchar](50) NULL DEFAULT (''),
	[Address Line 5] [varchar](50) NULL DEFAULT (''),
	[PostCode] [varchar](50) NULL DEFAULT (''),
	[Home Telephone] [varchar](50) NULL DEFAULT (''),
	[Mobile Telephone] [varchar](50) NULL DEFAULT (''),
	[RowNum] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
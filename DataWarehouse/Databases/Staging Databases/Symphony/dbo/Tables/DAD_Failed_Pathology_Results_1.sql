﻿CREATE TABLE [dbo].[DAD_Failed_Pathology_Results](
	[fpr_patid] [int] NOT NULL CONSTRAINT [DF__DAD_Failed_Pathology_Results__fpr_patid]  DEFAULT (0),
	[fpr_reason] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Failed_Pathology_Results__fpr_reason]  DEFAULT (''),
	[fpr_message] [varchar](4000) NOT NULL CONSTRAINT [DF__DAD_Failed_Pathology_Results__fpr_message]  DEFAULT (''),
	[fpr_CreatedBy] [int] NOT NULL CONSTRAINT [DF__DAD_Failed_Pathology_Results__fpr_CreatedBy]  DEFAULT (0),
	[fpr_DateCreated] [datetime] NOT NULL CONSTRAINT [DF__DAD_Failed_Pathology_Results__fpr_DateCreated]  DEFAULT (getdate())
) ON [PRIMARY]
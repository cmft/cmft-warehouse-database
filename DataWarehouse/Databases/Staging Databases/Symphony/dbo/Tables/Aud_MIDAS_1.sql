﻿CREATE TABLE [dbo].[Aud_MIDAS](
	[mid_identity] [int] IDENTITY(0,1) NOT NULL,
	[mid_id] [int] NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_id]  DEFAULT (0),
	[mid_link] [tinyint] NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_link]  DEFAULT (0),
	[mid_number] [varchar](20) NOT NULL CONSTRAINT [DF__MIDAS__Aud_mid_number]  DEFAULT (0),
	[mid_barcode] [varchar](100) NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_barcode]  DEFAULT (''),
	[mid_pages] [tinyint] NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_pages]  DEFAULT (0),
	[mid_documenttype] [int] NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_documenttype]  DEFAULT (0),
	[mid_imagepath] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_imagepath]  DEFAULT (''),
	[mid_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_createdby]  DEFAULT (0),
	[mid_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_update]  DEFAULT (getdate()),
	[mid_DeletedRecord] [bit] NOT NULL CONSTRAINT [DF__Aud_MIDAS__mid_deleted]  DEFAULT (0),
	[mid_contextid] [int] NULL,
	[mid_date] [datetime] NULL,
	[mid_deleted] [bit] NULL
) ON [PRIMARY]
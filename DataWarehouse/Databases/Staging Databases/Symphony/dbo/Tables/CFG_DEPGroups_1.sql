﻿CREATE TABLE [dbo].[CFG_DEPGroups](
	[dgp_id] [int] IDENTITY(1,1) NOT NULL,
	[dgp_dep] [smallint] NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_dep]  DEFAULT (0),
	[dgp_group] [int] NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_group]  DEFAULT (0),
	[dgp_access] [tinyint] NOT NULL CONSTRAINT [DF__Cfg_DEPGroups__dgp_access]  DEFAULT (0),
	[dgp_type] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_type]  DEFAULT (0),
	[dgp_attid] [int] NOT NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_attid]  DEFAULT (0),
	[dgp_universal] [bit] NOT NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_universal]  DEFAULT (0),
	[dgp_freeform] [bit] NOT NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_freeform]  DEFAULT (0),
	[dgp_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_createdby]  DEFAULT (0),
	[dgp_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_update]  DEFAULT (getdate()),
	[dgp_audioreminder] [bit] NOT NULL CONSTRAINT [DF__CFG_DEPGroups__dgp_audioreminder]  DEFAULT (0)
) ON [PRIMARY]
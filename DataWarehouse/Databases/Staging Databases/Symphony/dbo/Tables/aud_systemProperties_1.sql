﻿CREATE TABLE [dbo].[aud_systemProperties](
	[syp_sypid] [int] NOT NULL CONSTRAINT [DF__AUD_systemProperties__syp_sypid]  DEFAULT (0),
	[syp_type] [varchar](30) NOT NULL CONSTRAINT [DF__AUD_systemProperties__syp_type]  DEFAULT (''),
	[syp_value] [varchar](2000) NOT NULL CONSTRAINT [DF__AUD_systemProperties__syp_value]  DEFAULT (''),
	[syp_createdby] [int] NOT NULL CONSTRAINT [DF__AUD_systemProperties__syp_createdby]  DEFAULT (0),
	[syp_update] [datetime] NOT NULL CONSTRAINT [DF__AUD_systemProperties__syp_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[zImport_Lookups](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[lkp_parent] [varchar](80) NULL DEFAULT (''),
	[lkp_Level0] [varchar](80) NULL DEFAULT (''),
	[lkp_Level1] [varchar](80) NULL DEFAULT (''),
	[lkp_Level2] [varchar](80) NULL DEFAULT (''),
	[lkp_Level3] [varchar](80) NULL DEFAULT (''),
	[lkp_Level4] [varchar](80) NULL DEFAULT (''),
	[CDS_Code] [varchar](10) NULL DEFAULT (''),
	[comment] [varchar](200) NULL DEFAULT (''),
	[lkpparentid] [int] NULL DEFAULT ((0)),
	[lkpid0] [int] NULL DEFAULT ((0)),
	[lkpid1] [int] NULL DEFAULT ((0)),
	[lkpid2] [int] NULL DEFAULT ((0)),
	[lkpid3] [int] NULL DEFAULT ((0)),
	[lkpid4] [int] NULL DEFAULT ((0)),
	[Lkpid] [int] NULL DEFAULT ((0)),
	[toDelete] [smallint] NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_CFG_DeptSettings](
	[dps_identity] [int] IDENTITY(0,1) NOT NULL,
	[dps_id] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptSettings__dps_id]  DEFAULT (0),
	[dps_deptid] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptSettings__dps_deptid]  DEFAULT (0),
	[dps_name] [varchar](50) NOT NULL CONSTRAINT [DF__Aud_CFG_DeptSettings__dps_name]  DEFAULT (''),
	[dps_value] [varchar](500) NULL CONSTRAINT [DF__Aud_CFG_DeptSettings__dps_value]  DEFAULT (''),
	[dps_created] [datetime] NOT NULL CONSTRAINT [DF__aud_CFG_DeptSettings __dps_created]  DEFAULT (getdate()),
	[dps_createdby] [int] NOT NULL CONSTRAINT [DF__aud_CFG_DeptSettings __dps_createdby]  DEFAULT (0),
	[dps_updated] [datetime] NOT NULL CONSTRAINT [DF__aud_CFG_DeptSettings __dps_updated]  DEFAULT (getdate()),
	[dps_updatedby] [int] NOT NULL CONSTRAINT [DF__aud_CFG_DeptSettings __dps_updatedby]  DEFAULT (0),
	[dps_deleted] [bit] NOT NULL CONSTRAINT [DF__aud_CFG_DeptSettings__dps_deleted]  DEFAULT (0)
) ON [PRIMARY]
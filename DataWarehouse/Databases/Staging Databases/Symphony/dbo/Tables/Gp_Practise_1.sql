﻿CREATE TABLE [dbo].[Gp_Practise](
	[pr_praccode] [varchar](14) NOT NULL,
	[pr_telid] [int] NOT NULL CONSTRAINT [DF__Gp_practise__pr_telid]  DEFAULT (0),
	[pr_gpfhcode] [varchar](3) NOT NULL CONSTRAINT [DF__Gp_practise__pr_gpfhcode]  DEFAULT ('X98'),
	[pr_healthvisitor] [varchar](56) NOT NULL CONSTRAINT [DF__Gp_practise__pr_healthvisitor]  DEFAULT (''),
	[pr_localcode] [varchar](14) NOT NULL CONSTRAINT [DF__Gp_practise__pr_localcode]  DEFAULT (''),
	[pr_status] [int] NOT NULL CONSTRAINT [DF__Gp_practise__pr_status]  DEFAULT (0),
	[pr_localgp] [tinyint] NOT NULL CONSTRAINT [DF__Gp_practise__pr_localgp]  DEFAULT (1),
	[pr_pgrpcarecode] [varchar](14) NOT NULL CONSTRAINT [DF__GP_Practise__pr_pgrpcarecode]  DEFAULT (''),
	[pr_createdby] [int] NOT NULL CONSTRAINT [DF__Gp_practi__pr_createdby]  DEFAULT (0),
	[pr_update] [datetime] NOT NULL CONSTRAINT [DF__Gp_practise__pr_update]  DEFAULT (getdate()),
	[Pr_id] [int] IDENTITY(1,1) NOT NULL,
	[pr_letterpref] [varchar](35) NOT NULL CONSTRAINT [DF__GP_practise__pr_letterpref]  DEFAULT ('')
) ON [PRIMARY]
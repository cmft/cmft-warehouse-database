﻿CREATE TABLE [dbo].[ExtraFieldsConfigLog](
	[efcl_Id] [int] IDENTITY(1,1) NOT NULL,
	[efcl_FieldProperty] [varchar](100) NOT NULL,
	[efcl_NewFieldId] [int] NOT NULL,
	[efcl_OldFieldId] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Cfg_Entity_AttendanceType](
	[Eta_id] [int] IDENTITY(0,1) NOT NULL,
	[Eta_EntityId] [smallint] NOT NULL,
	[Eta_datid] [int] NOT NULL CONSTRAINT [DF__Cfg_Entity_AttendanceType__Eta_datid]  DEFAULT (0),
	[Eta_active] [bit] NOT NULL CONSTRAINT [DF__Cfg_Entity_AttendanceType__Eta_active]  DEFAULT (0),
	[Eta_type] [smallint] NOT NULL CONSTRAINT [DF__Cfg_Entity_AttendanceType_Eta_type]  DEFAULT (0),
	[Eta_createdBy] [int] NOT NULL CONSTRAINT [DF__Cfg_Entity_AttendanceType_Eta_createdBy]  DEFAULT (0),
	[Eta_UpdatedBy] [int] NOT NULL CONSTRAINT [DF__Cfg_Entity_AttendanceType_Eta_UpdatedBy]  DEFAULT (0)
) ON [PRIMARY]
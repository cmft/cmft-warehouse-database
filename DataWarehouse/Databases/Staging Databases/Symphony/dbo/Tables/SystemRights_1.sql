﻿CREATE TABLE [dbo].[SystemRights](
	[Syr_ID] [int] IDENTITY(0,1) NOT NULL,
	[Syr_SysRight] [int] NOT NULL CONSTRAINT [DF__SystemRights__syr_sysright]  DEFAULT (0),
	[Syr_Account] [int] NOT NULL CONSTRAINT [DF__SystemRights__syr_account]  DEFAULT (0),
	[Syr_AccountType] [tinyint] NOT NULL CONSTRAINT [DF__SystemRights__syr_accounttype]  DEFAULT (0),
	[Syr_Permission] [tinyint] NOT NULL CONSTRAINT [DF__SystemRights__syr_permission]  DEFAULT (0),
	[syr_createdby] [int] NOT NULL CONSTRAINT [DF__SystemRights__syr_createdby]  DEFAULT (0),
	[syr_created] [datetime] NOT NULL CONSTRAINT [DF__SystemRights__syr_created]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_ResultDetails_ExtraFields](
	[res_EF_ID] [int] IDENTITY(0,1) NOT NULL,
	[res_EF_extrafieldID] [int] NULL,
	[res_EF_FieldType] [smallint] NULL,
	[res_EF_FieldID] [int] NULL,
	[res_EF_atdid] [int] NULL,
	[res_EF_Value] [varchar](4000) NULL,
	[res_EF_DataCategory] [smallint] NULL,
	[res_EF_RecordID] [int] NULL,
	[res_EF_FieldCodeValues] [varchar](200) NULL,
	[Res_EF_ComputationState] [tinyint] NULL,
	[Res_EF_depId] [smallint] NULL,
	[Res_EF_CPMessagesIDs] [varchar](200) NULL,
	[res_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Aud_ResultDetails_ExtraFields__res_EF_CreatedBy]  DEFAULT ((0)),
	[res_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_ResultDetails_ExtraFields__res_EF_Created]  DEFAULT (getdate()),
	[res_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_ResultDetails_ExtraFields__res_EF_updatedby]  DEFAULT ((0)),
	[res_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_ResultDetails_ExtraFields__res_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
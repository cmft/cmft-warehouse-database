﻿CREATE TABLE [dbo].[Aud_Majax](
	[mjx_identity] [int] IDENTITY(0,1) NOT NULL,
	[mjx_id] [int] NOT NULL,
	[mjx_name] [varchar](50) NOT NULL,
	[mjx_startdate] [datetime] NOT NULL,
	[mjx_detailsid] [int] NOT NULL,
	[mjx_askifinincident] [bit] NOT NULL,
	[mjx_enddate] [datetime] NOT NULL,
	[mjx_createdby] [int] NOT NULL,
	[mjx_created] [datetime] NOT NULL,
	[mjx_deleted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DAD_Ser_Logging](
	[log_serviceid] [int] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_serviceid]  DEFAULT (0),
	[log_exceptions] [bit] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_exceptions]  DEFAULT (0),
	[log_success] [bit] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_success]  DEFAULT (0),
	[log_failures] [bit] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_failures]  DEFAULT (0),
	[log_type] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_type]  DEFAULT (0),
	[log_typeinterval] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_typeinterval]  DEFAULT (0),
	[log_typevalue] [varchar](20) NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_typevalue]  DEFAULT (''),
	[log_createnewfile] [bit] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_createnewfile]  DEFAULT (0),
	[log_filepath] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_filepath]  DEFAULT (''),
	[log_filename] [varchar](50) NOT NULL CONSTRAINT [DF__DAD_Ser_Logging_log_filename]  DEFAULT (''),
	[log_AudMsg] [bit] NOT NULL CONSTRAINT [DF_DAD_Ser_Logging_log_AudMsg]  DEFAULT (0),
	[log_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging__log_createdby]  DEFAULT (0),
	[log_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Ser_Logging__log_created]  DEFAULT (getdate())
) ON [PRIMARY]
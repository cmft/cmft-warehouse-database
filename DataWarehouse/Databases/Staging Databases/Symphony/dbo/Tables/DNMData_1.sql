﻿CREATE TABLE [dbo].[DNMData](
	[dnm_id] [int] IDENTITY(0,1) NOT NULL,
	[dnm_image] [varchar](8000) NOT NULL CONSTRAINT [DF__DNMData__dnm_image]  DEFAULT (''),
	[dnm_createdby] [int] NOT NULL CONSTRAINT [DF__DNMData__dnm_createdby]  DEFAULT (0),
	[dnm_created] [datetime] NOT NULL CONSTRAINT [DF__DNMData__dnm_created]  DEFAULT (getdate()),
	[dnm_updated] [datetime] NOT NULL CONSTRAINT [DF__DNMData__dnm_updated]  DEFAULT (getdate())
) ON [PRIMARY]
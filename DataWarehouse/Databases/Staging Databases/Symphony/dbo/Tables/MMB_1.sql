﻿CREATE TABLE [dbo].[MMB](
	[mmb_property] [varchar](100) NOT NULL,
	[mmb_value] [varchar](100) NOT NULL,
	[mmb_createdby] [int] NOT NULL,
	[mmb_created] [datetime] NOT NULL,
	[mmb_deptid] [int] NOT NULL
) ON [PRIMARY]
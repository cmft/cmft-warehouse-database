﻿CREATE TABLE [dbo].[TemporaryInv](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[res_atdid] [int] NULL,
	[res_date] [datetime] NULL,
	[res_result] [varchar](200) NULL
) ON [PRIMARY]
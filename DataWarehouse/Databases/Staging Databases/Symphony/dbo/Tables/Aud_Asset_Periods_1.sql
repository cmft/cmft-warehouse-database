﻿CREATE TABLE [dbo].[Aud_Asset_Periods](
	[asp_identity] [int] IDENTITY(0,1) NOT NULL,
	[asp_id] [int] NULL,
	[asp_parentid] [int] NULL,
	[asp_issuedate] [datetime] NULL,
	[asp_status] [smallint] NULL,
	[asp_createdby] [int] NULL,
	[asp_updated] [datetime] NULL,
	[asp_period] [int] NULL,
	[asp_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Asset_Periods__asp_deleted]  DEFAULT (0),
	[asp_datecreated] [datetime] NULL,
	[asp_updatedby] [int] NULL,
	[asp_returnDate] [datetime] NULL
) ON [PRIMARY]
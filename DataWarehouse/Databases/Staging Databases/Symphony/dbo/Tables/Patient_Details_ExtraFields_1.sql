﻿CREATE TABLE [dbo].[Patient_Details_ExtraFields](
	[pdt_EF_extrafieldID] [int] IDENTITY(0,1) NOT NULL,
	[pdt_EF_FieldType] [smallint] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__FieldType]  DEFAULT (0),
	[pdt_EF_FieldID] [int] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__FieldID]  DEFAULT (0),
	[pdt_EF_pdtpid] [int] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__pdtpid]  DEFAULT (0),
	[pdt_EF_Value] [varchar](4000) NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__Value]  DEFAULT (''),
	[pdt_EF_DataCategory] [smallint] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__DataCategory]  DEFAULT (0),
	[pdt_EF_RecordID] [int] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__RecordID]  DEFAULT (0),
	[pdt_EF_FieldCodeValues] [varchar](200) NOT NULL CONSTRAINT [DF_Patient_Details_ExtraFields_pdt_EF_FieldCodeValues]  DEFAULT (''),
	[pdt_EF_ComputationState] [tinyint] NOT NULL CONSTRAINT [DF_Patient_Details_ExtraFields_pdt_EF_ComputationState]  DEFAULT (0),
	[pdt_EF_depId] [int] NOT NULL CONSTRAINT [DF_Patient_Details_ExtraFields_pdt_EF_depId]  DEFAULT (0),
	[pdt_EF_CPMessagesIDs] [varchar](200) NOT NULL CONSTRAINT [DF_Patient_Details_ExtraFields_pdt_EF_CPMessagesIDs]  DEFAULT (''),
	[pdt_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__pdt_EF_CreatedBy]  DEFAULT ((0)),
	[pdt_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__pdt_EF_Created]  DEFAULT (getdate()),
	[pdt_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__pdt_EF_updatedby]  DEFAULT ((0)),
	[pdt_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Patient_Details_ExtraFields__pdt_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
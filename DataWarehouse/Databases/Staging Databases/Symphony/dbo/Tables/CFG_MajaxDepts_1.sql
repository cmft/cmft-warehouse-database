﻿CREATE TABLE [dbo].[CFG_MajaxDepts](
	[cmd_majaxid] [int] NOT NULL CONSTRAINT [DF__CFG_MajaxDepts__cmd_majaxid]  DEFAULT (0),
	[cmd_deptid] [int] NOT NULL CONSTRAINT [DF__CFG_MajaxDepts__cmd_deptid]  DEFAULT (0),
	[cmd_patients] [smallint] NOT NULL CONSTRAINT [DF__CFG_MajaxDepts__cmd_patients]  DEFAULT (0),
	[cmd_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_MajaxDepts __cmd_createdby]  DEFAULT (0),
	[cmd_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_MajaxDepts __cmd_created]  DEFAULT (getdate()),
	[cmd_updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_MajaxDepts __cmd_updated]  DEFAULT (getdate()),
	[cmd_updatedby] [int] NOT NULL CONSTRAINT [DF__CFG_MajaxDepts __cmd_updatedby]  DEFAULT (0)
) ON [PRIMARY]
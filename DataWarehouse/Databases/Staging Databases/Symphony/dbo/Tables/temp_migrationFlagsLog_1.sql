﻿CREATE TABLE [dbo].[temp_migrationFlagsLog](
	[tblid] [int] IDENTITY(1,1) NOT NULL,
	[flagname] [varchar](40) NULL,
	[flagid] [int] NULL,
	[flag_lkpid] [int] NULL
) ON [PRIMARY]
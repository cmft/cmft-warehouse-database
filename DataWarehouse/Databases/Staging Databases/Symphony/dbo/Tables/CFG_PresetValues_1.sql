﻿CREATE TABLE [dbo].[CFG_PresetValues](
	[psv_id] [int] IDENTITY(0,1) NOT NULL,
	[psv_formid] [smallint] NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_formid]  DEFAULT (0),
	[psv_exclude] [bit] NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_exclude]  DEFAULT (0),
	[psv_caution] [bit] NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_caution]  DEFAULT (0),
	[psv_used] [bit] NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_used]  DEFAULT (0),
	[psv_presetvalues] [varchar](1500) NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_presetvalues]  DEFAULT (''),
	[psv_cautionmsg] [varchar](255) NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_cautionmsgs]  DEFAULT (''),
	[psv_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_createdby]  DEFAULT (0),
	[psv_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_created]  DEFAULT (getdate()),
	[psv_updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_PresetValues__psv_updated]  DEFAULT (getdate())
) ON [PRIMARY]
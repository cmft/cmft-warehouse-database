﻿CREATE TABLE [dbo].[CFG_Images](
	[img_id] [int] IDENTITY(0,1) NOT NULL,
	[img_table] [varchar](100) NOT NULL CONSTRAINT [DF__CFG_Images__img_table]  DEFAULT (''),
	[img_recordid] [int] NOT NULL CONSTRAINT [DF__CFG_Images__img_recordid]  DEFAULT (0),
	[img_image] [image] NULL,
	[img_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_Images__img_createdby]  DEFAULT (0),
	[img_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_Images__img_update]  DEFAULT (getdate()),
	[img_status] [tinyint] NOT NULL CONSTRAINT [DF__CFG_Images__img_status]  DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
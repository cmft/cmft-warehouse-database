﻿CREATE TABLE [dbo].[Aud_DAD_Msg_SubFieldDelimiters](
	[mfd_identity] [int] IDENTITY(0,1) NOT NULL,
	[mfd_structureid] [int] NOT NULL,
	[mfd_delimiter] [varchar](5) NOT NULL,
	[mfd_sequence] [tinyint] NOT NULL,
	[mfd_createdby] [int] NOT NULL,
	[mfd_created] [datetime] NOT NULL
) ON [PRIMARY]
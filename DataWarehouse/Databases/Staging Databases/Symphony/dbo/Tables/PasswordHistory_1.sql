﻿CREATE TABLE [dbo].[PasswordHistory](
	[pwh_id] [int] IDENTITY(0,1) NOT NULL,
	[pwh_password] [varchar](30) NOT NULL CONSTRAINT [DF__PasswordHistory__pwh_password]  DEFAULT (''),
	[pwh_staffid] [int] NOT NULL CONSTRAINT [DF__PasswordHistory__pwh_staffid]  DEFAULT (0),
	[pwh_date] [datetime] NOT NULL CONSTRAINT [DF__PasswordHistory__pwh_date]  DEFAULT (getdate()),
	[pwh_createdby] [int] NOT NULL CONSTRAINT [DF__PasswordHistory__pwh_createdby]  DEFAULT (0),
	[pwh_created] [datetime] NOT NULL CONSTRAINT [DF__PasswordHistory__pwh_created]  DEFAULT (getdate()),
	[pwh_update] [datetime] NOT NULL CONSTRAINT [DF__PasswordHistory__pwh_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Import_CMMCAELookups](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[Field Description] [varchar](80) NULL DEFAULT (''),
	[Extract] [varchar](80) NULL DEFAULT (''),
	[Description] [varchar](20) NULL DEFAULT (''),
	[Symphony Parent Lookup] [varchar](80) NULL DEFAULT (''),
	[Symphony Lookup] [varchar](80) NULL DEFAULT (''),
	[LookupID] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[GPLocalSearch](
	[gpl_dha] [varchar](3) NOT NULL CONSTRAINT [DF__GPLocalSearch__gpl_dha]  DEFAULT (''),
	[gpl_postcode] [varchar](8) NOT NULL CONSTRAINT [DF__GPLocalSearch__gpl_postcode]  DEFAULT ('')
) ON [PRIMARY]
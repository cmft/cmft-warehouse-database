﻿CREATE TABLE [dbo].[Import_LookupMappings_Bulk](
	[lkp_tblname] [varchar](150) NULL,
	[lkp_name] [varchar](150) NULL,
	[lkp_tblid] [int] NULL,
	[lkp_id] [int] NULL,
	[mappingid] [varchar](1500) NULL
) ON [PRIMARY]
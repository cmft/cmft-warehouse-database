﻿CREATE TABLE [dbo].[CFG_LookupFlagItem](
	[lfi_id] [int] IDENTITY(0,1) NOT NULL,
	[lfi_flagid] [int] NOT NULL CONSTRAINT [DF__CFG_LookupFlagItem__lfi_flagid]  DEFAULT (0),
	[lfi_lookupid] [int] NOT NULL CONSTRAINT [DF__CFG_LookupFlagItem__lfi_lookup]  DEFAULT (0),
	[lfi_active] [bit] NOT NULL CONSTRAINT [DF__CFG_LookupFlagItem__lfi_active]  DEFAULT (1),
	[lfi_deleted] [bit] NOT NULL CONSTRAINT [DF__CFG_LookupFlagItem__lfi_deleted]  DEFAULT (0),
	[lfi_index] [smallint] NOT NULL CONSTRAINT [DF__CFG_LookupFlagItem__lfi_index]  DEFAULT (0),
	[lfi_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_LookupFlagItem__lfi_createdby]  DEFAULT (0),
	[lfi_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_LookupFlagItem__lfi_created]  DEFAULT (getdate()),
	[lfi_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_LookupFlagItem__lfi_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_DAD_Soap](
	[dsp_identity] [int] IDENTITY(0,1) NOT NULL,
	[dsp_serviceid] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Soap__dsp_serviceid]  DEFAULT (0),
	[dsp_operation] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_DAD_Soap__dsp_operation]  DEFAULT (''),
	[dsp_execaction] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_DAD_Soap__dsp_execaction]  DEFAULT (''),
	[dsp_description] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_DAD_Soap__dsp_description]  DEFAULT (''),
	[dsp_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Soap__dsp_createdby]  DEFAULT (0),
	[dsp_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DAD_Soap__dsp_created]  DEFAULT (getdate()),
	[dsp_LogonXMLFilePath] [varchar](2000) NULL,
	[dsp_LogonResponseXMLFilePath] [varchar](2000) NULL,
	[dsp_SoapID] [int] NULL
) ON [PRIMARY]
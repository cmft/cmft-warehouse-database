﻿CREATE TABLE [dbo].[CFG_DeptEpdCountSettings](
	[ECS_ID] [int] IDENTITY(0,1) NOT NULL,
	[ECS_DeptID] [int] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_DeptID]  DEFAULT (0),
	[ECS_MaxPatientAge] [smallint] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_MaxPatientAge]  DEFAULT (1),
	[ECS_EpdCountDuration] [int] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_EpdCountDuration]  DEFAULT (1),
	[ECS_IncludeClinicAttsInEpdCount] [bit] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_IncludeClinicAttsInEpdCount]  DEFAULT (1),
	[ECS_DisplayTotalEpdCount] [bit] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_DisplayTotalEpdCount]  DEFAULT (0),
	[ECS_Created] [datetime] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_Created]  DEFAULT (getdate()),
	[ECS_CreatedBy] [int] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_CreatedBy]  DEFAULT (0),
	[ECS_Updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_Updated]  DEFAULT (getdate()),
	[ECS_UpdatedBy] [int] NOT NULL CONSTRAINT [DF__CFG_DeptE__ECS_UpdatedBy]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MIDAS](
	[mid_id] [int] IDENTITY(0,1) NOT NULL,
	[mid_link] [tinyint] NOT NULL CONSTRAINT [DF__MIDAS__mid_link]  DEFAULT (0),
	[mid_number] [varchar](20) NOT NULL CONSTRAINT [DF__MIDAS__mid_number]  DEFAULT (0),
	[mid_barcode] [varchar](100) NOT NULL CONSTRAINT [DF__MIDAS__mid_barcode]  DEFAULT (''),
	[mid_pages] [tinyint] NOT NULL CONSTRAINT [DF__MIDAS__mid_pages]  DEFAULT (0),
	[mid_documenttype] [int] NOT NULL CONSTRAINT [DF__MIDAS__mid_documenttype]  DEFAULT (0),
	[mid_imagepath] [varchar](255) NOT NULL CONSTRAINT [DF__MIDAS__mid_imagepath]  DEFAULT (''),
	[mid_createdby] [int] NOT NULL CONSTRAINT [DF__MIDAS__mid_createdby]  DEFAULT (0),
	[mid_update] [datetime] NOT NULL CONSTRAINT [DF__MIDAS__mid_update]  DEFAULT (getdate()),
	[mid_contextid] [int] NULL,
	[mid_date] [datetime] NOT NULL CONSTRAINT [DF__MIDAS__mid_date]  DEFAULT (getdate()),
	[mid_deleted] [bit] NOT NULL CONSTRAINT [DF__MIDAS__mid_deleted]  DEFAULT (0)
) ON [PRIMARY]
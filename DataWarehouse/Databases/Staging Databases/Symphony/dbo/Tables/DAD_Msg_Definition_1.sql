﻿CREATE TABLE [dbo].[DAD_Msg_Definition](
	[mdf_id] [int] IDENTITY(0,1) NOT NULL,
	[mdf_structureid] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_structureid]  DEFAULT (0),
	[mdf_name] [varchar](100) NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_name]  DEFAULT (''),
	[mdf_direction] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_direction]  DEFAULT (0),
	[mdf_pmiaction] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_pmiaction]  DEFAULT (0),
	[mdf_msgaction] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_msgaction]  DEFAULT (0),
	[mdf_msgactionvalue] [varchar](10) NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_msgactionvalue]  DEFAULT (''),
	[mdf_lkpmapping] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_lkpmapping]  DEFAULT (0),
	[mdf_acktimeout] [smallint] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_acktimeout]  DEFAULT (0),
	[mdf_ackretries] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_ackretries]  DEFAULT (0),
	[mdf_description] [varchar](2000) NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_description]  DEFAULT (''),
	[mdf_hl7parsing] [bit] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_hl7parsing]  DEFAULT (0),
	[mdf_usequotes] [bit] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_usequotes]  DEFAULT (0),
	[mdf_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_createdby]  DEFAULT (0),
	[mdf_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_created]  DEFAULT (getdate()),
	[mdf_Status] [bit] NOT NULL CONSTRAINT [DF__DAD_Msg_Definition__mdf_Status]  DEFAULT (0)
) ON [PRIMARY]
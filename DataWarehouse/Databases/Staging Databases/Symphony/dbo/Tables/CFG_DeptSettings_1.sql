﻿CREATE TABLE [dbo].[CFG_DeptSettings](
	[dps_id] [int] IDENTITY(0,1) NOT NULL,
	[dps_deptid] [int] NOT NULL CONSTRAINT [DF__CFG_DeptSettings__dps_deptid]  DEFAULT (0),
	[dps_name] [varchar](50) NOT NULL CONSTRAINT [DF__CFG_DeptSettings__dps_name]  DEFAULT (''),
	[dps_value] [varchar](500) NULL CONSTRAINT [DF__CFG_DeptSettings__dps_value]  DEFAULT (''),
	[dps_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_DeptSettings __dps_created]  DEFAULT (getdate()),
	[dps_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DeptSettings __dps_createdby]  DEFAULT (0),
	[dps_updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_DeptSettings __dps_updated]  DEFAULT (getdate()),
	[dps_updatedby] [int] NOT NULL CONSTRAINT [DF__CFG_DeptSettings __dps_updatedby]  DEFAULT (0)
) ON [PRIMARY]
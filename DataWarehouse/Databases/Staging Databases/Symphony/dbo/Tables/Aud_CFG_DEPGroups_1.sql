﻿CREATE TABLE [dbo].[Aud_CFG_DEPGroups](
	[dgp_identity] [int] IDENTITY(1,1) NOT NULL,
	[dgp_id] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPGroups__dgp_id]  DEFAULT (0),
	[dgp_dep] [smallint] NOT NULL CONSTRAINT [DF_Aud_CFG_DEPGroups_dgp_dep]  DEFAULT (0),
	[dgp_group] [int] NOT NULL CONSTRAINT [DF_Aud_CFG_DEPGroups_dgp_group]  DEFAULT (0),
	[dgp_access] [tinyint] NOT NULL CONSTRAINT [DF_Aud_CFG_DEPGroups_dgp_access]  DEFAULT (0),
	[dgp_attid] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPGroups__dgp_attid]  DEFAULT (0),
	[dgp_universal] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPGroups__dgp_universal]  DEFAULT (0),
	[dgp_type] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPGroups__dgp_type]  DEFAULT (0),
	[dgp_freeform] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPGroups__dgp_freeform]  DEFAULT (0),
	[dgp_createdby] [int] NOT NULL CONSTRAINT [DF_Aud_CFG_DEPGroups_dgp_createdby]  DEFAULT (0),
	[dgp_update] [datetime] NOT NULL CONSTRAINT [DF_Aud_CFG_DEPGroups_dgp_update]  DEFAULT (getdate()),
	[dgp_deleted] [bit] NOT NULL CONSTRAINT [DF_Aud_CFG_DEPGroups_dgp_deleted]  DEFAULT (0),
	[dgp_audioreminder] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPGroups__dgp_audioreminder]  DEFAULT (0)
) ON [PRIMARY]
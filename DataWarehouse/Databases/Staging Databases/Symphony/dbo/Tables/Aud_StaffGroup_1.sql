﻿CREATE TABLE [dbo].[Aud_StaffGroup](
	[sg_identity] [int] IDENTITY(0,1) NOT NULL,
	[sg_staffid] [int] NOT NULL CONSTRAINT [DF__Aud_Staff__sg_staffid]  DEFAULT (0),
	[sg_grpid] [int] NOT NULL CONSTRAINT [DF__Aud_Staff__sg_grpid]  DEFAULT (0),
	[sg_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Staff__sg_createdby]  DEFAULT (0),
	[sg_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Staff__sg_update]  DEFAULT (getdate()),
	[sg_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Staff__sg_deleted]  DEFAULT (0)
) ON [PRIMARY]
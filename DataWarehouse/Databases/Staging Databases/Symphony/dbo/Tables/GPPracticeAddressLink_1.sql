﻿CREATE TABLE [dbo].[GPPracticeAddressLink](
	[gpa_gpid] [int] NOT NULL CONSTRAINT [DF__GPPracticeAddressLink__gpa_gpid]  DEFAULT ((0)),
	[gpa_prid] [int] NOT NULL CONSTRAINT [DF__GPPracticeAddressLink__gpa_prid]  DEFAULT ((0)),
	[gpa_praddid] [int] NOT NULL CONSTRAINT [DF__GPPracticeAddressLink__gpa_praddid]  DEFAULT ((0)),
	[gpa_CreatedBy] [int] NOT NULL CONSTRAINT [DF__GPPracticeAddressLink__gpa_CreatedBy]  DEFAULT ((0)),
	[gpa_updated] [datetime] NULL CONSTRAINT [DF__GPPracticeAddressLink__gpa_updated]  DEFAULT ('2200/01/01'),
	[gpa_localcode] [varchar](14) NOT NULL CONSTRAINT [DF__GPPracticeAddressLink__gpa_localcode]  DEFAULT (''),
	[gpa_telid] [int] NOT NULL CONSTRAINT [DF__GPPracticeAddressLink__gpa_telid]  DEFAULT ((0))
) ON [PRIMARY]
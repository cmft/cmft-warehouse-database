﻿CREATE TABLE [dbo].[CFG_PopUpItems](
	[pop_id] [int] IDENTITY(0,1) NOT NULL,
	[pop_popuptype] [tinyint] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_popuptype]  DEFAULT (0),
	[pop_popuptypeid] [int] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_popuptypeid]  DEFAULT (0),
	[pop_index] [smallint] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_index]  DEFAULT (0),
	[pop_condition] [tinyint] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_depid]  DEFAULT (0),
	[pop_fieldid] [int] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_fieldid]  DEFAULT (0),
	[pop_formid] [smallint] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_formid]  DEFAULT (0),
	[pop_updatedby] [int] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_updatedby]  DEFAULT (0),
	[pop_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_update]  DEFAULT (getdate()),
	[pop_PopUpScreenType] [smallint] NOT NULL CONSTRAINT [DF__CFG_PopUpItems__pop_PopUpScreenType]  DEFAULT (0)
) ON [PRIMARY]
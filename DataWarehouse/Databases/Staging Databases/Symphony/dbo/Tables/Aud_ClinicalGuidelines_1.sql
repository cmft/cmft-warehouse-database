﻿CREATE TABLE [dbo].[Aud_ClinicalGuidelines](
	[clg_identity] [int] IDENTITY(0,1) NOT NULL,
	[clg_lkpid] [int] NOT NULL,
	[clg_IsReference] [bit] NOT NULL,
	[clg_text] [ntext] NULL,
	[clg_createdby] [int] NOT NULL,
	[clg_created] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
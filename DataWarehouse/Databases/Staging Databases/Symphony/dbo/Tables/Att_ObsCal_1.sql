﻿CREATE TABLE [dbo].[Att_ObsCal](
	[aoc_atdid] [int] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_atdid]  DEFAULT (0),
	[aoc_temperature] [smallint] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_temperature]  DEFAULT (0),
	[aoc_general] [smallint] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_general]  DEFAULT (0),
	[aoc_oxygensat] [smallint] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_oxygensat]  DEFAULT (0),
	[aoc_twips] [smallint] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_twips]  DEFAULT (0),
	[aoc_createdby] [int] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_createdby]  DEFAULT (0),
	[aoc_created] [datetime] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_created]  DEFAULT (getdate()),
	[aoc_updatedby] [int] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_updatedby]  DEFAULT (0),
	[aoc_updated] [datetime] NOT NULL CONSTRAINT [DF__Att_ObsCal__aoc_updated]  DEFAULT (getdate())
) ON [PRIMARY]
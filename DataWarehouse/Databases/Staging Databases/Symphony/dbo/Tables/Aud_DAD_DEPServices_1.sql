﻿CREATE TABLE [dbo].[Aud_DAD_DEPServices](
	[DEPServ_identity] [int] IDENTITY(0,1) NOT NULL,
	[DEPServ_ID] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_ID]  DEFAULT (0),
	[DEPServ_ServiceID] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_ServiceID]  DEFAULT (0),
	[DEPServ_DEPID] [smallint] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_DEPID]  DEFAULT (0),
	[DEPServ_ExecuteDADService] [tinyint] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_ExecuteDADService]  DEFAULT (0),
	[DEPServ_DADDEPExecution] [tinyint] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_DADDEPExecution]  DEFAULT (0),
	[DEPServ_createdby] [int] NOT NULL CONSTRAINT [DF__Staff__Aud_DEPServ_createdby]  DEFAULT (0),
	[DEPServ_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_created]  DEFAULT (getdate()),
	[DEPServ_updated] [datetime] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_updated]  DEFAULT (getdate()),
	[DEPServ_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_updatedby]  DEFAULT (0),
	[DEPServ_ExcludedDEPID] [smallint] NOT NULL CONSTRAINT [DF__Aud_DAD_DEPServices__DEPServ_ExcludedDEPID]  DEFAULT ((0))
) ON [PRIMARY]
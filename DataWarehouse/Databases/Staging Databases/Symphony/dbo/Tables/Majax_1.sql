﻿CREATE TABLE [dbo].[Majax](
	[mjx_id] [int] IDENTITY(0,1) NOT NULL,
	[mjx_name] [varchar](50) NOT NULL CONSTRAINT [DF__Majax__mjx_name]  DEFAULT (''),
	[mjx_startdate] [datetime] NOT NULL CONSTRAINT [DF__Majax__mjx_startdate]  DEFAULT (getdate()),
	[mjx_detailsid] [int] NOT NULL CONSTRAINT [DF__Majax__mjx_detailsid]  DEFAULT (0),
	[mjx_askifinincident] [bit] NOT NULL CONSTRAINT [DF__Majax__mjx_askifinincident]  DEFAULT (0),
	[mjx_enddate] [datetime] NOT NULL CONSTRAINT [DF__Majax__mjx_enddate]  DEFAULT ('2200/01/01'),
	[mjx_createdby] [int] NOT NULL CONSTRAINT [DF__Majax__mjx_createdby]  DEFAULT (0),
	[mjx_created] [datetime] NOT NULL CONSTRAINT [DF__Majax__mjx_created]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_CFG_DeptAttTypes](
	[dat_identity] [int] IDENTITY(0,1) NOT NULL,
	[dat_id] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptAttTypes__dat_id]  DEFAULT (0),
	[dat_deptid] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptAttTypes__dat_deptid]  DEFAULT (0),
	[dat_atttype] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptAttTypes__dat_atttype]  DEFAULT (0),
	[dat_active] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptAttTypes__dat_active]  DEFAULT (0),
	[dat_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptAttTypes__dat_createdby]  DEFAULT (0),
	[dat_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptAttTypes__dat_created]  DEFAULT (getdate()),
	[dat_updated] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptAttTypes__dat_updated]  DEFAULT (getdate()),
	[dat_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DeptAttTypes__dat_updatedby]  DEFAULT (0),
	[dat_deleted] [bit] NOT NULL CONSTRAINT [DF__aud_CFG_DeptAttTypes__dat_deleted]  DEFAULT (0)
) ON [PRIMARY]
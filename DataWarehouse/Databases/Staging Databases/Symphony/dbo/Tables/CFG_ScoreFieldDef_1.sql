﻿CREATE TABLE [dbo].[CFG_ScoreFieldDef](
	[sfd_id] [int] IDENTITY(0,1) NOT NULL,
	[sfd_FieldId] [int] NOT NULL CONSTRAINT [DF_CFG_ScoreFieldDef_sfd_FieldId]  DEFAULT (0),
	[sfd_ScoreFieldDisplayMode] [smallint] NOT NULL CONSTRAINT [DF_CFG_ScoreFieldDef_sfd_ScoreFieldDisplayMode]  DEFAULT (3),
	[sfd_created] [datetime] NOT NULL CONSTRAINT [DF_CFG_ScoreFieldDef_sfd_created]  DEFAULT (getdate()),
	[sfd_createdBy] [int] NOT NULL CONSTRAINT [DF_CFG_ScoreFieldDef_sfd_createdBy]  DEFAULT (0),
	[sfd_updated] [datetime] NOT NULL CONSTRAINT [DF_CFG_ScoreFieldDef_sfd_updated]  DEFAULT (getdate()),
	[sfd_UpdatedBy] [int] NOT NULL CONSTRAINT [DF_CFG_ScoreFieldDef_sfd_UpdatedBy]  DEFAULT (0)
) ON [PRIMARY]
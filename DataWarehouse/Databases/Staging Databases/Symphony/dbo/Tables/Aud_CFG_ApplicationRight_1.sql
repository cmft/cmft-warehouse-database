﻿CREATE TABLE [dbo].[Aud_CFG_ApplicationRight](
	[apr_identity] [int] IDENTITY(0,1) NOT NULL,
	[apr_ID] [int] NULL,
	[apr_grpid] [int] NULL,
	[apr_apptype] [tinyint] NULL,
	[apr_accesslevel] [tinyint] NULL,
	[apr_Createdby] [int] NULL,
	[apr_Created] [datetime] NULL,
	[apr_Updated] [datetime] NULL,
	[apr_UpdateBy] [int] NULL
) ON [PRIMARY]
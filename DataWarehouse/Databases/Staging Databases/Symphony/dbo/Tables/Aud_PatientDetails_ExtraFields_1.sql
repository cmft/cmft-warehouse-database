﻿CREATE TABLE [dbo].[Aud_PatientDetails_ExtraFields](
	[pdt_EF_ID] [int] IDENTITY(0,1) NOT NULL,
	[pdt_EF_extrafieldID] [int] NULL,
	[pdt_EF_FieldType] [smallint] NULL,
	[pdt_EF_FieldID] [int] NULL,
	[pdt_EF_pdtpid] [int] NULL,
	[pdt_EF_Value] [varchar](4000) NULL,
	[pdt_EF_DataCategory] [smallint] NULL,
	[pdt_EF_RecordID] [int] NULL,
	[pdt_EF_FieldCodeValues] [varchar](200) NULL,
	[pdt_EF_ComputationState] [tinyint] NULL,
	[pdt_EF_depId] [int] NULL,
	[pdt_EF_CPMessagesIDs] [varchar](200) NULL,
	[pdt_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Aud_PatientDetails_ExtraFields__pdt_EF_CreatedBy]  DEFAULT ((0)),
	[pdt_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_PatientDetails_ExtraFields__pdt_EF_Created]  DEFAULT (getdate()),
	[pdt_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_PatientDetails_ExtraFields__pdt_EF_updatedby]  DEFAULT ((0)),
	[pdt_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_PatientDetails_ExtraFields__pdt_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
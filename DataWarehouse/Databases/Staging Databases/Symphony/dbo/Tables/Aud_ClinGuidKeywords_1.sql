﻿CREATE TABLE [dbo].[Aud_ClinGuidKeywords](
	[cgk_identity] [int] IDENTITY(0,1) NOT NULL,
	[cgk_lkpid] [int] NOT NULL,
	[cgk_kwdid] [int] NOT NULL,
	[cgk_createdby] [int] NOT NULL,
	[cgk_created] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[EBRANCHS](
	[EBrPracid] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationCode] [varchar](255) NULL DEFAULT (''),
	[OrganisationName] [varchar](255) NULL DEFAULT (''),
	[PANSHAcode] [varchar](255) NULL DEFAULT (''),
	[SHAcode] [varchar](255) NULL DEFAULT (''),
	[AddressLine1] [varchar](255) NULL DEFAULT (''),
	[AddressLine2] [varchar](255) NULL DEFAULT (''),
	[AddressLine3] [varchar](255) NULL DEFAULT (''),
	[AddressLine4] [varchar](255) NULL DEFAULT (''),
	[AddressLine5] [varchar](255) NULL DEFAULT (''),
	[Postcode] [varchar](255) NULL DEFAULT (''),
	[OpenDate] [varchar](255) NULL DEFAULT (''),
	[CloseDate] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse1] [varchar](255) NULL DEFAULT (''),
	[OrgSubTypecode] [varchar](255) NULL DEFAULT (''),
	[ParentOrgCode] [varchar](255) NULL DEFAULT (''),
	[JoinParentDate] [varchar](255) NULL DEFAULT (''),
	[LeftParentDate] [varchar](255) NULL DEFAULT (''),
	[Telephone] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse2] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse3] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse4] [varchar](255) NULL DEFAULT (''),
	[Amendedrecordindicator] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse5] [varchar](255) NULL DEFAULT (''),
	[GovtOfficeRegionCode] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse6] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse7] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse8] [varchar](255) NULL DEFAULT (''),
	[ebrpracmatchfound] [bit] NOT NULL DEFAULT ((0)),
	[StatusCode] [varchar](5) NULL DEFAULT (''),
	[NewOrgCode] [varchar](10) NULL DEFAULT (''),
	[brpraddid] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Assets](
	[ass_id] [int] IDENTITY(0,1) NOT NULL,
	[ass_OwnerId] [int] NOT NULL CONSTRAINT [DF__Assets__ass_OwnerId]  DEFAULT (0),
	[ass_assetid] [int] NOT NULL CONSTRAINT [DF__Assets__ass_assetid]  DEFAULT (0),
	[ass_status] [smallint] NOT NULL CONSTRAINT [DF__Assets__ass_status]  DEFAULT (0),
	[ass_createdby] [int] NOT NULL CONSTRAINT [DF__Assets__ass_createdby]  DEFAULT (0),
	[ass_updated] [datetime] NOT NULL CONSTRAINT [DF__Assets__ass_updated]  DEFAULT (getdate()),
	[ass_issuingDept] [int] NOT NULL CONSTRAINT [DF__Assets__ass_issuingDept]  DEFAULT (0),
	[ass_printjobids] [varchar](1000) NOT NULL CONSTRAINT [DF__Assets__ass_printjobids]  DEFAULT (''),
	[ass_datecreated] [datetime] NOT NULL CONSTRAINT [DF__Assets__ass_datecreated]  DEFAULT (getdate()),
	[ass_updatedby] [int] NOT NULL CONSTRAINT [DF__Assets__ass_updatedby]  DEFAULT (0),
	[ass_serialnumber1] [varchar](30) NOT NULL CONSTRAINT [DF__Assets_ass_serialnumber1]  DEFAULT (''),
	[ass_serialnumber2] [varchar](30) NOT NULL CONSTRAINT [DF__Assets_ass_serialnumber2]  DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_Locations](
	[loc_id] [smallint] IDENTITY(0,1) NOT NULL,
	[loc_parentid] [smallint] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_parentid]  DEFAULT (0),
	[loc_name] [varchar](100) NOT NULL CONSTRAINT [DF__CFG_Locations__loc_name]  DEFAULT (''),
	[loc_index] [smallint] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_index]  DEFAULT (0),
	[loc_active] [tinyint] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_active]  DEFAULT (0),
	[loc_description] [varchar](255) NOT NULL CONSTRAINT [DF__CFG_Locations__loc_description]  DEFAULT (''),
	[loc_deptid] [int] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_deptid]  DEFAULT (0),
	[loc_used] [bit] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_used]  DEFAULT (0),
	[loc_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_createdby]  DEFAULT (0),
	[loc_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_update]  DEFAULT (getdate()),
	[loc_reasonforsuspension] [varchar](255) NOT NULL CONSTRAINT [DF__CFG_Locations__loc_reasonforsuspension]  DEFAULT (''),
	[loc_suspendedon] [datetime] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_suspendedon]  DEFAULT ('2200/01/01'),
	[loc_suspendedby] [int] NOT NULL CONSTRAINT [DF__CFG_Locations__loc_suspendedby]  DEFAULT (0),
	[loc_MaxPatCategory] [smallint] NOT NULL CONSTRAINT [DF__cfg_locations__loc_MaxPatCategory]  DEFAULT ((0)),
	[loc_MPCValue] [int] NOT NULL CONSTRAINT [DF__cfg_locations__loc_MPCValue]  DEFAULT ((0))
) ON [PRIMARY]
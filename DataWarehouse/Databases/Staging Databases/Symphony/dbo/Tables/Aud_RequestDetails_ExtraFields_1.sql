﻿CREATE TABLE [dbo].[Aud_RequestDetails_ExtraFields](
	[req_EF_ID] [int] IDENTITY(0,1) NOT NULL,
	[req_EF_extrafieldID] [int] NULL,
	[req_EF_FieldType] [smallint] NULL,
	[req_EF_FieldID] [int] NULL,
	[req_EF_atdid] [int] NULL,
	[req_EF_Value] [varchar](4000) NULL,
	[req_EF_DataCategory] [smallint] NULL,
	[req_EF_RecordID] [int] NULL,
	[req_EF_FieldCodeValues] [varchar](200) NULL,
	[Req_EF_ComputationState] [tinyint] NULL,
	[Req_EF_depId] [smallint] NULL,
	[Req_EF_CPMessagesIDs] [varchar](200) NULL,
	[req_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Aud_RequestDetails_ExtraFields__req_EF_CreatedBy]  DEFAULT ((0)),
	[req_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_RequestDetails_ExtraFields__req_EF_Created]  DEFAULT (getdate()),
	[req_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_RequestDetails_ExtraFields__req_EF_updatedby]  DEFAULT ((0)),
	[req_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_RequestDetails_ExtraFields__req_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
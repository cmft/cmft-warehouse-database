﻿CREATE TABLE [dbo].[Patient_system_ids](
	[psi_id] [int] IDENTITY(0,1) NOT NULL,
	[psi_pid] [int] NOT NULL CONSTRAINT [DF__Patient_System_ids__psi_pid]  DEFAULT (0),
	[psi_system_name] [int] NOT NULL CONSTRAINT [DF__Patient_System_ids__psi_system_name]  DEFAULT (0),
	[psi_system_id] [varchar](30) NOT NULL CONSTRAINT [DF__Patient_System_ids__psi_system_id]  DEFAULT (''),
	[psi_status] [int] NOT NULL CONSTRAINT [DF__Patient_System_ids__psi_status]  DEFAULT (0),
	[psi_createdby] [int] NOT NULL CONSTRAINT [DF__Patient_System_ids__psi_createdby]  DEFAULT (0),
	[psi_update] [datetime] NOT NULL CONSTRAINT [DF__Patient_System_ids__psi_update]  DEFAULT (getdate()),
	[psi_system_idrev] [varchar](30) NOT NULL CONSTRAINT [DF__Patient_system_ids__psi_system_idrev]  DEFAULT ('')
) ON [PRIMARY]
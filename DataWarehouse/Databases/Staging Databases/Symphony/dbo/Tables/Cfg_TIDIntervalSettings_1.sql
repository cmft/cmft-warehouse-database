﻿CREATE TABLE [dbo].[Cfg_TIDIntervalSettings](
	[Tds_ID] [int] IDENTITY(0,1) NOT NULL,
	[Tds_TimeInDept] [int] NOT NULL CONSTRAINT [DF_Cfg_TIDIntervalSettings_TimeInDept]  DEFAULT (0),
	[Tds_Colour] [int] NOT NULL CONSTRAINT [DF_Cfg_TIDIntervalSettings_Tds_Colour]  DEFAULT (0),
	[Tds_DeptID] [int] NOT NULL CONSTRAINT [DF_Cfg_TIDIntervalSettings_Tds_DeptID]  DEFAULT (0),
	[Tds_Createdby] [int] NOT NULL CONSTRAINT [DF_Cfg_TIDIntervalSettings_Tds_Createdby]  DEFAULT (0),
	[Tds_Updated] [datetime] NOT NULL CONSTRAINT [DF_Cfg_TIDIntervalSettings_Tds_Updated]  DEFAULT (getdate()),
	[Tds_Created] [datetime] NOT NULL CONSTRAINT [DF_Cfg_TIDIntervalSettings_Tds_Created]  DEFAULT (getdate()),
	[Tds_Updatedby] [int] NOT NULL CONSTRAINT [DF_Cfg_TIDIntervalSettings_Tds_Updateby]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DAD_SaveMsg](
	[dsm_id] [int] IDENTITY(0,1) NOT NULL,
	[dsm_message] [varchar](4000) NOT NULL CONSTRAINT [DF__DAD_SaveMsg__dsm_message]  DEFAULT (''),
	[dsm_failed] [bit] NOT NULL CONSTRAINT [DF__DAD_SaveMsg__dsm_failed]  DEFAULT (0),
	[dsm_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_SaveMsg__dsm_createdby]  DEFAULT (0),
	[dsm_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_SaveMsg__dsm_created]  DEFAULT (getdate())
) ON [PRIMARY]
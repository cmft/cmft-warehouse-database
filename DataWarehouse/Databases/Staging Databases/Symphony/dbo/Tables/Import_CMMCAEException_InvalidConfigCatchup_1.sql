﻿CREATE TABLE [dbo].[Import_CMMCAEException_InvalidConfigCatchup](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[DataSETName] [varchar](50) NULL,
	[Health Organisation Id] [varchar](50) NULL,
	[Patient ID] [varchar](50) NULL,
	[AENo] [varchar](50) NULL,
	[AttendDate] [varchar](50) NULL,
	[Seen Date] [varchar](50) NULL,
	[Complaint Code 1] [varchar](50) NULL,
	[Complaint Code 2] [varchar](50) NULL,
	[Complaint Code 3] [varchar](50) NULL,
	[Consultant] [varchar](50) NULL,
	[Diagnosis Date] [varchar](50) NULL,
	[Diagnosis Code] [varchar](50) NULL,
	[Method Of Departure] [varchar](50) NULL,
	[Date of Departure] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_PatientConditions](
	[cnd_id] [int] IDENTITY(0,1) NOT NULL,
	[cnd_conditionid] [int] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_conditionid]  DEFAULT (0),
	[cnd_conditiontype] [tinyint] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_conditiontype]  DEFAULT (0),
	[cnd_depid] [smallint] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_depid]  DEFAULT (0),
	[cnd_defid] [int] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_defid]  DEFAULT (0),
	[cnd_operator] [int] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_operator]  DEFAULT (0),
	[cnd_value] [varchar](255) NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_value]  DEFAULT (''),
	[cnd_index] [int] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_index]  DEFAULT (0),
	[cnd_conditionjoin] [tinyint] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_conditionjoin]  DEFAULT (0),
	[cnd_evaluationtype] [tinyint] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_evaluationtype]  DEFAULT (0),
	[cnd_recordtype] [tinyint] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_recordtype]  DEFAULT (0),
	[cnd_deleted] [bit] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_deleted]  DEFAULT (0),
	[cnd_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_createdby]  DEFAULT (0),
	[cnd_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_created]  DEFAULT (getdate()),
	[cnd_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_PatientConditions__cnd_update]  DEFAULT (getdate())
) ON [PRIMARY]
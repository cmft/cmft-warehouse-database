﻿CREATE TABLE [dbo].[MappingTypes](
	[mt_id] [int] IDENTITY(0,1) NOT NULL,
	[mt_name] [varchar](200) NOT NULL CONSTRAINT [DF__MappingTypes__Mt_name]  DEFAULT (''),
	[mt_description] [varchar](200) NOT NULL CONSTRAINT [DF__MappingTypes__Mt_Description]  DEFAULT (''),
	[mt_used] [bit] NOT NULL CONSTRAINT [DF__MappingTypes__Mt_used]  DEFAULT (0),
	[mt_createdby] [int] NOT NULL CONSTRAINT [DF__MappingTypes__Mt_createdby]  DEFAULT (0),
	[mt_datecreated] [datetime] NOT NULL CONSTRAINT [DF__MappingTypes__Mt_datecreated]  DEFAULT (getdate())
) ON [PRIMARY]
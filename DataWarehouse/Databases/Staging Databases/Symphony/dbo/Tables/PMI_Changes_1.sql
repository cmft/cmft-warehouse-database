﻿CREATE TABLE [dbo].[PMI_Changes](
	[pmi_id] [int] NOT NULL,
	[pmi_transactiontype] [tinyint] NOT NULL,
	[pmi_uid] [varchar](50) NOT NULL,
	[pmi_mergeuid] [varchar](50) NOT NULL,
	[pmi_NHSNum] [varchar](17) NOT NULL,
	[pmi_surname] [varchar](35) NOT NULL,
	[pmi_title] [varchar](30) NOT NULL,
	[pmi_forename] [varchar](35) NOT NULL,
	[pmi_midnames] [varchar](35) NOT NULL,
	[pmi_sex] [varchar](30) NOT NULL,
	[pmi_dob] [datetime] NOT NULL,
	[pmi_dobtype] [varchar](30) NOT NULL,
	[pmi_gpid] [varchar](8) NOT NULL,
	[pmi_practise] [varchar](6) NOT NULL,
	[pmi_praddid] [int] NOT NULL,
	[pmi_dod] [datetime] NOT NULL,
	[pmi_maritalstatus] [varchar](30) NOT NULL,
	[pmi_religion] [varchar](30) NOT NULL,
	[pmi_ethnic] [varchar](30) NOT NULL,
	[pmi_occupation] [varchar](30) NOT NULL,
	[pmi_employmentstatus] [varchar](30) NOT NULL,
	[pmi_overseas] [varchar](30) NOT NULL,
	[pmi_nationality] [varchar](30) NOT NULL,
	[pmi_address1] [varchar](35) NOT NULL,
	[pmi_address2] [varchar](35) NOT NULL,
	[pmi_address3] [varchar](35) NOT NULL,
	[pmi_address4] [varchar](35) NOT NULL,
	[pmi_address5] [varchar](35) NOT NULL,
	[pmi_postcode] [varchar](8) NOT NULL,
	[pmi_dha] [varchar](4) NOT NULL,
	[pmi_pcg] [varchar](5) NOT NULL,
	[pmi_hometel] [varchar](25) NOT NULL,
	[pmi_worktel] [varchar](25) NOT NULL,
	[pmi_mobiletel] [varchar](25) NOT NULL,
	[pmi_noksurname] [varchar](35) NOT NULL,
	[pmi_nokforename] [varchar](35) NOT NULL,
	[pmi_nokmidname] [varchar](35) NOT NULL,
	[pmi_noktitle] [varchar](30) NOT NULL,
	[pmi_nokrelation] [varchar](30) NOT NULL,
	[pmi_nokaddress1] [varchar](50) NOT NULL,
	[pmi_nokaddress2] [varchar](50) NOT NULL,
	[pmi_nokaddress3] [varchar](50) NOT NULL,
	[pmi_nokaddress4] [varchar](50) NOT NULL,
	[pmi_nokaddress5] [varchar](50) NOT NULL,
	[pmi_nokpostcode] [varchar](8) NOT NULL,
	[pmi_nokhometel] [varchar](25) NOT NULL,
	[pmi_nokworktel] [varchar](25) NOT NULL,
	[pmi_transtatus] [tinyint] NOT NULL,
	[pmi_tranerror] [varchar](100) NOT NULL,
	[pmi_tempreg] [int] NOT NULL,
	[pmi_createdby] [int] NOT NULL,
	[msrepl_synctran_ts] [binary](8) NULL
) ON [PRIMARY]
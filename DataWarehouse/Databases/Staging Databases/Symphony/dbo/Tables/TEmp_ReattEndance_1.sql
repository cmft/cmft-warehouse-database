﻿CREATE TABLE [dbo].[TEmp_ReattEndance](
	[aeno] [varchar](50) NULL,
	[attEnddate] [varchar](50) NULL,
	[oid] [int] NULL,
	[fAtdNum] [varchar](25) NULL,
	[formatted] [smallint] NULL,
	[rowid] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[NPRACCUR](
	[NPracid] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationCode] [varchar](255) NULL DEFAULT (''),
	[OrganisationName] [varchar](255) NULL DEFAULT (''),
	[ROcode] [varchar](255) NULL DEFAULT (''),
	[HAcode] [varchar](255) NULL DEFAULT (''),
	[AddressLine1] [varchar](255) NULL DEFAULT (''),
	[AddressLine2] [varchar](255) NULL DEFAULT (''),
	[AddressLine3] [varchar](255) NULL DEFAULT (''),
	[AddressLine4] [varchar](255) NULL DEFAULT (''),
	[AddressLine5] [varchar](255) NULL DEFAULT (''),
	[Postcode] [varchar](255) NULL DEFAULT (''),
	[OpenDate] [varchar](255) NULL DEFAULT (''),
	[CloseDate] [varchar](255) NULL DEFAULT (''),
	[StatusCode] [varchar](255) NULL DEFAULT (''),
	[OrgSubTypecode] [varchar](255) NULL DEFAULT (''),
	[ParentOrgCode] [varchar](255) NULL DEFAULT (''),
	[JoinParentDate] [varchar](255) NULL DEFAULT (''),
	[LeftParentDate] [varchar](255) NULL DEFAULT (''),
	[Telephone] [varchar](255) NULL DEFAULT (''),
	[ContactName] [varchar](255) NULL DEFAULT (''),
	[AddressType] [varchar](255) NULL DEFAULT (''),
	[Field0] [varchar](255) NULL DEFAULT (''),
	[Amendedrecordindicator] [varchar](255) NULL DEFAULT (''),
	[WaveNumber] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse1] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse2] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse3] [varchar](255) NULL DEFAULT (''),
	[Availableforfutureuse4] [varchar](255) NULL DEFAULT (''),
	[praddid] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
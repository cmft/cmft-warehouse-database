﻿CREATE TABLE [dbo].[Filter](
	[flt_id] [int] IDENTITY(0,1) NOT NULL,
	[flt_name] [varchar](50) NOT NULL CONSTRAINT [DF__Filter__flt_name]  DEFAULT (''),
	[flt_description] [varchar](200) NOT NULL CONSTRAINT [DF__Filter__flt_description]  DEFAULT (''),
	[flt_ufvalue] [varchar](2000) NOT NULL CONSTRAINT [DF__Filter__flt_ufvalue]  DEFAULT (''),
	[flt_value] [varchar](5000) NOT NULL CONSTRAINT [DF__Filter__flt_value]  DEFAULT (''),
	[flt_DefaultFilter] [bit] NOT NULL CONSTRAINT [DF__Filter__flt_DefaultFilter]  DEFAULT (0),
	[flt_FilterType] [smallint] NOT NULL CONSTRAINT [DF__Filter__flt_FilterType]  DEFAULT (0),
	[flt_deptid] [int] NOT NULL CONSTRAINT [DF__Filter__flt_deptid]  DEFAULT (0),
	[flt_createdby] [int] NOT NULL CONSTRAINT [DF__Filter__flt_createdby]  DEFAULT (0),
	[flt_update] [datetime] NOT NULL CONSTRAINT [DF__Filter__flt_updated]  DEFAULT (getdate()),
	[flt_DashBoardDisplayFilter] [bit] NOT NULL CONSTRAINT [DF__Filter__flt_DashBoardDisplayFilter]  DEFAULT (0),
	[flt_TidDPatientGroupFilter] [bit] NOT NULL CONSTRAINT [DF__Filter__flt_TidDPatientGroupFilter]  DEFAULT (0),
	[flt_WTDPatientGroupFilter] [bit] NOT NULL CONSTRAINT [DF__Filter__flt_WTDPatientGroupFilter]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_CFG_DEForms](
	[def_identity] [int] IDENTITY(1,1) NOT NULL,
	[def_id] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_id]  DEFAULT (0),
	[def_name] [varchar](100) NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_name]  DEFAULT (''),
	[def_caption] [varchar](100) NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_caption]  DEFAULT (''),
	[def_hastoolbar] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_hastoolbar]  DEFAULT (0),
	[def_columns] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_columns]  DEFAULT (0),
	[def_gpletter] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_gpletter]  DEFAULT (1),
	[def_helptext] [varchar](255) NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_helptext]  DEFAULT (''),
	[def_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_createdby]  DEFAULT (0),
	[def_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_update]  DEFAULT (getdate()),
	[def_type] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_typw]  DEFAULT (0),
	[def_active] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForm__Def_Active]  DEFAULT (1),
	[def_displaymode] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_displaymode]  DEFAULT (0),
	[def_usepsvs] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_usepsvs]  DEFAULT (0),
	[def_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEForms__def_deleted]  DEFAULT (0),
	[def_dnmavailable] [bit] NULL,
	[Def_hasflag] [bit] NOT NULL
) ON [PRIMARY]
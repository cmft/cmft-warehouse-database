﻿CREATE TABLE [dbo].[Aud_CFG_MajaxDepts](
	[cmd_identity] [int] IDENTITY(0,1) NOT NULL,
	[cmd_majaxid] [int] NOT NULL,
	[cmd_deptid] [int] NOT NULL,
	[cmd_patients] [smallint] NOT NULL,
	[cmd_createdby] [int] NOT NULL,
	[cmd_created] [datetime] NOT NULL,
	[cmd_updated] [datetime] NOT NULL,
	[cmd_updatedby] [int] NOT NULL,
	[cmd_deleted] [bit] NOT NULL
) ON [PRIMARY]
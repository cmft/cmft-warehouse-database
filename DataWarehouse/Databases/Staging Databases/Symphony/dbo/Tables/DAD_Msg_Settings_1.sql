﻿CREATE TABLE [dbo].[DAD_Msg_Settings](
	[mst_defid] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_Settings__cst_defid]  DEFAULT (0),
	[mst_name] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Msg_Settings__mst_name]  DEFAULT (''),
	[mst_value] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Msg_Settings__mst_value]  DEFAULT (''),
	[mst_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_Settings__mst_createdby]  DEFAULT (0),
	[mst_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Msg_Settings__mst_created]  DEFAULT (getdate())
) ON [PRIMARY]
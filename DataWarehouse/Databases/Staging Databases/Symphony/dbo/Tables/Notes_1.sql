﻿CREATE TABLE [dbo].[Notes](
	[not_noteid] [int] IDENTITY(0,1) NOT NULL,
	[not_text] [varchar](4000) NOT NULL CONSTRAINT [DF__Notes__not_text]  DEFAULT (''),
	[not_recordedby] [int] NOT NULL CONSTRAINT [DF__Notes__not_recordedy]  DEFAULT (0),
	[not_createdby] [int] NOT NULL CONSTRAINT [DF__Notes__not_createdby]  DEFAULT (0),
	[not_update] [datetime] NOT NULL CONSTRAINT [DF__Notes__not_update]  DEFAULT (getdate())
) ON [PRIMARY]
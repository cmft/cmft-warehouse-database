﻿CREATE TABLE [dbo].[CFG_ApplicationRight](
	[apr_ID] [int] IDENTITY(0,1) NOT NULL,
	[apr_grpid] [int] NOT NULL CONSTRAINT [DF__CFG_ApplicationRight__apr_grpid]  DEFAULT (0),
	[apr_apptype] [tinyint] NOT NULL CONSTRAINT [DF__CFG_ApplicationRight__apr_apptype]  DEFAULT (0),
	[apr_accesslevel] [tinyint] NOT NULL CONSTRAINT [DF__CFG_ApplicationRight__apr_accesslevel]  DEFAULT (0),
	[apr_Createdby] [int] NOT NULL CONSTRAINT [DF__CFG_ApplicationRight__apr_Createdby]  DEFAULT (0),
	[apr_Created] [datetime] NOT NULL CONSTRAINT [DF__CFG_ApplicationRight__apr_Created]  DEFAULT (getdate()),
	[apr_Updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_ApplicationRight__apr_Updated]  DEFAULT (getdate()),
	[apr_UpdateBy] [int] NOT NULL CONSTRAINT [DF__CFG_ApplicationRight__apr_UpdateBy]  DEFAULT (0)
) ON [PRIMARY]
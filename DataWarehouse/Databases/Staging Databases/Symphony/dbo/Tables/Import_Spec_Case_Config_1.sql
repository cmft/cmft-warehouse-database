﻿CREATE TABLE [dbo].[Import_Spec_Case_Config](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[DataSetName] [varchar](50) NULL DEFAULT (''),
	[Health Organisation Id] [varchar](50) NULL DEFAULT (''),
	[Code] [varchar](50) NULL DEFAULT (''),
	[Description] [varchar](50) NULL DEFAULT (''),
	[DeliveredDate] [varchar](50) NULL DEFAULT (''),
	[LkpID] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
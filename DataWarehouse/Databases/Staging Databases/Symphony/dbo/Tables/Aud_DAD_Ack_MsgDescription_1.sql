﻿CREATE TABLE [dbo].[Aud_DAD_Ack_MsgDescription](
	[dam_identity] [int] IDENTITY(0,1) NOT NULL,
	[dam_msgdefid] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Ack_MsgDescription__dam_msgdefid]  DEFAULT (0),
	[dam_description] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_DAD_Ack_MsgDescription__dam_description]  DEFAULT (''),
	[dam_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Ack_MsgDescription__dsp_createdby]  DEFAULT (0),
	[dam_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DAD_Ack_MsgDescription__dam_created]  DEFAULT (getdate()),
	[Dam_ResendOnUpdate] [bit] NOT NULL CONSTRAINT [DF__Aud_DAD_Ack_MsgDescription__Dam_ResendOnUpdate]  DEFAULT ((0)),
	[Dam_StopQueue] [bit] NOT NULL CONSTRAINT [DF__Aud_DAD_Ack_MsgDescription__Dam_StopQueue]  DEFAULT ((0))
) ON [PRIMARY]
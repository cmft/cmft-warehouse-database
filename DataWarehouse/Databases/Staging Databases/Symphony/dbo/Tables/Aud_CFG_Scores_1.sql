﻿CREATE TABLE [dbo].[Aud_CFG_Scores](
	[sc_Identity] [int] IDENTITY(0,1) NOT NULL,
	[sc_Id] [int] NULL,
	[sc_ScoreFieldDefId] [int] NULL,
	[sc_Score] [int] NULL,
	[sc_Description] [varchar](4000) NULL,
	[sc_Index] [smallint] NULL,
	[sc_Created] [datetime] NULL,
	[sc_CreatedBy] [int] NULL,
	[sc_Updated] [datetime] NULL,
	[sc_UpdatedBy] [int] NULL,
	[sc_deleted] [int] NULL
) ON [PRIMARY]
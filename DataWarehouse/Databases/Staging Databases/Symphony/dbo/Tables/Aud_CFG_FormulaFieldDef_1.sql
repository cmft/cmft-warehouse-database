﻿CREATE TABLE [dbo].[Aud_CFG_FormulaFieldDef](
	[ffd_Identity] [int] IDENTITY(0,1) NOT NULL,
	[ffd_ID] [int] NULL,
	[ffd_ParentID] [int] NULL,
	[ffd_DisplayInScoreViewer] [bit] NULL,
	[ffd_PopUpResult] [bit] NULL,
	[ffd_Use1000Separator] [bit] NULL,
	[ffd_DecimalPlacesNo] [smallint] NULL,
	[ffd_DisplayIndFieldValues] [bit] NULL,
	[ffd_FieldCodeSeparator] [varchar](1) NULL,
	[ffd_Created] [datetime] NULL,
	[ffd_CreatedBy] [int] NULL,
	[ffd_Updated] [datetime] NULL,
	[ffd_UpdatedBy] [int] NULL,
	[ffd_deleted] [bit] NULL
) ON [PRIMARY]
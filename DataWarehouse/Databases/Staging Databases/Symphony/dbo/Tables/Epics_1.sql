﻿CREATE TABLE [dbo].[Epics](
	[epi_epicsid] [int] IDENTITY(0,1) NOT NULL,
	[epi_imageid] [int] NOT NULL CONSTRAINT [DF_Epics_epi_imageid]  DEFAULT (0),
	[epi_uploaddate] [datetime] NOT NULL CONSTRAINT [DF_Epics_epi_uploaddate]  DEFAULT (getdate()),
	[epi_notes] [int] NOT NULL CONSTRAINT [DF_Epics_epi_notes]  DEFAULT (0),
	[epi_parentno] [tinyint] NOT NULL CONSTRAINT [DF_Epics_epi_parentno]  DEFAULT (0),
	[epi_parentid] [int] NOT NULL CONSTRAINT [DF_Epics_epi_parentid]  DEFAULT (0),
	[epi_categories] [varchar](150) NOT NULL CONSTRAINT [DF_Epics_epi_categories]  DEFAULT (''),
	[epi_deleted] [bit] NOT NULL CONSTRAINT [DF_Epics_epi_deleted]  DEFAULT (0),
	[epi_createdby] [int] NOT NULL CONSTRAINT [DF_Epics_epi_createdby]  DEFAULT (0),
	[epi_update] [datetime] NOT NULL CONSTRAINT [DF_Epics_epi_update]  DEFAULT (getdate()),
	[Epi_ImagePath] [varchar](255) NOT NULL CONSTRAINT [DF__Epics__epi_ImagePath]  DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_ReportSettings](
	[rps_id] [int] IDENTITY(0,1) NOT NULL,
	[rps_name] [varchar](50) NOT NULL CONSTRAINT [df__cfg_reportsettings__rps_name]  DEFAULT (''),
	[rps_value] [int] NULL CONSTRAINT [df__cfg_reportsettings__rps_value]  DEFAULT (0),
	[rps_deptid] [int] NOT NULL CONSTRAINT [df__cfg_reportsettings__rps_deptid]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_FormulaFieldDef](
	[ffd_ID] [int] IDENTITY(0,1) NOT NULL,
	[ffd_ParentID] [int] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_ParentID]  DEFAULT (0),
	[ffd_DisplayInScoreViewer] [bit] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_DisplayInScoreViewer]  DEFAULT (1),
	[ffd_PopUpResult] [bit] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_PopUpResult]  DEFAULT (1),
	[ffd_Use1000Separator] [bit] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_Use1000Separator]  DEFAULT (0),
	[ffd_DecimalPlacesNo] [smallint] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_DecimalPlacesNo]  DEFAULT (2),
	[ffd_DisplayIndFieldValues] [bit] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_DisplayIndFieldValues]  DEFAULT (0),
	[ffd_FieldCodeSeparator] [varchar](1) NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_FieldCodeSeparator]  DEFAULT (''),
	[ffd_Created] [datetime] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_Created]  DEFAULT (getdate()),
	[ffd_CreatedBy] [int] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_CreatedBy]  DEFAULT (0),
	[ffd_Updated] [datetime] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_Updated]  DEFAULT (getdate()),
	[ffd_UpdatedBy] [int] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldDef_ffd_UpdatedBy]  DEFAULT (0)
) ON [PRIMARY]
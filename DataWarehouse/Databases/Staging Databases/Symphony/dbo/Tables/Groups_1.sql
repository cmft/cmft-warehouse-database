﻿CREATE TABLE [dbo].[Groups](
	[grp_grpid] [int] IDENTITY(0,1) NOT NULL,
	[grp_grpname] [varchar](30) NOT NULL CONSTRAINT [DF__Groups__grp_grpname]  DEFAULT (''),
	[grp_description] [varchar](4000) NOT NULL CONSTRAINT [DF__Groups__grp_description]  DEFAULT (''),
	[grp_systemlock] [int] NOT NULL CONSTRAINT [DF__Groups__grp_systemlock]  DEFAULT (0),
	[grp_editrestrict] [int] NOT NULL CONSTRAINT [DF__Groups__grp_editrestrict]  DEFAULT (0),
	[grp_deptid] [int] NOT NULL CONSTRAINT [DF__Groups__grp_deptid]  DEFAULT (0),
	[grp_admin] [bit] NOT NULL CONSTRAINT [DF__Groups__grp_admin]  DEFAULT (0),
	[grp_createdby] [int] NOT NULL CONSTRAINT [DF__Groups__grp_createdby]  DEFAULT (0),
	[grp_update] [datetime] NOT NULL CONSTRAINT [DF__Groups__grp_update]  DEFAULT (getdate()),
	[grp_created] [datetime] NOT NULL CONSTRAINT [DF__Groups__grp_created]  DEFAULT (getdate()),
	[grp_deleted] [bit] NOT NULL CONSTRAINT [DF__Groups__grp_deleted]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_Patient_Indicators](
	[pin_identity] [int] IDENTITY(0,1) NOT NULL,
	[pin_id] [int] NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__pin_id]  DEFAULT (0),
	[pin_pid] [int] NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__pin_pid]  DEFAULT (0),
	[pin_type] [int] NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__pin_type]  DEFAULT (0),
	[pin_indicator] [int] NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__pin_indicator]  DEFAULT (0),
	[pin_freetext] [varchar](50) NULL CONSTRAINT [DF__Aud_Patient_indicators__pin_freetext]  DEFAULT (''),
	[pin_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__pin_createdby]  DEFAULT (0),
	[pin_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__pin_update]  DEFAULT (getdate()),
	[pin_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__pin_deleted]  DEFAULT (0),
	[Pin_expirydate] [datetime] NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__Pin_expirydate]  DEFAULT ('01/01/2200'),
	[Pin_TPID] [varchar](20) NOT NULL CONSTRAINT [DF__Aud_Patient_Indicators__Pin_TPID]  DEFAULT (''),
	[pin_created] [datetime] NULL,
	[pin_updatedby] [int] NULL DEFAULT (0)
) ON [PRIMARY]
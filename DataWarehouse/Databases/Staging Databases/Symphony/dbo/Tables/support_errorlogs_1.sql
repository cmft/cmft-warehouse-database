﻿CREATE TABLE [dbo].[support_errorlogs](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[el_type] [varchar](255) NULL,
	[el_value] [varchar](255) NULL,
	[el_created] [datetime] NULL DEFAULT (getdate()-(1))
) ON [PRIMARY]
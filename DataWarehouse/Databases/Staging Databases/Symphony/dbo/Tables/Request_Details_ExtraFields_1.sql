﻿CREATE TABLE [dbo].[Request_Details_ExtraFields](
	[req_EF_extrafieldID] [int] IDENTITY(0,1) NOT NULL,
	[req_EF_FieldType] [smallint] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__FieldType]  DEFAULT (0),
	[req_EF_FieldID] [int] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__FieldID]  DEFAULT (0),
	[req_EF_atdid] [int] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__atdid]  DEFAULT (0),
	[req_EF_Value] [varchar](4000) NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__Value]  DEFAULT (''),
	[req_EF_DataCategory] [smallint] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__DataCategory]  DEFAULT (0),
	[req_EF_RecordID] [int] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__RecordID]  DEFAULT (0),
	[req_EF_FieldCodeValues] [varchar](200) NOT NULL CONSTRAINT [DF_Request_Details_ExtraFields_req_EF_FieldCodeValues]  DEFAULT (''),
	[Req_EF_ComputationState] [tinyint] NOT NULL CONSTRAINT [DF_Request_Details_ExtraFields_Req_EF_ComputationState]  DEFAULT (0),
	[Req_EF_depId] [smallint] NOT NULL CONSTRAINT [DF_Request_Details_ExtraFields_Req_EF_depId]  DEFAULT ((0)),
	[Req_EF_CPMessagesIDs] [varchar](200) NOT NULL CONSTRAINT [DF_Request_Details_ExtraFields_Req_EF_CPMessagesIDs]  DEFAULT (''),
	[req_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__req_EF_CreatedBy]  DEFAULT ((0)),
	[req_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__req_EF_Created]  DEFAULT (getdate()),
	[req_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__req_EF_updatedby]  DEFAULT ((0)),
	[req_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Request_Details_ExtraFields__req_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_DEPPrintJobs](
	[dpj_pjbid] [int] NOT NULL CONSTRAINT [DF__CFG_DEPPrintJobs__dpj_pjbid]  DEFAULT (0),
	[dpj_depid] [smallint] NOT NULL CONSTRAINT [DF__CFG_DEPPrintJobs__dpj_depid]  DEFAULT (0),
	[dpj_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_DEPPrintJobs__dpj_update]  DEFAULT (getdate()),
	[dpj_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DEPPrintJobs__dpj_createdby]  DEFAULT (0)
) ON [PRIMARY]
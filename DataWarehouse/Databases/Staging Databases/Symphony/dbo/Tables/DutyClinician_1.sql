﻿CREATE TABLE [dbo].[DutyClinician](
	[dcl_id] [int] IDENTITY(0,1) NOT NULL,
	[dcl_staffid] [int] NOT NULL CONSTRAINT [DF__DutyClinician__dcl_staffid]  DEFAULT (0),
	[dcl_startDate] [datetime] NOT NULL CONSTRAINT [DF_DutyClinician_dcl_startDate]  DEFAULT (getdate()),
	[dcl_enddate] [datetime] NOT NULL CONSTRAINT [DF_DutyClinician_dcl_endDate]  DEFAULT (dateadd(day,1,getdate())),
	[dcl_created] [datetime] NOT NULL CONSTRAINT [DF_DutyClinician_dcl_created]  DEFAULT (getdate()),
	[dcl_updated] [datetime] NOT NULL CONSTRAINT [DF_DutyClinician_dcl_updated]  DEFAULT (getdate()),
	[dcl_createdby] [int] NOT NULL CONSTRAINT [DF_DutyClinician_dcl_createdby]  DEFAULT (0),
	[dcl_updatedby] [int] NOT NULL CONSTRAINT [DF_DutyClinician_dcl_updatedby]  DEFAULT (0)
) ON [PRIMARY]
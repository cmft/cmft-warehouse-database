﻿CREATE TABLE [dbo].[CDSInterchangeHeader_Table](
	[CDS_INT_SEND_ID] [varchar](15) NULL,
	[CDS_INT_REC_ID] [varchar](15) NULL,
	[CDS_INT_CRL_REF] [int] NULL,
	[CDS_INT_PREPDATE] [varchar](10) NOT NULL,
	[CDS_INT_PREPTIME] [varchar](8) NULL,
	[CDS_INT_APP_REF] [varchar](6) NOT NULL,
	[CDS_INT_TST_IND] [varchar](1) NOT NULL
) ON [PRIMARY]
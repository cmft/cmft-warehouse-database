﻿CREATE TABLE [dbo].[ProhibitedPasswords](
	[prb_id] [int] IDENTITY(0,1) NOT NULL,
	[prb_password] [varchar](30) NOT NULL CONSTRAINT [DF__ProhibitedPasswords__prb_password]  DEFAULT (''),
	[prb_createdby] [int] NOT NULL CONSTRAINT [DF__ProhibitedPasswords__prb_createdby]  DEFAULT (0),
	[prb_created] [datetime] NOT NULL CONSTRAINT [DF__ProhibitedPasswords__prb_created]  DEFAULT (getdate()),
	[prb_update] [datetime] NOT NULL CONSTRAINT [DF__ProhibitedPasswords__prb_update]  DEFAULT (getdate())
) ON [PRIMARY]
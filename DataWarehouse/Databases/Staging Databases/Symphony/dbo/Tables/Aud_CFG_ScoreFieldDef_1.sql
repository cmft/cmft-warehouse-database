﻿CREATE TABLE [dbo].[Aud_CFG_ScoreFieldDef](
	[sfd_identity] [int] IDENTITY(0,1) NOT NULL,
	[sfd_id] [int] NULL,
	[sfd_FieldId] [int] NULL,
	[sfd_ScoreFieldDisplayMode] [smallint] NULL,
	[sfd_created] [datetime] NULL,
	[sfd_createdBy] [int] NULL,
	[sfd_updated] [datetime] NULL,
	[sfd_UpdatedBy] [int] NULL,
	[sfd_deleted] [int] NULL
) ON [PRIMARY]
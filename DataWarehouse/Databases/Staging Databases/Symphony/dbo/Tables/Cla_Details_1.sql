﻿CREATE TABLE [dbo].[Cla_Details](
	[cld_id] [int] IDENTITY(0,1) NOT NULL,
	[cld_clininst] [int] NOT NULL CONSTRAINT [DF__Cla_Details__cld_clininst]  DEFAULT (0),
	[cld_atdid] [int] NOT NULL CONSTRAINT [DF__Cla_Details__cld_atdid]  DEFAULT (0),
	[cld_waitingfors] [varchar](2000) NOT NULL CONSTRAINT [DF__Cla_Details__cld_waitingfors]  DEFAULT (''),
	[cld_trackinggrids] [varchar](2500) NOT NULL CONSTRAINT [DF__Cla_Details__cld_trackinggrids]  DEFAULT (''),
	[cld_usedlookups] [nvarchar](3500) NOT NULL CONSTRAINT [DF__Cla_Details__cld_usedlookups]  DEFAULT ('')
) ON [PRIMARY]
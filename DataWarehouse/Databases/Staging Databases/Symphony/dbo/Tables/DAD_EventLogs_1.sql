﻿CREATE TABLE [dbo].[DAD_EventLogs](
	[del_ID] [int] IDENTITY(1,1) NOT NULL,
	[del_serviceID] [int] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_serviceID]  DEFAULT (0),
	[del_msgdefID] [int] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_msgdefID]  DEFAULT (0),
	[del_machine] [varchar](50) NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_machine]  DEFAULT (''),
	[del_date] [datetime] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_date]  DEFAULT (getdate()),
	[del_type] [tinyint] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_type]  DEFAULT (0),
	[del_event] [varchar](4000) NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_event]  DEFAULT (''),
	[del_serviceuser] [int] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_serviceuser]  DEFAULT (0),
	[del_deleted] [bit] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_deleted]  DEFAULT (0),
	[del_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_created]  DEFAULT (getdate()),
	[del_updated] [datetime] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_updated]  DEFAULT (getdate()),
	[del_updatedby] [int] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_updatedby]  DEFAULT (0),
	[del_emailed] [bit] NOT NULL CONSTRAINT [DF__DAD_EventLogs__del_emailed]  DEFAULT (0)
) ON [PRIMARY]
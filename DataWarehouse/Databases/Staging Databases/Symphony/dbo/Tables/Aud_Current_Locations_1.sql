﻿CREATE TABLE [dbo].[Aud_Current_Locations](
	[cul_identity] [int] IDENTITY(0,1) NOT NULL,
	[cul_culid] [int] NOT NULL CONSTRAINT [DF__Aud_Current_Locations__cul_culid]  DEFAULT (0),
	[cul_atdid] [int] NOT NULL CONSTRAINT [DF__Aud_Current_Locations__cul_atdid]  DEFAULT (0),
	[cul_locationid] [int] NOT NULL CONSTRAINT [DF__Aud_Current_Locations__cul_locationid]  DEFAULT (0),
	[cul_locationdate] [datetime] NOT NULL CONSTRAINT [DF__Aud_Current_Locations_cul__locationdate]  DEFAULT (getdate()),
	[cul_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Current_Locations__cul_createdby]  DEFAULT (0),
	[cul_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Current_Locations__cul_update]  DEFAULT (getdate()),
	[cul_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Current_Locations__cul_deleted]  DEFAULT (0),
	[Cul_ReasonForMPC] [int] NOT NULL CONSTRAINT [DF_Aud_Current_locations_Cul_ReasonForMPC]  DEFAULT ((0))
) ON [PRIMARY]
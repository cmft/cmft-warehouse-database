﻿CREATE TABLE [dbo].[DummyRequests](
	[drq_id] [int] IDENTITY(0,1) NOT NULL,
	[drq_recordid] [int] NOT NULL CONSTRAINT [DF__DummyRequests__drq_recordid]  DEFAULT (0),
	[drq_assocdepid] [smallint] NOT NULL CONSTRAINT [DF__DummyRequests__drq_assocdepid]  DEFAULT (0),
	[drq_maindepid] [smallint] NOT NULL CONSTRAINT [DF__DummyRequests__drq_maindepid]  DEFAULT (0),
	[drq_atdid] [int] NOT NULL CONSTRAINT [DF__DummyRequests__drq_drq_atdid]  DEFAULT (0),
	[drq_requestdate] [datetime] NOT NULL CONSTRAINT [DF__DummyRequests__drq_requestdate]  DEFAULT ('01/01/2200'),
	[drq_deleted] [bit] NOT NULL CONSTRAINT [DF__DummyRequests__drq_deleted]  DEFAULT (0),
	[drq_createdby] [int] NOT NULL CONSTRAINT [DF__DummyRequests__drq_createdby]  DEFAULT (0),
	[drq_updated] [datetime] NOT NULL CONSTRAINT [DF__DummyRequests__drq_updated]  DEFAULT (getdate()),
	[drq_SDID] [int] NOT NULL CONSTRAINT [DF_DummyRequests__drq_SDID]  DEFAULT (0),
	[drq_IsForInterDeptDEP] [bit] NOT NULL CONSTRAINT [DF_DummyRequests__drq_IsForInterDeptDEP]  DEFAULT (0)
) ON [PRIMARY]
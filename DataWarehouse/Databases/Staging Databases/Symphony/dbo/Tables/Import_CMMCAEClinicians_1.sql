﻿CREATE TABLE [dbo].[Import_CMMCAEClinicians](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[PAS Code] [varchar](50) NULL DEFAULT (''),
	[Surname] [varchar](50) NULL DEFAULT (''),
	[Initial] [varchar](50) NULL DEFAULT (''),
	[Title] [varchar](50) NULL DEFAULT (''),
	[GMC Code] [varchar](50) NULL DEFAULT (''),
	[Import] [int] NULL DEFAULT ((0)),
	[Staff ID] [varchar](50) NULL DEFAULT (''),
	[NewTitle] [int] NULL DEFAULT (''),
	[LogOnID] [varchar](50) NULL DEFAULT (''),
	[Imported] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
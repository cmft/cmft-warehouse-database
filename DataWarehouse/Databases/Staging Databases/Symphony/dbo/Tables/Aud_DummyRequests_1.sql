﻿CREATE TABLE [dbo].[Aud_DummyRequests](
	[drq_identity] [int] IDENTITY(0,1) NOT NULL,
	[drq_id] [int] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_id]  DEFAULT (0),
	[drq_recordid] [int] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_recordid]  DEFAULT (0),
	[drq_assocdepid] [smallint] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_assocdepid]  DEFAULT (0),
	[drq_maindepid] [smallint] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_maindepid]  DEFAULT (0),
	[drq_atdid] [int] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_drq_atdid]  DEFAULT (0),
	[drq_requestdate] [datetime] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_requestdate]  DEFAULT ('01/01/2200'),
	[drq_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_deleted]  DEFAULT (0),
	[drq_isdeleted] [bit] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_isdeleted]  DEFAULT (0),
	[drq_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_createdby]  DEFAULT (0),
	[drq_updated] [datetime] NOT NULL CONSTRAINT [DF__Aud_DummyRequests__drq_updated]  DEFAULT (getdate()),
	[drq_SDID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LookupMappings](
	[flm_id] [int] IDENTITY(0,1) NOT NULL,
	[flm_lkpid] [int] NOT NULL CONSTRAINT [DF__LookupMappings__Flm_lkpID]  DEFAULT (0),
	[flm_mtid] [int] NOT NULL CONSTRAINT [DF__LookupMappings__Flm_MtID]  DEFAULT (0),
	[flm_value] [int] NOT NULL CONSTRAINT [DF__LookupMappings__Flm_Value]  DEFAULT (0),
	[flm_description] [varchar](200) NOT NULL CONSTRAINT [DF__LookupMappings__Flm_Description]  DEFAULT (''),
	[flm_createdby] [int] NOT NULL CONSTRAINT [DF__LookupMappings__Flm_createdBy]  DEFAULT (0),
	[flm_datecreated] [datetime] NOT NULL CONSTRAINT [DF__LookupMappings__Flm_Datecreated]  DEFAULT (getdate()),
	[flm_MappedType] [tinyint] NOT NULL CONSTRAINT [DF__LookupMappings__flm_MappedType]  DEFAULT (0)
) ON [PRIMARY]
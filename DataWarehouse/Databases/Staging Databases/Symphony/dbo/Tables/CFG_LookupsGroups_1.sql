﻿CREATE TABLE [dbo].[CFG_LookupsGroups](
	[lkg_lkpid] [int] NOT NULL,
	[lkg_group] [int] NULL,
	[lkg_createdby] [int] NOT NULL,
	[lkg_update] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_CFG_Stages](
	[stg_identity] [int] IDENTITY(0,1) NOT NULL,
	[stg_id] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_Stages__stg_id]  DEFAULT (0),
	[stg_name] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_CFG_Stages__stg_name]  DEFAULT (''),
	[stg_dep] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_Stages__stg_dep]  DEFAULT (0),
	[stg_index] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_Stages__stg_index]  DEFAULT (0),
	[stg_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_Stages__stg_createdby]  DEFAULT (0),
	[stg_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_Stages__stg_update]  DEFAULT (getdate()),
	[stg_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_Stages__stg_deleted]  DEFAULT (0)
) ON [PRIMARY]
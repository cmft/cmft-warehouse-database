﻿CREATE TABLE [dbo].[DAD_TPErrors](
	[tpe_patid] [int] NOT NULL CONSTRAINT [DF__DAD_TPErrors__tpe_patid]  DEFAULT (0),
	[tpe_defid] [int] NOT NULL CONSTRAINT [DF__DAD_TPErrors__tpe_defid]  DEFAULT (0),
	[tpe_error] [varchar](1000) NOT NULL CONSTRAINT [DF__DAD_TPErrors__tpe_error]  DEFAULT (''),
	[tpe_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_TPErrors__tpe_createdby]  DEFAULT (0),
	[tpe_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_TPErrors__tpe_created]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DAD_Application_TimeStamp](
	[dat_id] [int] IDENTITY(0,1) NOT NULL,
	[dat_appname] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Application_TimeStamp__dat_appname]  DEFAULT (''),
	[dat_timestamp] [datetime] NULL CONSTRAINT [DF__DAD_Application_TimeStamp__dat_timesteamp]  DEFAULT (''),
	[dat_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Application_TimeStamp__dat_createdby]  DEFAULT (0),
	[dat_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Application_TimeStamp__dat_created]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_DAD_ExtSOAP_Settings](
	[sst_Identity] [int] IDENTITY(1,1) NOT NULL,
	[sst_ID] [int] NULL,
	[sst_SOAPID] [int] NULL,
	[sst_Name] [varchar](225) NULL,
	[sst_Value] [varchar](255) NULL,
	[sst_created] [datetime] NULL,
	[sst_createdBy] [int] NULL,
	[sst_Updated] [datetime] NULL,
	[sst_UpdatedBy] [int] NULL,
	[sst_SOAPExtCategory] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Images](
	[img_imageid] [int] IDENTITY(0,1) NOT NULL,
	[img_image] [image] NULL,
	[img_createdby] [int] NOT NULL CONSTRAINT [DF__Images__img_createdby]  DEFAULT (0),
	[img_update] [datetime] NOT NULL CONSTRAINT [DF__Images__img_update]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
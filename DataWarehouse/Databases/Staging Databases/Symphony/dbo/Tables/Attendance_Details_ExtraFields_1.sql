﻿CREATE TABLE [dbo].[Attendance_Details_ExtraFields](
	[atd_EF_extrafieldID] [int] IDENTITY(0,1) NOT NULL,
	[atd_EF_FieldType] [smallint] NOT NULL CONSTRAINT [DF__Attendance_Details_ExtraFields__FieldType]  DEFAULT (0),
	[atd_EF_FieldID] [int] NOT NULL CONSTRAINT [DF__Attendance_Details_ExtraFields__FieldID]  DEFAULT (0),
	[atd_EF_atdid] [int] NOT NULL CONSTRAINT [DF__Attendance_Details_ExtraFields__atdid]  DEFAULT (0),
	[atd_EF_Value] [varchar](4000) NOT NULL CONSTRAINT [DF__Attendance_Details_ExtraFields__Value]  DEFAULT (''),
	[atd_EF_DataCategory] [smallint] NOT NULL CONSTRAINT [DF__Attendance_Details_ExtraFields__DataCategory]  DEFAULT (0),
	[atd_EF_RecordID] [int] NOT NULL CONSTRAINT [DF__Attendance_Details_ExtraFields__RecordID]  DEFAULT (0),
	[atd_EF_FieldCodeValues] [varchar](200) NOT NULL CONSTRAINT [DF_Attendance_Details_ExtraFields_atd_EF_FieldCodeValues]  DEFAULT (''),
	[atd_EF_ComputationState] [tinyint] NOT NULL CONSTRAINT [DF_Attendance_Details_ExtraFields_atd_EF_ComputationState]  DEFAULT (0),
	[atd_EF_depId] [smallint] NOT NULL CONSTRAINT [DF_Attendance_Details_ExtraFields_atd_EF_depId]  DEFAULT ((0)),
	[atd_EF_CPMessagesIDs] [varchar](200) NOT NULL CONSTRAINT [DF_Attendance_Details_ExtraFields_atd_EF_CPMessagesIDs]  DEFAULT (''),
	[Atd_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Attendance_details_extrafields__Atd_EF_CreatedBy]  DEFAULT ((0)),
	[Atd_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Attendance_details_extrafields__Atd_EF_Created]  DEFAULT (getdate()),
	[Atd_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Attendance_details_extrafields__Atd_EF_updatedby]  DEFAULT ((0)),
	[Atd_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Attendance_details_extrafields__Atd_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
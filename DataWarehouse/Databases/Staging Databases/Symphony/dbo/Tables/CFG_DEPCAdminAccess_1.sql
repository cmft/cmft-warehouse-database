﻿CREATE TABLE [dbo].[CFG_DEPCAdminAccess](
	[dpc_ID] [int] IDENTITY(0,1) NOT NULL,
	[dpc_Name] [varchar](50) NOT NULL CONSTRAINT [DF__CFG_DEPCAdminAccess__dpc_Name]  DEFAULT (''),
	[dpc_access] [bit] NOT NULL CONSTRAINT [DF__CFG_DEPCAdminAccess__dpc_access]  DEFAULT (0),
	[dpc_parenttype] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DEPCAdminAccess__dpc_parenttype]  DEFAULT (0),
	[dpc_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_DEPCAdminAccess__dpc_created]  DEFAULT (getdate())
) ON [PRIMARY]
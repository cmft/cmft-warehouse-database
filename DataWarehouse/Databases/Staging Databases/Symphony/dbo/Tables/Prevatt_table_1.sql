﻿CREATE TABLE [dbo].[Prevatt_table](
	[atd_id] [int] NULL,
	[epd_pid] [int] NULL,
	[atd_num] [varchar](20) NULL,
	[atd_attendancetype] [int] NULL,
	[atd_arrivaldate] [datetime] NOT NULL,
	[lkp_name] [varchar](2000) NULL,
	[Complaint] [varchar](2000) NULL,
	[Diagnosis] [varchar](2000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_Gp_Practise](
	[pr_identity] [int] IDENTITY(0,1) NOT NULL,
	[pr_praccode] [varchar](14) NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_praccode]  DEFAULT ('V81999'),
	[pr_addid] [int] NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_addid]  DEFAULT (0),
	[pr_telid] [int] NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_telid]  DEFAULT (0),
	[pr_gpfhcode] [varchar](3) NOT NULL CONSTRAINT [DF__Aud_Gp_Pr__pr_gpfhcode]  DEFAULT ('X98'),
	[pr_healthvisitor] [varchar](56) NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_healthvisitor]  DEFAULT (''),
	[pr_localcode] [varchar](14) NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_localcode]  DEFAULT (''),
	[pr_status] [int] NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_status]  DEFAULT (0),
	[pr_localgp] [tinyint] NOT NULL CONSTRAINT [DF__Aud_Gp_practise__pr_localgp]  DEFAULT (1),
	[pr_pgrpcarecode] [varchar](14) NOT NULL CONSTRAINT [DF__Aud_GP_Practise__pr_pgrpcarecode]  DEFAULT (''),
	[pr_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_createdby]  DEFAULT (0),
	[pr_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_update]  DEFAULT (getdate()),
	[pr_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Gp_Practise__pr_deleted]  DEFAULT (0),
	[pr_letterpref] [varchar](35) NULL CONSTRAINT [DF__Aud_GP_Practise__pr_letterpref]  DEFAULT (''),
	[pr_id] [int] NULL CONSTRAINT [DF__Aud_GP_Practise__pr_id]  DEFAULT ('')
) ON [PRIMARY]
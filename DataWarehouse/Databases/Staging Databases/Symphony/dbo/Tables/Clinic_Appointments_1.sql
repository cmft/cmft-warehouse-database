﻿CREATE TABLE [dbo].[Clinic_Appointments](
	[cla_clininst] [int] NOT NULL CONSTRAINT [DF_Clinic_Appointments_cla_clininst]  DEFAULT (0),
	[cla_atdid] [int] NOT NULL CONSTRAINT [DF_Clinic_Appointments_cla_atdid]  DEFAULT (0),
	[cla_currentlocation] [varchar](100) NOT NULL CONSTRAINT [DF_Clinic_Appointments_cla_currentlocation]  DEFAULT (''),
	[cla_currentlocationid] [smallint] NOT NULL CONSTRAINT [DF_Clinic_Appointments_cla_currentlocationid]  DEFAULT (0),
	[cla_seenby] [varchar](100) NOT NULL CONSTRAINT [DF__Clinic_Appointments__cla_seenby]  DEFAULT (''),
	[cla_assocdepdata] [varchar](1000) NOT NULL CONSTRAINT [DF__Clinic_Appointments__cla_assocdepdata]  DEFAULT (''),
	[cla_cancelled] [bit] NOT NULL CONSTRAINT [DF__Clinic_Appointments__cla_cancelled]  DEFAULT (0),
	[cla_SeenByInitials] [varchar](15) NOT NULL CONSTRAINT [DF__Clinic_Appointments__cla_SeenByInitials]  DEFAULT (''),
	[Cla_SeenByDateCreated] [datetime] NOT NULL CONSTRAINT [DF__Clinic_Appointments__Cla_SeenByDateCreated]  DEFAULT ('2200/01/01'),
	[Cla_SeenBySysProp] [varchar](15) NOT NULL CONSTRAINT [DF__Clinic_Appointments__Cla_SeenBySysProp]  DEFAULT (0)
) ON [PRIMARY]
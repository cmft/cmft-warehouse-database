﻿CREATE TABLE [dbo].[Aud_Clinic_Appointments](
	[cla_identity] [int] IDENTITY(0,1) NOT NULL,
	[cla_clininst] [int] NOT NULL CONSTRAINT [DF__Aud_Clinic_Appointments_cla_clininst]  DEFAULT (0),
	[cla_atdid] [int] NOT NULL CONSTRAINT [DF__Aud_Clinic_Appointments_cla_atdid]  DEFAULT (0),
	[cla_currentlocation] [varchar](100) NULL CONSTRAINT [DF__Aud_Clinic_Appointments_cla_currentlocation]  DEFAULT (''),
	[cla_currentlocationid] [smallint] NOT NULL CONSTRAINT [DF__Aud_Clinic_Appointments_cla_currentlocationid]  DEFAULT (0),
	[cla_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Clinic_Appointments__cla_deleted]  DEFAULT (0),
	[cla_cancelled] [bit] NOT NULL CONSTRAINT [DF__Aud_Clinic_Appointments__cla_cancelled]  DEFAULT (0),
	[cla_SeenByInitials] [varchar](15) NULL,
	[Cla_SeenByDateCreated] [datetime] NULL,
	[Cla_SeenBySysProp] [varchar](15) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_CFG_Dept](
	[dpt_identity] [int] IDENTITY(0,1) NOT NULL,
	[dpt_id] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_Dept__dpt_id]  DEFAULT (0),
	[dpt_name] [varchar](20) NOT NULL CONSTRAINT [DF__Aud_CFG_Dept__dpt_name]  DEFAULT (''),
	[dpt_shortname] [varchar](4) NOT NULL CONSTRAINT [DF__Aud_CFG_Dept__dpt_shortname]  DEFAULT (''),
	[dpt_hasbeenused] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_Dept__dpt_hasbeenused]  DEFAULT (0),
	[dpt_created] [datetime] NOT NULL CONSTRAINT [DF__aud_CFG_Dept__dpt_created]  DEFAULT (getdate()),
	[dpt_createdby] [int] NOT NULL CONSTRAINT [DF__aud_CFG_Dept__dpt_createdby]  DEFAULT (0),
	[dpt_updated] [datetime] NOT NULL CONSTRAINT [DF__aud_CFG_Dept__dpt_updated]  DEFAULT (getdate()),
	[dpt_updatedby] [int] NOT NULL CONSTRAINT [DF__aud_CFG_Dept__dpt_updatedby]  DEFAULT (0),
	[dpt_deleted] [bit] NOT NULL CONSTRAINT [DF__aud_CFG_Dept__dpt_deleted]  DEFAULT (0),
	[dpt_IsServiceDept] [bit] NULL
) ON [PRIMARY]
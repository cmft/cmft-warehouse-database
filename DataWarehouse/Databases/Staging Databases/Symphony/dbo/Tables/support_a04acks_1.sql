﻿CREATE TABLE [dbo].[support_a04acks](
	[ack_id] [int] IDENTITY(1,1) NOT NULL,
	[ack_delid] [int] NULL,
	[ack_deldate] [datetime] NULL,
	[ack_delevent] [varchar](4000) NULL,
	[ack_patientid] [int] NULL,
	[ack_atdnum] [varchar](20) NULL,
	[ack_updated] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DAD_Patient_Merges](
	[mrg_id] [int] IDENTITY(0,1) NOT NULL,
	[mrg_majorpid] [int] NOT NULL,
	[mrg_majorhospnum] [varchar](30) NOT NULL,
	[mrg_minorpid] [int] NOT NULL,
	[mrg_minorhospnum] [varchar](30) NOT NULL,
	[mrg_newhospnum] [varchar](30) NOT NULL,
	[mrg_datecreated] [datetime] NOT NULL
) ON [PRIMARY]
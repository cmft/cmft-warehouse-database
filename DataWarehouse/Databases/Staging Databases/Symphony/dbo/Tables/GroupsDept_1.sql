﻿CREATE TABLE [dbo].[GroupsDept](
	[gdt_grpid] [int] NOT NULL CONSTRAINT [DF__GroupsDept__gdt_grpid]  DEFAULT (0),
	[gdt_deptid] [int] NOT NULL CONSTRAINT [DF__GroupsDept__gdt_deptid]  DEFAULT (0),
	[gdt_createdby] [int] NOT NULL CONSTRAINT [DF__GroupsDept__gdt_createdby]  DEFAULT (0),
	[gdt_created] [datetime] NOT NULL CONSTRAINT [DF__GroupsDept__gdt_created]  DEFAULT (getdate()),
	[gdt_updated] [datetime] NOT NULL CONSTRAINT [DF__GroupsDept__gdt_updated]  DEFAULT (getdate()),
	[gdt_updatedby] [int] NOT NULL CONSTRAINT [DF__GroupsDept__gdt_updatedby]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_DAD_Con_PatSettings](
	[cpt_identity] [int] IDENTITY(0,1) NOT NULL,
	[cpt_parentid] [int] NOT NULL,
	[cpt_patientaction] [tinyint] NOT NULL,
	[cpt_actionexecution] [tinyint] NOT NULL,
	[cpt_output] [bit] NOT NULL,
	[cpt_pasync] [bit] NOT NULL,
	[cpt_createdby] [int] NOT NULL,
	[cpt_created] [datetime] NOT NULL,
	[cpt_splitscreen] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Pathology_Requests](
	[resID] [int] IDENTITY(1,1) NOT NULL,
	[res_atdid] [int] NULL,
	[res_date] [datetime] NULL,
	[res_result] [int] NULL
) ON [PRIMARY]
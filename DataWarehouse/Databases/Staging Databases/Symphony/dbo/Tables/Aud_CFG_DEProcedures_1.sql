﻿CREATE TABLE [dbo].[Aud_CFG_DEProcedures](
	[dep_identity] [smallint] IDENTITY(0,1) NOT NULL,
	[dep_id] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_id]  DEFAULT (0),
	[dep_parentid] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_parentid]  DEFAULT (0),
	[dep_name] [varchar](50) NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_name]  DEFAULT (''),
	[dep_Caption] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_caption]  DEFAULT (''),
	[dep_HelpText] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_helptext]  DEFAULT (''),
	[dep_type] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_type]  DEFAULT (0),
	[dep_sidedep] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_sidedep]  DEFAULT (0),
	[dep_index] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_index]  DEFAULT (0),
	[dep_includeingpletter] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_includeingpletter]  DEFAULT (1),
	[dep_AllowAfterDischarge] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_allowafterdischarge]  DEFAULT (0),
	[dep_IsApplication] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_isapplication]  DEFAULT (0),
	[dep_visible] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_visible]  DEFAULT (1),
	[dep_DisplayEpdCount] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_DisplayEpdCount]  DEFAULT (0),
	[dep_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_createdBy]  DEFAULT (0),
	[dep_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_update]  DEFAULT (getdate()),
	[dep_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_deleted]  DEFAULT (0),
	[dep_waitingtime] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_waitingtime]  DEFAULT (0),
	[dep_notificationtime] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_notificationtime]  DEFAULT (0),
	[dep_visibleinnam] [bit] NOT NULL CONSTRAINT [DF__AUD_CFG_DEProcedures__dep_VisibleInNAM]  DEFAULT (1),
	[dep_hasui] [bit] NOT NULL CONSTRAINT [DF__AUD_CFG_DEProcedures__dep_HasUI]  DEFAULT (1),
	[dep_active] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_active]  DEFAULT (0),
	[dep_used] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_Used]  DEFAULT (0),
	[dep_signing] [bit] NOT NULL CONSTRAINT [DF__AUD_CFG_DEProcedures__dep_Signing]  DEFAULT (0),
	[dep_sideType] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_sideType]  DEFAULT (0),
	[dep_interfacedep] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_interfacedep]  DEFAULT (0),
	[dep_locmandatory] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_Deprocedures__dep_locmandatory]  DEFAULT (0),
	[dep_isenotesdep] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_isenotesdep]  DEFAULT (0),
	[dep_IsServiceDep] [bit] NULL,
	[dep_FulfillingDept] [int] NULL,
	[dep_FulfillInParent] [bit] NULL,
	[dep_audioreminder] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_audioreminder]  DEFAULT (0),
	[dep_audiofile] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_audiofile]  DEFAULT (''),
	[dep_audioplaymode] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEProcedures__dep_audioplaymode]  DEFAULT (0),
	[dep_IsInterDeptDEP] [bit] NULL,
	[DEP_HasFlag] [bit] NOT NULL,
	[dep_IncludeDeadPatInFilter] [bit] NOT NULL CONSTRAINT [DF__aud_CFG_DEProcedures__dep_IncludeDeadPatInFilter]  DEFAULT ((0))
) ON [PRIMARY]
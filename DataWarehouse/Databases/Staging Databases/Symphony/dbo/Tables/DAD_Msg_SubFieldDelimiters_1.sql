﻿CREATE TABLE [dbo].[DAD_Msg_SubFieldDelimiters](
	[mfd_structureid] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_SubFieldDelimiters__mfd_structureid]  DEFAULT (0),
	[mfd_delimiter] [varchar](5) NOT NULL CONSTRAINT [DF__DAD_Msg_SubFieldDelimiters__mfd_delimiter]  DEFAULT (''),
	[mfd_sequence] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Msg_SubFieldDelimiters__mfd_sequence]  DEFAULT (0),
	[mfd_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_SubFieldDelimiters__mfd_createdby]  DEFAULT (0),
	[mfd_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Msg_SubFieldDelimiters__mfd_created]  DEFAULT (getdate())
) ON [PRIMARY]
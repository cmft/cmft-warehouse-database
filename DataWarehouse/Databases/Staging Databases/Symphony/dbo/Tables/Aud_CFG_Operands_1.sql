﻿CREATE TABLE [dbo].[Aud_CFG_Operands](
	[ffo_Identity] [int] IDENTITY(0,1) NOT NULL,
	[ffo_ID] [int] NULL,
	[ffo_OperandTypeId] [int] NULL,
	[ffo_OperandType] [smallint] NULL,
	[ffo_OperandRecordType] [tinyint] NULL,
	[ffo_FormulaFieldDefID] [int] NULL,
	[ffo_FieldCode] [varchar](10) NULL,
	[ffo_ParentOperandId] [int] NULL,
	[ffo_DefaultForBlanks] [varchar](50) NULL,
	[ffo_created] [datetime] NULL,
	[ffo_createdby] [int] NULL,
	[ffo_updated] [datetime] NULL,
	[ffo_updatedby] [int] NULL,
	[ffo_deleted] [bit] NULL
) ON [PRIMARY]
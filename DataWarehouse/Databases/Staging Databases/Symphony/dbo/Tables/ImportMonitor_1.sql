﻿CREATE TABLE [dbo].[ImportMonitor](
	[imp_id] [int] IDENTITY(1,1) NOT NULL,
	[imp_deptid] [int] NULL DEFAULT ((0)),
	[imp_importname] [varchar](255) NULL DEFAULT (''),
	[imp_scriptname] [varchar](255) NULL DEFAULT (''),
	[imp_rowsinsource] [int] NULL DEFAULT ((0)),
	[imp_rowstoimport] [int] NULL DEFAULT ((0)),
	[imp_tablerowsbefore] [int] NULL DEFAULT ((0)),
	[imp_rowsimported] [int] NULL DEFAULT ((0)),
	[imp_rowsupdated] [int] NULL DEFAULT ((0)),
	[imp_rowsfailed] [int] NULL DEFAULT ((0)),
	[imp_tablerowsafter] [int] NULL DEFAULT ((0)),
	[imp_scriptstarted] [datetime] NULL DEFAULT (''),
	[imp_scriptended] [datetime] NULL DEFAULT (''),
	[imp_importstarted] [datetime] NULL DEFAULT (''),
	[imp_importended] [datetime] NULL DEFAULT (''),
	[imp_importcode] [varchar](5) NULL DEFAULT ('')
) ON [PRIMARY]
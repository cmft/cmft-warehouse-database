﻿CREATE TABLE [dbo].[Aud_Att_ObsCal](
	[aoc_identity] [int] IDENTITY(0,1) NOT NULL,
	[aoc_atdid] [int] NOT NULL,
	[aoc_temperature] [smallint] NOT NULL,
	[aoc_general] [smallint] NOT NULL,
	[aoc_oxygensat] [smallint] NOT NULL,
	[aoc_twips] [smallint] NOT NULL,
	[aoc_createdby] [int] NOT NULL,
	[aoc_created] [datetime] NOT NULL,
	[aoc_updatedby] [int] NOT NULL,
	[aoc_updated] [datetime] NOT NULL,
	[aoc_deleted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_CFG_PopUpItems](
	[pop_identity] [int] IDENTITY(0,1) NOT NULL,
	[pop_id] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_id]  DEFAULT (0),
	[pop_popuptype] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_popuptype]  DEFAULT (0),
	[pop_popuptypeid] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_popuptypeid]  DEFAULT (0),
	[pop_index] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_index]  DEFAULT (0),
	[pop_condition] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_depid]  DEFAULT (0),
	[pop_fieldid] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_fieldid]  DEFAULT (0),
	[pop_formid] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_formid]  DEFAULT (0),
	[pop_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_updatedby]  DEFAULT (0),
	[pop_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_update]  DEFAULT (getdate()),
	[pop_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_PopUpItems__pop_deleted]  DEFAULT (0),
	[pop_PopUpScreenType] [smallint] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_Lookups](
	[lkp_identity] [int] IDENTITY(0,1) NOT NULL,
	[Lkp_ID] [int] NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_ID]  DEFAULT (0),
	[Lkp_ParentID] [int] NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_Parentid]  DEFAULT (0),
	[Lkp_Name] [varchar](80) NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_Name]  DEFAULT (''),
	[lkp_active] [bit] NOT NULL CONSTRAINT [DF__Aud_Lookups__lkp_active]  DEFAULT (1),
	[Lkp_DisplayOrder] [smallint] NOT NULL CONSTRAINT [DF__AUD_Lookups__Lkp_DisplayOrder]  DEFAULT (0),
	[Lkp_InAscOrder] [bit] NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_InAscOrder]  DEFAULT (1),
	[Lkp_Used] [bit] NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_Used]  DEFAULT (0),
	[Lkp_Datecreated] [datetime] NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_Datecreated]  DEFAULT (getdate()),
	[Lkp_System] [varchar](50) NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_System]  DEFAULT (''),
	[Lkp_FlatLookupID] [int] NOT NULL CONSTRAINT [DF__aud_Lookups__Lkp_FlatLookupID]  DEFAULT (0),
	[lkp_deptid] [int] NOT NULL CONSTRAINT [DF__Aud_Lookups__lkp_deptid]  DEFAULT (0),
	[Lkp_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_createdby]  DEFAULT (0),
	[Lkp_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Lookups__Lkp_update]  DEFAULT (getdate()),
	[lkp_isdeleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Lookups__lkp_isdeleted]  DEFAULT (0),
	[lkp_refreshed] [datetime] NOT NULL CONSTRAINT [DF__Aud_Lookups__lkp_refreshed]  DEFAULT (getdate()),
	[lkp_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Lookups__lkp_deleted]  DEFAULT (0),
	[lkp_IsSubAnalysed] [bit] NOT NULL CONSTRAINT [Aud_DF__Lookups__lkp_IsSubAnalysed]  DEFAULT (0),
	[lkp_TableID] [bit] NOT NULL CONSTRAINT [Aud_DF__Lookups__lkp_TableID]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[aud_patientmerge](
	[apm_identity] [int] IDENTITY(0,1) NOT NULL,
	[apm_majorpatpid] [int] NULL,
	[apm_minorpatpid] [int] NULL,
	[apm_date] [datetime] NULL DEFAULT (getdate()),
	[apm_user] [int] NULL
) ON [PRIMARY]
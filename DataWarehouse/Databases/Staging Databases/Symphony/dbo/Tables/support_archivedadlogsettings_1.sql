﻿CREATE TABLE [dbo].[support_archivedadlogsettings](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[archivedate] [datetime] NULL DEFAULT (getdate()-(1)),
	[delid] [int] NULL,
	[dataarchived] [bit] NULL DEFAULT ('0')
) ON [PRIMARY]
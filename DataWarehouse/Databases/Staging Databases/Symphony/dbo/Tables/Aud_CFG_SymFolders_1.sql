﻿CREATE TABLE [dbo].[Aud_CFG_SymFolders](
	[sf_Identity] [int] IDENTITY(0,1) NOT NULL,
	[sf_Id] [int] NULL,
	[sf_Name] [varchar](50) NULL,
	[sf_Description] [varchar](255) NULL,
	[sf_RecordType] [smallint] NULL,
	[sf_ParentId] [int] NULL,
	[sf_ClusterId] [int] NULL,
	[sf_Created] [datetime] NULL,
	[sf_CreatedBy] [int] NULL,
	[sf_Updated] [datetime] NULL,
	[sf_UpdatedBy] [int] NULL,
	[sf_Deleted] [int] NULL DEFAULT (0)
) ON [PRIMARY]
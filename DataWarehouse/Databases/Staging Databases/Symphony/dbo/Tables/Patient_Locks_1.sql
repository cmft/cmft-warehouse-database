﻿CREATE TABLE [dbo].[Patient_Locks](
	[plc_ID] [int] IDENTITY(0,1) NOT NULL,
	[plc_PID] [int] NOT NULL CONSTRAINT [DF__Patient_Locks__plc_PID]  DEFAULT (0),
	[plc_lockpc] [varchar](50) NOT NULL CONSTRAINT [DF__Patient_Locks__plc_lockpc]  DEFAULT (''),
	[plc_lockedby] [int] NOT NULL CONSTRAINT [DF__Patient_Locks__plc_lockedby]  DEFAULT (0),
	[plc_lockdate] [datetime] NOT NULL CONSTRAINT [DF__Patient_Locks__plc_lockdate]  DEFAULT (getdate()),
	[plc_DEPID] [smallint] NOT NULL CONSTRAINT [DF__patient_locks__plc_DEPID]  DEFAULT ((0)),
	[plc_PartiallyLockedDEPID] [smallint] NOT NULL CONSTRAINT [DF__patient_locks__plc_PartiallyLockedDEPID]  DEFAULT ((0)),
	[plc_IsFullyLocked] [bit] NOT NULL CONSTRAINT [DF__patient_locks__plc_IsFullyLocked]  DEFAULT ((0))
) ON [PRIMARY]
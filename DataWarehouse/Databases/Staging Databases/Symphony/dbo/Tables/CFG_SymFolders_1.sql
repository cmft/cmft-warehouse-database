﻿CREATE TABLE [dbo].[CFG_SymFolders](
	[sf_Id] [int] IDENTITY(0,1) NOT NULL,
	[sf_Name] [varchar](50) NOT NULL CONSTRAINT [DF_CFG_SymFolders_sf_Name]  DEFAULT (''),
	[sf_Description] [varchar](255) NOT NULL CONSTRAINT [DF_CFG_SymFolders_sf_Description]  DEFAULT (''),
	[sf_RecordType] [smallint] NOT NULL CONSTRAINT [DF_CFG_SymFolders_sf_RecordType]  DEFAULT (0),
	[sf_ParentId] [int] NOT NULL CONSTRAINT [DF_Table_1_sf_parentId]  DEFAULT (0),
	[sf_ClusterId] [int] NOT NULL CONSTRAINT [DF_CFG_SymFolders_sf_ClusterId]  DEFAULT (0),
	[sf_Created] [datetime] NOT NULL CONSTRAINT [DF_CFG_SymFolders_sf_Created]  DEFAULT (getdate()),
	[sf_CreatedBy] [int] NOT NULL CONSTRAINT [DF_CFG_SymFolders_sf_CreatedBy]  DEFAULT (0),
	[sf_Updated] [datetime] NOT NULL CONSTRAINT [DF_CFG_SymFolders_sf_Updated]  DEFAULT ('01/01/2200'),
	[sf_UpdatedBy] [int] NOT NULL CONSTRAINT [DF_CFG_SymFolders_sf_UpdatedBy]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PrintForAtt](
	[pfa_jobid] [int] NOT NULL CONSTRAINT [DF__PrintForAtt__pfa_jobid]  DEFAULT (0),
	[pfa_atdid] [int] NOT NULL CONSTRAINT [DF__PrintForAtt__pfa_atdid]  DEFAULT (0),
	[pfa_failed] [bit] NOT NULL CONSTRAINT [DF__PrintForAtt__pfa_failed]  DEFAULT (0),
	[pfa_id] [int] IDENTITY(1,1) NOT NULL,
	[pfa_jobdate] [datetime] NOT NULL CONSTRAINT [DF__printforatt__pfa_jobdate]  DEFAULT (getdate())
) ON [PRIMARY]
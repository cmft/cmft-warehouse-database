﻿CREATE TABLE [dbo].[CFG_DefaultLocs](
	[dl_ID] [int] IDENTITY(0,1) NOT NULL,
	[dl_DEPStageID] [smallint] NOT NULL CONSTRAINT [DF__CFG_DefaultLocs__dl_DEPStageID]  DEFAULT (0),
	[dl_LocationID] [smallint] NOT NULL CONSTRAINT [DF__CFG_DefaultLocs__dl_LocationID]  DEFAULT (0),
	[dl_InActiveDueToParent] [bit] NOT NULL CONSTRAINT [DF__CFG_DefaultLocs__dl_InActiveDueToParent]  DEFAULT (0),
	[dl_Index] [int] NOT NULL CONSTRAINT [DF__CFG_DefaultLocs__dl_Index]  DEFAULT (0),
	[dl_Createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DefaultLocs__dl_Createdby]  DEFAULT (0),
	[dl_Created] [datetime] NOT NULL CONSTRAINT [DF__CFG_DefaultLocs__dl_Created]  DEFAULT (getdate()),
	[dl_Updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_DefaultLocs__dl_Updated]  DEFAULT (getdate()),
	[dl_UpdateBy] [int] NOT NULL CONSTRAINT [DF__CFG_DefaultLocs__dl_UpdateBy]  DEFAULT (0)
) ON [PRIMARY]
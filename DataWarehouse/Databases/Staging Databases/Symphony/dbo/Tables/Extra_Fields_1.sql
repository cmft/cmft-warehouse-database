﻿CREATE TABLE [dbo].[Extra_Fields](
	[efd_efdid] [int] IDENTITY(0,1) NOT NULL,
	[efd_recordid] [int] NOT NULL CONSTRAINT [DF__Extra_Fields__efd_recordid]  DEFAULT (0),
	[efd_table] [varchar](30) NOT NULL CONSTRAINT [DF__Extra_Fields__efd_table]  DEFAULT (''),
	[efd_ffdid] [int] NOT NULL CONSTRAINT [DF__Extra_Fields__efd_ffdid]  DEFAULT (0),
	[efd_value] [varchar](255) NULL,
	[efd_createdby] [int] NOT NULL CONSTRAINT [DF__Extra_Fields__efd_createdby]  DEFAULT (0),
	[efd_update] [datetime] NOT NULL CONSTRAINT [DF__Extra_Fields__efd_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_Filter](
	[flt_identity] [int] IDENTITY(1,1) NOT NULL,
	[flt_id] [int] NOT NULL CONSTRAINT [DF__Aud_Filter__flt_id]  DEFAULT (0),
	[flt_name] [varchar](50) NOT NULL CONSTRAINT [DF__Aud_Filter__flt_name]  DEFAULT (''),
	[flt_description] [varchar](200) NOT NULL CONSTRAINT [DF__Aud_Filter__flt_description]  DEFAULT (''),
	[flt_ufvalue] [varchar](2000) NOT NULL CONSTRAINT [DF__Aud_Filter__flt_ufvalue]  DEFAULT (''),
	[flt_value] [varchar](5000) NOT NULL CONSTRAINT [DF__Aud_Filter__flt_value]  DEFAULT (''),
	[flt_DefaultFilter] [bit] NOT NULL CONSTRAINT [DF__Aud_Filter__flt_DefaultFilter]  DEFAULT (0),
	[flt_FilterType] [smallint] NOT NULL CONSTRAINT [DF__Aud_Filter__flt_FilterType]  DEFAULT (0),
	[flt_deptid] [int] NOT NULL CONSTRAINT [DF__Aud_Filter__flt_deptid]  DEFAULT (0),
	[flt_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Filter__flt_createdby]  DEFAULT (0),
	[flt_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Filter__flt_Updated]  DEFAULT (getdate()),
	[flt_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Filter__flt_deleted]  DEFAULT (0),
	[flt_DashBoardDisplayFilter] [bit] NULL,
	[flt_TidDPatientGroupFilter] [bit] NULL,
	[flt_WTDPatientGroupFilter] [bit] NULL
) ON [PRIMARY]
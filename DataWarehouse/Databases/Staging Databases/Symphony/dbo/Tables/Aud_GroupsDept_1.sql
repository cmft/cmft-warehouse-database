﻿CREATE TABLE [dbo].[Aud_GroupsDept](
	[gdt_identity] [int] IDENTITY(0,1) NOT NULL,
	[gdt_grpid] [int] NOT NULL CONSTRAINT [DF__Aud_GroupsDept__gdt_grpid]  DEFAULT (0),
	[gdt_deptid] [int] NOT NULL CONSTRAINT [DF__Aud_GroupsDept__gdt_deptid]  DEFAULT (0),
	[gdt_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_GroupsDept__gdt_createdby]  DEFAULT (0),
	[gdt_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_GroupsDept__gdt_created]  DEFAULT (getdate()),
	[gdt_updated] [datetime] NOT NULL CONSTRAINT [DF__Aud_GroupsDept__gdt_updated]  DEFAULT (getdate()),
	[gdt_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_GroupsDept__gdt_updatedby]  DEFAULT (0),
	[gdt_deleted] [bit] NOT NULL CONSTRAINT [DF__GroupsDept__gdt_deleted]  DEFAULT (0)
) ON [PRIMARY]
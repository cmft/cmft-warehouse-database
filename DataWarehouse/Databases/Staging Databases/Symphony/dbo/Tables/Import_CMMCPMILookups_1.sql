﻿CREATE TABLE [dbo].[Import_CMMCPMILookups](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[CMMCLkpTABLE] [varchar](80) NULL DEFAULT (NULL),
	[CMMCDesc] [varchar](80) NULL DEFAULT (NULL),
	[CMMCLocalCode] [varchar](20) NULL DEFAULT (''),
	[LkpParent] [varchar](80) NULL DEFAULT (''),
	[LkpName] [varchar](80) NULL DEFAULT (''),
	[LkpID] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
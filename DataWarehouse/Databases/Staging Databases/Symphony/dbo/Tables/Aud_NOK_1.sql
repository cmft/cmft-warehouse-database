﻿CREATE TABLE [dbo].[Aud_NOK](
	[nok_identity] [int] IDENTITY(0,1) NOT NULL,
	[nok_nokid] [int] NOT NULL CONSTRAINT [DF__Aud_NOK__nok_nokid]  DEFAULT (0),
	[nok_pid] [int] NOT NULL CONSTRAINT [DF__Aud_NOK__nok_pid]  DEFAULT (0),
	[nok_surname] [varchar](35) NOT NULL CONSTRAINT [DF__Aud_NOK__nok_surname]  DEFAULT (''),
	[nok_forename] [varchar](35) NOT NULL CONSTRAINT [DF__Aud_NOK__nok_forename]  DEFAULT (''),
	[nok_midnames] [varchar](35) NOT NULL CONSTRAINT [DF__Aud_NOK__nok_midnames]  DEFAULT (''),
	[nok_honors] [int] NOT NULL CONSTRAINT [DF__Aud_NOK__nok_honors]  DEFAULT (0),
	[nok_title] [int] NOT NULL CONSTRAINT [DF__Aud_NOK__nok_title]  DEFAULT (0),
	[nok_relation] [int] NOT NULL CONSTRAINT [DF__Aud_NOK__nok_relation]  DEFAULT (0),
	[nok_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_NOK__nok_createdby]  DEFAULT (0),
	[nok_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_NOK__nok_update]  DEFAULT (getdate()),
	[nok_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_NOK__nok_deleted]  DEFAULT (0),
	[Nok_NokType] [int] NULL
) ON [PRIMARY]
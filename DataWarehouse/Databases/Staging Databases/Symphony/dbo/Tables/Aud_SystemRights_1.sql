﻿CREATE TABLE [dbo].[Aud_SystemRights](
	[Syr_identity] [int] IDENTITY(0,1) NOT NULL,
	[Syr_ID] [int] NOT NULL CONSTRAINT [DF__Aud_SystemRights__syr_id]  DEFAULT (0),
	[Syr_SysRight] [int] NOT NULL CONSTRAINT [DF__Aud_SystemRights__syr_sysright]  DEFAULT (0),
	[Syr_Account] [int] NOT NULL CONSTRAINT [DF__Aud_SystemRights__syr_account]  DEFAULT (0),
	[Syr_AccountType] [tinyint] NOT NULL CONSTRAINT [DF__Aud_SystemRights__syr_accounttype]  DEFAULT (0),
	[Syr_Permission] [bit] NOT NULL CONSTRAINT [DF__Aud_SystemRights__syr_permission]  DEFAULT (0),
	[syr_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_SystemRights__syr_createdby]  DEFAULT (0),
	[syr_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_SystemRights__syr_created]  DEFAULT (getdate()),
	[Syr_Deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_SystemRights__syr_deleted]  DEFAULT (0)
) ON [PRIMARY]
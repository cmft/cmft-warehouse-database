﻿CREATE TABLE [dbo].[Aud_DAD_Msg_Structure](
	[mss_identity] [int] IDENTITY(0,1) NOT NULL,
	[mss_id] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_id]  DEFAULT (0),
	[mss_serviceid] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_serviceid]  DEFAULT (0),
	[mss_fragtype] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Aud_Msg_Structure__mss_fragtype]  DEFAULT (0),
	[mss_fragmaxsize] [smallint] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_fragmaxsize]  DEFAULT (0),
	[mss_fragheadlength] [tinyint] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_fragheadlength]  DEFAULT (0),
	[mss_fragheadsep] [varchar](10) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_fragheadsep]  DEFAULT (''),
	[mss_startchar] [varchar](10) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_fragstartchar]  DEFAULT (''),
	[mss_endchar] [varchar](10) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_fragendchar]  DEFAULT (''),
	[mss_segdelimiter] [varchar](10) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_segdelimiter]  DEFAULT (''),
	[mss_fielddelimiter] [varchar](10) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_fielddelimiter]  DEFAULT (''),
	[mss_repdelimiter] [varchar](10) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_repdelimiter]  DEFAULT (''),
	[mss_repoptional] [bit] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_repoptional]  DEFAULT (0),
	[mss_explicitdbs] [varchar](20) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_explicitdbs]  DEFAULT (''),
	[mss_waitforendchar] [bit] NOT NULL CONSTRAINT [DF__Aud_Dad_Msg_Structure__mss_waitforendchar]  DEFAULT (0),
	[mss_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_createdby]  DEFAULT (0),
	[mss_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Structure__mss_created]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Import_Spec_Case_Data](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[DataSetName] [varchar](50) NULL DEFAULT (''),
	[Health Organisation Id] [varchar](50) NULL DEFAULT (''),
	[Patient Id] [varchar](50) NULL DEFAULT (''),
	[Code] [varchar](50) NULL DEFAULT (''),
	[EnteredDate] [varchar](50) NULL DEFAULT (''),
	[LkpID] [int] NULL DEFAULT ((0)),
	[PatPID] [int] NULL DEFAULT ((0)),
	[Imported] [smallint] NULL DEFAULT ((0)),
	[DoNotImport] [smallint] NULL DEFAULT ((0))
) ON [PRIMARY]
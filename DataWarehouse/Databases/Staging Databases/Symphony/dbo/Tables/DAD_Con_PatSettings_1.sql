﻿CREATE TABLE [dbo].[DAD_Con_PatSettings](
	[cpt_parentid] [int] NOT NULL CONSTRAINT [DF__DAD_Con_PatSettings__cpt_parentid]  DEFAULT (0),
	[cpt_patientaction] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Con_PatSettings__cpt_patientaction]  DEFAULT (0),
	[cpt_actionexecution] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Con_PatSettings__cpt_actionexecution]  DEFAULT (0),
	[cpt_output] [bit] NOT NULL CONSTRAINT [DF__DAD_Con_PatSettings__cpt_output]  DEFAULT (0),
	[cpt_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Con_PatSettings__cpt_createdby]  DEFAULT (0),
	[cpt_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Con_PatSettings__cpt_created]  DEFAULT (getdate()),
	[cpt_splitscreen] [bit] NOT NULL CONSTRAINT [DF__DAD_Con_PatSettings__cpt_splitscreen]  DEFAULT (0),
	[cpt_pasync] [bit] NOT NULL CONSTRAINT [DF__DAD_Con_PatSettings__cpt_pasync]  DEFAULT (0)
) ON [PRIMARY]
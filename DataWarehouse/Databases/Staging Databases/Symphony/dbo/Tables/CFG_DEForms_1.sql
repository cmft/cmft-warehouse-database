﻿CREATE TABLE [dbo].[CFG_DEForms](
	[def_id] [smallint] IDENTITY(0,1) NOT NULL,
	[def_name] [varchar](100) NOT NULL CONSTRAINT [DF__CFG_DEForms__def_name]  DEFAULT (''),
	[def_caption] [varchar](100) NOT NULL CONSTRAINT [DF__CFG_DEForms__def_caption]  DEFAULT (''),
	[def_hastoolbar] [bit] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_hastoolbar]  DEFAULT (0),
	[def_columns] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_columns]  DEFAULT (0),
	[def_gpletter] [bit] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_gpletter]  DEFAULT (1),
	[def_helptext] [varchar](255) NOT NULL CONSTRAINT [DF__CFG_DEForms__def_helptext]  DEFAULT (''),
	[def_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_createdby]  DEFAULT (0),
	[def_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_update]  DEFAULT (getdate()),
	[def_type] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_typw]  DEFAULT (0),
	[def_active] [bit] NOT NULL CONSTRAINT [DF__CFG_DEForm__Def_Active]  DEFAULT (1),
	[def_displaymode] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_displaymode]  DEFAULT (0),
	[def_usepsvs] [bit] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_usepsvs]  DEFAULT (0),
	[def_dnmavailable] [bit] NOT NULL CONSTRAINT [DF__CFG_DEForms__def_dnmavailable]  DEFAULT (0),
	[DEF_HasFlag] [bit] NOT NULL CONSTRAINT [DF__cfg_deforms__DEF_HasFlag]  DEFAULT ((0))
) ON [PRIMARY]
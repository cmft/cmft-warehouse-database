﻿CREATE TABLE [dbo].[DAD_Row_Settings](
	[rst_rowid] [int] NOT NULL CONSTRAINT [DF__DAD_Row_Settings__rst_rowid]  DEFAULT (0),
	[rst_name] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Row_Settings__rst_name]  DEFAULT (''),
	[rst_value] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Row_Settings__rst_value]  DEFAULT (''),
	[rst_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Row_Settings__rst_createdby]  DEFAULT (0),
	[rst_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Row_Settings__rst_created]  DEFAULT (getdate())
) ON [PRIMARY]
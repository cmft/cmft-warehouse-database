﻿CREATE TABLE [dbo].[Import_Del_Lookups](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[lkp_id] [int] NULL DEFAULT ((0)),
	[lkp_parentid] [int] NULL DEFAULT ((0)),
	[lkp_flatlookupid] [int] NULL DEFAULT ((0)),
	[lkp_name] [varchar](80) NULL DEFAULT (NULL),
	[lkp_deleted] [int] NULL DEFAULT ((0))
) ON [PRIMARY]
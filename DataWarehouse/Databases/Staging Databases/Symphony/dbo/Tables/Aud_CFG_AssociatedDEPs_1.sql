﻿CREATE TABLE [dbo].[Aud_CFG_AssociatedDEPs](
	[asdep_identity] [int] IDENTITY(0,1) NOT NULL,
	[asdep_id] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_id]  DEFAULT (0),
	[asdep_maindepid] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_maindepid]  DEFAULT (0),
	[asdep_assocdepid] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_assocdepid]  DEFAULT (0),
	[asdep_atttype] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_atttype]  DEFAULT (0),
	[asdep_assoctype] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_assoctype]  DEFAULT (0),
	[asdep_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_createdby]  DEFAULT (0),
	[asdep_updated] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_updated]  DEFAULT (getdate()),
	[asdep_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_deleted]  DEFAULT (0),
	[asdep_when2bpreqdep] [tinyint] NOT NULL CONSTRAINT [DF__Aud_CFG_AssociatedDEPs__asdep_when2bpreqdep]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_QuickText](
	[qti_id] [int] IDENTITY(0,1) NOT NULL,
	[qti_fieldid] [int] NOT NULL CONSTRAINT [DF__CFG_QuickText__qti_fieldid]  DEFAULT (0),
	[qti_quicktext] [varchar](255) NOT NULL CONSTRAINT [DF__CFG_QuickText__qti_quicktext]  DEFAULT (''),
	[qti_deleted] [bit] NOT NULL CONSTRAINT [DF__CFG_QuickText__qti_deleted]  DEFAULT (0),
	[qti_updatedby] [int] NOT NULL CONSTRAINT [DF__CFG_QuickText__qti_updatedby]  DEFAULT (0),
	[qti_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_QuickText__qti_update]  DEFAULT (getdate()),
	[qti_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_QuickText__qti_created]  DEFAULT (getdate()),
	[qti_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_QuickText__qti_createdby]  DEFAULT (0)
) ON [PRIMARY]
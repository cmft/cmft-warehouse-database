﻿CREATE TABLE [dbo].[Hitec_Output](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pat_pid] [int] NOT NULL,
	[output] [varchar](255) NOT NULL,
	[date] [datetime] NOT NULL,
	[deptid] [int] NOT NULL
) ON [PRIMARY]
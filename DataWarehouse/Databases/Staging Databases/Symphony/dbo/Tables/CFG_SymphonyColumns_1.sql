﻿CREATE TABLE [dbo].[CFG_SymphonyColumns](
	[Syc_id] [int] IDENTITY(0,1) NOT NULL,
	[Syc_Name] [varchar](80) NOT NULL CONSTRAINT [DF__CFG_SymphonyColumns__Syc_Name]  DEFAULT (''),
	[Syc_Length] [int] NOT NULL CONSTRAINT [DF__CFG_SymphonyColumns__Syc_Length]  DEFAULT ((0))
) ON [PRIMARY]
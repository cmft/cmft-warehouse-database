﻿CREATE TABLE [dbo].[PasswordSettings](
	[pwd_id] [int] IDENTITY(0,1) NOT NULL,
	[pwd_type] [varchar](30) NOT NULL CONSTRAINT [DF__PasswordSettings__pwd_pwd_type]  DEFAULT (''),
	[pwd_value] [varchar](300) NOT NULL CONSTRAINT [DF__PasswordSettings__pwd_value]  DEFAULT (''),
	[pwd_createdby] [int] NOT NULL CONSTRAINT [DF__PasswordSettings__pwd_createdby]  DEFAULT (0),
	[pwd_created] [datetime] NOT NULL CONSTRAINT [DF__PasswordSettings__pwd_created]  DEFAULT (getdate()),
	[pwd_update] [datetime] NOT NULL CONSTRAINT [DF__PasswordSettings__pwd_update]  DEFAULT (getdate())
) ON [PRIMARY]
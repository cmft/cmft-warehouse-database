﻿CREATE TABLE [dbo].[DAD_Mail_Messages](
	[dmm_id] [int] IDENTITY(0,1) NOT NULL,
	[dmm_message] [varchar](500) NOT NULL,
	[dmm_subject] [varchar](255) NOT NULL,
	[dmm_createdby] [int] NOT NULL,
	[dmm_created] [datetime] NOT NULL
) ON [PRIMARY]
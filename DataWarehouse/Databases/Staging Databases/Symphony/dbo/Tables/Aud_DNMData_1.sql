﻿CREATE TABLE [dbo].[Aud_DNMData](
	[dnm_identity] [int] IDENTITY(0,1) NOT NULL,
	[dnm_id] [int] NOT NULL CONSTRAINT [DF__Aud_DNMData__dnm_id]  DEFAULT (0),
	[dnm_image] [varchar](8000) NULL CONSTRAINT [DF__Aud_DNMData__dnm_image]  DEFAULT (''),
	[dnm_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DNMData__dnm_createdby]  DEFAULT (0),
	[dnm_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DNMData__dnm_created]  DEFAULT (getdate()),
	[dnm_updated] [datetime] NOT NULL CONSTRAINT [DF__Aud_DNMData__dnm_updated]  DEFAULT (getdate()),
	[dnm_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_DNMData__dnm_deleted]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DAD_Failed_Messages_11052010](
	[fam_srvid] [int] NOT NULL,
	[fam_patid] [int] NOT NULL,
	[fam_msgdef] [int] NOT NULL,
	[fam_createdby] [int] NOT NULL,
	[fam_created] [datetime] NOT NULL,
	[fam_atdid] [int] NOT NULL,
	[fam_status] [tinyint] NOT NULL,
	[fam_reason] [varchar](3500) NOT NULL,
	[fam_deptid] [int] NOT NULL,
	[fam_emailed] [bit] NOT NULL,
	[fam_LocMsgLocID] [int] NOT NULL,
	[Fam_CurrentRecords] [nvarchar](255) NOT NULL,
	[fam_id] [int] NOT NULL,
	[fam_attempts] [smallint] NOT NULL
) ON [PRIMARY]
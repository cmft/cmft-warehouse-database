﻿CREATE TABLE [dbo].[Aud_Extra_Fields](
	[efd_identity] [int] IDENTITY(0,1) NOT NULL,
	[efd_efdid] [int] NOT NULL,
	[efd_recordid] [int] NOT NULL,
	[efd_table] [varchar](30) NOT NULL,
	[efd_ffdid] [int] NOT NULL,
	[efd_value] [varchar](255) NULL,
	[efd_createdby] [int] NOT NULL,
	[efd_update] [datetime] NOT NULL,
	[efd_deleted] [bit] NOT NULL
) ON [PRIMARY]
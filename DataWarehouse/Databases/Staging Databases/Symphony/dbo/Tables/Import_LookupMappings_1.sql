﻿CREATE TABLE [dbo].[Import_LookupMappings](
	[tblID] [int] IDENTITY(1,1) NOT NULL,
	[lkp_tblname] [varchar](150) NULL,
	[lkp_name] [varchar](150) NULL,
	[lkp_tblid] [int] NULL,
	[lkp_id] [int] NULL,
	[mappingid] [varchar](1500) NULL,
	[lkp_matched] [bit] NULL
) ON [PRIMARY]
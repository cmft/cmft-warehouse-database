﻿CREATE TABLE [dbo].[Aud_DAD_Msg_Settings](
	[mst_identity] [int] IDENTITY(0,1) NOT NULL,
	[mst_defid] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Settings__mst_defid]  DEFAULT (0),
	[mst_name] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Settings__mst_name]  DEFAULT (''),
	[mst_value] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Settings__mst_value]  DEFAULT (''),
	[mst_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Settings__mst_createdby]  DEFAULT (0),
	[mst_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DAD_Msg_Settings__mst_created]  DEFAULT (getdate())
) ON [PRIMARY]
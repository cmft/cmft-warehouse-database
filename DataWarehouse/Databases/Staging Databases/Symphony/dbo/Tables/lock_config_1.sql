﻿CREATE TABLE [dbo].[lock_config](
	[lck_configdata] [smallint] NOT NULL,
	[lck_record] [int] NOT NULL,
	[lck_datetime] [datetime] NOT NULL,
	[lck_lockedby] [int] NOT NULL,
	[lck_lockpc] [varchar](50) NOT NULL
) ON [PRIMARY]
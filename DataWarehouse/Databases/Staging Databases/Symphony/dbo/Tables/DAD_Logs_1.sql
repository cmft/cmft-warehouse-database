﻿CREATE TABLE [dbo].[DAD_Logs](
	[dls_patid] [int] NOT NULL CONSTRAINT [DF__DAD_Logs__dls_patid]  DEFAULT (0),
	[dls_msgdef] [int] NOT NULL CONSTRAINT [DF__DAD_Logs__dls_msgdef]  DEFAULT (0),
	[dls_message] [varchar](4000) NULL CONSTRAINT [DF__DAD_Logs__dls_message]  DEFAULT (''),
	[dls_reason] [varchar](255) NULL CONSTRAINT [DF__DAD_Logs__dls_reason]  DEFAULT (''),
	[dls_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Logs__dls_createdby]  DEFAULT (0),
	[dls_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Logs__dls_created]  DEFAULT (getdate()),
	[dls_emailed] [bit] NOT NULL CONSTRAINT [DF__DAD_Logs__dls_emailed]  DEFAULT (0)
) ON [PRIMARY]
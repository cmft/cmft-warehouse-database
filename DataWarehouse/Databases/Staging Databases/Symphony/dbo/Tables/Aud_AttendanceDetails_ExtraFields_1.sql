﻿CREATE TABLE [dbo].[Aud_AttendanceDetails_ExtraFields](
	[atd_EF_ID] [int] IDENTITY(0,1) NOT NULL,
	[atd_EF_extrafieldID] [int] NULL,
	[atd_EF_FieldType] [smallint] NULL,
	[atd_EF_FieldID] [int] NULL,
	[atd_EF_atdid] [int] NULL,
	[atd_EF_Value] [varchar](4000) NULL,
	[atd_EF_DataCategory] [smallint] NULL,
	[atd_EF_RecordID] [int] NULL,
	[atd_EF_FieldCodeValues] [varchar](200) NULL,
	[atd_EF_ComputationState] [tinyint] NULL,
	[atd_EF_depId] [smallint] NULL,
	[atd_EF_CPMessagesIDs] [varchar](200) NULL,
	[Atd_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Aud_attendancedetails_extrafields__Atd_EF_CreatedBy]  DEFAULT ((0)),
	[Atd_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_attendancedetails_extrafields__Atd_EF_Created]  DEFAULT (getdate()),
	[Atd_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_attendancedetails_extrafields__Atd_EF_updatedby]  DEFAULT ((0)),
	[Atd_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_attendancedetails_extrafields__Atd_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
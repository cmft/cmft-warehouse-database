﻿CREATE TABLE [dbo].[DAD_SendNewReg](
	[Dsnr_PatPid] [int] NOT NULL CONSTRAINT [PK__DAD_SendNewReg__Dsnr_PatPid]  DEFAULT (0),
	[Dsnr_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_SendNewReg__Dsnr_createdby]  DEFAULT (0),
	[Dsnr_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_SendNewReg__Dsnr_created]  DEFAULT (getdate())
) ON [PRIMARY]
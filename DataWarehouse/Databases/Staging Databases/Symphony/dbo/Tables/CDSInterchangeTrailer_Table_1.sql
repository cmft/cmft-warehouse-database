﻿CREATE TABLE [dbo].[CDSInterchangeTrailer_Table](
	[CDS_INT_CRL_REF] [int] NULL,
	[CDS_INT_COUNT] [int] NULL,
	[CDS_INT_SEND_ID] [varchar](15) NULL,
	[CDS_INT_REC_ID] [varchar](15) NULL
) ON [PRIMARY]
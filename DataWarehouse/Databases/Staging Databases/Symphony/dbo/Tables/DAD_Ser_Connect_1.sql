﻿CREATE TABLE [dbo].[DAD_Ser_Connect](
	[con_serviceid] [int] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_serviceid]  DEFAULT (0),
	[con_type] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_type]  DEFAULT (0),
	[con_freqtype] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_freqtype]  DEFAULT (0),
	[con_handshk] [bit] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_handshk]  DEFAULT (0),
	[con_acksent] [varchar](10) NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_acksent]  DEFAULT (''),
	[con_ackrecv] [varchar](10) NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_ackrecv]  DEFAULT (''),
	[con_naksent] [varchar](10) NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_naksent]  DEFAULT (''),
	[con_nakrecv] [varchar](10) NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_nakrecv]  DEFAULT (''),
	[con_timeout] [smallint] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_timeout]  DEFAULT (0),
	[con_retries] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_retries]  DEFAULT (0),
	[con_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_createdby]  DEFAULT (0),
	[con_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_created]  DEFAULT (getdate()),
	[con_queueoutbound] [bit] NOT NULL CONSTRAINT [DF__DAD_Ser_Connect__con_queueoutbound]  DEFAULT (0),
	[con_charconv] [tinyint] NOT NULL CONSTRAINT [DF__dad_ser_connect__con_charconv]  DEFAULT (0),
	[con_startchar] [varchar](50) NOT NULL CONSTRAINT [DF__dad_ser_connect__con_startchar]  DEFAULT (''),
	[con_endchar] [varchar](50) NOT NULL CONSTRAINT [DF__dad_ser_connect__con_endchar]  DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AUD_CFG_FormulaFieldMsgs](
	[ffpm_Identity] [int] IDENTITY(0,1) NOT NULL,
	[ffpm_Id] [int] NULL,
	[ffpm_ParentId] [int] NULL,
	[ffpm_Severity] [smallint] NULL,
	[ffpm_FPMessage] [varchar](1000) NULL,
	[ffpm_created] [datetime] NULL,
	[ffpm_Createdby] [int] NULL,
	[ffpm_Updated] [datetime] NULL,
	[ffpm_UpdatedBy] [int] NULL,
	[ffpm_deleted] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient_indicators](
	[pin_id] [int] IDENTITY(0,1) NOT NULL,
	[pin_pid] [int] NOT NULL CONSTRAINT [DF__Patient_indicators__pin_pid]  DEFAULT (0),
	[pin_type] [int] NOT NULL CONSTRAINT [DF__Patient_indicators__pin_type]  DEFAULT (0),
	[pin_indicator] [int] NOT NULL CONSTRAINT [DF__Patient_indicators__pin_indicator]  DEFAULT (0),
	[pin_freetext] [varchar](50) NOT NULL CONSTRAINT [DF__Patient_indicators__pin_freetext]  DEFAULT (''),
	[pin_createdby] [int] NOT NULL CONSTRAINT [DF__Patient_indicators__pin_createdby]  DEFAULT (0),
	[pin_created] [datetime] NOT NULL CONSTRAINT [DF__Patient_indicators__pin_update]  DEFAULT (getdate()),
	[Pin_expirydate] [datetime] NOT NULL CONSTRAINT [DF__Patient_Indicators__Pin_expirydate]  DEFAULT ('01/01/2200'),
	[Pin_TPID] [varchar](20) NOT NULL CONSTRAINT [DF__Patient_Indicators__Pin_TPID]  DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_eNotes](
	[nts_identity] [int] IDENTITY(0,1) NOT NULL,
	[nts_id] [int] NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_id]  DEFAULT (0),
	[nts_fieldid] [int] NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_fieldid]  DEFAULT (0),
	[nts_atdid] [int] NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_atdid]  DEFAULT (0),
	[nts_notetext] [varchar](4000) NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_notetext]  DEFAULT (''),
	[nts_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_update]  DEFAULT (getdate()),
	[nts_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_updatedby]  DEFAULT (0),
	[nts_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_created]  DEFAULT (getdate()),
	[nts_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_createdby]  DEFAULT (0),
	[nts_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_eNotes__nts_deleted]  DEFAULT (0),
	[nts_DEPID] [smallint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient](
	[pat_pid] [int] IDENTITY(0,1) NOT NULL,
	[pat_surname] [varchar](35) NOT NULL CONSTRAINT [DF__Patient__pat_surname]  DEFAULT (''),
	[pat_title] [int] NOT NULL CONSTRAINT [DF__Patient__pat_title]  DEFAULT (0),
	[pat_forename] [varchar](35) NOT NULL CONSTRAINT [DF__Patient__pat_forename]  DEFAULT (''),
	[pat_midnames] [varchar](35) NOT NULL CONSTRAINT [DF__Patient__pat_midnames]  DEFAULT (''),
	[pat_sex] [int] NOT NULL CONSTRAINT [DF__Patient__pat_sex]  DEFAULT (0),
	[pat_dob] [datetime] NOT NULL CONSTRAINT [DF__Patient__pat_dob]  DEFAULT ('01/01/1800'),
	[pat_dobtype] [int] NOT NULL CONSTRAINT [DF__Patient__pat_dobtype]  DEFAULT (0),
	[pat_tempreg] [tinyint] NOT NULL CONSTRAINT [DF__Patient__pat_tempreg]  DEFAULT (0),
	[pat_lastattno] [varchar](20) NOT NULL CONSTRAINT [DF__Patient__pat_lastattno]  DEFAULT (''),
	[pat_sursoundex] [varchar](4) NOT NULL CONSTRAINT [DF__Patient__pat_sursoundex]  DEFAULT (''),
	[pat_forsoundex] [varchar](4) NOT NULL CONSTRAINT [DF__Patient__pat_forsoundex]  DEFAULT (''),
	[pat_pasync] [bit] NOT NULL CONSTRAINT [DF__Patient__pat_pasync]  DEFAULT (0),
	[pat_createdby] [int] NOT NULL CONSTRAINT [DF__Patient__pat_createdby]  DEFAULT (0),
	[pat_update] [datetime] NOT NULL CONSTRAINT [DF__Patient__pat_update]  DEFAULT (getdate()),
	[pat_deleted] [bit] NOT NULL CONSTRAINT [DF__Patient__pat_deleted]  DEFAULT (0),
	[pat_IsDead] [bit] NOT NULL CONSTRAINT [DF__Patient__pat_IsDead]  DEFAULT (0)
) ON [PRIMARY]
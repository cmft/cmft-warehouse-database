﻿CREATE TABLE [dbo].[Cfg_DeptPDCSettings](
	[PDC_Id] [int] IDENTITY(0,1) NOT NULL,
	[PDC_DeptID] [int] NOT NULL CONSTRAINT [DF__Cfg_DeptPDCSettings__PDC_DeptID]  DEFAULT (0),
	[PDC_FieldID] [int] NOT NULL CONSTRAINT [DF__Cfg_DeptPDCSettings__PDC_FieldID]  DEFAULT (0),
	[PDC_Title] [varchar](50) NOT NULL CONSTRAINT [DF__Cfg_DeptPDCSettings__PDC_Title]  DEFAULT (''),
	[PDC_Createdby] [int] NOT NULL CONSTRAINT [DF__Cfg_DeptPDCSettings__PDC_CreatedBy]  DEFAULT (0),
	[PDC_Created] [datetime] NOT NULL CONSTRAINT [DF__Cfg_DeptPDCSettings__PDC_Created]  DEFAULT (getdate()),
	[PDC_Updated] [datetime] NOT NULL CONSTRAINT [DF__Cfg_DeptPDCSettings__PDC_Updated]  DEFAULT (getdate()),
	[PDC_PosIndex] [int] NOT NULL CONSTRAINT [DF__Cfg_DeptPDCSettings__PDC_PosIndex]  DEFAULT (0),
	[PDC_AttendanceType] [smallint] NOT NULL CONSTRAINT [DF__PDC_AttendanceType_PDC_AttendanceType]  DEFAULT (0)
) ON [PRIMARY]
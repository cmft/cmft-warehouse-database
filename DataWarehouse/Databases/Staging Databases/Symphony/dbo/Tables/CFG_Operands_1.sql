﻿CREATE TABLE [dbo].[CFG_Operands](
	[ffo_ID] [int] IDENTITY(0,1) NOT NULL,
	[ffo_OperandTypeId] [int] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_OperandTypeId]  DEFAULT (0),
	[ffo_OperandType] [smallint] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_OperandType]  DEFAULT (0),
	[ffo_OperandRecordType] [tinyint] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_OperandRecordType]  DEFAULT (0),
	[ffo_FormulaFieldDefID] [int] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_FormulaFieldDefID]  DEFAULT (0),
	[ffo_FieldCode] [varchar](10) NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_FieldCode]  DEFAULT (''),
	[ffo_ParentOperandId] [int] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_ParentOperandId]  DEFAULT (0),
	[ffo_DefaultForBlanks] [varchar](10) NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_DefaultForBlanks]  DEFAULT (''),
	[ffo_created] [datetime] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_created]  DEFAULT (getdate()),
	[ffo_createdby] [int] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_createdby]  DEFAULT (0),
	[ffo_updated] [datetime] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_updated]  DEFAULT (getdate()),
	[ffo_updatedby] [int] NOT NULL CONSTRAINT [DF_CFG_Operands_ffo_updatedby]  DEFAULT (0)
) ON [PRIMARY]
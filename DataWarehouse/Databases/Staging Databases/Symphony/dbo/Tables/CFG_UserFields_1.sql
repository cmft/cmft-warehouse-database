﻿CREATE TABLE [dbo].[CFG_UserFields](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [varchar](50) NULL,
	[TableName] [varchar](50) NULL,
	[FieldName] [varchar](50) NULL,
	[PropertyName] [varchar](50) NULL,
	[UserFriendlyName] [varchar](50) NULL,
	[DataType] [varchar](50) NULL,
	[HelpText] [varchar](255) NULL,
	[SymphonyDataType] [varchar](50) NULL
) ON [PRIMARY]
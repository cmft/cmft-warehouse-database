﻿CREATE TABLE [dbo].[patches](
	[dbp_id] [int] IDENTITY(1,1) NOT NULL,
	[dbp_name] [varchar](50) NOT NULL CONSTRAINT [DF__Patches__dbp_name]  DEFAULT (''),
	[dbp_date] [datetime] NOT NULL CONSTRAINT [DF__Patches__dbp_date]  DEFAULT (getdate())
) ON [PRIMARY]
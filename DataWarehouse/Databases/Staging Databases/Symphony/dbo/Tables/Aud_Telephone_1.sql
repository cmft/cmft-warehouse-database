﻿CREATE TABLE [dbo].[Aud_Telephone](
	[tel_identity] [int] IDENTITY(0,1) NOT NULL,
	[tel_telid] [int] NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_telid]  DEFAULT (0),
	[tel_home] [varchar](25) NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_home]  DEFAULT (''),
	[tel_work] [varchar](25) NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_work]  DEFAULT (''),
	[tel_mobile] [varchar](25) NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_mobile]  DEFAULT (''),
	[tel_extension] [varchar](25) NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_extension]  DEFAULT (''),
	[tel_pager] [varchar](25) NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_pager]  DEFAULT (''),
	[tel_fax] [varchar](25) NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_fax]  DEFAULT (''),
	[tel_email] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_email]  DEFAULT (''),
	[tel_phonetype] [int] NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_phonetype]  DEFAULT (0),
	[tel_linktype] [tinyint] NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_linktype]  DEFAULT (0),
	[tel_linkid] [int] NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_linkid]  DEFAULT (0),
	[tel_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_createdby]  DEFAULT (0),
	[tel_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_update]  DEFAULT (getdate()),
	[tel_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Telephone__tel_deleted]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_Scores](
	[sc_Id] [int] IDENTITY(0,1) NOT NULL,
	[sc_ScoreFieldDefId] [int] NOT NULL CONSTRAINT [DF_CFG_Scores_sc_ScoreFieldDefId]  DEFAULT (0),
	[sc_Score] [int] NOT NULL CONSTRAINT [DF_CFG_Scores_sc_Score]  DEFAULT (0),
	[sc_Description] [varchar](4000) NOT NULL CONSTRAINT [DF_CFG_Scores_sc_Description]  DEFAULT (''),
	[sc_Index] [smallint] NOT NULL CONSTRAINT [DF_CFG_Scores_sc_Index]  DEFAULT (0),
	[sc_Created] [datetime] NOT NULL CONSTRAINT [DF_CFG_Scores_sc_Created]  DEFAULT (getdate()),
	[sc_CreatedBy] [int] NOT NULL CONSTRAINT [DF_CFG_Scores_sc_CreatedBy]  DEFAULT (0),
	[sc_Updated] [datetime] NOT NULL CONSTRAINT [DF_CFG_Scores_sc_Updated]  DEFAULT (getdate()),
	[sc_UpdatedBy] [int] NOT NULL CONSTRAINT [DF_CFG_Scores_sc_UpdatedBy]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_DAD_Msg_EscapeDelimiters](
	[med_identity] [int] IDENTITY(0,1) NOT NULL,
	[med_structureid] [int] NOT NULL,
	[med_delimiter] [varchar](5) NOT NULL,
	[med_mapping] [varchar](50) NOT NULL,
	[med_optional] [bit] NOT NULL,
	[med_createdby] [int] NOT NULL,
	[med_created] [datetime] NOT NULL
) ON [PRIMARY]
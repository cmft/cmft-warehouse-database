﻿CREATE TABLE [dbo].[Cost_Level_Table](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[resid] [int] NULL,
	[res_atdid] [int] NULL,
	[res_result] [int] NULL,
	[not_text] [varchar](4000) NULL,
	[cost_level] [int] NULL
) ON [PRIMARY]
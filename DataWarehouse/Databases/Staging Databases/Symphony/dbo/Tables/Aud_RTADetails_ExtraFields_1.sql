﻿CREATE TABLE [dbo].[Aud_RTADetails_ExtraFields](
	[rta_EF_ID] [int] IDENTITY(0,1) NOT NULL,
	[rta_EF_extrafieldID] [int] NULL,
	[rta_EF_FieldType] [smallint] NULL,
	[rta_EF_FieldID] [int] NULL,
	[rta_EF_atdid] [int] NULL,
	[rta_EF_Value] [varchar](4000) NULL,
	[rta_EF_DataCategory] [smallint] NULL,
	[rta_EF_RecordID] [int] NULL,
	[rta_EF_FieldCodeValues] [varchar](200) NULL,
	[Rta_EF_ComputationState] [tinyint] NULL,
	[Rta_EF_depId] [smallint] NULL,
	[Rta_EF_CPMessagesIDs] [varchar](200) NULL,
	[rta_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Aud_RTADetails_ExtraFields__rta_EF_CreatedBy]  DEFAULT ((0)),
	[rta_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_RTADetails_ExtraFields__rta_EF_Created]  DEFAULT (getdate()),
	[rta_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_RTADetails_ExtraFields__rta_EF_updatedby]  DEFAULT ((0)),
	[rta_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_RTADetails_ExtraFields__rta_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_Cfg_DeptPDCSettings](
	[PDC_Identity] [int] IDENTITY(0,1) NOT NULL,
	[PDC_Id] [int] NULL,
	[PDC_DeptID] [int] NULL,
	[PDC_FieldID] [int] NULL,
	[PDC_Title] [varchar](50) NULL,
	[PDC_Createdby] [int] NULL,
	[PDC_Created] [datetime] NULL,
	[PDC_Updated] [datetime] NULL,
	[PDC_PosIndex] [int] NULL,
	[PDC_AttendanceType] [smallint] NULL
) ON [PRIMARY]
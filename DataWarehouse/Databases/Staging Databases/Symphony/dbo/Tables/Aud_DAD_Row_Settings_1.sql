﻿CREATE TABLE [dbo].[Aud_DAD_Row_Settings](
	[rst_identity] [int] IDENTITY(0,1) NOT NULL,
	[rst_rowid] [int] NOT NULL,
	[rst_name] [varchar](255) NOT NULL,
	[rst_value] [varchar](255) NOT NULL,
	[rst_createdby] [int] NOT NULL,
	[rst_created] [datetime] NOT NULL
) ON [PRIMARY]
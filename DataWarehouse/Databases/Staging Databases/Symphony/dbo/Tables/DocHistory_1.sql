﻿CREATE TABLE [dbo].[DocHistory](
	[doc_id] [int] IDENTITY(0,1) NOT NULL,
	[doc_name] [varchar](50) NOT NULL CONSTRAINT [DF__DocHistory__doc_name]  DEFAULT (''),
	[doc_pjbid] [int] NOT NULL CONSTRAINT [DF__DocHistory__doc_pjbid]  DEFAULT (0),
	[doc_atdid] [int] NOT NULL CONSTRAINT [DF__DocHistory__doc_atdid]  DEFAULT (0),
	[doc_printedby] [int] NOT NULL CONSTRAINT [DF__DocHistory__doc_printedby]  DEFAULT (0),
	[doc_printedon] [datetime] NOT NULL CONSTRAINT [DF__DocHistory__doc_printedon]  DEFAULT (getdate()),
	[doc_Imageid] [int] NOT NULL CONSTRAINT [DF__DocHistory__doc_Imageid]  DEFAULT (0),
	[doc_createdby] [int] NOT NULL CONSTRAINT [DF__DocHistory__doc_createdby]  DEFAULT (0),
	[doc_created] [datetime] NOT NULL CONSTRAINT [DF__Episode__doc_created]  DEFAULT (getdate()),
	[Doc_ImagePath] [varchar](255) NOT NULL CONSTRAINT [DF__DocHistory__Doc_ImagePath]  DEFAULT (''),
	[doc_status] [tinyint] NOT NULL CONSTRAINT [DF__dochistory__doc_status]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_RTA_Vehicles](
	[rtv_identity] [int] IDENTITY(0,1) NOT NULL,
	[rtv_rtvid] [int] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_rtvid]  DEFAULT (0),
	[rtv_rtaid] [int] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_rtaid]  DEFAULT (0),
	[rtv_type] [int] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_type]  DEFAULT (0),
	[rtv_driversurname] [varchar](35) NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_driversurname]  DEFAULT (''),
	[rtv_driverforename] [varchar](35) NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_driverforename]  DEFAULT (''),
	[rtv_driverinitials] [varchar](10) NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_driverinitials]  DEFAULT (''),
	[rtv_insnote] [int] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_insnote]  DEFAULT (0),
	[rtv_principal] [int] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_principal]  DEFAULT (0),
	[rtv_othervehicleknown] [int] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_othervehicleknown]  DEFAULT (0),
	[rtv_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_updatedby]  DEFAULT (0),
	[rtv_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_createdby]  DEFAULT (0),
	[rtv_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_update]  DEFAULT (getdate()),
	[rtv_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_created]  DEFAULT (getdate()),
	[rtv_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_RTA_Vehicles__rtv_deleted]  DEFAULT (0)
) ON [PRIMARY]
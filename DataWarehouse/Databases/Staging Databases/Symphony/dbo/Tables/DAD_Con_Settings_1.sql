﻿CREATE TABLE [dbo].[DAD_Con_Settings](
	[cst_parentid] [int] NOT NULL CONSTRAINT [DF__DAD_Con_Settings__cst_parentid]  DEFAULT (0),
	[cst_parent] [tinyint] NOT NULL CONSTRAINT [DF__DAD_Con_Settings__cst_parent]  DEFAULT (0),
	[cst_typename] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Con_Settings__cst_typename]  DEFAULT (''),
	[cst_value] [varchar](255) NOT NULL CONSTRAINT [DF__DAD_Con_Settings__cst_value]  DEFAULT (''),
	[cst_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Con_Settings__cst_createdby]  DEFAULT (0),
	[cst_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Con_Settings__cst_created]  DEFAULT (getdate())
) ON [PRIMARY]
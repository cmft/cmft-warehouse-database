﻿CREATE TABLE [dbo].[Aud_CFG_DefaultLocs](
	[dl_identity] [int] IDENTITY(0,1) NOT NULL,
	[dl_ID] [int] NULL,
	[dl_DEPStageID] [smallint] NULL,
	[dl_LocationID] [smallint] NULL,
	[dl_InActiveDueToParent] [bit] NULL,
	[dl_Index] [int] NULL,
	[dl_Createdby] [int] NULL,
	[dl_Created] [datetime] NULL,
	[dl_Updated] [datetime] NULL,
	[dl_UpdateBy] [int] NULL
) ON [PRIMARY]
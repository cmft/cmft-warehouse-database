﻿CREATE TABLE [dbo].[Import_CMMCAEException_NoMatchCatchup](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[DataSETName] [varchar](50) NULL DEFAULT (''),
	[Health Organisation Id] [varchar](50) NULL DEFAULT (''),
	[Patient ID] [varchar](50) NULL DEFAULT (''),
	[AENo] [varchar](50) NULL DEFAULT (''),
	[AttendDate] [varchar](50) NULL DEFAULT (''),
	[Seen Date] [varchar](50) NULL DEFAULT (''),
	[Complaint Code 1] [varchar](50) NULL DEFAULT (''),
	[Complaint Code 2] [varchar](50) NULL DEFAULT (''),
	[Complaint Code 3] [varchar](50) NULL DEFAULT (''),
	[Consultant] [varchar](50) NULL DEFAULT (''),
	[Diagnosis Date] [varchar](50) NULL DEFAULT (''),
	[Diagnosis Code] [varchar](50) NULL DEFAULT (''),
	[Method Of Departure] [varchar](50) NULL DEFAULT (''),
	[Date of Departure] [varchar](50) NULL DEFAULT ('')
) ON [PRIMARY]
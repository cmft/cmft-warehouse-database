﻿CREATE TABLE [dbo].[Aud_DAD_Msg_Definition](
	[mdf_identity] [int] IDENTITY(0,1) NOT NULL,
	[mdf_id] [int] NOT NULL,
	[mdf_structureid] [int] NOT NULL,
	[mdf_name] [varchar](100) NOT NULL,
	[mdf_direction] [tinyint] NOT NULL,
	[mdf_pmiaction] [tinyint] NOT NULL,
	[mdf_msgaction] [tinyint] NOT NULL,
	[mdf_msgactionvalue] [varchar](10) NOT NULL,
	[mdf_lkpmapping] [int] NOT NULL,
	[mdf_acktimeout] [smallint] NOT NULL,
	[mdf_ackretries] [tinyint] NOT NULL,
	[mdf_description] [varchar](2000) NOT NULL,
	[mdf_hl7parsing] [bit] NOT NULL,
	[mdf_usequotes] [bit] NOT NULL,
	[mdf_createdby] [int] NOT NULL,
	[mdf_created] [datetime] NOT NULL,
	[mdf_Status] [bit] NULL
) ON [PRIMARY]
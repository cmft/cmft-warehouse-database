﻿CREATE TABLE [dbo].[DAD_Msg_EscapeDelimiters](
	[med_structureid] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_EscapeDelimiters__med_structureid]  DEFAULT (0),
	[med_delimiter] [varchar](5) NOT NULL CONSTRAINT [DF__DAD_Msg_EscapeDelimiters__med_delimiter]  DEFAULT (''),
	[med_mapping] [varchar](50) NOT NULL CONSTRAINT [DF__DAD_Msg_EscapeDelimiters__med_mapping]  DEFAULT (''),
	[med_optional] [bit] NOT NULL CONSTRAINT [DF__DAD_Msg_EscapeDelimiters__med_optional]  DEFAULT (0),
	[med_createdby] [int] NOT NULL CONSTRAINT [DF__DAD_Msg_EscapeDelimiters__med_createdby]  DEFAULT (0),
	[med_created] [datetime] NOT NULL CONSTRAINT [DF__DAD_Msg_EscapeDelimiters__med_created]  DEFAULT (getdate())
) ON [PRIMARY]
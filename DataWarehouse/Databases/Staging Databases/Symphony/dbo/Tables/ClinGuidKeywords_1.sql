﻿CREATE TABLE [dbo].[ClinGuidKeywords](
	[cgk_lkpid] [int] NOT NULL CONSTRAINT [DF__ClinGuidKeywords__cgk_lkpid]  DEFAULT (0),
	[cgk_kwdid] [int] NOT NULL CONSTRAINT [DF__ClinGuidKeywords__cgk_kwdid]  DEFAULT (0),
	[cgk_createdby] [int] NOT NULL CONSTRAINT [DF__ClinGuidKeywords__cgk_createdby]  DEFAULT (0),
	[cgk_created] [datetime] NOT NULL CONSTRAINT [DF__ClinGuidKeywords__cgk_created]  DEFAULT (getdate())
) ON [PRIMARY]
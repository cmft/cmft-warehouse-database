﻿CREATE TABLE [dbo].[cfg_LkpItemsActivePeriod](
	[lks_Id] [int] IDENTITY(0,1) NOT NULL,
	[lks_lkpId] [int] NOT NULL CONSTRAINT [DF__cfg_LkpItemsActivePeriod__lks_lkpId]  DEFAULT (0),
	[lks_Active] [bit] NOT NULL CONSTRAINT [DF__cfg_LkpItemsActivePeriod__lks_Active]  DEFAULT (1),
	[lks_itemtype] [smallint] NOT NULL CONSTRAINT [DF__cfg_LkpItemsActivePeriod__lks_ItemType]  DEFAULT (0),
	[lks_parent] [int] NOT NULL CONSTRAINT [DF__cfg_LkpItemsActivePeriod__lks_Parent]  DEFAULT (0),
	[lks_createdBy] [int] NOT NULL CONSTRAINT [DF__cfg_LkpItemsActivePeriod__lks_createdBy]  DEFAULT (0),
	[lks_Datecreated] [datetime] NOT NULL CONSTRAINT [DF__cfg_LkpItemsActivePeriod__lks_Datecreated]  DEFAULT (getdate())
) ON [PRIMARY]
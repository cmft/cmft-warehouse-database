﻿CREATE TABLE [dbo].[Aud_CFG_DeptEpdCountSettings](
	[ECS_Identity] [int] IDENTITY(0,1) NOT NULL,
	[ECS_ID] [int] NULL,
	[ECS_DeptID] [int] NULL,
	[ECS_MaxPatientAge] [smallint] NULL,
	[ECS_EpdCountDuration] [int] NULL,
	[ECS_IncludeClinicAttsInEpdCount] [bit] NULL,
	[ECS_DisplayTotalEpdCount] [bit] NULL,
	[ECS_Created] [datetime] NULL,
	[ECS_CreatedBy] [int] NULL,
	[ECS_Updated] [datetime] NULL,
	[ECS_UpdatedBy] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CFG_DNMBrushes](
	[dnb_id] [int] IDENTITY(0,1) NOT NULL,
	[dnb_depid] [smallint] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_depid]  DEFAULT (0),
	[dnb_lookupid] [int] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_lookupid]  DEFAULT (0),
	[dnb_cursor] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_cursor]  DEFAULT (0),
	[dnb_iconid] [int] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_iconid]  DEFAULT (0),
	[dnb_drawingstyle] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_drawingstyle]  DEFAULT (0),
	[dnb_color] [int] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_color]  DEFAULT (0),
	[dnb_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_createdby]  DEFAULT (0),
	[dnb_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_created]  DEFAULT (getdate()),
	[dnb_deleted] [bit] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_deleted]  DEFAULT (0),
	[dnb_updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_DNMBrushes__dnb_updated]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_TriageExtraFields](
	[tri_EF_ID] [int] IDENTITY(0,1) NOT NULL,
	[tri_EF_extrafieldID] [int] NULL,
	[tri_EF_FieldType] [smallint] NULL,
	[tri_EF_FieldID] [int] NULL,
	[tri_EF_atdid] [int] NULL,
	[tri_EF_Value] [varchar](4000) NULL,
	[tri_EF_DataCategory] [smallint] NULL,
	[tri_EF_RecordID] [int] NULL,
	[tri_EF_FieldCodeValues] [varchar](200) NULL,
	[tri_EF_ComputationState] [tinyint] NULL,
	[Tri_EF_depId] [smallint] NULL,
	[Tri_EF_CPMessagesIDs] [varchar](200) NULL,
	[tri_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Aud_TriageExtraFields__tri_EF_CreatedBy]  DEFAULT ((0)),
	[tri_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_TriageExtraFields__tri_EF_Created]  DEFAULT (getdate()),
	[tri_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_TriageExtraFields__tri_EF_updatedby]  DEFAULT ((0)),
	[tri_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_TriageExtraFields__tri_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
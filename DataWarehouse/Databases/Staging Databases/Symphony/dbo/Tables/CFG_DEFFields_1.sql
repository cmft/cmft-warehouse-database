﻿CREATE TABLE [dbo].[CFG_DEFFields](
	[dff_deform] [smallint] NOT NULL CONSTRAINT [DF__CFG_DEFFi__dff_deform]  DEFAULT (0),
	[dff_defield] [int] NOT NULL CONSTRAINT [DF__CFG_DEFFi__dff_defield]  DEFAULT (0),
	[dff_fldmandatory] [bit] NOT NULL CONSTRAINT [DF__CFG_DEFFi__dff_fldmandatory]  DEFAULT (1),
	[dff_fldIndex] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DEFFields__dff_fldIndex]  DEFAULT (0),
	[dff_editingmandatory] [bit] NOT NULL CONSTRAINT [DF__CFG_DEFFields__dff_EditingMandatory]  DEFAULT (0),
	[dff_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DEFFields__dff_createdby]  DEFAULT (0),
	[dff_update] [datetime] NOT NULL CONSTRAINT [DF__CFG_DEFFields__dff_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_Episodes](
	[epd_identity] [int] IDENTITY(0,1) NOT NULL,
	[epd_id] [int] NOT NULL CONSTRAINT [DF__Aud_Episodes__epd_id]  DEFAULT (0),
	[epd_pid] [int] NOT NULL CONSTRAINT [DF__Aud_Episodes__epd_pid]  DEFAULT (0),
	[epd_num] [varchar](20) NOT NULL CONSTRAINT [DF__Aud_Episodes__epd_num]  DEFAULT (''),
	[epd_deptid] [int] NOT NULL CONSTRAINT [DF__Aud_Episodes__epd_deptid]  DEFAULT (0),
	[epd_recordedby] [int] NOT NULL CONSTRAINT [DF__Aud_Episodes__epd_recordedby]  DEFAULT (0),
	[epd_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Episodes__epd_createdby]  DEFAULT (0),
	[epd_update] [datetime] NOT NULL CONSTRAINT [DF___Episodes__epd_update]  DEFAULT (getdate()),
	[epd_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Episodes__epd_deleted]  DEFAULT (0)
) ON [PRIMARY]
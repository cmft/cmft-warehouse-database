﻿CREATE TABLE [dbo].[DAD_Service_Time_Stamps](
	[dsts_id] [int] IDENTITY(1,1) NOT NULL,
	[dsts_serviceid] [int] NOT NULL CONSTRAINT [DF_DAD_Service_Time_Stamps_dsts_serviceid]  DEFAULT (0),
	[dsts_createdby] [int] NOT NULL CONSTRAINT [DF_DAD_Service_Time_Stamps_dsts_createdby]  DEFAULT (0),
	[dsts_created] [datetime] NOT NULL CONSTRAINT [DF_DAD_Service_Time_Stamps_dsts_created]  DEFAULT (getdate()),
	[dsts_timestamp] [datetime] NOT NULL CONSTRAINT [DF_DAD_Service_Time_Stamps_dsts_timestamp]  DEFAULT ('2200/01/01')
) ON [PRIMARY]
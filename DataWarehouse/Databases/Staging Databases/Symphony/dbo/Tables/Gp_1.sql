﻿CREATE TABLE [dbo].[Gp](
	[gp_code] [varchar](14) NOT NULL CONSTRAINT [DF__Gp__gp_code]  DEFAULT ('G9999998'),
	[gp_startdate] [datetime] NOT NULL CONSTRAINT [DF__Gp__gp_startdate]  DEFAULT ('2200/01/01'),
	[gp_enddate] [datetime] NOT NULL CONSTRAINT [DF__Gp__gp_enddate]  DEFAULT ('2200/01/01'),
	[gp_surname] [varchar](35) NOT NULL CONSTRAINT [DF__Gp__gp_surname]  DEFAULT (''),
	[gp_initials] [varchar](30) NOT NULL CONSTRAINT [DF__Gp__gp_initials]  DEFAULT (''),
	[gp_title] [int] NOT NULL CONSTRAINT [DF__GP__gp_title]  DEFAULT (0),
	[gp_qualifications] [int] NOT NULL CONSTRAINT [DF__GP__gp_qualifications]  DEFAULT (0),
	[gp_status] [int] NOT NULL CONSTRAINT [DF__Gp__gp_status]  DEFAULT (0),
	[gp_letterpref] [varchar](35) NULL CONSTRAINT [DF__Gp__gp_letterpref]  DEFAULT (''),
	[gp_createdby] [int] NOT NULL CONSTRAINT [DF__Gp__gp_createdby]  DEFAULT (0),
	[gp_update] [datetime] NOT NULL CONSTRAINT [DF__Gp__gp_update]  DEFAULT (getdate()),
	[gp_id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
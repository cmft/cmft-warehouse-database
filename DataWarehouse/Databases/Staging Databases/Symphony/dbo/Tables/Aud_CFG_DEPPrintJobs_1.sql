﻿CREATE TABLE [dbo].[Aud_CFG_DEPPrintJobs](
	[dpj_identity] [int] IDENTITY(0,1) NOT NULL,
	[dpj_pjbid] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPPrintJobs__dpj_pjbid]  DEFAULT (0),
	[dpj_depid] [smallint] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPPrintJobs__dpj_depid]  DEFAULT (0),
	[dpj_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPPrintJobs__dpj_update]  DEFAULT (getdate()),
	[dpj_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPPrintJobs__dpj_createdby]  DEFAULT (0),
	[dpj_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_CFG_DEPPrintJobs__dpj_deleted]  DEFAULT (0)
) ON [PRIMARY]
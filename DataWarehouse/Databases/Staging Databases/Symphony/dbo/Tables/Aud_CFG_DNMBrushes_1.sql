﻿CREATE TABLE [dbo].[Aud_CFG_DNMBrushes](
	[dnb_identity] [int] IDENTITY(0,1) NOT NULL,
	[dnb_id] [int] NOT NULL,
	[dnb_depid] [smallint] NOT NULL,
	[dnb_lookupid] [int] NOT NULL,
	[dnb_cursor] [tinyint] NOT NULL,
	[dnb_iconid] [int] NOT NULL,
	[dnb_drawingstyle] [tinyint] NOT NULL,
	[dnb_color] [int] NOT NULL,
	[dnb_createdby] [int] NOT NULL,
	[dnb_created] [datetime] NOT NULL,
	[dnb_updated] [datetime] NOT NULL,
	[dnb_deleted] [bit] NOT NULL
) ON [PRIMARY]
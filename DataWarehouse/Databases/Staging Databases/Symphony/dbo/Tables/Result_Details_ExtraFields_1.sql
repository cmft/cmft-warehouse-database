﻿CREATE TABLE [dbo].[Result_Details_ExtraFields](
	[res_EF_extrafieldID] [int] IDENTITY(0,1) NOT NULL,
	[res_EF_FieldType] [smallint] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__FieldType]  DEFAULT (0),
	[res_EF_FieldID] [int] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__FieldID]  DEFAULT (0),
	[res_EF_atdid] [int] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__atdid]  DEFAULT (0),
	[res_EF_Value] [varchar](4000) NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__Value]  DEFAULT (''),
	[res_EF_DataCategory] [smallint] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__DataCategory]  DEFAULT (0),
	[res_EF_RecordID] [int] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__RecordID]  DEFAULT (0),
	[res_EF_FieldCodeValues] [varchar](200) NOT NULL CONSTRAINT [DF_Result_Details_ExtraFields_res_EF_FieldCodeValues]  DEFAULT (''),
	[Res_EF_ComputationState] [tinyint] NOT NULL CONSTRAINT [DF_Result_Details_ExtraFields_Res_EF_ComputationState]  DEFAULT (0),
	[Res_EF_depId] [smallint] NOT NULL CONSTRAINT [DF_Result_Details_ExtraFields_Res_EF_depId]  DEFAULT ((0)),
	[Res_EF_CPMessagesIDs] [varchar](200) NOT NULL CONSTRAINT [DF_Result_Details_ExtraFields_Res_EF_CPMessagesIDs]  DEFAULT (''),
	[res_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__res_EF_CreatedBy]  DEFAULT ((0)),
	[res_EF_created] [datetime] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__res_EF_Created]  DEFAULT (getdate()),
	[res_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__res_EF_updatedby]  DEFAULT ((0)),
	[res_EF_update] [datetime] NOT NULL CONSTRAINT [DF__Result_Details_ExtraFields__res_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Aud_DAD_Failed_Messages](
	[fam_srvid] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Failed_Messages__fam_srvid]  DEFAULT (0),
	[fam_patid] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Failed_Messages__fam_patid]  DEFAULT (0),
	[fam_msgdef] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Failed_Messages__fam_msgdef]  DEFAULT (0),
	[fam_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Failed_Messages__fam_createdby]  DEFAULT (0),
	[fam_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_DAD_Failed_Messages__fam_created]  DEFAULT (getdate()),
	[fam_atdid] [int] NOT NULL CONSTRAINT [AUD_DF__DAD_Failed_Messages__fam_atdid]  DEFAULT (0),
	[fam_status] [tinyint] NOT NULL CONSTRAINT [AUD_DF__DAD_Failed_Messages__fam_status]  DEFAULT ((0)),
	[fam_reason] [varchar](3500) NULL CONSTRAINT [AUD_DF__DAD_Failed_Messages__fam_reason]  DEFAULT (''),
	[fam_deptid] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Failed_Messages__fam_deptid]  DEFAULT (0),
	[fam_LocMsgLocID] [int] NOT NULL CONSTRAINT [DF__Aud_DAD_Failed_Messages__fam_LocMsgLocID]  DEFAULT ((0)),
	[Fam_CurrentRecords] [nvarchar](255) NOT NULL CONSTRAINT [DF__Aud_DAD_Failed_Messages__Fam_CurrentRecords]  DEFAULT (''),
	[Fam_id] [int] NOT NULL CONSTRAINT [DF__AUD_DAD_Failed_Messages__fam_id]  DEFAULT ((0)),
	[fam_identity] [int] IDENTITY(1,1) NOT NULL,
	[fam_attempts] [smallint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
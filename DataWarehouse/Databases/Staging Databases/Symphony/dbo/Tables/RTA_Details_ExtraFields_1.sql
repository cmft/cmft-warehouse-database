﻿CREATE TABLE [dbo].[RTA_Details_ExtraFields](
	[rta_EF_extrafieldID] [int] IDENTITY(0,1) NOT NULL,
	[rta_EF_FieldType] [smallint] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__FieldType]  DEFAULT (0),
	[rta_EF_FieldID] [int] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__FieldID]  DEFAULT (0),
	[rta_EF_atdid] [int] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__atdid]  DEFAULT (0),
	[rta_EF_Value] [varchar](4000) NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__Value]  DEFAULT (''),
	[rta_EF_DataCategory] [smallint] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__DataCategory]  DEFAULT (0),
	[rta_EF_RecordID] [int] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__RecordID]  DEFAULT (0),
	[rta_EF_FieldCodeValues] [varchar](200) NOT NULL CONSTRAINT [DF_RTA_Details_ExtraFields_rta_EF_FieldCodeValues]  DEFAULT (''),
	[Rta_EF_ComputationState] [tinyint] NOT NULL CONSTRAINT [DF_RTA_Details_ExtraFields_Rta_EF_ComputationState]  DEFAULT (0),
	[Rta_EF_depId] [smallint] NOT NULL CONSTRAINT [DF_RTA_Details_ExtraFields_Rta_EF_depId]  DEFAULT ((0)),
	[Rta_EF_CPMessagesIDs] [varchar](200) NOT NULL CONSTRAINT [DF_RTA_Details_ExtraFields_Rta_EF_CPMessagesIDs]  DEFAULT (''),
	[rta_EF_CreatedBy] [int] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__rta_EF_CreatedBy]  DEFAULT ((0)),
	[rta_EF_created] [datetime] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__rta_EF_Created]  DEFAULT (getdate()),
	[rta_EF_updatedby] [int] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__rta_EF_updatedby]  DEFAULT ((0)),
	[rta_EF_update] [datetime] NOT NULL CONSTRAINT [DF__RTA_Details_ExtraFields__rta_EF_update]  DEFAULT (getdate())
) ON [PRIMARY]
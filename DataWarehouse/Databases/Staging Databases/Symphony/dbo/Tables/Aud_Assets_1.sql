﻿CREATE TABLE [dbo].[Aud_Assets](
	[ass_identity] [int] IDENTITY(0,1) NOT NULL,
	[ass_id] [int] NULL,
	[ass_OwnerId] [int] NULL,
	[ass_assetid] [int] NULL,
	[ass_status] [smallint] NULL,
	[ass_createdby] [int] NULL,
	[ass_updated] [datetime] NULL,
	[ass_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Assets__ass_deleted]  DEFAULT (0),
	[ass_issuingDept] [int] NULL,
	[ass_printjobids] [varchar](1000) NULL,
	[ass_datecreated] [datetime] NULL,
	[ass_updatedby] [int] NULL,
	[ass_serialnumber1] [varchar](30) NULL,
	[ass_serialnumber2] [varchar](30) NULL
) ON [PRIMARY]
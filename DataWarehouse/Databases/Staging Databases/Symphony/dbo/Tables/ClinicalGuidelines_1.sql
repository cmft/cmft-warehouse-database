﻿CREATE TABLE [dbo].[ClinicalGuidelines](
	[clg_lkpid] [int] NOT NULL CONSTRAINT [DF__ClinicalGuidelines__clg_lkpid]  DEFAULT (0),
	[clg_IsReference] [bit] NOT NULL CONSTRAINT [DF__ClinicalGuidelines__clg_IsReference]  DEFAULT (0),
	[clg_text] [ntext] NULL,
	[clg_createdby] [int] NOT NULL CONSTRAINT [DF__ClinicalGuidelines__clg_createdby]  DEFAULT (0),
	[clg_created] [datetime] NOT NULL CONSTRAINT [DF__ClinicalGuidelines__clg_created]  DEFAULT (getdate()),
	[timestamp] [timestamp] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Keywords](
	[kwd_id] [int] IDENTITY(0,1) NOT NULL,
	[kwd_word] [varchar](100) NOT NULL CONSTRAINT [DF__Keywords__kwd_word]  DEFAULT (''),
	[kwd_createdby] [int] NOT NULL CONSTRAINT [DF__Keywords__kwd_createdby]  DEFAULT (0),
	[kwd_created] [datetime] NOT NULL CONSTRAINT [DF__Keywords__kwd_created]  DEFAULT (getdate())
) ON [PRIMARY]
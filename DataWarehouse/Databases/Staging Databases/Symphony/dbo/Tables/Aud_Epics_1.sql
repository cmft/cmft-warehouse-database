﻿CREATE TABLE [dbo].[Aud_Epics](
	[epi_identity] [int] IDENTITY(0,1) NOT NULL,
	[epi_epicsid] [int] NOT NULL CONSTRAINT [DF_Aud_Epics_aep_epicsid]  DEFAULT (0),
	[epi_imageid] [int] NOT NULL CONSTRAINT [DF_Aud_Epics_aep_imageid]  DEFAULT (0),
	[epi_uploaddate] [datetime] NOT NULL CONSTRAINT [DF_Aud_Epics_aep_uploaddate]  DEFAULT (getdate()),
	[epi_notes] [int] NOT NULL CONSTRAINT [DF_Aud_Epics_aep_notes]  DEFAULT (0),
	[epi_parentno] [int] NOT NULL CONSTRAINT [DF_Aud_Epics_aep_parentno]  DEFAULT (0),
	[epi_parentid] [int] NOT NULL CONSTRAINT [DF_Aud_Epics_aep_parentid]  DEFAULT (0),
	[epi_categories] [varchar](150) NOT NULL CONSTRAINT [DF_Aud_Epics_epi_categories]  DEFAULT (''),
	[epi_createdby] [int] NOT NULL CONSTRAINT [DF_Aud_Epics_aep_createdby]  DEFAULT (0),
	[epi_update] [datetime] NOT NULL CONSTRAINT [DF_Aud_Epics_aep_update]  DEFAULT (getdate()),
	[epi_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Epics__epi_deleted]  DEFAULT (0),
	[Epi_ImagePath] [varchar](255) NOT NULL CONSTRAINT [DF__Aud_Epics__epi_ImagePath]  DEFAULT ('')
) ON [PRIMARY]
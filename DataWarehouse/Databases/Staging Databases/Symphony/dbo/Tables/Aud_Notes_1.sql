﻿CREATE TABLE [dbo].[Aud_Notes](
	[not_identity] [int] IDENTITY(0,1) NOT NULL,
	[not_noteid] [int] NOT NULL CONSTRAINT [DF__Aud_Notes__not_noteid]  DEFAULT (0),
	[not_text] [varchar](4000) NOT NULL CONSTRAINT [DF__Aud_Notes__not_ttext]  DEFAULT (''),
	[not_recordedby] [int] NOT NULL CONSTRAINT [DF__Aud_Notes__not_recordedby]  DEFAULT (0),
	[not_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Notes__not_createdby]  DEFAULT (0),
	[not_update] [datetime] NOT NULL CONSTRAINT [DF__Aud_Notes__not_update]  DEFAULT (getdate()),
	[not_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Notes__not_deleted]  DEFAULT (0)
) ON [PRIMARY]
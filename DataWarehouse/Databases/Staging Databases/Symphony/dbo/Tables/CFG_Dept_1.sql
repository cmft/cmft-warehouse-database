﻿CREATE TABLE [dbo].[CFG_Dept](
	[dpt_id] [int] IDENTITY(0,1) NOT NULL,
	[dpt_name] [varchar](20) NOT NULL CONSTRAINT [DF__CFG_Dept__dpt_name]  DEFAULT (''),
	[dpt_shortname] [varchar](4) NOT NULL CONSTRAINT [DF__CFG_Dept__dpt_shortname]  DEFAULT (''),
	[dpt_hasbeenused] [bit] NOT NULL CONSTRAINT [DF__CFG_Dept__dpt_hasbeenused]  DEFAULT (0),
	[dpt_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_Dept__dpt_created]  DEFAULT (getdate()),
	[dpt_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_Dept__dpt_createdby]  DEFAULT (0),
	[dpt_updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_Dept__dpt_updated]  DEFAULT (getdate()),
	[dpt_updatedby] [int] NOT NULL CONSTRAINT [DF__CFG_Dept__dpt_updatedby]  DEFAULT (0),
	[dpt_IsServiceDept] [bit] NOT NULL CONSTRAINT [DF__Cfg_Dept__dpt_IsServiceDept]  DEFAULT (0)
) ON [PRIMARY]
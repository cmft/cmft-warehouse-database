﻿CREATE TABLE [dbo].[Aud_CFG_ClinicList](
	[cfg_identity] [int] IDENTITY(0,1) NOT NULL,
	[cfg_clinicid] [int] NOT NULL CONSTRAINT [DF__Aud_Cfg_ClinicList__cfg_clinicid]  DEFAULT (0),
	[cfg_type] [int] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_type]  DEFAULT (0),
	[cfg_usfname] [varchar](50) NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_usfname]  DEFAULT (''),
	[cfg_clinician] [int] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_clinician]  DEFAULT (0),
	[cfg_firstapptime] [datetime] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_firstapptime]  DEFAULT (getdate()),
	[cfg_numberofappts] [smallint] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_numberofappts]  DEFAULT (0),
	[cfg_apptlength] [smallint] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_apptlength]  DEFAULT (0),
	[cfg_maxpatsperappt] [smallint] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_maxpatsperappt]  DEFAULT (0),
	[cfg_maxpatsperclinic] [smallint] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_maxpatsperclinic]  DEFAULT (0),
	[cfg_asfirstattendance] [tinyint] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_asfirstattendance]  DEFAULT (0),
	[cfg_startdate] [datetime] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_startdate]  DEFAULT (getdate()),
	[cfg_enddate] [datetime] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_enddate]  DEFAULT ('01/01/2200'),
	[cfg_monday] [bit] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_daysofweek]  DEFAULT (0),
	[cfg_tuesday] [bit] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_tuesday]  DEFAULT (0),
	[cfg_wednesday] [bit] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_wednesday]  DEFAULT (0),
	[cfg_thursday] [bit] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist_cfg__thursday]  DEFAULT (0),
	[cfg_friday] [bit] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist_cfg__friday]  DEFAULT (0),
	[cfg_saturday] [bit] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist_cfg__saturday]  DEFAULT (0),
	[cfg_sunday] [bit] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist_cfg__sunday]  DEFAULT (0),
	[CFG_Deptid] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_ClinicList__CFG_Deptid]  DEFAULT (0),
	[cfg_createdby] [int] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_createdby]  DEFAULT (0),
	[cfg_created] [datetime] NOT NULL CONSTRAINT [DF__Aud_Cfg_Cliniclist__cfg_created]  DEFAULT (getdate()),
	[cfg_updatedby] [int] NOT NULL CONSTRAINT [DF__Aud_CFG_ClinicList_cfg_updatedby]  DEFAULT (0),
	[cfg_updated] [datetime] NOT NULL CONSTRAINT [DF__Aud_CFG_ClinicList_cfg_updated]  DEFAULT (getdate()),
	[cfg_deleted] [bit] NOT NULL CONSTRAINT [DF__Aud_Cfg_ClinicList__cfg_deleted]  DEFAULT (0)
) ON [PRIMARY]
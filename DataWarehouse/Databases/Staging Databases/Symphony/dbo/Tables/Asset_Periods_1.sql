﻿CREATE TABLE [dbo].[Asset_Periods](
	[asp_id] [int] IDENTITY(0,1) NOT NULL,
	[asp_parentid] [int] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_parentid]  DEFAULT (0),
	[asp_issuedate] [datetime] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_issuedate]  DEFAULT ('01/01/2200'),
	[asp_status] [smallint] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_status]  DEFAULT (0),
	[asp_createdby] [int] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_createdby]  DEFAULT (0),
	[asp_updated] [datetime] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_updated]  DEFAULT (getdate()),
	[asp_period] [int] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_period]  DEFAULT (0),
	[asp_datecreated] [datetime] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_datecreated]  DEFAULT (getdate()),
	[asp_updatedby] [int] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_updatedby]  DEFAULT (0),
	[asp_returnDate] [datetime] NOT NULL CONSTRAINT [DF__Asset_Periods__asp_returnDate]  DEFAULT ('01/01/2200')
) ON [PRIMARY]
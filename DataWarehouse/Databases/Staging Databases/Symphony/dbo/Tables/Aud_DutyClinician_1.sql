﻿CREATE TABLE [dbo].[Aud_DutyClinician](
	[dcl_identity] [int] IDENTITY(0,1) NOT NULL,
	[dcl_id] [int] NULL,
	[dcl_staffid] [int] NULL,
	[dcl_startDate] [datetime] NULL,
	[dcl_enddate] [datetime] NULL,
	[dcl_created] [datetime] NULL,
	[dcl_updated] [datetime] NULL,
	[dcl_createdby] [int] NULL,
	[dcl_updatedby] [int] NULL,
	[dcl_deleted] [bit] NULL CONSTRAINT [DF__Aud_DutyClinician__dcl_deleted]  DEFAULT (0)
) ON [PRIMARY]
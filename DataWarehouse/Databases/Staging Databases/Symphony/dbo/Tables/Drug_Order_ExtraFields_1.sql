﻿CREATE TABLE [dbo].[Drug_Order_ExtraFields](
	[dgo_EF_extrafieldID] [int] IDENTITY(0,1) NOT NULL,
	[dgo_EF_FieldType] [smallint] NOT NULL,
	[dgo_EF_FieldID] [int] NOT NULL,
	[dgo_EF_atdid] [int] NOT NULL,
	[dgo_EF_Value] [varchar](4000) NOT NULL,
	[dgo_EF_DataCategory] [smallint] NOT NULL,
	[dgo_EF_RecordID] [int] NOT NULL,
	[dgo_EF_FieldCodeValues] [varchar](200) NOT NULL,
	[dgo_EF_ComputationState] [tinyint] NOT NULL,
	[dgo_EF_depId] [smallint] NOT NULL,
	[dgo_EF_CPMessagesIDs] [varchar](200) NOT NULL,
	[dgo_EF_CreatedBy] [int] NOT NULL,
	[dgo_EF_created] [datetime] NOT NULL,
	[dgo_EF_updatedby] [int] NOT NULL,
	[dgo_EF_update] [datetime] NOT NULL
) ON [PRIMARY]
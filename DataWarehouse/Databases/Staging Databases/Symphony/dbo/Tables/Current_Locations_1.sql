﻿CREATE TABLE [dbo].[Current_Locations](
	[cul_culid] [int] IDENTITY(0,1) NOT NULL,
	[cul_atdid] [int] NOT NULL CONSTRAINT [DF_Current_Locations_cul_atdid]  DEFAULT (0),
	[cul_locationid] [smallint] NOT NULL CONSTRAINT [DF_Current_Locations_cul_locid]  DEFAULT (0),
	[cul_locationdate] [datetime] NOT NULL CONSTRAINT [DF_Current_Locations_cul_locationdate]  DEFAULT (getdate()),
	[cul_createdby] [int] NOT NULL CONSTRAINT [DF_Current_Locations_cul_createdby]  DEFAULT (0),
	[cul_update] [datetime] NOT NULL CONSTRAINT [DF_Current_Locations_cul_update]  DEFAULT (getdate()),
	[Cul_ReasonForMPC] [int] NOT NULL CONSTRAINT [DF_Current_locations_Cul_ReasonForMPC]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[HA_PCG](
	[ha_postcode] [varchar](10) NOT NULL CONSTRAINT [DF__HA_PCG__ha_postcode]  DEFAULT ('XXXXXXXXXX'),
	[ha_ha] [varchar](3) NOT NULL CONSTRAINT [DF__HA_PCG__ha_ha]  DEFAULT ('X98'),
	[ha_pcg] [varchar](5) NOT NULL CONSTRAINT [DF__HA_PCG__ha_pcg]  DEFAULT ('49998')
) ON [PRIMARY]
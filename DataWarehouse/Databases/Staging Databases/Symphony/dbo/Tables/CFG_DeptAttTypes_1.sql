﻿CREATE TABLE [dbo].[CFG_DeptAttTypes](
	[dat_id] [int] IDENTITY(0,1) NOT NULL,
	[dat_deptid] [int] NOT NULL CONSTRAINT [DF__CFG_DeptAttTypes__dat_deptid]  DEFAULT (0),
	[dat_atttype] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DeptAttTypes__dat_atttype]  DEFAULT (0),
	[dat_active] [bit] NOT NULL CONSTRAINT [DF__CFG_DeptAttTypes__dat_active]  DEFAULT (0),
	[dat_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DeptAttTypes__dat_createdby]  DEFAULT (0),
	[dat_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_DeptAttTypes__dat_created]  DEFAULT (getdate()),
	[dat_updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_DeptAttTypes__dat_updated]  DEFAULT (getdate()),
	[dat_updatedby] [int] NOT NULL CONSTRAINT [DF__CFG_DeptAttTypes__dat_updatedby]  DEFAULT (0)
) ON [PRIMARY]
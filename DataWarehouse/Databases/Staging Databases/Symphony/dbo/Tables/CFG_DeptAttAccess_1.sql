﻿CREATE TABLE [dbo].[CFG_DeptAttAccess](
	[daa_id] [int] IDENTITY(0,1) NOT NULL,
	[daa_deptid] [int] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess__daa_deptid]  DEFAULT (0),
	[daa_groupid] [int] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess__daa_groupid]  DEFAULT (0),
	[daa_attendancetype] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess__daa_attendancetype]  DEFAULT (0),
	[daa_cliniclistId] [int] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess__daa_cliniclistId]  DEFAULT (0),
	[daa_access] [tinyint] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess__daa_access]  DEFAULT (0),
	[daa_createdby] [int] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess __daa_createdby]  DEFAULT (0),
	[daa_created] [datetime] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess __daa_created]  DEFAULT (getdate()),
	[daa_updated] [datetime] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess __daa_updated]  DEFAULT (getdate()),
	[daa_updatedby] [int] NOT NULL CONSTRAINT [DF__CFG_DeptAttAccess __daa_updatedby]  DEFAULT (0)
) ON [PRIMARY]
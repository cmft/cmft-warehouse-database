﻿CREATE TABLE [dbo].[CFG_FormulaFieldMsgs](
	[ffpm_Id] [int] IDENTITY(0,1) NOT NULL,
	[ffpm_ParentId] [int] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldMsgs_ffpm_ParentId]  DEFAULT (0),
	[ffpm_Severity] [smallint] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldMsgs_ffpm_Severity]  DEFAULT (3),
	[ffpm_FPMessage] [varchar](1000) NOT NULL CONSTRAINT [DF_CFG_FormulaFieldMsgs_ffpm_FPMessage]  DEFAULT (''),
	[ffpm_created] [datetime] NOT NULL CONSTRAINT [DF_Table_2_ffd_created]  DEFAULT (getdate()),
	[ffpm_Createdby] [int] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldMsgs_ffpm_Createdby]  DEFAULT (0),
	[ffpm_Updated] [datetime] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldMsgs_ffpm_Updated]  DEFAULT (getdate()),
	[ffpm_UpdatedBy] [int] NOT NULL CONSTRAINT [DF_CFG_FormulaFieldMsgs_ffpm_UpdatedBy]  DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LKP_WorkPlanCode](
	[WorkLoadCode] [nvarchar](255) NULL,
	[WorkLoadTypeCode] [nvarchar](255) NULL,
	[WorkLoadTypeDesc] [nvarchar](255) NULL,
	[WorkLoadDesc] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[JobPlan](
	[ConsultantID] [int] NOT NULL,
	[WorkLoadID] [int] IDENTITY(1,1) NOT NULL,
	[DayOfWeekID] [int] NULL,
	[StartTime] [time](0) NULL,
	[FinishTime] [time](0) NULL,
	[SiteID] [nvarchar](255) NULL,
	[WorkLoadCode] [nvarchar](255) NULL,
	[FrequencyID] [nvarchar](255) NULL,
	[Comment] [nvarchar](255) NULL,
	[WorkTypeID] [int] NULL,
	[AddOnCHours] [numeric](18, 0) NULL
) ON [PRIMARY]
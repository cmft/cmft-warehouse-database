﻿CREATE TABLE [dbo].[LKP_DayOfWeek](
	[DayOfWeekID] [int] NULL,
	[DayOfWeekShort] [char](3) NULL,
	[DayOfWeekLong] [nvarchar](20) NULL
) ON [PRIMARY]
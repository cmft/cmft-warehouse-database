﻿CREATE TABLE [dbo].[LKP_Cons](
	[ConsultantCode] [nvarchar](255) NOT NULL,
	[MainSpecialty] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL
) ON [PRIMARY]
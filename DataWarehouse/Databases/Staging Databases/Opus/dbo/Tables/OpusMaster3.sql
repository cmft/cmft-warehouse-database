﻿CREATE TABLE [dbo].[OpusMaster3](
	[ConsultantCode] [nvarchar](255) NOT NULL,
	[DayOfWeekID] [nvarchar](255) NULL,
	[StartTime] [int] NULL,
	[FinishTime] [time](0) NULL,
	[Site] [nvarchar](255) NULL,
	[WorkLoadCode] [nvarchar](255) NULL,
	[FrequencyCode] [nvarchar](255) NULL,
	[Comment] [nvarchar](255) NULL,
	[WorkLoadID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[LKP_Frequency](
	[FrequencyID] [int] NOT NULL,
	[Frequency] [nvarchar](20) NULL,
	[Mulitplier] [numeric](18, 4) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AlternatePatientId](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[PatientId] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](20) NOT NULL,
	[SourceCode] [nvarchar](40) NOT NULL,
	[SourceName] [nvarchar](80) NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
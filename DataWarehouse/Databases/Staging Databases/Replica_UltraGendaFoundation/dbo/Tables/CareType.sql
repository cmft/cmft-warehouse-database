﻿CREATE TABLE [dbo].[CareType](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[Code] [nvarchar](40) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Type] [nchar](1) NOT NULL,
	[ReferrerType] [nchar](1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ContactInfo](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[PartyId] [uniqueidentifier] NOT NULL,
	[PartyType] [nchar](1) NOT NULL,
	[Sequence] [smallint] NOT NULL,
	[Type] [nchar](1) NOT NULL,
	[CommunicationType] [nchar](1) NOT NULL,
	[Detail] [nvarchar](80) NOT NULL
) ON [PRIMARY]
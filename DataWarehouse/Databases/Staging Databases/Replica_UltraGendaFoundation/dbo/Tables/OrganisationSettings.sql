﻿CREATE TABLE [dbo].[OrganisationSettings](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[IsPracticeInUse] [bit] NOT NULL,
	[IsPrefixInUse] [bit] NOT NULL,
	[IsStreet2InUse] [bit] NOT NULL,
	[IsStateInUse] [bit] NOT NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
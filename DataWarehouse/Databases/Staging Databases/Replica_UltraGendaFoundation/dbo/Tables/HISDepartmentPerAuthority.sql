﻿CREATE TABLE [dbo].[HISDepartmentPerAuthority](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[AuthorityId] [uniqueidentifier] NOT NULL,
	[HISDepartmentId] [uniqueidentifier] NOT NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
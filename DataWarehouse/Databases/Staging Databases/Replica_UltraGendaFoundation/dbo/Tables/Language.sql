﻿CREATE TABLE [dbo].[Language](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[Abbreviation] [nvarchar](20) NOT NULL,
	[Code] [nvarchar](40) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[ISOCode] [nchar](2) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[SequenceCorrespondence] [smallint] NULL,
	[SequencePatient] [smallint] NULL,
	[SequenceUser] [smallint] NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PracticeLocation](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[OrganisationalUnitId] [uniqueidentifier] NOT NULL,
	[PracticeId] [uniqueidentifier] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
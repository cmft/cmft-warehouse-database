﻿CREATE TABLE [dbo].[StaffMember](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[PersonId] [uniqueidentifier] NOT NULL,
	[Abbreviation] [nvarchar](20) NOT NULL,
	[Code] [nvarchar](40) NOT NULL,
	[ActiveDate] [datetime] NOT NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
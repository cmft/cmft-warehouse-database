﻿CREATE TABLE [dbo].[CareOrganisation](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[CareTypeId] [uniqueidentifier] NOT NULL,
	[OrganisationalUnitId] [uniqueidentifier] NOT NULL,
	[Abbreviation] [nvarchar](20) NOT NULL,
	[Code] [nvarchar](40) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[NationalCode] [nvarchar](20) NULL,
	[ActiveDate] [datetime] NOT NULL
) ON [PRIMARY]
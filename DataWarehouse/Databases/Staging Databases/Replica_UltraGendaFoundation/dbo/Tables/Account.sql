﻿CREATE TABLE [dbo].[Account](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[LoginName] [nvarchar](40) NOT NULL,
	[PasswordHash] [varbinary](50) NULL,
	[PersonId] [uniqueidentifier] NOT NULL,
	[PeriodOfValidity_Begin] [datetime] NULL,
	[PeriodOfValidity_End] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]
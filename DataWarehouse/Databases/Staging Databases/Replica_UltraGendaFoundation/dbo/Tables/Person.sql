﻿CREATE TABLE [dbo].[Person](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[Title] [nvarchar](40) NULL,
	[FamilyName] [nvarchar](80) NOT NULL,
	[FamilyNamePrefix] [nvarchar](20) NULL,
	[SoundexFamilyName] [nvarchar](80) NULL,
	[MaidenName] [nvarchar](80) NULL,
	[MaidenNamePrefix] [nvarchar](20) NULL,
	[SoundexMaidenName] [nvarchar](80) NULL,
	[PartnerName] [nvarchar](80) NULL,
	[PartnerNamePrefix] [nvarchar](20) NULL,
	[GivenName] [nvarchar](80) NULL,
	[SoundexGivenName] [nvarchar](80) NULL,
	[PenName] [nvarchar](80) NULL,
	[MiddleInitial] [nvarchar](20) NULL,
	[Sex] [nchar](1) NOT NULL,
	[BirthPlace] [nvarchar](80) NULL,
	[BirthDate] [datetime] NULL,
	[DeathDate] [datetime] NULL,
	[NationalCode] [nvarchar](40) NULL,
	[MaritalStatusId] [uniqueidentifier] NULL,
	[EthnicOriginId] [uniqueidentifier] NULL,
	[ReligionId] [uniqueidentifier] NULL,
	[SpokenLanguageId] [uniqueidentifier] NULL,
	[CorrespondenceLanguageId] [uniqueidentifier] NULL,
	[UserLanguageId] [uniqueidentifier] NULL,
	[NationalityId] [uniqueidentifier] NULL,
	[ParentGuardianName] [nvarchar](80) NULL
) ON [PRIMARY]
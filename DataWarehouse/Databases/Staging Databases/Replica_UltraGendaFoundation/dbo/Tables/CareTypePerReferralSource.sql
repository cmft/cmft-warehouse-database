﻿CREATE TABLE [dbo].[CareTypePerReferralSource](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[ReferralSourceId] [uniqueidentifier] NOT NULL,
	[CareTypeId] [uniqueidentifier] NOT NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PracticePerCareProvider](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[CareProviderId] [uniqueidentifier] NOT NULL,
	[PracticeId] [uniqueidentifier] NOT NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
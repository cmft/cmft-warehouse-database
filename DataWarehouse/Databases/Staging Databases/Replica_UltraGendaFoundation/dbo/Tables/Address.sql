﻿CREATE TABLE [dbo].[Address](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[PartyId] [uniqueidentifier] NOT NULL,
	[PartyType] [nchar](1) NOT NULL,
	[Sequence] [smallint] NOT NULL,
	[Street1] [nvarchar](80) NULL,
	[Street2] [nvarchar](80) NULL,
	[Postcode] [nvarchar](20) NULL,
	[City] [nvarchar](80) NULL,
	[State] [nvarchar](80) NULL,
	[CountryText] [nvarchar](80) NULL,
	[CountryCode] [nvarchar](20) NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]
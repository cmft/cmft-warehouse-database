﻿CREATE TABLE [dbo].[StaffRolePerIStaffMember](
	[Id] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[LastModifiedOn] [datetime] NOT NULL,
	[LastModifiedBy] [uniqueidentifier] NULL,
	[AuthorityId] [uniqueidentifier] NULL,
	[StaffMemberId] [uniqueidentifier] NULL,
	[StaffRoleId] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NULL,
	[PeriodOfValidity_Begin] [datetime] NULL,
	[PeriodOfValidity_End] [datetime] NULL,
	[TimestampPro] [binary](8) NULL
) ON [PRIMARY]
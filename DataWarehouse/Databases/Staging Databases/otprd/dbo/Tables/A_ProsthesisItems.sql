﻿CREATE TABLE [dbo].[A_ProsthesisItems](
	[PIT_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[PIT_SEQU] [numeric](9, 0) NOT NULL,
	[PIT_OP_SEQU] [numeric](9, 0) NOT NULL,
	[PIT_II_SEQU] [numeric](9, 0) NOT NULL,
	[PIT_QTY] [numeric](9, 0) NOT NULL,
	[PIT_BATCH_NUM] [varchar](50) NULL,
	[PIT_LOT_NUM] [varchar](50) NULL,
	[PIT_LOG_DATE] [datetime] NULL,
	[PIT_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PIT_SUPPLIER] [varchar](100) NULL,
	[PIT_TIN_SEQU] [numeric](9, 0) NOT NULL,
	[PIT_TYPE] [numeric](9, 0) NOT NULL,
	[PIT_OI_SEQU] [numeric](9, 0) NOT NULL,
	[PIT_BS_SEQU] [numeric](9, 0) NOT NULL,
	[PIT_OPENED_NOT_USED] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
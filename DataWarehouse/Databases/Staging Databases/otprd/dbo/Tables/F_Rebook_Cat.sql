﻿CREATE TABLE [dbo].[F_Rebook_Cat](
	[RBC_SEQU] [numeric](9, 0) NOT NULL,
	[RBC_CODE] [varchar](10) NULL,
	[RBC_DESCRIPTION] [varchar](255) NULL,
	[RBC_INACTIVE] [numeric](3, 0) NOT NULL,
	[RBC_CA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
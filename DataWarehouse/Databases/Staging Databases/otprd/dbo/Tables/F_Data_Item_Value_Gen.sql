﻿CREATE TABLE [dbo].[F_Data_Item_Value_Gen](
	[DIVG_SEQU] [numeric](9, 0) NOT NULL,
	[DIVG_DIV_SEQU] [numeric](9, 0) NOT NULL,
	[DIVG_TYPE] [varchar](30) NULL,
	[DIVG_SUBTYPE] [varchar](30) NULL,
	[DIVG_GLI_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Data_Item_Header](
	[DIH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIH_CODE] [varchar](10) NULL,
	[DIH_NAME] [varchar](30) NULL,
	[DIH_DESCRIPTION] [varchar](255) NULL,
	[DIH_EPS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIH_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[DIH_RULES] [text] NULL,
	[DIH_SIGNED_BY_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIH_SIGNED_BY_TIME] [datetime] NULL,
	[DIH_DID_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIH_FCR_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Data_It__DIH_F__352997DB]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Alert_Details](
	[AD_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AD_DATE] [datetime] NULL,
	[AD_DESCRIPTION] [text] NULL,
	[AD_UNUSED_1] [varchar](10) NULL,
	[AD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AD_LOG_DATE] [datetime] NULL,
	[AD_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[AD_PI_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AD_ST_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AD_AL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AD_EPS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AD_EXTERNAL_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[AD_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_NPfIT_Mapping](
	[NPM_SEQU] [numeric](9, 0) NOT NULL,
	[NPM_WORKGROUP_ID] [varchar](100) NULL,
	[NPM_OBJECT_SEQU] [numeric](9, 0) NOT NULL,
	[NPM_OBJECT_TYPE] [varchar](20) NULL,
	[NPM_PRIMARY] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Equip_Conflict](
	[EQC_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_CONFLICT1_TYPE] [numeric](3, 0) NOT NULL,
	[EQC_CONFLICT1_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_CONFLICT2_TYPE] [numeric](3, 0) NOT NULL,
	[EQC_CONFLICT2_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_REASON_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_STAFF_SEQU] [numeric](9, 0) NOT NULL,
	[EQC_REASON_TEXT] [varchar](255) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
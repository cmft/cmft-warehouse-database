﻿CREATE TABLE [dbo].[A_Multiple_Cons_Template](
	[MCT_SEQU] [numeric](9, 0) NOT NULL,
	[MCT_IT_SEQU] [numeric](9, 0) NOT NULL,
	[MCT_DAY_NO] [numeric](3, 0) NOT NULL,
	[MCT_SESSION] [numeric](3, 0) NOT NULL,
	[MCT_CON_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
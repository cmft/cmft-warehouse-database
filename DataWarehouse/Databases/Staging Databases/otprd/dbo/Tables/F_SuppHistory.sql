﻿CREATE TABLE [dbo].[F_SuppHistory](
	[SIH_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SIH_SEQU] [numeric](9, 0) NOT NULL,
	[SIH_SUP_SEQU] [numeric](9, 0) NOT NULL,
	[SIH_II_SEQU] [numeric](9, 0) NOT NULL,
	[SIH_LOG_DATE] [datetime] NULL
) ON [PRIMARY]
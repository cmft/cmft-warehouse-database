﻿CREATE TABLE [dbo].[A_Agents](
	[AG_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[AG_SEQU] [numeric](9, 0) NOT NULL,
	[AG_CODE] [varchar](5) NULL,
	[AG_DESC] [varchar](60) NULL,
	[AG_INACTIVE] [numeric](3, 0) NOT NULL,
	[AG_LOG_DATE] [datetime] NULL,
	[AG_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[AG_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_EquipProced](
	[EP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[EP_CD_SEQU] [numeric](9, 0) NOT NULL,
	[EP_ITY_SEQU] [numeric](9, 0) NOT NULL,
	[EP_LOG_DATE] [datetime] NULL,
	[EP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[EP_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Recovery_Delay](
	[RD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RD_OP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RD_DE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RD_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RD_FREE_TEXT] [varchar](400) NULL
) ON [PRIMARY]
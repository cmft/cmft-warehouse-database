﻿CREATE TABLE [dbo].[F_Unplanned](
	[UP_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UP_CODE] [varchar](5) NULL,
	[UP_DESC] [varchar](60) NULL,
	[UP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UP_LOG_DATE] [datetime] NULL,
	[UP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[UP_NOT_SPECIFIED] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UP_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UP_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Unplann__UP_CA__38AF44A5]  DEFAULT ((0)),
	[UP_START_DATE] [datetime] NULL,
	[UP_END_DATE] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Theatre_Type](
	[THT_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[THT_CODE] [varchar](10) NULL,
	[THT_DESC] [varchar](60) NULL,
	[THT_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT ((0)),
	[THT_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0))
) ON [PRIMARY]
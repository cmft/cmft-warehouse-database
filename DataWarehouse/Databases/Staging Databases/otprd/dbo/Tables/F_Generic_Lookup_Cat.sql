﻿CREATE TABLE [dbo].[F_Generic_Lookup_Cat](
	[GLC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[GLC_CODE] [varchar](10) NULL,
	[GLC_DESCRIPTION] [varchar](255) NULL,
	[GLC_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[GLC_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Generic__GLC_C__54575F1A]  DEFAULT ((0))
) ON [PRIMARY]
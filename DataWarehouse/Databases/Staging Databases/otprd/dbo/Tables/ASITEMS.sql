﻿CREATE TABLE [dbo].[ASITEMS](
	[SI_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SI_SUCODE] [varchar](5) NULL,
	[SI_MINUTE] [numeric](9, 0) NOT NULL,
	[SI_PRSEQ] [numeric](9, 0) NOT NULL,
	[SI_UNIQ] [varchar](13) NOT NULL,
	[SI_NUM] [numeric](9, 0) NOT NULL,
	[SI_LOG_DATE] [datetime] NULL,
	[SI_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SI_CD_SEQU] [numeric](9, 0) NOT NULL,
	[SI_SU_SEQU] [numeric](9, 0) NOT NULL,
	[SI_COMMON] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Licence_Users_Login](
	[LIUL_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIUL_LIU_SEQU] [varchar](25) NULL,
	[LIUL_LIP_SEQU] [varchar](25) NULL,
	[LIUL_SPID] [numeric](18, 0) NULL,
	[LIUL_DOMUSER] [varchar](50) NULL,
	[LIUL_IP] [varchar](15) NULL,
	[LIUL_CA_SEQU] [varchar](25) NULL,
	[LIUL_CAMPUS_CODE] [varchar](255) NULL,
	[LIUL_CAMPUS_NAME] [varchar](255) NULL,
	[LIUL_DATE_TIME_START] [datetime] NULL,
	[LIUL_DATE_TIME_EXIT] [datetime] NULL,
	[LIUL_SITE_CODE] [varchar](25) NOT NULL,
	[LIUL_BREACH] [numeric](3, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Inv_Extras](
	[IEX_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[IEX_SEQU] [numeric](9, 0) NOT NULL,
	[IEX_II_SEQU] [numeric](9, 0) NOT NULL,
	[IEX_PA_SEQU] [numeric](9, 0) NOT NULL,
	[IEX_QTY] [numeric](9, 0) NOT NULL,
	[IEX_LOG_DATE] [datetime] NULL,
	[IEX_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[IEX_ACTION] [varchar](1) NULL,
	[IEX_CD_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
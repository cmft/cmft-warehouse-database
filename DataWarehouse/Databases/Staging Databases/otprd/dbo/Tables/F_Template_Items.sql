﻿CREATE TABLE [dbo].[F_Template_Items](
	[TIT_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TIT_CD_SEQU] [numeric](9, 0) NOT NULL,
	[TIT_CD_CD_SEQU] [numeric](9, 0) NOT NULL,
	[TIT_PRIMARY] [numeric](3, 0) NOT NULL,
	[TIT_SEQU] [numeric](9, 0) NOT NULL,
	[TIT_ORDER] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
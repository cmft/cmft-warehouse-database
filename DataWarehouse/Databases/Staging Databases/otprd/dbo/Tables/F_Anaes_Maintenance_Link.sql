﻿CREATE TABLE [dbo].[F_Anaes_Maintenance_Link](
	[MANL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MANL_PAD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MANL_MAN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_User_Campus_Access](
	[UCA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UCA_US_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UCA_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UCA_UNIQUE] [varchar](20) NOT NULL
) ON [PRIMARY]
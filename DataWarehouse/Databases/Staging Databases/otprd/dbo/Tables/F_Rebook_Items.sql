﻿CREATE TABLE [dbo].[F_Rebook_Items](
	[RBI_SEQU] [numeric](9, 0) NOT NULL,
	[RBI_CODE] [varchar](10) NULL,
	[RBI_DESCRIPTION] [varchar](255) NULL,
	[RBI_EXT_DB] [varchar](10) NULL,
	[RBI_EXTRACT] [varchar](10) NULL,
	[RBI_INACTIVE] [numeric](3, 0) NOT NULL,
	[RBI_RBC_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
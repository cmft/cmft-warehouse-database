﻿CREATE TABLE [dbo].[A_InvKey](
	[IK_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[IK_KEY_WORD] [varchar](30) NULL,
	[IK_II_SEQU] [numeric](9, 0) NOT NULL,
	[IK_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
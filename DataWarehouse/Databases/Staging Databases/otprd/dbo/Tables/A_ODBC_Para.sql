﻿CREATE TABLE [dbo].[A_ODBC_Para](
	[ODBC_DATE_1] [datetime] NULL,
	[ODBC_DATE_2] [datetime] NULL,
	[ODBC_US_NAME] [varchar](100) NULL,
	[ODBC_US_PASS] [varchar](100) NULL,
	[ODBC_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ODBC_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
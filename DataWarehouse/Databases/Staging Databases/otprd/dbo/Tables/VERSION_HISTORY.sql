﻿CREATE TABLE [dbo].[VERSION_HISTORY](
	[VERSN_REFNO] [int] IDENTITY(1,1) NOT NULL,
	[Number] [varchar](20) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[Comments] [text] NULL,
	[Apply_dttm] [datetime] NOT NULL DEFAULT (getdate()),
	[User_Apply] [varchar](50) NOT NULL DEFAULT (suser_sname())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
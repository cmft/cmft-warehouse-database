﻿CREATE TABLE [dbo].[WCHCN_Unlinked_Data](
	[WCHCN_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[WCHCN_MRN] [varchar](12) NULL,
	[WCHCN_ADMISSION_DATE] [datetime] NULL,
	[WCHCN_ADMISSION_TIME] [datetime] NULL,
	[WCHCN_DISCHARGE_DATE] [datetime] NULL,
	[WCHCN_DISCHARGE_TIME] [datetime] NULL,
	[WCHCN_EPISODE_NO] [varchar](20) NULL,
	[WCHCN_LOCATION] [varchar](10) NULL,
	[WCHCN_CREATE_DATE] [datetime] NULL,
	[WCHCN_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Previous_Ops](
	[POP_MRN] [varchar](10) NOT NULL,
	[POP_DOB] [datetime] NULL,
	[POP_OP_DATE] [datetime] NOT NULL,
	[POP_OP_DESC] [varchar](200) NULL,
	[POP_CONS] [varchar](60) NULL,
	[POP_SURG] [varchar](60) NOT NULL,
	[POP_ANAES] [varchar](60) NULL,
	[POP_TH_CODE] [varchar](8) NULL
) ON [PRIMARY]
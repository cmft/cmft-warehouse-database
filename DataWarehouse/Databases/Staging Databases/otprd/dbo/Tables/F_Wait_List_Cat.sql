﻿CREATE TABLE [dbo].[F_Wait_List_Cat](
	[WLC_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WLC_CODE] [varchar](5) NULL,
	[WLC_DESCRIPTION] [varchar](60) NULL,
	[WLC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[WLC_LOG_DATE] [datetime] NULL,
	[WLC_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[WLC_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[WLC_EXTRACT_CODE] [varchar](5) NULL,
	[WLC_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Wait_Li__WLC_C__46093FC3]  DEFAULT ((0))
) ON [PRIMARY]
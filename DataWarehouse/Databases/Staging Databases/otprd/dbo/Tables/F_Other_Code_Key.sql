﻿CREATE TABLE [dbo].[F_Other_Code_Key](
	[OCK_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[OCK_KEY_WORD] [varchar](30) NULL,
	[OCK_OTC_SEQU] [numeric](9, 0) NOT NULL,
	[OCK_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Vascular_Access](
	[VAS_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[VAS_SEQU] [numeric](9, 0) NOT NULL,
	[VAS_CODE] [varchar](5) NULL,
	[VAS_DESCRIPTION] [varchar](60) NULL,
	[VAS_INACTIVE] [numeric](3, 0) NOT NULL,
	[VAS_LOG_DATE] [datetime] NULL,
	[VAS_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[VAS_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Licence_Useage](
	[LIG_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIG_LIL_SEQU] [varchar](25) NULL,
	[LIG_LIP_SEQU] [varchar](25) NULL,
	[LIG_ACCU_MINS] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIG_LAST_UPDATE] [datetime] NULL,
	[LIG_CREATE_DATE] [datetime] NULL,
	[LIG_SITE_CODE] [varchar](25) NOT NULL
) ON [PRIMARY]
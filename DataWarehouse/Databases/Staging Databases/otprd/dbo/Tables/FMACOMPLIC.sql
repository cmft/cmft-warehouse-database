﻿CREATE TABLE [dbo].[FMACOMPLIC](
	[MA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MA_CODE] [varchar](5) NULL,
	[MA_DESC] [varchar](60) NULL,
	[MA_DEFINITION] [text] NULL,
	[MA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MA_LOG_DATE] [datetime] NULL,
	[MA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[MA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[MA_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__FMACOMPLI__MA_CA__4238AEDF]  DEFAULT ((0)),
	[MA_START_DATE] [datetime] NULL,
	[MA_END_DATE] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
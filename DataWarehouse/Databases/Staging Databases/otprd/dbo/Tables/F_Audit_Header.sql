﻿CREATE TABLE [dbo].[F_Audit_Header](
	[AUDH_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[AUDH_DATE_TIME] [datetime] NULL,
	[AUDH_MACHINE_ID] [varchar](255) NULL,
	[AUDH_USER_NAME] [varchar](255) NULL,
	[AUDH_ACTION] [varchar](255) NULL,
	[AUDH_OBJECT_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[AUDH_OBJECT_NAME] [varchar](255) NULL
) ON [PRIMARY]
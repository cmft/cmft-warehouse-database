﻿CREATE TABLE [dbo].[F_Surg_Times_Hist](
	[st_sequ] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[st_op_sequ] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[st_su_sequ] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[st_field_type] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[st_updated_on_date_time] [datetime] NULL,
	[ST_LAST_DATE_TIME] [datetime] NULL,
	[st_modified_date_time] [datetime] NULL
) ON [PRIMARY]
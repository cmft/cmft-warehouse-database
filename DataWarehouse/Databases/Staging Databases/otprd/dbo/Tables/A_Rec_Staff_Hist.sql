﻿CREATE TABLE [dbo].[A_Rec_Staff_Hist](
	[RSH_SEQU] [numeric](9, 0) NOT NULL,
	[RSH_REC_SEQU] [numeric](9, 0) NOT NULL,
	[RSH_PRIM_NURSE_SEQU] [numeric](9, 0) NOT NULL,
	[RSH_SEC_NURSE_SEQU] [numeric](9, 0) NOT NULL,
	[RSH_DATE_TIME] [datetime] NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
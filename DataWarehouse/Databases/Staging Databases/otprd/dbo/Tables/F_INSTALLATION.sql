﻿CREATE TABLE [dbo].[F_INSTALLATION](
	[INT_EDIS_VRSN] [varchar](15) NOT NULL,
	[INT_DTE_INSTALL] [datetime] NOT NULL,
	[INT_DESCRIPTION] [varchar](255) NULL
) ON [PRIMARY]
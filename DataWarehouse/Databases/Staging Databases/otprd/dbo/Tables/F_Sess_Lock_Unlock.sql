﻿CREATE TABLE [dbo].[F_Sess_Lock_Unlock](
	[SLU_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SLU_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SLU_SS_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SLU_REASON_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SLU_DATE_TIME] [datetime] NULL,
	[SLU_SS_COMMENTS] [text] NULL,
	[SLU_TYPE] [numeric](3, 0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
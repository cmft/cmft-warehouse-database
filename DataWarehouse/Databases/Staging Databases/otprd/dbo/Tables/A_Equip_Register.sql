﻿CREATE TABLE [dbo].[A_Equip_Register](
	[EQL_SEQU] [numeric](9, 0) NOT NULL,
	[EQL_II_SEQU] [numeric](9, 0) NOT NULL,
	[EQL_DATE_LOANED] [datetime] NULL,
	[EQL_EXPECTED_RETURN] [datetime] NULL,
	[EQL_ACTUAL_RETURN] [datetime] NULL,
	[EQL_LOAN_TYPE] [numeric](3, 0) NOT NULL,
	[EQL_LOAN_TO_SEQU] [numeric](9, 0) NOT NULL,
	[EQL_LOAN_TO_NAME] [varchar](100) NULL,
	[EQL_RETURN_FLAG] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
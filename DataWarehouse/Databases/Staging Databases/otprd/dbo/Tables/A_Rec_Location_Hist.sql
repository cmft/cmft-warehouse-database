﻿CREATE TABLE [dbo].[A_Rec_Location_Hist](
	[RLH_SEQU] [numeric](9, 0) NOT NULL,
	[RLH_RCL_SEQU] [numeric](9, 0) NOT NULL,
	[RLH_DATE_TIME_IN] [datetime] NULL,
	[RLH_DATE_TIME_OUT] [datetime] NULL,
	[RLH_REC_SEQU] [numeric](9, 0) NOT NULL,
	[RLH_UN_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
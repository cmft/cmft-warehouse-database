﻿CREATE TABLE [dbo].[A_Mand_Link](
	[ML_SEQU] [numeric](9, 0) NOT NULL,
	[ML_CA_SEQU] [numeric](9, 0) NOT NULL,
	[ML_FIELD_NAME] [varchar](200) NULL,
	[ML_ERROR_MSG] [varchar](200) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
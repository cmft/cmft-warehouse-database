﻿CREATE TABLE [dbo].[F_Cancel_Group](
	[CG_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CG_CODE] [varchar](5) NULL,
	[CG_DESC] [varchar](60) NULL,
	[CG_LOG_DATE] [datetime] NULL,
	[CG_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[CG_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CG_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CG_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Cancel___CG_CA__080C0D4A]  DEFAULT ((0)),
	[CG_START_DATE] [datetime] NULL,
	[CG_END_DATE] [datetime] NULL
) ON [PRIMARY]
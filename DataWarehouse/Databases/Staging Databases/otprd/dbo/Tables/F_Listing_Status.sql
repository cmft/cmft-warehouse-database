﻿CREATE TABLE [dbo].[F_Listing_Status](
	[LS_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LS_CODE] [varchar](6) NULL,
	[LS_DESCRIPTION] [varchar](60) NULL,
	[LS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LS_DEFAULT_INSERT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[LS_LOG_DATE] [datetime] NULL,
	[LS_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[LS_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[LS_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[LS_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Listing__LS_CA__1FE396DB]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Alias_Details](
	[ALS_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ALS_ALIAS_UR_NO] [varchar](20) NULL,
	[ALS_MAIN_UR_NO] [varchar](20) NULL,
	[ALS_SEQU] [numeric](9, 0) NOT NULL,
	[ALS_PIN_SEQU] [numeric](9, 0) NOT NULL,
	[ALS_CA_SEQU] [numeric](9, 0) NOT NULL,
	[ALS_TYPE] [varchar](10) NULL
) ON [PRIMARY]
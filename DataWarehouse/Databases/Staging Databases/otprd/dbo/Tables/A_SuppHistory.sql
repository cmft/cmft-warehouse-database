﻿CREATE TABLE [dbo].[A_SuppHistory](
	[SIH_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SIH_SEQU] [numeric](9, 0) NOT NULL,
	[SIH_SUP_SEQU] [numeric](9, 0) NOT NULL,
	[SIH_II_SEQU] [numeric](9, 0) NOT NULL,
	[SIH_LOG_DATE] [datetime] NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
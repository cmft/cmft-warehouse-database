﻿CREATE TABLE [dbo].[F_Formats](
	[FO_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[FO_NAME] [varchar](45) NULL,
	[FO_TYPE] [varchar](10) NULL,
	[FO_DESC] [varchar](200) NULL,
	[FO_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[FO_REP_PRINTER] [varchar](100) NULL,
	[FO_REP_DEST] [numeric](3, 0) NOT NULL DEFAULT (0),
	[FO_REP_PAGE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[FO_REP_MESSAGE] [varchar](100) NULL,
	[FO_REP_CONTROL] [numeric](3, 0) NOT NULL DEFAULT (0),
	[FO_REP_FOOTER_LEFT] [varchar](20) NULL,
	[FO_REP_FOOTER_RIGHT] [varchar](20) NULL,
	[FO_REPORT_NAME] [varchar](100) NULL
) ON [PRIMARY]
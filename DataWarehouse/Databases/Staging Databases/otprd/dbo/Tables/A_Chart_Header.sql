﻿CREATE TABLE [dbo].[A_Chart_Header](
	[CHT_SEQU] [numeric](9, 0) NOT NULL,
	[CHT_EPS_SEQU] [numeric](9, 0) NOT NULL,
	[CHT_EXTERNAL_ID] [varchar](30) NULL,
	[CHT_LAST_EWS_TIME] [datetime] NULL,
	[CHT_TYPE] [numeric](3, 0) NOT NULL,
	[CHT_LAST_EWS_VALUE] [numeric](9, 0) NOT NULL,
	[CHT_LAST_EWS_VERIFIED] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
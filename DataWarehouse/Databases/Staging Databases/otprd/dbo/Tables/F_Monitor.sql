﻿CREATE TABLE [dbo].[F_Monitor](
	[MON_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MON_CODE] [varchar](5) NULL,
	[MON_DESCRIPTION] [varchar](60) NULL,
	[MON_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MON_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[MON_LOG_DATE] [datetime] NULL,
	[MON_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[MON_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Monitor__MON_C__24A84BF8]  DEFAULT ((0))
) ON [PRIMARY]
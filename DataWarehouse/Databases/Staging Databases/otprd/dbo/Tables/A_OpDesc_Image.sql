﻿CREATE TABLE [dbo].[A_OpDesc_Image](
	[ODI_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ODI_SEQU] [numeric](9, 0) NOT NULL,
	[ODI_OP_SEQU] [numeric](9, 0) NOT NULL,
	[ODI_OIM_SEQU] [numeric](9, 0) NOT NULL,
	[ODI_LOG_DATE] [datetime] NULL,
	[ODI_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL,
	[ODI_EXT_NAME] [varchar](36) NULL
) ON [PRIMARY]
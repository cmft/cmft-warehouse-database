﻿CREATE TABLE [dbo].[F_Store_Forward](
	[SAF_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_PA_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_OP_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_REC_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_DATE] [datetime] NULL,
	[SAF_MESSAGE] [text] NULL,
	[SAF_EVENT] [varchar](30) NULL,
	[SAF_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_PATIENT_NAME] [varchar](140) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
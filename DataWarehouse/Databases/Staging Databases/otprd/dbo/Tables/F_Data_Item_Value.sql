﻿CREATE TABLE [dbo].[F_Data_Item_Value](
	[DIV_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIV_DIH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DIV_NAME] [varchar](100) NULL,
	[DIV_DESCRIPTION] [varchar](255) NULL,
	[DIV_TYPE] [varchar](30) NULL,
	[DIV_SUBTYPE] [varchar](30) NULL,
	[DIV_ORDER] [numeric](9, 0) NULL,
	[DIV_VALUE] [text] NULL,
	[DIV_DIV_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
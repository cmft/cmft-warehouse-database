﻿CREATE TABLE [dbo].[F_Audit_Item](
	[AUDI_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[AUDI_AUDH_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[AUDI_NAME] [varchar](255) NULL,
	[AUDI_VALUE] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Airway](
	[AIR_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AIR_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AIR_CODE] [varchar](5) NULL,
	[AIR_DESCRIPTION] [varchar](60) NULL,
	[AIR_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[AIR_LOG_DATE] [datetime] NULL,
	[AIR_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[AIR_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Airway__AIR_CA__7E82A310]  DEFAULT ((0))
) ON [PRIMARY]
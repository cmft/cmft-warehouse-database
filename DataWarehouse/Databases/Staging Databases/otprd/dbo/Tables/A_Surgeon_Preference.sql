﻿CREATE TABLE [dbo].[A_Surgeon_Preference](
	[SPR_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SPR_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_II_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_CD_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_PRI_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_SU_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_LOG_DATE] [datetime] NULL,
	[SPR_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SPR_QTY] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
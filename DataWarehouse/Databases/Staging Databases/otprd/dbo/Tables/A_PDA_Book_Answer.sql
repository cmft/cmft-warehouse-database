﻿CREATE TABLE [dbo].[A_PDA_Book_Answer](
	[PBA_SEQU] [numeric](9, 0) NOT NULL,
	[PBA_PBQ_SEQU] [numeric](9, 0) NOT NULL,
	[PBA_PA_SEQU] [numeric](9, 0) NOT NULL,
	[PBA_PDQ_SEQU] [numeric](9, 0) NOT NULL,
	[PBA_SET_NO] [numeric](3, 0) NOT NULL,
	[PBA_ANSWER_1] [varchar](255) NULL,
	[PBA_ANSWER_2] [varchar](255) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
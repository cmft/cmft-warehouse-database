﻿CREATE TABLE [dbo].[A_Message](
	[MSG_SEQU] [numeric](9, 0) NOT NULL,
	[MSG_MESSAGE] [varchar](1000) NULL,
	[MSG_RECEIPIENT_SEQU] [varchar](1000) NULL,
	[MSG_READ_BY] [varchar](1000) NULL,
	[MSG_TYPE] [numeric](3, 0) NOT NULL,
	[MSG_GEN_DATE_TIME] [datetime] NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Letter_History](
	[LETH_SEQU] [numeric](9, 0) NOT NULL,
	[LETH_EPS_SEQU] [numeric](9, 0) NOT NULL,
	[LETH_PRINTED_DATE_TIME] [datetime] NULL,
	[LETH_TXT_SEQU] [numeric](9, 0) NOT NULL,
	[LETH_IS_PRINTED] [numeric](3, 0) NOT NULL,
	[LETH_LET_SEQU] [numeric](9, 0) NOT NULL,
	[LETH_LET_NAME] [varchar](100) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Licence_Period](
	[LIP_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIP_START_DATE] [datetime] NULL,
	[LIP_END_DATE] [datetime] NULL,
	[LIP_EXTRACT_DATE] [datetime] NULL,
	[LIP_CREATE_DATE] [datetime] NULL,
	[LIP_SITE_CODE] [varchar](25) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Sess_Closure_Hist](
	[CH_SEQU] [numeric](9, 0) NOT NULL,
	[CH_SS_SEQU] [numeric](9, 0) NOT NULL,
	[CH_CT_SEQU] [numeric](9, 0) NOT NULL,
	[CH_STATUS] [varchar](50) NULL,
	[CH_DATE_TIME] [datetime] NULL,
	[CH_COMMENTS] [varchar](2000) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_RetRecTrans](
	[RRT_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[RRT_RET_SEQU] [numeric](9, 0) NOT NULL,
	[RRT_ROL_SEQU] [numeric](9, 0) NOT NULL,
	[RRT_SEQU] [numeric](9, 0) NOT NULL,
	[RRT_QTY] [numeric](9, 0) NOT NULL,
	[RRT_LOG_DATE] [datetime] NULL,
	[RRT_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
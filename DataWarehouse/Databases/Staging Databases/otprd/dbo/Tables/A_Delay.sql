﻿CREATE TABLE [dbo].[A_Delay](
	[DE_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[DE_CODE] [varchar](5) NULL,
	[DE_DESCRIPTION] [varchar](60) NULL,
	[DE_SEQU] [numeric](9, 0) NOT NULL,
	[DE_LOG_DATE] [datetime] NULL,
	[DE_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[DE_INACTIVE] [numeric](3, 0) NOT NULL,
	[DE_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL,
	[DE_FREE_TEXT] [numeric](3, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Licence_Users](
	[LIU_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIU_LIL_SEQU] [varchar](25) NULL,
	[LIU_LIP_SEQU] [varchar](25) NULL,
	[LIU_USER_ID] [varchar](25) NULL,
	[LIU_USER_NAME] [varchar](50) NULL,
	[LIU_LAST_LOGON] [datetime] NULL,
	[LIU_CREATE_DATE] [datetime] NULL,
	[LIU_SITE_CODE] [varchar](25) NOT NULL
) ON [PRIMARY]
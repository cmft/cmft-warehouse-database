﻿CREATE TABLE [dbo].[F_Procedure_Key](
	[PK_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PK_KEY_WORD] [varchar](30) NULL,
	[PK_CD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PK_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Audit_Logon_Off](
	[AUD_SPID] [numeric](18, 0) NOT NULL DEFAULT ((0)),
	[AUD_SERIAL] [varchar](20) NOT NULL,
	[AUD_IP] [varchar](15) NULL,
	[AUD_MACHINE] [varchar](50) NULL,
	[AUD_DOMUSER] [varchar](50) NULL,
	[AUD_APP_US_NAME] [varchar](20) NOT NULL,
	[AUD_APP_US_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[AUD_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[AUD_DATE_TIME_START] [datetime] NOT NULL,
	[AUD_DATE_TIME_EXIT] [datetime] NULL,
	[AUD_SYS_CRASHED] [numeric](3, 0) NOT NULL CONSTRAINT [DF__F_Audit_L__AUD_S__2E279491]  DEFAULT ((0))
) ON [PRIMARY]
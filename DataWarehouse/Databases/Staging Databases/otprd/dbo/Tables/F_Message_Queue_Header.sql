﻿CREATE TABLE [dbo].[F_Message_Queue_Header](
	[MQH_NAME] [varchar](64) NOT NULL,
	[MQH_STATUS] [numeric](3, 0) NOT NULL,
	[MQH_STATUS_MESSAGE] [varchar](256) NULL
) ON [PRIMARY]
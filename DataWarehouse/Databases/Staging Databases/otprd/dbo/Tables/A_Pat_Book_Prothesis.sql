﻿CREATE TABLE [dbo].[A_Pat_Book_Prothesis](
	[PBP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[PBP_DATE] [datetime] NULL,
	[PBP_QTY] [numeric](9, 0) NOT NULL,
	[PBP_II_SEQU] [numeric](9, 0) NOT NULL,
	[PBP_PA_SEQU] [numeric](9, 0) NOT NULL,
	[PBP_INV_DESCRIPTION] [varchar](100) NULL,
	[PBP_LOCATION] [varchar](25) NULL,
	[PBP_SU_SEQU] [numeric](9, 0) NOT NULL,
	[PBP_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
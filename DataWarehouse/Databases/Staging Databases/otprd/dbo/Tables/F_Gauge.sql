﻿CREATE TABLE [dbo].[F_Gauge](
	[GA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[GA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[GA_CODE] [varchar](5) NULL,
	[GA_DESCRIPTION] [varchar](60) NULL,
	[GA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[GA_LOG_DATE] [datetime] NULL,
	[GA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[GA_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Gauge__GA_CA_S__137DBFF6]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Sess_Closure_Hist](
	[CH_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[CH_SS_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[CH_CT_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[CH_STATUS] [varchar](50) NULL,
	[CH_DATE_TIME] [datetime] NULL,
	[CH_COMMENTS] [varchar](2000) NULL
) ON [PRIMARY]
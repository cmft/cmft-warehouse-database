﻿CREATE TABLE [dbo].[F_Delay_Status](
	[DES_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DES_CODE] [varchar](3) NULL,
	[DES_DESC] [varchar](60) NULL,
	[DES_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[DES_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[DES_LOG_DATE] [datetime] NULL,
	[DES_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[DES_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[DES_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[DES_DELAY_COUNTER] [numeric](3, 0) NOT NULL DEFAULT (0),
	[DES_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Delay_S__DES_C__0CD0C267]  DEFAULT ((0)),
	[DES_START_DATE] [datetime] NULL,
	[DES_END_DATE] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Book_Audit_Hist](
	[BAH_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[BAH_PA_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[BAH_DATE_TIME] [datetime] NULL,
	[BAH_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[BAH_ACTION] [char](1) NULL,
	[BAH_PREV_PROCEDURE] [varchar](700) NULL,
	[STAFF_USER_TYPE] [numeric](3, 0) NULL
) ON [PRIMARY]
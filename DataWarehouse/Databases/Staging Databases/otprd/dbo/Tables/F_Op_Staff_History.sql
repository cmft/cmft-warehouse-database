﻿CREATE TABLE [dbo].[F_Op_Staff_History](
	[OSH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[OSH_ROLE_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[OSH_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[OSH_TIME_OUT] [datetime] NULL,
	[OSH_OI_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[OSH_TIME_IN] [datetime] NULL
) ON [PRIMARY]
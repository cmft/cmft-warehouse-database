﻿CREATE TABLE [dbo].[A_Alias_Details](
	[ALS_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ALS_ALIAS_UR_NO] [varchar](20) NULL,
	[ALS_MAIN_UR_NO] [varchar](20) NULL,
	[ALS_SEQU] [numeric](9, 0) NOT NULL,
	[ALS_PIN_SEQU] [numeric](9, 0) NOT NULL,
	[ALS_CA_SEQU] [numeric](9, 0) NOT NULL,
	[ALS_TYPE] [varchar](10) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
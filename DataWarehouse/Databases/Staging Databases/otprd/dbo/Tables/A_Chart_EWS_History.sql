﻿CREATE TABLE [dbo].[A_Chart_EWS_History](
	[CEH_SEQU] [numeric](9, 0) NOT NULL,
	[CEH_CHT_SEQU] [numeric](9, 0) NOT NULL,
	[CEH_EWS_TIME] [datetime] NULL,
	[CEH_EWS_VALUE] [numeric](9, 0) NOT NULL,
	[CEH_ACTION_TIME] [datetime] NULL,
	[CEH_ACTION_STAFF] [numeric](9, 0) NOT NULL,
	[CEH_NOTIFY_STAFF] [numeric](9, 0) NOT NULL,
	[CEH_NONOTIFY_REASON] [varchar](1000) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
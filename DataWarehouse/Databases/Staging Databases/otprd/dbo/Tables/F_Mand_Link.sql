﻿CREATE TABLE [dbo].[F_Mand_Link](
	[ML_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ML_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ML_FIELD_NAME] [varchar](200) NULL,
	[ML_ERROR_MSG] [varchar](200) NULL
) ON [PRIMARY]
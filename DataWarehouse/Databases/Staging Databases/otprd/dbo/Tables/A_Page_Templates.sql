﻿CREATE TABLE [dbo].[A_Page_Templates](
	[PT_SEQU] [numeric](9, 0) NOT NULL,
	[PT_FORMAT_NAME] [varchar](60) NULL,
	[PT_FORMAT_DESC] [varchar](60) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
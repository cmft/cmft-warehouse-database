﻿CREATE TABLE [dbo].[F_Generic_Lookup_Items](
	[GLI_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[GLI_CODE] [varchar](10) NULL,
	[GLI_DESCRIPTION] [varchar](255) NULL,
	[GLI_EXT_DB] [varchar](10) NULL,
	[GLI_EXTRACT] [varchar](10) NULL,
	[GLI_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[GLI_GLC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[GLI_SORT_ORDER] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
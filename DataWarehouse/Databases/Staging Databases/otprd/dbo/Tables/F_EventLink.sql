﻿CREATE TABLE [dbo].[F_EventLink](
	[EVL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[EVL_DESCRIPTION] [varchar](30) NULL,
	[EVL_DETAILS] [text] NULL,
	[EVL_OP_SEQU] [numeric](9, 0) NOT NULL,
	[EVL_LOG_DATE] [datetime] NULL,
	[EVL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[EVL_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
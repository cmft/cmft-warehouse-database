﻿CREATE TABLE [dbo].[F_Clinical_Urgency](
	[CU_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CU_CODE] [varchar](3) NULL,
	[CU_DESCRIPTION] [varchar](60) NULL,
	[CU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CU_DEFAULTONINSERT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CU_MAX_DAY] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CU_LOG_DATE] [datetime] NULL,
	[CU_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[CU_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CU_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CU_EXTRACT_CODE] [varchar](5) NULL,
	[CU_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Clinica__CU_CA__4420F751]  DEFAULT ((0))
) ON [PRIMARY]
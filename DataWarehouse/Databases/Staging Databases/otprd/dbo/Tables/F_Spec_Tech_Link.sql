﻿CREATE TABLE [dbo].[F_Spec_Tech_Link](
	[SPTL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SPTL_PAD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SPTL_SPT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
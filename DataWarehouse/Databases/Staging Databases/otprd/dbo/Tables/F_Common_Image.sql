﻿CREATE TABLE [dbo].[F_Common_Image](
	[CIM_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CIM_OIM_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CIM_S1_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CIM_UNIQ_ID] [varchar](15) NOT NULL
) ON [PRIMARY]
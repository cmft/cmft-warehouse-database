﻿CREATE TABLE [dbo].[F_Pre_Op_Assess_Link](
	[POAL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[POAL_PAD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[POAL_POA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Rec_Disch_Status](
	[RDS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RDS_CODE] [varchar](20) NULL,
	[RDS_DESCRIPTION] [varchar](60) NULL,
	[RDS_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RDS_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RDS_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Rec_Dis__RDS_C__33EA8F88]  DEFAULT ((0))
) ON [PRIMARY]
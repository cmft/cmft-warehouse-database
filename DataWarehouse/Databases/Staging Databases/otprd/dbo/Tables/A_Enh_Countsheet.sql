﻿CREATE TABLE [dbo].[A_Enh_Countsheet](
	[ECS_SEQU] [numeric](9, 0) NULL,
	[ECS_PRE_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_COUNT1_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_COUNT2_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_COUNT3_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_FIRST_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_SECOND_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_FINAL_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_OP_SEQU] [numeric](9, 0) NOT NULL,
	[ECS_PRE_REASON] [varchar](500) NULL,
	[ECS_COUNT1_REASON] [varchar](500) NULL,
	[ECS_COUNT2_REASON] [varchar](500) NULL,
	[ECS_COUNT3_REASON] [varchar](500) NULL,
	[ECS_FIRST_REASON] [varchar](500) NULL,
	[ECS_SECOND_REASON] [varchar](500) NULL,
	[ECS_FINAL_REASON] [varchar](500) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
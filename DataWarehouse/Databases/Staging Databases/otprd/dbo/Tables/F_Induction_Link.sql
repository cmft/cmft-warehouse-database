﻿CREATE TABLE [dbo].[F_Induction_Link](
	[INDL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[INDL_PAD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[INDL_IND_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_RecDrugLink](
	[RDL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[RDL_CODE] [varchar](5) NULL,
	[RDL_DESCRIPTION] [varchar](40) NULL,
	[RDL_OP_SEQU] [numeric](9, 0) NOT NULL,
	[RDL_LOG_DATE] [datetime] NULL,
	[RDL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[RDL_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
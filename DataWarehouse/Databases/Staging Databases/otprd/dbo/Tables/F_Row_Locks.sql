﻿CREATE TABLE [dbo].[F_Row_Locks](
	[RLK_IDX] [varchar](50) NOT NULL,
	[RLK_US_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RLK_PROCESS] [varchar](50) NULL,
	[RLK_LOCK_DATE_TIME] [datetime] NULL,
	[RLK_WORKSTATION] [varchar](50) NULL,
	[RLK_CALLED_BY] [varchar](100) NULL,
	[RLK_USER_CT_ID] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RLK_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RLK_DB_SID] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RLK_DB_SERIAL] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RLK_WIND_WORKSTATION] [varchar](50) NULL,
	[RLK_LOGIN_TIME] [datetime] NULL,
	[RLK_NET_ADDRESS] [varchar](20) NULL
) ON [PRIMARY]
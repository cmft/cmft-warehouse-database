﻿CREATE TABLE [dbo].[A_Chart_Item_Header](
	[CIH_SEQU] [numeric](9, 0) NOT NULL,
	[CIH_CHT_SEQU] [numeric](9, 0) NOT NULL,
	[CIH_ITEM_ID] [numeric](9, 0) NOT NULL,
	[CIH_NAME] [varchar](50) NULL,
	[CIH_TYPE] [varchar](30) NULL,
	[CIH_SUBTYPE] [varchar](30) NULL,
	[CIH_GRAPH] [numeric](3, 0) NOT NULL,
	[CIH_LIMIT] [varchar](1000) NULL,
	[CIH_ORDER] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
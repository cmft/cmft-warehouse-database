﻿CREATE TABLE [dbo].[F_Order](
	[OR_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[OR_DATE] [datetime] NULL,
	[OR_ORDER_NUM] [numeric](9, 0) NOT NULL,
	[OR_AUTHORISATION] [varchar](100) NULL,
	[OR_COMMENT] [varchar](200) NULL,
	[OR_SUP_SEQU] [numeric](9, 0) NOT NULL,
	[OR_SEQU] [numeric](9, 0) NOT NULL,
	[OR_SD_SEQU] [numeric](9, 0) NOT NULL,
	[OR_COMPLETED] [numeric](3, 0) NOT NULL,
	[OR_LOG_DATE] [datetime] NULL,
	[OR_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
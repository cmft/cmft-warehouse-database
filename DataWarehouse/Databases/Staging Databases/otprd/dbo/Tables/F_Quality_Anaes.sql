﻿CREATE TABLE [dbo].[F_Quality_Anaes](
	[QLY_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[QLY_SEQU] [numeric](9, 0) NOT NULL,
	[QLY_CODE] [varchar](5) NULL,
	[QLY_DESCRIPTION] [varchar](60) NULL,
	[QLY_INACTIVE] [numeric](3, 0) NOT NULL,
	[QLY_LOG_DATE] [datetime] NULL,
	[QLY_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[QLY_CA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
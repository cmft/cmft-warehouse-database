﻿CREATE TABLE [dbo].[F_Chart_EWS_History](
	[CEH_SEQU] [numeric](9, 0) NOT NULL,
	[CEH_CHT_SEQU] [numeric](9, 0) NOT NULL,
	[CEH_EWS_TIME] [datetime] NULL,
	[CEH_EWS_VALUE] [numeric](9, 0) NOT NULL,
	[CEH_ACTION_TIME] [datetime] NULL,
	[CEH_ACTION_STAFF] [numeric](9, 0) NOT NULL,
	[CEH_NOTIFY_STAFF] [numeric](9, 0) NOT NULL,
	[CEH_NONOTIFY_REASON] [varchar](1000) NULL
) ON [PRIMARY]
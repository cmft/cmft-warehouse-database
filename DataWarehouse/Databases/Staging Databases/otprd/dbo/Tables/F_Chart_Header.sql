﻿CREATE TABLE [dbo].[F_Chart_Header](
	[CHT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CHT_EPS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CHT_EXTERNAL_ID] [varchar](30) NULL,
	[CHT_LAST_EWS_TIME] [datetime] NULL,
	[CHT_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CHT_LAST_EWS_VALUE] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CHT_LAST_EWS_VERIFIED] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
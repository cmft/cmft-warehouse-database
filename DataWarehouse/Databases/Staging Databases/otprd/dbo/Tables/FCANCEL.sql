﻿CREATE TABLE [dbo].[FCANCEL](
	[CN_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CN_CODE] [varchar](6) NULL,
	[CN_DESC] [varchar](60) NULL,
	[CN_LOG_DATE] [datetime] NULL,
	[CN_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[CN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CN_CG_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CN_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CN_DES_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CN_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__FCANCEL__CN_CA_S__0717E911]  DEFAULT ((0)),
	[CN_START_DATE] [datetime] NULL,
	[CN_END_DATE] [datetime] NULL
) ON [PRIMARY]
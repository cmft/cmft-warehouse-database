﻿CREATE TABLE [dbo].[F_Enh_Auditprint](
	[FEA_SEQU] [numeric](9, 0) NOT NULL,
	[FEA_SU_SEQU] [numeric](9, 0) NOT NULL,
	[FEA_US_SEQU] [numeric](9, 0) NOT NULL,
	[FEA_PRINT_TIME] [datetime] NOT NULL,
	[FEA_FCR_SEQU] [numeric](9, 0) NULL,
	[FEA_OP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
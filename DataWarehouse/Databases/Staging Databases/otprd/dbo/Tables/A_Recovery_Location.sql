﻿CREATE TABLE [dbo].[A_Recovery_Location](
	[RCL_CODE] [varchar](5) NULL,
	[RCL_DESCRIPTION] [varchar](60) NULL,
	[RCL_SEQU] [numeric](9, 0) NOT NULL,
	[RCL_INACTIVE] [numeric](3, 0) NOT NULL,
	[RCL_UN_SEQU] [numeric](9, 0) NOT NULL,
	[RCL_REC_SEQU] [numeric](9, 0) NOT NULL,
	[RCL_CHART_LOCATION] [varchar](30) NULL,
	[RCL_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
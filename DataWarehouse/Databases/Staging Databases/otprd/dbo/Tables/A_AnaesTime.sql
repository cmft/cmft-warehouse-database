﻿CREATE TABLE [dbo].[A_AnaesTime](
	[ATI_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ATI_CD_SEQU] [numeric](9, 0) NOT NULL,
	[ATI_AN_SEQU] [numeric](9, 0) NOT NULL,
	[ATI_NUMBER] [numeric](9, 0) NOT NULL,
	[ATI_TIME] [numeric](9, 0) NOT NULL,
	[ATI_LOG_DATE] [datetime] NULL,
	[ATI_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ATI_SU_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
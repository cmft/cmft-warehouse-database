﻿CREATE TABLE [dbo].[F_Common_Template](
	[CT_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CT_ODT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CT_S1_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CT_UNIQ_ID] [varchar](15) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Rebook_Cat](
	[RBC_SEQU] [numeric](9, 0) NOT NULL,
	[RBC_CODE] [varchar](10) NULL,
	[RBC_DESCRIPTION] [varchar](255) NULL,
	[RBC_INACTIVE] [numeric](3, 0) NOT NULL,
	[RBC_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
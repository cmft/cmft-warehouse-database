﻿CREATE TABLE [dbo].[F_Protocols](
	[PRL_SEQN] [numeric](9, 0) NOT NULL,
	[PRL_SEQU] [numeric](9, 0) NOT NULL,
	[PRL_CODE] [varchar](5) NULL,
	[PRL_DESCRIPTION] [varchar](60) NULL,
	[PRL_URL_HTML_PAGE] [varchar](200) NULL,
	[PRL_WORLDWIDEWEB] [numeric](3, 0) NOT NULL,
	[PRL_CA_SEQU] [numeric](9, 0) NOT NULL,
	[PRL_INACTIVE] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[FUNIT](
	[UN_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UN_CODE] [varchar](5) NULL,
	[UN_DESC] [varchar](60) NULL,
	[UN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UN_LOG_DATE] [datetime] NULL,
	[UN_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[UN_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[UN_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UN_EXT_CODE] [varchar](5) NULL,
	[UN_CSSD_PRINTER] [varchar](255) NULL,
	[UN_CSSD_AUTOPRINT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[UN_IMS_SUITE_MAP] [numeric](9, 0) NOT NULL CONSTRAINT [DF__FUNIT__UN_IMS_SU__7310F064]  DEFAULT ((0))
) ON [PRIMARY]
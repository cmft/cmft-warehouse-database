﻿CREATE TABLE [dbo].[F_Team_Change_Hist](
	[FTC_SEQU] [numeric](9, 0) NOT NULL,
	[FTC_TEAM_NAME] [varchar](125) NOT NULL,
	[FTC_SIGN_OFF_TIME] [datetime] NOT NULL,
	[FTC_COUNT_CORRECT] [numeric](3, 0) NULL,
	[FTC_COMMENTS] [varchar](1000) NULL,
	[FTC_SU_SEQU] [numeric](9, 0) NULL,
	[FTC_FCR_SEQU] [numeric](9, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_NOK](
	[NOK_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[NOK_PIN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[NOK_LAST_NAME] [varchar](30) NULL,
	[NOK_FIRST_NAME] [varchar](30) NULL,
	[NOK_RS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[NOK_STREET] [varchar](60) NULL,
	[NOK_STREET_2] [varchar](60) NULL,
	[NOK_SUBURB] [varchar](60) NULL,
	[NOK_CITY] [varchar](60) NULL,
	[NOK_STATE] [varchar](12) NULL,
	[NOK_POSTCODE] [varchar](12) NULL,
	[NOK_PHONE_HOME] [varchar](25) NULL,
	[NOK_PHONE_WORK] [varchar](25) NULL,
	[NOK_MOBILE_PHONE] [varchar](25) NULL,
	[NOK_EMAIL] [varchar](60) NULL
) ON [PRIMARY]
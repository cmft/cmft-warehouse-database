﻿CREATE TABLE [dbo].[A_RecOrdTrans](
	[ROT_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[ROT_ROI_SEQU] [numeric](9, 0) NOT NULL,
	[ROT_OI_SEQU] [numeric](9, 0) NOT NULL,
	[ROT_SEQU] [numeric](9, 0) NOT NULL,
	[ROT_QTY] [numeric](9, 0) NOT NULL,
	[ROT_BATCH_NUM] [varchar](50) NULL,
	[ROT_EXPIRY_DATE] [datetime] NULL,
	[ROT_LOG_DATE] [datetime] NULL,
	[ROT_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Pat_Anaes_Link](
	[PANL_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PANL_ANA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PANL_OP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PANL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PANL_LOG_DATE] [datetime] NULL,
	[PANL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PANL_PAD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
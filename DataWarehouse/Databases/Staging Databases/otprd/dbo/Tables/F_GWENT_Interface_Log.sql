﻿CREATE TABLE [dbo].[F_GWENT_Interface_Log](
	[GWENT_INT_DATE_TIME] [datetime] NULL,
	[GWENT_INT_SU_SEQU] [numeric](9, 0) NOT NULL,
	[GWENT_INT_EPS_SEQU] [numeric](9, 0) NOT NULL,
	[GWENT_INT_CODES] [varchar](4000) NULL,
	[GWENT_INT_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
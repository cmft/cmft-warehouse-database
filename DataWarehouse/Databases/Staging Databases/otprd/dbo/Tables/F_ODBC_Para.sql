﻿CREATE TABLE [dbo].[F_ODBC_Para](
	[ODBC_DATE_1] [datetime] NULL,
	[ODBC_DATE_2] [datetime] NULL,
	[ODBC_DATA_SOURCE] [text] NULL,
	[ODBC_US_NAME] [varchar](100) NULL,
	[ODBC_US_PASS] [varchar](100) NULL,
	[ODBC_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ODBC_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
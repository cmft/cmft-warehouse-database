﻿CREATE TABLE [dbo].[A_RUS_Setup](
	[RUS_SEQU] [numeric](9, 0) NOT NULL,
	[RUS_LAST_DATE] [datetime] NULL,
	[RUS_DATE_FORMAT] [varchar](15) NULL,
	[RUS_EXTRACT_EXTENSION] [varchar](5) NULL,
	[RUS_BATCH_ID] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
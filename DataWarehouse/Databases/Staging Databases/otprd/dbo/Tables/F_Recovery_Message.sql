﻿CREATE TABLE [dbo].[F_Recovery_Message](
	[RCM_CODE] [varchar](5) NULL,
	[RCM_MESSAGE] [varchar](255) NULL,
	[RCM_SEQU] [numeric](9, 0) NOT NULL,
	[RCM_LOG_DATE] [datetime] NULL,
	[RCM_INACTIVE] [numeric](3, 0) NOT NULL,
	[RCM_CA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
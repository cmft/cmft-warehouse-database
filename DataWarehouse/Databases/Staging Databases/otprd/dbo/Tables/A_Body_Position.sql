﻿CREATE TABLE [dbo].[A_Body_Position](
	[BP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[BP_SEQU] [numeric](9, 0) NOT NULL,
	[BP_CODE] [varchar](5) NULL,
	[BP_DESC] [varchar](60) NULL,
	[BP_INACTIVE] [numeric](3, 0) NOT NULL,
	[BP_LOG_DATE] [datetime] NULL,
	[BP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[BP_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Postcode](
	[PO_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[PO_POSTCODE] [varchar](8) NOT NULL,
	[PO_LOCAL_CODE] [numeric](3, 0) NOT NULL,
	[PO_FILLER_01] [numeric](3, 0) NOT NULL,
	[PO_STATE] [varchar](6) NOT NULL,
	[PO_COUNTRY] [varchar](6) NULL,
	[PO_SEQU] [numeric](9, 0) NOT NULL,
	[PO_LOCKED] [numeric](3, 0) NOT NULL,
	[PO_SUBURB] [varchar](40) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Licence_Header](
	[LIH_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIH_SITE_CODE] [varchar](25) NOT NULL,
	[LIH_CREATE_DATE] [datetime] NULL,
	[LIH_PERIOD_TYPE] [numeric](3, 0) NOT NULL DEFAULT ((0)),
	[LIH_PERIOD_START] [datetime] NULL,
	[LIH_PERIOD_DAYS] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIH_EMAIL_ADDR] [varchar](50) NULL,
	[LIH_EMAIL_LIL_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIH_EMAIL_USER] [varchar](63) NULL,
	[LIH_EMAIL_PASS] [varchar](50) NULL,
	[LIH_EMAIL_COPY_ADDR] [varchar](50) NULL
) ON [PRIMARY]
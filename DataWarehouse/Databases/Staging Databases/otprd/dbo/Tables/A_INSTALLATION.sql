﻿CREATE TABLE [dbo].[A_INSTALLATION](
	[INT_EDIS_VRSN] [varchar](9) NULL,
	[INT_DTE_INSTALL] [datetime] NULL,
	[INT_DESCRIPTION] [varchar](255) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
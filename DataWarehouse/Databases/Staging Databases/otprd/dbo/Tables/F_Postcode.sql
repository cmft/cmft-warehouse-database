﻿CREATE TABLE [dbo].[F_Postcode](
	[PO_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[PO_POSTCODE] [varchar](8) NOT NULL,
	[PO_LOCAL_CODE] [numeric](3, 0) NOT NULL,
	[PO_FILLER_01] [numeric](3, 0) NOT NULL,
	[PO_STATE] [varchar](6) NOT NULL,
	[PO_COUNTRY] [varchar](6) NULL,
	[PO_SEQU] [numeric](9, 0) NOT NULL,
	[PO_LOCKED] [numeric](3, 0) NOT NULL,
	[PO_SUBURB] [varchar](40) NOT NULL
) ON [PRIMARY]
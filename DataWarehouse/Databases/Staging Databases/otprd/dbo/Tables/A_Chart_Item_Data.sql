﻿CREATE TABLE [dbo].[A_Chart_Item_Data](
	[CID_SEQU] [numeric](9, 0) NOT NULL,
	[CID_CIH_SEQU] [numeric](9, 0) NOT NULL,
	[CID_DATETIME] [datetime] NULL,
	[CID_DATA] [numeric](11, 2) NOT NULL,
	[CID_NOTES] [varchar](255) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
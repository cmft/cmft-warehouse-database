﻿CREATE TABLE [dbo].[F_EquipProced](
	[EP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[EP_CD_SEQU] [numeric](9, 0) NOT NULL,
	[EP_ITY_SEQU] [numeric](9, 0) NOT NULL,
	[EP_LOG_DATE] [datetime] NULL,
	[EP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[EP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
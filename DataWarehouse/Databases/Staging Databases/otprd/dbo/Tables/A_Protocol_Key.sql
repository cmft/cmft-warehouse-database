﻿CREATE TABLE [dbo].[A_Protocol_Key](
	[PROK_SEQN] [numeric](9, 0) NOT NULL,
	[PROK_KEY_WORD] [varchar](30) NULL,
	[PROK_PRL_SEQU] [numeric](9, 0) NOT NULL,
	[PROK_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
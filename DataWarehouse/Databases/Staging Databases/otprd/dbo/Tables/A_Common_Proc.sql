﻿CREATE TABLE [dbo].[A_Common_Proc](
	[CS_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[CS_UNIQ_ID] [varchar](15) NOT NULL,
	[CS_CD_SEQU] [numeric](9, 0) NOT NULL,
	[CS_S1_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
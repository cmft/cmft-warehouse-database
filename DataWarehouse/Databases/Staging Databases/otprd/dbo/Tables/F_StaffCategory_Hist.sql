﻿CREATE TABLE [dbo].[F_StaffCategory_Hist](
	[SCH_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SCH_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SCH_START_DATE] [datetime] NULL,
	[SCH_END_DATE] [datetime] NULL,
	[SCH_SC_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0))
) ON [PRIMARY]
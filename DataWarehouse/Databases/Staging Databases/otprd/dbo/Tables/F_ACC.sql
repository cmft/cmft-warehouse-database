﻿CREATE TABLE [dbo].[F_ACC](
	[ACC_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ACC_CODE] [varchar](5) NULL,
	[ACC_DESC] [varchar](40) NULL,
	[ACC_SEQU] [numeric](9, 0) NOT NULL,
	[ACC_LOG_DATE] [datetime] NULL,
	[ACC_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ACC_INACTIVE] [numeric](3, 0) NOT NULL,
	[ACC_EXTRACT_CODE] [varchar](5) NULL,
	[ACC_CA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
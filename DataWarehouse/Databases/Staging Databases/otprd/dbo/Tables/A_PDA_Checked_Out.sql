﻿CREATE TABLE [dbo].[A_PDA_Checked_Out](
	[PDCO_BOOKING_SEQU] [numeric](9, 0) NOT NULL,
	[PDCO_USER_SEQU] [numeric](9, 0) NOT NULL,
	[PDCO_CHECK_OUT_TIME] [datetime] NULL,
	[PDCO_PDA_NAME] [varchar](50) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
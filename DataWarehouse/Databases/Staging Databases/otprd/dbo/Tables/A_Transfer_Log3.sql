﻿CREATE TABLE [dbo].[A_Transfer_Log3](
	[TL3_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TL3_DATE_TIME_STARTED] [datetime] NULL,
	[TL3_FILLER_01] [numeric](3, 0) NOT NULL,
	[TL3_DATE_TIME_FINISHED] [datetime] NULL,
	[TL3_STATUS] [numeric](3, 0) NOT NULL,
	[TL3_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_LettersBook](
	[LEB_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[LEB_NAME] [varchar](50) NULL,
	[LEB_TEXT] [text] NULL,
	[LEB_LOG_DATE] [datetime] NULL,
	[LEB_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[LEB_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
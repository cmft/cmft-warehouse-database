﻿CREATE TABLE [dbo].[F_Residence](
	[RES_SEQU] [numeric](9, 0) NOT NULL,
	[RES_CODE] [varchar](20) NULL,
	[RES_DESC] [varchar](60) NULL,
	[RES_MAP_DB] [varchar](20) NULL,
	[RES_EXTRACT_VALUE] [varchar](20) NULL,
	[RES_INACTIVE] [numeric](3, 0) NOT NULL,
	[RES_LOCKED] [numeric](3, 0) NOT NULL,
	[RES_CA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
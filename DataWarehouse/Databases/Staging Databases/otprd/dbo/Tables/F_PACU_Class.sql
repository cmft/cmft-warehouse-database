﻿CREATE TABLE [dbo].[F_PACU_Class](
	[PC_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PC_CODE] [varchar](5) NULL,
	[PC_DESC] [varchar](60) NULL,
	[PC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PC_LOG_DATE] [datetime] NULL,
	[PC_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PC_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[PC_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_PACU_Cl__PC_CA__32024716]  DEFAULT ((0))
) ON [PRIMARY]
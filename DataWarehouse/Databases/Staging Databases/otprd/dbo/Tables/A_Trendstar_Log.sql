﻿CREATE TABLE [dbo].[A_Trendstar_Log](
	[TSL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TSL_DATE_TIME] [datetime] NULL,
	[TSL_FILLER_01] [numeric](3, 0) NOT NULL,
	[TSL_DATE_START] [datetime] NULL,
	[TSL_DATE_FINISH] [datetime] NULL,
	[TSL_LOG_DATE] [datetime] NULL,
	[TSL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[TSL_STATUS] [numeric](3, 0) NOT NULL,
	[TSL_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
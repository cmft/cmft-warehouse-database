﻿CREATE TABLE [dbo].[F_Insure_Status](
	[IS_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[IS_CODE] [varchar](5) NULL,
	[IS_DESCRIPTION] [varchar](60) NULL,
	[IS_SEQU] [numeric](9, 0) NOT NULL,
	[IS_LOG_DATE] [datetime] NULL,
	[IS_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[IS_INACTIVE] [numeric](3, 0) NOT NULL,
	[IS_EXTRACT_CODE] [varchar](5) NULL,
	[IS_CA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
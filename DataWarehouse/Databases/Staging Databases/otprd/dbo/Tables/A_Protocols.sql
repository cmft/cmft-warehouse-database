﻿CREATE TABLE [dbo].[A_Protocols](
	[PRL_SEQN] [numeric](9, 0) NOT NULL,
	[PRL_SEQU] [numeric](9, 0) NOT NULL,
	[PRL_CODE] [varchar](5) NULL,
	[PRL_DESCRIPTION] [varchar](60) NULL,
	[PRL_URL_HTML_PAGE] [varchar](200) NULL,
	[PRL_WORLDWIDEWEB] [numeric](3, 0) NOT NULL,
	[PRL_CA_SEQU] [numeric](9, 0) NOT NULL,
	[PRL_INACTIVE] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
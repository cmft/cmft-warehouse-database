﻿CREATE TABLE [dbo].[F_InvLocation](
	[IL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[IL_CODE] [varchar](5) NULL,
	[IL_DESC] [varchar](60) NULL,
	[IL_SEQU] [numeric](9, 0) NOT NULL,
	[IL_LOG_DATE] [datetime] NULL,
	[IL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[IL_RECOVERY] [numeric](3, 0) NOT NULL,
	[IL_UN_SEQU] [numeric](9, 0) NOT NULL,
	[IL_INACTIVE] [numeric](3, 0) NOT NULL,
	[IL_CA_SEQU] [numeric](9, 0) NOT NULL,
	[IL_EXTERNAL_FLAG] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
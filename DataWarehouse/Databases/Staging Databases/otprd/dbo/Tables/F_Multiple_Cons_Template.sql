﻿CREATE TABLE [dbo].[F_Multiple_Cons_Template](
	[MCT_SEQU] [numeric](9, 0) NOT NULL,
	[MCT_IT_SEQU] [numeric](9, 0) NOT NULL,
	[MCT_DAY_NO] [numeric](3, 0) NOT NULL,
	[MCT_SESSION] [numeric](3, 0) NOT NULL,
	[MCT_CON_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Tourniquet](
	[TO_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TO_SEQU] [numeric](9, 0) NOT NULL,
	[TO_OP_SEQU] [numeric](9, 0) NOT NULL,
	[TO_DATE_TIME_ON] [datetime] NULL,
	[TO_DATE_TIME_OFF] [datetime] NULL,
	[TO_LOG_DATE] [datetime] NULL,
	[TO_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[TO_BS_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL,
	[TO_PRESSURE] [varchar](9) NULL
) ON [PRIMARY]
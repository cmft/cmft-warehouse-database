﻿CREATE TABLE [dbo].[A_CompInvItems](
	[CII_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[CII_COMP_II_SEQU] [numeric](9, 0) NOT NULL,
	[CII_II_SEQU] [numeric](9, 0) NOT NULL,
	[CII_AMOUNT] [numeric](9, 0) NOT NULL,
	[CII_SEQU] [numeric](9, 0) NOT NULL,
	[CII_LOG_DATE] [datetime] NULL,
	[CII_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL,
	[CII_PACKETS] [numeric](3, 0) NOT NULL,
	[CII_COMP_EQP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
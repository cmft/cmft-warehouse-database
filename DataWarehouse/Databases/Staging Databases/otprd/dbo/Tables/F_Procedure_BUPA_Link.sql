﻿CREATE TABLE [dbo].[F_Procedure_BUPA_Link](
	[PBUPA_CD_SEQU] [numeric](9, 0) NOT NULL,
	[PBUPA_BUPA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
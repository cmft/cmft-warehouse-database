﻿CREATE TABLE [dbo].[F_Generic_Reason](
	[GR_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[GR_CODE] [varchar](2) NULL,
	[GR_DESCRIPTION] [varchar](60) NULL,
	[GR_EXT_CODE] [varchar](5) NULL,
	[GR_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[GR_LOG_DATE] [datetime] NULL,
	[GR_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[GR_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[GR_EXTRACT_CODE] [varchar](5) NULL,
	[GR_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[GR_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Generic__GR_CA__1471E42F]  DEFAULT ((0))
) ON [PRIMARY]
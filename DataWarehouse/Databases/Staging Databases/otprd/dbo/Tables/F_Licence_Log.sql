﻿CREATE TABLE [dbo].[F_Licence_Log](
	[LIL_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[LIL_CREATE_DATE] [datetime] NULL,
	[LIL_SERIAL_NUMBER] [varchar](25) NULL,
	[LIL_COMPUTER_ID] [varchar](100) NULL,
	[LIL_SITE_CODE] [varchar](25) NOT NULL
) ON [PRIMARY]
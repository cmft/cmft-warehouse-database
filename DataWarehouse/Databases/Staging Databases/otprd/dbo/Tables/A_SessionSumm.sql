﻿CREATE TABLE [dbo].[A_SessionSumm](
	[SS_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SS_TOT_ACTUAL] [numeric](9, 0) NOT NULL,
	[SS_TOT_AVAIL] [numeric](9, 0) NOT NULL,
	[SS_DATE_SESS] [varchar](14) NULL,
	[SS_START_DATE_TIME] [datetime] NULL,
	[SS_SESSION] [numeric](3, 0) NOT NULL,
	[SS_THEATRE] [numeric](9, 0) NOT NULL,
	[SS_CONSULTANT] [numeric](9, 0) NOT NULL,
	[SS_SPECIALTY] [numeric](9, 0) NOT NULL,
	[SS_FILLER_01] [datetime] NULL,
	[SS_FINISH_DATE_TIME] [datetime] NULL,
	[SS_SEQU] [numeric](9, 0) NOT NULL,
	[SS_CONS_SEQU] [numeric](9, 0) NOT NULL,
	[SS_SURG1_SEQU] [numeric](9, 0) NOT NULL,
	[SS_SURG2_SEQU] [numeric](9, 0) NOT NULL,
	[SS_ANAES1_SEQU] [numeric](9, 0) NOT NULL,
	[SS_ANAES2_SEQU] [numeric](9, 0) NOT NULL,
	[SS_ANAES3_SEQU] [numeric](9, 0) NOT NULL,
	[SS_SCOUT_SEQU] [numeric](9, 0) NOT NULL,
	[SS_INST_SEQU] [numeric](9, 0) NOT NULL,
	[SS_ANAESN_SEQU] [numeric](9, 0) NOT NULL,
	[SS_LOG_DATE] [datetime] NULL,
	[SS_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SS_TI_SEQU] [numeric](9, 0) NOT NULL,
	[SS_TH_SEQU] [numeric](9, 0) NOT NULL,
	[SS_S1_SEQU] [numeric](9, 0) NOT NULL,
	[SS_SURG3_SEQU] [numeric](9, 0) NOT NULL,
	[SS_VIRTUAL_ALLOC] [numeric](9, 0) NOT NULL,
	[SS_ORDER] [numeric](3, 0) NOT NULL,
	[SS_CANCELLED] [numeric](9, 0) NOT NULL,
	[SS_REASON] [varchar](255) NULL,
	[SS_REASON_DATE_TIME] [datetime] NULL,
	[SS_REASON_STAFF_SEQU] [numeric](9, 0) NOT NULL,
	[SS_REINSTATE_STAFF_SEQU] [numeric](9, 0) NOT NULL,
	[SS_REINSTATE_REASON_SEQU] [numeric](9, 0) NOT NULL,
	[SS_CT_SEQU] [numeric](9, 0) NOT NULL,
	[SS_ST_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL,
	[SS_SET_CODE] [varchar](60) NULL,
	[SS_THT_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SS_THT_CODE] [varchar](60) NULL,
	[SS_LOCK_UNLOCK_TYPE] [numeric](3, 0) NOT NULL CONSTRAINT [DF__A_Session__SS_LO__24BE25E8]  DEFAULT ((0))
) ON [PRIMARY]
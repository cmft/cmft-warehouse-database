﻿CREATE TABLE [dbo].[F_PainRelief](
	[PAI_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PAI_BUTTON_1] [varchar](20) NULL,
	[PAI_BUTTON_2] [varchar](20) NULL,
	[PAI_BUTTON_3] [varchar](20) NULL,
	[PAI_BUTTON_4] [varchar](20) NULL,
	[PAI_BUTTON_5] [varchar](20) NULL,
	[PAI_BUTTON_6] [varchar](20) NULL,
	[PAI_BUTTON_7] [varchar](20) NULL,
	[PAI_BUTTON_8] [varchar](20) NULL,
	[PAI_BUTTON_9] [varchar](20) NULL,
	[PAI_BUTTON_10] [varchar](20) NULL,
	[PAI_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PAI_LOG_DATE] [datetime] NULL,
	[PAI_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Initiator_Link](
	[INL_SEQU] [numeric](9, 0) NOT NULL,
	[INL_GLI_SEQU] [numeric](9, 0) NOT NULL,
	[INL_CN_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
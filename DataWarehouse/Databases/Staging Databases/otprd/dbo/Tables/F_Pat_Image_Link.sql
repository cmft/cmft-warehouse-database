﻿CREATE TABLE [dbo].[F_Pat_Image_Link](
	[PIL_SEQU] [numeric](9, 0) NOT NULL,
	[PIL_PAT_SEQU] [numeric](9, 0) NOT NULL,
	[PIL_OIM_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Pat_Pre_Image_Link](
	[PPIL_SEQU] [numeric](9, 0) NOT NULL,
	[PPIL_PRE_SEQU] [numeric](9, 0) NOT NULL,
	[PPIL_OIM_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
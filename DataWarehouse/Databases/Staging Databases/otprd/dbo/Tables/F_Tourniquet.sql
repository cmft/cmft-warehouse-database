﻿CREATE TABLE [dbo].[F_Tourniquet](
	[TO_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TO_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TO_OP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TO_DATE_TIME_ON] [datetime] NULL,
	[TO_DATE_TIME_OFF] [datetime] NULL,
	[TO_LOG_DATE] [datetime] NULL,
	[TO_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[TO_BS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TO_PRESSURE] [varchar](9) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Anaes_Maintenance](
	[MAN_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MAN_CODE] [varchar](5) NULL,
	[MAN_DESCRIPTION] [varchar](60) NULL,
	[MAN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MAN_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[MAN_LOG_DATE] [datetime] NULL,
	[MAN_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[MAN_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Anaes_M__MAN_C__20D7BB14]  DEFAULT ((0))
) ON [PRIMARY]
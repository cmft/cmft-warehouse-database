﻿CREATE TABLE [dbo].[A_Problems](
	[PRB_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[PRB_SEQU] [numeric](9, 0) NOT NULL,
	[PRB_CODE] [varchar](5) NULL,
	[PRB_DESCRIPTION] [varchar](60) NULL,
	[PRB_INACTIVE] [numeric](3, 0) NOT NULL,
	[PRB_LOG_DATE] [datetime] NULL,
	[PRB_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PRB_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
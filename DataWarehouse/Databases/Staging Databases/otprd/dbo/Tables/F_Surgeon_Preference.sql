﻿CREATE TABLE [dbo].[F_Surgeon_Preference](
	[SPR_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SPR_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_II_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_CD_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_PRI_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_SU_SEQU] [numeric](9, 0) NOT NULL,
	[SPR_LOG_DATE] [datetime] NULL,
	[SPR_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SPR_QTY] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
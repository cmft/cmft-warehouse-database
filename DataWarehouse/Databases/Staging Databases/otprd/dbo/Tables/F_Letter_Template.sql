﻿CREATE TABLE [dbo].[F_Letter_Template](
	[LET_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LET_NAME] [varchar](100) NULL,
	[LET_DESCRIPTION] [varchar](255) NULL,
	[LET_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[LET_TXT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LET_LOGO_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LET_LEFT_LOGO_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[LET_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Letter___LET_C__1EEF72A2]  DEFAULT ((0))
) ON [PRIMARY]
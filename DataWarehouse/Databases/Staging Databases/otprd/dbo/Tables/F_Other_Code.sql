﻿CREATE TABLE [dbo].[F_Other_Code](
	[OTC_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[OTC_CODE] [varchar](5) NULL,
	[OTC_DESCRIPTION] [varchar](60) NULL,
	[OTC_SEQU] [numeric](9, 0) NOT NULL,
	[OTC_LOG_DATE] [datetime] NULL,
	[OTC_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[OTC_S1_SEQU] [numeric](9, 0) NOT NULL,
	[OTC_INACTIVE] [numeric](3, 0) NOT NULL,
	[OTC_CA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_ReportQueLists](
	[RQL_REP_SEQU] [numeric](9, 0) NOT NULL,
	[RQL_REP_LIST_ID] [numeric](3, 0) NOT NULL,
	[RQL_LIST] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
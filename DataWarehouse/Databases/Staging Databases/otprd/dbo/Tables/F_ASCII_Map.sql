﻿CREATE TABLE [dbo].[F_ASCII_Map](
	[ASC_SEQU] [numeric](9, 0) NOT NULL,
	[ASC_DEC] [numeric](9, 0) NOT NULL,
	[ASC_MAPPED] [varchar](20) NULL,
	[ASC_IR_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
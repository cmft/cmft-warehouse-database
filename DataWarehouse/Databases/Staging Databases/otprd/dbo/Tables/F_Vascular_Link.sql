﻿CREATE TABLE [dbo].[F_Vascular_Link](
	[VASL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[VASL_PAD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[VASL_VAS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[VASL_GA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Marital_Status](
	[MS_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MS_CODE] [varchar](5) NULL,
	[MS_DESCRIPTION] [varchar](60) NULL,
	[MS_EXT_CODE] [varchar](5) NULL,
	[MS_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[MS_LOG_DATE] [datetime] NULL,
	[MS_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[MS_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[MS_EXTRACT_CODE] [varchar](5) NULL,
	[MS_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[MS_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[MS_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Marital__MS_CA__21CBDF4D]  DEFAULT ((0))
) ON [PRIMARY]
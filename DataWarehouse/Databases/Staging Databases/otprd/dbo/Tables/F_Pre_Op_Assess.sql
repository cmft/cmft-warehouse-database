﻿CREATE TABLE [dbo].[F_Pre_Op_Assess](
	[POA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[POA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[POA_CODE] [varchar](5) NULL,
	[POA_DESCRIPTION] [varchar](60) NULL,
	[POA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[POA_LOG_DATE] [datetime] NULL,
	[POA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[POA_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Pre_Op___POA_C__025333F4]  DEFAULT ((0))
) ON [PRIMARY]
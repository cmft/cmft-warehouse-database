﻿CREATE TABLE [dbo].[A_NPfIT_LR](
	[NLR_SEQU] [numeric](9, 0) NOT NULL,
	[NLR_LR_IDENT] [varchar](255) NULL,
	[NLR_NHS_NUMBER] [varchar](20) NULL,
	[NLR_WORKGROUP] [varchar](255) NULL,
	[NLR_STATUS] [varchar](20) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
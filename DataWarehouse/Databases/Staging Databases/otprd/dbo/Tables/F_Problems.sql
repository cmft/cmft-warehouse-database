﻿CREATE TABLE [dbo].[F_Problems](
	[PRB_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PRB_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[PRB_CODE] [varchar](5) NULL,
	[PRB_DESCRIPTION] [varchar](60) NULL,
	[PRB_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[PRB_LOG_DATE] [datetime] NULL,
	[PRB_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PRB_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Problem__PRB_C__2C496DC0]  DEFAULT ((0))
) ON [PRIMARY]
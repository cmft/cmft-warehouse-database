﻿CREATE TABLE [dbo].[F_OrdItems](
	[ORI_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[ORI_QTY] [numeric](9, 0) NOT NULL,
	[ORI_OR_SEQU] [numeric](9, 0) NOT NULL,
	[ORI_SII_SEQU] [numeric](9, 0) NOT NULL,
	[ORI_SEQU] [numeric](9, 0) NOT NULL,
	[ORI_II_SEQU] [numeric](9, 0) NOT NULL,
	[ORI_LOG_DATE] [datetime] NULL,
	[ORI_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Trendstar_Log](
	[TSL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TSL_DATE_TIME] [datetime] NULL,
	[TSL_FILLER_01] [numeric](3, 0) NOT NULL,
	[TSL_DATE_START] [datetime] NULL,
	[TSL_DATE_FINISH] [datetime] NULL,
	[TSL_LOG_DATE] [datetime] NULL,
	[TSL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[TSL_STATUS] [numeric](3, 0) NOT NULL,
	[TSL_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
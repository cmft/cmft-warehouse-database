﻿CREATE TABLE [dbo].[A_ACC](
	[ACC_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[ACC_CODE] [varchar](5) NULL,
	[ACC_DESC] [varchar](40) NULL,
	[ACC_SEQU] [numeric](9, 0) NOT NULL,
	[ACC_LOG_DATE] [datetime] NULL,
	[ACC_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ACC_INACTIVE] [numeric](3, 0) NOT NULL,
	[ACC_EXTRACT_CODE] [varchar](5) NULL,
	[ACC_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Intfce_Repos](
	[IR_SEQU] [numeric](9, 0) NOT NULL,
	[IR_INTFC_NAME] [varchar](30) NULL,
	[IR_ACTIVE] [numeric](3, 0) NOT NULL,
	[IR_CONN_PARAM_1] [varchar](30) NULL,
	[IR_CONN_PARAM_2] [varchar](30) NULL,
	[IR_CONN_PARAM_3] [varchar](30) NULL,
	[IR_CONN_PARAM_4] [varchar](30) NULL,
	[IR_CONN_PARAM_5] [varchar](30) NULL,
	[IR_CUSTOM_PARAM_1] [numeric](3, 0) NOT NULL,
	[IR_NACK_RETRY] [numeric](9, 0) NOT NULL,
	[IR_RETRY_COMS] [numeric](9, 0) NOT NULL,
	[IR_RETRY_TIMEOUT] [numeric](9, 0) NOT NULL,
	[IR_CA_SEQU] [numeric](9, 0) NOT NULL,
	[IR_HL7_NAME] [varchar](20) NULL
) ON [PRIMARY]
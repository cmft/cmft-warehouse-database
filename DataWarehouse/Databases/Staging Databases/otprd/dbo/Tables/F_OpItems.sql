﻿CREATE TABLE [dbo].[F_OpItems](
	[OPI_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[OPI_SEQU] [numeric](9, 0) NOT NULL,
	[OPI_BS_SEQU] [numeric](9, 0) NOT NULL,
	[OPI_II_SEQU] [numeric](9, 0) NOT NULL,
	[OPI_OP_SEQU] [numeric](9, 0) NOT NULL,
	[OPI_ITY_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
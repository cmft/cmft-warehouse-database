﻿CREATE TABLE [dbo].[A_Team_Change_Hist](
	[FTC_SEQU] [numeric](9, 0) NOT NULL,
	[FTC_TEAM_NAME] [varchar](125) NOT NULL,
	[FTC_SIGN_OFF_TIME] [datetime] NOT NULL,
	[FTC_COUNT_CORRECT] [numeric](3, 0) NULL,
	[FTC_COMMENTS] [varchar](1000) NULL,
	[FTC_SU_SEQU] [numeric](9, 0) NULL,
	[FTC_FCR_SEQU] [numeric](9, 0) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
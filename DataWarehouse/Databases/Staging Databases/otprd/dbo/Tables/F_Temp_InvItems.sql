﻿CREATE TABLE [dbo].[F_Temp_InvItems](
	[TIN_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TIN_SERIAL_NO] [varchar](25) NULL,
	[TIN_DESCRIPTION] [varchar](200) NULL,
	[TIN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TIN_PAT_COST] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TIN_IT_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[TIN_LOG_DATE] [datetime] NULL,
	[TIN_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[TIN_REBATE_NO] [varchar](25) NULL,
	[TIN_TYPE] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
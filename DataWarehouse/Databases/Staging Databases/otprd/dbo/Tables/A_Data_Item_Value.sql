﻿CREATE TABLE [dbo].[A_Data_Item_Value](
	[DIV_SEQU] [numeric](9, 0) NOT NULL,
	[DIV_DIH_SEQU] [numeric](9, 0) NOT NULL,
	[DIV_NAME] [varchar](100) NULL,
	[DIV_DESCRIPTION] [varchar](255) NULL,
	[DIV_TYPE] [varchar](30) NULL,
	[DIV_SUBTYPE] [varchar](30) NULL,
	[DIV_ORDER] [numeric](9, 0) NULL,
	[DIV_DIV_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
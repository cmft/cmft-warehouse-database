﻿CREATE TABLE [dbo].[F_NPfIT_LR](
	[NLR_SEQU] [numeric](9, 0) NOT NULL,
	[NLR_LR_IDENT] [varchar](255) NULL,
	[NLR_NHS_NUMBER] [varchar](20) NULL,
	[NLR_WORKGROUP] [varchar](255) NULL,
	[NLR_STATUS] [varchar](20) NULL
) ON [PRIMARY]
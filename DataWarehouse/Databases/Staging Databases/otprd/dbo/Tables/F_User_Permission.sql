﻿CREATE TABLE [dbo].[F_User_Permission](
	[USP_US_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[USP_PERMISSION] [varchar](255) NOT NULL,
	[USP_ALLOW] [numeric](3, 0) NOT NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Ethnicity](
	[ETH_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ETH_CODE] [varchar](3) NULL,
	[ETH_DESCRIPTION] [varchar](60) NULL,
	[ETH_DEFAULT] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ETH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ETH_LOG_DATE] [datetime] NULL,
	[ETH_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ETH_EXT_CODE] [varchar](5) NULL,
	[ETH_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ETH_EXTRACT_CODE] [varchar](5) NULL,
	[ETH_EXT_CODE_FLAG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ETH_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Ethnici__ETH_C__7BA63665]  DEFAULT ((0))
) ON [PRIMARY]
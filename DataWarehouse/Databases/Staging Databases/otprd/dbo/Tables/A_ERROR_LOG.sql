﻿CREATE TABLE [dbo].[A_ERROR_LOG](
	[EL_DATE_TIME] [datetime] NULL,
	[EL_FILLER_01] [numeric](3, 0) NOT NULL,
	[EL_CODE] [varchar](10) NULL,
	[EL_TEXT] [varchar](200) NULL,
	[EL_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
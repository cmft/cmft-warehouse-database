﻿CREATE TABLE [dbo].[F_PatInvItems](
	[PII_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[PII_DATE] [datetime] NULL,
	[PII_DATE_RET] [datetime] NULL,
	[PII_CURRENT] [numeric](9, 0) NOT NULL,
	[PII_OP_SEQU] [numeric](9, 0) NOT NULL,
	[PII_ROT_SEQU] [numeric](9, 0) NOT NULL,
	[PII_II_SEQU] [numeric](9, 0) NOT NULL,
	[PII_QTY] [numeric](9, 0) NOT NULL,
	[PII_SEQU] [numeric](9, 0) NOT NULL,
	[PII_BD_SEQU] [numeric](9, 0) NOT NULL,
	[PII_SD_SEQU] [numeric](9, 0) NOT NULL,
	[PII_LOG_DATE] [datetime] NULL,
	[PII_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[PII_PA_SEQU] [numeric](9, 0) NOT NULL,
	[PII_CD_SEQU] [numeric](9, 0) NOT NULL,
	[PII_ALTERED] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
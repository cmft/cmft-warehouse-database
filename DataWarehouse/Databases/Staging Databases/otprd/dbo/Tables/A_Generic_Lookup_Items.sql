﻿CREATE TABLE [dbo].[A_Generic_Lookup_Items](
	[GLI_SEQU] [numeric](9, 0) NOT NULL,
	[GLI_CODE] [varchar](10) NULL,
	[GLI_DESCRIPTION] [varchar](255) NULL,
	[GLI_EXT_DB] [varchar](10) NULL,
	[GLI_EXTRACT] [varchar](10) NULL,
	[GLI_INACTIVE] [numeric](3, 0) NOT NULL,
	[GLI_GLC_SEQU] [numeric](9, 0) NOT NULL,
	[GLI_SORT_ORDER] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Status_History](
	[SH_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[SH_DATE_START] [datetime] NULL,
	[SH_WD_SEQU] [numeric](9, 0) NOT NULL,
	[SH_STATUS_SEQU] [numeric](9, 0) NOT NULL,
	[SH_STATUS_FLAG] [varchar](2) NULL,
	[SH_LOG_DATE] [datetime] NULL,
	[SH_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[SH_DATE_FINISH] [datetime] NULL,
	[SH_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
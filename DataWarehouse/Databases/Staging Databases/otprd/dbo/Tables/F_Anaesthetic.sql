﻿CREATE TABLE [dbo].[F_Anaesthetic](
	[ANA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ANA_CODE] [varchar](6) NULL,
	[ANA_DESCRIPTION] [varchar](60) NULL,
	[ANA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ANA_LOG_DATE] [datetime] NULL,
	[ANA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ANA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ANA_MINIMAL_TIMINGS] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ANA_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Anaesth__ANA_C__7F76C749]  DEFAULT ((0)),
	[ANA_START_DATE] [datetime] NULL,
	[ANA_END_DATE] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Session_Type](
	[SET_SEQU] [numeric](9, 0) NOT NULL,
	[SET_CODE] [varchar](10) NULL,
	[SET_DESC] [varchar](60) NULL,
	[SET_INACTIVE] [numeric](3, 0) NOT NULL,
	[SET_OP_TYPE] [numeric](3, 0) NOT NULL,
	[SET_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_NPfIT_Mapping](
	[NPM_SEQU] [numeric](9, 0) NOT NULL,
	[NPM_WORKGROUP_ID] [varchar](100) NULL,
	[NPM_OBJECT_SEQU] [numeric](9, 0) NOT NULL,
	[NPM_OBJECT_TYPE] [varchar](20) NULL,
	[NPM_PRIMARY] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
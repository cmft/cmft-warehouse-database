﻿CREATE TABLE [dbo].[A_State](
	[STA_SEQU] [numeric](9, 0) NOT NULL,
	[STA_CODE] [varchar](12) NULL,
	[STA_DESC] [varchar](60) NULL,
	[STA_MAP_DB] [varchar](12) NULL,
	[STA_EXTRACT_VALUE] [varchar](12) NULL,
	[STA_LOCKED] [numeric](3, 0) NOT NULL,
	[STA_INACTIVE] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
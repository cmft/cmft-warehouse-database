﻿CREATE TABLE [dbo].[F_Protocol_Key](
	[PROK_SEQN] [numeric](9, 0) NOT NULL,
	[PROK_KEY_WORD] [varchar](30) NULL,
	[PROK_PRL_SEQU] [numeric](9, 0) NOT NULL,
	[PROK_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
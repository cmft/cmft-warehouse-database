﻿CREATE TABLE [dbo].[F_Returns](
	[RET_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[RET_DATE] [datetime] NULL,
	[RET_SII_SEQU] [numeric](9, 0) NOT NULL,
	[RET_SEQU] [numeric](9, 0) NOT NULL,
	[RET_ROI_SEQU] [numeric](9, 0) NOT NULL,
	[RET_IRD_SEQU] [numeric](9, 0) NOT NULL,
	[RET_QTY] [numeric](9, 0) NOT NULL,
	[RET_LOG_DATE] [datetime] NULL,
	[RET_LOG_DETAILS_FILLER] [varchar](1) NULL
) ON [PRIMARY]
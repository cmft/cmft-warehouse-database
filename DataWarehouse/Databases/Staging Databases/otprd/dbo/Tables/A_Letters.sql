﻿CREATE TABLE [dbo].[A_Letters](
	[LE_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[LE_NAME] [varchar](50) NULL,
	[LE_LOG_DATE] [datetime] NULL,
	[LE_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[LE_AUDIT_LETTER] [numeric](3, 0) NOT NULL,
	[LE_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Inv_Booking_Items](
	[IBI_SEQU] [numeric](9, 0) NOT NULL,
	[IBI_PA_SEQU] [numeric](9, 0) NOT NULL,
	[IBI_PI_SEQU] [numeric](9, 0) NOT NULL,
	[IBI_II_SEQU] [numeric](9, 0) NOT NULL,
	[IBI_QTY] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
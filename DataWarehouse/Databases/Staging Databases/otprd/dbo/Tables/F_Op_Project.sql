﻿CREATE TABLE [dbo].[F_Op_Project](
	[OPPJ_SEQU] [numeric](9, 0) NOT NULL,
	[OPPJ_OP_SEQU] [numeric](9, 0) NOT NULL,
	[OPPJ_PRJ_SEQU] [numeric](9, 0) NOT NULL,
	[OPPJ_MANUAL] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
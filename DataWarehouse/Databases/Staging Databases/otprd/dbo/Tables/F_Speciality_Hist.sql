﻿CREATE TABLE [dbo].[F_Speciality_Hist](
	[SPH_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SPH_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SPH_START_DATE] [datetime] NULL,
	[SPH_END_DATE] [datetime] NULL,
	[SPH_S1_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0))
) ON [PRIMARY]
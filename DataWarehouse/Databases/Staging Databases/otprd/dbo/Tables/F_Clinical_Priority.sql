﻿CREATE TABLE [dbo].[F_Clinical_Priority](
	[CLP_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CLP_CODE] [varchar](3) NULL,
	[CLP_DESCRIPTION] [varchar](60) NULL,
	[CLP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CLP_MAX_TIME] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CLP_LOG_DATE] [datetime] NULL,
	[CLP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[CLP_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[CLP_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Clinica__CLP_C__09003183]  DEFAULT ((0)),
	[CLP_START_DATE] [datetime] NULL,
	[CLP_END_DATE] [datetime] NULL,
	[CLP_OP_TYPE] [numeric](3, 0) NOT NULL CONSTRAINT [DF__F_Clinica__CLP_O__38DA2D2E]  DEFAULT ((0))
) ON [PRIMARY]
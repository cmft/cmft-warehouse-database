﻿CREATE TABLE [dbo].[F_Out_Province](
	[OOP_SEQU] [numeric](9, 0) NOT NULL,
	[OOP_CODE] [varchar](12) NULL,
	[OOP_DESC] [varchar](60) NULL,
	[OOP_MAP_DB] [varchar](12) NULL,
	[OOP_EXTRACT_VALUE] [varchar](12) NULL,
	[OOP_INACTIVE] [numeric](3, 0) NOT NULL,
	[OOP_LOCKED] [numeric](3, 0) NOT NULL,
	[OOP_CA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Transfer_Log3](
	[TL3_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[TL3_DATE_TIME_STARTED] [datetime] NULL,
	[TL3_FILLER_01] [numeric](3, 0) NOT NULL,
	[TL3_DATE_TIME_FINISHED] [datetime] NULL,
	[TL3_STATUS] [numeric](3, 0) NOT NULL,
	[TL3_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
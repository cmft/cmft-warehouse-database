﻿CREATE TABLE [dbo].[F_AnaesTime](
	[ATI_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ATI_CD_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ATI_AN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ATI_NUMBER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ATI_TIME] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ATI_LOG_DATE] [datetime] NULL,
	[ATI_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ATI_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
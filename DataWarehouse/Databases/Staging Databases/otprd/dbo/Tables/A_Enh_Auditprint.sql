﻿CREATE TABLE [dbo].[A_Enh_Auditprint](
	[FEA_SEQU] [numeric](9, 0) NOT NULL,
	[FEA_SU_SEQU] [numeric](9, 0) NOT NULL,
	[FEA_US_SEQU] [numeric](9, 0) NOT NULL,
	[FEA_PRINT_TIME] [datetime] NOT NULL,
	[FEA_FCR_SEQU] [numeric](9, 0) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL,
	[FEA_OP_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
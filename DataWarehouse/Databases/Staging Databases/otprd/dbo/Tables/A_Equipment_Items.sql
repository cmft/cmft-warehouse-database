﻿CREATE TABLE [dbo].[A_Equipment_Items](
	[EQI_SEQU] [numeric](9, 0) NOT NULL,
	[EQI_CODE] [varchar](30) NULL,
	[EQI_SERIAL_NO] [varchar](30) NULL,
	[EQI_IL_SEQU] [numeric](9, 0) NOT NULL,
	[EQI_COMMENTS] [varchar](255) NULL,
	[EQI_PURCHASE_DATE] [datetime] NULL,
	[EQI_PURCHASE_COST] [numeric](9, 2) NOT NULL,
	[EQI_IN_SERVICE_DATE] [datetime] NULL,
	[EQI_MAINT_FREQ] [numeric](9, 0) NOT NULL,
	[EQI_MAINT_UNIT] [numeric](3, 0) NOT NULL,
	[EQI_PROJ_DISPOSAL_DATE] [datetime] NULL,
	[EQI_ACTUAL_DISPOSAL_DATE] [datetime] NULL,
	[EQI_DISPOSAL_COMMENTS] [varchar](255) NULL,
	[EQI_EQP_SEQU] [numeric](9, 0) NOT NULL,
	[EQI_TURNAROUND_TIME] [numeric](9, 0) NOT NULL,
	[EQI_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
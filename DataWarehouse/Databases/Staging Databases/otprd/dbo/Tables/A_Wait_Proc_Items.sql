﻿CREATE TABLE [dbo].[A_Wait_Proc_Items](
	[WP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[WP_WD_SEQU] [numeric](9, 0) NOT NULL,
	[WP_CD_SEQU] [numeric](9, 0) NOT NULL,
	[WP_PRIMARY_CD] [numeric](3, 0) NOT NULL,
	[WP_TIME] [numeric](9, 0) NOT NULL,
	[WP_TIME2] [numeric](9, 0) NOT NULL,
	[WP_TIME3] [numeric](9, 0) NOT NULL,
	[WP_EST_TIME] [numeric](9, 0) NOT NULL,
	[WP_LOG_DATE] [datetime] NULL,
	[WP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[WP_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_RecDrugLink](
	[RDL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[RDL_CODE] [varchar](5) NULL,
	[RDL_DESCRIPTION] [varchar](40) NULL,
	[RDL_DETAILS] [text] NULL,
	[RDL_OP_SEQU] [numeric](9, 0) NOT NULL,
	[RDL_LOG_DATE] [datetime] NULL,
	[RDL_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[RDL_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Rec_History](
	[RCH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RCH_REC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RCH_DATE_TIME] [datetime] NULL,
	[RCH_STATUS] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RCH_REASON_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RCH_DELAY_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
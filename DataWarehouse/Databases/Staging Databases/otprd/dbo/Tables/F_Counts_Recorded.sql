﻿CREATE TABLE [dbo].[F_Counts_Recorded](
	[FCR_SEQU] [numeric](9, 0) NOT NULL,
	[FCR_OP_SEQU] [numeric](9, 0) NULL,
	[FCR_OI_SEQU] [numeric](9, 0) NULL,
	[FCR_TEAM_NAME] [varchar](125) NULL,
	[FCR_SU_SEQU] [numeric](9, 0) NULL,
	[FCR_SU_NAME] [varchar](125) NULL,
	[FCR_DIH_SEQU] [numeric](9, 0) NULL,
	[FCR_COUNT_TYPES] [varchar](100) NULL,
	[FCR_INC_SU_SEQU] [numeric](9, 0) NULL,
	[FCR_INC_COMMENTS] [varchar](1000) NULL,
	[FCR_OPERATION_COUNT] [numeric](3, 0) NULL,
	[FCR_ECS_SEQU] [numeric](9, 0) NULL,
	[FCR_TCO_TEAM_NAME] [varchar](125) NULL,
	[FCR_TCO_COUNT_CORRECT] [numeric](3, 0) NULL,
	[FCR_TCO_COMMENTS] [varchar](1000) NULL,
	[FCR_TCO_SU_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Recovery_Location](
	[RCL_CODE] [varchar](5) NULL,
	[RCL_DESCRIPTION] [varchar](60) NULL,
	[RCL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RCL_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[RCL_UN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RCL_REC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RCL_CHART_LOCATION] [varchar](30) NULL,
	[RCL_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Recover__RCL_C__34DEB3C1]  DEFAULT ((0))
) ON [PRIMARY]
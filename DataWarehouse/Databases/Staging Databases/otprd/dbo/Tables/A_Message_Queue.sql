﻿CREATE TABLE [dbo].[A_Message_Queue](
	[MSQ_SEQU] [numeric](9, 0) NOT NULL,
	[MSQ_DATE_TIME] [datetime] NULL,
	[MSQ_INTERNAL_VISIT_SEQU] [numeric](9, 0) NOT NULL,
	[MSQ_PATIENT_SEQU] [numeric](9, 0) NOT NULL,
	[MSQ_MQH_NAME] [varchar](64) NULL,
	[MSQ_EVENT] [varchar](30) NULL,
	[MSQ_STATUS] [numeric](3, 0) NOT NULL,
	[MSQ_ERROR_MESSAGE] [varchar](256) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_ERROR_LOG](
	[EL_DATE_TIME] [datetime] NULL,
	[EL_FILLER_01] [numeric](3, 0) NOT NULL,
	[EL_CODE] [varchar](10) NULL,
	[EL_TEXT] [varchar](200) NULL,
	[EL_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Chart_Item_Data](
	[CID_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CID_CIH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[CID_DATETIME] [datetime] NULL,
	[CID_DATA] [numeric](11, 2) NOT NULL DEFAULT (0),
	[CID_NOTES] [varchar](255) NULL
) ON [PRIMARY]
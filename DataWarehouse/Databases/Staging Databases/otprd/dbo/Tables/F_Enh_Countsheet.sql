﻿CREATE TABLE [dbo].[F_Enh_Countsheet](
	[ECS_SEQU] [numeric](9, 0) NOT NULL,
	[ECS_ITEMS] [text] NULL,
	[ECS_PRE_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_COUNT1_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_COUNT2_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_COUNT3_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_FIRST_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_SECOND_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_FINAL_SU_SEQU] [numeric](9, 0) NULL,
	[ECS_OP_SEQU] [numeric](9, 0) NOT NULL,
	[ECS_PRE_REASON] [varchar](500) NULL,
	[ECS_COUNT1_REASON] [varchar](500) NULL,
	[ECS_COUNT2_REASON] [varchar](500) NULL,
	[ECS_COUNT3_REASON] [varchar](500) NULL,
	[ECS_FIRST_REASON] [varchar](500) NULL,
	[ECS_SECOND_REASON] [varchar](500) NULL,
	[ECS_FINAL_REASON] [varchar](500) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
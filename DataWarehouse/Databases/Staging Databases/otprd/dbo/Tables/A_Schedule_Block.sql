﻿CREATE TABLE [dbo].[A_Schedule_Block](
	[SB_SEQU] [numeric](9, 0) NOT NULL,
	[SB_SHD_SEQU] [numeric](9, 0) NOT NULL,
	[SB_START_TIME] [datetime] NULL,
	[SB_NO_OF_TIME_BLOCKS] [numeric](3, 0) NOT NULL,
	[SB_BLOCK_REASON] [varchar](40) NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
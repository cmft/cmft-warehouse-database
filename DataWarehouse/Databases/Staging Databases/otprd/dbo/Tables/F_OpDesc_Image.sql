﻿CREATE TABLE [dbo].[F_OpDesc_Image](
	[ODI_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ODI_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ODI_OP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ODI_OIM_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ODI_LOG_DATE] [datetime] NULL,
	[ODI_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ODI_EXT_NAME] [varchar](36) NULL,
	[ODI_EXT_IMAGE] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
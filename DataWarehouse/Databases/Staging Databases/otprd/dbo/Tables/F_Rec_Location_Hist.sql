﻿CREATE TABLE [dbo].[F_Rec_Location_Hist](
	[RLH_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RLH_RCL_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RLH_DATE_TIME_IN] [datetime] NULL,
	[RLH_DATE_TIME_OUT] [datetime] NULL,
	[RLH_REC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[RLH_UN_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
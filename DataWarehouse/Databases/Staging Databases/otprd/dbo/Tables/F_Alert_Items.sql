﻿CREATE TABLE [dbo].[F_Alert_Items](
	[ALI_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ALI_CODE] [varchar](10) NULL,
	[ALI_DESCRIPTION] [varchar](255) NULL,
	[ALI_EXT_DB] [varchar](10) NULL,
	[ALI_EXTRACT] [varchar](10) NULL,
	[ALI_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ALI_ALC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ALI_EPISODIC] [numeric](3, 0) NOT NULL DEFAULT (0)
) ON [PRIMARY]
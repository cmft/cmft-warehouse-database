﻿CREATE TABLE [dbo].[FS1SPEC](
	[S1_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[S1_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[S1_LOG_DATE] [datetime] NULL,
	[S1_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[S1_DISTRIB] [varchar](100) NULL,
	[S1_CODE] [varchar](8) NULL,
	[S1_DESC] [varchar](100) NULL,
	[S1_CODE_NO] [varchar](3) NULL,
	[S1_CHANGE_OVER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[S1_EMERG] [numeric](3, 0) NOT NULL DEFAULT (0),
	[S1_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[S1_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__FS1SPEC__S1_CA_S__3A978D17]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_Staff_Campus_Access](
	[SCA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SCA_SU_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SCA_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[SCA_UNIQUE] [varchar](20) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[A_Store_Forward](
	[SAF_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_PA_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_OP_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_REC_SEQU] [numeric](9, 0) NOT NULL,
	[SAF_DATE] [datetime] NULL,
	[SAF_EVENT] [varchar](30) NULL,
	[SAF_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL,
	[SAF_PATIENT_NAME] [varchar](140) NULL
) ON [PRIMARY]
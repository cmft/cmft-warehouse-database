﻿CREATE TABLE [dbo].[F_Body_Position](
	[BP_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[BP_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[BP_CODE] [varchar](5) NULL,
	[BP_DESC] [varchar](60) NULL,
	[BP_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[BP_LOG_DATE] [datetime] NULL,
	[BP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[BP_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Body_Po__BP_CA__043B7C66]  DEFAULT ((0))
) ON [PRIMARY]
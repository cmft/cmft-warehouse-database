﻿CREATE TABLE [dbo].[A_Indicator_Proc](
	[IP_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[IP_CODE] [varchar](5) NULL,
	[IP_DESCRIPTION] [varchar](170) NULL,
	[IP_SEQU] [numeric](9, 0) NOT NULL,
	[IP_LOG_DATE] [datetime] NULL,
	[IP_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[IP_INACTIVE] [numeric](3, 0) NOT NULL,
	[IP_EXTRACT_CODE] [varchar](5) NULL,
	[IP_CA_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
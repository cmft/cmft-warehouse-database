﻿CREATE TABLE [dbo].[F_Equip_Register](
	[EQL_SEQU] [numeric](9, 0) NOT NULL,
	[EQL_II_SEQU] [numeric](9, 0) NOT NULL,
	[EQL_DATE_LOANED] [datetime] NULL,
	[EQL_EXPECTED_RETURN] [datetime] NULL,
	[EQL_ACTUAL_RETURN] [datetime] NULL,
	[EQL_LOAN_TYPE] [numeric](3, 0) NOT NULL,
	[EQL_LOAN_TO_SEQU] [numeric](9, 0) NOT NULL,
	[EQL_LOAN_TO_NAME] [varchar](100) NULL,
	[EQL_RETURN_FLAG] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
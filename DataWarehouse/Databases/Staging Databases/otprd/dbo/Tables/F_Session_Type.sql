﻿CREATE TABLE [dbo].[F_Session_Type](
	[SET_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0)),
	[SET_CODE] [varchar](10) NULL,
	[SET_DESC] [varchar](60) NULL,
	[SET_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT ((0)),
	[SET_OP_TYPE] [numeric](3, 0) NOT NULL DEFAULT ((0)),
	[SET_CA_SEQU] [numeric](9, 0) NOT NULL DEFAULT ((0))
) ON [PRIMARY]
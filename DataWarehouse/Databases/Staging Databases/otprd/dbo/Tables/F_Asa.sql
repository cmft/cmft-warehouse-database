﻿CREATE TABLE [dbo].[F_Asa](
	[ASA_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ASA_CODE] [varchar](2) NULL,
	[ASA_DESCRIPTION] [varchar](255) NULL,
	[ASA_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ASA_LOG_DATE] [datetime] NULL,
	[ASA_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[ASA_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ASA_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Asa__ASA_CA_SE__015F0FBB]  DEFAULT ((0)),
	[ASA_START_DATE] [datetime] NULL,
	[ASA_END_DATE] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_ONLINE_HELP](
	[FOH_SEQU] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[FOH_NAME] [varchar](100) NULL,
	[FOH_TYPE] [varchar](10) NULL,
	[FOH_PATH_DETAILS] [varchar](300) NULL,
	[FOH_CHM_NAME] [varchar](50) NULL,
	[FOH_USER_TYPE] [numeric](3, 0) NULL
) ON [PRIMARY]
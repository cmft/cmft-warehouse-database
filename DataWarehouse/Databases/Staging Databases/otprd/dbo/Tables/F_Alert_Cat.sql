﻿CREATE TABLE [dbo].[F_Alert_Cat](
	[ALC_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[ALC_CODE] [varchar](10) NULL,
	[ALC_DESCRIPTION] [varchar](255) NULL,
	[ALC_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ALC_TYPE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[ALC_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Alert_C__ALC_C__526F16A8]  DEFAULT ((0))
) ON [PRIMARY]
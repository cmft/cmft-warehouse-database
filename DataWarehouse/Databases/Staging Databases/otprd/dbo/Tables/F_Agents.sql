﻿CREATE TABLE [dbo].[F_Agents](
	[AG_SEQN_FILLER] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AG_SEQU] [numeric](9, 0) NOT NULL DEFAULT (0),
	[AG_CODE] [varchar](5) NULL,
	[AG_DESC] [varchar](60) NULL,
	[AG_INACTIVE] [numeric](3, 0) NOT NULL DEFAULT (0),
	[AG_LOG_DATE] [datetime] NULL,
	[AG_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[AG_CA_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__F_Agents__AG_CA___7D8E7ED7]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[F_InvRetDisp](
	[IRD_SEQ_FILLER] [numeric](9, 0) NOT NULL,
	[IRD_CODE] [varchar](5) NULL,
	[IRD_DESC] [varchar](60) NULL,
	[IRD_SEQU] [numeric](9, 0) NOT NULL,
	[IRD_LOG_DATE] [datetime] NULL,
	[IRD_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[IRD_INACTIVE] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AOCOMPITEMS](
	[OC_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[OC_CODE] [varchar](5) NULL,
	[OC_CAM_SEQU] [numeric](9, 0) NOT NULL,
	[OC_OISEQ] [numeric](9, 0) NOT NULL,
	[OC_OI_SEQU] [numeric](9, 0) NOT NULL,
	[OC_LOG_DATE] [datetime] NULL,
	[OC_LOG_DETAILS_FILLER] [varchar](1) NULL,
	[OC_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL,
	[OC_REC_SEQU] [numeric](9, 0) NOT NULL CONSTRAINT [DF__AOCOMPITE__OC_RE__1B34BBAE]  DEFAULT ((0))
) ON [PRIMARY]
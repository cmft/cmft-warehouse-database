﻿CREATE TABLE [dbo].[A_StaffCategory_Hist](
	[SCH_SEQU] [numeric](9, 0) NOT NULL,
	[SCH_SU_SEQU] [numeric](9, 0) NOT NULL,
	[SCH_START_DATE] [datetime] NULL,
	[SCH_END_DATE] [datetime] NULL,
	[SCH_SC_SEQU] [numeric](9, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
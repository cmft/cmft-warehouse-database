﻿CREATE TABLE [dbo].[F_Problems_Link](
	[PRBL_SEQU] [numeric](9, 0) NOT NULL,
	[PRBL_PAD_SEQU] [numeric](9, 0) NOT NULL,
	[PRBL_PRB_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
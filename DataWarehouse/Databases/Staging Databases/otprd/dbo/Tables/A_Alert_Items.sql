﻿CREATE TABLE [dbo].[A_Alert_Items](
	[ALI_SEQU] [numeric](9, 0) NOT NULL,
	[ALI_CODE] [varchar](10) NULL,
	[ALI_DESCRIPTION] [varchar](255) NULL,
	[ALI_EXT_DB] [varchar](10) NULL,
	[ALI_EXTRACT] [varchar](10) NULL,
	[ALI_INACTIVE] [numeric](3, 0) NOT NULL,
	[ALI_ALC_SEQU] [numeric](9, 0) NOT NULL,
	[ALI_EPISODIC] [numeric](3, 0) NOT NULL,
	[SID_SPID] [numeric](18, 0) NULL,
	[D_ACTION] [varchar](1) NULL,
	[COUNTER] [bigint] IDENTITY(1,1) NOT NULL,
	[INSERT_DATE_TIME] [datetime] NULL,
	[MACHINENAME] [varchar](50) NULL,
	[PROGRAM_NAME] [varchar](50) NULL,
	[NT_DOMAIN] [varchar](50) NULL,
	[NT_USERNAME] [varchar](50) NULL
) ON [PRIMARY]
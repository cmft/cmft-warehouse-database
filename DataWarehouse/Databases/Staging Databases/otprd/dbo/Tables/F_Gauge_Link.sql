﻿CREATE TABLE [dbo].[F_Gauge_Link](
	[GAL_SEQN_FILLER] [numeric](9, 0) NOT NULL,
	[GAL_PAD_SEQU] [numeric](9, 0) NOT NULL,
	[GAL_GA_SEQU] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
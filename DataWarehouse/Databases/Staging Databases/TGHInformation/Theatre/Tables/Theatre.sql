﻿CREATE TABLE [Theatre].[Theatre](
	[TheatreCode] [int] NOT NULL,
	[Theatre] [varchar](255) NULL,
	[OperatingSuiteCode] [int] NOT NULL,
	[TheatreCode1] [varchar](8) NULL,
	[InactiveFlag] [bit] NOT NULL,
	[InterfaceCode] [varchar](10) NULL
) ON [PRIMARY]
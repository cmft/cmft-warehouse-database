﻿CREATE TABLE [Theatre].[AdmissionType](
	[AdmissionTypeCode] [int] NOT NULL,
	[AdmissionTypeCode1] [varchar](5) NULL,
	[AdmissionType] [varchar](255) NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
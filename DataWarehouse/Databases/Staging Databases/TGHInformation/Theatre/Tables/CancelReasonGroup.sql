﻿CREATE TABLE [Theatre].[CancelReasonGroup](
	[CancelReasonGroupCode] [int] NOT NULL,
	[CancelReasonGroup] [varchar](255) NULL,
	[CancelReasonGroupCode1] [varchar](5) NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [Theatre].[CancelReason](
	[CancelReasonCode] [int] NOT NULL,
	[CancelReason] [varchar](255) NULL,
	[CancelReasonCode1] [varchar](5) NULL,
	[CancelReasonGroupCode] [int] NOT NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
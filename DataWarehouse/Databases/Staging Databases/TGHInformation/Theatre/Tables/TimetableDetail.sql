﻿CREATE TABLE [Theatre].[TimetableDetail](
	[TimetableDetailCode] [int] NOT NULL,
	[SessionNumber] [tinyint] NOT NULL,
	[DayNumber] [tinyint] NOT NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[TheatreCode] [int] NULL,
	[SessionStartTime] [datetime] NULL,
	[SessionEndTime] [datetime] NULL,
	[ConsultantCode] [varchar](10) NULL,
	[AnaesthetistCode] [varchar](10) NULL,
	[SpecialtyCode] [varchar](10) NULL,
	[TimetableTemplateCode] [int] NULL,
	[LogLastUpdated] [datetime] NULL,
	[RecordLogDetails] [varchar](255) NULL,
	[SessionMinutes] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Theatre].[Staff](
	[StaffCode] [int] NOT NULL,
	[Surname] [varchar](60) NULL,
	[Forename] [varchar](60) NULL,
	[Initial] [varchar](3) NULL,
	[StaffCode1] [varchar](10) NULL,
	[StaffCategoryCode] [int] NULL,
	[SpecialtyCode] [int] NULL
) ON [PRIMARY]
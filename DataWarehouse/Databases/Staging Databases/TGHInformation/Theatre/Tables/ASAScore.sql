﻿CREATE TABLE [Theatre].[ASAScore](
	[ASAScoreCode] [int] NOT NULL,
	[ASAScore] [varchar](255) NULL,
	[ASAScoreCode1] [varchar](2) NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
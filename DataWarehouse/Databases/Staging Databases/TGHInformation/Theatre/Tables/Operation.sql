﻿CREATE TABLE [Theatre].[Operation](
	[OperationCode] [int] NOT NULL,
	[Operation] [varchar](255) NULL,
	[OperationCode1] [varchar](8) NULL,
	[InactiveFlag] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TraffordIPActivity](
	[ProviderSpellNo] [varchar](50) NULL,
	[NationalSpecialtyCode] [varchar](50) NULL,
	[spec_desc] [varchar](251) NULL,
	[AdmissionTime] [smalldatetime] NULL,
	[DischargeTime] [smalldatetime] NULL,
	[SourceAdministrativeCategoryCode] [varchar](100) NULL,
	[NationalAdmissionMethodCode] [varchar](50) NULL,
	[NationalAdmissionMethod] [varchar](200) NULL,
	[TYPE] [varchar](14) NOT NULL,
	[TYPE2] [varchar](13) NOT NULL,
	[SourceWardCode] [varchar](100) NULL,
	[LastWeeksCases] [int] NOT NULL,
	[NationalAdministrativeCategoryCode] [varchar](50) NULL,
	[NationalAdministrativeCategory] [varchar](200) NULL
) ON [PRIMARY]
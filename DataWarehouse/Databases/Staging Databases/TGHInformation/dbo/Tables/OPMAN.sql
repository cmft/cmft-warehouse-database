﻿CREATE TABLE [dbo].[OPMAN](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Appointmentdate] [smalldatetime] NOT NULL,
	[TreatmentFunctionCode] [varchar](10) NOT NULL,
	[ConsultantCode] [varchar](50) NOT NULL,
	[FirstAttendanceCode] [varchar](10) NOT NULL,
	[DNACode] [varchar](10) NOT NULL,
	[Activity] [int] NOT NULL,
	[Comment] [nvarchar](25) NULL
) ON [PRIMARY]
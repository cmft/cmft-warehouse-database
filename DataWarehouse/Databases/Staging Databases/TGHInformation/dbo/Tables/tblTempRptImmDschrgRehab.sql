﻿CREATE TABLE [dbo].[tblTempRptImmDschrgRehab](
	[LoadDate] [datetime] NOT NULL,
	[PatientNoId] [int] NOT NULL,
	[PatientNo] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[EPMINumber] [varchar](20) NULL,
	[AdmissionNumber] [varchar](15) NULL,
	[MatchingLevel] [int] NULL,
	[Title] [varchar](max) NULL,
	[Forenames] [varchar](max) NULL,
	[Surname] [varchar](max) NULL,
	[AdmissionDate] [datetime] NULL,
	[DischargeDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
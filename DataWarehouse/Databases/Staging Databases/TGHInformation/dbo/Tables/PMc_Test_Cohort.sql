﻿CREATE TABLE [dbo].[PMc_Test_Cohort](
	[NationalNumber1] [nvarchar](40) NULL,
	[FamilyName] [nvarchar](80) NOT NULL,
	[GivenName] [nvarchar](80) NULL,
	[BirthDate] [datetime] NULL,
	[sex] [nchar](1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CancelledOps](
	[CODate] [smalldatetime] NOT NULL,
	[COConsultant] [varchar](50) NOT NULL,
	[COSpecialty] [varchar](10) NOT NULL,
	[COPatientID] [varchar](50) NOT NULL,
	[COReason] [int] NOT NULL,
	[CONewTCIDateNeeded] [varchar](5) NULL,
	[CONewTCIDate] [smalldatetime] NULL,
	[COPatientsChoice] [varchar](5) NULL,
	[COComment] [varchar](50) NULL
) ON [PRIMARY]
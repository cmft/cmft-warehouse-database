﻿CREATE TABLE [dbo].[TraffordOPWL](
	[Districtno] [varchar](50) NULL,
	[Hospital] [int] NULL,
	[InternalNo] [varchar](50) NOT NULL,
	[EpisodeNo] [int] NULL,
	[ApptDate] [datetime] NULL,
	[Doctor] [int] NULL,
	[Clinic] [int] NULL,
	[ClinicSpecialty] [int] NULL,
	[CancelBy] [int] NULL,
	[ApptType] [int] NULL,
	[refsource] [int] NULL,
	[REF_CONS] [int] NULL,
	[REF_SPEC] [varchar](10) NULL,
	[spec_desc] [varchar](211) NULL,
	[Consul] [int] NULL,
	[Derived_Appoint_Type] [int] NULL,
	[Division] [int] NULL,
	[primproc] [int] NULL,
	[N_F] [varchar](10) NULL,
	[diag] [int] NULL,
	[WeekNo] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[Enddate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AETempRecord](
	[SourceUniqueID] [int] NOT NULL,
	[DistrictNo] [varchar](30) NULL,
	[EPMINo] [varchar](30) NULL,
	[NHSNumber] [varchar](8000) NULL,
	[AttendanceNumber] [varchar](20) NOT NULL,
	[ArrivalDate] [datetime] NULL,
	[ArrivalTime] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REFMAN](
	[OriginalRTTStartDate] [smalldatetime] NULL,
	[MainSpecialtyCode] [varchar](3) NULL,
	[ConsultantCode] [varchar](14) NULL,
	[ReferralType] [varchar](30) NULL,
	[Activity] [int] NULL,
	[Comment] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Tariff](
	[HRGcode] [nvarchar](255) NULL,
	[PlannedSameDayTariff] [float] NULL,
	[ELECTIVE 1 DAY] [float] NULL,
	[ELECTIVE 2 DAYSorMore] [float] NULL,
	[ElectiveLongStayTrimpoint] [float] NULL,
	[NON ELECTIVE 1DAY] [float] NULL,
	[NON ELECTIVE2DAYSorMore] [float] NULL,
	[NonelectiveLongStayTrimpoint] [float] NULL,
	[Perdaylongstaypayment] [float] NULL,
	[ShortStayElectivetariffapplicable] [nvarchar](255) NULL,
	[ShortStayElectiveTariff] [float] NULL,
	[Reducedshortstayemergencytariff _applicable?] [nvarchar](255) NULL,
	[ReducedshortstayemergencytariffPerce] [float] NULL,
	[Reducedshortstayemergencytariff] [float] NULL,
	[EligibleforSpecialisedTopup] [nvarchar](255) NULL
) ON [PRIMARY]
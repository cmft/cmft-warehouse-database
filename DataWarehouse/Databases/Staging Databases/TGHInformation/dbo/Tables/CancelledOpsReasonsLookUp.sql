﻿CREATE TABLE [dbo].[CancelledOpsReasonsLookUp](
	[COReasonCode] [int] NOT NULL,
	[COReasonDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
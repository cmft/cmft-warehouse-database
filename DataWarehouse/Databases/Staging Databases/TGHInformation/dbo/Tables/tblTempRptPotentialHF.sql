﻿CREATE TABLE [dbo].[tblTempRptPotentialHF](
	[PatientNoId] [int] NOT NULL,
	[PatientNo] [varchar](max) NULL,
	[NHSNumber] [varchar](max) NULL,
	[Title] [varchar](max) NULL,
	[Forenames] [varchar](max) NULL,
	[Surname] [varchar](max) NULL,
	[DateOfBirth] [varchar](max) NULL,
	[EFResult] [varchar](9) NULL,
	[EjectionFraction] [varchar](max) NULL,
	[ReportDate] [varchar](max) NULL,
	[Comments] [varchar](max) NULL,
	[EPMINumber] [varchar](20) NULL,
	[p_Location] [varchar](15) NULL,
	[ProviderSpellNo] [varchar](12) NULL,
	[AdmissionDate] [varchar](10) NULL,
	[DischargeDate] [varchar](8) NULL,
	[EndWardTypeCode] [varchar](15) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
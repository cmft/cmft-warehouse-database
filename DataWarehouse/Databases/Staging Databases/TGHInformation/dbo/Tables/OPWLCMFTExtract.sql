﻿CREATE TABLE [dbo].[OPWLCMFTExtract](
	[Hospital] [int] NULL,
	[InternalNo] [varchar](50) NOT NULL,
	[EpisodeNo] [int] NULL,
	[ApptDate] [smalldatetime] NULL,
	[Doctor] [int] NULL,
	[Clinic] [int] NULL,
	[ClinicSpecialty] [int] NULL,
	[CancelBy] [int] NULL,
	[ApptType] [int] NULL,
	[refsource] [int] NULL,
	[REF_CONS] [int] NULL,
	[REF_SPEC] [varchar](10) NULL,
	[spec_desc] [varchar](91) NULL,
	[Consul] [int] NULL,
	[Derived_Appoint_Type] [int] NULL,
	[Division] [varchar](10) NULL,
	[primproc] [int] NULL,
	[N_F] [varchar](10) NULL,
	[diag] [int] NULL
) ON [PRIMARY]
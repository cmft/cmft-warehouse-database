﻿CREATE TABLE [dbo].[EthnicUpdates](
	[EPMIno] [varchar](30) NOT NULL,
	[DistrictNo] [nvarchar](50) NULL,
	[EthnicType] [varchar](80) NOT NULL,
	[ETHORG_CODE] [varchar](8) NULL,
	[Result] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
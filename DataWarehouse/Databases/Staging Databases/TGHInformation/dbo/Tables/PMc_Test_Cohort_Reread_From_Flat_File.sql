﻿CREATE TABLE [dbo].[PMc_Test_Cohort_Reread_From_Flat_File](
	[NHSNumber] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[Forename] [nvarchar](255) NULL,
	[DateOfBirth] [datetime] NULL,
	[SexCode] [nvarchar](10) NULL
) ON [PRIMARY]
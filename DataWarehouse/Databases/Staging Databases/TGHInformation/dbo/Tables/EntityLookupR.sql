﻿CREATE TABLE [dbo].[EntityLookupR](
	[EntityTypeCode] [varchar](50) NOT NULL,
	[EntityCode] [varchar](50) NOT NULL,
	[Description] [varchar](255) NOT NULL
) ON [PRIMARY]
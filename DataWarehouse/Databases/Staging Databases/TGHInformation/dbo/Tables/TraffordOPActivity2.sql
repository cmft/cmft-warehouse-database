﻿CREATE TABLE [dbo].[TraffordOPActivity2](
	[op_act_hosp] [varchar](50) NULL,
	[op_act_int_no] [varchar](50) NULL,
	[ApptType] [varchar](900) NOT NULL,
	[ApptDate] [datetime] NULL,
	[AttendStat] [varchar](3) NOT NULL,
	[InternalNo] [int] NULL,
	[RefSource] [int] NULL,
	[Consultant] [varchar](max) NULL,
	[Hospital] [int] NULL,
	[Specialty] [varchar](50) NULL,
	[Div] [int] NULL,
	[N_F] [varchar](1) NULL,
	[spec_desc] [varchar](251) NULL,
	[diag] [int] NULL,
	[week_no] [int] NULL,
	[PrimProc] [int] NULL,
	[can_by] [int] NULL,
	[can_date] [int] NULL,
	[category] [int] NULL,
	[dir] [int] NULL,
	[wk] [int] NULL,
	[appdate] [smalldatetime] NULL,
	[Duplicate] [varchar](17) NOT NULL,
	[HRGCode] [varchar](10) NULL,
	[DerivedNF] [varchar](1) NULL,
	[LastWeeksCases] [int] NOT NULL,
	[SourceAdministrativeCategoryCode] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [Theo].[SYS_UserDefinedFilters](
	[UserDefinedFilterID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[SQLWhereClause] [nvarchar](max) NULL,
	[FilterName] [nvarchar](50) NULL,
	[DefaultFilter] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
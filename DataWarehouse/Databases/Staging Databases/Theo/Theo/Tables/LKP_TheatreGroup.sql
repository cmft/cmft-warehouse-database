﻿CREATE TABLE [Theo].[LKP_TheatreGroup](
	[TheatreGroupID] [int] IDENTITY(1,1) NOT NULL,
	[TheatreGroupCaption] [nvarchar](50) NOT NULL,
	[ColourR] [int] NULL,
	[ColourG] [int] NULL,
	[ColourB] [int] NULL
) ON [PRIMARY]
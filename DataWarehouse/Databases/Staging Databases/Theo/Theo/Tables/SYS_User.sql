﻿CREATE TABLE [Theo].[SYS_User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[ADLogin] [nvarchar](50) NULL,
	[UserTypeID] [int] NULL,
	[ConsultantID] [int] NULL,
	[MyRed] [int] NULL,
	[MyGreen] [int] NULL,
	[MyBlue] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [Theo].[LKP_Consultant](
	[ConsultantID] [int] IDENTITY(1,1) NOT NULL,
	[Surname] [nvarchar](50) NULL,
	[Forename] [nvarchar](50) NULL,
	[Title] [nvarchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Theo].[LKP_Theatre](
	[TheatreCode] [nvarchar](10) NOT NULL,
	[TheatreCaption] [nvarchar](75) NULL,
	[ColourR] [int] NULL,
	[ColourG] [int] NULL,
	[ColourB] [int] NULL,
	[ShowInLists] [int] NULL CONSTRAINT [DF_LKP_Theatre_ShowInLists]  DEFAULT ((1)),
	[DirectorateCodes] [nvarchar](100) NULL,
	[TheatreGroup] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [Theo].[LKP_Period](
	[PeriodID] [int] IDENTITY(1,1) NOT NULL,
	[PeriodDescription] [nvarchar](10) NULL,
	[Ordering] [int] NULL
) ON [PRIMARY]
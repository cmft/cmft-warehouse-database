﻿CREATE TABLE [Theo].[LKP_ConsultantMapping](
	[SourceConsultantCode] [nvarchar](25) NOT NULL,
	[SourceContextCode] [nvarchar](25) NOT NULL,
	[TheoConsultantID] [int] NULL
) ON [PRIMARY]
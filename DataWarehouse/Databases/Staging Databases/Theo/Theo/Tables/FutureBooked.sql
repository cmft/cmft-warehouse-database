﻿CREATE TABLE [Theo].[FutureBooked](
	[TheatreCode] [varchar](8) NULL,
	[PeriodID] [int] NULL,
	[OperationDate] [date] NULL,
	[EstimatedStartTime] [datetime] NULL,
	[Casenote] [varchar](16) NULL,
	[PatientFullName] [varchar](122) NULL,
	[PatientDOB] [datetime] NULL,
	[PatientAge] [varchar](27) NULL,
	[OperationComment] [varchar](1000) NULL,
	[SurgeonComment] [text] NULL,
	[Specialty] [varchar](255) NULL,
	[Consultant1] [varchar](10) NULL,
	[Consultant2] [varchar](10) NULL,
	[IntendedSurgeon1] [varchar](10) NULL,
	[IntendedSurgeon2] [varchar](10) NULL,
	[Anaesthetist1] [varchar](10) NULL,
	[Anaesthetist2] [varchar](10) NULL,
	[SessionIdentifier] [numeric](9, 0) NOT NULL,
	[OperationDuration] [numeric](9, 0) NOT NULL,
	[Aneasthetic] [varchar](3) NULL,
	[ListOrder] [int] NULL,
	[SessionDuration] [int] NULL,
	[FeedAreaCode] [nvarchar](50) NULL,
	[ConsultantID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
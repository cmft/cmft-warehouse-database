﻿CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varbinary](128) NULL,
	[UserType] [xml] NULL,
	[SuperUser] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[TARNCandidateLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CandidateID] [int] NOT NULL,
	[Details] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
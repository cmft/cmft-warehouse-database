﻿CREATE TABLE [dbo].[OpsProcedure](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProcedureTypeID] [int] NOT NULL,
	[ProcedureName] [varchar](150) NOT NULL
) ON [PRIMARY]
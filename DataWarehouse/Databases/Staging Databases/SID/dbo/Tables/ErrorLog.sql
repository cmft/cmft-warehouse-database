﻿CREATE TABLE [dbo].[ErrorLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[Message] [varchar](500) NOT NULL,
	[Stack] [varchar](4000) NOT NULL,
	[Page] [varchar](250) NOT NULL,
	[Method] [varchar](150) NOT NULL,
	[InnerMessage] [varchar](500) NULL
) ON [PRIMARY]
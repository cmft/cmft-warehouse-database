﻿CREATE TABLE [dbo].[TypeOfPresentation](
	[TypeOfPresentationID] [int] NOT NULL,
	[TypeOfPresentationDesc] [varchar](50) NOT NULL,
	[SystemID] [int] NULL
) ON [PRIMARY]
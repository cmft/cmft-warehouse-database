﻿CREATE TABLE [dbo].[TARNCandidateData](
	[ID] [int] IDENTITY(50000,1) NOT NULL,
	[CandidateID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
	[ActionID] [int] NOT NULL,
	[Details] [xml] NULL,
	[DateTimeCreated] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
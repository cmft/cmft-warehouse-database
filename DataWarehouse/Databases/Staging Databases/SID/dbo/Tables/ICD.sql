﻿CREATE TABLE [dbo].[ICD](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TarnID] [int] NOT NULL,
	[Description] [nvarchar](150) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NULL
) ON [PRIMARY]
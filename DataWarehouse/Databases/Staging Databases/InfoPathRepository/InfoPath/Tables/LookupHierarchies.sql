﻿CREATE TABLE [InfoPath].[LookupHierarchies](
	[ParentLookupID] [int] NOT NULL,
	[ChildLookupID] [int] NOT NULL
) ON [PRIMARY]
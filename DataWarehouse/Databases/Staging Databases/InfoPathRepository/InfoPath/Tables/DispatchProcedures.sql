﻿CREATE TABLE [InfoPath].[DispatchProcedures](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[formTypeID] [int] NOT NULL,
	[dispatchProcedure] [sysname] NOT NULL,
	[dispatchType] [int] NOT NULL
) ON [PRIMARY]
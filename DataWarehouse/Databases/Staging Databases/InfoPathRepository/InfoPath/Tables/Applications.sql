﻿CREATE TABLE [InfoPath].[Applications](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationName] [nvarchar](256) NOT NULL
) ON [PRIMARY]
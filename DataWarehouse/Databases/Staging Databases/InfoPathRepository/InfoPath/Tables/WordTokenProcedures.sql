﻿CREATE TABLE [InfoPath].[WordTokenProcedures](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[formTypeID] [int] NOT NULL,
	[DocumentTypeCode] [varchar](10) NOT NULL,
	[GetWordTokenProcedure] [sysname] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [InfoPath].[XMLRequestDefinitions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[request] [nvarchar](128) NOT NULL,
	[dispatchProcedure] [nvarchar](128) NOT NULL,
	[exampleRequest] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
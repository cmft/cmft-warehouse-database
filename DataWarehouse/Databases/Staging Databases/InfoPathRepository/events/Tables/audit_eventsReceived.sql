﻿CREATE TABLE [events].[audit_eventsReceived](
	[objectName] [sysname] NOT NULL,
	[eventName] [sysname] NOT NULL,
	[eventData] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
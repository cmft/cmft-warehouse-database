﻿CREATE TABLE [events].[EventsReceived](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[timeOfEvent] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[objectName] [varchar](96) NULL,
	[eventName] [varchar](96) NULL,
	[eventData] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
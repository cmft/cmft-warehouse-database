﻿CREATE TABLE [Questionnaire].[Forms](
	[id] [int] NOT NULL,
	[FormTypeID] [int] NOT NULL,
	[createdAt] [datetime2](7) NOT NULL DEFAULT (getutcdate()),
	[lastUpdated] [datetime2](7) NULL,
	[isComplete] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
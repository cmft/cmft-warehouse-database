﻿CREATE TABLE [dbo].[MDT_procedures](
	[Procedure] [nvarchar](255) NULL,
	[Code] [nvarchar](255) NULL,
	[procedureInternalID] [int] IDENTITY(1,1) NOT NULL,
	[displayOrder] [int] NULL
) ON [PRIMARY]
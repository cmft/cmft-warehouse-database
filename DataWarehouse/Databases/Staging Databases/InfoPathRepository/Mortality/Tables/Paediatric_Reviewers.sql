﻿CREATE TABLE [Mortality].[Paediatric_Reviewers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DomainLogin] [sysname] NOT NULL,
	[Name] [sysname] NOT NULL,
	[isActive] [bit] NULL DEFAULT ((1)),
	[emailTo] [varchar](255) NULL,
	[emailCC] [varchar](255) NULL,
	[emailBCC] [varchar](255) NULL,
	[emailOverrideRedirect] [varchar](255) NULL
) ON [PRIMARY]
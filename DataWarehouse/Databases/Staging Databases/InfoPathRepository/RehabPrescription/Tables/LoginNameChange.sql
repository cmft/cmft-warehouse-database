﻿CREATE TABLE [RehabPrescription].[LoginNameChange](
	[oldLogin] [varchar](255) NOT NULL,
	[newLogin] [varchar](255) NOT NULL
) ON [PRIMARY]
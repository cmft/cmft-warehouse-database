﻿CREATE TABLE [Audit].[XmlRequests](
	[id] [int] IDENTITY(1000000,1) NOT NULL,
	[datetimeOfCallLocal] [datetime2](7) NULL DEFAULT (sysdatetime()),
	[datetimeOfCallUTC] [datetime2](7) NULL DEFAULT (sysutcdatetime()),
	[request] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
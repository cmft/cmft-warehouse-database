﻿CREATE TABLE [Audit].[MessagesProcessed](
	[dialog] [uniqueidentifier] NULL,
	[msg] [nvarchar](15) NULL,
	[msgName] [sysname] NULL,
	[LoggedAt] [datetime2](7) NOT NULL CONSTRAINT [DF__MessagesP__Logge__1DE57479]  DEFAULT (sysdatetime())
) ON [PRIMARY]
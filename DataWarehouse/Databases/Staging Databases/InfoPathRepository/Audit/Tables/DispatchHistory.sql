﻿CREATE TABLE [Audit].[DispatchHistory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ts] [datetime2](7) NOT NULL DEFAULT (sysdatetime()),
	[infoPathID] [int] NOT NULL,
	[infoPathFormXML] [xml] NOT NULL,
	[dispatchType] [int] NOT NULL,
	[dispatchedToProcedure] [sysname] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
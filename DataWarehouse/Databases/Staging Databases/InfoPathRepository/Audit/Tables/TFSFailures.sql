﻿CREATE TABLE [Audit].[TFSFailures](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[InfoPathID] [int] NOT NULL,
	[eventTime] [datetime2](7) NOT NULL DEFAULT (sysdatetime())
) ON [PRIMARY]
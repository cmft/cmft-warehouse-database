﻿CREATE TABLE [Forms].[ViewNames](
	[FormTypeID] [int] NOT NULL,
	[FormTypeViewName] [varchar](128) NOT NULL
) ON [PRIMARY]
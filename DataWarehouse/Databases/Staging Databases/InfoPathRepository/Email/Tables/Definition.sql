﻿CREATE TABLE [Email].[Definition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[shortName] [cmft].[name] NOT NULL,
	[description] [cmft].[description] NOT NULL,
	[applicationID] [int] NOT NULL,
	[subjectXSL] [xml] NOT NULL,
	[bodyXSL] [xml] NOT NULL,
	[emailBodyType] [char](1) NOT NULL,
	[fromAddressOverride] [varchar](max) NULL,
	[replyToAddressOverride] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[improvement](
	[improID] [int] IDENTITY(1,1) NOT NULL,
	[ino] [int] NULL,
	[issue] [text] NULL,
	[issue_exist] [text] NULL,
	[vincent_factor] [int] NULL,
	[v1] [int] NULL,
	[v2] [int] NULL,
	[v3] [int] NULL,
	[severity] [int] NULL,
	[iaction] [text] NULL,
	[timescales] [text] NULL,
	[lead] [varchar](255) NULL,
	[seniorID] [int] NULL,
	[update_action] [text] NULL,
	[update_date] [datetime] NULL,
	[parent_improID] [int] NULL CONSTRAINT [DF_improvement_parent_improID]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
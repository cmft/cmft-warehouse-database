﻿CREATE TABLE [dbo].[coordinator](
	[coordinatorID] [int] IDENTITY(1,1) NOT NULL,
	[coordinatorName] [varchar](255) NULL,
	[coordinatorEmail] [varchar](255) NULL,
	[type] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[schedule](
	[scheduleID] [int] IDENTITY(1,1) NOT NULL,
	[leaderID] [int] NULL,
	[sch_clrID] [int] NULL,
	[sch_month] [int] NULL,
	[sch_year] [int] NULL,
	[sch_date] [datetime] NULL,
	[details] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
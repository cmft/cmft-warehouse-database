﻿CREATE TABLE [dbo].[sch_clr](
	[sch_clrID] [int] IDENTITY(1,1) NOT NULL,
	[detail] [varchar](255) NULL,
	[colour_code] [nchar](10) NULL
) ON [PRIMARY]
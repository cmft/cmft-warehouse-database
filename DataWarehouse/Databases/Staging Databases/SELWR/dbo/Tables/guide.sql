﻿CREATE TABLE [dbo].[guide](
	[guideID] [int] IDENTITY(1,1) NOT NULL,
	[guide_name] [varchar](255) NULL,
	[guide_email] [varchar](255) NULL
) ON [PRIMARY]
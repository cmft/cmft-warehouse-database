﻿CREATE TABLE [dbo].[v3](
	[v3ID] [int] IDENTITY(1,1) NOT NULL,
	[v3_name] [varchar](255) NULL,
	[v1ID] [int] NULL,
	[v2ID] [int] NULL,
	[v3_value] [int] NULL
) ON [PRIMARY]
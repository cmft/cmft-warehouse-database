﻿CREATE TABLE [dbo].[answers](
	[seniorID] [int] NOT NULL,
	[questionID] [int] NOT NULL,
	[answer_given] [nchar](10) NULL,
	[notes] [text] NULL,
	[sectionID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
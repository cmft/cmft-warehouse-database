﻿CREATE TABLE [dbo].[impro_act](
	[impro_actID] [int] IDENTITY(1,1) NOT NULL,
	[improID] [int] NULL,
	[seniorID] [int] NULL,
	[iaction] [text] NULL,
	[timescales] [text] NULL,
	[lead] [varchar](255) NULL,
	[update_action] [text] NULL,
	[update_date] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
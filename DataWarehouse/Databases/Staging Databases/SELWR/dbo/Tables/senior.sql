﻿CREATE TABLE [dbo].[senior](
	[seniorID] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NULL,
	[ward] [int] NULL,
	[leaderID] [int] NULL,
	[guide] [int] NULL,
	[coordinator] [int] NULL,
	[date_inputID] [int] NULL,
	[start_period] [varchar](50) NULL,
	[end_period] [varchar](50) NULL,
	[pa] [int] NULL
) ON [PRIMARY]
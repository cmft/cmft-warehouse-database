﻿CREATE TABLE [dbo].[v2](
	[v2ID] [int] IDENTITY(1,1) NOT NULL,
	[v1ID] [int] NULL,
	[v2_name] [varchar](255) NULL,
	[v2_value] [int] NULL
) ON [PRIMARY]
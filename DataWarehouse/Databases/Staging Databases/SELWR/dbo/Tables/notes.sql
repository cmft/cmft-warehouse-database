﻿CREATE TABLE [dbo].[notes](
	[noteID] [int] IDENTITY(1,1) NOT NULL,
	[seniorID] [int] NULL,
	[note] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
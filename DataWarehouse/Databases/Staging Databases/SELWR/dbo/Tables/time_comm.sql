﻿CREATE TABLE [dbo].[time_comm](
	[timeID] [int] IDENTITY(1,1) NOT NULL,
	[seniorID] [int] NOT NULL,
	[start_hour] [nchar](10) NULL,
	[start_minute] [nchar](10) NULL
) ON [PRIMARY]
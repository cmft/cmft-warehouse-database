﻿CREATE TABLE [dbo].[leader](
	[leaderID] [int] IDENTITY(1,1) NOT NULL,
	[leader_name] [nvarchar](255) NULL,
	[guideID] [int] NULL,
	[leader_email] [varchar](255) NULL,
	[leader_order] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[questions](
	[questionID] [int] IDENTITY(1,1) NOT NULL,
	[question] [ntext] NULL,
	[sectionID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
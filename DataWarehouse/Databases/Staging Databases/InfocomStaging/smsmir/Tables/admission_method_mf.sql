﻿CREATE TABLE [smsmir].[admission_method_mf](
	[Code] [varchar](8) NOT NULL,
	[CodeDesc] [varchar](32) NULL,
	[IntValEng] [varchar](5) NULL,
	[ImpCode] [varchar](4) NOT NULL,
	[LglDelInd] [varchar](1) NULL,
	[NatCode] [varchar](2) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[tblIncludedDiagnoses](
	[Diagnosis] [varchar](6) NULL,
	[DiagnosisWithoutDot] [varchar](10) NULL
) ON [PRIMARY]
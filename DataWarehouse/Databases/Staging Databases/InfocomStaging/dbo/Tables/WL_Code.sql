﻿CREATE TABLE [dbo].[WL_Code](
	[ImpCode] [varchar](4) NOT NULL,
	[Hospital] [varchar](4) NULL,
	[WLCode] [varchar](6) NOT NULL,
	[WLDesc] [varchar](30) NULL,
	[ProvCode] [varchar](4) NULL,
	[ConType] [varchar](3) NULL,
	[MainConsultant] [varchar](6) NULL,
	[ListNumber] [int] NULL,
	[ListSpecialty] [varchar](6) NULL,
	[WLMaxAdmTimeOrd] [int] NULL,
	[WLMaxAdmTimeDay] [int] NULL,
	[ElectAdmListNo] [varchar](3) NULL,
	[Consultant1] [varchar](6) NULL,
	[Consultant2] [varchar](6) NULL,
	[Consultant3] [varchar](6) NULL,
	[LglDelInd] [char](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RTT_Pathway](
	[ImpCode] [varchar](4) NOT NULL,
	[PathwayNo] [varchar](25) NOT NULL,
	[InternalNo] [varchar](9) NULL,
	[StartDate] [datetime2](7) NULL,
	[EndDate] [datetime2](7) NULL,
	[Condition] [varchar](20) NULL,
	[CurrentStatus] [varchar](4) NULL,
	[CurrentStatusDt] [datetime2](7) NULL,
	[SpecCode] [varchar](8) NULL,
	[ConsCode] [varchar](8) NULL,
	[Provider] [varchar](10) NULL,
	[OSV] [varchar](1) NULL,
	[Private] [varchar](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Merge](
	[ImpCode] [varchar](4) NOT NULL,
	[Hospital] [varchar](4) NOT NULL,
	[InternalNo] [varchar](9) NOT NULL,
	[EpisodeNo] [varchar](13) NOT NULL,
	[MergeInternalNo] [varchar](9) NOT NULL,
	[MergeEpisodeNo] [varchar](13) NOT NULL
) ON [PRIMARY]
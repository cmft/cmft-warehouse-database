﻿CREATE TABLE [dbo].[Tracking_AE](
	[ImpCode] [varchar](4) NOT NULL,
	[Hospital] [varchar](4) NOT NULL,
	[InternalNo] [varchar](9) NOT NULL,
	[EpisodeNo] [varchar](13) NOT NULL,
	[VisitType] [varchar](2) NOT NULL,
	[TrackDate1] [varchar](12) NOT NULL,
	[TrackDate] [datetime2](7) NULL,
	[StepCode] [varchar](6) NOT NULL,
	[DocType] [varchar](3) NULL,
	[Doctor] [varchar](8) NULL,
	[Nurse] [varchar](8) NULL,
	[Priority] [varchar](6) NULL,
	[AttendDate1] [varchar](12) NOT NULL
) ON [PRIMARY]
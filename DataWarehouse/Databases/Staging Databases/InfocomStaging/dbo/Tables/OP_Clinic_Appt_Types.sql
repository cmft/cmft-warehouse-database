﻿CREATE TABLE [dbo].[OP_Clinic_Appt_Types](
	[ImpCode] [varchar](4) NOT NULL,
	[Hospital] [varchar](4) NOT NULL,
	[ClinicType] [varchar](6) NOT NULL,
	[Clinic] [varchar](8) NOT NULL,
	[ClinicDate] [varchar](8) NOT NULL,
	[Doctor] [varchar](8) NOT NULL,
	[ApptDay] [varchar](3) NOT NULL,
	[SessionStartTime] [int] NOT NULL,
	[TimeslotStart] [int] NOT NULL,
	[ApptType] [varchar](3) NOT NULL,
	[BookingUnits] [int] NULL,
	[BookingUnitsUsed] [int] NULL,
	[BookingUnitsFree] [int] NULL
) ON [PRIMARY]
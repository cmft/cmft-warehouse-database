﻿CREATE TABLE [dbo].[Code_1](
	[ImpCode] [varchar](4) NOT NULL,
	[CodeLink] [varchar](12) NOT NULL,
	[Code] [varchar](8) NOT NULL,
	[CodeDesc] [varchar](32) NULL,
	[IntValEng] [varchar](5) NULL,
	[IntValSco] [varchar](5) NULL,
	[LglDelInd] [varchar](1) NULL,
	[Data1] [varchar](10) NULL
) ON [PRIMARY]
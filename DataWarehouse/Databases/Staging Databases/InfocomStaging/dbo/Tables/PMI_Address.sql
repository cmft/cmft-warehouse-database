﻿CREATE TABLE [dbo].[PMI_Address](
	[ImpCode] [varchar](8) NOT NULL,
	[InternalNo] [varchar](13) NOT NULL,
	[AddressType] [varchar](2) NOT NULL,
	[StartDate] [datetime2](7) NULL,
	[EndDate] [datetime2](7) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[Address3] [varchar](30) NULL,
	[Address4] [varchar](30) NULL,
	[PostCode] [varchar](12) NULL,
	[DOR] [varchar](15) NULL,
	[HomePhone] [varchar](23) NULL,
	[WorkPhone] [varchar](23) NULL,
	[Summary1] [varchar](8) NULL,
	[Summary2] [varchar](8) NULL,
	[Summary3] [varchar](8) NULL
) ON [PRIMARY]
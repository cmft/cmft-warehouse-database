﻿CREATE TABLE [dbo].[Clinic](
	[ImpCode] [varchar](4) NOT NULL,
	[Hospital] [varchar](4) NULL,
	[ClinType] [varchar](3) NOT NULL,
	[Clinic] [varchar](8) NOT NULL,
	[ClinicDesc] [varchar](72) NULL,
	[ConType] [varchar](3) NULL,
	[Consultant] [varchar](8) NULL,
	[Specialty] [varchar](8) NULL,
	[ProvCode] [varchar](8) NULL,
	[FuncClinic] [varchar](5) NULL,
	[ConsText] [varchar](20) NULL,
	[ReportToLoc] [varchar](6) NULL,
	[FirstAvail] [varchar](3) NULL,
	[PullingDays] [int] NULL,
	[DNALetter] [varchar](3) NULL,
	[Location] [varchar](5) NULL,
	[LglDelInd] [char](1) NULL
) ON [PRIMARY]
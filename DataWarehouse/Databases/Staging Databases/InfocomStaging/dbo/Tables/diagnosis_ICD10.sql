﻿CREATE TABLE [dbo].[diagnosis_ICD10](
	[ImpCode] [int] NULL,
	[CodeType] [varchar](2) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Descr] [varchar](72) NOT NULL,
	[AltDesc] [varchar](72) NULL,
	[LglDelInd] [char](1) NULL
) ON [PRIMARY]
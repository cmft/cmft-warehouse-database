﻿CREATE TABLE [dbo].[Episode](
	[ImpCode] [varchar](8) NOT NULL,
	[Hospital] [varchar](4) NULL,
	[InternalNo] [varchar](13) NOT NULL,
	[EpisodeNo] [varchar](13) NOT NULL,
	[EpStartDate] [datetime2](7) NOT NULL,
	[EpEndDate] [datetime2](7) NULL,
	[EpiRefBy] [varchar](3) NULL,
	[EpiCode] [varchar](8) NULL,
	[Category] [varchar](3) NULL,
	[RefCode] [varchar](2) NULL,
	[RefReason] [varchar](4) NULL,
	[AEGroupType] [varchar](2) NULL,
	[AEGroup] [varchar](8) NULL,
	[CaseNoteNo] [varchar](14) NULL,
	[Summary1] [varchar](8) NULL,
	[Summary2] [varchar](8) NULL,
	[Summary3] [varchar](8) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RTT_Pathway_Epi](
	[ImpCode] [varchar](4) NOT NULL,
	[PathwayNo] [varchar](25) NULL,
	[InternalNo] [varchar](9) NOT NULL,
	[EpisodeNo] [varchar](9) NOT NULL
) ON [PRIMARY]
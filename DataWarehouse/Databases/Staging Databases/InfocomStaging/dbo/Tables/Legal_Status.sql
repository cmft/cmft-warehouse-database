﻿CREATE TABLE [dbo].[Legal_Status](
	[ImpCode] [varchar](4) NOT NULL,
	[Hospital] [varchar](4) NOT NULL,
	[InternalNo] [varchar](9) NOT NULL,
	[EpisodeNo] [varchar](13) NOT NULL,
	[VisitType] [varchar](2) NOT NULL,
	[LegalStatusStartDateTime1] [varchar](12) NOT NULL,
	[LegalStatusStartDateTime] [datetime2](7) NULL,
	[PsychiatricPatientStatus] [varchar](1) NULL,
	[MentalCategory] [varchar](1) NULL,
	[LegalStatus] [varchar](2) NULL,
	[PreviousAdmStatus] [varchar](1) NULL,
	[AdmUnderSupDischarge] [varchar](3) NULL,
	[Summary1] [varchar](8) NULL,
	[Summary2] [varchar](8) NULL,
	[Summary3] [varchar](8) NULL
) ON [PRIMARY]
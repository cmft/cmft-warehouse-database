﻿CREATE TABLE [dbo].[Specialty](
	[ImpCode] [varchar](4) NOT NULL,
	[Specialty] [varchar](8) NOT NULL,
	[Directorate] [char](8) NULL,
	[SpecDesc] [varchar](32) NULL,
	[SpecGroup] [varchar](4) NULL,
	[HAACode] [varchar](2) NULL,
	[DoHCode] [varchar](4) NULL,
	[SupraReg] [varchar](2) NULL,
	[SpecType] [varchar](2) NULL,
	[GPFHExempt] [varchar](3) NULL,
	[SpecScInt] [varchar](5) NULL,
	[SpecScLoc] [varchar](1) NULL,
	[LglDelInd] [varchar](1) NULL,
	[KarsID] [char](2) NULL
) ON [PRIMARY]
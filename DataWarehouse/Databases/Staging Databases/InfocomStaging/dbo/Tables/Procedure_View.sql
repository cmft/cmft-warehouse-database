﻿CREATE TABLE [dbo].[Procedure_View](
	[ImpCode] [int] NULL,
	[CodeType] [varchar](2) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Descr] [varchar](72) NOT NULL,
	[AltDesc] [varchar](72) NULL,
	[LglDelInd] [char](1) NULL,
	[Chargeable] [varchar](4) NULL,
	[EffDate] [datetime2](7) NULL
) ON [PRIMARY]
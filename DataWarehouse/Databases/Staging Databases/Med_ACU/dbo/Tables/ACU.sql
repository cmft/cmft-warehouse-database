﻿CREATE TABLE [dbo].[ACU](
	[DateOfReferral] [datetime] NULL,
	[Forename] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[HospitalNumber] [nvarchar](255) NULL,
	[DOB] [datetime] NULL,
	[SoRID] [nvarchar](255) NULL,
	[DirectedToID] [nvarchar](255) NULL,
	[OutcomeID] [nvarchar](255) NULL,
	[OtherClinicalComments] [nvarchar](255) NULL,
	[DateOfAttendance] [datetime] NULL,
	[NHSNumber] [nvarchar](255) NULL,
	[ReasonForPresentation] [nvarchar](255) NULL,
	[ACUID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
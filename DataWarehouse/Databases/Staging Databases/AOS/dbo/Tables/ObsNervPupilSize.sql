﻿CREATE TABLE [dbo].[ObsNervPupilSize](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TARNID] [int] NOT NULL,
	[Description] [varchar](60) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NULL
) ON [PRIMARY]
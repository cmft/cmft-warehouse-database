﻿CREATE TABLE [dbo].[Status](
	[StatusID] [int] NOT NULL,
	[StatusDesc] [varchar](20) NOT NULL,
	[SystemID] [int] NULL
) ON [PRIMARY]
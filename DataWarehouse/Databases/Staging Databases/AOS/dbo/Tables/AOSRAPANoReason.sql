﻿CREATE TABLE [dbo].[AOSRAPANoReason](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TarnID] [int] NOT NULL,
	[Description] [nvarchar](60) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NULL
) ON [PRIMARY]
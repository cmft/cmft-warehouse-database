﻿CREATE TABLE [dbo].[IncidentLocation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TarnID] [int] NOT NULL,
	[Description] [nvarchar](60) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_IncidentLocation_Active]  DEFAULT ((1)),
	[Order] [int] NULL
) ON [PRIMARY]
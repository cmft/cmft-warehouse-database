﻿CREATE TABLE [dbo].[PatientTask](
	[EgressID] [int] NOT NULL,
	[PatientTaskID] [int] IDENTITY(1,1) NOT NULL,
	[TaskID] [int] NOT NULL,
	[StaffID] [int] NULL,
	[DateTime_Ordered] [datetime] NULL,
	[DateTime_Due] [datetime] NULL,
	[DateTime_Completed] [datetime] NULL,
	[Reason_for_delay] [nvarchar](255) NULL,
	[Revised_Date] [datetime] NULL,
	[Comments] [nvarchar](255) NULL
) ON [PRIMARY]
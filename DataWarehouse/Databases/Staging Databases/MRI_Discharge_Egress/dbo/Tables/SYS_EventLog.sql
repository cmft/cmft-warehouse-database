﻿CREATE TABLE [dbo].[SYS_EventLog](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NULL,
	[PCName] [nvarchar](50) NULL,
	[EventTime] [datetime] NULL,
	[OldValue] [nvarchar](max) NULL,
	[NewValue] [nvarchar](max) NULL,
	[EventName] [nvarchar](50) NULL,
	[EventDescription] [nvarchar](50) NULL,
	[EgressID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
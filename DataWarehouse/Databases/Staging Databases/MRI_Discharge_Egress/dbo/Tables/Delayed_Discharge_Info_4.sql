﻿CREATE TABLE [dbo].[Delayed_Discharge_Info_4](
	[EgressID] [int] IDENTITY(1,1) NOT NULL,
	[GlobalProviderSpellNo] [varchar](50) NULL,
	[CasenoteNumber] [varchar](20) NULL,
	[PatientForename] [varchar](80) NULL,
	[PatientSurname] [varchar](80) NULL,
	[Date_Admitted_To_The_Trust] [datetime] NULL,
	[MFD_Medically_Fit_Date] [datetime] NULL,
	[PlannedDestinationID] [varchar](50) NULL,
	[SITREP_Breach_Reason] [varchar](50) NULL,
	[DateOfBirth] [datetime] NULL,
	[Age] [varchar](27) NULL,
	[Locality_PostCode] [nvarchar](100) NULL,
	[Ward] [nvarchar](30) NULL,
	[Date_Admitted_To_The_Ward] [datetime] NULL,
	[Discharge_Delay_days] [char](50) NULL,
	[Locality_GPPractice] [nvarchar](50) NULL,
	[Outstanding_Tasks] [int] NULL,
	[DischargeFlag] [bit] NOT NULL CONSTRAINT [DF_Delayed_Discharge_Info_5_DischargeFlag]  DEFAULT ((0)),
	[MFFlag] [bit] NOT NULL CONSTRAINT [DF_Delayed_Discharge_Info_5_MFFlag]  DEFAULT ((1)),
	[DischargeDate] [datetime] NULL,
	[LA] [nvarchar](100) NULL,
	[Postcode] [varchar](8) NULL
) ON [PRIMARY]
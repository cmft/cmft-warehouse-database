﻿CREATE TABLE [dbo].[testChildren](
	[McKessonID] [int] IDENTITY(-9999999,1) NOT NULL,
	[NHSNumber] [varchar](10) NOT NULL,
	[FirstName] [varchar](128) NOT NULL,
	[LastName] [varchar](128) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[SexCode] [char](1) NOT NULL CONSTRAINT [DF_testChildren_SexCode]  DEFAULT ('M')
) ON [PRIMARY]
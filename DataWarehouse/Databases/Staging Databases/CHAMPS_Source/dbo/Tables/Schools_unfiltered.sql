﻿CREATE TABLE [dbo].[Schools_unfiltered](
	[SchoolCode] [varchar](10) NOT NULL,
	[School] [varchar](60) NULL,
	[Postcode] [varchar](8) NULL,
	[Active] [char](1) NULL,
	[Schoolcategorycode] [char](1) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ChildBase](
	[SourceUniqueID] [int] NOT NULL,
	[Forename] [varchar](35) NULL,
	[Surname] [varchar](35) NULL,
	[DateOfBirth] [date] NULL,
	[SexCode] [varchar](12) NULL,
	[Postcode] [varchar](10) NULL,
	[SchoolCode] [varchar](10) NULL,
	[ContactType] [varchar](5) NOT NULL,
	[NHSNumber] [varchar](10) NOT NULL,
	[DateOfDeath] [date] NULL,
	[isIncluded] [bit] NULL DEFAULT ((1)),
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[Street] [varchar](40) NULL
) ON [PRIMARY]
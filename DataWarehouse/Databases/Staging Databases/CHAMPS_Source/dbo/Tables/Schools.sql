﻿CREATE TABLE [dbo].[Schools](
	[SchoolCode] [varchar](10) NOT NULL,
	[School] [varchar](60) NULL,
	[Postcode] [varchar](8) NULL
) ON [PRIMARY]
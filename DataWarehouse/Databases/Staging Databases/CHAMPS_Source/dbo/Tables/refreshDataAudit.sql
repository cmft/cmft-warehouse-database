﻿CREATE TABLE [dbo].[refreshDataAudit](
	[ts] [datetime2](7) NOT NULL DEFAULT (sysutcdatetime()),
	[ChildCount] [int] NOT NULL,
	[SchoolCount] [int] NOT NULL
) ON [PRIMARY]
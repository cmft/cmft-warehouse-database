﻿CREATE TABLE [dbo].[ChildDemographicExtras](
	[sourceuniqueid] [int] NOT NULL,
	[childhealthnumber] [varchar](20) NULL,
	[pupilnumber] [varchar](20) NULL,
	[schoolcode] [varchar](10) NULL,
	[EthnicOriginCode] [varchar](12) NULL,
	[GPCode] [int] NULL,
	[PracticeCode] [varchar](50) NULL,
	[PCTCode] [varchar](50) NULL,
	[NHSNumber] [varchar](20) NULL
) ON [PRIMARY]
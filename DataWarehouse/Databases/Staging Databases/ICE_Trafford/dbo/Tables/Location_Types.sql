﻿CREATE TABLE [dbo].[Location_Types](
	[Location_Type_Index] [smallint] IDENTITY(1,1) NOT NULL,
	[Location_Type] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
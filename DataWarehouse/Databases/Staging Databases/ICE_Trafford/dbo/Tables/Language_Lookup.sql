﻿CREATE TABLE [dbo].[Language_Lookup](
	[Language_Index] [smallint] IDENTITY(1,1) NOT NULL,
	[Language] [varchar](100) NOT NULL
) ON [PRIMARY]
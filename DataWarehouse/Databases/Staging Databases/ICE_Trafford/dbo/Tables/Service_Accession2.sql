﻿CREATE TABLE [dbo].[Service_Accession2](
	[Accession_Index] [int] NOT NULL,
	[Accession_Type] [varchar](50) NULL,
	[Localation_Code] [varchar](50) NULL,
	[Provider_ID] [int] NULL,
	[Accession_Prefix] [varchar](50) NULL,
	[Accession_Number] [int] NULL,
	[Accession_Minimum] [int] NULL,
	[Accession_Maximum] [int] NULL,
	[Allow_Reuse] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_Prices](
	[Price_Index] [int] NOT NULL,
	[Test_Code] [varchar](8) NULL,
	[Test_Description] [varchar](60) NULL,
	[Provider_ID] [int] NULL,
	[Billing_Code] [varchar](8) NULL,
	[Billing_Year] [varchar](9) NULL,
	[Billing_Price] [money] NULL,
	[Billing_Department] [varchar](8) NULL
) ON [PRIMARY]
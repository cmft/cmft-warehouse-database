﻿CREATE TABLE [dbo].[Spine_Clinic_Slots](
	[USRN] [uniqueidentifier] NOT NULL,
	[UBRN] [varchar](15) NULL,
	[ClinicIdentifierID] [int] NOT NULL,
	[SlotDateTime] [datetime] NOT NULL,
	[SlotSequence] [tinyint] NOT NULL,
	[Active] [bit] NOT NULL,
	[Rebooking] [bit] NOT NULL,
	[Service_Identifier_Id] [varchar](20) NOT NULL,
	[uarn] [uniqueidentifier] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[StatusLastUpdatedDateTime] [datetime] NULL
) ON [PRIMARY]
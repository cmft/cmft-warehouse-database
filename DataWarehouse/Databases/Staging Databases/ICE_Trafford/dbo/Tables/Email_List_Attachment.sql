﻿CREATE TABLE [dbo].[Email_List_Attachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email_Index] [int] NOT NULL,
	[Attachment] [varchar](255) NULL,
	[Del_Attachment_Sent] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_Analysers](
	[Analyser_Index] [int] NOT NULL,
	[Analyser_Code] [varchar](10) NULL,
	[Analyser_Description] [varchar](30) NULL
) ON [PRIMARY]
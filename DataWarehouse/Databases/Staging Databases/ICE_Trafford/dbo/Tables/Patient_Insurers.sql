﻿CREATE TABLE [dbo].[Patient_Insurers](
	[Insurer_Index] [int] IDENTITY(1,1) NOT NULL,
	[Patient_Index] [int] NOT NULL,
	[Type] [varchar](3) NOT NULL,
	[Carrier_Index] [int] NULL,
	[Plan_Index] [int] NULL,
	[Policy_Num] [varchar](20) NOT NULL,
	[Effective_Date] [smalldatetime] NOT NULL,
	[Group_Num] [varchar](20) NULL,
	[Policy_Holder] [varchar](50) NULL,
	[Pat_Relationship_To_Insured] [varchar](50) NULL,
	[State] [smallint] NULL,
	[Priority] [smallint] NOT NULL,
	[Authorisation_Num] [varchar](20) NULL,
	[Injury_Date] [smalldatetime] NULL,
	[Accident_Ind] [bit] NULL,
	[Employer] [varchar](50) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
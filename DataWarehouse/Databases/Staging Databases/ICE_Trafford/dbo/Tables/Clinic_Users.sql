﻿CREATE TABLE [dbo].[Clinic_Users](
	[Clinic_Index] [int] NOT NULL,
	[Remote_Type] [char](1) NOT NULL,
	[Remote_Index] [int] NOT NULL,
	[Can_Book] [bit] NOT NULL,
	[Can_Report] [bit] NOT NULL,
	[Can_Administer] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Clinics](
	[Clinic_Index] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Specialty] [smallint] NULL,
	[Clinic_Type] [tinyint] NOT NULL,
	[Walkin] [bit] NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
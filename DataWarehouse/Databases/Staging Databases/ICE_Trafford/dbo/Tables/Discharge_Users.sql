﻿CREATE TABLE [dbo].[Discharge_Users](
	[Clinician_National_Code] [varchar](16) NOT NULL,
	[User_Name] [varchar](25) NOT NULL,
	[User_Is_Consultant] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Spine_Clinic_Lookup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClinicCode] [varchar](20) NOT NULL,
	[Specialty] [smallint] NULL
) ON [PRIMARY]
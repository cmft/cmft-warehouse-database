﻿CREATE TABLE [dbo].[ClinicalFormEntry](
	[EntryId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[DateTimeOfEntry] [datetime] NOT NULL,
	[UserIndex] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateLastEdited] [datetime] NOT NULL,
	[Active] [bit] NOT NULL,
	[Completed] [bit] NOT NULL,
	[AnnotatableImageId] [int] NULL,
	[SeriesId] [int] NULL,
	[DatapointNumber] [tinyint] NULL,
	[XCoord] [smallint] NULL,
	[YCoord] [smallint] NULL,
	[TemplateId] [int] NOT NULL,
	[HasOutOfRangeValue] [bit] NOT NULL,
	[LocationId] [int] NULL,
	[ClinicianID] [int] NULL,
	[HasBeenViewed] [bit] NOT NULL,
	[Date_Last_Completed] [datetime] NULL,
	[ReportGenerated] [bit] NOT NULL
) ON [PRIMARY]
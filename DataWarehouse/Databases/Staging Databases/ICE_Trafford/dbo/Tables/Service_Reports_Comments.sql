﻿CREATE TABLE [dbo].[Service_Reports_Comments](
	[Report_Comment_Index] [int] IDENTITY(1,1) NOT NULL,
	[Service_Report_Index] [int] NOT NULL,
	[Service_Report_Comment] [varchar](140) NULL,
	[Comment_Type] [varchar](2) NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
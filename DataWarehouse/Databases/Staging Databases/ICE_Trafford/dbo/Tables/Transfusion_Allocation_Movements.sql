﻿CREATE TABLE [dbo].[Transfusion_Allocation_Movements](
	[Movement_Index] [int] IDENTITY(1,1) NOT NULL,
	[Stock_Index] [int] NOT NULL,
	[Transfusion_Index] [int] NULL,
	[Date_Of_Movement] [datetime] NOT NULL,
	[Action] [tinyint] NOT NULL,
	[Fridge_Index] [int] NULL,
	[User_Index] [int] NULL,
	[Notes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
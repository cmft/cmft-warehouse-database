﻿CREATE TABLE [dbo].[ClinicalFormAnnotatableImage](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[ImageBinary] [image] NULL,
	[WarningWatermark] [tinyint] NOT NULL DEFAULT ((0)),
	[WarningWatermarkColour] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
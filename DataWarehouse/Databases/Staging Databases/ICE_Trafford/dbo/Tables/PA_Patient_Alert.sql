﻿CREATE TABLE [dbo].[PA_Patient_Alert](
	[Alert_Index] [int] IDENTITY(1,1) NOT NULL,
	[Type_Index] [int] NOT NULL,
	[Patient_Id_Key] [int] NOT NULL,
	[Expiry_Date] [smalldatetime] NULL,
	[Lives_Remaining] [tinyint] NOT NULL,
	[User_Entered_Text] [text] NULL,
	[Is_Active]  AS (case when [Lives_Remaining]>(0) AND ([Expiry_Date] IS NULL OR [Expiry_Date]>getutcdate()) then (1) else (0) end),
	[Last_Updated] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
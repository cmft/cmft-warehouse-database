﻿CREATE TABLE [dbo].[Audit_Applications](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](200) NULL
) ON [PRIMARY]
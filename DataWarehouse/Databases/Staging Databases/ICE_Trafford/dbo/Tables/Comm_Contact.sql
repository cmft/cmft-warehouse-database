﻿CREATE TABLE [dbo].[Comm_Contact](
	[Comm_Contact_Key] [varchar](32) NULL,
	[Comm_Spell_Key] [varchar](28) NULL,
	[Contact_Number] [varchar](4) NULL,
	[Contacts] [datetime] NULL
) ON [PRIMARY]
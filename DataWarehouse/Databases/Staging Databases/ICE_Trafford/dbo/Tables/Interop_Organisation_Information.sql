﻿CREATE TABLE [dbo].[Interop_Organisation_Information](
	[organisation] [varchar](6) NOT NULL,
	[name] [varchar](100) NULL,
	[connection] [varchar](200) NOT NULL,
	[url] [varchar](200) NOT NULL
) ON [PRIMARY]
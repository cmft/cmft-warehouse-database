﻿CREATE TABLE [dbo].[Service_Tests_Information](
	[Tid] [int] NOT NULL,
	[TSpec] [varchar](10) NULL,
	[TName] [varchar](50) NULL,
	[TTubes] [varchar](255) NULL,
	[TComments] [varchar](255) NULL,
	[Org_Code] [varchar](6) NULL
) ON [PRIMARY]
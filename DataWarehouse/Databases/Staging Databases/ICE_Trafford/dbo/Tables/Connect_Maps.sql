﻿CREATE TABLE [dbo].[Connect_Maps](
	[Map_Name] [varchar](15) NULL,
	[Inbound_Process] [varchar](20) NULL,
	[Outbound_Process] [varchar](20) NULL,
	[Map_Active] [bit] NOT NULL,
	[Validation_Map] [bit] NOT NULL,
	[ICE_In_Map] [bit] NOT NULL,
	[ICE_Out_Map] [bit] NOT NULL
) ON [PRIMARY]
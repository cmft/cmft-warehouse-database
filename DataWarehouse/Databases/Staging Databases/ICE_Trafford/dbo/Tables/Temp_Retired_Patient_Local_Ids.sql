﻿CREATE TABLE [dbo].[Temp_Retired_Patient_Local_Ids](
	[Patient_Id_Key] [int] NOT NULL,
	[Patient_Local_Id] [varchar](30) NOT NULL,
	[Org_Code] [varchar](6) NULL,
	[Main_Identifier] [bit] NULL,
	[Active_Casenote] [bit] NOT NULL,
	[Hidden] [bit] NOT NULL,
	[Hospital_Number] [varchar](24) NULL,
	[Id] [int] NOT NULL,
	[Retired] [bit] NOT NULL,
	[Type_Id] [int] NULL
) ON [PRIMARY]
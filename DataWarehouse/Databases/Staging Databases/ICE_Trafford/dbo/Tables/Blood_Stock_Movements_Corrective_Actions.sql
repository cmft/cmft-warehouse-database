﻿CREATE TABLE [dbo].[Blood_Stock_Movements_Corrective_Actions](
	[Corrective_Action_Index] [int] IDENTITY(1,1) NOT NULL,
	[Movement_Index] [int] NOT NULL,
	[Corrective_Action] [text] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Clinic_Slot_Pattern_Slots](
	[Slot_Index] [int] IDENTITY(1,1) NOT NULL,
	[Pattern_Index] [int] NOT NULL,
	[DayOfWeek] [tinyint] NOT NULL,
	[StartTime] [smalldatetime] NOT NULL,
	[Duration] [smallint] NOT NULL,
	[CutoffMinsBefore] [smallint] NULL,
	[Sequence] [smallint] NOT NULL,
	[Urgency_Index] [tinyint] NOT NULL,
	[Clinician] [int] NULL,
	[Specialty] [int] NULL,
	[infinite_slot] [bit] NOT NULL CONSTRAINT [DF_clinic_slot_pattern_slots_inifinite]  DEFAULT (0)
) ON [PRIMARY]
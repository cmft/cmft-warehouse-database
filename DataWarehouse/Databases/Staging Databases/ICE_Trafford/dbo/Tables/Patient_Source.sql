﻿CREATE TABLE [dbo].[Patient_Source](
	[Patient_id_key] [int] NOT NULL,
	[Seq_No] [int] NOT NULL,
	[Source_id] [smallint] NOT NULL,
	[User_Index] [int] NULL,
	[Date_added] [datetime] NOT NULL
) ON [PRIMARY]
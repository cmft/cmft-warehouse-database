﻿CREATE TABLE [dbo].[MI_Query](
	[Index] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[Description] [varchar](1000) NULL,
	[SQL] [text] NOT NULL,
	[Timeout] [int] NOT NULL DEFAULT ((30)),
	[Max_Rows] [int] NOT NULL DEFAULT ((1000)),
	[RestrictedApplicationId] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
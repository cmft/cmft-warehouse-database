﻿CREATE TABLE [dbo].[ICE_EPR_External_References](
	[Event_Index] [int] IDENTITY(1,1) NOT NULL,
	[Link_Index] [int] NOT NULL,
	[Event_Name] [varchar](50) NOT NULL,
	[Event_Description] [varchar](255) NOT NULL,
	[External_URL] [varchar](2048) NULL,
	[Integrate_URL] [bit] NULL
) ON [PRIMARY]
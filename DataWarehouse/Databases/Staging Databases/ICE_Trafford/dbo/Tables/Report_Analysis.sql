﻿CREATE TABLE [dbo].[Report_Analysis](
	[Analysis_Index] [int] IDENTITY(1,1) NOT NULL,
	[Patient_Index] [int] NULL,
	[User_Index] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[Template_Indicator] [bit] NOT NULL,
	[Patient_Specific] [bit] NOT NULL,
	[Alert_Creator] [bit] NOT NULL,
	[Patient_Group_Specific] [bit] NOT NULL,
	[Patient_Group] [smallint] NULL,
	[Alert_Requesting_Clinician] [bit] NOT NULL DEFAULT ((0)),
	[Report_Filter_Type] [int] NOT NULL DEFAULT ((0)),
	[Report_Filter_Operator] [char](3) NULL,
	[Group_Checks] [bit] NOT NULL DEFAULT ((0)),
	[Associated_Alert_Type_Index] [int] NULL,
	[Associated_Alert_Options] [tinyint] NOT NULL DEFAULT ((3))
) ON [PRIMARY]
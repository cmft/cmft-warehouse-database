﻿CREATE TABLE [dbo].[ToolbarItems](
	[ItemID] [int] NOT NULL,
	[PanelName] [varchar](25) NOT NULL,
	[ItemURL] [varchar](200) NULL,
	[ItemRequiresPatient] [bit] NULL,
	[EditPatient] [bit] NULL,
	[Position] [tinyint] NULL CONSTRAINT [DF_ToolbarItems_Position]  DEFAULT (9),
	[ApplicationID] [int] NULL,
	[ItemImageBinary] [varbinary](8000) NULL,
	[Local_Admin] [bit] NULL
) ON [PRIMARY]
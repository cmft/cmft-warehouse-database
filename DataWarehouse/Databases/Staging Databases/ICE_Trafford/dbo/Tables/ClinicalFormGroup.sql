﻿CREATE TABLE [dbo].[ClinicalFormGroup](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
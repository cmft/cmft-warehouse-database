﻿CREATE TABLE [dbo].[Request_Tubes](
	[Tube_Index] [int] NOT NULL,
	[Code] [varchar](6) NOT NULL,
	[Name] [varchar](25) NULL,
	[Description] [varchar](50) NULL,
	[Vol] [numeric](18, 5) NULL,
	[Min_Vol] [numeric](18, 5) NULL,
	[Paed_Min_Vol] [numeric](18, 5) NULL,
	[Colour] [int] NULL,
	[EDI_SampleCode] [varchar](8) NULL,
	[EDI_SampleText] [varchar](65) NULL,
	[Stock_Code] [varchar](25) NULL,
	[Unit_Of_Issue] [varchar](25) NULL,
	[ReOrder_Qty] [varchar](25) NULL,
	[Date_Added] [datetime] NULL,
	[organisation] [varchar](6) NOT NULL,
	[Prefix] [varchar](10) NULL,
	[Suffix] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[External_Links_Predefined_Items](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[RequiresPatient] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalFormTitlebarFormat](
	[TitlebarFormatIndex] [int] IDENTITY(1,1) NOT NULL,
	[Factor_Id] [int] NOT NULL,
	[FontSize] [int] NULL,
	[Bold] [bit] NULL,
	[Italics] [bit] NULL,
	[TextColour] [varchar](50) NULL,
	[BackColour] [varchar](50) NULL
) ON [PRIMARY]
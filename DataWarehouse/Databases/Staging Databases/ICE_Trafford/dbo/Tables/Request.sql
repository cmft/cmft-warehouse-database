﻿CREATE TABLE [dbo].[Request](
	[Request_Id] [int] IDENTITY(1,1) NOT NULL,
	[Date_Added] [datetime] NULL,
	[Interop_Index] [int] NULL,
	[GlobalClinicalDetail] [varchar](2048) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient_Local_ID_Exceptions](
	[Exception_ID] [int] IDENTITY(1,1) NOT NULL,
	[Problem_Local_ID] [int] NULL,
	[Main_Local_ID] [int] NULL,
	[Date_Added] [datetime] NOT NULL CONSTRAINT [DF_Patient_Local_ID_PAS_Exceptions_Date_Added]  DEFAULT (getdate()),
	[Exception_Type] [int] NOT NULL CONSTRAINT [DF_Patient_Local_ID_PAS_Exceptions_Exception_Type]  DEFAULT (0),
	[TRXID] [varchar](20) NULL,
	[Date_Processed] [datetime] NULL,
	[Action_Type] [bit] NULL,
	[ApplicationID] [tinyint] NOT NULL
) ON [PRIMARY]
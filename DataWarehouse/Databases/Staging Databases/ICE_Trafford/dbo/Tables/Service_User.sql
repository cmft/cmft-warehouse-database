﻿CREATE TABLE [dbo].[Service_User](
	[User_Index] [int] IDENTITY(1,1) NOT NULL,
	[User_Name] [varchar](25) NOT NULL,
	[User_FullName] [varchar](35) NULL,
	[User_Initials] [varchar](10) NULL,
	[User_Description] [varchar](50) NULL,
	[User_Password] [varchar](64) NOT NULL,
	[Pwd_ExpiryDate] [datetime] NULL,
	[Previous_Pwd1] [varchar](64) NULL,
	[Previous_Pwd2] [varchar](64) NULL,
	[User_Group] [varchar](50) NULL,
	[User_Mail_Group] [varchar](50) NULL,
	[Organisation] [varchar](6) NULL,
	[Security_Levels] [varchar](50) NULL,
	[User_Start_Date] [datetime] NULL,
	[User_End_Date] [datetime] NULL,
	[Pwd_Change_OnNext] [bit] NOT NULL,
	[Pwd_CannotChange] [bit] NOT NULL,
	[Pwd_NeverExpires] [bit] NOT NULL,
	[IPAddressMain] [varchar](11) NULL,
	[IPAddressStart] [varchar](3) NULL,
	[IPAddressEnd] [varchar](3) NULL,
	[Logged_On] [int] NULL,
	[SpecialPanelCode] [varchar](10) NULL,
	[Active] [bit] NOT NULL,
	[Date_Added] [datetime] NULL,
	[User_Grade] [varchar](40) NULL,
	[User_Contact] [varchar](35) NULL,
	[User_Addr_Line1] [varchar](40) NULL,
	[User_Addr_Line2] [varchar](40) NULL,
	[User_Addr_Line3] [varchar](40) NULL,
	[User_Title] [varchar](35) NULL,
	[User_Deleted] [int] NULL,
	[User_Type] [tinyint] NULL,
	[NotepadSecurityLevel] [tinyint] NULL,
	[Contact_Number] [varchar](12) NULL,
	[Language_Index] [int] NOT NULL DEFAULT (1),
	[Clinician_Index] [int] NOT NULL DEFAULT (0),
	[Email_Address] [varchar](100) NULL
) ON [PRIMARY]
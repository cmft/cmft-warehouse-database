﻿CREATE TABLE [dbo].[CRIR_Specialty](
	[Specialty_Code] [smallint] NOT NULL,
	[Specialty] [varchar](68) NULL,
	[Code_Type] [varchar](2) NOT NULL DEFAULT ('MS')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ICEMail_Attachment](
	[Attachment_Id] [int] IDENTITY(1,1) NOT NULL,
	[Mail_Id] [int] NOT NULL,
	[Application_Id] [int] NOT NULL,
	[Remote_Index] [int] NOT NULL
) ON [PRIMARY]
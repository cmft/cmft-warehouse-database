﻿CREATE TABLE [dbo].[Clinical_Letter_Role_Copies](
	[Role_Copy_Index] [int] IDENTITY(1,1) NOT NULL,
	[Role_Index] [tinyint] NOT NULL,
	[Copy_Name] [varchar](50) NOT NULL,
	[Order] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalFormFactorRangeItem](
	[RangeId] [int] IDENTITY(1,1) NOT NULL,
	[FactorId] [int] NOT NULL,
	[DataType] [tinyint] NOT NULL,
	[MinValue] [decimal](19, 4) NOT NULL,
	[MaxValue] [decimal](19, 4) NOT NULL,
	[Order] [int] NOT NULL,
	[ScoreValue] [tinyint] NULL
) ON [PRIMARY]
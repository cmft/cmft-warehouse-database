﻿CREATE TABLE [dbo].[Clinical_Letter_Diagnoses](
	[Diagnosis_Index] [int] IDENTITY(1,1) NOT NULL,
	[Letter_Index] [int] NOT NULL,
	[Diagnosis_Type] [char](1) NOT NULL,
	[Diagnosis_Code] [varchar](8) NOT NULL,
	[Diagnosis_Text] [nchar](255) NOT NULL,
	[Episode_Index] [int] NOT NULL,
	[Date_Of_Diagnosis] [datetime] NULL,
	[Comments] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
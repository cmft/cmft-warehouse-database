﻿CREATE TABLE [dbo].[Clinic_Slot_Bookings_Notes](
	[Note_Index] [int] IDENTITY(1,1) NOT NULL,
	[Booking_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Note] [varchar](255) NOT NULL,
	[Active] [bit] NOT NULL,
	[DateAdded] [smalldatetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Configuration_Programs](
	[Program_Index] [int] NOT NULL,
	[Organisation] [varchar](6) NOT NULL,
	[Program_Name] [varchar](50) NOT NULL
) ON [PRIMARY]
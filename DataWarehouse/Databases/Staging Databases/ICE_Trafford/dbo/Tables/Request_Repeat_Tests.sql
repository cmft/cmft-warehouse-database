﻿CREATE TABLE [dbo].[Request_Repeat_Tests](
	[Patient_Index] [int] NOT NULL,
	[Test_Index] [int] NOT NULL,
	[Frequency_Notes] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[dbVersion](
	[Version] [int] NOT NULL,
	[LastAmended] [datetime] NOT NULL,
	[ReleaseName] [varchar](10) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Referral_Protocol_Questions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [varchar](10) NOT NULL,
	[question] [varchar](200) NOT NULL,
	[comment] [varchar](50) NULL,
	[help] [varchar](200) NULL,
	[options] [varchar](200) NULL,
	[img] [varchar](5) NULL,
	[active] [smallint] NOT NULL,
	[create_dt] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient_Local_IDs_Extra_Data](
	[Patient_Local_ID_Index] [int] NOT NULL,
	[Key_Index] [int] NOT NULL,
	[Coded_Value] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_Orders](
	[Order_Id] [varchar](40) NOT NULL,
	[Status] [varchar](2) NULL,
	[Date_Added] [datetime] NULL,
	[Line_Detail] [varchar](500) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient_Notes](
	[Patient_Local_id] [varchar](16) NULL,
	[Date_Time_Stamp] [datetime] NULL,
	[Notes_Key] [varchar](50) NULL,
	[Type] [varchar](30) NULL
) ON [PRIMARY]
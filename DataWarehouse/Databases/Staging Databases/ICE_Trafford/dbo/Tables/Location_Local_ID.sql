﻿CREATE TABLE [dbo].[Location_Local_ID](
	[Location_Local_ID_Index] [int] IDENTITY(1,1) NOT NULL,
	[Organisation_Code] [varchar](6) NOT NULL,
	[Local_Location_Code] [varchar](30) NOT NULL,
	[Location_Index] [int] NOT NULL,
	[System_Provider_Index] [int] NOT NULL,
	[Inbound] [bit] NOT NULL DEFAULT (1),
	[Outbound] [bit] NOT NULL DEFAULT (1)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_AMB_Configuration](
	[Cfg_Index] [int] NOT NULL,
	[Cfg_Id] [varchar](20) NULL,
	[Cfg_Name] [varchar](20) NULL,
	[Cfg_Number] [int] NULL,
	[Cfg_Category] [varchar](20) NULL,
	[Cfg_Description] [varchar](255) NULL,
	[Cfg_Value] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalFormFactor](
	[FactorId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Datatype] [tinyint] NOT NULL,
	[SnomedCode] [varchar](12) NULL,
	[MinValue] [decimal](19, 4) NULL,
	[MaxValue] [decimal](19, 4) NULL,
	[CanScore] [bit] NOT NULL,
	[ICETableType] [tinyint] NULL,
	[TestNameIndex] [int] NULL,
	[Active] [bit] NOT NULL,
	[IncludeInMIReports] [bit] NOT NULL,
	[UOM] [varchar](35) NULL,
	[Prompt] [varchar](1000) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Referral_Protocols](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](50) NOT NULL,
	[specialty_id] [smallint] NOT NULL,
	[diagnosis_id] [varchar](5) NOT NULL,
	[role] [tinyint] NOT NULL,
	[active] [tinyint] NOT NULL,
	[protocol] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Legitimate_Relationship](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[User_Index] [int] NOT NULL,
	[Patient_Index] [int] NOT NULL,
	[Reason] [varchar](100) NOT NULL,
	[Expiry] [smalldatetime] NOT NULL,
	[Created_DateTime] [smalldatetime] NOT NULL
) ON [PRIMARY]
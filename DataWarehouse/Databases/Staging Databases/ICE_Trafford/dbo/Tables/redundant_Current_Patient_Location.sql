﻿CREATE TABLE [dbo].[redundant_Current_Patient_Location](
	[Location_Key] [varchar](46) NOT NULL,
	[Location_Code] [varchar](16) NULL,
	[Patient_Local_ID] [varchar](30) NULL,
	[Set_Up_App] [varchar](3) NULL,
	[Location_Start_date] [datetime] NULL,
	[Location_End_Date] [datetime] NULL,
	[HIS_Start_Date] [datetime] NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Spine_Service_Identifiers_Lookup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Service_Identifier_ID] [varchar](20) NOT NULL,
	[Clinic_Lookup_ID] [int] NOT NULL
) ON [PRIMARY]
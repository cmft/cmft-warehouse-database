﻿CREATE TABLE [dbo].[Spine_Cancellation_Mappings](
	[EBS_Cancellation_Code] [tinyint] NOT NULL,
	[PAS_Cancellation_Type] [char](1) NOT NULL,
	[PAS_Cancellation_Code] [varchar](10) NOT NULL,
	[PAS_Cancellation_Description] [varchar](50) NOT NULL
) ON [PRIMARY]
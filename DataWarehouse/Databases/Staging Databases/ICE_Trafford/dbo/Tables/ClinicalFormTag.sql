﻿CREATE TABLE [dbo].[ClinicalFormTag](
	[TagId] [int] IDENTITY(1,1) NOT NULL,
	[TagName] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
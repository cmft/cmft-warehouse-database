﻿CREATE TABLE [dbo].[Request_Sample_Prompts](
	[Sample_Panel_ID] [int] NOT NULL,
	[Prompt_Index] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Clinic_Purpose] [smallint] NULL
) ON [PRIMARY]
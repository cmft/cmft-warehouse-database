﻿CREATE TABLE [dbo].[Service_User_Roles](
	[Role_Index] [tinyint] IDENTITY(1,1) NOT NULL,
	[Role_Name] [varchar](20) NOT NULL,
	[Role_Type] [char](1) NOT NULL,
	[AD_Security_Group] [varchar](255) NULL,
	[Demand_LR] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
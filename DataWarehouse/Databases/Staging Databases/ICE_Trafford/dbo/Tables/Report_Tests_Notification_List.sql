﻿CREATE TABLE [dbo].[Report_Tests_Notification_List](
	[EntryID] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [varchar](255) NULL
) ON [PRIMARY]
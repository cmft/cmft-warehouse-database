﻿CREATE TABLE [dbo].[User_Notes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_index] [int] NOT NULL,
	[active] [bit] NOT NULL,
	[create_dt] [datetime] NOT NULL,
	[update_dt] [datetime] NOT NULL,
	[note] [varchar](8000) NOT NULL
) ON [PRIMARY]
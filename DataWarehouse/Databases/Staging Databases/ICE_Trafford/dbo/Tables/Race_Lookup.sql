﻿CREATE TABLE [dbo].[Race_Lookup](
	[Race_Index] [smallint] IDENTITY(1,1) NOT NULL,
	[Race] [varchar](50) NOT NULL
) ON [PRIMARY]
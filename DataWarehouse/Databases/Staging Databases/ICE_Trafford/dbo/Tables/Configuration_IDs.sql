﻿CREATE TABLE [dbo].[Configuration_IDs](
	[Program_Index] [int] NOT NULL,
	[Config_Index] [int] NOT NULL,
	[Config_Name] [varchar](50) NOT NULL,
	[Config_Notes] [varchar](255) NULL,
	[Value_Type] [smallint] NULL,
	[Default_Value] [varchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Report_Test_Role_Permissions](
	[Test_Combination_Index] [int] NOT NULL,
	[Role_Index] [int] NOT NULL,
	[Permitted] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_Investigations](
	[Investigation_Index] [int] IDENTITY(1,1) NOT NULL,
	[Service_Report_Index] [int] NOT NULL,
	[Investigation_Code] [varchar](10) NULL,
	[Investigation_Requested] [varchar](60) NULL,
	[Comment_Marker] [bit] NOT NULL,
	[Priority_Marker] [bit] NOT NULL,
	[Date_Added] [datetime] NULL,
	[Sample_Index] [int] NULL,
	[EDI_RC_Index] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Report_Analysis_Item](
	[Analysis_Item_Index] [int] IDENTITY(1,1) NOT NULL,
	[Analysis_Index] [int] NOT NULL,
	[Test_Name_Index] [int] NOT NULL,
	[Analysis_Type] [tinyint] NOT NULL,
	[Analysis_Val1] [decimal](9, 3) NULL,
	[Analysis_Val2] [decimal](9, 3) NULL,
	[Analysis_Text] [varchar](100) NULL,
	[Message] [varchar](100) NULL,
	[Apply_To_All_Tests] [bit] NOT NULL DEFAULT ((0)),
	[Mark_As_Abnormal] [bit] NOT NULL DEFAULT ((0)),
	[Associated_Alert_Type_Index] [int] NULL,
	[Associated_Alert_Options] [tinyint] NOT NULL DEFAULT ((3))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[National_GPs](
	[Clinician_National_Code] [varchar](8) NOT NULL,
	[Clinician_Name] [varchar](60) NOT NULL,
	[Practice_Code] [varchar](7) NOT NULL,
	[Address_Line_1] [varchar](35) NULL,
	[Address_Line_2] [varchar](35) NULL,
	[Address_Line_3] [varchar](35) NULL,
	[Address_Line_4] [varchar](35) NULL,
	[Address_Line_5] [varchar](35) NULL,
	[Address_Postcode] [varchar](9) NULL,
	[Telephone_Number] [varchar](12) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalFormFactorPicklistItem](
	[PicklistId] [int] IDENTITY(1,1) NOT NULL,
	[FactorId] [int] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Order] [int] NOT NULL,
	[ScoreVal] [tinyint] NULL
) ON [PRIMARY]
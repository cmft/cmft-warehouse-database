﻿CREATE TABLE [dbo].[Ward_Notes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[usertype] [smallint] NOT NULL,
	[active] [bit] NOT NULL,
	[note] [varchar](8000) NOT NULL,
	[Location_Index] [int] NOT NULL
) ON [PRIMARY]
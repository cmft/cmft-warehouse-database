﻿CREATE TABLE [dbo].[ServerSide_Printer_Group_Profile_Settings](
	[Setting_Index] [int] IDENTITY(1,1) NOT NULL,
	[Group_Index] [int] NOT NULL,
	[Profile_Index] [int] NOT NULL,
	[Margin_Top] [real] NULL,
	[Margin_Bottom] [real] NULL,
	[Margin_Left] [real] NULL,
	[Margin_Right] [real] NULL,
	[Portrait] [bit] NOT NULL CONSTRAINT [DF_ServerSide_Printer_Group_Profile_Settings_Portrait]  DEFAULT ((1)),
	[PatientDetailsHeader] [bit] NOT NULL CONSTRAINT [DF_ServerSide_Printer_Group_Profile_Settings_PatientDetailsHeader]  DEFAULT ((0)),
	[PagesFooter] [bit] NOT NULL CONSTRAINT [DF_ServerSide_Printer_Group_Profile_Settings_PagesFooter]  DEFAULT ((0)),
	[Collate] [bit] NOT NULL CONSTRAINT [DF_ServerSide_Printer_Group_Profile_Settings_Collate]  DEFAULT ((0)),
	[Duplex] [tinyint] NULL,
	[PaperSize] [varchar](50) NULL,
	[PaperSource] [varchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Diagnosis](
	[Diagnosis_key] [int] IDENTITY(1,1) NOT NULL,
	[Diagnosis_Number] [int] NULL,
	[Diagnosis_type] [varchar](1) NULL,
	[Diagnosis_Code] [varchar](8) NULL,
	[Diagnosis_date] [datetime] NULL,
	[Date_added] [datetime] NULL,
	[Comment_Text] [varchar](5000) NULL,
	[CongenitalorAcquired] [varchar](1) NULL,
	[BurnsCase] [varchar](1) NULL,
	[Injury_Text] [varchar](100) NULL,
	[Episode_Index] [int] NOT NULL
) ON [PRIMARY]
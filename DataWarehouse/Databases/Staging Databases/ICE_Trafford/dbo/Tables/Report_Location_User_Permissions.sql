﻿CREATE TABLE [dbo].[Report_Location_User_Permissions](
	[Location_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Permitted] [bit] NOT NULL
) ON [PRIMARY]
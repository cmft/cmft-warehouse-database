﻿CREATE TABLE [dbo].[PatientListTeam](
	[TeamIndex] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Owner] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY]
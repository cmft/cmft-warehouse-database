﻿

CREATE VIEW [dbo].[ICEView_ReportSummary]
as
	select
		srq.Service_Request_Index AS Service_Request_Index, 
		srq.Hospital_Number AS HospitalNumber, 
		srq.Clinician_Index AS Clinician_Index, 
		srq.Patient_id_key AS Patient_Id_Key, 
		srq.Location_Index AS Location_Index, 
		srp.Service_Report_Index AS Service_Report_Index, 
		srp.Service_Report_ID AS Report_ID, 
		srp.Service_Report_Type AS Specialty, 
		srp.DateTime_of_Report AS DateTime_Of_Report, 
		srp.Status AS Report_Status, 
		srp.Date_Added AS Date_Added, 
		rfr.Reason_Code AS Filed_Reason_Code, 
		rfr.Reason AS Filed_Reason_Description, 
		afr.Filed_Date AS Filed_Date, 
		afr.User_Index AS Filed_User_Index, 
		rhr.Reason_Code AS Hidden_Reason_Code, 
		rhr.Reason AS Hidden_Reason_Description
	from
		dbo.Service_Requests srq
		inner join dbo.Service_Reports srp on srq.Service_Request_Index = srp.Service_Request_Index
		left outer join dbo.Audit_Filed_Reports afr on srp.Service_Report_Index = afr.Report_Index
		left outer join dbo.Report_Filing_Reasons rfr on afr.Reason_Index = rfr.Reason_Index
		left outer join dbo.Report_Hiding_Reasons rhr on srp.Category = rfr.Reason_Index
	where
		srq.Status = 'RR'


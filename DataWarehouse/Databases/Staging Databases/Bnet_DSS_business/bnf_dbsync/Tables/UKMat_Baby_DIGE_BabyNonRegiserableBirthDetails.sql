﻿CREATE TABLE [bnf_dbsync].[UKMat_Baby_DIGE_BabyNonRegiserableBirthDetails](
	[EntityID] [uniqueidentifier] NOT NULL,
	[DateAndTimeDelivered] [datetime] NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
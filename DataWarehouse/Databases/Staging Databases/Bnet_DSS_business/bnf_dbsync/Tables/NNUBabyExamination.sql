﻿CREATE TABLE [bnf_dbsync].[NNUBabyExamination](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateOfExaminationLocal] [datetime] NULL,
	[HeadCircumference] [decimal](10, 1) NULL,
	[Skin] [nvarchar](35) NULL,
	[SkinComments] [nvarchar](500) NULL,
	[Cranium] [nvarchar](35) NULL,
	[CraniumComments] [nvarchar](500) NULL,
	[Fontanelle] [nvarchar](35) NULL,
	[FontanelleComments] [nvarchar](500) NULL,
	[Sutures] [nvarchar](35) NULL,
	[SuturesComments] [nvarchar](500) NULL,
	[RedReflex] [nvarchar](35) NULL,
	[RedReflexComments] [nvarchar](500) NULL,
	[Ears] [nvarchar](35) NULL,
	[EarsComments] [nvarchar](500) NULL,
	[PalateSuck] [nvarchar](35) NULL,
	[PalateSuckComments] [nvarchar](500) NULL,
	[Spine] [nvarchar](35) NULL,
	[SpineComments] [nvarchar](500) NULL,
	[Breath] [nvarchar](35) NULL,
	[BreathComments] [nvarchar](500) NULL,
	[Heart] [nvarchar](35) NULL,
	[HeartComments] [nvarchar](500) NULL,
	[Femoral] [nvarchar](35) NULL,
	[FemoralComments] [nvarchar](500) NULL,
	[Abdomen] [nvarchar](35) NULL,
	[AbdomenComments] [nvarchar](500) NULL,
	[Genitalia] [nvarchar](35) NULL,
	[GenitaliaComments] [nvarchar](500) NULL,
	[Anus] [nvarchar](35) NULL,
	[AnusComments] [nvarchar](500) NULL,
	[Hands] [nvarchar](35) NULL,
	[HandsComments] [nvarchar](500) NULL,
	[Feet] [nvarchar](35) NULL,
	[FeetComments] [nvarchar](500) NULL,
	[Hips] [nvarchar](35) NULL,
	[HipsComments] [nvarchar](500) NULL,
	[Tone] [nvarchar](35) NULL,
	[ToneComments] [nvarchar](500) NULL,
	[Movement] [nvarchar](35) NULL,
	[MovementComments] [nvarchar](500) NULL,
	[Moro] [nvarchar](35) NULL,
	[MoroComments] [nvarchar](500) NULL,
	[Overall] [nvarchar](35) NULL,
	[OverallComments] [nvarchar](500) NULL,
	[Other] [nvarchar](500) NULL,
	[NameOfExaminer] [nvarchar](500) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUBabyExamination_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
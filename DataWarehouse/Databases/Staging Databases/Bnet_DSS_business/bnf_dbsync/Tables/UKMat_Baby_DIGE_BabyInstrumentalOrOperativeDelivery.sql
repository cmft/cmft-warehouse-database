﻿CREATE TABLE [bnf_dbsync].[UKMat_Baby_DIGE_BabyInstrumentalOrOperativeDelivery](
	[EntityID] [uniqueidentifier] NOT NULL,
	[FailedInstrumental] [nvarchar](100) NULL,
	[BreechDelivery_Type] [nvarchar](100) NULL,
	[DateTimeForcepsDeliveryCommenced] [datetime] NULL,
	[DateTimeForcepsDeliveryAbandoned] [datetime] NULL,
	[TypeOfForcepsForCMACE] [nvarchar](100) NULL,
	[DateTimeVentouseDeliveryCommenced] [datetime] NULL,
	[DateTimeVentouseDeliveryAbandoned] [datetime] NULL,
	[RotationalVentouseDelivery] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
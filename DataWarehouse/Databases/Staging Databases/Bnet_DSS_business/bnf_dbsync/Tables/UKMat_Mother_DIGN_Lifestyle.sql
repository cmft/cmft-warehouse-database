﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_Lifestyle](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[Date] [datetime] NULL,
	[UpdateType] [nvarchar](100) NULL,
	[PartnershipStatus] [nvarchar](100) NULL,
	[RecreationalDrugsOrSubstancesTaken] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_Observations](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[ConsentObtained] [nvarchar](100) NULL,
	[ObservationType] [nvarchar](100) NULL,
	[Urinalysis] [nvarchar](100) NULL,
	[Urinalyis_Outcome] [nvarchar](100) NULL,
	[Height] [decimal](18, 2) NULL,
	[Weight] [decimal](18, 2) NULL,
	[BMI] [decimal](18, 2) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
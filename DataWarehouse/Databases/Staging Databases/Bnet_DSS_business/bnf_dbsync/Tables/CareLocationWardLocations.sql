﻿CREATE TABLE [bnf_dbsync].[CareLocationWardLocations](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[LocationName] [nvarchar](280) NULL,
	[WardName] [nvarchar](150) NULL,
	[Description] [nvarchar](250) NULL,
	[WardCareType] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_CareLocationWardLocations_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
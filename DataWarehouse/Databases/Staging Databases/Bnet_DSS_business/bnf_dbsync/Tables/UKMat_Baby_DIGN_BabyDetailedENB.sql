﻿CREATE TABLE [bnf_dbsync].[UKMat_Baby_DIGN_BabyDetailedENB](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DetailedENB_ExaminationType] [nvarchar](100) NULL,
	[SeverityOfBirthTrauma] [nvarchar](100) NULL,
	[BirthTraumaDetails] [nvarchar](4000) NULL,
	[AbnormalitiesSuspected] [nvarchar](100) NULL,
	[AbnormalitiesConfirmed] [nvarchar](100) NULL,
	[AbnormalitiesConfirmedWhen] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
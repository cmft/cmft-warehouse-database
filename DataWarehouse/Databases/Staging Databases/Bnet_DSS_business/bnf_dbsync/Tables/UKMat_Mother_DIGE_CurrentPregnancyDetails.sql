﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGE_CurrentPregnancyDetails](
	[EntityID] [uniqueidentifier] NOT NULL,
	[ReasonBookedLate] [nvarchar](100) NULL,
	[PregnantBefore] [nvarchar](1) NULL,
	[CertaintyOfLMP] [nvarchar](100) NULL,
	[LMP] [datetime] NULL,
	[EDD_Dates] [datetime] NULL,
	[EDD_Scan] [datetime] NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
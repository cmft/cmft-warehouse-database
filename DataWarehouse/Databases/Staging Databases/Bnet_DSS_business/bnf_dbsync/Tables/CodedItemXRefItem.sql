﻿CREATE TABLE [bnf_dbsync].[CodedItemXRefItem](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteLocal] [datetime] NULL,
	[CodingSystem] [nvarchar](200) NULL,
	[Value1] [nvarchar](200) NULL,
	[Value2] [nvarchar](200) NULL,
	[Value3] [nvarchar](200) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_CodedItemXRefItem_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
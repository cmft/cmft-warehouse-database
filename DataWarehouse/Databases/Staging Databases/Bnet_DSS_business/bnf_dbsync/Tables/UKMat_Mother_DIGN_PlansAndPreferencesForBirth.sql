﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_PlansAndPreferencesForBirth](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
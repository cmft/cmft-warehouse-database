﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_MicrobiologicalTest](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[Result_GBS] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_dbsync].[NetworkLocations](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[CareLocationID] [uniqueidentifier] NULL,
	[Created] [datetime] NULL,
	[nhsCode] [nvarchar](50) NULL,
	[Name] [nvarchar](500) NULL,
	[shortName] [nvarchar](500) NULL,
	[nnndir_unitid] [int] NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
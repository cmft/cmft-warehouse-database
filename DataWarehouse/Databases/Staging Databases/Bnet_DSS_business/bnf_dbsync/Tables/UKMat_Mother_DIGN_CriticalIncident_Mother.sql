﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_CriticalIncident_Mother](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[Date] [datetime] NULL,
	[TypeDetails] [nvarchar](100) NULL,
	[Type] [nvarchar](100) NULL,
	[Details] [nvarchar](max) NULL,
	[LoggedBy] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [bnf_dbsync].[CareLocnStatus](
	[EntityID] [uniqueidentifier] NOT NULL,
	[DateLastBabyUpdatedLocal] [datetime] NULL,
	[DateInvoicedLocal] [datetime] NULL,
	[InvoiceOrderNumber] [nvarchar](200) NULL,
	[PurchaseOrderNumber] [nvarchar](200) NULL,
	[Paid_Stored] [nvarchar](10) NULL,
	[DatePaidLocal] [datetime] NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_CareLocnStatus_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
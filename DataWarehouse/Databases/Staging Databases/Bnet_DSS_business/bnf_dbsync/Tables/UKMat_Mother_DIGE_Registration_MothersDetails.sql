﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGE_Registration_MothersDetails](
	[EntityID] [uniqueidentifier] NOT NULL,
	[Address1Residential] [nvarchar](400) NULL,
	[Address2Residential] [nvarchar](400) NULL,
	[Address3Residential] [nvarchar](400) NULL,
	[Address4Residential] [nvarchar](400) NULL,
	[PostcodeResidential] [nvarchar](400) NULL,
	[EmailAddress] [nvarchar](280) NULL,
	[PrimaryLanguage] [nvarchar](100) NULL,
	[EmploymentStatus] [nvarchar](100) NULL,
	[SupportStatusAtBooking] [nvarchar](100) NULL,
	[Religion] [nvarchar](100) NULL,
	[ReligionScotland] [nvarchar](100) NULL,
	[EmploymentStatusPartner] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
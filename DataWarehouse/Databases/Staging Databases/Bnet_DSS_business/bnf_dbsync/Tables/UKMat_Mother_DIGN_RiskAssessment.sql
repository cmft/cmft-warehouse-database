﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_RiskAssessment](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[RiskAssessmentPeriod] [nvarchar](100) NULL,
	[CurrentPregnancyRiskFactors] [nvarchar](50) NULL,
	[Risk] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
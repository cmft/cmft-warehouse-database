﻿CREATE TABLE [bnf_dbsync].[NNUVaccinations](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[VaccinationsGiven] [nvarchar](500) NULL,
	[VaccinationNotes] [nvarchar](4000) NULL,
	[BCGVaccinations] [nvarchar](100) NULL,
	[BCGGiven] [datetime] NULL,
	[BCGBatch] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
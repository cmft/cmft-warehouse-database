﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_AntiD](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[ConsentObtained] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
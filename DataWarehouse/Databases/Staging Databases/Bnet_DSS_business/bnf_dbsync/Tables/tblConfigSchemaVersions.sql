﻿CREATE TABLE [bnf_dbsync].[tblConfigSchemaVersions](
	[ConfigName] [nvarchar](100) NOT NULL,
	[DAFID] [nvarchar](100) NOT NULL,
	[SchemaVersion] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL
) ON [PRIMARY]
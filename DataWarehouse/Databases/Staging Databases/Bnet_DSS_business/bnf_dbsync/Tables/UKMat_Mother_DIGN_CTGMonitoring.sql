﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_CTGMonitoring](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[Fetus1_Opinion] [nvarchar](100) NULL,
	[Fetus2_Opinion] [nvarchar](100) NULL,
	[Fetus3_Opinion] [nvarchar](100) NULL,
	[Fetus4_Opinion] [nvarchar](100) NULL,
	[Fetus5_Opinion] [nvarchar](100) NULL,
	[Fetus6_Opinion] [nvarchar](100) NULL,
	[Fetus7_Opinion] [nvarchar](100) NULL,
	[Fetus8_Opinion] [nvarchar](100) NULL,
	[Fetus9_Opinion] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
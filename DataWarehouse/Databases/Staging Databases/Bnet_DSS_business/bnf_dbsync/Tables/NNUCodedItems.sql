﻿CREATE TABLE [bnf_dbsync].[NNUCodedItems](
	[EntityID] [uniqueidentifier] NOT NULL,
	[CodeType] [nvarchar](20) NOT NULL,
	[EpisodePoint] [nvarchar](20) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[NumDays] [int] NULL,
	[FirstDate] [datetime] NULL,
	[LastDate] [datetime] NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUCodedItems_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
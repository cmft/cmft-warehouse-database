﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_Admission](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[LengthOfStayHours] [int] NULL,
	[ReasonMainDisplay] [nvarchar](100) NULL,
	[ReasonMainStore] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
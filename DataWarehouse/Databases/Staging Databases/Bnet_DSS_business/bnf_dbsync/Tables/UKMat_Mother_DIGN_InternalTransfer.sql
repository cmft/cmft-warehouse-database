﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_InternalTransfer](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[TransferredTo] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
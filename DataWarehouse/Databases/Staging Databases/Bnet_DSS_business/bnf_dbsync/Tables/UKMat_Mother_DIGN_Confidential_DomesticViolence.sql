﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_Confidential_DomesticViolence](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[DomesticViolence] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
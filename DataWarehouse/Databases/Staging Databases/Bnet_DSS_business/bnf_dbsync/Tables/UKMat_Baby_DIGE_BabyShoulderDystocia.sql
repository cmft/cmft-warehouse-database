﻿CREATE TABLE [bnf_dbsync].[UKMat_Baby_DIGE_BabyShoulderDystocia](
	[EntityID] [uniqueidentifier] NOT NULL,
	[Anticipated] [nvarchar](100) NULL,
	[Grade] [nvarchar](100) NULL,
	[EmergencyHelpSummoned] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
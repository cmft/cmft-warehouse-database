﻿CREATE TABLE [bnf_dbsync].[tblSchemaVersion](
	[SchemaVersion] [int] NOT NULL,
	[ReleaseDate] [datetime] NOT NULL,
	[Comments] [nvarchar](500) NULL
) ON [PRIMARY]
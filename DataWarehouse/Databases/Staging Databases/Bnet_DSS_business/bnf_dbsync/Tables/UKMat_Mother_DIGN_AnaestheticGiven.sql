﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_AnaestheticGiven](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[AnaestheticGiven_Display] [nvarchar](100) NULL,
	[AnaestheticGiven] [nvarchar](100) NULL,
	[RecoveryCareRequired] [nvarchar](1) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_dbsync].[PortalOutEmail](
	[EntityID] [uniqueidentifier] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[SentFrom] [nvarchar](max) NULL,
	[SentTo] [nvarchar](max) NOT NULL,
	[CC] [nvarchar](max) NULL,
	[Body] [nvarchar](max) NULL,
	[Attachments] [varbinary](max) NULL,
	[DateSent] [datetime] NULL,
	[Subject] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
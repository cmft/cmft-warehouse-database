﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_VaginalExamination](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[ConsentObtained] [nvarchar](100) NULL,
	[CervicalStretchAndMembranesSweepAccepted] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
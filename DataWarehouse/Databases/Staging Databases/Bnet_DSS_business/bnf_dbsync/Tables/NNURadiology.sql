﻿CREATE TABLE [bnf_dbsync].[NNURadiology](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[RadiologyInvestigations] [nvarchar](4000) NULL,
	[RadiologyNotes] [nvarchar](4000) NULL,
	[RadiologyNotesForDischargeLetter] [nvarchar](4000) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNURadiology_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
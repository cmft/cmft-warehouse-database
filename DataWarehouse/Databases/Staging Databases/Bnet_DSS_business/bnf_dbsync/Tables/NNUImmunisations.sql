﻿CREATE TABLE [bnf_dbsync].[NNUImmunisations](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[ImmunisationsGiven] [nvarchar](4000) NULL,
	[ImmunisationsNotes] [nvarchar](4000) NULL,
	[ImmunisationsNotesForDischargeLetter] [nvarchar](4000) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUImmunisations_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
﻿CREATE TABLE [bnf_dbsync].[NNUBloodSpotScreening](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[Consent] [nvarchar](1) NULL,
	[BiochemicalScreeningTakenFor] [nvarchar](200) NULL,
	[ScreeningTaken] [nvarchar](1) NULL,
	[ScreeningSent] [nvarchar](1) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUBloodSpotScreening_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
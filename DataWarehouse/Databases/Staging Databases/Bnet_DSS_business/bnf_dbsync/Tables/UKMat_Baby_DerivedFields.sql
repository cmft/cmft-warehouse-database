﻿CREATE TABLE [bnf_dbsync].[UKMat_Baby_DerivedFields](
	[EntityID] [uniqueidentifier] NOT NULL,
	[MotherEntityID] [uniqueidentifier] NULL,
	[AllCriticalIncidents] [nvarchar](4000) NULL,
	[SkinToSkinContact_Interval_Hours] [int] NULL,
	[SkinToSkinContact_Interval_Minutes] [int] NULL,
	[FirstFeed_Interval_Hours] [int] NULL,
	[FirstFeed_Interval_Minutes] [int] NULL,
	[FirstFeed] [nvarchar](4000) NULL,
	[ReasonNotFedWithin1Hour] [nvarchar](4000) NULL,
	[FeedTransferToCommunity] [nvarchar](4000) NULL,
	[FeedTransferToHealthWorker] [nvarchar](4000) NULL,
	[PresentationImmediatelyBeforeDelivery] [nvarchar](4000) NULL,
	[HeadToBody_Interval_Hours] [int] NULL,
	[HeadToBody_Interval_Minutes] [int] NULL,
	[AdmittedToNNU] [nvarchar](10) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_dbsync].[NNUCranialUSS](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[ScanCarriedOutBy] [nvarchar](250) NULL,
	[TestCarriedOutByDesignation] [nvarchar](250) NULL,
	[NormalScan] [nvarchar](1) NULL,
	[RightIVH] [nvarchar](1) NULL,
	[RightDilation] [nvarchar](1) NULL,
	[RightPorencephalic] [nvarchar](1) NULL,
	[LeftIVH] [nvarchar](1) NULL,
	[LeftDilation] [nvarchar](1) NULL,
	[LeftPorencephalic] [nvarchar](1) NULL,
	[PVL] [nvarchar](1) NULL,
	[Hydrocephalus] [nvarchar](1) NULL,
	[Comments] [nvarchar](500) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUCranialUSS_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
﻿CREATE TABLE [bnf_dbsync].[UKMat_Baby_DIGN_BabyBloodTestsAndResults](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[ConsentObtained] [nvarchar](100) NULL,
	[Location] [nvarchar](100) NULL,
	[TestsPeformed] [nvarchar](100) NULL,
	[Result_Haemoglobin] [decimal](18, 1) NULL,
	[Result_Haemoglobin_gl] [int] NULL,
	[Result_FBC_WCC] [decimal](18, 2) NULL,
	[Result_FBC_Platelets] [decimal](18, 2) NULL,
	[Result_FBC_Platelets_ml] [decimal](18, 2) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [bnf_dbsync].[CodedItemDetail](
	[EntityID] [uniqueidentifier] NOT NULL,
	[ID] [nvarchar](50) NULL,
	[Name] [nvarchar](500) NULL,
	[CodeTypeName] [nvarchar](250) NULL,
	[CodeTypeCode] [nvarchar](50) NULL,
	[Active_Stored] [nvarchar](20) NULL,
	[Keywords] [nvarchar](500) NULL,
	[Categories] [nvarchar](500) NULL,
	[Notes] [nvarchar](max) NULL,
	[ParamsXML] [nvarchar](max) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_CodedItemDetail_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
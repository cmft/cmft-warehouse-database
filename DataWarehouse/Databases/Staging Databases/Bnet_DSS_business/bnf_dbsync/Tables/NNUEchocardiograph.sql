﻿CREATE TABLE [bnf_dbsync].[NNUEchocardiograph](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[EchoComments] [nvarchar](4000) NULL,
	[TestCarriedOutBy] [nvarchar](250) NULL,
	[TestCarriedOutByDesignation] [nvarchar](250) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUEchocardiograph_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
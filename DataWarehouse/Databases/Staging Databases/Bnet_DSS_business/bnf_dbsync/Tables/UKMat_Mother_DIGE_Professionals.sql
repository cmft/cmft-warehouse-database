﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGE_Professionals](
	[EntityID] [uniqueidentifier] NOT NULL,
	[NamedMidwife] [nvarchar](100) NULL,
	[NamedConsultant] [nvarchar](100) NULL,
	[NationalCode_UsualGP] [nvarchar](100) NULL,
	[Surname_UsualGP] [nvarchar](500) NULL,
	[PracticeName_UsualGP] [nvarchar](500) NULL,
	[Address1_UsualGP] [nvarchar](500) NULL,
	[Address2_UsualGP] [nvarchar](500) NULL,
	[Address3_UsualGP] [nvarchar](500) NULL,
	[Address4_UsualGP] [nvarchar](500) NULL,
	[Postcode_UsualGP] [nvarchar](500) NULL,
	[TelephoneNumber_Main_UsualGP] [nvarchar](100) NULL,
	[PracticeCode_UsualGP] [nvarchar](500) NULL,
	[PCTCode_UsualGP] [nvarchar](500) NULL,
	[CCGCode_UsualGP] [nvarchar](500) NULL,
	[LeadProfessional_Type_AtBooking] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
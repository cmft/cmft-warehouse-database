﻿CREATE TABLE [bnf_dbsync].[Networks](
	[EntityID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[NetworkType] [nvarchar](50) NULL,
	[NetworkUserCredentials] [nvarchar](10) NULL,
	[UKRegion] [nvarchar](500) NULL,
	[nnndirDBPatID] [int] NULL,
	[Description] [nvarchar](4000) NULL,
	[CMedNotes] [nvarchar](4000) NULL,
	[ManagerName] [nvarchar](200) NULL,
	[ManagerEmail] [nvarchar](200) NULL,
	[ManagerTel] [nvarchar](200) NULL,
	[ManagerFax] [nvarchar](200) NULL,
	[ManagerAddress] [nvarchar](2000) NULL,
	[RecordTimestamp] [datetime] NOT NULL,
	[OwnServiceDesk] [nvarchar](1) NULL
) ON [PRIMARY]
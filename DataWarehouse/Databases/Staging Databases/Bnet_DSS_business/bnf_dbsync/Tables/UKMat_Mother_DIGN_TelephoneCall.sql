﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_TelephoneCall](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[Caller1] [nvarchar](100) NULL,
	[Caller2] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
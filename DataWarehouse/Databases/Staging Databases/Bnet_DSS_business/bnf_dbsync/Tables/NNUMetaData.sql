﻿CREATE TABLE [bnf_dbsync].[NNUMetaData](
	[EntityID] [uniqueidentifier] NOT NULL,
	[xmlDailySummary] [nvarchar](max) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUMetaData_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
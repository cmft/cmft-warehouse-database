﻿CREATE TABLE [bnf_dbsync].[NNUHearingTest](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[HearingTestScreeningModeRight] [nvarchar](20) NULL,
	[HearingTestRightResult] [nvarchar](20) NULL,
	[HearingTestScreeningMode] [nvarchar](20) NULL,
	[HearingTestLeftResult] [nvarchar](20) NULL,
	[HearingTestFollowUpNeeded] [nvarchar](50) NULL,
	[HearingTestNotes] [nvarchar](4000) NULL,
	[HearingTestNotesForDischargeLetter] [nvarchar](4000) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUHearingTest_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
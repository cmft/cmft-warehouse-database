﻿CREATE TABLE [bnf_dbsync].[tblSyncTableIndex](
	[TableName] [nvarchar](250) NOT NULL,
	[ConfigName] [nvarchar](200) NOT NULL,
	[DigName] [nvarchar](200) NOT NULL,
	[CurrentConfigVersion] [nvarchar](20) NOT NULL,
	[SyncToCurrentVersionDate] [datetime] NOT NULL CONSTRAINT [DF_tblSyncTableIndex_LastSyncDate]  DEFAULT (getutcdate()),
	[CreatedWithConfigVersion] [nvarchar](20) NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_tblSyncTableIndex_CreatedDate]  DEFAULT (getutcdate()),
	[DropRequestedByAdmin] [bit] NOT NULL CONSTRAINT [DF_tblSyncTableIndex_DropRequestedByAdmin]  DEFAULT ((0))
) ON [PRIMARY]
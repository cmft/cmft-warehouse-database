﻿CREATE TABLE [bnf_dbsync].[NNUCommunityFollowUp](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[weight] [int] NULL,
	[HeadCircumference] [decimal](10, 1) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUCommunityFollowUp_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
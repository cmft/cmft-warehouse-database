﻿CREATE TABLE [bnf_dbsync].[NNUNEC](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[XRayPerformed] [nvarchar](1) NULL,
	[NEC] [nvarchar](50) NULL,
	[XRayAppearances] [nvarchar](250) NULL,
	[ClinicalFindings] [nvarchar](250) NULL,
	[TransferredManagementNEC] [nvarchar](1) NULL,
	[LaparotomyPerformed] [nvarchar](10) NULL,
	[VisualInspectionConfirmationNEC] [nvarchar](5) NULL,
	[HistologyConfirmationNEC] [nvarchar](25) NULL,
	[PeritonealDrainInsertedNEC] [nvarchar](10) NULL,
	[NECNotes] [nvarchar](4000) NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_NNUNEC_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
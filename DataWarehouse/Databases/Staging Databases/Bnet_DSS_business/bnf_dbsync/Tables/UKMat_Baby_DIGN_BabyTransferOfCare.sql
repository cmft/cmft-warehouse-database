﻿CREATE TABLE [bnf_dbsync].[UKMat_Baby_DIGN_BabyTransferOfCare](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[TransferType] [nvarchar](100) NULL,
	[SMR02_NeonatalIndicator] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
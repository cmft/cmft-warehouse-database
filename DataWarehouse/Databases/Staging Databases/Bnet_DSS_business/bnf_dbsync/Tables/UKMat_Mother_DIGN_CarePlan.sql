﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_CarePlan](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[HospitalAttachedTo] [nvarchar](100) NULL,
	[HospitalAttachedToID] [uniqueidentifier] NULL,
	[NamedConsultant] [nvarchar](100) NULL,
	[NamedMidwife] [nvarchar](100) NULL,
	[Date] [datetime] NULL,
	[UpdateType] [nvarchar](100) NULL,
	[AntenatalCareType] [nvarchar](100) NULL,
	[DeliveryPlan] [nvarchar](100) NULL,
	[PlaceOfBirth] [nvarchar](100) NULL,
	[ActualPlaceOfBirth] [nvarchar](100) NULL,
	[LeadProfessionalType] [nvarchar](100) NULL,
	[Team] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
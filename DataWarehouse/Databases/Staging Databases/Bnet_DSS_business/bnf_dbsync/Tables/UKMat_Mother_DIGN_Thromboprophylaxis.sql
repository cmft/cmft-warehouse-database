﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_Thromboprophylaxis](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[Date] [datetime] NULL,
	[EarlyMobilisationAndHydration] [nvarchar](100) NULL,
	[TEDStockings] [nvarchar](100) NULL,
	[MedicationPrescribed] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
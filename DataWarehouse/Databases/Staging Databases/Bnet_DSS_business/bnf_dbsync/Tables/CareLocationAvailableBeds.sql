﻿CREATE TABLE [bnf_dbsync].[CareLocationAvailableBeds](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[Date] [datetime] NULL,
	[AvailableBeds] [int] NULL,
	[FundedBeds] [int] NULL,
	[CancelledOps] [int] NULL,
	[RecordTimestamp] [datetime] NOT NULL CONSTRAINT [DF_CareLocationAvailableBeds_RecordTimestamp]  DEFAULT (getutcdate())
) ON [PRIMARY]
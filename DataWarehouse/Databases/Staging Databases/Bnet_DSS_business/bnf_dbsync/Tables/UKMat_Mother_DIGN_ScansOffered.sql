﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_ScansOffered](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[TestsOffered] [nvarchar](100) NULL,
	[TestsOfferedScotland] [nvarchar](100) NULL,
	[TestsPeformed] [nvarchar](4000) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
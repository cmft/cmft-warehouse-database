﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_TransferOfCare](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[DateNoteTime] [datetime] NULL,
	[TransferType] [nvarchar](100) NULL,
	[Baby1_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[Baby2_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[Baby3_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[Baby4_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[Baby5_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[Baby6_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[Baby7_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[Baby8_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[Baby9_ReasonNotTransferredWithMother] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
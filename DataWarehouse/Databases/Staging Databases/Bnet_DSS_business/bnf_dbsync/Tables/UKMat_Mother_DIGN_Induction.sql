﻿CREATE TABLE [bnf_dbsync].[UKMat_Mother_DIGN_Induction](
	[EntityID] [uniqueidentifier] NOT NULL,
	[noteTime] [datetime] NOT NULL,
	[Date] [datetime] NULL,
	[InductionMethod] [nvarchar](100) NULL,
	[RecordTimestamp] [datetime] NOT NULL
) ON [PRIMARY]
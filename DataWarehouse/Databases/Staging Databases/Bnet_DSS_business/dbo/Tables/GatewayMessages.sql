﻿CREATE TABLE [dbo].[GatewayMessages](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[InterfaceID] [nvarchar](50) NOT NULL,
	[Live] [bit] NOT NULL,
	[SenderID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [bnf_report].[tblReportDefinitions](
	[ReportDefinitionId] [uniqueidentifier] NOT NULL,
	[CreateDateUTC] [datetime] NOT NULL CONSTRAINT [DF_tblReportDefinitions_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedForUserId] [uniqueidentifier] NOT NULL,
	[ReportDefinitionXml] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [bnf_report].[tblPatientReportCache](
	[CachedReportId] [nvarchar](80) NOT NULL,
	[EntityId] [uniqueidentifier] NOT NULL,
	[ScriptName] [nvarchar](200) NULL,
	[ReportBlob] [varbinary](max) NOT NULL,
	[FileSize] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
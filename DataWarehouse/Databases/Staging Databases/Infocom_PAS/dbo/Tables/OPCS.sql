﻿CREATE TABLE [dbo].[OPCS](
	[OPCS] [varchar](5) NOT NULL,
	[PROCDESC] [varchar](60) NOT NULL,
	[CHGBL] [char](1) NOT NULL,
	[EFG] [char](4) NULL,
	[LDF] [char](1) NOT NULL,
	[Diagnostic_Flag] [char](1) NULL,
	[Diag_Line_Number] [char](4) NULL
) ON [PRIMARY]
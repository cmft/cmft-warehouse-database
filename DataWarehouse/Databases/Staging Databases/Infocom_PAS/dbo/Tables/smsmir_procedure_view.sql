﻿CREATE TABLE [dbo].[smsmir_procedure_view](
	[ImpCode] [varchar](4) NULL,
	[CodeType] [varchar](2) NOT NULL,
	[Code] [varchar](5) NOT NULL,
	[Descr] [varchar](55) NULL,
	[AltDesc] [varchar](60) NULL,
	[LglDelInd] [varchar](255) NULL,
	[Chargeable] [varchar](3) NULL,
	[EffDate] [datetime] NULL
) ON [PRIMARY]
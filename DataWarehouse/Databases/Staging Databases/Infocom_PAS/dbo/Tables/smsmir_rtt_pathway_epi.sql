﻿CREATE TABLE [dbo].[smsmir_rtt_pathway_epi](
	[ImpCode] [varchar](4) NOT NULL,
	[PathwayNo] [nvarchar](25) NULL,
	[InternalNo] [varchar](20) NOT NULL,
	[EpisodeNo] [varchar](20) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[smsmir_preadmission](
	[InternalNo] [varchar](20) NOT NULL,
	[EpisodeNo] [varchar](20) NOT NULL,
	[PreAdmitDate1] [datetime2](7) NULL,
	[ExpAdmitDate] [datetime2](7) NULL,
	[OutcomeDate] [nvarchar](25) NULL,
	[OutcomeCode] [nvarchar](3) NULL,
	[PreAdmCancRsn] [nvarchar](6) NULL,
	[Hospital] [nvarchar](4) NULL,
	[Consultant] [nvarchar](6) NULL,
	[Specialty] [nvarchar](4) NULL,
	[ExpWard] [nvarchar](4) NULL,
	[PrimProcedure] [nvarchar](7) NULL,
	[EpPostCode] [nvarchar](10) NULL,
	[MethAdm] [nvarchar](2) NULL,
	[IntMan] [nvarchar](1) NULL,
	[WLDate] [nvarchar](10) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
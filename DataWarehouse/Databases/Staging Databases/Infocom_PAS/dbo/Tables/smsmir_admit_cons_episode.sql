﻿CREATE TABLE [dbo].[smsmir_admit_cons_episode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InternalNo] [varchar](20) NOT NULL,
	[AdmitDate] [datetime2](7) NULL,
	[EpisodeNo] [varchar](20) NOT NULL,
	[PrimaryProc] [varchar](10) NULL,
	[CEStartDate] [datetime] NULL,
	[CEEndDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[smsmir_waiting_list_activity](
	[InternalNo] [varchar](20) NOT NULL,
	[EpisodeNo] [varchar](20) NOT NULL,
	[WLActvStartDate] [datetime2](7) NULL,
	[WLActvType] [varchar](3) NOT NULL,
	[WLActvEndDate] [datetime2](7) NULL,
	[SuspInitBy] [varchar](3) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
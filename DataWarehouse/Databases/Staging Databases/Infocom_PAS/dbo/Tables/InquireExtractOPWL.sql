﻿CREATE TABLE [dbo].[InquireExtractOPWL](
	[SourcePatientNo] [varchar](20) NOT NULL,
	[SourceEncounterNo] [varchar](20) NOT NULL,
	[AppointmentType] [varchar](5) NULL,
	[DateOnList] [varchar](20) NULL,
	[TreatmentCode] [varchar](10) NULL,
	[OpRegDate] [varchar](20) NULL
) ON [PRIMARY]
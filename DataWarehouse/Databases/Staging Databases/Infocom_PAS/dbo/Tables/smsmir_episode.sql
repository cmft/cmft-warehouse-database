﻿CREATE TABLE [dbo].[smsmir_episode](
	[InternalNo] [varchar](20) NOT NULL,
	[ImpCode] [varchar](4) NOT NULL,
	[EpisodeNo] [varchar](20) NOT NULL,
	[CaseNoteNo] [nvarchar](14) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[smsmir_specialty](
	[ImpCode] [varchar](4) NOT NULL,
	[Specialty] [varchar](4) NOT NULL,
	[Directorate] [int] NULL,
	[SpecDesc] [varchar](32) NULL,
	[SpecGroup] [varchar](4) NULL,
	[HAACode] [varchar](2) NULL,
	[DoHCode] [varchar](4) NULL,
	[SupraReg] [varchar](2) NULL,
	[SpecType] [varchar](2) NULL,
	[GPFHExempt] [varchar](3) NULL,
	[SpecScInt] [varchar](5) NULL,
	[SpecScLoc] [varchar](1) NULL,
	[LglDelInd] [float] NULL,
	[KarsID] [varchar](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[cmancons_Directorates](
	[Hospital] [varchar](255) NULL,
	[Specialty] [varchar](255) NULL,
	[DoHCode] [varchar](255) NULL,
	[spec] [varchar](255) NULL,
	[Directorate] [varchar](255) NULL,
	[Division] [varchar](255) NULL,
	[cmraspec] [varchar](255) NULL,
	[diag] [varchar](255) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[cmancons_repcat](
	[CodeLink] [nvarchar](8) NOT NULL,
	[Code] [nvarchar](8) NOT NULL,
	[RepCat] [nvarchar](4) NULL,
	[RepCatDesc] [nvarchar](30) NULL
) ON [PRIMARY]
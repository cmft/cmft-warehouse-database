﻿CREATE VIEW [dbo].[v_smsmir_rtt_pathway_epi] AS

/******************************************************************************
**  Name: dbo.v_smsmir_rtt_pathway_epi
**  Purpose: RTT Infocom Conversion Pathway number lookup 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/

	SELECT
		 ImpCode		= 'MARI'
		,PathwayNo		= PathwayNumber
		,InternalNo		= InternalPatientNumber
		,EpisodeNo		= EpisodeNumber
		
	FROM
	
		PAS.Inquire.RTTPTEPIPATHWAY
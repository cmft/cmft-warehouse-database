﻿CREATE VIEW [dbo].[v_smsmir_preadmission] AS

/******************************************************************************
**  Name: dbo.v_smsmir_preadmission
**  Purpose: RTT Infocom Conversion Preadmissions 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/

	SELECT 
	 InternalNo = Preadmission.InternalPatientNumber
	,EpisodeNo = Preadmission.EpisodeNumber

	,PreAdmitDate1 =
		case
		when Preadmission.ActivityTime = '24:00'
			then dateadd(minute , 1 , convert(datetime2(7) , Preadmission.ActivityDate + ' 23:59', 103))
		else convert(datetime2(7) , Preadmission.ActivityDate + ' ' + Preadmission.ActivityTime, 103)
		end

	,ExpAdmitDate =
		case
		when Preadmission.TCITime = '24:00'
			then dateadd(minute , 1 , convert(datetime2(7) , Preadmission.TCIDate + ' 23:59',103))
		else convert(datetime2(7) , Preadmission.TCIDate + ' ' + Preadmission.TCITime,103)
		end
	,OutcomeDate = PreadmitCancel.ActivityDate + ' ' + PreadmitCancel.ActivityTime
	,OutcomeCode = 
		CASE
			WHEN PreadmitCancel.CancelReasonCd = 'OTPT' AND PreadmitCancel.CancelledBy IS NULL THEN 'P'
			WHEN PreadmitCancel.CancelReasonCd = 'DEAD' THEN 'HBD'
			ELSE PreadmitCancel.CancelledBy
		END
	
	,PreAdmCancRsn = PreadmitCancel.CancelReasonCd
		--CASE
		--	WHEN		PreadmitCancel.CancelReasonCd = 'TCIB' 
		--			AND PreadmitCancel.Urgency < 4 
		--			AND PreadmitCancel.CharterCancelDeferInd = 'NO'
		--		THEN 'URG'
		--	ELSE	PreadmitCancel.CancelReasonCd
		--END
	,Preadmission.Hospital
	,Preadmission.Consultant
	,Preadmission.Specialty
	,ExpWard = Preadmission.Ward
	,PrimProcedure = WLEntry.IntendedProc
	,EpPostCode	= patient.PtAddrPostCode
	,MethAdm = WLEntry.MethodOfAdm
	,IntMan = WLEntry.IntdMgmt
	,WLDate = WLEntry.OriginalDateOnList
from
	PAS.Inquire.WLACTIVITY Preadmission

--LEFT join PAS.Inquire.EPIREGISTRATION Registration
--on	Registration.InternalPatientNumber = Preadmission.InternalPatientNumber
--and	Registration.EpisodeNumber = Preadmission.EpisodeNumber

inner join PAS.Inquire.PATDATA Patient
on	Patient.InternalPatientNumber = Preadmission.InternalPatientNumber

inner join PAS.Inquire.WLENTRY WLEntry
on	WLEntry.InternalPatientNumber = Preadmission.InternalPatientNumber
and	WLEntry.EpisodeNumber = Preadmission.EpisodeNumber

OUTER APPLY								-- Method to return the next Preadmit Cancelled row
(
	SELECT *
	FROM
	(
		SELECT							-- Returns the row containing the Min(EpsActvDtimeInt), ie next row 				
			 A.ActivityDate				-- after the current Preadmission row
			,A.ActivityTime				-- A more obvious solution would be to use a TOP 1 and ORDER BY  
			,A.CancelledBy				-- but this leads to un-predictable results and is best avoided
			,CancelReasonCd = A.CharterCancelCode
			,A.Activity
			,A.CharterCancelDeferInd
			,A.Urgency
		FROM PAS.Inquire.WLACTIVITY A
		LEFT JOIN PAS.Inquire.WLACTIVITY B
			ON		A.InternalPatientNumber = B.InternalPatientNumber
				AND A.EpisodeNumber = B.EpisodeNumber
				AND B.EpsActvDtimeInt < A.EpsActvDtimeInt
				AND B.EpsActvDtimeInt > Preadmission.EpsActvDtimeInt
		WHERE
			A.InternalPatientNumber = Preadmission.InternalPatientNumber
		and	A.EpisodeNumber = Preadmission.EpisodeNumber
		and A.EpsActvDtimeInt > Preadmission.EpsActvDtimeInt		-- Want the row after current Preadmission row
		and B.InternalPatientNumber IS NULL
	) PADM
	
	WHERE 
		PADM.Activity = 'Preadmit Cancel'
) PreadmitCancel

--left join PAS.Inquire.WLACTIVITY PreadmitCancel
--on	PreadmitCancel.InternalPatientNumber = Preadmission.InternalPatientNumber
--and	PreadmitCancel.EpisodeNumber = Preadmission.EpisodeNumber
--and	PreadmitCancel.PrevActivityDtimeInt = Preadmission.EpsActvDtimeInt
--and	PreadmitCancel.EpsActvTypeInt = 6

where
	Preadmission.EpsActvTypeInt = 5
--and
--	Preadmission.InternalPatientNumber = 2512924
--and	Preadmission.EpisodeNumber = 12

--order by
--	Preadmission.EpsActvDtimeInt
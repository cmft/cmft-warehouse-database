﻿CREATE VIEW [dbo].[v_smsmir_procedure_view] AS

/******************************************************************************
**  Name: dbo.v_smsmir_procedure_view
**  Purpose: RTT Infocom Conversion Procedure lookup
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
** 16.10.12      MH       Changed to query the PAS.Inquire table directly
******************************************************************************/

	SELECT
	
		 ImpCode		= NULL
		,CodeType		= 'OP'
		,Code			= OPCS4ID
		,Descr			= Opcs4Dx3
		,AltDesc		= Opcs4Dx4
		,LglDelInd		= Opcs4Ldf
		,Chargeable		= CASE WHEN Opcs4GpfhChargeable = 1 THEN 'YES' ELSE NULL END
		,EffDate		= CAST(Opcs4GpfhEffDate AS DATETIME)

	FROM
		PAS.Inquire.OPCS4
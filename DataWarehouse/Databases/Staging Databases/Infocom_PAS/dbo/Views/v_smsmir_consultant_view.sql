﻿CREATE VIEW [dbo].[v_smsmir_consultant_view] AS

/******************************************************************************
**  Name: dbo.v_smsmir_consultant_view
**  Purpose: RTT Infocom Conversion Consultant lookup
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/
		
	SELECT		
		 ImpCode			= 'MARI'
		,DrType				= 'CON'
		,DrCode				= CONSMASTID
		,Title				= ForenamePfx
		,Surname			= Surname
		,Initials			= Inits
		,GMCCode			= GMCCode
		,DOHCode			= ConsultantCode2
		,ExtConsFlag		= ExternalCons
		,ExtConsProvCode	= ProviderCode
		,Specialty1			= PrimarySpecialty
		,Specialty2			= NULL
		,Specialty3			= NULL
		,Specialty4			= NULL
		,Specialty5			= NULL
		,Specialty6			= NULL
		,Specialty7			= NULL
		,Specialty8			= NULL
		,LglDelInd			= ABS(Deleted)
	FROM
		[$(PAS)].Inquire.CONSMAST
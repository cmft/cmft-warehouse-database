﻿CREATE VIEW [dbo].[v_smsmir_patient] AS

/******************************************************************************
**  Name: dbo.v_smsmir_patient
**  Purpose: RTT Infocom Conversion Patient lookup
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/
		
	SELECT		

		 ImpCode		= 'MARI'
		,InternalNo		= InternalPatientNumber
		,DOB			= [Utility].[fn_InquireDateTimeConvert](PtDOB,NULL)
		,DeathInd		= PtDeathIndInt
		,DeathYear		= YEAR([Utility].[fn_InquireDateTimeConvert](PtDateOfDeath,NULL))
		,DateOfDeath	= [Utility].[fn_InquireDateTimeConvert](PtDateOfDeath,NULL)
		,Sex			= Sex
		,MaritalStatus	= MaritalStatus
		,Surname		= Surname
		,Forenames		= Forenames
		,Title			= Title
		,NextOfKin		= NokName
		,BloodGroup		= NULL
		,Rhesus			= NULL
		,RiskFactor1	= NULL
		,EthnicOrigin	= EthnicType
		,RiskFactor2	= NULL
		,Religion		= Religion
		,PrevSurname	= PreviousSurname1
		,NHSNo			= NHSNumber
		,NHSNoStatus	= NHSNumberStatus
		,DOR			= DistrictOfResidenceCode
		,DistrictNo		= DistrictNumber
		,MilitaryNo		= NULL
		,PostCode		= PtPostAddrPostcode
		,GPType			= 'GP'
		,RegGP			= GpCode
		,GDPType		= 'GDP'
		,RegGDP			= GdpCode
		,MRSAFlag		= MRSA
		,MRSADate		= MRSASetDt
		,PatSchool		= NULL
		,PatOccn		= NULL
		,Summary1		= NULL
		,Summary2		= NULL
		,Summary3		= NULL
		,BirthName		= NULL
		,PlaceOfBirth	= NULL
		,CarerRelCd		= NULL
		,CPARevDate		= NULL
		,CPALastUpDate	= NULL
		,CPAPriority	= NULL
		,CPARiskCat1	= NULL
		,CPARiskCat2	= NULL
		,CPARiskCat3	= NULL
		,CPARiskCat4	= NULL
		,CPARiskCat5	= NULL
		,CPAGAFScore	= NULL
		,CPACategoryX	= NULL
		,CPADependencyX	= NULL
		,ExtPmiAddLine1	= NULL
		,ExtPmiAddLine2	= NULL
		,ExtPmiAddLine3	= NULL
		,ExtPmiAddLine4	= NULL
		,ExtPmiAddLine5	= NULL
		,ExtPmiAddPostcd= NULL
		
	FROM
		PAS.Inquire.PATDATA
﻿CREATE VIEW [dbo].[v_smsmir_specialty] AS

/******************************************************************************
**  Name: dbo.v_smsmir_specialty
**  Purpose: RTT Infocom Conversion Specialty lookup
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
** 16.10.12      MH       Changed to query the PAS.Inquire tables directly
******************************************************************************/

	SELECT
	
		 ImpCode		= 'MARI'
		,Specialty		= SPECID
		,Directorate	= NULL
		,SpecDesc		= Description
		,SpecGroup		= SpecGroup
		,HAACode		= HaaCode
		,DoHCode		= DohCode
		,SupraReg		= SupraregCode
		,SpecType		= SpecialtyType
		,GPFHExempt		= GpfhExempt
		,SpecScInt		= ScottishIntValue
		,SpecScLoc		= ScottishLocalCode
		,LglDelInd		= ABS(MfRecStsInd)
		,KarsID			= KarsIdCode

	FROM
		PAS.Inquire.SPEC
﻿CREATE VIEW [dbo].[v_cmancons_ActiveWL] AS

/******************************************************************************
**  Name: dbo.v_cmancons_ActiveWL
**  Purpose: RTT Infocom Conversion cmancons Active Waitng List
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/

select
	 InternalNo = SourcePatientNo
	,EpisodeNo = SourceEncounterNo
	,Patient = SourcePatientNo + '~' + SourceEncounterNo
	,DistrictNo
	,Hospital = SiteCode
	,Category = AdminCategoryCode
	,NHSNo = NHSNumber

	,DeathInd =
		case
		when DateOfDeath is not null then 'YES'
		else 'NO'
		end

	,Name = PatientSurname + ', ' + PatientForename
	,Sex = SexCode
	,DOB = DateOfBirth
	,KornerWLDate = AddedToWaitingListTime
	,OriginalWLDate = AddedToWaitingListTime
	,SuspDays = CountOfDaysSuspended
	,CurrentAdmissionDays = null
	,SuspUntilDt = SuspensionEndDate
	,MostRec_SuspUntilDt = convert(datetime2 , null)
	,LastReviewDate = convert(datetime2 , null)
	,CharterWait = KornerCharterWaitMonths
	,DaysWaiting = KornerWait
	,KornerMonths = KornerWaitMonths
	,Waitband = null
	,TCIDate
	,CanTCIDate = convert(datetime2 , null)
	,PrimProcedure = IntendedPrimaryOperationCode
	,OperationText = Operation
	,CurrentStatus = WLStatus
	,Consultant = ConsultantCode
	,Specialty = SpecialtyCode
	,WLCode = WaitingListCode
	,KornerSpec = NationalSpecialtyCode
	,MethodAdmission = AdmissionMethodCode
	,IntMgt = ManagementIntentionCode
	,Urgency = PriorityCode
	,ExpWard = WardCode
	,PostCode
	,Practice = RegisteredPracticeCode
	,BookingType = BookingTypeCode
	,WLDiagGroup = null
	,ExpOpDate = convert(datetime2 , null)
	,HomePhone = null
	,WorkPhone = null
	,ShortNotice = null
	,RunStartDate = CensusDate
	,CensusDate
	,Age = null
	,CEAEpisode = null
from
	Warehouse.APC.WaitingList
where
	CensusDate = (select max(CensusDate) from Warehouse.APC.WaitingList)
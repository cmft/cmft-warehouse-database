﻿CREATE VIEW [dbo].[v_smsmir_episode] AS

/******************************************************************************
**  Name: dbo.v_smsmir_episode
**  Purpose: RTT Infocom Conversion APC Consultant Spells 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
** 22.10.12      MH       Remove filter on EpiAdd1
******************************************************************************/
	
	SELECT
		 InternalNo		= E.InternalPatientNumber
		,ImpCode		= 'MARI'
		,EpisodeNo		= E.EpisodeNumber
		,CaseNoteNo		= MAX(COALESCE(A.CaseNoteNumber, O.CaseNoteNumber, W.CaseNoteNumber,''))
		
	FROM
		[$(PAS)].Inquire.EPIREGISTRATION E
		LEFT JOIN PAS.Inquire.ADMITDISCH A  
			ON		E.EpisodeNumber			= A.EpisodeNumber
				AND	E.InternalPatientNumber = A.InternalPatientNumber
				
		LEFT JOIN PAS.Inquire.OPA O
			ON		E.InternalPatientNumber = O.InternalPatientNumber 
				AND E.EpisodeNumber			= O.EpisodeNumber
				
		LEFT JOIN PAS.Inquire.WLENTRY W
			ON		E.InternalPatientNumber = W.InternalPatientNumber 
				AND E.EpisodeNumber			= W.EpisodeNumber
	--WHERE
	--		E.EpiAdd1 IS NOT NULL
		
	GROUP BY 
		 E.InternalPatientNumber
		,E.EpisodeNumber
		
		
	--SELECT

	--	 InternalNo		= E.InternalPatientNumber
	--	,ImpCode		= 'MARI'
	--	,EpisodeNo		= E.EpisodeNumber
	--	,CaseNoteNo		= MAX(A.CaseNoteNumber)
		
	--FROM
	--	PAS.Inquire.CONSEPISODE E
	--	LEFT JOIN PAS.Inquire.ADMITDISCH A  ON
	--			E.EpisodeNumber = A.EpisodeNumber
	--		AND	E.InternalPatientNumber = A.InternalPatientNumber
		
	--GROUP BY 
	--	 E.InternalPatientNumber
	--	,E.EpisodeNumber
		
	--UNION
	
	--SELECT

	--	 O.InternalPatientNumber
	--	,'MARI'
	--	,O.EpisodeNumber
	--	,MAX(O.CaseNoteNumber)
		
	--FROM
	--	PAS.Inquire.OPA O
		
	--GROUP BY 
	--	 O.InternalPatientNumber
	--	,O.EpisodeNumber
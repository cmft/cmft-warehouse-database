﻿CREATE VIEW [dbo].[v_smsmir_admit_cons_episode] AS

/******************************************************************************
**  Name: dbo.v_smsmir_admit_cons_episode
**  Purpose: RTT Infocom Conversion APC Consultant Spells 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/

	SELECT 

		 InternalNo		= AD.InternalPatientNumber
		,AdmitDate		= CASE
							WHEN AD.AdmissionTime IN ('23:59','24:00') THEN DATEADD(MINUTE, -1,Utility.fn_InquireDateTimeConvert(AD.AdmissionDate, AD.AdmissionTime))
							ELSE Utility.fn_InquireDateTimeConvert(AD.AdmissionDate, AD.AdmissionTime)
						  END
		,EpisodeNo		= AD.EpisodeNumber
		,PrimaryProc	= FCE.KornerEpisodePrimaryProcedureCode
		,CEStartDate	= CASE
								WHEN FCE.FCEStartTime IN ('23:59','24:00') THEN DATEADD(MINUTE, -1,Utility.fn_InquireDateTimeConvert(FCE.FCEStartDate, FCE.FCEStartTime))
								ELSE Utility.fn_InquireDateTimeConvert(FCE.FCEStartDate, FCE.FCEStartTime)
						  END

		,CEEndDate		= CASE
								WHEN FCE.FCEEndTime IN ('23:59','24:00') THEN DATEADD(MINUTE, -1,Utility.fn_InquireDateTimeConvert(FCE.FCEEndDate, FCE.FCEEndTime))
								ELSE Utility.fn_InquireDateTimeConvert(FCE.FCEEndDate, FCE.FCEEndTime)
						  END
							  		
	FROM
			
	[$(PAS)].Inquire.FCEEXT FCE

	INNER JOIN PAS.Inquire.ADMITDISCH AD
		ON 	AD.InternalPatientNumber	= FCE.InternalPatientNumber
			AND	AD.EpisodeNumber			= FCE.EpisodeNumber		

	UNION ALL
	
	SELECT
		 InternalNo		= AD.InternalPatientNumber
		,AdmitDate		= CASE
							WHEN AD.AdmissionTime IN ('23:59','24:00') THEN DATEADD(MINUTE, -1,Utility.fn_InquireDateTimeConvert(AD.AdmissionDate, AD.AdmissionTime))
							ELSE Utility.fn_InquireDateTimeConvert(AD.AdmissionDate, AD.AdmissionTime)
						  END
		,EpisodeNo		= AD.EpisodeNumber
		,PrimaryProc	= NULL	
		,CEStartDate	= CASE
							WHEN Episode.EpisodeStartTime IN ('23:59','24:00') THEN DATEADD(MINUTE, -1,Utility.fn_InquireDateTimeConvert(Episode.EpisodeStartDate, Episode.EpisodeStartTime))
							ELSE Utility.fn_InquireDateTimeConvert(Episode.EpisodeStartDate, Episode.EpisodeStartTime)
						  END

		,CEEndDate	= NULL
		
	FROM
		PAS.Inquire.MIDNIGHTBEDSTATE M
		
		INNER JOIN PAS.Inquire.ADMITDISCH AD 
			ON		M.EpisodeNumber = AD.EpisodeNumber
				AND	M.InternalPatientNumber =  AD.InternalPatientNumber
				
	INNER JOIN PAS.Inquire.CONSEPISODE  Episode 
	ON	M.EpisodeNumber = Episode.EpisodeNumber
	AND	M.InternalPatientNumber = Episode.InternalPatientNumber
	
	WHERE
		Episode.EpsActvDtimeInt <= convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112) + '2400'
		AND	(
				Episode.ConsultantEpisodeEndDttm > convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112) + '2400'
			OR	Episode.ConsultantEpisodeEndDttm Is Null
			)
		AND M.StatisticsDate = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)
			
	--	 InternalNo		= SourcePatientNo
	--	,AdmitDate		= CAST(AdmissionTime AS DATETIME2(7))
	--	,EpisodeNo		= SourceSpellNo
	--	,PrimaryProc	= PrimaryOperationCode
		
	--FROM
	
	--	warehouse.APC.Encounter
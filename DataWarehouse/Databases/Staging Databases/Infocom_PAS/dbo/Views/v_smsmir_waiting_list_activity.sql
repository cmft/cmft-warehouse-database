﻿CREATE VIEW [dbo].[v_smsmir_waiting_list_activity] AS

/******************************************************************************
**  Name: dbo.v_smsmir_waiting_list_activity
**  Purpose: RTT Infocom Conversion Outpatient Referrals 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/

	SELECT
		 InternalNo			= InternalPatientNumber
		,EpisodeNo			= EpisodeNumber
		,WLActvStartDate	= DATEADD(MINUTE, CAST(SUBSTRING(WLA.EpsActvDTimeInt,11,2) AS INTEGER), DATEADD(HOUR, CAST(SUBSTRING(WLA.EpsActvDTimeInt,9,2) AS INTEGER), CAST(SUBSTRING(WLA.EpsActvDTimeInt,1,8) AS DATETIME)))
		,WLActvType			=
			CASE
				Activity
					WHEN 'WL Suspend'	THEN 'SUS'
					WHEN 'WL Transfer'	THEN 'TRA'
					WHEN 'WL Def'		THEN 'DEF'
					ELSE 'UNK'
			END
		,WLActvEndDate		= 
			CASE 
				WHEN Activity = 'WL transfer' THEN NULL 
				ELSE DATEADD(MINUTE, CAST(SUBSTRING(PREVACTIVITY.EpsActvDTimeInt,11,2) AS INTEGER), DATEADD(HOUR, CAST(SUBSTRING(PREVACTIVITY.EpsActvDTimeInt,9,2) AS INTEGER), CAST(SUBSTRING(PREVACTIVITY.EpsActvDTimeInt,1,8) AS DATETIME2(7))))
			END
		,SuspInitBy			=
			CASE
				WlSuspensionInitiator
					WHEN 1	THEN 'CON'
					WHEN 2	THEN 'PAT'
					ELSE NULL
			END
		
	FROM
	
		PAS.Inquire.WLACTIVITY WLA
		OUTER APPLY
		(
			SELECT
				 PWLA.EpsActvDTimeInt
			FROM
				PAS.Inquire.WLACTIVITY PWLA
				
			WHERE 
					PWLA.InternalPatientNumber	= WLA.InternalPatientNumber
				AND	PWLA.EpisodeNumber			= WLA.EpisodeNumber
				AND	PWLA.PrevActivityDTimeInt	= WLA.EpsActvDTimeInt
				AND PWLA.Activity NOT IN ('WL Cancel','WL Suspend','WL Transfer')
		) PREVACTIVITY
		
	WHERE
		Activity IN ('WL Suspend', 'WL Transfer', 'WL Def')
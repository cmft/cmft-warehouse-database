﻿CREATE VIEW [dbo].[v_smsmir_waiting_list] AS

/******************************************************************************
**  Name: dbo.v_smsmir_waiting_list
**  Purpose: RTT Infocom Conversion waiting list 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
** 14.01.13      MH       Tweaking of CEA Episode number and exclude where IntdMgmt column is NULL
******************************************************************************/

SELECT 
	 Hospital = CASE WHEN WLC.Hospital IS NULL THEN 'DIS' ELSE WLC.Hospital END
	,InternalNo = WLE.InternalPatientNumber
	,EpisodeNo = WLE.EpisodeNumber
	,VisitType = 'WL'
	,EpisodeNo1 = WLE.EpisodeNumber
	,CaseNoteNo = WLE.CaseNoteNumber
	,WLDate = --WLE.WLOnListDate + ' ' + COALESCE(WLE.WLOnListTime, '00:01')
		case
			when WLE.WLOnListTime = '24:00' then DATEADD(DAY, 1, CONVERT(DATETIME2(7), WLE.WLOnListDate, 103)) 
			else CONVERT(DATETIME2(7), WLE.WLOnListDate + ' ' + COALESCE(WLE.WLOnListTime, '00:01'), 103)
		end
	,WLCode = WLE.WLCode  --WLC.WaitingList --Current data from WLACTIVITY
	,RefCode = WLE.Referral
	,Urgency = WLE.Urgency
	,IntMan = WLE.IntdMgmt
	,MethAdm = WLE.MethodOfAdm
	,AdmissionReason = WLE.AdmReason
	,SpecCase = WLE.SpecialCase

	,ExpAdmitDate =
		CONVERT(DATETIME2(7)
			,WLE.WLApproxAdmissionDate + ' ' + 
			coalesce(
				case
				when WLE.WLApproxAdmissionTime = '24:00' then '23:59'
				else WLE.WLApproxAdmissionTime
				end
				,'00:01'
			)
			,103
		)

	,ExpWard = OrigWLC.Ward
	,RemovalReason = WLR.RemovalReason
	,RemovalDate = 
		case
		when WLR.ActivityTime  = '24:00'
			then dateadd(minute , 1 , convert(datetime2(7) , WLR.ActivityDate + ' 23:59', 103))
		else convert(datetime2(7) , WLR.ActivityDate + ' ' + WLR.ActivityTime, 103)
		end
	
	--CONVERT(DATETIME2(7), WLR.ActivityDate, 103)
	,EpiRefBy = EPR.ReferredBy
	,EpiCode =	
	  CASE 
		WHEN EPR.ReferredBy IN ('CNN','CON','DC')	THEN EPR.Consultant
		WHEN EPR.ReferredBy IN ('GDP')				THEN EPR.EpiGDP
		ELSE										     WLE.EpiGPCode --EPR.EpiGP
	  END

	--,EpiCode =
	--	coalesce(EPR.EpiGP , EpiGDP)

	,ConType = 'CON' -- EPIREGISTRATION?
	,Consultant = COALESCE(OrigWLC.Consultant, WLE.Consultant)
	,Specialty = coalesce(OrigWLC.Specialty, WLC.Specialty, AD.Specialty ,  WLE.Specialty) --coalesce(AD.Specialty , WLC.Specialty , WLE.Specialty)
	,Con1Type = 'CON' -- EPIREGISTRATION?
	,JointConsultant1 = WLE.JointCons1
	,JointSpecialty1 = WLE.JointSpec1
	,Con2Type = 'CON'
	,JointSpecialty2 = WLE.JointSpec2
	,JointConsultant2 = WLE.JointCons2
	,Category = WLE.Category
	,ExpLos = WLE.ExpectedLos
	,DgType = NULL
	,WlDiagGroup = WLE.DiagGroup
	,ShortNotice = WLE.ShortNotice
	,AccPerson = NULL
	,PrimProcType = NULL
	,PrimProcedure = WLE.IntendedProc
	,OperationText1 = WLE.Operation
	,OperationText2 = null
	,Transport = WLE.Transport
	,InCareInd = WLE.InCare
	,ProvDiagType = NULL
	,ProvDiagnosis = NULL

	,OrigDateOnList =
		CONVERT(DATETIME2(7)
			,WLE.OriginalDateOnList + ' ' + 
			coalesce(
				case
				when WLE.OriginalDateOnListTime = '24:00' then '23:59'
				else WLE.OriginalDateOnListTime
				end
				,'00:01'
			)
			,103
		)

	,WLPGType = NULL
	,WLProcGroup = NULL
	,WLPostCode = null --NULL --PATDATA
	,EpDOR = NULL --PATDATA
	
	--,GuarAdmDate =
	--	cast(WLE.GuaranteedAdmissionDate + ' 00:01' as DATETIME2(7))
	
	,WtgGteeExcCode = NULL
	,GpReferralLettNo = NULL
	,HRGType = NULL
	,ProvHRG = NULL
	,ProvEFG = NULL
	,RankProc = NULL
	,Summary1 = NULL
	,Summary2 = NULL
	,Summary3 = NULL
	,ContractId = WLE.ContractID

	,WLEffectDate =  CONVERT(DATETIME,OrigWLC.ActivityDate,103)
	    		
			--,CONVERT(DATETIME2(7)
			--	,WLE.OriginalDateOnList + ' ' + 
			--	coalesce(
			--		case
			--		when WLE.OriginalDateOnListTime = '24:00' then '23:59'
			--		else WLE.OriginalDateOnListTime
			--		end
			--		,'00:00'
			--	)
			--	,103
			--)

	    --CONVERT(DATETIME2(7),WLR.Activitydate, 103)
	--,CEAEpisode = CASE 
	--					--WHEN WLE.CEA IS NULL THEN CEAROW.EpisodeNumber
	--					WHEN WLE.CEA = 1     THEN NULL
	--					ELSE WLE.CEA
	--			  END
	,CEAEpisode = CEAROW.EpisodeNumber
	,ReadDiag = WLE.ReadProvDiag
	,ReadDiagTerm = NULL
	,ReadIProc = WLE.ReadPrimIntProc
	,ReadIProcTerm = NULL

	,Summary4 =
		case
		when AD.ADMITDISCHID is not null then 'ADMITTED'
		end

	,SumDate1 =
		CONVERT(
			DATETIME2(7)
			,AD.AdmissionDate + ' ' + 
			coalesce(
				case
				when AD.AdmissionTime = '24:00' then '23:59'
				else AD.AdmissionTime 
				end
				,'00:01'
			)
			,103
		)

	,SumDate2 = NULL
	,SumInt1 = NULL
	,SumInt2 = NULL
	,SumFloat = NULL
	,BookingType = WLE.BookingType
	,LastReviewDate = WLE.LastReviewDate
	,LastReviewResponse = NULL
	,OrigWLCode = OrigWLC.WaitingList

	,CurrentCons =
		coalesce(WLC.Consultant, AD.Consultant,  WLE.Consultant)

	,CurrentSpec =
		coalesce(WLC.Specialty , AD.Specialty, WLC.Specialty , WLE.Specialty)

	,ReasonReferral = WLE.ReasonForReferral
	,DecisionToRefer = NULL 
	,ImpCode = 'MARI'

from
	PAS.Inquire.WLENTRY WLE

inner join PAS.Inquire.EPIREGISTRATION EPR
on	EPR.InternalPatientNumber = WLE.InternalPatientNumber
and	EPR.EpisodeNumber = WLE.EpisodeNumber

left join PAS.Inquire.ADMITDISCH AD
on	AD.InternalPatientNumber = WLE.InternalPatientNumber
and	AD.EpisodeNumber = WLE.EpisodeNumber

left join PAS.Inquire.WLACTIVITY WLR
on	WLR.InternalPatientNumber = WLE.InternalPatientNumber
and	WLR.EpisodeNumber = WLE.EpisodeNumber
and	WLR.EpsActvTypeInt = 4		-- WL Cancel

left join PAS.Inquire.WLACTIVITY WLC
on	WLC.InternalPatientNumber = WLE.InternalPatientNumber
and	WLC.EpisodeNumber = WLE.EpisodeNumber
and	WLC.EpsActvTypeInt IN (1,19)	-- WL Add, WL Transfer
--and	WLC.EpsActvTypeInt != 4
and	not exists
		(
		select
			1
		from
			PAS.Inquire.WLACTIVITY LaterWLC
		where
			LaterWLC.InternalPatientNumber = WLC.InternalPatientNumber
		and	LaterWLC.EpisodeNumber = WLC.EpisodeNumber
--		and	LaterWLC.EpsActvTypeInt != 4
		and	LaterWLC.EpsActvTypeInt IN (1, 19)	-- WL Add, WL Transfer
		and LaterWLC.EpsActvDtimeInt > WLC.EpsActvDtimeInt
		)
left join PAS.Inquire.WLACTIVITY OrigWLC
on	OrigWLC.InternalPatientNumber = WLE.InternalPatientNumber
and	OrigWLC.EpisodeNumber = WLE.EpisodeNumber
and	OrigWLC.EpsActvTypeInt IN (1)	-- WL Add

OUTER APPLY
(
	SELECT	
		EpisodeNumber = MAX(EpisodeNumber)
	FROM
		PAS.Inquire.WLENTRY
	WHERE
		InternalPatientNumber  = WLE.InternalPatientNumber
	AND	EpisodeNumber         != WLE.EpisodeNumber
	AND	CEA					   = 1
	AND AdmissionDateTmInt <= COALESCE(WLE.AdmissionDateTmInt,'209912122359')
) CEAROW

where
	WLE.EpsActvDtimeInt > 200612312400
AND	WLE.IntdMgmt IS NOT NULL			-- Examination of Infocom equivalent there were no rows present 
										-- with this set to NULL
﻿CREATE VIEW [dbo].[v_smsmir_outpatient_referral] AS

/******************************************************************************
**  Name: dbo.v_smsmir_outpatient_referral
**  Purpose: RTT Infocom Conversion Outpatient Referrals 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/

	SELECT

		 InternalNo			= PASRF.InternalPatientNumber
		,EpisodeNo			= PASRF.EpisodeNumber
		,ReferralDate		= CASE 
								WHEN SUBSTRING(PASRF.OpRegDtimeInt,9,2) = 24
									THEN
										DATEADD(MINUTE, 1,
											DATEADD(DAY, 1,
												CAST
												(
													 SUBSTRING(PASRF.OpRegDtimeInt,1,4)
													+ '-'
													+ SUBSTRING(PASRF.OpRegDtimeInt,5,2)
													+ '-'
													+ SUBSTRING(PASRF.OpRegDtimeInt,7,2)	
													
													AS DATETIME										
												)
											)
										)
								ELSE
								  CAST
								  (
									 SUBSTRING(PASRF.OpRegDtimeInt,1,4)
									+ '-'
									+ SUBSTRING(PASRF.OpRegDtimeInt,5,2)
									+ '-'
									+ SUBSTRING(PASRF.OpRegDtimeInt,7,2)
									+ ' '
									+ SUBSTRING(PASRF.OpRegDtimeInt,9,2)
									+ ':'
									+ SUBSTRING(PASRF.OpRegDtimeInt,11,2)
									AS DATETIME
								   )
							  END
		,Hospital			= PASRF.HospitalCode
		,PriorityType		= PASRF.PriorityType
		,DischDate			= CASE
								WHEN	PASRF.DischargeTm = '23:59' THEN CONVERT(DATETIME, PASRF.Dischargedt + ' ' + PASRF.DischargeTm, 103)
								ELSE	[Utility].[fn_InquireDateTimeConvert](PASRF.Dischargedt, PASRF.DischargeTm)
							  END
		,RefSource			= PASRF.RefBy
		,EpiRefBy			= REG.ReferredBy -- OPE.ReferredByCode
		,EpiCode			= CASE 
								WHEN REG.ReferredBy IN ('CNN','CON','DC')	THEN REG.Consultant
								WHEN REG.ReferredBy IN ('GDP')				THEN REG.EpiGDP
								ELSE										     REG.EpiGP
							  END
		,Category			= PASRF.Category
		,RefReasonCode		= PASRF.ReasonForRef
		,ConType			= 'CON'
		,Consultant			= PASRF.ConsCode
		,Specialty			= PASRF.Specialty
		,EpPostCode			= REPLACE(REG.Epipc,'  ',' ')
		,CaseNoteNo			= PASRF.CaseNoteNumber
		,WLTreatment		= OPWL.TreatmentCode
		,WLDate				= DATEADD(MINUTE,1,CONVERT(DATETIME,OPWL.DateOnList,103))
		,WLApptType			= OPWL.AppointmentType
		,Summary1			= NULL
		,Summary2			= COALESCE(RCATSPEC.RepCat, RCATRSN.RepCat, RCATRSRC.RepCat, 'SUM')
		,DecToRefDate		= DATEADD(MINUTE,1,CONVERT(DATETIME,PASRF.DecisionToRefer,103))
		,DischargeReason	= OPDISCHARGE.ReasonCode
		,ContractId			= ALLOCATEDCONTRACT.ContractId
		,CurrentContractId	= COALESCE(CURRCONTRACT.ContractId, ALLOCATEDCONTRACT.ContractId)
		
	FROM
		PAS.Inquire.OPREFERRAL PASRF	

	LEFT JOIN PAS.Inquire.EPIREGISTRATION REG
		ON		REG.InternalPatientNumber = PASRF.InternalPatientNumber
			AND	REG.EpisodeNumber = PASRF.EpisodeNumber
			
	LEFT JOIN PAS.Inquire.OPDISCHARGE 
		ON	PASRF.InternalPatientNumber = OPDISCHARGE.InternalPatientNumber
		AND	PASRF.EpisodeNumber = OPDISCHARGE.EpisodeNumber
		
	LEFT JOIN Infocom_PAS.dbo.cmancons_repcat RCATSPEC
		ON		RCATSPEC.CodeLink	= 'Spec'
			AND	PASRF.Specialty = RCATSPEC.Code
	
	LEFT JOIN Infocom_PAS.dbo.cmancons_repcat RCATRSN
		ON		RCATRSN.CodeLink	= 'ReflRsn'
			AND	PASRF.ReasonForRef = RCATRSN.Code

	LEFT JOIN Infocom_PAS.dbo.cmancons_repcat RCATRSRC
		ON		RCATRSRC.CodeLink	= 'RefSce'
			AND	PASRF.RefBy = RCATRSRC.Code
		
	LEFT JOIN Infocom_PAS.dbo.InquireExtractOPWL OPWL
		ON		PASRF.InternalPatientNumber		= OPWL.SourcePatientNo
			AND	PASRF.EpisodeNumber	= OPWL.SourceEncounterNo
	
	LEFT JOIN PAS.Inquire.ALLOCATEDCONTRACT 
		ON	ALLOCATEDCONTRACT.InternalPatientNumber = PASRF.InternalPatientNumber
		AND	ALLOCATEDCONTRACT.EpisodeNumber = PASRF.EpisodeNumber
		AND	ALLOCATEDCONTRACT.CmReverseDtTime = 999999999999
			
	OUTER APPLY
	(
		SELECT
			ALLOCATEDCONTRACT.ContractId
		FROM
			PAS.Inquire.ALLOCATEDCONTRACT
		LEFT JOIN 
			PAS.Inquire.ALLOCATEDCONTRACT LaterCONTRACT
				ON		ALLOCATEDCONTRACT.InternalPatientNumber = LaterCONTRACT.InternalPatientNumber
					AND ALLOCATEDCONTRACT.EpisodeNumber			= LaterCONTRACT.EpisodeNumber
					AND ALLOCATEDCONTRACT.CmReverseDttime		< LaterCONTRACT.CmReverseDttime
					AND LaterCONTRACT.CmReverseDttime		   <> '999999999999'
		WHERE
				ALLOCATEDCONTRACT.InternalPatientNumber	= PASRF.InternalPatientNumber
			AND ALLOCATEDCONTRACT.EpisodeNumber			= PASRF.EpisodeNumber
			AND CAST(LEFT(ALLOCATEDCONTRACT.CmNeutralDttime,8) AS DATETIME)	   <= CONVERT(DATETIME, COALESCE(PASRF.DischargeDt,'31/12/2099'), 103)
			AND ALLOCATEDCONTRACT.CmReverseDttime <> '999999999999'			-- Note if there are no rows with an effective Start date, the COALESCE
															-- in the column assignmment will default to the ContractSerialNo column
															-- from the RF.Encounter table, which is the CmReverseDttime = '999999999999' 
															-- ALLOCATEDCONTRACT row
			AND LaterCONTRACT.ALLOCATEDCONTRACTID IS NULL
	) CURRCONTRACT
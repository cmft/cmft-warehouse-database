﻿CREATE VIEW [dbo].[v_smsmir_GP_view] AS

/******************************************************************************
**  Name: dbo.v_smsmir_GP_view
**  Purpose: RTT Infocom Conversion GP lookup
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
** 16.10.12      MH       Changed to query PAS.Inquire table directly
******************************************************************************/

	SELECT
	
		 ImpCode				= 'MARI'
		,DrType					= 'GP'
		,DrCode					= GPID
		,Title					= Title
		,Surname				= Surname
		,Initials				= Initials
		,Address1				= Address
		,Address2				= GpAddrLine2
		,Address3				= GpAddrLine3
		,Address4				= GpAddrLine4
		,PostCode				= Postcode
		,Phone					= Phone
		,NationalCode			= NationalCode
		,DOHCode				= DohCode
		,Practice				= PracticeCode
		,GPFHCode				= GpfhCode
		,DateBecameGPFH			= CAST(GeneralPractitionerDateJoinedFhSchemeInt AS DATETIME)
		,DateJoinedPractice		= [Utility].[fn_InquireDateTimeConvert](DateJoinedPractice, NULL)
		,LglDelInd				= ABS(MfRecStsInd)
		
	FROM
	
		[$(PAS)].Inquire.GP
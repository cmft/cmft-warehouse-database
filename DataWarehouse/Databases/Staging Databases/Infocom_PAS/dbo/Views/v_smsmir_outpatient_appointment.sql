﻿CREATE VIEW [dbo].[v_smsmir_outpatient_appointment] AS

/******************************************************************************
**  Name: dbo.v_smsmir_outpatient_appointment
**  Purpose: RTT Infocom Conversion Outpatient appointments 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 08.08.12      MH       Created
******************************************************************************/

	SELECT

		 Hospital			= OPREFERRAL.HospitalCode
		,InternalNo			= OPA.InternalPatientNumber
		,ApptBookedDate		= [Utility].[fn_InquireDateTimeConvert](OPA.ApptBookedDate,OPA.ApptBookedTime)
		,ApptDate			= [Utility].[fn_InquireDateTimeConvert](OPA.ApptDate, OPA.ApptTime)
		,EpisodeNo			= OPA.EpisodeNumber
		,Category			= OPA.PtCategory
		,Doctor				= OPA.ResCode
		,Clinic				= OPA.ClinicCode
		,ApptType			= OPA.ApptType
		,CancelBy			= OPA.CancelBy
		,CancelDate			= [Utility].[fn_InquireDateTimeConvert](OPA.ApptCancDate, OPA.ApptCancTime)
		,EpiPostCode		= REPLACE(OPA.Postcode,'  ',' ')
		,ClinicSpecialty	= OPA.ClinicSpecialty
		,Summary1			= Practice.ParentOrganisationCode
		,Summary2			= COALESCE(RCATCLIN.RepCat, OPREF.Summary2, 'XXXA')
		,ContractId			= COALESCE(OPA.ApptConractId, CURRCONTRACT.ContractId)
		,CancelReason		= Attend.SchReasonForCancellationCodeAppointment
		,PrimaryProc		= OPA.ApptPrimaryProcedureCode
		,AttendStat			= CASE OPA.ApptStatus
								WHEN 'NR' THEN NULL
								ELSE OPA.ApptStatus
							  END
		,Disposal			= OPA.Disposal
		,PrimDiagnosis		= OPA.RefPrimaryDiagnosisCode
		,RTTPeriodStatus	= OPA.RTTPeriodStatus
		,ReferralDateInt	= OPA.ReferralDateInt
		
	FROM
	
	PAS.Inquire.OPA
	
	INNER JOIN PAS.Inquire.OPREFERRAL 
		ON		OPA.InternalPatientNumber = OPREFERRAL.InternalPatientNumber
			AND	OPA.EpisodeNumber = OPREFERRAL.EpisodeNumber
			AND	OPA.ReferralDateInt = OPREFERRAL.OpRegDtimeInt
			
	LEFT JOIN PAS.Inquire.OPAPPOINTATTEND Attend 
		ON		OPA.InternalPatientNumber = Attend.InternalPatientNumber
			AND	OPA.EpisodeNumber = Attend.EpisodeNumber
			AND	OPA.PtApptStartDtimeInt = Attend.PtApptStartDtimeInt
			AND	OPA.ResCode = Attend.ResCode
		
	LEFT JOIN Infocom_PAS.dbo.cmancons_repcat RCATCLIN
		ON		RCATCLIN.CodeLink	= 'Clinic'
			AND	OPA.ClinicCode = RCATCLIN.Code
			
	LEFT JOIN smsmir_outpatient_referral OPREF
		ON		OPA.InternalPatientNumber	= OPREF.InternalNo
			AND	OPA.EpisodeNumber			= OPREF.EpisodeNo
			
	LEFT JOIN Organisation.dbo.Practice Practice
		ON	Practice.OrganisationCode = OPA.PracticeCode

	--LEFT JOIN Organisation.dbo.Postcode Postcode
	--	ON	Postcode.Postcode =
	--			case
	--			when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
	--			when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
	--			else Encounter.Postcode
	--			end
				
	OUTER APPLY
	(
		SELECT
			ALLOCATEDCONTRACT.ContractId
		FROM
			PAS.Inquire.ALLOCATEDCONTRACT
		LEFT JOIN 
			PAS.Inquire.ALLOCATEDCONTRACT LaterCONTRACT
				ON		ALLOCATEDCONTRACT.InternalPatientNumber = LaterCONTRACT.InternalPatientNumber
					AND ALLOCATEDCONTRACT.EpisodeNumber			= LaterCONTRACT.EpisodeNumber
					AND ALLOCATEDCONTRACT.CmReverseDttime		< LaterCONTRACT.CmReverseDttime
					AND LaterCONTRACT.CmReverseDttime		   <> '999999999999'
		WHERE
				ALLOCATEDCONTRACT.InternalPatientNumber	= OPA.InternalPatientNumber
			AND ALLOCATEDCONTRACT.EpisodeNumber			= OPA.EpisodeNumber
			AND CAST(LEFT(ALLOCATEDCONTRACT.CmNeutralDttime,8) AS DATETIME)	   <= CONVERT(DATETIME, OPA.ApptDate, 103)
			AND ALLOCATEDCONTRACT.CmReverseDttime <> '999999999999'			-- Note if there are no rows with an effective Start date, the COALESCE
															-- in the column assignmment will default to the ContractSerialNo column
															-- from the RF.Encounter table, which is the CmReverseDttime = '999999999999' 
															-- ALLOCATEDCONTRACT row
			AND LaterCONTRACT.ALLOCATEDCONTRACTID IS NULL
	) CURRCONTRACT
	
	WHERE CONVERT(DATETIME, OPA.ApptDate, 103) >= '1 Jan 2007'
﻿CREATE TABLE [rtt].[Period](
	[id] [int] NOT NULL,
	[STL_A_period_start] [datetime] NULL,
	[STL_A_period_end] [datetime] NULL,
	[STL_NA_period_start] [datetime] NULL,
	[STL_NA_period_end] [datetime] NULL,
	[Monitor_month_start] [datetime] NULL,
	[Monitor_month_end] [datetime] NULL,
	[Month] [nvarchar](50) NULL,
	[Monthyr] [nvarchar](50) NULL
) ON [PRIMARY]
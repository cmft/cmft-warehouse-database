﻿CREATE TABLE [rtt].[PeriodUpdate](
	[id] [int] NOT NULL,
	[STL_period_start] [datetime] NULL,
	[STL_period_end] [datetime] NULL,
	[Monitor_month_start] [datetime] NULL,
	[Monitor_month_end] [datetime] NULL,
	[Month] [nvarchar](50) NULL,
	[Monthyr] [nvarchar](50) NULL
) ON [PRIMARY]
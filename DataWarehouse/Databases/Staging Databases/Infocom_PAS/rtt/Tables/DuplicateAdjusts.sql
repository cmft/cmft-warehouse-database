﻿CREATE TABLE [rtt].[DuplicateAdjusts](
	[id] [int] NOT NULL,
	[InternalNo] [nvarchar](50) NOT NULL,
	[EpisodeNo] [nvarchar](50) NOT NULL,
	[WL_WLDate] [datetime] NULL,
	[WL_status] [nvarchar](10) NULL,
	[LEpisodeNo] [nvarchar](50) NOT NULL,
	[LWL_WLDate] [datetime] NULL,
	[LWL_status] [nvarchar](10) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](200) NULL,
	[Comments] [nvarchar](255) NULL,
	[Actioned] [bit] NOT NULL
) ON [PRIMARY]
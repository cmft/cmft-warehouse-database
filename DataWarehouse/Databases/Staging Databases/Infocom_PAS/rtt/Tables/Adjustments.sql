﻿CREATE TABLE [rtt].[Adjustments](
	[id] [int] NOT NULL,
	[InternalNo] [nvarchar](50) NOT NULL,
	[EpisodeNo] [nvarchar](50) NOT NULL,
	[PrevMonthStart] [nvarchar](1) NULL,
	[NotPathway] [bit] NOT NULL,
	[DaysToRemove] [int] NULL,
	[Comment] [nvarchar](255) NULL,
	[EndDate] [datetime] NULL,
	[Pause] [bit] NOT NULL,
	[PauseStartDate] [datetime] NULL,
	[PauseEndDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](200) NULL,
	[ToClose] [nvarchar](1) NULL,
	[Admitted] [nvarchar](6) NULL,
	[Division] [nvarchar](100) NULL,
	[Actioned] [bit] NOT NULL
) ON [PRIMARY]
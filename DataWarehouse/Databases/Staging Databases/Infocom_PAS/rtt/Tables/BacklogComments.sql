﻿CREATE TABLE [rtt].[BacklogComments](
	[id] [int] NOT NULL,
	[InternalNo] [nvarchar](50) NOT NULL,
	[EpisodeNo] [nvarchar](50) NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](200) NULL,
	[PComment] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
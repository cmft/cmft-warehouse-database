﻿CREATE TABLE [rtt].[not pw moa18-ward84 pathways](
	[id] [int] NOT NULL,
	[MethAdm] [nvarchar](2) NULL,
	[Ward] [nvarchar](4) NULL,
	[AdmitDate] [datetime] NULL,
	[Hospital] [nvarchar](4) NULL,
	[InternalNo] [nvarchar](9) NULL,
	[EpisodeNo] [nvarchar](9) NULL,
	[Consultant] [nvarchar](8) NULL,
	[Specialty] [nvarchar](8) NULL,
	[CaseNoteNo] [nvarchar](14) NULL,
	[DistrictNo] [nvarchar](16) NULL,
	[DoHCode] [nvarchar](4) NULL,
	[Surname] [nvarchar](60) NULL,
	[Forename] [nvarchar](30) NULL
) ON [PRIMARY]
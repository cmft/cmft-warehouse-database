﻿CREATE TABLE [icwsys].[InstructionTextLookup](
	[ProductFormID] [int] NOT NULL,
	[ProductRouteID] [int] NOT NULL,
	[AdministrationInstructionID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

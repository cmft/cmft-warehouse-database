﻿CREATE TABLE [icwsys].[AdminTimeLog](
	[AdminTimeLogID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[OldDate] [datetime] NOT NULL,
	[NewDate] [datetime] NOT NULL,
	[EntityID] [int] NOT NULL,
	[Notes] [varchar](8000) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
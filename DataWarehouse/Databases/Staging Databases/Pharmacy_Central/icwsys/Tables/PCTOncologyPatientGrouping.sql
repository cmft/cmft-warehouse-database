﻿CREATE TABLE [icwsys].[PCTOncologyPatientGrouping](
	[PCTOncologyPatientGroupingID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [int] NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
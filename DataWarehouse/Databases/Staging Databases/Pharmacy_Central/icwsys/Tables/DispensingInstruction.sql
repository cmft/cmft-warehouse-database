﻿CREATE TABLE [icwsys].[DispensingInstruction](
	[NoteID] [int] NOT NULL,
	[Detail] [varchar](4096) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

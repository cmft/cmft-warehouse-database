﻿CREATE TABLE [icwsys].[FormRuleEvent](
	[FormRuleEventID] [int] IDENTITY(1,1) NOT NULL,
	[ControlID] [int] NULL,
	[FormRuleEventTypeID] [int] NOT NULL,
	[PropertyName] [varchar](50) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
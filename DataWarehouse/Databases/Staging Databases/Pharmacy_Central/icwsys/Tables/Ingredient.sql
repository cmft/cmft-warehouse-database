﻿CREATE TABLE [icwsys].[Ingredient](
	[IngredientID] [int] IDENTITY(0,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[UnitID] [int] NULL,
	[Quantity] [float] NULL,
	[QuantityMin] [float] NULL,
	[QuantityMax] [float] NULL,
	[UnitID_Time] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[PrimaryIngredient] [bit] NOT NULL DEFAULT ((0)),
	[ProductFormID_Dose] [int] NULL,
	[ProductPackageID_Dose] [int] NULL,
	[ProductFormID] [int] NULL,
	[ProductIndexID_Brand] [int] NULL,
	[ProductStrengthQuantity] [float] NULL,
	[UnitID_ProductStrength] [int] NULL,
	[UnitID_ProductStrengthPer] [int] NULL,
	[ProductPacksizeQuantity] [float] NULL,
	[Description_Drug] [varchar](100) NULL,
	[Description_Dose] [varchar](1000) NULL,
	[DoseCalculation_XML] [text] NULL,
	[UnitID_ProductPacksize] [int] NULL,
	[ProductFormID_ProductPacksize] [int] NULL,
	[ProductPackageID_ProductPacksize] [int] NULL,
	[ProductPacksizeUnit] [varchar](50) NULL,
	[DoseDecimalPlaces] [int] NOT NULL DEFAULT ((2)),
	[ProductPackageID] [int] NULL,
	[Description_FullProduct] [varchar](200) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

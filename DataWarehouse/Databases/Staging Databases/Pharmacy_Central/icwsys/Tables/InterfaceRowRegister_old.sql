﻿CREATE TABLE [icwsys].[InterfaceRowRegister_old](
	[InterfaceRowRegisterID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
	[CompletionStatusID] [int] NOT NULL,
	[Table] [varchar](128) NOT NULL,
	[ID] [int] NOT NULL,
	[Revision] [int] NOT NULL,
	[DBVersion] [timestamp] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Radiology](
	[RequestID] [int] NOT NULL,
	[TransportRequired] [bit] NULL,
	[Save] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
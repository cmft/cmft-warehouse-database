﻿CREATE TABLE [icwsys].[ChangeReport](
	[NoteID] [int] NOT NULL,
	[XML] [varchar](7000) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[TextDocument](
	[NoteID] [int] NOT NULL,
	[Detail] [text] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

﻿CREATE TABLE [icwsys].[Locale](
	[LocaleID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](256) NOT NULL,
	[Abbreviation] [char](3) NOT NULL,
	[DialingCode] [char](3) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
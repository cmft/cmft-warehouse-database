﻿CREATE TABLE [icwsys].[eMMPatchLog](
	[eMMPatchLogID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NULL,
	[Date] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[SupplyRequestType](
	[SupplyRequestTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[IsFP10] [bit] NULL CONSTRAINT [DF_SupplyRequestType_IsFP10]  DEFAULT ((0)),
	[OrderReportID] [int] NULL,
	[Outpatient] [bit] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL,
	[IsPN] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
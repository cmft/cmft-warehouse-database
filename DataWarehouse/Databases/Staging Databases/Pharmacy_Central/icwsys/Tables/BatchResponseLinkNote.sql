﻿CREATE TABLE [icwsys].[BatchResponseLinkNote](
	[ResponseID] [int] NOT NULL,
	[NoteID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
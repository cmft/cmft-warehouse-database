﻿CREATE TABLE [icwsys].[ProductLookup2](
	[ProductID] [int] NOT NULL,
	[ProductIndexID] [int] NOT NULL,
	[ProductIndexGroupID] [int] NOT NULL,
	[ProductLookupTypeID] [int] NOT NULL,
	[Default] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
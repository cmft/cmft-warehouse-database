﻿CREATE TABLE [icwsys].[DssWarningOverride](
	[DssWarningOverrideID] [int] IDENTITY(1,1) NOT NULL,
	[AlertType] [int] NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[UsersRoles] [varchar](200) NULL,
	[SeverityID] [int] NULL,
	[OverrideReasonID] [int] NULL,
	[FreeText] [varchar](200) NULL,
	[LogID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Header] [varchar](50) NULL
) ON [PRIMARY]
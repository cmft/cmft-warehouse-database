﻿CREATE TABLE [icwsys].[LocationDynamicDescription](
	[LocationDynamicDescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[LocationTypeID] [int] NOT NULL,
	[ColumnID] [int] NULL,
	[ColumnID_Lookup] [int] NULL,
	[Order] [int] NOT NULL,
	[Description] [varchar](128) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[DynamicDescriptionTypeID] [int] NOT NULL
) ON [PRIMARY]

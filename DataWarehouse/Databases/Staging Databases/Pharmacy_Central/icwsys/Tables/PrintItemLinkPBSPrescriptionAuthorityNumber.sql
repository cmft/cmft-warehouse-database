﻿CREATE TABLE [icwsys].[PrintItemLinkPBSPrescriptionAuthorityNumber](
	[PrintItemLinkPBSPrescriptionAuthorityNumberID] [int] IDENTITY(1,1) NOT NULL,
	[PrintItemID] [int] NOT NULL,
	[PBSPrescriptionAuthorityNumber] [varchar](8) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
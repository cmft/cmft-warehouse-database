﻿CREATE TABLE [icwsys].[ColumnMask](
	[ColumnMaskID] [int] IDENTITY(1,1) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
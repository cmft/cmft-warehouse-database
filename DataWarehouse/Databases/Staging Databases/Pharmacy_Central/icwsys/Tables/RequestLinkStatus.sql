﻿CREATE TABLE [icwsys].[RequestLinkStatus](
	[RequestID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

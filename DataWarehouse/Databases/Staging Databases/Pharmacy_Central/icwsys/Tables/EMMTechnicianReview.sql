﻿CREATE TABLE [icwsys].[EMMTechnicianReview](
	[NoteID] [int] NOT NULL,
	[EMMDispensingTypeID] [int] NULL DEFAULT (NULL),
	[PharmacyCanSupply] [bit] NULL DEFAULT (NULL),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[DSSUpdateInfo](
	[DSSUpdateInfoID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL DEFAULT (getdate()),
	[Source] [varchar](256) NOT NULL,
	[Detail] [varchar](max) NOT NULL,
	[StackTrace] [varchar](max) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ActivityLinkEntity](
	[ActivityID] [numeric](9, 0) NOT NULL,
	[EntityID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[NumericalResult](
	[ResponseID] [int] NOT NULL,
	[Result] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
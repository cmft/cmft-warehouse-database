﻿CREATE TABLE [icwsys].[ResponseLinkStatus](
	[ResponseID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

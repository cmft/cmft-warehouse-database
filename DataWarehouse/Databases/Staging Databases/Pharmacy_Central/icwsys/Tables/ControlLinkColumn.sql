﻿CREATE TABLE [icwsys].[ControlLinkColumn](
	[ControlID] [int] NOT NULL,
	[ColumnID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
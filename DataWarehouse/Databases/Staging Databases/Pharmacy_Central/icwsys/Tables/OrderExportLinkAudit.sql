﻿CREATE TABLE [icwsys].[OrderExportLinkAudit](
	[OrderExportLinkAuditID] [int] IDENTITY(1,1) NOT NULL,
	[OrderExportAuditID] [int] NOT NULL,
	[OrderExportTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
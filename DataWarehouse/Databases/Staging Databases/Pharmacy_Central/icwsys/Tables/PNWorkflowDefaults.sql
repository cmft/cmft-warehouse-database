﻿CREATE TABLE [icwsys].[PNWorkflowDefaults](
	[PNWorkflowDefaultsID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[LocationID_Site] [int] NULL,
	[PrintedText] [varchar](25) NULL,
	[IssuedText] [varchar](25) NULL,
	[NoSupplyFlagSetText] [varchar](25) NULL,
	[PNSupplyFlag1Text] [varchar](25) NULL,
	[PNSupplyFlag2Text] [varchar](25) NULL,
	[PNSupplyFlag3Text] [varchar](25) NULL,
	[CompleteText] [varchar](25) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[MonitorSchedule](
	[MonitorScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[MonitorScheduleTypeID] [int] NOT NULL,
	[Interval] [int] NULL,
	[Occurs] [int] NULL,
	[Monday] [bit] NOT NULL CONSTRAINT [DF_MonitorSchedule_Monday]  DEFAULT ((0)),
	[Tuesday] [bit] NOT NULL CONSTRAINT [DF_MonitorSchedule_Tuesday]  DEFAULT ((0)),
	[Wednesday] [bit] NOT NULL CONSTRAINT [DF_MonitorSchedule_Wednesday]  DEFAULT ((0)),
	[Thursday] [bit] NOT NULL CONSTRAINT [DF_MonitorSchedule_Thursday]  DEFAULT ((0)),
	[Friday] [bit] NOT NULL CONSTRAINT [DF_MonitorSchedule_Friday]  DEFAULT ((0)),
	[Saturday] [bit] NOT NULL CONSTRAINT [DF_MonitorSchedule_Saturday]  DEFAULT ((0)),
	[Sunday] [bit] NOT NULL CONSTRAINT [DF_MonitorSchedule_Sunday]  DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
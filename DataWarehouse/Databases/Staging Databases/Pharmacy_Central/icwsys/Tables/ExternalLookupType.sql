﻿CREATE TABLE [icwsys].[ExternalLookupType](
	[ExternalLookupTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Url] [varchar](4096) NOT NULL,
	[IsAvailable] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
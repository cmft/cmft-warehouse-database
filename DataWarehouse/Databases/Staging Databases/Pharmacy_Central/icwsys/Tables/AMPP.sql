﻿CREATE TABLE [icwsys].[AMPP](
	[ProductID] [int] NOT NULL,
	[ProductLicenseID] [int] NOT NULL CONSTRAINT [DF_AMPP_ProductLicenseID]  DEFAULT (0),
	[ProductPackageID] [int] NOT NULL CONSTRAINT [DF_AMPP_ProductPackageID]  DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__AMPP___RowGUID__5B89DA4E]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[ScheduleTemplateFrequencyItem](
	[ScheduleTemplateFrequencyItemID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleTemplateFrequencyID] [int] NOT NULL,
	[UnitID] [int] NOT NULL,
	[Start] [int] NOT NULL,
	[End] [int] NOT NULL,
	[Frequency] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
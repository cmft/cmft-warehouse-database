﻿CREATE TABLE [icwsys].[FormRuleClause](
	[FormRuleClauseID] [int] IDENTITY(1,1) NOT NULL,
	[FormRuleID] [int] NULL,
	[FormRuleOperatorID] [int] NOT NULL,
	[FormRuleConditionalID] [int] NOT NULL,
	[FormRuleValue1ID] [int] NULL,
	[FormRuleValue2ID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
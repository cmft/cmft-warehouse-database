﻿CREATE TABLE [icwsys].[DSSLegalClassification](
	[DSSLegalClassificationID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CDCupboard] [bit] NOT NULL,
	[CDHandwriting] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__DSSLegalC___RowG__5EB523C6]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
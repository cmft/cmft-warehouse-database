﻿CREATE TABLE [icwsys].[ResponseTypeLinkTable](
	[ResponseTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

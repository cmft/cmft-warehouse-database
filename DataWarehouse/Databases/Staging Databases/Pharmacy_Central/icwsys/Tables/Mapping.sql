﻿CREATE TABLE [icwsys].[Mapping](
	[MappingID] [int] IDENTITY(1,1) NOT NULL,
	[MappingGroupID] [int] NOT NULL,
	[FromCode] [varchar](128) NOT NULL,
	[ToCode] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[InterfaceComponentID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WardSpaceType](
	[WardSpaceTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[Colour] [varchar](50) NULL DEFAULT ('#dcdcdc'),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
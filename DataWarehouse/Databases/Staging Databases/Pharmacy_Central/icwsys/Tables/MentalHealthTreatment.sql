﻿CREATE TABLE [icwsys].[MentalHealthTreatment](
	[MentalHealthTreatmentID] [int] IDENTITY(1,1) NOT NULL,
	[NoteID_MentalHealthForm] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[PRN] [int] NOT NULL,
	[BNFLimit] [float] NOT NULL,
	[Notes] [varchar](1000) NULL,
	[ProductRouteID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[TreatmentType] [int] NULL,
	[OrderCatalogueID_TreatmentReason] [int] NULL,
	[MentalHealthCatalogueID] [int] NULL,
	[ValidDuration] [int] NULL,
	[UnitID_Valid] [int] NULL
) ON [PRIMARY]
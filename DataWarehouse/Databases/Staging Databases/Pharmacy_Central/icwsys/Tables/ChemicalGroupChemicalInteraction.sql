﻿CREATE TABLE [icwsys].[ChemicalGroupChemicalInteraction](
	[ChemicalGroupChemicalInteractionID] [int] IDENTITY(1,1) NOT NULL,
	[ChemicalGroupID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[SeverityID] [int] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ChemicalG___RowG__07B0134A]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
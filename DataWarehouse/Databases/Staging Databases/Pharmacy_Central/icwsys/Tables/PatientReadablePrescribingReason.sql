﻿CREATE TABLE [icwsys].[PatientReadablePrescribingReason](
	[PatientReadablePrescribingReasonID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[OrderCatalogueID] [int] NULL,
	[ArbTextID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[LookupControl](
	[ControlID] [int] NOT NULL,
	[LookupControlTypeID] [int] NOT NULL,
	[ExternalLookupTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Compliance](
	[NoteID] [int] NOT NULL,
	[ComplianceaidID] [int] NOT NULL,
	[Notes] [varchar](250) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
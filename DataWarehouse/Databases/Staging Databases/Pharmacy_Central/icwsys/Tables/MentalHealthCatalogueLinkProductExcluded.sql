﻿CREATE TABLE [icwsys].[MentalHealthCatalogueLinkProductExcluded](
	[MentalHealthCatalogueID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
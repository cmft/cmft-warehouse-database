﻿CREATE TABLE [icwsys].[PharmacyStoresReprint](
	[PharmacyStoresReprintID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Category] [varchar](10) NOT NULL,
	[Name] [varchar](15) NOT NULL,
	[Document] [varchar](max) NOT NULL,
	[Date] [datetime] NOT NULL DEFAULT (getdate()),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
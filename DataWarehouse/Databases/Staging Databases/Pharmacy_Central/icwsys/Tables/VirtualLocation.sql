﻿CREATE TABLE [icwsys].[VirtualLocation](
	[LocationID] [int] NOT NULL,
	[VirtualLocationTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
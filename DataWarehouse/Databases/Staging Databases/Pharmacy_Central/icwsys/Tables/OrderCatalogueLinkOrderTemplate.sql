﻿CREATE TABLE [icwsys].[OrderCatalogueLinkOrderTemplate](
	[OrderCatalogueID] [int] NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__OrderCata___RowG__3F35525E]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[wConfiguration](
	[WConfigurationID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Category] [varchar](255) NOT NULL,
	[Section] [varchar](255) NOT NULL,
	[Key] [varchar](255) NOT NULL,
	[Value] [varchar](8000) NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__wConfigur___RowG__154C678F]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[DSS] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ProductFormulaTemplate](
	[ProductID] [int] NOT NULL,
	[DefaultXML] [varchar](7000) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
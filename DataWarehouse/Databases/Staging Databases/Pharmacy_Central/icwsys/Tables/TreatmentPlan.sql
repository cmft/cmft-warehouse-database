﻿CREATE TABLE [icwsys].[TreatmentPlan](
	[TreatmentPlanID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[StartDate] [datetime] NULL,
	[Description] [varchar](100) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[LocationID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Complete] [bit] NOT NULL,
	[StreamlineEntryAllowed] [bit] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Form](
	[FormID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](1024) NULL,
	[DisplayDefault] [bit] NOT NULL,
	[Width] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[IsSubForm] [bit] NULL CONSTRAINT [DF_Form_IsSubForm]  DEFAULT ((0))
) ON [PRIMARY]
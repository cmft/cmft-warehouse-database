﻿CREATE TABLE [icwsys].[RequestCancellation](
	[NoteID] [int] NOT NULL,
	[RequestID] [int] NOT NULL,
	[DiscontinuationReasonID] [int] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
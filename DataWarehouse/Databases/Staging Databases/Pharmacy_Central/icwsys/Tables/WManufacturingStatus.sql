﻿CREATE TABLE [icwsys].[WManufacturingStatus](
	[WManufacturingStatusID] [int] IDENTITY(0,1) NOT NULL,
	[Code] [varchar](1) NOT NULL,
	[Description] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
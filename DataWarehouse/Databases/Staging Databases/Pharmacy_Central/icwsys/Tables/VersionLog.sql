﻿CREATE TABLE [icwsys].[VersionLog](
	[PatchLogID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[Description] [varchar](512) NOT NULL,
	[Date] [datetime] NULL DEFAULT (getdate()),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[MentalHealthCatalogueLinkOrderCatalogueTreatmentReason](
	[MentalHealthCatalogueID] [int] NOT NULL,
	[OrderCatalogueID_Reason] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[TerminalType](
	[TerminalTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
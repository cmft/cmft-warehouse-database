﻿CREATE TABLE [icwsys].[PCTClaimFile](
	[PCTClaimFileID] [int] IDENTITY(1,1) NOT NULL,
	[DataSpecificationRelease] [char](3) NULL,
	[SLANumber] [varchar](7) NULL,
	[Generated] [datetime] NULL,
	[System] [varchar](10) NULL,
	[SystemVersion] [varchar](6) NULL,
	[ScheduleDate] [datetime] NULL,
	[ClaimDate] [datetime] NOT NULL,
	[FileID] [int] NULL,
	[SiteID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
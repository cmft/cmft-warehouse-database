﻿CREATE TABLE [icwsys].[eMMMedsRecInformation](
	[NoteID] [int] NOT NULL,
	[eMMMedsRecInformationSourceID] [int] NOT NULL,
	[eMMMedsRecInformationSourceID_Second] [int] NULL,
	[Comments] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
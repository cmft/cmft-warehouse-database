﻿CREATE TABLE [icwsys].[GenericTemplateWarnings](
	[GenericTemplateWarningsID] [int] IDENTITY(1,1) NOT NULL,
	[EntityIDBy] [int] NOT NULL,
	[EntityIDAgainst] [int] NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[Override] [bit] NOT NULL,
	[WarningDate] [datetime] NULL DEFAULT (getdate()),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
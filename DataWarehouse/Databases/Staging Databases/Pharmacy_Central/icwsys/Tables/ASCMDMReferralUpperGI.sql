﻿CREATE TABLE [icwsys].[ASCMDMReferralUpperGI](
	[RequestID] [int] NOT NULL,
	[ASCReferralReasonID] [int] NOT NULL,
	[RadiologyQuestion] [varchar](100) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
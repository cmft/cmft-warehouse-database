﻿CREATE TABLE [icwsys].[wStoreStatusCountLog](
	[StoreStatusCountID] [int] IDENTITY(1,1) NOT NULL,
	[Created] [datetime] NOT NULL DEFAULT (getdate()),
	[SiteID] [int] NOT NULL,
	[Status] [char](1) NOT NULL,
	[Counter] [int] NOT NULL,
	[SourceID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[GP_Practice2](
	[PracticeCode] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[Address3] [nvarchar](50) NULL,
	[Address4] [nvarchar](50) NULL,
	[PostCode] [nvarchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[RowID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
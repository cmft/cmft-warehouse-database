﻿CREATE TABLE [icwsys].[AdministrationInstruction](
	[AdministrationInstructionID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

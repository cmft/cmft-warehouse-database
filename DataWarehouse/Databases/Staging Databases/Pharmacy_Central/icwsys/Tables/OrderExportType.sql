﻿CREATE TABLE [icwsys].[OrderExportType](
	[OrderExportTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [char](4) NOT NULL,
	[Detail] [varchar](50) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
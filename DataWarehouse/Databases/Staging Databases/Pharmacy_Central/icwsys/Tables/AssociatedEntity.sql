﻿CREATE TABLE [icwsys].[AssociatedEntity](
	[AssociatedEntityID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[AssociatedEntityTypeID] [int] NOT NULL,
	[EntityID_AssociatedEntity] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
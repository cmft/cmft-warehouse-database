﻿CREATE TABLE [icwsys].[PNRulePrescriptionProforma](
	[PNRuleID] [int] NOT NULL,
	[Volume_mL] [float] NOT NULL,
	[Nitrogen_grams] [float] NOT NULL,
	[Glucose_grams] [float] NOT NULL,
	[Fat_grams] [float] NOT NULL,
	[Sodium_mmol] [float] NOT NULL,
	[Potassium_mmol] [float] NOT NULL,
	[Calcium_mmol] [float] NOT NULL,
	[Magnesium_mmol] [float] NOT NULL,
	[Zinc_micromol] [float] NOT NULL,
	[Phosphate_mmol] [float] NOT NULL,
	[Selenium_nanomol] [float] NOT NULL,
	[Copper_micromol] [float] NOT NULL,
	[Iron_micromol] [float] NOT NULL,
	[AqueousVitamins_mL] [float] NOT NULL,
	[LipidVitamins_mL] [float] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ASCPatientLeave](
	[NoteID] [int] NOT NULL,
	[ExpectedReturnDate] [datetime] NULL,
	[LeaveDate] [datetime] NOT NULL,
	[ReturnedDate] [datetime] NULL,
	[LeaveTypeID] [int] NULL,
	[LeaveDestinationID] [int] NULL,
	[LeaveResultID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
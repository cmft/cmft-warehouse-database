﻿CREATE TABLE [icwsys].[PharmacyPrescriptionLock](
	[PrescriptionID] [int] NOT NULL,
	[SessionID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EntityLocation](
	[EntityLocationID] [int] IDENTITY(1,1) NOT NULL,
	[EntityLocationTypeID] [int] NULL,
	[EntityID] [int] NOT NULL,
	[EntityTypeID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Preference] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL CONSTRAINT [DF_EntityLocation_StartDate]  DEFAULT (getdate()),
	[StopDate] [datetime] NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_EntityLocation_Active]  DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EntityLoc___RowG__66A129A8]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

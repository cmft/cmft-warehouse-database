﻿CREATE TABLE [icwsys].[EpisodeOrderAlias](
	[EpisodeOrderAliasID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](15) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
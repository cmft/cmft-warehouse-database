﻿CREATE TABLE [icwsys].[PBSRestrictionPrescription](
	[PBSRestrictionPrescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID_Prescription] [int] NOT NULL,
	[ProductID_Chemical] [int] NOT NULL,
	[PBSRestrictionID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
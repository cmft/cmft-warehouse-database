﻿CREATE TABLE [icwsys].[ActivityLinkProduct](
	[ActivityID] [numeric](9, 0) NOT NULL,
	[ProductID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
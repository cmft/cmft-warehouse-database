﻿CREATE TABLE [icwsys].[WFMStockAccountSheetLayout](
	[WFMStockAccountSheetLayoutID] [int] IDENTITY(1,1) NOT NULL,
	[WFMStockAccountSheetLayoutID_Parent] [int] NULL,
	[SectionType] [char](1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[RuleCodes] [varchar](150) NOT NULL,
	[BackgroundColour] [int] NULL,
	[TextColour] [int] NULL,
	[SortIndex] [int] NOT NULL,
	[ShowQuantity] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
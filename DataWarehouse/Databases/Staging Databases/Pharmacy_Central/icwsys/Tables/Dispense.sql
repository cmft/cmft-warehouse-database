﻿CREATE TABLE [icwsys].[Dispense](
	[RequestID] [int] NOT NULL,
	[GP_To_Continue] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[AdministrationStandard](
	[ResponseID] [int] NOT NULL,
	[Dose] [float] NOT NULL,
	[UnitID_Dose] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

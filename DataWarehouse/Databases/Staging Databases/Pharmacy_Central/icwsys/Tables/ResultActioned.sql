﻿CREATE TABLE [icwsys].[ResultActioned](
	[NoteID] [int] NOT NULL,
	[ActionType_ResultID] [int] NOT NULL,
	[Comments] [varchar](1024) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
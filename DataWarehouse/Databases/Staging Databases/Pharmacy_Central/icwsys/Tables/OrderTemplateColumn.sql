﻿CREATE TABLE [icwsys].[OrderTemplateColumn](
	[ColumnID] [int] NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
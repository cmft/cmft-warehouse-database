﻿CREATE TABLE [icwsys].[DynamicDescriptionType](
	[DynamicDescriptionTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](256) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
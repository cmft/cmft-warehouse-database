﻿CREATE TABLE [icwsys].[NoteTypeAlias](
	[NoteTypeAliasID] [int] IDENTITY(1,1) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[NoteTypeID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](50) NOT NULL,
	[Default] [bit] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
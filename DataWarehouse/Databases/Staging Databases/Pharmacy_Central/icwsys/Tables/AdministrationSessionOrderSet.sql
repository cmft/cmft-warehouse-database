﻿CREATE TABLE [icwsys].[AdministrationSessionOrderSet](
	[RequestID] [int] NOT NULL,
	[DurationMinutes] [int] NOT NULL,
	[MeetingID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
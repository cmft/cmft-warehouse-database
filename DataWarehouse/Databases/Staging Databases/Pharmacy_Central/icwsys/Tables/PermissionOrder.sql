﻿CREATE TABLE [icwsys].[PermissionOrder](
	[PolicyOrderID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[Allow] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PCTPrescriberType](
	[PCTPrescriberTypeID] [int] IDENTITY(0,1) NOT NULL,
	[PCTPrescriberTypeCode] [varchar](3) NULL,
	[Description] [varchar](50) NULL,
	[Detail] [varchar](255) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Pharmacist](
	[EntityID] [int] NOT NULL,
	[RPGSBnumber] [char](10) NULL,
	[Grade] [char](1) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
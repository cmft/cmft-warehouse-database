﻿CREATE TABLE [icwsys].[SupplyRequest](
	[RequestID] [int] NOT NULL,
	[SupplyRequestTypeID] [int] NOT NULL,
	[ProductID_Mapped] [int] NOT NULL,
	[IsVirtualProduct] [bit] NOT NULL,
	[QuantityRequested] [float] NULL,
	[FormID_Quantity] [int] NULL,
	[UnitID_Quantity] [int] NULL,
	[PackageID_Quantity] [int] NULL,
	[DaysRequested] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[FreeTextQuantity] [varchar](128) NULL,
	[ProductStrengthQuantity] [float] NULL,
	[UnitID_ProductStrength] [int] NULL,
	[UnitID_ProductStrengthPer] [int] NULL,
	[ProductIndexID_Brand] [int] NULL,
	[ProductPackageID] [int] NULL
) ON [PRIMARY]
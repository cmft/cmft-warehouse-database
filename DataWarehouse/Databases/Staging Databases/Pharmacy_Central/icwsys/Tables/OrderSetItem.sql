﻿CREATE TABLE [icwsys].[OrderSetItem](
	[OrderSetItemID] [int] IDENTITY(1,1) NOT NULL,
	[OrderSetItemID_Requisit] [int] NOT NULL CONSTRAINT [DF_OrderSetItem_OrderSetItemID_Requisit]  DEFAULT (0),
	[OrderTemplateID_OrderSet] [int] NOT NULL,
	[OrderTemplateID_OrderTemplate] [int] NOT NULL,
	[RequestDate_Offset] [int] NOT NULL CONSTRAINT [DF_OrderSetItem_RequestDate_Offset]  DEFAULT (0),
	[Mandatory] [bit] NOT NULL CONSTRAINT [DF_OrderSetItem_Mandatory]  DEFAULT (0),
	[DependancyTypeID] [int] NULL,
	[IndexOrder] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
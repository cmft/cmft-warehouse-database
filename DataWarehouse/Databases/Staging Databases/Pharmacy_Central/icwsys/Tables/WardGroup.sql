﻿CREATE TABLE [icwsys].[WardGroup](
	[WardGroupID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](1) NOT NULL CONSTRAINT [DF_WardGroup_Description]  DEFAULT (''''),
	[Detail] [varchar](1024) NOT NULL,
	[RxNumStart] [int] NOT NULL DEFAULT ((0)),
	[RxNumEnd] [int] NOT NULL DEFAULT ((0)),
	[RxNumCountByWardGroup] [bit] NOT NULL DEFAULT ((0)),
	[Out_Of_Use] [bit] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
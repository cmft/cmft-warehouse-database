﻿CREATE TABLE [icwsys].[PBSEPAveragePrices](
	[PBSEPAveragePricesID] [int] IDENTITY(1,1) NOT NULL,
	[PBSMasterEPAveragePricesID] [int] NULL,
	[PBSCode] [varchar](3) NULL,
	[AverageRate] [float] NULL,
	[MaxPricePayable] [float] NULL,
	[MaxPriceToPatients] [float] NULL,
	[MaxQty] [int] NULL,
	[IssueUnit] [varchar](10) NULL,
	[MaxRepeats] [int] NULL,
	[Description] [varchar](50) NULL,
	[DateLastUpdated] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
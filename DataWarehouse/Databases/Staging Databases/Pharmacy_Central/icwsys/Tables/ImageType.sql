﻿CREATE TABLE [icwsys].[ImageType](
	[ImageTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[TableID] [int] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
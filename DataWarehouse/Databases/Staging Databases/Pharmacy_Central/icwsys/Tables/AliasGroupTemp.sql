﻿CREATE TABLE [icwsys].[AliasGroupTemp](
	[AliasGroupID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL,
	[DisplayName] [varchar](50) NULL,
	[Format] [varchar](50) NULL,
	[UniqueValueRequired] [bit] NOT NULL
) ON [PRIMARY]
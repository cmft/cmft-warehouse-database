﻿CREATE TABLE [icwsys].[PBSRequestType](
	[PBSRequestTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[OrderReportID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
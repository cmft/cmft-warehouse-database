﻿CREATE TABLE [icwsys].[LocationOpenHours](
	[LocationOpenHoursID] [int] IDENTITY(1,1) NOT NULL,
	[DayID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[StartHour] [int] NOT NULL,
	[StartMinute] [int] NOT NULL,
	[EndHour] [int] NOT NULL,
	[EndMinute] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

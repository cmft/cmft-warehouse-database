﻿CREATE TABLE [icwsys].[OpeningBalance](
	[OpeningBalanceID] [int] IDENTITY(1,1) NOT NULL,
	[Year] [int] NOT NULL,
	[Period] [int] NOT NULL,
	[Quantity] [numeric](18, 2) NULL,
	[Value] [numeric](18, 2) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Range](
	[RangeID] [int] IDENTITY(1,1) NOT NULL,
	[High] [float] NULL,
	[Low] [float] NULL,
	[UnitID] [int] NOT NULL,
	[RoutineID] [int] NOT NULL,
	[RangeTestOutcomeID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
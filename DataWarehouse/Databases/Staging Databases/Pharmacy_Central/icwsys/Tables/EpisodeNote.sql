﻿CREATE TABLE [icwsys].[EpisodeNote](
	[NoteID] [int] NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Expired] [bit] NULL,
	[ExpiryDateTime] [datetime] NULL
) ON [PRIMARY]

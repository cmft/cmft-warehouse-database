﻿CREATE TABLE [icwsys].[TableLinkColumn](
	[TableID] [int] NOT NULL,
	[ColumnID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PrintBatch](
	[PrintBatchID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_PrintBatch_CreatedDate]  DEFAULT (getdate()),
	[EntityID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Preview] [bit] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
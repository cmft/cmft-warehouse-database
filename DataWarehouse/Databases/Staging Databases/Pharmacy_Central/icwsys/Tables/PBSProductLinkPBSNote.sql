﻿CREATE TABLE [icwsys].[PBSProductLinkPBSNote](
	[PBSMasterProductID] [int] NOT NULL,
	[PBSNoteID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
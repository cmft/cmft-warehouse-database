﻿CREATE TABLE [icwsys].[EntityAlias](
	[EntityAliasID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL CONSTRAINT [DF_EntityAlias_Default]  DEFAULT (0),
	[IsValid] [bit] NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EntityAli___RowG__4E523CC6]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

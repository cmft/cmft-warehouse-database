﻿CREATE TABLE [icwsys].[CalculatedField](
	[ColumnID] [int] NOT NULL,
	[Formula] [varchar](256) NULL,
	[TableID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
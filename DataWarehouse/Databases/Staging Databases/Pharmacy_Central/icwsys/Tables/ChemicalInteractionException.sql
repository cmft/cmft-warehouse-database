﻿CREATE TABLE [icwsys].[ChemicalInteractionException](
	[ProductA] [int] NOT NULL,
	[ProductRouteA] [int] NOT NULL,
	[ProductB] [int] NOT NULL,
	[ProductRouteB] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL,
	[DSS] [bit] NOT NULL
) ON [PRIMARY]
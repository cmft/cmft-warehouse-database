﻿CREATE TABLE [icwsys].[PatientTransferNote](
	[NoteID] [int] NOT NULL,
	[TransferDestinationID] [int] NOT NULL,
	[Address] [varchar](1024) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[TransferMethodID] [int] NULL,
	[TransferDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ASCPatientConsent](
	[NoteID] [int] NOT NULL,
	[NoteID_LegalStatus] [int] NOT NULL,
	[ActiveFrom] [datetime] NOT NULL,
	[Expiry] [datetime] NULL,
	[Terminated] [datetime] NULL,
	[PatientConsentSourceID] [int] NOT NULL,
	[PatientConsentStatusID] [int] NOT NULL,
	[ConsentFormTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
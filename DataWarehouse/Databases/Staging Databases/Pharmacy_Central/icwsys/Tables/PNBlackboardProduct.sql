﻿CREATE TABLE [icwsys].[PNBlackboardProduct](
	[PNBlackboardProductID] [int] IDENTITY(1,1) NOT NULL,
	[PNBlackboardID] [int] NOT NULL,
	[PNCode] [varchar](8) NOT NULL,
	[Description] [varchar](29) NOT NULL,
	[Volume_mL] [float] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
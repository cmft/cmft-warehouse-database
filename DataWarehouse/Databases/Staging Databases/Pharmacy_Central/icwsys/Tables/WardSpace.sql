﻿CREATE TABLE [icwsys].[WardSpace](
	[LocationID] [int] NOT NULL,
	[X] [int] NOT NULL,
	[Y] [int] NOT NULL,
	[TopBorder] [bit] NOT NULL,
	[BottomBorder] [bit] NOT NULL,
	[LeftBorder] [bit] NOT NULL,
	[RightBorder] [bit] NOT NULL,
	[WardSpaceTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
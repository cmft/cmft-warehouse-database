﻿CREATE TABLE [icwsys].[MentalHealthCatalogue](
	[MentalHealthCatalogueID] [int] IDENTITY(1,1) NOT NULL,
	[OrderCatalogueID] [int] NOT NULL,
	[Description] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_Deleted] [bit] NULL
) ON [PRIMARY]
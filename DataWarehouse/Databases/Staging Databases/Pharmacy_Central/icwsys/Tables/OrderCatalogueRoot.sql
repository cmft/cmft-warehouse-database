﻿CREATE TABLE [icwsys].[OrderCatalogueRoot](
	[OrderCatalogueRootID] [int] IDENTITY(1,1) NOT NULL,
	[EntityTypeID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[ImageURL] [varchar](128) NOT NULL DEFAULT ('classSearchStructure.gif'),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__OrderCata___RowG__31A64D16]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[Search] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
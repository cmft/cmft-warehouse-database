﻿CREATE TABLE [icwsys].[PCTRepeat](
	[PCTRepeatID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[PCTPrescriberID] [int] NOT NULL,
	[PCTFlag] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
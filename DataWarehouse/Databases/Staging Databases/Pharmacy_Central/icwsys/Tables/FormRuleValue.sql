﻿CREATE TABLE [icwsys].[FormRuleValue](
	[FormRuleValueID] [int] IDENTITY(1,1) NOT NULL,
	[FormRuleValueTypeID] [int] NOT NULL,
	[Value] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
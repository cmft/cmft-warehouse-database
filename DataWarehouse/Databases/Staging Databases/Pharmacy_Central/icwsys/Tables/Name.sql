﻿CREATE TABLE [icwsys].[Name](
	[NameID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[NameTypeID] [int] NOT NULL,
	[Forename1] [varchar](35) NULL,
	[Forename2] [varchar](35) NULL,
	[Forename3] [varchar](35) NULL,
	[Surname] [varchar](35) NOT NULL,
	[Title] [varchar](20) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
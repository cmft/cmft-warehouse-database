﻿CREATE TABLE [icwsys].[ApplicationLog](
	[ApplicationLogID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationLogID_parent] [int] NOT NULL,
	[LogTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[Occurred] [datetime] NOT NULL CONSTRAINT [DF_ApplicationLog_Occurred]  DEFAULT (getdate()),
	[Source] [text] NOT NULL,
	[Description] [text] NOT NULL,
	[InterfaceComponentID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
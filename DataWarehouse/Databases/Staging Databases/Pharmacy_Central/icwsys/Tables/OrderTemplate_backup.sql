﻿CREATE TABLE [icwsys].[OrderTemplate_backup](
	[OrderTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[RequestTypeID] [int] NOT NULL,
	[NoteTypeID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[DefaultXML] [varchar](6000) NOT NULL,
	[DssReferenceID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[SupervisedCommunityTreatment](
	[EpisodeID] [int] NOT NULL,
	[Expiry] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
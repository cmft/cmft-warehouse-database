﻿CREATE TABLE [icwsys].[PNLog](
	[PNLogID] [int] IDENTITY(1,1) NOT NULL,
	[Occurred] [datetime] NOT NULL,
	[UserInitials] [varchar](3) NOT NULL,
	[EntityID_User] [int] NOT NULL,
	[TerminalName] [varchar](15) NOT NULL,
	[SiteNumber] [int] NULL,
	[LocationID_Site] [int] NULL,
	[EntityID_Patient] [int] NULL,
	[EpisodeID] [int] NULL,
	[PNProductID] [int] NULL,
	[PNRuleID] [int] NULL,
	[Description] [varchar](max) NOT NULL,
	[StackTrace] [varchar](max) NULL,
	[RequestID_Regimen] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
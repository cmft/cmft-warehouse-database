﻿CREATE TABLE [icwsys].[ChemicalLinkTherapeuticGroup](
	[ProductID] [int] NOT NULL,
	[TherapeuticGroupID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ChemicalL___RowG__3C112EF5]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

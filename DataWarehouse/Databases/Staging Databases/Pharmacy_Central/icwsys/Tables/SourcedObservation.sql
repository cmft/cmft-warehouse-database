﻿CREATE TABLE [icwsys].[SourcedObservation](
	[NoteID] [int] NOT NULL,
	[ObservationSourceID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
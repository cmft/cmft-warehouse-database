﻿CREATE TABLE [icwsys].[PendingItemBatch](
	[PendingItemID] [int] NOT NULL,
	[ModalityID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
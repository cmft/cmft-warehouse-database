﻿CREATE TABLE [icwsys].[DSSOrderCatalogueMapping](
	[OriginalOrderCatalogueID] [int] NOT NULL,
	[OriginalOrderCatalogueID_Parent] [int] NOT NULL,
	[NewOrderCatalogueID] [int] NOT NULL,
	[NewOrderCatalogueID_Parent] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
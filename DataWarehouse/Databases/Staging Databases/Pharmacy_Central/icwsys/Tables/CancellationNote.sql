﻿CREATE TABLE [icwsys].[CancellationNote](
	[NoteID] [int] NOT NULL,
	[DiscontinuationReasonID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
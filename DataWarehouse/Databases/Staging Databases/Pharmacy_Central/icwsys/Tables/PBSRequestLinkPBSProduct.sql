﻿CREATE TABLE [icwsys].[PBSRequestLinkPBSProduct](
	[RequestID] [int] NOT NULL,
	[PBSProductID_Mapped] [int] NOT NULL,
	[ProductStrength] [float] NULL,
	[UnitID_ProductStrength] [int] NULL,
	[UnitID_ProductStrengthPer] [int] NULL,
	[ProductIndexID_Brand] [int] NULL,
	[ProductPackageID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
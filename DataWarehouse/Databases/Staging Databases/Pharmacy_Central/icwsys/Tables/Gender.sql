﻿CREATE TABLE [icwsys].[Gender](
	[GenderID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Gender___RowGUID__520DBF11]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

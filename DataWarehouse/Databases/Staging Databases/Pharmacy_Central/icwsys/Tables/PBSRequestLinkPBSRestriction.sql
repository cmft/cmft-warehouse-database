﻿CREATE TABLE [icwsys].[PBSRequestLinkPBSRestriction](
	[PBSRequestLinkPBSRestrictionID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[PBSRestrictionID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PNRegimenProductVolume](
	[PNRegimenProductVolumeID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[PNProductID] [int] NOT NULL,
	[Volume_mL] [float] NOT NULL,
	[TotalVolumeIncOverage] [float] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
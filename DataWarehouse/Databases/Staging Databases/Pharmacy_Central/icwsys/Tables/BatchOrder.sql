﻿CREATE TABLE [icwsys].[BatchOrder](
	[RequestID] [int] NOT NULL,
	[ModalityID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
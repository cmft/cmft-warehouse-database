﻿CREATE TABLE [icwsys].[PCTPrescription](
	[PCTPrescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID_Prescription] [int] NULL,
	[PrescriberEntityID] [int] NOT NULL,
	[PCTOncologyPatientGroupingID] [int] NOT NULL,
	[PrescriptionFormNumber] [varchar](9) NOT NULL,
	[SpecialAuthorityNumber] [varchar](10) NULL,
	[SpecialistEndorserEntityID] [int] NULL,
	[EndorsementDate] [datetime] NULL,
	[FullWastage] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PrescriptionStandard](
	[RequestID] [int] NOT NULL,
	[UnitID_Dose] [int] NOT NULL,
	[ProductFormID_Dose] [int] NULL,
	[Dose] [float] NOT NULL,
	[DoseLow] [float] NULL,
	[SupplimentaryText] [varchar](1024) NULL,
	[ProductPackageID_Dose] [int] NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[OrderTemplateControl](
	[ControlID] [int] NOT NULL,
	[OrderTemplateDisplayTypeID] [int] NOT NULL,
	[OrderTemplateOptionsTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[SubFormID] [int] NULL
) ON [PRIMARY]
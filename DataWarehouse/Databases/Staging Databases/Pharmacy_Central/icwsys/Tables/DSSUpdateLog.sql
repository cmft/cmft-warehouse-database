﻿CREATE TABLE [icwsys].[DSSUpdateLog](
	[DSSUpdateLogID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL DEFAULT (getdate()),
	[EntityID] [int] NOT NULL,
	[TableID] [int] NULL,
	[Description] [varchar](50) NOT NULL,
	[Data] [text] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EntityLocationQueue](
	[EntityLocationQueueID] [int] IDENTITY(1,1) NOT NULL,
	[EntityLocationTypeID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EntityAliasLinkExpiryDate](
	[EntityAliasLinkExpiryDateID] [int] IDENTITY(1,1) NOT NULL,
	[EntityAliasID] [int] NOT NULL,
	[ExpiryDate] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
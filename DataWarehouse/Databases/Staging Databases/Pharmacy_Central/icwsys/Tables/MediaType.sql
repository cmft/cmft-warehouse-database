﻿CREATE TABLE [icwsys].[MediaType](
	[MediaTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
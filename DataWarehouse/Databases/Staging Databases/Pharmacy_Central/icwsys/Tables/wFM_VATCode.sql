﻿CREATE TABLE [icwsys].[wFM_VATCode](
	[wFM_VATCodeID] [int] IDENTITY(1,1) NOT NULL,
	[SiteNumber] [int] NOT NULL,
	[key] [varchar](255) NOT NULL,
	[value] [varchar](255) NOT NULL,
	[Month] [varchar](6) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
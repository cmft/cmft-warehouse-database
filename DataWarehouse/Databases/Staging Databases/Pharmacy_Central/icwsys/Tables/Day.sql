﻿CREATE TABLE [icwsys].[Day](
	[DayID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
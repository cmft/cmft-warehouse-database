﻿CREATE TABLE [icwsys].[ScheduleEvent](
	[ScheduleEventID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleEventID_Parent] [int] NOT NULL,
	[ScheduleID] [int] NOT NULL,
	[UnitID_Offset] [int] NOT NULL,
	[Offset] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
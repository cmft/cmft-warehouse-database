﻿CREATE TABLE [icwsys].[PharmacyLabelReprint](
	[PharmacyLabelReprintID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[LabelNumber] [int] NOT NULL,
	[WLabelID] [int] NOT NULL,
	[Label] [varchar](max) NOT NULL,
	[Prefix] [varchar](3) NULL,
	[Date] [datetime] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
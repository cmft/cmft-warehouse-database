﻿CREATE TABLE [icwsys].[Discharge](
	[RequestID] [int] NOT NULL,
	[Date] [datetime] NULL,
	[Comments] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
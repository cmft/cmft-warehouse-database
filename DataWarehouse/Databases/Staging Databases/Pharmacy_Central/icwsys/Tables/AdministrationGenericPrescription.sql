﻿CREATE TABLE [icwsys].[AdministrationGenericPrescription](
	[ResponseID] [int] NOT NULL,
	[DrugName] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[LocationLinkEpmaGroup](
	[LocationLinkEpmaGroupID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NULL,
	[EpmaGroupID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
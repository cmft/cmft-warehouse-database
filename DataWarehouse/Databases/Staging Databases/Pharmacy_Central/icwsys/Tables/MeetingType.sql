﻿CREATE TABLE [icwsys].[MeetingType](
	[MeetingTypeID] [int] IDENTITY(0,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
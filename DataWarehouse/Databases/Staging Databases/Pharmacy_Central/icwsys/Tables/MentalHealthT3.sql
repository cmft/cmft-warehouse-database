﻿CREATE TABLE [icwsys].[MentalHealthT3](
	[NoteID] [int] NOT NULL,
	[ConsultedNurseName] [varchar](100) NOT NULL,
	[Consulted] [varchar](100) NOT NULL,
	[ConsultedProfession] [varchar](50) NOT NULL,
	[PatientIsNotCapable] [bit] NOT NULL,
	[PatientHasNotConsented] [bit] NOT NULL,
	[Reasons] [varchar](1000) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
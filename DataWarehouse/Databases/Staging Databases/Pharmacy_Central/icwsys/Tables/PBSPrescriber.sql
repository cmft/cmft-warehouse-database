﻿CREATE TABLE [icwsys].[PBSPrescriber](
	[EntityID] [int] NOT NULL,
	[PBSPrescriberTypeID] [int] NOT NULL,
	[PBSPrescriberCode] [varchar](7) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
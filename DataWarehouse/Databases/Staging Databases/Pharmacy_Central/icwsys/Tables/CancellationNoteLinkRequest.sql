﻿CREATE TABLE [icwsys].[CancellationNoteLinkRequest](
	[NoteID] [int] NOT NULL,
	[RequestID_Cancelled] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ActivityLinkLocation](
	[ActivityID] [numeric](9, 0) NOT NULL,
	[LocationID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
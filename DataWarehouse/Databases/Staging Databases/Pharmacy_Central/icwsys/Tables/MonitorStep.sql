﻿CREATE TABLE [icwsys].[MonitorStep](
	[MonitorStepID] [int] IDENTITY(1,1) NOT NULL,
	[MonitorJobID] [int] NOT NULL,
	[StepOrder] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[RoutineID] [int] NOT NULL,
	[FailureTransformPath] [varchar](1024) NULL,
	[SuccessTransformPath] [varchar](1024) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_MonitorStep_Active]  DEFAULT ((1)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[RequestLinkRadiologyStatusType](
	[RequestID] [int] NOT NULL,
	[RadiologyStatusTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Description] [varchar](128) NULL
) ON [PRIMARY]
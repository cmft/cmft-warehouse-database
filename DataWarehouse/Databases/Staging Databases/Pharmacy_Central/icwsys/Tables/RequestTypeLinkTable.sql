﻿CREATE TABLE [icwsys].[RequestTypeLinkTable](
	[RequestTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

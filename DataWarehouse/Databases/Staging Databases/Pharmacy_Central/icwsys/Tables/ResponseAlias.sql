﻿CREATE TABLE [icwsys].[ResponseAlias](
	[ResponseAliasID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
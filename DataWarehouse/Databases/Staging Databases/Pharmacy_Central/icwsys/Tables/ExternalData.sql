﻿CREATE TABLE [icwsys].[ExternalData](
	[NoteID] [int] NOT NULL,
	[ExternalDataSourceID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Detail] [varchar](8000) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PolicyDesktop](
	[PolicyDesktopID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](50) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WardPlacement](
	[LocationID] [int] NOT NULL,
	[WardPlacementTypeID] [int] NOT NULL,
	[StatusEndDateTime] [datetime] NULL,
	[NewWardPlacementStatusID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[AdtReceived](
	[AdtReceivedID] [int] IDENTITY(1,1) NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
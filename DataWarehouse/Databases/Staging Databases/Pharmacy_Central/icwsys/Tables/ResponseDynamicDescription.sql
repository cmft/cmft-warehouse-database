﻿CREATE TABLE [icwsys].[ResponseDynamicDescription](
	[ResponseDynamicDescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseTypeID] [int] NOT NULL,
	[Short] [bit] NOT NULL CONSTRAINT [DF_ResponseDynamicDescription_Short]  DEFAULT (0),
	[ColumnID] [int] NULL,
	[ColumnID_Lookup] [int] NULL,
	[Order] [int] NOT NULL,
	[Description] [varchar](128) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[DynamicDescriptionTypeID] [int] NOT NULL DEFAULT ((1))
) ON [PRIMARY]

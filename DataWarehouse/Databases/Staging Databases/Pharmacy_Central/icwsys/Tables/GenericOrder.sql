﻿CREATE TABLE [icwsys].[GenericOrder](
	[RequestID] [int] NOT NULL,
	[ModalityID] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[Comments] [varchar](1028) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
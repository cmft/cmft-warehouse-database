﻿CREATE TABLE [icwsys].[DSSMasterSiteLinkSiteDrug](
	[DSSMasterSiteLinkSiteDrugID] [int] IDENTITY(1,1) NOT NULL,
	[DrugID_Master] [int] NULL,
	[DrugID_Site] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[SMSPrintBatch](
	[PrintBatchID] [int] NOT NULL,
	[ConsultantID] [int] NULL,
	[SMSPrintBatchModeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
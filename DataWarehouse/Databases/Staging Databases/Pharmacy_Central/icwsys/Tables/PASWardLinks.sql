﻿CREATE TABLE [icwsys].[PASWardLinks](
	[PasWardCode] [varchar](4) NULL,
	[PasWard] [varchar](30) NULL,
	[AscWardCode] [varchar](5) NULL,
	[AscWardName] [varchar](35) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[MentalHealthCTO11](
	[NoteID] [int] NOT NULL,
	[Consulted] [varchar](100) NOT NULL,
	[ConsultedProfession] [varchar](50) NOT NULL,
	[SecondConsulted] [varchar](100) NOT NULL,
	[SecondConsultedProfession] [varchar](50) NOT NULL,
	[CommunityTreatmentConditions] [varchar](1000) NULL,
	[HospitalTreatmentConditions] [varchar](1000) NULL,
	[Reasons] [varchar](1000) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
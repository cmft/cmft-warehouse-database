﻿CREATE TABLE [icwsys].[Chair](
	[LocationID] [int] NOT NULL,
	[X] [int] NULL,
	[Y] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

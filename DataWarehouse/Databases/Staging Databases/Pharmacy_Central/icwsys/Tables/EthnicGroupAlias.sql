﻿CREATE TABLE [icwsys].[EthnicGroupAlias](
	[EthnicGroupAliasID] [int] IDENTITY(1,1) NOT NULL,
	[EthnicGroupID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
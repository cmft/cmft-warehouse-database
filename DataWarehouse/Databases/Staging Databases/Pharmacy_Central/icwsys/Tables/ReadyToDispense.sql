﻿CREATE TABLE [icwsys].[ReadyToDispense](
	[NoteID] [int] NOT NULL,
	[Notes] [varchar](250) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
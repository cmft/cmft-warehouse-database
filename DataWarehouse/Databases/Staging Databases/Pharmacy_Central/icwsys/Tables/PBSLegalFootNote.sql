﻿CREATE TABLE [icwsys].[PBSLegalFootNote](
	[PBSLegalFootNoteID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[Out_Of_Use] [bit] NULL CONSTRAINT [DF_PBSLegalFootNote_Out_Of_Use]  DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
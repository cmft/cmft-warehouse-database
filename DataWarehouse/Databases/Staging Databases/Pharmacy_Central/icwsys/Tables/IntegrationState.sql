﻿CREATE TABLE [icwsys].[IntegrationState](
	[IntegrationStateID] [int] IDENTITY(1,1) NOT NULL,
	[data] [varchar](8000) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WindowParameter](
	[WindowParameterID] [int] IDENTITY(1,1) NOT NULL,
	[WindowID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Value] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ChemicalInteraction](
	[ChemicalInteractionID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID_A] [int] NOT NULL,
	[ProductID_B] [int] NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[SeverityID] [int] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ChemicalI___RowG__03DF8266]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PCTPatient](
	[PCTPatientID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[HUHCNo] [varchar](10) NULL,
	[HUHCExpiry] [datetime] NULL,
	[CSC] [bit] NOT NULL,
	[CSCExpiry] [datetime] NULL,
	[PermResHokianga] [bit] NOT NULL,
	[PHORegistered] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
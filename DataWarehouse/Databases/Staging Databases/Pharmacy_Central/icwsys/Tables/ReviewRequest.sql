﻿CREATE TABLE [icwsys].[ReviewRequest](
	[RequestID] [int] NOT NULL,
	[ReviewAction] [varchar](128) NOT NULL,
	[ReviewIn] [int] NOT NULL,
	[ReviewPeriod] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

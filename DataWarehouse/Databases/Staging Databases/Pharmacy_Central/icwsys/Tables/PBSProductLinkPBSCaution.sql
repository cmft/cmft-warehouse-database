﻿CREATE TABLE [icwsys].[PBSProductLinkPBSCaution](
	[PBSMasterProductID] [int] NOT NULL,
	[PBSCautionID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
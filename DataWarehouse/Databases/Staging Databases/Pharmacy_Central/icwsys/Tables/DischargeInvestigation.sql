﻿CREATE TABLE [icwsys].[DischargeInvestigation](
	[NoteID] [int] NOT NULL,
	[OrderCatalogueID_Investigation] [int] NOT NULL,
	[Notes] [varchar](4000) NULL,
	[AwaitingResult] [bit] NOT NULL,
	[IsNormal] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

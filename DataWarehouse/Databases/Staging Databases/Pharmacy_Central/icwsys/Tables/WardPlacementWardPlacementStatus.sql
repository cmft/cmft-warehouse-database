﻿CREATE TABLE [icwsys].[WardPlacementWardPlacementStatus](
	[WardPlacementWardPlacementStatusID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[WardPlacementStatusID] [int] NOT NULL,
	[WardPlacementStatusReasonID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
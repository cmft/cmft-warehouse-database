﻿CREATE TABLE [icwsys].[PBSRestrictionTreatmentPlan](
	[PBSRestrictionTreatmentPlanID] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentPlanID] [int] NOT NULL,
	[ProductID_Chemical] [int] NOT NULL,
	[PBSRestrictionID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
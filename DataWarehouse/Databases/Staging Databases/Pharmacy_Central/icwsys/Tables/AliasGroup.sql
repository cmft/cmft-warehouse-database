﻿CREATE TABLE [icwsys].[AliasGroup](
	[AliasGroupID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__AliasGrou___RowG__09635192]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[DisplayName] [varchar](50) NULL,
	[Format] [varchar](50) NULL,
	[UniqueValueRequired] [bit] NOT NULL DEFAULT ((1))
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[MessageStatus](
	[MessageStatusID] [int] IDENTITY(1,1) NOT NULL,
	[InterfaceComponentID] [int] NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL,
	[StatusID] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL CONSTRAINT [DF_MessageStatus_Timestamp]  DEFAULT (getdate()),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
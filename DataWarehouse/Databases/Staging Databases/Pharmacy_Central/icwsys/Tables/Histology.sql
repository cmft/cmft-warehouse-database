﻿CREATE TABLE [icwsys].[Histology](
	[RequestID] [int] NOT NULL,
	[PatientWaiting] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
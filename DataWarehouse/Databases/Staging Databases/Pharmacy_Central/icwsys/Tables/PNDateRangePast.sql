﻿CREATE TABLE [icwsys].[PNDateRangePast](
	[PNDateRangePastID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Days] [int] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
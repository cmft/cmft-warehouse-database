﻿CREATE TABLE [icwsys].[SpecsFinalV2](
	[Area] [varchar](31) NULL,
	[ASC-Code] [varchar](4) NULL,
	[ASC-Expansion] [varchar](26) NULL,
	[PAS-Specialty] [varchar](20) NULL,
	[PAS-Description] [varchar](34) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
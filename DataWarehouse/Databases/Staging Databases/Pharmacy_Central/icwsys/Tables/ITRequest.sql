﻿CREATE TABLE [icwsys].[ITRequest](
	[RequestID] [int] NOT NULL,
	[Date] [datetime] NULL,
	[ProblemDesc] [varchar](50) NULL,
	[OtherComments] [varchar](50) NULL,
	[DayID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
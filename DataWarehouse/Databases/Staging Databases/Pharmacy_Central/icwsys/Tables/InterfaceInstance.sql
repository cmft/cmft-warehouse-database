﻿CREATE TABLE [icwsys].[InterfaceInstance](
	[InterfaceInstanceID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[ServiceName] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
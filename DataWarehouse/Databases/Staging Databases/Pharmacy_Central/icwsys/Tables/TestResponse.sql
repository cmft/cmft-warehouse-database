﻿CREATE TABLE [icwsys].[TestResponse](
	[ResponseID] [int] NOT NULL,
	[Results] [varchar](50) NULL,
	[NewTextField] [char](30) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
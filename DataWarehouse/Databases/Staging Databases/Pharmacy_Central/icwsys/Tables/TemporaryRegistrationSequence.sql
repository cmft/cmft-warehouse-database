﻿CREATE TABLE [icwsys].[TemporaryRegistrationSequence](
	[SequenceID] [int] IDENTITY(1,1) NOT NULL,
	[DummyValue] [varchar](1) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ResponseType](
	[ResponseTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[DisplayOrder] [tinyint] NOT NULL CONSTRAINT [DF_ResponseType_DisplayOrder]  DEFAULT (1),
	[CurrentPhrase] [varchar](50) NOT NULL CONSTRAINT [DF_ResponseType_CurrentPhrase]  DEFAULT ('-'),
	[AmendmentPendingPhrase] [varchar](50) NOT NULL CONSTRAINT [DF_ResponseType_AmendmentPendingPhrase]  DEFAULT ('Amendment pending'),
	[SupercededPhrase] [varchar](50) NOT NULL CONSTRAINT [DF_ResponseType_SupercededPhrase]  DEFAULT ('Superceded'),
	[AutoCommit] [bit] NOT NULL CONSTRAINT [DF_ResponseType_AutoCommit]  DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ResponseT___RowG__6ADECA72]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[OrderingFrequency] [int] NOT NULL DEFAULT (0),
	[ExpirationStartTableID] [int] NULL,
	[ExpirationStartColumnID_Date] [int] NULL,
	[ExpirationStartColumnID_Time] [int] NULL
) ON [PRIMARY]

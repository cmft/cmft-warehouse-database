﻿CREATE TABLE [icwsys].[RepeatDispensingPatient](
	[RepeatDispensingPatientID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[SupplyDays] [int] NULL,
	[ADM] [bit] NULL,
	[InUse] [bit] NOT NULL,
	[SupplyPatternID] [int] NULL,
	[AdditionalInformation] [varchar](30) NULL,
	[RepeatDispensingBatchTemplateID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Updated] [datetime] NULL,
	[UpdatedBy] [int] NULL
) ON [PRIMARY]
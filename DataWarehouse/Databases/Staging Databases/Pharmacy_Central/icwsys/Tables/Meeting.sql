﻿CREATE TABLE [icwsys].[Meeting](
	[MeetingID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](256) NULL,
	[MeetingTypeID] [int] NOT NULL DEFAULT ((0)),
	[MeetingDateTime] [datetime] NOT NULL DEFAULT ((0)),
	[EntityID_Creator] [int] NOT NULL DEFAULT ((0)),
	[CreationDateTime] [datetime] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL,
	[LocationID] [int] NULL DEFAULT ((0)),
	[SharepointCalendarIdentifier] [varchar](128) NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [Meeting__RowGUID]  DEFAULT (newsequentialid()),
	[_RowVersion] [int] NOT NULL CONSTRAINT [Meeting__RowVersion]  DEFAULT ((1))
) ON [PRIMARY]
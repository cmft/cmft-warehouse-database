﻿CREATE TABLE [icwsys].[Modality](
	[ModalityID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[BlockUnresultedDuplicateOrders] [bit] NOT NULL DEFAULT (1),
	[ModalityID_Parent] [int] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
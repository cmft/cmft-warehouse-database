﻿CREATE TABLE [icwsys].[Sample](
	[SampleID] [int] IDENTITY(1,1) NOT NULL,
	[Data] [varchar](50) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PlannedItemDependancy](
	[PlannedItemDependancyID] [int] IDENTITY(1,1) NOT NULL,
	[PlannedItemID] [int] NOT NULL,
	[PlannedItemID_Requisit] [int] NULL,
	[TreatmentPlanID_Requisit] [int] NULL,
	[OffsetMinutes] [int] NOT NULL,
	[DependancyTypeID] [int] NULL,
	[IndexOrder] [int] NULL,
	[Mandatory] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
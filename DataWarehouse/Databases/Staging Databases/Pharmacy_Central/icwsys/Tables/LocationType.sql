﻿CREATE TABLE [icwsys].[LocationType](
	[LocationTypeID] [int] IDENTITY(1,1) NOT NULL,
	[LocationTypeID_Parent] [int] NOT NULL CONSTRAINT [DF_LocationType_LocationTypeID_Parent]  DEFAULT (0),
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__LocationT___RowG__376ED76E]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0)),
	[SupportsLayoutEditing] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

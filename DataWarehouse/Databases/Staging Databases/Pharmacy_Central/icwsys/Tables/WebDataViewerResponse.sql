﻿CREATE TABLE [icwsys].[WebDataViewerResponse](
	[NoteID] [int] NOT NULL,
	[XmlBlob] [text] NULL,
	[RequestType] [varchar](255) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
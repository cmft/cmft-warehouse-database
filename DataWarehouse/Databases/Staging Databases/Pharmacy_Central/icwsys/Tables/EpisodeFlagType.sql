﻿CREATE TABLE [icwsys].[EpisodeFlagType](
	[EpisodeFlagTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[Colour] [varchar](50) NULL DEFAULT ('NULL'),
	[Formula] [varchar](256) NULL DEFAULT ('NULL'),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[State](
	[StateID] [int] IDENTITY(1,1) NOT NULL,
	[SessionID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[PrimaryKey] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
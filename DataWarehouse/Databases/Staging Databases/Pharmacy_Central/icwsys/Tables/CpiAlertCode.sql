﻿CREATE TABLE [icwsys].[CpiAlertCode](
	[CpiAlertCodeID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[ShowInBedManagement] [bit] NOT NULL DEFAULT ((1)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
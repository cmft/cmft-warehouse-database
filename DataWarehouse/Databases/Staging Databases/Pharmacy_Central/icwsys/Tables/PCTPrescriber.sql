﻿CREATE TABLE [icwsys].[PCTPrescriber](
	[EntityID] [int] NOT NULL,
	[PCTPrescriberTypeID] [int] NOT NULL,
	[NZMCNumber] [varchar](10) NOT NULL,
	[PCTLegacyCode] [varchar](5) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ReprintItem](
	[ReprintItemID] [int] IDENTITY(1,1) NOT NULL,
	[ReprintBatchID] [int] NOT NULL,
	[PrintItemID] [int] NOT NULL,
	[ReprintedDate] [datetime] NULL,
	[PrintStatusID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
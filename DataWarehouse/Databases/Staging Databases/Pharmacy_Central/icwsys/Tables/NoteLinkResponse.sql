﻿CREATE TABLE [icwsys].[NoteLinkResponse](
	[NoteID] [int] NOT NULL,
	[ResponseID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

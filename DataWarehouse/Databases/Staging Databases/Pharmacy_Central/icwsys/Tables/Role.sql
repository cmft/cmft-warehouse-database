﻿CREATE TABLE [icwsys].[Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[Individual] [bit] NOT NULL CONSTRAINT [DF_Role_Individual]  DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL,
	[DefaultDesktop] [int] NOT NULL DEFAULT ((0)),
	[Source] [varchar](25) NOT NULL DEFAULT ('ICW')
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[MentalHealthTreatmentLinkProductExclusions](
	[MentalHealthTreatmentID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
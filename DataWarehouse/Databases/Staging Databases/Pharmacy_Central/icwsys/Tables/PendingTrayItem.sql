﻿CREATE TABLE [icwsys].[PendingTrayItem](
	[PendingTrayItemID] [int] IDENTITY(1,1) NOT NULL,
	[Data] [xml] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[EntityID_User] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[LocationID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
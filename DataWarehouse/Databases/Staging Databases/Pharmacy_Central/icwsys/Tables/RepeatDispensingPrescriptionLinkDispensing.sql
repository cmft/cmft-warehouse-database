﻿CREATE TABLE [icwsys].[RepeatDispensingPrescriptionLinkDispensing](
	[PrescriptionID] [int] NOT NULL,
	[DispensingID] [int] NOT NULL,
	[InUse] [bit] NULL,
	[Quantity] [float] NOT NULL,
	[RepeatDispensingPrescriptionLinkDispensingID] [int] IDENTITY(1,1) NOT NULL,
	[SessionLock] [int] NULL,
	[JVM] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Updated] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[RepeatTotal] [int] NOT NULL,
	[RepeatRemaining] [int] NOT NULL,
	[PrescriptionExpiry] [datetime] NULL
) ON [PRIMARY]
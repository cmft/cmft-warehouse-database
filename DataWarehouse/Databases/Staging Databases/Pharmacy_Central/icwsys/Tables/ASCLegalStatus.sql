﻿CREATE TABLE [icwsys].[ASCLegalStatus](
	[NoteID] [int] NOT NULL,
	[ActiveFrom] [datetime] NOT NULL,
	[Expiry] [datetime] NOT NULL,
	[LegalStatusCodeID] [int] NOT NULL,
	[Finished] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
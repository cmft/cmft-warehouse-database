﻿CREATE TABLE [icwsys].[Image](
	[ImageID] [int] IDENTITY(1,1) NOT NULL,
	[ImageTypeID] [int] NOT NULL,
	[Description] [varchar](128) NULL,
	[Detail] [varchar](1024) NULL,
	[ImageData] [varbinary](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ImageDate] [datetime] NULL,
	[EntityID] [int] NOT NULL,
	[_deleted] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
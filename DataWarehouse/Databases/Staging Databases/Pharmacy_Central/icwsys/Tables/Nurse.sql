﻿CREATE TABLE [icwsys].[Nurse](
	[EntityID] [int] NOT NULL,
	[RegNumber] [varchar](20) NOT NULL,
	[Grade] [varchar](20) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
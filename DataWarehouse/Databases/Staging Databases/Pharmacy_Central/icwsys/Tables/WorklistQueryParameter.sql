﻿CREATE TABLE [icwsys].[WorklistQueryParameter](
	[WorklistQueryParameterID] [int] IDENTITY(1,1) NOT NULL,
	[WorklistQueryID] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[WorklistParameterTypeID] [int] NOT NULL,
	[MaxTextLength] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
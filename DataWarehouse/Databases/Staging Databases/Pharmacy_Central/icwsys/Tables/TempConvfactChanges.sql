﻿CREATE TABLE [icwsys].[TempConvfactChanges](
	[NSVCode] [varchar](7) NULL,
	[new convfact] [float] NULL,
	[old convfact] [float] NULL,
	[tto] [float] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
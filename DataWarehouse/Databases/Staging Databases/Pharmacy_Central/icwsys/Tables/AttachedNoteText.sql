﻿CREATE TABLE [icwsys].[AttachedNoteText](
	[NoteID] [int] NOT NULL,
	[Detail] [varchar](8000) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

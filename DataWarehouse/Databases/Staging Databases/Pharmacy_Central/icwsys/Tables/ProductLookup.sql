﻿CREATE TABLE [icwsys].[ProductLookup](
	[ProductLookupID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[ProductIndexID] [int] NOT NULL,
	[ProductIndexGroupID] [int] NOT NULL,
	[ProductLookupTypeID] [int] NOT NULL,
	[Default] [bit] NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ProductLo___RowG__252DC99D]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
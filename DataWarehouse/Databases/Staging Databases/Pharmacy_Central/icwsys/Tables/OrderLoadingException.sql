﻿CREATE TABLE [icwsys].[OrderLoadingException](
	[OrderLoadingExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[OrderLoadingID] [int] NOT NULL,
	[SiteProductDataID] [int] NULL,
	[Description] [varchar](256) NULL,
	[wLookupID] [int] NULL,
	[DateTime] [datetime] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EntityType](
	[EntityTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EntityTyp___RowG__2DD5BC32]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[DSS] [bit] NOT NULL CONSTRAINT [DF_ENTITYTYPE_DSS]  DEFAULT ((0))
) ON [PRIMARY]

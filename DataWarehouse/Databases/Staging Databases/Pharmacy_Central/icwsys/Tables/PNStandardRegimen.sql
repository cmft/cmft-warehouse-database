﻿CREATE TABLE [icwsys].[PNStandardRegimen](
	[PNStandardRegimenID] [int] IDENTITY(1,1) NOT NULL,
	[RegimenName] [varchar](30) NOT NULL,
	[PerKilo] [bit] NOT NULL DEFAULT ((0)),
	[InUse] [bit] NOT NULL DEFAULT ((0)),
	[Description] [varchar](50) NULL,
	[Information] [varchar](max) NULL,
	[LastModDate] [datetime] NOT NULL DEFAULT (getdate()),
	[LastModEntityID_User] [int] NOT NULL,
	[LastModTerminal] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
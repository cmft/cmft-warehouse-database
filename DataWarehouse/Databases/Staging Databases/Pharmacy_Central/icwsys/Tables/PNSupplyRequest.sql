﻿CREATE TABLE [icwsys].[PNSupplyRequest](
	[RequestID] [int] NOT NULL,
	[BatchNumber] [varchar](30) NULL,
	[AdminStartDate] [datetime] NULL,
	[NumberOfLabelsAminoCombined] [int] NULL,
	[NumberOfLabelsLipid] [int] NULL,
	[BaxaCompounder] [bit] NULL,
	[BaxaIncludeLipid] [bit] NULL,
	[PreperationDate] [datetime] NOT NULL DEFAULT (getdate()),
	[ExpiryDaysAqueousCombined] [int] NOT NULL DEFAULT ((1)),
	[ExpiryDaysLipid] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
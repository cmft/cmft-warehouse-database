﻿CREATE TABLE [icwsys].[CounselResponse](
	[ResponseID] [int] NOT NULL,
	[Comment] [char](100) NULL,
	[CounsellingResponseID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WaitingTime](
	[RequestID] [int] NOT NULL,
	[DateEntered] [datetime] NULL,
	[Comment] [char](100) NULL,
	[TimeEntered] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
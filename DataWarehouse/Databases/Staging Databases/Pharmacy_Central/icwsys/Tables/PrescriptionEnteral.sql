﻿CREATE TABLE [icwsys].[PrescriptionEnteral](
	[RequestID] [int] NOT NULL,
	[UnitID_InfusionDuration] [int] NULL,
	[UnitID_RateMass] [int] NULL,
	[UnitID_RateTime] [int] NULL,
	[Continuous] [bit] NULL,
	[InfusionDuration] [float] NULL,
	[InfusionDurationLow] [float] NULL,
	[Rate] [float] NULL,
	[RateMin] [float] NULL,
	[RateMax] [float] NULL,
	[SupplimentaryText] [varchar](1024) NULL,
	[EnteralDeviceID] [int] NULL,
	[RateCalculation_XML] [text] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
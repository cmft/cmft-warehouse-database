﻿CREATE TABLE [icwsys].[WPatientSpecificOrder](
	[WPatientSpecificOrderID] [int] IDENTITY(1,1) NOT NULL,
	[PSO_RequestID] [int] NOT NULL,
	[PSOText] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
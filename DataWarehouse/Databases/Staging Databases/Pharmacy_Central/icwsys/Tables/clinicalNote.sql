﻿CREATE TABLE [icwsys].[clinicalNote](
	[NoteID] [int] NOT NULL,
	[ClinicalNotes] [varchar](50) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[DispenseResponse](
	[ResponseID] [int] NOT NULL,
	[Done] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
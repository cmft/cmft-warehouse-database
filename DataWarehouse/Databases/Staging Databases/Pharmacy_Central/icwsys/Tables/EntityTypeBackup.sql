﻿CREATE TABLE [icwsys].[EntityTypeBackup](
	[EntityTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
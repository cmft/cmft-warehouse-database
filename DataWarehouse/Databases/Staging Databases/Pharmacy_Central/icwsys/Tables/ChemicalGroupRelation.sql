﻿CREATE TABLE [icwsys].[ChemicalGroupRelation](
	[ChemicalGroupID] [int] NOT NULL,
	[ChemicalGroupID_Related] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ChemicalG___RowG__116E87AE]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

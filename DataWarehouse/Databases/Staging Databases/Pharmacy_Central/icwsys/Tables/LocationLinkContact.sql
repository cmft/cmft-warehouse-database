﻿CREATE TABLE [icwsys].[LocationLinkContact](
	[LocationID] [int] NOT NULL,
	[ContactID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
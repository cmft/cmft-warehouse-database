﻿CREATE TABLE [icwsys].[OrderTemplatePermission](
	[OrderTemplatePermissionID] [int] IDENTITY(1,1) NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[AllowView] [bit] NOT NULL CONSTRAINT [DF_OrderTemplatePermission_AllowView]  DEFAULT (1),
	[_TableVersion] [timestamp] NOT NULL,
	[AllowTranscription] [bit] NOT NULL DEFAULT ((0)),
	[AllowPrescription] [bit] NOT NULL DEFAULT ((1)),
	[AllowVerbalOrder] [bit] NOT NULL DEFAULT ((0)),
	[AllowOnBehalfOf] [bit] NOT NULL DEFAULT ((0)),
	[AllowPGDHomelyRemedy] [bit] NOT NULL DEFAULT ((0)),
	[AllowPGDPrePack] [bit] NOT NULL DEFAULT ((0)),
	[AllowSupplementaryPrescription] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
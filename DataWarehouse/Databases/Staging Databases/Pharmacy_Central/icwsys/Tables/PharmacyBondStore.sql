﻿CREATE TABLE [icwsys].[PharmacyBondStore](
	[PharmacyBondStoreID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[NSVCode] [varchar](7) NOT NULL,
	[Description] [varchar](56) NOT NULL,
	[Expiry] [datetime] NULL,
	[Issue_To_Bond] [datetime] NOT NULL,
	[BatchNumber] [varchar](25) NULL,
	[Qty] [float] NOT NULL,
	[QAQty] [float] NOT NULL,
	[TotalCostExTax] [float] NOT NULL,
	[TotalCostTax] [float] NOT NULL,
	[TotalCostIncTax] [float] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EpisodeMergeOrder](
	[RequestID] [int] NOT NULL,
	[EpisodeID_Master] [int] NOT NULL,
	[EpisodeID_Merged] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
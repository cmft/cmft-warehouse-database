﻿CREATE TABLE [icwsys].[ObservationSource](
	[ObservationSourceID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WlabelAudit](
	[WlabelAuditID] [int] IDENTITY(1,1) NOT NULL,
	[DateChanged] [datetime] NOT NULL DEFAULT (getdate()),
	[RequestID_Dispensing] [int] NOT NULL,
	[SiteID] [int] NOT NULL,
	[UserID] [varchar](3) NOT NULL,
	[EntityID_User] [int] NOT NULL,
	[NewLabelText] [varchar](255) NULL,
	[OldLabelText] [varchar](255) NULL,
	[NewWarcode] [varchar](12) NULL,
	[OldWarcode] [varchar](12) NULL,
	[NewInscode] [varchar](12) NULL,
	[OldInscode] [varchar](12) NULL,
	[NewIssType] [varchar](1) NULL,
	[OldIssType] [varchar](1) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Control](
	[ControlID] [int] IDENTITY(1,1) NOT NULL,
	[ControlTypeID] [int] NOT NULL,
	[ControlSpanID] [int] NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[TabIndex] [int] NOT NULL,
	[BreakAfter] [bit] NOT NULL,
	[Sync] [bit] NOT NULL,
	[Caption] [varchar](1024) NULL,
	[Bold] [bit] NOT NULL,
	[Italic] [bit] NOT NULL,
	[LineCount] [int] NULL,
	[Tag] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[ControlLabel] [varchar](max) NULL,
	[ControlLabelPosition] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
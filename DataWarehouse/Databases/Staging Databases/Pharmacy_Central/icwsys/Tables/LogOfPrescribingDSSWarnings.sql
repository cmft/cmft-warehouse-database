﻿CREATE TABLE [icwsys].[LogOfPrescribingDSSWarnings](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[OrderTemplateID] [int] NOT NULL,
	[RequestID] [int] NULL,
	[EpisodeID] [int] NOT NULL,
	[AuthorisingEntityID] [int] NOT NULL,
	[EntityLocationID] [int] NOT NULL,
	[UserResponse] [int] NOT NULL,
	[Detail] [text] NULL,
	[PendingItemID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
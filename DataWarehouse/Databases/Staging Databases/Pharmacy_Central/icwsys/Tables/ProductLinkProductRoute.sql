﻿CREATE TABLE [icwsys].[ProductLinkProductRoute](
	[ProductID] [int] NOT NULL,
	[ProductRouteID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ProductLi___RowG__33C39FB2]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

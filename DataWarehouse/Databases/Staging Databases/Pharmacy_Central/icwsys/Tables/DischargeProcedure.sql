﻿CREATE TABLE [icwsys].[DischargeProcedure](
	[NoteID] [int] NOT NULL,
	[OrderCatalogueID_Procedure] [int] NOT NULL,
	[Notes] [varchar](4000) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

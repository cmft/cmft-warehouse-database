﻿CREATE TABLE [icwsys].[EpisodeOrderLinkDssWarningOverride](
	[RequestID] [int] NOT NULL,
	[DssWarningOverrideID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
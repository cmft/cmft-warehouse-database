﻿CREATE TABLE [icwsys].[PCTTransactionStatus](
	[PCTTransactionStatusID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [char](1) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ReportGroup](
	[ReportGroupID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Detail] [varchar](1024) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ReportGro___RowG__58FC4A70]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

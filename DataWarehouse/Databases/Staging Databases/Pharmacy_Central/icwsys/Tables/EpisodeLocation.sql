﻿CREATE TABLE [icwsys].[EpisodeLocation](
	[EpisodeLocationID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL DEFAULT (getdate()),
	[EndDate] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EpisodeLo___RowG__3D27B0C4]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
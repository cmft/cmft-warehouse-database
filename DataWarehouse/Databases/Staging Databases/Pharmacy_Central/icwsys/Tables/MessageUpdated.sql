﻿CREATE TABLE [icwsys].[MessageUpdated](
	[MessageUpdatedID] [int] IDENTITY(1,1) NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL,
	[TableID] [int] NOT NULL,
	[PrimaryKey] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
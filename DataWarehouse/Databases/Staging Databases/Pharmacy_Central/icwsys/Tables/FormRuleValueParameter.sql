﻿CREATE TABLE [icwsys].[FormRuleValueParameter](
	[FormRuleValueParameterID] [int] IDENTITY(1,1) NOT NULL,
	[FormRuleValueID] [int] NULL,
	[Description] [varchar](50) NOT NULL,
	[Value] [varchar](128) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
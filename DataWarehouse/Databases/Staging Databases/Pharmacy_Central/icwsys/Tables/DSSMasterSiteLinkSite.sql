﻿CREATE TABLE [icwsys].[DSSMasterSiteLinkSite](
	[DSSMasterSiteLinkSiteID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[DSSMasterSiteID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
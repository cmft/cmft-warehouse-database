﻿CREATE TABLE [icwsys].[FormRule](
	[FormRuleID] [int] IDENTITY(1,1) NOT NULL,
	[FormRuleEventID] [int] NULL,
	[Name] [varchar](50) NOT NULL,
	[ReturnValue] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
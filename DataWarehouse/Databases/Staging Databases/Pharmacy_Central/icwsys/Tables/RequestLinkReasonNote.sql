﻿CREATE TABLE [icwsys].[RequestLinkReasonNote](
	[RequestID] [int] NOT NULL,
	[NoteID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
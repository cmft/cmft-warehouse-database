﻿CREATE TABLE [icwsys].[WindowEvent](
	[WindowEventID] [int] IDENTITY(1,1) NOT NULL,
	[WindowID] [int] NOT NULL,
	[ToolMenuID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
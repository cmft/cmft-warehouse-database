﻿CREATE TABLE [icwsys].[ProductRoute](
	[ProductRouteID] [int] IDENTITY(1,1) NOT NULL,
	[ProductRouteID_Parent] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[Infusion] [bit] NOT NULL CONSTRAINT [DF_ProductRoute_Infusion]  DEFAULT (0),
	[Topical] [bit] NOT NULL CONSTRAINT [DF_ProductRoute_Topical]  DEFAULT (0),
	[ProductRouteSimpleID] [int] NOT NULL CONSTRAINT [DF_ProductRoute_ProductRouteSimpleID]  DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ProductRo___RowG__0F513512]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]

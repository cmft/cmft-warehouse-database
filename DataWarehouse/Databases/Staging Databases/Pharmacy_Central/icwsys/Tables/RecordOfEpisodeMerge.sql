﻿CREATE TABLE [icwsys].[RecordOfEpisodeMerge](
	[NoteID] [int] NOT NULL,
	[EntityID_before] [int] NOT NULL,
	[EntityID_after] [int] NOT NULL,
	[EpisodeID_Parent_before] [int] NOT NULL,
	[EpisodeID_Parent_after] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
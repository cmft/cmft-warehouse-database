﻿CREATE TABLE [icwsys].[PatientNote](
	[NoteID] [int] NOT NULL,
	[Notes] [varchar](50) NULL,
	[HighPriority] [bit] NULL,
	[SuppliedBy] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[CustomDataBuilder](
	[CustomDataBuilderId] [int] IDENTITY(1,1) NOT NULL,
	[ServerName] [nvarchar](256) NULL,
	[Building] [bit] NULL,
	[BuildRequired] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
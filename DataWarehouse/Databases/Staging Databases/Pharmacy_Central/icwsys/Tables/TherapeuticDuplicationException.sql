﻿CREATE TABLE [icwsys].[TherapeuticDuplicationException](
	[TherapeuticDuplicationExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID_A] [int] NOT NULL,
	[ProductRouteID_A] [int] NOT NULL,
	[ProductID_B] [int] NOT NULL,
	[ProductRouteID_B] [int] NOT NULL,
	[DSS] [bit] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]

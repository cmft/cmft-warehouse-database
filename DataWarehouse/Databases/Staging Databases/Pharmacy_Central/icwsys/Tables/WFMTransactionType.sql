﻿CREATE TABLE [icwsys].[WFMTransactionType](
	[WFMTransactionTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[PharmacyLog] [char](1) NOT NULL,
	[Kind] [char](1) NOT NULL,
	[_Deleted] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[AuditLogException](
	[AuditLogExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[Ward](
	[LocationID] [int] NOT NULL,
	[WardTypeID] [int] NOT NULL,
	[Male] [bit] NOT NULL,
	[Female] [bit] NOT NULL,
	[SingleRooms] [int] NOT NULL,
	[out_of_use] [bit] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL,
	[WardGroupID] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[ProductFormula](
	[NoteID] [int] NOT NULL,
	[RequestID] [int] NOT NULL,
	[ProductID_Reconstitution] [int] NULL,
	[ProductID_Diluent] [int] NULL,
	[UnitID_ReconConcentrationMass] [int] NOT NULL,
	[UnitID_ConcentrationMass] [int] NOT NULL,
	[ReconstitutionVolumeML] [float] NULL,
	[ReconstitutionConcentration] [float] NULL,
	[DiluentVolumeML] [float] NULL,
	[FinalConcentration] [float] NULL,
	[FinalVolumeML] [float] NULL,
	[InstructionText] [varchar](5120) NULL,
	[WarningText] [varchar](1024) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
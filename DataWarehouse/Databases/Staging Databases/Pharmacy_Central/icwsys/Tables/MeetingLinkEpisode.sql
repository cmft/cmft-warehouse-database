﻿CREATE TABLE [icwsys].[MeetingLinkEpisode](
	[MeetingID] [int] NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
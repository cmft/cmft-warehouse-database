﻿CREATE TABLE [icwsys].[WardGroupSite](
	[WardGroupID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
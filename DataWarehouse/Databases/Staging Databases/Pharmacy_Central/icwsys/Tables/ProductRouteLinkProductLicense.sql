﻿CREATE TABLE [icwsys].[ProductRouteLinkProductLicense](
	[ProductRouteID] [int] NOT NULL,
	[ProductLicenseID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[InterfaceMessageRouting](
	[InterfaceMessageRoutingID] [int] IDENTITY(1,1) NOT NULL,
	[InterfaceComponentID] [int] NOT NULL,
	[MessageType] [varchar](512) NOT NULL,
	[TableID_Destination] [int] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
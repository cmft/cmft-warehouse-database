﻿CREATE TABLE [icwsys].[Episode](
	[EpisodeID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID_Parent] [int] NOT NULL CONSTRAINT [DF_Episode_EpisodeID_Parent]  DEFAULT (0),
	[StatusID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[EpisodeTypeID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Episode_DateCreated]  DEFAULT (getdate()),
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[CaseNo] [varchar](25) NULL CONSTRAINT [DF_Episode_CaseNo]  DEFAULT ('None'),
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Episode___RowGUI__133176F8]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

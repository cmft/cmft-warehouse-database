﻿CREATE TABLE [icwsys].[temp_product](
	[ProductID] [int] NULL,
	[ProductID_Local] [int] NULL,
	[Type] [varchar](128) NULL,
	[LookupType] [varchar](128) NULL,
	[IndexGroup] [varchar](128) NULL,
	[Description] [varchar](128) NULL,
	[Manufacturer] [varchar](128) NULL,
	[Tradename] [varchar](128) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
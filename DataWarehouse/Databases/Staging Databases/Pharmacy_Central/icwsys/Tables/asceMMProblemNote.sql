﻿CREATE TABLE [icwsys].[asceMMProblemNote](
	[NoteID] [int] NOT NULL,
	[asceMMProblemID] [int] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL,
	[Comments] [varchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PBSHICStatus2Include](
	[HICStatus] [varchar](8) NOT NULL,
	[Description] [varchar](250) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
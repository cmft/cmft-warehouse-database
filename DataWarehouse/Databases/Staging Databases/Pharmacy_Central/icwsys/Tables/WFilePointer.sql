﻿CREATE TABLE [icwsys].[WFilePointer](
	[WFilePointerID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Category] [varchar](255) NOT NULL,
	[PointerID] [int] NOT NULL,
	[DSSMasterSiteID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[LocationLinkStatus](
	[LocationID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

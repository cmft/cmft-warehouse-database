﻿CREATE TABLE [icwsys].[MonitorJob](
	[MonitorJobID] [int] IDENTITY(1,1) NOT NULL,
	[InterfaceInstanceID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[MonitorScheduleID] [int] NOT NULL,
	[NextRun] [datetime] NOT NULL,
	[LastRun] [datetime] NOT NULL CONSTRAINT [DF_MonitorJob_LastRun]  DEFAULT (getdate()),
	[LastRun_StatusID] [int] NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_MonitorJob_Active]  DEFAULT ((1)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PNStandardRegimenPNCodeVolume](
	[PNStandardRegimenPNCodeVolumeID] [int] IDENTITY(1,1) NOT NULL,
	[PNStandardRegimenID] [int] NOT NULL,
	[PNCode] [varchar](8) NOT NULL,
	[Volume] [float] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
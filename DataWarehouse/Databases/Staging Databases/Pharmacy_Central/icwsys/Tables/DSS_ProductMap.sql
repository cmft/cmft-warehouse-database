﻿CREATE TABLE [icwsys].[DSS_ProductMap](
	[ProductID] [int] NOT NULL,
	[CHEM_ID] [int] NULL CONSTRAINT [DF_DSS_ProductMap_CHEM_ID]  DEFAULT (0),
	[TM_ID] [int] NULL CONSTRAINT [DF_DSS_ProductMap_TM_ID]  DEFAULT (0),
	[AMP_ID] [int] NULL CONSTRAINT [DF_DSS_ProductMap_AMP_ID]  DEFAULT (0),
	[AMPP_ID] [int] NULL CONSTRAINT [DF_DSS_ProductMap_AMPP_ID]  DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[DischargeComorbidity](
	[NoteID] [int] NOT NULL,
	[OrderCatalogueID_Comorbidity] [int] NOT NULL,
	[Notes] [varchar](4000) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

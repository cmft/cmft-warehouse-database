﻿CREATE TABLE [icwsys].[EntityImage](
	[ImageID] [int] NOT NULL,
	[EntityID_Subject] [int] NOT NULL,
	[DefaultImage] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
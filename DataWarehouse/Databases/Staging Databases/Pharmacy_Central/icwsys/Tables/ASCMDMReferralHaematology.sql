﻿CREATE TABLE [icwsys].[ASCMDMReferralHaematology](
	[RequestID] [int] NOT NULL,
	[Date_Pathology_3] [datetime] NULL,
	[PathologySite_3] [varchar](50) NULL,
	[PathologySpecimen_3] [varchar](20) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
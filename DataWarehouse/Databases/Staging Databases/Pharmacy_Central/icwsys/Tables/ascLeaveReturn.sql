﻿CREATE TABLE [icwsys].[ascLeaveReturn](
	[ResponseID] [int] NOT NULL,
	[LeaveResultID] [int] NOT NULL,
	[ReturnedDate] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
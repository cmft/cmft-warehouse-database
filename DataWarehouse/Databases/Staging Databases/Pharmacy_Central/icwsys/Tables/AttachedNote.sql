﻿CREATE TABLE [icwsys].[AttachedNote](
	[NoteID] [int] NOT NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_AttachedNote_Enabled]  DEFAULT (1),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[SpeedTest](
	[SpeedTestID] [int] IDENTITY(1,1) NOT NULL,
	[VarcharValue] [varchar](50) NULL,
	[IntValue] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
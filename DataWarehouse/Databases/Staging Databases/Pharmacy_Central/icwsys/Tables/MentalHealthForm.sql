﻿CREATE TABLE [icwsys].[MentalHealthForm](
	[NoteID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Address] [varchar](300) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WLookup](
	[WLookupID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[SiteID] [int] NOT NULL,
	[WLookupContextID] [int] NOT NULL,
	[Value] [varchar](1024) NOT NULL,
	[InUse] [bit] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__wLookup___RowGUI__13641F1D]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[DSS] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[NoteTypeLinkNoteGroup](
	[NoteTypeID] [int] NOT NULL,
	[NoteGroupID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
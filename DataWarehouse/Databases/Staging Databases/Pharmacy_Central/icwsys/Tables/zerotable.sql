﻿CREATE TABLE [icwsys].[zerotable](
	[table] [varchar](128) NULL,
	[column] [varchar](128) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
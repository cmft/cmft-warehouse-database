﻿CREATE TABLE [icwsys].[PermissionRoutine](
	[PolicyRoutineID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[RoutineID] [int] NOT NULL,
	[Allow] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
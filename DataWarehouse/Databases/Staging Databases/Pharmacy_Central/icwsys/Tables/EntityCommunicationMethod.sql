﻿CREATE TABLE [icwsys].[EntityCommunicationMethod](
	[EntityCommunicationMethodID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[CommunicationMethodTypeID] [int] NOT NULL,
	[Value] [varchar](128) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
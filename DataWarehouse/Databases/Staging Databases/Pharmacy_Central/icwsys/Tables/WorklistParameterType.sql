﻿CREATE TABLE [icwsys].[WorklistParameterType](
	[WorklistParameterTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Detail] [varchar](512) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ResultsViewerQuickSearch](
	[ResultsViewerQuickSearchID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[EntityID] [int] NOT NULL DEFAULT ((-1)),
	[QuickSearchCriteria] [text] NOT NULL DEFAULT (''),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
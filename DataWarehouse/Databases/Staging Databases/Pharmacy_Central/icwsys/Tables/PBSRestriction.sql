﻿CREATE TABLE [icwsys].[PBSRestriction](
	[PBSRestrictionID] [int] NOT NULL,
	[RestrictionText] [varchar](max) NOT NULL,
	[AuthorityMethod] [char](1) NULL,
	[Out_Of_Use] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
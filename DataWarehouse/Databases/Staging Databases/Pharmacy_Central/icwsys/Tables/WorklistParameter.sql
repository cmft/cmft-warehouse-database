﻿CREATE TABLE [icwsys].[WorklistParameter](
	[WorklistParameterID] [int] IDENTITY(1,1) NOT NULL,
	[WorklistID] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[OrdinalPosition] [int] NOT NULL,
	[WorklistParameterTypeID] [int] NOT NULL,
	[RoutineID] [int] NULL,
	[DefaultValue] [varchar](1024) NULL,
	[MaxTextLength] [int] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ScheduleActivity](
	[ScheduleActivityID] [int] IDENTITY(1,1) NOT NULL,
	[Token] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
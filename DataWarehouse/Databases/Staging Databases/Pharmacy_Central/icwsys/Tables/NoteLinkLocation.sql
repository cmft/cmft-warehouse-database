﻿CREATE TABLE [icwsys].[NoteLinkLocation](
	[NoteID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[AdditionalRoutine](
	[RoutineID_Parent] [int] NOT NULL,
	[RoutineID_Child] [int] NOT NULL,
	[Order] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
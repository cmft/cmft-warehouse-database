﻿CREATE TABLE [icwsys].[NewCostCentresSFM](
	[Supplier] [nvarchar](255) NULL,
	[FullName] [nvarchar](255) NULL,
	[v8CostCentre] [nvarchar](255) NULL,
	[WebCostCentre] [nvarchar](255) NULL,
	[LenV8] [float] NULL,
	[Interim] [nvarchar](255) NULL,
	[LenInterims] [float] NULL,
	[Final] [float] NULL,
	[Lenfinal] [float] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
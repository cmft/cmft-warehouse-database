﻿CREATE TABLE [icwsys].[WFMDailyStockLevel](
	[NSVCode] [char](7) NOT NULL,
	[LocationID_Site] [int] NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[StockLevelInIssueUnits] [real] NOT NULL,
	[StockValueExVat] [real] NOT NULL,
	[StockValueIncVat] [real] NOT NULL,
	[StockValueVat] [real] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
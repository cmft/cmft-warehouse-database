﻿CREATE TABLE [icwsys].[OutPatientAppointment](
	[EpisodeID] [int] NOT NULL,
	[ArrivalDateTime] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
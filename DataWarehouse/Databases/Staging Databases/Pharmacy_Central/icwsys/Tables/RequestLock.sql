﻿CREATE TABLE [icwsys].[RequestLock](
	[RequestLockID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[EntityID_User] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[DesktopID] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[LockCount] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[wStoreArchiveSource](
	[SourceID] [int] NOT NULL,
	[SourceName] [varchar](100) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
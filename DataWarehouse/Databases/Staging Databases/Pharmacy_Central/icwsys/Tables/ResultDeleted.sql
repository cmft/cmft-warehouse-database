﻿CREATE TABLE [icwsys].[ResultDeleted](
	[NoteID] [int] NOT NULL,
	[Comments] [varchar](221) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
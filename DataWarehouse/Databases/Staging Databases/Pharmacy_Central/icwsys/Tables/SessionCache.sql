﻿CREATE TABLE [icwsys].[SessionCache](
	[SessionCacheID] [int] IDENTITY(1,1) NOT NULL,
	[SessionID] [int] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[SerializedState] [varbinary](max) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[ApplicationName] [nvarchar](128) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WTransLogExtra](
	[WTransLogID] [int] NOT NULL,
	[YearMonth] [varchar](6) NULL,
	[LabelText] [varchar](255) NULL,
	[StartDate] [int] NULL,
	[BatchNumber] [int] NULL,
	[LocationID_Site] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[WtranslogExtraID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[PrescriptionCreationType](
	[PrescriptionCreationTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](200) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
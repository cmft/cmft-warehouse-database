﻿CREATE TABLE [icwsys].[Schedule](
	[ScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[StopDate] [datetime] NULL,
	[LocationID] [int] NOT NULL CONSTRAINT [DF_Schedule_LocationID]  DEFAULT (0),
	[ScheduleTemplateID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
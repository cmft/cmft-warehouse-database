﻿CREATE TABLE [icwsys].[Counselling](
	[RequestID] [int] NOT NULL,
	[Comment] [char](100) NULL,
	[CounsellingTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
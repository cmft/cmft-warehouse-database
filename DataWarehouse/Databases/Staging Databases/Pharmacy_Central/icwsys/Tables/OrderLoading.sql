﻿CREATE TABLE [icwsys].[OrderLoading](
	[OrderLoadingID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[SiteID] [int] NOT NULL,
	[LoadingNumber] [int] NOT NULL,
	[CreatedUser_EntityID] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[UpdatedUser_EntityID] [int] NULL,
	[UpdatedDateTime] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
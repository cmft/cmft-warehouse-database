﻿CREATE TABLE [icwsys].[ProductOrder](
	[RequestID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NULL,
	[PrintableDirection] [varchar](72) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[PrescriptionCreationTypeID] [int] NOT NULL DEFAULT ((1)),
	[AuthorisingUser] [varchar](50) NULL
) ON [PRIMARY]
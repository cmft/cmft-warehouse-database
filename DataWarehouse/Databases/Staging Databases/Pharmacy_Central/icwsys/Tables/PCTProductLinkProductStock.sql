﻿CREATE TABLE [icwsys].[PCTProductLinkProductStock](
	[PCTProductLinkProductStockID] [int] IDENTITY(1,1) NOT NULL,
	[ProductStockID] [int] NOT NULL,
	[PCTMasterProductID] [int] NOT NULL,
	[Primary] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
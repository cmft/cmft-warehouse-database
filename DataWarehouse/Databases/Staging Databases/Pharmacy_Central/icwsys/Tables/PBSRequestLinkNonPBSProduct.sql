﻿CREATE TABLE [icwsys].[PBSRequestLinkNonPBSProduct](
	[RequestID] [int] NOT NULL,
	[ProductName] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
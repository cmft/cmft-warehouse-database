﻿CREATE TABLE [icwsys].[SupportResponse](
	[ResponseID] [int] NOT NULL,
	[Date] [datetime] NULL,
	[Comments] [text] NULL,
	[Complete] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
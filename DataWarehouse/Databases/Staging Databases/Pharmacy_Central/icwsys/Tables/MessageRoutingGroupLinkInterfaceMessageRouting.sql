﻿CREATE TABLE [icwsys].[MessageRoutingGroupLinkInterfaceMessageRouting](
	[MessageRoutingGroupID] [int] NOT NULL,
	[InterfaceMessageRoutingID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
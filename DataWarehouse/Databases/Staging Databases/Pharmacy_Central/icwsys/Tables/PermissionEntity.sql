﻿CREATE TABLE [icwsys].[PermissionEntity](
	[PolicyEntityID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[EntityTypeID] [int] NOT NULL,
	[Allow] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
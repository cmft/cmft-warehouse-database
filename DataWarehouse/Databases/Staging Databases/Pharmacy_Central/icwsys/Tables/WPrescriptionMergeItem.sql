﻿CREATE TABLE [icwsys].[WPrescriptionMergeItem](
	[WPrescriptionMergeItemID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID_WPrescriptionMerge] [int] NOT NULL,
	[RequestID_Prescription] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[IndexOrder] [int] NOT NULL DEFAULT ((1)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
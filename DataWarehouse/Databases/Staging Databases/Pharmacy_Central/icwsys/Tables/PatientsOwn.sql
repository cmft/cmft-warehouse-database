﻿CREATE TABLE [icwsys].[PatientsOwn](
	[NoteID] [int] NOT NULL,
	[POMStatusID] [int] NOT NULL DEFAULT (1),
	[Comments] [varchar](3000) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
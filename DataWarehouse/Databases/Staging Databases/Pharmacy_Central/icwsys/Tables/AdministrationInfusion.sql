﻿CREATE TABLE [icwsys].[AdministrationInfusion](
	[ResponseID] [int] NOT NULL,
	[Volume] [float] NOT NULL,
	[UnitID_Volume] [int] NOT NULL,
	[Action] [varchar](50) NOT NULL,
	[Ended] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[FlowRate] [float] NULL,
	[UnitID_FlowRate] [int] NULL,
	[UnitID_FlowRateTime] [int] NULL
) ON [PRIMARY]

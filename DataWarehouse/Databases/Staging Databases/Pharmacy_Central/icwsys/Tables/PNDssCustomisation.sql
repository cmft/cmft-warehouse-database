﻿CREATE TABLE [icwsys].[PNDssCustomisation](
	[PNDssCustomisationID] [int] IDENTITY(1,1) NOT NULL,
	[PNProductID] [int] NULL,
	[PNRuleID] [int] NULL,
	[CustomerID] [uniqueidentifier] NOT NULL,
	[TableID] [int] NOT NULL,
	[ParameterName] [varchar](50) NOT NULL,
	[Value] [varchar](max) NULL,
	[LastModDate] [datetime] NOT NULL,
	[LastModUser] [varchar](3) NULL,
	[LastModTerm] [varchar](15) NULL,
	[ExtraInfo] [varchar](max) NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
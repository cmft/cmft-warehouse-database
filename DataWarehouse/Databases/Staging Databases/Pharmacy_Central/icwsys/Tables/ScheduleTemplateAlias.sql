﻿CREATE TABLE [icwsys].[ScheduleTemplateAlias](
	[ScheduleAliasID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleTemplateID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Description] [varchar](512) NOT NULL,
	[Alias] [varchar](12) NOT NULL,
	[Default] [bit] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ScheduleT___RowG__7C3E609E]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[PRN] [bit] NOT NULL DEFAULT ((0)),
	[ScheduleTemplateAliasID]  AS ([ScheduleAliasID])
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[RxTrackerBookOut](
	[ResponseID] [int] NOT NULL,
	[Comments] [text] NULL,
	[RxTrackerReturnMethodID] [int] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[InterventionNote](
	[NoteID] [int] NOT NULL,
	[Detail] [varchar](2048) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

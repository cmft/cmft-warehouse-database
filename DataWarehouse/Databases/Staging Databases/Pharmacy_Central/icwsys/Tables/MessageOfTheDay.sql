﻿CREATE TABLE [icwsys].[MessageOfTheDay](
	[MessageOfTheDayID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](150) NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[Importance] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EntityLinkStatus](
	[EntityID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

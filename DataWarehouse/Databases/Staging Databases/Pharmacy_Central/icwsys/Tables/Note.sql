﻿CREATE TABLE [icwsys].[Note](
	[NoteID] [int] IDENTITY(1,1) NOT NULL,
	[NoteTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[NoteID_Thread] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Note_CreatedDate]  DEFAULT (getdate()),
	[_TableVersion] [timestamp] NOT NULL,
	[LocationID] [int] NOT NULL DEFAULT ((0)),
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Note___RowGUID__1504B0D1]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0)),
	[OriginID] [int] NOT NULL CONSTRAINT [DF_Note_OriginID]  DEFAULT ((0))
) ON [PRIMARY]

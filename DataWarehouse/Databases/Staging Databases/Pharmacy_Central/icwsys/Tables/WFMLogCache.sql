﻿CREATE TABLE [icwsys].[WFMLogCache](
	[WLogID] [int] NOT NULL,
	[PharmacyLog] [char](1) NOT NULL,
	[RuleCode] [smallint] NOT NULL,
	[AccountCode_Debit] [smallint] NOT NULL,
	[AccountCode_Credit] [smallint] NOT NULL,
	[LocationID_Site] [int] NULL,
	[SupCode] [char](5) NOT NULL,
	[WardCode] [char](5) NOT NULL,
	[OrderNum] [varchar](10) NOT NULL,
	[NSVCode] [char](7) NOT NULL,
	[Qty] [real] NULL,
	[CostExVat] [real] NULL,
	[CostIncVat] [real] NULL,
	[VatCost] [real] NULL,
	[LogDateTime] [datetime] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[AccountCodeType] [char](1) NOT NULL
) ON [PRIMARY]
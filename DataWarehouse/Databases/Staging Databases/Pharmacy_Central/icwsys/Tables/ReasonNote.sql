﻿CREATE TABLE [icwsys].[ReasonNote](
	[NoteID] [int] NOT NULL,
	[OrderCatalogueID] [int] NULL,
	[ArbTextID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
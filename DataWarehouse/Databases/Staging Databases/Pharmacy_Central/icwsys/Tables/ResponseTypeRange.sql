﻿CREATE TABLE [icwsys].[ResponseTypeRange](
	[ResponseTypeRangeID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseTypeID] [int] NOT NULL,
	[ColumnID] [int] NOT NULL,
	[RangeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[ConsListAmmend](
	[AscConCode] [varchar](4) NULL,
	[AscCONSNAME] [varchar](26) NULL,
	[AscSpecialties] [varchar](4) NULL,
	[PASConCode] [varchar](5) NULL,
	[PASConName] [varchar](26) NULL,
	[Instruction] [varchar](51) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
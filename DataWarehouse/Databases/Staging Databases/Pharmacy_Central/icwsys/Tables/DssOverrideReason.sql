﻿CREATE TABLE [icwsys].[DssOverrideReason](
	[DssOverrideReasonID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](200) NOT NULL,
	[Categories] [varchar](50) NOT NULL,
	[_Deleted] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
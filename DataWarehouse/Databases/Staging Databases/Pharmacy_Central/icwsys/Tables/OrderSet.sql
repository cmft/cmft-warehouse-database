﻿CREATE TABLE [icwsys].[OrderSet](
	[RequestID] [int] NOT NULL,
	[Modified] [bit] NOT NULL CONSTRAINT [DF_OrderSet_Modified]  DEFAULT (0),
	[ContentsAreOptions] [bit] NOT NULL CONSTRAINT [DF_OrderSet_ContentsAreOptions]  DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL,
	[PrescriptionCreationTypeID] [int] NOT NULL DEFAULT ((1)),
	[AuthorisingUser] [varchar](50) NULL
) ON [PRIMARY]
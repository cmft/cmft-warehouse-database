﻿CREATE TABLE [icwsys].[ValidityRule](
	[ValidityRuleID] [int] IDENTITY(1,1) NOT NULL,
	[NoteTypeID] [int] NULL,
	[ResponseTypeID] [int] NULL,
	[From] [float] NOT NULL,
	[FromUnitID] [int] NOT NULL,
	[To] [float] NOT NULL,
	[ToUnitID] [int] NOT NULL,
	[ValidityPeriod] [float] NOT NULL,
	[ValidityPeriodUnitID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[OrderCatalogue](
	[OrderCatalogueID] [int] IDENTITY(1,1) NOT NULL,
	[OrderCatalogueID_Parent] [int] NOT NULL,
	[OrderCatalogueRootID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](512) NULL,
	[Hidden] [bit] NOT NULL DEFAULT (0),
	[ImageURL] [varchar](128) NOT NULL DEFAULT ('classFolderClosed.gif'),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__OrderCata___RowG__39476EDE]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[OrderCatalogueRootID_Link] [int] NULL
) ON [PRIMARY]
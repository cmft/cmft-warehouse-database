﻿CREATE TABLE [icwsys].[EntityAliasException](
	[EntityAliasExceptionID] [int] IDENTITY(0,1) NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Failed] [int] NOT NULL,
	[LastFailure] [datetime] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
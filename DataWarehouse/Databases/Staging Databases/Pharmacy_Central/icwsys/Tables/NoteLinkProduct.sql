﻿CREATE TABLE [icwsys].[NoteLinkProduct](
	[NoteID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[WStockLabels](
	[WStockLabelsID] [int] IDENTITY(1,1) NOT NULL,
	[DrugCode] [nvarchar](7) NOT NULL,
	[WardCode] [nvarchar](5) NOT NULL,
	[RTFFileName] [nvarchar](13) NOT NULL,
	[SiteID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

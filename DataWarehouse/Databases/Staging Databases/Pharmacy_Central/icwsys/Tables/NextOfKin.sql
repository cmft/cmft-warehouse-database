﻿CREATE TABLE [icwsys].[NextOfKin](
	[EntityID] [int] NOT NULL,
	[WorkTelephoneNo] [varchar](35) NULL,
	[FamilyRelationshipID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

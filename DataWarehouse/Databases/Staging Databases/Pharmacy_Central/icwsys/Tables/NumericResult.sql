﻿CREATE TABLE [icwsys].[NumericResult](
	[ResponseID] [int] NOT NULL,
	[HighRange] [float] NULL,
	[LowRange] [float] NULL,
	[Preliminary] [bit] NULL,
	[RangeTestOutcomeID] [int] NULL,
	[UnitID] [int] NULL,
	[Value] [float] NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[HighRangeDisplayText] [varchar](256) NULL,
	[LowRangeDisplayText] [varchar](256) NULL,
	[ValueDisplayText] [varchar](256) NULL
) ON [PRIMARY]
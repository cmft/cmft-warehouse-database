﻿CREATE TABLE [icwsys].[CpiAlert](
	[NoteID] [int] NOT NULL,
	[CpiAlertTypeID] [int] NOT NULL,
	[CpiAlertCodeID] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
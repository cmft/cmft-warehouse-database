﻿CREATE TABLE [icwsys].[ControlType](
	[ControlTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[IsToolboxItem] [bit] NOT NULL DEFAULT ((0)),
	[ImageUrl] [varchar](max) NOT NULL DEFAULT ('NULL')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
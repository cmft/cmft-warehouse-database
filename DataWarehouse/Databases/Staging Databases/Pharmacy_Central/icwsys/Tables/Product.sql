﻿CREATE TABLE [icwsys].[Product](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductTypeID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[NonNumericalDosing] [bit] NOT NULL CONSTRAINT [DF_Product_NonNumericalDosing]  DEFAULT (0),
	[TemplatePermission] [bit] NOT NULL CONSTRAINT [DF_Product_TemplatePermission]  DEFAULT (1),
	[Version] [int] NOT NULL CONSTRAINT [DF_Product_Version]  DEFAULT (0),
	[Discontinued] [bit] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Product___RowGUI__190FA976]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[Description] [varchar](600) NOT NULL DEFAULT (''),
	[TPN] [bit] NOT NULL DEFAULT (0),
	[Description_Short] [varchar](128) NOT NULL DEFAULT ('')
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[MentalHealthT2](
	[NoteID] [int] NOT NULL,
	[ApprovedByClinician] [bit] NOT NULL,
	[ApprovedBySOAD] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
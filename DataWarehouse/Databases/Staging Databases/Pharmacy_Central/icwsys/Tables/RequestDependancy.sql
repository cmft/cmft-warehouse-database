﻿CREATE TABLE [icwsys].[RequestDependancy](
	[RequestDependancyID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[RequestID_Requisit] [int] NOT NULL,
	[NoteID_Requisit] [int] NOT NULL,
	[OffsetMinutes] [int] NOT NULL DEFAULT (0),
	[DependancyTypeID] [int] NULL,
	[IndexOrder] [int] NULL,
	[Mandatory] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[PatientDemographicData](
	[NoteID] [int] NOT NULL,
	[WorkTelephoneNo] [varchar](35) NULL,
	[Occupation] [varchar](255) NULL,
	[EmploymentStatusID] [int] NULL,
	[MaritalStatusID] [int] NULL,
	[ReligionID] [int] NULL,
	[LanguageSpokenID] [int] NULL,
	[EthnicGroupID] [int] NULL,
	[DateOfDeath] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
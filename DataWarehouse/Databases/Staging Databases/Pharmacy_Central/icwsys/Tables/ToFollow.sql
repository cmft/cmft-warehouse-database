﻿CREATE TABLE [icwsys].[ToFollow](
	[NoteID] [int] NOT NULL,
	[Notes] [varchar](250) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Comments] [varchar](30) NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EntityRoleAlias](
	[EntityRoleAliasID] [int] IDENTITY(1,1) NOT NULL,
	[EntityRoleID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[IsValid] [bit] NULL,
	[Default] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PNPrescriptionProductVolume](
	[PNPrescriptionProductVolumeID] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [int] NOT NULL,
	[PNProductID] [int] NOT NULL,
	[Volume_mL] [float] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
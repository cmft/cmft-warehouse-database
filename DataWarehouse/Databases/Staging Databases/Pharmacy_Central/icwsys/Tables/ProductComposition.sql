﻿CREATE TABLE [icwsys].[ProductComposition](
	[ProductID_Parent] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [float] NULL,
	[UnitID] [int] NULL,
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
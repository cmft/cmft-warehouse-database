﻿CREATE TABLE [icwsys].[ProductRouteImage](
	[ProductRouteID] [int] NOT NULL,
	[Image_Male] [varchar](128) NOT NULL,
	[Image_Female] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ProductRo___RowG__31E27D4F]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
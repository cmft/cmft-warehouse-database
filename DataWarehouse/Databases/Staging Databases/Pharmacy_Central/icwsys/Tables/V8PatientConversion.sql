﻿CREATE TABLE [icwsys].[V8PatientConversion](
	[EntityID] [int] NOT NULL,
	[FilePosn] [int] NOT NULL,
	[LocationID_Site] [int] NOT NULL,
	[Recno] [char](10) NOT NULL,
	[CaseNo] [char](10) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
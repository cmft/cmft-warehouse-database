﻿CREATE TABLE [icwsys].[InterfaceTableLinkModality](
	[InterfaceTableID] [int] NOT NULL,
	[ModalityID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
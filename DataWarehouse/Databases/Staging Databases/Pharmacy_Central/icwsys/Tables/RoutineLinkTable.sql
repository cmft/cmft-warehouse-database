﻿CREATE TABLE [icwsys].[RoutineLinkTable](
	[RoutineID] [int] NOT NULL,
	[TableID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
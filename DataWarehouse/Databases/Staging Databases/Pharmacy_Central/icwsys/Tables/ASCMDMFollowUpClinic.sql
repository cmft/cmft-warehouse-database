﻿CREATE TABLE [icwsys].[ASCMDMFollowUpClinic](
	[ASCMDMFollowUpClinicID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[ASCTumourTypeID] [int] NOT NULL CONSTRAINT [DF_ASCMDMFollowUpClinic_ASCTumourTypeID]  DEFAULT ((0))
) ON [PRIMARY]
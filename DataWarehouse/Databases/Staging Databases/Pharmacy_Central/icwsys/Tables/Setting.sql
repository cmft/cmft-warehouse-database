﻿CREATE TABLE [icwsys].[Setting](
	[System] [varchar](50) NOT NULL,
	[Section] [varchar](50) NOT NULL,
	[Key] [varchar](50) NOT NULL,
	[Value] [varchar](4096) NULL,
	[Description] [varchar](1024) NULL,
	[RoleID] [int] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL,
	[SettingID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
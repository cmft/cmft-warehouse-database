﻿CREATE TABLE [icwsys].[PNRule](
	[PNRuleID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID_Site] [int] NOT NULL,
	[RuleNumber] [int] NOT NULL,
	[RuleType] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[RuleSQL] [varchar](max) NOT NULL,
	[Critical] [bit] NOT NULL,
	[PerKilo] [bit] NOT NULL,
	[InUse] [bit] NOT NULL,
	[PNCode] [varchar](8) NOT NULL DEFAULT (''),
	[Ingredient] [varchar](5) NOT NULL DEFAULT (''),
	[Explanation] [varchar](1024) NOT NULL,
	[LastModDate] [datetime] NULL,
	[LastModUser] [varchar](3) NOT NULL,
	[LastModTerm] [varchar](15) NOT NULL,
	[Info] [varchar](max) NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__PNRule___RowGUID__0599E6BD]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
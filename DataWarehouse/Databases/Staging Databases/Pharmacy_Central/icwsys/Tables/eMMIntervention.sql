﻿CREATE TABLE [icwsys].[eMMIntervention](
	[NoteID] [int] NOT NULL,
	[eMMInterventionReasonID] [int] NOT NULL DEFAULT ((0)),
	[Comments] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[PendingItemLinkAttachedNote](
	[PendingItemID] [int] NOT NULL,
	[NoteID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
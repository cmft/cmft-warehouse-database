﻿CREATE TABLE [icwsys].[ColumnType](
	[ColumnTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
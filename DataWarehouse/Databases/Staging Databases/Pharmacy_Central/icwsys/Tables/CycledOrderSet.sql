﻿CREATE TABLE [icwsys].[CycledOrderSet](
	[RequestID] [int] NOT NULL,
	[CycleNumber] [int] NOT NULL,
	[RequestID_CopiedFrom] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
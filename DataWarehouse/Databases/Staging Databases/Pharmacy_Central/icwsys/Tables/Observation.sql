﻿CREATE TABLE [icwsys].[Observation](
	[NoteID] [int] NOT NULL,
	[UnitID] [int] NOT NULL,
	[Value] [float] NOT NULL,
	[ObservationDate] [datetime] NOT NULL,
	[ObservationTime] [datetime] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
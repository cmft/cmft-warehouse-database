﻿CREATE TABLE [icwsys].[TherapeuticGroup](
	[TherapeuticGroupID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Therapeut___RowG__7084F734]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[IncludeInTherapeuticDoseRangeCheck] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

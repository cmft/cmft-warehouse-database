﻿CREATE TABLE [icwsys].[RecordOfEntityImageMerge](
	[RecordOfEntityImageMergeID] [int] IDENTITY(1,1) NOT NULL,
	[NoteID] [int] NOT NULL,
	[EntityID_Before] [int] NOT NULL,
	[EntityID_After] [int] NOT NULL,
	[ImageID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
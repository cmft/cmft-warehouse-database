﻿CREATE TABLE [icwsys].[ConsMasterfile](
	[Consultant] [varchar](50) NULL,
	[ConsultantCode2] [varchar](50) NULL,
	[ContractedSpecialty] [varchar](50) NULL,
	[PrimarySpecialty] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[GMCCode] [varchar](50) NULL,
	[ExternalCons] [varchar](50) NULL,
	[ForenamePfx] [varchar](50) NULL,
	[Inits] [varchar](50) NULL,
	[KornerCode] [varchar](50) NULL,
	[Midwife] [varchar](50) NULL,
	[ProviderCode] [varchar](50) NULL,
	[Expression_14] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
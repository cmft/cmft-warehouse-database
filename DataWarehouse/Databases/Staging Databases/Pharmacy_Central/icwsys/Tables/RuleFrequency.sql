﻿CREATE TABLE [icwsys].[RuleFrequency](
	[RuleFrequencyID] [int] IDENTITY(1,1) NOT NULL,
	[RuleID] [int] NOT NULL,
	[Frequency] [int] NOT NULL,
	[Duration] [int] NOT NULL,
	[UnitID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL,
	[_Deleted] [bit] NOT NULL
) ON [PRIMARY]
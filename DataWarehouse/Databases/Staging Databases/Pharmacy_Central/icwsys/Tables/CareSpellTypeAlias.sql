﻿CREATE TABLE [icwsys].[CareSpellTypeAlias](
	[CareSpellTypeAliasID] [int] IDENTITY(1,1) NOT NULL,
	[CareSpellTypeID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
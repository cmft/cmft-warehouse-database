﻿CREATE TABLE [icwsys].[PBSBeneficiaryType](
	[PBSBeneficiaryTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Code] [varchar](1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Detail] [varchar](255) NULL,
	[CTSPaymentCategoryID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
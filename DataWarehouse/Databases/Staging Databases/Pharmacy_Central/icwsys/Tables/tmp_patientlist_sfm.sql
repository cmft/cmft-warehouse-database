﻿CREATE TABLE [icwsys].[tmp_patientlist_sfm](
	[EntityID] [int] NOT NULL,
	[Title] [varchar](128) NULL,
	[forename] [varchar](128) NULL,
	[surname] [varchar](128) NOT NULL,
	[DOB] [datetime] NULL,
	[alias] [varchar](255) NOT NULL,
	[Description] [varchar](128) NOT NULL
) ON [PRIMARY]
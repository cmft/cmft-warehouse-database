﻿CREATE TABLE [icwsys].[PermissionDesktop](
	[PolicyDesktopID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[DesktopID] [int] NOT NULL,
	[Allow] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
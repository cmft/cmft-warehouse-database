﻿CREATE TABLE [icwsys].[Pathology](
	[RequestID] [int] NOT NULL,
	[PreviousXrays] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WorklistQuery](
	[WorklistQueryID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[OrdinalPosition] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[WorklistID] [int] NULL,
	[SQL] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
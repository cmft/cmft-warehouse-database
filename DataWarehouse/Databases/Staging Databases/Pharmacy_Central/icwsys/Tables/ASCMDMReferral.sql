﻿CREATE TABLE [icwsys].[ASCMDMReferral](
	[RequestID] [int] NOT NULL,
	[ReferringClinicianName] [varchar](100) NOT NULL,
	[ReferringClinicianNumber] [varchar](25) NOT NULL,
	[ClinicalNotes] [varchar](1000) NOT NULL,
	[Question] [varchar](100) NOT NULL,
	[ASCTumourTypeID] [int] NOT NULL,
	[Date_Pathology_1] [datetime] NULL,
	[PathologySite_1] [varchar](50) NULL,
	[PathologySpecimen_1] [varchar](20) NULL,
	[Date_Pathology_2] [datetime] NULL,
	[PathologySite_2] [varchar](50) NULL,
	[PathologySpecimen_2] [varchar](20) NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[ASCQuestionTypeID] [int] NULL
) ON [PRIMARY]
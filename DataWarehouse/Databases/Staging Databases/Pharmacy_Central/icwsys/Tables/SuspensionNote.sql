﻿CREATE TABLE [icwsys].[SuspensionNote](
	[NoteID] [int] NOT NULL,
	[SuspendOn] [datetime] NOT NULL,
	[UnsuspendOn] [datetime] NULL,
	[UnsuspendAfterDoses] [int] NULL,
	[UnsuspendedOn] [datetime] NULL,
	[UnsuspendedBy] [int] NULL,
	[SuspensionReason] [varchar](256) NULL,
	[SuspensionReasonID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
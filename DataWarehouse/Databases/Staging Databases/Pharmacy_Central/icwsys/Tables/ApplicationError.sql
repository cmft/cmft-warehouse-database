﻿CREATE TABLE [icwsys].[ApplicationError](
	[ApplicationLogID] [int] NOT NULL,
	[ErrorGroupID] [int] NOT NULL,
	[StackTrace] [text] NOT NULL,
	[ExtraInfo] [text] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[OrderCatalogueLinkChemicalOLAP](
	[OrderCatalogueID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__OrderCata___RowG__65622155]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
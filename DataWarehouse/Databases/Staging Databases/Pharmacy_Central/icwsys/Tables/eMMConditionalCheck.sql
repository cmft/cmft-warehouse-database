﻿CREATE TABLE [icwsys].[eMMConditionalCheck](
	[NoteID] [int] NOT NULL,
	[ascEMMConditionalCheckReasonID] [int] NOT NULL,
	[Comments] [varchar](200) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ChangedCreationTypeNote](
	[NoteID] [int] NOT NULL,
	[PrescriptionCreationTypeID_From] [int] NOT NULL,
	[PrescriptionCreationTypeID_To] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[RepeatDispensingBatchAuditLog](
	[RepeatDispensingBatchAuditLogID] [int] IDENTITY(1,1) NOT NULL,
	[RepeatDispensingBatchID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[DateChanged] [datetime] NOT NULL,
	[EntityID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
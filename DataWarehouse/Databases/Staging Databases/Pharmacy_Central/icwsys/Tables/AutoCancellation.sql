﻿CREATE TABLE [icwsys].[AutoCancellation](
	[AutoCancellationID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[CancellationTypeID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
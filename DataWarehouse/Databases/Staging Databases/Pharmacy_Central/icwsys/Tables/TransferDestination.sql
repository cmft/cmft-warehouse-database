﻿CREATE TABLE [icwsys].[TransferDestination](
	[TransferDestinationID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__TransferD___RowG__11341FED]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0)),
	[GroupCode] [varchar](10) NULL DEFAULT ('')
) ON [PRIMARY]
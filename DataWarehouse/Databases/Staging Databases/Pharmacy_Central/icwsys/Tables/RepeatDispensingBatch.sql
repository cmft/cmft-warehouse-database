﻿CREATE TABLE [icwsys].[RepeatDispensingBatch](
	[RepeatDispensingBatchID] [int] IDENTITY(1,1) NOT NULL,
	[BatchDescription] [varchar](8000) NOT NULL,
	[StatusID] [int] NOT NULL,
	[Factor] [int] NOT NULL,
	[RepeatDispensingBatchTemplateID] [int] NULL,
	[StartDate] [datetime] NULL,
	[StartSlot] [tinyint] NULL,
	[TotalSlots] [int] NULL,
	[Breakfast] [bit] NULL,
	[Lunch] [bit] NULL,
	[Tea] [bit] NULL,
	[Night] [bit] NULL,
	[BagLabelsPerPatient] [int] NULL,
	[SortByDate] [bit] NULL,
	[LocationID] [int] NULL,
	[IncludeManual] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
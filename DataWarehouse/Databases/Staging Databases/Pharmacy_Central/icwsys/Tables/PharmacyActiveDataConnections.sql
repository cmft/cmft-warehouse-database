﻿CREATE TABLE [icwsys].[PharmacyActiveDataConnections](
	[PharmacyActiveDataConnectionsID] [int] IDENTITY(1,1) NOT NULL,
	[SessionID] [int] NOT NULL,
	[URLToken] [varchar](8000) NOT NULL,
	[Key] [varchar](8000) NOT NULL,
	[Created] [datetime] NOT NULL,
	[CreatedByEntityID] [int] NOT NULL,
	[LastUsed] [datetime] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
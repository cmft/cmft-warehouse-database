﻿CREATE TABLE [icwsys].[DischargeMethodLinkDischargeDestination](
	[DischargeMethodID] [int] NOT NULL,
	[DischargeDestinationID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
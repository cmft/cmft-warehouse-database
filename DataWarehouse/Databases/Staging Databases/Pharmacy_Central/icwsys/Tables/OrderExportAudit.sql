﻿CREATE TABLE [icwsys].[OrderExportAudit](
	[OrderExportAuditID] [int] IDENTITY(1,1) NOT NULL,
	[OrderExportID] [int] NOT NULL,
	[PatientID] [int] NOT NULL,
	[RawXML] [xml] NOT NULL,
	[Exported] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
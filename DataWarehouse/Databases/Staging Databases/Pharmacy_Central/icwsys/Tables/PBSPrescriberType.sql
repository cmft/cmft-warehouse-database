﻿CREATE TABLE [icwsys].[PBSPrescriberType](
	[PBSPrescriberTypeID] [int] IDENTITY(0,1) NOT NULL,
	[PBSPrescriberTypeCode] [varchar](3) NULL,
	[Description] [varchar](50) NULL,
	[Detail] [varchar](255) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[ChemotherapyMDM](
	[MeetingID] [int] NOT NULL,
	[ASCTumourTypeID] [int] NOT NULL,
	[PatientCapacity] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
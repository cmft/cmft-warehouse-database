﻿CREATE TABLE [icwsys].[FreeFormatLabel](
	[RequestID] [int] NOT NULL,
	[LabelText] [varchar](250) NULL,
	[HeaderText] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
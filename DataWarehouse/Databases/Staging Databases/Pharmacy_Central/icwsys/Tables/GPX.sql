﻿CREATE TABLE [icwsys].[GPX](
	[EntityID] [int] NOT NULL,
	[GpfhCode] [varchar](3) NULL,
	[Address1] [varchar](255) NULL,
	[Address2] [varchar](255) NULL,
	[Address3] [varchar](255) NULL,
	[Address4] [varchar](255) NULL,
	[Postcode] [varchar](10) NULL,
	[GPID] [varchar](20) NULL,
	[PracticeCode] [varchar](10) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
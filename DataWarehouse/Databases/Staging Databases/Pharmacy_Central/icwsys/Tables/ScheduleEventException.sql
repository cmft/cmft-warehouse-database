﻿CREATE TABLE [icwsys].[ScheduleEventException](
	[ScheduleEventExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleEventID] [int] NOT NULL,
	[ColumnID] [int] NULL,
	[Value] [varchar](1024) NULL,
	[Offset] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
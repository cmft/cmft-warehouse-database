﻿CREATE TABLE [icwsys].[ChemicalGroupInteraction](
	[ChemicalGroupInteractionID] [int] IDENTITY(1,1) NOT NULL,
	[ChemicalGroupID_A] [int] NOT NULL,
	[ChemicalGroupID_B] [int] NOT NULL,
	[Description] [varchar](1024) NOT NULL,
	[SeverityID] [int] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ChemicalG___RowG__000EF182]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EpisodeEventLog](
	[NoteID] [int] NOT NULL,
	[EpisodeEventLogTypeID] [int] NOT NULL,
	[Message] [varchar](256) NOT NULL,
	[AcknowledgedDate] [datetime] NULL,
	[AcknowledgedByEntityId] [int] NULL,
	[AcknowledgedLocationID] [int] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
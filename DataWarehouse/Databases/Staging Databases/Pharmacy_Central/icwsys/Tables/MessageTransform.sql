﻿CREATE TABLE [icwsys].[MessageTransform](
	[MessageTransformID] [int] IDENTITY(1,1) NOT NULL,
	[SourceMsgGuid] [uniqueidentifier] NOT NULL,
	[TransformMsgGuid] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
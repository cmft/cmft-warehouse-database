﻿CREATE TABLE [icwsys].[PBSProductSaltName](
	[PBSProductSaltNameID] [int] IDENTITY(1,1) NOT NULL,
	[SaltName] [varchar](128) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
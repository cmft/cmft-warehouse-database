﻿CREATE TABLE [icwsys].[Warnings](
	[WARNINGCode] [varchar](6) NULL,
	[WARNINGText] [varchar](142) NULL,
	[Notes] [varchar](23) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
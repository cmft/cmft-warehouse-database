﻿CREATE TABLE [icwsys].[OrderExport](
	[OrderExportID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ExportType] [char](8) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
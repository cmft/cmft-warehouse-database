﻿CREATE TABLE [icwsys].[PBSNote](
	[PBSNoteID] [int] NOT NULL,
	[NoteText] [varchar](max) NOT NULL,
	[Out_Of_Use] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[TemplateColumnAttribute](
	[TemplateColumnAttributeID] [int] IDENTITY(1,1) NOT NULL,
	[OrderTemplateID] [int] NOT NULL,
	[ColumnID] [int] NOT NULL,
	[ColumnMaskID] [int] NULL,
	[ValidationSoft] [varchar](max) NULL,
	[ValidationHard] [varchar](max) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
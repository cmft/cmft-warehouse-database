﻿CREATE TABLE [icwsys].[TherapeuticMoiety](
	[ProductID] [int] NOT NULL,
	[ProductStateID] [int] NOT NULL,
	[ProductFormID] [int] NOT NULL,
	[Diluent] [bit] NULL CONSTRAINT [DF_TherapeuticMoiety_Diluent]  DEFAULT (0),
	[MeteredDosing] [bit] NOT NULL DEFAULT (0),
	[CombinedDosing] [bit] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Therapeut___RowG__4F240369]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[AlwaysDiluted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

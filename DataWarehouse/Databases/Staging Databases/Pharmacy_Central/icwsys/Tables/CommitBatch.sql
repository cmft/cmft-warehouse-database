﻿CREATE TABLE [icwsys].[CommitBatch](
	[CommitBatchID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL DEFAULT (getdate()),
	[SiteGeneratedCode] [varchar](32) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
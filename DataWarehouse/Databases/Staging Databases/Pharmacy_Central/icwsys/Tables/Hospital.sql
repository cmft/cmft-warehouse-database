﻿CREATE TABLE [icwsys].[Hospital](
	[LocationID] [int] NOT NULL,
	[code] [varchar](3) NOT NULL,
	[Address1] [varchar](255) NULL,
	[Address2] [varchar](255) NULL,
	[Address3] [varchar](255) NULL,
	[Address4] [varchar](255) NULL,
	[Postcode] [varchar](10) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[EntityLinkSpecialty](
	[EntityID] [int] NOT NULL,
	[SpecialtyID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

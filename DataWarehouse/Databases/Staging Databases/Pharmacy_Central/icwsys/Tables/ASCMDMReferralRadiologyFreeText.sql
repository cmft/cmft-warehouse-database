﻿CREATE TABLE [icwsys].[ASCMDMReferralRadiologyFreeText](
	[RequestID] [int] NOT NULL,
	[Comments_Imaging] [varchar](250) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
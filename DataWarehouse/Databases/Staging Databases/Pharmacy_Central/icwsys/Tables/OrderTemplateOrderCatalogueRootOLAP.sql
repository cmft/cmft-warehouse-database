﻿CREATE TABLE [icwsys].[OrderTemplateOrderCatalogueRootOLAP](
	[OrderTemplateID] [int] NOT NULL,
	[OrderCatalogueRootID] [int] NOT NULL,
	[OrderCatalogueRoot] [varchar](255) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
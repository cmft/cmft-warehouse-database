﻿CREATE TABLE [icwsys].[WPharmacyLog](
	[WPharmacyLogID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL DEFAULT (getdate()),
	[SiteID] [int] NULL,
	[EntityID_User] [int] NULL,
	[Terminal] [varchar](15) NULL,
	[Description] [varchar](8000) NULL,
	[Detail] [varchar](8000) NULL,
	[State] [int] NULL,
	[Thread] [int] NULL,
	[SessionID] [int] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL,
	[NSVCode] [varchar](7) NOT NULL DEFAULT ('')
) ON [PRIMARY]
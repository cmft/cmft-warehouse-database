﻿CREATE TABLE [icwsys].[DiscontinuationReason](
	[DiscontinuationReasonID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[ClinicalReason] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[Visible] [bit] NOT NULL CONSTRAINT [DiscontinuationReason_Visible_Default]  DEFAULT ((1))
) ON [PRIMARY]
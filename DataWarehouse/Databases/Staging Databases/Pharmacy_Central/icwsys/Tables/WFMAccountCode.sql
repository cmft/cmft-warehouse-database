﻿CREATE TABLE [icwsys].[WFMAccountCode](
	[WFMAccountCodeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Code] [smallint] NOT NULL,
	[_Deleted] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
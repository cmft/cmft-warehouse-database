﻿CREATE TABLE [icwsys].[OrderExportPicker](
	[OrderExportPickerID] [int] IDENTITY(1,1) NOT NULL,
	[OrderExportID] [int] NOT NULL,
	[PickedAll] [bit] NOT NULL,
	[PickedLatest] [bit] NOT NULL,
	[NoteTypeID] [int] NULL,
	[RequestTypeID] [int] NULL,
	[ResponseTypeID] [int] NULL,
	[OrderCatalogueFolder] [varchar](100) NULL,
	[ExternalTypeText] [varchar](100) NULL,
	[ItemPicked] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
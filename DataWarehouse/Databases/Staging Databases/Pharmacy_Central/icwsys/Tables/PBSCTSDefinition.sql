﻿CREATE TABLE [icwsys].[PBSCTSDefinition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataSection] [int] NULL DEFAULT (0),
	[ExportDataTable] [varchar](250) NULL,
	[OutputFieldOrder] [int] NULL DEFAULT (0),
	[OutputFieldName] [varchar](250) NULL,
	[OutputFormat] [varchar](250) NULL,
	[FunctionName] [varchar](50) NULL,
	[FunctionParam1] [varchar](50) NULL,
	[FunctionParam2] [varchar](50) NULL,
	[FunctionParam3] [varchar](50) NULL,
	[FunctionParam4] [varchar](50) NULL,
	[FunctionParam5] [varchar](50) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
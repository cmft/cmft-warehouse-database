﻿CREATE TABLE [icwsys].[TextDocument_NonMandatory](
	[NoteID] [int] NOT NULL,
	[Detail] [text] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
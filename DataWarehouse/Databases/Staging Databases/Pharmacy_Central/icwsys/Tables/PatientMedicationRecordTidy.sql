﻿CREATE TABLE [icwsys].[PatientMedicationRecordTidy](
	[RequestID] [int] NOT NULL,
	[Placeholder] [bit] NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
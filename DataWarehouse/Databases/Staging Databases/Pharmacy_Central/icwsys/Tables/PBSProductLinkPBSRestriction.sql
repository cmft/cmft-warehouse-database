﻿CREATE TABLE [icwsys].[PBSProductLinkPBSRestriction](
	[PBSMasterProductID] [int] NOT NULL,
	[PBSRestrictionID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
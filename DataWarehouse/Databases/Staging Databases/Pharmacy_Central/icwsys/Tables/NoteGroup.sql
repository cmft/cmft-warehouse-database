﻿CREATE TABLE [icwsys].[NoteGroup](
	[NoteGroupID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [varchar](50) NOT NULL,
	[NoneText] [varchar](255) NOT NULL,
	[NoteTypeID_Default] [int] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
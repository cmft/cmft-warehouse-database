﻿CREATE TABLE [icwsys].[OverseasVisitorStatus](
	[OverseasVisitorStatusID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Detail] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__OverseasV___RowG__1C89CFBB]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
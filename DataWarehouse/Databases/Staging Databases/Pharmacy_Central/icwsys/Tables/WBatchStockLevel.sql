﻿CREATE TABLE [icwsys].[WBatchStockLevel](
	[WBatchStockLevelID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Description] [varchar](56) NULL,
	[NSVcode] [varchar](7) NULL,
	[Batchnumber] [varchar](25) NULL,
	[Expiry] [datetime] NULL,
	[qty] [float] NULL,
	[SessionLock] [int] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
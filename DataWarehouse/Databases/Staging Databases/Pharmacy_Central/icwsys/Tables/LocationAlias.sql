﻿CREATE TABLE [icwsys].[LocationAlias](
	[LocationAliasID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL CONSTRAINT [DF_LocationAlias_Default]  DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

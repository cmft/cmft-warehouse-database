﻿CREATE TABLE [icwsys].[wconfiguration_sfm_20140902](
	[WConfigurationID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Category] [varchar](255) NOT NULL,
	[Section] [varchar](255) NOT NULL,
	[Key] [varchar](255) NOT NULL,
	[Value] [varchar](8000) NULL,
	[_RowVersion] [int] NOT NULL,
	[_RowGUID] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL,
	[DSS] [bit] NOT NULL
) ON [PRIMARY]
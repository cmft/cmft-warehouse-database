﻿CREATE TABLE [icwsys].[WProductStockLinkBNF](
	[WProductStockLinkBNFID] [int] IDENTITY(1,1) NOT NULL,
	[ProductStockID] [int] NOT NULL,
	[BNF] [varchar](13) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[WLookupContext](
	[WLookupContextID] [int] IDENTITY(1,1) NOT NULL,
	[Context] [varchar](255) NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__WLookupCo___RowG__4D90C0B2]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

﻿CREATE TABLE [icwsys].[CancellationNoteLinkTreatmentPlan](
	[NoteID] [int] NOT NULL,
	[TreatmentPlanID_Cancelled] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
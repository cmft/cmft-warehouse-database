﻿CREATE TABLE [icwsys].[ControlImage](
	[ControlImageID] [int] IDENTITY(1,1) NOT NULL,
	[ControlID] [int] NULL,
	[Alt] [varchar](50) NULL,
	[Src] [varchar](150) NOT NULL,
	[Mode] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
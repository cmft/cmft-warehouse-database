﻿CREATE TABLE [icwsys].[OrderLoadingDetail](
	[OrderLoadingDetailID] [int] IDENTITY(1,1) NOT NULL,
	[WOrderNum] [int] NOT NULL,
	[OrderLoadingID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [icwsys].[RepeatDispensingSupplyPatterns](
	[SupplyPatternID] [int] NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Days] [int] NOT NULL,
	[IsDefault] [bit] NULL,
	[SplitDays] [int] NOT NULL DEFAULT ((0)),
	[Active] [bit] NOT NULL DEFAULT ((1)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
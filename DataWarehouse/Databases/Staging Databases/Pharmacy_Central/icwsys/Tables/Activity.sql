﻿CREATE TABLE [icwsys].[Activity](
	[ActivityID] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[SessionID] [int] NOT NULL,
	[ActionType] [varchar](50) NOT NULL,
	[User] [varchar](255) NOT NULL,
	[Table] [varchar](128) NOT NULL,
	[Terminal] [varchar](50) NOT NULL,
	[ActionDate] [datetime] NOT NULL,
	[Record] [varchar](6000) NOT NULL,
	[Notes] [varchar](1024) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
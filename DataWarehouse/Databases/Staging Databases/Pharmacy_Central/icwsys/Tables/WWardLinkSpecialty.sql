﻿CREATE TABLE [icwsys].[WWardLinkSpecialty](
	[WWardLinkSpecialtyID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [int] NOT NULL,
	[Ward] [varchar](5) NOT NULL,
	[Specialty] [varchar](5) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

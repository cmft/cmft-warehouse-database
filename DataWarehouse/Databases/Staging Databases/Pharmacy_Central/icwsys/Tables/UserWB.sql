﻿CREATE TABLE [icwsys].[UserWB](
	[EntityID] [int] NOT NULL,
	[Inits] [varchar](3) NOT NULL,
	[SiteNumber] [varchar](3) NOT NULL,
	[Volume] [varchar](1) NOT NULL,
	[AccessLevel] [varchar](10) NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]

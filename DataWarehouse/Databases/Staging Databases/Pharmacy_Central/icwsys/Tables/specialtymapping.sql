﻿CREATE TABLE [icwsys].[specialtymapping](
	[ASC-Code] [varchar](4) NULL,
	[PAS-Specialty] [varchar](5) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
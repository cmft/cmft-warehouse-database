﻿CREATE TABLE [icwsys].[WaitingTimeResponse](
	[ResponseID] [int] NOT NULL,
	[DateComplete] [datetime] NULL,
	[TimeComplete] [datetime] NULL,
	[DateChecked] [datetime] NULL,
	[TimeChecked] [datetime] NULL,
	[Comments] [varchar](250) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
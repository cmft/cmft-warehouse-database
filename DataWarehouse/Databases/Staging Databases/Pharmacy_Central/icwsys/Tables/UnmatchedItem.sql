﻿CREATE TABLE [icwsys].[UnmatchedItem](
	[RequestID] [int] NOT NULL,
	[NativeData] [varchar](max) NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [icwsys].[OverseasVisitorStatusAlias](
	[OverseasVisitorStatusAliasID] [int] IDENTITY(1,1) NOT NULL,
	[OverseasVisitorStatusID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL DEFAULT ((0)),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
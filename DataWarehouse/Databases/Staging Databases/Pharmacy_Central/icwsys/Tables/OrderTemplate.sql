﻿CREATE TABLE [icwsys].[OrderTemplate](
	[OrderTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[TableID] [int] NOT NULL,
	[RequestTypeID] [int] NOT NULL,
	[NoteTypeID] [int] NOT NULL,
	[Description] [varchar](256) NOT NULL,
	[Detail] [text] NULL,
	[DefaultXML] [varchar](6000) NOT NULL,
	[DSSReferenceID] [int] NOT NULL DEFAULT (0),
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__OrderTemp___RowG__01C22FCA]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0),
	[CustomImage] [varchar](255) NULL,
	[FurtherDescription] [varchar](256) NULL,
	[NonDSSDetail] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
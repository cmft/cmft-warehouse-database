﻿CREATE TABLE [icwsys].[SpecialtyAlias](
	[SpecialtyAliasID] [int] IDENTITY(1,1) NOT NULL,
	[SpecialtyID] [int] NOT NULL,
	[AliasGroupID] [int] NOT NULL,
	[Alias] [varchar](255) NOT NULL,
	[Default] [bit] NOT NULL DEFAULT (0),
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
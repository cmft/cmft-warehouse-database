﻿CREATE TABLE [icwsys].[ChemicalLinkChemicalGroup](
	[ProductID] [int] NOT NULL,
	[ChemicalGroupID] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT (1),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__ChemicalL___RowG__0B80A42E]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT (0),
	[_Deleted] [bit] NOT NULL DEFAULT (0)
) ON [PRIMARY]
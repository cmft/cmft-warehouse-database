﻿CREATE TABLE [icwsys].[ascLeaveHasLeft](
	[RequestID] [int] NOT NULL,
	[ExpectedReturnDate] [datetime] NULL,
	[LeaveDate] [datetime] NOT NULL,
	[LeaveTypeID] [int] NOT NULL,
	[LeaveDestinationID] [int] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
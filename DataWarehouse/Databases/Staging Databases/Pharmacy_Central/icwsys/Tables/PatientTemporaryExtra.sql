﻿CREATE TABLE [icwsys].[PatientTemporaryExtra](
	[EntityID] [int] NOT NULL,
	[IndigenousStatusID] [int] NULL,
	[TemporaryRegistration] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
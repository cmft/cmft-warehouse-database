﻿CREATE TABLE [icwsys].[PNIngredient](
	[PNIngredientID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[ShortDescription] [varchar](10) NOT NULL,
	[DBName] [varchar](100) NOT NULL,
	[Unit] [varchar](50) NOT NULL,
	[ForPrescribing] [bit] NOT NULL,
	[ForViewAdjust] [bit] NOT NULL,
	[SortIndex] [int] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__PNIngredi___RowG__79340FD8]  DEFAULT (newsequentialid()),
	[_TableVersion] [timestamp] NOT NULL,
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
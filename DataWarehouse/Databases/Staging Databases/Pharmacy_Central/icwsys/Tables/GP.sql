﻿CREATE TABLE [icwsys].[GP](
	[EntityID] [int] NOT NULL,
	[GPClassificationID] [int] NOT NULL,
	[RegNumber] [varchar](20) NOT NULL,
	[Contract] [char](1) NOT NULL,
	[Obstetric] [bit] NOT NULL,
	[JobShare] [bit] NOT NULL,
	[Trainer] [bit] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
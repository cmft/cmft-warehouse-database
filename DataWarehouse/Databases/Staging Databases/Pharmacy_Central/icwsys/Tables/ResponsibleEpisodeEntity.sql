﻿CREATE TABLE [icwsys].[ResponsibleEpisodeEntity](
	[ResponsibleEpisodeEntityID] [int] IDENTITY(1,1) NOT NULL,
	[EpisodeID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[EntityRoleID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL CONSTRAINT [DF_ResponsibleEpisodeEntity_StartDate]  DEFAULT (getdate()),
	[EndDate] [datetime] NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_ResponsibleEpisodeEntity_Active]  DEFAULT (1),
	[_TableVersion] [timestamp] NOT NULL,
	[_RowVersion] [int] NOT NULL DEFAULT ((1)),
	[_RowGUID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Responsib___RowG__32AA2251]  DEFAULT (newsequentialid()),
	[_QA] [bit] NOT NULL DEFAULT ((0)),
	[_Deleted] [bit] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

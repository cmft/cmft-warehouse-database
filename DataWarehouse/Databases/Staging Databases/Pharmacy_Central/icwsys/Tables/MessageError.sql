﻿CREATE TABLE [icwsys].[MessageError](
	[ApplicationLogID] [int] NOT NULL,
	[MessageGuid] [uniqueidentifier] NOT NULL,
	[_TableVersion] [timestamp] NOT NULL
) ON [PRIMARY]
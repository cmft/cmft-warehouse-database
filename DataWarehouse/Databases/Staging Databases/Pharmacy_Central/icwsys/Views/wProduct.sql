﻿CREATE  view [icwsys].[wProduct] as
select
		a.ProductID
	,	a.barcode
	,	a.siscode
	,	a.code
	,	a.labeldescription
	,	a.tradename
	,	a.printformv
	,	a.storesdescription
	,	a.convfact
	,	a.mlsperpack
	,	a.cyto
	,	a.warcode
	,	a.warcode2
	,	a.inscode
	,	a.DosesperIssueUnit
	,	a.DosingUnits
	,	a.DPSForm
	,	a.Drugid
	,	a.LabelInIssueUnits
	,	a.Canusespoon
	,	a.DSSMasterSiteID
	,	a.SiteProductDataID
	,	a.labeldescription  [description]
	,  	a.bnf 
	,	a.MinDailyDose
	,	a.MaxDailyDose
	,	a.MinDoseFrequency
	,	a.MaxDoseFrequency
	,	a.warcode_Locked
	,	a.warcode2_Locked
	,	a.inscode_Locked
	,	a.StoresDescription_Locked
	,	a.LabelDescription_Locked
	,   a.CanUseSpoon_Locked
	,	a.PhysicalDescription
	,	a.PASANPCCode
	,	b.ProductStockID
	,	b.LocationID_Site
	,	b.inuse
	,	b.formulary
	,	b.labelformat
	,	b.extralabel
	,	b.expiryminutes
	,	b.minissue
	,	b.maxissue
	,	b.lastissued
	,	b.issueWholePack
	,	b.stocklvl
	,	b.sisstock
	,	b.livestockctrl
	,	b.ordercycle
	,	b.cyclelength
	,	b.outstanding
	,	b.loccode
	,	b.loccode2
	,	b.anuse
	,	b.usethisperiod
	,	b.recalcatperiodend
	,	b.datelastperiodend
	,	b.usagedamping
	,	b.safetyfactor
	,	b.supcode
	,	b.altsupcode
	,	b.lastordered
	,	b.stocktakestatus
	,	b.laststocktakedate
	,	b.laststocktaketime
	,	b.batchtracking
	,	b.cost
	,	b.lossesgains
	,	b.ledcode
	,	b.pflag
	,	b.message
	,	b.UserMsg
	,	b.PILnumber
	,	b.PIL2
	,	b.CreatedUser
	,	b.createdterminal
	,	b.createddate
	,	b.createdtime
	,	b.modifieduser
	,	b.modifiedterminal
	,	b.modifieddate
	,	b.modifiedtime
	,	b.[local]
	,	b.CIVAS
	,  	b.mgPerml	
	,  	b.maxInfusionRate	
	,  	b.InfusionTime  
	,  	b.Minmgperml  
	,  	b.Maxmgperml 
	,  	b.IVContainer  
	,  	b.DisplacementVolume  
	,  	b.storespack  
--	,  	b.bnf 
	,	b.therapcode 
	,  	b.reorderlvl
	,  	b.reorderqty
--	,  	b.dosesperissueunit
	,	b.ReconVol
	,	b.ReconAbbr
	,	b.Diluent1
	,	b.Diluent2
	,	b.DDDValue
	,	b.DDDUnits
	,	b.UserField1
	,	b.UserField2
	,	b.UserField3
	,	b.HIProduct
	,	b.PIPCode
	,	b.MasterPIP
	,	b.EDILinkCode
	,  	c.SuppRefNo
	,  	c.WSupplierProfileID
	,  	c.vatrate
	,  	c.sislistprice
	,  	c.contprice
	,  	c.lastreconcileprice
	,  	c.contno
	,  	c.reorderpcksize
	,  	c.leadtime
--	,  	c.reorderlvl
--	,  	c.reorderqty
--	,  	c.PrimarySup
	,	c.SupplierTradeName
	,   b.PNExclude
	,	b.EyeLabel
	,	b.PSOLabel
	,   b.ExpiryWarnDays    -- XN 78339 09Jan13
	,   a.DSS               -- XN 25Jan13
	,   a.DMandDReference   -- XN 11Mar14
from
	SiteProductData a
	--join DSS_AMPPMAPPER map on (a.DrugID = map.DrugID)
	join ProductStock b on (a.DrugID = b.DrugID)
	join DSSMasterSiteLinkSite d on (a.DSSMasterSiteID = d.DSSMasterSiteID and d.SiteID = b.LocationID_Site )
	join WSupplierProfile c on (a.siscode = c.nsvcode and b.supcode = c.supcode and b.locationID_Site = c.locationID_Site )




﻿CREATE TABLE [dbo].[REP_EMAIL_TEMPLATES](
	[ETE_BODY] [nvarchar](4000) NULL,
	[ETE_LOCKSEQ] [decimal](9, 0) NULL,
	[ETE_NAME] [nvarchar](80) NULL,
	[ETE_OID] [decimal](18, 0) NOT NULL,
	[ETE_SUBJECT] [nvarchar](255) NULL,
	[ETE_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_DATA_EXCHANGE_BATCH_TASK](
	[DBT_DEB_OID] [decimal](18, 0) NULL,
	[DBT_DET_OID] [decimal](18, 0) NULL,
	[DBT_LOCKSEQ] [decimal](9, 0) NULL,
	[DBT_OID] [decimal](18, 0) NOT NULL,
	[DBT_ORDERING] [decimal](10, 0) NULL,
	[DBT_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[IFC_PRIMITIVE_TYPE](
	[PTY_OID] [decimal](18, 0) NOT NULL,
	[PTY_LOCKSEQ] [decimal](9, 0) NULL,
	[PTY_NAME] [nvarchar](50) NULL,
	[PTY_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
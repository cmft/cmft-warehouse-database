﻿CREATE TABLE [dbo].[ITSM_FREE_DAYS](
	[FRD_OID] [decimal](18, 0) NOT NULL,
	[FRD_LOCKSEQ] [decimal](9, 0) NULL,
	[FRD_FREEDAY] [datetime] NULL,
	[FRD_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
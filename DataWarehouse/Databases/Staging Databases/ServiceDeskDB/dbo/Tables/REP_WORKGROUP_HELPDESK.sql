﻿CREATE TABLE [dbo].[REP_WORKGROUP_HELPDESK](
	[Wk_Helpdesk] [varchar](50) NULL,
	[WOG_ID] [decimal](18, 0) NULL,
	[WOG_NAME] [nvarchar](50) NULL
) ON [PRIMARY]
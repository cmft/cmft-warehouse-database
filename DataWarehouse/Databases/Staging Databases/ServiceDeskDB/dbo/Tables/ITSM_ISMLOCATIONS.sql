﻿CREATE TABLE [dbo].[ITSM_ISMLOCATIONS](
	[ISL_INTERVAL] [decimal](10, 0) NULL,
	[ISL_ISM_OID] [decimal](18, 0) NULL,
	[ISL_LOCKSEQ] [decimal](9, 0) NULL,
	[ISL_NAME] [nvarchar](255) NULL,
	[ISL_OID] [decimal](18, 0) NOT NULL,
	[ISL_TEM_OID] [decimal](18, 0) NULL,
	[ISL_TIMEOUT] [decimal](10, 0) NULL
) ON [PRIMARY]
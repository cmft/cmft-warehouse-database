﻿CREATE TABLE [dbo].[IFC_COLUMNTYPE](
	[CTY_OID] [decimal](18, 0) NOT NULL,
	[CTY_LOCKSEQ] [decimal](9, 0) NULL,
	[CTY_COLUMNTYPE] [nvarchar](50) NULL,
	[CTY_RDB_OID] [decimal](18, 0) NULL,
	[CTY_ATT_OID] [decimal](18, 0) NULL,
	[CTY_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
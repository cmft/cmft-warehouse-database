﻿CREATE TABLE [dbo].[REP_LABELS](
	[LBL_MOD_OID] [decimal](18, 0) NULL,
	[LBL_ADDTOENUMERATION] [decimal](1, 0) NULL,
	[LBL_JAVA_CONSTANT] [nvarchar](80) NULL,
	[LBL_OID] [decimal](18, 0) NOT NULL,
	[LBL_COMMENT] [nvarchar](255) NULL,
	[LBL_LOCKSEQ] [decimal](9, 0) NULL,
	[LBL_LCA_OID] [decimal](18, 0) NULL,
	[LBL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
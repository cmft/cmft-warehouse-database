﻿CREATE TABLE [dbo].[REP_LABELS_LOCALE](
	[LAL_OID] [decimal](18, 0) NOT NULL,
	[LAL_TEXT] [nvarchar](255) NULL,
	[LAL_LOCKSEQ] [decimal](9, 0) NULL,
	[LAL_LBL_OID] [decimal](18, 0) NULL,
	[LAL_LNG_OID] [decimal](18, 0) NULL,
	[LAL_REVISED] [decimal](1, 0) NULL,
	[LAL_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[LAL_CREATED] [datetime] NULL,
	[LAL_MODIFIED] [datetime] NULL,
	[LAL_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[LAL_ATS_OID] [decimal](18, 0) NULL,
	[LAL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_FILTERINFO](
	[FIL_OID] [decimal](18, 0) NOT NULL,
	[FIL_LOCKSEQ] [decimal](9, 0) NULL,
	[FIL_ENT_OID] [decimal](18, 0) NULL,
	[FIL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
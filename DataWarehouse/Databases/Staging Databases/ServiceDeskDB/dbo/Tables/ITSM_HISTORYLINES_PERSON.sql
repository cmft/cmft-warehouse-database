﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_PERSON](
	[HPS_OID] [decimal](18, 0) NOT NULL,
	[HPS_LOCKSEQ] [decimal](9, 0) NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[HPS_SYSTEM] [decimal](1, 0) NULL,
	[HPS_VALUEFROM] [decimal](18, 0) NULL,
	[HPS_VALUETO] [decimal](18, 0) NULL,
	[HPS_VALUEATR_OID] [decimal](18, 0) NULL,
	[HPS_SUBJECT] [nvarchar](255) NULL,
	[HPS_PER_OID] [decimal](18, 0) NULL,
	[HPS_ACC_OID] [decimal](18, 0) NULL,
	[HPS_TEM_OID] [decimal](18, 0) NULL,
	[HPS_MOD_ACC_OID] [decimal](18, 0) NULL,
	[HPS_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
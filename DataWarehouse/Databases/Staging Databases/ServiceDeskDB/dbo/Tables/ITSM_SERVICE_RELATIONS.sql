﻿CREATE TABLE [dbo].[ITSM_SERVICE_RELATIONS](
	[SRE_LOCKSEQ] [decimal](9, 0) NULL,
	[SRE_OID] [decimal](18, 0) NOT NULL,
	[SRE_REVRTY_OID] [decimal](18, 0) NULL,
	[SRE_ENT_OID] [decimal](18, 0) NULL,
	[SRE_RTY_OID] [decimal](18, 0) NULL,
	[SRE_INC_OID] [decimal](18, 0) NULL,
	[SRE_PRO_OID] [decimal](18, 0) NULL,
	[SRE_SER_OID] [decimal](18, 0) NULL,
	[SRE_TEM_OID] [decimal](18, 0) NULL,
	[SRE_CHA_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
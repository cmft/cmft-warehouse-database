﻿CREATE TABLE [dbo].[REP_PAGEINFOS](
	[PAI_OID] [decimal](18, 0) NOT NULL,
	[PAI_NO] [decimal](10, 0) NULL,
	[PAI_LOCKSEQ] [decimal](9, 0) NULL,
	[PAI_FOI_OID] [decimal](18, 0) NULL,
	[PAI_COMMENT] [nvarchar](255) NULL,
	[PAI_JAVACONSTANT] [nvarchar](80) NULL,
	[PAI_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
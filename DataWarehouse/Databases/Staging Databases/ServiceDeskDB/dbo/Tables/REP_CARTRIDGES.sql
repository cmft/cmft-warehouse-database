﻿CREATE TABLE [dbo].[REP_CARTRIDGES](
	[CAR_MOD_OID] [decimal](18, 0) NULL,
	[CAR_OID] [decimal](18, 0) NOT NULL,
	[CAR_LOCKSEQ] [decimal](9, 0) NULL,
	[CAR_CLASS] [nvarchar](256) NULL,
	[CAR_LBL_OID] [decimal](18, 0) NULL,
	[CAR_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_EXTERNAL_ENTITIES](
	[REE_OID] [decimal](18, 0) NOT NULL,
	[REE_LOCKSEQ] [decimal](9, 0) NULL,
	[REE_TEM_OID] [decimal](18, 0) NULL,
	[REE_RLS_OID] [decimal](18, 0) NULL,
	[REE_NAME] [nvarchar](50) NULL,
	[REE_TEMPLATE_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
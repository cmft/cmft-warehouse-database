﻿CREATE TABLE [dbo].[REP_PAGES_LOCALE](
	[PAL_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[PAL_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[PAL_LNG_OID] [decimal](18, 0) NULL,
	[PAL_PAI_OID] [decimal](18, 0) NULL,
	[PAL_ATS_OID] [decimal](18, 0) NULL,
	[PAL_CREATED] [datetime] NULL,
	[PAL_MODIFIED] [datetime] NULL,
	[PAL_LOCKSEQ] [decimal](9, 0) NULL,
	[PAL_OID] [decimal](18, 0) NOT NULL,
	[PAL_TEXT] [nvarchar](255) NULL,
	[PAL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
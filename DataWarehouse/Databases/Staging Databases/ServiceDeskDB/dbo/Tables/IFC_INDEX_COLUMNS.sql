﻿CREATE TABLE [dbo].[IFC_INDEX_COLUMNS](
	[IDC_OID] [decimal](18, 0) NOT NULL,
	[IDC_ORDERING] [decimal](10, 0) NULL,
	[IDC_LOCKSEQ] [decimal](9, 0) NULL,
	[IDC_IDX_OID] [decimal](18, 0) NULL,
	[IDC_COL_OID] [decimal](18, 0) NULL,
	[IDC_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_TEMPLATE_ACCESS](
	[TAC_OID] [decimal](18, 0) NOT NULL,
	[TAC_LOCKSEQ] [decimal](9, 0) NULL,
	[TAC_TEM_OID] [decimal](18, 0) NULL,
	[TAC_ROL_OID] [decimal](18, 0) NULL,
	[TAC_DEFAULTTEMPLATE] [decimal](1, 0) NULL,
	[TAC_TEMPLATE_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_WORKGROUP](
	[IHW_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[IHW_TEM_OID] [decimal](18, 0) NULL,
	[IHW_OID] [decimal](18, 0) NOT NULL,
	[IHW_LOCKSEQ] [decimal](9, 0) NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[IHW_SYSTEM] [decimal](1, 0) NULL,
	[IHW_VALUEFROM] [decimal](18, 0) NULL,
	[IHW_VALUETO] [decimal](18, 0) NULL,
	[IHW_VALUEATR_OID] [decimal](18, 0) NULL,
	[IHW_SUBJECT] [nvarchar](255) NULL,
	[IHW_WOG_OID] [decimal](18, 0) NULL,
	[IHW_CREATED_BY_OID] [decimal](18, 0) NULL,
	[IHW_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
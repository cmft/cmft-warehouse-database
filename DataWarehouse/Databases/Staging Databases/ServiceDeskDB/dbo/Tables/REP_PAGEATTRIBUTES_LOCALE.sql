﻿CREATE TABLE [dbo].[REP_PAGEATTRIBUTES_LOCALE](
	[PAT_ATS_OID] [decimal](18, 0) NOT NULL,
	[PAT_CREATED] [datetime] NULL,
	[PAT_CREATED_BY_OID] [decimal](18, 0) NULL,
	[PAT_LNG_OID] [decimal](18, 0) NULL,
	[PAT_LOCKSEQ] [decimal](9, 0) NULL,
	[PAT_MODIFIED] [datetime] NULL,
	[PAT_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[PAT_OID] [decimal](18, 0) NOT NULL,
	[PAT_PAA_OID] [decimal](18, 0) NULL,
	[PAT_TEM_OID] [decimal](18, 0) NULL,
	[PAT_TEXT] [nvarchar](255) NULL
) ON [PRIMARY]
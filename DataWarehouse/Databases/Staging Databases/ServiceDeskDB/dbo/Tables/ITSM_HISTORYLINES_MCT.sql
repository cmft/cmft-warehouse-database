﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_MCT](
	[HMC_OID] [decimal](18, 0) NOT NULL,
	[HMC_LOCKSEQ] [decimal](9, 0) NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[HMC_SYSTEM] [decimal](1, 0) NULL,
	[HMC_VALUEFROM] [decimal](18, 0) NULL,
	[HMC_VALUETO] [decimal](18, 0) NULL,
	[HMC_VALUEATR_OID] [decimal](18, 0) NULL,
	[HMC_SUBJECT] [nvarchar](255) NULL,
	[HMC_MCT_OID] [decimal](18, 0) NULL,
	[REG_CREATED_BY_OID] [decimal](18, 0) NULL,
	[REG_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[HMC_TEM_OID] [decimal](18, 0) NULL,
	[HMC_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_MESSAGES_LOCALE](
	[MSL_OID] [decimal](18, 0) NOT NULL,
	[MSL_TEXT] [nvarchar](255) NULL,
	[MSL_LOCKSEQ] [decimal](9, 0) NULL,
	[MSL_LNG_OID] [decimal](18, 0) NULL,
	[MSL_MES_OID] [decimal](18, 0) NULL,
	[MSL_REVISED] [decimal](1, 0) NULL,
	[MSL_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[MSL_CREATED] [datetime] NULL,
	[MSL_MODIFIED] [datetime] NULL,
	[MSL_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[MSL_ATS_OID] [decimal](18, 0) NULL,
	[MSL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_ATTR_PER_CATEGORY](
	[APC_OID] [decimal](18, 0) NOT NULL,
	[APC_ATR_OID] [decimal](18, 0) NULL,
	[APC_CAT_OID] [decimal](18, 0) NULL,
	[APC_LOCKSEQ] [decimal](9, 0) NULL,
	[APC_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
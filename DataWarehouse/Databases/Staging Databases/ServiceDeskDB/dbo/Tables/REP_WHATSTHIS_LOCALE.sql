﻿CREATE TABLE [dbo].[REP_WHATSTHIS_LOCALE](
	[WHL_OID] [decimal](18, 0) NOT NULL,
	[WHL_TEXT] [nvarchar](4000) NULL,
	[WHL_LOCKSEQ] [decimal](9, 0) NULL,
	[WHL_LNG_OID] [decimal](18, 0) NULL,
	[WHL_WHS_OID] [decimal](18, 0) NULL,
	[WHL_REVISED] [decimal](1, 0) NULL,
	[WHL_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[WHL_CREATED] [datetime] NULL,
	[WHL_MODIFIED] [datetime] NULL,
	[WHL_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[WHL_ATS_OID] [decimal](18, 0) NULL,
	[WHL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[IFC_IMPORTANCES](
	[IMP_OID] [decimal](18, 0) NOT NULL,
	[IMP_LOCKSEQ] [decimal](18, 0) NOT NULL,
	[IMP_TEXT] [nvarchar](40) NOT NULL,
	[IMP_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
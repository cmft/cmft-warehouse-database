﻿CREATE TABLE [dbo].[IFC_SEQUENCES](
	[SEQ_CAT_OID] [decimal](18, 0) NOT NULL,
	[SEQ_USN] [decimal](18, 0) NULL,
	[SEQ_LOCKSEQ] [decimal](10, 0) NULL,
	[SEQ_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
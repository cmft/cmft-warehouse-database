﻿CREATE TABLE [dbo].[REP_EXPORT_VIEW](
	[REV_INFO] [nvarchar](4000) NULL,
	[REV_ITEM] [decimal](18, 0) NULL,
	[REV_LOCKSEQ] [decimal](9, 0) NULL,
	[REV_NAME] [nvarchar](80) NULL,
	[REV_OID] [decimal](18, 0) NOT NULL,
	[REV_SUB_ITEM] [decimal](18, 0) NULL,
	[REV_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
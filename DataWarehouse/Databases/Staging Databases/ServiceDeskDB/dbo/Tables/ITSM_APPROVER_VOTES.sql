﻿CREATE TABLE [dbo].[ITSM_APPROVER_VOTES](
	[APV_OID] [decimal](18, 0) NOT NULL,
	[APV_APT_OID] [decimal](18, 0) NULL,
	[APV_APPROVED] [decimal](1, 0) NULL,
	[APV_LOCKSEQ] [decimal](9, 0) NULL,
	[APV_PER_OID] [decimal](18, 0) NULL,
	[APV_TEM_OID] [decimal](18, 0) NULL,
	[APV_REASON] [nvarchar](255) NULL
) ON [PRIMARY]
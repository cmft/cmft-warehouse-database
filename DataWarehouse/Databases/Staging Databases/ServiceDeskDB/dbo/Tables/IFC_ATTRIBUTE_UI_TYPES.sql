﻿CREATE TABLE [dbo].[IFC_ATTRIBUTE_UI_TYPES](
	[UIT_MOD_OID] [decimal](18, 0) NULL,
	[UIT_OID] [decimal](18, 0) NOT NULL,
	[UIT_NAME] [nvarchar](50) NULL,
	[UIT_LOCKSEQ] [decimal](9, 0) NULL,
	[UIT_ATT_OID] [decimal](18, 0) NULL,
	[UIT_UII_OID] [decimal](18, 0) NULL,
	[UIT_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
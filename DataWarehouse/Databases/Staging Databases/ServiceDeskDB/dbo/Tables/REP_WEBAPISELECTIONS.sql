﻿CREATE TABLE [dbo].[REP_WEBAPISELECTIONS](
	[WAS_ENT_OID] [decimal](18, 0) NULL,
	[WAS_FIELDS] [nvarchar](4000) NULL,
	[WAS_LOCKSEQ] [decimal](9, 0) NULL,
	[WAS_OID] [decimal](18, 0) NOT NULL,
	[WAS_RCD_OID] [decimal](18, 0) NULL,
	[WAS_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_ATTACHMENTS](
	[AHS_OID] [decimal](18, 0) NOT NULL,
	[AHS_BASENAME] [nvarchar](255) NULL,
	[AHS_ATT_OID] [decimal](18, 0) NULL,
	[AHS_ENT_OID] [decimal](18, 0) NULL,
	[AHS_FILENAME] [nvarchar](255) NULL,
	[AHS_LOCKSEQ] [decimal](9, 0) NULL,
	[AHS_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
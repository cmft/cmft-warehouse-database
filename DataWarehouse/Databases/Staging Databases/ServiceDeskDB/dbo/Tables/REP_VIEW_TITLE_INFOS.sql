﻿CREATE TABLE [dbo].[REP_VIEW_TITLE_INFOS](
	[VTI_ENT_OID] [decimal](18, 0) NULL,
	[VTI_JAV_OID] [decimal](18, 0) NULL,
	[VTI_COMMENT] [nvarchar](255) NULL,
	[VTI_JAVACONSTANT] [nvarchar](40) NULL,
	[VTI_LOCKSEQ] [decimal](9, 0) NULL,
	[VTI_OID] [decimal](18, 0) NOT NULL,
	[VTI_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
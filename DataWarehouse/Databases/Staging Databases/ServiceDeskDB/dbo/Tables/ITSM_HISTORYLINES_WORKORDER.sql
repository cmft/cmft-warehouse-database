﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_WORKORDER](
	[HWK_OID] [decimal](18, 0) NOT NULL,
	[HWK_LOCKSEQ] [decimal](9, 0) NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[HWK_SYSTEM] [decimal](1, 0) NULL,
	[HWK_VALUEFROM] [decimal](18, 0) NULL,
	[HWK_VALUETO] [decimal](18, 0) NULL,
	[HWK_VALUEATR_OID] [decimal](18, 0) NULL,
	[HWK_SUBJECT] [nvarchar](255) NULL,
	[HWK_WOR_OID] [decimal](18, 0) NULL,
	[REG_CREATED_BY_OID] [decimal](18, 0) NULL,
	[REG_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[HWK_TEM_OID] [decimal](18, 0) NULL,
	[HWK_SPENTTIME] [float] NULL,
	[HWK_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
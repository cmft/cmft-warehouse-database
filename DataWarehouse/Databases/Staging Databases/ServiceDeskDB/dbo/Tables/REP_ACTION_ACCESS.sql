﻿CREATE TABLE [dbo].[REP_ACTION_ACCESS](
	[AAC_OID] [decimal](18, 0) NOT NULL,
	[AAC_ACT_OID] [decimal](18, 0) NULL,
	[AAC_ROL_OID] [decimal](18, 0) NULL,
	[AAC_LOCKSEQ] [decimal](9, 0) NULL,
	[AAC_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
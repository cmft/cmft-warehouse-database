﻿CREATE TABLE [dbo].[REP_CODES](
	[RCD_OID] [decimal](18, 0) NOT NULL,
	[RCD_LOCKSEQ] [decimal](9, 0) NULL,
	[RCD_JAVACONSTANT] [nvarchar](40) NULL,
	[RCD_ICON] [nvarchar](256) NULL,
	[RCD_COMMENT] [nvarchar](255) NULL,
	[RCD_CODEDISABLED] [decimal](1, 0) NULL,
	[RCD_SUBTYPE] [decimal](18, 0) NULL,
	[RCD_RCD_OID] [decimal](18, 0) NULL,
	[RCD_TEM_OID] [decimal](18, 0) NULL,
	[RCD_ORDERING] [decimal](10, 0) NULL,
	[RCD_JAV_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
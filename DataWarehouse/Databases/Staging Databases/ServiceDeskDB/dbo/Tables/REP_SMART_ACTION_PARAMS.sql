﻿CREATE TABLE [dbo].[REP_SMART_ACTION_PARAMS](
	[SAP_OID] [decimal](18, 0) NOT NULL,
	[SAP_NAME] [nvarchar](50) NULL,
	[SAP_POSTTEXT] [nvarchar](50) NULL,
	[SAP_ACT_OID] [decimal](18, 0) NULL,
	[SAP_ORDERING] [decimal](10, 0) NULL,
	[SAP_LOCKSEQ] [decimal](9, 0) NULL,
	[SAP_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
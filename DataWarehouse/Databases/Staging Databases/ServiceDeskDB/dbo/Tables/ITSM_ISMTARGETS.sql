﻿CREATE TABLE [dbo].[ITSM_ISMTARGETS](
	[IST_HOST] [nvarchar](255) NULL,
	[IST_ISM_OID] [decimal](18, 0) NULL,
	[IST_LOCKSEQ] [decimal](9, 0) NULL,
	[IST_OID] [decimal](18, 0) NOT NULL,
	[IST_PORT] [decimal](10, 0) NULL,
	[IST_TEM_OID] [decimal](18, 0) NULL,
	[IST_URL] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_DATA_EXCHANGE_TASKS](
	[DET_RLS_OID] [decimal](18, 0) NULL,
	[DET_NAME] [nvarchar](255) NULL,
	[DET_EXPORTMAPPING] [nvarchar](255) NULL,
	[DET_EXCHANGEFILE] [nvarchar](255) NULL,
	[DET_IMPORT] [decimal](1, 0) NULL,
	[DET_DEBUG] [decimal](1, 0) NULL,
	[DET_OID] [decimal](18, 0) NOT NULL,
	[DET_LOCKSEQ] [decimal](9, 0) NULL,
	[DET_ACC_OID] [decimal](18, 0) NULL,
	[DET_TEM_OID] [decimal](18, 0) NULL,
	[DET_EXPORT] [decimal](1, 0) NULL,
	[DET_EXCHANGEPREVFILE] [nvarchar](255) NULL,
	[DET_EXCHANGEDELTA] [decimal](1, 0) NULL
) ON [PRIMARY]
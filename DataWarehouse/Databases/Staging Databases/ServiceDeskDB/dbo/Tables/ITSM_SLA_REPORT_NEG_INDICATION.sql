﻿CREATE TABLE [dbo].[ITSM_SLA_REPORT_NEG_INDICATION](
	[RNI_INC_OID] [decimal](18, 0) NULL,
	[RNI_LOCKSEQ] [decimal](9, 0) NULL,
	[RNI_OID] [decimal](18, 0) NOT NULL,
	[RNI_SER_OID] [decimal](18, 0) NULL,
	[RNI_SRR_OID] [decimal](18, 0) NULL,
	[RNI_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
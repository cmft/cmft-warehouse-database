﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_CONFIG](
	[HCF_OID] [decimal](18, 0) NOT NULL,
	[HCF_LOCKSEQ] [decimal](9, 0) NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[HCF_SYSTEM] [decimal](1, 0) NULL,
	[HCF_VALUEFROM] [decimal](18, 0) NULL,
	[HCF_VALUETO] [decimal](18, 0) NULL,
	[HCF_VALUEATR_OID] [decimal](18, 0) NULL,
	[HCF_SUBJECT] [nvarchar](255) NULL,
	[HCF_CIT_OID] [decimal](18, 0) NULL,
	[REG_CREATED_BY_OID] [decimal](18, 0) NULL,
	[REG_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[HCF_TEM_OID] [decimal](18, 0) NULL,
	[HCF_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
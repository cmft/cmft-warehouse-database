﻿CREATE TABLE [dbo].[REP_TEMPL_ENTITYSET_VALUES](
	[TEV_OID] [decimal](18, 0) NOT NULL,
	[TEV_LOCKSEQ] [decimal](9, 0) NULL,
	[TEV_TEM_OID] [decimal](18, 0) NULL,
	[TEV_TVA_OID] [decimal](18, 0) NULL,
	[TEV_TEMPLATE_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[IFC_TABLES](
	[TAB_OID] [decimal](18, 0) NOT NULL,
	[TAB_NAME] [nvarchar](30) NULL,
	[TAB_LOCKSEQ] [decimal](9, 0) NULL,
	[TAB_ACRONYM] [nvarchar](3) NULL,
	[TAB_FILL_ALWAYS] [decimal](1, 0) NULL,
	[TAB_ONLYFILLWHENDEMODATA] [decimal](1, 0) NULL,
	[TAB_TEM_OID] [decimal](18, 0) NULL,
	[TAB_MOD_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
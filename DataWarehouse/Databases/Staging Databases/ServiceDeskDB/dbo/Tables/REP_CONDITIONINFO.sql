﻿CREATE TABLE [dbo].[REP_CONDITIONINFO](
	[CON_OID] [decimal](18, 0) NOT NULL,
	[CON_LOCKSEQ] [decimal](9, 0) NULL,
	[CON_OPERATOR] [decimal](10, 0) NULL,
	[CON_VALUE] [nvarchar](255) NULL,
	[CON_FIL_OID] [decimal](18, 0) NULL,
	[CON_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
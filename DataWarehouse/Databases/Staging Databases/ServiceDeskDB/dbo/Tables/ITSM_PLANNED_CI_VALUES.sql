﻿CREATE TABLE [dbo].[ITSM_PLANNED_CI_VALUES](
	[PCV_ATR_OID] [decimal](18, 0) NULL,
	[PCV_LOCKSEQ] [decimal](9, 0) NULL,
	[PCV_OID] [decimal](18, 0) NOT NULL,
	[PCV_TEM_OID] [decimal](18, 0) NULL,
	[PCV_VALUE] [nvarchar](4000) NULL,
	[PCV_WCI_OID] [decimal](18, 0) NULL,
	[PCV_ATR_AGGREGATION_OID] [decimal](18, 0) NULL,
	[PCV_MAKEEMPTY] [decimal](1, 0) NULL,
	[PCV_USERELATIVEDATE] [decimal](1, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_OPENINGS_SCHEDULES](
	[OSC_LOCKSEQ] [decimal](9, 0) NULL,
	[OSC_NAME] [nvarchar](50) NULL,
	[OSC_OID] [decimal](18, 0) NOT NULL,
	[OSC_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
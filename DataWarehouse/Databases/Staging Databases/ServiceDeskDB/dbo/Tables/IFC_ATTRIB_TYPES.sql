﻿CREATE TABLE [dbo].[IFC_ATTRIB_TYPES](
	[ATT_OID] [decimal](18, 0) NOT NULL,
	[ATT_NAME] [nvarchar](50) NULL,
	[ATT_COLUMN_LENGTH] [decimal](10, 0) NULL,
	[ATT_CLASS] [nvarchar](256) NULL,
	[ATT_LOCKSEQ] [decimal](9, 0) NULL,
	[ATT_SYT_OID] [decimal](18, 0) NULL,
	[ATT_PTY_OID] [decimal](18, 0) NULL,
	[ATT_TEM_OID] [decimal](18, 0) NULL,
	[ATT_MOD_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_PRIORITYIMPACTSETTINGS](
	[PIS_PRI_OID] [decimal](18, 0) NULL,
	[PIS_IMP_OID] [decimal](18, 0) NULL,
	[PIS_OID] [decimal](18, 0) NOT NULL,
	[PIS_LOCKSEQ] [decimal](9, 0) NULL,
	[PIS_SEL_OID] [decimal](18, 0) NULL,
	[PIS_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
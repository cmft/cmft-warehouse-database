﻿CREATE TABLE [dbo].[ITSM_HIST_INFO_PERSON](
	[HIR_INFORMATION] [nvarchar](4000) NULL,
	[HIR_HPS_OID] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
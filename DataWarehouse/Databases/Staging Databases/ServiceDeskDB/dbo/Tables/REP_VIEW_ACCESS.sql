﻿CREATE TABLE [dbo].[REP_VIEW_ACCESS](
	[VAC_OID] [decimal](18, 0) NOT NULL,
	[VAC_LOCKSEQ] [decimal](9, 0) NULL,
	[VAC_VIW_OID] [decimal](18, 0) NULL,
	[VAC_ROL_OID] [decimal](18, 0) NULL,
	[VAC_DEFAULTVIEW] [decimal](1, 0) NULL,
	[VAC_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
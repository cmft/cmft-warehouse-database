﻿CREATE TABLE [dbo].[REP_APPLICATIONS](
	[APP_OID] [decimal](18, 0) NOT NULL,
	[APP_NAME] [nvarchar](50) NULL,
	[APP_WORKINGDIRECTORY] [nvarchar](80) NULL,
	[APP_COMMANDLINE] [nvarchar](80) NULL,
	[APP_LOCKSEQ] [decimal](9, 0) NULL,
	[APP_DESCRIPTION] [nvarchar](255) NULL,
	[APP_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_ATTR_PER_STATUS](
	[APS_OID] [decimal](18, 0) NOT NULL,
	[APS_ATR_OID] [decimal](18, 0) NULL,
	[APS_COD_OID] [decimal](18, 0) NULL,
	[APS_LOCKSEQ] [decimal](9, 0) NULL,
	[APS_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
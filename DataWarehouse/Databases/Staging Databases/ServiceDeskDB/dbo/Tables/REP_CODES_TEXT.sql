﻿CREATE TABLE [dbo].[REP_CODES_TEXT](
	[RCT_OID] [decimal](18, 0) NOT NULL,
	[RCT_CREATED] [datetime] NULL,
	[RCT_MODIFIED] [datetime] NULL,
	[RCT_LOCKSEQ] [decimal](9, 0) NULL,
	[RCT_NAME] [nvarchar](255) NULL,
	[RCT_REVISED] [decimal](1, 0) NULL,
	[RCT_TEM_OID] [decimal](18, 0) NULL,
	[RCT_ATS_OID] [decimal](18, 0) NULL,
	[RCT_RCD_OID] [decimal](18, 0) NULL,
	[RCT_LNG_OID] [decimal](18, 0) NULL,
	[RCT_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[RCT_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
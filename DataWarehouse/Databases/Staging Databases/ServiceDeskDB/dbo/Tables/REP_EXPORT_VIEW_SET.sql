﻿CREATE TABLE [dbo].[REP_EXPORT_VIEW_SET](
	[EVS_LOCKSEQ] [decimal](9, 0) NULL,
	[EVS_NAME] [nvarchar](80) NULL,
	[EVS_OID] [decimal](18, 0) NOT NULL,
	[EVS_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
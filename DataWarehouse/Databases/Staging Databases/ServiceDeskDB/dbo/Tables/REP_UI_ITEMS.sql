﻿CREATE TABLE [dbo].[REP_UI_ITEMS](
	[UII_MOD_OID] [decimal](18, 0) NULL,
	[UII_OID] [decimal](18, 0) NOT NULL,
	[UII_NAME] [nvarchar](256) NULL,
	[UII_LOCKSEQ] [decimal](9, 0) NULL,
	[UII_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
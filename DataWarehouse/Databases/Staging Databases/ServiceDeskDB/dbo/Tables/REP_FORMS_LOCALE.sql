﻿CREATE TABLE [dbo].[REP_FORMS_LOCALE](
	[FLO_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[FLO_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[FLO_LNG_OID] [decimal](18, 0) NULL,
	[FLO_ATS_OID] [decimal](18, 0) NULL,
	[FLO_FOI_OID] [decimal](18, 0) NULL,
	[FLO_CREATED] [datetime] NULL,
	[FLO_MODIFIED] [datetime] NULL,
	[FLO_OID] [decimal](18, 0) NOT NULL,
	[FLO_LOCKSEQ] [decimal](9, 0) NULL,
	[FLO_TEXT] [nvarchar](255) NULL,
	[FLO_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
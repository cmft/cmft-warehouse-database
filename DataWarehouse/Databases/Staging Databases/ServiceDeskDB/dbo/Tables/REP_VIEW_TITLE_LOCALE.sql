﻿CREATE TABLE [dbo].[REP_VIEW_TITLE_LOCALE](
	[VTL_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[VTL_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[VTL_LNG_OID] [decimal](18, 0) NULL,
	[VTL_VTI_OID] [decimal](18, 0) NULL,
	[VTL_ATS_OID] [decimal](18, 0) NULL,
	[VTL_LOCKSEQ] [decimal](9, 0) NULL,
	[VTL_CREATED] [datetime] NULL,
	[VTL_MODIFIED] [datetime] NULL,
	[VTL_OID] [decimal](18, 0) NOT NULL,
	[VTL_TEXT] [nvarchar](255) NULL,
	[VTL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_WORKORDER_CIS](
	[WCI_OID] [decimal](18, 0) NOT NULL,
	[WCI_LOCKSEQ] [decimal](9, 0) NULL,
	[WCI_CIT_OID] [decimal](18, 0) NULL,
	[WCI_WOR_OID] [decimal](18, 0) NULL,
	[WCI_TEM_OID] [decimal](18, 0) NULL,
	[WCI_OUTAGEAFFECTED] [decimal](1, 0) NULL,
	[WCI_OUI_SEV_OID] [decimal](18, 0) NULL,
	[WCI_OUI_MSG_OID] [decimal](18, 0) NULL,
	[WCI_OUI_MSGGROUP] [nvarchar](80) NULL,
	[WCI_OUI_STATUSVARIABLE] [nvarchar](80) NULL,
	[WCI_OUI_APPLICATION] [nvarchar](80) NULL,
	[WCI_OUI_OBJECT] [nvarchar](80) NULL,
	[WCI_OUI_TEXT] [nvarchar](80) NULL,
	[WCI_OUI_MSGTYPE] [nvarchar](80) NULL,
	[WCI_OUI_SERVICE_NAME] [nvarchar](80) NULL
) ON [PRIMARY]
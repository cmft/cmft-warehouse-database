﻿CREATE TABLE [dbo].[ITSM_CI_ORGS](
	[CIO_CIT_OID] [decimal](18, 0) NULL,
	[CIO_ORG_OID] [decimal](18, 0) NULL,
	[CIO_TEM_OID] [decimal](18, 0) NULL,
	[CIO_OID] [decimal](18, 0) NOT NULL,
	[CIO_LOCKSEQ] [decimal](9, 0) NULL
) ON [PRIMARY]
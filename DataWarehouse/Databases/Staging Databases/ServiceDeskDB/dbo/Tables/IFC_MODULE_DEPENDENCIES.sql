﻿CREATE TABLE [dbo].[IFC_MODULE_DEPENDENCIES](
	[MDS_OID] [decimal](18, 0) NOT NULL,
	[MDS_LOCKSEQ] [decimal](9, 0) NULL,
	[MDS_MOD_FROM_OID] [decimal](18, 0) NULL,
	[MDS_MOD_TO_OID] [decimal](18, 0) NULL,
	[MDS_TEM_OID] [decimal](18, 0) NULL,
	[MDS_VERSION] [nvarchar](40) NULL
) ON [PRIMARY]
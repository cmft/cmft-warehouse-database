﻿CREATE TABLE [dbo].[IFC_ATTRIBUTES](
	[ATR_ATT_OID] [decimal](18, 0) NULL,
	[ATR_UII_OID] [decimal](18, 0) NULL,
	[ATR_DEPENDENT] [decimal](1, 0) NULL,
	[ATR_AGGREGATION] [decimal](1, 0) NULL,
	[ATR_ATR_RELATION_OID] [decimal](18, 0) NULL,
	[ATR_ENTITY_TO_OID] [decimal](18, 0) NULL,
	[ATR_OID] [decimal](18, 0) NOT NULL,
	[ATR_NAME] [nvarchar](50) NULL,
	[ATR_COMPUTED] [decimal](1, 0) NULL,
	[ATR_LOCKSEQ] [decimal](9, 0) NULL,
	[ATR_FROM_VALUE] [decimal](10, 0) NULL,
	[ATR_TO_VALUE] [decimal](10, 0) NULL,
	[ATR_ENABLE_FOR_ALL_CATEGORIES] [decimal](1, 0) NULL,
	[ATR_SHOWINFIELDSEL] [decimal](1, 0) NULL,
	[ATR_DEFAULTSHOW] [decimal](1, 0) NULL,
	[ATR_ORDERING] [decimal](1, 0) NULL,
	[ATR_CUSTOMFIELD] [decimal](1, 0) NULL,
	[ATR_CUSTFIELDACT] [decimal](1, 0) NULL,
	[ATR_ENT_OID] [decimal](18, 0) NULL,
	[ATR_WHS_OID] [decimal](18, 0) NULL,
	[ATR_LBL_OID] [decimal](18, 0) NULL,
	[ATR_DIF_OID] [decimal](18, 0) NULL,
	[ATR_ALWAYSREQUIRED] [decimal](1, 0) NULL,
	[ATR_TEM_OID] [decimal](18, 0) NULL,
	[ATR_BASED_ON_OID] [decimal](18, 0) NULL,
	[ATR_CALCULATIONVALUE] [decimal](18, 5) NULL,
	[ATR_RCD_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
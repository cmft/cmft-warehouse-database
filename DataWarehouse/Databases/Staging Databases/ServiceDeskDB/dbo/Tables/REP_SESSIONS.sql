﻿CREATE TABLE [dbo].[REP_SESSIONS](
	[SES_OID] [decimal](18, 0) NOT NULL,
	[SES_LOCKSEQ] [decimal](9, 0) NOT NULL,
	[SES_CREATED] [datetime] NOT NULL,
	[SES_ACC_OID] [decimal](18, 0) NOT NULL,
	[SES_SERVER_IPADDRESS] [nvarchar](255) NOT NULL,
	[SES_CLIENT_IPADDRESS] [nvarchar](255) NULL,
	[SES_CLIENT_IPPORT] [decimal](9, 0) NULL,
	[SES_TEM_OID] [decimal](18, 0) NULL,
	[SES_THREADNAME] [nvarchar](255) NULL,
	[SES_SERVER_IPPORT] [decimal](9, 0) NOT NULL,
	[SES_MARKEDFORREMOVAL] [decimal](1, 0) NULL,
	[SES_ISCONCURRENT] [decimal](1, 0) NULL,
	[SES_SVI_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
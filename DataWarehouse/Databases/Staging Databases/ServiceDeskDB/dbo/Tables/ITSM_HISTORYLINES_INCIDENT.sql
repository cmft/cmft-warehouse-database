﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_INCIDENT](
	[HIN_SPENTTIME] [float] NULL,
	[HIN_OID] [decimal](18, 0) NOT NULL,
	[HIN_LOCKSEQ] [decimal](9, 0) NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[HIN_SYSTEM] [decimal](1, 0) NULL,
	[HIN_VALUEFROM] [decimal](18, 0) NULL,
	[HIN_VALUETO] [decimal](18, 0) NULL,
	[HIN_VALUEATR_OID] [decimal](18, 0) NULL,
	[HIN_SUBJECT] [nvarchar](255) NULL,
	[HIN_INC_OID] [decimal](18, 0) NULL,
	[REG_CREATED_BY_OID] [decimal](18, 0) NULL,
	[REG_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[HIN_TEM_OID] [decimal](18, 0) NULL,
	[HIN_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
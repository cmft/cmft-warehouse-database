﻿CREATE TABLE [dbo].[REP_LABEL_CATEGORY](
	[LCA_OID] [decimal](18, 0) NOT NULL,
	[LCA_LOCKSEQ] [decimal](9, 0) NULL,
	[LCA_NAME] [nvarchar](50) NULL,
	[LCA_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
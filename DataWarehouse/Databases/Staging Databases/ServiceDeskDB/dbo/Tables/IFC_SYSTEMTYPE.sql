﻿CREATE TABLE [dbo].[IFC_SYSTEMTYPE](
	[SYT_OID] [decimal](18, 0) NOT NULL,
	[SYT_LOCKSEQ] [decimal](9, 0) NULL,
	[SYT_NAME] [nvarchar](50) NULL,
	[SYT_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
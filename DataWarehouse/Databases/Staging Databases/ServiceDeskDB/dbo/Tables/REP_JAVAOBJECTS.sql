﻿CREATE TABLE [dbo].[REP_JAVAOBJECTS](
	[JAV_OID] [decimal](18, 0) NOT NULL,
	[JAV_LOCKSEQ] [decimal](18, 0) NOT NULL,
	[JAV_ENTITY] [decimal](18, 0) NOT NULL,
	[JAV_INSTANCE] [nvarchar](4000) NOT NULL,
	[JAV_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
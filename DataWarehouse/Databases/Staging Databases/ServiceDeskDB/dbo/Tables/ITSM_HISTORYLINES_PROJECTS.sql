﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_PROJECTS](
	[HPJ_MOD_ACC_OID] [decimal](18, 0) NULL,
	[HPJ_CRE_ACC_OID] [decimal](18, 0) NULL,
	[HPJ_PRJ_OID] [decimal](18, 0) NULL,
	[HPJ_LOCKSEQ] [decimal](9, 0) NULL,
	[HPJ_ATTRIBUTE] [decimal](18, 0) NULL,
	[HPJ_VALUE_FROM] [decimal](18, 0) NULL,
	[HPJ_SYSTEM] [decimal](1, 0) NULL,
	[HPJ_OID] [decimal](18, 0) NOT NULL,
	[HPJ_VALUE_TO] [decimal](18, 0) NULL,
	[HPJ_SUBJECT] [nvarchar](255) NULL,
	[HPJ_CREATED] [datetime] NULL,
	[HPJ_MODIFIED] [datetime] NULL,
	[HPJ_TEM_OID] [decimal](18, 0) NULL,
	[HPJ_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
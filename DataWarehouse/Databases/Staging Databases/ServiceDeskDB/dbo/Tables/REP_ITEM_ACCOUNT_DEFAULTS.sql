﻿CREATE TABLE [dbo].[REP_ITEM_ACCOUNT_DEFAULTS](
	[IAD_TEM_OID] [decimal](18, 0) NULL,
	[IAD_OID] [decimal](18, 0) NOT NULL,
	[IAD_DEFAULT_TEM_OID] [decimal](18, 0) NULL,
	[IAD_FOI_OID] [decimal](18, 0) NULL,
	[IAD_JAV_OID] [decimal](18, 0) NULL,
	[IAD_ACC_OID] [decimal](18, 0) NULL,
	[IAD_ENT_OID] [decimal](18, 0) NULL,
	[IAD_LOCKSEQ] [decimal](9, 0) NULL
) ON [PRIMARY]
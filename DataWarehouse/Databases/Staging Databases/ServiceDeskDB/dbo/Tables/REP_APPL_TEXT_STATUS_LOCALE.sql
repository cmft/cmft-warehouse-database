﻿CREATE TABLE [dbo].[REP_APPL_TEXT_STATUS_LOCALE](
	[ATL_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[ATL_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[ATL_LNG_OID] [decimal](18, 0) NULL,
	[ATL_ATS_OID] [decimal](18, 0) NULL,
	[ATL_STATUS_ATS_OID] [decimal](18, 0) NULL,
	[ATL_OID] [decimal](18, 0) NOT NULL,
	[ATL_LOCKSEQ] [decimal](9, 0) NULL,
	[ATL_TEXT] [nvarchar](255) NULL,
	[ATL_CREATED] [datetime] NULL,
	[ATL_MODIFIED] [datetime] NULL,
	[ATL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
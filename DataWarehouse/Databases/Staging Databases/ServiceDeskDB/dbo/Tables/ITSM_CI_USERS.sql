﻿CREATE TABLE [dbo].[ITSM_CI_USERS](
	[CIU_OID] [decimal](18, 0) NOT NULL,
	[CIU_CIT_OID] [decimal](18, 0) NULL,
	[CIU_LOCKSEQ] [decimal](9, 0) NULL,
	[CIU_USER_PER_OID] [decimal](18, 0) NULL,
	[CIU_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
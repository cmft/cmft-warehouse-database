﻿CREATE TABLE [dbo].[ITSM_PROJECTS](
	[PRJ_ASS_CONTR_OUT_ORG] [decimal](18, 0) NULL,
	[PRJ_TEM_OID] [decimal](18, 0) NULL,
	[PRJ_PROJ_MAN_OID] [decimal](18, 0) NULL,
	[PRJ_IMP_OID] [decimal](18, 0) NULL,
	[PRJ_ASSIGN_STATUS] [decimal](18, 0) NULL,
	[PRJ_ASSIGN_PRIORITY] [decimal](18, 0) NULL,
	[PRJ_ASS_WOG_OID] [decimal](18, 0) NULL,
	[PRJ_PRI_OID] [decimal](18, 0) NULL,
	[PRJ_MOD_ACC_OID] [decimal](18, 0) NULL,
	[PRJ_CRE_ACC_OID] [decimal](18, 0) NULL,
	[PRJ_CREATED] [datetime] NULL,
	[PRJ_MODIFIED] [datetime] NULL,
	[PRJ_ASSIGN_REFERENCE] [nvarchar](50) NULL,
	[PRJ_ASSIGN_EXTDEADLINE] [datetime] NULL,
	[PRJ_APT_STATUS] [decimal](18, 0) NULL,
	[PRJ_APT_DEADLINE] [datetime] NULL,
	[PRJ_APT_WOG_OID] [decimal](18, 0) NULL,
	[PRJ_INITIATOR_PER_OID] [decimal](18, 0) NULL,
	[PRJ_REQUESTOR_PER_OID] [decimal](18, 0) NULL,
	[PRJ_APT_NROFAPPROVERSREQUIRED] [decimal](9, 0) NULL,
	[PRJ_APT_NROFAPPROVERS] [decimal](9, 0) NULL,
	[PRJ_APT_NROFAPPROVERSAPPROVED] [decimal](9, 0) NULL,
	[PRJ_APT_DESCRIPTION] [nvarchar](80) NULL,
	[SLC_PRJ_PER_OID] [decimal](18, 0) NULL,
	[PRJ_ID] [decimal](18, 0) NULL,
	[PRJ_OID] [decimal](18, 0) NOT NULL,
	[PRJ_LOCKSEQ] [decimal](9, 0) NULL,
	[PRJ_DESCRIPTION] [nvarchar](80) NULL,
	[PRJ_LATE_FINISH] [datetime] NULL,
	[PRJ_LATE_START] [datetime] NULL,
	[PRJ_EARLY_START] [datetime] NULL,
	[PRJ_EARLY_FINISH] [datetime] NULL,
	[PRJ_MAXIMUM_DURATION] [float] NULL,
	[PRJ_PLAN_DURATION] [float] NULL,
	[PRJ_PLANNED_COST] [decimal](12, 2) NULL,
	[PRJ_PLAN_START] [datetime] NULL,
	[PRJ_PLAN_FINISH] [datetime] NULL,
	[PRJ_ACTUAL_DURATION] [float] NULL,
	[PRJ_ACTUAL_COST] [decimal](12, 2) NULL,
	[PRJ_ACTUAL_START] [datetime] NULL,
	[PRJ_ACTUAL_FINISH] [datetime] NULL,
	[PRJ_DEADLINE] [datetime] NULL,
	[PRJ_ATTACHMENT_EXISTS] [decimal](1, 0) NULL,
	[PRJ_STA_OID] [decimal](18, 0) NULL,
	[PRJ_CAT_OID] [decimal](18, 0) NULL,
	[PRJ_POO_OID] [decimal](18, 0) NULL,
	[PRJ_SOURCEID] [nvarchar](80) NULL,
	[PRJ_ASS_WOG_FROM_OID] [decimal](18, 0) NULL,
	[PRJ_ASS_PER_TO_OID] [decimal](18, 0) NULL,
	[PRJ_ASS_PER_FROM_OID] [decimal](18, 0) NULL,
	[PRJ_ASS_CONTR_OUT_PER] [decimal](18, 0) NULL
) ON [PRIMARY]
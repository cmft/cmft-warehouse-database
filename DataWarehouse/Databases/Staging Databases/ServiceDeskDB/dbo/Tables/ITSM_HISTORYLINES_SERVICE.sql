﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_SERVICE](
	[HSR_SRV_OID] [decimal](18, 0) NULL,
	[HSR_CRE_ACC_OID] [decimal](18, 0) NULL,
	[HSR_MOD_ACC_OID] [decimal](18, 0) NULL,
	[HSR_CREATED] [datetime] NULL,
	[HSR_MODIFIED] [datetime] NULL,
	[HSR_ATTRIBUTE] [decimal](18, 0) NULL,
	[HSR_LOCKSEQ] [decimal](9, 0) NULL,
	[HSR_OID] [decimal](18, 0) NOT NULL,
	[HSR_SUBJECT] [nvarchar](255) NULL,
	[HSR_SYSTEM] [decimal](1, 0) NULL,
	[HSR_VALUE_FROM] [decimal](18, 0) NULL,
	[HSR_VALUE_TO] [decimal](18, 0) NULL,
	[HSR_TEM_OID] [decimal](18, 0) NULL,
	[HSR_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_TEMPLATE_VALUES](
	[TVA_CURRENT_PERSON] [nvarchar](80) NULL,
	[TVA_OID] [decimal](18, 0) NOT NULL,
	[TVA_LOCKSEQ] [decimal](9, 0) NULL,
	[TVA_VALUE] [nvarchar](4000) NULL,
	[TVA_TEM_OID] [decimal](18, 0) NULL,
	[TVA_ATR_OID] [decimal](18, 0) NULL,
	[TVA_ATR_AGGREGATION_OID] [decimal](18, 0) NULL,
	[TVA_TEM_VALUE_OID] [decimal](18, 0) NULL,
	[TVA_TEMPLATE_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
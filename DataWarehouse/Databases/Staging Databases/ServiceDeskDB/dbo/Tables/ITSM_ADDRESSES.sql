﻿CREATE TABLE [dbo].[ITSM_ADDRESSES](
	[ADR_OID] [decimal](18, 0) NOT NULL,
	[ADR_STREET1] [nvarchar](50) NULL,
	[ADR_ZIPPOSTALCODE] [nvarchar](50) NULL,
	[ADR_COUNTRYREGION] [nvarchar](50) NULL,
	[ADR_ORG_OID] [decimal](18, 0) NULL,
	[ADR_TYP_OID] [decimal](18, 0) NULL,
	[ADR_PER_OID] [decimal](18, 0) NULL,
	[ADR_STATEPROVINCE] [nvarchar](50) NULL,
	[ADR_CITY] [nvarchar](50) NULL,
	[ADR_LOCKSEQ] [decimal](9, 0) NULL,
	[ADR_STREET2] [nvarchar](50) NULL,
	[ADR_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
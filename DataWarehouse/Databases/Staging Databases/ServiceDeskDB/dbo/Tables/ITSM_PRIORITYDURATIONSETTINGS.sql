﻿CREATE TABLE [dbo].[ITSM_PRIORITYDURATIONSETTINGS](
	[PDS_OID] [decimal](18, 0) NOT NULL,
	[PDS_MAXIMUMDURATION] [float] NULL,
	[PDS_PRIORITY] [decimal](18, 0) NULL,
	[PDS_LOCKSEQ] [decimal](9, 0) NULL,
	[PDS_ENTITY] [decimal](18, 0) NULL,
	[PDS_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_HOLIDAY_CALENDARS](
	[HCR_INCLUDEFREEDAYS] [decimal](1, 0) NULL,
	[HCR_LOCKSEQ] [decimal](9, 0) NULL,
	[HCR_NAME] [nvarchar](50) NULL,
	[HCR_OID] [decimal](18, 0) NOT NULL,
	[HCR_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
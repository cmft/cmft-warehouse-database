﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_ORGANIZATION](
	[IHO_ORG_OID] [decimal](18, 0) NULL,
	[IHO_ACC_OID] [decimal](18, 0) NULL,
	[IHO_TEM_OID] [decimal](18, 0) NULL,
	[IHO_MOD_ACC_OID] [decimal](18, 0) NULL,
	[IHO_OID] [decimal](18, 0) NOT NULL,
	[IHO_LOCKSEQ] [decimal](9, 0) NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[IHO_SYSTEM] [decimal](1, 0) NULL,
	[IHO_VALUEFROM] [decimal](18, 0) NULL,
	[IHO_VALUETO] [decimal](18, 0) NULL,
	[IHO_VALUEATR_OID] [decimal](18, 0) NULL,
	[IHO_SUBJECT] [nvarchar](255) NULL,
	[IHO_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
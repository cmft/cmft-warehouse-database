﻿CREATE TABLE [dbo].[REP_COLUMNINFO](
	[RCI_OID] [decimal](18, 0) NOT NULL,
	[RCI_LOCKSEQ] [decimal](9, 0) NULL,
	[RCI_SUBTYPE] [decimal](18, 0) NULL,
	[RCI_ALIGNMENT] [decimal](10, 0) NULL,
	[RCI_VISIBLE] [decimal](1, 0) NULL,
	[RCI_WIDTH] [decimal](10, 0) NULL,
	[RCI_COLUMNPOSITION] [decimal](10, 0) NULL,
	[RCI_ENT_OID] [decimal](18, 0) NULL,
	[RCI_DISPLAYFORMAT] [decimal](10, 0) NULL,
	[RCI_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
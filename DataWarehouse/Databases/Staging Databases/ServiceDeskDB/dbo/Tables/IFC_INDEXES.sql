﻿CREATE TABLE [dbo].[IFC_INDEXES](
	[IDX_OID] [decimal](18, 0) NOT NULL,
	[IDX_NAME] [nvarchar](50) NULL,
	[IDX_UNIQUE] [decimal](1, 0) NULL,
	[IDX_LOCKSEQ] [decimal](9, 0) NULL,
	[IDX_TAB_OID] [decimal](18, 0) NULL,
	[IDX_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
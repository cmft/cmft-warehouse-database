﻿CREATE TABLE [dbo].[REP_FORMINFOS](
	[FOI_OID] [decimal](18, 0) NOT NULL,
	[FOI_NAME] [nvarchar](40) NULL,
	[FOI_LOCKSEQ] [decimal](9, 0) NULL,
	[FOI_ENT_OID] [decimal](18, 0) NULL,
	[FOI_COMMENT] [nvarchar](255) NULL,
	[FOI_JAVACONSTANT] [nvarchar](80) NULL,
	[FOI_TEM_OID] [decimal](18, 0) NULL,
	[FOI_WIDTH] [decimal](10, 0) NULL,
	[FOI_HEIGHT] [decimal](10, 0) NULL
) ON [PRIMARY]
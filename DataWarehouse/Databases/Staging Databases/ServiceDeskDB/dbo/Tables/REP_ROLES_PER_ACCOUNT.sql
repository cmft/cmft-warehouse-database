﻿CREATE TABLE [dbo].[REP_ROLES_PER_ACCOUNT](
	[RPA_OID] [decimal](18, 0) NOT NULL,
	[RPA_LOCKSEQ] [decimal](9, 0) NULL,
	[RPA_ACC_OID] [decimal](18, 0) NULL,
	[RPA_ROL_OID] [decimal](18, 0) NULL,
	[RPA_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
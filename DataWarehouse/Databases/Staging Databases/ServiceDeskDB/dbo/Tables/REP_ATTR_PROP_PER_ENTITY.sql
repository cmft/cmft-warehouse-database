﻿CREATE TABLE [dbo].[REP_ATTR_PROP_PER_ENTITY](
	[APE_ATR_OID] [decimal](18, 0) NULL,
	[APE_ENT_OID] [decimal](18, 0) NULL,
	[APE_OID] [decimal](18, 0) NOT NULL,
	[APE_LOCKSEQ] [decimal](9, 0) NULL,
	[APE_EXTENDLOOKUP] [decimal](1, 0) NULL,
	[APE_REQUIRED] [decimal](1, 0) NULL,
	[APE_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
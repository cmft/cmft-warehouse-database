﻿CREATE TABLE [dbo].[REP_EXPORT_VIEW_SET_ELEMENT](
	[EVE_EVS_OID] [decimal](18, 0) NULL,
	[EVE_LOCKSEQ] [decimal](9, 0) NULL,
	[EVE_OID] [decimal](18, 0) NOT NULL,
	[EVE_ORDERING] [decimal](10, 0) NULL,
	[EVE_REV_OID] [decimal](18, 0) NULL,
	[EVE_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
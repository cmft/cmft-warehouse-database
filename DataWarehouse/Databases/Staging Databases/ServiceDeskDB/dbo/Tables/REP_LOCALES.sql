﻿CREATE TABLE [dbo].[REP_LOCALES](
	[LNG_OID] [decimal](18, 0) NOT NULL,
	[LNG_SHORTTEXT] [nvarchar](40) NULL,
	[LNG_LOCKSEQ] [decimal](9, 0) NULL,
	[LNG_ID] [decimal](9, 0) NULL,
	[LNG_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_CODES_LOCALE](
	[CDL_LOCKSEQ] [decimal](9, 0) NULL,
	[CDL_NAME] [nvarchar](255) NULL,
	[CDL_OID] [decimal](18, 0) NOT NULL,
	[CDL_COD_OID] [decimal](18, 0) NULL,
	[CDL_LNG_OID] [decimal](18, 0) NULL,
	[CDL_REVISED] [decimal](1, 0) NULL,
	[CDL_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[CDL_CREATED] [datetime] NULL,
	[CDL_MODIFIED] [datetime] NULL,
	[CDL_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[CDL_ATS_OID] [decimal](18, 0) NULL,
	[CDL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
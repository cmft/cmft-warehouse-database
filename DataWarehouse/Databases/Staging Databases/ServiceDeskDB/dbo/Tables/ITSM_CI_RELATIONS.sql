﻿CREATE TABLE [dbo].[ITSM_CI_RELATIONS](
	[CIR_OID] [decimal](18, 0) NOT NULL,
	[CIR_LOCKSEQ] [decimal](9, 0) NULL,
	[CIR_CIFROM_OID] [decimal](18, 0) NULL,
	[CIR_CITO_OID] [decimal](18, 0) NULL,
	[CIR_RTY_OID] [decimal](18, 0) NULL,
	[CIR_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_ROLES_INCLUDE](
	[ROI_OID] [decimal](18, 0) NOT NULL,
	[ROI_LOCKSEQ] [decimal](9, 0) NULL,
	[ROI_ROL_PARENT] [decimal](18, 0) NULL,
	[ROI_ROL_CHILD] [decimal](18, 0) NULL,
	[ROI_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
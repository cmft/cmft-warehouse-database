﻿CREATE TABLE [dbo].[REP_LOAD_SETTINGS](
	[RLS_LOCKSEQ] [decimal](9, 0) NULL,
	[RLS_NAME] [nvarchar](50) NULL,
	[RLS_OID] [decimal](18, 0) NOT NULL,
	[RLS_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
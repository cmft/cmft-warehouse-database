﻿CREATE TABLE [dbo].[REP_TEMPLATES](
	[TEM_OID] [decimal](18, 0) NOT NULL,
	[TEM_LOCKSEQ] [decimal](9, 0) NULL,
	[TEM_NAME] [nvarchar](50) NULL,
	[TEM_DEFAULTTEMPLATE] [decimal](1, 0) NULL,
	[TEM_ENT_OID] [decimal](18, 0) NULL,
	[TEM_TEM_OID] [decimal](18, 0) NULL,
	[TEM_CAT_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
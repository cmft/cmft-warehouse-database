﻿CREATE TABLE [dbo].[REP_GENERAL_SETTINGS](
	[SET_OID] [decimal](18, 0) NOT NULL,
	[SET_LOCKSEQ] [decimal](9, 0) NULL,
	[SET_VALUE] [nvarchar](255) NULL,
	[SET_LBL_OID] [decimal](18, 0) NULL,
	[SET_TEM_OID] [decimal](18, 0) NULL,
	[SET_ATT_OID] [decimal](18, 0) NULL,
	[SET_MOD_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
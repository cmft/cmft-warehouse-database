﻿CREATE TABLE [dbo].[REP_PAGEATTRIBUTES](
	[PAA_GROUPBOXTOP] [decimal](1, 0) NULL,
	[PAA_GROUPBOXBOTTOM] [decimal](1, 0) NULL,
	[PAA_COMMENT] [nvarchar](255) NULL,
	[PAA_JAVACONSTANT] [nvarchar](80) NULL,
	[PAA_OID] [decimal](18, 0) NOT NULL,
	[PAA_ATTRIBUTE_INDEX] [decimal](10, 0) NULL,
	[PAA_LOCKSEQ] [decimal](9, 0) NULL,
	[PAA_PAI_OID] [decimal](18, 0) NULL,
	[PAA_ATR_OID] [decimal](18, 0) NULL,
	[PAA_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
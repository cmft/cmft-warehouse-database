﻿CREATE TABLE [dbo].[ITSM_PROBLEMS](
	[PRO_WAITFORWO] [decimal](1, 0) NULL,
	[SLC_PRO_PER_OID] [decimal](18, 0) NULL,
	[PRO_ATTACHMENT_EXISTS] [decimal](1, 0) NULL,
	[PRO_ASSIGN_REFERENCENUMBER] [nvarchar](50) NULL,
	[PRO_PLANFINISH] [datetime] NULL,
	[PRO_PLANSTART] [datetime] NULL,
	[PRO_EARLYFINISH] [datetime] NULL,
	[PRO_LATESTART] [datetime] NULL,
	[PRO_EARLYSTART] [datetime] NULL,
	[PRO_LATEFINISH] [datetime] NULL,
	[PRO_ASSIGN_EXTDEADLINE] [datetime] NULL,
	[PRO_ACTUALCOST] [decimal](12, 2) NULL,
	[PRO_PLANNEDCOST] [decimal](12, 2) NULL,
	[PRO_OID] [decimal](18, 0) NOT NULL,
	[PRO_LOCKSEQ] [decimal](9, 0) NULL,
	[PRO_WORKAROUND] [nvarchar](255) NULL,
	[PRO_DESCRIPTION] [nvarchar](80) NULL,
	[PRO_ACTUALSTART] [datetime] NULL,
	[PRO_ACTUALFINISH] [datetime] NULL,
	[PRO_ACTUALDURATION] [float] NULL,
	[PRO_DEADLINE] [datetime] NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[PRO_ID] [decimal](18, 0) NULL,
	[PRO_PLANDURATION] [float] NULL,
	[PRO_PRKNOWNERROR] [decimal](1, 0) NULL,
	[PRO_CLO_OID] [decimal](18, 0) NULL,
	[PRO_ASSIGN_PRIORITY] [decimal](18, 0) NULL,
	[PRO_TEM_OID] [decimal](18, 0) NULL,
	[ASS_WOG_OID] [decimal](18, 0) NULL,
	[ASS_PER_TO_OID] [decimal](18, 0) NULL,
	[REG_CREATED_BY_OID] [decimal](18, 0) NULL,
	[REG_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[PRO_STA_OID] [decimal](18, 0) NULL,
	[PRO_ASSIGN_STATUS] [decimal](18, 0) NULL,
	[PRO_IMP_OID] [decimal](18, 0) NULL,
	[PRO_PRI_OID] [decimal](18, 0) NULL,
	[PRO_CIT_OID] [decimal](18, 0) NULL,
	[PRO_POO_OID] [decimal](18, 0) NULL,
	[ASS_CONTR_OUT_ORG] [decimal](18, 0) NULL,
	[ASS_CONTR_OUT_PER] [decimal](18, 0) NULL,
	[PRO_CAT_OID] [decimal](18, 0) NULL,
	[PRO_CLA_OID] [decimal](18, 0) NULL,
	[PRO_MAXIMUMDURATION] [decimal](22, 0) NULL,
	[ASS_WOG_FROM_OID] [decimal](18, 0) NULL,
	[ASS_PER_FROM_OID] [decimal](18, 0) NULL,
	[PRO_SOURCEID] [nvarchar](80) NULL,
	[PRO_APT_STATUS] [decimal](18, 0) NULL,
	[PRO_APT_DEADLINE] [datetime] NULL,
	[PRO_APT_WOG_OID] [decimal](18, 0) NULL,
	[PRO_INITIATOR_PER_OID] [decimal](18, 0) NULL,
	[PRO_REQUESTOR_PER_OID] [decimal](18, 0) NULL,
	[PRO_APT_NROFAPPROVERSREQUIRED] [decimal](9, 0) NULL,
	[PRO_APT_NROFAPPROVERS] [decimal](9, 0) NULL,
	[PRO_APT_NROFAPPROVERSAPPROVED] [decimal](9, 0) NULL,
	[PRO_APT_DESCRIPTION] [nvarchar](80) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_WHATSTHIS](
	[WHS_COMMENT] [nvarchar](255) NULL,
	[WHS_OID] [decimal](18, 0) NOT NULL,
	[WHS_JAVA_CONSTANT] [nvarchar](80) NULL,
	[WHS_LOCKSEQ] [decimal](9, 0) NULL,
	[WHS_TEM_OID] [decimal](18, 0) NULL,
	[WHS_MOD_OID] [decimal](18, 0) NULL,
	[WHS_ADDTOENUMERATION] [decimal](1, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_SERVICE_CIS](
	[SCI_CIT_OID] [decimal](18, 0) NULL,
	[SCI_OID] [decimal](18, 0) NOT NULL,
	[SCI_SRV_OID] [decimal](18, 0) NULL,
	[SCI_LOCKSEQ] [decimal](9, 0) NULL,
	[SCI_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
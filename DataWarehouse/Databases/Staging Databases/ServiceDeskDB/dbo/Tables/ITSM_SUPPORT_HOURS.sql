﻿CREATE TABLE [dbo].[ITSM_SUPPORT_HOURS](
	[SHO_OID] [decimal](18, 0) NOT NULL,
	[SHO_TO] [float] NULL,
	[SHO_FROM] [float] NULL,
	[SHO_LOCKSEQ] [decimal](9, 0) NULL,
	[SHO_TEM_OID] [decimal](18, 0) NULL,
	[SHO_SEL_OID] [decimal](18, 0) NULL,
	[SHO_OSC_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
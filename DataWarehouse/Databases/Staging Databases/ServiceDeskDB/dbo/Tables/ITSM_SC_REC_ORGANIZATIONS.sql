﻿CREATE TABLE [dbo].[ITSM_SC_REC_ORGANIZATIONS](
	[SRO_SLA_OID] [decimal](18, 0) NULL,
	[SRO_LOCKSEQ] [decimal](9, 0) NULL,
	[SRO_ORG_OID] [decimal](18, 0) NULL,
	[SRO_OID] [decimal](18, 0) NOT NULL,
	[SRO_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
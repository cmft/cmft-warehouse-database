﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_SLA](
	[HSA_SLA_OID] [decimal](18, 0) NULL,
	[HSA_CRE_ACC_OID] [decimal](18, 0) NULL,
	[HSA_MOD_ACC_OID] [decimal](18, 0) NULL,
	[HSA_VALUE_TO] [decimal](18, 0) NULL,
	[HSA_VALUE_FROM] [decimal](18, 0) NULL,
	[HSA_SYSTEM] [decimal](1, 0) NULL,
	[HSA_SUBJECT] [nvarchar](255) NULL,
	[HSA_OID] [decimal](18, 0) NOT NULL,
	[HSA_MODIFIED] [datetime] NULL,
	[HSA_CREATED] [datetime] NULL,
	[HSA_LOCKSEQ] [decimal](9, 0) NULL,
	[HSA_ATTRIBUTE] [decimal](18, 0) NULL,
	[HSA_TEM_OID] [decimal](18, 0) NULL,
	[HSA_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
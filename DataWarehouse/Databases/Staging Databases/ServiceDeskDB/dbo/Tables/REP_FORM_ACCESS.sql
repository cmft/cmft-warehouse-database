﻿CREATE TABLE [dbo].[REP_FORM_ACCESS](
	[FAC_OID] [decimal](18, 0) NOT NULL,
	[FAC_LOCKSEQ] [decimal](9, 0) NULL,
	[FAC_ROL_OID] [decimal](18, 0) NULL,
	[FAC_FOI_OID] [decimal](18, 0) NULL,
	[FAC_DEFAULTFORM] [decimal](1, 0) NULL,
	[FAC_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
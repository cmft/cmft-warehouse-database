﻿CREATE TABLE [dbo].[ITSM_SERVICE_LEVEL](
	[SEL_NAME] [nvarchar](50) NULL,
	[SEL_LOCKSEQ] [decimal](9, 0) NULL,
	[SEL_OID] [decimal](18, 0) NOT NULL,
	[SEL_DESCRIPTION] [nvarchar](80) NULL,
	[SEL_DEFAULT] [decimal](1, 0) NULL,
	[SEL_ICON] [nvarchar](256) NULL,
	[SEL_TEM_OID] [decimal](18, 0) NULL,
	[SEL_BLOCKED] [decimal](1, 0) NULL,
	[SEL_WEIGHT] [decimal](10, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_TIME_ZONES](
	[TMZ_LOCKSEQ] [decimal](9, 0) NULL,
	[TMZ_ADD] [decimal](1, 0) NULL,
	[TMZ_SEARCHCODE] [nvarchar](20) NULL,
	[TMZ_OID] [decimal](18, 0) NOT NULL,
	[TMZ_NAME] [nvarchar](50) NULL,
	[TMZ_OFFSET] [float] NULL,
	[TMZ_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
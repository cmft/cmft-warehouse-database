﻿CREATE TABLE [dbo].[REP_EXTERNAL_ATTRIBUTES](
	[REA_DEFAULT] [nvarchar](255) NULL,
	[REA_FILTERINFO] [nvarchar](4000) NULL,
	[REA_SEA_ATR_OID] [decimal](18, 0) NULL,
	[REA_OID] [decimal](18, 0) NOT NULL,
	[REA_REE_OID] [decimal](18, 0) NULL,
	[REA_PRIMARYKEY] [decimal](1, 0) NULL,
	[REA_LOCKSEQ] [decimal](9, 0) NULL,
	[REA_NAME] [nvarchar](50) NULL,
	[REA_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_ACTIONS](
	[ACT_OID] [decimal](18, 0) NOT NULL,
	[ACT_LOCKSEQ] [decimal](9, 0) NULL,
	[ACT_DESCRIPTION] [nvarchar](255) NULL,
	[ACT_DISABLED] [decimal](1, 0) NULL,
	[OVA_ACT_VIEWREADONLY] [decimal](1, 0) NULL,
	[ACT_ENT_OID] [decimal](18, 0) NULL,
	[OVA_ACT_VIW_OID] [decimal](18, 0) NULL,
	[SMA_ACT_APP_OID] [decimal](18, 0) NULL,
	[ACT_SUBTYPE] [decimal](18, 0) NULL,
	[ACT_JAVACONSTANT] [nvarchar](80) NULL,
	[ACT_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
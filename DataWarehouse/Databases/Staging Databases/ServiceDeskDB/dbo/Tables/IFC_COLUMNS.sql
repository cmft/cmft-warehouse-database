﻿CREATE TABLE [dbo].[IFC_COLUMNS](
	[COL_OID] [decimal](18, 0) NOT NULL,
	[COL_NAME] [nvarchar](30) NULL,
	[COL_LOCKSEQ] [decimal](9, 0) NULL,
	[COL_PRIMARYKEY] [decimal](1, 0) NULL,
	[COL_TAB_OID] [decimal](18, 0) NULL,
	[COL_ATR_OID] [decimal](18, 0) NULL,
	[COL_ENT_REF] [decimal](18, 0) NULL,
	[REL_COL_TO] [decimal](18, 0) NULL,
	[COL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REP_EXTERNATTR_MAPPINGS](
	[REM_OID] [decimal](18, 0) NOT NULL,
	[REM_REA_OID] [decimal](18, 0) NULL,
	[REM_LOCKSEQ] [decimal](9, 0) NULL,
	[REM_ATR_OID] [decimal](18, 0) NULL,
	[REM_SEQUENCE] [decimal](2, 0) NULL,
	[REM_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
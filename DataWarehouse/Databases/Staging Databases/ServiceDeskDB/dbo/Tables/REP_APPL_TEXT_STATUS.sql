﻿CREATE TABLE [dbo].[REP_APPL_TEXT_STATUS](
	[ATS_OID] [decimal](18, 0) NOT NULL,
	[ATS_LOCKSEQ] [decimal](9, 0) NULL,
	[ATS_JAVACONSTANT] [nvarchar](40) NULL,
	[ATS_COMMENT] [nvarchar](255) NULL,
	[ATS_ORDERING] [decimal](10, 0) NULL,
	[ATS_STATUSNEW] [decimal](1, 0) NULL,
	[ATS_STATUSREAD] [decimal](1, 0) NULL,
	[ATS_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
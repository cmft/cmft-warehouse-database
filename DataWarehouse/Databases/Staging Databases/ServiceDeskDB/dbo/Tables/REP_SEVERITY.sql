﻿CREATE TABLE [dbo].[REP_SEVERITY](
	[SEV_OID] [decimal](18, 0) NOT NULL,
	[SEV_LOCKSEQ] [decimal](9, 0) NULL,
	[SEV_NAME] [nvarchar](50) NULL,
	[SEV_ICON] [nvarchar](256) NULL,
	[SEV_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
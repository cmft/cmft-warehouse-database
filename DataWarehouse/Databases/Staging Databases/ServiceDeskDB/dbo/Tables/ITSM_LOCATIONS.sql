﻿CREATE TABLE [dbo].[ITSM_LOCATIONS](
	[LOC_OID] [decimal](18, 0) NOT NULL,
	[LOC_SEARCHCODE] [nvarchar](50) NULL,
	[LOC_REMARK] [nvarchar](255) NULL,
	[LOC_CAT_OID] [decimal](18, 0) NULL,
	[LOC_PARENT] [decimal](18, 0) NULL,
	[LOC_LOCKSEQ] [decimal](9, 0) NULL,
	[LOC_NOTSELECTABLE] [decimal](1, 0) NULL,
	[LOC_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
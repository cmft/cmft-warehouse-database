﻿CREATE TABLE [dbo].[REP_OVERVIEW_CONDITIONS](
	[ROC_OID] [decimal](18, 0) NOT NULL,
	[ROC_LOCKSEQ] [decimal](9, 0) NULL,
	[ROC_OPERATOR] [decimal](10, 0) NULL,
	[ROC_FROMATR1_OID] [decimal](18, 0) NULL,
	[ROC_FROMATR2_OID] [decimal](18, 0) NULL,
	[ROC_FROMATR3_OID] [decimal](18, 0) NULL,
	[ROC_TOATR1_OID] [decimal](18, 0) NULL,
	[ROC_TOATR2_OID] [decimal](18, 0) NULL,
	[ROC_TOATR3_OID] [decimal](18, 0) NULL,
	[ROC_ACT_OID] [decimal](18, 0) NULL,
	[ROC_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
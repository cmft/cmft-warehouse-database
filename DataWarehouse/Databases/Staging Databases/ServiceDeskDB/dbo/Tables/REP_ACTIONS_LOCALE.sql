﻿CREATE TABLE [dbo].[REP_ACTIONS_LOCALE](
	[ACL_CREATED_BY_ACC_OID] [decimal](18, 0) NULL,
	[ACL_MODIFIED_BY_ACC_OID] [decimal](18, 0) NULL,
	[ACL_LNG_OID] [decimal](18, 0) NULL,
	[ACL_ATS_OID] [decimal](18, 0) NULL,
	[ACL_ACT_OID] [decimal](18, 0) NULL,
	[ACL_CREATED] [datetime] NULL,
	[ACL_MODIFIED] [datetime] NULL,
	[ACL_OID] [decimal](18, 0) NOT NULL,
	[ACL_LOCKSEQ] [decimal](9, 0) NULL,
	[ACL_TEXT] [nvarchar](255) NULL,
	[ACL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
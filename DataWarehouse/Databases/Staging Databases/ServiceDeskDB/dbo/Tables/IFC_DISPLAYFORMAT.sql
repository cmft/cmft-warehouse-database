﻿CREATE TABLE [dbo].[IFC_DISPLAYFORMAT](
	[DIF_OID] [decimal](18, 0) NOT NULL,
	[DIF_LOCKSEQ] [decimal](9, 0) NULL,
	[DIF_NAME] [nvarchar](50) NULL,
	[DIF_ID] [decimal](18, 0) NULL,
	[DIF_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
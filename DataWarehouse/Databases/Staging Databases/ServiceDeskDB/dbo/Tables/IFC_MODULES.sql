﻿CREATE TABLE [dbo].[IFC_MODULES](
	[MOD_OID] [decimal](18, 0) NOT NULL,
	[MOD_JAVAPACKAGE] [nvarchar](40) NULL,
	[MOD_LOCKSEQ] [decimal](9, 0) NULL,
	[MOD_NAME] [nvarchar](50) NULL,
	[MOD_TEM_OID] [decimal](18, 0) NULL,
	[MOD_SUBFOLDERNAME] [nvarchar](40) NULL,
	[MOD_VERSION] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[IFC_INDEX_MEMOS](
	[IDM_COMMENT] [nvarchar](4000) NULL,
	[IDM_OID] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
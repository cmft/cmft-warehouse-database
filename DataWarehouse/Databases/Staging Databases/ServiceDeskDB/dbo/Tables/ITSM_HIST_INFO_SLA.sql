﻿CREATE TABLE [dbo].[ITSM_HIST_INFO_SLA](
	[HIA_HSA_OID] [decimal](18, 0) NOT NULL,
	[HIA_INFORMATION] [nvarchar](4000) NULL
) ON [PRIMARY]
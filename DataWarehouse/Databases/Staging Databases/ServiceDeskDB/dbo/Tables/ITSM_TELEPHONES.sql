﻿CREATE TABLE [dbo].[ITSM_TELEPHONES](
	[TEL_LOCKSEQ] [decimal](9, 0) NULL,
	[TEL_OID] [decimal](18, 0) NOT NULL,
	[TEL_NUMBER] [nvarchar](30) NULL,
	[TEL_TYP_OID] [decimal](18, 0) NULL,
	[TEL_ORG_OID] [decimal](18, 0) NULL,
	[TEL_PER_OID] [decimal](18, 0) NULL,
	[TEL_TEM_OID] [decimal](18, 0) NULL,
	[TEL_PRIMARY] [decimal](1, 0) NULL
) ON [PRIMARY]
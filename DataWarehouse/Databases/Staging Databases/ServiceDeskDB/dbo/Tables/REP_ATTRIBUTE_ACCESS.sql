﻿CREATE TABLE [dbo].[REP_ATTRIBUTE_ACCESS](
	[ATA_OID] [decimal](18, 0) NOT NULL,
	[ATA_MODIFY] [decimal](1, 0) NULL,
	[ATA_LOCKSEQ] [decimal](9, 0) NULL,
	[ATA_ENA_OID] [decimal](18, 0) NULL,
	[ATA_ATR_OID] [decimal](18, 0) NULL,
	[ATA_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
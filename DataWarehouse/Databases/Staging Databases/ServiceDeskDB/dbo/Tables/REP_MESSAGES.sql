﻿CREATE TABLE [dbo].[REP_MESSAGES](
	[MES_OID] [decimal](18, 0) NOT NULL,
	[MES_COMMENT] [nvarchar](255) NULL,
	[MES_SHOWHELP] [decimal](1, 0) NULL,
	[MES_LOCKSEQ] [decimal](9, 0) NULL,
	[MES_JAVA_CONSTANT] [nvarchar](80) NULL,
	[MES_SEV_OID] [decimal](18, 0) NULL,
	[MES_TEM_OID] [decimal](18, 0) NULL,
	[MES_MOD_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_SERVICECALL](
	[HSC_SPENTTIME] [float] NULL,
	[HSC_OID] [decimal](18, 0) NOT NULL,
	[HSC_LOCKSEQ] [decimal](9, 0) NULL,
	[HSC_CREATED] [datetime] NULL,
	[HSC_MODIFIED] [datetime] NULL,
	[HSC_SYSTEM] [decimal](1, 0) NULL,
	[HSC_VALUEFROM] [decimal](18, 0) NULL,
	[HSC_VALUETO] [decimal](18, 0) NULL,
	[HSC_SUBJECT] [nvarchar](255) NULL,
	[HSC_SER_OID] [decimal](18, 0) NULL,
	[REG_CREATEDBY_OID] [decimal](18, 0) NULL,
	[REG_MODIFIEDBY_OID] [decimal](18, 0) NULL,
	[HSC_VALUEATR_OID] [decimal](18, 0) NULL,
	[HSC_TEM_OID] [decimal](18, 0) NULL,
	[HSC_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
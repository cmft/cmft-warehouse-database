﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_CHANGE](
	[HCH_OID] [decimal](18, 0) NOT NULL,
	[HCH_LOCKSEQ] [decimal](9, 0) NULL,
	[REG_CREATED] [datetime] NULL,
	[REG_MODIFIED] [datetime] NULL,
	[HCH_SYSTEM] [decimal](1, 0) NULL,
	[HCH_VALUEFROM] [decimal](18, 0) NULL,
	[HCH_VALUETO] [decimal](18, 0) NULL,
	[HCH_VALUEATR_OID] [decimal](18, 0) NULL,
	[HCH_SUBJECT] [nvarchar](255) NULL,
	[HCH_CHA_OID] [decimal](18, 0) NULL,
	[REG_CREATED_BY_OID] [decimal](18, 0) NULL,
	[REG_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[HCH_TEM_OID] [decimal](18, 0) NULL,
	[HCH_SPENTTIME] [float] NULL,
	[HCH_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_CICOMPONENTS](
	[CIC_OID] [decimal](18, 0) NOT NULL,
	[CIC_LOCKSEQ] [decimal](9, 0) NULL,
	[CIC_CITA_OID] [decimal](18, 0) NULL,
	[CIC_TEM_OID] [decimal](18, 0) NULL,
	[CIC_CITB_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
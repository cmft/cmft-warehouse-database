﻿CREATE TABLE [dbo].[IFC_RDBMS](
	[RDB_OID] [decimal](18, 0) NOT NULL,
	[RDB_LOCKSEQ] [decimal](9, 0) NULL,
	[RDB_NAME] [nvarchar](50) NULL,
	[RDB_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
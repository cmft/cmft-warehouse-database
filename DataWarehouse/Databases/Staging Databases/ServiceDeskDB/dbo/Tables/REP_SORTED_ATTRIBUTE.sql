﻿CREATE TABLE [dbo].[REP_SORTED_ATTRIBUTE](
	[RSA_OID] [decimal](18, 0) NOT NULL,
	[RSA_LOCKSEQ] [decimal](9, 0) NULL,
	[RSA_ORDERING] [decimal](10, 0) NULL,
	[RSA_SUBTYPE] [decimal](18, 0) NULL,
	[RSA_RCI_OID] [decimal](18, 0) NULL,
	[RSA_ATR_OID] [decimal](18, 0) NULL,
	[RSA_CON_OID] [decimal](18, 0) NULL,
	[RSA_SAP_OID] [decimal](18, 0) NULL,
	[RSA_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
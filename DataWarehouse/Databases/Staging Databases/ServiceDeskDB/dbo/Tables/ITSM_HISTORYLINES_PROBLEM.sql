﻿CREATE TABLE [dbo].[ITSM_HISTORYLINES_PROBLEM](
	[HPR_SPENTTIME] [float] NULL,
	[REG_CREATED_BY_OID] [decimal](18, 0) NULL,
	[REG_MODIFIED_BY_OID] [decimal](18, 0) NULL,
	[HPR_TEM_OID] [decimal](18, 0) NULL,
	[HPR_OID] [decimal](18, 0) NOT NULL,
	[HPR_LOCKSEQ] [decimal](9, 0) NULL,
	[HPR_CREATED] [datetime] NULL,
	[HPR_MODIFIED] [datetime] NULL,
	[HPR_SYSTEM] [decimal](1, 0) NULL,
	[HPR_VALUEFROM] [decimal](18, 0) NULL,
	[HPR_VALUETO] [decimal](18, 0) NULL,
	[HPR_SUBJECT] [nvarchar](255) NULL,
	[HPR_VALUEATR_OID] [decimal](18, 0) NULL,
	[HPR_PRO_OID] [decimal](18, 0) NULL,
	[HPR_NEWVALUE] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
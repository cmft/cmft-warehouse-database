﻿CREATE TABLE [dbo].[ITSM_HOLIDAYS](
	[HOL_DATE] [datetime] NULL,
	[HOL_HCR_OID] [decimal](18, 0) NULL,
	[HOL_LOCKSEQ] [decimal](9, 0) NULL,
	[HOL_NAME] [nvarchar](50) NULL,
	[HOL_OID] [decimal](18, 0) NOT NULL,
	[HOL_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
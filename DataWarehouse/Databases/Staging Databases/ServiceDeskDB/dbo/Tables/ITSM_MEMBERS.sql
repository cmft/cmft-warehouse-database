﻿CREATE TABLE [dbo].[ITSM_MEMBERS](
	[MEM_LOCKSEQ] [decimal](9, 0) NULL,
	[MEM_PER_OID] [decimal](18, 0) NULL,
	[MEM_OID] [decimal](18, 0) NOT NULL,
	[MEM_WOG_OID] [decimal](18, 0) NULL,
	[MEM_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ITSM_SC_REC_PERSONS](
	[SRP_SLA_OID] [decimal](18, 0) NULL,
	[SRP_PER_OID] [decimal](18, 0) NULL,
	[SRP_LOCKSEQ] [decimal](9, 0) NULL,
	[SRP_OID] [decimal](18, 0) NOT NULL,
	[SRP_TEM_OID] [decimal](18, 0) NULL
) ON [PRIMARY]
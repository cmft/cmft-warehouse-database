﻿CREATE TABLE [dbo].[IFC_AUDITLOGS](
	[aul_created] [datetime] NULL,
	[aul_lockseq] [decimal](9, 0) NULL,
	[aul_logentry] [nvarchar](4000) NULL,
	[aul_loginkey] [nvarchar](40) NULL,
	[aul_loginname] [nvarchar](40) NULL,
	[aul_oid] [decimal](18, 0) NOT NULL,
	[aul_rcd_oid] [decimal](18, 0) NULL,
	[aul_target] [nvarchar](40) NULL,
	[aul_tem_oid] [decimal](18, 0) NULL
) ON [PRIMARY]
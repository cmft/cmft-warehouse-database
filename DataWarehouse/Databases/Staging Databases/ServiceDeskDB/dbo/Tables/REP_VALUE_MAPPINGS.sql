﻿CREATE TABLE [dbo].[REP_VALUE_MAPPINGS](
	[RVM_REA_OID] [decimal](18, 0) NULL,
	[RVM_OID] [decimal](18, 0) NOT NULL,
	[RVM_EXT_VALUE] [nvarchar](255) NULL,
	[RVM_LOCKSEQ] [decimal](9, 0) NULL,
	[RVM_TEM_OID] [decimal](18, 0) NULL,
	[RVM_VALUE] [decimal](18, 0) NULL
) ON [PRIMARY]
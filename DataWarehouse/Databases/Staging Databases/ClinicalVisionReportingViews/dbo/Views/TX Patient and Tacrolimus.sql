﻿CREATE VIEW [dbo].[TX Patient and Tacrolimus]
AS
SELECT     dbo.RenalPatientActive.LastName, dbo.RenalPatientActive.MedicalRecordNo, dbo.RenalPatientActive.FirstName, dbo.RenalPatientActive.DateOfBirth_Local, 
                      dbo.RenalPatientActive.Age, dbo.RenalPatientActive.Sex, dbo.PatientLabPanelToxicology.LabTestDate_Local, dbo.RenalPatientActive.ObjectID, 
                      dbo.PatientLabPanelToxicology.PatientObjectID, dbo.PatientLabPanelToxicology.TacrolimusNum
FROM         dbo.RenalPatientActive INNER JOIN
                      dbo.PatientLabPanelToxicology ON dbo.RenalPatientActive.ObjectID = dbo.PatientLabPanelToxicology.PatientObjectID
WHERE     (dbo.PatientLabPanelToxicology.LabTestDate_Local BETWEEN CONVERT(DATETIME, '2012-07-16 00:00:00', 102) AND CONVERT(DATETIME, '2013-07-16 00:00:00', 
                      102)) AND (dbo.PatientLabPanelToxicology.TacrolimusNum < 4)
﻿CREATE VIEW [dbo].[URR]
AS
SELECT     dbo.RenalPatientActive.ObjectID, dbo.RenalPatientActive.FullName, dbo.RenalPatientActive.MedicalRecordNo, 
                      dbo.PatientLabPanelRenalAdequacy.LabTestDate_Local, dbo.PatientLabPanelRenalAdequacy.URRNum, dbo.RenalPatientActive.PrimaryRenalModality
FROM         dbo.RenalPatientActive INNER JOIN
                      dbo.PatientLabPanelRenalAdequacy ON dbo.RenalPatientActive.ObjectID = dbo.PatientLabPanelRenalAdequacy.PatientObjectID
WHERE     (dbo.PatientLabPanelRenalAdequacy.LabTestDate_Local BETWEEN CONVERT(DATETIME, '2013-02-01 00:00:00', 102) AND CONVERT(DATETIME, 
                      '2013-07-16 00:00:00', 102)) AND (dbo.RenalPatientActive.PrimaryRenalModality = N'hd') AND (NOT (dbo.PatientLabPanelRenalAdequacy.URRNum IS NULL))
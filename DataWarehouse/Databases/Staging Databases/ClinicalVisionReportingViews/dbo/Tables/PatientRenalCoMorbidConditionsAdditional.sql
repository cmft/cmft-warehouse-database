﻿CREATE TABLE [dbo].[PatientRenalCoMorbidConditionsAdditional](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[RenalRegistrationObjectID] [int] NOT NULL,
	[Condition] [nvarchar](256) NULL,
	[Found] [nvarchar](256) NULL
) ON [PRIMARY]
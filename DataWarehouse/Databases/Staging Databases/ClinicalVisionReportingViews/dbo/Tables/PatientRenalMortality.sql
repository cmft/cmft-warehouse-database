﻿CREATE TABLE [dbo].[PatientRenalMortality](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[ESRDCauseOfDeath] [nvarchar](256) NULL,
	[RRTAtDeath] [nvarchar](256) NULL,
	[RenalTransplant] [nvarchar](256) NULL,
	[DateOfLastTransplant_Local] [datetime] NULL,
	[KidneyFunctioningAtDeath] [nvarchar](256) NULL,
	[RRTResumed] [nvarchar](256) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
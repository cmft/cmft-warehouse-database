﻿CREATE TABLE [dbo].[TransplantReportGroup](
	[ObjectID] [int] NOT NULL,
	[Name] [nvarchar](40) NULL,
	[Description] [nvarchar](255) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
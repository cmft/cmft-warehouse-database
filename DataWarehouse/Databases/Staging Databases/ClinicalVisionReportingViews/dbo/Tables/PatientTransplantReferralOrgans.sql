﻿CREATE TABLE [dbo].[PatientTransplantReferralOrgans](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[ReferralObjectID] [int] NOT NULL,
	[Organ] [nvarchar](256) NULL,
	[Relationship] [nvarchar](256) NULL,
	[OtherRelationship] [nvarchar](100) NULL,
	[ConfirmedBy] [nvarchar](40) NULL,
	[HTANumber] [nvarchar](6) NULL,
	[Provider] [nvarchar](100) NULL
) ON [PRIMARY]
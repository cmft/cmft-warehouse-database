﻿CREATE TABLE [dbo].[Dialyzer](
	[ObjectID] [int] NOT NULL,
	[Name] [nvarchar](40) NULL,
	[AllowReuse] [tinyint] NULL,
	[MaxUses] [int] NULL,
	[CutOffPercent] [int] NULL,
	[Volume] [float] NULL,
	[VolumeUnits] [nvarchar](256) NULL,
	[VolumeInLiters] [float] NULL,
	[VendorName] [nvarchar](40) NULL,
	[VendorObjectID] [int] NULL
) ON [PRIMARY]
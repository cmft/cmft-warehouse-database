﻿CREATE TABLE [dbo].[ServiceMasterChargeCode](
	[ObjectID] [int] NOT NULL,
	[ServiceMasterObjectID] [int] NULL,
	[ServiceMasterOrderCode] [nvarchar](50) NULL,
	[ServiceMasterName] [nvarchar](4000) NULL,
	[ServiceMasterDescription_RTF] [nvarchar](max) NULL,
	[ServiceMasterDescription_HTML] [nvarchar](max) NULL,
	[ChargeMasterObjectID] [int] NULL,
	[ChargeMasterCode] [nvarchar](50) NULL,
	[ChargeMasterDescription] [nvarchar](40) NULL,
	[ChargeMasterBillingInterface] [nvarchar](256) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[WhenToCharge] [nvarchar](256) NULL,
	[DaysOfWeek] [nvarchar](33) NOT NULL,
	[SupplySource] [nvarchar](256) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
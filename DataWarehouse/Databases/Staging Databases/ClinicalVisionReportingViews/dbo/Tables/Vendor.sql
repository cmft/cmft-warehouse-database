﻿CREATE TABLE [dbo].[Vendor](
	[ObjectID] [int] NOT NULL,
	[Name] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL,
	[ProductLines] [nvarchar](40) NULL,
	[MailingAddress] [nvarchar](1000) NULL,
	[PrimaryPhone] [nvarchar](156) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientSkinTests](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Date_Local] [datetime] NULL,
	[SkinTest] [nvarchar](256) NULL,
	[Reason] [nvarchar](256) NULL,
	[Reaction] [nvarchar](256) NULL,
	[InformationSource] [nvarchar](256) NULL,
	[Notes] [nvarchar](max) NULL,
	[StaffObjectID] [int] NULL,
	[StaffName] [nvarchar](40) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
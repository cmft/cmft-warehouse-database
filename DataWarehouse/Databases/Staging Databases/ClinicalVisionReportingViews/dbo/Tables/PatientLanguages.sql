﻿CREATE TABLE [dbo].[PatientLanguages](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NOT NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[LanguageName] [nvarchar](256) NULL
) ON [PRIMARY]
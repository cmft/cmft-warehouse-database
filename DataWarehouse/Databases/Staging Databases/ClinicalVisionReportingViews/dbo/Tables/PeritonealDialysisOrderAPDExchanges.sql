﻿CREATE TABLE [dbo].[PeritonealDialysisOrderAPDExchanges](
	[PeritonealDialysisObjectID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[ExchangeNo] [int] NULL,
	[Solution] [nvarchar](256) NULL,
	[NumberOfBags] [int] NULL,
	[Volume] [float] NULL,
	[VolumeUnits] [nvarchar](256) NULL,
	[VolumeInLiters] [float] NULL,
	[TimeOfDay_Local] [datetime] NULL
) ON [PRIMARY]
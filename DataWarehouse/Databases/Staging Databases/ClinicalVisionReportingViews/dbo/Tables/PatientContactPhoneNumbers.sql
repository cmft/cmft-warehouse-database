﻿CREATE TABLE [dbo].[PatientContactPhoneNumbers](
	[ObjectID] [int] NULL,
	[PatientContactObjectID] [int] NOT NULL,
	[PatientContactFullName] [nvarchar](607) NULL,
	[AreaCityCode] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Extension] [nvarchar](50) NULL,
	[Comment] [nvarchar](40) NULL,
	[UseCode] [nvarchar](256) NULL,
	[IsPrimary] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicService_Observation](
	[FollowupServiceObjectID] [int] NULL,
	[OfficeVisitServiceObjectID] [int] NULL,
	[WorkupProtocolServiceObjectID] [int] NULL,
	[ObservationObjectID] [int] NOT NULL
) ON [PRIMARY]
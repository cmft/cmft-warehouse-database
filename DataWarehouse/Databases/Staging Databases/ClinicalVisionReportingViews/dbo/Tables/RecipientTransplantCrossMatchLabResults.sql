﻿CREATE TABLE [dbo].[RecipientTransplantCrossMatchLabResults](
	[ObjectID] [int] NOT NULL,
	[CrossMatchTransplantEventObjectID] [int] NULL,
	[ResultName] [nvarchar](256) NULL,
	[RecipientDate_Local] [datetime] NULL,
	[RecipientResult] [nvarchar](50) NULL,
	[DonorDate_Local] [datetime] NULL,
	[DonorResult] [nvarchar](50) NULL
) ON [PRIMARY]
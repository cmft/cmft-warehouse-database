﻿CREATE TABLE [dbo].[PatientLabPanelHistologyAndMicrobiology](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[LabTestObjectID] [int] NULL,
	[LabTestDate_Local] [datetime] NULL,
	[LabTestQualifier] [nvarchar](256) NULL,
	[LabTestStatus] [nvarchar](256) NULL,
	[HistologyNum] [float] NULL,
	[HistologyStr] [nvarchar](50) NULL,
	[HistologyFlg] [nvarchar](20) NULL
) ON [PRIMARY]
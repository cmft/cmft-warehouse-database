﻿CREATE TABLE [dbo].[DietaryOrderParenteralDietAdditives](
	[DietaryOrderObjectID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[DietAdditive] [nvarchar](256) NULL,
	[AdditiveDose] [float] NULL,
	[AdditiveDoseUnits] [nvarchar](256) NULL,
	[AdditiveDosage] [nvarchar](100) NULL
) ON [PRIMARY]
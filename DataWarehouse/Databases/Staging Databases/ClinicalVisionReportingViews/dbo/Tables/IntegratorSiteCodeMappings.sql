﻿CREATE TABLE [dbo].[IntegratorSiteCodeMappings](
	[ObjectID] [int] NOT NULL,
	[FieldValue] [nvarchar](100) NULL,
	[CVEnumCode] [nvarchar](256) NULL,
	[CVEnum] [nvarchar](256) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientKidneyTransplantStatuses](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NOT NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[DaysElapsed] [int] NULL,
	[AgeAtEvent] [numeric](18, 0) NULL,
	[TimelineEvent] [nvarchar](256) NULL,
	[TimelineEventDetail] [nvarchar](256) NULL,
	[InformationSource] [nvarchar](256) NULL,
	[Notes] [nvarchar](max) NULL,
	[TransplantType] [nvarchar](256) NULL,
	[TransplantOrgan] [nvarchar](256) NULL,
	[IsPrimary] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
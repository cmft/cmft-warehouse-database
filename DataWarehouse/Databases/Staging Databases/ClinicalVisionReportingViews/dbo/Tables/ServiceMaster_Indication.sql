﻿CREATE TABLE [dbo].[ServiceMaster_Indication](
	[ServiceMasterObjectID] [int] NOT NULL,
	[ProblemDiagnosisObjectID] [int] NULL,
	[MedicalDiagnosis] [nvarchar](255) NULL,
	[MedicalDiagnosisCode] [nvarchar](1000) NULL,
	[DiagnosisMedicalSpecialty] [nvarchar](256) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientObservationEvaluations](
	[ObjectID] [int] NOT NULL,
	[ObservationComponentObjectID] [int] NULL,
	[Evaluation] [nvarchar](256) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[AppointmentBookPages](
	[ObjectID] [int] NOT NULL,
	[AppointmentBookObjectID] [int] NULL,
	[PageDate_Local] [datetime] NULL,
	[DayOfWeek] [nvarchar](9) NOT NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Equipment](
	[ObjectID] [int] NOT NULL,
	[Name] [nvarchar](40) NULL,
	[SerialNumber] [nvarchar](20) NULL,
	[EquipmentType] [nvarchar](256) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[CurrentLocation] [nvarchar](40) NULL
) ON [PRIMARY]
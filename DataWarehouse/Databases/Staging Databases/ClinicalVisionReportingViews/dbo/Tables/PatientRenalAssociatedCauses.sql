﻿CREATE TABLE [dbo].[PatientRenalAssociatedCauses](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[RenalRegistrationObjectID] [int] NOT NULL,
	[AssociatedCause] [nvarchar](256) NULL
) ON [PRIMARY]
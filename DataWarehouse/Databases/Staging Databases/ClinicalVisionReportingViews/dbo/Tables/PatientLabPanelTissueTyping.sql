﻿CREATE TABLE [dbo].[PatientLabPanelTissueTyping](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[LabTestObjectID] [int] NULL,
	[LabTestDate_Local] [datetime] NULL,
	[LabTestQualifier] [nvarchar](256) NULL,
	[LabTestStatus] [nvarchar](256) NULL,
	[BloodTypeNum] [float] NULL,
	[BloodTypeStr] [nvarchar](50) NULL,
	[BloodTypeFlg] [nvarchar](20) NULL,
	[RhNum] [float] NULL,
	[RhStr] [nvarchar](50) NULL,
	[RhFlg] [nvarchar](20) NULL,
	[HLAANum] [float] NULL,
	[HLAAStr] [nvarchar](50) NULL,
	[HLAAFlg] [nvarchar](20) NULL,
	[HLABNum] [float] NULL,
	[HLABStr] [nvarchar](50) NULL,
	[HLABFlg] [nvarchar](20) NULL,
	[HLABwNum] [float] NULL,
	[HLABwStr] [nvarchar](50) NULL,
	[HLABwFlg] [nvarchar](20) NULL,
	[HLACwNum] [float] NULL,
	[HLACwStr] [nvarchar](50) NULL,
	[HLACwFlg] [nvarchar](20) NULL,
	[HLADrNum] [float] NULL,
	[HLADrStr] [nvarchar](50) NULL,
	[HLADrFlg] [nvarchar](20) NULL,
	[HLADqNum] [float] NULL,
	[HLADqStr] [nvarchar](50) NULL,
	[HLADqFlg] [nvarchar](20) NULL,
	[PercentTCellsNum] [float] NULL,
	[PercentTCellsStr] [nvarchar](50) NULL,
	[PercentTCellsFlg] [nvarchar](20) NULL,
	[PercentBCellsNum] [float] NULL,
	[PercentBCellsStr] [nvarchar](50) NULL,
	[PercentBCellsFlg] [nvarchar](20) NULL,
	[CMVNum] [float] NULL,
	[CMVStr] [nvarchar](50) NULL,
	[CMVFlg] [nvarchar](20) NULL,
	[HIVNum] [float] NULL,
	[HIVStr] [nvarchar](50) NULL,
	[HIVFlg] [nvarchar](20) NULL,
	[IncompatibilityNum] [float] NULL,
	[IncompatibilityStr] [nvarchar](50) NULL,
	[IncompatibilityFlg] [nvarchar](20) NULL,
	[DirectCoombsNum] [float] NULL,
	[DirectCoombsStr] [nvarchar](50) NULL,
	[DirectCoombsFlg] [nvarchar](20) NULL,
	[IndirectCoombsNum] [float] NULL,
	[IndirectCoombsStr] [nvarchar](50) NULL,
	[IndirectCoombsFlg] [nvarchar](20) NULL,
	[PRANum] [float] NULL,
	[PRAStr] [nvarchar](50) NULL,
	[PRAFlg] [nvarchar](20) NULL
) ON [PRIMARY]
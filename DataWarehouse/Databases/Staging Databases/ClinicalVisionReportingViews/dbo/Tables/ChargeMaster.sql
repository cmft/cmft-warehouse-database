﻿CREATE TABLE [dbo].[ChargeMaster](
	[ObjectID] [int] NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Description] [nvarchar](40) NULL,
	[ProcedureCodeObjectID] [int] NULL,
	[ProcedureCode] [nvarchar](1000) NULL,
	[ProcedureDescription] [nvarchar](255) NULL,
	[BillingInterface] [nvarchar](256) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientTransportation](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[ProviderObjectID] [int] NULL,
	[TransportationProvider] [nvarchar](100) NULL,
	[Comments] [nvarchar](max) NULL,
	[IsPrimary] [int] NOT NULL,
	[MailingAddress] [nvarchar](1000) NULL,
	[PrimaryPhone] [nvarchar](156) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
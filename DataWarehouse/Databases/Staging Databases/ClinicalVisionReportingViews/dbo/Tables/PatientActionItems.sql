﻿CREATE TABLE [dbo].[PatientActionItems](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[DateRaised_Local] [datetime] NULL,
	[RaisedByFullName] [nvarchar](40) NULL,
	[RaisedByObjectID] [int] NULL,
	[Subject] [nvarchar](255) NULL,
	[Notes] [nvarchar](max) NULL,
	[OrderObjectID] [int] NULL,
	[Actioned] [tinyint] NULL,
	[ActionedByFullName] [nvarchar](40) NULL,
	[ActionedByObjectID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
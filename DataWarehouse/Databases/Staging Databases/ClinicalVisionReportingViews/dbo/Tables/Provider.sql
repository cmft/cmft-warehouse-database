﻿CREATE TABLE [dbo].[Provider](
	[ObjectID] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ProviderType] [nvarchar](256) NULL,
	[InternalDepartmentNo] [nvarchar](20) NULL,
	[ShortName] [nvarchar](50) NULL,
	[Comments] [nvarchar](max) NULL,
	[MailingAddress] [nvarchar](1000) NULL,
	[MailingAddressObjectID] [int] NULL,
	[PrimaryPhone] [nvarchar](156) NULL,
	[PrimaryPhoneObjectID] [int] NULL,
	[GPMedicalPracticeNo] [nvarchar](20) NULL,
	[EDTANo] [nvarchar](20) NULL,
	[UKTTSNo] [nvarchar](20) NULL,
	[RenalRegistryNo] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientConsents](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Date_Local] [datetime] NULL,
	[InformationSource] [nvarchar](256) NULL,
	[Document_RTF] [nvarchar](max) NULL,
	[Document_HTML] [nvarchar](max) NULL,
	[Document_Other] [image] NULL,
	[DocumentTemplateName] [nvarchar](100) NULL,
	[ConsentType] [nvarchar](256) NULL,
	[SignedBy] [nvarchar](40) NULL,
	[ObtainedByFullName] [nvarchar](40) NULL,
	[ObtainedBySignature] [nvarchar](607) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
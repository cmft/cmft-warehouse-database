﻿CREATE TABLE [dbo].[PatientTransplantStatusLabResultJustifications](
	[TransplantStatusObjectID] [int] NOT NULL,
	[ResultObjectID] [int] NULL,
	[ResultName] [nvarchar](256) NULL,
	[OtherResultName] [nvarchar](40) NULL,
	[ResultNumeric] [float] NULL,
	[ResultString] [nvarchar](50) NULL,
	[ResultCode] [nvarchar](256) NULL,
	[ResultFlag] [nvarchar](20) NULL,
	[ResultType] [nvarchar](256) NULL,
	[Result] [nvarchar](256) NULL,
	[ResultUnits] [nvarchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientRenalModality](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[Modality] [nvarchar](256) NULL,
	[ModalitySetting] [nvarchar](256) NULL,
	[DialysisProvider] [nvarchar](100) NULL,
	[EventDetail] [nvarchar](256) NULL,
	[InformationSource] [nvarchar](256) NULL,
	[ReasonForChange] [nvarchar](256) NULL,
	[Notes] [nvarchar](max) NULL,
	[IsPrimary] [int] NOT NULL,
	[AgeAtEvent] [numeric](18, 0) NULL,
	[DialysisProviderObjectID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientTransplantEventSurgicalProcedures](
	[TransplantEventObjectID] [int] NOT NULL,
	[SurgicalProcedureObjectID] [int] NULL,
	[EventDetail] [nvarchar](256) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[ProcedureDescription] [nvarchar](1256) NULL,
	[Surgeon] [nvarchar](40) NULL,
	[Anesthetist] [nvarchar](40) NULL
) ON [PRIMARY]
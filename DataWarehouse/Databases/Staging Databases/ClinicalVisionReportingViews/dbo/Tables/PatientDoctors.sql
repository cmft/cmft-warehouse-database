﻿CREATE TABLE [dbo].[PatientDoctors](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[ConsultantObjectID] [int] NULL,
	[Consultant] [nvarchar](40) NULL,
	[MedicalSpecialty] [nvarchar](256) NULL,
	[PrimaryPhone] [nvarchar](156) NULL
) ON [PRIMARY]
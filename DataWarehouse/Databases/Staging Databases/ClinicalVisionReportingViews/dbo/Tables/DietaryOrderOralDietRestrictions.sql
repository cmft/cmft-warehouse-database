﻿CREATE TABLE [dbo].[DietaryOrderOralDietRestrictions](
	[DietaryOrderObjectID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[DietRestriction] [nvarchar](256) NULL
) ON [PRIMARY]
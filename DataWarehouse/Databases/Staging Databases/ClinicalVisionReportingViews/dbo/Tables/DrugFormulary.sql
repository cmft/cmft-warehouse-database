﻿CREATE TABLE [dbo].[DrugFormulary](
	[ObjectID] [int] NOT NULL,
	[SiteFormularyDrugName] [nvarchar](40) NULL,
	[StandardFormularyDrugName] [nvarchar](100) NULL,
	[AllowSelection] [tinyint] NULL,
	[Comment] [nvarchar](40) NULL
) ON [PRIMARY]
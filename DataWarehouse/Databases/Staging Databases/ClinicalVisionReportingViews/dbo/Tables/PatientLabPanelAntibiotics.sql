﻿CREATE TABLE [dbo].[PatientLabPanelAntibiotics](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[LabTestObjectID] [int] NULL,
	[LabTestDate_Local] [datetime] NULL,
	[LabTestQualifier] [nvarchar](256) NULL,
	[LabTestStatus] [nvarchar](256) NULL,
	[AmikacinNum] [float] NULL,
	[AmikacinStr] [nvarchar](50) NULL,
	[AmikacinFlg] [nvarchar](20) NULL,
	[ChloramphenicolNum] [float] NULL,
	[ChloramphenicolStr] [nvarchar](50) NULL,
	[ChloramphenicolFlg] [nvarchar](20) NULL,
	[GentamycinNum] [float] NULL,
	[GentamycinStr] [nvarchar](50) NULL,
	[GentamycinFlg] [nvarchar](20) NULL,
	[TobramycinNum] [float] NULL,
	[TobramycinStr] [nvarchar](50) NULL,
	[TobramycinFlg] [nvarchar](20) NULL,
	[VancomycinNum] [float] NULL,
	[VancomycinStr] [nvarchar](50) NULL,
	[VancomycinFlg] [nvarchar](20) NULL
) ON [PRIMARY]
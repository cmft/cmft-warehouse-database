﻿CREATE TABLE [dbo].[IntegratorMappableFields](
	[ObjectID] [int] NOT NULL,
	[FieldID] [nvarchar](256) NULL,
	[CVEnum] [nvarchar](256) NULL,
	[InterfaceType] [nvarchar](256) NULL
) ON [PRIMARY]
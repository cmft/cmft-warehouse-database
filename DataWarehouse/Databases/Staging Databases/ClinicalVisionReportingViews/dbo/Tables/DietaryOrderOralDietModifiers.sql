﻿CREATE TABLE [dbo].[DietaryOrderOralDietModifiers](
	[DietaryOrderObjectID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[DietModifier] [nvarchar](256) NULL
) ON [PRIMARY]
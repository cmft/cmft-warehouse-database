﻿CREATE TABLE [dbo].[OrderSignatureTrails](
	[ObjectID] [int] NOT NULL,
	[OrderObjectID] [int] NULL,
	[SignatureStatus] [nvarchar](256) NULL,
	[EnteredBy] [nvarchar](40) NULL,
	[EnteredByStaffObjectID] [int] NULL,
	[CoSignedBy] [nvarchar](40) NULL,
	[CoSignedByStaffObjectID] [int] NULL,
	[VerifiedBy] [nvarchar](40) NULL,
	[VerifiedByStaffObjectID] [int] NULL
) ON [PRIMARY]
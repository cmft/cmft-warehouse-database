﻿CREATE TABLE [dbo].[PatientRenalCoMorbidConditions](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Condition] [nvarchar](256) NULL,
	[Found] [nvarchar](256) NULL
) ON [PRIMARY]
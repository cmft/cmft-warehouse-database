﻿CREATE TABLE [dbo].[IntegratorADTMappings](
	[ObjectID] [int] NOT NULL,
	[FieldValue] [nvarchar](100) NULL,
	[CVEnumCode] [nvarchar](256) NULL,
	[CVEnum] [nvarchar](256) NULL,
	[MappableFieldObjectID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[HealthAuthority](
	[ObjectID] [int] NOT NULL,
	[Name] [nvarchar](45) NULL,
	[InternalCode] [nvarchar](40) NULL,
	[District] [nvarchar](256) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[MailingAddress] [nvarchar](692) NULL,
	[PrimaryPhone] [nvarchar](156) NULL
) ON [PRIMARY]
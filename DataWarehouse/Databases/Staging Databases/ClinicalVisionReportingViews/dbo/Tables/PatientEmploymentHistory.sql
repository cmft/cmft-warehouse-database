﻿CREATE TABLE [dbo].[PatientEmploymentHistory](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[EmploymentStatus] [nvarchar](256) NULL,
	[Occupation] [nvarchar](256) NULL,
	[OccupationOther] [nvarchar](30) NULL,
	[JobTitle] [nvarchar](30) NULL,
	[IsPrimaryEmployment] [int] NOT NULL,
	[Notes] [nvarchar](max) NULL,
	[Employer] [nvarchar](30) NULL,
	[PrimaryEmploymentAddress] [nvarchar](1000) NULL,
	[PrimaryEmploymentPhone] [nvarchar](156) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
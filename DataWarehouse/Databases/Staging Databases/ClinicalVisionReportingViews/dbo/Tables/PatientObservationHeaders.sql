﻿CREATE TABLE [dbo].[PatientObservationHeaders](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Date_Local] [datetime] NULL,
	[InformationSource] [nvarchar](256) NULL,
	[TimingQualifier] [nvarchar](256) NULL,
	[Notes] [nvarchar](max) NULL,
	[TemplateName] [nvarchar](40) NULL,
	[ObservationType] [nvarchar](256) NULL,
	[ProductModule] [nvarchar](256) NULL,
	[TimelineEventObjectID] [int] NULL,
	[StaffObjectID] [int] NULL,
	[Staff] [nvarchar](40) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
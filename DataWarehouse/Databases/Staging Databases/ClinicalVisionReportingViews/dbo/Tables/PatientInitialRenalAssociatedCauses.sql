﻿CREATE TABLE [dbo].[PatientInitialRenalAssociatedCauses](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[AssociatedCause] [nvarchar](256) NULL
) ON [PRIMARY]
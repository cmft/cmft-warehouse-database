﻿CREATE TABLE [dbo].[CommonDiagnosis](
	[ObjectID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[System] [nvarchar](256) NULL,
	[MedicalSpeciality] [nvarchar](256) NULL,
	[DiagnosisCode] [nvarchar](1000) NULL,
	[DiagnosisDescription] [nvarchar](255) NULL,
	[UseCustomCode] [tinyint] NULL,
	[CustomDiagnosisCode] [nvarchar](100) NULL,
	[CustomDiagnosisDescription] [nvarchar](255) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientIdentificationPicture](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NOT NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[IdentificationPicture] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
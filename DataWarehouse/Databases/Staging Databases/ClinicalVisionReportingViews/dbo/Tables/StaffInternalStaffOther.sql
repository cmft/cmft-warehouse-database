﻿CREATE TABLE [dbo].[StaffInternalStaffOther](
	[ObjectID] [int] NOT NULL,
	[InterfaceSource] [nvarchar](256) NULL,
	[InternalStaffObjectID] [int] NULL,
	[InternalStaffName] [nvarchar](40) NULL,
	[InternalStaffMedicalSpeciality] [nvarchar](256) NULL,
	[OtherStaffName] [nvarchar](30) NULL,
	[OtherStaffMedicalSpeciality] [nvarchar](256) NULL,
	[OtherStaffNotes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
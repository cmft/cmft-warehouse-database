﻿CREATE TABLE [dbo].[PatientAddresses](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NOT NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[AttentionLine] [nvarchar](40) NULL,
	[StreetAddress] [nvarchar](40) NULL,
	[StreetAddressLine2] [nvarchar](40) NULL,
	[City] [nvarchar](40) NULL,
	[County] [nvarchar](100) NULL,
	[Country] [nvarchar](256) NULL,
	[PostCode] [nvarchar](50) NULL,
	[AddressType] [nvarchar](256) NULL,
	[IsMailingAddress] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientClinicPlans](
	[ObjectID] [int] NOT NULL,
	[OfficeVisitServiceObjectID] [int] NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Date_Local] [datetime] NULL,
	[InformationSource] [nvarchar](256) NULL,
	[TimingQualifier] [nvarchar](256) NULL,
	[StaffObjectID] [int] NULL,
	[StaffName] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL,
	[Document_RTF] [nvarchar](max) NULL,
	[Document_HTML] [nvarchar](max) NULL,
	[Document_Other] [image] NULL,
	[DocumentTemplateName] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientHemodialysisOrderMachinePrograms](
	[ObjectID] [int] NULL,
	[HemodialysisOrderObjectID] [int] NOT NULL,
	[ProgramName] [nvarchar](256) NULL,
	[ParameterNumber] [int] NULL,
	[ParameterName] [nvarchar](256) NULL,
	[ParameterAdministrationTime] [nvarchar](256) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientTransplantStatusTimelineJustifications](
	[TransplantStatusObjectID] [int] NOT NULL,
	[TimelineObjectID] [int] NOT NULL,
	[TimelineStartDate_Local] [datetime] NULL,
	[TimelineStopDate_Local] [datetime] NULL,
	[TimelineEvent] [nvarchar](256) NULL,
	[TimelineEventDetail] [nvarchar](256) NULL
) ON [PRIMARY]
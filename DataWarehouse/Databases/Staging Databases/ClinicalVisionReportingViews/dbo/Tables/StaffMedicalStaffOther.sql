﻿CREATE TABLE [dbo].[StaffMedicalStaffOther](
	[ObjectID] [int] NOT NULL,
	[MedicalStaffObjectID] [int] NULL,
	[MedicalStaffName] [nvarchar](40) NULL,
	[MedicalStaffMedicalSpeciality] [nvarchar](256) NULL,
	[OtherStaffName] [nvarchar](30) NULL,
	[OtherStaffMedicalSpeciality] [nvarchar](256) NULL,
	[OtherStaffNotes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
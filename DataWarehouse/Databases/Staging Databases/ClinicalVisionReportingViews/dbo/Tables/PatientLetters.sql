﻿CREATE TABLE [dbo].[PatientLetters](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Date_Local] [datetime] NULL,
	[ToStaffObjectID] [int] NULL,
	[To] [nvarchar](40) NULL,
	[FromStaffObjectID] [int] NULL,
	[From] [nvarchar](40) NULL,
	[LetterSent] [tinyint] NULL,
	[DateLetterSent_Local] [datetime] NULL,
	[Document_RTF] [nvarchar](max) NULL,
	[Document_HTML] [nvarchar](max) NULL,
	[Document_Other] [image] NULL,
	[AttachedDocumentObjectID] [int] NULL,
	[DocumentTemplateName] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientRenalClinicalIndications](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[RenalRegistrationObjectID] [int] NOT NULL,
	[ClinicalIndication] [nvarchar](256) NULL
) ON [PRIMARY]
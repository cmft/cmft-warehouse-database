﻿CREATE TABLE [dbo].[PatientPhoneNumbers](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NOT NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[AreaCityCode] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Extension] [nvarchar](50) NULL,
	[Comment] [nvarchar](40) NULL,
	[UseCode] [nvarchar](256) NULL,
	[IsPrimary] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientHemodialysisTargetFluidLoss](
	[ObjectID] [int] NOT NULL,
	[HemodialysisServiceObjectID] [int] NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[ServiceDate_Local] [datetime] NULL,
	[WeightOfPatient] [float] NULL,
	[IdealBodyWeight] [float] NULL,
	[WeightDifference] [float] NULL,
	[IndirectSalineOnOff] [float] NULL,
	[FluidIntake] [float] NULL,
	[SalineFlushesDrugsBlood] [float] NULL,
	[TargetFluidLoss] [float] NULL,
	[StaffObjectID] [int] NULL,
	[StaffName] [nvarchar](40) NULL
) ON [PRIMARY]
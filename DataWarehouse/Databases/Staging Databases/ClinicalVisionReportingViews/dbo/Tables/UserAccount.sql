﻿CREATE TABLE [dbo].[UserAccount](
	[ObjectID] [int] NOT NULL,
	[UserName] [nvarchar](40) NULL,
	[StaffObjectID] [int] NULL,
	[StaffName] [nvarchar](40) NULL,
	[DomainName] [nvarchar](40) NULL,
	[DefaultPackage] [nvarchar](40) NULL,
	[DefaultRole] [nvarchar](40) NULL,
	[UserGroup] [nvarchar](40) NULL,
	[ExpiryDate] [datetime] NULL
) ON [PRIMARY]
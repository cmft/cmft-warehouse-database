﻿CREATE TABLE [dbo].[ClinicService_PhysicalExam](
	[FollowupServiceObjectID] [int] NULL,
	[OfficeVisitServiceObjectID] [int] NULL,
	[WorkupProtocolServiceObjectID] [int] NULL,
	[PhysicalExamObjectID] [int] NOT NULL
) ON [PRIMARY]
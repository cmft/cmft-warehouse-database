﻿CREATE TABLE [dbo].[CareTeam_StaffInternal](
	[CareTeamObjectID] [int] NULL,
	[StaffInternalObjectID] [int] NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[CaregiverRole] [nvarchar](256) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientCarePlanAssessmentEvaluations](
	[ObjectID] [int] NULL,
	[CarePlanAssessmentObjectID] [int] NOT NULL,
	[EvaluationDate_Local] [datetime] NULL,
	[StaffObjectID] [int] NULL,
	[StaffName] [nvarchar](40) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientTransplantHistoryOrgansTransplanted](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[TransplantHistoryObjectID] [int] NOT NULL,
	[TransplantDate_Local] [datetime] NULL,
	[Organ] [nvarchar](256) NULL
) ON [PRIMARY]
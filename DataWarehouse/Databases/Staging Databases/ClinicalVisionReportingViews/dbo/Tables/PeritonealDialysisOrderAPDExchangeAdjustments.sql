﻿CREATE TABLE [dbo].[PeritonealDialysisOrderAPDExchangeAdjustments](
	[PeritonealDialysisObjectID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[Criteria] [nvarchar](256) NULL,
	[Comment] [nvarchar](40) NULL,
	[Solution] [nvarchar](256) NULL,
	[Volume] [float] NULL,
	[VolumeUnits] [nvarchar](256) NULL,
	[VolumeInLiters] [float] NULL,
	[NumberOfBags] [int] NULL
) ON [PRIMARY]
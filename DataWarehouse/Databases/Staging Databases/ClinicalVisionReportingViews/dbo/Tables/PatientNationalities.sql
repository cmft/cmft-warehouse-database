﻿CREATE TABLE [dbo].[PatientNationalities](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NOT NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Nationality] [nvarchar](256) NULL
) ON [PRIMARY]
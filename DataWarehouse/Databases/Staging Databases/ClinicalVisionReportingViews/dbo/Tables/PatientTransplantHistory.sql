﻿CREATE TABLE [dbo].[PatientTransplantHistory](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[TransplantDate_Local] [datetime] NULL,
	[AgeAtEvent] [numeric](18, 0) NULL,
	[Provider] [nvarchar](100) NULL,
	[FailureDate_Local] [datetime] NULL,
	[ReasonForFailure] [nvarchar](256) NULL,
	[InformationSource] [nvarchar](256) NULL,
	[DonorType] [nvarchar](256) NULL,
	[TransplantEvent] [nvarchar](256) NULL,
	[GeneticRelationship] [nvarchar](256) NULL,
	[OtherRelationship] [nvarchar](100) NULL,
	[RegistryDate_Local] [datetime] NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[VersionInfo](
	[Build Label] [varchar](12) NULL,
	[Build Date] [varchar](24) NULL,
	[Locale] [nvarchar](14) NULL,
	[RenalTransplant] [varchar](12) NULL,
	[SysAdmin] [varchar](12) NULL,
	[cvAudit] [varchar](12) NULL,
	[ClinicalVisionRenal] [varchar](12) NULL,
	[Integrator] [varchar](12) NULL,
	[cvIntegratorAudit] [varchar](12) NULL,
	[ClinicalVisionTransplant] [varchar](12) NULL,
	[ClinicalVisionCore] [varchar](12) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[IntegratorLabResultMappings](
	[ObjectID] [int] NOT NULL,
	[FieldValue] [nvarchar](100) NULL,
	[CVEnumCode] [nvarchar](256) NULL,
	[CVEnum] [nvarchar](256) NULL,
	[TimingQualifier] [nvarchar](256) NULL,
	[IsMicrobiology] [tinyint] NULL,
	[VendorID] [nvarchar](100) NULL,
	[MappableFieldObjectID] [int] NULL
) ON [PRIMARY]
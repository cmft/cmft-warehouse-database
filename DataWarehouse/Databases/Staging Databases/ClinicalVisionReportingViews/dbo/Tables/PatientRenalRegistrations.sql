﻿CREATE TABLE [dbo].[PatientRenalRegistrations](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[OnsetDate_Local] [datetime] NULL,
	[BasisForDiagnosis] [nvarchar](256) NULL,
	[PrimaryRenalDiagnosis] [nvarchar](256) NULL,
	[OtherDiagnosis] [nvarchar](max) NULL,
	[PhysicalExamObjectID] [int] NULL,
	[PhysicalExamHeightInCm] [float] NULL,
	[PhysicalExamWeightInKg] [float] NULL,
	[Remarks] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
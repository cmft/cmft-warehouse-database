﻿CREATE TABLE [dbo].[PatientHealthAuthorities](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[IDNo] [nvarchar](20) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[HealthAuthorityObjectID] [int] NULL,
	[HealthAuthorityName] [nvarchar](45) NULL,
	[HealthAuthorityDistrict] [nvarchar](256) NULL
) ON [PRIMARY]
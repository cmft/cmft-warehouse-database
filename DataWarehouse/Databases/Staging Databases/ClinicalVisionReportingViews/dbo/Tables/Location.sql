﻿CREATE TABLE [dbo].[Location](
	[ObjectID] [int] NOT NULL,
	[Name] [nvarchar](40) NULL,
	[Ward] [nvarchar](256) NULL,
	[ProviderObjectID] [int] NULL,
	[ProviderName] [nvarchar](100) NULL,
	[ProviderType] [nvarchar](256) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
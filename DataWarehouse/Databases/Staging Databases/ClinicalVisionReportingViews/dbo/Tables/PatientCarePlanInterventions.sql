﻿CREATE TABLE [dbo].[PatientCarePlanInterventions](
	[ObjectID] [int] NOT NULL,
	[CarePlanObjectID] [int] NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[InterventionName] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[Completed] [int] NOT NULL,
	[CompletedDate_Local] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
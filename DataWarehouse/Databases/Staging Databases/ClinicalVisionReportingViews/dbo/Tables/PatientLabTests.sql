﻿CREATE TABLE [dbo].[PatientLabTests](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Date_Local] [datetime] NULL,
	[TestName] [nvarchar](4000) NULL,
	[OtherTestName] [nvarchar](40) NULL,
	[InterfaceSource] [nvarchar](256) NULL,
	[TimingQualifier] [nvarchar](256) NULL,
	[StaffObjectID] [int] NULL,
	[StaffName] [nvarchar](40) NULL,
	[ProviderObjectID] [int] NULL,
	[Provider] [nvarchar](100) NULL,
	[AccessionNumber] [nvarchar](40) NULL,
	[ResultStatus] [nvarchar](256) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
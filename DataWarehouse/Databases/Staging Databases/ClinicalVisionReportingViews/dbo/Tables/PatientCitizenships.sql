﻿CREATE TABLE [dbo].[PatientCitizenships](
	[ObjectID] [int] NULL,
	[PatientObjectID] [int] NOT NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[Citizenship] [nvarchar](256) NULL
) ON [PRIMARY]
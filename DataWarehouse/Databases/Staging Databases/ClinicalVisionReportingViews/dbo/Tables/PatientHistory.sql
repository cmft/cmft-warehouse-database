﻿CREATE TABLE [dbo].[PatientHistory](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[TimingQualifier] [nvarchar](256) NULL,
	[InformationSource] [nvarchar](256) NULL,
	[StaffObjectID] [int] NULL,
	[Staff] [nvarchar](40) NULL,
	[ReasonHistoryTaken] [nvarchar](256) NULL,
	[ChiefComplaint] [nvarchar](max) NULL,
	[HistoryOfPresentIllness] [nvarchar](max) NULL,
	[PastMedicalHistory] [nvarchar](max) NULL,
	[FamilyHistory] [nvarchar](max) NULL,
	[PsychosocialHistory] [nvarchar](max) NULL,
	[SurgicalHistory] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
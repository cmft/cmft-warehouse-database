﻿CREATE TABLE [dbo].[PeritonealDialysisOrderAPDAdditives](
	[PeritonealDialysisObjectID] [int] NOT NULL,
	[ObjectID] [int] NULL,
	[Additive] [nvarchar](256) NULL,
	[AdditiveDose] [float] NULL,
	[AdditiveDoseUnits] [nvarchar](256) NULL,
	[Instructions] [nvarchar](40) NULL
) ON [PRIMARY]
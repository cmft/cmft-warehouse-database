﻿CREATE TABLE [dbo].[PatientAliases](
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[ObjectID] [int] NOT NULL,
	[FirstName] [nvarchar](30) NULL,
	[MiddleName] [nvarchar](30) NULL,
	[LastName] [nvarchar](30) NULL,
	[Title] [nvarchar](256) NULL,
	[Suffix] [nvarchar](256) NULL,
	[Degree] [nvarchar](256) NULL,
	[AliasType] [nvarchar](256) NULL,
	[Notes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
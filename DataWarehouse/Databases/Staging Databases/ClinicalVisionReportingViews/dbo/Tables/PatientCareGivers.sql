﻿CREATE TABLE [dbo].[PatientCareGivers](
	[PatientObjectID] [int] NOT NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[StartDate_Local] [datetime] NULL,
	[StopDate_Local] [datetime] NULL,
	[CareTeamObjectID] [int] NULL,
	[CareTeamName] [nvarchar](40) NULL,
	[CareGiverName] [nvarchar](40) NULL,
	[CareGiverRole] [nvarchar](256) NULL,
	[StaffObjectID] [int] NULL
) ON [PRIMARY]
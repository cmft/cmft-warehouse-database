﻿CREATE TABLE [dbo].[OrderSignatureNotifications](
	[ObjectID] [int] NOT NULL,
	[OrderObjectID] [int] NULL,
	[MessageTitle] [nvarchar](255) NULL,
	[MessageSentDate_Local] [datetime] NULL,
	[MessageRead] [tinyint] NULL,
	[MessageReadDate_Local] [datetime] NULL,
	[RecipientObjectID] [int] NULL,
	[RecipientName] [nvarchar](40) NULL,
	[SenderObjectID] [int] NULL,
	[SenderName] [nvarchar](40) NULL
) ON [PRIMARY]
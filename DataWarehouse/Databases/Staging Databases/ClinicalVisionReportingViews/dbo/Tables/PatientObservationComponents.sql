﻿CREATE TABLE [dbo].[PatientObservationComponents](
	[ObjectID] [int] NOT NULL,
	[ObservationHeaderObjectID] [int] NULL,
	[Sequence] [int] NULL,
	[Required] [tinyint] NULL,
	[ObservationCategory] [nvarchar](256) NULL,
	[ComponentNotes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
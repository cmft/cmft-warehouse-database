﻿CREATE TABLE [dbo].[PatientLabPanelUreaClearance](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[LabTestObjectID] [int] NULL,
	[LabTestDate_Local] [datetime] NULL,
	[LabTestQualifier] [nvarchar](256) NULL,
	[LabTestStatus] [nvarchar](256) NULL,
	[24hUreaNum] [float] NULL,
	[24hUreaStr] [nvarchar](50) NULL,
	[24hUreaFlg] [nvarchar](20) NULL,
	[CollTimeNum] [float] NULL,
	[CollTimeStr] [nvarchar](50) NULL,
	[CollTimeFlg] [nvarchar](20) NULL,
	[BloodUreaNitrogenNum] [float] NULL,
	[BloodUreaNitrogenStr] [nvarchar](50) NULL,
	[BloodUreaNitrogenFlg] [nvarchar](20) NULL,
	[UreaUrineNum] [float] NULL,
	[UreaUrineStr] [nvarchar](50) NULL,
	[UreaUrineFlg] [nvarchar](20) NULL,
	[UreaClearanceNum] [float] NULL,
	[UreaClearanceStr] [nvarchar](50) NULL,
	[UreaClearanceFlg] [nvarchar](20) NULL,
	[VolumeNum] [float] NULL,
	[VolumeStr] [nvarchar](50) NULL,
	[VolumeFlg] [nvarchar](20) NULL
) ON [PRIMARY]
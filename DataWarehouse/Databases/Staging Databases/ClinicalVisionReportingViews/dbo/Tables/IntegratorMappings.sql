﻿CREATE TABLE [dbo].[IntegratorMappings](
	[ObjectID] [int] NOT NULL,
	[FieldID] [nvarchar](256) NULL,
	[CV4Enum] [nvarchar](256) NULL,
	[InterfaceType] [nvarchar](256) NULL,
	[ADTMappingFieldValue] [nvarchar](100) NULL,
	[ADTMappingCV4EnumCode] [nvarchar](256) NULL,
	[ADTMappingCV4Enum] [nvarchar](256) NULL,
	[LabResultMappingFieldValue] [nvarchar](100) NULL,
	[LabResultMappingCV4EnumCode] [nvarchar](256) NULL,
	[LabResultMappingCV4Enum] [nvarchar](256) NULL,
	[OutboundMappingFieldValue] [nvarchar](100) NULL,
	[OutboundMappingCV4EnumCode] [nvarchar](256) NULL,
	[OutboundMappingCV4Enum] [nvarchar](256) NULL
) ON [PRIMARY]
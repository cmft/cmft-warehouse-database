﻿CREATE TABLE [dbo].[PatientTransplantStatusOrderJustifications](
	[TransplantStatusObjectID] [int] NOT NULL,
	[OrderObjectID] [int] NULL,
	[OrderDate_Local] [datetime] NULL,
	[OrderStartDate_Local] [datetime] NULL,
	[OrderEndDate_Local] [datetime] NULL,
	[ServiceType] [nvarchar](256) NULL,
	[ServiceMasterName] [nvarchar](4000) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PatientHistoryObservationComponents](
	[ObjectID] [int] NOT NULL,
	[PatientObjectID] [int] NULL,
	[PatientFullName] [nvarchar](40) NULL,
	[PatientMedicalRecordNo] [nvarchar](12) NULL,
	[PatientHistoryObjectID] [int] NULL,
	[ComponentSequence] [int] NULL,
	[ObservationCategory] [nvarchar](256) NULL,
	[ComponentNotes] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
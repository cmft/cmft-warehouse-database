﻿CREATE TABLE [dbo].[Tbl_PatOtherAppointments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Districtno] [varchar](20) NULL,
	[APPT_START] [varchar](5) NULL,
	[APPT_END] [varchar](5) NULL,
	[APPT_DATE] [datetime] NULL,
	[APPOINT_TYPE] [varchar](1) NULL,
	[APPT_DESCRIP] [varchar](50) NULL,
	[RESOURCEID] [varchar](50) NULL,
	[AuditInsertDate] [datetime] NULL,
	[AuditUpdateDate] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[REGLOC](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RegimeId] [varchar](16) NULL,
	[PhCode] [varchar](3) NULL CONSTRAINT [DF_REGLOC_PhCode]  DEFAULT ('')
) ON [PRIMARY]
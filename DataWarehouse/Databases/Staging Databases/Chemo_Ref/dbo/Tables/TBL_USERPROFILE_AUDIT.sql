﻿CREATE TABLE [dbo].[TBL_USERPROFILE_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProfileID] [int] NOT NULL,
	[Status] [varchar](1) NULL,
	[dteChange] [datetime] NULL,
	[UserID] [varchar](16) NULL
) ON [PRIMARY]
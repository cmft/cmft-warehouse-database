﻿CREATE TABLE [dbo].[Tbl_ResourceGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GroupCode] [varchar](6) NULL,
	[ResourceCode] [varchar](6) NULL
) ON [PRIMARY]
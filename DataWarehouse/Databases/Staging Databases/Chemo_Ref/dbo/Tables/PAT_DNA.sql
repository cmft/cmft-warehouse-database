﻿CREATE TABLE [dbo].[PAT_DNA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[APPTiD] [uniqueidentifier] NULL,
	[DIARYDATE] [datetime] NULL,
	[DNA_REASON] [varchar](6) NULL,
	[APPT_START] [char](5) NULL,
	[APPT_END] [char](5) NULL
) ON [PRIMARY]
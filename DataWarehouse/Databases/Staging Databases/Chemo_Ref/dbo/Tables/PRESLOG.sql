﻿CREATE TABLE [dbo].[PRESLOG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UNIQUEID] [varchar](10) NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[REGIME] [varchar](16) NULL,
	[CYCLE] [numeric](3, 0) NULL,
	[CHARTNUM] [varchar](10) NULL,
	[CHARTTYPE] [varchar](1) NULL,
	[PARTSEQ] [varchar](10) NULL,
	[PRESDATE] [datetime] NULL,
	[PRESTIME] [varchar](5) NULL,
	[PRESUSER] [varchar](12) NULL,
	[PRNTTYPE] [varchar](1) NULL,
	[STARTDAY] [numeric](3, 0) NULL,
	[ENDDAY] [numeric](3, 0) NULL,
	[PAGECOUNT] [numeric](3, 0) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PARAMS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IDENTIFIER] [varchar](1) NOT NULL,
	[CODE] [varchar](6) NOT NULL,
	[TEXT] [varchar](60) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MODHIST](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[REGIME] [varchar](16) NOT NULL,
	[CYCLE] [numeric](3, 0) NOT NULL,
	[DAYNO] [numeric](3, 0) NOT NULL,
	[DRUGNAME] [varchar](30) NOT NULL,
	[REASON] [varchar](6) NOT NULL,
	[ACTION] [varchar](60) NOT NULL,
	[OLDDOSE] [numeric](6, 0) NOT NULL,
	[NEWDOSE] [numeric](6, 0) NOT NULL,
	[APPT_DATE] [datetime] NOT NULL,
	[USER1] [varchar](10) NOT NULL,
	[MODDATE] [datetime] NOT NULL,
	[REG_CYC] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
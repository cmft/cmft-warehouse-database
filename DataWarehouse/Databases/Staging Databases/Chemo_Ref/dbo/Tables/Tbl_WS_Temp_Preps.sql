﻿CREATE TABLE [dbo].[Tbl_WS_Temp_Preps](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Template_Id] [uniqueidentifier] NOT NULL,
	[PrepId] [uniqueidentifier] NOT NULL,
	[DispOrder] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_RMT_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActionByUser] [varchar](50) NOT NULL,
	[ActionText] [varchar](50) NOT NULL,
	[ActionDateTime] [datetime] NOT NULL,
	[TEMP_PATIENT_ID] [int] NOT NULL
) ON [PRIMARY]
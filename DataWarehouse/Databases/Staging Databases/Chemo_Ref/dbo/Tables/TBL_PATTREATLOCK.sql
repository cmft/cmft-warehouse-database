﻿CREATE TABLE [dbo].[TBL_PATTREATLOCK](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[REGIME] [varchar](16) NULL,
	[CYCLE] [numeric](3, 0) NULL,
	[UNAME] [varchar](32) NULL,
	[DATEACTIVE] [datetime] NULL,
	[ARCHIVE] [bit] NULL
) ON [PRIMARY]
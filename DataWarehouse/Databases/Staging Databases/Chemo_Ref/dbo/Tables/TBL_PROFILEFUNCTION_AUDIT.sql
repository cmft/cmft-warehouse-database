﻿CREATE TABLE [dbo].[TBL_PROFILEFUNCTION_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProfileID] [int] NOT NULL,
	[FunctionID] [varchar](6) NOT NULL,
	[dteCreated] [datetime] NULL,
	[CreatedBy] [varchar](12) NULL,
	[dteDeleted] [datetime] NULL,
	[DeletedBy] [varchar](12) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CTESTRES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[REGIME] [varchar](16) NOT NULL,
	[CYCLE] [numeric](3, 0) NOT NULL,
	[CTESTID] [varchar](10) NOT NULL,
	[T_DATE] [datetime] NULL,
	[T_VALUE] [varchar](10) NULL,
	[DAYNO] [numeric](3, 0) NULL,
	[SOURCE] [varchar](32) NULL,
	[ISMANUAL] [bit] NULL
) ON [PRIMARY]
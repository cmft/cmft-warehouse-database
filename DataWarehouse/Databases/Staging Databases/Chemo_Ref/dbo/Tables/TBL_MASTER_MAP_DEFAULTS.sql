﻿CREATE TABLE [dbo].[TBL_MASTER_MAP_DEFAULTS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MM_ROW_GUID] [varchar](38) NOT NULL,
	[TL_TRCODE] [varchar](3) NULL
) ON [PRIMARY]
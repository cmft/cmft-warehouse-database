﻿CREATE TABLE [dbo].[Interventions](
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_Interventions_rowguid]  DEFAULT (newid()),
	[InterventionDescription] [varchar](30) NOT NULL,
	[InterventionTime] [int] NULL,
	[InterventionNWU] [int] NULL,
	[InterventionPWU] [int] NULL,
	[InterventionBeforeTreat] [bit] NULL,
	[InterventionFirstTreat] [bit] NULL,
	[InterventionAfterAllTreat] [bit] NULL,
	[nwuFirstSlot] [numeric](5, 2) NULL,
	[nwuOtherSlot] [numeric](5, 2) NULL,
	[nwuFinalSlot] [numeric](5, 2) NULL,
	[ActivityCode] [char](6) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[DOSEBAND](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[COURSE] [varchar](16) NULL,
	[DRUGNAME] [varchar](30) NULL,
	[UPVAL] [numeric](8, 2) NULL,
	[LOWVAL] [numeric](8, 2) NULL,
	[STDDOSE] [numeric](8, 2) NULL,
	[CID] [varchar](10) NULL,
	[ROUTE] [varchar](15) NULL
) ON [PRIMARY]
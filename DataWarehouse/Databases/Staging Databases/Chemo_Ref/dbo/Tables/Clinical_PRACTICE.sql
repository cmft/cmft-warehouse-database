﻿CREATE TABLE [dbo].[Clinical_PRACTICE](
	[PRACTICE_ID] [varchar](10) NOT NULL,
	[ADDRESS_LINE_1] [varchar](100) NULL,
	[ADDRESS_LINE_2] [varchar](100) NULL,
	[ADDRESS_LINE_3] [varchar](100) NULL,
	[TOWN] [varchar](100) NULL,
	[COUNTY] [varchar](100) NULL,
	[COUNTRY] [varchar](100) NULL,
	[POSTCODE] [varchar](20) NULL,
	[WORK_PHONE] [varchar](20) NULL,
	[FAX_PHONE] [varchar](20) NULL,
	[SUB_PRACTICE_PARENT] [varchar](10) NULL
) ON [PRIMARY]
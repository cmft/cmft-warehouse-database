﻿CREATE TABLE [dbo].[TBL_USERPROFILE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProfileID] [int] NOT NULL,
	[Description] [varchar](50) NULL,
	[Release] [bit] NULL,
	[Archive] [bit] NULL,
	[DateChange] [datetime] NULL,
	[UserID] [varchar](12) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[MachGroup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[machine] [varchar](5) NOT NULL,
	[machinegroup] [varchar](6) NOT NULL
) ON [PRIMARY]
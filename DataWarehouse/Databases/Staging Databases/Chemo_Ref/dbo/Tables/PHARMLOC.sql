﻿CREATE TABLE [dbo].[PHARMLOC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PhCode] [varchar](3) NULL,
	[PhName] [varchar](25) NULL,
	[PhPrinterLoc1] [varchar](50) NULL,
	[PhPrinterLoc2] [varchar](50) NULL,
	[PhPrinterLoc3] [varchar](50) NULL,
	[PhPrinterLoc4] [varchar](50) NULL,
	[PhNumber] [int] NULL,
	[PhWsBatchno] [varchar](5) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[VEHICLE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TEXT] [varchar](60) NOT NULL,
	[VOLUME] [numeric](6, 1) NULL,
	[MANU] [varchar](12) NULL,
	[BATCHNO] [varchar](10) NULL,
	[EXPIRY] [datetime] NULL,
	[MAX_VOL] [numeric](6, 1) NULL,
	[SMALLBAG] [bit] NULL,
	[STDOVERAGE] [numeric](6, 0) NULL,
	[SPECGRVTY] [numeric](7, 2) NULL,
	[PLCODE] [varchar](3) NULL
) ON [PRIMARY]
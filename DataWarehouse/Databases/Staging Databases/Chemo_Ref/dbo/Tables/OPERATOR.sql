﻿CREATE TABLE [dbo].[OPERATOR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OPPWD] [varchar](16) NOT NULL,
	[OPUID] [varchar](12) NOT NULL,
	[FULLNAME] [varchar](32) NULL,
	[ULEV] [numeric](1, 0) NOT NULL,
	[ACCESS] [varchar](40) NULL,
	[SPEC_OPT] [varchar](1) NULL,
	[TRIAL_OPT] [varchar](6) NULL,
	[VER_OPT] [varchar](6) NULL CONSTRAINT [DF_OPERATOR_VER_OPT]  DEFAULT ('CLI'),
	[TRCODE] [varchar](3) NULL CONSTRAINT [DF_OPERATOR_TRCODE]  DEFAULT (''),
	[CONSMAP] [varchar](50) NULL,
	[lastpass] [datetime] NULL,
	[LASTLOGIN] [datetime] NULL,
	[ProfileID] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_PROGRAMFUNCTIONS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FunctionID] [varchar](20) NOT NULL,
	[FunctionDescription] [varchar](50) NULL,
	[ORDERNUM] [int] NULL
) ON [PRIMARY]
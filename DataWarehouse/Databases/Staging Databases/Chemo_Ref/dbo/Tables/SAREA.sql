﻿CREATE TABLE [dbo].[SAREA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[SAREA] [numeric](6, 3) NOT NULL,
	[HEIGHT] [numeric](5, 2) NULL,
	[WEIGHT] [numeric](5, 1) NULL,
	[CALC_DATE] [datetime] NULL,
	[CALC_USER] [varchar](10) NULL,
	[CAPPED] [bit] NULL,
	[ERROR] [bit] NULL
) ON [PRIMARY]
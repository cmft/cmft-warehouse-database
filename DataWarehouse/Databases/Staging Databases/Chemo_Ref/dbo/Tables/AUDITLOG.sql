﻿CREATE TABLE [dbo].[AUDITLOG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AUD_USERID] [varchar](12) NOT NULL,
	[AUD_DATE] [datetime] NOT NULL,
	[AUD_TIME] [varchar](5) NOT NULL,
	[AUD_DESC] [text] NULL,
	[AUD_ACT] [varchar](15) NULL,
	[AUD_FIELD1] [varchar](20) NULL,
	[AUD_FIELD2] [varchar](20) NULL,
	[AUD_FIELD3] [varchar](15) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
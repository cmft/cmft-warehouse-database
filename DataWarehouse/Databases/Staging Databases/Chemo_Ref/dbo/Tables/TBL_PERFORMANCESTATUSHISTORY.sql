﻿CREATE TABLE [dbo].[TBL_PERFORMANCESTATUSHISTORY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[SCORE] [varchar](6) NOT NULL,
	[DATE_ENTERED] [datetime] NULL,
	[USERNAME] [varchar](32) NULL,
	[REGIME] [varchar](16) NULL,
	[CYCLE] [numeric](3, 0) NOT NULL,
	[DAYNO] [numeric](3, 0) NOT NULL,
	[PatCourseId] [uniqueidentifier] NULL
) ON [PRIMARY]
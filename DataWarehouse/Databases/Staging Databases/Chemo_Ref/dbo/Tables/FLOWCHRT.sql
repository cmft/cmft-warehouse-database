﻿CREATE TABLE [dbo].[FLOWCHRT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PROFILE] [varchar](10) NULL,
	[CHART] [varchar](25) NULL,
	[TABNUM] [numeric](1, 0) NULL,
	[ORDERNUM] [numeric](2, 0) NULL,
	[CID] [varchar](10) NOT NULL,
	[DISPCAP] [varchar](10) NULL,
	[USEULN] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_WS_Fields](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FieldUid] [uniqueidentifier] NOT NULL,
	[Field_Description] [char](30) NULL,
	[FieldName] [char](20) NULL,
	[Field_var] [char](50) NULL,
	[Type] [char](3) NULL,
	[GroupCode] [char](3) NULL,
	[UserDisplay] [bit] NULL
) ON [PRIMARY]
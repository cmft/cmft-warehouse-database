﻿CREATE TABLE [dbo].[Tbl_Resource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](6) NULL,
	[Description] [varchar](30) NULL
) ON [PRIMARY]
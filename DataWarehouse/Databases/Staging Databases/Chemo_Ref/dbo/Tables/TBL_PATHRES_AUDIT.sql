﻿CREATE TABLE [dbo].[TBL_PATHRES_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[CODE] [varchar](50) NULL,
	[DESCRIPTION] [varchar](500) NULL,
	[ITEM_VALUE] [varchar](50) NULL,
	[AUDIT_DATE] [datetime] NULL,
	[CALLED_FROM] [varchar](50) NULL
) ON [PRIMARY]
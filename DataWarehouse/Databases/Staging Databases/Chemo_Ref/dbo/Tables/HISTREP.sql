﻿CREATE TABLE [dbo].[HISTREP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HISTREPNUM] [varchar](16) NULL,
	[TYPE] [varchar](1) NULL,
	[NAME] [varchar](16) NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[HISTREPKEY] [varchar](10) NULL
) ON [PRIMARY]
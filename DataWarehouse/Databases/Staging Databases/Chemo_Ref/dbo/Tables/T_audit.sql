﻿CREATE TABLE [dbo].[T_audit](
	[PATCOURSEID] [uniqueidentifier] NULL,
	[ApptId] [uniqueidentifier] NULL,
	[machine] [varchar](6) NULL,
	[action] [varchar](100) NULL,
	[descrip] [text] NULL,
	[r_eason] [varchar](3) NULL,
	[u_ser] [varchar](50) NULL,
	[d_ate] [datetime] NULL,
	[timestamp_column] [timestamp] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[actionType] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
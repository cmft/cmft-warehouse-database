﻿CREATE TABLE [dbo].[STDPRESL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PRESNAME] [varchar](30) NOT NULL,
	[DRUGNAME] [varchar](30) NOT NULL,
	[CALC] [varchar](10) NULL,
	[ROUTE] [varchar](15) NULL,
	[VEHICLE] [varchar](60) NULL,
	[DRUGDOSE] [numeric](6, 0) NULL,
	[UNIT] [varchar](15) NULL,
	[DTIME] [varchar](5) NULL,
	[FREQ] [varchar](10) NULL,
	[DURATION] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ROUTE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TEXT] [varchar](15) NOT NULL,
	[ROUTABBRV] [varchar](4) NULL,
	[ROUTCOMM] [varchar](30) NULL,
	[ROUTECHRT] [varchar](10) NULL
) ON [PRIMARY]
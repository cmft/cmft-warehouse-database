﻿CREATE TABLE [dbo].[TBL_PatOtherMeds](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[districtno] [varchar](20) NOT NULL,
	[DrugATCcode] [varchar](7) NOT NULL,
	[DrugDescrip] [varchar](250) NULL,
	[DTRegistered] [datetime] NULL,
	[RegUseruid] [varchar](12) NULL,
	[RegUserFullname] [varchar](32) NULL,
	[DTDeleted] [datetime] NULL,
	[IsAllergy] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[DelUserid] [varchar](12) NULL,
	[DeluserFullName] [varchar](32) NULL
) ON [PRIMARY]
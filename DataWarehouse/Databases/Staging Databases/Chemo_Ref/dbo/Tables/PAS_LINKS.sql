﻿CREATE TABLE [dbo].[PAS_LINKS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAS_Code] [varchar](10) NOT NULL,
	[TL_TrCode] [varchar](3) NOT NULL,
	[Primary_PAS] [bit] NOT NULL CONSTRAINT [DF_PAS_LINKS_Primary_PAS]  DEFAULT ((0))
) ON [PRIMARY]
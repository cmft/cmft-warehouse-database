﻿CREATE TABLE [dbo].[VEHSIZE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VEHICLE] [varchar](60) NULL,
	[VOLUME] [numeric](6, 0) NULL,
	[MANU] [varchar](12) NULL,
	[BATCHNO] [varchar](10) NULL,
	[EXPIRY] [datetime] NULL,
	[MAX_VOL] [numeric](6, 0) NULL,
	[STDOVERAGE] [numeric](18, 0) NULL,
	[STOCKCODE] [varchar](30) NULL,
	[PLCODE] [varchar](3) NULL
) ON [PRIMARY]
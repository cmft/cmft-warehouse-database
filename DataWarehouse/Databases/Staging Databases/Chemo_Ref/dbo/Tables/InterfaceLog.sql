﻿CREATE TABLE [dbo].[InterfaceLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Interface] [varchar](50) NULL,
	[InterfaceMessageID] [uniqueidentifier] NOT NULL,
	[MessageType] [nvarchar](max) NULL,
	[LogType] [varchar](50) NULL,
	[PatientNumber] [varchar](50) NULL,
	[Message] [varchar](200) NULL,
	[LogReason] [nvarchar](max) NULL,
	[MessageData] [nvarchar](max) NULL,
	[SQLData] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
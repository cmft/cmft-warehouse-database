﻿CREATE TABLE [dbo].[WRKSHEET](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DRUGNAME] [varchar](30) NOT NULL,
	[ROUTE] [varchar](15) NOT NULL,
	[STORAGE] [varchar](3) NULL,
	[EXPIRY] [numeric](3, 0) NULL,
	[TEXT] [varchar](4000) NULL,
	[VEHICLE] [varchar](60) NULL,
	[DILUENT] [varchar](20) NULL,
	[STORAGEL] [varchar](3) NULL,
	[PLCODE] [varchar](3) NULL
) ON [PRIMARY]
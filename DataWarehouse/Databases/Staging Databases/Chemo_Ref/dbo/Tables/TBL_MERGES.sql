﻿CREATE TABLE [dbo].[TBL_MERGES](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DATE_REQ] [datetime] NULL,
	[NAME] [varchar](200) NULL,
	[RETAINED] [varchar](20) NULL,
	[NON_RETAINED] [varchar](20) NULL,
	[DATE_TRX] [datetime] NULL,
	[USERID] [varchar](12) NULL
) ON [PRIMARY]
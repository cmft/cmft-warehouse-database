﻿CREATE TABLE [dbo].[Subscriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_NO] [varchar](20) NOT NULL,
	[SCI] [nchar](20) NOT NULL,
	[SUB_ID] [nchar](200) NOT NULL,
	[LastRun] [datetime] NOT NULL
) ON [PRIMARY]
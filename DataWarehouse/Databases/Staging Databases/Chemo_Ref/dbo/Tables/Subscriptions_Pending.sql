﻿CREATE TABLE [dbo].[Subscriptions_Pending](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PAT_NO] [varchar](20) NOT NULL,
	[STATUS] [nchar](20) NOT NULL,
	[ROWGUID] [uniqueidentifier] NOT NULL,
	[CREATED] [datetime] NOT NULL,
	[SENDAS] [varchar](3) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[GP](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GP_REFNO] [varchar](20) NOT NULL,
	[GP_NAME] [varchar](200) NOT NULL,
	[GP_PRACTIC] [varchar](100) NULL,
	[GP_ADDR1] [varchar](100) NULL,
	[GP_ADDR2] [varchar](100) NULL,
	[GP_TOWN] [varchar](100) NULL,
	[GP_PCODE] [varchar](20) NULL,
	[GP_PHONE] [varchar](20) NULL,
	[GP_FUNDHLD] [varchar](1) NULL,
	[GP_TYPE] [varchar](1) NULL,
	[GP_SURGERYCODE] [varchar](10) NULL
) ON [PRIMARY]
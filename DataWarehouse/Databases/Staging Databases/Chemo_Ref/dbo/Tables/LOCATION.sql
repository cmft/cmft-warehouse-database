﻿CREATE TABLE [dbo].[LOCATION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CID] [varchar](10) NULL,
	[CODE] [varchar](6) NULL,
	[LOCNAME] [varchar](20) NULL,
	[IP] [bit] NULL,
	[OP] [bit] NULL,
	[DC] [bit] NULL,
	[PHARM] [varchar](10) NULL,
	[TRCODE] [varchar](3) NULL CONSTRAINT [DF_LOCATION_TRCODE]  DEFAULT ('')
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[COIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InterfaceName] [nchar](10) NOT NULL,
	[InterfaceType] [nchar](10) NOT NULL,
	[Status] [nchar](10) NOT NULL,
	[ConnectionType] [nchar](10) NOT NULL,
	[MessageType] [nchar](10) NOT NULL,
	[Address] [nchar](20) NOT NULL,
	[Port] [nchar](10) NULL,
	[TimerInterval] [float] NULL,
	[UserName] [nchar](20) NULL,
	[Password] [nchar](20) NULL,
	[Hospital] [nchar](20) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Tbl_WS_TemplateLoc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Template_Id] [uniqueidentifier] NOT NULL,
	[PharmLoc] [char](3) NOT NULL
) ON [PRIMARY]
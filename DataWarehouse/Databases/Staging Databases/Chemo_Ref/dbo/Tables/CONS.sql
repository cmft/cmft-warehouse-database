﻿CREATE TABLE [dbo].[CONS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RC_REFNO] [varchar](6) NOT NULL,
	[RC_NAME] [varchar](30) NOT NULL,
	[RC_HOSP] [varchar](30) NULL,
	[RC_ADDR1] [varchar](30) NULL,
	[RC_ADDR2] [varchar](30) NULL,
	[RC_TOWN] [varchar](30) NULL,
	[RC_PCODE] [varchar](8) NULL,
	[RC_PHONE] [varchar](12) NULL,
	[ARCHIVE] [bit] NULL
) ON [PRIMARY]
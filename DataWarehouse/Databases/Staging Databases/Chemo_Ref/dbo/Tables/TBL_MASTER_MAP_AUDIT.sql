﻿CREATE TABLE [dbo].[TBL_MASTER_MAP_AUDIT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[USERNAME] [varchar](50) NOT NULL,
	[USER_ID] [varchar](50) NULL,
	[SECURITY_LEV] [int] NULL,
	[RECORD_NUMBER] [int] NOT NULL,
	[OTHER_NO] [varchar](50) NULL,
	[ONCOLOGY_NO] [varchar](20) NULL,
	[DATE_TIME] [datetime] NOT NULL,
	[ACTION] [varchar](50) NOT NULL
) ON [PRIMARY]
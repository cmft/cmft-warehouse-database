﻿CREATE TABLE [dbo].[DILUENT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TEXT] [varchar](20) NOT NULL,
	[VIAL_SIZE] [numeric](6, 1) NULL,
	[MANU] [varchar](12) NULL,
	[BATCHNO] [varchar](7) NULL,
	[EXPIRY] [datetime] NULL,
	[PLCODE] [varchar](3) NULL
) ON [PRIMARY]
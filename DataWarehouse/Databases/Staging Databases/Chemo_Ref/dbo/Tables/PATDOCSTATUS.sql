﻿CREATE TABLE [dbo].[PATDOCSTATUS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Districtno] [varchar](20) NULL,
	[DocName] [varchar](256) NULL,
	[DocStatus] [char](1) NULL,
	[ConfirmUser] [varchar](20) NULL,
	[ConfirmDateTime] [datetime] NULL,
	[careno] [char](2) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_WS_Fields_Groups](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[GroupCode] [char](3) NOT NULL,
	[GroupDescription] [varchar](50) NOT NULL,
	[GroupOrder] [int] NOT NULL
) ON [PRIMARY]
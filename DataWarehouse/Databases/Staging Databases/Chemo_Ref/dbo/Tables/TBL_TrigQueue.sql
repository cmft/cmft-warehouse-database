﻿CREATE TABLE [dbo].[TBL_TrigQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Districtno] [varchar](20) NULL,
	[CareNo] [varchar](2) NULL,
	[Regime] [varchar](16) NULL,
	[Cycle] [numeric](5, 0) NULL,
	[Level] [varchar](6) NOT NULL,
	[Processed] [bit] NOT NULL,
	[Error] [varchar](200) NULL,
	[Created] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[HYPERLINKS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[Text] [varchar](200) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_WS_LabelSizes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[LabelId] [uniqueidentifier] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Page_Width] [float] NULL,
	[Page_Height] [float] NULL,
	[Margin_Top] [float] NULL,
	[Margin_Bottom] [float] NULL,
	[MarginLeft] [float] NULL,
	[MarginRight] [float] NULL,
	[Orientation] [int] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[TBL_PAS_UPDATEABLE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PRIMARY_PAS] [varchar](20) NOT NULL,
	[SECONDARY_PAS] [varchar](20) NOT NULL
) ON [PRIMARY]
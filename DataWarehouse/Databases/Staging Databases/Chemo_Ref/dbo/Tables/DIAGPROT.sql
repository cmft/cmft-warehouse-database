﻿CREATE TABLE [dbo].[DIAGPROT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PROTID] [varchar](10) NOT NULL,
	[DIAGCODE] [varchar](6) NOT NULL,
	[HRG] [varchar](15) NULL
) ON [PRIMARY]
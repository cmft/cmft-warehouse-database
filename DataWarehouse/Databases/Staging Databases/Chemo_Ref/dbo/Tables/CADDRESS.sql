﻿CREATE TABLE [dbo].[CADDRESS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DISTRICTNO] [varchar](20) NULL,
	[CUR_ADDR1] [varchar](30) NULL,
	[CUR_ADDR2] [varchar](30) NULL,
	[CUR_ADDR3] [varchar](30) NULL,
	[CUR_ADDR4] [varchar](30) NULL,
	[CUR_POST] [varchar](8) NULL,
	[CUR_HPHONE] [varchar](17) NULL,
	[CUR_WPHONE] [varchar](17) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[RealParents](
	[dateCreated] [datetime] NULL,
	[AccountID] [uniqueidentifier] NOT NULL,
	[AccountName] [nvarchar](100) NULL,
	[appl_ContactIdName] [nvarchar](160) NULL,
	[ContactId] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[SourceDest](
	[s] [nvarchar](max) NOT NULL DEFAULT ('*'),
	[i] [nvarchar](max) NOT NULL,
	[f] [nvarchar](max) NOT NULL,
	[Created] [datetime2](3) NULL CONSTRAINT [DF_dbo_SourceDest_Created]  DEFAULT (getdate()),
	[ByWhom] [varchar](50) NULL CONSTRAINT [DF_dbo_SourceDest_ByWhom]  DEFAULT (suser_name())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
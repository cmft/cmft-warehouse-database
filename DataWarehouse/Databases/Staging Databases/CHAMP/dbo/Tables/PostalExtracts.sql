﻿CREATE TABLE [dbo].[PostalExtracts](
	[DateTimeOfExtract] [datetime2](7) NOT NULL,
	[AssessmentDate] [datetime2](7) NOT NULL,
	[McKessonID] [int] NULL,
	[Forename] [varchar](35) NOT NULL,
	[Surname] [varchar](35) NOT NULL,
	[NHSNumber] [varchar](10) NULL,
	[SchoolCode] [varchar](10) NULL
) ON [PRIMARY]
﻿CREATE TABLE [ReportBase].[ContactUsRequest](
	[id] [int] NOT NULL,
	[timeOfContactUsRequest] [datetime2](7) NOT NULL,
	[email] [varchar](255) NOT NULL,
	[content] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [ReportBase].[BMICalculatorUsers](
	[date] [datetime] NULL,
	[time] [datetime] NULL,
	[s-ip] [varchar](18) NULL,
	[cs-method] [varchar](128) NULL,
	[cs-uri-stem] [varchar](100) NULL,
	[cs-uri-query] [varchar](2048) NULL,
	[s-port] [bigint] NULL,
	[cs-username] [varchar](62) NULL,
	[c-ip] [varchar](30) NULL,
	[cs(User-Agent)] [varchar](418) NULL,
	[sc-status] [bigint] NULL,
	[sc-substatus] [bigint] NULL,
	[sc-win32-status] [bigint] NULL,
	[time-taken] [bigint] NULL
) ON [PRIMARY]
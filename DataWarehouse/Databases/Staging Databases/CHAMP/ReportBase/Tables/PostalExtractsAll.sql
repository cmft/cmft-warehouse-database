﻿CREATE TABLE [ReportBase].[PostalExtractsAll](
	[DateTimeOfExtract] [datetime2](7) NOT NULL,
	[LineNumber] [int] NOT NULL,
	[AssessmentDate] [datetime2](7) NOT NULL,
	[McKessonID] [int] NULL,
	[Forename] [varchar](35) NOT NULL,
	[Surname] [varchar](35) NOT NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[Postcode] [varchar](10) NULL,
	[NHSNumber] [varchar](10) NULL,
	[SchoolCode] [varchar](10) NULL,
	[YearStartSept] [smallint] NULL,
	[SourceTable] [varchar](20) NOT NULL
) ON [PRIMARY]
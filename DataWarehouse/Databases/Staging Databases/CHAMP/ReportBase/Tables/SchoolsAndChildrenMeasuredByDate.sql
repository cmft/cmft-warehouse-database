﻿CREATE TABLE [ReportBase].[SchoolsAndChildrenMeasuredByDate](
	[Schoolcode] [nvarchar](20) NULL,
	[School] [nvarchar](100) NULL,
	[YearGroup] [varchar](9) NOT NULL,
	[AssessmentDate] [date] NULL,
	[Number Of Children Measured] [int] NULL,
	[Total Number Of Children] [int] NULL,
	[Number Of Children OptedOut] [int] NULL,
	[Percent Measured] [float] NULL
) ON [PRIMARY]
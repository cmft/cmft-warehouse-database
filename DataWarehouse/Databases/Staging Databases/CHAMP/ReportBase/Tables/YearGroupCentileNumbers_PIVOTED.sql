﻿CREATE TABLE [ReportBase].[YearGroupCentileNumbers_PIVOTED](
	[YearGroup] [varchar](9) NOT NULL,
	[Excluded] [int] NULL,
	[91st-98th] [int] NULL,
	[98th-99.6th] [int] NULL,
	[Over 99.6th] [int] NULL
) ON [PRIMARY]
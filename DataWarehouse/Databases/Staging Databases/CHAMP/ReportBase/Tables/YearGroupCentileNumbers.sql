﻿CREATE TABLE [ReportBase].[YearGroupCentileNumbers](
	[YearGroup] [varchar](9) NOT NULL,
	[Classification] [varchar](11) NULL,
	[ClassificationOrder] [int] NULL,
	[Number Of Children] [int] NULL
) ON [PRIMARY]
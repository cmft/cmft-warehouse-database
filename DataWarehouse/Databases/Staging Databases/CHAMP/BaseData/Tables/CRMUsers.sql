﻿CREATE TABLE [BaseData].[CRMUsers](
	[SystemUserId] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](64) NULL,
	[lastname] [nvarchar](64) NULL,
	[DomainName] [nvarchar](1024) NOT NULL
) ON [PRIMARY]
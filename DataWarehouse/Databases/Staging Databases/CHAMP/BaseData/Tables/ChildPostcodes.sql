﻿CREATE TABLE [BaseData].[ChildPostcodes](
	[ContactId] [uniqueidentifier] NOT NULL,
	[Postcode] [varchar](10) NULL
) ON [PRIMARY]
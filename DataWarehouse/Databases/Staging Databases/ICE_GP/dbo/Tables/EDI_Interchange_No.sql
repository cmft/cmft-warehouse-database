﻿CREATE TABLE [dbo].[EDI_Interchange_No](
	[Ref_Index] [int] NOT NULL,
	[EDI_Msg_Format] [varchar](16) NOT NULL DEFAULT ('None'),
	[EDI_Last_Interchange] [int] NULL
) ON [PRIMARY]
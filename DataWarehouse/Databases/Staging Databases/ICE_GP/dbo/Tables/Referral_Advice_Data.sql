﻿CREATE TABLE [dbo].[Referral_Advice_Data](
	[id] [int] NOT NULL,
	[user_id] [varchar](10) NOT NULL,
	[role] [smallint] NOT NULL,
	[post_dt] [datetime] NOT NULL,
	[msg] [varchar](7000) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_Requests_AuditTrail](
	[Audit_Index] [int] IDENTITY(1,1) NOT NULL,
	[Service_Request_Index] [int] NULL,
	[Service_Report_Index] [int] NULL,
	[Audit_Username] [varchar](50) NULL,
	[Action_Type] [varchar](50) NULL,
	[Action_Description] [varchar](255) NULL,
	[Contact_Name] [varchar](35) NULL,
	[Contact_Number] [varchar](35) NULL,
	[Request_Priority] [varchar](6) NULL,
	[Output_Mode] [int] NULL,
	[Date_Added] [datetime] NULL,
	[Time_Added] [datetime] NULL
) ON [PRIMARY]
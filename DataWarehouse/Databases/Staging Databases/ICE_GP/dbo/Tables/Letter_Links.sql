﻿CREATE TABLE [dbo].[Letter_Links](
	[Link_ID] [int] IDENTITY(1,1) NOT NULL,
	[href] [varchar](300) NOT NULL,
	[description] [varchar](300) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Whiteboard_Patients](
	[Whiteboard_Index] [int] NOT NULL,
	[Patient_Index] [int] NOT NULL,
	[Patient_Data] [varchar](64) NULL,
	[Arrival_DateTime] [datetime] NOT NULL,
	[Date_Added] [datetime] NOT NULL
) ON [PRIMARY]
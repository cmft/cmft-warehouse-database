﻿CREATE TABLE [dbo].[EMail_Rep_List](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Location_Index] [int] NOT NULL,
	[Clinician_Index] [int] NOT NULL,
	[Service_Report_Index] [int] NOT NULL
) ON [PRIMARY]
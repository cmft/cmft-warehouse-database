﻿CREATE TABLE [dbo].[Clinical_Letter_Paragraph_Replacement_Reason](
	[ReasonIndex] [int] IDENTITY(1,1) NOT NULL,
	[Reason] [varchar](50) NOT NULL
) ON [PRIMARY]
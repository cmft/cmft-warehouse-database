﻿CREATE TABLE [dbo].[Service_CDC_TB_Practices](
	[Practice_Index] [int] NOT NULL,
	[Practice_Ref] [nvarchar](255) NULL,
	[Lead_GP] [nvarchar](255) NULL,
	[Prac_Addr_1] [nvarchar](255) NULL,
	[Prac_Addr_2] [nvarchar](255) NULL,
	[Prac_Addr_3] [nvarchar](255) NULL,
	[Prac_Addr_4] [nvarchar](255) NULL,
	[Prac_Postcode] [nvarchar](255) NULL
) ON [PRIMARY]
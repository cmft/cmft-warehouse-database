﻿CREATE TABLE [dbo].[BloodGroups_EmergencyPermission](
	[Product_Group_Index] [int] NOT NULL,
	[Stock_Blood_Group_Index] [int] NOT NULL,
	[EmergencyBlood] [varchar](10) NOT NULL
) ON [PRIMARY]
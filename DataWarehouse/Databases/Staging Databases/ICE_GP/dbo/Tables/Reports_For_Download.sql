﻿CREATE TABLE [dbo].[Reports_For_Download](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Clinician_National_Code] [varchar](10) NOT NULL,
	[Report_Index] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL,
	[Process_Indicator] [bit] NULL DEFAULT (0),
	[Location_National_Code] [varchar](20) NULL
) ON [PRIMARY]
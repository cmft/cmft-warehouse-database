﻿CREATE TABLE [dbo].[ServerSide_Printer_Groups](
	[Group_Index] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
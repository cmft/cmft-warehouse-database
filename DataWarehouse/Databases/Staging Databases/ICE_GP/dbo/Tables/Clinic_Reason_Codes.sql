﻿CREATE TABLE [dbo].[Clinic_Reason_Codes](
	[Reason_Index] [smallint] IDENTITY(1,1) NOT NULL,
	[Reason_Type] [char](1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_Request_Test_Diagnoses](
	[Service_Request_Id] [int] NOT NULL,
	[Test_Id] [int] NOT NULL,
	[Order] [tinyint] NOT NULL,
	[Diagnoses_Code] [varchar](10) NOT NULL,
	[Is_ICD10_Type] [bit] NOT NULL
) ON [PRIMARY]
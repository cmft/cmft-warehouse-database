﻿CREATE TABLE [dbo].[gp_redundant_keep_for_a_while](
	[gp_National_Code] [varchar](8) NOT NULL,
	[gp_org_national_code] [varchar](6) NULL,
	[gp_name] [varchar](35) NULL,
	[gp_Active] [bit] NOT NULL,
	[Date_Added] [datetime] NULL,
	[email] [varchar](150) NULL
) ON [PRIMARY]
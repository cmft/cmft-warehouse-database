﻿CREATE TABLE [dbo].[Referral_Protocol_Advice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[specialty_id] [int] NOT NULL,
	[description] [varchar](100) NOT NULL,
	[advice] [varchar](7500) NOT NULL,
	[active] [tinyint] NOT NULL
) ON [PRIMARY]
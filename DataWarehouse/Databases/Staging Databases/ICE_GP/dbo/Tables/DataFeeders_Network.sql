﻿CREATE TABLE [dbo].[DataFeeders_Network](
	[DataFeeder_Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Net_Address] [varchar](100) NOT NULL,
	[Has_FF] [bit] NULL,
	[Has_TCP] [bit] NULL,
	[Has_FTP] [bit] NULL,
	[Has_MSMQ] [bit] NULL,
	[Has_SQL] [bit] NULL
) ON [PRIMARY]
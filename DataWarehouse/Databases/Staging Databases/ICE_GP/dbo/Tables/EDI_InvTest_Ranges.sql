﻿CREATE TABLE [dbo].[EDI_InvTest_Ranges](
	[Organisation] [varchar](6) NOT NULL,
	[EDI_InvTest_Code] [varchar](10) NULL,
	[EDI_Spec_Code] [varchar](4) NULL,
	[EDI_Range_MinAge] [int] NULL,
	[EDI_Range_MaxAge] [int] NULL,
	[EDI_Range_Sex] [varchar](1) NULL,
	[EDI_Range_LO] [varchar](10) NULL,
	[EDI_Range_HI] [varchar](10) NULL,
	[EDI_Range_Comment] [varchar](70) NULL,
	[EDI_Range_UOM] [varchar](15) NULL,
	[EDI_Index] [int] NULL
) ON [PRIMARY]
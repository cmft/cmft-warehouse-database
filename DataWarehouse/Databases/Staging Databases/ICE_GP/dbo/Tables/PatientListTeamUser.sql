﻿CREATE TABLE [dbo].[PatientListTeamUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TeamIndex] [int] NOT NULL,
	[UserIndex] [int] NOT NULL,
	[Manager] [bit] NOT NULL,
	[ExpiryDate] [datetime] NULL
) ON [PRIMARY]
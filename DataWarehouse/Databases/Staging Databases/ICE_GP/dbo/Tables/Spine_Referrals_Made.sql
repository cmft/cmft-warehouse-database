﻿CREATE TABLE [dbo].[Spine_Referrals_Made](
	[UBRN] [varchar](15) NOT NULL,
	[ReferralDateTime] [datetime] NOT NULL,
	[PatientNHSnumber] [varchar](15) NOT NULL,
	[GPnationalcode] [varchar](15) NOT NULL,
	[GPpracticecode] [varchar](15) NOT NULL,
	[Priority] [tinyint] NULL
) ON [PRIMARY]
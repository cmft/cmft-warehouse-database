﻿CREATE TABLE [dbo].[Offline_Device_Register](
	[Offline_Device_ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [varchar](50) NOT NULL,
	[Alias] [varchar](20) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Request_Sample_Panels](
	[Sample_Panel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Panel_Name] [varchar](50) NOT NULL,
	[Danger] [bit] NOT NULL,
	[organisation] [varchar](6) NOT NULL,
	[Clinical_Details_Display] [bit] NOT NULL CONSTRAINT [DF__Request_S__Clini__460A0B1A]  DEFAULT (0),
	[Freetext_Maximum_Length] [smallint] NULL CONSTRAINT [DF_Request_Sample_Panels_Freetext_Maximum_Length]  DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Clinical_Letter_Paragraph_Templates_Regular_Terms](
	[Term_Index] [int] IDENTITY(1,1) NOT NULL,
	[Paragraph_Template_Index] [int] NOT NULL,
	[Item_Text] [text] NOT NULL,
	[Specialty] [smallint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Patient_Extra_Data_Keys](
	[Key_Index] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Order] [tinyint] NOT NULL,
	[Display] [bit] NOT NULL
) ON [PRIMARY]
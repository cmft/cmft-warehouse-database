﻿CREATE TABLE [dbo].[DataFeeders_ReplayLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FeedId] [int] NOT NULL,
	[FeedType] [char](1) NOT NULL,
	[Message] [text] NOT NULL,
	[CorrelationId] [uniqueidentifier] NULL,
	[FailReason] [text] NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_DataFeeders_ReplayLogs_Created]  DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Request_Priority](
	[Priority_ID] [int] IDENTITY(1,1) NOT NULL,
	[Priority_Desc] [varchar](20) NOT NULL,
	[Priority_Urgent] [bit] NOT NULL,
	[Priority_Default] [bit] NOT NULL,
	[Priority_Active] [bit] NOT NULL,
	[Colour_Index] [int] NOT NULL,
	[Priority_Value] [varchar](8) NULL,
	[Organisation] [varchar](6) NOT NULL
) ON [PRIMARY]
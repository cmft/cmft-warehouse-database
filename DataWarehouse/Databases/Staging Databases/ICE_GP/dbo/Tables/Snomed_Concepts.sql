﻿CREATE TABLE [dbo].[Snomed_Concepts](
	[ConceptId] [varchar](9) NOT NULL,
	[ConceptStatus] [tinyint] NOT NULL,
	[FullySpecifiedName] [varchar](255) NOT NULL,
	[CTV3ID] [varchar](5) NOT NULL,
	[SnomedID] [varchar](8) NOT NULL,
	[IsPrimitive] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Whiteboard_Templates](
	[Index] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](32) NOT NULL,
	[Description] [varchar](256) NULL,
	[File] [varchar](128) NOT NULL
) ON [PRIMARY]
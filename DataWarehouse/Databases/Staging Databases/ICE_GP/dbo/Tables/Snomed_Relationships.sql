﻿CREATE TABLE [dbo].[Snomed_Relationships](
	[RelationshipID] [varchar](10) NOT NULL,
	[ConceptId1] [varchar](9) NOT NULL,
	[RelationshipType] [varchar](9) NOT NULL,
	[ConceptId2] [varchar](9) NOT NULL,
	[CharacteristicType] [tinyint] NOT NULL,
	[Refinability] [tinyint] NOT NULL,
	[RelationshipGroup] [tinyint] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Interop_Services](
	[service] [varchar](50) NOT NULL,
	[url] [varchar](200) NOT NULL,
	[description] [varchar](200) NOT NULL,
	[requery] [bit] NOT NULL CONSTRAINT [DF_Interop_Services_requery]  DEFAULT (0),
	[findpatient] [bit] NOT NULL CONSTRAINT [DF_Interop_Services_findpatient]  DEFAULT (0),
	[addpatient] [bit] NOT NULL CONSTRAINT [DF_Interop_Services_addpatient]  DEFAULT (0),
	[menuname] [varchar](30) NULL,
	[active] [bit] NOT NULL,
	[applicationid] [int] NULL,
	[updatepatient] [bit] NOT NULL DEFAULT (0),
	[id] [smallint] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_Requests_Priorities](
	[Priority_Code] [varchar](6) NULL,
	[Priority_Description] [varchar](25) NULL,
	[Screen_Order] [int] NULL,
	[Org_Code] [varchar](6) NULL
) ON [PRIMARY]
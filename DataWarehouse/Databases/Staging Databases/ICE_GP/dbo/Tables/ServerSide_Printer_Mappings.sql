﻿CREATE TABLE [dbo].[ServerSide_Printer_Mappings](
	[Mapping_Index] [int] IDENTITY(1,1) NOT NULL,
	[Remote_Index] [int] NULL,
	[Profile_Index] [int] NOT NULL,
	[Type] [varchar](5) NOT NULL,
	[Printer_Index] [int] NOT NULL
) ON [PRIMARY]
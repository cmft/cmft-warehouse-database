﻿CREATE TABLE [dbo].[Request_Panels_Pages](
	[PanelID] [smallint] NULL,
	[PageName] [varchar](15) NULL,
	[Sequence] [smallint] NULL,
	[Page_ID] [int] IDENTITY(1,1) NOT NULL,
	[Restricted] [bit] NOT NULL
) ON [PRIMARY]
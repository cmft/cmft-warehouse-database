﻿CREATE TABLE [dbo].[EventData](
	[KeyID] [int] NOT NULL,
	[Value] [text] NOT NULL,
	[Event_ID] [int] NOT NULL DEFAULT (0),
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HashOfValue] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
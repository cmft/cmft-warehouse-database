﻿CREATE TABLE [dbo].[Request_Conditions](
	[Condition_Index] [int] IDENTITY(1,1) NOT NULL,
	[Condition] [varchar](100) NOT NULL,
	[Org_Code] [varchar](5) NOT NULL,
	[Active] [bit] NOT NULL,
	[Type] [char](1) NOT NULL,
	[SnomedCode] [varchar](12) NULL,
	[Condition_HelpText] [varchar](1000) NULL,
	[Condition_HelpURL] [varchar](500) NULL
) ON [PRIMARY]
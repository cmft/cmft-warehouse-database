﻿CREATE TABLE [dbo].[ICD10](
	[ICD10_Code] [varchar](4) NOT NULL,
	[ICD10_Desc_1] [varchar](60) NOT NULL,
	[ICD10_Desc_2] [varchar](60) NOT NULL,
	[ICD10_Dagger] [smallint] NULL,
	[ICD10_Dual_Classn_1] [varchar](4) NULL,
	[ICD10_Dual_Classn_2] [varchar](4) NULL,
	[ICD10_Dual_Classn_3] [varchar](4) NULL,
	[ICD10_Dual_Classn_4] [varchar](4) NULL,
	[ICD10_Dual_Classn_5] [varchar](4) NULL,
	[ICD10_Sex] [varchar](1) NULL,
	[ICD10_Min_Age] [smallint] NULL,
	[ICD10_Max_Age] [smallint] NULL
) ON [PRIMARY]
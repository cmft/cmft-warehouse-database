﻿CREATE TABLE [dbo].[PatientList](
	[ListIndex] [int] IDENTITY(1,1) NOT NULL,
	[ListType] [tinyint] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Owner] [int] NOT NULL,
	[Refresh] [int] NOT NULL,
	[Timeout] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[LastUpdate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[MasterListIndex] [int] NULL,
	[IsPublic] [bit] NOT NULL DEFAULT ((0)),
	[ShowPendingRemovals] [bit] NOT NULL DEFAULT ((0)),
	[ClinicalFormTemplateIndex] [int] NULL,
	[Organisation] [varchar](6) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Clinical_Headings](
	[Heading_ID] [int] NULL,
	[Heading_Text] [varchar](50) NULL,
	[Heading_Type] [varchar](50) NULL,
	[Heading_Read] [varchar](5) NULL,
	[Heading_CIT] [varchar](3) NULL
) ON [PRIMARY]
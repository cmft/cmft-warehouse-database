﻿CREATE TABLE [dbo].[Service_Results_Comments](
	[Result_Comment_Index] [int] IDENTITY(1,1) NOT NULL,
	[Service_Result_Index] [int] NOT NULL,
	[Service_Result_Comment] [varchar](140) NULL,
	[Comment_Type] [varchar](2) NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
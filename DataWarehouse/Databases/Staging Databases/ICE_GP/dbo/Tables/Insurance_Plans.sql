﻿CREATE TABLE [dbo].[Insurance_Plans](
	[Plan_Index] [int] IDENTITY(1,1) NOT NULL,
	[Carrier_Index] [int] NOT NULL,
	[Code] [varchar](8) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Address_Line1] [varchar](100) NOT NULL,
	[Address_Line2] [varchar](100) NOT NULL,
	[Address_Line3] [varchar](100) NOT NULL,
	[Address_Line4] [varchar](100) NOT NULL,
	[PostCode] [varchar](10) NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
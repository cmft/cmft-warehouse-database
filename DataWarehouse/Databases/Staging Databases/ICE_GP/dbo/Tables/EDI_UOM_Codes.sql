﻿CREATE TABLE [dbo].[EDI_UOM_Codes](
	[EDI_Local_UOM] [varchar](30) NOT NULL,
	[EDI_Nat_UOM] [varchar](30) NULL,
	[index] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
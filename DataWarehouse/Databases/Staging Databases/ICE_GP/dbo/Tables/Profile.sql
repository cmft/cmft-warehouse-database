﻿CREATE TABLE [dbo].[Profile](
	[Profile_Index] [int] IDENTITY(1,1) NOT NULL,
	[Profile_Name] [varchar](60) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Notes] [varchar](200) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalFormFactorInGroup](
	[FactorId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[Order] [int] NOT NULL
) ON [PRIMARY]
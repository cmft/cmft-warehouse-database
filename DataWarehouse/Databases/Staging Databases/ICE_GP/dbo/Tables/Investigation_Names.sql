﻿CREATE TABLE [dbo].[Investigation_Names](
	[Investigation_Name_Index] [int] IDENTITY(1,1) NOT NULL,
	[Investigation_Name] [varchar](60) NOT NULL,
	[Investigation_Order] [int] NULL,
	[Can_Archive] [bit] NOT NULL CONSTRAINT [DF_Investigation_Names_Can_Archive]  DEFAULT (1)
) ON [PRIMARY]
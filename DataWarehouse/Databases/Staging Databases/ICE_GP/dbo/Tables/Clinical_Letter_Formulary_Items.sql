﻿CREATE TABLE [dbo].[Clinical_Letter_Formulary_Items](
	[Item_Index] [int] IDENTITY(1,1) NOT NULL,
	[Read_Code] [varchar](8) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[Custom_Item] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Alert_Type_Index_To_Trigger] [int] NULL DEFAULT (NULL)
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_User_ReportSecurity](
	[ReportSecurity_index] [int] NOT NULL,
	[Report_Type] [varchar](5) NULL,
	[Target_Text] [varchar](100) NULL,
	[Investigation] [varchar](60) NULL,
	[Result_Rubric] [varchar](60) NULL,
	[Result] [varchar](70) NULL,
	[Lower_Limit] [varchar](35) NULL,
	[Upper_Limit] [varchar](35) NULL,
	[Review_Level] [int] NULL,
	[Comment_Level] [int] NULL,
	[Active] [int] NULL
) ON [PRIMARY]
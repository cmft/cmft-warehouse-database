﻿CREATE TABLE [dbo].[dbVersion](
	[Version] [int] NOT NULL,
	[LastAmended] [datetime] NOT NULL,
	[ReleaseName] [varchar](10) NOT NULL,
	[InstalledHotfixes] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]
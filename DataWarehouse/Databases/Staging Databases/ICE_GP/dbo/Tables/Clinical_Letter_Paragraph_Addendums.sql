﻿CREATE TABLE [dbo].[Clinical_Letter_Paragraph_Addendums](
	[Addendum_Index] [int] IDENTITY(1,1) NOT NULL,
	[Paragraph_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Addendum_Text] [text] NOT NULL,
	[Date_Added] [datetime] NOT NULL,
	[Date_Completed] [datetime] NULL,
	[Date_Modified] [datetime] NULL,
	[Modified] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
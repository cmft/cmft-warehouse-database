﻿CREATE TABLE [dbo].[Clinical_Letter_Edits](
	[Edit_Index] [int] IDENTITY(1,1) NOT NULL,
	[Letter_Index] [int] NOT NULL,
	[Object_Type] [char](1) NOT NULL,
	[Object_Index] [int] NOT NULL,
	[User_Index] [int] NOT NULL,
	[Date_Edited] [datetime] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ClinicalFormTemplateImage](
	[TemplateId] [int] NOT NULL,
	[ImageId] [int] NOT NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]
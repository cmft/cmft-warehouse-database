﻿CREATE TABLE [dbo].[Service_BloodBank](
	[Test_Index] [int] NOT NULL,
	[Test_Code] [varchar](8) NULL,
	[Test_Description] [varchar](65) NULL,
	[Test_Screen_Position] [varchar](10) NULL,
	[Test_Screen_Caption] [varchar](45) NULL,
	[Test_Special_Help] [varchar](65) NULL,
	[Provider_ID] [int] NULL,
	[Read_Code] [varchar](5) NULL,
	[Tube_Code] [varchar](10) NULL,
	[Tube_Volume] [varchar](10) NULL,
	[Enabled] [bit] NOT NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Temp_DCH_EMail_Address](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CODE] [varchar](8) NOT NULL,
	[ADDRESS] [varchar](500) NOT NULL
) ON [PRIMARY]
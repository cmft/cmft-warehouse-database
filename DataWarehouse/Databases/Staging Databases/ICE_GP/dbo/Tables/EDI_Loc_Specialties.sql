﻿CREATE TABLE [dbo].[EDI_Loc_Specialties](
	[Organisation] [varchar](10) NOT NULL,
	[EDI_Nat_Code] [varchar](10) NOT NULL,
	[EDI_Korner_Code] [int] NOT NULL,
	[EDI_Specialty] [varchar](30) NULL,
	[EDI_Msg_Format] [varchar](41) NOT NULL,
	[EDI_Msg_Active] [bit] NOT NULL,
	[EDI_Run_Frequency] [varchar](4) NULL,
	[EDI_Last_Run] [datetime] NULL,
	[EDI_S_Time1] [varchar](5) NULL,
	[EDI_S_Time2] [varchar](5) NULL,
	[EDI_S_Time3] [varchar](5) NULL,
	[EDI_S_Time4] [varchar](5) NULL,
	[EDI_LTS_Index] [int] NOT NULL
) ON [PRIMARY]
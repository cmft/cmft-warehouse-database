﻿CREATE TABLE [dbo].[Blood_Stock_Notes](
	[Note_Index] [int] IDENTITY(1,1) NOT NULL,
	[Stock_Index] [int] NOT NULL,
	[Note_Type] [char](1) NOT NULL,
	[Note] [text] NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
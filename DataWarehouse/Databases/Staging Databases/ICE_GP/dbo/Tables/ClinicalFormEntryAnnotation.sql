﻿CREATE TABLE [dbo].[ClinicalFormEntryAnnotation](
	[AnnotationId] [int] IDENTITY(1,1) NOT NULL,
	[EntryId] [int] NOT NULL,
	[XCoord] [smallint] NOT NULL,
	[YCoord] [smallint] NOT NULL,
	[TypeOfAnnotation] [tinyint] NOT NULL,
	[AnnotationText] [varchar](255) NULL,
	[Height] [smallint] NULL,
	[Width] [smallint] NULL
) ON [PRIMARY]
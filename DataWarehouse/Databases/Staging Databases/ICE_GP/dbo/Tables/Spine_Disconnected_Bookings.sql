﻿CREATE TABLE [dbo].[Spine_Disconnected_Bookings](
	[UBRN] [varchar](15) NULL,
	[Patient_Index] [int] NOT NULL,
	[Patient_Booking_Index] [smallint] NOT NULL,
	[Locked] [bit] NOT NULL,
	[Booking_Active] [bit] NOT NULL,
	[Cancellation_Reason] [tinyint] NULL,
	[Did_Not_Attend_Reason] [tinyint] NULL,
	[Date_Added] [datetime] NOT NULL,
	[Notes] [text] NULL,
	[UARN] [uniqueidentifier] NOT NULL,
	[ReferringGP] [varchar](20) NULL,
	[ReferringGPpractice] [varchar](20) NULL,
	[DateOfReferralDecision] [datetime] NULL,
	[Date_Last_Edited] [datetime] NOT NULL,
	[Urgent] [tinyint] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
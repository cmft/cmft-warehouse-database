﻿CREATE TABLE [dbo].[Transfusion_Bedside_Check_Confirmations](
	[Confirmation_Index] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
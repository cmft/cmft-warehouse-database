﻿CREATE TABLE [dbo].[EventCodes](
	[Code] [int] NOT NULL,
	[EventType] [tinyint] NOT NULL,
	[EventName] [varchar](100) NOT NULL,
	[LifespanDays] [smallint] NOT NULL,
	[Severity] [tinyint] NULL,
	[EventNotes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
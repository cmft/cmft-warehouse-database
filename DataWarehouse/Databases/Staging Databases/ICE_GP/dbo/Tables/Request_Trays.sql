﻿CREATE TABLE [dbo].[Request_Trays](
	[Tray_Index] [int] IDENTITY(1,1) NOT NULL,
	[Test_Index] [int] NULL,
	[Instrument_Index] [int] NULL,
	[Tube_index] [int] NULL,
	[Tray_Name] [varchar](50) NULL
) ON [PRIMARY]
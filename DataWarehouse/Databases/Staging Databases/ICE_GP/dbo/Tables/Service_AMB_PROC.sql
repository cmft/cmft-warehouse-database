﻿CREATE TABLE [dbo].[Service_AMB_PROC](
	[Proc_Index] [int] NOT NULL,
	[Visit_Index] [int] NULL,
	[Proc_Type] [varchar](50) NULL,
	[Status] [varchar](255) NULL,
	[Comment] [varchar](255) NULL,
	[FirstBookingID] [int] NULL,
	[SecondBookingID] [int] NULL,
	[Date_Added] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Service_Battery_Tests](
	[Service_Battery_Index] [int] NOT NULL,
	[Service_Battery_Test_Order] [int] NULL,
	[Service_Battery_Test_Code] [varchar](10) NULL,
	[Service_Battery_Test_Name] [varchar](30) NULL
) ON [PRIMARY]
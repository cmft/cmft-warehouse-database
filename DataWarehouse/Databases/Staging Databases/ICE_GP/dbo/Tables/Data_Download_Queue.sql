﻿CREATE TABLE [dbo].[Data_Download_Queue](
	[DDQ_Index] [int] IDENTITY(1,1) NOT NULL,
	[LocationIndex] [int] NOT NULL,
	[ServiceIndex] [int] NOT NULL,
	[InteropIndex] [int] NULL,
	[Status] [bit] NOT NULL DEFAULT (0),
	[ServiceType] [char](3) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateDownloaded] [datetime] NULL,
	[XMLMessage] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
﻿CREATE TABLE [dbo].[Languages_Phrasebook](
	[Language_Index] [int] NOT NULL,
	[Phrase_Type] [char](1) NULL,
	[Phrase_Key] [varchar](50) NULL,
	[Phrase] [varchar](300) NULL
) ON [PRIMARY]
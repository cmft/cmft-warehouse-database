﻿CREATE TABLE [dbo].[Pending_Merges](
	[index] [int] IDENTITY(1,1) NOT NULL,
	[CorrectIDKey] [int] NOT NULL,
	[IncorrectIDKey] [int] NOT NULL,
	[Delete_Hosp_Nos] [varchar](500) NOT NULL,
	[User_ID] [int] NOT NULL,
	[App_ID] [int] NOT NULL,
	[Date_Added] [datetime] NOT NULL,
	[Date_Complete] [datetime] NULL
) ON [PRIMARY]
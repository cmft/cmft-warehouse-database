﻿CREATE TABLE [dbo].[Mon_RangeVals](
	[RangeID] [int] NOT NULL,
	[RangeIndex] [smallint] NOT NULL,
	[Lower] [varchar](16) NULL,
	[Upper] [varchar](16) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[PA_Patient_Alert_HL7_Trigger_Set](
	[Trigger_Set_Index] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Associated_Alert_Type_Index] [int] NOT NULL,
	[Options] [tinyint] NOT NULL,
	[Active] [bit] NOT NULL,
	[All_Must_Match] [bit] NOT NULL,
	[Total_Set] [bigint] NOT NULL,
	[Total_Cleared] [bigint] NOT NULL,
	[Totals_Last_Reset] [smalldatetime] NOT NULL
) ON [PRIMARY]
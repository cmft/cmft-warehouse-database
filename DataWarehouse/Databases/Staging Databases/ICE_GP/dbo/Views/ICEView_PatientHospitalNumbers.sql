﻿CREATE VIEW [dbo].[ICEView_PatientHospitalNumbers]
as
	select
		p.Patient_Id_Key AS Patient_Id_Key,
		pl.HospitalNumber AS HospitalNumber,
		pl.Org_Code AS Org_Code,
		pl.Main_Identifier AS Main_Identifier,
		pl.Active_Casenote AS Active_Casenote,
		pl.Hidden AS Hidden,
		pl.Retired AS Retired
	from
		patient p
		inner join patient_local_ids pl on p.patient_id_key = pl.patient_id_key
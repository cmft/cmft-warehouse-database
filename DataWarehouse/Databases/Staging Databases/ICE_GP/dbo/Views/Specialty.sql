﻿CREATE VIEW [dbo].[Specialty]
        AS
            SELECT Specialty_Code, Specialty
                FROM dbo.Local_Specialty AS IS1
            UNION
            SELECT Specialty_Code, Specialty
                FROM dbo.CRIR_Specialty AS CS
                    WHERE (Specialty_Code NOT IN 
                        (SELECT Specialty_Code FROM dbo.Local_Specialty AS IS2))
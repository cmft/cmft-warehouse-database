﻿CREATE VIEW [dbo].[ICEView_ReportResults]
as
	select
		r.Investigation_Index AS Investigation_Index, 
		r.Sample_Index AS Sample_Index, 
		r.Result_Index AS Result_Index, 
		r.Result_Code AS Result_Code, 
		r.Result_Rubric AS Result_Name, 
		r.Abnormal_Flag AS Abnormal_Flag, 
		r.UOM_Text AS Unit_Of_Measure, 
		r.Result AS Result
	from
		service_investigations i
		inner join service_results r on i.investigation_index = r.investigation_index
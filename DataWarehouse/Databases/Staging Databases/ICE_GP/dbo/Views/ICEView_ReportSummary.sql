﻿CREATE VIEW [dbo].[ICEView_ReportSummary]
as
	select
		srq.Service_Request_Index AS Service_Request_Index, 
		srq.Hospital_Number AS HospitalNumber, 
		srq.Clinician_Index AS Clinician_Index, 
		srq.Patient_Id_Key AS Patient_Id_Key, 
		srq.Location_Index AS Location_Index, 
		srp.Service_Report_Index AS Service_Report_Index, 
		srp.Service_Report_ID AS Report_ID, 
		srp.Service_Report_Type AS Specialty, 
		srp.DateTime_Of_Report AS DateTime_Of_Report, 
		srp.Status AS Report_Status, 
		srp.Date_Added AS Date_Added, 
		rfr.Reason_Code AS Filed_Reason_Code, 
		rfr.Reason AS Filed_Reason_Description, 
		afr.Filed_Date AS Filed_Date, 
		afr.User_Index AS Filed_User_Index, 
		rhr.Reason_Code AS Hidden_Reason_Code, 
		rhr.Reason AS Hidden_Reason_Description
	from
		service_requests srq
		inner join service_reports srp on srq.service_request_index = srp.service_request_index
		left outer join audit_filed_reports afr on srp.service_report_index = afr.report_index
		left outer join report_filing_reasons rfr on afr.reason_index = rfr.reason_index
		left outer join report_hiding_reasons rhr on srp.category = rfr.reason_index
	where
		srq.status = 'RR'
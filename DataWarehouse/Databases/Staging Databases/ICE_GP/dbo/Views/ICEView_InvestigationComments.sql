﻿CREATE VIEW [dbo].[ICEView_InvestigationComments]
as
	select
		sic.Service_Investigation_Index AS Investigation_Index,
		sic.Service_Investigation_Comment AS Comment
	from
		service_invest_comments sic
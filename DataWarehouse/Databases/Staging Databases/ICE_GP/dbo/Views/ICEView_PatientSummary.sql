﻿CREATE VIEW [dbo].[ICEView_PatientSummary]
as
select
p.Patient_Id_Key AS Patient_Id_Key, 
p.New_NHS_No AS New_NHS_No,
p.NNN_Status_Indicator AS NNN_Status_Indicator, 
p.Date_Of_Birth AS Date_Of_Birth, 
p.Date_Of_Death AS Date_Of_Death, 
ceg.Ethnic_Classn AS Ethnic_Group_Description, 
cms.Marital_Status AS Marital_Status_Description, 
p.Sex AS Sex, 
p.Registerd_Gp AS Registered_Gp_Code, 
p.Practice_ID AS Registered_Gp_Practice_Code, 
p.Title AS Title, 
p.Surname AS Surname, 
p.Midname AS Midname, 
p.Forename AS Forename, 
p.Pat_Addr_Line1 AS Address_Line1, 
p.Pat_Addr_Line2 AS Address_Line2, 
p.Pat_Addr_Line3 AS Address_Line3, 
p.Pat_Addr_Line4 AS Address_Line4, 
p.Pat_Addr_Line5 AS Address_Line5, 
p.Pat_Postcode AS Postcode, 
p.Date_Added AS Date_Added, 
p.Date_Last_Amended AS Date_Last_Amended, 
p.Master_Patient_Id_Key AS Master_Patient_Id_Key, 
p.Master_Record AS Master_Record, 
p.Deceased_Flag AS Deceased_Flag, 
pl.HospitalNumber AS Primary_Hospital_Number,
p.SSN,
l.Language_Name,
r.Race,
p.Email_Address,
p.Mobile_Phone,
Case p.Payer_Preference
When 'C' Then 'Client Pay'
When 'I' Then 'Insurance'
When 'S' Then 'Self Pay'
End as Payer_Preference
from
Patient p
left outer join Race_Lookup r on r.race_index = p.Race
left outer join Languages l on p.Primary_Language = l.language_index
left outer join Patient_Local_IDs pl on p.patient_id_key = pl.patient_id_key and pl.main_identifier = 1 and pl.hidden = 0 and pl.retired = 0
left outer join CRIR_Ethnic_Group ceg on p.ethnic_group = ceg.ethnic_code
left outer join CRIR_Marital_Status cms on p.marital_status = cms.marital_code
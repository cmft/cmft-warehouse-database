﻿CREATE VIEW [dbo].[ICEView_ReportInvestigations]
as
	select
		sr.Service_Report_Index AS Service_Report_Index, 
		i.Sample_Index AS Sample_Index, 
		i.Investigation_Index AS Investigation_Index, 
		i.Investigation_Code AS Investigation_Code, 
		i.Investigation_Requested AS Investigation_Name
	from
		service_reports sr
		inner join service_investigations i on sr.service_report_index = i.service_report_index
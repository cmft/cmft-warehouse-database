﻿CREATE VIEW [dbo].[ICEView_OrderTests]
as
	select
		sr.Service_Request_Index AS Service_Request_Index, 
		st.Service_Request_Test_Index AS Service_Request_Test_Index, 
		case st.Test_Status 
			when 1 then 'Requested'
			when 2 then 'Included as part of another test'
			when 3 then 'Reflexed'
			when 4 then 'No longer included in the order'
			else 'UNKNOWN'
		end AS Status, 
		rt.Test_Code AS Test_Code, 
		rt.Screen_Caption AS Test_Name,
		rt.Test_Index,
		rt.Request_Validity_Period,
		st.Repeat_Reason
	from
		service_requests sr
		inner join service_request_test st on sr.service_request_index = st.service_request_id
		inner join request_tests rt on st.test_id = rt.test_index
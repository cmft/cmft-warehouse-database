﻿CREATE VIEW [dbo].[ICEView_ReportComments]
as
	select
		src.Service_Report_Index AS Service_Report_Index, 
		src.Service_Report_Comment AS Comment
	from
		service_reports_comments src
﻿CREATE VIEW [dbo].[ICEView_ResultComments]
as
	select
		src.Service_Result_Index AS Result_Index, 
		src.Service_Result_Comment AS Comment
	from
		service_results_comments src
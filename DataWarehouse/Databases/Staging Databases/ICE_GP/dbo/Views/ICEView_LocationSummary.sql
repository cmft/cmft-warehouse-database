﻿CREATE VIEW [dbo].[ICEView_LocationSummary]
as
	select
		l.Location_Index AS Location_Index, 
		l.Clinic_Name AS Name, 
		t.Location_Type AS Type, 
		l.Location_Active AS Active, 
		l.Date_Added AS Date_Added, 
		l.Location_Nat_Code AS National_Code, 
		l.Organisation_Code AS Org_Code, 
		l.Location_Specialty AS Specialty_Code, 
		s.Specialty AS Specialty_Description, 
		l.Address_Line1 AS Address_Line1, 
		l.Address_Line2 AS Address_Line2, 
		l.Address_Line3 AS Address_Line3, 
		l.Address_Line4 AS Address_Line4, 
		l.Postcode AS Postcode, 
		l.Telephone AS Telephone, 
		l.Master_Location_Index AS Master_Location_Index, 
		l.Master_Record AS Master_Record, 
		l.Practice AS GP_Practice_Ind, 
		l.Site_Code AS Site_Code
	from
		location l
		left outer join location_types t on l.clinic_purpose = t.location_type_index
		left outer join specialty s on l.location_specialty = cast(s.specialty_code as varchar)
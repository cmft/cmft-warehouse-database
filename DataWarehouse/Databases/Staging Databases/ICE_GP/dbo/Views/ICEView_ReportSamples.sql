﻿CREATE VIEW [dbo].[ICEView_ReportSamples]
as
	select
		sr.Service_Report_Index AS Service_Report_Index, 
		ss.Sample_Index AS Sample_Index, 
		ss.Sample_Id AS Sample_Number, 
		ss.Sample_Code AS Sample_Code, 
		ss.Sample_Text AS Sample_Text, 
		ss.Collection_DateTime AS Collection_DateTime
	from
		service_reports sr
		inner join service_samples ss on sr.service_request_index = ss.service_request_index
﻿CREATE VIEW [dbo].[ICEView_OrderSummary]
as
      select
            sr.Service_Request_Index AS Service_Request_Index, 
            sr.Hospital_Number AS HospitalNumber, 
            sr.DateTime_Of_Request AS Date_Last_Edited, 
            sr.Date_Added AS Date_Added, 
            sr.DateTime_Received,
            sr.Status AS Status, 
            sr.Clinician_Index AS Clinician_Index, 
            sr.Patient_Id_Key AS Patient_Id_Key, 
            sr.Location_Index AS Location_Index, 
            srd.Cat AS Category, 
            srd.Ctm AS Collection_Time, 
            srd.Cdt AS Collection_Date, 
            srd.Sid AS Order_Accession_Number, 
            sr.Requested_By AS User_Index, 
            srd.Third_Party_Id AS Third_Party_Id, 
            srd.Verification_Status AS Verification_Status, 
            rp.Priority_Desc AS Priority, 
            lr.Room_Code AS Location_Room_Code, 
            sp.Provider_ID,
            sp.Provider_Name AS Provider,
            srd.sample_Option_Index as Option_Index
      from
            service_requests sr 
            inner join service_requests_details srd on sr.service_request_index = srd.request_index
            inner join request_priority rp on srd.Pri_Index = rp.Priority_ID
            inner join service_providers sp on sr.service_provider_id = sp.provider_id
            left outer join location_rooms lr on srd.Location_Room_Index = lr.room_index
      where
            not sr.request_id is null
﻿CREATE TABLE [dbo].[Division](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DivisionName] [nvarchar](50) NULL,
	[ContactName] [nvarchar](50) NULL,
	[ContactEmail] [nvarchar](50) NULL,
	[ContactTelephone] [nvarchar](50) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CostCode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CostCode] [nvarchar](50) NULL,
	[CodeName] [nvarchar](50) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
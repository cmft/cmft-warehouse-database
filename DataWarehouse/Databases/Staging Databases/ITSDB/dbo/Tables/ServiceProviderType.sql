﻿CREATE TABLE [dbo].[ServiceProviderType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ServiceProviderTypeName] [nvarchar](50) NULL
) ON [PRIMARY]
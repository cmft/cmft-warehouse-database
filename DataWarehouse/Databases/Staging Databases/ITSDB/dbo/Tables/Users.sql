﻿CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Firstname] [nvarchar](25) NULL,
	[Lastname] [nvarchar](25) NULL,
	[UserLogin] [nvarchar](70) NULL,
	[Active] [bit] NULL,
	[UserTypeID] [int] NULL,
	[InterpreterID] [int] NULL,
	[TelephoneNo] [nvarchar](50) NULL,
	[EmailAddress] [nvarchar](50) NULL,
	[Password] [varbinary](128) NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ServiceProviderLanguage](
	[ServiceProviderID] [int] NOT NULL,
	[LanguageID] [int] NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[BankHoliday](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY]
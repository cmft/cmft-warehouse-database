﻿CREATE TABLE [dbo].[Interpreter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[EMailAddress] [nvarchar](50) NULL,
	[ServiceProviderID] [int] NULL,
	[InterpreterTypeID] [int] NULL,
	[HourlyRate] [decimal](10, 2) NULL,
	[Comments] [nvarchar](1000) NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
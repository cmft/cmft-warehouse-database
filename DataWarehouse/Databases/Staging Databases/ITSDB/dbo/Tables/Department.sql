﻿CREATE TABLE [dbo].[Department](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentName] [nvarchar](50) NULL,
	[DivisionID] [int] NULL,
	[ContactName] [nvarchar](50) NULL,
	[ContactEmail] [nvarchar](50) NULL,
	[ContactTelephone] [nvarchar](50) NULL,
	[Active] [bit] NOT NULL,
	[CostCode] [nvarchar](50) NULL
) ON [PRIMARY]
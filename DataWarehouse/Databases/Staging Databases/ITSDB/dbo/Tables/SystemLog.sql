﻿CREATE TABLE [dbo].[SystemLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LogTime] [datetime] NULL,
	[LogType] [nvarchar](50) NULL,
	[LogDescription] [nvarchar](max) NULL,
	[IPAddress] [nvarchar](50) NULL,
	[Username] [nvarchar](100) NULL,
	[SystemModule] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
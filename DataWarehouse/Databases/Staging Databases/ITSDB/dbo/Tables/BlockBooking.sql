﻿CREATE TABLE [dbo].[BlockBooking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RefNo] [nvarchar](50) NULL,
	[DWMY] [nvarchar](50) NULL,
	[cMonday] [bit] NULL,
	[cTuesday] [bit] NULL,
	[cWednesday] [bit] NULL,
	[cThursday] [bit] NULL,
	[cFriday] [bit] NULL,
	[cSaturday] [bit] NULL,
	[cSunday] [bit] NULL,
	[Occurrences] [int] NULL
) ON [PRIMARY]
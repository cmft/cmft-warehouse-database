﻿CREATE TABLE [dbo].[Settings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OOHStart] [datetime] NULL,
	[OOHCurrentStart] [datetime] NULL,
	[OOHEnd] [datetime] NULL,
	[OOHActive] [bit] NULL,
	[OOHMessage] [nvarchar](max) NULL,
	[OOHPopupText1] [nvarchar](max) NULL,
	[OOHPopupText2] [nvarchar](max) NULL,
	[EmailMessage] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
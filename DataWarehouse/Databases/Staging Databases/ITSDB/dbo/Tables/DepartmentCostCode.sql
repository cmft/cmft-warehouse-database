﻿CREATE TABLE [dbo].[DepartmentCostCode](
	[DepartmentID] [int] NOT NULL,
	[CostCodeID] [int] NOT NULL,
	[DefaultCode] [bit] NOT NULL
) ON [PRIMARY]
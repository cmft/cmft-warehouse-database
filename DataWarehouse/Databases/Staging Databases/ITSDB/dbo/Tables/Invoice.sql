﻿CREATE TABLE [dbo].[Invoice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceRef] [varchar](50) NULL,
	[ServiceProviderID] [int] NULL,
	[InvoiceDate] [datetime] NULL,
	[Amount] [decimal](10, 2) NULL,
	[AuthorisedBy] [varchar](50) NULL,
	[AuthorisedAt] [datetime] NULL
) ON [PRIMARY]
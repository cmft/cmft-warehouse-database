﻿CREATE TABLE [dbo].[InterpreterType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InterpreterTypeName] [nvarchar](50) NULL
) ON [PRIMARY]
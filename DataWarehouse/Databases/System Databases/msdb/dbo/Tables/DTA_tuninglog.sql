﻿CREATE TABLE [dbo].[DTA_tuninglog] (
    [SessionID]  INT          NOT NULL,
    [RowID]      INT          NOT NULL,
    [CategoryID] NVARCHAR (4) NOT NULL,
    [Event]      NTEXT        NULL,
    [Statement]  NTEXT        NULL,
    [Frequency]  INT          NOT NULL,
    [Reason]     NTEXT        NULL,
    FOREIGN KEY ([SessionID]) REFERENCES [dbo].[DTA_input] ([SessionID]) ON DELETE CASCADE
);


GO
CREATE CLUSTERED INDEX [DTA_tuninglog_index]
    ON [dbo].[DTA_tuninglog]([SessionID] ASC, [RowID] ASC);


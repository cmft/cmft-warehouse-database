﻿CREATE ROLE [ServerGroupAdministratorRole]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'ServerGroupAdministratorRole', @membername = N'CMMC\Sec - Warehouse BI Developers';


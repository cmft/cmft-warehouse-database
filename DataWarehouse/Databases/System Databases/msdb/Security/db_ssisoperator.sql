﻿CREATE ROLE [db_ssisoperator]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'db_ssisoperator', @membername = N'dc_operator';


GO
EXECUTE sp_addrolemember @rolename = N'db_ssisoperator', @membername = N'dc_proxy';


GO
EXECUTE sp_addrolemember @rolename = N'db_ssisoperator', @membername = N'MS_DataCollectorInternalUser';


﻿CREATE ROLE [ServerGroupReaderRole]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'ServerGroupReaderRole', @membername = N'ServerGroupAdministratorRole';


GO
EXECUTE sp_addrolemember @rolename = N'ServerGroupReaderRole', @membername = N'Readers';


GO
EXECUTE sp_addrolemember @rolename = N'ServerGroupReaderRole', @membername = N'CMMC\Sec - Warehouse BI Developers';


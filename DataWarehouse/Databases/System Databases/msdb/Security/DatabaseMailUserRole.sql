﻿CREATE ROLE [DatabaseMailUserRole]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'DatabaseMailUserRole', @membername = N'MS_DataCollectorInternalUser';


GO
EXECUTE sp_addrolemember @rolename = N'DatabaseMailUserRole', @membername = N'CMMC\SVC-DW-SQLINST1';


GO
EXECUTE sp_addrolemember @rolename = N'DatabaseMailUserRole', @membername = N'Readers';


GO
EXECUTE sp_addrolemember @rolename = N'DatabaseMailUserRole', @membername = N'MailUser';


GO
EXECUTE sp_addrolemember @rolename = N'DatabaseMailUserRole', @membername = N'Dan-MailUser';


GO
EXECUTE sp_addrolemember @rolename = N'DatabaseMailUserRole', @membername = N'tgh.information';


﻿CREATE ROLE [SQLAgentOperatorRole]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentOperatorRole', @membername = N'PolicyAdministratorRole';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentOperatorRole', @membername = N'RSExecRole';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentOperatorRole', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentOperatorRole', @membername = N'CMMC\Rachel.Royston';


﻿CREATE ROLE [db_ssisltduser]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'db_ssisltduser', @membername = N'dc_operator';


GO
EXECUTE sp_addrolemember @rolename = N'db_ssisltduser', @membername = N'dc_proxy';


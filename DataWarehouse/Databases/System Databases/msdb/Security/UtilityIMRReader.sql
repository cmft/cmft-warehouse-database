﻿CREATE ROLE [UtilityIMRReader]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'UtilityIMRReader', @membername = N'UtilityIMRWriter';


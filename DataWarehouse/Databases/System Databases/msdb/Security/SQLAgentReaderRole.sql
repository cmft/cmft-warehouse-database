﻿CREATE ROLE [SQLAgentReaderRole]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentReaderRole', @membername = N'SQLAgentOperatorRole';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentReaderRole', @membername = N'Readers';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentReaderRole', @membername = N'CMMC\Delwyn.Jones';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentReaderRole', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentReaderRole', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentReaderRole', @membername = N'CMMC\Rachel.Royston';


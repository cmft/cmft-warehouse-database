﻿CREATE ROLE [dc_operator]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'dc_operator', @membername = N'dc_admin';


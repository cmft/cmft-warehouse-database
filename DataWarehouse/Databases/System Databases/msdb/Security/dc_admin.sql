﻿CREATE ROLE [dc_admin]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'dc_admin', @membername = N'MS_DataCollectorInternalUser';


﻿CREATE ROLE [SQLAgentUserRole]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentUserRole', @membername = N'SQLAgentReaderRole';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentUserRole', @membername = N'dc_operator';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentUserRole', @membername = N'MS_DataCollectorInternalUser';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentUserRole', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentUserRole', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'SQLAgentUserRole', @membername = N'CMMC\Rachel.Royston';


﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'RSExecRole';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\sec - sp information';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Thomas.Drury';


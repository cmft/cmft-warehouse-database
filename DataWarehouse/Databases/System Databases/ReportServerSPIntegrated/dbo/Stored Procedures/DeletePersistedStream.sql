﻿
CREATE PROCEDURE [dbo].[DeletePersistedStream]
@SessionID varchar(32),
@Index int
AS

delete from [ReportServerSPIntegratedTempDB].dbo.PersistedStream where SessionID = @SessionID and [Index] = @Index

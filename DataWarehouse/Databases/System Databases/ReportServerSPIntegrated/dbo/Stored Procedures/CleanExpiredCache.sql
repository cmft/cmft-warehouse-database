﻿
CREATE PROCEDURE [dbo].[CleanExpiredCache]
AS
SET NOCOUNT OFF
DECLARE @now as datetime
SET @now = DATEADD(minute, -1, GETDATE())

UPDATE SN
SET
   PermanentRefcount = PermanentRefcount - 1
FROM
   [ReportServerSPIntegratedTempDB].dbo.SnapshotData AS SN
   INNER JOIN [ReportServerSPIntegratedTempDB].dbo.ExecutionCache AS EC ON SN.SnapshotDataID = EC.SnapshotDataID
WHERE
   EC.AbsoluteExpiration < @now
   
DELETE EC
FROM
   [ReportServerSPIntegratedTempDB].dbo.ExecutionCache AS EC
WHERE
   EC.AbsoluteExpiration < @now

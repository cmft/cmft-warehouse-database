﻿
CREATE PROCEDURE [dbo].[DeleteExpiredPersistedStreams]
AS
SET NOCOUNT OFF
SET DEADLOCK_PRIORITY LOW
DELETE
    [ReportServerSPIntegratedTempDB].dbo.PersistedStream
FROM 
    (SELECT TOP 1 * FROM [ReportServerSPIntegratedTempDB].dbo.PersistedStream PS2 WHERE PS2.RefCount = 0 AND GETDATE() > PS2.ExpirationDate) AS e1
WHERE 
    e1.SessionID = [ReportServerSPIntegratedTempDB].dbo.PersistedStream.[SessionID] AND
    e1.[Index] = [ReportServerSPIntegratedTempDB].dbo.PersistedStream.[Index]

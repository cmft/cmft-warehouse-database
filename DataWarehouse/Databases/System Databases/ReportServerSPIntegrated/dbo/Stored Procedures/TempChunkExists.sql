﻿
CREATE PROC [dbo].[TempChunkExists]
	@ChunkId uniqueidentifier
AS
BEGIN
	SELECT COUNT(1) FROM [ReportServerSPIntegratedTempDB].dbo.SegmentedChunk
	WHERE ChunkId = @ChunkId
END

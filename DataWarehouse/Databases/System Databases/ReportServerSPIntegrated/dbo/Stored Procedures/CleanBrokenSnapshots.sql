﻿
CREATE PROCEDURE [dbo].[CleanBrokenSnapshots]
@Machine nvarchar(512),
@SnapshotsCleaned int OUTPUT,
@ChunksCleaned int OUTPUT,
@TempSnapshotID uniqueidentifier OUTPUT
AS
    SET DEADLOCK_PRIORITY LOW
    DECLARE @now AS datetime
    SELECT @now = GETDATE()
    
    CREATE TABLE #tempSnapshot (SnapshotDataID uniqueidentifier)
    INSERT INTO #tempSnapshot SELECT TOP 1 SnapshotDataID 
    FROM SnapshotData  WITH (NOLOCK) 
    where SnapshotData.PermanentRefcount <= 0 
    AND ExpirationDate < @now
    SET @SnapshotsCleaned = @@ROWCOUNT

    DELETE ChunkData FROM ChunkData INNER JOIN #tempSnapshot
    ON ChunkData.SnapshotDataID = #tempSnapshot.SnapshotDataID
    SET @ChunksCleaned = @@ROWCOUNT

    DELETE SnapshotData FROM SnapshotData INNER JOIN #tempSnapshot
    ON SnapshotData.SnapshotDataID = #tempSnapshot.SnapshotDataID
    
    TRUNCATE TABLE #tempSnapshot

    INSERT INTO #tempSnapshot SELECT TOP 1 SnapshotDataID 
    FROM [ReportServerSPIntegratedTempDB].dbo.SnapshotData  WITH (NOLOCK) 
    where [ReportServerSPIntegratedTempDB].dbo.SnapshotData.PermanentRefcount <= 0 
    AND [ReportServerSPIntegratedTempDB].dbo.SnapshotData.ExpirationDate < @now
    AND [ReportServerSPIntegratedTempDB].dbo.SnapshotData.Machine = @Machine
    SET @SnapshotsCleaned = @SnapshotsCleaned + @@ROWCOUNT

    SELECT @TempSnapshotID = (SELECT SnapshotDataID FROM #tempSnapshot)

    DELETE [ReportServerSPIntegratedTempDB].dbo.ChunkData FROM [ReportServerSPIntegratedTempDB].dbo.ChunkData INNER JOIN #tempSnapshot
    ON [ReportServerSPIntegratedTempDB].dbo.ChunkData.SnapshotDataID = #tempSnapshot.SnapshotDataID
    SET @ChunksCleaned = @ChunksCleaned + @@ROWCOUNT

    DELETE [ReportServerSPIntegratedTempDB].dbo.SnapshotData FROM [ReportServerSPIntegratedTempDB].dbo.SnapshotData INNER JOIN #tempSnapshot
    ON [ReportServerSPIntegratedTempDB].dbo.SnapshotData.SnapshotDataID = #tempSnapshot.SnapshotDataID

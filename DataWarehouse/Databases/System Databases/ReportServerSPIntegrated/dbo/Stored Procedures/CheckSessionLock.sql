﻿
CREATE PROCEDURE [dbo].[CheckSessionLock]
@SessionID as varchar(32),
@LockVersion  int OUTPUT
AS
DECLARE @Selected nvarchar(32)
SELECT @Selected=SessionID, @LockVersion = LockVersion FROM [ReportServerSPIntegratedTempDB].dbo.SessionLock WITH (ROWLOCK) WHERE SessionID = @SessionID

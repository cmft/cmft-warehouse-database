﻿
CREATE PROCEDURE [dbo].[DeleteDataSets]
@ItemID [uniqueidentifier]
AS
DELETE
FROM [DataSets]
WHERE [ItemID] = @ItemID
DELETE
FROM [ReportServerSPIntegratedTempDB].dbo.TempDataSets
WHERE [ItemID] = @ItemID

﻿
CREATE PROCEDURE [dbo].[GetNextPortionPersistedStream]
@DataPointer binary(16),
@DataIndex int,
@Length int
AS

READTEXT [ReportServerSPIntegratedTempDB].dbo.PersistedStream.Content @DataPointer @DataIndex @Length

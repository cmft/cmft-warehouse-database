﻿
CREATE PROCEDURE [dbo].[DeletePersistedStreams]
@SessionID varchar(32)
AS
SET NOCOUNT OFF
delete 
    [ReportServerSPIntegratedTempDB].dbo.PersistedStream
from 
    (select top 1 * from [ReportServerSPIntegratedTempDB].dbo.PersistedStream PS2 where PS2.SessionID = @SessionID) as e1
where 
    e1.SessionID = [ReportServerSPIntegratedTempDB].dbo.PersistedStream.[SessionID] and
    e1.[Index] = [ReportServerSPIntegratedTempDB].dbo.PersistedStream.[Index]

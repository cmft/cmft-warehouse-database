﻿EXECUTE sp_addsrvrolemember @loginame = N'NT AUTHORITY\SYSTEM', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'NT SERVICE\MSSQLSERVER', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'CMMC\Rob.Kershaw', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'NT SERVICE\SQLSERVERAGENT', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'CMMC\dan.forster', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'dw_reporting', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'CMMC\Data.Warehouse', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'data.warehouse.sql32', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'data.warehouse', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'sp.warehouse', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'CMMC\WAREHOUSE2$', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'CMMC\Jabran.Rafiq', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'CMMC\Sec - Warehouse SQL DBAs', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'tgh.data.warehouse', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'distributor_admin', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'CMMC\Sec - Warehouse Managers', @rolename = N'sysadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'solarwinds_SAM', @rolename = N'serveradmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'solarwinds_SAM', @rolename = N'processadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'solarwinds_SAM', @rolename = N'diskadmin';


GO
EXECUTE sp_addsrvrolemember @loginame = N'solarwinds_SAM', @rolename = N'dbcreator';


GO
EXECUTE sp_addsrvrolemember @loginame = N'solarwinds_SAM', @rolename = N'bulkadmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'John Schofield';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'WHAdmin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'InfounitMembersRO';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\SP-DevTest-Admin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Colin.Hunter';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


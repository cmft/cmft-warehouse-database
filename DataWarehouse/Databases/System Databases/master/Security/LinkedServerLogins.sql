﻿EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'INFOCOM', @useself = N'FALSE', @rmtuser = N'dforster';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CMIS', @useself = N'FALSE', @rmtuser = N'cmisread';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'INFORMATION', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'WAREHOUSE2\SQL32', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'SQL32LEGACY', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'WAREHOUSE2\DEV', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'ICOM', @useself = N'FALSE', @rmtuser = N'dforster';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'ADSI', @useself = N'FALSE', @rmtuser = N'cmmc\dan.forster';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'ACC01', @useself = N'FALSE', @rmtuser = N'Admin';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'SHAREPOINT', @useself = N'FALSE';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'INQUIREITANIUM', @useself = N'FALSE';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'INQUIRE', @useself = N'FALSE', @rmtuser = N'PRD';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'CMMC\dan.forster', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'CMMC\Jabran.Rafiq', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'CMMC\Ian.Connolly', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'CMMC\Gareth.Cunnah', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'CMMC\Helen.Shackleton', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'CMMC\Darren.Griffiths', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'CMMC\Wendy.Collier', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH', @useself = N'FALSE', @locallogin = N'slam.link', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xsqlcluster.xtrafford.nhs.uk';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xsqlcluster.xtrafford.nhs.uk', @useself = N'FALSE', @locallogin = N'CMMC\dan.forster', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xsqlcluster.xtrafford.nhs.uk', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xsqlcluster.xtrafford.nhs.uk', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xsqlcluster.xtrafford.nhs.uk', @useself = N'FALSE', @locallogin = N'CMMC\Jabran.Rafiq', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xsqlcluster.xtrafford.nhs.uk', @useself = N'FALSE', @locallogin = N'tgh.data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'TIETEST';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CMFTED1', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'BORAS-SQL', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'EPMI';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'EPMI', @useself = N'FALSE', @locallogin = N'CMMC\dan.forster', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'EPMI', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'EPMI', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'EPMI', @useself = N'FALSE', @locallogin = N'CMMC\Jabran.Rafiq', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'EPMI', @useself = N'FALSE', @locallogin = N'CMMC\Thomas.Drury', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'TECHDEVSQL', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER', @useself = N'FALSE', @locallogin = N'CMMC\dan.forster', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER', @useself = N'FALSE', @locallogin = N'CMMC\Gareth.Summerfield', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER', @useself = N'FALSE', @locallogin = N'CMMC\Jabran.Rafiq', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER', @useself = N'FALSE', @locallogin = N'CMMC\Gareth.Cunnah', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER', @useself = N'FALSE', @locallogin = N'cmmc\cassian.butterworth', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER', @useself = N'FALSE', @locallogin = N'CMMC\Darren.Griffiths', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CMFT-SP-SQL';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CMMCSQL';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CMFTTFS', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'MEDISOFT', @useself = N'FALSE', @rmtuser = N'MED_UI_USER';


GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQLDEV01\DEV';


--GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'INQUIREPROD', @useself = N'FALSE', @rmtuser = N'PRD';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'BLOODTRACK', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'BEDMANSQLPROD', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CAREPLUS', @useself = N'FALSE';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH.XTRAFFORD.NHS.UK', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH.XTRAFFORD.NHS.UK', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'DWV2-DB-TEST\BIGSCIENCE', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'INFORM-PROD.XCMMC.NHS.UK';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'SOMERSETCANCER', @useself = N'FALSE', @rmtuser = N'scr.dwaccess';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xepr';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xepr', @useself = N'FALSE', @locallogin = N'CMMC\dan.forster', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xepr', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xepr', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xepr', @useself = N'FALSE', @locallogin = N'CMMC\Jabran.Rafiq', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'xepr', @useself = N'FALSE', @locallogin = N'CMMC\Gareth.Cunnah', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'WAREHOUSELEGACY', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'MEDISEC', @useself = N'FALSE', @rmtuser = N'chameleon';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XICESQL', @useself = N'FALSE', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XICESQL', @useself = N'FALSE', @locallogin = N'CMMC\dan.forster', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XICESQL', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XICESQL', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XICESQL', @useself = N'FALSE', @locallogin = N'CMMC\Jabran.Rafiq', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XICESQL', @useself = N'FALSE', @locallogin = N'CMMC\Gareth.Cunnah', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XICESQL.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER1';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER1', @useself = N'FALSE', @locallogin = N'CMMC\dan.forster', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER1', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER1', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER1', @useself = N'FALSE', @locallogin = N'CMMC\Jabran.Rafiq', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER1', @useself = N'FALSE', @locallogin = N'CMMC\Gareth.Cunnah', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XSQLCLUSTER1', @useself = N'FALSE', @locallogin = N'slam.link', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH2';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH2', @useself = N'FALSE', @locallogin = N'CMMC\dan.forster', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH2', @useself = N'FALSE', @locallogin = N'CMMC\Data.Warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH2', @useself = N'FALSE', @locallogin = N'data.warehouse', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH2', @useself = N'FALSE', @locallogin = N'CMMC\Jabran.Rafiq', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH2', @useself = N'FALSE', @locallogin = N'CMMC\Gareth.Cunnah', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'XDATAWH2', @useself = N'FALSE', @locallogin = N'slam.link', @rmtuser = N'cmft.data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'GMTIS', @useself = N'FALSE', @rmtuser = N'CMFTCOMSQL';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMPS', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMPSPROD', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CMFTSLAM', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQLTEST02\QA';


--GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQLTEST01\UAT';


--GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQLTEST01\DEMO';


--GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQLTEST01\PREPROD', @useself = N'FALSE', @rmtuser = N'data.warehouse';


--GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQLTEST01\TRAINING';


--GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQL01';


--GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQL02';


--GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'DWV2-DB-TEST', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'PERFORMANCERS', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMPREPROD', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'WAREHOUSERS01', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CRM', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CMFTWEB2', @useself = N'FALSE', @rmtuser = N'IPAdmin';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'DAWN', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CAREPLUSNEW', @useself = N'FALSE';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'INQUIREQCD', @useself = N'FALSE';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CMIS_TEST', @useself = N'FALSE', @rmtuser = N'CMIS_read';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'ICESQLMIRROR', @useself = N'FALSE', @rmtuser = N'dwice';


GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQL01\CHAMELEON';


--GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'CHAMSQL02\CHAMELEON';


--GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'SQL.PREPROD.CHAMELEON.XCMMC.NHS.UK\PREPROD', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'SLAM-DEV', @useself = N'FALSE', @rmtuser = N'data.warehouse';


GO
--EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'ASCRIBEDBMIRROR';


--GO
EXECUTE sp_addlinkedsrvlogin @rmtsrvname = N'AscribeDBMirrorExec', @useself = N'FALSE', @rmtuser = N'data.warehouse';


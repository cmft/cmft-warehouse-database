﻿CREATE LOGIN [CMMC\Sec - Warehouse SQL MetOffice Editors]
    FROM WINDOWS WITH DEFAULT_DATABASE = [MetOffice], DEFAULT_LANGUAGE = [us_english];


﻿CREATE SERVER AUDIT [Audit-20110516-132037] TO FILE (FILEPATH = N'\\warehousefs\WarehouseSQL_Audit\', MAXSIZE = 5 MB, MAX_ROLLOVER_FILES = 100, RESERVE_DISK_SPACE = ON) WITH (AUDIT_GUID = 'd9abe93f-d068-4cd2-8036-ce567dfe97ce');


GO
ALTER SERVER AUDIT [Audit-20110516-132037] WITH (STATE = ON);


﻿EXECUTE sp_addlinkedserver @server = N'INFOCOM', @srvproduct = N'Sybase ASE', @provider = N'ASEOLEDB', @datasrc = N'InfocomDB';


GO
EXECUTE sp_addlinkedserver @server = N'CMIS', @srvproduct = N'CMIS', @provider = N'OraOLEDB.Oracle', @datasrc = N'MATL', @provstr = N'Provider=OraOLEDB.Oracle;Data Source=(DESCRIPTION=(CID=MATL)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=CMIS)(PORT=1521)))(CONNECT_DATA=(SID=MATL)(SERVER=DEDICATED)));User Id=cmisread;Password=cmisread1;';


GO
EXECUTE sp_serveroption @server = N'CMIS', @optname = N'use remote collation', @optvalue = N'FALSE';


GO
EXECUTE sp_addlinkedserver @server = N'INFORMATION', @srvproduct = N'SQL Server';


GO
EXECUTE sp_serveroption @server = N'INFORMATION', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'INFORMATION', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'WAREHOUSE2\SQL32', @srvproduct = N'SQL Server';


GO
EXECUTE sp_serveroption @server = N'WAREHOUSE2\SQL32', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'WAREHOUSE2\SQL32', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'SQL32LEGACY', @srvproduct = N'SQLOLEDB.1', @provider = N'SQLNCLI10', @datasrc = N'WAREHOUSE2\SQL32', @catalog = N'Legacy';


GO
EXECUTE sp_serveroption @server = N'SQL32LEGACY', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'SQL32LEGACY', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'WAREHOUSE2\DEV', @srvproduct = N'SQL Server';


GO
EXECUTE sp_serveroption @server = N'WAREHOUSE2\DEV', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'WAREHOUSE2\DEV', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'ICOM', @srvproduct = N'Sybase ASE', @provider = N'ASEOLEDB', @datasrc = N'InfocomDB';


GO
EXECUTE sp_serveroption @server = N'ICOM', @optname = N'lazy schema validation', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'ADSI', @srvproduct = N'Active Directory Services 2.5', @provider = N'ADSDSOObject', @datasrc = N'adsdatasource';


GO
EXECUTE sp_addlinkedserver @server = N'ACC01', @srvproduct = N'Microsoft Access', @provider = N'Microsoft.ACE.OLEDB.12.0', @datasrc = N'\\warehousefs\SmallDatasets\SCAT\RoutinePatientDatabase.mdb';


GO
EXECUTE sp_addlinkedserver @server = N'SHAREPOINT', @srvproduct = N'Sharepoint Access', @provider = N'Microsoft.ACE.OLEDB.12.0', @datasrc = N'\\warehousefs\SHAREPOINTDATA$\SPGateway.accdb';


GO
EXECUTE sp_addlinkedserver @server = N'INQUIREITANIUM', @srvproduct = N'Intersystems CacheODBC', @provider = N'MSDASQL', @datasrc = N'InquireItanium';


GO
EXECUTE sp_addlinkedserver @server = N'INQUIRE', @srvproduct = N'Intersystems CacheODBC', @provider = N'MSDASQL', @datasrc = N'INQUIREITANIUM';


GO
EXECUTE sp_addlinkedserver @server = N'XDATAWH', @srvproduct = N'SQLServer', @provider = N'SQLNCLI10', @datasrc = N'XDATAWH.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_serveroption @server = N'XDATAWH', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'XDATAWH', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'xsqlcluster.xtrafford.nhs.uk', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'TIETEST', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'CMFTED1', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'BORAS-SQL', @srvproduct = N'SQL Server';


GO
EXECUTE sp_serveroption @server = N'BORAS-SQL', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'BORAS-SQL', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'EPMI', @srvproduct = N'', @provider = N'SQLNCLI10', @datasrc = N'EPMI01.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_addlinkedserver @server = N'TECHDEVSQL', @srvproduct = N'SQL Server';


GO
EXECUTE sp_serveroption @server = N'TECHDEVSQL', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'TECHDEVSQL', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'XSQLCLUSTER', @srvproduct = N'SQLServer', @provider = N'SQLNCLI10', @datasrc = N'XSQLCLUSTER.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_serveroption @server = N'XSQLCLUSTER', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'XSQLCLUSTER', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'CMFT-SP-SQL', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'CMMCSQL', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'CMFTTFS', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'MEDISOFT', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'INQUIREPROD', @srvproduct = N'Intersystems CacheODBC', @provider = N'MSDASQL', @datasrc = N'INQUIREPROD';


GO
EXECUTE sp_addlinkedserver @server = N'BLOODTRACK', @srvproduct = N'SQL Server';


GO
EXECUTE sp_serveroption @server = N'BLOODTRACK', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'BEDMANSQLPROD', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'CAREPLUS', @srvproduct = N'Intersystems CacheODBC', @provider = N'MSDASQL', @datasrc = N'CarePlus';


GO
EXECUTE sp_addlinkedserver @server = N'XDATAWH.XTRAFFORD.NHS.UK', @srvproduct = N'XDATAWH.XTRAFFORD.NHS.UK', @provider = N'SQLNCLI';


GO
EXECUTE sp_addlinkedserver @server = N'DWV2-DB-TEST\BIGSCIENCE', @srvproduct = N'SQLOLEDB.1', @provider = N'SQLNCLI10', @datasrc = N'DWV2-DB-TEST\BIGSCIENCE', @catalog = N'master';


GO
EXECUTE sp_serveroption @server = N'DWV2-DB-TEST\BIGSCIENCE', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'DWV2-DB-TEST\BIGSCIENCE', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'INFORM-PROD.XCMMC.NHS.UK', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'SOMERSETCANCER', @srvproduct = N'SQLServer', @provider = N'SQLNCLI10', @datasrc = N'SCR.XCMMC.NHS.UK';


GO
EXECUTE sp_serveroption @server = N'SOMERSETCANCER', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'SOMERSETCANCER', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'xepr', @srvproduct = N'', @provider = N'SQLNCLI10', @datasrc = N'xepr.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_addlinkedserver @server = N'WAREHOUSELEGACY', @srvproduct = N'SQLSERVER', @provider = N'SQLNCLI10', @datasrc = N'WAREHOUSELEGACY', @catalog = N'RTT';


GO
EXECUTE sp_serveroption @server = N'WAREHOUSELEGACY', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'WAREHOUSELEGACY', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'MEDISEC', @srvproduct = N'SQLServer', @provider = N'SQLNCLI10', @datasrc = N'CMFT-MEDISEC';


GO
EXECUTE sp_addlinkedserver @server = N'XICESQL', @srvproduct = N'SQLServer', @provider = N'SQLNCLI10', @datasrc = N'XICESQL.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_addlinkedserver @server = N'XICESQL.XTRAFFORD.NHS.UK', @srvproduct = N'Sqlserver', @provider = N'SQLNCLI10', @datasrc = N'XICESQL.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_addlinkedserver @server = N'XSQLCLUSTER1', @srvproduct = N'SQLServer', @provider = N'SQLNCLI10', @datasrc = N'XSQLCLUSTER.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_serveroption @server = N'XSQLCLUSTER1', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'XSQLCLUSTER1', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'XDATAWH2', @srvproduct = N'SQLServer', @provider = N'SQLNCLI10', @datasrc = N'XDATAWH2.XTRAFFORD.NHS.UK';


GO
EXECUTE sp_serveroption @server = N'XDATAWH2', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'XDATAWH2', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'GMTIS', @srvproduct = N'SQLServer', @provider = N'SQLNCLI10', @datasrc = N'db.tisdw.nhs.uk';


GO
EXECUTE sp_addlinkedserver @server = N'CHAMPS', @srvproduct = N'', @provider = N'SQLNCLI', @datasrc = N'crmdevtest01';


GO
EXECUTE sp_addlinkedserver @server = N'CHAMPSPROD', @srvproduct = N'', @provider = N'SQLNCLI', @datasrc = N'crm01';


GO
EXECUTE sp_addlinkedserver @server = N'CMFTSLAM', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'DWV2-DB-TEST', @srvproduct = N'SQLOLEDB.1', @provider = N'SQLNCLI10', @datasrc = N'DWV2-DB-TEST', @catalog = N'master';


GO
EXECUTE sp_serveroption @server = N'DWV2-DB-TEST', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'DWV2-DB-TEST', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'PERFORMANCERS', @srvproduct = N'SQLOLEDB.1', @provider = N'SQLNCLI10', @datasrc = N'PERFORMANCERS', @catalog = N'master';


GO
EXECUTE sp_serveroption @server = N'PERFORMANCERS', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'PERFORMANCERS', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'CHAMPREPROD', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'WAREHOUSERS01', @srvproduct = N'SQLOLEDB.1', @provider = N'SQLNCLI10', @datasrc = N'WAREHOUSERS01', @catalog = N'master';


GO
EXECUTE sp_serveroption @server = N'WAREHOUSERS01', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'WAREHOUSERS01', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'CRM', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'CMFTWEB2', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'DAWN', @srvproduct = N'XSTORAGE.XTRAFFORD.NHS.UK\DAWN', @provider = N'SQLNCLI', @datasrc = N'XSTORAGE.XTRAFFORD.NHS.UK\DAWN', @catalog = N'dawnac';


GO
EXECUTE sp_serveroption @server = N'DAWN', @optname = N'collation compatible', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'DAWN', @optname = N'lazy schema validation', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'CAREPLUSNEW', @srvproduct = N'Intersystems CacheODBC', @provider = N'MSDASQL', @datasrc = N'CAREPLUSNEW';


GO
EXECUTE sp_addlinkedserver @server = N'INQUIREQCD', @srvproduct = N'Intersystems CacheODBC', @provider = N'MSDASQL', @datasrc = N'QCD';


GO
EXECUTE sp_addlinkedserver @server = N'CMIS_TEST', @srvproduct = N'CMIS', @provider = N'OraOLEDB.Oracle', @datasrc = N'MATQ', @provstr = N'Provider=OraOLEDB.Oracle;Data Source=(DESCRIPTION=(CID=MATQ)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=CMIS)(PORT=1521)))(CONNECT_DATA=(SID=MATQ)(SERVER=DEDICATED)));User Id=CMIS_read;Password=CMIS_read1;';


GO
EXECUTE sp_serveroption @server = N'CMIS_TEST', @optname = N'use remote collation', @optvalue = N'FALSE';


GO
EXECUTE sp_addlinkedserver @server = N'ICESQLMIRROR', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'SQL.PREPROD.CHAMELEON.XCMMC.NHS.UK\PREPROD', @srvproduct = N'SQL Server';


GO
EXECUTE sp_serveroption @server = N'SQL.PREPROD.CHAMELEON.XCMMC.NHS.UK\PREPROD', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'SQL.PREPROD.CHAMELEON.XCMMC.NHS.UK\PREPROD', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'SLAM-DEV', @srvproduct = N'SQL Server';


GO
EXECUTE sp_serveroption @server = N'SLAM-DEV', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'SLAM-DEV', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'AscribeDBMirrorExec', @srvproduct = N'', @provider = N'SQLNCLI', @datasrc = N'ascribedbmirror';


GO
EXECUTE sp_serveroption @server = N'AscribeDBMirrorExec', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'AscribeDBMirrorExec', @optname = N'rpc out', @optvalue = N'TRUE';


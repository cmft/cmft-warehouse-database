﻿CREATE proc dbo.getACUSourceSQL @tableName sysname
as
begin
  DECLARE @sql nvarchar(max);
  
  declare @cols table (idx int identity(1,1) not null, columnExpression nvarchar(128))
  insert @cols(columnExpression)
  select case 
		 when type_name!='VARCHAR' then '{fn CONVERT("' + column_name + '",SQL_CHAR)} "' + column_name+'"' 
		 else '"' + column_name + '"' end columnExpression
from DataDictionary.dbo.columns 
where dbid = 30  
AND table_name = @tableName

declare @maxCol int = (select MAX(idx) from @cols);
declare @curCol int = 2;
select @sql = 'select ' + columnExpression
from @cols where idx = 1;

while @curCol<=@maxCol begin
select @sql = @sql + ',' + columnExpression
from @cols where idx = @curCol;
	
  set @curCol+=1
end;

select @sql += ' from ' + @tableName;

select replace(@sql,'"','&quot;');

end;

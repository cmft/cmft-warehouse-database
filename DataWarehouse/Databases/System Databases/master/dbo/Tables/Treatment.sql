﻿CREATE TABLE [dbo].[Treatment] (
    [pkiTreatmentID]                INT           NOT NULL,
    [fkiPatientID]                  INT           NOT NULL,
    [fkiTreatmentPlanID]            INT           NOT NULL,
    [dRecommendedApptDate]          DATETIME      NULL,
    [dPlannedApptDate]              DATETIME      NULL,
    [dINRDate]                      DATETIME      NOT NULL,
    [dRecommendedNextApptDate]      DATETIME      NULL,
    [nINR]                          FLOAT (53)    NOT NULL,
    [nDose]                         FLOAT (53)    NOT NULL,
    [fkiRegimeID]                   INT           NULL,
    [nTestFlag]                     INT           NOT NULL,
    [nCalcDose]                     FLOAT (53)    NULL,
    [nCalcFlag]                     INT           NULL,
    [fkiAlgorithmID]                INT           NULL,
    [mTreatmentNotes]               TEXT          COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cTreatmentNotesBrief]          VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [nTreatmentNotesImportant]      INT           NOT NULL,
    [lWarnFlag]                     BIT           NOT NULL,
    [dCalcNextTestDate]             DATETIME      NULL,
    [lSeeDrFlag]                    BIT           NOT NULL,
    [lSeenDrFlag]                   BIT           NOT NULL,
    [lReviewFlag]                   BIT           NOT NULL,
    [lInRangeFlag]                  BIT           NOT NULL,
    [nDayOnTreatment]               INT           NULL,
    [cSampleID]                     VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [nMissOrBoostDose]              FLOAT (53)    NULL,
    [nMissOrBoostDays]              INT           NULL,
    [cMissOrBoostInstruction]       VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [nCalcMissOrBoostDose]          FLOAT (53)    NULL,
    [nCalcMissOrBoostDays]          INT           NULL,
    [lInPatient]                    BIT           NOT NULL,
    [nContactStatus]                INT           NULL,
    [fkiClinicID]                   INT           NULL,
    [nDosingPatternOffSet]          INT           NULL,
    [fkiAuthorisedHCprofessionalID] INT           NULL,
    [lAuthorised]                   BIT           NOT NULL,
    [dAuthorisedDateTime]           DATETIME      NULL,
    [cDosingInstructionFull]        TEXT          COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cDosingInstructionPartial]     VARCHAR (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [nDayPattern]                   INT           NOT NULL,
    [nDaysSinceLastTreatment]       FLOAT (53)    NULL,
    [nDaysInRange]                  FLOAT (53)    NULL,
    [nDaysBelowRange]               FLOAT (53)    NULL,
    [nDaysAboveRange]               FLOAT (53)    NULL,
    [fkiResultRangeID]              INT           NULL,
    [cStatus]                       VARCHAR (16)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [mStatusHistory]                TEXT          COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cSubStatus]                    VARCHAR (16)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cAcknowledgedWarnings]         VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [nPreviousINR]                  FLOAT (53)    NULL,
    [nNonAttendanceCount]           INT           NOT NULL,
    [fkiClinicSlotDayID]            INT           NULL,
    [lClinicSlotDayReserveCapUsed]  BIT           NOT NULL,
    [iACTherapy]                    INT           NOT NULL,
    [nPreviousDose]                 FLOAT (53)    NULL,
    [lCustomizedWeekPattern]        BIT           NOT NULL,
    [nDailyWeekly]                  INT           NOT NULL,
    [mCustomInstructions]           TEXT          COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [fkiLMWHdrugID]                 INT           NULL,
    [iDaysInInstruction]            INT           NULL,
    [nCustomizationLevel]           INT           NOT NULL,
    [nActualDose]                   FLOAT (53)    NULL,
    [cAutoAuthorisationResult]      VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [nPredictedINR]                 FLOAT (53)    NULL,
    [fkiPredictedResultRangeID]     INT           NULL
);


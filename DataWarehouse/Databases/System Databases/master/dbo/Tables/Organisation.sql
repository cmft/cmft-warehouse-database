﻿CREATE TABLE [dbo].[Organisation] (
    [pkiOrganisationID]     INT           NOT NULL,
    [fkiOrganisationTypeID] INT           NOT NULL,
    [cName]                 VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [cAddress1]             VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cAddress2]             VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cTown]                 VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cCounty]               VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cPostcode]             VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cTelephone]            VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cFax]                  VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [cEmail]                VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [fkiHealthAuthorityID]  INT           NOT NULL,
    [lInUse]                BIT           NOT NULL,
    [cOrganisationCode]     VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL
);

